<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers;

use CommunecterController;

/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class CoController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions()
	{
	    return array(
	        'init'  => 'survey.controllers.actions.openForm.InitAction',
	        
	        'index'  => \PixelHumain\PixelHumain\modules\survey\controllers\actions\IndexAction::class,
	        
	        'form'  => \PixelHumain\PixelHumain\modules\survey\controllers\actions\FormAction::class,
	        'forms'  => \PixelHumain\PixelHumain\modules\survey\controllers\actions\FormsAction::class,
	        'edit'  => \PixelHumain\PixelHumain\modules\survey\controllers\actions\EditAction::class,
	        'new'  => \PixelHumain\PixelHumain\modules\survey\controllers\actions\NewAction::class,
	        'save'  => \PixelHumain\PixelHumain\modules\survey\controllers\actions\SaveAction::class,
	        'update'=> \PixelHumain\PixelHumain\modules\survey\controllers\actions\UpdateAction::class,
	        'update2'=> \PixelHumain\PixelHumain\modules\survey\controllers\actions\Update2Action::class,
	        'delete'  => \PixelHumain\PixelHumain\modules\survey\controllers\actions\DeleteAction::class,
	        'roles'=> \PixelHumain\PixelHumain\modules\survey\controllers\actions\RolesAction::class,
	        'tags'=> \PixelHumain\PixelHumain\modules\survey\controllers\actions\TagsAction::class,
	        'step'=> \PixelHumain\PixelHumain\modules\survey\controllers\actions\StepAction::class,
	        'logs'=> \PixelHumain\PixelHumain\modules\survey\controllers\actions\LogsAction::class,
	        'answers'  => \PixelHumain\PixelHumain\modules\survey\controllers\actions\AnswersAction::class,
	        'answer'  => \PixelHumain\PixelHumain\modules\survey\controllers\actions\AnswerAction::class,
	        'risk'  => \PixelHumain\PixelHumain\modules\survey\controllers\actions\RiskAction::class,
	        'action'  => \PixelHumain\PixelHumain\modules\survey\controllers\actions\ActionAction::class,
	        'active'  => \PixelHumain\PixelHumain\modules\survey\controllers\actions\ActiveAction::class,
	        'searchadminform'  => \PixelHumain\PixelHumain\modules\survey\controllers\actions\SearchAdminFormAction::class,
	        'updatedocumentids'=> \PixelHumain\PixelHumain\modules\survey\controllers\actions\UpdateDocumentIdsAction::class,
	        'admin'=> \PixelHumain\PixelHumain\modules\survey\controllers\actions\AdminAction::class,
	        'members'=> \PixelHumain\PixelHumain\modules\survey\controllers\actions\MembersAction::class,
	        'searchadminmembers' => \PixelHumain\PixelHumain\modules\survey\controllers\actions\SearchAdminMembersAction::class,
	        'updatepriorisation'=> \PixelHumain\PixelHumain\modules\survey\controllers\actions\UpdatePriorisationAction::class,
	        'pdf'=> \PixelHumain\PixelHumain\modules\survey\controllers\actions\PdfAction::class,
	        'switch'=> \PixelHumain\PixelHumain\modules\survey\controllers\actions\SwitchAction::class,
	        'generate'=> \PixelHumain\PixelHumain\modules\survey\controllers\actions\GenerateAction::class,
	    );
	}

}
