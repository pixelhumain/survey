<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers;

use CommunecterController;

class AnswerController extends CommunecterController {


    public function beforeAction($action) {
        parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions()
	{
	    return array(
	        'new'  => \PixelHumain\PixelHumain\modules\survey\controllers\answer\NewAction::class,
	        'index'  => \PixelHumain\PixelHumain\modules\survey\controllers\answer\IndexAction::class,
	        'all' => \PixelHumain\PixelHumain\modules\graph\controllers\actions\ctenat\AllAction::class,
	        'admindirectory' => \PixelHumain\PixelHumain\modules\survey\controllers\answer\DirectoryAction::class,
	        'directory'=>\PixelHumain\PixelHumain\modules\survey\controllers\answer\DirectoryAction::class,
	        'dashboard'=>\PixelHumain\PixelHumain\modules\survey\controllers\answer\DashboardAction::class,
            'graphbuilder'=>\PixelHumain\PixelHumain\modules\survey\controllers\answer\GraphbuilderAction::class,
            'observatory'=>\PixelHumain\PixelHumain\modules\survey\controllers\answer\ObservatoryAction::class,
	        'ocecoformdashboard'=>\PixelHumain\PixelHumain\modules\survey\controllers\answer\OcecoformdashboardAction::class,
	        'ocecoorgadashboard'=>\PixelHumain\PixelHumain\modules\survey\controllers\answer\OcecoorgadashboardAction::class,
	        'get'  => \PixelHumain\PixelHumain\modules\survey\controllers\answer\GetAction::class,
	        'sendmail'  => \PixelHumain\PixelHumain\modules\survey\controllers\answer\mail\SendAction::class,
	        'validate'  => \PixelHumain\PixelHumain\modules\survey\controllers\answer\ValidateAction::class,
	        "reloadinput" => \PixelHumain\PixelHumain\modules\survey\controllers\answer\ReloadinputAction::class,
	        "reloadwizard" => \PixelHumain\PixelHumain\modules\survey\controllers\answer\ReloadwizardAction::class,
	        "newanswer" => \PixelHumain\PixelHumain\modules\survey\controllers\answer\NewanswerAction::class,
			"aapevaluate" => \PixelHumain\PixelHumain\modules\survey\controllers\answer\AapEvaluateAction::class,
            "answerwithemail" => \PixelHumain\PixelHumain\modules\survey\controllers\answer\AnswerwithemailAction::class,
            "getinput" => \PixelHumain\PixelHumain\modules\survey\controllers\answer\GetinputAction::class,
            "countvisit" => \PixelHumain\PixelHumain\modules\survey\controllers\answer\CountvisitAction::class,
            "getstep" => \PixelHumain\PixelHumain\modules\survey\controllers\answer\GetstepAction::class,
            "getparamswizard" => \PixelHumain\PixelHumain\modules\survey\controllers\answer\GetParamsWizardAction::class,
            "updatewizard" => \PixelHumain\PixelHumain\modules\survey\controllers\answer\UpdateWizardAction::class,
            "answernotification" => \PixelHumain\PixelHumain\modules\survey\controllers\answer\AnswerNotificationAction::class,
            "addproposition" => \PixelHumain\PixelHumain\modules\survey\controllers\answer\AddpropositionAction::class,
            "importanswer" => \PixelHumain\PixelHumain\modules\survey\controllers\answer\ImportanswerAction::class,
            "rcnotification" => \PixelHumain\PixelHumain\modules\survey\controllers\answer\RcnotificationAction::class,
            "multidashboard" => \PixelHumain\PixelHumain\modules\survey\controllers\answer\MultiorgadashboardAction::class,
            "transferanswer" => \PixelHumain\PixelHumain\modules\survey\controllers\answer\TransferAnswerAction::class,
            "actionlist" => \PixelHumain\PixelHumain\modules\survey\controllers\answer\ActionlistAction::class,
			"renamekey" => \PixelHumain\PixelHumain\modules\survey\controllers\answer\RenamekeyAction::class,
			"commontable" =>  \PixelHumain\PixelHumain\modules\survey\controllers\answer\CommonTableAction::class,
			// "useranswers" => \PixelHumain\PixelHumain\modules\survey\controllers\answer\UserAnswersAction::class,
			"checkemailuser" => \PixelHumain\PixelHumain\modules\survey\controllers\answer\CheckEmailUserAction::class,
            "answer" => \PixelHumain\PixelHumain\modules\survey\controllers\answer\AnswerAction::class,
            "gettotalfinancement" => \PixelHumain\PixelHumain\modules\survey\controllers\answer\GetTotalFinancementAction::class,
            "getparamsfinancerstandalone" => \PixelHumain\PixelHumain\modules\survey\controllers\answer\GetParmsFinancerStandaloneAction::class,
			"managelog" => \PixelHumain\PixelHumain\modules\survey\controllers\answer\ManageLogActions::class,

        );
	}
	public function actionViews($tpl="survey.views.tpls.forms.cplx.answers"){
		// Rest::json($_POST);exit;
        return $this->renderPartial($tpl, $_POST, true);
	}
}
