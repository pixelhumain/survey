<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers;

use CommunecterController;
class FormController extends CommunecterController {


    public function beforeAction($action) {
        parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions()
	{
	    return array(
	        'create'			=> 'survey.controllers.form.CreateAction',
	        'edit'				=> \PixelHumain\PixelHumain\modules\survey\controllers\form\EditAction::class,
	        'get'				=> \PixelHumain\PixelHumain\modules\survey\controllers\form\GetAction::class,
	        'getallparent'		=> \PixelHumain\PixelHumain\modules\survey\controllers\form\GetAllParentAction::class,
	        'getopenform'		=> \PixelHumain\PixelHumain\modules\survey\controllers\form\GetOpenFormAction::class,
	        'delete'			=> \PixelHumain\PixelHumain\modules\survey\controllers\form\DeleteAction::class,
	        'admindirectory'	=>'survey.controllers.form.admin.DirectoryAction',
			'schema'			=> \PixelHumain\PixelHumain\modules\survey\controllers\form\SchemaAction::class,
			'generateinputs'	=> \PixelHumain\PixelHumain\modules\survey\controllers\form\GenerateinputsAction::class,
			'duplicatetemplate'	=> \PixelHumain\PixelHumain\modules\survey\controllers\form\DuplicateTemplateAction::class,
			'generateproject'	=> \PixelHumain\PixelHumain\modules\survey\controllers\ocecoform\GenerateprojectAction::class,
			'linkproject'		=> \PixelHumain\PixelHumain\modules\survey\controllers\ocecoform\LinkProjectAction::class,
            'getaapconfig'		=> \PixelHumain\PixelHumain\modules\survey\controllers\ocecoform\GetAction::class,
			'sendemail'			=> \PixelHumain\PixelHumain\modules\survey\controllers\form\SendEmailAction::class,
            'generatedepense'	=> \PixelHumain\PixelHumain\modules\survey\controllers\ocecoform\GeneratedepenseAction::class,
            'templatizeform'	=> \PixelHumain\PixelHumain\modules\survey\controllers\form\TemplatizeformAction::class,
            'getaapview'		=> \PixelHumain\PixelHumain\modules\survey\controllers\ocecoform\GetaapviewAction::class,
            'checkfeedback' 	=> \PixelHumain\PixelHumain\modules\survey\controllers\form\CheckFeedbackAction::class,
			'connectlink'		=> \PixelHumain\PixelHumain\modules\survey\controllers\ocecoform\ConnectLinkAction::class,
            'coremudashboard'	=> \PixelHumain\PixelHumain\modules\survey\controllers\ocecoform\CoremudashboardAction::class,
            'coremufinancement'	=> \PixelHumain\PixelHumain\modules\survey\controllers\ocecoform\CoremufinancementAction::class,
            'getaaprole'		=> \PixelHumain\PixelHumain\modules\survey\controllers\ocecoform\GetaaproleAction::class,
            'pingupdate'		=> \PixelHumain\PixelHumain\modules\survey\controllers\ocecoform\OnUpdatePingAction::class,
            "switchcoremu"		=> \PixelHumain\PixelHumain\modules\survey\controllers\form\SwitchCoremuAction::class,
			"getinput"			=> \PixelHumain\PixelHumain\modules\survey\controllers\form\InputAction::class,
            'financerlogs'	=> \PixelHumain\PixelHumain\modules\survey\controllers\ocecoform\FinancerLogsAction::class,
            'checkform'         => \PixelHumain\PixelHumain\modules\survey\controllers\form\CheckFormExistAction::class,
            'duplicatefinance'	=> \PixelHumain\PixelHumain\modules\survey\controllers\ocecoform\DuplicateFinanceAction::class,
            'changefintype'	=> \PixelHumain\PixelHumain\modules\survey\controllers\ocecoform\ChangefintypeAction::class,
	    );
	}
}
