<?php


class NotifyAction extends CAction
{
    public function run($id=null, $answer=null, $contextId=null, $contextType=null, $homelink = true)
    {
        //send notification
        Notification::constructNotification(
        //verb
            ActStr::VERB_ADD,
            //author
            [
                "id" => Yii::app()->session["userId"],
                "name" => Yii::app()->session["name"]
            ],
            //target
            [
                "id" => $parentFormId,
                "type" =>$form["parent"][$parentFormId]["type"]
            ],
            //object
            [
                "id" => $form["_id"],
                "type" => Form::COLLECTION
            ],
            //levelType
            Form::COLLECTION,
            //context
            NULL,
            //value
            [
                "id" => $answer["_id"],
                "type" => Answer::COLLECTION
            ]
        );
    }
}