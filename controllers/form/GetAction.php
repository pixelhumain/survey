<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\form;
use Answer;
use CTKAction;
use Form;
use PHDB;
use Rest;
use Slug;
use Yii;

class GetAction extends CTKAction
{
	//if $slug : getd all the forms for an element.slug
	//http://127.0.0.1/survey/form/get/slug/dealAH
	//if $id : opens the forms 
    public function run($slug = null,$form=null, $tpl=null)
    {
    	
    	$controller=$this->getController();
        
    	$params = [  ];
    	if(!empty($slug)){
    		$controller->layout = "//layouts/empty";
    		if( empty($tpl) )
    			$tpl = 'survey.views.tpls.forms.oforms';
	   		$el = Slug::getElementBySlug($slug);
	   		$params = [ "el"=>$el["el"] ];
	   		//$params["forms"] = PHDB::find( Form::COLLECTION, [ "parentSlug"=>$slug ] );
	   		$params["forms"] = PHDB::find( Form::COLLECTION, [ "parent.".$el["id"] => array('$exists' => 1) ] );
	   		$params["forms"] += PHDB::find( Form::COLLECTION, [ "formCommunity.".$el["id"] => array('$exists' => 1) ] );
			//$params["forms"][0]["name"] = "todo COForms name";
			//var_dump($el["el"]["costum"]["form"]); exit;
			if(isset($params["forms"]))
			{
				foreach ( $params["forms"] as $fix => $f ) 
				{
				    $params["forms"][$fix]['canEditForm'] = Form::canAdmin(Yii::app()->session['userId'], $f);
					$formId = "";
					if(isset($f['subForms'])){
						foreach ($f['subForms'] as $ix => $fid) 
						{
							if($formId != "")
								$formId .= "|";
							if (!is_array($fid)){
                                $formId .= $fid;
                            }else {
							    foreach ($fid as $idf => $valuef){
							        if (is_string($idf)){
							            $formId.= $idf;
                                    }else{
							            $formId.= "";
                                    }
                                }
                            }
							$params["forms"][$fix][$fid] = PHDB::findOne( Form::COLLECTION, [ "id"=>$fid ] );
						}
					}
					// $params["forms"][$fix]["answers"] = PHDB::count( Form::ANSWER_COLLECTION, ["formId"=>$formId , "parentSlug" => $slug] );

					$params["forms"][$fix]["answers"] = PHDB::count( Answer::COLLECTION, ["form"=>$fix] );
				}
			}

	   	} 
	   	// IS IT IMPORTANT TO OPEN A FORM WITHOUT AN ANSWER
	   	// else if(!empty($form))
	   	// {
	   	// 	$controller->layout = "//layouts/mainSearch";
	   	// 	$canEdit = false;

	    //     if( isset(Yii::app()->session["userId"])  && isset($controller->costum["contextType"]) && isset($controller->costum["contextId"]) )
	    //         $canEdit = Authorisation::canEditItem(Yii::app()->session["userId"],@$controller->costum["contextType"], @$controller->costum["contextId"]);
	        
	    //     //TODO if no canEdit et test exist then 
	    //       //redirect unTpl 

	    //     $params = ["canEdit" => $canEdit];
	    //     if( isset($test) ){
	    //         $params["tpl"]=$id;
	    //         $params["test"]=$test;
	    //     }

	    //     if(isset($_GET["form"]))
	    //         $params = Form::getDataForAnswer($params);

	    //     $params['el'] = Slug::getElementBySlug( $params["parentForm"]["parentSlug"] )["el"];

	    //     $tpl=(!empty($params["parentForm"]["tpl"])) ? $params["parentForm"]["tpl"] : "survey.views.tpls.forms.formWizard";
	   	// }

	   	//var_dump($params);exit;
	   	if($tpl=="json")
			return Rest::json( $params );
		else if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial( $tpl , $params, true);
        else
         	return $controller->render( $tpl , $params, true);

    }
}