<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\form;
use Answer;
use CAction;
use Form;
use PHDB;
use Yii;

class EditAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id=null, $answer=null, $contextId=null, $contextType=null, $homelink = true)
    {
    	$this->getController()->layout = "//layouts/empty";
        $params = array();

        if(!empty($id) && !empty(Yii::app()->session["userId"]) ){
            $form = PHDB::findOneById( Form::COLLECTION , $id);
            $canEditForm = Form::canAdmin(Yii::app()->session['userId'], $form);
            if($canEditForm === true){
                if(!empty($answer)){
                    $params["answerId"]=$answer;
                    $answer = PHDB::findOneById( Form::ANSWER_COLLECTION, $answer);
                } else {
                    $answer = PHDB::findOne( Form::ANSWER_COLLECTION, array("form"=>$id, "draft" => true));
                    if(empty($answer))
                        $answer = Answer::generateAnswer($form, true);
                    $params["answerId"] = (String) $answer["_id"];
                }

                if ($contextId != null && $contextType != null) {
                    $params["contextId"] = $contextId;
                    $params["contextType"] = $contextType;
                }

                $params["form"] = $form ;
                //Rest::json($params); exit;
                $params = Form::getDataForm($params);
                $params["answer"] = $answer;
                $params["canEditForm"] = $canEditForm;
                $params["canAdminAnswer"] = true;
                $params["canEdit"] = true;
                $params["canSee"] = true;
                $params["isNew"] = false;
                $params["showForm"] = true;
                $params["mode"] = "fa";
                $params["usersAnswered"] = array();
                $tpl=(!empty($form["tpl"])) ? $form["tpl"] : "survey.views.tpls.forms.formWizard";
                //var_dump($params); exit;

                $opalContextId = null;
                $opalContextType = null;
                if ($contextId != null &&  $contextType != null) {
                    $opalContextId = $contextId;
                    $opalContextType = $contextType;
                }  

                if (!$homelink) {
                    echo $this->getController()->renderPartial("survey.views.tpls.answers.header",["parentFormId" => $answer, "adminRight" => true, "opalContextId" => $opalContextId, "opalContextType" => $opalContextType] ,true );

                    echo '<div id="mainDash" class="mainDash">';
                }

                return $this->getController()->renderPartial($tpl,$params );

                if (!$homelink) {
                    echo '</div>';
                }
                
            } else {
                if(Yii::app()->request->isAjaxRequest)
                    return $this->getController()->renderPartial("co2.views.default.unTpl",array("msg"=>Yii::t("common", "You are not allow to access to this answer"),"icon"=>"fa-lock"));
                else
                    return $this->getController()->render("co2.views.default.unTpl",array("msg"=>Yii::t("common", "You are not allow to access to this answer"),"icon"=>"fa-lock"));
            }
        } else {
            if(Yii::app()->request->isAjaxRequest)
                return $this->getController()->renderPartial("co2.views.default.unTpl",array("msg"=>Yii::t("common", "You are not allow to access to this answer"),"icon"=>"fa-lock"));
            else
                return $this->getController()->render("co2.views.default.unTpl",array("msg"=>Yii::t("common", "You are not allow to access to this answer"),"icon"=>"fa-lock"));
        }
	 			
    }
 }
 // Propriétaire
/* [] Rempli un dossier Etape 1 
    ( ) Soit logué / Pas Loggué  
    [ ] Nom prénom (propriétaire)
    [ ] Email (propriétaire)
    [ ] Téléphone (propriétaire)
    [ ] Title 
    [ ] Image
    [ ] Date 
    [ ] Description
    [ ] Opérateur lien 
    [ ] validate
    [ ] Déposé proriétaitre 
 Valide => answer (Pas dopérateur)

// Place au marché
Je mengage =>
    tel opérateur (orga / -> admin)

 // Une fois crée étape 2 
[ ] Rempli step 2 avec le gadjo */