<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\form; 
use CTKAction;
use PHDB;
use Yii;
use Authorisation;
use Form;
use Rest;
use Person;
use Event;
use Project;
use Organization;
use Element;
use Costum;
use Link;

class GetAllParentAction extends CTKAction
{
    public function run()
    {
    	$controller=$this->getController();
        $slugParent = [];
        $allForm= PHDB::find(Form::COLLECTION, array("parent" => array('$exists' => 1)),array("parent"));
        foreach($allForm as $kf => $vf){

            
            if(Form::isFormAdmin($kf)){
                foreach($vf["parent"] as $kp => $vParent){ 
                    $parent = PHDB::findOneById( $vParent["type"] , $kp,["name","slug"]);
                    if($parent)
                        $slugParent[$parent["slug"]] = $parent["name"];
                }
            }
        }
        return Rest::json( $slugParent );
    }
    
    
}