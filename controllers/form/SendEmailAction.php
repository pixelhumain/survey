<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\form;
use CAction;
use Cron;
use Mail;
use Person;
use PHDB;
use Yii;
use function json_encode;

class SendEmailAction extends \PixelHumain\PixelHumain\components\Action
{
    private $DURATION_BEFORE_END = "7 days";
    private $TPL_NOTIF_AAP_END = "notifAapEnd";

    public function run($type=null){
        switch($type){
            case "notifAapEnd":
                $this->sendEmailBeforeEndAap($_POST);
                break;
        }
        return json_encode(["status" => 1]);
    }

    private function sendEmailBeforeEndAap($params)
    {
        $mailsToUpdate = PHDB::find(Cron::COLLECTION, ["tplParams.form.id" => $params["id"]]);
        if ($mailsToUpdate && sizeof($mailsToUpdate) < 0) {

        } else {
            $mailData = array(
                "status" => Cron::STATUS_FUTURE,
                "type" => Cron::TYPE_MAIL,
                "sendOn" => date("Y-m-d", strtotime("-" . $this->DURATION_BEFORE_END, strtotime($params["startDate"]))),
                "tpl" => $this->TPL_NOTIF_AAP_END,
                "subject" => "Cloture de l'appel à projet " . $params["name"],
                "from" => Yii::app()->params['adminEmail'],
                "tplParams" => array(
                    "form" => [
                        "id" => $params["id"]
                    ],
                    "name" => $params["name"],
                    "description" => $params["description"],
                    "parent" => $params["parent"],
                    "startDate" => $params["startDate"],
                    "endDate" => $params["endDate"],
                    "logo" => Yii::app()->params["logoUrl"],
                    "logo2" => Yii::app()->params["logoUrl2"]
                )
            );
            $members = $this->getMembers($params["parent"]["id"], $params["parent"]["type"]);
            foreach ($members as $member) {
                $mailData["to"] = $member["email"];
                Mail::schedule($mailData);
            }
        }
    }

    private function getMembers($id, $type)
    {
        $el = PHDB::findOneById($type, $id);
        $members = [];
        if ($el) {
            foreach ($el["links"]["members"] as $memberId => $memberInfo)
                $members[] = PHDB::findOneById(Person::COLLECTION, $memberId);
        }
        return $members;
    }
}