<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\form;

use CTKAction;
use Form;
use PHDB;
use Yii;
use Rest;

class DuplicateTemplateAction extends \PixelHumain\PixelHumain\components\Action { 
	public function run($type=null,$templateCostumId=null,$active=null) {	
    	$controller = $this->getController();
        //var_dump($templateCostumId);exit;

    	$formParent = PHDB::findOne(Form::COLLECTION,array("parent.".$templateCostumId => array('$exists' => true),"type" => $type));

    	//var_dump($formParent);exit;

    	unset($formParent["_id"]);
        unset($formParent["type"]);

    	//var_dump($controller->costum);exit;
    	$dupForm = $formParent;
        $dupForm["created"] = time();
        $dupForm["creator"] = Yii::app()->session["userId"];
        $dupForm["parent"] = [];

        //     var_dump($controller);

        // exit();
        if(!empty($controller->costum))
    	    $dupForm["parent"][$controller->costum["contextId"]]=["type"=>$controller->costum["contextType"],"name"=>($controller->costum["title"]) ? $controller->costum["title"] : $controller->costum["contextSlug"]];
    	

    	//$subForm = [];
        if (!empty($dupForm["subForms"]) && $type != "aapConfig" && !empty($controller->costum)){
			foreach ($dupForm["subForms"] as $vtsid => $vts){
				// $posSlug=strrpos($vts, $_POST("costumSlug"));
    //             $prevSubName=strlen($vts)-$posSlug;
    //             $prevSubName=substr($vts, 0,-$prevSubName);
				// var_dump($prevSubName);exit;
                
                $formuniqid = $controller->costum["contextSlug"] . uniqid();

                $dupForm["subForms"][$vtsid] = $formuniqid;

                $sbform = PHDB::findOne(Form::COLLECTION, array(
                	"id" => $vts
                ));




                $sbform["id"] = $formuniqid;

                unset($sbform["_id"]);
                unset($sbform["updated"]);

 				$sbform["created"] = time();
                $sbform["creator"] = Yii::app()->session["userId"];
    
                Yii::app()->mongodb->selectCollection(Form::COLLECTION)->insert($sbform);
                            
            }
            $dupForm["copy"] = true; 
            if($active){
            	$dupForm["active"]=true;
            }
            foreach($dupForm["params"] as $kpar => $vpar){
                
                if(strpos($kpar, "validateStep")!==false){
                    //var_dump($vpar);
                    $stepKey=array_search($vpar["step"], $formParent["subForms"]);
                    //var_dump($stepKey);
                    $dupForm["params"][$kpar]["step"]= $dupForm["subForms"][$stepKey];

                }
            }
		}
		//var_dump($dupForm);exit;
		Yii::app()->mongodb->selectCollection(Form::COLLECTION)->insert($dupForm);		 
    	
        return Rest::json(array("result"=>true, "msg"=>Yii::t("common","Information saved"),"map"=> $dupForm));
	}
}