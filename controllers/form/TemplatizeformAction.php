<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\form;

use Form;
use MongoId;
use Person;
use PHDB;
use Rest;
use Yii;

class TemplatizeformAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id) {
        $ctrl=$this->getController();

        if (!Person::logguedAndValid())
            return Rest::json(['result' => false, "msg" => Yii::t("common", "Please Login First"), "icon" => "fa-sign-in"]);

        //check form et session exist
        if($form = PHDB::findOne( Form::COLLECTION , [ "_id"=>new MongoId($id) ] ) )
        {
            $subForm = [];
            if (!empty($form["subForms"])){
                $subForm = $form["subForms"];
            }
            // if( Authorisation::isElementAdmin($id, Form::ANSWER_COLLECTION, Yii::app()->session["userId"], false)) {
            PHDB::update(Form::COLLECTION, array("_id" => new MongoId($id)) , array('$set' => array("type" => "template", "private" => "false" )));
            // } else
            return Rest::json(['result' => true, "msg" => Yii::t("project", "Success"), "icon" => "fa-check"]);
        } else
            return Rest::json(['result' => false, "msg" => "Formulaire introuvable", "icon" => "fa-search"]);
    }
}