<?php
namespace PixelHumain\PixelHumain\modules\survey\controllers\form;

use Answer;
use Form;
use MongoId;
use PHDB;
use Yii;
use yii\helpers\Json;

class SwitchCoremuAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($form=null, $active=null)
    {
        $return = Form::switchcoremu($form , $active);
        return Json::encode($return);
    }
}
