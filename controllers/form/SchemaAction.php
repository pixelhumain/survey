<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\form;

use CAction, Person, Rest, Yii, PHDB, Form, MongoId, Costum;
class SchemaAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id) {
		$ctrl=$this->getController();
		
		if ( ! Person::logguedAndValid() ) 
 			return Rest::json(['result' => false, "msg"=>Yii::t("common","Please Login First"),"icon"=>"fa-sign-in"]);

        //check form et session exist
        echo "url : http://127.0.0.1/survey/form/schema/id/".$id;
        if($form = PHDB::findOne( Form::COLLECTION , [ "id"=>$id ] ) ) 
        {
            
            
            echo "<h3>//collection forms (Main Form) : id : ".$form["id"]."</h3><br/>";
            unset($form["_id"]);
            echo json_encode($form); 


            //cascade delete all subforms 
            if(isset($form["subForms"]))
            {
                foreach ($form["subForms"] as $ix => $fid) {
                    echo "<h3>//collection forms (subForm) : id : ".$fid." </h3> <br/>";
                    $subF = PHDB::findOne( Form::COLLECTION, ["id" => $fid]);
                    unset($subF["_id"]);
                    echo json_encode( $subF );
                    
                }
            }

            if(isset($form["parent"]))
            {
                foreach ($form["parent"] as $ix => $el) {
                    $elId = $ix;
                    $elType = $el["type"];
                    $elName = $el["name"];
                }
                $el = PHDB::findOne( $elType, ["_id" => new MongoId( $elId ) ],["costum","slug"]);                    
            }

            if(isset($el["costum"]['slug']))
            {
                echo "<h3>//collection costum</h3> ".$el["costum"]['slug']."<br/>";
                $costum = PHDB::findOne( Costum::COLLECTION, ["slug" => $el["costum"]['slug'] ]);
                unset($costum["_id"]);
                echo json_encode( $costum );
                    
            } else if( isset( $el['slug'] ) ){
                echo "<h3>//collection costum</h3> ".$el["costum"]['slug']."<br/>";
                echo json_encode( PHDB::find( Costum::COLLECTION, ["slug" => $el['slug'] ]) );
            }

            echo "<h3>//collection ".$elType." : ".$elName."</h3> parent element id type :  : ".$elId."<br/>";
            echo "parent element slug : ".$el["slug"]."<br/>";
            echo json_encode( $el );

        } else {
            $docsJSON = file_get_contents("../../modules/costum/data/".$id.".json", FILE_USE_INCLUDE_PATH);
            if( !empty($docsJSON) ){ 
                echo json_decode($docsJSON,true);
            } else 
                echo json_encode(['result' => false, "msg"=>"Formulaire introuvable","icon"=>"fa-search"]); 
        }
	}
}