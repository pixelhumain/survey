<?php


namespace PixelHumain\PixelHumain\modules\survey\controllers\form;
use CAction;
use Form;
use PHDB;
use Rest;
class CheckFormExistAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
        $controller=$this->getController();
        $id = $_POST["parentId"];
        $params = [ ];

        $params["forms"] = PHDB::find( Form::COLLECTION, array( "parent.".$id => array('$exists' => true)));
        $params["value"] = false;
        if (!empty($params["forms"])) {
            $params["value"] = true;
        }
        return Rest::json($params);
    }
}