<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\form;
use CAction;
use Form;
use MongoId;
use Person;
use PHDB;
use Rest;
use Yii;

class DeleteAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id) {
		$ctrl=$this->getController();
		
		if ( ! Person::logguedAndValid() ) 
 			return Rest::json(['result' => false, "msg"=>Yii::t("common","Please Login First"),"icon"=>"fa-sign-in"]);

        //check form et session exist
        if($form = PHDB::findOne( Form::COLLECTION , [ "_id"=>new MongoId($id) ] ) ) 
        {
            // if( Authorisation::isElementAdmin($id, Form::ANSWER_COLLECTION, Yii::app()->session["userId"], false)) {

                //cascade delete all subforms
                if(isset($form["type"]) && isset($form["subForms"]) && $form["type"]=="aapConfig"){
                    $inputsArray = array();
                    PHDB::remove( Form::ANSWER_COLLECTION, ["form" => (string)$form["_id"]]);
                    foreach ($form["subForms"] as $ksub => $vsub) {
                        PHDB::remove( Form::COLLECTION, ["id" => $vsub["inputs"]]);
                    }
                }
                elseif(isset($form["subForms"]))
                {
                    PHDB::remove( Form::ANSWER_COLLECTION, ["formId" => implode("|", $form["subForms"]) ]);
                    foreach ($form["subForms"] as $ix => $fid) {
                        PHDB::remove( Form::COLLECTION, ["id" => $fid]);
                    }
                }

                if(isset($form["id"])){
                    $isSubForm = PHDB::find( Form::COLLECTION , [ "subForms"=>@$form["id"] ] );
                    foreach ($isSubForm as $fid => $f) {
                        if(!isset($f['subType'])) {
                            $newSubForms = $f['subForms'];
                            $setParams = [];
                            array_splice($newSubForms, array_search($form["id"], $newSubForms), 1);
                            if(isset($f['params']) && !isset($f['subType']) && count($newSubForms) > 0) {
                                foreach($f['params'] as $kfp => $vfp) {
                                    if(isset($vfp['step'])) {
                                        foreach($newSubForms as $knsub => $vnsub) {
                                            if(strpos($kfp, 'validateStep'.$vnsub) !== false) {
                                                if(isset($newSubForms[$knsub+1])) {
                                                    $setParams['params.'.$kfp.'.step'] = $newSubForms[$knsub+1];
                                                } else {
                                                    $setParams['params.'.$kfp.'.step'] = $vnsub;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        PHDB::update( Form::COLLECTION, [ "_id" => new MongoId($fid) ],
                                                        ['$pull' => [ "subForms" => $form["id"] ]]);
                        if(!empty($setParams) && !isset($f['subType'])) {
                            PHDB::update( Form::COLLECTION, [ "_id" => new MongoId($fid) ],
                                                        ['$set' => $setParams]);
                        }
                    }
                }
                PHDB::remove( Form::COLLECTION, ["_id" => new MongoId((string)$form["_id"])] );
                
                return Rest::json(['result' => true, "msg"=>"Le formulaire été supprimée"]);
                
            // } else 
            //   echo Rest::json(['result' => false, "msg"=>Yii::t("project", "Unauthorized Access."),"icon"=>"fa-lock"]); 
        } else 
            return Rest::json(['result' => false, "msg"=>"Formulaire introuvable","icon"=>"fa-search"]); 
	}
}