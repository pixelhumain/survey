<?php


namespace PixelHumain\PixelHumain\modules\survey\controllers\form;

use CTKAction;
use Element;
use Form;
use PHDB;
use Rest;
use Slug;

class CheckFeedbackAction extends CTKAction
{
    public function run()
    {
        $controller=$this->getController();
        $id = $_POST["id"];
        $type = $_POST["type"];
        $params = [ ];

        $params["forms"] = PHDB::find( Form::COLLECTION, array( "parent.".$id => array('$exists' => true ) , "level2subType" => "feedback"  ) );

        $params["el"] = PHDB::findOneById( $type, $id );

        $params["feedbackTpl"] = PHDB::find( Form::COLLECTION, array( "level2subType" => "feedbackTpl" ) );

        $params["value"] = false;
        if (!empty($params["forms"])) {
            $params["value"] = true;
        }

        return Rest::json( $params );

    }
}