<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\form;
use Answer;
use CAction;
use Form;
use MongoId;
use PHDB;
use Rest;
use Yii;
use Zone;

class InputAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($request)
    {
        $controller = $this->getController();
		return $this->$request();
    }

    private function getQPVChooser() {
        $globalParams = $_POST["inputGlobalParams"];
        
        $paramsData = [];
        $currentAnswer = (!empty($globalParams["answer"]) && isset($globalParams["answer"]["answers"][$globalParams["form"]["id"]][$globalParams["key"]])) ? $globalParams["answer"]["answers"][$globalParams["form"]["id"]][$globalParams["key"]] : [];
        $parentFormParams = PHDB::findOneById(Form::COLLECTION, $globalParams["parentForm"]["_id"], ["params"]);
        if( isset($parentFormParams["params"][$globalParams["kunik"]]) ) 
            $paramsData =  $parentFormParams["params"][$globalParams["kunik"]];
    
        $query = ['$and' => [
            array('$or' => [])
        ]];
    
        if(!empty($paramsData["typeQuariter"]))
            $query['$and'][] = array("type" => ['$in' => $paramsData["typeQuariter"]]);
        
        if(!empty($paramsData["typeQuariter"]))
            $query['$and'][0]['$or'][] = array("insee" => ['$in' => $paramsData["codeInseeQuartier"]]);
        
        if(!empty($paramsData["otherQuartier"])){
            $otherQuartiers = array_keys($paramsData["otherQuartier"]);
            $otherQuartiers = array_map(function($v){
                return new MongoId($v);
            },$otherQuartiers);
            $query['$and'][0]['$or'][] = array("_id" => ['$in' => $otherQuartiers]);
        }
        if(empty($query['$and'][0]['$or'])) unset($query['$and'][0]['$or']);
    
        $quarties = [];
        if(!empty($query['$and'][0]))
        $quarties = PHDB::findAndSort(Zone::COLLECTION,$query,array("name"=> 1),0,array("name","codeQPV"));
        $counQPV = " (".count($quarties)." Quartier(s))";
        
        $globalParams["quarties"] = $quarties;
        $globalParams["counQPV"] = $counQPV;
        $globalParams["currentAnswer"] = $currentAnswer;
        $globalParams["paramsData"] = $paramsData;
        $globalParams["query"] = $query;
        return $this->getController()->renderPartial('survey.views.tpls.forms.cplx.qpvChooserView', $globalParams);
    }
}