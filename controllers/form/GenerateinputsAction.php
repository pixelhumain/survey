<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\form;

use CTKAction, Yii, Rest, PHDB, Form;
class GenerateinputsAction extends CTKAction
{
	public function run($form=null, $contextId=null, $contextType=null)
    {
		if(!empty($_POST)){
				$inputs = array();
				for ($i=1; $i <= 5; $i++) { 
					if(!empty($_POST["proposition"]) && $i == $_POST["proposition"]){ // get proposition position(order) and add defaults question(inputs)
						$inputs[$i] = ["isSpecific" => true, "type" => "openForm"];
						if(!empty($_POST["formParent"])) 
							$inputs[$i] = array_merge($inputs[$i],["formParent" => $_POST["formParent"],"step" =>"aapStep".$i]);
							$propositionInputs = [];

						if(isset($_POST["ocecoform"]) && $_POST["ocecoform"]==true){
							$propositionInputs = [
								"titre" => [
									"label" => "Nom de la proposition",
									"placeholder" => "Nom de la proposition",
									"info" => "",
									"type" => "text",
								],
								"description" => [
									"label" => "Description de la proposition",
									"placeholder" => "Nom de la proposition",
									"info" => "",
									"type" => "textarea",
								],
								"image"=> [
									"label"=> "Ajouter une image pour votre proposition",
									"placeholder"=> "",
									"info"=> "",
									"type"=> "tpls.forms.uploader",
									"uploader"=> [
									  "docType"=> "image",
									  "contentKey"=> "profil",
									  "paste"=> "true",
									  "restricted"=> "true",
									  "itemLimit"=> "5"
									]
								],
								"context"=> [
									"label"=> "Contexte",
									"placeholder"=> "",
									"info"=> "Rattacher cette proposition à une organisation",
									"type"=> "tpls.forms.aap.context"
								],
								"adress" => [
									"label" => "Adresse(s)",
									"info" => "",
									"type" => "tpls.forms.cplx.address",
								],
								"depense" => [
									"label" => "Les dépenses",
									"placeholder" => "",
									"info" => "",
									"type" => "tpls.forms.ocecoform.budget",
								],
								"tags" => [
									"label" => "Thématique",
									"placeholder" => "",
									"info" => "",
									"type" => "tpls.forms.tags",
								],
								"urgency" => [
									"label" => "Urgence",
									"placeholder" => "",
									"info" => "",
									"type" => "tpls.forms.cplx.checkboxNew",
								]
							];
							unset($propositionInputs["context"],$propositionInputs["adress"]);
						}
						if(isset($_POST["aap"]) && $_POST["aap"]==true){
							$propositionInputs = [
								/** PORTEUR DU PROJET **/
								"email" => [
									"label" => "Adresse e-mail",
									"placeholder" => "Votre adresse e-mail",
									"info" => "",
									"type" => "tpls.forms.emailUser",
									"positions" => isset($_POST["formParent"]) ? array($_POST["formParent"] => "1") : ""
								],
								"separatorProjectOwner" => [
									"label" => "PORTEUR",
									"placeholder" => "",
									"info" => "",
									"type" => "tpls.forms.titleSeparator",
									"positions" => isset($_POST["formParent"]) ? array($_POST["formParent"] => "2") : ""
								],
								"association" => [
									"label" => "Nom de l'association",
									"placeholder" => "Votre réponse",
									"info" => "",
									"type" => "text",
									"positions" => isset($_POST["formParent"]) ? array($_POST["formParent"] => "3") : ""
								],
								"adress" => [
									"label" => "Adresse du siège",
									"info" => "",
									"type" => "tpls.forms.cplx.address",
									"positions" => isset($_POST["formParent"]) ? array($_POST["formParent"] => "4") : ""
								],
								"dateCreationAssociation" => [
									"label" => "Date de création de l'association",
									"placeholder" => "Votre réponse",
									"info" => "",
									"type" => "date",
									"positions" => isset($_POST["formParent"]) ? array($_POST["formParent"] => "5") : ""
								],
								"rnaNumber" => [
									"label" => "Numéro RNA",
									"placeholder" => "Votre réponse",
									"info" => "",
									"type" => "text",
									"positions" => isset($_POST["formParent"]) ? array($_POST["formParent"] => "7") : ""
								],
								"rnaNumber" => [
									"label" => "Numéro RNA",
									"placeholder" => "Votre réponse",
									"info" => "",
									"type" => "text",
									"positions" => isset($_POST["formParent"]) ? array($_POST["formParent"] => "7") : ""
								],
								"plannedStartDate" => [
									"label" => "Date de démarrage prévue",
									"placeholder" => "Votre réponse",
									"info" => "",
									"type" => "text",
									"positions" => isset($_POST["formParent"]) ? array($_POST["formParent"] => "7") : ""
								],
								"mainActivitiesAssociation" => [
									"label" => "Activités principales de l'association",
									"placeholder" => "Votre réponse",
									"info" => "",
									"type" => "textarea",
									"positions" => isset($_POST["formParent"]) ? array($_POST["formParent"] => "7") : ""
								],
								"numberOfMembers" => [
									"label" => "Nombre d'adhérents",
									"placeholder" => "Votre réponse",
									"info" => "",
									"type" => "number",
									"positions" => isset($_POST["formParent"]) ? array($_POST["formParent"] => "9") : ""
								],
								"numberOfVolunteers" => [
									"label" => "Nombre de bénévoles",
									"placeholder" => "Votre réponse",
									"info" => "",
									"type" => "text",
									"positions" =>  isset($_POST["formParent"]) ? array($_POST["formParent"] => "10") : "" 
								],
								"numberOfEmployees" => [
									"label" => "Nombre de salariés",
									"placeholder" => "Votre réponse",
									"info" => "",
									"type" => "text",
									"positions" =>  isset($_POST["formParent"]) ? array($_POST["formParent"] => "10") : "" 
								],
								"namePresident" => [
									"label" => "Nom et prénoms du(de la) président(e)",
									"placeholder" => "Votre réponse",
									"info" => "",
									"type" => "text",
									"positions" =>  isset($_POST["formParent"]) ? array($_POST["formParent"] => "10") : "" 
								],
								"webSiteOrFacebook" => [
									"label" => "Site internet ou page facebook de l'associatioin",
									"placeholder" => "Votre réponse",
									"info" => "",
									"type" => "text",
									"positions" =>  isset($_POST["formParent"]) ? array($_POST["formParent"] => "10") : "" 
								],
								"annualGlobalBudget" => [
									"label" => "Budget global annuel",
									"placeholder" => "",
									"info" => "",
									"type" => "number",
									"positions" =>  isset($_POST["formParent"]) ? array($_POST["formParent"] => "11") : "" 
								],
								/** LE PROJET **/
								"separatorProject" => [
									"label" => "PROJET",
									"placeholder" => "",
									"info" => "",
									"type" => "tpls.forms.titleSeparator",
									"positions" =>  isset($_POST["formParent"]) ? array($_POST["formParent"] => "12") : "" 
								],
								"nameProjectManager" => [
									"label" => "Nom et Prénom de la personne en charge du projet",
									"placeholder" => "Votre réponse",
									"info" => "",
									"type" => "text",
									"positions" => isset($_POST["formParent"]) ? array($_POST["formParent"] => "7") : ""
								],
								"phoneProjectManager" => [
									"label" => "Numéro de téléphone de la personne en charge du projet",
									"placeholder" => "Votre réponse",
									"info" => "",
									"type" => "text",
									"positions" => isset($_POST["formParent"]) ? array($_POST["formParent"] => "8") : ""
								],
								"titre" => [
									"label" => "Intitulé du projet *",
									"placeholder" => "Votre réponse",
									"info" => "",
									"type" => "text",
									"positions" =>  isset($_POST["formParent"]) ? array($_POST["formParent"] => "13") : "" 
								],
								"interventionArea" => [
									"label" => "Quartier(s) d'intervention",
									"placeholder" => "",
									"info" => "",
									"type" => "tpls.forms.cplx.checkboxNew",
									"positions" => isset($_POST["formParent"]) ? array($_POST["formParent"] => "6") : ""
								],
								"publicCible" => [
									"label" => "Quel est le public cible?",
									"placeholder" => "",
									"info" => "",
									"type" => "tpls.forms.cplx.radioNew",
									"positions" =>  isset($_POST["formParent"]) ? array($_POST["formParent"] => "22") : "" 
								],
								"description" => [
									"label" => "Description détaillée du projet *",
									"placeholder" => "Votre réponse",
									"info" => "",
									"type" => "textarea",
									"positions" =>  isset($_POST["formParent"]) ? array($_POST["formParent"] => "14") : "" 
								],
								"piliers" => [
									"label" => "Piliers",
									"placeholder" => "Votre réponse",
									"info" => "",
									"type" => "text",
									"positions" =>  isset($_POST["formParent"]) ? array($_POST["formParent"] => "14") : "" 
								],
								"tags" => [
									"label" => "Thématique",
									"placeholder" => "",
									"info" => "",
									"type" => "tpls.forms.tagsNoRemote",
									"positions" =>  isset($_POST["formParent"]) ? array($_POST["formParent"] => "28") : "" 
								],
								"image"=> [
									"label"=> "Ajouter une image pour votre proposition",
									"placeholder"=> "",
									"info"=> "",
									"type"=> "tpls.forms.uploader",
									"uploader"=> [
									  "docType"=> "image",
									  "contentKey"=> "profil",
									  "paste"=> "true",
									  "restricted"=> "true",
									  "itemLimit"=> "5"
									],
									"positions" =>  isset($_POST["formParent"]) ? array($_POST["formParent"] => "15") : "" 
								],
								"mainObjectives" => [
									"label" => "Les objectifs principaux (Trois maximum, merci de les numéroter) *",
									"placeholder" => "Votre réponse",
									"info" => "",
									"type" => "tpls.forms.tags",
									"positions" =>  isset($_POST["formParent"]) ? array($_POST["formParent"] => "17") : "" 
								],
								"depense" => [
									"label" => "Ligne de dépenses",
									"placeholder" => "",
									"info" => "",
									"type" => "tpls.forms.ocecoform.budget",
									"positions" =>  isset($_POST["formParent"]) ? array($_POST["formParent"] => "18") : "" 
								],
								/* MODALITES DE MISE EN OEUVRE DU PROJET*/
								"separatorProjecModality" => [
									"label" => "MODALITES DE MISE EN OEUVRE DU PROJET",
									"placeholder" => "",
									"info" => "",
									"type" => "tpls.forms.titleSeparator",
									"positions" =>  isset($_POST["formParent"]) ? array($_POST["formParent"] => "19") : "" 
								],
								"exactLocationAction" => [
									"label" => "Lieu précis du déroulement de l'action *",
									"placeholder" => "Votre réponse",
									"info" => "",
									"type" => "text",
									"positions" =>  isset($_POST["formParent"]) ? array($_POST["formParent"] => "20") : "" 
								],
								"numberStakeholdersProject" => [
									"label" => "Nombre d'intervenants dans le projet",
									"placeholder" => "Votre réponse",
									"info" => "",
									"type" => "number",
									"positions" =>  isset($_POST["formParent"]) ? array($_POST["formParent"] => "21") : "" 
								],
								"totalNumberBeneficiaries" => [
									"label" => "Nombre total de bénéficiaires *",
									"placeholder" => "Votre réponse",
									"info" => "",
									"type" => "number",
									"positions" =>  isset($_POST["formParent"]) ? array($_POST["formParent"] => "23") : "" 
								],
								"startDate" => [
									"label" => "Date de commencement *",
									"placeholder" => "Votre réponse",
									"info" => "",
									"type" => "date",
									"positions" =>  isset($_POST["formParent"]) ? array($_POST["formParent"] => "24") : "" 
								],
								"plannedDuration" => [
									"label" => "Durée prévue *",
									"placeholder" => "Votre réponse",
									"info" => "",
									"type" => "text",
									"positions" =>  isset($_POST["formParent"]) ? array($_POST["formParent"] => "25") : "" 
								],
								"axesTFPB" => [
									"label" => "Les axes de la TFPB (sélectionner celui qui correspond à votre projet)",
									"placeholder" => "",
									"info" => "",
									"type" => "tpls.forms.cplx.radioNew",
									"positions" =>  isset($_POST["formParent"]) ? array($_POST["formParent"] => "26") : "" 
								],
								"principalDomaine" => [
									"label" => "Domaine principal",
									"placeholder" => "Votre réponse",
									"info" => "",
									"type" => "tpls.forms.tags",
									"positions" =>  isset($_POST["formParent"]) ? array($_POST["formParent"] => "27") : "" 
								],
								"urgency" => [
									"label" => "Urgence",
									"placeholder" => "",
									"info" => "",
									"type" => "tpls.forms.cplx.checkboxNew",
									"positions" =>  isset($_POST["formParent"]) ? array($_POST["formParent"] => "29") : "" 
								]
							];
						}

						$inputs[$i] = array_merge($inputs[$i], ["inputs" => $propositionInputs]);
					};
					// if(!empty($_POST["caracterisation"]) && $i == $_POST["caracterisation"]){ // get proposition position(order) and add defaults question(inputs)
					// 	$inputs[$i] = ["isSpecific" => true, "type" => "openForm"];
					// 	if(!empty($_POST["formParent"])) 
					// 		$inputs[$i] = array_merge($inputs[$i],["formParent" => $_POST["formParent"],"step" =>"aapStep".$i]);
					// 	$caracterisationInputs = [
					// 		"caracterisation" => [
					// 			"label" => "Nom de la caracterisation",
					// 			"placeholder" => "Nom de la caracterisation",
					// 			"info" => "",
					// 			"type" => "tpls.forms.aap.characterization"
					// 		]
					// 	];
					// 	$inputs[$i] = array_merge($inputs[$i], ["inputs" => $caracterisationInputs]);
					// };
					if(!empty($_POST["evaluation"]) && $i == $_POST["evaluation"]){ // get evaluation position(order) and add defaults question(inputs)
						$inputs[$i] = ["isSpecific" => true, "type" => "openForm"];
						if(!empty($_POST["formParent"])) 
							$inputs[$i] = array_merge($inputs[$i],["formParent" => $_POST["formParent"],"step" =>"aapStep".$i]);
						// $evaluationInputs = [
						// 	"evaluation" => [
						// 		"label" => "Evaluation",
						// 		"placeholder" => "Evaluation",
						// 		"info" => "",
						// 		"type" => "tpls.forms.aap.evaluation",
						// 	],
						// 	"generateproject" => [
						// 		"label" => "Générer un projet",
						// 		"placeholder" => "Générer un projet",
						// 		"info" => "",
						// 		"type" => "tpls.forms.ocecoform.generateprojectbtn"
						// 	]
						// ];
						//if(isset($_POST["ocecoform"]) && $_POST["ocecoform"]==true){
							$evaluationInputs = [
								"decide" => [
									"label" => "Dépenses",
									"placeholder" => "Dépenses",
									"info" => "",
									"type" => "tpls.forms.ocecoform.multiDecide",
									"position" => "1"
								]
							];
						//}
						$inputs[$i] = array_merge($inputs[$i], ["inputs" => $evaluationInputs]);
					}
					if(!empty($_POST["financement"]) && $i == $_POST["financement"]){ // get financement position(order) and add defaults question(inputs)
						$inputs[$i] = ["isSpecific" => true, "type" => "openForm"];
						if(!empty($_POST["formParent"])) 
							$inputs[$i] = array_merge($inputs[$i],["formParent" => $_POST["formParent"],"step" =>"aapStep".$i]);
						$financementInputs = [
							"financer" => [
								"label" => "Financement",
								"placeholder" => "financement",
								"info" => "",
								"type" => "tpls.forms.ocecoform.financementFromBudget",
							]	
						];
						//if(isset($_POST["ocecoform"]) && $_POST["ocecoform"]==true){
							$financementInputs["generateproject"] = [
								"label" => "Projet associé",
								"placeholder" => "",
								"info" => "Générer le projet",
								"type" => "tpls.forms.ocecoform.generateprojectbtn"
							];
						//}
						$inputs[$i] = array_merge($inputs[$i], ["inputs" => $financementInputs]);
					}
					if(!empty($_POST["suivie"]) && $i == $_POST["suivie"]){ // get financement position(order) and add defaults question(inputs)
						$inputs[$i] = ["isSpecific" => true, "type" => "openForm"];
						if(!empty($_POST["formParent"])) 
							$inputs[$i] = array_merge($inputs[$i],["formParent" => $_POST["formParent"],"step" =>"aapStep".$i]);
						$suiviInputs = [
							"suivredepense" => [
								"label" => "Dépenses",
								"placeholder" => "Dépenses",
								"info" => "",
								"type" => "tpls.forms.ocecoform.suiviFromBudget",
							]	
						];
						$inputs[$i] = array_merge($inputs[$i], ["inputs" => $suiviInputs]);
					}
				}
				Yii::app()->mongodb->selectCollection("inputs")->batchInsert($inputs);
				return Rest::json(array("result" => true,"data" => $inputs, "msg" => Yii::t("common", "Champs généré")));
		}else{
			$formParent = PHDB::findOneById(Form::COLLECTION, $form);
			$context = null;
			if (!empty($contextId) && !empty($contextType)) {
				$ctxEl = PHDB::findOneById($contextType, $contextId, ["name", "address"]);
				$context = [
					$contextId => [
						"type" => $contextType,
						"name" => $ctxEl["name"]
					]
				];
			}
			$inputs = Form::generateInputs($formParent, $context);
			return Rest::json($inputs);
		}
    }
}