<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\form;

use CTKAction, PHDB, Form, Yii, Answer, Rest;
class GetOpenFormAction extends CTKAction
{
	//if $slug : getd all the forms for an element.slug
	//http://127.0.0.1/survey/form/get/slug/dealAH
	//if $id : opens the forms 
    public function run($slug = null,$form=null, $tpl=null)
    {
    	
    	$controller=$this->getController();
        
    	$params = [  ];

		$controller->layout = "//layouts/empty";
   		//$params["forms"] = PHDB::find( Form::COLLECTION, [ "parentSlug"=>$slug ] );
   		$params["forms"] = PHDB::find( Form::COLLECTION, [ "openForm" => true ] );
		if(isset($params["forms"]))
		{
			foreach ( $params["forms"] as $fix => $f ) 
			{
				$params["forms"][$fix]['canEditForm'] = Form::canAdmin(Yii::app()->session['userId'], $f);
				$formId = "";
				if(isset($f['subForms'])){
					foreach ($f['subForms'] as $ix => $fid) 
					{
						if($formId != "")
							$formId .= "|";
						$formId .= $fid;
						$params["forms"][$fix][$fid] = PHDB::findOne( Form::COLLECTION, [ "id"=>$fid ] );
					}
				}
				// $params["forms"][$fix]["answers"] = PHDB::count( Form::ANSWER_COLLECTION, ["formId"=>$formId , "parentSlug" => $slug] );

				$params["forms"][$fix]["answers"] = PHDB::count( Answer::COLLECTION, ["form"=>$fix] );
			}
		}
		return Rest::json( $params );

    }
}