<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers;

use CommunecterController;
use Yii;

/**
 * DefaultController.php
 *
 * OneScreenApp for Communecting people
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class DefaultController extends CommunecterController {


  public function beforeAction($action)
	{
    //parent::initPage();
	  return parent::beforeAction($action);
	}

	public function actionIndex() 
	{
    /*if( @Yii::app()->params["module"]["parent"] && !@Yii::app()->params["module"]["overwrite"][Yii::app()->controller->id][ Yii::app()->controller->action->id ] ){
      $this->redirect(Yii::app()->createUrl( "/".Yii::app()->params["module"]["parent"]."/".Yii::app()->controller->id."/".Yii::app()->controller->action->id ));
    }*/
    
    	if(Yii::app()->request->isAjaxRequest)
        return $this->renderPartial("index");
      else
      {
        $this->layout = "//layouts/empty";
        return $this->render("index");
      }
  }

  public function actionDoc($md="README") 
  {
      return file_get_contents('../../modules/'.$this->module->id.'/'.$md.'.md');
  }

}
