<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers;

use CommunecterController;

class ExportationController extends CommunecterController {
    public function beforeAction($action) {
        parent::initPage();
        return parent::beforeAction($action);
    }

    public function actions() {
        return [
            'multiple-file-csv-coform' => \PixelHumain\PixelHumain\modules\survey\controllers\actions\exportationCsv\MultipleFileCSVExport::class,
            'csv-coform' => \PixelHumain\PixelHumain\modules\survey\controllers\actions\exportationCsv\CoformGeneriqueExportCSVAction::class,
            'csv-aap'    => \PixelHumain\PixelHumain\modules\survey\controllers\actions\exportationCsv\AapGeneriqueExportCSVAction::class
        ];
    }
}
