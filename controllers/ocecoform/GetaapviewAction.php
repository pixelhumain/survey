<?php
namespace PixelHumain\PixelHumain\modules\survey\controllers\ocecoform;

use CTKAction;
use PHDB;
use Rest;
class GetaapviewAction extends CTKAction
{
    public function run($urll=null,$slugConfig=null,$formStandalone=null)
    {
        $urll= str_replace(".", "/", $urll);
        $params = [
           "urlR" => $urll,
            "modalrender" => true
        ];
        if(!empty($slugConfig)){
            $params["slug"] = "coSindniSmarterre";//$slugConfig;
            $params["formStandalone"] = true;
            $params["kunik"] = "kunik".rand();
        } 
            

        return $this->getController()->renderPartial("costum.views.custom.appelAProjet.callForProjects.main", $params,true );
    }
}