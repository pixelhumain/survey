<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\ocecoform;

use CTKAction, PHDB, Form;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;
use Rest;

class ChangefintypeAction extends CTKAction
{
    public function run()
    {
        $return = false;
        if (!empty($_POST["answer"]) && !empty($_POST["financeurid"])) {
            $return = Aap::changefintype($_POST["answer"], $_POST["financeurid"]);
        }

        return Rest::json($return);
    }
}
