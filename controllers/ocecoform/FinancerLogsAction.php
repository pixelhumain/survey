<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\ocecoform;

use CTKAction, PHDB, Form;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;
use Rest;

class FinancerLogsAction extends CTKAction
{
    public function run($form = null,$answer = null)
    {
        if (empty($answer)) {
            $allAnswers = PHDB::find(Form::ANSWER_COLLECTION, array("form" => $form), array("answers.aapStep1.titre", "answers.aapStep1.depense"));
        } else {
            $allAnswers = [$answer => PHDB::findOneById(Form::ANSWER_COLLECTION, $answer, array("answers.aapStep1.titre", "answers.aapStep1.depense"))];
        }

        $allAnswersCoremu = Aap::financerLogs($form,$answer);
        $allAnswersCoremu["CompleteAnswers"] = $allAnswers;

        return Rest::json($allAnswersCoremu);
    }
}
