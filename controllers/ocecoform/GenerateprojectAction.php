<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\ocecoform;

use ActStr;
use CTKAction, PHDB, Form, Answer, Rest;
use Notification;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;
use Yii;

class GenerateprojectAction extends CTKAction {
	//if $slug : getd all the forms for an element.slug
	//http://127.0.0.1/survey/form/get/slug/dealAH
	//if $id : opens the forms 
	public function run($answerId = null, $parentId = null, $parentType = null) {
		if (empty($answerId))
			return (Rest::json([
				"success"	=> 0,
				"content"	=> "Empty answer id"
			]));
		$db_answer = PHDB::findOneById(Form::ANSWER_COLLECTION, $answerId);
		if (!empty($db_answer['project']['id']))
			return (Rest::json([
				"success"	=> 0,
				"content"	=> "Already have a project"
			]));
		$links = [];
		$actions = [];
		$financer = [];
		$images = [];
		$depense = [];
		$parent_request = [];
		$db_form = [];
		$db_form_config = [];
		if (!empty($db_answer['form']))
			$db_form = PHDB::findOneById(Form::COLLECTION, $db_answer["form"]);
		if (!empty($db_form['type']) && $db_form['type'] === 'aap')
			$db_form_config = PHDB::findOneById(Form::COLLECTION, $db_form["config"]);
		if (!empty($parentId) && !empty($parentType)) {
			$db_parent = PHDB::findOneById($parentType, $parentId, ['name']);
			$parent_request[$parentId] = [
				'type'	=> $parentType,
				'name'	=> $db_parent['name']
			];
		}
		$mappings = [
			'name' => $db_form_config['mapping']['proposition'] ?? '',
			'desc' => $db_form_config['mapping']['description'] ?? '',
			'depenses' => $db_form_config['mapping']['depense'] ?? [],
		];
		if (!empty($mappings['name']))
			$name = UtilsHelper::array_val_at_path($db_answer, $mappings['name']);
		if (!empty($mappings['name']))
			$descr = UtilsHelper::array_val_at_path($db_answer, $mappings['desc']);
		if (!empty($mappings['depenses'])) {
			foreach ($mappings['depenses'] as $depense_key => $depense) {
				$action = [
					'depenseKey'	=>  $depense_key,
					'credits'		=> 1,
					'estimates'		=> [],
				];
				if (!empty($depense['poste']))
					$action['poste'] = $depense['poste'];
				if (!empty($depense['price']))
					$action['credits'] = intval($depense['price']);
				if (!empty($depense['estimates']) && is_array($depense['estimates'])) {
					foreach ($depense['estimates'] as $est_key => $estimate) {
						if (!empty($estimate['selected']) && filter_var($estimate['selected'], FILTER_VALIDATE_BOOLEAN))
							$action['user'] = $est_key;
						else
							$action['user'] = $db_answer['user'];
					}
				} else
					$action['user'] = $db_answer['user'];
				if (!empty($depense['actionid']))
					$actions[] = $action;
				if (!empty($depense['financer']) && is_array($depense['financer'])) {
					foreach ($depense['financer'] as $finance) {
						if (!empty($financer['id']))
							$financer[] = $finance['id'];
					}
				}
			}
		}
		$image_fields = ['profilImageUrl', 'profilMarkerImageUrl', 'profilMediumImageUrl', 'profilThumbImageUrl'];
		foreach ($image_fields as $image_field) {
			if (!empty($db_answer[$image_field]))
				$images[$image_field] = $db_answer[$image_field];
		}
		if (!empty($db_answer["links"])) {
			foreach ($db_answer["links"] as $key => $value) {
				if ($key == "answered") {
					$links = array_unique(array_merge($links, $value), SORT_REGULAR);
				} elseif (is_array($value)) {
					$links = array_unique(array_merge($links, array_keys($value)), SORT_REGULAR);
				}
			}
		}
		if (!empty($name)) {
			$project = Answer::GenerateProjectFromAnswer($name, $descr, array_unique($links), $financer, $parent_request, $actions, $db_answer, $images);
			self::notifySwitchToProject($db_answer, $db_form, $db_form_config);
			return Rest::json($project);
		}
		return (Rest::json(false));
	}

	private function notifySwitchToProject(&$db_answer, $db_form = [], $db_form_config = []) {
		$contextId = array_keys($db_answer["context"])[0];
		$id = (string) $db_answer['_id'];
		$contextType = $db_answer["context"][$contextId]["type"];
		$answerName = "";

		if (!empty($db_answer["answers"]["aapStep1"]["titre"])) {
			$answerName = $db_answer["answers"]["aapStep1"]["titre"];
			$res["{what}"] = [$answerName];
		}

		Notification::constructNotification(
			ActStr::PROPOSAL_TO_PROJECT,
			array(
				"id" => Yii::app()->session["userId"],
				"name" => Yii::app()->session["user"]["name"]
			),
			array(
				"id" => $contextId,
				"type" => $contextType
			),
			array(
				"type" => Form::ANSWER_COLLECTION,
				"id" => $id,
				"name" => $answerName,
				"isAap" => true,
				"url" => Aap::generateNotificationUrl($id, $db_answer, $db_form, $db_form_config),
				"statut" => null
			),
			Form::ANSWER_COLLECTION,
			null,
			null
		);
	}
}
