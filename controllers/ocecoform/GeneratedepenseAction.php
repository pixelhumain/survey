<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\ocecoform;

use Actions;
use CTKAction;
use MongoId;
use PHDB;
use Project;
use Rest;

class GeneratedepenseAction extends CTKAction
{
    //if $slug : getd all the forms for an element.slug
    //http://127.0.0.1/survey/form/get/slug/dealAH
    //if $id : opens the forms
    public function run($projectid=null)
    {
        $action = PHDB::find(Actions::COLLECTION, [
            "parentId" => (string)$projectid,
            "parentType" => Project::COLLECTION
        ] );
        //var_dump($action); exit();
        return Rest::json($action);

    }
}