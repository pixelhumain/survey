<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\ocecoform;

use CTKAction, PHDB, Form;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;
use Rest;

class DuplicateFinanceAction extends CTKAction
{
    public function run()
    {
        $return = false;
        if (!empty($_POST["answer"]) && !empty($_POST["financeurid"]) && !empty($_POST["action"] )) {
            $return = Aap::doublonnement($_POST["answer"], $_POST["financeurid"], $_POST["action"]);
        }

        return Rest::json($return);
    }
}
