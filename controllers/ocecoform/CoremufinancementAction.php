<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\ocecoform;

use CTKAction, PHDB, Form;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;
use Rest;

class CoremufinancementAction extends CTKAction
{
    public function run($el_form_id = null , $tableonly = false, $depenseid = null , $isAjax = false , $jsononly = false)
    {
        if (empty($depenseid)) {
            $allAnswers = PHDB::find(Form::ANSWER_COLLECTION, array("form" => $el_form_id), array("answers.aapStep1.titre", "answers.aapStep1.depense"));
        } else {
            $allAnswers = [$depenseid => PHDB::findOneById(Form::ANSWER_COLLECTION, $depenseid, array("answers.aapStep1.titre", "answers.aapStep1.depense"))];
        }

        //var_dump($allAnswers);

        $allAnswersCoremu = Aap::answerCoremuFinancerVariables($allAnswers);

        //var_dump($allAnswersCoremu);

        $mybarcolor = ["#50bd60", "#b30040"];
        $allAnswersCoremu["mybarcolor"] = $mybarcolor;
        $allAnswersCoremu["tableonly"] = $tableonly;
        $allAnswersCoremu["isAjax"] = $isAjax;
        $allAnswersCoremu["depenseid"] = $depenseid;
        $allAnswersCoremu["el_form_id"] = $el_form_id;
        if ($jsononly) {
            return Rest::json($allAnswersCoremu);
        } else {
            return $this->getController()->renderPartial("costum.views.custom.appelAProjet.callForProjects.coremu.coremufinancement", $allAnswersCoremu, true);
        }
    }
}
