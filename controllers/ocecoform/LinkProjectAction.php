<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\ocecoform;

use Action;
use CTKAction;
use Form;
use PHDB;
use Project;
use Rest;
use Room;
use Yii;

class LinkProjectAction extends CTKAction {
	public function run($answer) {
		$db_answer = PHDB::findOneById(Form::ANSWER_COLLECTION, $answer, ["project", "answers.aapStep1.depense"]);
		$user = Yii::app()->session["userId"] ?? "";
		if (empty($db_answer["project"]["id"]) || empty($user))
			return (Rest::json([
				"success"	=> 0,
				"content"	=> "Required informations incomplete"
			]));
		$db_actions = [];
		$depenses = !empty($db_answer["answers"]["aapStep1"]["depense"]) && is_array($db_answer["answers"]["aapStep1"]["depense"]) ? $db_answer["answers"]["aapStep1"]["depense"] : [];
		$depenses = array_filter($depenses, fn($d) => !empty($d["poste"]) && isset($d["price"]));
		foreach ($depenses as $depense) {
			$action = [
				"name"			=> $depense["poste"],
				"credits"		=> intval($depense["price"]),
				"parentType"	=> Project::COLLECTION,
				"parentId"		=> $db_answer["project"]["id"],
				"idParentRoom"	=> Room::getOrCreateActionRoom([
					"parentType"	=> Project::COLLECTION,
					"parentId"		=> $db_answer["project"]["id"],
				]),
				"status"		=> "todo",
				'creator'		=> $user,
				'idUserAuthor'	=> $user,
				"created"		=> time(),
			];
			if (!empty($depense["description"]))
				$action["description"] = $depense["description"];
			Yii::app()->mongodb
				->selectCollection(Action::COLLECTION)
				->insert($action);
			$db_actions[(string)$action["_id"]] = $action;
		}
		return (Rest::json([
			"success"	=> 1,
			"content"	=> $db_actions,
		]));
	}
}
