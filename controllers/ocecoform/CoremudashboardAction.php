<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\ocecoform;

use CTKAction, PHDB, Form;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;
use Rest;

class CoremudashboardAction extends CTKAction
{
    public function run($formid = null , $tableonly = false, $answer = null , $isAjax = false , $jsononly = false)
    {
        $onefield = false;
        if (empty($depenseid) && !empty($formid)) {
            $allAnswers = PHDB::find(Form::ANSWER_COLLECTION, array("form" => $formid), array("answers.aapStep1.titre", "answers.aapStep1.depense"));
        } else {
            $onefield = true;
            $allAnswers = [$answer => PHDB::findOneById(Form::ANSWER_COLLECTION, $answer, array("answers.aapStep1.titre", "answers.aapStep1.depense"))];
        }

        $allAnswersCoremu = Aap::answerCoremuTableVariables($allAnswers , $onefield , $tableonly);

        $allAnswersCoremu["mybarcolor"] = ["#50bd60", "#b30040"];
        $allAnswersCoremu["isAjax"] = $isAjax;
        $allAnswersCoremu["answer"] = $answer;
        $allAnswersCoremu["formid"] = $formid;
        if ($jsononly) {
            return Rest::json($allAnswersCoremu);
        } else {
            return $this->getController()->renderPartial("costum.views.custom.appelAProjet.callForProjects.coremu.coremudashboard", $allAnswersCoremu, true);
        }
    }
}
