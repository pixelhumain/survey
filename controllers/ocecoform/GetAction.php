<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\ocecoform;

use CTKAction;
use PHDB;
use Rest;

class GetAction extends CTKAction
{
    public function run($formid=null)
    {
        $params = [];
        if ($formid != null){
            $params["forms"] = PHDB::findOneById("forms", $formid);
            if (isset($params["forms"]["parent"])){
                foreach ($params["forms"]["parent"] as $idP => $parent){
                    $params["parent"] = PHDB::findOneById($parent["type"], $idP);
                }
            }
        }

        return Rest::json($params);
    }
}