<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\ocecoform;

use Authorisation;
use CTKAction, PHDB, Form, Aap;
use Rest;
use Yii;

class OnUpdatePingAction extends CTKAction
{
    public function run($answerId)
    {
        $coWs = Yii::app()->params['cows'];
        $curl = curl_init($coWs["pingRefreshCoformAnswerUrl"]);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode(["answer" => $answerId]));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_exec($curl);
    }
}
