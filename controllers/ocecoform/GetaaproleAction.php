<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\ocecoform;

use Authorisation;
use CTKAction, PHDB, Form, Aap;
use Rest;
use Yii;

class GetaaproleAction extends CTKAction
{
    public function run($answerId , $depenseposition = null , $candidateId = null)
    {
        $params = array(
            "isFormAdmin" => false,
            "candEditProposition" => false,
            "canCandidate" => false,
            "canEditCandidature" => false
        );

        $answers = PHDB::findOneById(Form::ANSWER_COLLECTION , $answerId);
        $forms = PHDB::findOneById(Form::COLLECTION , $answers["form"]);
        $element  = PHDB::findOneById($forms["parent"][array_keys($forms["parent"])[0]]["type"] , array_keys($forms["parent"])[0]);

        if (Authorisation::isUserSuperAdmin(Yii::app()->session['userId']) || Authorisation::isElementAdmin($element["_id"] , $element["type"] , Yii::app()->session['userId']) ){
            $params["isFormAdmin"] = true;
        }

        if ($depenseposition != null && !empty($answers["answers"]["aapStep1"]["depense"][$depenseposition])){
            if (
                (!empty($answers["answers"]["aapStep1"]["depense"][$depenseposition]["user"]) && Yii::app()->session['userId'] == $answers["answers"]["aapStep1"]["depense"][$depenseposition]["user"])
                || ( empty($answers["answers"]["aapStep1"]["depense"][$depenseposition]["user"])  && Authorisation::isElementMember($element["_id"] , $element["type"] , Yii::app()->session['userId']) )
                || $params["isFormAdmin"]
            ){
                $params["candEditProposition"] = true;
            }
        }

        if (
            Authorisation::isElementMember($element["_id"] , $element["type"] , Yii::app()->session['userId'])
            || $params["isFormAdmin"]
        ){
            $params["canCandidate"] = true;
        }

        if (
        $depenseposition != null && $candidateId != null &&
        (
            (!empty($answers["answers"]["aapStep1"]["depense"][$depenseposition]["estimates"][$candidateId]) && $answers["answers"]["aapStep1"]["depense"][$depenseposition]["estimates"][$candidateId] == Yii::app()->session['userId'])
            || (!empty($answers["answers"]["aapStep1"]["depense"][$depenseposition]["estimates"][$candidateId]["addedBy"]) &&  $answers["answers"]["aapStep1"]["depense"][$depenseposition]["estimates"][$candidateId]["addedBy"] == Yii::app()->session['userId'])
            || $params["isFormAdmin"]
        )
        ){
            $params["canEditCandidature"] = true;
        }

        return Rest::json($params);
    }
}