<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\ocecoform;

use CTKAction, PHDB, Form, Answer, Rest,Person,Project,Link,Yii;
class ConnectLinkAction extends CTKAction
{
	public function run($parentId,$parentType,$childType)
    {
		if(!empty($_POST["links"])){
			foreach ($_POST["links"] as $keylink => $valuelink) {
				$child = [];
				$child["childType"] = $childType;
				$child["childId"] = $valuelink;
				Link::connectParentToChild($parentId, $parentType, $child, false, Yii::app()->session["userId"], "");
			}
		}
    }
}