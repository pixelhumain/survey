<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;
use Answer;
use Authorisation;
use CTKAction;
use Form;
use MongoId;
use PHDB;
use Rest;
use Yii;

class GetAction extends CTKAction
{
	//if $slug : getd all the forms for an element.slug
	//if $id : opens the forms 
    public function run($form = null, $userId = null, $tpl = null)
    {
    	
    	$controller=$this->getController();
        $controller->layout = "//layouts/empty";
		$params = [];
		$params["form"] = PHDB::findOne( Form::COLLECTION, [ "_id"=>new MongoId($form) ] );
		$params["forms"] = [];
		foreach ($params["form"]["subForms"] as $ix => $formId) {
			$f = PHDB::findOne(Form::COLLECTION, ["id"=>$formId]);
			$params["forms"][$formId] = $f;
		}
		$params["what"] = (isset($params["form"]["what"])) ? $params["form"]["what"] : "réponses";
		$params['el'] =  Form::getFirstParentForm($params["form"]);
		$params["allAnswers"] = PHDB::find( Answer::COLLECTION, ["form"=> $form] );
		$params['canEdit'] =  ( isset(Yii::app()->session["userId"])  && isset($el["type"]) && isset($el["id"]) ) ? 
	    	Authorisation::canEditItem(Yii::app()->session["userId"],$el["type"], $el["id"])
	    	: false ;
		
   		$params["wizid"] = $form;

   		if(empty($tpl))
   			$tpl=(!empty($params["form"]["answersTpl"])) ? $params["form"]["answersTpl"] : 'survey.views.tpls.forms.cplx.answers';

   		if($tpl == "json"){
   			return Rest::json($params);
   		} else if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial( $tpl , $params, true);
        else
         	return $controller->render( $tpl , $params, true);

    }
}