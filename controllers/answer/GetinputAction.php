<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use Answer;
use Form;
use MongoId;
use PHDB;
use Yii;

class GetinputAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id=null, $answer=null, $mode=null, $form=null, $formstep=null, $inputid=null, $viewmode=null)
    {
        $this->getController()->layout = "//layouts/empty";
        $params = array();

        if(!empty($form))
            $form = PHDB::findOneById( Form::COLLECTION , $form);


        if (!empty(Yii::app()->session["userId"])){
            if(!empty($id)){

                $answer = PHDB::findOne( Form::ANSWER_COLLECTION, array("_id"=>new MongoId($id)));

                if(!empty($answer)){
                    //Rest::json($answer); exit;
                    if(empty($form) && !empty($answer["form"]))
                        $form = PHDB::findOneById( Form::COLLECTION , $form);

                    $params["answerId"]=$answer;

                    $params["form"] = $form ;
                    $params = Form::getDataForm($params);

                    $params["answer"] = $answer;

                    $params["inputid"] = $inputid;
                    $params["formStep"] = $formstep;
                    $params["anwsform"] = $form;

                    $params["rendermodal"] = true;

                    $canEditForm = false;
                    $canAdminAnswer = false;
                    $canEditAnswer = false;
                    $canSeeAnswer = false;

                    if( empty($mode) || ( $mode != "w" && $mode != "r" ) ||
                        ( !empty($answer["validated"]) && $answer["validated"] == true) )
                        $mode = "r";

                    if(!empty(Yii::app()->session['userId'])){
                        $canEditAnswer = Answer::canEdit($params["answer"], $form, Yii::app()->session['userId'], @$parentForm);
                        $canAdminAnswer = Answer::canAdmin($params["answer"], $form);

                        if($canEditAnswer === false && $canAdminAnswer === false)
                            $canSeeAnswer = Answer::canAccess($params["answer"], $form, Yii::app()->session['userId'], @$parentForm);
                        else
                            $canSeeAnswer = true;

                        if (isset($params["form"]["anyOnewithLinkCanAnswer"]) && $params["form"]["anyOnewithLinkCanAnswer"] == 'true') {
                            $canEditAnswer = true;
                            $canSeeAnswer = true;
                        }
                    }

                    if(!empty($parentForm["type"]) && ($parentForm["type"] == "aap" || $parentForm["type"] == "aapConfig") ){

                        if( $parentForm["type"] == "aap"){
                            $configEl = PHDB::findOneById(Form::COLLECTION, $parentForm["config"]);
                        }elseif ( $parentForm["type"] == "aapConfig"){
                            $configEl = $parentForm;
                        }

                        foreach ($configEl["subForms"] as $idConfigEl => $valueConfigEl){
                            $forms[$idConfigEl] = $valueConfigEl;

                        }

                        $form["subForms"] = array_keys($configEl["subForms"]);
                    }


                    if (!empty($parentForm["type"]) && $parentForm["type"] == "aapConfig" ){
                        $formsubForms = [];
                        foreach($configEl["subForms"] as $ksubF => $vsubF) {
                            if(isset($vsubF["active"]) && $vsubF["active"]==true) // check if aapCOnfig subForms (step) is active and push to subforms array
                                $formsubForms[] = $ksubF;
                        }
                    }else {
                        if (!empty($form["subForms"])) {
                            $formsubForms = $form["subForms"];
                        }
                    }
                    if (!empty($formsubForms[$formstep])){
                        $params["form"]["inputs"] = $formsubForms[$formstep];
                    }

                    if( $mode == "w" && $canEditAnswer === false && $canAdminAnswer === false )
                        $mode = "rw";

                    $params["parentForm"] = $form;
                    $params["standAlone"] = true;

                    if($canSeeAnswer === true ){
                        $params["canEditForm"] = $canEditForm;
                        $params["canAdminAnswer"] = $canAdminAnswer;
                        $params["canEdit"] = $canEditAnswer;
                        $params["canSee"] = $canSeeAnswer;
                        $params["mode"] = (!empty($mode) ? $mode : "r");
                        // Rest::json($form); exit();
                        $tpl= "survey.views.tpls.forms.formpart";
                        /*$inputs = PHDB::findOne(Form::INPUTS_COLLECTION,array("step"=>$formstep,"formParent" => (string)$form["_id"]));
                        $views = "<style>.portfolio-modal .modal-content{text-align:left}</style>";
                        $views .= $this->getController()->renderPartial($tpl, $params);
                        if(!empty($inputs["inputs"])){
                            foreach ($inputs["inputs"] as $key => $value) {
                                if($key != "decide" || $key != $inputid){
                                    $params["inputid"] = $key;
                                    $views .= $this->getController()->renderPartial($tpl, $params);
                                }
                            }
                        }
                        return $views;*/
                        //var_dump($tpl); exit();
                        return $this->getController()->renderPartial($tpl, $params);
                    } else {
                        if (Yii::app()->request->isAjaxRequest)
                            return $this->getController()->renderPartial("co2.views.default.unTpl", array("msg" => Yii::t("common", "You are not allowed to access to this answer"), "icon" => "fa-lock"));
                        else
                            return $this->getController()->render("co2.views.default.unTpl", array("msg" => Yii::t("common", "You are not allowed to access to this answer"), "icon" => "fa-lock"));
                    }
                } else {
                    if (Yii::app()->request->isAjaxRequest)
                        return $this->getController()->renderPartial("co2.views.default.unTpl", array("msg" => Yii::t("common", "You are not allowed to access to this answer , invalid url"), "icon" => "fa-lock"));
                    else
                        return $this->getController()->render("co2.views.default.unTpl", array("msg" => Yii::t("common", "You are not allowed to access to this answer , invalid url"), "icon" => "fa-lock"));
                }
            } else {
                if (Yii::app()->request->isAjaxRequest)
                    return $this->getController()->renderPartial("co2.views.default.unTpl", array("msg" => Yii::t("common", "You are not allowed to access to this answer , invalid url"), "icon" => "fa-lock"));
                else
                    return $this->getController()->render("co2.views.default.unTpl", array("msg" => Yii::t("common", "You are not allowed to access to this answer , invalid url"), "icon" => "fa-lock"));
            }
        } else {

            if (Yii::app()->request->isAjaxRequest)
                return $this->getController()->renderPartial("co2.views.default.unTpl", array("msg" => Yii::t("common", "You are not allow to access to this answer , not connected"), "icon" => "fa-lock"));
            else
                return $this->getController()->render("co2.views.default.unTpl", array("msg" => Yii::t("common", "You are not allow to access to this answer, not connected"), "icon" => "fa-lock"));

        }



    }
}
