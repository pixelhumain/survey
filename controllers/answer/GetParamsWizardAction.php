<?php
namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use Answer;
use Form;
use MongoId;
use PHDB;
use Yii;
use yii\helpers\Json;

class GetParamsWizardAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($form=null, $answer=null , $standAlone=false)
    {
        $params = Form::getParamsWizard( $form , $answer , filter_var($standAlone, FILTER_VALIDATE_BOOLEAN));
        return Json::encode($params);
    }
}
