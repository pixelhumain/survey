<?php
namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use Answer;
use Element;
use Form;
use PHDB;
use Yii;

class AddpropositionAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($contextId = null, $contextType = null, $answer = null)
    {

        // if (!empty(Yii::app()->session["userId"])) {
            if ($contextId != null && $contextType != null && $answer == null) {

                $params = [];
                $el = Element::getByTypeAndId($contextType, $contextId);

                $params = [ "el"=>$el];
                //$params["forms"] = PHDB::find( Form::COLLECTION, [ "parentSlug"=>$slug ] );
                $params["forms"] = PHDB::find( Form::COLLECTION, [ "parent.".$el["_id"] => array('$exists' => 1) ] );
                $params["forms"] += PHDB::find( Form::COLLECTION, [ "formCommunity.".$el["_id"] => array('$exists' => 1) ] );


                if(isset($params["forms"]))
                {
                    foreach ( $params["forms"] as $fix => $f )
                    {
                        $params["forms"][$fix]['canEditForm'] = Form::canAdmin(Yii::app()->session['userId'], $f);
                        $formId = "";
                        if(isset($f['subForms'])){
                            foreach ($f['subForms'] as $ix => $fid)
                            {
                                if($formId != "")
                                    $formId .= "|";
                                if (!is_array($fid)){
                                    $formId .= $fid;
                                }else {
                                    foreach ($fid as $idf => $valuef){
                                        if (is_string($idf)){
                                            $formId.= $idf;
                                        }else{
                                            $formId.= "";
                                        }
                                    }
                                }
                                $params["forms"][$fix][$fid] = PHDB::findOne( Form::COLLECTION, [ "id"=>$fid ] );
                            }
                        }
                        // $params["forms"][$fix]["answers"] = PHDB::count( Form::ANSWER_COLLECTION, ["formId"=>$formId , "parentSlug" => $slug] );

                        $params["forms"][$fix]["answers"] = PHDB::count( Answer::COLLECTION, ["form"=>$fix] );
                    }
                }

                return $this->getController()->renderPartial("costum.views.custom.appelAProjet.addproposition",["formData" => $params] );
            }
        // }else{
        //     if(Yii::app()->request->isAjaxRequest)
        //         return $this->getController()->renderPartial("co2.views.default.unTpl",array("msg"=>"Vous n'êtes pas connecté ","icon"=>"fa-lock"));
        //     else
        //         return $this->getController()->render("co2.views.default.unTpl",array("msg"=>"Vous n'êtes pas connecté ","icon"=>"fa-lock"));
        // }
    }
}