<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use Authorisation;
use CAction;
use Element;
use Form;
use Link;
use Person;
use PHDB;
use Slug;
use Yii;

/**
 * Display the directory of back office
 * @param String $id Not mandatory : if specify, look for the person with this Id.
 * Else will get the id of the person logged
 * @return type
 */
class DashboardAction extends \PixelHumain\PixelHumain\components\Action {
	public function run($id = null, $answer = null, $form = null, $title = "", $contextId = null, $contextType = null, $elSlug = null) {
		$controller = $this->getController();
		if ($contextId != null || $contextType != null) {
			$formanswers = PHDB::find(Form::ANSWER_COLLECTION, array("form" => $answer, "context." . $contextId => array('$exists' => 1)));
		} else {
			$formanswers = PHDB::find(Form::ANSWER_COLLECTION, array("form" => $answer));
		}

		$prntForm = PHDB::findOneById(Form::COLLECTION, $answer);

		$countForm = 0;
		$tittleList = [];
		$adminRight = false;

		$answerStateOption = [];
		if (!empty($prntForm["params"]["answerStateOption"])) {
			$answerStateOption = $prntForm["params"]["answerStateOption"];
		}

		//permission to all Dashboard
		$canAddAnswer = false;
		$canEditEachotherAnswer = false;
		$canReadEachOtherAnswer = false;

		if ($contextId != null || $contextType != null) {
			$myanswers = PHDB::find(Form::ANSWER_COLLECTION, array("form" => $answer, "user" => Yii::app()->session["userId"], "context." . $contextId => array('$exists' => 1)));
		} else {
			$myanswers = PHDB::find(Form::ANSWER_COLLECTION, array("form" => $answer, "user" => Yii::app()->session["userId"]));
		}

		if ($contextId != null || $contextType != null) {
			$itemId = $contextId;
			$itemType = $contextType;
		} else {
			foreach ($prntForm["parent"] as $prntFormkey => $prntFormvalue) {
				$itemId = $prntFormkey;
				$itemType = $prntFormvalue["type"];
			}
		}

		// var_dump($itemId, $itemType, Yii::app()->session["userId"]);exit();

		if (
			(
				//is form active
				isset($prntForm["active"]) &&
				$prntForm["active"] == "true"
			)
			&&
			(
				//is form between start and end date 
				!isset($prntForm["startDate"]) ||
				(time() > strtotime(str_replace("/", "-", $prntForm["startDate"])) &&
					(!isset($prntForm["endDate"]) || time() <= strtotime(str_replace("/", "-", $prntForm["endDate"])))) ||
				(isset($prntForm["endDate"]) && $prntForm["endDate"] == "")
			)
			&&
			(
				// is one answer per person
				!isset($prntForm["oneAnswerPerPers"]) ||
				(
					isset($prntForm["oneAnswerPerPers"]) &&
					$prntForm["oneAnswerPerPers"] == "false"
				) ||
				(
					isset($prntForm["oneAnswerPerPers"]) &&
					$prntForm["oneAnswerPerPers"] == "true" && empty($myanswers)

				)
			)
			&&
			(
				//private
				!isset($prntForm["private"]) ||
				(
					isset($prntForm["private"]) &&
					$prntForm["private"] == "false"
				)
				||
				(
					isset($prntForm["private"]) &&
					$prntForm["private"] == "true" && Link::isLinked($itemId, $itemType, Yii::app()->session["userId"])
				)
			)
			&&
			!empty(Yii::app()->session["userId"])


		) {
			$canAddAnswer = true;
		}

		if (
			(
				//is form active
				isset($prntForm["active"]) &&
				$prntForm["active"] == "true"
			)
			&&
			(
				//is form between start and end date 
				!isset($prntForm["startDate"]) ||
				(time() > strtotime(str_replace("/", "-", $prntForm["startDate"])) &&
					(!isset($prntForm["endDate"]) || time() <= strtotime(str_replace("/", "-", $prntForm["endDate"])))) ||
				(isset($prntForm["endDate"]) && $prntForm["endDate"] == "")
			)
			&&
			(
				//private
				!isset($prntForm["private"]) ||
				(
					isset($prntForm["private"]) &&
					$prntForm["private"] == "false"
				)
				||
				(
					isset($prntForm["private"]) &&
					$prntForm["private"] == "true" && Link::isLinked($itemId, $itemType, Yii::app()->session["userId"])
				)
			)

		) {
			$canEditEachotherAnswer = true;
		}


		if (
			(
				//is form active
				isset($prntForm["active"]) &&
				$prntForm["active"] == "true"
			)
			&&
			(
				//private
				!isset($prntForm["private"]) ||
				(
					isset($prntForm["private"]) &&
					$prntForm["private"] == "false"
				)
				||
				(
					isset($prntForm["private"]) &&
					$prntForm["private"] == "true" && Link::isLinked($itemId, $itemType, Yii::app()->session["userId"])
				)
			)
			&&
			(
				//private
				!isset($prntForm["canReadOtherAnswers"]) ||
				(
					isset($prntForm["canReadOtherAnswers"]) &&
					$prntForm["canReadOtherAnswers"] == "true"
				)
			)
		) {
			$canReadEachOtherAnswer = true;
		}

		$titletype = [
			"text",
			"select",
			"tpls.forms.cplx.multitextvalidation",
			"tpls.forms.cplx.multiCheckboxPlus",
			"tpls.forms.cplx.multiRadio",
			"tpls.forms.cplx.name"
		];


		if (isset($prntForm["active"])) {
			$active = filter_var($prntForm["active"], FILTER_VALIDATE_BOOLEAN);
		}
		if (isset($prntForm["canModify"])) {
			$canModify = filter_var($prntForm["canModify"], FILTER_VALIDATE_BOOLEAN);
		}
		if (isset($prntForm["canReadOtherAnswers"])) {
			$canReadOtherAnswers = filter_var($prntForm["canReadOtherAnswers"], FILTER_VALIDATE_BOOLEAN);
		}
		if (isset($prntForm["oneAnswerPerPers "])) {
			$oneAnswerPerPers  = filter_var($prntForm["oneAnswerPerPers "], FILTER_VALIDATE_BOOLEAN);
		}
		if (isset($prntForm["private "])) {
			$private  = filter_var($prntForm["private "], FILTER_VALIDATE_BOOLEAN);
		}
		if (isset($prntForm["showAnswers"])) {
			$showAnswers = filter_var($prntForm["showAnswers"], FILTER_VALIDATE_BOOLEAN);
		}

		foreach ($prntForm["parent"] as $ip => $vp) {
			$communityLinks = Element::getCommunityByTypeAndId($vp["type"], $ip);
			$user = $communityLinks[Yii::app()->session["userId"]] ?? [];
			if (!empty($user["isAdmin"]) && filter_var($user["isAdmin"], FILTER_VALIDATE_BOOL) && empty($user["isAdminPending"]))
				$adminRight = true;
			else if (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]))
				$adminRight = true;
		}

		if (isset($prntForm["subForms"])) {
			if (isset($prntForm["type"]) && $prntForm["type"] == "aapConfig") {
				$prntForm["subForms"] = array_keys($prntForm["subForms"]);
			}
			for ($i = 0; $i < sizeof($prntForm["subForms"]); $i++) {


				${"childForm" . $i} = PHDB::find(Form::COLLECTION, array("id" => $prntForm["subForms"][$i]));

				//array_push($sectionName, $prntForm["subForms"][$i]);

				// array_push($sectionId, $prntForm["subForms"][$i]);
				//if(isset(var))
				//array_push($sectionName, ${"childForm".$i}["name"]);
				// var_dump(${"childForm".$i});
				foreach (${"childForm" . $i} as $key => $value) {
					if (isset($value["inputs"])) {
						$countForm += sizeof($value["inputs"]);

						foreach ($value["inputs"] as $inpId => $inp) {
							if (in_array(
								$inp["type"],
								$titletype
							)) {
								array_push($tittleList, [
									"sectionId" => $prntForm["subForms"][$i],
									"sectionName" => $value["name"],
									"title" => $inp["label"],
									"id" => $inpId
								]);
							}
						}
					}
				}
			}
		}

		$title = [];
		$title[0] = "";
		$title[1] = "";
		$what = "dossiers ";

		if (isset($prntForm["params"]["inputForAnswerName"]["title"])) {
			$inputForAnswerNamekey = array_search($prntForm["params"]["inputForAnswerName"]["title"], array_column($tittleList, 'id'));
			$title[0] = $tittleList[$inputForAnswerNamekey]["sectionId"];
			$title[1] = $prntForm["params"]["inputForAnswerName"]["title"];
		}

		if (isset($prntForm["type"]) && $prntForm["type"] == "aap") {
			$configEl = PHDB::findOneById(Form::COLLECTION, $prntForm["config"]);
			if (isset($configEl["params"]["inputForAnswerName"]["title"])) {
				$inputForAnswerNamekey = array_search($configEl["params"]["inputForAnswerName"]["title"], array_column($tittleList, 'id'));
				$title[0] = $tittleList[$inputForAnswerNamekey]["sectionId"];
				$title[1] = $configEl["params"]["inputForAnswerName"]["title"];
			}

			if (isset($parentForm["type"]) && ($parentForm["type"] == "aap" || $parentForm["type"] == "aapConfig")) {
				if ($parentForm["type"] == "aap") {
					$configEl = PHDB::findOneById(Form::COLLECTION, $parentForm["config"]);
				} elseif ($parentForm["type"] == "aapConfig") {
					$configEl = $parentForm;
				}

				if (!empty($configEl)) {
					$inputsObl = PHDB::findOneById(Form::INPUTS_COLLECTION, $configEl["subForms"][$formId]["inputs"]);
					if (!empty($inputsObl["inputs"])) {
						$form["inputs"] = $inputsObl["inputs"];
						foreach ($form["inputs"] as $idInputs => $valInputs) {
							$form["inputs"][$idInputs]["canAdmin"] = Authorisation::isParentAdmin((string)$configEl["_id"], Form::COLLECTION, Yii::app()->session["userId"]);
							$form["inputs"][$idInputs]["parentid"] = (string)$inputsObl["_id"];
						}
					}
				}


				if ($parentForm["type"] == "aap") {
					$inputsapp = PHDB::findOne(Form::INPUTS_COLLECTION, array("formParent" => (string)$parentForm["_id"], "step" => $formId));

					if (!empty($inputsapp["inputs"])) {
						foreach ($inputsapp["inputs"] as $idInputs => $valInputs) {
							$inputsapp["inputs"][$idInputs]["canAdmin"] = Authorisation::isParentAdmin((string)$parentForm["_id"], Form::COLLECTION, Yii::app()->session["userId"]);
							$inputsapp["inputs"][$idInputs]["parentid"] = (string)$inputsapp["_id"];
						}
					}

					if (!empty($form["inputs"]) && !is_string($form["inputs"]) && !empty($inputsapp["inputs"])) {
						$form["inputs"] = array_merge($form["inputs"], $inputsapp["inputs"]);
					} elseif (!empty($inputsapp["inputs"])) {
						$form["inputs"] = $inputsapp["inputs"];
					}
				}

				$form["id"] = $formId;

				//    $inputsEl = PHDB::findOne(Form::INPUTS_COLLECTION, array( "parent.".$contextId => array('$exists'=>1), "subForm" => $formId ));
			}
		}

		if (isset($prntForm["what"])) {
			$what .= $prntForm["what"];
		}

		$allfields = [];

		//variable declaration
		$index = [];
		$updatedate = [];
		$countAns = [];
		$percentage = [];
		$users = [];

		$responsetitle = [];
		$right = [];
		$submited = [];

		$badge = [];


		foreach ($formanswers as $idAns => $valueAns) {
			if (isset($valueAns["answers"]) || $valueAns["user"] == Yii::app()->session["userId"]) {

				if (isset($valueAns["answers"][$title[0]][$title[1]])) {
					array_push($responsetitle, $valueAns["answers"][$title[0]][$title[1]]);
				} else {
					array_push($responsetitle, $what);
				}

				$cn = false;
				if (isset($valueAns["user"]) && $valueAns["user"] == Yii::app()->session["userId"]) {
					$cn = true;
				}
				if (isset($valueAns["links"]["answered"])) {
					foreach ($valueAns["links"]["answered"] as $linkid => $linkvalue) {
						if ($linkid == Yii::app()->session["userId"]) {
							$cn = true;
						}
					}
				}

				array_push($right, $cn);

				$countrsp = 0;
				$percntrsp = 0;
				if (empty($valueAns["answers"])) {
					$valueAns["answers"] = [];
				}
				foreach ($valueAns["answers"] as $formName => $formAns) {
					$countrsp += sizeof($formAns);
				}
				$percntrsp = ($countrsp * 100) / $countForm;

				$countrsp =  sprintf("%02d", $countrsp);
				$percntrsp = round($percntrsp, 2);

				$us = [];
				if (isset($valueAns["links"]["answered"])) {

					for ($i = 0; $i < sizeof($valueAns["links"]["answered"]); $i++) {
						$u = PHDB::findOneById(Person::COLLECTION, $valueAns["links"]["answered"][$i]);
						array_push($us, $u);
					}
				}

				if (isset($valueAns["submited"])) {
					array_push($submited, $valueAns["submited"]);
				} else {
					array_push($submited, []);
				}

				if (isset($valueAns["status"])) {
					array_push($badge, $valueAns["status"]);
				} else {
					array_push($badge, []);
				}

				array_push($users, $us);
				array_push($index, $idAns);
				if (isset($valueAns["updated"])) {
					array_push($updatedate, date("d-m-Y", $valueAns["updated"]));
				} else {
					array_push($updatedate, "");
				}
				array_push($countAns, $countrsp);
				array_push($percentage, $percntrsp);

				// if(isset($value["updated"])){
				//     $dated;
				//     $dated = new MongoDate($value["updated"]):

				//     array_push($updatedate, date('Y-m-d H:i:s', $dated->sec));
				// }
			}
		}

		$allfields = [
			"index" => $index,
			"updatedate" => $updatedate,
			"countAns" => $countAns,
			"submited" => $submited,
			"countForm" => sprintf("%02d", $countForm),
			"percentage" => $percentage,
			"responsetitle" => $responsetitle,
			"users" => $users,
			"titleparams" => $tittleList,
			"parentFormId" => $answer,
			"selectedtitleparams" => $title[1],
			"dossier" => $what,
			"right" => $right,
			"badge" => $badge
		];

		// $allfields = [$index,$updatedate,$countAns,$countForm];
		$elBySlug = Slug::getElementBySlug($elSlug, array("name", "type", "collection", "profilImageUrl", "slug", "oceco", "links"));
		$params = [
			"answerStateOption" => $answerStateOption,
			"allanswers" => $allfields,
			"formanswers" => $formanswers,
			"adminRight" => $adminRight,
			"p_active" => @$active,
			"p_canModify" => @$canModify,
			"p_canReadOtherAnswers" => @$canReadOtherAnswers,
			"p_oneAnswerPerPers " => @$oneAnswerPerPers,
			"p_private " => @$private,
			"p_showAnswers" => @$showAnswers,
			"canAddAnswer" => $canAddAnswer,
			"canEditEachotherAnswer" => $canEditEachotherAnswer,
			"canReadEachOtherAnswer" => $canReadEachOtherAnswer,
			"contextId" => $contextId,
			"contextType" => $contextType,
			"elBySlug" => $elBySlug
		];

		$tpl = "survey.views.tpls.answers.dashboard";
		return $controller->renderPartial($tpl, $params, true);
	}
}
