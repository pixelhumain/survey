<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;
use Answer;
use CAction;
use Rest;

class ValidateAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        //$controller = $this->getController();
    	$params = $_POST;
    	$params["controller"] = $this->getController();
        $res = Answer::validate($params);
        return Rest::json($res);
    }
 }
