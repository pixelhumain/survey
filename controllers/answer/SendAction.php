<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;
use Answer;
use CAction;
use Rest;

class SendAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run($id, $tpl="validation", $step=null) {
        $controller=$this->getController();
        $msg=null;
        $infos=array();
        if(isset($_POST["tpl"]))
          $tpl=$_POST["tpl"];
        if(isset($_POST["step"]))
          $step=$_POST["step"];
        // ALL infos       
        if(isset($_POST["tplObject"]))
          $infos["tplObject"]=$_POST["tplObject"];
        if(isset($_POST["html"]))
          $infos["html"]=$_POST["html"];
        if(isset($_POST["msg"]))
          $infos["msg"]=$_POST["msg"];
        if(isset($_POST["tplMail"]))
          $infos["emails"]=$_POST["tplMail"];
        if(isset($_POST["listContact"]))
          $infos["community"]=$_POST["listContact"];
        
        $mailParams=Answer::mailProcess($id, $tpl, $step, $infos);
        return Rest::json(array("result"=>true, "msg"=>"Ok : webhook handdled"));
    }
}
