<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use ActStr;
use Answer;
use Cron;
use Element;
use Form;
use HtmlHelper;
use Mail;
use MongoId;
use MongoRegex;
use Notification;
use Person;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;
use Yii;
use Rest;
use yii\helpers\Url;

/**
 * Display the directory of back office
 * @param String $id Not mandatory : if specify, look for the person with this Id.
 * Else will get the id of the person logged
 */
class AnswerwithemailAction extends \PixelHumain\PixelHumain\components\Action
{

    public function run($key=null, $id=null, $form=null, $answer=null, $mode=null, $email=null, $standalone = false, $resLocation = ''){

        if (isset($_POST["id"]) && isset($_POST["email"]) && isset($_POST["form"])){
            $id = $_POST["id"];
            $form = $_POST["form"];
            $email = $_POST["email"];
        }
        if(isset($_POST["resLocation"])) {
            $resLocation = $_POST["resLocation"];
        }
        $candirectaccess = false;

        if (!empty($form) && $id == "new")
            $parentform = PHDB::findOneById(Form::COLLECTION, $form);
        elseif( !empty($id) && $id != "new") {
            $allansws = PHDB::findOneById(Form::ANSWER_COLLECTION, $id);

            $parentform = PHDB::findOneById(Form::COLLECTION, $allansws["form"]);

        }

        if (
                isset($parentform["temporarymembercanreply"]) && ($parentform["temporarymembercanreply"] == true || $parentform["temporarymembercanreply"]=="true")
            && isset($parentform["withconfirmation"]) && $parentform["withconfirmation"] == false
        ){
            $timezone = !empty(Yii::app()->session["timezone"]) ? Yii::app()->session["timezone"] : "UTC";
            date_default_timezone_set($timezone);
            $startDate = !empty($parentForm["startDateNoconfirmation"]) ? $parentForm["startDateNoconfirmation"]->toDateTime()->format('c') : null;
            $endDate = !empty($parentForm["endDateNoconfirmation"]) ? $parentForm["endDateNoconfirmation"]->toDateTime()->format('c') : null;
            $now = date('d-m-Y H:i', time());
            $checkDate = Api::checkDateStatus($now,$startDate,$endDate);

            if(
                (
                    ( !isset($parentForm["session"]) || $parentForm["session"] == false ) &&
                    ($checkDate == "include" )
                ) ||
                (
                    (isset($parentForm["session"]) && $parentForm["session"] == true && isset($parentForm["sessionMapping"]) && isset($parentForm["actualSession"])) &&
                    (Api::path_get_value($answer, $parentForm["sessionMapping"] , '.') == null || Api::path_get_value($answer, $parentForm["sessionMapping"] , '.') == $parentForm["actualSession"] ) &&
                    ($checkDate == "include" )
                )
            ) {
                return $this->processAnswer(["id" => $id, "form" => $form, "answer" => $answer, "mode" => $mode, "email" => $email], false, $parentform, $mode, $standalone, $resLocation);
            } else {
                if($checkDate == "past")
                    return "Les réponses commenceront le ".$parentform["startDateNoconfirmation"];
                else if ($checkDate == "late")
                    return Rest::json([ "result" => false , "msg" => "Le temps de réponses est écoulé depuis le ".$parentform["endDateNoconfirmation"] ]);

            }

        } else {
            if($key){
                return $this->processAnswer($key, true, null, $mode, $standalone, $resLocation);
            }else if($id && $email){
                if ($id == "new"){
                    $answerD = Answer::generateAnswer($parentform, false);
                    $id = (string)$answerD["_id"];
                }
                $params = [
                    "id"=>$id,
                    "email"=>$email,
                    "form"=>$form,
                    "answer"=>$answer,
                    "mode"=>$mode
                ];
                return $this->sendConfirmationMail($id, $email , $resLocation);
            }
        }

    }

    function sendConfirmationMail($id, $email , $resLocation){
        $user = PHDB::findOne(Person::COLLECTION, array( "email" => new MongoRegex( '/^'.preg_quote(trim($email)).'$/i' ) ), array("_id", "name", "email"));

        if(empty($user)){
            $key = $this->encrypt_decrypt("encrypt", json_encode(["id"=>$id, "email"=>$email , "resLocation"=>$resLocation]));

            $urlToSend = Yii::app()->getRequest()->getBaseUrl(true)."/survey/answer/answerwithemail/key/".$key;

            $form = $this->getAnswerForm($id);
            $formParent = [];
            foreach($form["parent"] as $k => $v){
                $formParent = $v;
                $formParent["id"] = $k;
            }
            $params = [
                "tpl"=>"answer.emailConfirmation",
                "tplObject"=>"Repondre au formulaire ".$form["name"]." de ".$formParent["name"],
                "tplMail"=>$email,
                "url"=>$urlToSend,
                "urlToSend"=>$urlToSend,
                "formName"=>$form["name"],
                "formParent"=>$formParent
            ];

            if($resLocation != ""){
                $params['resLocation'] = $resLocation;
            }

            $mailParams = [
                "type" => Cron::TYPE_MAIL,
                "tpl"=>$params["tpl"],
                "subject" => $params["tplObject"],
                "from"=>Yii::app()->params['adminEmail'],
                "to" => $params["tplMail"],
                "tplParams" => Mail::initTplParams($params)
            ];
            $mailParams=Mail::getCustomMail($mailParams);
            Mail::schedule($mailParams);

            return Rest::json([
                "status"=>true,
                "msg"=> "Nous avons envoyé l'invitation dans votre boite email."
            ]);
        }else{
            return Rest::json([
                "status"=>false,
                "msg"=> "L'adresse email que vous avez fournie existe déjà."
            ]);
        }
    }

    function processAnswer($key, $needkey=true, $parentForm = null, $mode=null, $standalone=null, $resLocation = ""){

        if (!$needkey){
            $params = $key;
        }else{
            $params = $this->encrypt_decrypt("decrypt", $key);
        }

        if($params || !$needkey){
            if ($needkey){
                $params = json_decode($params,true);
            }

            $user = PHDB::findOne(Person::COLLECTION, array( "email" => new MongoRegex( '/^'.preg_quote(trim($params["email"])).'$/i' ) ));
            $userFormRes = [];

            $id = $params["id"];
            $formTitle = 'nom';

            if (!empty($user) && $id == "new" && !empty($form) ){
                $userFormRes = PHDB::findOne(Form::ANSWER_COLLECTION, array("user" => (string)$user["_id"], "form" => $form));
            }

            if(isset($parentForm['name'])) {
                $formTitle = $parentForm['name'];
            }

            if(empty($user) && empty($userFormRes)){
                //obligé de creer user car on a toujours besoin d'un userID
                $pwdRandom =  Person::random_password(8);
                $newPerson = array("name" => Person::generedUserNameByEmail($params["email"]), "username" => Person::generedUserNameByEmail($params["email"]), "email" => $params["email"], "pwd" => $pwdRandom);

                $user=Person::insert(
                    $newPerson,
                    Person::REGISTER_MODE_TWO_STEPS,
                    null,
                    [
                        'mailToResend' => [
                            'pwd' => $pwdRandom,
                            'redirectUrl' => ''
                        ]
                    ]
                );

                $user = PHDB::findOneById(Person::COLLECTION, $user["id"]);

                if( $user ){
                    Person::saveUserSessionData($user);
                }

                if($id == "new"){
                    $parentff = PHDB::findOneById(Form::COLLECTION, $params["form"]);

                    $answerData = Answer::generateAnswer($parentff, false);
                    if($user && isset($user['email'])) {
                        $newPerson["_id"] = (string) $user["_id"];
                        $standaloneRedir=(!empty($standalone) && $standalone==true) ? ".standalone.true" : "";
                        $modeRedir=(!empty($mode)) ? ".mode.".$mode : ".mode.r";
                        $redirectUrl = 'answer.index.id.'. (string) $answerData["_id"] .$modeRedir.$standaloneRedir;
                        Mail::validatePersonWithNewPwd($newPerson, $pwdRandom, $formTitle, $redirectUrl);
                        Person::updatePersonField((string) $user['_id'], 'mailToResend', ['pwd' => $pwdRandom, 'redirectUrl' => $redirectUrl, 'formTitle' => $formTitle], (string) $user['_id']);
                    }
                }else{
                    $answerData = Answer::getById($id);
                }

                $contextId = ""; $contextType = "";
                foreach($answerData["context"] as $k => $v){
                    $contextId = $k;
                    $contextType = $v["type"];
                }

                $context = Element::getByTypeAndId($contextType, $contextId);

                if($id == "new"){
                    $parentff = PHDB::findOneById(Form::COLLECTION, $params["form"]);

                    // $answerData = Answer::generateAnswer($parentff, false);

                    if (isset($parentff["type"]) && $parentff["type"] == "aap"){
                        //$baseurl = (!empty($context["costum"])) ? Yii::app()->getBaseUrl(true) . "/costum/co/index/slug/" . $context["slug"] : Yii::app()->getBaseUrl(true);
                        //var_dump($answerData);exit;
                        $formconfig = PHDB::findOneById(Form::COLLECTION, $parentff["config"]);

                        foreach($formconfig["parent"] as $k => $v){
                            $contextIdconfig = $k;
                            $contextTypeconfig = $v["type"];
                        }

                        $contextconfig = Element::getByTypeAndId($contextTypeconfig, $contextIdconfig);

                        $baseurl = (!empty($contextconfig["costum"])) ? Yii::app()->getBaseUrl(true) . "/costum/co/index/slug/" . $contextconfig["slug"] : Yii::app()->getBaseUrl(true);

                        if(!$needkey) {
                            return Rest::json([
                                "msg" => "Vous allez être automatiquement redirigé",
                                "status" => true,
                                "answerId" => (string)$answerData["_id"],
                                "location" => $resLocation == "" ? $baseurl . "#answer.index.id.new.form." . (string)$params["form"] . ".mode.w.standalone.true" : $baseurl . $resLocation
                            ]);
                        }else {
                                // INTREGRER LES COSTUMS QUI N'UTILISE PAS DE TEMPLATE DU TYPE CTE !
                            if(isset($params["resLocation"]) && $params["resLocation"] != "" ){
                                return Yii::$app->response->redirect($params["resLocation"]);
                            }else {
                                return Yii::$app->response->redirect(Url::to($baseurl . "#answer.index.id.new.form." . (string)$params["form"] . ".mode.w.standalone.true"), '302')->send();
                            }
                        }
                    }else {
                        $baseurl = (!empty($context["costum"])) ? Yii::app()->getBaseUrl(true) . "/costum/co/index/slug/" . $context["slug"] : Yii::app()->getBaseUrl(true);
                        //var_dump($answerData);exit;

                        if (!$needkey) {
                            return Rest::json([
                                "msg" => "Vous allez être automatiquement redirigé",
                                "status" => true,
                                "answerId" => (string)$answerData["_id"],
                                "location" => $resLocation == "" ? $baseurl . "/#answer.index.id." . (string)$answerData["_id"] . ".mode.w" : $baseurl . $resLocation
                            ]);
                        }else {
                            // INTREGRER LES COSTUMS QUI N'UTILISE PAS DE TEMPLATE DU TYPE CTE !
                            if(isset($params["resLocation"]) && $params["resLocation"] != "" ){
                                return Yii::$app->response->redirect($params["resLocation"]);
                            }else {
                                return Yii::$app->response->redirect(Url::to($baseurl . "/#answer.index.id." . $answerData["_id"] . ".mode.w"), '302')->send();
                            }
                        }
                    }
                }else{
                    $baseurl = (!empty($context["costum"])) ? Yii::app()->getBaseUrl(true)."/costum/co/index/slug/".$context["slug"] : Yii::app()->getBaseUrl(true);

                    //var_dump($id);exit;

                    if(!$needkey) {
                        return Rest::json([
                            "msg" => "Vous allez être automatiquement redirigé",
                            "status" => true,
                            "answerId" => $id,
                            "location" => $resLocation == "" ? $baseurl . "/#answer.index.id." . $id . ".mode.w" : $baseurl . $resLocation
                        ]);
                    }else{

                        // INTREGRER LES COSTUMS QUI N'UTILISE PAS DE TEMPLATE DU TYPE CTE !
                        if(isset($params["resLocation"]) && $params["resLocation"] != "" ){
                            return Yii::$app->response->redirect($params["resLocation"]);
                        }else {
                            return Yii::$app->response->redirect(Url::to($baseurl."/#answer.index.id.".$id.".mode.w"), '302')->send();
                        }
                    }

                }

            }else{
                $answer=(isset($params["form"]) && isset($user["_id"])) ? PHDB::findOne(Form::ANSWER_COLLECTION,array("form"=>$params["form"],"user"=>(string)$user["_id"])) : null;
                Person::saveUserSessionData($user);
                if(!empty($answer)){
                    $baseurl = (isset($_POST["costumSlug"]) && !empty($_POST["costumSlug"])) ? Yii::app()->getBaseUrl(true) . "/costum/co/index/slug/" . $_POST["costumSlug"] : Yii::app()->getBaseUrl(true);

                    // $validationKey =Person::getValidationKeyCheck($user["_id"]);
                    // $urlValidation = $baseurl;
                    //var_dump(Yii::$app->response->redirect);exit;
                    return Yii::$app->response->redirect(Url::to($baseurl."/#answer.index.id.".(string)$answer["_id"].".mode.w"), '302')->send();

                }else{
                    return Rest::json([
                        "msg" => "L'adresse email que vous avez fournie existe déjà. Veuillez vous connectez à votre compte pour pouvoir répondre.",
                        "status" => false
                    ]);
                }


            }
        }else{
            return Rest::json([
                "msg" => "Invalid key",
                "status" => false
            ]);
        }
    }

    function encrypt_decrypt($action, $string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = "Z3X6TB0awl";
        $secret_iv = 'MKj9xChXa2';
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if( $action == 'decrypt' ) {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }

    function getAnswerForm($answerId){
        $answer = PHDB::findOneById(Answer::COLLECTION, $answerId);
        if(!$answer)
            return null;
        return PHDB::findOneById(Form::COLLECTION, $answer["form"]);
    }
}
