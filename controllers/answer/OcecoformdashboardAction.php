<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use CAction, PHDB, Form, Actions, mongoId, Yii;
class OcecoformdashboardAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($answerid=null)
    {
        $action = [];
        $project = [];
        $title = "";
        $tplDashboard = "survey.views.tpls.answers.ocecoformdashboard";

        $controller = $this->getController();

        $answers = PHDB::find(Form::ANSWER_COLLECTION, array("_id" => new MongoId($answerid),"answers.aapStep1.titre" => ['$exists' => true]));

        foreach ($answers as $key => $value) {
            if (!isset($answers[$key]["answers"])) {
                unset($answers[$key]);
            }
            $formparentid = $value["form"];
        }

        $fp = PHDB::findOneById(Form::COLLECTION, $formparentid);
        if (isset($fp["type"]) && $fp["type"] == "aap"){
            $fpconfig = PHDB::findOneById(Form::COLLECTION, $fp["config"]);
            if (isset($fpconfig["tplDashboard"])){
                $tplDashboard = $fpconfig["tplDashboard"];
            }
        }

        $el_parent_slug = "";

        if (isset($fp["parent"][array_keys($fp["parent"])[0]])) {
            $parent = PHDB::findOneById($fp["parent"][array_keys($fp["parent"])[0]]["type"], array_keys($fp["parent"])[0]);

            if (isset($parent["parent"])){
                $el_parent_id = array_keys($parent["parent"])[0];
                $mfp = PHDB::findOne(Form::COLLECTION, array("config" => $fp["config"] , "parent.".$el_parent_id => ['$exists' => true]  ));
                $el_parent_form_id = (string)$mfp["_id"];
                $mparent = PHDB::findOneById( $parent["parent"][array_keys($parent["parent"])[0]]["type"] , array_keys($parent["parent"])[0] );
                $el_parent_slug = $mparent["slug"];
                $haveparent = true;
            }else{
                $el_parent_form_id = "";
                $haveparent = false;
                $el_parent_slug = "";
            }
        }else{
            $el_parent_form_id = "";
            $el_parent_slug = "";
            $haveparent = false;
        }

        $parent = [];

        foreach ($fp["parent"] as $keyp => $valuep) {
            $parent = $valuep;
            break;
        }

        foreach ($answers as $anskey => $ansvalue) {
            if (!empty($ansvalue["project"]["id"])) {

                $action[$anskey] = PHDB::find( Actions::COLLECTION,  array( "parentId" => $ansvalue["project"]["id"])  );

            }else{
                $action[$anskey] = [];
            }
        }

        $action[(string)$parent["_id"]] = PHDB::find( Actions::COLLECTION,  array( "parentId" => (string)$parent["_id"])  );

        foreach ($action as $actionid => $act) {
            if ( empty( $act["tasks"])){
                if ($act["status"] == "todo"){
                    $action[$actionid]["tasks"][] = array("task" => $act["name"] , "userId" => $act["creator"] , "checked" => false);
                }
            }
        }
        $allData = $answers;

        foreach ($answers as $key => $value) {
            if (isset($answers[$key]["answers"]["aapStep1"]["depense"])) {
                $answers[$key] = $answers[$key]["answers"]["aapStep1"]["depense"];
                foreach($allData[$key]["answers"]["aapStep1"]["depense"] as $al => $av){
                    foreach($allData[$key]["answers"]["aapStep1"] as $al2 => $av2) {
                        if (!isset($answers[$key][$al][$al2])) {
                            $answers[$key][$al][$al2] = $av2;
                        }
                    }
                }
            } else {
                $answers[$key] = [];
            }
        }



        $params = [
            "allData" => $allData,
            "allDataAns" => $answers,
            "project" => $project,
            "action" => $action,
            "title" => $title,
            "formparent" => $fp,
            "parent" => $parent,
            "el_parent_form_id" => $el_parent_form_id,
            "el_parent_slug" => $el_parent_slug,
            "hv_parent" => $haveparent
        ];

        if (isset($_POST["urlR"])){
            $params["urlR"] = $_POST["urlR"];
        }

        array_walk_recursive(
            $params, function (&$value)
        {
            $value = htmlspecialchars(html_entity_decode($value, ENT_QUOTES, 'UTF-8'), ENT_QUOTES, 'UTF-8');
        }
        );

    	if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial("survey.views.tpls.answers.ocecoformdashboard",$params,true);              
        else {
    		$this->getController()->layout = "//layouts/empty";
    		return $this->getController()->render($tpl,$params);
        }
    }
}
    	