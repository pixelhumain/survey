<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;
use Answer;
use Authorisation;
use CAction;
use Form;
use Rest;
use type;
use Yii;

/**
 * Display the directory of back office
 * @param String $id Not mandatory : if specify, look for the person with this Id.
 * Else will get the id of the person logged
 * @return type
 */
class DirectoryAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run($source=null, $form=null){
		$controller = $this->getController();
		$res=array();
		$searchParams = $_POST;
		$res = Form::getBySourceAndId($source, $form); 
		$answers=Answer::globalAutocomplete($res["form"], $searchParams);
		$answerList = Form::listForAdmin($answers["results"]) ;
		if(isset($res["form"]["type"]) && $res["form"]["type"] != "aap")
			$answerList = Answer::getDataAnswers($answerList, $res["forms"], $res["form"]);

		if(isset($answers["count"]))
			$res["count"]=$answers["count"];
		$res["results"] = $answerList ;
		//ce code ne marchera pas pour deux formulaire identaique , d'un meme slug 
		//TODO utiliser le parntForm Id en plus 
		$res['canEdit'] =  ( isset(Yii::app()->session["userId"])  && isset($el["type"]) && isset($el["id"]) ) ? 
	    	Authorisation::canEditItem(Yii::app()->session["userId"],$el["type"], $el["id"])
	    	: false ;

		return Rest::json( $res );
	}
}
