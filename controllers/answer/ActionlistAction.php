<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;
use ActStr;
use Answer;
use Cron;
use Element;
use CAction;
use Form;
use Mail;
use MongoId;
use MongoRegex;
use Notification;
use Person;
use PHDB;
use Rest;
use Yii;
use yii\helpers\Url;

class ActionlistAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id=null, $standalone= false , $email = null , $key = null)
    {
        if (isset($_POST["id"]) && isset($_POST["email"])){
            $id = $_POST["id"];
            $email = $_POST["email"];
        }

        if (!empty($id) || !empty($key)) {
            if(empty($key)) {
                $form = PHDB::findOneById(Form::COLLECTION, $id);
                $isAap = !empty($form["type"]) && $form["type"] == "aap";                $configForm = [];

                $slug = "";
                $contextelement = $form["parent"];
                if ($isAap && !empty($form["config"])) {
                    $configForm = PHDB::findOneById(Form::COLLECTION, $form["config"]);
                    $contextelement = $configForm["parent"];
                    $keyParent = array_keys($configForm["parent"])[0];
                    $elConfig = PHDB::findOneById($configForm["parent"][$keyParent]["type"],$keyParent,array("slug"));

                    $slug = $elConfig["slug"];
                }


                $answers = PHDB::find(Form::ANSWER_COLLECTION, array("form" => $id));

                $el = $form["parent"];
                $elKey = array_keys($form["parent"])[0];
                $el = PHDB::findOneById($form["parent"][$elKey]["type"], $elKey,array("slug"));
                $params = [
                    "form" => $form,
                    "isAap" => $isAap,
                    "configForm" => $configForm,
                    "contextelement" => $contextelement,
                    "answers" => $answers,
                    "el" => $el,
                    "slug" => $slug,
                ];
            }

            if (
                isset($form["temporarymembercanreply"]) && ($form["temporarymembercanreply"] == true || $form["temporarymembercanreply"]=="true")
                && isset($form["withconfirmation"]) && $form["withconfirmation"] == false
            ){
                if (!empty($email)) {
                    $params["email"] = $email;
                    return $this->processList($params, false);
                }
            }else{
                if($key){
                    return $this->processList($key);
                }else if($id && $email){
                    return $this->sendConfirmationMail($id, $email);
                }
            }

            $tplc = (!empty($form["actionlisttpl"])) ? $form["actionlisttpl"] : "survey.views.default.actionlist";

            return $this->getController()->renderPartial($tplc, $params);

        }
    }

    function sendConfirmationMail($id, $email){
        $user = PHDB::findOne(Person::COLLECTION, array( "email" => new MongoRegex( '/^'.preg_quote(trim($email)).'$/i' ) ), array("_id", "name", "email"));

        if(empty($user)){
            $key = $this->encrypt_decrypt("encrypt", json_encode(["id"=>$id, "email"=>$email]));

            $urlToSend = Yii::app()->getRequest()->getBaseUrl(true)."/survey/answer/actionlist/key/".$key;

            $form = PHDB::findOneById(Form::COLLECTION, $id);

            foreach($form["parent"] as $k => $v){
                $formParent = $v;
                $formParent["id"] = $k;
            }
            $params = [
                "tpl"=>"answer.emailListConfirmation",
                "tplObject"=>"Acceder à la liste des actions du formulaire ".$form["name"]." de ".$formParent["name"],
                "tplMail"=>$email,
                "url"=>$urlToSend,
                "urlToSend"=>$urlToSend,
                "formName"=>$form["name"],
                "formParent"=>$formParent
            ];

            $mailParams = [
                "type" => Cron::TYPE_MAIL,
                "tpl"=>$params["tpl"],
                "subject" => $params["tplObject"],
                "from"=>Yii::app()->params['adminEmail'],
                "to" => $params["tplMail"],
                "tplParams" => Mail::initTplParams($params)
            ];
            $mailParams=Mail::getCustomMail($mailParams);
            Mail::schedule($mailParams);

            return Rest::json([
                "status"=>true,
                "msg"=> "Nous avons envoyer l'invitation dans votre boite email."
            ]);
        }else{
            return Rest::json([
                "status"=>false,
                "msg"=> "L'adresse email que vous avez fourni existe déjà."
            ]);
        }
    }

    function processList($key, $needkey=true){

        if (!$needkey){
            $params = $key;
        }else{
            $params = $this->encrypt_decrypt("decrypt", $key);
        }

        if($params || !$needkey){
            if ($needkey){
                $params = json_decode($params,true);
            }

            $user = PHDB::findOne(Person::COLLECTION, array( "email" => new MongoRegex( '/^'.preg_quote(trim($params["email"])).'$/i' ) ), array("_id", "name", "email"));

            if (!empty($params['form']["_id"])) {
                $id = (string)$params["form"]["_id"];
            }elseif (!empty($params['id'])){
                $id = $params['id'];
            }

            if(empty($user)){
                //obligé de creer user car on a toujours besoin d'un userID
                $newPerson = array("name" => Person::generedUserNameByEmail($params["email"]),  "email" => $params["email"]);

                $user=Person::insert($newPerson, Person::REGISTER_MODE_MINIMAL, null, null);

                $user = PHDB::findOneById(Person::COLLECTION, $user["id"]);

                if( $user ){
                    Person::saveUserSessionData($user);
                }

                $form = PHDB::findOneById(Form::COLLECTION, $id);
                $isAap = (!isset($form["type"]) || ($form["type"] != "aap" && $form["type"] != "aapConfig"));
                $configForm = [];

                $contextelement = $form["parent"];

                if ($isAap && !empty($form["config"])) {
                    $configForm = PHDB::findOneById(Form::COLLECTION, $form["config"]);
                    $contextelement = $configForm["parent"];
                }
                foreach ($contextelement as $idcontext => $valuecontext) {
                    $contextelement = PHDB::findOneById ($valuecontext["type"] , $idcontext);
                }

                $answers = PHDB::find(Form::ANSWER_COLLECTION, array("form" => $id));

                $baseurl = (!empty($contextelement["costum"])) ? Yii::app()->getBaseUrl(true)."/costum/co/index/slug/".$contextelement["slug"] : Yii::app()->getBaseUrl(true);

                if(!$needkey) {
                    return Rest::json([
                        "msg" => "Vous allez être automatiquement redirigé",
                        "status" => true,
                        "formId" => $id,
                        "location" => $baseurl . "/#answer.actionlist.id." . $id
                    ]);
                } else {

                    // INTREGRER LES COSTUMS QUI N'UTILISE PAS DE TEMPLATE DU TYPE CTE !

                    return Yii::$app->response->redirect(Url::to($baseurl."/#answer.actionlist.id.".$id), '302')->send();
                }

            }else{
                return Rest::json([
                    "msg" => "L'adresse email que vous avez fourni existe déjà",
                    "status" => false
                ]);
            }
        }else{
            return Rest::json([
                "msg" => "Invalid key",
                "status" => false
            ]);
        }
    }

    function encrypt_decrypt($action, $string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = "Z3X6TB0awl";
        $secret_iv = 'MKj9xChXa2';
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if( $action == 'decrypt' ) {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }
}
