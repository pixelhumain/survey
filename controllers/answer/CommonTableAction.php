<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use Authorisation;
use Citoyen;
use MongoId;
use Organization;
use PHDB,Form,Rest,CTKException,Yii;
class CommonTableAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($action)
    {
            $params = $_POST;
            $params2 = $_GET;
            $params = array_merge($params,$params2);
            return self::$action($params);
    }

    private function detail($params){
        $answers = PHDB::find(Form::ANSWER_COLLECTION,[
            "answers.yesOrNo".$params["fieldKey"].".".$params["criteria"].".criteria" => array('$exists'=> true),
            "form" => $params["form"]
        ],["user", "links.organizations","answers.yesOrNo".$params["fieldKey"]]);

        $userId = [];
        foreach ($answers as $key => $value) {
            if(!empty($value["links"]["organizations"]) && isset(array_keys($value["links"]["organizations"])[0])){
                $userId[] = new MongoId(array_keys($value["links"]["organizations"])[0]);
            }
            if(!empty($value["user"])){
                $userId[] = new MongoId($value["user"]);
            }

            if(!empty($value["answers"]["yesOrNo".$params["fieldKey"]])){
                foreach ($value["answers"]["yesOrNo".$params["fieldKey"]] as $kr => $vr) {
                    if(!empty($vr["user"])){
                        $userId[] = new MongoId($vr["user"]);
                    }
                }
            }
        }
        //$userId = array_unique($userId);
        $users = [];
        if(!empty($userId) && is_array($userId)){
            $users = PHDB::find(Citoyen::COLLECTION,array("_id" => ['$in' => $userId]),array("name","profilThumbImageUrl"));
            $users = array_merge($users,PHDB::find(Organization::COLLECTION,array("_id" => ['$in' => $userId]),array("name","profilThumbImageUrl")));
        }
        
        /*foreach ($answers as $key => $value) {
            if(!empty($value["user"])){
                $answers[$key]["user"] = [
                    "id" => $value['user'],
                    "name" => $users[$value['user']]["name"],
                    "profilThumbImageUrl" => !empty($users[$value['user']]["profilThumbImageUrl"]) ? $users[$value['user']]["profilThumbImageUrl"] : ""
                ];
            }
        }*/
        return Rest::json(array("answers" => $answers,"users" => $users));
    }

    private function accriteria($params){
        /*$answers = PHDB::find(Form::ANSWER_COLLECTION,[
            "answers.yesOrNo".$params["fieldKey"] => array('$exists'=> true),
            "form" => $params["form"]
        ],["user","answers.yesOrNo".$params["fieldKey"]]);

        $addedCriteria = [];
        foreach ($answers as $kans => $vans) {
            foreach ($vans["answers"]["yesOrNo".$params["fieldKey"]] as $key => $value) {
                if(!empty($value["criteria"]) && 
                !in_array($value["criteria"],$addedCriteria) && 
                preg_match_all("/".$params['term']."/i", $value["criteria"]) != ""){
                    $addedCriteria[] = ["id" => $value["criteria"], "label" => $value["criteria"], "value" => $value["criteria"]];
            }
            }
        }
        return Rest::json($addedCriteria);*/
        $answers = PHDB::find(Form::ANSWER_COLLECTION,[
            "answers.yesOrNo".$params["fieldKey"].".".$params["criteria"] => array('$exists'=> true),
            "form" => $params["form"]
        ],["user","answers.yesOrNo".$params["fieldKey"].".".$params["criteria"]]);

        $addedCriteria = [];
        foreach ($answers as $kans => $vans) {
            foreach ($vans["answers"]["yesOrNo".$params["fieldKey"]] as $key => $value) {
                if(empty($params["term"]) && !empty($value["criteria"]) && !in_array($value["criteria"],$addedCriteria)) {
                    $addedCriteria[] = $value["criteria"];
                }elseif(!empty($params["term"]) && !empty($value["criteria"]) && 
                    !in_array($value["criteria"],$addedCriteria) && 
                    preg_match_all("/".$params['term']."/i", $value["criteria"]) != ""){
                        $addedCriteria[] = $value["criteria"];
                }
            }
        }
        return Rest::json($addedCriteria);
    }

    private function toolscounter($params){
        $counter = PHDB::count(Form::ANSWER_COLLECTION,[
            "answers.yesOrNo".$params["fieldKey"].".".$params["criteria"].".criteria" => array('$exists'=> true),
            "form" => $params["form"]
        ]);
        return Rest::json(["counter" => $counter]);
    }

    private function alltools($params){
        if(!empty($params["form"]) && !empty($params["fieldKey"])){
            $criterias = $answers = PHDB::find(Form::ANSWER_COLLECTION,[
                "answers.criterias".$params["fieldKey"] => array('$exists'=> true),
                "form" => $params["form"]
            ],["user","answers.criterias".$params["fieldKey"]]);
            $criteriasKeys = [];
            $criteriasValues = [];
            foreach ($criterias as $key => $value) {
                if(!empty($value["answers"]["criterias".$params["fieldKey"]])){
                    //$criteriasValues[] = $value["answers"]["criterias".$params["fieldKey"]];
                    foreach ($value["answers"]["criterias".$params["fieldKey"]] as $k => $v) {
                        $criteriasKeys[] = $k;
                        $criteriasValues[$k] = $v;
                    }
                }
            }

            $answers = PHDB::find(Form::ANSWER_COLLECTION,[
                "answers.yesOrNo".$params["fieldKey"] => array('$exists'=> true),
                "form" => $params["form"]
            ],["user","answers.yesOrNo".$params["fieldKey"]]);
            $userId = [];
            foreach ($answers as $key => $value) {
                if(!empty($value["user"])){
                    $userId[] = new MongoId($value["user"]);
                }
            }

            $users = [];
            if(!empty($userId))
                $users = PHDB::find(Citoyen::COLLECTION,array("_id" => ['$in' => $userId]),array("name","profilThumbImageUrl"));
            foreach ($answers as $key => $value) {
                if(!empty($value["answers"]["yesOrNo".$params["fieldKey"]])){
                    foreach ($value["answers"]["yesOrNo".$params["fieldKey"]] as $k => $v) {
                        if(!in_array($k,$criteriasKeys)){
                            unset($answers[$key]["answers"]["yesOrNo".$params["fieldKey"]][$k]);
                        }
                    }

                }
                if(empty($answers[$key]["answers"]["yesOrNo".$params["fieldKey"]])){
                    unset($answers[$key]);
                }else{
                    if(!empty($value["user"])){
                        $answers[$key]["user"] = [
                            "id" => $value['user'],
                            "name" => $users[$value['user']]["name"],
                            "profilThumbImageUrl" => !empty($users[$value['user']]["profilThumbImageUrl"]) ? $users[$value['user']]["profilThumbImageUrl"] : ""
                        ];
                    }
                }
            }
            return Rest::json(array("answers" => $answers,"criterias" => $criteriasValues));
        }
    }
}