<?php
namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use Answer;
use Form;
use PHDB;
use Yii;
use Rest;
use Exception;
use MongoDate;
use DateTime;
class ImportanswerAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $allAns = [];
        foreach ($_POST["data"] as $pdataid => $pdata){

            $form = PHDB::findOneById( Form::COLLECTION, (string)$_POST["form"] ) ;
            $ans = [
                "collection" => Form::ANSWER_COLLECTION,
                "user" => Yii::app()->session["userId"],
                "links"=>[
                    "answered" => [ Yii::app()->session["userId"] ]
                ],
                "created" => time(),
                "form" => (string)$_POST["form"],
                "context" => $form["parent"],
                "imported" => true
            ];

            if(isset($form["type"]) && $form["type"] =="aap")
                $ans["acceptation"] = "pending";

            foreach ($_POST["params"] as $dpid => $dp){

                if ($dp["type"] != "" && $dp["path"] != "" && isset($pdata[$dpid]) && !empty($pdata[$dpid]) && $pdata[$dpid] != "") {
                    if ($dp["type"] == "financement" && isset($dp["tittle"]) && $dp["tittle"] != "" ){
                        $title_key = array_search('answers.aapStep1.titre', array_column($_POST["params"], 'path'));
                        $pdata[$dpid] = $this->string_set_type($_POST["data"][$pdataid][$dpid], $dp["type"], $dp["tittle"] , $_POST["data"][$pdataid][$title_key] );
                    }else {
                        $pdata[$dpid] = $this->string_set_type($_POST["data"][$pdataid][$dpid], $dp["type"]);
                    }
                }

                if (isset($pdata[$dpid]) && $pdata[$dpid] != "" && $dp["path"] != ""){
                    $this->array_set_value($ans, $dp["path"], $pdata[$dpid], ".");
                }

            }

            try {
                $saved = Yii::app()->mongodb->selectCollection(Answer::COLLECTION)->insert($ans);
            } catch (Exception $e){

            }

            array_push($allAns, $ans);
        }

        return Rest::json(array("result"=>true,"msg"=>"enregistré", "data"=>$allAns));
    }

    function string_set_type($tosetvalue, $type, $title = null, $poste = null){
        if(!empty($type)){
            if ($type == "int"){
                return intval($tosetvalue);
            }
            if ($type == "float"){
                return floatval($tosetvalue);
            }
            elseif($type == "boolean"){
                return filter_var($tosetvalue, FILTER_VALIDATE_BOOLEAN);
            }
            elseif ($type == "isoDate"){
                // $date = new DateTime($tosetvalue);
                return new MongoDate(strtotime($tosetvalue));
            }
            elseif ($type == "timestamp"){
                $date = new DateTime($tosetvalue);
                return $date->getTimestamp();
            }
            elseif ($type == "checkbox"){

                $returnv = [];

                if ($tosetvalue != ""){
                    $returnv = explode(",", $tosetvalue);
                }

                return $returnv;
            }
            elseif ($type == "array"){

                $pieces = explode(",", $tosetvalue);

                return $pieces;
            }
            elseif ($type == "radiobutton"){
                return $tosetvalue;
            }
            elseif ($type == "financement" && $title != null && $poste){
                return array(
                    "poste" => $poste,
                    "price" => intval($tosetvalue),
                    "financer" => array(
                        [
                            "line" => $title,
                            "amount" => intval($tosetvalue),
                            "user" => Yii::app()->session["userId"],
                            "date" => new MongoDate(time()),
                            "name" => $title
                        ]
                    )
                );
            }
        }
        return null;
    }

    function array_get_value(array &$array, $path, $glue = '.')
    {
        if (!is_array($path)) {
            $path = explode($glue, $path);
        }

        $ref = &$array;

        foreach ((array) $path as $parent) {
            if (is_array($ref) && array_key_exists($parent, $ref)) {
                $ref = &$ref[$parent];
            } else {
                return null;
            }
        }
        return $ref;
    }

    function array_set_value(array &$array, $path, $value, $glue = '.')
    {
        if (!is_array($path)) {
            $path = explode($glue, (string) $path);
        }

        $keyy = array_search('', $path);

        if ($keyy === false) {

            $ref = &$array;

            foreach ($path as $parent) {
                if (isset($ref) && !is_array($ref)) {
                    $ref = array();
                }

                $ref = &$ref[$parent];
            }

            $ref = $value;
        }
    }

    function array_unset_value(&$array, $path, $glue = '.')
    {
        if (!is_array($path)) {
            $path = explode($glue, $path);
        }

        $key = array_shift($path);

        if (empty($path)) {
            unset($array[$key]);
        } else {
            array_unset_value($array[$key], $path);
        }
    }
}
