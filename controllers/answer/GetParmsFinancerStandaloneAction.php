<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use ActStr;
use Answer;
use Authorisation;
use DateTimeImmutable;
use Element;
use CAction;
use Form;
use MongoId;
use Notification;
use Person;
use PHDB;
use HtmlHelper;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\actor\Organization;
use Yii;
use RocketChat;
use Rest;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;

class GetParmsFinancerStandaloneAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($answerid = null, $formid = null , $step="aapStep1" , $input = "depense" )
    {
        $this->getController()->layout = "//layouts/empty";
        $return = [
            "titre" => "",
            "description" => "",
            "userpercent" => 0,
            "percent" => 0,
            "finalpercent" => 0,
            "useramount" => 0,
            "amountearn" => 0,
            "finalamountearn" => 0,
            "total" => 0,
            "reste" => 0,
            "finalreste" => 0,
            "financer" => [],
            "porter" => [],
            "financement" => [],
            "lines" => [],
            "arraylines" => [],
            "params" => []
        ];

        //Get Data
        if (!empty($answerid)) {
            $params = Form::getParamsForm($answerid, $formid, "aapStep3", "financer" , "w", null, true);
            $return["params"]  =$params;
            $answer = $params["answer"];
            $return["titre"] = isset($answer["answers"]["aapStep1"]["titre"]) ? $answer["answers"]["aapStep1"]["titre"] : "";
            $return["description"] = isset($answer["answers"]["aapStep1"]["description"]) ? $answer["answers"]["aapStep1"]["description"] : "";
            if(!empty($answer["answers"]["aapStep1"]["depense"])) {
                $return["arraylines"] = $answer["answers"]["aapStep1"]["depense"];
            }
            foreach ($return["arraylines"] as $in => $item){
                if(isset($item["financer"])) {
                    $return["financement"][$in] =  $item["financer"];
                } else{
                    $return["financement"][$in] =  [];
                }
            }

            //$return["financement"] = isset($answer["answers"]["aapStep1"]["depense"]) ? array_column($answer["answers"]["aapStep1"]["depense"], 'financer') : 0;
            $return["lines"] = isset($answer["answers"]["aapStep1"]["depense"]) ? array_merge_recursive(...$return["financement"]) : 0;
            $return["amountearn"] = isset($answer["answers"]["aapStep1"]["depense"]) ? array_sum(array_column($return["lines"], "amount")) : 0;
            $return["total"] = isset($answer["answers"]["aapStep1"]["depense"]) ? array_sum(array_column($answer["answers"]["aapStep1"]["depense"], 'price')) : 0;

            foreach ($return["financement"] as $in => $item){
                $return["arraylines"][$in]["amountearn"] =  array_sum(array_column($item , "amount"));
            }

            foreach ($return["lines"] as $in => $item){
                $finTempId = "";
                if(!empty($item["name"])){
                    $finTempId .= $this->clean($item["name"]);
                }
                if(!empty($item["email"])){
                    $finTempId .= $this->clean($item["email"]);
                }
                if(!isset($return["financer"][$finTempId])){
                    if(isset($return["financer"][$finTempId]["id"])){
                        $return["financer"][$finTempId] = PHDB::findOneById( Person::COLLECTION ,$return["financer"][$finTempId]["id"] );
                        if(empty($return["financer"][$finTempId])){
                            $return["financer"][$finTempId] = PHDB::findOneById( Organization::COLLECTION ,$return["financer"][$finTempId]["id"] );
                        }
                    }

                    $return["financer"][$finTempId]["name"] = @$item["name"];
                    $return["financer"][$finTempId]["email"] = @$item["email"];

                    $return["financer"][$finTempId]["amount"] = @$item["amount"];
                } else {
                    $return["financer"][$finTempId]["amount"] += @$item["amount"];
                }
            }

            $return["reste"] = $return["total"] - $return["amountearn"];
            if($return["total"] != 0){
                $return["percent"] = round(($return["amountearn"] / $return["total"])*100 , 2) * 1;
            }

            if(isset($answer["links"]["contributors"])){
                foreach ($answer["links"]["contributors"] as $in => $item){
                    $return["porter"][$in] = PHDB::findOneById( Person::COLLECTION , $in );
                }
            }

        }

        return Rest::json($return);
    }

    public function clean($string) {
        $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

}
