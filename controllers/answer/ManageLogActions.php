<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;
use Rest;
use PHDB;
use Log;
use MongoId;
use MongoDate;
use DateTimeZone;
use DateTime;
use Form;

class ManageLogActions extends \PixelHumain\PixelHumain\components\Action {
  public function run($action) {
    $data = $_POST;
    return self::$action($data);
  }
  private function save ($data) { 
    $session = $_SESSION;
    $initEntryLog = Log::setLogBeforeAction("answer/update");
    $userId = $session["userId"];
    $userName = (isset($session["user"]["name"])) ? $session["user"]["name"] : "Unknown";
    $formId = $data["elt"]["form"] ?? [];
    $answerPath = $data["path"] ?? "";
    $value = $data["value"] ?? "";
    $time = $data["time"];
    $inputAndStepName = self::getInputAndStepName($answerPath, $formId);
    $recordLog = PHDB::findOne(Log::COLLECTION, ["logContent" => "allRecordsLog","formId" => $formId, "sessionDate" => date("d/m/y", time())]);
    $headerLog = PHDB::findOne(Log::COLLECTION, ["logContent" => "headerLog", "formId" => $formId, "sessionDate" => date("d/m/y", time())]);
    $headerLog = $headerLog ?? []; 
    $logHeade = [];
    $logHeade["formId"] = $formId;
    $logHeade["created"] = new MongoDate();
    $logHeade["action"] = $initEntryLog["action"];
    $logHeade["sessionDate"] = date("d/m/y", time());
    $logHeade["countUpdate"] = 0;
    $logHeade["monthRef"] = strval(date('m'));
    $logHeade["logContent"] = "headerLog";
    $recordLog["records"][$time]["answerPath"] = $answerPath;
    $recordLog["records"][$time]["stepName"] = $inputAndStepName["stepName"];
    $recordLog["records"][$time]["inputName"] = $inputAndStepName["inputName"];
    $recordLog["records"][$time]["userId"] = $userId;
    $recordLog["records"][$time]["userName"] = $userName;
    $recordLog["records"][$time]["ipAddress"] = $initEntryLog["ipAddress"];
    $recordLog["records"][$time]["browser"] = $initEntryLog["browser"];
    if (count($headerLog) == 0) {
        Log::save($logHeade);
    }
    if (!isset($recordLog["logContent"])) {
        $logHeade["logContent"] = "allRecordsLog";
        unset($logHeade["countUpdate"]);
        unset($logHeade["answerId"]);
        unset($logHeade["monthRef"]);
        $logHeade["records"] = $recordLog["records"];
        Log::save($logHeade);
    } else {

        Log::save($recordLog);
    }
    $logCounter["countUpdate"] = count($recordLog["records"]);
    PHDB::update(Log::COLLECTION, ["logContent" => "headerLog", "formId" => $formId, "sessionDate" => date("d/m/y", time())],array('$set' => $logCounter));
  }
  private function get ($data) { 
    $res = ["result" => false];
    if (isset($data["filter"])) {
        $logData = Log::getWhere($data["filter"]);
        if (isset($logData)) {
            $res["result"] = true;
            $res["data"] = $logData;
        }
    }
    return Rest::json($res);
  }
  private function clean ($data) { 
    $dayNumberLimitation = (isset($data["dayLimit"])) ? $data["dayLimit"] : 30;
    PHDB::remove(self::COLLECTION, array( 
        "action"=> "answer/update"
        , "created" => array('$lt' => new MongoDate(strtotime($dateLimit)))
    ));
  }
  private function getInputAndStepName ($path, $formId) {
    $result = [];
    $inputName = "Question non reconnue";
    $stepName = "Etape non reconnue";
    $form = PHDB::findOne(Form::COLLECTION, ["_id" => new MongoId($formId)], ["subForms" => 1]);
    if (!empty($form["subForms"])) {
      $stepValue = array_filter($form["subForms"], function ($subForm) use ($path) {return strpos($path, $subForm);});
      $stepValue = array_values($stepValue)[0];
      $answerInput = array_filter(explode(".", $path), function ($item) use ($stepValue) {return (count(explode($stepValue, $item)) > 1) && (strlen($item) != strlen($stepValue));}); 
      $pathArray = explode(".", $path);
      $answerInput = count($answerInput) > 1 ? array_values($answerInput)[0] : (count($pathArray) > 1 ? $pathArray[2] : "notSelected");
      $decomposeAnsInput = explode($stepValue, $answerInput);
      $formInput = "".$stepValue."".(count($decomposeAnsInput) > 1 ? $decomposeAnsInput[1] : $decomposeAnsInput[0]);
      $formInputPath = "inputs.".$formInput.".label";
      $form = PHDB::findOne(Form::COLLECTION, ["id" => $stepValue], [$formInputPath => 1, "name" => 1]);
      $inputName = (isset($form["inputs"][$formInput]["label"])) ? $form["inputs"][$formInput]["label"] : $inputName;
      $stepName = (isset($form["name"])) ? $form["name"] : $stepName;
    }
    $result["inputName"] = $inputName;
    $result["stepName"] = $stepName;
    return $result;
  }
}