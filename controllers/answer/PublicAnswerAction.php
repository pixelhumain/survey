<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use Answer;
use Citoyen;
use Form;
use InflectorHelper;
use MongoId;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
use Yii;

class PublicAnswerAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($request) {
        $controller = $this->getController();
        $params = $_POST;
        $params["controller"] = $controller;
        return self::$request($params);
    }

    private function get_chart_view($params) {
        $controller = $params["controller"];
        $params["tpl"] = "survey.views.tpls.answers.evaluationGraph";
        $resData = [
            "containerId" => $params["containerId"],
            "chartWidth" => $params["chartWidth"] ?? "40vw",
            "chartHeight" => $params["chartHeight"] ?? "40vh",
            "chartType" => $params["chartType"] ?? "radar",
            "chartData" => [
                "labels" => [],
                "datasets" => [
                    [
                        "label" => "",
                        "backgroundColor" => "rgba(54, 162, 235, 0.0)",
                        "borderColor" => "rgb(54, 162, 235)",
                        "borderWidth" => 1,
                        "data" => [],
                    ],
                ],
            ],
            "users" => [],
            "chartLegend" => []
        ];
        if (isset($params["dataChartBuilder"]["inputType"])) {
            switch ($params["dataChartBuilder"]["inputType"]) {
                case 'tpls.forms.cplx.checkboxNew':
                    $answerData = self::getChartDataByPath($params["dataChartBuilder"]["answerId"], Answer::COLLECTION, $params["dataChartBuilder"]["answerPath"]);
                    $answers = UtilsHelper::array_val_at_path($answerData["data"], $params["dataChartBuilder"]["answerPath"], ".");
                    $inputData = self::getInputParams($params["dataChartBuilder"]["inputParamsId"], $params["dataChartBuilder"]["inputParamsCollection"], $params["dataChartBuilder"]["inputParamsPath"]);
                    $inputParams = UtilsHelper::array_val_at_path($inputData["data"], $params["dataChartBuilder"]["inputParamsPath"], ".");
                    $datasetCount = [];
                    foreach ($inputParams as $index => $inputParamsVal) {
                        $resData["chartData"]["labels"][] = chr(65 + $index);
                        $resData["chartLegend"][chr(65 + $index)] = $inputParamsVal;
                        $currentValUnik = $index . "_" . InflectorHelper::slugify($inputParamsVal);
                        foreach ($answers as $userId => $userAnswer) {
                            if (!isset($resData["users"][$userId])) {
                                $resData["users"][$userId] = PHDB::findOneById(Citoyen::COLLECTION, $userId, ["name", "username", "profilThumbImageUrl"]);
                            }
                            if (in_array($currentValUnik, $userAnswer)) {
                                if (!isset($datasetCount[$index])) {
                                    $datasetCount[$index] = 0;
                                }
                                $datasetCount[$index]++;
                            } else {
                                if (!isset($datasetCount[$index])) {
                                    $datasetCount[$index] = 0;
                                }
                            }
                        }
                        $resData["chartData"]["datasets"][0]["data"][] = $datasetCount[$index] ?? 0;
                    }
                    $resData["maxDatasetValue"] = max($datasetCount);
                    $resData["minDatasetValue"] = min($datasetCount);
                    break;
                
                default:
                    break;
            }
        }
        return $controller->renderPartial($params["tpl"], $resData);
    }

    private function get_multieval_view($params) {
        $controller = $params["controller"];
        $params["tpl"] = "survey.views.tpls.answers.evaluationGraph";
        $resData = [
            "containerId" => $params["containerId"],
            "chartWidth" => $params["chartWidth"] ?? "50vw",
            "chartHeight" => $params["chartHeight"] ?? "50vh",
            "chartType" => $params["chartType"] ?? "radar",
            "chartData" => [
                "labels" => [],
                "datasets" => [
                ],
            ],
            "minDatasetValue" => 0,
            "maxDatasetValue" => 1,
            "users" => [],
            "axePointsLabel" => [],
            "chartLegend" => []
        ];
        if (isset($params["dataChartBuilder"]["stepId"])) {
            $stepInputs = PHDB::findOneById($params["dataChartBuilder"]["stepType"], $params["dataChartBuilder"]["stepId"], ["inputs"]);
            if (isset($stepInputs["inputs"])) {
                $formData = [
                    "answerFields" => [],
                    "inputData" => [],
                    "paramsData" => [
                        "inputParamsFields" => []
                    ],
                    "datasets" => [],
                ];
                foreach ($stepInputs["inputs"] as $inputKey => $inputValue) {
                    if (isset($inputValue["activeMultieval"]) && $inputValue["activeMultieval"]) {
                        $splittedInputType = explode(".", $inputValue["type"]);
                        $inputKunik = $splittedInputType[count($splittedInputType) - 1].$inputKey;
                        $currentInputAnswerPath = "answers.".$params["dataChartBuilder"]["stepKey"]. ".". $inputKey. "_multiEval";
                        $currentParamsPath = "params.".$inputKunik;
                        $formData["answerFields"][] = $currentInputAnswerPath;
                        $formData["inputData"][] = [
                            "label" => $inputValue["label"],
                            "evaluationKey" => isset($inputValue["evaluationKey"])  ? $inputValue["evaluationKey"] : chr(65 + count($formData["inputData"])),
                            "kunik" => $inputKunik
                        ];
                        $formData["datasets"][] = 0;
                        $formData["paramsData"]["inputParamsFields"][] = $currentParamsPath;
                        // if (in_array($inputValue["type"], ["tpls.forms.cplx.checkboxNew", "tpls.forms.cplx.radioNew"])) {
                        //     $answerData = self::getChartDataByPath($params["dataChartBuilder"]["answerId"], Answer::COLLECTION, $currentInputAnswerPath);
                        //     $answers = UtilsHelper::array_val_at_path($answerData["data"], $currentInputAnswerPath, ".");
                        //     $inputData = self::getInputParams($params["dataChartBuilder"]["inputParamsId"], Form::COLLECTION, $params["dataChartBuilder"]["inputParamsPath"]. ".". $inputKunik);
                        //     $inputParams = UtilsHelper::array_val_at_path($inputData["data"], $currentParamsPath, ".");
                        //     $datasetCount = [];
                        // }
                    }
                }
                if (!empty($formData["answerFields"])) {
                    $answerData = self::getChartDataByPath($params["dataChartBuilder"]["answerId"], Answer::COLLECTION, $formData["answerFields"]);
                    $inputData = self::getInputParams($params["dataChartBuilder"]["inputParamsId"], Form::COLLECTION, $formData["paramsData"]["inputParamsFields"]);
                    $inputParams = [];
                    $dataSets = [];
                    $usersData = [
                        "usersIds" => [],
                        "usersDateAnswer" => [],
                        "autoEval" => [],
                    ];
                    foreach ($formData["paramsData"]["inputParamsFields"] as $index => $path) {
                        $currentParams = UtilsHelper::array_val_at_path($inputData["data"], $path, ".");
                        $inputParams[isset($formData["inputData"][$index]["kunik"]) ? $formData["inputData"][$index]["kunik"] : "" ] = $currentParams;
                        if (isset($formData["inputData"][$index]["evaluationKey"])) { 
                            $resData["chartData"]["labels"][] = $formData["inputData"][$index]["evaluationKey"];
                            $resData["chartLegend"][$formData["inputData"][$index]["evaluationKey"]] = $formData["inputData"][$index]["label"];
                            if (isset($currentParams["list"])) {
                                $resData["axePointsLabel"][$formData["inputData"][$index]["evaluationKey"]] = $currentParams["list"];
                            }
                        }
                        $answers = UtilsHelper::array_val_at_path($answerData["data"], $formData["answerFields"][$index], ".");
                        if (!empty($answers) && isset($currentParams["list"])) {
                            foreach ($answers as $userId => $answerValue) {
                                $usersData["usersIds"] = array_unique(array_merge($usersData["usersIds"], [$userId]));
                                if (isset($answerData["data"]["user"]) && $answerData["data"]["user"] == $userId) {
                                    $usersData["autoEval"][$userId] = true;
                                }
                                if (!isset($usersData["usersDateAnswer"][$userId])) {
                                    $usersData["usersDateAnswer"][$userId] = isset($answerValue["date"]) ? $answerValue["date"] : null;
                                }
                                $findedIndex = self::findSlugifyInex(isset($answerValue["answer"]) ? $answerValue["answer"] : $answerValue, $currentParams["list"]);
                                if (!isset($dataSets[$userId]["data"])) {
                                    $dataSets[$userId]["data"] = $formData["datasets"];
                                }
                                $dataSets[$userId]["data"][$index] = $findedIndex + 1;
                            }
                        }
                    }
                    if (!empty($usersData["usersIds"])) {
                        $resData["users"] = PHDB::findByIds(Citoyen::COLLECTION, $usersData["usersIds"], ["name", "username", "profilThumbImageUrl"]);
                        $timezone = !empty(Yii::app()->session["timezone"]) ? Yii::app()->session["timezone"] : "UTC";
                        date_default_timezone_set($timezone);
                        foreach ($resData["users"] as $userId => $userData) {
                            if (isset($usersData["autoEval"][$userId])) {
                                $resData["users"][$userId]["isAutoEval"] = true;
                            }
                            $resData["users"][$userId]["dateAnswer"] = !empty($usersData["usersDateAnswer"][$userId]) ?  $usersData["usersDateAnswer"][$userId]->toDateTime()->format('c') : null;
                        }
                    }
                    foreach ($dataSets as $userId => $dataValue) {
                        $resData["chartData"]["datasets"][] = [
                            "label" => isset($resData["users"][$userId]) ? $resData["users"][$userId]["name"] : "",
                            "backgroundColor" => "rgba(54, 162, 235, 0.0)",
                            "borderColor" => sprintf('rgb(%d, %d, %d)', rand(0, 200), rand(0, 200), rand(0, 200)),
                            "borderWidth" => 1,
                            "data" => $dataValue["data"],
                        ];
                    }
                    $resData["maxDatasetValue"] = self::getMaxDatasetValue($resData["chartData"]["datasets"]);
                }
            }
        }
        return $controller->renderPartial($params["tpl"], $resData);
    }

    private function findSlugifyInex ($need, $arr) {
        $index = 0;
        foreach ($arr as $key => $value) {
            if ($index . "_" .InflectorHelper::slugify($value) == $need) {
                return $index;
            }
            $index++;
        }
        return -1;
    }

    private function getMaxDatasetValue($datasets) {
        $maxValue = 0;
        foreach ($datasets as $dataset) {
            $maxValue = max($maxValue, max($dataset["data"]));
        }
        return $maxValue;
    }

    private function getChartDataByPath($id, $collection, $path) {
        $res = [];
        $field = ["user"];
        is_array($path) ? $field = array_merge($field, $path) : $field[] = [$path];
        $res["data"] = PHDB::findOneById($collection, $id, $field);
        return $res;
    }

    private function getInputParams($id, $collection, $path) {
        $res = [];
        $res["data"] = PHDB::findOneById($collection, $id, is_array($path) == 'array' ? $path : [$path]);
        return $res;
    }
}