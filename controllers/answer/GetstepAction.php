<?php
namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use Answer;
use Form;
use MongoId;
use PHDB;
use Yii;

class GetstepAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id=null, $answer=null, $mode=null, $form=null, $formstep=null, $inputid=null, $viewmode=null)
    {
        if(!empty($form))
            $form = PHDB::findOneById( Form::COLLECTION , $form);

        $connexionmodeParams = [
            "id" => $id,
            "answer" => $answer,
            "mode" => $mode,
            "form" => $form
        ];
        // var_dump($form);exit;
        // var_dump(Yii::app()->session["userId"]);exit;
        //var_dump(Yii::app()->request);exit;

        if ($id == "new"){
            $context = $form["parent"];
            $ans = Answer::generateAnswer($form, false, $context);
            $id = (string)$ans["_id"];
        }
        $this->getController()->layout = "//layouts/empty";
        $params = array();

        if (!empty(Yii::app()->session["userId"])){
            if(!empty($id)){

                $params["answerId"]=$id;
                $answer = PHDB::findOne( Form::ANSWER_COLLECTION, array("_id"=>new MongoId($id)));
                //Rest::json($answer); exit;
                if(empty($form) && !empty($answer["form"]))
                    $form = PHDB::findOneById( Form::COLLECTION , $answer["form"]);

                $params["form"] = $form ;
                $params = Form::getDataForm($params);

                $params["answer"] = $answer;

                $params["inputid"] = $inputid;
                $params["formStep"] = $formstep;
                $params["anwsform"] = $form;

                $params["rendermodal"] = true;

                $canEditForm = false;
                $canAdminAnswer = false;
                $canEditAnswer = false;
                $canSeeAnswer = false;

                if( empty($mode) || ( $mode != "w" && $mode != "r" ) ||
                    ( !empty($answer["validated"]) && $answer["validated"] == true) )
                    $mode = "r";

                if(!empty(Yii::app()->session['userId'])){
                    $canEditAnswer = Answer::canEdit($params["answer"], $form, Yii::app()->session['userId'], @$parentForm);
                    $canAdminAnswer = Answer::canAdmin($params["answer"], $form);

                    if($canEditAnswer === false && $canAdminAnswer === false)
                        $canSeeAnswer = Answer::canAccess($params["answer"], $form, Yii::app()->session['userId'], @$parentForm);
                    else
                        $canSeeAnswer = true;

                    if (isset($params["form"]["anyOnewithLinkCanAnswer"]) && $params["form"]["anyOnewithLinkCanAnswer"] == 'true') {
                        $canEditAnswer = true;
                        $canSeeAnswer = true;
                    }
                }

                if(!empty($parentForm["type"]) && ($parentForm["type"] == "aap" || $parentForm["type"] == "aapConfig") ){

                    if( $parentForm["type"] == "aap"){
                        $configEl = PHDB::findOneById(Form::COLLECTION, $parentForm["config"]);
                    }elseif ( $parentForm["type"] == "aapConfig"){
                        $configEl = $parentForm;
                    }

                    foreach ($configEl["subForms"] as $idConfigEl => $valueConfigEl){
                        $forms[$idConfigEl] = $valueConfigEl;

                    }

                    $form["subForms"] = array_keys($configEl["subForms"]);
                }


                if (!empty($parentForm["type"]) && $parentForm["type"] == "aapConfig" ){
                    $formsubForms = [];
                    foreach($configEl["subForms"] as $ksubF => $vsubF) {
                        if(isset($vsubF["active"]) && $vsubF["active"]==true) // check if aapCOnfig subForms (step) is active and push to subforms array
                            $formsubForms[] = $ksubF;
                    }
                }else {
                    if (!empty($form["subForms"])) {
                        $formsubForms = $form["subForms"];
                    }
                }
                if (!empty($formsubForms[$formstep])){
                    $params["form"]["inputs"] = $formsubForms[$formstep];
                }

                if( $mode == "w" && $canEditAnswer === false && $canAdminAnswer === false )
                    $mode = "rw";

                $params["parentForm"] = $form;
                $params["standAlone"] = true;

                if($canSeeAnswer === true ){
                    $params["canEditForm"] = $canEditForm;
                    $params["canAdminAnswer"] = $canAdminAnswer;
                    $params["canEdit"] = $canEditAnswer;
                    $params["canSee"] = $canSeeAnswer;
                    $params["mode"] = (!empty($mode) ? $mode : "r");
                    // Rest::json($form); exit();
                    $tpl= "survey.views.tpls.forms.formstep";
                    //var_dump($params["form"]);
                    return $this->getController()->renderPartial($tpl,$params );
                } else {
                    if(Yii::app()->request->isAjaxRequest)
                        return $this->getController()->renderPartial("co2.views.default.unTpl",array("msg"=>Yii::t("common", "You are not allowed to access to this answer"),"icon"=>"fa-lock"));
                    else
                        return $this->getController()->render("co2.views.default.unTpl",array("msg"=>Yii::t("common", "You are not allowed to access to this answer"),"icon"=>"fa-lock"));
                }
            } else {
                if(Yii::app()->request->isAjaxRequest)
                    return $this->getController()->renderPartial("co2.views.default.unTpl",array("msg"=>Yii::t("common", "You are not allowed to access to this answer"),"icon"=>"fa-lock"));
                else
                    return $this->getController()->render("co2.views.default.unTpl",array("msg"=>Yii::t("common", "You are not allowed to access to this answer"),"icon"=>"fa-lock"));
            }
        }else {
            if(!empty($form["temporarymembercanreply"]) && ($form["temporarymembercanreply"] == "true" || $form["temporarymembercanreply"] == true)){
                    return $this->getController()->renderPartial("survey.views.default.connexionmode", $connexionmodeParams);
                }
            // echo $this->getController()->renderPartial("costum.views.custom.appelAProjet.addproposition",["formData" => $params] );

            // if(Yii::app()->request->isAjaxRequest)
            //     return $this->getController()->renderPartial("co2.views.default.unTpl",array("msg"=>Yii::t("common", "You are not allow to access to this answer"),"icon"=>"fa-lock"));
            else
                return $this->getController()->render("co2.views.default.unTpl",array("msg"=>Yii::t("common", "You are not allow to access to this answer"),"icon"=>"fa-lock"));

        }

    }
}
