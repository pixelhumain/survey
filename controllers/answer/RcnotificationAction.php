<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use Person;
use Form;
use PHDB;
// use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\actor\Person;
use Yii;
use Rest;
use Project;
use Action;
use RocketChat;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;

class RcnotificationAction extends \PixelHumain\PixelHumain\components\Action {
    public function run($action, $answerid = null) {
        $tab = Aap::badgelabel();

        $parent = [];
        $parentslug = [];
        $groupChat = [];
        $channelChat = [];
        $parenthasRC = false;

        if (isset($_POST["url"])) {
            $url = $_POST["url"];
        } else {
            $url = "";
        }

        if (!empty($action && !empty($answerid))) {
            $answer = PHDB::findOneById(Form::ANSWER_COLLECTION, $answerid);
            if (empty($answer)) {
                return Rest::json(array("result" => false, "msg" => "invalid answer"));
            }
            $formparent = PHDB::findOneById(Form::COLLECTION, $answer["form"]);
            if (isset($formparent["paramsNotification"][$action]) && ($formparent["paramsNotification"][$action] == false || $formparent["paramsNotification"][$action] == "false")) {
                return Rest::json(array("result" => false, "msg" => "notification disabled"));
            }

            foreach ($formparent["parent"] as $fpid => $fp) {

                $pr = PHDB::findOneById($fp["type"], $fpid);

                if (isset($pr["hasRC"]) && filter_var($pr["hasRC"], FILTER_VALIDATE_BOOLEAN)) {
                    if (!empty($pr["tools"]["chat"]["int"])) {
                        foreach ($pr["tools"]["chat"]["int"] as $t => $ch) {
                            if (str_contains($ch["url"], "channel")) {
                                array_push($channelChat, $ch["name"]);
                            } elseif (str_contains($ch["url"], "group")) {
                                array_push($groupChat, $ch["name"]);
                            }
                        }
                    } else {
                        array_push($channelChat, $pr["slug"]);
                    }
                }
                array_push($parent, $pr);
            }

            $project = null;
            $projectslug = "";
            if (!empty($answer["project"]["id"])) {
                $project = PHDB::findOneById(Project::COLLECTION, $answer["project"]["id"]);
                $projectslug = $project["slug"];
            }

            switch ($action) {
                case "newproposition":
                    $message = " @" . Yii::app()->session['user']["username"] . " a ajoutée une nouvelle proposition nommée : `" . @$_POST["proposition"] . "`";
                    self::send($channelChat, $groupChat, $message, $url, $projectslug);
                    break;
                case "deleteproposition":
                    $message = " @" . Yii::app()->session['user']["username"] . " a supprimé la proposition nommée : `" . @$_POST["proposition"] . "`";
                    self::send($channelChat, $groupChat, $message, $url, $projectslug);
                    break;
                case "newstatus":
                    $status = "";
                    if (isset($_POST["status"])) {
                        foreach ($_POST["status"] as $asid => $as) {
                            $status .= " " . isset($tab[$as]) ? $tab[$as] : $as . "|";
                        }
                    }
                    if ($status == "") {
                        $status = "`(sans status)`";
                    }
                    $message = " @" . Yii::app()->session['user']["username"] . " a mis à jour le status de : " . @$answer["answers"]["aapStep1"]["titre"] . " en :" . $status;

                    foreach ($channelChat as $psid => $ps) {
                        RocketChat::post($ps, $message, $url, true);
                    }
                    foreach ($groupChat as $psid => $ps) {
                        RocketChat::post($ps, $message, $url, false);
                    }
                    if ($projectslug != "") {
                        RocketChat::post($projectslug, $message, $url);
                    }
                    break;
                case "newdepense":

                    if (!empty($_POST["name"])) {
                        $message = " @" . Yii::app()->session['user']["username"] . " a ajoutée une nouvelle ligne de depense pour la proposition : `" . @$answer["answers"]["aapStep1"]["titre"] . " ` intitulée : `" . $_POST["name"] . "`";
                    }elseif (isset($_POST["pos"])) {
                        $message = " @" . Yii::app()->session['user']["username"] . " a ajoutée une nouvelle ligne de depense pour la proposition : `" . @$answer["answers"]["aapStep1"]["titre"] . " ` intitulée : `" . @$answer["answers"]["aapStep1"]["depense"][$_POST["pos"]]["poste"] . "`";
                    }else {
                        $message = " @" . Yii::app()->session['user']["username"] . " a ajoutée une nouvelle ligne de depense pour la proposition : `" . @$answer["answers"]["aapStep1"]["titre"] . "`";
                    }
                    foreach ($channelChat as $psid => $ps) {
                        RocketChat::post($ps, $message, $url, true);
                    }
                    foreach ($groupChat as $psid => $ps) {
                        RocketChat::post($ps, $message, $url, false);
                    }
                    if ($projectslug != "") {
                        RocketChat::post($projectslug, $message, $url);
                    }
                    break;
                case "deletedepense":

                    if (isset($_POST["pos"])) {
                        $message = " @" . Yii::app()->session['user']["username"] . " a supprimé la ligne de depense intitulée : `" . @$answer["answers"]["aapStep1"]["depense"][$_POST["pos"]]["poste"] . "` sur la proposition : `" . @$answer["answers"]["aapStep1"]["titre"] . " ` ";
                    } else {
                        $message = " @" . Yii::app()->session['user']["username"] . " a supprimé  une ligne de depense pour la proposition : `" . @$answer["answers"]["aapStep1"]["titre"] . "`";
                    }
                    foreach ($channelChat as $psid => $ps) {
                        RocketChat::post($ps, $message, $url, true);
                    }
                    foreach ($groupChat as $psid => $ps) {
                        RocketChat::post($ps, $message, $url, false);
                    }
                    if ($projectslug != "") {
                        RocketChat::post($projectslug, $message, $url);
                    }
                    break;
                case "newfinancement":
                    if (isset($_POST["pos"])) {
                        if ($_POST["pos"] == "") {
                            $_POST["pos"] = 0;
                        }
                        $message = " @" . Yii::app()->session['user']["username"] . " a ajoutée une nouvelle ligne de finanancement pour la proposition : `" . @$answer["answers"]["aapStep1"]["titre"] . " ` depense : `" . @$answer["answers"]["aapStep1"]["depense"][$_POST["pos"]]["poste"] . "`";
                    } else {
                        $message = " @" . Yii::app()->session['user']["username"] . " a ajoutée de nouvelles lignes de finanancement pour la proposition " . @$answer["answers"]["aapStep1"]["titre"];
                    }

                    self::send($channelChat, $groupChat, $message, $url, $projectslug);
                    break;
                case "deletefinancement":
                    if (isset($_POST["pos"])) {
                        if ($_POST["pos"] == "") {
                            $_POST["pos"] = 0;
                        }
                        $message = " @" . Yii::app()->session['user']["username"] . " a supprimé une ligne de finanancement pour la depense : `" . @$answer["answers"]["aapStep1"]["depense"][$_POST["pos"]]["poste"] . "` proposition : `" . @$answer["answers"]["aapStep1"]["titre"] . " ` ";
                    } else {
                        $message = " @" . Yii::app()->session['user']["username"] . "  a supprimé une ligne de finanancement pour la proposition " . @$answer["answers"]["aapStep1"]["titre"];
                    }

                    self::send($channelChat, $groupChat, $message, $url, $projectslug);
                    break;
                case "newpayement":
                    if (isset($_POST["pos"])) {
                        if ($_POST["pos"] == "") {
                            $_POST["pos"] = 0;
                        }
                        $message = " @" . Yii::app()->session['user']["username"] . " a ajoutée une nouvelle ligne de payement pour la proposition  : `" . @$answer["answers"]["aapStep1"]["titre"] . " ` depense : `" . @$answer["answers"]["aapStep1"]["depense"][$_POST["pos"]]["poste"] . "`";
                    } else {
                        $message = " @" . Yii::app()->session['user']["username"] . " a ajoutée de nouvelles lignes de payement pour la proposition " . @$answer["answers"]["aapStep1"]["titre"];
                    }

                    foreach ($channelChat as $psid => $ps) {
                        RocketChat::post($ps, $message, $url, true);
                    }
                    foreach ($groupChat as $psid => $ps) {
                        RocketChat::post($ps, $message, $url, false);
                    }
                    if ($projectslug != "") {
                        RocketChat::post($projectslug, $message, $url);
                    }
                    break;
                case "deletepayement":
                    if (isset($_POST["pos"])) {
                        if ($_POST["pos"] == "") {
                            $_POST["pos"] = 0;
                        }
                        $message = " @" . Yii::app()->session['user']["username"] . " a supprimé une ligne de payement du depense : `" . @$answer["answers"]["aapStep1"]["depense"][$_POST["pos"]]["poste"] . "` proposition  : `" . @$answer["answers"]["aapStep1"]["titre"] . " ` ";
                    } else {
                        $message = " @" . Yii::app()->session['user']["username"] . " a supprimé une ligne de payement pour la proposition " . @$answer["answers"]["aapStep1"]["titre"];
                    }

                    foreach ($channelChat as $psid => $ps) {
                        RocketChat::post($ps, $message, $url, true);
                    }
                    foreach ($groupChat as $psid => $ps) {
                        RocketChat::post($ps, $message, $url, false);
                    }
                    if ($projectslug != "") {
                        RocketChat::post($projectslug, $message, $url);
                    }
                    break;
                case "newtask":
                    if (isset($_POST["pos"]) && $_POST["pos"] == "") {
                        $_POST["pos"] = 0;
                    }
                    if (isset($_POST["pos"]) && isset($_POST["task"])) {
                        $message = " @" . Yii::app()->session['user']["username"] . " a ajoutée une nouvelle tache `" . $_POST["task"] . "` pour la proposition  : `" . @$answer["answers"]["aapStep1"]["titre"] . " ` depense : `" . @$answer["answers"]["aapStep1"]["depense"][$_POST["pos"]]["poste"] . "`";
                    } else {
                        $message = " @" . Yii::app()->session['user']["username"] . " a ajoutée une nouvelle tache sur la depense : " . @$answer["answers"]["aapStep1"]["titre"];
                    }

                    self::send($channelChat, $groupChat, $message, $url, $projectslug);
                    break;
                case "deletetask":
                    if (isset($_POST["pos"]) && $_POST["pos"] == "") {
                        $_POST["pos"] = 0;
                    }
                    if (isset($_POST["pos"]) && isset($_POST["task"])) {
                        $message = " @" . Yii::app()->session['user']["username"] . " a supprimé la tache `" . $_POST["task"] . "` pour la proposition  : `" . @$answer["answers"]["aapStep1"]["titre"] . " ` depense : `" . @$answer["answers"]["aapStep1"]["depense"][$_POST["pos"]]["poste"] . "`";
                    } else {
                        $message = " @" . Yii::app()->session['user']["username"] . " a supprimé une tache sur la depense : " . @$answer["answers"]["aapStep1"]["titre"];
                    }

                    self::send($channelChat, $groupChat, $message, $url, $projectslug);
                    break;
                case "checktask":
                    if (isset($_POST["actname"])) {
                        $message = " @" . Yii::app()->session['user']["username"] . " a completé la tache `" . $_POST["task"] . "` pour la proposition  : `" . @$answer["answers"]["aapStep1"]["titre"] . " ` depense : `" . @$_POST["actname"] . "`";
                    } else {
                        $message = " @" . Yii::app()->session['user']["username"] . " a ajoutée une nouvelle proposition nommée : " . @$answer["answers"]["aapStep1"]["titre"];
                    }

                    foreach ($channelChat as $psid => $ps) {
                        RocketChat::post($ps, $message, $url, true);
                    }
                    foreach ($groupChat as $psid => $ps) {
                        RocketChat::post($ps, $message, $url, false);
                    }
                    if ($projectslug != "") {
                        RocketChat::post($projectslug, $message, $url);
                    }
                    break;
                case "addperson":
                    if (!empty($_POST["pers"])) {
                        $message = "";
                        foreach ($_POST["pers"] as $persid => $pers) {
                            $presinfo = PHDB::findOneById(Person::COLLECTION, $persid);
                            if (!empty($presinfo['username']))
                                $msgUser = "L'utilisateur @" . $presinfo['username'];
                            else
                                $msgUser = ' Quelqu\'un';
                            $message .= $msgUser . " a été ajouté à la proposition `" . @$answer["answers"]["aapStep1"]["titre"] . "`" . PHP_EOL . " ";
                        }
                    }
                    self::send($channelChat, $groupChat, $message, $url, $projectslug);
                    break;
                case "rmperson":
                    if (!empty($_POST["pers"])) {
                        $message = "";
                        foreach ($_POST["pers"] as $persid => $pers) {
                            $presinfo = PHDB::findOneById(Person::COLLECTION, $persid);
                            if (!empty($presinfo['username']))
                                $msgUser = "L'utilisateur @" . $presinfo['username'];
                            else
                                $msgUser = ' Quelqu\'un';
                            $message .= $msgUser . " a été retiré de la proposition `" . @$answer["answers"]["aapStep1"]["titre"] . "`" . PHP_EOL . " ";
                        }
                    }
                    self::send($channelChat, $groupChat, $message, $url, $projectslug);
                    break;
                    if ($_POST["projectname"] && $_POST["actname"]) {
                        $message = " @" . Yii::app()->session['user']["username"] . " a annuler l'action " . $_POST["actname"] . " de " . $_POST["projectname"];
                    }
                    if (!empty($_POST["channelChat"])) $channelChat = [$_POST["channelChat"]];
                    self::send($channelChat, $groupChat, $message, $url, $projectslug);
                    break;
            }
            return Rest::json(array("result" => true, "msg" => $message));
        } else {
            switch ($action) {
                case "newaction":
					$actionName = $_POST["actname"] ?? '';
	                $projectName = $_POST["projectname"] ?? '';
					if(empty($actionName) && !empty($_POST['actionId'])) {
						$dbAction = PHDB::findOneById(Action::COLLECTION, $_POST['actionId'], ['name']);
						$actionName = $dbAction['name'];
					}
					if(empty($projectName) && !empty($_POST['projectId'])) {
						$dbProject = PHDB::findOneById(Project::COLLECTION, $_POST['projectId'], ['name', 'slug']);
						$projectName = $dbProject['name'];
					}
					if(!empty($dbProject['slug']) && empty($_POST["channelChat"])) {
						$channelChat = [$dbProject['slug']];
					}
                    if (!empty($actionName) && !empty($projectName)) {
                        $message = " @" . Yii::app()->session['user']["username"] . " a ajouté une action **\" $actionName \"** dans ** $projectName **";
                    }
                    if (!empty($_POST["channelChat"])) $channelChat = [$_POST["channelChat"]];
                    self::send($channelChat, $groupChat, $message, $url, null);
                    break;
                case "newstatusaction":
                    if (isset($_POST["actname"]) && $_POST["status"]) {
                        $action = PHDB::findOneById(Action::COLLECTION,$_POST["actionId"],array("name"));
                        if(!empty($action)){
                            $project = PHDB::findOneById(Project::COLLECTION,$action["parentId"],array("name","slug"));
                            if ($_POST["status"] == "done")
                            $message = " @" . Yii::app()->session['user']["username"] . " a terminé l'action " . $action["name"] . " de " . $project["name"];
                        }
                    }
                    if (!empty($_POST["channelChat"])) $channelChat = [$_POST["channelChat"]];
                    self::send($channelChat, $groupChat, $message, $url, null);
                    break;
                case "newtask2":
                    if (isset($_POST["taskname"]) && $_POST["projectname"] && $_POST["actname"]) {
                        $message = " @" . Yii::app()->session['user']["username"] . " a ajouté la tâche **" . $_POST["taskname"] . "** sur l'action **" . $_POST["actname"] . "** de **" . $_POST["projectname"] . "**";
                    }
                    if (!empty($_POST["channelChat"])) $channelChat = [$_POST["channelChat"]];
                    self::send($channelChat, $groupChat, $message, $url, null);
                    break;
                case "checktask2":
                    if (isset($_POST["taskname"]) && $_POST["projectname"] && $_POST["actname"]) {
                        $message = " @" . Yii::app()->session['user']["username"] . " a terminé la tâche **" . $_POST["taskname"] . "** sur l'action **" . $_POST["actname"] . "** de **" . $_POST["projectname"] . "**";
                    }
                    if (!empty($_POST["channelChat"])) $channelChat = [$_POST["channelChat"]];
                    self::send($channelChat, $groupChat, $message, $url, null);
                    break;
                case "newbug":
                    if (!empty($_POST["projectname"]) && $_POST["actname"]) {
                        $message = " @" . Yii::app()->session['user']["username"] . " a déclaré un bug **" . $_POST["actname"] . "** dans le projet **" . $_POST["projectname"] . "**";
                    }
                    if (!empty($_POST["channelChat"])) $channelChat = [$_POST["channelChat"]];
                    self::send($channelChat, $groupChat, $message, $url, null);
                    break;
                case "commentaction":
                    if ($_POST["actionId"]) {
                        $action = PHDB::findOneById(Action::COLLECTION,$_POST["actionId"],array("name","parentId"));
                        if(!empty($action)){
                            $project = PHDB::findOneById(Project::COLLECTION,$action["parentId"],array("name","slug"));
                            $message = " @" . Yii::app()->session['user']["username"] . " a commenté l'action " . $action["name"] . " de " . $project["name"];
                        }
                    }
                    if (!empty($_POST["channelChat"])) 
                        $channelChat = [$_POST["channelChat"]];
                    else 
                        $channelChat = $project["slug"];
                    self::send($channelChat, $groupChat, $message, $url, null);
                    break;
                case "cancelaction":
                    if ($_POST["projectname"] && $_POST["actname"]) {
                        $message = " @" . Yii::app()->session['user']["username"] . " a annuler l'action " . $_POST["actname"] . " de " . $_POST["projectname"];
                    }
                    if (!empty($_POST["channelChat"])) $channelChat = [$_POST["channelChat"]];
                    self::send($channelChat, $groupChat, $message, $url, null);
                    break;
                case "questioncms":
                    $alias = Yii::app()->session["user"]["name"];
                    $avatar = "/avatar/".Yii::app()->session["user"]["slug"];
                    if (isset($_POST["message"]) && isset($_POST["sujet"]) && isset($_POST["costumSlug"])) {
                        $message = " @" . Yii::app()->session["user"]["username"] . " a posé une question sur le costum " . $_POST["costumSlug"] . ",\n".
                                   "\nSujet: ". $_POST["sujet"] . "".
                                   "\nQuestion: " . $_POST["message"];
                    }
                    if (isset($_POST["channelChat"])) $channelChat = [$_POST["channelChat"]];
                    self::send($channelChat, $groupChat, $message, $url, null, $alias, $avatar);
                    break;
                case "feedbackcms":
                    $alias = Yii::app()->session["user"]["name"];
                    $avatar = "/avatar/".Yii::app()->session["user"]["slug"];
                    if (isset($_POST["type"]) && isset($_POST["sujet"]) && isset($_POST["feedback"])) {
                        $message = " @" . Yii::app()->session["user"]["username"] . " vient de soumettre un " . $_POST["type"] ."\n".
                                    "\nSujet: ". $_POST["sujet"] . ", ".
                                    "\nMessage: " . $_POST["feedback"];
                    }
                    if (isset($_POST["channelChat"])) $channelChat = [$_POST["channelChat"]];
                    self::send($channelChat, $groupChat, $message, $url, null, $alias, $avatar);
                    break;
                case "addabusnewsco":
                    $alias = Yii::app()->session["user"]["name"];
                    $avatar = "/avatar/".Yii::app()->session["user"]["slug"];
                    if (isset($_POST["id"]) && isset($_POST["reason"]) && isset($_POST["comment"])) {
                        $message = " @" . Yii::app()->session["user"]["username"] . " vient de signaler un contenu inapproprié pour *" . $_POST["reason"] ."*\n".
                                    "Commentaire: " . $_POST["comment"]."\n".
                                    "`id : ".$_POST["id"]."`";
                    }
                    if (isset($_POST["channelChat"])) $channelChat = [$_POST["channelChat"]];
                    self::send($channelChat, $groupChat, $message, $url, null, $alias, $avatar);
                    break;
                }
            return Rest::json(array("result" => true, "msg" => $message));
        }
    }
    private function send($channelChat, $groupChat, $message, $url, $projectslug, $alias = "oceco", $avatar="https://oce.co.tools/avatar.png") {
        foreach ($channelChat as $psid => $ps) {
            RocketChat::post($ps, $message, $url, true, $alias, $avatar);
        }
        foreach ($groupChat as $psid => $ps) {
            RocketChat::post($ps, $message, $url, false, $alias, $avatar);
        }
        if ($projectslug != "") {
            RocketChat::post($projectslug, $message, $url);
        }
    }
}
