<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use CAction, PHDB, Form, Actions, mongoId, Yii,Zone,Project;
use Citoyen;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\actor\Organization;

class OcecoorgadashboardAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($formparentid=null , $compact = false)
    {
        $controller = $this->getController();
        $action = [];
        $project = [];
        $title = "";
        $tplDashboard = "survey.views.tpls.answers.ocecoorgadashboard";
        $isGroup = false;

        
        $fp = PHDB::findOneById(Form::COLLECTION, $formparentid);

        if(isset($fp["coremu"]) && $fp["coremu"] == true ){
            $tplDashboard = "survey.views.tpls.answers.coremuGlobalDashboard";
        }
        if($compact){
            $tplDashboard = "survey.views.tpls.answers.coremuGlobalTable";
        }

        if (isset($fp["type"]) && $fp["type"] == "aap") {
            $fpconfig = PHDB::findOneById(Form::COLLECTION, $fp["config"]);
            if (isset($fpconfig["tplDashboard"])) {
                $tplDashboard = $fpconfig["tplDashboard"];
            }
        }

        $parent = PHDB::findOneById($fp["parent"][array_keys($fp["parent"])[0]]["type"], array_keys($fp["parent"])[0]);
        $sousorga = PHDB::find("organizations" , array("parent.".array_keys($fp["parent"])[0] => ['$exists' => true] ) );

        if (sizeof($sousorga) == 0 && empty($parent["parent"]) ) {
            $answers = PHDB::find(Form::ANSWER_COLLECTION, array("form" => $formparentid, "answers.aapStep1.titre" => ['$exists' => true]));
            $el_parent_slug = "";
            foreach ($answers as $key => $value) {
                if (!isset($answers[$key]["answers"])) {
                    unset($answers[$key]);
                }
            }

            if (isset($fp["parent"][array_keys($fp["parent"])[0]])) {
                if (isset($parent["parent"])) {
                    $el_parent_id = array_keys($parent["parent"])[0];
                    $mfp = PHDB::findOne(Form::COLLECTION, array("config" => $fp["config"], "parent." . $el_parent_id => ['$exists' => true]));
                    $el_parent_form_id = (string)$mfp["_id"];
                    $mparent = PHDB::findOneById($parent["parent"][array_keys($parent["parent"])[0]]["type"], array_keys($parent["parent"])[0]);
                    $el_parent_slug = $mparent["slug"];
                    $haveparent = true;
                } else {
                    $el_parent_form_id = "";
                    $haveparent = false;
                    $el_parent_slug = "";
                }
            } else {
                $el_parent_form_id = "";
                $el_parent_slug = "";
                $haveparent = false;
            }

            foreach ($fp["parent"] as $keyp => $valuep) {
                $parent = $valuep;
                break;
            }
            if(!empty($parent["_id"])){
                $action = $this->getActions($answers,$parent["_id"]);
            }
            

            $allData = $answers;

            foreach ($answers as $key => $value) {
                if (isset($answers[$key]["answers"]["aapStep1"]["depense"])) {
                    $answers[$key] = $answers[$key]["answers"]["aapStep1"]["depense"];
                    foreach ($allData[$key]["answers"]["aapStep1"]["depense"] as $al => $av) {
                        foreach ($allData[$key]["answers"]["aapStep1"] as $al2 => $av2) {
                            if (!isset($answers[$key][$al][$al2])) {
                                $answers[$key][$al][$al2] = $av2;
                            }
                        }
                    }
                } else {
                    $answers[$key] = [];
                }
            }

            $params = [
                "allData" => $allData,
                "allDataAns" => $answers,
                "project" => $project,
                "action" => $action,
                "title" => $title,
                "formparent" => $fp,
                "parent" => $parent,
                "el_parent_form_id" => $el_parent_form_id,
                "el_parent_slug" => $el_parent_slug,
                "hv_parent" => $haveparent
            ];

            if (isset($_POST["urlR"])) {
                $params["urlR"] = $_POST["urlR"];
            }

            /*array_walk_recursive(
                $params, function (&$value)
            {
                $value = htmlspecialchars(html_entity_decode($value, ENT_QUOTES, 'UTF-8'), ENT_QUOTES, 'UTF-8');
            }
            );*/

        } else {

            if(!empty($parent["parent"])){
                $sousorga = PHDB::find("organizations" , array("parent.".array_keys($parent["parent"])[0] => ['$exists' => true] ) , ["name"] );
                $originalfp = $fp;
                $originalparent = $parent;

                $fp = PHDB::findOne( Form::COLLECTION, array( "parent.".array_keys($parent["parent"])[0] => ['$exists' => true] , "config" => $fp["config"] ) );
                $parent = PHDB::findOneById("organizations" , array_keys($parent["parent"])[0]);

            }

            if (isset($fp["type"]) && $fp["type"] == "aap"){
                $fpconfig = PHDB::findOneById(Form::COLLECTION, $fp["config"]);
                if (isset($fpconfig["tplDashboard"])){
                    $tplDashboard = $fpconfig["tplDashboard"];
                }
                if (isset($fpconfig["multitplDashboard"])){
                    $tplDashboard = $fpconfig["multitplDashboard"];
                }
            }

            if (isset($fp["parent"][array_keys($fp["parent"])[0]])){

                $allsousorgaid = [];

                foreach ($sousorga as $ss => $ssd){
                    array_push($allsousorgaid, (string)$ssd["_id"]);
                }

                array_push($allsousorgaid, (string)$parent["_id"]);

                $sousorgaform = array();
                foreach ($allsousorgaid as $all => $allss) {
                    $sousorgaformn = PHDB::findOne(Form::COLLECTION, array("config" => $fp["config"], "parent.".$allss => ['$exists' => true]));
                    if (!empty($sousorgaformn["_id"])) {
                        $sousorgaform[(string)$sousorgaformn["_id"]] = $sousorgaformn;
                    }
                    //$action[(string)$all] = PHDB::find( Actions::COLLECTION,  array( "parentId" => $all)  );
                }
                $allorgaid = array_keys($sousorgaform);

                $answers = PHDB::find(Form::ANSWER_COLLECTION, array( "form" => [ '$in' => $allorgaid ],"answers.aapStep1.titre" => ['$exists' => true]) );

                foreach ($sousorga as $sid => $sorga) {
                    $sousorgafilter[$sorga["name"]] = $sid;
                    $rsousorgafilter[$sid] = $sorga["name"];
                }

                $sousorgafilter[$parent["name"]] = array_keys($fp["parent"])[0];
                $rsousorgafilter[array_keys($fp["parent"])[0]] =  $parent["name"];
            }

            $action = $this->getActions($answers,(string)$parent["_id"]);
            $project = $this->getProjects((string)$parent["_id"]);
            $contributors = $this->getContributors($answers,(string)$parent["_id"]);
            /*echo "<pre>";
            var_dump($contributors);
            echo "</pre>";exit;*/
            /*$actionpar = PHDB::find( Actions::COLLECTION,  array( "parentId" => (string)$parent["_id"])  );
            $action = $actionpar;*/

            /*foreach ($action as $actionid => $act) {
                if (!empty($act["creator"]) && !empty($act["name"])){
                    $action[$actionid]["tasks"][] = array("taskId" => $actionid , "task" => @$act["name"] , "userId" => @$act["creator"] , "contributors" => [$act["creator"] => array("type" => "citoyens")] , "checked" => false);
                }
            }*/

            /*foreach ($action as $actionid => $act) {
                if (!empty($act['tasks'])){
                    foreach ($act['tasks'] as $taskid => $tsk) {
                        if (empty($tsk["contributors"])){
                            $action[$actionid]["tasks"][$taskid]["contributors"] = array();
                        }
                    }
                }
            }*/


            $allData = $answers;
            $quartiers = [];
            foreach ($answers as $key => $value) {
                if(!empty($value["answers"]["aapStep1"]["quartiers"])){
                    $quartiers = array_merge($quartiers,$value["answers"]["aapStep1"]["quartiers"]);
                }

                if (isset($answers[$key]["answers"]["aapStep1"]["depense"])) {
                    $answers[$key] = $answers[$key]["answers"]["aapStep1"]["depense"];
                    foreach($allData[$key]["answers"]["aapStep1"]["depense"] as $al => $av){
                        foreach($allData[$key]["answers"]["aapStep1"] as $al2 => $av2) {
                            if (!isset($answers[$key][$al][$al2])) {
                                $answers[$key][$al][$al2] = $av2;
                            }
                        }
                        $answers[$key][$al]["context"] = $allData[$key]["context"];
                    }
                } else {
                    $answers[$key] = [];
                }
            }

            $quartiers = array_map(function($val){
                return new MongoId($val);
            },$quartiers);

            $quartiers =  PHDB::find(Zone:: COLLECTION,array("_id" => ['$in' => $quartiers]),array("name","insee"));

            $params = [
                "allData" => $allData,
                "allDataAns" => $answers,
                "contributors" => $contributors,
                "project" => $project,
                "action" => $action,
                "title" => $title,
                "formparent" => $fp,
                "parent" => $parent,
                "multiorga" => true,
                "sousorga" => $sousorgafilter,
                "rsousorga" => $rsousorgafilter,
                "quartiers" => $quartiers,
                "hv_parent" => ""
            ];

            if (isset($originalparent)){
                $params["defaultCACs"] = $originalparent["name"];
            }

            array_walk_recursive(
                $params, function (&$value)
            {
                $value = htmlspecialchars(html_entity_decode($value, ENT_QUOTES, 'UTF-8'), ENT_QUOTES, 'UTF-8');
            }
            );
        }


        if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial($tplDashboard , $params,true);
        else {
            $this->getController()->layout = "//layouts/empty";
            return $this->getController()->render($tplDashboard,$params);
        }
    }

    private function getActions($answers,$parentId){
        $projetsIds = [];
        $projects = $this->getProjects($parentId);
        if(!empty($projects))
            $projetsIds =  array_keys($projects);
        $actions = [];

        foreach ($answers as $anskey => $ansvalue) {
            if (!empty($ansvalue["project"]["id"])) 
                $projetsIds[] = $ansvalue["project"]["id"];
        }
        $projetsIds = array_values(array_unique($projetsIds));
        $projetsIds[] = $parentId;
        $actions = PHDB::find(Actions::COLLECTION,  array(
                "parentId" => ['$in' => $projetsIds],
                "status" => ['$ne' => "disabled"],
                //"status" => "todo"
        ));

        /*foreach ($actions as $actionid => $act) {
            $user = null;
            if(!empty($act["idUserAuthor"])) 
                $user = $act["idUserAuthor"]; 
            else 
                $user = $act["creator"];

            if (!isset($act["tasks"]) && !empty($act["status"]) && $act["status"] != "disable") {
                if(!empty($act["idUserAuthor"]) || !empty($act["creator"])){
                    $actions[$actionid]["tasks"][] = array(
                        "task" => $act["name"], 
                        "userId" => $user,
                        "contributors" => array($user => ["type" => "citoyens"]),
                        "checked" => false
                    );
                }
            }

            if(isset($act["tasks"]) && !empty($act["status"]) && $act["status"] != "disable"){
                foreach ($act["tasks"] as $ktask => $vtask) {
                    if(empty($vtask["contributors"])){
                        $actions[$actionid]["tasks"][$ktask]["contributors"] = array($user => ["type" => "citoyens"]);
                    }
                }
            }
        }*/
        return $actions;
    }

    private function getContributors($answers,$parentId){
        $contributorsIds = [];
        $contributors = [];
        $actions = $this->getActions($answers,$parentId);
        foreach ($actions as $kaction => $vaction) {
            if(!empty($vaction["links"]["contributors"])){
                foreach ($vaction["links"]["contributors"] as $kcontrib => $vcontrib) {
                    if(!in_array($kcontrib,$contributorsIds)) $contributorsIds[] = new MongoId($kcontrib);
                }
            }
            if(!empty($vaction["idUserAuthor"])){
                if(!in_array($vaction["idUserAuthor"],$contributorsIds)) $contributorsIds[] = new MongoId($vaction["idUserAuthor"]);
            }elseif(!empty($vaction["creator"])){
                if(!in_array($vaction["creator"],$contributorsIds)) $contributorsIds[] = new MongoId($vaction["idUserAuthor"]);
            }

            if(!empty($vaction["tasks"])){
                foreach ($vaction["tasks"] as $ktask => $vtask) {
                    if(!empty($vtask["contributors"])){
                        foreach ($vtask["contributors"] as $kcontrib => $vcontrib) {
                            if(!in_array($kcontrib,$contributorsIds)) $contributorsIds[] = new MongoId($kcontrib);
                        }
                    }
                }
            }
        }
        $contributors = PHDB::find(Citoyen::COLLECTION,array("_id" => ['$in' => $contributorsIds]),array("name","slug"));
        return $contributors;
    }

    private function getProjects($parentId){
        $projects = PHDB::find(Project::COLLECTION,array("parent.".$parentId => ['$exists' => true]),array("_id"));
        return $projects;
    }
}
