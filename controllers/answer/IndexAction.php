<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;
use ActStr;
use Answer;
use Authorisation;
use Element;
use CAction;
use Form;
use MongoId;
use Notification;
use PHDB;
use HtmlHelper;
use Yii;
use RocketChat;
use Rest;

class IndexAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id=null, $answer=null, $mode=null, $standalone= false, $form=null, $contextId=null, $contextType=null , $feedback=null , $typefeedback = null,$ask = null)
    {
        $isNew = false;
    	$this->getController()->layout = "//layouts/empty";
        $params = array();

        $isAap = (!isset($form["type"]) || ($form["type"] != "aap" && $form["type"] != "aapConfig"));

        if(!empty($form)){
            $form = PHDB::findOneById( Form::COLLECTION , $form);
            if (isset($form["session"]) && $form["session"] == true && isset($form["sessionMapping"]) && isset($form["actualSession"])) {
                $params["userAnswers"] = PHDB::find(Form::ANSWER_COLLECTION, array("form" => (string)$form["_id"], "user" => Yii::app()->session["userId"] , $form["sessionMapping"] => $form["actualSession"]),array("created","answers.aapStep1.titre","links"));
            } else {
                $params["userAnswers"] = PHDB::find(Form::ANSWER_COLLECTION, array("form" => (string)$form["_id"], "user" => Yii::app()->session["userId"]),array("created","answers.aapStep1.titre","links"));
            }
        }

        if (!empty(Yii::app()->session["userId"])){
            if(!empty($id)){
                if($id == "new" && empty($ask) && $standalone && !empty($params["userAnswers"]) &&
                        (
                            (isset($form["oneAnswerPerPers"]) && $form["oneAnswerPerPers"] != true ) ||
                            !isset($form["oneAnswerPerPers"])
                        )
                   ){
                    $params["isNew"] = true;
                    $params["form"] = $form ;
                    $params["contextId"] = $contextId;
                    $params["contextType"] = $contextType;

                    if ($standalone && !empty($form["tplformWizard"])){
                        $tpl = $form["tplformWizard"];
                    }elseif ($standalone){
                        $tpl = (!empty($form["tpl"])) ? $form["tpl"] : "survey.views.tpls.forms.standalone.formWizard";
                    }
                    return $this->getController()->renderPartial($tpl,$params );
                }elseif(($id == "new" &&  !empty($ask) && $standalone) || $id == "new" ){
                    if(!empty(Yii::app()->session["userId"])){
                        $context = null;
                        if(!empty($contextId) && !empty($contextType)){
                            $ctxEl = PHDB::findOneById( $contextType , $contextId,["name","address"]);
                            $context = [
                                $contextId => [
                                    "type" => $contextType,
                                    "name" => $ctxEl["name"]
                                ]
                            ];
                        }

                        $myAnswer = NULL;

                        if((!isset($form["type"]) || ($form["type"] != "aap" && $form["type"] != "aapConfig")) && isset($form["oneAnswerPerPers"]) && ($form["oneAnswerPerPers"] == "true" || $form["oneAnswerPerPers"] == true)){

                            $myAnswer = PHDB::findAndSort(Form::ANSWER_COLLECTION, array("user"=>Yii::app()->session["userId"], "form"=>$form["_id"]->{'$id'}),array("created" => -1));
                            if(!empty($myAnswer))
                                $myAnswer = array_values($myAnswer)[0];
                        }

                        if($myAnswer!=NULL && !empty($myAnswer)){
                            $answer = $myAnswer;
                        }else{
                            $answer = Answer::generateAnswer($form, false, $context);

                            if (!empty($form["projectGeneration"]) && $form["projectGeneration"] == "onStart"){

                                $project = Answer::GenerateProjectFromAnswer("project".uniqid(), "", [Yii::app()->session["userId"]], [], $form["parent"], [], (string)$answer["_id"]);

                                Element::updatePathValue( Form::ANSWER_COLLECTION, (string)$answer["_id"], "project", ["id" => (string)$project["projet"]]);

                                $answer["project"] = ["id" => (string)$project["projet"]];
                            }
                            /* $groupChat = [];
                            $channelChat = [];

                            foreach ($form["parent"] as $fpid => $fp){
                                $pr= PHDB::findOneById($fp["type"], $fpid);

                                if (isset($pr["hasRC"]) && filter_var($pr["hasRC"], FILTER_VALIDATE_BOOLEAN)){
                                    if (!empty($pr["tools"]["chat"]["int"])){
                                        foreach ($pr["tools"]["chat"]["int"] as $t => $ch){
                                            if (str_contains($ch["url"], "channel")) {
                                                array_push($channelChat, $ch["name"]);
                                            }elseif (str_contains($ch["url"], "group")){
                                                array_push($groupChat, $ch["name"]);
                                            }
                                        }
                                    }else{
                                        // array_push($channelChat, $pr["slug"]);
                                    }
                                }
                                array_push($parent, $pr);
                            } */

                            // Send RC notification

                            if(isset($_POST["url"])){
                                $url = $_POST["url"];
                            } else {
                                $url = "";
                            }

                            $message = " @" . Yii::app()->session['user']["username"] . " a répondu au formulaire : `".@$form["name"]."`";
                            //TODO à déplacer à l'envoie du formulaire
                            /* foreach ($channelChat as $psid => $ps){
                                try {
                                    RocketChat::post($ps, $message , $url, true );
                                } catch (CTKException $e) {
                                //
                                }
                            } foreach ($groupChat as $psid => $ps){
                                try {
                                    RocketChat::post($ps, $message , $url, false );
                                } catch (CTKException $e) {
                                //
                                }
                            } */

                        }


                        $params["answerId"] = $answer["_id"];
                        $mode = "w" ;
                        reset($form["parent"]);
                        $parentFormId = key($form["parent"]);

                        //send notification
                        Notification::constructNotification(
                            //verb
                            ActStr::VERB_ADD,
                            //author
                            [
                                "id" => Yii::app()->session["userId"],
                                "name" => Yii::app()->session["user"]["name"]
                            ],
                            //target
                            [
                                "id" => $parentFormId,
                                "type" =>$form["parent"][$parentFormId]["type"]
                            ],
                            //object
                            [
                                "id" => $form["_id"],
                                "type" => Form::COLLECTION
                            ],
                            //levelType
                            Form::COLLECTION,
                            //context
                            NULL,
                            //value
                            [
                                "id" => $answer["_id"],
                                "type" => Answer::COLLECTION
                            ]
                        );

                    } else {
                        if(Yii::app()->request->isAjaxRequest) {


                            return $this->getController()->renderPartial("co2.views.default.unTpl", array("msg" => Yii::t("form", "You are not allow to access to this form. Please check if you are logged in."), "icon" => "fa-lock"));
                        }
                        else {

                            return $this->getController()->render("co2.views.default.unTpl", array("msg" => Yii::t("form", "You are not allow to access to this form. Please check if you are logged in."), "icon" => "fa-lock"));
                        }
                        exit;
                    }

                }else {
                    $params["answerId"]=$id;
                    $answer = PHDB::findOne( Form::ANSWER_COLLECTION, array("_id"=>new MongoId($id)));
                    /*if((!empty($contextId) && !empty($contextType) && Authorisation::isElementAdmin($contextId,$contextType,Yii::app()->session["userId"])) ||
                        Form::canAdmin($answer["form"]) ||
                        ($answer["user"] == Yii::app()->session["userId"])
                    ){*/
                        if(!empty($answer["form"]))
                        $form = PHDB::findOneById( Form::COLLECTION , $answer["form"]);

                        if($isAap && !empty($form) && isset($form["stepOneOnly"]) && $form["stepOneOnly"]==true){
                            $baseurl = (!empty($context["costum"])) ? Yii::app()->getBaseUrl(true)."/costum/co/index/slug/".$context["slug"] : Yii::app()->getBaseUrl(true);
                            header("Location: ". $baseurl ."/survey/answer/getstep/id/".$id."/mode/w/form/".(string)$form["_id"]."/formstep/aapStep1");
                        }
                    /*}else{
                        if(Yii::app()->request->isAjaxRequest)
                            return $this->getController()->renderPartial("co2.views.default.unTpl",array("msg"=>Yii::t("survey", "You are not allowed to access to this answer. You don't have the right"),"icon"=>"fa-lock"));
                        else
                            return $this->getController()->render("co2.views.default.unTpl",array("msg"=>Yii::t("survey", "You are not allowed to access to this answer. You don't have the right"),"icon"=>"fa-lock"));
                    }*/
                }

                $params["form"] = $form ;
                $params = Form::getDataForm($params);

                $params["contextId"] = $contextId;
                $params["contextType"] = $contextType;

                $params["answer"] = $answer;

                $params["isNew"] = $isNew;
                $canEditForm = false;
                $canAdminAnswer = false;
                $canEditAnswer = false;
                $canSeeAnswer = false;

                if( empty($mode) || ( $mode != "w" && $mode != "r" ) ||
                    ( !empty($answer["validated"]) && $answer["validated"] == true) )
                    $mode = "r";

                if(!empty(Yii::app()->session['userId'])){
                    $canEditAnswer = Answer::canEdit($params["answer"], $form, Yii::app()->session['userId'], @$parentForm);
                    $canAdminAnswer = Answer::canAdmin($params["answer"], $form);

                    if($canEditAnswer === false && $canAdminAnswer === false)
                        $canSeeAnswer = Answer::canAccess($params["answer"], $form, Yii::app()->session['userId'], @$parentForm);
                    else
                        $canSeeAnswer = true;

                    if (isset($params["form"]["anyOnewithLinkCanAnswer"]) && ($params["form"]["anyOnewithLinkCanAnswer"] == 'true' || $params["form"]["anyOnewithLinkCanAnswer"] == true )) {
                        $canEditAnswer = true;
                        $canSeeAnswer = true;
                    }

                    if (isset($params["form"]["type"]) && ($params["form"]["type"] == "aap" || $params["form"]["type"] == "aapConfig")){
                        if (isset($params["form"]["params"]["onlymemberaccess"]) && $params["form"]["params"]["onlymemberaccess"] == 'true') {
                            $com_aap = Element::getCommunityByParentTypeAndId($params["form"]["parent"]);
                            if (!empty($com_aap[Yii::app()->session['userId']])) {
                                $canSeeAnswer = true;
                            }else{
                                $canSeeAnswer = false;
                            }
                        }
                        if (isset($params["form"]["params"]["onlyauthorcanedit"]) && $params["form"]["params"]["onlyauthorcanedit"] == 'true') {
                            if ($canAdminAnswer || (!empty(Yii::app()->session['userId']) && $params["answer"]["user"] == Yii::app()->session['userId'])) {
                                $canSeeAnswer = true;
                            }else{
                                $canSeeAnswer = false;
                            }
                        }
                    }

                }

                if( $mode == "w" && $canEditAnswer === false && $canAdminAnswer === false )
                    $mode = "rw";

                if($canSeeAnswer === true ){
                    $params["canEditForm"] = $canEditForm;
                    $params["canAdminAnswer"] = $canAdminAnswer;
                    $params["canEdit"] = $canEditAnswer;
                    $params["canSee"] = $canSeeAnswer;
                    $params["mode"] = (!empty($mode) ? $mode : "r");
                    $params["usersAnswered"] = array();
                    // Rest::json($form); exit();
                    if ($standalone && !empty($form["tplformWizard"])){
                        $tpl = $form["tplformWizard"];
                    }elseif ($standalone){
                        $tpl = (!empty($form["tpl"])) ? $form["tpl"] : "survey.views.tpls.forms.standalone.formWizard";
                    }elseif ($feedback && $typefeedback){
                        Element::updatePathValue( Form::ANSWER_COLLECTION, (string)$answer["_id"], "answers.aapStep1.tags", [$typefeedback]);
                        $params["typefeedback"] = $typefeedback;
                        $tpl = (!empty($form["tpl"])) ? $form["tpl"] : "survey.views.tpls.forms.feedback.formWizard";
                    }
                    else {
                        $tpl = (!empty($form["tpl"])) ? $form["tpl"] : "survey.views.tpls.forms.formWizard";
                    }
                    //$tpl = "survey.views.tpls.forms.formMediator";
                    if (isset($answer["links"]) && isset($answer["links"]["updated"])) {
                        foreach ($answer["links"]["updated"] as $e => $v) {
                            $params["usersAnswered"][] = PHDB::findOne("citoyens", array('_id' => new MongoId($e)),["name","profilImageUrl"]);
                        }
                    }

                    return $this->getController()->renderPartial($tpl,$params );
                } else {
                    if(Yii::app()->request->isAjaxRequest)
                        return $this->getController()->renderPartial("co2.views.default.unTpl",array("msg"=>Yii::t("survey", "You are not allowed to access to this answer. You don't have the right"),"icon"=>"fa-lock"));
                    else
                        return $this->getController()->render("co2.views.default.unTpl",array("msg"=>Yii::t("survey", "You are not allowed to access to this answer. You don't have the right"),"icon"=>"fa-lock"));
                }
            } else {
                if(Yii::app()->request->isAjaxRequest)
                    return $this->getController()->renderPartial("co2.views.default.unTpl",array("msg"=>Yii::t("survey", "You are not allowed to access to this answer ! reason : bad url"),"icon"=>"fa-lock"));
                else
                    return $this->getController()->render("co2.views.default.unTpl",array("msg"=>Yii::t("survey", "You are not allowed to access to this answer ! reason : bad url"),"icon"=>"fa-lock"));
            }
        }else {

            if (!empty($id)){
                if ($id == "new"){
                    $form = PHDB::findOneById( Form::COLLECTION , $form["_id"]);
                }else{
                    $answer = PHDB::findOneById( Form::ANSWER_COLLECTION , $id);
                    $form = PHDB::findOneById( Form::COLLECTION , $answer["form"]);
                }

                if (!empty($form["temporarymembercanreply"]) && ($form["temporarymembercanreply"] == "true" || $form["temporarymembercanreply"] == true)){
                    $connexionmodeParams = [
                        "id" => $id,
                        "answer" => $answer,
                        "mode" => $mode,
                        "form" => $form
                    ];
                    if ($standalone && !empty($form["tplformconnexionmode"])){
                        $tplc = $form["tplformconnexionmode"];
                    }else {
                        $tplc = (!empty($form["tpl"])) ? $form["tpl"] : "survey.views.default.connexionmode";
                    }
                    return $this->getController()->renderPartial($tplc, $connexionmodeParams);
                } else
                    if (!empty($form["disconnectedPage"]) && $form["disconnectedPage"] == "connexionModal"){

                        if(Yii::app()->request->isAjaxRequest) {

                            $output = "<style> #coformlogin form { display : contents; }</style><div id='coformlogin'>";
                            HtmlHelper::registerCssAndScriptsFiles( array('/js/default/loginRegister.js') , Yii::app()->getModule( "co2" )->getAssetsUrl());

                            $output .= $this->getController()->renderPartial('webroot.themes.CO2.views.layouts.loginRegisterForCoform');
                            $output .= "<div>";
                            return $output;

                        }else {

                            $output = "<style #coformlogin .form { display : contents; }></style><div id='coformlogin'>";
                            $output .= $this->getController()->render('webroot.themes.CO2.views.layouts.loginRegister');
                            $output .= "<div>";
                            return $output;
                        }
                    }
                else{
                    if(Yii::app()->request->isAjaxRequest)
                        return $this->getController()->renderPartial("co2.views.default.unTpl",array("msg"=>Yii::t("survey", "You are not allowed to access to this answer ! Reason : not logged"),"icon"=>"fa-lock","reason"=>"unlogged"));
                    else
                        return $this->getController()->render("co2.views.default.unTpl",array("msg"=>Yii::t("survey", "You are not allowed to access to this answer ! Reason : not logged"),"icon"=>"fa-lock","reason"=>"unlogged"));
                }
            } else{
                if(Yii::app()->request->isAjaxRequest)
                    return $this->getController()->renderPartial("co2.views.default.unTpl",array("msg"=>Yii::t("survey", "You are not allow to access to this answer ! reason : bad url"),"icon"=>"fa-lock"));
                else
                    return $this->getController()->render("co2.views.default.unTpl",array("msg"=>Yii::t("survey", "You are not allow to access to this answer ! reason : bbas url"),"icon"=>"fa-lock"));
            }
        }
    }
}
