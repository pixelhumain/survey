<?php
namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use Form;
use PHDB;
use Rest;
use Yii;
use MongoId;
use Organization;

class CountvisitAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($answerid = null,$formId=null)
    {
        $count = $this->getCounter($formId);
        if(!empty($answerid))
                $this->updateAnswerView($answerid);
        return Rest::json(array("notSeenCounter" => $count));
    }

    private function getCounter($formId=null){
        $count = 0;
        if(!empty(Yii::app()->session['userId'])){
            $inputsStep1 = PHDB::findOne(Form::INPUTS_COLLECTION,array("formParent" => $formId,"step" => "aapStep1"),array("inputs.year"));
            $form = PHDB::findOneById(Form::COLLECTION,$formId,array("actualSession"));
            
            $query = array(
                "form" => $formId,
                "user" => ['$ne' => Yii::app()->session['userId']],
                "answers.aapStep1.titre" => array('$exists' => true),
                "views.".Yii::app()->session['userId'] => ['$exists' => false],
                'status'=>['$not'=>array('$eq' =>"finish")],
                'project.id' => ['$exists'=> false]
            );
    
            if(isset($inputsStep1["inputs"]["year"])){
                $year = date('Y');
                if(!empty(Yii::app()->session["aapSession-".(string)$form["_id"]])){
					$year = Yii::app()->session["aapSession-".(string)$form["_id"]];
				}elseif(!empty($form["actualSession"])){
					$year = $form["actualSession"];
				}
                $query["answers.aapStep1.year"] = $year;
            }
    
            $count = PHDB::count(
                Form::ANSWER_COLLECTION,
                $query
            );
        }
        return $count;  
    }

    private function updateAnswerView($answerId=null){
        if(!empty(Yii::app()->session['userId'])){
            PHDB::update(Form::ANSWER_COLLECTION,
            array(
                "_id" => new MongoId($answerId)
            ),
            array(
                '$set' => ["views.".Yii::app()->session['userId'].".date" => time()]
            ));
        }
    }
}