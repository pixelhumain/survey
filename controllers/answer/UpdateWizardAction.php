<?php
namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use Answer;
use CTKException;
use Form;
use Mail;
use MongoId;
use PHDB;
use Rest;
use Yii;
use yii\helpers\Json;

class UpdateWizardAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($form=null, $answer=null , $step=null)
    {
        $return = [
            "value" => false,
            "msg" => ""
        ];
        $params = Form::getParamsForm($answer, $form);

        if(!empty($step) && !empty(array_values($params["steps"])[$step])){
            $isValidated = Form::validateStep($params , $step);
            if ($isValidated){
                $return["value"] = true;
                if(!empty($params["steps"]["afterValidated"])){
                    if(!empty($params["steps"]["afterValidated"]["sendEmail"]) && $params["steps"]["afterValidated"]["sendEmail"] && !empty($params["steps"]["afterValidated"]["emailTo"])){
                        $mailList =[];
                        $paramsMail = [
                            "tpl" => 'basic',
                            "tplObject"  => $params["steps"]["afterValidated"]["mailObject"],
                            "tplMail" => $mailList,
                            "html" => $params["steps"]["afterValidated"]["mailMessage"],
                            "btnRedirect" => [
                                "hash" => "#answer.index.id." . $answer . ".mode.w.standalone.true",
                                "label" => "Accéder au formulaire"
                            ]
                        ];
                        if(in_array("user", $params["steps"]["afterValidated"]["emailTo"]) && !empty($params["communityCitoyenGroup"][Yii::app()->session['userId']])){
                            array_push($mailList , $params["communityCitoyenGroup"][Yii::app()->session['userId']]["email"]);
                        }
                        foreach ($params["parent"]["links"]["members"] as $communityId => $community){
                            if(
                                in_array("admin", $params["steps"]["afterValidated"]["emailTo"]) &&
                                !empty($community["isAdmin"]) && $community["isAdmin"] &&
                                !empty($params["communityCitoyenGroup"][$communityId]["email"])
                            ){
                                array_push($mailList , $params["communityCitoyenGroup"][$communityId]["email"]);
                            }
                            if(
                                in_array("role", $params["steps"]["afterValidated"]["emailTo"]) &&
                                !empty($community["roles"]) && sizeof(array_intersect($params["steps"]["afterValidated"]["destRole"], $community["roles"])) <= 0 &&
                                !empty($params["communityCitoyenGroup"][$communityId]["email"])
                            ){
                                array_push($mailList , $params["communityCitoyenGroup"][$communityId]["email"]);
                            }

                            try {
                                $res = Mail::createAndSend($paramsMail);
                            } catch (CTKException $e) {
                                Rest::sendResponse(450, "Webhook : ".$e->getMessage());
                                die;
                            }
                        }

                    }
                    if(!empty($params["steps"]["afterValidated"]["showModal"]) && $params["steps"]["afterValidated"]["showModal"]){


                    }
                }
            }
        }
        return $return;
    }
}
