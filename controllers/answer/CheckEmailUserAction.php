<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use MongoRegex;
use Person;
use PHDB;
use Rest;

class CheckEmailUserAction extends \PixelHumain\PixelHumain\components\Action
{

    public function run($id=null, $form=null, $email=null){
        if (isset($_POST["id"]) && isset($_POST["email"]) && isset($_POST["form"])){
            $id = $_POST["id"];
            $form = $_POST["form"];
            $email = $_POST["email"];
        }
        $user = PHDB::findOne(Person::COLLECTION, array( "email" => new MongoRegex( '/^'.preg_quote(trim($email)).'$/i' ) ));

        if(empty($user)) {
            $res = ['duplicated' => false];
        } else {
            $res = ['duplicated' => true];
            $res["id"]=(string)$user["_id"];
            if(isset($user['roles']['tobeactivated']) && ($user['roles']['tobeactivated'] == 'true' || $user['roles']['tobeactivated'] == true)) {
                $res['tobeactivated'] = true;
            } else {
                $res['tobeactivated'] = false;
            }
        }
        return Rest::json($res);
    }
}