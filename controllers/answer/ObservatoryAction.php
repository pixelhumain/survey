<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use CAction, Authorisation, PHDB, Form, Element, Yii, Organization, MongoId, Zone;
 /**
  * Display the directory of back office
  * @param String $id Not mandatory : if specify, look for the person with this Id. 
  * Else will get the id of the person logged
  * @return type
  */
class ObservatoryAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run($id=null, $form=null, $week=null, $title = "", $contextId = null, $contextType = null)
    {
        $controller = $this->getController();
        $adminRight = Authorisation::isInterfaceAdmin();
        $zone = $_POST["zone"]??"";

        $zones = PHDB::findByIds(Zone::COLLECTION, ["58be480794ef47d1250ddbcc","58be480a94ef47d1250ddbcd","58be480c94ef47d1250ddbce",
        "58be4af494ef47df1d0ddbcc", "58be4bd194ef47e31d0ddbcb", "58be53b694ef47df1d0ddbcd", "5979d18f6ff9925a108b456c",
        "5979d1986ff9925a108b456d", "5979d1a16ff9925a108b456e", "5979d1ac6ff9925a108b456f", "5979d1c36ff9925a108b4570",
        "5979d1c96ff9925a108b4571", "5979d1cd6ff9925a108b4572", "5979d1d46ff9925a108b4573", "5979d1e26ff9925a108b4574",
        "5979d1ed6ff9925a108b4576", "5979d1f26ff9925a108b4577", "5979d1fc6ff9925a108b4579", "598412b76ff9920d048b4569",
        "5f645ff48b509cbd228b456d", "5fa3afb58b509cfa4a8b4567", "5fa3b01a8b509cd8018b456c", "62a093346a17b24a171419bc"], ["name"]);

        // Total answers 
        $totalAnswer=PHDB::count(Form::ANSWER_COLLECTION, array("form"=>$form,"answers"=>array('$exists' => 1)));
        $prntForm = PHDB::findOneById(Form::COLLECTION, $form );
        
        // Data Answer per week
        $answerByWeek = $answerUpdatedByWeek = array(
            "Monday"=>["label"=>Yii::t("translate","Monday"), "value"=>0], 
            "Tuesday"=>["label"=>Yii::t("translate","Tuesday"), "value"=>0], 
            "Wednesday"=>["label"=>Yii::t("translate","Wednesday"), "value"=>0], 
            "Thursday"=>["label"=>Yii::t("translate","Thursday"), "value"=>0],
            "Friday"=>["label"=>Yii::t("translate","Friday"), "value"=>0],
            "Saturday"=>["label"=>Yii::t("translate","Saturday"), "value"=>0],
            "Sunday"=>["label"=>Yii::t("translate","Sunday"), "value"=>0]
        );

        // Date All answer by day;
        $allAnswerByDate = array();
        // Date All answer updated by day;
        $allAnswerUpdatedByDate = array();
        // Orga Completed form
        $completedAnsOrg=[];
        // Orga Not completed form yet
        $ongoingAnsOrg=[];


        $subF=[];
        // Data Answer by step
        $validatedStepsCounter = array();
        // Data Answer by step
        $counterCurrentSteps = array();
        
        foreach($prntForm["subForms"] as $ind=>$step){
            $subF[$step]=PHDB::findOne(Form::COLLECTION, array("id"=>$step));
            $validatedStepsCounter[$step]= array("label" => $subF[$step]["name"], "value"=>0);
            $counterCurrentSteps[$step]=array("label" => $subF[$step]["name"], "value"=>0);
        }
        
        $weekNumber = (!empty($week)) ? $week : date("W");
        $weekBeginning = (isset($week) && $week=='allWeeks' && isset($prntForm["startDate"])) ? strtotime(str_replace('/', '-', $prntForm["startDate"])) : strtotime(sprintf("%4dW%02d", date("Y") , $weekNumber ));
        $weekEnding = (isset($week) && $week=='allWeeks' && isset($prntForm["endDate"])) ? strtotime(str_replace('/', '-', $prntForm["endDate"])) : strtotime(sprintf("%4dW%02d", date("Y") , $weekNumber+1 ));
        // var_dump($weekNumber); var_dump(date("d-m-y", $weekBeginning)); var_dump(date("d-m-y", $weekEnding));
        
        $matchedElementByZone = array();
        $obsQuery = array(
            "form"=>$form, 
            "answers"=>array('$exists' => 1), 
            "updated"=>array('$gte' => $weekBeginning,'$lt' => $weekEnding)
        );
        // Filter by zone
        if($zone!=""){
            $matchedElementByZone = PHDB::find(
                Organization::COLLECTION, 
                array("address.level3Name"=>$zone,
                '$or' => array(
                    array("reference.costum"=>"franceTierslieux"),
                    array("source.keys"=>"franceTierslieux")
                )), ["name", "collection"]);
            $orga = array();

            foreach ($matchedElementByZone as $key => $item) {
                unset($item["_id"]);
                $item["type"] = $item["collection"];
                unset($item["collection"]);
                array_push($orga, [$key => $item]);
            }
            $obsQuery["links.".Organization::COLLECTION] = array('$in'=>$orga);
        }
        //var_dump(array_keys($matchedElementByZone));
        $answersData = PHDB::findAndSortAndLimitAndIndex( Form::ANSWER_COLLECTION, $obsQuery, array("created"=>1, "updated"=>1));
        $nbAnwers=sizeof($answersData);

        // Validation step id mapping
        $vStpIdMapping = array();
        foreach($prntForm["subForms"] as $step ){
            $pattern = '/validateStep' . preg_quote($step, '/') . '/';
            $validationId=preg_grep($pattern,array_keys($prntForm["params"]));
            $validationId=(!empty($validationId) && sizeof($validationId)==1) ? array_values($validationId)[0] : null ;
            //var_dump($step, $validationId);
            $vStpIdMapping[$step]=$validationId;
            /*if(!empty($validationId) && !empty($va["answers"][$step]) && !empty($va["answers"][$step][str_replace("validateStep", "", $lastValidationId)])){}*/
         }

        $lastStepId = $prntForm["subForms"][array_key_last($prntForm["subForms"])];
        $lastValidationId = str_replace("validateStep", "", $vStpIdMapping[$lastStepId]);
        
        foreach($answersData as $ida=>$va){
            // Created answers
            if(isset($va["created"])){
                $d = date("d", $va["created"]);
                $m = date("m", $va["created"]);
                $y = date("Y", $va["created"]);
                $dateString = $d." / ".$m." / ".$y;
                if(!isset($allAnswerByDate[$dateString])){
                    $allAnswerByDate[$dateString]= ["label"=>$dateString, "value"=>1, "id"=>$ida];
                }else{
                    $allAnswerByDate[$dateString]["value"]+=1;
                }
                
                // answers by week
                $answerByWeek[date("l", $va["created"])]["value"]+=1;
            }

            // Updated answers
            if(isset($va["updated"])){
                $d = date("d", $va["updated"]);
                $m = date("m", $va["updated"]);
                $y = date("Y", $va["updated"]);
                $dateString = $d." / ".$m." / ".$y;
                if(!isset($allAnswerUpdatedByDate[$dateString])){
                    $allAnswerUpdatedByDate[$dateString]= ["label"=>$dateString, "value"=>1, "id"=>$ida];
                }else{
                    $allAnswerUpdatedByDate[$dateString]["value"]+=1;
                }
                // answers updated by week
                $answerUpdatedByWeek[date("l", $va["updated"])]["value"]+=1;
            }
            
            // answers by steps // 
            foreach ($validatedStepsCounter as $stepKey => $stepName) {
                $vsid =  str_replace("validateStep", "", $vStpIdMapping[$stepKey]);
                if(isset($vsid) && isset($va["answers"]) && isset($va["answers"][$stepKey]) && isset($va["answers"][$stepKey][$vsid])){
                    $validatedStepsCounter[$stepKey]["value"]+=1;
                }
                
                if(isset($va["step"]) && $va["step"]==$stepKey){
                    $counterCurrentSteps[$stepKey]["value"]+=1;
                }
            }

            if(!empty($va["links"]) && !empty($va["links"][Organization::COLLECTION]) && sizeof(array_keys($va["links"][Organization::COLLECTION]))==1){
                array_push($linkedOrg,new MongoId(array_keys($va["links"][Organization::COLLECTION])[0]));
                if(!empty($lastValidationId) && !empty($va["answers"][$lastStepId]) && !empty($va["answers"][$lastStepId][$lastValidationId])){
                  array_push($completedAnsOrg,new MongoId(array_keys($va["links"][Organization::COLLECTION])[0]));
                }else{
                  array_push($ongoingAnsOrg,new MongoId(array_keys($va["links"][Organization::COLLECTION])[0]));
                }
            }
        }

        $perRegions = array();
        $completedOrgInfo=Element::getElementSimpleByIds($completedAnsOrg,Organization::COLLECTION,array(), array("_id","name","address"));
        $ongoingOrgInfo=Element::getElementSimpleByIds($ongoingAnsOrg,Organization::COLLECTION,array(), array("_id","name","address"));
        $linkedOrgInfo=array_merge($completedOrgInfo,$ongoingOrgInfo);

        foreach ($linkedOrgInfo as $key => $org) {
            if(isset($org["address"]) && isset($org["address"]["level3Name"])){
                $region = $org["address"]["level3Name"];
                if(isset($perRegions[$region])){
                    $perRegions[$region]["value"] += 1;
                }else{
                    $perRegions[$region] = array("label"=>$region, "value"=>1);
                }
            }
        }

        $params = [
            "allanswers" => array("parentFormId"=> $form, "count"=>$totalAnswer),
            "adminRight" => $adminRight,
            "contextId" => $contextId,
            "contextType" => $contextType,
            "coform" => $prntForm,
            "currentWeek" => $weekNumber,
            "effectif" => $nbAnwers,
            "dataSets" => array(),
            "zones" => $zones,
            "activeZone" => $zone
        ];

        if($week == "allWeeks"){
            $params["dataSets"]["answerPerDate"] = array(
                "graphType"=>"line", 
                "col" => "6",
                "title"=>"Evolution des réponses créées depuis le lancement", 
                "data"=>$allAnswerByDate);
            
            $params["dataSets"]["answerPerUpdateDate"] = array(
                "graphType"=>"line", 
                "col" => "6",
                "title"=>"Evolution de la mise à jour", 
                "data"=>$allAnswerUpdatedByDate);
        }else{
            $params["dataSets"]["answerPerWeek"] = array(
                "graphType"=>"line", 
                "col" => "6",
                "title"=>"Nombre de réponses créées du <span style='color:#A4C044'>".date("d/m/y", $weekBeginning)."</span> au  <span style='color:#A4C044'>".date("d/m/y", $weekEnding)."</span> (Total : $nbAnwers)", 
                "data"=>$answerByWeek);
            
            $params["dataSets"]["answerUpdatedPerWeek"] = array(
                "graphType"=>"line", 
                "col" => "6",
                "title"=>"Nombre de réponses mises à jour du <span style='color:#A4C044'>".date("d/m/y", $weekBeginning)."</span> au  <span style='color:#A4C044'>".date("d-m-y", $weekEnding)."</span> (Total : $nbAnwers)", 
                "data"=>$answerUpdatedByWeek);
        }

        $params["dataSets"]["currentSteps"] = array(
            "graphType"=>"bar", 
            "height"=>"250",
            "percent"=>true,
            "title"=>"Taux des étapes en cours (Total : $nbAnwers)",
            "data"=>array_values($counterCurrentSteps));

        $params["dataSets"]["answerPerStep"] = array(
            "graphType"=>"bar", 
            "height"=>"250",
            "title"=>"Taux des réponses par rapports aux étapes validés (Total : $nbAnwers)",
            "data"=>array_values($validatedStepsCounter));

        $params["dataSets"]["answerCompletedRegions"] = array(
            "graphType"=>"doughnut", 
            "height"=>"250",
            "col"=>"5",
            "title"=>"Taux de remplissages (Total : $nbAnwers)",
            "data"=>array(
                "completed"=>array("label"=> "Réponses complet", "value"=>count($completedOrgInfo)), 
                "ongoing"=>array("label"=> "Réponses en cours", "value"=>count($ongoingOrgInfo))
            ));

        if(count($perRegions)>1){
            $params["dataSets"]["answerPerRegions"] = array(
                "graphType"=>"doughnut", 
                "height"=>"250",
                "col"=>"7",
                "title"=>"Taux des Réponses Par Région (Total : $nbAnwers)",
                "data"=>$perRegions);
        }
	       
        $tpl= "survey.views.tpls.answers.observatory";
	    return $controller->renderPartial($tpl,$params,true);
    }
}
