<?php
namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use Form;
use PHDB;
use Yii;
use Organization;
use Actions;

class MultiorgadashboardAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($formparentid=null)
    {
        $action = [];
        $project = [];
        $title = "";
        $tplDashboard = "survey.views.tpls.answers.ocecoorgadashboard";

        $controller = $this->getController();

        //$answers = [ $formparentid => PHDB::find(Form::ANSWER_COLLECTION, array("form" => $formparentid,"answers.aapStep1" => ['$exists' => true])) ];

        $fp = PHDB::findOneById(Form::COLLECTION, $formparentid);
        if (isset($fp["type"]) && $fp["type"] == "aap"){
            $fpconfig = PHDB::findOneById(Form::COLLECTION, $fp["config"]);
            if (isset($fpconfig["tplDashboard"])){
                $tplDashboard = $fpconfig["tplDashboard"];
            }
            if (isset($fp["tplDashboard"])){
                $tplDashboard = $fp["tplDashboard"];
            }
            if (isset($fpconfig["multitplDashboard"])){
                $tplDashboard = $fpconfig["multitplDashboard"];
            }
            if (isset($fp["multitplDashboard"])){
                $tplDashboard = $fp["multitplDashboard"];
            }
        }

        if (isset($fp["parent"][array_keys($fp["parent"])[0]])){
            $parent = PHDB::findOneById($fp["parent"][array_keys($fp["parent"])[0]]["type"], array_keys($fp["parent"])[0]);

            $sousorga =  PHDB::find( Organization::COLLECTION , array("parent.".array_keys($fp["parent"])[0] => ['$exists' => true]  ), ["name"]);

            $allsousorgaid = [];

            foreach ($sousorga as $ss => $ssd){
                array_push($allsousorgaid, (string)$ssd["_id"]);
            }

            array_push($allsousorgaid, (string)$parent["_id"]);

            $sousorgaform = array();
            foreach ($allsousorgaid as $all => $allss) {
                $sousorgaformn = PHDB::findOne(Form::COLLECTION, array("config" => $fp["config"], "parent.".$allss => ['$exists' => true]));
                if (!empty($sousorgaformn["_id"])) {
                    $sousorgaform[(string)$sousorgaformn["_id"]] = $sousorgaformn;
                }
                //$action[(string)$all] = PHDB::find( Actions::COLLECTION,  array( "parentId" => $all)  );
            }
            $allorgaid = array_keys($sousorgaform);

            $answers = PHDB::find(Form::ANSWER_COLLECTION, array( "form" => [ '$in' => $allorgaid ]) );

            foreach ($sousorga as $sid => $sorga) {
                $sousorgafilter[$sorga["name"]] = $sid;
                $rsousorgafilter[$sid] = $sorga["name"];
            }

            $sousorgafilter[$parent["name"]] = array_keys($fp["parent"])[0];
            $rsousorgafilter[array_keys($fp["parent"])[0]] =  $parent["name"];
        }

        foreach ($answers as $key => $value) {
            if (!isset($answers[$key]["answers"])) {
                unset($answers[$key]);
            }
        }

        foreach ($answers as $anskey => $ansvalue) {
            if (!empty($ansvalue["project"]["id"])) {

                $action[$anskey] = PHDB::find( Actions::COLLECTION,  array( "parentId" => $ansvalue["project"]["id"])  );

            }else{
                $action[$anskey] = [];
            }
        }

        $action[(string)$parent["_id"]][] = PHDB::find( Actions::COLLECTION,  array( "parentId" => (string)$parent["_id"])  );

        $actionpar = PHDB::find( Actions::COLLECTION,  array( "parentId" => (string)$parent["_id"])  );

        foreach($actionpar as $actionparid => $actionpar){
            $action[$actionparid] = $actionpar;
        }

        foreach ($action as $actionid => $act) {
            if (!empty($act["creator"]) && !empty($act["name"])){
                //if (empty($act["status"])){
                    $action[$actionid]["tasks"][] = array("taskId" => $actionid , "task" => @$act["name"] , "userId" => @$act["creator"] , "contributors" => [$act["creator"] => array("type" => "citoyens")] , "checked" => false);

                //}
            }
            //var_dump($act);
        }

        foreach ($action as $actionid => $act) {
            if (!empty($act['tasks'])){
                foreach ($act['tasks'] as $taskid => $tsk) {
                    if (empty($tsk["contributors"])){
                        $action[$actionid]["tasks"][$taskid]["contributors"] = array();
                        //$action[$actionid]["tasks"][] = array("taskId" => $actionid, "task" => @$act["name"], "userId" => @$act["creator"], "contributors" => [$act["creator"] => array("type" => "citoyens")], "checked" => false);
                    }
                }
            }
        }

        //var_dump($action);

        $allData = $answers;

        foreach ($answers as $key => $value) {
            if (isset($answers[$key]["answers"]["aapStep1"]["depense"])) {
                $answers[$key] = $answers[$key]["answers"]["aapStep1"]["depense"];
                foreach($allData[$key]["answers"]["aapStep1"]["depense"] as $al => $av){
                    foreach($allData[$key]["answers"]["aapStep1"] as $al2 => $av2) {
                        if (!isset($answers[$key][$al][$al2])) {
                            $answers[$key][$al][$al2] = $av2;
                        }
                    }
                    $answers[$key][$al]["context"] = $allData[$key]["context"];
                }
            } else {
                $answers[$key] = [];
            }
        }

        $params = [
            "allData" => $allData,
            "allDataAns" => $answers,
            "project" => $project,
            "action" => $action,
            "title" => $title,
            "formparent" => $fp,
            "parent" => $parent,
            "multiorga" => true,
            "sousorga" => $sousorgafilter,
            "rsousorga" => $rsousorgafilter,
            "hv_parent" => ""
        ];

        array_walk_recursive(
            $params, function (&$value)
        {
            $value = htmlspecialchars(html_entity_decode($value, ENT_QUOTES, 'UTF-8'), ENT_QUOTES, 'UTF-8');
        }
        );

        if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial($tplDashboard , $params,true);
        else {
            $this->getController()->layout = "//layouts/empty";
            return $this->getController()->render($tplDashboard,$params);
        }
    }
}
        