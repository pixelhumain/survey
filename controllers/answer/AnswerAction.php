<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use Answer;
use DateTimeImmutable;
use Element;
use Form;
use MongoId;
use HtmlHelper;
use Organization;
use Person;
use PHDB;
use Document;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
use Yii;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;

class AnswerAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id = null, $mode = "w", $view = "default" , $standalone = false , $form = null, $typefeedback = null, $ask = null , $step = null , $input = null , $isajax = false , $project = null, $isinsideform = true, $aapview = "parentform")
    {
        $this->getController()->layout = "//layouts/empty";
        $params = [];
        $editMode = isset($_POST["editMode"]) ? $_POST["editMode"] : "w";
        $configView = isset($_POST["configView"]) ? $_POST["configView"] : "inputlistConfig";
        $askAacAction = isset($_POST["aacAsk"]) ? $_POST["aacAsk"] : true;
        //Get Data
        if (!empty($id) || !empty($form) || !empty($project)) {
            $params = Form::getParamsForm($id, $form , $step , $input, $editMode , $project);
            $answer = $params['answer'];
            if(empty($params["answer"]) && $editMode == "fa" && isset($params["canAdminAnswer"]) && $params["canAdminAnswer"]) {
                $params["answer"]["_id"] = isset($params["parentForm"]["_id"]) ? new MongoId($params["parentForm"]["_id"]) : null;
                $params["answer"]["form"] = isset($params["parentForm"]["_id"]) ? (string) $params["parentForm"]["_id"] : null;
                $params["answer"]["collection"] = "answers";
            }
            if(isset(Form::$coformQuestionIcons)) {
                $params["coformQuestionIcons"] = Form::$coformQuestionIcons;
            }
            if($id != "new" && $mode != "fa") {
                $id = (string)$params["answer"]["_id"];
            }

        } else {
            $params["msg"] = "You are not allow to access! reason : bad url";
            $params["icon"] = "fa-lock";
        }

        if($view == "standalone" || $standalone ){
            $params["isStandalone"] = true;
            $view = "standalone";
        }else{
            $params["isStandalone"] = false;
        }
        $params["isCustom"] = $view == "custom";

        $params["isAsking"] = filter_var($ask, FILTER_VALIDATE_BOOLEAN);
        $params["aapview"] = $aapview;
        $checkDate = "include";
        $timezone = !empty(Yii::app()->session["timezone"]) ? Yii::app()->session["timezone"] : "UTC";
        date_default_timezone_set($timezone);
        $startDate = null;
        $endDate = null;
        if(!empty($params["parentForm"]["startDate"]) && !is_int($params["parentForm"]["startDate"])) {
            $startDate = !empty($params["parentForm"]["startDate"]) ? $params["parentForm"]["startDate"]->toDateTime()->format('c') : null;
            $endDate = !empty($params["parentForm"]["endDate"]) ? $params["parentForm"]["endDate"]->toDateTime()->format('c') : null;
        }elseif(!empty($params["parentForm"]["startDate"])){
            $startDate = isset($params["parentForm"]["startDate"]) ? str_replace("/","-",$params["parentForm"]["startDate"]) : null;
            $endDate = isset($params["parentForm"]["endDate"]) ? str_replace("/","-",$params["parentForm"]["endDate"]) : null;
        }
        $now = date('d-m-Y H:i', time());
        if(isset($startDate))
            $checkDate = Api::checkDateStatus($now,$startDate,$endDate);
        if (empty($answer) && UtilsHelper::is_valid_mongoid($id))
            $answer = PHDB::findOneById(Form::ANSWER_COLLECTION, $id);

        if($checkDate == "include" && $params["parentForm"]["active"] == true){
            $params["isOpenToTempAcount"] = true;
        }else{
            $params["isOpenToTempAcount"] = false;
        }

        //Prepare Data
        if (!empty(Yii::app()->session["userId"])) {
            if ($params["canSeeAnswer"] && $view == "feedback" && $typefeedback) {
                Element::updatePathValue(Form::ANSWER_COLLECTION, (string)$answer["_id"], "answers.aapStep1.tags", [$typefeedback]);
                $params["typefeedback"] = $typefeedback;
            }
            if ($params["isNew"] == true) {
                if (!empty($params["parentForm"]["aapType"]) && $params["parentForm"]["aapType"] == "aac" && filter_var($askAacAction, FILTER_VALIDATE_BOOLEAN) && isset($params["myOthersCommuns"]) && count($params["myOthersCommuns"]) > 0) {
                    $params["tpl"] = "survey.views.tpls.forms.askAac";
                } else if ($params["canSeeAnswer"]) {
                    if (isset($params["parentForm"]["oneAnswerPerPers"]) && $params["parentForm"]["oneAnswerPerPers"] == true && !empty($params["userAnswers"])) {
                        $params["answer"] = array_values($params["userAnswers"])[0];
                        $params["msg"] = "Formulaire à reponse unique, reponse récupéré";
                    } elseif (!filter_var($ask, FILTER_VALIDATE_BOOLEAN)) {
                        if(empty($params["context"]["type"])){
                            $params["context"]["type"] = "citoyens";
                        }
                        if ($params["canAdminAnswer"] || ((!isset($params["parentForm"]["active"]) || $params["parentForm"]["active"] == true) && $checkDate == "include")) {
                            $params["answer"] = Answer::generateAnswer($params["parentForm"], false, [
                                (string)$params["context"]["_id"] => [
                                    "type" => $params["context"]["type"],
                                    "name" => $params["context"]["name"]
                                ]
                            ]);
                            if(isset($params["parentForm"]["type"]) && $params["parentForm"]["type"] === "aap")
                                Aap::notificationCreateProposal((string)$params["answer"]["_id"]);
                        } else {

                        }
                    }

                    if (!empty($params["parentForm"]["projectGeneration"]) && $params["parentForm"]["projectGeneration"] == "onStart") {

                        $project = Answer::GenerateProjectFromAnswer("project" . uniqid(), "", [Yii::app()->session["userId"]], [], $params["parentForm"]["parent"], [], $answer);

                        Element::updatePathValue(Form::ANSWER_COLLECTION, (string)$answer["_id"], "project", ["id" => (string)$project["projet"]]);

                        $answer["project"] = ["id" => (string)$project["projet"]];
                    }
                }
            } else if (empty($params["answer"])) {
                $params["answer"]["_id"] = new MongoId($params["parentForm"]["_id"]);
                $params["answer"]["form"] = $params["parentForm"]["_id"];
                $params["answer"]["collection"] = "answers";
            }
        } else {
            $params["msg"] = Yii::t("survey", "You are not allowed to access! You are not logged");
            $params["icon"] = "fa-lock";
            $params["reason"] = "unlogged";
        }
        //Prepare view
        $tpl = "co2.views.default.unTpl";
        if (!empty($id) || !empty($form)) {

            if ($view == "slide"){
                $tpl = $params["tplSlide"];
            } elseif ($view == "offline" && isset($params["parentForm"]["notloggedview"]) && $params["parentForm"]["notloggedview"]){
                $tpl = $params["tplOffline"];
            } elseif ($view == "custom" && empty(Yii::app()->session["userId"])){
                $tpl = $params["tplCustom"];
            } else {
                if (!empty(Yii::app()->session["userId"])) {
                    if ($mode == "fa") {
                        $tpl = isset($params["parentForm"]["type"]) && $params["parentForm"]["type"] == "aap" && !isset($_POST["configView"]) ? "survey.views.tpls.forms.configuration" : "survey.views.tpls.forms.config.".$configView;
                    } else if ($params["canSeeAnswer"] && $mode != "fa") {
                        if ($params["canAdminAnswer"] || !isset($params["parentForm"]["active"]) || $params["parentForm"]["active"] == true) {
                            if ($params["canAdminAnswer"] || $checkDate == "include") {
                                $params["mode"] = isset($_POST["editMode"]) ? $_POST["editMode"] : $mode;
                                if ($view == "standalone") {
                                    $tpl = $params["tplStandalone"];
                                } elseif ($view == "feedback" && $typefeedback) {
                                    $tpl = $params["tplFeedback"];
                                } elseif ($view == "custom"){
                                    $tpl = $params["tplCustom"];
                                } else {
                                    $tpl = $params["tpl"];
                                }
                            } elseif ($checkDate == "past") {
                                $params["msg"] = Yii::t("survey", "You are not allowed to access. Form is not active anymore. Survey was ended on ") . (Api::is_timestamp($endDate) ? date('d/m/Y H:i:s', $endDate) : str_replace("-", "/", $endDate));
                                $params["icon"] = "fa-lock";
                            } elseif ($checkDate == "late") {
                                $params["msg"] = Yii::t("survey", "You are not allowed to access. Form is not active yet. Survey will start on ") . (Api::is_timestamp($startDate) ? date('d/m/Y H:i:s', $startDate) : str_replace("-", "/", $startDate));
                                $params["icon"] = "fa-lock";
                            }
                        } else {
                            $params["msg"] = Yii::t("survey", "You are not allowed to access. Form is not active");
                            $params["icon"] = "fa-lock";
                        }
                    } else {
                        $params["msg"] = Yii::t("survey", "You are not allowed to access. You don't have the right");
                        $params["icon"] = "fa-lock";
                    }
                } else {
                    if (isset($params["parentForm"]["temporarymembercanreply"]) && $params["parentForm"]["temporarymembercanreply"] && $mode != "fa") {
                        if (!isset($params["parentForm"]["active"]) || $params["parentForm"]["active"] == true) {
                            if (
                                $params["canAdminAnswer"] ||
                                (!isset($params["parentForm"]["startDate"]) || (is_numeric($params["parentForm"]["startDate"]) && time() > $params["parentForm"]["startDate"]) || (!is_numeric($params["parentForm"]["startDate"]) && time() > $this->getStrtotime($params["parentForm"]["startDate"]))) &&
                                (!isset($params["parentForm"]["startDateNoconfirmation"]) || (is_numeric($params["parentForm"]["startDateNoconfirmation"]) && time() > $params["parentForm"]["startDateNoconfirmation"]) || (!is_numeric($params["parentForm"]["startDateNoconfirmation"]) && time() > $this->getStrtotime($params["parentForm"]["startDateNoconfirmation"])))
                            ) {
                                if (
                                    $params["canAdminAnswer"] ||
                                    (!isset($params["parentForm"]["endDate"]) || (is_numeric($params["parentForm"]["endDate"]) && time() < $params["parentForm"]["endDate"]) || (!is_numeric($params["parentForm"]["endDate"]) && time() < $this->getStrtotime($params["parentForm"]["endDate"]))) &&
                                    (!isset($params["parentForm"]["endDateNoconfirmation"]) || (is_numeric($params["parentForm"]["endDateNoconfirmation"]) && time() < $params["parentForm"]["endDateNoconfirmation"]) || (!is_numeric($params["parentForm"]["endDateNoconfirmation"]) && time() < $this->getStrtotime($params["parentForm"]["endDateNoconfirmation"])))
                                ) {
                                    $tpl = $params["tplconnexionmode"];
                                } else {
                                    $params["msg"] = Yii::t("survey", "You are not allowed to access. Form is not active anymore. Survey was ended on " . (is_numeric($params["parentForm"]["endDateNoconfirmation"]) ? date('d/m/Y', $params["parentForm"]["endDateNoconfirmation"]) : date('d / m / Y', $this->getStrtotime($params["parentForm"]["endDateNoconfirmation"], "d/m/Y"))));
                                    $params["icon"] = "fa-lock";
                                }
                            } else {
                                $params["msg"] = Yii::t("survey", "You are not allowed to access. Form is not active yet. Survey will start on " . (is_numeric($params["parentForm"]["startDateNoconfirmation"]) ? date('d/m/Y', $params["parentForm"]["startDateNoconfirmation"]) : date('d / m / Y', $this->getStrtotime($params["parentForm"]["startDateNoconfirmation"], "d/m/Y"))));
                                $params["icon"] = "fa-lock";
                            }
                        } else {
                            $params["msg"] = Yii::t("survey", "You are not allowed to access. Form is not active");
                            $params["icon"] = "fa-lock";
                        }
                        if ($params["canAdminAnswer"] || !isset($params["parentForm"]["active"]) || $params["parentForm"]["active"] == true) {
                            if ($params["canAdminAnswer"] || $checkDate == "include") {
                                $tpl = $params["tplconnexionmode"];
                            } elseif ($checkDate == "past") {
                                $params["msg"] = Yii::t("survey", "You are not allowed to access. Form is not active anymore. Survey was ended on " . date('m/d/Y H:i:s', $endDate));
                                $params["icon"] = "fa-lock";
                            } elseif ($checkDate == "late") {
                                $params["msg"] = Yii::t("survey", "You are not allowed to access. Form is not active yet. Survey will start on " . date('m/d/Y H:i:s', $startDate));
                                $params["icon"] = "fa-lock";
                            }
                        } else {
                            $params["msg"] = Yii::t("survey", "You are not allowed to access. Form is not active");
                            $params["icon"] = "fa-lock";
                        }
                    } else {
                        if (!empty($params["parentForm"]["disconnectedPage"]) && $params["parentForm"]["disconnectedPage"] == "connexionModal") {
                            if (Yii::app()->request->isAjaxRequest) {

                                $output = "<style> #coformlogin form { display : contents; }</style><div id='coformlogin'>";
                                HtmlHelper::registerCssAndScriptsFiles(array('/js/default/loginRegister.js'), Yii::app()->getModule("co2")->getAssetsUrl());

                                $output .= $this->getController()->renderPartial('webroot.themes.CO2.views.layouts.loginRegisterForCoform');
                                $output .= "<div>";
                                return $output;

                            } else {

                                $output = "<style #coformlogin .form { display : contents; }></style><div id='coformlogin'>";
                                $output .= $this->getController()->render('webroot.themes.CO2.views.layouts.loginRegister');
                                $output .= "<div>";
                                return $output;
                            }
                        }
                    }
                }
            }
        }

        $params["isajax"] = $isajax;
        $params["isInsideForm"] = $isinsideform;

        if($step == "aapStep3" || $step == "aapStep1"){
            if(!empty($answer["_id"])) {
                $params["answerfiles"] = Document::getListDocumentsWhere(array(
                    "id" => (string)$answer["_id"],
                    "type" => 'answers'), "image");
            }else{
                $params["answerfiles"] = [];
            }

            $params["Arr"] = Document::getListDocumentsWhere(
                array(
                    "id" => (string)$params["parentForm"]["_id"],
                    "type" => 'form',
                    "subKey" => 'bgStandalone',
                ),
                "image"
            );
            if(isset($params["parentForm"]["coremu"]) && $params["parentForm"]["coremu"] == true){
                $links=@$params["context"]["links"];
                $params["get_links"] = Element::getAllLinks($links,"organizations", (string)$params["context"]["_id"]);
            }else {
                $params["get_tls"] = isset(Yii::app()->session['userId']) ? Organization::get_tls(['name', 'profilImageUrl'], Yii::app()->session['userId'], 1) : [];
            }
            $params["get_orga"] = isset(Yii::app()->session['userId']) ? PHDB::find(Organization::COLLECTION, ["links.members.".Yii::app()->session['userId'].".isAdmin" => true] , ['name', 'profilImageUrl']) : [];
            $params["totalfinancement"] = Form::getFormTotalFinancement(isset($answer["form"]) ? $answer["form"] : (string) $params["parentForm"]["_id"] ,$params["parentForm"] , $params["context"] );
            $params["payementPartialConfig"] = Form::getPayementPartialConfig(Yii::app()->session['userId']);
            $params["isftlCommun"] = isset($params["context"]["slug"]["coremu"]) ? true : false;
        }

        //Render view
        if (Yii::app()->request->isAjaxRequest)
            return $this->getController()->renderPartial($tpl, $params);
        return $this->getController()->render($tpl, $params);
    }

    function getStrtotime($timeDateStr, $formatOfStr="j/m/Y"){
        $timeStamp = DateTimeImmutable::createFromFormat($formatOfStr,$timeDateStr);
        if($timeStamp===false){
            // Bad date string or format string.
            return -6858133619; // 3/09/1752
        } else {
            // Date string and format ok.
            return $timeStamp->format("U"); // UNIX timestamp from 1/01/1970,  0:00:00 gmt
        }
    }
}
