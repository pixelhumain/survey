<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;
use CAction;
use Form;
use PHDB;

class NewAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id=null, $type=null , $tpl=null, $contextId=null, $contextType=null)
    {
    	$this->getController()->layout = "//layouts/empty";
        $params = array();
        if(!empty($id)){
            $params["forms"] = PHDB::find(Form::COLLECTION, 
                [ "parent.".$id => ['$exists'=>1] ], 
                [ "name" ] ) ;

        } else if( !empty($this->getController()->costum) ){
            $params["forms"]  = PHDB::find(Form::COLLECTION, 
                [ '$and' => [
                    [ '$or' =>[
                        ["active" => true],
                        ["active" => "true"]
                       ] 
                    ],    
                    ["parent.".$this->getController()->costum["contextId"] => ['$exists' => 1]
                    // array('$or' => array(
                    //         array("parent.".$this->getController()->costum["contextId"] => array('$exists' => 1) ),
                    //         array("source.keys" => array('$in' => array($this->getController()->costum["slug"]) ) )
                    //     ) )
                ]]], ["name"] ) ; 
        }
        if(!empty($contextId))
            $params["contextId"] = $contextId;

        if(!empty($contextType))
            $params["contextType"] = $contextType;

        //var_dump($this->getController()->costum); exit;
        if($tpl == null)
            $tpl= "survey.views.tpls.views.new";
            // Rest::json($params);exit;

		return $this->getController()->renderPartial($tpl,$params ); 
        
	 			
    }
 }