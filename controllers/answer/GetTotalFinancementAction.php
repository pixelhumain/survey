<?php
namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use Answer;
use Form;
use MongoId;
use PHDB;
use Yii;
use yii\helpers\Json;

class GetTotalFinancementAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($form)
    {
        $params = Form::getFormTotalFinancement($form);
        return Json::encode($params);
    }
}
