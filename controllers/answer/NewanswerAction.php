<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use CAction, Element, Answer, PHDB, MongoId, Form, Rest;
 /**
  * Display the directory of back office
  * @param String $id Not mandatory : if specify, look for the person with this Id. 
  * Else will get the id of the person logged
  * @return type
  */
class NewanswerAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run($form=null, $contextId=null, $contextType=null)
    {
      if($_POST["action"] == "newanswer"){
        $params = Element::getElementById($_POST["answerId"],Answer::COLLECTION);
        $params["seen"] = $_POST["seen"];
        PHDB::update(Answer::COLLECTION,[ "_id" => new MongoId($_POST["answerId"]) ],['$set'=>$params]);
      }elseif($_POST["action"] == "confirm"){
        $params = Element::getElementById($_POST["answerId"],Answer::COLLECTION);
        $params["confirmed"] = $_POST["confirmed"];
        PHDB::update(Answer::COLLECTION,[ "_id" => new MongoId($_POST["answerId"]) ],['$set'=>$params]);
      }elseif($_POST["action"] == "paid"){
        $params = Element::getElementById($_POST["answerId"],Answer::COLLECTION);
        $params["paid"] = $_POST["paid"];
        PHDB::update(Answer::COLLECTION,[ "_id" => new MongoId($_POST["answerId"]) ],['$set'=>$params]);
      }else{
    	$formParent =PHDB::findOneById( Form::COLLECTION, $form);
    	$context = null;
        if(!empty($contextId) && !empty($contextType)){
        	$ctxEl = PHDB::findOneById( $contextType , $contextId,["name","address"]);
        	$context = [
        		$contextId => [
        			"type" => $contextType,
        			"name" => $ctxEl["name"]
        		]
        	];
        }
		$answer = Answer::generateAnswer($formParent, false, $context);
		return Rest::json($answer);
    }
  }
}