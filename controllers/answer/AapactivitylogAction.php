<?php
namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use Person;
use Form;
use PHDB;
// use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\actor\Person;
use Yii;
use Rest;
use Project;
use RocketChat;
class AapactivitylogAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($action, $answerid)
    {
        $tab = ["progress" => "En cours", "finished" => "Terminé", "abandoned" => "Abandonée", "newaction" => "Nouvelle proposition", "call" => "Appel au participations", "projectstate" => "En Projet", "finance" => "appel au financement", "vote" => "En evaluation" , "underconstruction" => "En construction" , "prevalided" => "Pré validé", "validproject" => "Projet validé", "voted" => "Voté",];

        if (!empty($action && !empty($answerid))) {
            $answer = PHDB::findOneById(Form::ANSWER_COLLECTION, $answerid);
            if (empty($answer)){
                return Rest::json(array("result" => false, "msg" => "invalid answer"));
            }
            $formparent = PHDB::findOneById(Form::COLLECTION, $answer["form"]);
            if (isset($formparent["paramsNotification"][$action]) && ($formparent["paramsNotification"][$action] == false || $formparent["paramsNotification"][$action] == "false")) {
                return Rest::json(array("result" => false, "msg" => "notification disabled"));
            }
            $parent = [];
            $parentslug = [];
            $groupChat = [];
            $channelChat = [];
            $parenthasRC = false;

            foreach ($formparent["parent"] as $fpid => $fp){

                $pr= PHDB::findOneById($fp["type"], $fpid);

                if (isset($pr["hasRC"]) && filter_var($pr["hasRC"], FILTER_VALIDATE_BOOLEAN)){
                    if (!empty($pr["tools"]["chat"]["int"])){
                        foreach ($pr["tools"]["chat"]["int"] as $t => $ch){
                            if (str_contains($ch["url"], "channel")) {
                                array_push($channelChat, $ch["name"]);
                            }elseif (str_contains($ch["url"], "group")){
                                array_push($groupChat, $ch["name"]);
                            }
                        }
                    }else{
                        array_push($channelChat, $pr["slug"]);
                    }
                }
                array_push($parent, $pr);

            }
            $project = null;
            $projectslug = "";
            if (!empty($answer["project"]["id"])){
                $project = PHDB::findOneById(Project::COLLECTION, $answer["project"]["id"]);
                $projectslug = $project["slug"];
            }

            if(isset($_POST["url"])){
                $url = $_POST["url"];
            } else {
                $url = "";
            }

            switch ($action) {
                case "newproposition" :
                    $message = " @" . Yii::app()->session['user']["username"] . " a ajoutée une nouvelle proposition nommée : `".@$_POST["proposition"]."`";
                    foreach ($channelChat as $psid => $ps){
                        RocketChat::post($ps, $message , $url, true );
                    }
                    foreach ($groupChat as $psid => $ps){
                        RocketChat::post($ps, $message , $url , false);
                    }
                    if ($projectslug != ""){
                        RocketChat::post($projectslug, $message , $url);
                    }
                    break;
                case "deleteproposition" :
                    $message = " @" . Yii::app()->session['user']["username"] . " a supprimé la proposition nommée : `".@$_POST["proposition"]."`";
                    foreach ($channelChat as $psid => $ps){
                        RocketChat::post($ps, $message , $url, true );
                    }
                    foreach ($groupChat as $psid => $ps){
                        RocketChat::post($ps, $message , $url , false);
                    }
                    if ($projectslug != ""){
                        RocketChat::post($projectslug, $message , $url);
                    }
                    break;
                case "newstatus" :
                    $status = "";
                    if (isset($_POST["status"])){
                        foreach ($_POST["status"] as $asid => $as){
                            $status .= " ". @$tab[$as]."|";
                        }
                    }
                    $message = " @" . Yii::app()->session['user']["username"] . " a mis à jour le status de : " . @$answer["answers"]["aapStep1"]["titre"] . " en :" . $status;

                    foreach ($channelChat as $psid => $ps){
                        RocketChat::post($ps, $message , $url, true );
                    } foreach ($groupChat as $psid => $ps){
                    RocketChat::post($ps, $message , $url , false);
                }
                    if ($projectslug != ""){
                        RocketChat::post($projectslug, $message , $url);
                    }
                    break;
                case "newdepense" :

                    if (isset($_POST["pos"])) {
                        $message = " @" . Yii::app()->session['user']["username"] . " a ajoutée une nouvelle ligne de depense pour la proposition : `" . @$answer["answers"]["aapStep1"]["titre"] ." ` intitulée : `" . @$answer["answers"]["aapStep1"]["depense"][$_POST["pos"]]["poste"] . "`" ;
                    }else{
                        $message = " @" . Yii::app()->session['user']["username"] . " a ajoutée une nouvelle ligne de depense pour la proposition : `" . @$answer["answers"]["aapStep1"]["titre"] . "`";
                    }
                    foreach ($channelChat as $psid => $ps){
                        RocketChat::post($ps, $message , $url, true );
                    } foreach ($groupChat as $psid => $ps){
                    RocketChat::post($ps, $message , $url , false);
                }
                    if ($projectslug != ""){
                        RocketChat::post($projectslug, $message , $url);
                    }
                    break;
                case "deletedepense" :

                    if (isset($_POST["pos"])) {
                        $message = " @" . Yii::app()->session['user']["username"] . " a supprimé la ligne de depense intitulée : `" . @$answer["answers"]["aapStep1"]["depense"][$_POST["pos"]]["poste"] . "` sur la proposition : `" . @$answer["answers"]["aapStep1"]["titre"] ." ` " ;
                    }else{
                        $message = " @" . Yii::app()->session['user']["username"] . " a supprimé  une ligne de depense pour la proposition : `" . @$answer["answers"]["aapStep1"]["titre"] . "`";
                    }
                    foreach ($channelChat as $psid => $ps){
                        RocketChat::post($ps, $message , $url, true );
                    } foreach ($groupChat as $psid => $ps){
                    RocketChat::post($ps, $message , $url , false);
                }
                    if ($projectslug != ""){
                        RocketChat::post($projectslug, $message , $url);
                    }
                    break;
                case "newfinancement" :
                    if (isset($_POST["pos"])) {
                        if ($_POST["pos"] == ""){
                            $_POST["pos"] = 0;
                        }
                        $message = " @" . Yii::app()->session['user']["username"] . " a ajoutée une nouvelle ligne de finanancement pour la proposition : `" . @$answer["answers"]["aapStep1"]["titre"] . " ` depense : `" . @$answer["answers"]["aapStep1"]["depense"][$_POST["pos"]]["poste"] . "`";
                    }else{
                        $message = " @" . Yii::app()->session['user']["username"] . " a ajoutée de nouvelles lignes de finanancement pour la proposition " . @$answer["answers"]["aapStep1"]["titre"];
                    }

                    foreach ($channelChat as $psid => $ps){
                        RocketChat::post($ps, $message , $url, true );
                    }
                    foreach ($groupChat as $psid => $ps){
                        RocketChat::post($ps, $message , $url , false);
                    }
                    if ($projectslug != ""){
                        RocketChat::post($projectslug, $message , $url);
                    }
                    break;
                case "deletefinancement" :
                    if (isset($_POST["pos"])) {
                        if ($_POST["pos"] == ""){
                            $_POST["pos"] = 0;
                        }
                        $message = " @" . Yii::app()->session['user']["username"] . " a supprimé une ligne de finanancement pour la depense : `" . @$answer["answers"]["aapStep1"]["depense"][$_POST["pos"]]["poste"] . "` proposition : `" . @$answer["answers"]["aapStep1"]["titre"] . " ` ";
                    }else{
                        $message = " @" . Yii::app()->session['user']["username"] . "  a supprimé une ligne de finanancement pour la proposition " . @$answer["answers"]["aapStep1"]["titre"];
                    }

                    foreach ($channelChat as $psid => $ps){
                        RocketChat::post($ps, $message , $url, true );
                    }
                    foreach ($groupChat as $psid => $ps){
                        RocketChat::post($ps, $message , $url , false);
                    }
                    if ($projectslug != ""){
                        RocketChat::post($projectslug, $message , $url);
                    }
                    break;
                case "newpayement" :
                    if (isset($_POST["pos"])) {
                        if( $_POST["pos"] == ""){
                            $_POST["pos"] = 0;
                        }
                        $message = " @" . Yii::app()->session['user']["username"] . " a ajoutée une nouvelle ligne de payement pour la proposition  : `" . @$answer["answers"]["aapStep1"]["titre"] . " ` depense : `" . @$answer["answers"]["aapStep1"]["depense"][$_POST["pos"]]["poste"] . "`";
                    }else{
                        $message = " @" . Yii::app()->session['user']["username"] . " a ajoutée de nouvelles lignes de payement pour la proposition " . @$answer["answers"]["aapStep1"]["titre"];
                    }

                    foreach ($channelChat as $psid => $ps){
                        RocketChat::post($ps, $message , $url, true );
                    } foreach ($groupChat as $psid => $ps){
                    RocketChat::post($ps, $message , $url , false);
                }
                    if ($projectslug != ""){
                        RocketChat::post($projectslug, $message , $url);
                    }
                    break;
                case "deletepayement" :
                    if (isset($_POST["pos"])) {
                        if( $_POST["pos"] == ""){
                            $_POST["pos"] = 0;
                        }
                        $message = " @" . Yii::app()->session['user']["username"] . " a supprimé une ligne de payement du depense : `" . @$answer["answers"]["aapStep1"]["depense"][$_POST["pos"]]["poste"] . "` proposition  : `" . @$answer["answers"]["aapStep1"]["titre"] . " ` ";
                    }else{
                        $message = " @" . Yii::app()->session['user']["username"] . " a supprimé une ligne de payement pour la proposition " . @$answer["answers"]["aapStep1"]["titre"];
                    }

                    foreach ($channelChat as $psid => $ps){
                        RocketChat::post($ps, $message , $url, true );
                    } foreach ($groupChat as $psid => $ps){
                    RocketChat::post($ps, $message , $url , false);
                }
                    if ($projectslug != ""){
                        RocketChat::post($projectslug, $message , $url);
                    }
                    break;
                case "newtask":
                    if (isset($_POST["pos"]) && $_POST["pos"] == ""){
                        $_POST["pos"] = 0;
                    }
                    if (isset($_POST["pos"]) && isset($_POST["task"])) {
                        $message = " @" . Yii::app()->session['user']["username"] . " a ajoutée une nouvelle tache `". $_POST["task"]. "` pour la proposition  : `" . @$answer["answers"]["aapStep1"]["titre"] . " ` depense : `" . @$answer["answers"]["aapStep1"]["depense"][$_POST["pos"]]["poste"] . "`";
                    }else{
                        $message = " @" . Yii::app()->session['user']["username"] . " a ajoutée une nouvelle tache sur la depense : " . @$answer["answers"]["aapStep1"]["titre"];
                    }

                    foreach ($channelChat as $psid => $ps){
                        RocketChat::post($ps, $message , $url, true );
                    }
                    foreach ($groupChat as $psid => $ps){
                        RocketChat::post($ps, $message , $url , false);
                    }
                    if ($projectslug != ""){
                        RocketChat::post($projectslug, $message , $url);
                    }
                    break;
                case "deletetask":
                    if (isset($_POST["pos"]) && $_POST["pos"] == ""){
                        $_POST["pos"] = 0;
                    }
                    if (isset($_POST["pos"]) && isset($_POST["task"])) {
                        $message = " @" . Yii::app()->session['user']["username"] . " a supprimé la tache `". $_POST["task"]. "` pour la proposition  : `" . @$answer["answers"]["aapStep1"]["titre"] . " ` depense : `" . @$answer["answers"]["aapStep1"]["depense"][$_POST["pos"]]["poste"] . "`";
                    }else{
                        $message = " @" . Yii::app()->session['user']["username"] . " a supprimé une tache sur la depense : " . @$answer["answers"]["aapStep1"]["titre"];
                    }

                    foreach ($channelChat as $psid => $ps){
                        RocketChat::post($ps, $message , $url, true );
                    }
                    foreach ($groupChat as $psid => $ps){
                        RocketChat::post($ps, $message , $url , false);
                    }
                    if ($projectslug != ""){
                        RocketChat::post($projectslug, $message , $url);
                    }
                    break;
                case "checktask" :
                    if (isset($_POST["actname"])) {
                        $message = " @" . Yii::app()->session['user']["username"] . " a completé la tache `". $_POST["task"]. "` pour la proposition  : `" . @$answer["answers"]["aapStep1"]["titre"] . " ` depense : `" . @$_POST["actname"] . "`";
                    }else{
                        $message = " @" . Yii::app()->session['user']["username"] . " a ajoutée une nouvelle proposition nommée : " . @$answer["answers"]["aapStep1"]["titre"];
                    }

                    foreach ($channelChat as $psid => $ps){
                        RocketChat::post($ps, $message , $url, true );
                    }
                    foreach ($groupChat as $psid => $ps){
                        RocketChat::post($ps, $message , $url , false);
                    }
                    if ($projectslug != ""){
                        RocketChat::post($projectslug, $message, $url);
                    }
                    break;
                case "addperson" :
                    if (!empty($_POST["pers"])){
                        $message = "";
                        foreach ($_POST["pers"] as $persid => $pers){
                            $presinfo = PHDB::findOneById(Person::COLLECTION, $persid);
                            $message .= "L'utilisateur @".@$presinfo["username"]."a été ajouté au proposition `".@$answer["answers"]["aapStep1"]["titre"]."`".PHP_EOL." ";
                        }
                    }
                    foreach ($channelChat as $psid => $ps){
                        RocketChat::post($ps, $message , $url, true );
                    }
                    foreach ($groupChat as $psid => $ps){
                        RocketChat::post($ps, $message , $url , false);
                    }
                    if ($projectslug != ""){
                        RocketChat::post($projectslug, $message , $url);
                    }
                    break;
                case "rmperson" :
                    if (!empty($_POST["pers"])){
                        $message = "";
                        foreach ($_POST["pers"] as $persid => $pers){
                            $presinfo = PHDB::findOneById(Person::COLLECTION, $persid);
                            $message .= "L'utilisateur @".@$presinfo["username"]."a été enlevée du proposition `".@$answer["answers"]["aapStep1"]["titre"]."`";
                        }
                    }
                    foreach ($channelChat as $psid => $ps){
                        RocketChat::post($ps, $message , $url, true );
                    }
                    foreach ($groupChat as $psid => $ps){
                        RocketChat::post($ps, $message , $url , false);
                    }
                    if ($projectslug != ""){
                        RocketChat::post($projectslug, $message , $url);
                    }
                    break;
            }
            return Rest::json(array("result" => true, "msg" => $message));
        }
    }
}
