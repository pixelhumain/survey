<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use CAction, PHDB, Form, Element, Yii, countresponse, countquestion, cumulreponse, moyennereponse, sorting, crossing;
 /**
  * Display the directory of back office
  * @param String $id Not mandatory : if specify, look for the person with this Id. 
  * Else will get the id of the person logged
  * @return type
  */
class GraphbuilderAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run($id=null, $answer=null, $mode=null, $form=null, $title = "", $contextId = null, $contextType = null)
    {

          $adminRight = false;
          $index = [];
          $type = [];
          $title = [];
          $height = [];
          $allgraph = [];
          $allanswers = [];
          $render = [];
          $countForm = 0;
          $tittleList = [];

          $titletype = [
              "text",
              "select",
              "tpls.forms.cplx.multitextvalidation",
              "tpls.forms.cplx.multiCheckboxPlus",
              "tpls.forms.cplx.multiRadio"
          ];

          $controller = $this->getController();
          $myanswer = PHDB::find( Form::ANSWER_COLLECTION, array("form"=>$answer));
          $prntForm = PHDB::findOneById(Form::COLLECTION, $answer );

          foreach ($prntForm["parent"] as $ip => $vp) {
            $communityLinks = Element::getCommunityByTypeAndId($vp["type"],$ip);
            foreach ($communityLinks as $cmid => $cmvalue) {
                if ($cmid == Yii::app()->session["userId"] && $cmvalue["isAdmin"] == true) {
                  $adminRight = true;
                }
            }
          }

          //if (isset(Yii::app()->session["userId"]) && Yii::app()->session["userId"] == $prntForm[""] ) {
          //   # code...
          // }

          function getvalue($myanswer, $value){
              $allvalue = [];
              foreach ($myanswer as $n => $m) {
                  if (isset($m[$value])) {
                      array_push($allvalue, $m[$value]);
                  }
              }

              return $allvalue;
          }

          function countresponse($allanswers){
              $count = 0;

              foreach ($allanswers as $key => $value) {
                  $count += sizeof($value);
              }

              return $count ; 
          }

          function countquestion($allanswers, $question){
              $count = 0;

              foreach ($allanswers as $key => $value) {
                  // var_dump($value);
                  if (is_array($value)) {
                    foreach ($value as $k => $v) {
                      if(strpos($k , $question) !== false){
                            $count ++;
                          }
                      }
                  }
              }

              return $count ; 
          }

          function cumulreponse($allanswers, $question){
              $count = 0;

              foreach ($allanswers as $key => $value) {
                  // var_dump($value);
                  foreach ($value as $k => $v) {
                      if(strpos($k , $question) !== false){
                          $count += $v;
                      }
                  }
              }

              return $count ; 
          }

          function moyennereponse($allanswers, $question){
              $count = 0;
              $number = 0;

              foreach ($allanswers as $key => $value) {
                  // var_dump($value);
                  foreach ($value as $k => $v) {
                      if(strpos($k , $question) !== false){
                          $count += $v;
                          $number++;
                      }
                  }
              }
              if ($count == 0) {
                return 0;
              } else {
                   return round($count/$number,2) ; 
              }
             
          }

          function sorting($allanswers, $subtype, $question){
              $data = [ "label" => [], "dataset" => [], "type" => $subtype ];

              foreach ($allanswers as $key => $value) {
                  foreach ($value as $k => $v) {
                      if(strpos($k , $question) !== false){

                          if(is_array($v)){
                              if (!in_array($v["value"], $data["label"])) {
                                  array_push($data["label"], $v["value"]);
                                  array_push($data["dataset"], 1);
                              } else {
                                  $range = array_search($v["value"], $data["label"]);
                                  $data["dataset"][$range] ++;
                              }
                          } else {
                              if (!in_array($v, $data["label"])) {
                                  array_push($data["label"], $v);
                                  array_push($data["dataset"], 1);
                              } else {
                                  $range = array_search($v, $data["label"]);
                                  $data["dataset"][$range] ++;
                              }
                          }
                      }
                  }
              }

              return $data;
          }

          function crossing($allanswers , $subtype, $variables, $func){
              $data = [ "label" => [], "labels" => [], "datasets" => [], "type" => $subtype ];

              foreach ($func["datasets"] as $key => $value) {
                  ${$value."label"} = [];
                  ${$value."data"} = [];
              }

               foreach ($allanswers as $key => $value) {
                  foreach ($value as $k => $v) {
                      if(strpos($k , $variables["label"]) !== false){
                          $variables["label"] = $k;
                      }
                      if(strpos($k , $variables["var1"]) !== false){
                          $variables["var1"] = $k;
                      }
                  }
              }

              foreach ($allanswers as $key => $value) {
                  foreach ($value as $k => $v) {
                      if(strpos($k , $variables["label"]) !== false){

                          if(is_array($v)){
                              if (!in_array($v["value"], $data["label"])) {
                                  array_push($data["label"], $v["value"]);
                              } 
                          } else {
                              if (!in_array($v, $data["label"])) {
                                  array_push($data["label"], $v);
                              }
                          }

                      }
                  }
              }


              foreach ($allanswers as $key => $value) {
                  foreach ($value as $k => $v) {
                      if(strpos($k , $variables["var1"]) !== false){

                          if(is_array($v)){
                              if (!in_array($v["value"], $data["labels"])) {
                                  array_push($data["labels"], $v["value"]);
                              } 
                          } else {
                              if (!in_array($v, $data["labels"])) {
                                  array_push($data["labels"], $v);
                              }
                          }

                      }
                  }
              }

              foreach ($data["labels"] as $mm => $nn) {
                  ${"datafor".$nn} = [];
                  foreach ($data["label"] as $d => $f) {
                      array_push(${"datafor".$nn}, 0);
                  }
              }

              foreach ($data["label"] as $m => $n) {
                  foreach ($allanswers as $key => $value) {
                      if (isset($value[$variables["label"]])) {
                          if(is_array($value[$variables["label"]])){
                              
                              if($value[$variables["label"]]["value"] == $n){
                                  foreach ($data["labels"] as $w => $x) {
                                      if(is_array($value[$variables["var1"]])){
                                          if($value[$variables["var1"]]["value"] == $x){
                                              $range = array_search($n, $data["label"]);
                                              ${"datafor".$x}[$range]++;
                                          }
                                      } else {
                                          if($value[$variables["var1"]] == $x){
                                              $range = array_search($n, $data["label"]);
                                              ${"datafor".$x}[$range]++;
                                          }
                                      }
                                  }
                              }

                          } else {

                              if($value[$variables["label"]] == $n){
                                  foreach ($data["labels"] as $w => $x) {
                                      if(is_array($value[$variables["var1"]])){
                                          if($value[$variables["var1"]]["value"] == $x){
                                              $range = array_search($n, $data["label"]);
                                              ${"datafor".$x}[$range]++;
                                          }
                                      } else {
                                          if($value[$variables["var1"]] == $x){
                                              $range = array_search($n, $data["label"]);
                                              ${"datafor".$x}[$range]++;
                                          }
                                      }
                                  }
                              }

                          }
                      }
                  }
              }

              
              foreach ($data["labels"] as $mf => $nf) {
                  array_push($data["datasets"],  ${"datafor".$nf});
              }
                  

              return $data;
          }

          if (!empty($prntForm["subForms"])) {
            for ( $i = 0; $i < sizeof($prntForm["subForms"]); $i++){
                

                ${"childForm".$i} = PHDB::find( Form::COLLECTION, array("id"=>$prntForm["subForms"][$i]));


                //array_push($sectionName, $prntForm["subForms"][$i]);
                
                // array_push($sectionId, $prntForm["subForms"][$i]);
                //if(isset(var))
                //array_push($sectionName, ${"childForm".$i}["name"]);
               // var_dump(${"childForm".$i});
                foreach (${"childForm".$i} as $key => $value) {
                  if(isset($value["inputs"])){
                    $countForm += sizeof($value["inputs"]);
                    
                    foreach ($value["inputs"] as $inpId => $inp) {
                        if ( in_array($inp["type"], $titletype
                          )) {
                            array_push($tittleList, [
                                "sectionId" => $prntForm["subForms"][$i],
                                "sectionName" => $value["name"],
                                "title" => $inp["label"],
                                "id" => $inpId
                            ]);
                        }
                    }

                  }
                }
            }
          } else {

          }

          foreach ($myanswer as $idans => $ans) {
              if(isset($ans["answers"])){
                  foreach ($ans["answers"] as $name => $content) {
                      array_push($allanswers, $content);
                  }
              }
          }

          //var_dump($allanswers);exit();

          if (isset($prntForm["graph"])) {
            foreach ($prntForm["graph"] as $key => $value) {
                 array_push($index, preg_replace('/\s\s+/', ' ', $key));
                 array_push($type, $value["type"]);
                 array_push($title, $value["label"]);

                 if (isset($value["height"])) {
                    array_push($height, $value["height"]);
                 } else {
                    array_push($height, "");
                 }

                 $canRender = true;
                 if (isset($value["render"])) {
                      
                      if ($value["type"] == "count") {
                          if($value["render"]["subtype"] == "totalanswer"){
                              array_push($render, countresponse($allanswers));
                          }

                          elseif($value["render"]["subtype"] == "totalquestion"){
                              if (isset($value["render"]["variables"]) and isset($value["render"]["variables"]["question"])) {
                                  array_push($render, countquestion($allanswers, $value["render"]["variables"]["question"]));
                              } else {
                                  $canRender = false;
                              }
                          }   
                      }

                      elseif ($value["type"] == "cumul") {
                          if($value["render"]["subtype"] == "cumulreponse"){
                              array_push($render, cumulreponse($allanswers, $value["render"]["variables"]["question"]));
                          } 

                          elseif($value["render"]["subtype"] == "moyenne"){
                              if (isset($value["render"]["variables"]) and isset($value["render"]["variables"]["question"])) {
                                  array_push($render, moyennereponse($allanswers, $value["render"]["variables"]["question"]));
                              } else {
                                  $canRender = false;
                              }
                          }
                      }

                      elseif ($value["type"] == "sorting") {
                          if(isset($value["render"]["subtype"])){
                              array_push($render, sorting($allanswers, $value["render"]["subtype"], $value["render"]["variables"]["question"]));
                          } else {
                                  $canRender = false;
                              }
                      }

                      elseif ($value["type"] == "crossing") {
                          if(isset($value["render"]["subtype"])){
                              array_push($render, crossing($allanswers, $value["render"]["subtype"], $value["render"]["variables"], $value["render"]["func"]));
                          } else {
                                  $canRender = false;
                              }
                      }

                      // elseif(isset($value["function"]["variables"]) and $value["function"]["variables"] != "")
                      // {

                          // else if ($value["type"] == "line" or $value["type"] == "bar" or $value["type"] == "pie") {
                          //     if (isset($value["function"]["data"]) and $value["function"]["data"] != "") {
                          //           /////// Transformation ////////////
                          //           // foreach ($value["function"]["variables"] as $varname => $varcontent) {
                                        
                          //           //     ${$varname."user"} = [];
                                        
                          //           //     foreach ($varcontent as $functype => $typevalue) {
                          //           //         var_dump($typevalue);
                          //           //         if ($functype == "valueof") {
                          //           //             ${$varname."user"} = getvalue($allanswers, $typevalue);
                          //           //         }
                          //           //     }

                          //           // }

                          //           // foreach ($value["function"]["data"] as $elId => $el) {

                          //           //     if($el["func"] == "none"){
                          //           //         if(isset(${$el["var"][0]."user"})){
                          //           //             array_push($allgraph, ${$el["var"][0]."user"});
                          //           //         }
                          //           //     }

                          //           //     if($el["func"] == "group"){
                          //           //         if(isset(${$el["var"][0]."user"}) and isset(${$el["var"][1]."user"})){
                          //           //               var_dump(${$el["var"][0]."user"});
                          //           //               array_push($allgraph, ${$el["var"][0]."user"});
                          //           //         }
                          //           //     }
                          //           // }


                                  
                          //     } else {
                          //         $canRender = false;
                          //     }
                          // } 

                          


                      // } else {
                      //     $canRender = false;
                      // }

                 } else {
                    $canRender = false;
                 }

                 if(!$canRender){
                    array_push($render, null);
                 }
            }
          }

          $allfields = [
              "parentFormId" => $answer,
              "name" => $prntForm["name"],
              "index" => $index,
              "type" => $type,
              "title" => $title,
              "height" => $height,
              "render" => $render,
              "titleparams" => $tittleList
          ];          
    			
          $params = [
    				"allanswers" => $allfields,
            "adminRight" => $adminRight,
            "contextId" => $contextId,
            "contextType" => $contextType
    			];
	       
          $tpl= "survey.views.tpls.answers.graphbuilder";
	       return $controller->renderPartial($tpl,$params,true);
    }
}
