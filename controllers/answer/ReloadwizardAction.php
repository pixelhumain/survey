<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use CAction, PHDB, Form;
class ReloadwizardAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {

        $forms = $_POST["forms"];
        $el = @$_POST["el"];
        $active = $_POST["active"];
        $color1 = $_POST["color1"];
        $color2 = $_POST["color2"];
        $canEdit = filter_var($_POST["canEdit"], FILTER_VALIDATE_BOOLEAN) ;
        $canEditForm = filter_var($_POST["canEditForm"], FILTER_VALIDATE_BOOLEAN) ;
        $canAdminAnswer = filter_var($_POST["canAdminAnswer"], FILTER_VALIDATE_BOOLEAN) ;
        $canAnswer = filter_var(@$_POST["canAnswer"], FILTER_VALIDATE_BOOLEAN) ;
        $showForm = $_POST["showForm"];
        $mode = $_POST["mode"];
        $showWizard = $_POST["showWizard"];
        $wizid = $_POST["wizid"];

        if (isset($_POST["contextId"])) {
            $contextId = $_POST["contextId"];
        }else {
            $contextId = null;
        }
        if (isset($_POST["contextType"])) {
            $contextType = $_POST["contextType"];
        }else {
            $contextType = null;
        }

        $answer = PHDB::findOneById(Form::ANSWER_COLLECTION, $_POST["answerId"] );

        $parentForm = PHDB::findOneById(Form::COLLECTION, $answer["form"] );

        foreach ($parentForm["parent"] as $elkey => $elvalue) {
            $el = PHDB::findOneById($elvalue["type"], $elkey );
        }

        $form = PHDB::findOneById(Form::COLLECTION, $_POST["formId"] );

        $forms = [];
        if (isset($parentForm["subForms"])) {
            $arr_value = [];
            foreach ($parentForm["subForms"] as $key => $value) {
                $arr_value[] = $value;
               /* $shift = PHDB::find(Form::COLLECTION, array("id" => $value));
                //var_dump($shift);exit;
                $forms[$value] = array_shift($shift);*/
            }
            $arr_values = PHDB::find(Form::COLLECTION, array("id" => ['$in' => $arr_value]));
            foreach ($arr_values as $key => $value) {
                $shift = $arr_values[$key];
                $forms[$value["id"]] =  $arr_values[$key];
            }
        }
            
        if( isset($answer) && !empty($showForm) && $showForm == true  ) {
            $params = [
                "parentForm"=>$parentForm,
                "form" => $form, 
                "forms"=>$forms,
                "el" => $el,
                "active" => "all",
                "color1" => $color1,
                "color2" => $color2,
                "canEdit" => $canEdit,
                "canEditForm" => $canEditForm,
                "canAdminAnswer" => $canAdminAnswer,
                "answer"=>$answer,
                "showForm" => $showForm,
                "mode" => $mode,
                "showWizard"=>true,
                "wizid"=> $wizid,
                "contextId" => @$contextId,
                "contextType" => @$contextType
            ];

            if (isset($parentForm["tplstepWizard"]) && isset($_POST["standalone"]) && filter_var( $_POST["standalone"] , FILTER_VALIDATE_BOOLEAN)){
                $tplstepWizard = $parentForm["tplstepWizard"];
            }else{
                $tplstepWizard = "survey.views.tpls.forms.wizard";
            }

            $controller = $this->getController();
            return $controller->renderPartial($tplstepWizard , $params);
        }
    }
}