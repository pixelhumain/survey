<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use Authorisation;
use PHDB,Form,Rest,CTKException,Yii;
class RenamekeyAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        if(Form::canAdmin($_POST["formId"])){
            try {
                PHDB::updateWithOptions( 
                    "answers",
                    array($_POST["path"] => ['$exists' => true],"form" => $_POST["formId"]),
                    array('$rename' => [ $_POST["path"] => $_POST["value"]] ), 
                    array('multi' => true) ,
                );
                return Rest::json(array("result"=>true, "msg"=> Yii::t("common","Everything is allRight"), $_POST["path"]=>$_POST["value"]));
            } catch (CTKException $e) {
                return Rest::json(array("result"=>false, "msg"=>$e->getMessage(), $_POST["path"]=>$_POST["value"]));
            }
        } 
    }
}