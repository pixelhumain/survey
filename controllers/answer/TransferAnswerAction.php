<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use Authorisation;
use CAction, PHDB, Form, Actions, mongoId, Yii;
class TransferAnswerAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id=null)
    {
        $tpl = "survey.views.tpls.answers.transferanswer";
        $params = [
            "answerid" => $id
        ];

        $controller = $this->getController();

        $answer = PHDB::findOneById(Form::ANSWER_COLLECTION, $id);
        $formparent = PHDB::findOneById(Form::COLLECTION, $answer["form"]);
        if (!empty($formparent["config"])){
            $formconfig = PHDB::findOneById(Form::COLLECTION, $formparent["config"]);
            $parentid = array_keys($formparent["parent"])[0];
            if (!empty($formconfig)){
               $formaap = PHDB::find(Form::COLLECTION, array( "config" => $formparent["config"] , "type" => "aap"));
               $params["formaap"] = [];
               foreach ($formaap as $fid => $faap){
                   if (Authorisation::isParentAdmin($fid , Form::COLLECTION , Yii::app()->session["userId"])){
                       $params["formaap"][$fid] = $faap;
                   }
               }
            }
        }

        if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial($tpl , $params,true);
        else {
            $this->getController()->layout = "//layouts/empty";
            return $this->getController()->render($tpl , $params);
        }
    }
}
