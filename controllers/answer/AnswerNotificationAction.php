<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use ActStr;
use Answer;
use Authorisation;
use DateTimeImmutable;
use Element;
use CAction;
use Form;
use MongoId;
use Notification;
use PHDB;
use HtmlHelper;
use Yii;
use RocketChat;
use Rest;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;

class AnswerNotificationAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $this->getController()->layout = "//layouts/empty";

        // Send RC notification

        if (isset($_POST["url"])) {
            $id = $_POST["id"];
            $url = $_POST["url"];
            $parentForm = $_POST["parentForm"];
        } else {
            $url = "";
        }

        $message = " @" . Yii::app()->session['user']["username"] . " a répondu au formulaire : `" . @$parentForm["name"] . "`";

        //send notification
        Notification::constructNotification(
        //verb
            ActStr::VERB_ADD,
            //author
            [
                "id" => Yii::app()->session["userId"],
                "name" => Yii::app()->session["user"]["name"]
            ],
            //target
            [
                "id" => array_keys($parentForm["parent"])[0],
                "type" => $parentForm["parent"][array_keys($parentForm["parent"])[0]]["type"]
            ],
            //object
            [
                "id" => array_keys($parentForm["parent"])[0],
                "type" => Form::COLLECTION
            ],
            //levelType
            Form::COLLECTION,
            //context
            NULL,
            //value
            [
                "id" => $id,
                "type" => Answer::COLLECTION
            ]
        );

    }


}
