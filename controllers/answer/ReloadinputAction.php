<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;

use CAction, PHDB, Form, MongoId, Authorisation, Yii;
class ReloadinputAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {

        $answerId = $_POST["answerId"];
        $key = $_POST["key"];
        $formId = $_POST["formId"];
        $wizard = $_POST["wizard"];
        $mode = $_POST["mode"];
        $canEdit = filter_var($_POST["canEdit"], FILTER_VALIDATE_BOOLEAN) ;
        $canEditForm = filter_var($_POST["canEditForm"], FILTER_VALIDATE_BOOLEAN) ;
        $canAdminAnswer = filter_var($_POST["canAdminAnswer"], FILTER_VALIDATE_BOOLEAN) ;
        $canAnswer = filter_var($_POST["canAnswer"], FILTER_VALIDATE_BOOLEAN) ;
        $saveOneByOne = filter_var($_POST["saveOneByOne"], FILTER_VALIDATE_BOOLEAN) ;
        if (isset($_POST["contextId"])) {
            $contextId = $_POST["contextId"];
        }else {
            $contextId = null;
        }
        if (isset($_POST["contextType"])) {
            $contextType = $_POST["contextType"];
        }else {
            $contextType = null;
        }

        $editQuestionBtn = $_POST["editQuestionBtn"];
        $titleColor = $_POST["titleColor"];

        $controller = $this->getController();

        $answer = PHDB::findOneById(Form::ANSWER_COLLECTION, $answerId );

        $parentForm = PHDB::findOneById(Form::COLLECTION, $answer["form"] );

        foreach ($parentForm["parent"] as $elkey => $elvalue) {
            $el = PHDB::findOneById($elvalue["type"], $elkey );
        }

        if (!empty($formId) && strlen($formId) == 24 && MongoId::isValid($formId)){
            $form = PHDB::findOneById(Form::COLLECTION, $formId );
        }else if(!empty($formId)){
            $form = PHDB::findOne(Form::COLLECTION, array('id' => $formId));
        }else{
            $form = [];
        }

        $answers = [];

        if(isset($parentForm["type"]) && ($parentForm["type"] == "aapConfig" || $parentForm["type"] == "aap")){
            if( $parentForm["type"] == "aap"){
                $configEl = PHDB::findOneById(Form::COLLECTION, $parentForm["config"]);
            }elseif ( $parentForm["type"] == "aapConfig"){
                $configEl = $parentForm;
            }

            if (!empty($configEl["subForms"][$formId]["inputs"])){
                $inputsObl = PHDB::findOneById(Form::INPUTS_COLLECTION, $configEl["subForms"][$formId]["inputs"]);
                $form["inputs"] = $inputsObl["inputs"];
                foreach ($form["inputs"] as $idInputs => $valInputs){
                    $form["inputs"][$idInputs]["canAdmin"] = Authorisation::isParentAdmin((string)$configEl["_id"], Form::COLLECTION , Yii::app()->session["userId"]);
                    $form["inputs"][$idInputs]["parentid"] = (string)$inputsObl["_id"];
                }
            }

            if ($parentForm["type"] == "aap"){
                $inputsapp = PHDB::findOne(Form::INPUTS_COLLECTION, array("formParent" => (string)$parentForm["_id"] , "step" => $formId));

                if (!empty($inputsapp["inputs"])) {
                    foreach ($inputsapp["inputs"] as $idInputs => $valInputs) {
                        $inputsapp["inputs"][$idInputs]["canAdmin"] = Authorisation::isParentAdmin((string)$parentForm["_id"], Form::COLLECTION, Yii::app()->session["userId"]);
                        $inputsapp["inputs"][$idInputs]["parentid"] = (string)$inputsapp["_id"];
                    }
                }

                if (!empty($form["inputs"]) && !is_string($form["inputs"]) && !empty($inputsapp["inputs"])){
                    $form["inputs"] = array_merge($form["inputs"] , $inputsapp["inputs"]);
                } elseif (!empty($inputsapp["inputs"]) ){
                    $form["inputs"] = $inputsapp["inputs"];
                }

                if(isset($form["inputs"][$key])) {
                    $input = $form["inputs"][$key];
                }
                $form["id"] = $formId;
            }

        }else {
            $input = $form["inputs"][$key];
        }
        $answerPath = "answers.".$key.".";

        $kunikT = explode( ".", $input["type"]);
        $keyTpl = ( count($kunikT)>1 ) ? $kunikT[ count($kunikT)-1 ] : $input["type"];
        $kunik = $keyTpl.$key;

        if(isset($wizard))
        {
            if($wizard){
                $answerPath = "answers.".$form["id"].".".$key.".";
                if( isset($answer["answers"][$form["id"]][$key]) && count($answer["answers"][$form["id"]][$key])>0 )
                    $answers = $answer["answers"][$form["id"]][$key];
            }
            else {
                if(isset($answer["answers"][$key]))
                    $answers = $answer["answers"][$key];
            }
        }

        $tpl = $input["type"];
        if(in_array($tpl, ["textarea","markdown","wysiwyg"])){
            $tpl = "tpls.forms.textarea";
        }
        else if(empty($tpl) || in_array($tpl, ["text","button","color","date","datetime-local","email","image","month","number","radio","range","tel","time","url","week","tags","hidden"])){
            $tpl = "tpls.forms.text";
        }
        else if(in_array($tpl, ["sectionTitle"])){
            $tpl = "tpls.forms.".$tpl;
        }

        $p = [
            "el" => $el,
            "parentForm"=> $parentForm,
            "form"      => $form,
            "key"       => $key,
            "keyTpl"       => $keyTpl,
            "kunik"     => $kunik,
            "input"     => $input,
            "type"      => $input["type"],
            "answerPath"=> $answerPath,
            "answer"    => $answer,
            "answers"   => $answers,//sub answers for this input
            "label"     => $input["label"] ,//$ct." - ".$input["label"] ,
            "titleColor"=> $titleColor,
            "info"      => isset($input["info"]) ? $input["info"] : "" ,
            "placeholder" => isset($input["placeholder"]) ? $input["placeholder"] : "" ,
            "mode"      => $mode,
            "canEdit"   => $canEdit,
            "canEditForm" => @$canEditForm,
            "canAdminAnswer" => $canAdminAnswer,
            "canAnswer" => $canAnswer,
            "editQuestionBtn" => @$editQuestionBtn,
            "usersAnswered" => @$usersAnswered,
            "saveOneByOne" => $saveOneByOne,
            "wizard"    => (isset($wizard) && $wizard == true),
            "tpl" => $tpl,
            "contextId" => $contextId,
            "contextType" => $contextType,
            "formId" => $formId
              ];

        if( stripos( $tpl , "tpls.forms." ) !== false )
            $tplT = explode(".", $input["type"]);

        if(isset($tplT[2]) && $tplT[2] == "select" && isset($input["options"]))
            $p["options"] = $input["options"];
        return '<div class="col-md-12 col-sm-12 col-xs-12">'. $controller->renderPartial( "survey.views.".$tpl , $p ) . '</div>';
    }
}
