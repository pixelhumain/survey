<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\answer;
use CAction;
use Form;
use MongoId;
use PHDB;
use Yii;

class AapEvaluateAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($parentformid=null)
    {
        $controller = $this->getController();     
        $allAnswers = PHDB::find(Form::ANSWER_COLLECTION, array("form" => (string)$parentformid,"answers.aapStep1.titre" => ['$exists'=>true]));
        $form = Form::getBySourceAndId(null,$parentformid);
        $formConfig =  PHDB::findOne( Form::COLLECTION , array("_id" => new MongoId(@$form["form"]["config"])));
        $params = [
            "allAnswers" => $allAnswers,
            "parentFormId" => $parentformid,
            "form" => $form,
            "formConfig" => $formConfig
        ];
        if(isset($_POST["sortBycriteriaIndex"]))
            $params["sortBycriteriaIndex"] = $_POST["sortBycriteriaIndex"];
        if(isset($_POST["order"]))
            $params["order"] = $_POST["order"];
        if(isset($_POST["slug"]))
            $params["slug"] = $_POST["slug"];
        if(isset($_POST["el_slug"]))
            $params["el_slug"] = $_POST["el_slug"];

        if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial("survey.views.tpls.answers.aapEvaluate", $params, true);
        /*else {
            $this->getController()->layout = "//layouts/empty";
            $this->getController()->render($tpl,$params);
        }*/
    }
}
        