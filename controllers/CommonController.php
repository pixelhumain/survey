<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers;

use CommunecterController;

class CommonController extends CommunecterController {
    public function beforeAction($action) {
        parent::initPage();
		return parent::beforeAction($action);
  	}

    public function actions() {
        return [
            "publicanswer" => \PixelHumain\PixelHumain\modules\survey\controllers\answer\PublicAnswerAction::class,
        ];
    }
}