<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers;

use CommunecterController;

/**
 * ApiController.php
 *
 * azotlive application
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 18/07/2014
 */
class ApiController extends CommunecterController {

  public function beforeAction($action)
  {
	  return parent::beforeAction($action);
  }

  public function actions() {
      return array(
      	'form'  => 'survey.controllers.actions.form.GetAction',
      	'answers'  => 'survey.controllers.actions.answer.GetAction',
      );
  }

}