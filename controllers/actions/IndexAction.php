<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\actions;
use CAction;
use Form;
use PHDB;
use Rest;
use Slug;
use Yii;

class IndexAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id=null,$session=null,$pId=null,$pType=null,$copy=null,$view=null)
    {
    	$this->getController()->layout = "//layouts/empty";
    	if( @$id )
    	{
	 		if( @$form = PHDB::findOne( Form::COLLECTION , array("id"=>$id) ) )
	 		{

	 			$this->getController()->pageTitle = @$form["seo"]["title"];
				$this->getController()->keywords = @$form["seo"]["keywords"];
				
				$form["t"] = time();
	 			//pour etre sur qu'on passe par le process dans CO pour enregistrer on decodera le hash dans l'autre sens 
	 			$form["h"] = hash('sha256', $form["t"].Yii::app()->params["idOpenAgenda"] );
	 			
	 			//scenarios are sometimes copies set as db.forms.id.xxx.scenario
	 			//so we overload with the parent scenario map 
	 			$formSrc = null;
	 			if( is_string($form["scenario"]) && stripos( $form["scenario"] , "db.") !== false){
	 				$pathT = explode(".",$form["scenario"]); 
	    			$formSrc = PHDB::findOne( $pathT[1] , array( $pathT[2] => $pathT[3] ) );
	    			$form["scenario"] = $formSrc[$pathT[4]];
	    			
	    			if(isset($formSrc[ "description" ]))
	    				$form["description"] = $formSrc["description"];

	    			if(isset($formSrc[ "generateStepCardsTpl" ])){
	    				$form["generateStepCardsTpl"] = $formSrc[ "generateStepCardsTpl" ];
	    				$form["description"] = $this->getController()->renderPartial( $formSrc[ "generateStepCardsTpl" ],null,true);
	    			}
	    			
	 			}

	 			if($view){
	 				return Rest::json(array('form' =>$form, "formSrc"=>$formSrc ));
	 				exit;
	 			}

	 			$answers = array();
	 			if ( @$session){
	 				$answers[$session] = PHDB::find( Form::ANSWER_COLLECTION , 
	 												array("formId"=>$id,
	 													  "session"=>$session,
	 													  "user"=> @Yii::app()->session["userId"] ) );
	 			} else {
	 				//si pas de session fourni on liste toute les 
	 				if(@$form["session"]){
			 			foreach ($form["session"] as $s => $sv) {
				 			$answers[$s] = PHDB::find( Form::ANSWER_COLLECTION , 
				 							array("formId"=>$id,
				 								  "session"=>(string)$s,
				 								  "user"=> @Yii::app()->session["userId"] ) );
				 		}
				 	} 
				 	//si on est sur un form child du scenario
				 	//on a forcement la session qui est transmise donc on est dans le if 
			 	}

			 	// gérer les dates de la session
	 			$startDate = null;
	 			$endDate = null;
	 			//si on est sur un child form
	 			$sessionExist = false;
	 			if( @$form["parentSurvey"] ){
	 				$form["parentSurvey"] = PHDB::findOne( Form::COLLECTION , array("id"=>$form["parentSurvey"]) );
	 				if($form["parentSurvey"]["session"][$session]){
	 					$sessionExist = true;
		 				if(@$form["parentSurvey"]["session"][$session]["startDate"])
		 					$startDate = $form["parentSurvey"]["session"][$session]["startDate"];
		 				if(@$form["parentSurvey"]["session"][$session]["endDate"])
		 					$endDate = $form["parentSurvey"]["session"][$session]["endDate"];
		 			}
	 			} else {
	 				//sinon on est sur le form parent, point de départ d'un survey
 					$sessionExist = true;
	 				if(@$form["session"][$session]["startDate"])
	 					$startDate = $form["session"][$session]["startDate"];
	 				if(@$form["session"][$session]["endDate"])
	 					$endDate = $form["session"][$session]["endDate"];
	 			}

	 			$params = array( "form"    => $form, 
	 							 "answers" => $answers );
	 			if($startDate)
	 				$params["startDate"] = $startDate;
	 			if($endDate)
	 				$params["endDate"] = $endDate;

	 			if(!$sessionExist)
	 				return $this->getController()->render("co2.views.default.unTpl",array("msg"=>"Session introuvable sur ".$id,"icon"=>"fa-search"));
	 			else if(!isset($form["scenario"]))
	 				return $this->getController()->render("co2.views.default.unTpl",array("msg"=>"Sorry, No Scenario here ","icon"=>"fa--circle-times"));
	 			else {
	 				if( Yii::app()->request->isAjaxRequest && (!isset( $page["json"] )) )
						return $this->getController()->renderPartial("index",$params );
	 				else 
			 			return $this->getController()->render("index",$params );
	 			}
	 		} 
	 		//duplicate survey for identaical scenario
	 		// use case : cterr for all cte territories
	 		else if( isset($copy) 
	 			&& PHDB::findOne( Form::COLLECTION , array( "id"=>$copy ) ) 
	 			&& Slug::getElementBySlug($id) )
	 		{
	 			//if( @Yii::app()->session['costum']['survey']["generate"]["auto"] ){
		 			//launch process automaticall
		 			$el = Slug::getElementBySlug($id);
		 			$pId = $el['id'];
		 			$pType = $el['type'];
		 			$res = Form::generate($id,$copy,$pId, $pType);
			        if ( isset($res['render']) ) 
			            return $this->getController()->render($res["render"],array("msg"=>$res["msg"],"icon"=>$res["icon"]));
			        else
		 				return $this->getController()->redirect( Yii::app()->createUrl("/survey/co/index/id/".$id."/session/1") );
		 		// } 
		 		// else if(@Yii::app()->session['costum']['survey']["generate"]["if"]["status"]["selected"]){
		 		// 	echo $this->getController()->render("generate" , array( 
		 		// 		"id"	=> $id,
		 		// 		"icon"	=> "fa-cogs",
		 		// 		"copy"	=> $copy,
		 		// 		"text"	=> Yii::app()->session['costum']['survey']["generate"]["text"],
		 		// 		"title"	=> Yii::app()->session['costum']['survey']["generate"]["title"],
		 		// 		"parentId"	=> $pId, 
		 		// 		"parentType"=> $pType
		 		// 	));
		 		// }
	 		}
		 	else 
		 		return $this->getController()->render("co2.views.default.unTpl",array("msg"=>"Formulaire introuvable","icon"=>"fa-search"));
		 } else {
		 	$forms = PHDB::find( Form::COLLECTION , array("parentSurvey"=>array('$exists'=>0) ) );
		 	return $this->getController()->render("home" , array( "forms"=>$forms ));
		 }
    }
}