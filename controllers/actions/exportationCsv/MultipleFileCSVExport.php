<?php
    
    namespace PixelHumain\PixelHumain\modules\survey\controllers\actions\exportationCsv;
    // ini_set('max_execution_time',3000);
    use Answer;
    use CacheHelper;
    use Form;
    use FranceTierslieux;
    use MongoId;
    use Organization;
    use PHDB;
    use PixelHumain\PixelHumain\components\Action;
    use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\actor\Person;
    use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
    use Project;
    use Rest;
    use Yii;
    use Mapping;
    use Zone;
    use Document;
    use Authorisation;
    use Costum;

    class MultipleFileCSVExport extends Action {
        
        private function getValueByDotPath($assocArray , $path) {

            $split = explode(".", $path);
            // var_dump($split);exit;
            for ($i=0; $i < count($split) ; $i++) 
            { 
                $key = $split[$i];
                if( isset( $assocArray[  $key ] ) )
                    $assocArray = $assocArray[ $key ];
                else
                    $assocArray = "";//"<span class='text-red'>error in path : $path </span>";
            }
            return $assocArray;
        }

        private $head_temporaries = [
            'tpls.forms.cplx.checkboxNew'               => [],
            'tpls.forms.cplx.multiCheckboxPlus'         => [],
            'tpls.forms.cplx.multiRadio'                => [],
            'tpls.forms.cplx.radioNew'                  => [],
            'tpls.forms.cplx.simpleTable'               => [],
            'tpls.forms.cplx.element'                   => [],
            'tpls.forms.cplx.finder'                    => [],
            'tpls.forms.costum.franceTierslieux.finder' => [],
            'tpls.forms.evaluation.evaluation'          => []
        ];
        private $specials = [
            'tpls.forms.cplx.multiCheckboxPlus'         => 'multiCheckboxPlus',
            // 'tpls.forms.cplx.checkboxNew'         => 'checkboxNew',
            'tpls.forms.cplx.finder'                    => 'finder',
            'tpls.forms.costum.franceTierslieux.finder' => 'finder',
            'tpls.forms.cplx.listing'                   => 'listing',
            // 'tpls.forms.cplx.radioNew'                  => 'radioNew',
            'tpls.forms.cplx.multiRadio'                => 'multiRadio',
            'tpls.forms.evaluation.evaluation'          => 'evaluation',
            'tpls.forms.evaluation.multitextvalidation'          => 'multitextvalidation',
        ];
        
        private $emails = [];
        private $alias = [];
        
        // Spéciale FRTL 2023
        private $region_filters = [];
        
        public function run() {
            
            $output = [];
            $form_fields = ['name', 'config', 'subForms', 'creator', 'params', 'id', 'inputs', 'preferences', 'dataAccess'];
            $previous_answer_field = 'previousAnswerId';
            $answer_db_fields = [
                'form',
                'answers',
                'status',
                'user',
                'links.answered',
                'links.' . Organization::COLLECTION,
                'links.' . Project::COLLECTION,
                $previous_answer_field
            ];
            $form_id_and_formid_mapping = [
                'id_to_formid' => [],
                'formid_to_id' => []
            ];
            $form_id_to_name = [];

            $dataAccessStep = [];
            // transformer les différents posts
            $mappingFTL=PHDB::findOne(Mapping::COLLECTION,array("name"=>"mappingExportFTL"));
            // var_dump($mappingFTL);exit;

            $_POST["mappingLabel"]=(!empty($mappingFTL)) ? $mappingFTL["fields"] : $_POST["mappingLabel"];
            $main_form_steps = !empty($_POST['steps']) && is_array($_POST['steps']) ? $_POST['steps'] : [];
            $query = !empty($_POST['query']) && is_array($_POST['query']) ? $_POST['query'] : ['form' => null];
            $following_answer_id = $_POST['following_answer_id'] ?? 'followingAnswerId';
            $excluded_inputs = $_POST['excluded_inputs'] ?? [];
            $is_exporting_dynform = filter_var($_POST['is_exporting_dynform'] ?? false, FILTER_VALIDATE_BOOL);
            
            // Spéciale FRTL 2023
            $this->region_filters = !empty($_POST['region_filters']) && is_array($_POST['region_filters']) ? $_POST['region_filters'] : [];
            $regionName="";
            if(!empty($this->region_filters)){
                foreach($this->region_filters as $ind=>$level3){
                    $region=PHDB::findOne(Zone::COLLECTION,array("_id"=>new MongoId($level3)),array("name"));
                    $regionName=$regionName.'_'.$region["name"];
                }
                
            }

            $all_steps = $main_form_steps;
            $db_main_form = PHDB::findOne(Form::COLLECTION, ['_id' => new MongoId($query['form'])], $form_fields);
            
            $where_form = ['$or' => []];
            if (!empty($db_main_form['subForms']) && is_array($db_main_form['subForms'])) {
                $ins = ['id' => ['$in' => []]];
                foreach ($db_main_form['subForms'] as $sub_form) {
                    UtilsHelper::push_array_if_not_exists($sub_form, $ins['id']['$in']);
                }
                $where_form['$or'][] = $ins;
            }
            
            // addQuery verifying that last step is validated
            $completed_answer_attribute = '';
            if (filter_var($_POST['completed_answers_only'] ?? false, FILTER_VALIDATE_BOOLEAN)) {
                $last_subform_id = end($db_main_form['subForms']);
                $lastSubf = PHDB::findOne(Form::COLLECTION, ['id' => $last_subform_id], $form_fields);
                $inputs = $lastSubf["inputs"] ?? [];
                foreach ($inputs as $kfield => $vfield) {
                    if (str_contains($vfield["type"], "validateStep")) {
                        $keyLastValidateStep = $kfield;
                    }
                }
                $completed_answer_attribute = "answers.$last_subform_id.$keyLastValidateStep";
            }
            
            if (filter_var($_POST['is_exporting_following_answer'], FILTER_VALIDATE_BOOLEAN) && $this->has_following_answers($query['form'], $following_answer_id)) {
                $answer_db_fields[] = $following_answer_id;
                
                // get forms references
                $criteria = [
                    'form'               => $query['form'],
                    $following_answer_id => ['$exists' => 1]
                ];
                if (!empty($completed_answer_attribute)) $criteria[$completed_answer_attribute] = ['$exists' => 1];
                $db_following_answers = PHDB::distinct(Answer::COLLECTION, $following_answer_id, $criteria);
                $ids = array_map(fn($answer) => new MongoId($answer), $db_following_answers);
                $db_following_forms = PHDB::distinct(Answer::COLLECTION, 'form', ['_id' => ['$in' => $ids]]);
                $ids = array_map(fn($form) => new MongoId($form), $db_following_forms);
                $where_form['$or'][] = ['_id' => ['$in' => $ids]];
                $db_following_parent_forms = PHDB::find(Form::COLLECTION, ['_id' => ['$in' => $ids]], $form_fields);
                $ids = [];
                foreach ($db_following_parent_forms as $db_following_parent_form) {
                    $ids = array_merge($db_following_parent_form['subForms'] ?? [], $ids);
                }
                $where_form['$or'][] = ['id' => ['$in' => $ids]];
            }
            $db_forms = PHDB::find(Form::COLLECTION, $where_form, $form_fields);
            $db_forms[$query['form']] = $db_main_form;
            $db_parent_forms = [];
            $db_sub_forms = [];
            
            // Séparer les formparents et les subforms
            foreach ($db_forms as $form_id => $form_value) {
                if (empty($form_value['subForms'])) $db_sub_forms[$form_id] = $form_value;
                else $db_parent_forms[$form_id] = $form_value;
            }
            
            if (!empty($db_following_forms)) {
                foreach ($db_following_forms as $db_following_form) $following_forms_inputs[$db_following_form] = $db_forms[$db_following_form]['inputs'] ?? [];
            }
            
            foreach ($db_sub_forms as $db_form) {
                $parent_forms = array_filter($db_parent_forms, fn($form) => isset($form['subForms']) && in_array($db_form['id'], $form['subForms']));
                $parent_form = end($parent_forms);
                $nameSubForm=(isset($_POST['mappingLabel']) && array_search($db_form['name'],array_keys($_POST['mappingLabel']))!==false) ? $_POST['mappingLabel'][$db_form['name']]: $parent_form['name'].'_' . $db_form['name'];
                // var_dump($db_form['name'],array_search($parent_form['name'],array_keys($_POST['mappingLabel'])),$nameSubForm);
                $output[$db_form['id']] = [
                    'heads'  => [],
                    'bodies' => [],
                    'name'   => $nameSubForm.$regionName
                ];
            }
            
            // mapping formId and ids
            foreach ($db_forms as $form_id => $form_value) {
                $form_id_and_formid_mapping['id_to_formid'][$form_value['id']] = $form_id;
                $form_id_and_formid_mapping['formid_to_id'][$form_id] = $form_value['id'];
                if(isset($form_value["dataAccess"])){
                    $dataAccessStep[$form_value["id"]]=$form_value["dataAccess"];
                }
                $form_id_to_name[$form_value['id']]=$form_value['name'];
            }
            $answer_where = ['form' => ['$in' => array_keys($form_id_and_formid_mapping['formid_to_id'])], 'answers' => ['$exists' => 1]];
            
            $db_answers = PHDB::find(Answer::COLLECTION, $answer_where, $answer_db_fields);
            $db_following_answers = [];
            $db_main_answers = [];
            
            // Séparer les answers principales et les autres
            foreach ($db_answers as $db_answer_key => $db_answer_value) {
                // Récupération des emails
                if (filter_var($_POST['is_exporting_user_email'] ?? false, FILTER_VALIDATE_BOOLEAN) && !empty($db_answer_value['links']['answered'])) {
                    $answereds = $db_answer_value['links']['answered'];
                    if (is_string($answereds)) UtilsHelper::push_array_if_not_exists($answereds, $this->emails);
                    elseif (is_array($answereds)) {
                        foreach ($answereds as $answered) UtilsHelper::push_array_if_not_exists($answered, $this->emails);
                    }
                }
                
                // Séparer les réponses
                if ($db_answer_value['form'] === $query['form']) {
                    if (!empty($completed_answer_attribute)) {
                        $exploded = explode('.', $completed_answer_attribute);
                        if (!empty(UtilsHelper::look_at_array_in_depth($db_answer_value, $exploded)))
                            $db_main_answers[$db_answer_key] = $db_answer_value;
                    } else $db_main_answers[$db_answer_key] = $db_answer_value;
                } else $db_following_answers[$db_answer_key] = $db_answer_value;
            }
            $this->emails = array_map(fn($email) => new MongoId($email), $this->emails);
            $this->emails = PHDB::find(\Person::COLLECTION, ['_id' => ['$in' => $this->emails]], ['email']);
            
            $dynform_organizations = [];
            $filtered_dynform_organizations = [];
            // exporter les informations relatives à l'organisation
            if ($is_exporting_dynform) {
                foreach ($db_main_answers as $db_answer) {
                    if (!empty($db_answer['links'][Organization::COLLECTION])) {
                        $ids = array_keys($db_answer['links'][Organization::COLLECTION]);
                        foreach ($ids as $id) UtilsHelper::push_array_if_not_exists($id, $dynform_organizations);
                    }
                }
            }
            
            // exporter les informations relatives à l'organisation
            $dynform_organization_ids = array_map(fn($id) => new MongoId($id), $dynform_organizations);
            // $orderingQuery=($query['form']=="63e0a8abeac0741b506fb4f7") ? array("idRecensementTL2023"=>1) : array();
            $db_organizations = PHDB::find(Organization::COLLECTION, ['_id' => ['$in' => $dynform_organization_ids]]);
            // if ($is_exporting_dynform) {
            foreach ($db_organizations as $db_organization_id => $db_organization_value) {
                $output["orgadynform"] = $output["orgadynform"] ?? [
                    'name'   => (isset($_POST['mappingLabel']) && array_search('Organization',array_keys($_POST['mappingLabel']))!==false) ? $_POST['mappingLabel']['Organization'].$regionName  : 'Organization'.$regionName,
                    'heads'  => [],
                    'bodies' => []
                ];
                $organization_mapping = $this->get_dynform_exportation_mapping(Organization::COLLECTION, $query['form'], []);
                $response = $this->map_data_from_template($db_organization_value, $organization_mapping, [
                    'address' => $_POST['settings']['address'],
                    'geo'     => $_POST['settings']['address'],
                    'socialNetwork'=>'monocolumn',
                    'telephone' => 'monocolumn'
                ]);
                $existingRegion=(isset($db_organization_value["address"]["level3"])) ? $db_organization_value["address"]["level3"] : "";
                $filteredAddress=(empty($this->region_filters) || (!empty($existingRegion) && in_array($existingRegion, $this->region_filters))) ? true : false;
                // var_dump($existingRegion, $this->region_filters);exit;
                $is_separated_column = array_key_exists('(DF) level3Name', $response['body']) && in_array($response['body']['(DF) level3Name'], $this->region_filters);
                $adresse = explode(";", $response['body']['(DF) Adresse'] ?? []);
                $is_unified_column = array_key_exists('(DF) Adresse', $response['body']) && in_array(end($adresse), $this->region_filters);
                
                if (!$filteredAddress) continue;
                
                UtilsHelper::push_array_if_not_exists($db_organization_id, $filtered_dynform_organizations);
                $output["orgadynform"]['heads'] = $response['header'];
                $output["orgadynform"]['bodies'][] = $response['body'];
            }
            usort($output["orgadynform"]['bodies'], array($this,"compare_idunique"));
            // }
            // Reponses principales
            $reponses_principales_a_exclure = [];
            foreach ($main_form_steps as $step_key => $step_value) {
                $dataAccess=(isset($dataAccessStep[$step_key])) ? $dataAccessStep[$step_key] : [] ;
                foreach ($db_main_answers as $answer_id => $db_answer) {
                    $costum=CacheHelper::getCostum();
                    $userId=Yii::app()->session["userId"];
				    $user=PHDB::findOne(\Person::COLLECTION,array("_id"=>new MongoId($userId)));
                    $canExport=true;
                    if(isset($db_main_form["preferences"]["openDataCondition"])){
                        foreach($db_main_form["preferences"]["openDataCondition"] as $path=>$mapping){
                            $value=$this->getValueByDotPath($db_answer["answers"],$path);
                            // var_dump($value);exit;
                            if(is_string($value)){
                                $value=$value;
                            }
                            else if(is_array($value) && !empty($value["value"])){
                                $value=$value["value"];
                            }
                            else{
                                continue;
                            }
                            
                            if(isset($db_answer["answers"],$db_main_form["preferences"]["openDataCondition"][$path][$value])){
                                $canExport=$db_main_form["preferences"]["openDataCondition"][$path][$value];
                            }else{
                                $canExport=false;
                            }
                        }
                    }
                   
				    $holderId=(!empty($db_answer["links"]["organizations"]) && count(array_keys($db_answer["links"]["organizations"]))>0) ? array_keys($db_answer["links"]["organizations"])[0] : null ;
				    $holderType=(isset($db_answer["links"]["organizations"][$holderId]["type"])) ? $db_answer["links"]["organizations"][$holderId]["type"] : null;
				    // $holder=PHDB::findOne($holderType,array("_id"=>new MongoId($holderId)));
				    $rolesInHolder = isset($user["links"]["memberOf"][$holderId]["roles"]) ? $user["links"]["memberOf"][$holderId]["roles"] : null;
				    $rolesInContext= (!empty($costum["contextId"]) && !empty($user["links"]["memberOf"][$costum["contextId"]]["roles"])) ? $user["links"]["memberOf"][$costum["contextId"]]["roles"] : null;
                    $isHolderAdmin=isset($user["links"]["memberOf"][$holderId]["isAdmin"]) ? $user["links"]["memberOf"][$holderId]["isAdmin"] : false;
			        $adminInContext= (isset($costum["contextId"]) && isset($user["links"]["memberOf"][$costum["contextId"]]["isAdmin"])) ? $user["links"]["memberOf"][$costum["contextId"]]["isAdmin"] : false;
                    if(!isset($user["links"]["memberOf"][$holderId]["isAdminPending"]) && isset($user["links"]["memberOf"][$holderId]["isAdmin"]) && $user["links"]["memberOf"][$holderId]["isAdmin"]){
					    $userAuthExport=true;
				    }
                    $authAnswerPathAndValue=(isset($db_main_form["preferences"]["dataAccess"]["mappingPathValueRoles"])) ? $db_main_form["preferences"]["dataAccess"]["mappingPathValueRoles"] : [];
                    $answererAuthRoles=[];
                    foreach($authAnswerPathAndValue as $path=>$mapping){
                        $value=$this->getValueByDotPath($db_answer["answers"],$path);
                        if(isset($authAnswerPathAndValue[$path][$value])){
                            $answererAuthRoles[]=$authAnswerPathAndValue[$path][$value];
                        }
                        
                    }
                    // var_dump($answererAuthRoles);exit;
                    $commonRolesAnswererUser=(isset($rolesInHolder) && isset($rolesInContext)) ? array_intersect($rolesInHolder,$rolesInContext,$answererAuthRoles) : [];
				    $canExport = (sizeof($commonRolesAnswererUser)>0 || $isHolderAdmin || $adminInContext) ? true : $canExport;
                    // var_dump($rolesInContext,$rolesInHolder,$answererAuthRoles,$isHolderAdmin);exit;
                    // var_dump($rolesInHolder,$answererAuthRoles,$isHolderAdmin);exit;
                    if ($canExport && !empty($this->region_filters)) {
                        // var_dump($this->region_filters);exit;
                        $organization_links = !empty($db_answer['links']['organizations']) ? array_keys($db_answer['links']['organizations']) : [];             
                        if (empty($organization_links)){
                            $reponses_principales_a_exclure[] = $answer_id;
                        }else if(!empty($organization_links) && isset($db_organizations[$organization_links[0]]["address"]["level3Name"]) && !in_array($db_organizations[$organization_links[0]]["address"]["level3"],$this->region_filters)){
                        //    var_dump($db_organizations[$organization_links[0]]["address"]["level3Name"]);exit;
                           $reponses_principales_a_exclure[] = $answer_id;
                        }

                        // foreach ($organization_links as $organization_link) {
                        // we only the first organization in answer links link
                        // var_dump($organization_links,$answer_id,$reponses_principales_a_exclure);exit;
                        if(!in_array($answer_id,$reponses_principales_a_exclure) && !empty($organization_links)){                           
                            $body = $this->get_formated_answer_at_step($db_answer, $step_key, $step_value, $db_main_form, $dataAccess);
                            
                            $id_orga=array_keys($db_answer["links"]["organizations"])[0];
                            if(isset($db_organizations[$id_orga])){
                            $orga_following=$db_organizations[$id_orga];
                            $organization_mapping = $this->get_dynform_exportation_mapping(Organization::COLLECTION, "followingAnswers", []);
                           $response = $this->map_data_from_template($orga_following, $organization_mapping, [
                                'address' => $_POST['settings']['address'],
                                'geo'     => $_POST['settings']['address'],
                                'socialNetwork'=>'monocolumn'
    
                            ]);
                            // To set in FTL rules models
                            // var_dump($body,$response);exit;
                            // if(isset($body["ID_REP"]) && $query["form"]=="63e0a8abeac0741b506fb4f7"){
                            //     $body["ID_REP"]=$response["idRecensement23"] ?? "";
                            // }
                            $is_separated_column = array_key_exists('(DF) level3Name', $response['body']) && in_array($response['body']['(DF) level3Name'], $this->region_filters);
                            $adresse = explode(";", $response['body']['(DF) Adresse'] ?? []);
                            $is_unified_column = array_key_exists('(DF) Adresse', $response['body']) && in_array(end($adresse), $this->region_filters);
                            $body=array_merge_recursive($response["body"],$body);
                            $orga_headers=$response["header"];
                            }
                            
                        // foreach ($organization_links as $organization_link) {
                            // Exporter la réponse dont l'orga est à exporter
                            if (in_array($id_orga, $filtered_dynform_organizations)) {
                               
                                $check_index = 0;
                                $check_index += filter_var($_POST['is_exporting_answer_id'] ?? false, FILTER_VALIDATE_BOOLEAN) ? 1 : 0;
                                $check_index += filter_var($_POST['is_exporting_user_email'] ?? false, FILTER_VALIDATE_BOOLEAN) ? 1 : 0;
                                $body_keys = array_slice(array_keys($body), $check_index);
                                
                                $not_empties = array_filter($body, function ($value, $key) use ($body_keys) {
                                    return in_array($key, $body_keys) && !empty($value);
                                },                          ARRAY_FILTER_USE_BOTH);
                                if (!empty($not_empties)) $output[$step_key]['bodies'][] = $body;
                                
                            } else /* Exclure la réponse */ $reponses_principales_a_exclure[] = $answer_id;
                        // }
                        }
                    } else if($canExport) {
                        $body = $this->get_formated_answer_at_step($db_answer, $step_key, $step_value, $db_main_form, $dataAccess);                        
                        // $organization_links = !empty($db_answer['links']['organizations']) ? array_keys($db_answer['links']['organizations']) : [];
                        // if (empty($organization_links)) $reponses_principales_a_exclure[] = $answer_id;
                        // foreach ($organization_links as $organization_link) {
                        //     // Exporter la réponse dont l'orga est à exporter
                        //     if (in_array($organization_link, $filtered_dynform_organizations)) {
                        //         $body = $this->get_formated_answer_at_step($db_answer, $step_key, $step_value, $db_main_form);
                        //         $check_index = 0;
                        //         $check_index += filter_var($_POST['is_exporting_answer_id'] ?? false, FILTER_VALIDATE_BOOLEAN) ? 1 : 0;
                        //         $check_index += filter_var($_POST['is_exporting_user_email'] ?? false, FILTER_VALIDATE_BOOLEAN) ? 1 : 0;
                        //         $body_keys = array_slice(array_keys($body), $check_index);
                                
                        //         $not_empties = array_filter($body, function ($value, $key) use ($body_keys) {
                        //             return in_array($key, $body_keys) && !empty($value);
                        //         },                          ARRAY_FILTER_USE_BOTH);
                        //         if (!empty($not_empties)) $output[$step_key]['bodies'][] = $body;
                                
                        //     } else /* Exclure la réponse */ $reponses_principales_a_exclure[] = $answer_id;
                        //     organization_link
                        // }
                        if(isset($db_answer["links"]) && !empty($db_answer["links"]["organizations"]) && !empty($body)){
                            $id_orga=array_keys($db_answer["links"]["organizations"])[0];
                            if(isset($db_organizations[$id_orga])){
                            $orga_following=$db_organizations[$id_orga];
                            $organization_mapping = $this->get_dynform_exportation_mapping(Organization::COLLECTION, "followingAnswers", []);
                           $response = $this->map_data_from_template($orga_following, $organization_mapping, [
                                'address' => $_POST['settings']['address'],
                                'geo'     => $_POST['settings']['address'],
                                'socialNetwork'=>'monocolumn'
    
                            ]);
                            // To set in FTL rules models
                            // var_dump($body,$response);exit;
                            // if(isset($body["ID_REP"]) && isset($response["body"]["ID_REP"]) && isset($response["header"][1]["ID_REP"])){
                            //     $body["ID_REP"]=$response["body"]["ID_REP"] ?? "";
                            //     unset($response["body"]["ID_REP"]);
                            //     unset($response["header"][1]["ID_REP"]);
                            // }
                            $is_separated_column = array_key_exists('(DF) level3Name', $response['body']) && in_array($response['body']['(DF) level3Name'], $this->region_filters);
                            $adresse = explode(";", $response['body']['(DF) Adresse'] ?? []);
                            $is_unified_column = array_key_exists('(DF) Adresse', $response['body']) && in_array(end($adresse), $this->region_filters);
                            $body=array_merge_recursive($response["body"],$body);
                            $orga_headers=$response["header"];
                            }
                        }
                        
                        
                        $check_index = 0;
                        $check_index += filter_var($_POST['is_exporting_answer_id'] ?? false, FILTER_VALIDATE_BOOLEAN) ? 1 : 0;
                        $check_index += filter_var($_POST['is_exporting_user_email'] ?? false, FILTER_VALIDATE_BOOLEAN) ? 1 : 0;
                        $body_keys = array_slice(array_keys($body), $check_index);
                        
                        $not_empties = array_filter($body, function ($value, $key) use ($body_keys) {
                            return in_array($key, $body_keys) && !empty($value);
                        },                          ARRAY_FILTER_USE_BOTH);
                        if (!empty($not_empties)) $output[$step_key]['bodies'][] = $body;
                    }
                    // var_dump($output[$step_key]["bodies"][],$organization_mapping,$query['form']);exit;
                }
                $stepTitle=$form_id_to_name[$step_key];
                $stepTitle=(isset($_POST["mappingLabel"][$stepTitle]) && in_array($_POST["mappingLabel"][$stepTitle])!==false) ? $_POST["mappingLabel"][$stepTitle] : $stepTitle ;
                foreach($this->alias as $ind=>$valAlias){
                    if(!isset($valAlias["Step"])){
                        $this->alias[$ind]["Step"]=$stepTitle;
                    }
                }
            }
            
            // exit(json_encode($reponses_principales_a_exclure));

             $secondAnswerorga=[];

            if ($is_exporting_dynform) {
                foreach ($db_following_answers as $db_answer) {
                    if (!empty($db_answer['links'][Organization::COLLECTION])) {
                        $ids = array_keys($db_answer['links'][Organization::COLLECTION]);
                        foreach ($ids as $id) UtilsHelper::push_array_if_not_exists($id, $secondAnswerorga);
                    }
                }
            }
            $dynform_organization_ids = array_map(fn($id) => new MongoId($id), $secondAnswerorga);
            $db_organizations = PHDB::find(Organization::COLLECTION, ['_id' => ['$in' => $dynform_organization_ids]]);
            $organization_links = !empty($db_answer['links']['organizations']) ? array_keys($db_answer['links']['organizations']) : [];
            
            // Exporter les following answers
            
            foreach ($db_sub_forms as $sub_form_value) {
                $step_key = $sub_form_value['id'];
                if (in_array($step_key, $db_main_form['subForms'])) continue;
               
                foreach ($db_following_answers as $db_answer) {
                    $previous_answer_id = $db_answer[$previous_answer_field] ?? '';
                    
                    if (!empty($reponses_principales_a_exclure) && in_array($previous_answer_id, $reponses_principales_a_exclure) || empty($previous_answer_id) || !array_key_exists($previous_answer_id, $db_main_answers)) continue;
                    $inputs = $this->reorder_form_inputs($sub_form_value['inputs'], $excluded_inputs);
                    $dataAccess=(!empty($sub_form_value['dataAccess'])) ? $sub_form_value['dataAccess'] : [];
                    $all_steps[$step_key] = $inputs;
                    // var_dump($all_steps);exit;
                    $parent_forms = array_filter($db_parent_forms, fn($form) => !empty($form['subForms']) && in_array($step_key, $form['subForms']));
                    $parent_form = end($parent_forms);
                    // var_dump($parent_forms);exit;
                    $body = $this->get_formated_answer_at_step($db_answer, $step_key, $inputs, $parent_form, $dataAccess);

                    if(isset($db_answer["links"]) && !empty($db_answer["links"]["organizations"]) && !empty($body)){
                        $id_orga=array_keys($db_answer["links"]["organizations"])[0];
                        $orga_following=(isset($db_organizations[$id_orga])) ? $db_organizations[$id_orga] : [];
                        $organization_mapping = $this->get_dynform_exportation_mapping(Organization::COLLECTION, "followingAnswers", []);
                        if(!empty($orga_following) && !empty($organization_mapping)){
                            $response = $this->map_data_from_template($orga_following, $organization_mapping, [
                                'address' => $_POST['settings']['address'],
                                'geo'     => $_POST['settings']['address'],
                                'socialNetwork'=>'monocolumn'
                            ]);
                        }else{
                            $response=[
                                "body"=>[],
                                "header"=>[]
                            ];
                        }
                        // To set in FTL rules models
                            // var_dump($body,$response);exit;
                        // if(isset($body["ID_REP"]) && isset($response["body"]["ID_REP"]) && isset($response["header"][1]["ID_REP"])){
                        //         $body["ID_REP"]=$response["body"]["ID_REP"] ?? "";
                        //         unset($response["body"]["ID_REP"]);
                        //         unset($response["header"][1]["ID_REP"]);
                        //     }
                        
                        $is_separated_column = array_key_exists('(DF) level3Name', $response['body']) && in_array($response['body']['(DF) level3Name'], $this->region_filters);
                        $adresse = explode(";", $response['body']['(DF) Adresse'] ?? []);
                        $is_unified_column = array_key_exists('(DF) Adresse', $response['body']) && in_array(end($adresse), $this->region_filters);
                        $body=array_merge_recursive($response["body"],$body);
                        $orga_headers=$response["header"];
                    }
                    $check_index = 0;
                    $check_index += filter_var($_POST['is_exporting_answer_id'] ?? false, FILTER_VALIDATE_BOOLEAN) ? 1 : 0;
                    $check_index += filter_var($_POST['is_exporting_user_email'] ?? false, FILTER_VALIDATE_BOOLEAN) ? 1 : 0;
                    $body_keys = array_slice(array_keys($body), $check_index);
                    
                    $not_empties = array_filter($body, function ($value, $key) use ($body_keys) {
                        return in_array($key, $body_keys) && !empty($value);
                    },                          ARRAY_FILTER_USE_BOTH);
                    if (!empty($not_empties)){
                        $output[$step_key]['bodies'][] = $body;
                    }
                }
            }

            if(!empty($_POST['mappingLabel'])){
                $output["metadata"]=[];
                $output["metadata"]["heads"]=["Step","Label","Alias","Types"];
                $output["metadata"]["bodies"]=$this->alias;
                $output["metadata"]["name"]="Métadonnées";
            }

            
            // var_dump($this->alias);exit;
            
            
            // Créer les entêtes
            foreach ($all_steps as $step_key => $step_value) {
                // $exportObj=new MultipleFileCSVExport();

                // var_dump(is_callable(array($this,"compare_idunique")));exit;
                // var_dump($output[$step_key]['bodies']);
                
                // var_dump($output[$step_key]['bodies']);exit;
                $output[$step_key]['heads'] = $this->get_headers_at_step($step_value);
                if(isset($orga_headers)){
                    $output[$step_key]['heads']=array_merge_recursive($orga_headers,$output[$step_key]['heads']);
                }
                // sort alphabetically by name
                usort($output[$step_key]['bodies'], array($this,"compare_idunique"));
            }
            // var_dump($output);exit;
            
            return Rest::json($output);
        }

        public function compare_idunique($a,$b){
            // return "ok";()
            $res=0;
            if(isset($a["ID_UNIQUE"]) && isset($b["ID_UNIQUE"])){
                $res= $a["ID_UNIQUE"] - $b["ID_UNIQUE"];
            }

            return $res;
         }
        private function has_following_answers(string $form, string $attribute): bool {
            return PHDB::count(Answer::COLLECTION, ['form' => $form, $attribute => ['$exists' => 1]]) > 0;
        }
        
        private function get_formated_answer_at_step(array &$answer, string $step_key, array &$fields_in_step, array &$parent_form, $stepAccess=[]): array {
            // var_dump($stepAccess,$step_key,$answer);exit;
            $costum=CacheHelper::getCostum();
            $stepNumber=array_search($step_key,$parent_form["subForms"]) + 1;
            $stepNumber = (strlen((string)$stepNumber)>1) ? (string)$stepNumber : "0".(string)$stepNumber;
            // $stepLabel= $_POST["mappingLabel"] ? :
            $must_put_complex_checkbox_aside = filter_var($_POST['is_custom_checkbox_aside'], FILTER_VALIDATE_BOOLEAN);
            $output = [];
            $generaticStartFields=[];
            if (filter_var($_POST['is_exporting_answer_id'] ?? false, FILTER_VALIDATE_BOOLEAN)) {
                $generaticStartFields['ID_REP'] = (string)$answer['_id'];
            }
            if (filter_var($_POST['is_exporting_user_email'] ?? false, FILTER_VALIDATE_BOOLEAN) && !empty($answer['links']['answered'])) {
                $answereds = $answer['links']['answered'];
                $value = '';
                if (is_string($answereds)) $value = $this->emails[$answereds]['email'] ?? "$answereds : Supprimé";
                elseif (is_array($answereds)) {
                    foreach ($answereds as $answered) {
                        $email = $this->emails[$answered]['email'] ?? "$answered : Supprimé";
                        $value .= $email . ";";
                    }
                    $value = substr($value, 0, -3);
                }

                if(!empty($costum)){    
                   if(filter_var($_POST['is_exporting_user_email'] ?? false, FILTER_VALIDATE_BOOLEAN) && Authorisation::isInterfaceAdmin($costum["contextId"], $costum["contextType"])){
                        $generaticStartFields['EMAIL_REPONDANT'] = $value;
                    } 
                }       
            }
            $userAuthStep=true;
            if(!empty($stepAccess)){
                $authStepPathAndValue=(isset($stepAccess["mappingPathValueRoles"])) ? $stepAccess["mappingPathValueRoles"] : [];
                $stepAuthRolesAns=[];
                foreach($authStepPathAndValue as $path=>$mapping){
                    $value=$this->getValueByDotPath($answer["answers"],$path);
                    if(!empty($value) && is_array($value)){
                        foreach($value as $relVal){
                            if(isset($mapping[$relVal])){
                                $stepAuthRolesAns[]=$mapping[$relVal];
                            }
                        }
                    }else if(isset($authStepPathAndValue[$path][$value]) && is_string($value)){
                        $stepAuthRolesAns[]=$authStepPathAndValue[$path][$value];
                    }
                }
                $costum=CacheHelper::getCostum();
                $userId=Yii::app()->session["userId"];
                $user=PHDB::findOne(\Person::COLLECTION,array("_id"=>new MongoId($userId)));
                $canExport=true;
                $holderId=(!empty($answer["links"]["organizations"]) && count(array_keys($answer["links"]["organizations"]))>0) ? array_keys($answer["links"]["organizations"])[0] : "" ;
                $holderType=(isset($answer["links"]["organizations"][$holderId]["type"])) ? $answer["links"]["organizations"][$holderId]["type"] : "";
                // $holder=PHDB::findOne($holderType,array("_id"=>new MongoId($holderId)));
                $rolesInHolder = isset($user["links"]["memberOf"][$holderId]["roles"]) ? $user["links"]["memberOf"][$holderId]["roles"] : [];
                $rolesInContext= (isset($costum["contextId"]) && isset($user["links"]["memberOf"][$costum["contextId"]]["roles"])) ? $user["links"]["memberOf"][$costum["contextId"]]["roles"] : [];
                $isHolderAdmin=isset($user["links"]["memberOf"][$holderId]["isAdmin"]) ? $user["links"]["memberOf"][$holderId]["isAdmin"] : false;
                $adminInContext= (isset($costum["contextId"]) && isset($user["links"]["memberOf"][$costum["contextId"]]["isAdmin"])) ? $user["links"]["memberOf"][$costum["contextId"]]["isAdmin"] : false;
                
                // check authorised roles to access step
                // $authStepRoles=isset($stepAccess["roles"]) ? $stepAccess["roles"] : [];
                $authStepRoles=array_merge($rolesInContext,$rolesInHolder);
                // Comparison with user roles if exist
                $authStepUserRoles=array_intersect($authStepRoles,$stepAuthRolesAns);
                // authorization if common roles or not roles condition in dataAccess.roles
                $userAuthStep = (sizeof($authStepUserRoles)>0 || in_array("all",$stepAuthRolesAns) || $isHolderAdmin || $adminInContext)  ? true : false; 
                // var_dump($stepAccess,$authStepUserRoles,$stepAuthRolesAns,$authStepRoles,$userAuthStep,$answer["answers"]["franceTierslieux2022023_732_9"]);exit;
                // var_dump($userAuthStep);exit;
            }
            // var_dump($answer);exit;
            // phpinfo();
            $canExport=true;
            if(isset($stepAccess["openDataCondition"])){
                // var_dump($stepAccess["openDataCondition"]);
                if (!function_exists('array_is_list')) {
                    function array_is_list(array $arr)
                    {
                        if ($arr === []) {
                            return true;
                        }
                        return array_keys($arr) === range(0, count($arr) - 1);
                    }
                }
                foreach($stepAccess["openDataCondition"] as $path=>$mapping){
                    // var_dump($path,$mapping,$this->getValueByDotPath($answer["answers"],$path));
                    $value=$this->getValueByDotPath($answer["answers"],$path);
                    // var_dump($value);
                    if(is_string($value)){
                        $value=$value;
                    }
                    else if(is_array($value) && !empty($value["value"])){
                        $value=$value["value"];
                    }else if(is_array($value)){
                        $value=$value;
                    }else{
                        continue;
                    }
                    // var_dump(is_array($stepAccess["openDataCondition"][$path]));exit;
                    if(is_array($value)){
                        if(array_is_list($stepAccess["openDataCondition"][$path])){
                            $openDataArray=$stepAccess["openDataCondition"][$path];
                        }else{
                            $openDataArray=array_keys($stepAccess["openDataCondition"][$path]);
                        }
                        if(!empty(array_intersect($value,$openDataArray))){
                            $canExport=true;
                            // var_dump($value,$openDataArray,array_intersect($value,$openDataArray));exit;
                        }else{
                            $canExport=false;
                        }
                        
                        // var_dump($mapping);exit;
                    }else if(isset($answer["answers"],$stepAccess["openDataCondition"][$path][$value])){
                        $canExport=$stepAccess["openDataCondition"][$path][$value];
                    }else{
                        $canExport=false;
                    }
                }
                if($canExport==true){
                    // var_dump($answer["answers"]["franceTierslieux2022023_732_9"]["franceTierslieux2022023_732_9lfgghmsgu4m891squfq"]);exit;
                }
            }
                
            foreach ($fields_in_step as $indField => $field) {
                if(!$userAuthStep && !$canExport){
                    continue;
                }   
                $label = trim(htmlspecialchars_decode((str_replace('  ', ' ', $field['label']))));

                $typeAlias="";
                // $label=str_replace(" ","_",$label);
                if (!isset($answer['answers'][$step_key])) continue;
                // Trier le format de réponse
                $current_step = '';
                if (!empty($answer['answers'][$step_key])) {
                    $current_step = $answer['answers'][$step_key];
                } elseif (array_key_exists($field['type'], $this->specials)) {
                    $special_key = $this->specials[$field['type']] . $field['value'];
                    if (!empty($answer['answers'][$step_key][$special_key])) {
                        $current_step = $answer['answers'][$step_key][$special_key];
                    } elseif (!empty($answer['answers'][$special_key])) {
                        $current_step = $answer['answers'][$special_key];
                    }
                }

                
                $field_value = ($this->specials[$field['type']] ?? '') . $field['value'];
                $default_value = $current_step[$field_value] ?? '';

                $this->head_temporaries[$field['type']][$label] = $this->head_temporaries[$field['type']][$label] ?? [];
                // var_dump($default_value);exit;

                if(isset($_POST["mappingLabel"][$label])){
                    // var_dump($field,$fields_in_step);exit;
                    // if($fields_in_steps[$indField-1])
                    $existingAnswerBoolean=true;
                    if(Costum::isSameFunction("existingAnswerBooleanExceptions")){
                        $existingAnswerBoolean = Costum::sameFunction("existingAnswerBooleanExceptions", array("key"=>$field["value"],"value"=>$default_value));
                    } 
                    if($existingAnswerBoolean){
                        $output[$_POST["mappingLabel"][$label]."_REP"]=(!empty($default_value)) ? "1" : "0" ;
                        UtilsHelper::push_array_if_not_exists($_POST["mappingLabel"][$label]."_REP", $this->head_temporaries[$field['type']][$label]);
                        $typeAlias="booléen (boolean) (0;1)";
                        UtilsHelper::push_array_if_not_exists(["Label"=>"Répondants à ".$label,"Alias"=>$_POST["mappingLabel"][$label]."_REP","Types"=>$typeAlias],$this->alias);  
                    }
                }
               
                // Spécifique pour le recensement france tiers lieux
                $specificForms=["63e0a8abeac0741b506fb4f7","63eda892a1a0a456c56c8322","64099d655ccb551e194061d2","64099fea0194fd19f34cd262","6409a25aa4dfbd34e62f4284","6409a5df049dd652fd08cee2","6409a80759ffd023f5787cb6","6409aa135ccb551e194061d6","6409adab2d6aa804e438f884","64114fcc15e4b7051212e7e2","64115356a9b9de7ecd781552"];
                if (in_array((string)$parent_form['_id'],$specificForms)) $default_value = FranceTierslieux::answerRules($field_value, $default_value);               

                $nonSpecialForMapping=["text","number"];
                // var_dump($label,$_POST["mappingLabel"][$label],in_array($field["type"],$nonSpecialForMapping));exit;
                if(!$userAuthStep && !$canExport){
                    $default_value="";    
                }
    
                
                switch ($field['type']) {
                    case 'tpls.forms.cplx.address':
                        $addresses = $current_step['address'];
                        if ($_POST['settings']['address'] == 'multicolumn') {
                            $output['StreetAddress'] = $addresses['streetAddress'];
                            $output['postalCode'] = $addresses['postalCode'];
                            $output['addressLocality'] = $addresses['addressLocality'];
                            $output['addressCountry'] = $addresses['addressCountry'];
                        } else {
                            $output[$label] = implode(";", [
                                $addresses['streetAddress'],
                                $addresses['postalCode'],
                                $addresses['addressLocality'],
                                $addresses['addressCountry']
                            ]);
                        }
                        break;
                    case 'tpls.forms.cplx.checkboxNew':
                        if ($_POST['settings']['checkbox'] == 'multicolumn') {
                            if(isset($_POST['mappingLabel'])){
                                // $nbCplx=array_count_values(array_values($parent_form['params'][$field_value]['tofill']))['cplx'] ?? 0;
                                // $indexSingleCplx=($nbCplx==1) ? array_search("cplx",array_values($parent_form['params'][$field_value]['tofill'])) : null;
                                // $singleCplxOption=($indexSingleCplx!==null) ? array_keys($parent_form['params'][$field_value]['tofill'])[$indexSingleCplx] : ""; 
                                                               $indexLabel=array_search($label,array_keys($_POST['mappingLabel']));
                                $valueColLabel=(isset($_POST['mappingLabel']) && $indexLabel!==false) ? array_values($_POST['mappingLabel'])[$indexLabel] : $label;
                                // $gathered=[];
                                // $gatheredCplx=[];
                                UtilsHelper::push_array_if_not_exists($valueColLabel, $this->head_temporaries[$field['type']][$label]);
                                $typeAlias="chaîne de caractères (str) à choix multiples";
                                $optionList=implode(";",$parent_form['params']["checkboxNew".$field_value]['list']);
                                $typeAlias=$typeAlias." (".$optionList.")";
                                UtilsHelper::push_array_if_not_exists(["Label"=>$label,"Alias"=>$valueColLabel,"Types"=>$typeAlias],$this->alias);
                                // $cplxValueColLabel=(!empty($singleCplxOption)) ? $valueColLabel."_".$singleCplxOption : $valueColLabel."_COMPL";
                                // UtilsHelper::push_array_if_not_exists($cplxValueColLabel, $this->head_temporaries[$field['type']][$label]);
                                // $is_using_rank_system = filter_var($parent_form['params'][$field_value]['global']['rank'] ?? false, FILTER_VALIDATE_BOOL);
                                // $allVal=[]
                                // foreach ($default_value as $response) {
                                //     $allVal[]=$response

                                    
                                // }
                                $output[$valueColLabel]=implode(";", $default_value);
                                // var_dump($gathered);exit;

                            }
                            $this->head_temporaries[$field['type']][$label] = $this->head_temporaries[$field['type']][$label] ?? [];
                            foreach ($parent_form['params']["checkboxNew".$field_value]['list'] as $choix) {
                                $header = $label."_".$choix;
                                if(isset($_POST['mappingLabel']) && (in_array($header,array_keys($_POST['mappingLabel'])) || in_array($choix,array_keys($_POST['mappingLabel'])))){
                                    $temporLabel= (in_array($choix,array_keys($_POST['mappingLabel']))!==false) ? $choix : $header;  
                                    $indexOption=array_search($temporLabel,array_keys($_POST['mappingLabel']));
                                    $header=($indexOption!==false) ? array_values($_POST['mappingLabel'])[$indexOption] : $header; 
                                    $output[$header]="0";
                                    UtilsHelper::push_array_if_not_exists($header, $this->head_temporaries[$field['type']][$label]);
                                    $typeAlias="booléen (boolean) (0;1)";
                                    UtilsHelper::push_array_if_not_exists(["Label"=>$temporLabel,"Alias"=>$header,"Types"=>$typeAlias],$this->alias);
                                    
                                    // $this->alias[]=[$temporLabel,$header];
                                }
                            }
                            
                            foreach ($default_value as $response) {
                                // var_dump($default_value);exit;
                                $new_index = $label . '_' . $response;
                                if ($_POST['settings']['checkbox'] === 'multicolumn'){
                                    $new_index = $label.'_'.$response;
                                    if(isset($_POST['mappingLabel']) && (in_array($response,array_keys($_POST['mappingLabel'])) || in_array($new_index,array_keys($_POST['mappingLabel'])) )){
                                        $temporLabel= (in_array($response,array_keys($_POST['mappingLabel']))!==false) ?  $response : $new_index; 
                                        $indexOption=array_search($temporLabel,array_keys($_POST['mappingLabel']));
                                        $new_index=($indexOption!==false) ? array_values($_POST['mappingLabel'])[$indexOption] : $new_index;
                                        $response="1";
                                        // $this->alias[]=[$temporLabel,$new_index];
                                        $typeAlias="booléen (boolean) (0;1)";
                                        UtilsHelper::push_array_if_not_exists(["Label"=>$temporLabel,"Alias"=>$new_index,"Types"=>$typeAlias],$this->alias);
                                    }
                                    UtilsHelper::push_array_if_not_exists($new_index, $this->head_temporaries[$field['type']][$label]);
                                    $output[$new_index] = $response;
                                    
                                }
                                UtilsHelper::push_array_if_not_exists($new_index, $this->head_temporaries[$field['type']][$label]);
                                $output[$new_index] = $response;
                            }
                        } else $output[$label] = implode(";", $default_value);
                        break;
                    case 'tpls.forms.tagsFix':
                    case 'tpls.forms.cplx.listing':
                    case 'tpls.forms.tags':
                        $output[$label] = implode(";", $default_value);
                        break;
                    case 'tpls.forms.uploader':
                        $output[$label] = '';
                        $connected_user = strval(Yii::app()->session['userId']);
                        $answer_files = Document::getListDocumentsWhere(['id' => (string)$answer['_id']], 'file');
                        foreach ($answer_files as $file) {
                            if (!empty($file['author']) && in_array($connected_user, [$parent_form['creator'], $answer['user'], $file['author']])) {
                                $output[$label] .= $file['folder'] . '/' . $file['name'] . '; ';
                            }
                        }
                        if (!empty($output[$label])) $output[$label] = substr($output[$label], 0, -2);
                        break;
                    case 'tpls.forms.cplx.simpleTable':
                        if (empty($this->head_temporaries[$field['type']][$label])) {
                            for ($i = 1; $i < count($default_value[0]); $i++) {
                                $this->head_temporaries[$field['type']][$label][] = $label . '_' . $default_value[0][$i];
                            }
                        }
                        
                        $length = (!empty($default_value)) ? count($default_value[0]) : 0;
                        for ($i = 1; $i < $length; $i++) {
                            $new_index = $label . '_' . $default_value[0][$i];
                            $exploded = [];
                            for ($j = 1; $j < count($default_value); $j++) {
                                if (!empty($default_value[$j][$i])) $exploded[] = strtolower($default_value[$j][$i]) === 'x' ? $default_value[$j][0] : $default_value[$j][0] . ' ' . $default_value[$j][$i];
                            }
                            $output[$new_index] = implode(";", $exploded);
                        }
                        break;
                    case 'tpls.forms.cplx.element':
                        foreach ($default_value as $element) {
                            $information = [];
                            switch ($element['type']) {
                                case 'organizations':
                                    $information = PHDB::findOneById(Organization::COLLECTION, $element['id'], ['name']);
                                    break;
                                case 'event':
                                case 'events':
                                    $information = PHDB::findOneById(Event::COLLECTION, $element['id'], ['name']);
                                    break;
                                case 'projects':
                                    $information = PHDB::findOneById(Project::COLLECTION, $element['id'], ['name']);
                                    break;
                                case 'poi':
                                    $information = PHDB::findOneById(Poi::COLLECTION, $element['id'], ['name']);
                                    break;
                            }
                            $new_index = $label . '_' . $information['name'];
                            $output[$new_index] = '"' . $information['name'] . '";"' . $element['id'] . '";"' . $element['type'] . '"';
                            UtilsHelper::push_array_if_not_exists($new_index, $this->head_temporaries[$field['type']][$label]);
                        }
                        break;
                    case 'tpls.forms.cplx.rangeTwoSliders':
                        $output[$label] = 'Entre ' . $default_value['min'] . ' et ' . $default_value['max'];
                        break;
                    case 'tpls.forms.select':
                        $choices = PHDB::findOneById(Form::COLLECTION, $_POST['query']['form'], ["params.$field_value.options"]);
                        $output[$label] = $choices['params'][$field_value]['options'][intval($default_value) - 1];
                        break;
                    case 'tpls.forms.cplx.multiCheckboxPlus':
                        // traitement de l'entête
                        $is_using_rank_system = filter_var($parent_form['params'][$field_value]['global']['rank'] ?? false, FILTER_VALIDATE_BOOL);
                        if ($_POST['settings']['checkbox'] === 'multicolumn') {
                            // loop for francetiers-lieux valeurs
                            if(isset($_POST['mappingLabel'])){
                                $nbCplx=(isset(array_count_values(array_values($parent_form['params'][$field_value]['tofill']))['cplx']) && isset($parent_form['params'][$field_value]['tofill'])) ? array_count_values(array_values($parent_form['params'][$field_value]['tofill']))['cplx'] : 0;
                                // var_dump($nbCplx);exit;
                                $indexSingleCplx=($nbCplx==1) ? array_search("cplx",array_values($parent_form['params'][$field_value]['tofill'])) : null;
                                $singleCplxOption=($indexSingleCplx!==null) ? array_keys($parent_form['params'][$field_value]['tofill'])[$indexSingleCplx] : ""; 
                                // var_dump($nbCplx,$indexSingleCplx,array_keys($parent_form['params'][$field_value]['tofill']),$singleCplxOption);exit;
                                $indexLabel=array_search($label,array_keys($_POST['mappingLabel']));
                                $valueColLabel=(isset($_POST['mappingLabel']) && $indexLabel!==false) ? array_values($_POST['mappingLabel'])[$indexLabel] : $label;
                                // $output[$valueColLabel] = $default_value ?? '';
                                // if(!empty($default_value)){
                                // var_dump($default_value);exit;
                                // }

                                $gathered=[];
                                $gatheredCplx=[];
                                UtilsHelper::push_array_if_not_exists($valueColLabel, $this->head_temporaries[$field['type']][$label]);
                                // $this->alias[$label]=$valueColLabel;
                                $typeAlias="chaîne de caractères (str) à choix multiples";
                                $optionList=implode(";",$parent_form['params'][$field_value]['global']['list']);
                                $rankType=($is_using_rank_system) ? ", classer par ordre d'importance" : "";
                                $typeAlias=$typeAlias.$rankType." (".$optionList.")";
                                UtilsHelper::push_array_if_not_exists(["Label"=>$label,"Alias"=>$valueColLabel,"Types"=>$typeAlias],$this->alias);
                                $cplxValueColLabel=(!empty($singleCplxOption)) ? $valueColLabel."_".$singleCplxOption : $valueColLabel."_COMPL";
                                //------ Pas besoin de regrouper les valeurs complexes dans une seule colonnes puiqu'elles sont détaillés dans plusieurs après
                                // UtilsHelper::push_array_if_not_exists($cplxValueColLabel, $this->head_temporaries[$field['type']][$label]);
                                // UtilsHelper::push_array_if_not_exists(["Label"=>$label."_COMPL","Alias"=>$cplxValueColLabel],$this->alias);
                                
                                // $is_using_rank_system = filter_var($parent_form['params'][$field_value]['global']['rank'] ?? false, FILTER_VALIDATE_BOOL);
                                foreach ($default_value as $responses) {
                                    foreach ($responses as $response => $question) {
                                        
                                        if (array_search('simple', $question) === 'type'){
                                            
                                            if ($is_using_rank_system && !empty($question['rank'])) $gathered[] = $question['rank'] . ' - ' . $response;
                                            else $gathered[] = $response;

                                        //    var_dump($response);exit;
                                            
    
                                            
                                        } else {
                                            // var_dump($response);exit;
                                            if($is_using_rank_system && !empty($question['rank'])) {
                                                $gatheredCplx[] = (!empty($singleCplxOption)) ? $question['rank'] . ' - '.$question['textsup'] : $question['rank'] . ' - ' .$question['value']." : ".$question['textsup'];
                                            }else {
                                                //--- Différence dans les valeurs : si une seule réponse compléxe (option dans le header de la colonne) sinon option : valeur
                                                // $gatheredCplx[] = (!empty($singleCplxOption)) ? $question['textsup'] : $question['value']." : ".$question['textsup'];
                                                if((isset($parent_form['params'][$field_value]['global']['cplxType']) && $parent_form['params'][$field_value]['global']['cplxType']=="number")){
                                                    $gatheredCplx[] = $question['value'];
                                                    $cplxValueColLabel=$valueColLabel;
                                                }

                                            }    
                                        }
                                    }
                                }
                                // var_dump($gathered);exit;

                            }
                            if (!empty($gathered)) {
                                // var_dump($default_value);exit;
                                // var_dump($gathered);exit;
                                $output[$valueColLabel] = implode(";", $gathered);
                            }
                            if (!empty($gatheredCplx)){
                                $output[$cplxValueColLabel] = implode(";", $gatheredCplx);
                                // $output[$valueColLabel] = "";
                            }

                            if($cplxValueColLabel==$valueColLabel){
                                $output[$valueColLabel]=implode(";",array_merge($gathered,$gatheredCplx));
                            }    

                            foreach ($parent_form['params'][$field_value]['global']['list'] as $choix) {
                                $header = $label."_".$choix;
                                if(isset($_POST['mappingLabel']) && (in_array($header,array_keys($_POST['mappingLabel'])) || in_array($choix,array_keys($_POST['mappingLabel'])))){
                                    // $offsetArray=(isset($indexLabel)) ? $indexLabel : 0 ;
                                    $temporLabel= (in_array($choix,array_keys($_POST['mappingLabel']))!==false) ? $choix : $header;  
                                    // var_dump($temporLabel);
                                    $indexOption=array_search($temporLabel,array_keys($_POST['mappingLabel']));
                                    $header=($indexOption!==false) ? array_values($_POST['mappingLabel'])[$indexOption] : $header; 
                                    $output[$header]="0";
                                    if((isset($parent_form['params'][$field_value]["tofill"][$choix]) && $parent_form['params'][$field_value]["tofill"][$choix]=="simple") || !isset($parent_form['params'][$field_value]['global']['cplxType']) || (isset($parent_form['params'][$field_value]['global']['cplxType']) && $parent_form['params'][$field_value]['global']['cplxType']!=="number")){
                                        UtilsHelper::push_array_if_not_exists($header, $this->head_temporaries[$field['type']][$label]);
                                    }    
                                    // $this->alias[$temporLabel]=$header;
                                    // $this->alias[]=["Label"=>$temporLabel,"Alias"=>$header];
                                    $typeAlias="booléen (boolean) (0;1)";
                                    // var_dump($parent_form['params'][$field_value]['global']['nbAnswersMax']);exit;
                                    $rankRange=(!empty($parent_form['params'][$field_value]['global']['nbAnswersMax'])) ? "(0 - ".$parent_form['params'][$field_value]['global']['nbAnswersMax'].")" : ""; 
                                    $typeAlias=($is_using_rank_system) ? "nombre (number) ".$rankRange : $typeAlias;
                                    if((isset($parent_form['params'][$field_value]["tofill"][$choix]) && $parent_form['params'][$field_value]["tofill"][$choix]=="simple") || !isset($parent_form['params'][$field_value]['global']['cplxType']) || (isset($parent_form['params'][$field_value]['global']['cplxType']) && $parent_form['params'][$field_value]['global']['cplxType']!=="number")){
                                        UtilsHelper::push_array_if_not_exists(["Label"=>$temporLabel,"Alias"=>$header,"Types"=>$typeAlias],$this->alias);
                                    }
                                    if(isset($parent_form['params'][$field_value]['tofill'][$choix]) && $parent_form['params'][$field_value]['tofill'][$choix]=="cplx"){
                                        // var_dump($_POST['mappingLabel'][$temporLabel."_COMPL"],$indexOption);exit;
                                        $aliasCompl=(isset($_POST['mappingLabel'][$temporLabel."_COMPL"])) ? $_POST['mappingLabel'][$temporLabel."_COMPL"] : $header."_COMPL";
                                        UtilsHelper::push_array_if_not_exists($aliasCompl, $this->head_temporaries[$field['type']][$label]);
                                        // $this->alias[$temporLabel]=$header."_COMPL";
                                        // $this->alias[]=["Label"=>$temporLabel,"Alias"=>$header."_COMPL"];
                                        // $regex="/(^\d*[.,]?\d*$)?/";
                                        // $number="0,6a55653";
                                        // preg_match($regex,$number,$res);

                                        // type des champs cplx (exemple CDI => number)
                                        $typeAlias=(!empty($parent_form['params'][$field_value]['global']['cplxType']) && $parent_form['params'][$field_value]['global']['cplxType']=="number") ? "nombre (number)" : "chaîne de caractères (str)";
                                        // var_dump($this->alias);
                                        UtilsHelper::push_array_if_not_exists(["Label"=>$temporLabel."_COMPL","Alias"=>$aliasCompl,"Types"=>$typeAlias],$this->alias);
                                        // var_dump($this->alias);exit;
                                    }
                                }
                                else{
                                    UtilsHelper::push_array_if_not_exists($header, $this->head_temporaries[$field['type']][$label]);
                                    // $alias[$header]=$header;
                                }
                                // var_dump($header);
                               
                            }
                            // exit;
                        }
                        // traitement dun contenu
                        $grouped = [];
                        
                        // Ranger les réponses par rang
                        $default_value_length = count($default_value);
                        do {
                            $permutation = false;
                            for ($index = 0; $index < $default_value_length - 1; $index++) {
                                $current = $default_value[$index];
                                $next = $default_value[$index + 1];
                                $current_content = end($current);
                                $next_content = end($next);
                                if (!empty($current_content['rank']) && !empty($next_content['rank']) && intval($current_content['rank']) > intval($next_content['rank'])) {
                                    $default_value[$index + 1] = $current;
                                    $default_value[$index] = $next;
                                    $permutation = true;
                                }
                            }
                        } while ($permutation);
                        
                        
                        foreach ($default_value as $responses) {
                            foreach ($responses as $response => $question) {
                                if (array_search('simple', $question) === 'type'){
                                    if ($_POST['settings']['checkbox'] === 'multicolumn'){
                                        // var_dump($response,$question['value']);exit;
                                        $new_index = $label.'_'.$response;
                                        if(isset($_POST['mappingLabel']) && (in_array($response,array_keys($_POST['mappingLabel'])) || in_array($new_index,array_keys($_POST['mappingLabel'])) )){  
                                            $temporLabel= (in_array($response,array_keys($_POST['mappingLabel']))!==false) ?  $response : $new_index; 
                                            $indexOption=array_search($temporLabel,array_keys($_POST['mappingLabel']));
                                            $new_index=($indexOption!==false) ? array_values($_POST['mappingLabel'])[$indexOption] : $new_index;
                                            
                                            // $temporLabel= (in_array($choix,array_keys($_POST['mappingLabel']))!==false) ? $choix : $header;  
                                            // $indexOption=array_search($temporLabel,array_keys($_POST['mappingLabel']));
                                            // $header=($indexOption!==false) ? array_values($_POST['mappingLabel'])[$indexOption] : $header; 
                                            $response=($is_using_rank_system && !empty($question["rank"])) ? $question["rank"] : "1";
                                            // $bodies[$new_index] = $valueReturned;
                                            // if(isset($_POST['mappingLabel']) && in_array($response,array_keys($_POST['mappingLabel']))){
                                                //     $offsetArray=(isset($indexLabel)) ? $indexLabel : 0 ;
                                                //     $indexOption=array_search($response,array_slice(array_keys($_POST['mappingLabel']),$offsetArray,null,true));
                                                //     $new_index=($indexOption!==false) ? array_values($_POST['mappingLabel'])[$indexOption] : $new_index; 
                                                    
                                                // }
                                            // $this->alias[]=["Label"=>$temporLabel,"Alias"=>$new_index];
                                            // UtilsHelper::push_array_if_not_exists(["Label"=>$temporLabel,"Alias"=>$new_index],$this->alias);
                                            // $this->alias[$temporLabel]=$new_index;
                                        }else{
                                            // var_dump($new_index);exit;
                                        }
                                        UtilsHelper::push_array_if_not_exists($new_index, $this->head_temporaries[$field['type']][$label]);
                                        $output[$new_index] = $response;
                                        
                                    } else {
                                        if ($is_using_rank_system && !empty($question['rank'])) $grouped[] = $question['rank'] . ' - ' . $response;
                                        else $grouped[] = $response;
                                    }
                                } else {
                                    if ($_POST['settings']['checkbox'] === 'multicolumn' || $must_put_complex_checkbox_aside) {
                                            $new_index = $label. '_' . $question['value'];
                                            $valueReturned=$question['textsup'];
                                            if(isset($_POST['mappingLabel']) && (in_array($response,array_keys($_POST['mappingLabel'])) || in_array($new_index,array_keys($_POST['mappingLabel'])))){
                                                $temporLabel= (in_array($new_index,array_keys($_POST['mappingLabel']))!==false) ?  $new_index : $response;  
                                                $indexOption=array_search($temporLabel,array_keys($_POST['mappingLabel']));
                                                $new_index=($indexOption!==false) ? array_values($_POST['mappingLabel'])[$indexOption] : $new_index;

                                                $aliasCompl=(isset($_POST['mappingLabel'][$temporLabel."_COMPL"])) ? $_POST['mappingLabel'][$temporLabel."_COMPL"] : $new_index."_COMPL";

                                                // add cplx value in another column
                                                UtilsHelper::push_array_if_not_exists($aliasCompl, $this->head_temporaries[$field['type']][$label]);
                                                $output[$aliasCompl] = $question['textsup'];
                                                // $this->alias[$temporLabel]=$new_index.'_COMPL';
                                                // $this->alias[]=["Label"=>$temporLabel,"Alias"=>$new_index.'_COMPL'];
                                                // $regex="/(^\d*[.,]?\d*$)?/";
                                                // $strType=$question['textsup'];
                                                // var_dump($question["textsup"]);exit;
                                                // preg_match($regex,$strType,$res);
                                                // $typeAlias=(!empty($res[0])) ? "nombre (number)" : "chaîne de caractères (str)";
                                                // UtilsHelper::push_array_if_not_exists(["Label"=>$temporLabel,"Alias"=>$new_index.'_COMPL',"Types"=>$typeAlias],$this->alias);
                                                
                                                $valueReturned=($is_using_rank_system && !empty($question["rank"])) ? $question["rank"] : "1";
                                            }
                                            if(!isset($parent_form['params'][$field_value]['global']['cplxType']) || (isset($parent_form['params'][$field_value]['global']['cplxType']) && $parent_form['params'][$field_value]['global']['cplxType']!=="number")){
                                                UtilsHelper::push_array_if_not_exists($new_index, $this->head_temporaries[$field['type']][$label]);
                                            }
                                            $output[$new_index] = $valueReturned;
                                            // $alias[$valueReturned]=$new_index;
                                            
                                    } else {
                                        if ($is_using_rank_system && !empty($question['rank'])) $grouped[] = $question['rank'] . ' - ' . $question['value'] . '_' . $question['textsup'];
                                        else $grouped[] = $question['value'] . '_' . $question['textsup'];
                                    }
                                }
                            }
                        }
                        if (!empty($grouped)) $output[$label] = implode(";", $grouped);
                        break;
                    case 'tpls.forms.cplx.finder':
                    case 'tpls.forms.costum.franceTierslieux.finder':
                        $count = count($default_value);
                        
                        if ($_POST['settings']['finder'] === 'complete') {
                            $column_counter = 0;
                            foreach ($default_value as $finder) {
                                $column_counter++;
                                $new_index = $count > 1 ? "$label : Colonne $column_counter" : $label;
                                $output[$new_index] = '"' . $finder['name'] . '";"' . $finder['id'] . '";"' . $finder['type'] . '"';
                                UtilsHelper::push_array_if_not_exists($new_index, $this->head_temporaries[$field['type']][$label]);
                            }
                        } else if ($_POST['settings']['finder_column'] === 'monocolumn') {
                            $grouped = [];
                            foreach ($default_value as $finder) {
                                $grouped[] = $finder['name'];
                                UtilsHelper::push_array_if_not_exists($label, $this->head_temporaries[$field['type']][$label]);
                            }
                            $output[$label] = implode(";", $grouped);
                        } else {
                            $column_counter = 0;
                            foreach ($default_value as $finder) {
                                $column_counter++;
                                $new_index = $count > 1 ? "$label : Colonne $column_counter" : $label;
                                $output[$new_index] = $finder['name'];
                                UtilsHelper::push_array_if_not_exists($new_index, $this->head_temporaries[$field['type']][$label]);
                            }
                        }
                        break;
                    case 'tpls.forms.cplx.radioNew':
                        if ($_POST['settings']['checkbox'] === 'multicolumn') {
                            // var_dump(array_search($label,array_keys($_POST['mappingLabel'])));exit;
                            // var_dump($parent_form['params'][$field_value]['global']['list']);exit;
                            $optionList=$parent_form['params']["radioNew".$field_value]['list'];
                            if(isset($_POST['mappingLabel'])){
                                $indexLabel=array_search($label,array_keys($_POST['mappingLabel']));
                                $valueColLabel=(isset($_POST['mappingLabel']) && $indexLabel!==false) ? array_values($_POST['mappingLabel'])[$indexLabel] : $label;
                                $output[$valueColLabel] = $default_value ?? '';
                                UtilsHelper::push_array_if_not_exists($valueColLabel, $this->head_temporaries[$field['type']][$label]);
                                // $this->alias[$label]=$valueColLabel;
                                // $this->alias[]=["Label"=>$label,"Alias"=>$valueColLabel];
                                $typeAlias="chaînes de caractères (str) à choix multiples";
                                $typeAlias=$typeAlias." (".implode(';',$optionList).")";
                                UtilsHelper::push_array_if_not_exists(["Label"=>$label,"Alias"=>$valueColLabel,"Types"=>$typeAlias],$this->alias);

                            }
                            
                            
                            
                            foreach ($optionList as $choix) {
                                // $choix=str_replace(" ","_",$choix);
                                $header = $label."_".$choix;
                                                      
                                if(isset($_POST['mappingLabel']) && (in_array($header,array_keys($_POST['mappingLabel'])) || in_array($choix,array_keys($_POST['mappingLabel'])))){
                                    $temporLabel= (in_array($choix,array_keys($_POST['mappingLabel']))!==false) ? $choix : $header;  
                                    $indexOption=array_search($temporLabel,array_keys($_POST['mappingLabel']));
                                    $header=($indexOption!==false) ? array_values($_POST['mappingLabel'])[$indexOption] : $header;                 
                                    $output[$header]="0";
                                    $temporLabel[$temporLabel]=$header;
                                    $alias=(isset($_POST['mappingLabel'][$header])) ? $_POST['mappingLabel'][$header] : $header;
                                    $typeAlias="booléen (boolean) (0;1)";
                                    UtilsHelper::push_array_if_not_exists(["Label"=>$label."_".$choix,"Alias"=>$alias,"Types"=>$typeAlias],$this->alias);
                                    
                                    
                                }else{
                                    $output[$header]="";
                                }
                                // $choix=str_replace("_"," ",$choix);
                                if($choix==$default_value){
                                    $checkedHeader=$header;
                               }
                                
                                UtilsHelper::push_array_if_not_exists($header, $this->head_temporaries[$field['type']][$label]);
                                
                            }
                            UtilsHelper::push_array_if_not_exists($header, $this->head_temporaries[$field['type']][$label]);
                        }
                        if(isset($checkedHeader)){
                            if(isset($_POST['mappingLabel']) && (in_array($checkedHeader,array_values($_POST['mappingLabel'])))){
                                $output[$checkedHeader] = "1";
                            }else{
                                $output[$checkedHeader] = $default_value;
                            }
                        }else{
                            $output[$header] = (empty($default_value) && $_POST['mappingLabel']) && (in_array($header,array_values($_POST['mappingLabel']))) ? "0" : $default_value;
                        }    
                        break;
                    case 'tpls.forms.cplx.multiRadio':
                        if ($_POST['settings']['checkbox'] === 'multicolumn') {
                            // var_dump(array_search($label,array_keys($_POST['mappingLabel'])));exit;
                            // var_dump($parent_form['params'][$field_value]['global']['list']);exit;
                            $optionList=$parent_form['params'][$field_value]['global']['list'];

                            // Donner les valeurs sur une colonne quand mapping

                            // $nbCplx=(isset(array_count_values(array_values($parent_form['params'][$field_value]['tofill']))['cplx']) && isset($parent_form['params'][$field_value]['tofill'])) ? array_count_values(array_values($parent_form['params'][$field_value]['tofill']))['cplx'] : 0;
                            // var_dump($nbCplx ,$label);exit;

                            if(isset($_POST['mappingLabel'])){
                                $indexLabel=array_search($label,array_keys($_POST['mappingLabel']));
                                $valueColLabel=($indexLabel!==false) ? array_values($_POST['mappingLabel'])[$indexLabel] : $label;
                                if (array_search('simple', $default_value) === 'type') $output[$valueColLabel] = $default_value['value'] ?? '';
                                else $output[$valueColLabel] =  (!empty($default_value)) ? $default_value['value']." : ".$default_value['textsup'] :  '';
                                UtilsHelper::push_array_if_not_exists($valueColLabel, $this->head_temporaries[$field['type']][$label]);
                                // $this->alias[$label]=$valueColLabel;
                                // $this->alias[]=["Label"=>$label,"Alias"=>$valueColLabel];  
                                $typeAlias="chaînes de caractères (str) à choix multiples";
                                $typeAlias=$typeAlias." (".implode(";",$optionList).")";
                                UtilsHelper::push_array_if_not_exists(["Label"=>$label,"Alias"=>$valueColLabel,"Types"=>$typeAlias],$this->alias);                            

                            }

                            
                            
                            

                            
                            foreach ($optionList as $choix) {
                                // $choix=str_replace(" ","_",$choix);
                                $header = $label."_".$choix;
                                // $originalHeader=$header
                                // var_dump($choix,$_Po)       
                                // var_dump(in_array($choix,array_keys($_POST['mappingLabel'])));exit;               
                                if(isset($_POST['mappingLabel']) && (in_array($header,array_keys($_POST['mappingLabel'])) || in_array($choix,array_keys($_POST['mappingLabel'])))){
                                    $temporLabel= (in_array($choix,array_keys($_POST['mappingLabel']))!==false) ? $choix : $header;  
                                    $indexOption=array_search($temporLabel,array_keys($_POST['mappingLabel']));
                                    $header=($indexOption!==false) ? array_values($_POST['mappingLabel'])[$indexOption] : $header;                 
                                    $output[$header]="0";
                                    UtilsHelper::push_array_if_not_exists($header, $this->head_temporaries[$field['type']][$label]);
                                    // $this->alias[$temporLabel]=$header;    
                                    // $this->alias[]=["Label"=>$temporLabel,"Alias"=>$header];
                                    if($temporLabel!==$header){
                                       $typeAlias="booléen (boolean) (0;1)";
                                       UtilsHelper::push_array_if_not_exists(["Label"=>$temporLabel,"Alias"=>$header,"Types"=>$typeAlias],$this->alias);
                                    }   
                                    if(isset($parent_form['params'][$field_value]['tofill']) && $parent_form['params'][$field_value]['tofill'][$choix]=="cplx"){
                                        $aliasCompl=(isset($_POST['mappingLabel'][$temporLabel."_COMPL"])) ? $_POST['mappingLabel'][$temporLabel."_COMPL"] : $header."_COMPL";
                                        UtilsHelper::push_array_if_not_exists($aliasCompl, $this->head_temporaries[$field['type']][$label]);
                                        // $this->alias[$temporLabel]=$header."_COMPL";    
                                        // $this->alias[]=["Label"=>$temporLabel,"Alias"=>$header."_COMPL"];
                                        $typeAlias=(!empty($parent_form['params'][$field_value]['cplxType']) && $parent_form['params'][$field_value]['cplxType']=="number") ? "nombre (number)" : "chaîne de caractères (str)";
                                        UtilsHelper::push_array_if_not_exists(["Label"=>$temporLabel."_COMPL","Alias"=>$aliasCompl,"Types"=>$typeAlias],$this->alias);   
                                    }
                                    
                                    
                                    
                                    
                                }else{
                                    // var_dump($header);exit;
                                    $output[$header]="";
                                    UtilsHelper::push_array_if_not_exists($header, $this->head_temporaries[$field['type']][$label]);
                                }
                                // var_dump($default_value);exit;
                                // $choix=str_replace("_"," ",$choix);
                                if(is_array($default_value) && $choix==$default_value['value']){
                                    $checkedHeader=$header;
                                }
                                
                                
                                
                            }
                            UtilsHelper::push_array_if_not_exists($header, $this->head_temporaries[$field['type']][$label]);
                        }
                        
                        if (array_search('cplx', $default_value) === 'type'){
                            // var_dump($checkedHeader);
                            // var_dump($default_value);exit;
                            if(isset($checkedHeader)){ 
                                if(isset($_POST['mappingLabel']) && (in_array($checkedHeader,array_values($_POST['mappingLabel'])))){
                                    $output[$checkedHeader] = "1";
                                    $labelCplx=$label."_".$default_value["value"]."_COMPL";
                                    
                                    $aliasCompl=(isset($_POST['mappingLabel'][$labelCplx])) ? $_POST['mappingLabel'][$labelCplx] : $checkedHeader."_COMPL";
                                    // var_dump($temporLabel,$_POST['mappingLabel'][$labelCplx],$aliasCompl);exit;
                                    UtilsHelper::push_array_if_not_exists($aliasCompl, $this->head_temporaries[$field['type']][$label]);
                                    $output[$aliasCompl] = $default_value['textsup'];
                                    

                                    // UtilsHelper::push_array_if_not_exists(["Label"=>$checkedHeader.'_COMPL',"Alias"=>$checkedHeader."_COMPL"],$this->alias);
                                }else if(is_array($default_value)){
                                    $output[$checkedHeader] =$default_value['textsup'];
                                }   
                               
                            }else{
                                // var_dump($header);exit;
                                $output[$header] = $default_value['value'];
                            }
                        }
                        else{
                            if(isset($checkedHeader)){ 
                                if(isset($_POST['mappingLabel']) && (in_array($checkedHeader,array_values($_POST['mappingLabel'])))){
                                    $output[$checkedHeader] = "1";
                                }else if(is_array($default_value)){
                                    $output[$checkedHeader] = $default_value['value'];
                                }                                
                            }else if(is_array($default_value)){
                                // var_dump($header);exit;
                                $output[$header] = $default_value['value'];
                            }                          
                        }
                        // if (array_search('simple', $default_value) === 'type') $output[$header] = $default_value['value'];
                        // else $output[$header] = $default_value['textsup'] ?? '';
                        break;
                    case 'tpls.forms.evaluation.evaluation':
                        $criterias = $parent_form['params']['criterias' . $field['value']] ?? [];
                        $default_value = is_array($default_value) ? $default_value : [];
                        foreach ($default_value as $evaluation_label => $criteria) {
                            $key = array_keys($criteria)[0];
                            $new_index = "$label : $evaluation_label";
                            $output[$new_index] = $criterias[$key]['name'];
                            UtilsHelper::push_array_if_not_exists($new_index, $this->head_temporaries[$field['type']][$label]);
                        }
                        break;
                    default:
                        $header=$label;
                        
                        if($_POST["mappingLabel"] && isset($_POST["mappingLabel"][$label])){                
                            $header=$_POST["mappingLabel"][$label];
                            // $this->alias[$label]=$header;
                            // $this->alias[]=["Label"=>$label,"Alias"=>$header];
                            $typeAlias=($field["type"]=="number") ? "nombre (number)" : "chaîne de caractères (str)";
                            // var_dump($label,$step_key);exit;
                            UtilsHelper::push_array_if_not_exists(["Label"=>$label,"Alias"=>$header,"Types"=>$typeAlias],$this->alias);
                            UtilsHelper::push_array_if_not_exists($header, $this->head_temporaries[$field['type']][$label]);
                        } 
                        // var_dump($header);exit;
                    
                        $output[$header] = $default_value;
                        break;
                }
            }

            //merge with generic field only if answer form inputs exist 
            if(!empty($output)){
                $output=array_merge_recursive($generaticStartFields,$output);
            }
           
            // enlever les lignes vides
            foreach ($output as $output_key => $output_value) {
                if (!empty($output_value) && !preg_match('/^([0-9]*.\s)?email$/i', $output_key)) return $output;
            }
            return [];
        }
        
        private function reorder_form_inputs(array &$inputs, array &$excluded_inputs): array {
            $ordered_inputs = [];
            $unordered_inputs = [];
            foreach ($inputs as $input_key => $input) {
                if (!empty($input['type']) && !in_array($input['type'], $excluded_inputs)) {
                    $label = $input_key;
                    if (!empty($input['label'])) $label = $input['label'];
                    else if (!empty($input['info'])) $label = $input['info'];
                    
                    $label = htmlspecialchars($label);
                    
                    if (isset($input['position'])) {
                        // sélectionner la position de l'élément dans le formulaire
                        $position = intval($input['position']);
                        
                        $ordered_inputs[] = [
                            'label'    => $label,
                            'value'    => $input_key,
                            'position' => $position,
                            'type'     => $input['type']
                        ];
                    } else {
                        $unordered_inputs[] = [
                            'label' => $label,
                            'value' => $input_key,
                            'type'  => $input['type']
                        ];
                    }
                }
            }
            
            // reordonner selon les ordres fournis dans la base de données
            do {
                $permutation = false;
                $length = count($ordered_inputs);
                for ($i = 0; $i < $length - 1; $i++) {
                    if ($ordered_inputs[$i]['position'] > $ordered_inputs[$i + 1]['position']) {
                        $permutation = true;
                        [$ordered_inputs[$i], $ordered_inputs[$i + 1]] = [$ordered_inputs[$i + 1], $ordered_inputs[$i]];
                    }
                }
            } while ($permutation);
            
            foreach ($ordered_inputs as $input_index => $ordered_input) {
                unset($ordered_input['position']);
                $ordered_inputs[$input_index] = $ordered_input;
            }
            return [...$ordered_inputs, ...$unordered_inputs];
        }
        
        private function get_headers_at_step(array &$fields): array {
            $costum=CacheHelper::getCostum();
            $output = [];
            if (filter_var($_POST['is_exporting_answer_id'], FILTER_VALIDATE_BOOLEAN)) {
                $output[] = 'ID_REP';
            }
            if(!empty($costum)){
                if (filter_var($_POST['is_exporting_user_email'] ?? false, FILTER_VALIDATE_BOOLEAN) && Authorisation::isInterfaceAdmin($costum["contextId"], $costum["contextType"])) {
                    $output[] = 'EMAIL_REPONDANT';
                }
            }    
            foreach ($fields as $field) {
                // Arranger les champs
                $label = trim(htmlspecialchars_decode((str_replace('  ', ' ', $field['label']))));
                // $label=str_replace(" ","_",$label);
                switch ($field['type']) {
                    case 'tpls.forms.cplx.address':
                        if ($_POST['settings']['address'] == 'multicolumn') $output = [
                            ...$output, ...[
                                'StreetAddress',
                                'postalCode',
                                'addressLocality',
                                'addressCountry'
                            ]
                        ];
                        else $output[] = $label;
                        break;
                    case 'tpls.forms.cplx.checkboxNew':
                    case 'tpls.forms.cplx.multiCheckboxPlus':
                        if ($_POST['settings']['checkbox'] != 'multicolumn') $output[] = $label;
                        if (!empty($this->head_temporaries[$field['type']][$label])) $output = [
                            ...$output,
                            ...$this->head_temporaries[$field['type']][$label]
                        ];
                        break;
                    case 'tpls.forms.cplx.multiCheckboxPlus':
                        if ($_POST['settings']['checkbox'] != 'multicolumn') $output[] = $label;
                        if (!empty($this->head_temporaries[$field['type']][$label])) $output = [
                            ...$output,
                            ...$this->head_temporaries[$field['type']][$label]
                        ];
                        break;
                    case 'tpls.forms.cplx.radioNew':
                    case 'tpls.forms.cplx.multiRadio':
                        if ($_POST['settings']['checkbox'] != 'multicolumn') $output[] = $label;
                        if (!empty($this->head_temporaries[$field['type']][$label])) $output = [
                            ...$output,
                            ...$this->head_temporaries[$field['type']][$label]
                        ];
                    break;
                    case 'tpls.forms.cplx.radioNew':
                            if ($_POST['settings']['checkbox'] != 'multicolumn') $output[] = $label;
                            if (!empty($this->head_temporaries[$field['type']][$label])) $output = [
                                ...$output,
                                ...$this->head_temporaries[$field['type']][$label]
                            ];
                            break;                      
                    case 'tpls.forms.cplx.element':
                    case 'tpls.forms.cplx.simpleTable':
                    case 'tpls.forms.evaluation.evaluation':
                    case 'tpls.forms.cplx.finder':
                    case 'tpls.forms.costum.franceTierslieux.finder':
                        if (!empty($this->head_temporaries[$field['type']][$label])) $output = [
                            ...$output,
                            ...$this->head_temporaries[$field['type']][$label]
                        ];
                        break;
                    default:
                        if (!empty($this->head_temporaries[$field['type']][$label])) $output = [
                            ...$output,
                            ...$this->head_temporaries[$field['type']][$label]
                        ];
                        // $output[] = $label;
                        break;
                }
            }
            return $output;
        }
        
        private function get_dynform_exportation_mapping(string $__element_type, string $__form_id, array $__fields = [], $__occurence = 0): array {
            $costum = CacheHelper::getCostum();
            
            // Supprimer Autre
            if (!empty($costum['lists']['manageModel']) && in_array('Autre', $costum['lists']['manageModel'])) {
                $index = array_search('Autre', $costum['lists']['manageModel']);
                array_splice($costum['lists']['manageModel'], $index, 1);
            }
            
            $column_occurence = ($__occurence > 0 ? "$__occurence. " : '') . '(DF) ';
            $default = [
                'organizations' => [
                    [
                        'fields'       => ['name'],
                        'formats'      => ['{0}'],
                        'column_names' => [$column_occurence . 'Nom de l\'organisation']
                    ],
                    [
                        'fields'     => ['address'],
                        'conditions' => [
                            'monocolumn'  => [
                                'fields'       => [
                                    'address.streetAddress', 'address.postalCode', 'address.addressLocality', 'address.addressCountry'
                                ],
                                'formats'      => ['{0};{1};{2};{3}'],
                                'column_names' => [$column_occurence . 'Adresse']
                            ],
                            'multicolumn' => [
                                'fields'       => [
                                    'address.streetAddress', 'address.postalCode', 'address.addressLocality', 'address.addressCountry'
                                ],
                                'formats'      => [
                                    '{0}',
                                    '{1}',
                                    '{2}',
                                    '{3}'
                                ],
                                'column_names' => [
                                    $column_occurence . 'StreetAddress',
                                    $column_occurence . 'Code postal',
                                    $column_occurence . 'addressLocality',
                                    $column_occurence . 'addressCountry'
                                ]
                            ]
                        ]
                    ]
                ],
                'projects'      => []
            ];
            $organizations = [
                'followingAnswers' => [
                    [
                        'fields'       => 'idRecensementTL2023',
                        'formats'      => '{0}',
                        'column_names' => 'ID_UNIQUE',
                        'field_type'    => 'nombre (number)'

                    ],
                    // [
                    //     'fields'       => 'idRecensement23',
                    //     'formats'      => '{0}',
                    //     'column_names' => 'ID_REP',
                    //     'field_type'    => 'nombre (number)'

                    // ],
                    [
                        'fields'       => 'name',
                        'formats'      => '{0}',
                        'column_names' => 'NOM',
                        'field_type'   => 'chaîne de caractères (str)'
                    ],
                    [
                        'fields'     => 'address',
                        'conditions' => [
                            'monocolumn'  => [
                                'fields'       => [
                                    'address.streetAddress',
                                    'address.postalCode',
                                    'address.addressLocality',
                                    'address.addressCountry',
                                    'address.level3Name',
                                    // 'address.level4Name'
                                ],
                                'formats'      => ['{0};{1};{2};{3};{4}'],
                                'column_names' => ['ADRESSE'],
                                'field_type' => [
                                    'chaîne de caractères (str)',
                                    'nombre (number)',
                                    'chaîne de caractères (str)',
                                    'chaîne de caractères (str) (FR, GF, GP, MQ, NC, RE, YT, case vide)',
                                    'chaînes de caractères à choix multiples (Auvergne Rhône-Alpes;Bourgogne-France-Comté;Bretagne;Centre-Val de Loire;Corse;Grand Est;Hauts-de-France;Ile-de-France;Normandie;Nouvelle-Aquitaine;Occitanie;Pays de la Loire;Provence Alpes Côte d\'Azur;Guadeloupe;Guyane;Martinique;Mayotte;Réunion;Nouvelle Calédonie)',
                                    // 'chaînes de caractères à choix multiples (liste des départements)'
                                ]
                            ],
                            'multicolumn' => [
                                'fields'       => [
                                    'address.streetAddress',
                                    'address.postalCode',
                                    'address.addressLocality',
                                    // 'address.addressCountry',
                                    'address.level3Name',
                                    // 'address.level4Name'
                                ],
                                'formats'      => [
                                    '{0}',
                                    '{1}',
                                    '{2}',
                                    // '{3}',
                                    '{3}',
                                    // '{4}'
                                ],
                                'column_names' => [
                                    'ADRESSE',
                                    'CODPOST',
                                    'VILLE',
                                    // 'PAYS',
                                    'REGION',
                                    // 'DEP'
                                ],
                                'field_type' => [
                                    'chaîne de caractères (str)',
                                    'nombre (number)',
                                    'chaîne de caractères (str)',
                                    // 'chaîne de caractères (str) (FR, GF, GP, MQ, NC, RE, YT, case vide)',
                                    'chaînes de caractères à choix multiples (Auvergne Rhône-Alpes;Bourgogne-France-Comté;Bretagne;Centre-Val de Loire;Corse;Grand Est;Hauts-de-France;Ile-de-France;Normandie;Nouvelle-Aquitaine;Occitanie;Pays de la Loire;Provence Alpes Côte d\'Azur;Guadeloupe;Guyane;Martinique;Mayotte;Réunion;Nouvelle Calédonie)',
                                    // 'chaînes de caractères à choix multiples (liste des départements)'
                                ]
                            ]
                        ]
                    ]
                ],   
                // enquête france tiers-lieux 2023
                '63e0a8abeac0741b506fb4f7' => [
                    [
                        'fields'       => 'idRecensementTL2023',
                        'formats'      => '{0}',
                        'column_names' => 'ID_UNIQUE',
                        'field_type'   => 'nombre (number)'

                    ],
                    [
                        'fields'       => 'name',
                        'formats'      => '{0}',
                        'column_names' => 'NOM',
                        'field_type'   => 'chaîne de caractères (str)'
                    ],
                    [
                        'fields'       => 'address.codeInsee',
                        'formats'      => '{0}',
                        'column_names' => 'INSEE',
                        'field_type'   => 'nombre (number)'
                    ],
                    [
                        'fields'       => 'address.inseeDensity',
                        'formats'      => '{0}',
                        'column_names' => 'DECOUP',
                        'field_type'   => 'nombre (number)'
                    ],
                    [
                        'fields'       => 'idRecensement23',
                        'formats'      => '{0}',
                        'column_names' => 'ID_REP',
                        'field_type'   => 'chaîne de caractères (str)'
                    ],
                    // [
                    //     'fields'       => '',
                    //     'formats'      => '{0}',
                    //     'column_names' => 'EMAIL_REPONDANTS',
                    //     'field_type'   => 'chaîne de caractères (str) (adresse email)'
                    // ],
                    [
                        'fields'       => 'openingDate',
                        'formats'      => '{0}',
                        'column_names' => 'DATE_OUV',
                        'field_type'   => 'date (date)'
                    ],
                    [
                        'fields'       => 'shortDescription',
                        'formats'      => '{0}',
                        'column_names' => 'DESCRI_COU',
                        'field_type'   => 'chaîne de caractères (str)'
                    ],
                    [
                        'fields'       => 'holderOrganization',
                        'formats'      => '{0}',
                        'column_names' => 'NOM_STRC_PORT',
                        'field_type'   => 'chaîne de caractères (str)'
                    ],
                    [
                        'fields'       => 'tags',
                        'label'        => 'Mode de gestion',
                        'array_filter' => $costum['lists']['manageModel'] ?? [],
                        'array_values' => [],
                        'transform'    => [
                            'callable' => 'end',
                            'args'     => [
                                ['raw' => true, 'value' => 'array_values']
                            ]
                        ],
                        'formats'      => '{0}',
                        'column_names' => 'GEST',
                        'field_type'   => 'chaîne de caractères (str) à choix multiples'
                    ],
                    [
                        'fields'       => 'extraManageModel',
                        'label'        => 'Si autre mode de gestion, précisez', 
                        'formats'      => '{0}',
                        'column_names' => 'AUTRE_GEST',
                        'field_type'   => 'chaîne de caractères (str)'
                    ],
                    [
                        'fields'       => 'tags',
                        'label'        => 'Dans quelle(s) famille(s) de tiers-lieux vous reconnaissez-vous ?',
                        'array_filter' => $costum['lists']['typePlace'] ?? [],
                        'array_values' => [],
                        // 'separator'    => " ; \n",
                        'transform'    => [
                            'callable' => 'explode',
                            'args'     => [
                                // ['raw' => true, 'value' => 'separator'],
                                ['raw' => true, 'value' => 'array_values']
                            ]
                        ],
                        'formats'      => ['{0}'],
                        'column_names' => 'FAM_TL',
                        'field_type'   => 'chaîne de caractères (str) à choix multiples'
                    ],
                    [
                        'fields'       => 'extraTypePlace',
                        'formats'      => '{0}',
                        'column_names' => 'AUTRE_FAM_TL',
                        'field_type'   => 'chaîne de caractères (str)'
                    ],
                    [
                        'fields'       => 'tags',
                        'label'        => 'Compagnon France Tiers-Lieux',
                        'array_filter' => $costum['lists']['compagnon'] ?? [],
                        'array_values' => [],
                        'separator'    => ";",
                        'transform'    => [
                            'callable' => 'implode',
                            'args'     => [
                                ['raw' => true, 'value' => 'separator'],
                                ['raw' => true, 'value' => 'array_values']
                            ]
                        ],
                        'formats'      => '{0}',
                        'column_names' => 'COMP_FTL',
                        'field_type'   => 'chaîne de caractères (str)'

                    ],
                    [
                        'fields'       => 'buildingSurfaceArea',
                        'formats'      => '{0}',
                        'column_names' => 'SURF_BATI',
                        'field_type'   => 'nombre (number)'
                    ],
                    [
                        'fields'       => 'siteSurfaceArea',
                        'formats'      => '{0}',
                        'column_names' => 'SURF_AREA',
                        'field_type'   => 'nombre (number)'
                    ],
                    [
                        'fields'     => 'address',
                        'conditions' => [
                            'monocolumn'  => [
                                'fields'       => [
                                    'address.streetAddress',
                                    'address.postalCode',
                                    'address.addressLocality',
                                    'address.addressCountry',
                                    'address.level3Name',
                                    // 'address.level4Name'
                                ],
                                'formats'      => ['{0};{1};{2};{3};{4}'],
                                'column_names' => [
                                    'ADRESSE',
                                    'CODPOST',
                                    'VILLE',
                                    'PAYS',
                                    'REGION',
                                    // 'DEP'
                                ],
                                'field_type' => [
                                    'chaîne de caractères (str)',
                                    'nombre (number)',
                                    'chaîne de caractères (str)',
                                    'chaîne de caractères (str) (FR, GF, GP, MQ, NC, RE, YT, case vide)',
                                    'chaînes de caractères à choix multiples (Auvergne Rhône-Alpes;Bourgogne-France-Comté;Bretagne;Centre-Val de Loire;Corse;Grand Est;Hauts-de-France;Ile-de-France;Normandie;Nouvelle-Aquitaine;Occitanie;Pays de la Loire;Provence Alpes Côte d\'Azur;Guadeloupe;Guyane;Martinique;Mayotte;Réunion;Nouvelle Calédonie)',
                                    // 'chaînes de caractères à choix multiples (liste des départements)'
                                ]
                            ],
                            'multicolumn' => [
                                'fields'       => [
                                    'address.streetAddress',
                                    'address.postalCode',
                                    'address.addressLocality',
                                    // 'address.addressCountry',
                                    'address.level3Name',
                                    // 'address.level4Name'
                                ],
                                'formats'      => [
                                    '{0}',
                                    '{1}',
                                    '{2}',
                                    // '{3}',
                                    '{3}',
                                    // '{4}'
                                ],
                                'column_names' => [
                                    'ADRESSE',
                                    'CODPOST',
                                    'VILLE',
                                    // 'PAYS',
                                    'REGION',
                                    // 'DEP'
                                ],
                                'field_type' => [
                                    'chaîne de caractères (str)',
                                    'nombre (number)',
                                    'chaîne de caractères (str)',
                                    // 'chaîne de caractères (str) (FR, GF, GP, MQ, NC, RE, YT, case vide)',
                                    'chaînes de caractères à choix multiples (Auvergne Rhône-Alpes;Bourgogne-France-Comté;Bretagne;Centre-Val de Loire;Corse;Grand Est;Hauts-de-France;Ile-de-France;Normandie;Nouvelle-Aquitaine;Occitanie;Pays de la Loire;Provence Alpes Côte d\'Azur;Guadeloupe;Guyane;Martinique;Mayotte;Réunion;Nouvelle Calédonie)',
                                    // 'chaînes de caractères à choix multiples (liste des départements)'
                                ]
                            ]
                        ]
                    ],
                    [
                        'fields'     => 'geo',
                        'conditions' => [
                            'monocolumn'  => [
                                'fields'       => [
                                    'geo.latitude',
                                    'geo.longitude',
                                ],
                                'formats'      => [
                                    'Latitude:{0};Longitude:{1}',
                                ],
                                'column_names' => [
                                    'COORDO',
                                ]
                            ],
                            'multicolumn' => [
                                'fields'       => [
                                    'geo.latitude',
                                    'geo.longitude',
                                ],
                                'formats'      => [
                                    '{0}',
                                    '{1}',
                                ],
                                'column_names' => [
                                    'LATITUDE',
                                    'LONGITUDE',
                                ],
                                'field_type' => [
                                    'nombre (number)',
                                    'nombre (number)'
                                ]
                            ],
                        ],
                    ],
                    [
                        'fields'       => 'profilImageUrl',
                        'formats'      => Yii::app()->getBaseUrl(true) . '{0}',
                        'column_names' => 'LOGO',
                        'field_type'   => 'chaîne de caractères (str) (URL)'
                    ],
                    [
                        'fields'       => 'socialNetwork',
                        'conditions' => [
                            'monocolumn'  => [
                                'fields'       => [
                                    'socialNetwork.facebook',
                                    'socialNetwork.twitter',
                                    'socialNetwork.instagram',
                                    'socialNetwork.linkedin',
                                    'socialNetwork.mastodon',
                                    'socialNetwork.movilab'
                                ],
                                'formats'      => [
                                    '{0};{1};{2};{3};{4};{5}'
                                ],
                                'column_names' => [
                                    'SOCIAUX'
                                ],
                                'field_type' => ['chaîne de caractères (str) (URL)']
                            ],
                            'multicolumn' => [
                                'fields'       => [
                                    'socialNetwork.facebook',
                                    'socialNetwork.twitter',
                                    'socialNetwork.instagram',
                                    'socialNetwork.linkedin',
                                    'socialNetwork.mastodon',
                                    'socialNetwork.movilab'
                                ],
                                'formats'      => [
                                    '{0};{1};{2};{3};{4};{5}',
                                    '{0}',
                                    '{1}',
                                    '{2}',
                                    '{3}',
                                    '{4}',
                                    '{5}'
                                ],
                                'column_names' => [
                                    'SOCIAUX',
                                    'SOCIAUX_FACE',
                                    'SOCIAUX_TWIT',
                                    'SOCIAUX_INSTA',
                                    'SOCIAUX_LINK',
                                    'SOCIAUX_MASTO',
                                    'SOCIAUX_MOVI'
                                ]
                            ]
                        ]
                    ],
                    [
                        'fields'       => 'url',
                        'formats'      => '{0}',
                        'transform'    => [
                            'callable' => 'urldecode',
                            'args'     => [
                                ['raw' => false]
                            ]
                        ],
                        'column_names' => 'INTERNET',
                        'field_type' => 'chaîne de caractères (str) (URL)'
                    ],
                    [
                        'fields'       => 'openingHours',
                        'transform'    => [
                            'class'    => 'TranslateFtl',
                            'callable' => 'openingHours',
                            'args'     => [
                                ['raw' => false]
                            ]
                        ],
                        'formats'      => '{0}',
                        'column_names' => 'HOR_OUV',
                        'field_type' => 'chaîne de caractères (str)'
                    ],
                    [
                        'fields'       => 'email',
                        'formats'      => '{0}',
                        'column_names' => 'EMAIL',
                        'field_type' => 'chaîne de caractères (str) (adresse email)'
                    ],
                    [
                        'fields'       => 'telephone',
                        // 'formats'      => '{0}',
                        'conditions' => [
                            'monocolumn'  => [
                                'fields'       => 'telephone',
                                'formats'      => '{0}',
                                'column_names' => 'TELEPHONE',
                                'field_type' => 'nombre (number)',
                                // 'fields'       => 'telephone',
                                // 'valueField' => '',
                                'transform'    => [
                                    'class'    => 'TranslateFtl',
                                    'callable' => 'telephone',
                                    'args'     => [
                                        ['raw' => true]
                                    ]
                                ]
                            ],
                            'multicolumn'  => [
                                'fields'       => [
                                    'telephone.fixe', 'telephone.mobile'
                                ],
                                'formats'      => ['{0}{1}'],
                                'field_type' => ['nombre (number)'],
                                // 'fields'       => 'telephone',
                                'column_names' => ['TELEPHONE'],
                                'transform'    => [
                                    'class'    => 'TranslateFtl',
                                    'callable' => 'telephone',
                                    'args'     => [
                                        ['raw' => true]
                                    ]
                                ]
                            ]
                        ],   
                        // 'column_names' => 'TELEPHONE'
                    ],
                    [
                        'fields'       => 'video',
                        'array_values' => [],
                        'separator'    => ";",
                        'transform'    => [
                            'callable' => 'implode',
                            'args'     => [
                                ['raw' => true, 'value' => 'separator'],
                                ['raw' => false]
                            ]
                        ],
                        'formats'      => '{0}',
                        'column_names' => 'VIDEO',
                        'field_type' => 'chaîne de caractères (str) (URL)'
                    ],
                    [
                        'fields'       => 'description',
                        'formats'      => '{0}',
                        'column_names' => 'DESCRI_LONG',
                        'field_type' => 'chaîne de caractères (str)'
                    ]
                ]
            ];
            $projects = [];
            switch ($__element_type) {
                case Organization::COLLECTION:
                    if (array_key_exists($__form_id, $organizations)) $output = $organizations[$__form_id];
                    else $output = $default['organizations'];
                    break;
                case Project::COLLECTION:
                    if (array_key_exists($__form_id, $projects)) $output = $projects[$__form_id];
                    else $output = $default['projects'];
                    break;
            }
            
            // Vérifier s'il y a une préférence des champs à éxporter
            if (!empty($__fields)) {
                $temp_out = [];
                foreach ($output as $out) {
                    if (in_array($out['fields'], $__fields)) $temp_out[] = $out;
                }
                $output = $temp_out;
            }
            return $output;
        }
        
        private function map_data_from_template(array &$__data = [], array &$__mapping_template = [], array $__constraint = []): array {
            $output = ['header' => [], 'body' => []];
            $costum = CacheHelper::getCostum();
            $nameMappingStepOrg=(isset($_POST['mappingLabel']) && array_search('Organization',array_keys($_POST['mappingLabel']))!==false) ? $_POST['mappingLabel']['Organization']  : 'Organization';
           
            foreach ($__mapping_template as $mapping) {
                $typeAlias="";

                if (!empty($mapping['conditions'])){
                  $index_condition=is_string($mapping['fields']) ? $mapping['fields'] : $mapping['fields'][0];
                    if(isset($__constraint[$index_condition])){
                        $current_mapping = $mapping['conditions'][$__constraint[$index_condition]];
                    }    
                }else{
                    $current_mapping = $mapping;
                }


                
                // normaliser les formats
                if (!empty($current_mapping['array_filter'])) {
                    $explode = explode('.', $current_mapping['fields']);
                    $array = UtilsHelper::look_at_array_in_depth($__data, $explode);
                    $values = [];
                    if (!empty($array)) {
                        foreach ($array as $_array) {
                            if (in_array($_array, $current_mapping['array_filter']))
                                UtilsHelper::push_array_if_not_exists($_array, $values);
                        }
                    }
                    $current_mapping['array_values'] = $values;
                }
                if($_POST["mappingLabel"]){
                    if(is_array($current_mapping['column_names'])){
                        foreach($current_mapping['column_names'] as $indName=>$columnName){
                            $indexAlias=array_search(trim($columnName),array_values($_POST["mappingLabel"]));
                            $colLabel=($indexAlias!==false) ? array_keys($_POST["mappingLabel"])[$indexAlias] : $columnName;

                            $typeAlias=$current_mapping['field_type'][$indName] ?? "";
                            UtilsHelper::push_array_if_not_exists(["Step"=>$nameMappingStepOrg,"Label"=>$colLabel,"Alias"=>$columnName,"Types"=>$typeAlias],$this->alias);
                        }
                    }else{
                        $indexAlias=array_search(trim($current_mapping['column_names']),array_values($_POST["mappingLabel"]));
                       $colLabel=($indexAlias!==false) ? array_keys($_POST["mappingLabel"])[$indexAlias] : $current_mapping['column_names'];

                        $typeAlias=$current_mapping["field_type"] ?? "";
                        if(!empty($current_mapping["array_filter"]) && $current_mapping['column_names']!=="COMP_FTL"){
                            // var_dump($current_mapping["array_filter"]);exit;
                            $optionValues=implode(";",$current_mapping["array_filter"]);
                            $typeAlias=$typeAlias." (".$optionValues.")";
                        }
                        UtilsHelper::push_array_if_not_exists(["Step"=>$nameMappingStepOrg,"Label"=>$colLabel,"Alias"=>$current_mapping['column_names'],"Types"=>$typeAlias],$this->alias);
                    }
                    
                }
                if($_POST["mappingLabel"] && !empty($current_mapping['array_filter']) && $current_mapping['column_names']!=="COMP_FTL"){
                    // && !empty($current_mapping['array_filter'])
                    // var_dump($current_mapping,$__data,$__mapping_template$costum["typeObj"]);exit;
                    $indexLabel=array_search(trim($current_mapping['label']),array_keys($_POST["mappingLabel"]));
                    $colLabel=($indexLabel!==false) ? array_values($_POST["mappingLabel"])[$indexLabel] : $current_mapping['label'];
                    UtilsHelper::push_array_if_not_exists($colLabel, $output['header']);
                    $output['body'][$colLabel]=implode(";", $values);
                    if(is_string($current_mapping["column_names"])){
                    $answered="0";
                    if(!empty($current_mapping['array_values'])){
                        $answered="1";
                    }
                    UtilsHelper::push_array_if_not_exists($current_mapping["column_names"]."_REP", $output['header']);
                    $output['body'][$current_mapping["column_names"]."_REP"]=$answered;
                    $indexAlias=array_search(trim($current_mapping['column_names']."_REP"),array_values($_POST["mappingLabel"]));
                    $colLabel=($indexAlias!==false) ? array_keys($_POST["mappingLabel"])[$indexAlias] : $current_mapping['column_names']."_REP";
                    
                    $typeAlias="booléen (boolean) (0;1)";
                    UtilsHelper::push_array_if_not_exists(["Step"=>$nameMappingStepOrg,"Label"=>$colLabel,"Alias"=>$current_mapping['column_names']."_REP","Types"=>$typeAlias],$this->alias);
                    // var_dump($current_mapping['array_values'],$current_mapping);exit;
                    }
                    foreach($current_mapping['array_filter'] as $allVal){
                        $indexMap=array_search(trim($allVal),array_keys($_POST["mappingLabel"]));
                        $new_index=($indexMap!==false) ? array_values($_POST["mappingLabel"])[$indexMap] : $allVal;
                        UtilsHelper::push_array_if_not_exists($new_index, $output['header']);
                        // $this->alias[$allVal]=$new_index;
                        // $this->alias[]=["Label"=>$allVal,"Alias"=>$new_index];
                        $typeAlias="booléen (boolean) (0;1)";
                        UtilsHelper::push_array_if_not_exists(["Step"=>$nameMappingStepOrg,"Label"=>$allVal,"Alias"=>$new_index,"Types"=>$typeAlias],$this->alias);
                        if(in_array($allVal,$current_mapping['array_values']) && $indexMap!==false){
                            $output['body'][$new_index]="1";
                        }else{
                            $output['body'][$new_index]="0";
                        }
                        
                    }
                }else if(!empty($current_mapping['transform'])) {
                   
                    UtilsHelper::push_array_if_not_exists($current_mapping["column_names"], $output['header']);
                    
                    $transformation = $current_mapping['transform'];
                    $args = [];
                    if (!empty($transformation['args'])) {
                        foreach ($transformation['args'] as $transformation_arg) {
                            
                            if ($transformation_arg['raw'] === true){
                                
                                $argValue=(!empty($transformation_arg['value']) && isset($current_mapping[$transformation_arg['value']])) ? $current_mapping[$transformation_arg['value']] : ((!empty($__data["telephone"]) && isset($current_mapping["fields"]) && is_string($current_mapping["fields"])) ?  $__data[$current_mapping["fields"]] : [] );
                                UtilsHelper::push_array_if_not_exists($argValue, $args);                               
                            } 
                            else {
                                if (is_string($current_mapping['fields'])) {
                                    if (!empty($__data[$current_mapping['fields']])) UtilsHelper::push_array_if_not_exists($__data[$current_mapping['fields']], $args);
                                } else {
                                    foreach ($current_mapping['fields'] as $current_mapping_field) {
                                        if (!empty($__data[$current_mapping_field])) UtilsHelper::push_array_if_not_exists($__data[$current_mapping_field], $args);
                                    }
                                }
                            }
                        }
                    }
                    if (!empty($transformation['class'])) {
                        $function = [
                            $transformation['class'],
                            $transformation['callable']
                        ];
                        $value = call_user_func_array($function, $args);
                    } else $value = call_user_func_array($transformation['callable'], $args);
                    $output['body'][$current_mapping['column_names']] = preg_replace('/\{0}/', $value, $current_mapping['formats']);
                } else if (is_string($current_mapping['fields']) && $current_mapping['formats']) {
                    // if($current_mapping['column_names']!=="EMAIL_REPONDANTS"){
                        UtilsHelper::push_array_if_not_exists($current_mapping["column_names"], $output['header']);
                    // }
                    
                    $split = explode('.', $current_mapping['fields']);
                    // if($current_mapping['column_names']=="SOCIAUX") var_dump($current_mapping['formats']);exit;
                    $text = UtilsHelper::look_at_array_in_depth($__data, $split);
                    $output['body'][$current_mapping['column_names']] = (!empty($text)) ? preg_replace('/\{0}/', $text, $current_mapping['formats']) : $text;
                } else if (is_array($current_mapping['fields'])) {
                    foreach ($current_mapping['column_names'] as $column_index => $column_name) {
                        UtilsHelper::push_array_if_not_exists($column_name, $output['header']);
                        $template = $current_mapping['formats'][$column_index];
                        foreach ($current_mapping['fields'] as $field_index => $field) {
                            $split = explode('.', $current_mapping['fields'][$field_index]);
                            $text = UtilsHelper::look_at_array_in_depth($__data, $split);
                            $text = (is_array($text)) ? $text[0] : $text;
                            // var_dump($template,$field_index);
                            $template = preg_replace("/\{$field_index\}/", $text, $template);

                        }
                        $exists_empty = true;
                        while ($exists_empty) {
                            $template = str_replace(';;', ';', $template);
                            $template = preg_replace('/^;/', '', $template);
                            $template = preg_replace('/;$/', '', $template);
                            // var_dump(preg_match('/;\s;\s;/', $template));exit;
                            $exists_empty =  preg_match('/;;/', $template) || preg_match('/^;/', $template) || preg_match('/;$/', $template);
                        }
                        // in_array("SOCIAUX",$current_mapping['column_names'])
                        // if(in_array("SOCIAUX",$current_mapping['column_names'])) var_dump($template,);exit;
                        $output['body'][$column_name] = $template === ';' ? '' : $template;
                    }
                }
            }
            return $output;
        }
    }