<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\actions\exportationCsv;

use Answer;
use CacheHelper;
use Comment;
use Document;
use Event;
use Form;
use Organization;
use PHDB;
use PixelHumain\PixelHumain\components\Action;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\actor\Person;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
use Poi;
use Project;
use Rest;
use Yii;
use FranceTierslieux;

class CoformGeneriqueExportCSVAction extends Action {
	private $head_temporaries = [
		'tpls.forms.cplx.checkboxNew'               => [],
		'tpls.forms.cplx.multiCheckboxPlus'         => [],
		'tpls.forms.cplx.simpleTable'               => [],
		'tpls.forms.cplx.element'                   => [],
		'tpls.forms.cplx.finder'                    => [],
		'tpls.forms.costum.franceTierslieux.finder' => [],
		'tpls.forms.evaluation.evaluation'          => [],
		'tpls.forms.evaluation.commonTableV2'       => [],
	];
	private $specials = [
		'tpls.forms.cplx.multiCheckboxPlus'			=> 'multiCheckboxPlus',
		'tpls.forms.cplx.finder'					=> 'finder',
		'tpls.forms.costum.franceTierslieux.finder'	=> 'finder',
		'tpls.forms.cplx.listing'					=> 'listing',
		'tpls.forms.cplx.multiRadio'				=> 'multiRadio',
		'tpls.forms.evaluation.evaluation'			=> 'evaluation',
		'tpls.forms.evaluation.multitextvalidation'	=> 'multitextvalidation',
	];
	private $post_heads = [];
	private $form_headers = [];
	private $answered = [];

	// Spéciale FRTL 2023
	private $region_filters = [];

	public function run($get_input = null) {
		if (!empty($get_input)) {
			return json_encode(PHDB::findOneById(Form::COLLECTION, $_POST['id'], ["inputs.$get_input"])["inputs"][$get_input]);
		}
		$form = PHDB::findOneById(Form::COLLECTION, $_POST['query']['form'], ['creator', 'params', 'subForms']);
		$following_answer_id = $_POST['following_answer_id'] ?? 'followingAnswerId';
		$excluded_inputs = $_POST['excluded_inputs'] ?? [];
		$is_exporting_following_answer_id = !empty($_POST['is_exporting_following_answer']) && filter_var($_POST['is_exporting_following_answer'], FILTER_VALIDATE_BOOLEAN);
		$is_exporting_user_email = !empty($_POST['is_exporting_user_email']) && filter_var($_POST['is_exporting_user_email'], FILTER_VALIDATE_BOOLEAN);
		$parent_form_fields = ['name', 'config', 'subForms', 'creator', 'params'];
		$second_answer_fields = [];
		$completed_answers_only = $_POST['completed_answers_only'];

		// Spéciale FRTL 2023
		$this->region_filters = !empty($_POST['region_filters']) && is_array($_POST['region_filters']) ? $_POST['region_filters'] : [];

		// addQuery verifying that last step is validated
		if ($completed_answers_only && $completed_answers_only !== "false") {
			$sizeSubf = sizeof($form["subForms"]);
			$idLastSubf = $form["subForms"][$sizeSubf - 1];
			$lastSubf = PHDB::findOne(Form::COLLECTION, ['id' => $idLastSubf], ['id', 'name', 'inputs']);
			foreach ($lastSubf["inputs"] as $kfield => $vfield) {
				if (str_contains($vfield["type"], "validateStep"))
					$keyLastValidateStep = $kfield;
			}
			if (isset($keyLastValidateStep))
				$_POST['query']['answers.' . $idLastSubf . '.' . $keyLastValidateStep] = array('$exists' => 'true');
		}

		$answer_db_fields = [
			'form',
			'answers',
			'status',
			'user',
			'links.answered',
			'links.' . Organization::COLLECTION,
			'links.' . Project::COLLECTION,
			$following_answer_id
		];
		$db_answers = PHDB::find(Answer::COLLECTION, $_POST['query'], $answer_db_fields);
		$output = ['heads' => [], 'bodies' => []];

		$where_count = [
			'form' => $_POST['query']['form']
		];
		$where_count[$following_answer_id] = ['$exists' => true];
		$has_following_answer_id = PHDB::count(Answer::COLLECTION, $where_count) > 0;

		foreach ($db_answers as $db_answer) {
			$answer_fields = [];
			if ($has_following_answer_id && $is_exporting_following_answer_id && !empty($db_answer[$following_answer_id])) {
				$db_following_answer = PHDB::findOneById(Answer::COLLECTION, $db_answer[$following_answer_id], $answer_db_fields);
				if (empty($db_following_answer)) continue;
				$db_following_form = PHDB::findOneById(Form::COLLECTION, $db_following_answer['form'], $parent_form_fields);
				$sub_forms = $db_following_form['subForms'] ?? [];

				foreach ($sub_forms as $sub_form) {
					$db_subform = PHDB::findOne(Form::COLLECTION, ['id' => $sub_form], ['id', 'name', 'inputs']);
					$inputs = $db_subform['inputs'] ?? [];
					$ordered_inputs = [];
					$unordered_inputs = [];

					foreach ($inputs as $input_key => $input) {
						if (!empty($input['type']) && !in_array($input['type'], $excluded_inputs)) {
							$label = $input_key;

							if (!empty($input['label'])) $label = $input['label'];
							else if (!empty($input['info'])) $label = $input['info'];

							$label = htmlspecialchars($label);

							if (isset($input['position'])) {
								// sélectionner la position de l'élément dans le formulaire
								$position = intval($input['position']);

								$ordered_inputs[] = [
									'label'    => $label,
									'value'    => $input_key,
									'position' => $position,
									'step'     => $sub_form,
									'type'     => $input['type']
								];
							} else {
								$unordered_inputs[] = [
									'label' => $label,
									'value' => $input_key,
									'step'  => $sub_form,
									'type'  => $input['type']
								];
							}
						}
					}

					// reordonner selon les ordres fournis dans la base de données
					do {
						$permutation = false;
						$length = count($ordered_inputs);
						for ($i = 0; $i < $length - 1; $i++) {
							if ($ordered_inputs[$i]['position'] > $ordered_inputs[$i + 1]['position']) {
								$permutation = true;
								[$ordered_inputs[$i], $ordered_inputs[$i + 1]] = [$ordered_inputs[$i + 1], $ordered_inputs[$i]];
							}
						}
					} while ($permutation);

					foreach ($ordered_inputs as $input_index => $ordered_input) {
						unset($ordered_input['position']);
						$ordered_inputs[$input_index] = $ordered_input;
					}
					$answer_fields = [...$answer_fields, ...$ordered_inputs, ...$unordered_inputs];
				}
			}
			foreach ($answer_fields as $answer_field)
				UtilsHelper::push_array_if_not_exists($answer_field, $second_answer_fields);

			// Récupérer les réponses
			if (!$is_exporting_following_answer_id)
				$body = $this->get_fromated_csv_body($db_answers, $db_answer, $_POST['champs'], $form);
			else {
				$body = $this->get_fromated_csv_body($db_answers, $db_answer, $_POST['champs'], $form, 1);
				if (!empty($body) && !empty($db_following_answer)) $body += $this->get_fromated_csv_body($db_answers, $db_following_answer, $answer_fields, $db_following_form, 2);
			}

			// Récupération des emails
			if (!empty($body)) {
				if ($is_exporting_user_email && !empty($db_answer['links']['answered'])) {
					$answered = [];
					$db_persons = array_filter($db_answer['links']['answered'], fn($link) => empty($this->answered[$link]));
					$db_persons = PHDB::findByIds(\Person::COLLECTION, $db_persons, ["email"]);

					foreach ($db_answer['links']['answered'] as $answered_link) {
						if (empty($this->answered[$answered_link]))
							$this->answered[$answered_link] = $db_persons[$answered_link]['email'] ?? '';
						$answered[] = $this->answered[$answered_link];
					}
					$body['Email du répondant'] = implode(" ; ", $answered);
				}
				$output['bodies'][] = $body;
			}
		}

		// Préparer les headers
		if (!$is_exporting_following_answer_id) $output['heads'] = $this->generate_headers([$_POST['champs']]);
		else $output['heads'] = $this->generate_headers([$_POST['champs'], $second_answer_fields]);

		if ($is_exporting_user_email) $output['heads'] = ['Email du répondant', ...$output['heads']];
		return Rest::json($output);
	}

	private function generate_headers(array $fields = []): array {
		$output = [];
		if (filter_var($_POST['is_exporting_answer_id'], FILTER_VALIDATE_BOOLEAN))
			$output[] = 'Identifiant de la réponse';
		foreach ($fields as $key_field => $value_field) {
			$field_len = count($fields);
			$occurrence = $field_len > 1 ? ($key_field + 1) . '. ' : '';
			foreach ($value_field as $index => $field) {
				$field['label'] = trim(str_replace('  ', ' ', $field['label']));
				// Arranger les champs
				switch ($field['type']) {
					case 'tpls.forms.cplx.address':
						if ($_POST['settings']['address'] == 'multicolumn') $output = array_merge($output, [
							$occurrence . 'StreetAddress',
							$occurrence . 'postalCode',
							$occurrence . 'addressLocality',
							$occurrence . 'addressCountry'
						]);
						else
							$output[] = $occurrence . $field['label'];
						break;
					case 'tpls.forms.cplx.checkboxNew':
					case 'tpls.forms.cplx.multiRadio':
					case 'tpls.forms.cplx.multiCheckboxPlus':
						if ($_POST['settings']['checkbox'] != 'multicolumn') $output[] = $occurrence . $field['label'];
						if (!empty($this->head_temporaries[$field['type']][$occurrence . $field['label']]))
							$output = array_merge($output, $this->head_temporaries[$field['type']][$occurrence . $field['label']]);
						break;
					case 'tpls.forms.cplx.element':
					case 'tpls.forms.cplx.simpleTable':
					case 'tpls.forms.evaluation.evaluation':
					case 'tpls.forms.evaluation.commonTableV2':
					case 'tpls.forms.cplx.finder':
					case 'tpls.forms.costum.franceTierslieux.finder':
						if (!empty($this->head_temporaries[$field['type']][$occurrence . $field['label']]))
							$output = array_merge($output, $this->head_temporaries[$field['type']][$occurrence . $field['label']]);
						break;
					default:
						$output[] = $occurrence . $field['label'];
						break;
				}
				$head_index = !empty($_POST['is_exporting_following_answer']) && filter_var($_POST['is_exporting_following_answer'], FILTER_VALIDATE_BOOLEAN) ? $key_field + 1 : 0;
				if (($field['value'] === $_POST['export_dynform_after'] || $index === $field_len - 1) && !empty($this->post_heads[$head_index])) foreach ($this->post_heads[$head_index] as $post_head) $output[] = $post_head;
			}
		}
		return $output;
	}

	private function get_fromated_csv_body(&$all_ans, &$one_ans, &$fields, &$form, $occ = 0): array {
		$answer_id = (string)$one_ans["_id"];
		$must_put_complex_checkbox_aside = !empty($_POST['is_custom_checkbox_aside']) && filter_var($_POST['is_custom_checkbox_aside'], FILTER_VALIDATE_BOOLEAN);
		$num_tool_map = [
			"criteria"	=>	"Critère",
			"happiness"	=>	"Humeur",
			"note"		=>	"Note",
			"comment"	=> "Commentaires"
		];
		$num_tool_humour_map = [
			"love"			=> 4,
			"happySmile"	=> 3,
			"neutral"		=> 2,
			"sad"			=> 1,
			"cry"			=> 0,
		];
		$bodies = [];
		if (filter_var($_POST['is_exporting_answer_id'], FILTER_VALIDATE_BOOLEAN))
			$bodies['Identifiant de la réponse'] = (string)$one_ans['_id'];
		// common table v2
		$ctv2_criterias = [];
		$column_occurence = $occ > 0 ? "$occ. " : '';
		//            if (!empty($one_ans['links']['answered'])) {
		//                $db_users = PHDB::findByIds(Person::COLLECTION, $one_ans['links']['answered'], ['email']);
		//                $emails = [];
		//                foreach ($db_users as $db_user) $emails[] = $db_user['email'];
		//                $bodies[$column_occurence . 'Email'] = implode(" ;\n", $emails);
		//            }
		foreach ($fields as $field) {
			$field['label'] = trim(str_replace('  ', ' ', $field['label']));

			// replace question label if in the mapping
			if ($field["type"] === "tpls.forms.evaluation.commonTableV2") {
				$new_index = $column_occurence . $field["label"];
				$this->head_temporaries[$field['type']][$new_index] = $this->head_temporaries[$field['type']][$new_index] ?? [];
				if (empty($ctv2_criterias[$field['value']])) {
					$ctv2_criterias[$field['value']] = $form["params"]["criterias" . $field["value"]] ?? [];
					$ctv2_answers = array_filter($all_ans, fn($a) => !empty($a["answers"]["criterias" . $field["value"]]));
					foreach ($ctv2_answers as $ctv2_answer) {
						$crits = $ctv2_answer["answers"]["criterias" . $field["value"]];
						foreach ($crits as $kcrit => $vcrit)
							$ctv2_criterias[$field['value']][$kcrit] = $vcrit;
					}
				}
				$ans_criterias = $ctv2_criterias[$field['value']];
				$db_comments = PHDB::find(Comment::COLLECTION, [
					"contextId"		=>	["\$in" => array_values(array_map(fn($a) => $a["fromAnswerId"], $ans_criterias))],
					"contextType"	=> Form::ANSWER_COLLECTION
				]);
				$ans_yes_or_no = $one_ans["answers"]["yesOrNo" . $field['value']] ?? [];
				foreach ($ans_criterias as $criteria => $ans_criteria) {
					$value = $ans_yes_or_no[$criteria] ?? [];
					foreach ($num_tool_map as $map => $num_tool) {
						$temp_index = "$new_index : " . $ans_criteria["usage"] . " : $num_tool";
						UtilsHelper::push_array_if_not_exists($temp_index, $this->head_temporaries[$field['type']][$new_index]);
						if (!empty($value[$map])) {
							if ($map === "happiness")
								$bodies[$temp_index] = $num_tool_humour_map[$value[$map]];
							else
								$bodies[$temp_index] = $value[$map];
						} else if ($map === "comment") {
							$comments = array_filter($db_comments, fn($c) => $c["path"] === $criteria && $c["contextId"] === $answer_id);
							$comments = array_values(array_map(fn($c) => $c["text"], $comments));
							$bodies[$temp_index] = implode("\n", $comments);
						}
					}
				}
			} else if (!empty($one_ans['answers'][$field['step']][$field['value']])) {
				// Spécifique pour le recensement france tiers lieux
				if ((string)$form["_id"] == "63e0a8abeac0741b506fb4f7") {
					$one_ans['answers'][$field['step']][$field['value']] = FranceTierslieux::answerRules($field["value"], $one_ans['answers'][$field['step']][$field['value']]);
				}
				switch ($field["type"]) {
					case "tpls.forms.cplx.address":
						$addresses = $one_ans["answers"][$field['step']][$field["value"]]['address'];
						if ($_POST['settings']['address'] == 'multicolumn') {
							$bodies[$column_occurence . 'StreetAddress'] = $addresses['streetAddress'];
							$bodies[$column_occurence . 'postalCode'] = $addresses['postalCode'];
							$bodies[$column_occurence . 'addressLocality'] = $addresses['addressLocality'];
							$bodies[$column_occurence . 'addressCountry'] = $addresses['addressCountry'];
						} else {
							$bodies[$column_occurence . $field['label']] = implode(" ; ", [
								$addresses['streetAddress'],
								$addresses['postalCode'],
								$addresses['addressLocality'],
								$addresses['addressCountry']
							]);
						}
						break;
					case 'tpls.forms.cplx.checkboxNew':
						$values = $one_ans["answers"][$field["step"]][$field["value"]];
						if ($_POST['settings']['checkbox'] == 'multicolumn') {
							$this->head_temporaries[$field['type']][$column_occurence . $field['label']] = $this->head_temporaries[$field['type']][$column_occurence . $field['label']] ?? [];
							foreach ($values as $value) {
								$new_index = $column_occurence . $field['label'] . ' : ' . $value;
								UtilsHelper::push_array_if_not_exists($new_index, $this->head_temporaries[$field['type']][$column_occurence . $field['label']]);
								$bodies[$new_index] = $value;
							}
						} else $bodies[$column_occurence . $field['label']] = implode(" ; \n", $one_ans["answers"][$field["step"]][$field["value"]]);
						break;
					case "tpls.forms.tagsFix":
					case "tpls.forms.tags":
						$bodies[$column_occurence . $field['label']] = implode(" ; \n", $one_ans["answers"][$field["step"]][$field["value"]]);
						break;
					case "tpls.forms.uploader":
						$baseUrl = empty($_SERVER['HTTPS']) ? "http://" . $_SERVER['SERVER_NAME'] : "https://" . $_SERVER['SERVER_NAME'];
						$bodies[$column_occurence . $field['label']] = '';
						$connected_user = strval(Yii::app()->session['userId']);
						$isElementAdmin = false;
						$controller = $this->getController();
						if (isset($controller->costum["isCostumAdmin"]))
							$isElementAdmin = $controller->costum["isCostumAdmin"];
						$answer_files = Document::getListDocumentsWhere(["id" => (string)$one_ans['_id']], 'file');
						foreach ($answer_files as $file) {
							if (isset($file["subKey"]) && $file["subKey"] == $field['step'] . "." . $field["value"]) {
								if (!empty($file['author']) && (in_array($connected_user, [$form['creator'], $one_ans['user'], $file['author']]) || $isElementAdmin)) {
									$bodies[$column_occurence . $field['label']] .= $baseUrl . "/upload/communecter/" . $file['folder'] . '/' . $file['name'] . '; ';
								}
							}
						}
						if (!empty($bodies[$column_occurence . $field['label']])) $bodies[$column_occurence . $field['label']] = substr($bodies[$column_occurence . $field['label']], 0, -2);
						break;
					case 'tpls.forms.cplx.simpleTable':
						$simple_table = $one_ans['answers'][$field['step']][$field['value']];
						$this->head_temporaries[$field['type']][$field['label']] = $this->head_temporaries[$field['type']][$field['label']] ?? [];
						if (empty($this->head_temporaries[$field['type']][$field['label']])) {
							$this->head_temporaries[$field['type']][$field['label']] = [];
							for ($i = 1; $i < count($simple_table[0]); $i++) {
								$new_index = $field['label'] . ' : ' . $simple_table[0][$i];
								UtilsHelper::push_array_if_not_exists($new_index, $this->head_temporaries[$field['type']][$field['label']]);
							}
						}

						$length = count($simple_table[0]);
						for ($i = 1; $i < $length; $i++) {
							$new_index = $column_occurence . $field['label'] . ' : ' . $simple_table[0][$i];
							$exploded = [];
							for ($j = 1; $j < count($simple_table); $j++) {
								if (!empty($simple_table[$j][$i])) $exploded[] = strtolower($simple_table[$j][$i]) === 'x' ? $simple_table[$j][0] : $simple_table[$j][0] . ' ' . $simple_table[$j][$i];
							}
							$bodies[$new_index] = implode(" ; \n", $exploded);
							UtilsHelper::push_array_if_not_exists($new_index, $this->head_temporaries[$field['type']][$field['label']]);
						}
						break;
					case 'tpls.forms.cplx.element':
						$elements = $one_ans['answers'][$field['step']][$field['value']];
						$this->head_temporaries[$field['type']][$field['label']] = $this->head_temporaries[$field['type']][$field['label']] ?? [];
						foreach ($elements as $element) {
							$information = [];
							switch ($element['type']) {
								case 'organizations':
									$information = PHDB::findOneById(Organization::COLLECTION, $element['id'], ['name']);
									break;
								case 'event':
									$information = PHDB::findOneById(Event::COLLECTION, $element['id'], ['name']);
									break;
								case 'projects':
									$information = PHDB::findOneById(Project::COLLECTION, $element['id'], ['name']);
									break;
								case 'poi':
									$information = PHDB::findOneById(Poi::COLLECTION, $element['id'], ['name']);
									break;
							}
							$new_index = $column_occurence . $field['label'] . ' : ' . $information['name'];
							$bodies[$new_index] = '"' . $information['name'] . '"; "' . $element['id'] . '"; "' . $element['type'] . '"';
							UtilsHelper::push_array_if_not_exists($new_index, $this->head_temporaries[$field['type']][$column_occurence . $field['label']]);
						}
						break;
					case 'tpls.forms.cplx.rangeTwoSliders':
						$range_two_sliders = $one_ans["answers"][$field["step"]][$field["value"]];
						$bodies[$column_occurence . $field['label']] = 'Entre ' . $range_two_sliders['min'] . ' et ' . $range_two_sliders['max'];
						break;
					case 'tpls.forms.select':
						$select = $one_ans["answers"][$field["step"]][$field["value"]];
						if (is_numeric($select)) {
							$choices = PHDB::findOneById(Form::COLLECTION, $_POST['query']['form'], ['params.' . $field["value"] . '.options']);
							$bodies[$column_occurence . $field['label']] = $choices['params'][$field["value"]]['options'][$select - 1];
						} else
							$bodies[$column_occurence . $field['label']] = $select;
						break;
					default:
						$bodies[$column_occurence . $field['label']] = $one_ans["answers"][$field['step']][$field["value"]];
						break;
				}
			} else if (array_key_exists($field["type"], $this->specials) && (!empty($one_ans['answers'][$field['step']][$this->specials[$field['type']] . $field["value"]]) || !empty($one_ans['answers'][$this->specials[$field['type']] . $field["value"]]))) {
				$value = $this->specials[$field['type']] . $field["value"];
				if ((string)$form["_id"] == "63e0a8abeac0741b506fb4f7") 
					$one_ans['answers'][$field['step']][$value] = FranceTierslieux::answerRules($field["value"], $one_ans['answers'][$field['step']][$value]);
				switch ($field["type"]) {
					case 'tpls.forms.cplx.multiCheckboxPlus':
						$values = $one_ans["answers"][$field['step']][$value];

						// traitement de l'entête
						if ($_POST['settings']['checkbox'] === 'multicolumn') {
							$this->head_temporaries[$field['type']][$column_occurence . $field['label']] = $this->head_temporaries[$field['type']][$column_occurence . $field['label']] ?? [];
							foreach ($form['params'][$value]['global']['list'] as $choix) {
								$header = $column_occurence . $field['label'] . ' : ' . $choix;

								if (isset($_POST['mappingLabel']) && (in_array($field['label'] . " : " . $choix, array_keys($_POST['mappingLabel'])) || in_array($choix, array_keys($_POST['mappingLabel'])))) {
									// $offsetArray=(isset($indexLabel)) ? $indexLabel : 0 ;
									$temporLabel = (in_array($choix, array_keys($_POST['mappingLabel'])) !== false) ? $choix : $field['label'] . " : " . $choix;
									// var_dump($temporLabel);
									$indexOption = array_search($temporLabel, array_keys($_POST['mappingLabel']));
									$header = ($indexOption !== false) ? array_values($_POST['mappingLabel'])[$indexOption] : $header;
								}
							}
						}
						// traitement dun contenu
						$grouped = [];

						// Ranger les réponses par rang
						$values_length = count($values);
						do {
							$permutation = false;
							for ($index = 0; $index < $values_length - 1; $index++) {
								$current = $values[$index];
								$next = $values[$index + 1];
								$current_content = end($current);
								$next_content = end($next);
								if (!empty($current_content['rank']) && !empty($next_content['rank']) && intval($current_content['rank']) > intval($next_content['rank'])) {
									$values[$index + 1] = $current;
									$values[$index] = $next;
									$permutation = true;
								}
							}
						} while ($permutation);

						$this->head_temporaries[$field['type']][$column_occurence . $field['label']] = $this->head_temporaries[$field['type']][$column_occurence . $field['label']] ?? [];
						foreach ($values as $responses) {
							foreach ($responses as $response => $question) {
								if (array_search('simple', $question) === 'type') {
									if ($_POST['settings']['checkbox'] === 'multicolumn') {
										$new_index = $column_occurence . $field['label'] . ' : ' . $response;
										$valueReturned = $response;

										if (isset($_POST['mappingLabel']) && (in_array($response, array_keys($_POST['mappingLabel'])) || in_array($field['label'] . " : " . $response, array_keys($_POST['mappingLabel'])))) {

											// $temporLabel= (in_array($field['label']." : ".$response,array_keys($_POST['mappingLabel']))!==false) ?  $field['label']." : ".$response : $response;  
											$indexOption = array_search($response, array_keys($_POST['mappingLabel']));
											$new_index = ($indexOption !== false) ? array_values($_POST['mappingLabel'])[$indexOption] : $new_index;
											$valueReturned = "1";
											// $bodies[$new_index] = $valueReturned;
											// if(isset($_POST['mappingLabel']) && in_array($response,array_keys($_POST['mappingLabel']))){
											//     $offsetArray=(isset($indexLabel)) ? $indexLabel : 0 ;
											//     $indexOption=array_search($response,array_slice(array_keys($_POST['mappingLabel']),$offsetArray,null,true));
											//     $new_index=($indexOption!==false) ? array_values($_POST['mappingLabel'])[$indexOption] : $new_index; 

											// }
										}
										$bodies[$new_index] = $valueReturned;
										UtilsHelper::push_array_if_not_exists($new_index, $this->head_temporaries[$field['type']][$column_occurence . $field['label']]);
									} else {
										if (!empty($form['params'][$value]['global']['rank']) && filter_var($form['params'][$value]['global']['rank'], FILTER_VALIDATE_BOOL) && !empty($question['rank'])) $grouped[] = '[' . $question['rank'] . '] ' . $response;
										else $grouped[] = $response;
									}
								} else {
									if ($_POST['settings']['checkbox'] === 'multicolumn' || $must_put_complex_checkbox_aside) {
										$new_index = $column_occurence . $field['label'] . ' : ' . $question['value'];
										$valueReturned = $question['textsup'];
										if (isset($_POST['mappingLabel']) && (in_array($response, array_keys($_POST['mappingLabel'])) || in_array($field['label'] . " : " . $response, array_keys($_POST['mappingLabel'])))) {
											$temporLabel = (in_array($field['label'] . " : " . $response, array_keys($_POST['mappingLabel'])) !== false) ?  $field['label'] . " : " . $response : $response;
											$indexOption = array_search($temporLabel, array_keys($_POST['mappingLabel']));
											$new_index = ($indexOption !== false) ? array_values($_POST['mappingLabel'])[$indexOption] : $new_index;

											// add cplx value in another column
											UtilsHelper::push_array_if_not_exists($new_index . '_COMPL', $this->head_temporaries[$field['type']][$column_occurence . $field['label']]);
											$bodies[$new_index . '_COMPL'] = $question['textsup'];

											$valueReturned = "1";
										}
										$bodies[$new_index] = $valueReturned;
										UtilsHelper::push_array_if_not_exists($new_index, $this->head_temporaries[$field['type']][$column_occurence . $field['label']]);
									} else {
										if (!empty($form['params'][$value]['global']['rank']) && filter_var($form['params'][$value]['global']['rank'], FILTER_VALIDATE_BOOL) && !empty($question['rank'])) $grouped[] = '[' . $question['rank'] . '] ' . $question['value'] . ' : ' . $question['textsup'];
										else $grouped[] = $question['value'] . ' : ' . $question['textsup'];
									}
								}
							}
						}
						if (!empty($grouped)) $bodies[$column_occurence . $field['label']] = implode(" ; \n", $grouped);
						break;
					case 'tpls.forms.cplx.finder':
					case 'tpls.forms.costum.franceTierslieux.finder':
						$finders = $one_ans["answers"][$field['step']][$value];
						$this->head_temporaries[$field['type']][$column_occurence . $field['label']] = $this->head_temporaries[$field['type']][$column_occurence . $field['label']] ?? [];

						if ($_POST['settings']['finder'] === 'complete') {
							$column_counter = 0;
							foreach ($finders as $finder) {
								$column_counter++;
								$new_index = $column_occurence . $field['label'] . " : Colonne $column_counter";
								$bodies[$new_index] = '"' . $finder['name'] . '"; "' . $finder['id'] . '"; "' . $finder['type'] . '"';
								UtilsHelper::push_array_if_not_exists($new_index, $this->head_temporaries[$field['type']][$column_occurence . $field['label']]);
							}
						} else if ($_POST['settings']['finder_column'] === 'monocolumn') {
							$grouped = [];
							foreach ($finders as $finder) {
								$grouped[] = $finder['name'];
								UtilsHelper::push_array_if_not_exists($column_occurence . $field['label'], $this->head_temporaries[$field['type']][$column_occurence . $field['label']]);
							}
							$bodies[$column_occurence . $field['label']] = implode(" ; ", $grouped);
						} else {
							$column_counter = 0;
							foreach ($finders as $finder) {
								$column_counter++;
								$new_index = $column_occurence . $field['label'] . " : Colonne $column_counter";
								$bodies[$new_index] = $finder['name'];
								UtilsHelper::push_array_if_not_exists($new_index, $this->head_temporaries[$field['type']][$column_occurence . $field['label']]);
							}
						}
						break;
					case 'tpls.forms.cplx.multiRadio':
						$this->head_temporaries[$field['type']][$column_occurence . $field['label']] = $this->head_temporaries[$field['type']][$column_occurence . $field['label']] ?? [];
						$question = $one_ans["answers"][$field['step']]['multiRadio' . $field['value']];
						$new_index = $column_occurence . $field['label'];
						$valueReturned = $question['value'];
						if (array_search('simple', $question) === 'type') {
							if (isset($_POST['mappingLabel']) && (in_array($question['value'], array_keys($_POST['mappingLabel'])) || in_array($field['label'], array_keys($_POST['mappingLabel'])))) {
								$temporLabel = (in_array($field['label'], array_keys($_POST['mappingLabel'])) !== false) ?  $field['label'] : $question['value'];
								$indexOption = array_search($temporLabel, array_keys($_POST['mappingLabel']));
								$new_index = ($indexOption !== false) ? array_values($_POST['mappingLabel'])[$indexOption] : $new_index;
							}
						} else {
							if ($_POST['settings']['checkbox'] === 'multicolumn' || $must_put_complex_checkbox_aside) {
								$valueReturned = $question['textsup'];
								if (isset($_POST['mappingLabel']) && (in_array($question['value'], array_keys($_POST['mappingLabel'])) || in_array($field['label'] . " : " . $question['value'], array_keys($_POST['mappingLabel'])))) {

									$temporLabel = (in_array($field['label'] . " : " . $question['value'], array_keys($_POST['mappingLabel'])) !== false) ?  $field['label'] . " : " . $question['value'] : $question['value'];
									$indexOption = array_search($temporLabel, array_keys($_POST['mappingLabel']));
									$new_index = ($indexOption !== false) ? array_values($_POST['mappingLabel'])[$indexOption] : $new_index;

									// add cplx value in another column
									UtilsHelper::push_array_if_not_exists($new_index . '_COMPL', $this->head_temporaries[$field['type']][$column_occurence . $field['label']]);
									$bodies[$new_index . '_COMPL'] = $question['textsup'];
									$valueReturned = "1";
								}
							}
						}
						$bodies[$new_index] = $valueReturned;
						UtilsHelper::push_array_if_not_exists($new_index, $this->head_temporaries[$field['type']][$column_occurence . $field['label']]);
						break;
					case 'tpls.forms.evaluation.evaluation':
						$criterias = $form['params']['criterias' . $field['value']] ?? [];
						$evaluations = $one_ans["answers"][$value] ?? [];
						if (!isset($this->head_temporaries[$field['type']][$column_occurence . $field['label']])) $this->head_temporaries[$field['type']][$column_occurence . $field['label']] = [];
						foreach ($evaluations as $label => $criteria) {
							$key = array_keys($criteria)[0];
							$new_index = $column_occurence . $field['label'] . ' : ' . $label;
							$bodies[$new_index] = $criterias[$key]['name'];
							UtilsHelper::push_array_if_not_exists($new_index, $this->head_temporaries[$field['type']][$column_occurence . $field['label']]);
						}
						break;
					case 'tpls.forms.cplx.listing':
						$bodies[$column_occurence . $field['label']] = implode(" ; \n", $one_ans["answers"][$field['step']][$value]);
						break;
				}
			} else {
				if ($field['value'] !== 'custom') $bodies[$column_occurence . $field['label']] = "";
			}
		}

		// traitement des dynforms liés au coform
		$is_exporting_dynform = !empty($_POST['is_exporting_dynform']) && filter_var($_POST['is_exporting_dynform'], FILTER_VALIDATE_BOOL);
		if ($is_exporting_dynform && $occ <= 1 && !empty($one_ans['links'])) {
			if (!empty($one_ans['links'][Organization::COLLECTION])) {
				$ids = array_keys($one_ans['links'][Organization::COLLECTION]);
				$db_organizations = PHDB::findByIds(Organization::COLLECTION, $ids);
				foreach ($db_organizations as $db_organization) {
					$organization_mapping = $this->get_dynform_exportation_mapping(Organization::COLLECTION, $_POST['query']['form'], [], $occ);
					$response = $this->map_data_from_template($db_organization, $organization_mapping, [
						'address' => $_POST['settings']['address'],
						'geo' => $_POST['settings']['address'],
						'socialNetwork' => "multicolumn"
					]);

					foreach ($response['header'] as $header) {
						if (!isset($this->post_heads[$occ]) || empty($this->post_heads[$occ])) $this->post_heads[$occ] = [];
						UtilsHelper::push_array_if_not_exists($header, $this->post_heads[$occ]);
					}
					foreach ($response['body'] as $body_key => $body_value) $bodies[$body_key] = $body_value;
				}
			}
		}

		// Filtrer sur les régions
		if (!empty($this->region_filters)) {
			$has_separated_column = array_key_exists($column_occurence . '(DF) level3Name', $bodies);
			$has_unified_column = array_key_exists($column_occurence . '(DF) Adresse', $bodies);
			if (!$has_separated_column && !$has_unified_column) $bodies = [];
			else {
				foreach ($bodies as $body_index => $body_value) {
					$body_value = str_replace('  ', ' ', trim($body_value));
					$header = $has_separated_column ? $column_occurence . '(DF) level3Name' : $column_occurence . '(DF) Adresse';

					if ($has_separated_column) $value = $body_value;
					else if ($has_unified_column) {
						$exploded = explode(" ; ", $body_value);
						$exploded = !$exploded ? [] : $exploded;
						$value = end($exploded);
					}

					if ($body_index === $header && !in_array($value, $this->region_filters)) {
						$bodies = [];
						break;
					}
				}
			}
		}

		// enlever les lignes vides
		foreach ($bodies as $body_key => $body_value) {
			if (!empty($body_value) && !preg_match('/^([0-9]*.\s)?email$/i', $body_key))
				return $bodies;
		}
		return [];
	}

	private function get_dynform_exportation_mapping(string $__element_type, string $form_id, array $fields = [], $occ = 0): array {
		$costum = CacheHelper::getCostum();

		// Supprimer Autre
		// if (!empty($costum['lists']['manageModel']) && in_array('Autre', $costum['lists']['manageModel'])) {
		//     $index = array_search('Autre', $costum['lists']['manageModel']);
		//     array_splice($costum['lists']['manageModel'], $index, 1);
		// }

		$column_occurence = ($occ > 0 ? "$occ. " : '') . '(DF) ';
		$default = [
			'organizations' => [
				[
					'fields'       => ['name'],
					'formats'      => ['{0}'],
					'column_names' => [$column_occurence . 'Nom de l\'organisation']
				],
				[
					'fields'     => ['address'],
					'conditions' => [
						'monocolumn'  => [
							'fields'       => [
								'address.streetAddress',
								'address.postalCode',
								'address.addressLocality',
								'address.addressCountry'
							],
							'formats'      => ['{0} ; {1} ; {2} ; {3}'],
							'column_names' => [$column_occurence . 'Adresse']
						],
						'multicolumn' => [
							'fields'       => [
								'address.streetAddress',
								'address.postalCode',
								'address.addressLocality',
								'address.addressCountry'
							],
							'formats'      => [
								'{0}',
								'{1}',
								'{2}',
								'{3}'
							],
							'column_names' => [
								$column_occurence . 'StreetAddress',
								$column_occurence . 'Code postal',
								$column_occurence . 'addressLocality',
								$column_occurence . 'addressCountry'
							]
						]
					]
				]
			],
			'projects'      => []
		];
		$organizations = [
			// enquête france tiers-lieux 2023
			'63e0a8abeac0741b506fb4f7' => [
				[
					'fields'       => 'name',
					'formats'      => '{0}',
					'column_names' => 'NOM'
				],
				[
					'fields'       => 'openingDate',
					'formats'      => '{0}',
					'column_names' => 'DATE_OUV'
				],
				[
					'fields'       => 'shortDescription',
					'formats'      => '{0}',
					'column_names' => 'DESCRI_COU'
				],
				[
					'fields'       => 'holderOrganization',
					'formats'      => '{0}',
					'column_names' => 'NOM_STRC_PORT'
				],
				[
					'fields'       => 'tags',
					'array_filter' => $costum['lists']['manageModel'] ?? [],
					'array_values' => [],
					'transform'    => [
						'callable' => 'end',
						'args'     => [
							['raw' => true, 'value' => 'array_values']
						]
					],
					'formats'      => '{0}',
					'column_names' => 'MODE_GEST'
				],
				[
					'fields'       => 'extraManageModel',
					'formats'      => '{0}',
					'column_names' => 'AUTRE_MG'
				],
				[
					'fields'       => 'tags',
					'array_filter' => $costum['lists']['typePlace'] ?? [],
					'array_values' => [],
					// 'separator'    => " ; \n",
					'transform'    => [
						'callable' => 'explode',
						'args'     => [
							// ['raw' => true, 'value' => 'separator'],
							['raw' => true, 'value' => 'array_values']
						]
					],
					'formats'      => ['{0}'],
					'column_names' => ['FAM_TL']
				],
				[
					'fields'       => 'extraTypePlace',
					'formats'      => '{0}',
					'column_names' => 'AUTRE_FAM_TL'
				],
				[
					'fields'       => 'tags',
					'array_filter' => $costum['lists']['compagnon'] ?? [],
					'array_values' => [],
					'separator'    => " ; \n",
					'transform'    => [
						'callable' => 'implode',
						'args'     => [
							['raw' => true, 'value' => 'separator'],
							['raw' => true, 'value' => 'array_values']
						]
					],
					'formats'      => '{0}',
					'column_names' => 'COMP_FTL'
				],
				[
					'fields'       => 'buildingSurfaceArea',
					'formats'      => '{0}',
					'column_names' => 'SURF_BATI'
				],
				[
					'fields'       => 'siteSurfaceArea',
					'formats'      => '{0}',
					'column_names' => 'SURF_AREA'
				],
				[
					'fields'     => 'address',
					'conditions' => [
						'monocolumn'  => [
							'fields'       => [
								'address.streetAddress',
								'address.postalCode',
								'address.addressLocality',
								'address.addressCountry',
								'address.level3Name',
							],
							'formats'      => ['{0} ; {1} ; {2} ; {3} ; {4}'],
							'column_names' => ['ADRESSE']
						],
						'multicolumn' => [
							'fields'       => [
								'address.streetAddress',
								'address.postalCode',
								'address.addressLocality',
								'address.addressCountry',
								'address.level3Name',
							],
							'formats'      => [
								'{0}',
								'{1}',
								'{2}',
								'{3}',
								'{4}',
							],
							'column_names' => [
								'ADRESSE',
								'CODPOST',
								'VILLE',
								'PAYS',
								'REGION',
							]
						]
					]
				],
				[
					'fields'       => 'address.codeInsee',
					'formats'      => '{0}',
					'column_names' => 'INSEE'
				],
				[
					'fields'     => 'geo',
					'conditions' => [
						'monocolumn'  => [
							'fields'       => [
								'geo.latitude',
								'geo.longitude',
							],
							'formats'      => [
								'Latitude : {0} ; Longitude : {1}',
							],
							'column_names' => [
								'COORDO',
							]
						],
						'multicolumn' => [
							'fields'       => [
								'geo.latitude',
								'geo.longitude',
							],
							'formats'      => [
								'{0}',
								'{1}',
							],
							'column_names' => [
								'LATITUDE',
								'LONGITUDE',
							]
						],
					],
				],
				[
					'fields'       => 'profilImageUrl',
					'formats'      => Yii::app()->getBaseUrl(true) . '{0}',
					'column_names' => 'LOGO'
				],
				[
					'fields'       => 'socialNetwork',
					'conditions' => [
						'monocolumn'  => [
							'fields'       => [
								'socialNetwork.facebook',
								'socialNetwork.twitter',
								'socialNetwork.instagram',
								'socialNetwork.linkedin',
								'socialNetwork.mastodon',
								'socialNetwork.movilab'
							],
							'formats'      => [
								'{0}; {1}; {2}; {3}; {4}; {5}; {6}'
							],
							'column_names' => [
								'SOCIAUX'
							]
						],
						'multicolumn' => [
							'fields'       => [
								'socialNetwork.facebook',
								'socialNetwork.twitter',
								'socialNetwork.instagram',
								'socialNetwork.linkedin',
								'socialNetwork.mastodon',
								'socialNetwork.movilab'
							],
							'formats'      => [
								'{0}',
								'{1}',
								'{2}',
								'{3}',
								'{4}',
								'{5}',
								'{6}'
							],
							'column_names' => [
								'SOCIAUX_FACE',
								'SOCIAUX_TWIT',
								'SOCIAUX_INSTA',
								'SOCIAUX_LINK',
								'SOCIAUX_MASTO',
								'SOCIAUX_MOVI'
							]
						]
					]
				],
				[
					'fields'       => 'url',
					'formats'      => '{0}',
					'transform'    => [
						'callable' => 'urldecode',
						'args'     => [
							['raw' => false]
						]
					],
					'column_names' => 'INTERNET'
				],
				[
					'fields'       => 'openingHours',
					'transform'    => [
						'class'    => 'TranslateFtl',
						'callable' => 'openingHours',
						'args'     => [
							['raw' => false]
						]
					],
					'formats'      => '{0}',
					'column_names' => 'HOR_OUV'
				],
				[
					'fields'       => 'email',
					'formats'      => '{0}',
					'column_names' => 'EMAIL'
				],
				[
					'fields'       => 'telephone',
					'formats'      => '{0}',
					'column_names' => 'TELEPHONE'
				],
				[
					'fields'       => 'video',
					'array_values' => [],
					'separator'    => " ; \n",
					'transform'    => [
						'callable' => 'implode',
						'args'     => [
							['raw' => true, 'value' => 'separator'],
							['raw' => false]
						]
					],
					'formats'      => '{0}',
					'column_names' => 'VIDEO'
				],
				[
					'fields'       => 'description',
					'formats'      => '{0}',
					'column_names' => 'DESCRI_LONG'
				]
			]
		];
		$projects = [];
		switch ($__element_type) {
			case Organization::COLLECTION:
				if (array_key_exists($form_id, $organizations)) $output = $organizations[$form_id];
				else $output = $default['organizations'];
				break;
			case Project::COLLECTION:
				if (array_key_exists($form_id, $projects)) $output = $projects[$form_id];
				else $output = $default['projects'];
				break;
		}

		// Vérifier s'il y a une préférence des champs à éxporter
		if (!empty($fields)) {
			$temp_out = [];
			foreach ($output as $out) {
				if (in_array($out['fields'], $fields)) $temp_out[] = $out;
			}
			$output = $temp_out;
		}
		return $output;
	}

	private function map_data_from_template(array &$__data = [], array &$__mapping_template = [], array $__constraint = []): array {
		$output = ['header' => [], 'body' => []];
		foreach ($__mapping_template as $mapping) {
			if (!empty($mapping['conditions'])) $current_mapping = $mapping['conditions'][$__constraint[is_string($mapping['fields']) ? $mapping['fields'] : $mapping['fields'][0]]];
			else $current_mapping = $mapping;

			// normaliser les formats
			//array_filter désigne les valeurs qu'on veut faire ressortir dans le champ fields spécifiés
			if (!empty($current_mapping['array_filter'])) {
				$explode = explode('.', $current_mapping['fields']);
				// va chercher les données du champ spécifié dans fields
				$array = UtilsHelper::look_at_array_in_depth($__data, $explode);
				$values = [];
				if (!empty($array)) {
					foreach ($array as $_array) {
						if (in_array($_array, $current_mapping['array_filter']))
							UtilsHelper::push_array_if_not_exists($_array, $values);
					}
				}
				$current_mapping['array_values'] = $values;
			}

			if ($_POST["mappingLabel"] && isset($current_mapping['array_filter'])) {
				foreach ($current_mapping['array_filter'] as $allVal) {
					$indexMap = array_search(trim($allVal), array_keys($_POST["mappingLabel"]));
					$new_index = ($indexMap !== false) ? array_values($_POST["mappingLabel"])[$indexMap] : $allVal;
					UtilsHelper::push_array_if_not_exists($new_index, $output['header']);
					if (in_array($allVal, $current_mapping['array_values']) && $indexMap !== false) {
						$output['body'][$new_index] = "1";
					}
				}
			} else if (!empty($current_mapping['transform'])) {
				$output['header'][] = $current_mapping['column_names'];

				$transformation = $current_mapping['transform'];
				$args = [];
				if (!empty($transformation['args'])) {
					foreach ($transformation['args'] as $transformation_arg) {
						if ($transformation_arg['raw'] === true) UtilsHelper::push_array_if_not_exists($current_mapping[$transformation_arg['value']], $args);
						else {
							if (is_string($current_mapping['fields'])) {
								if (!empty($__data[$current_mapping['fields']])) UtilsHelper::push_array_if_not_exists($__data[$current_mapping['fields']], $args);
							} else {
								foreach ($current_mapping['fields'] as $current_mapping_field) {
									if (!empty($__data[$current_mapping_field])) UtilsHelper::push_array_if_not_exists($__data[$current_mapping_field], $args);
								}
							}
						}
					}
				}
				if (!empty($transformation['class'])) {
					$function = [
						$transformation['class'],
						$transformation['callable']
					];
					$value = call_user_func_array($function, $args);
				} else $value = call_user_func_array($transformation['callable'], $args);
				$output['body'][$current_mapping['column_names']] = preg_replace('/\{0}/', $value, $current_mapping['formats']);
			} else if (is_string($current_mapping['fields']) && $current_mapping['formats']) {
				$output['header'][] = $current_mapping['column_names'];
				$split = explode('.', $current_mapping['fields']);
				$text = UtilsHelper::look_at_array_in_depth($__data, $split);
				$output['body'][$current_mapping['column_names']] = preg_replace('/\{0}/', $text, $current_mapping['formats']);
			} else if (is_array($current_mapping['fields'])) {
				foreach ($current_mapping['column_names'] as $column_index => $column_name) {
					UtilsHelper::push_array_if_not_exists($column_name, $output['header']);
					$template = $current_mapping['formats'][$column_index];
					foreach ($current_mapping['fields'] as $field_index => $field) {
						$split = explode('.', $current_mapping['fields'][$field_index]);
						$text = UtilsHelper::look_at_array_in_depth($__data, $split);
						$template = preg_replace("/\{$field_index}/", $text, $template);
					}
					$exists_empty = true;
					while ($exists_empty) {
						$template = str_replace(' ; ; ', ' ; ', $template);
						$template = preg_replace('/^\s;\s/', '', $template);
						$template = preg_replace('/\s;\s$/', '', $template);
						$exists_empty = preg_match('/\s;\s;\s/', $template) || preg_match('/^\s;\s/', $template) || preg_match('/\s;\s$/', $template);
					}
					$output['body'][$column_name] = $template === ' ; ' ? '' : $template;
				}
			}
		}
		return $output;
	}
}
