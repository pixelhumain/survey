<?php
    
    namespace PixelHumain\PixelHumain\modules\survey\controllers\actions\exportationCsv;
    
    use Answer;
    use Citoyen;
    use Rest;
    use PHDB;
    use Form;
    use MongoId;
    use MongoRegex;
    use Project;
    
    class AapGeneriqueExportCSVAction extends \PixelHumain\PixelHumain\components\Action {
        public function run() {
            $post = $_POST['query'];
            $filters = $_POST["query"]['filters'];
            $query = ['form' => $filters['form'], 'answers.aapStep1.titre' => ['$exists' => true]];
            
            $evaluation_type = '';
            $answers = [];
            $form_database = PHDB::findOneById(Form::COLLECTION, $query['form'], ['inputConfig', 'params.configSelectionCriteria']);
            
            // Mapping des labels dans l'évaluation à tableau 2D
            $mappingCriteriaEvaluationField = [];
            if (!empty($form_database['params']['configSelectionCriteria'])) {
                $configSelectionCriteria = $form_database['params']['configSelectionCriteria'];
                if (!empty($configSelectionCriteria['criterions'])) {
                    $criterionFields = [];
                    foreach ($configSelectionCriteria['criterions'] as $criterions) {
                        $criterionFields[] = 'inputs.' . $criterions['fieldKey'];
                    }
                    $inputMappingDatabase = PHDB::findOne(Form::INPUTS_COLLECTION, [
                        'formParent' => $query['form'], 'step' => 'aapStep1'
                    ],                                    $criterionFields);
                    foreach ($inputMappingDatabase['inputs'] as $criterion => $inputMapping) {
                        $mappingCriteriaEvaluationField[$criterion] = $inputMapping['label'];
                    }
                }
                if (!empty($configSelectionCriteria['unassociatedCriterions'])) {
                    foreach ($configSelectionCriteria['unassociatedCriterions'] as $unassociatedCriterions) {
                        $mappingCriteriaEvaluationField[$unassociatedCriterions['fieldKey']] = $unassociatedCriterions['fieldLabel'];
                    }
                }
            }
            
            // définition du type d'évaluation
            if (!empty($form_database['inputConfig']['multiDecide'])) {
                $evaluation_type = $form_database['inputConfig']['multiDecide'];
            }
            $output = ["heads" => [], "bodies" => []];
            $optionsList = [
                "progress"          => "En cours",
                "underconstruction" => "En construction",
                "vote"              => "En evaluation",
                "finance"           => "En financement",
                "call"              => "Appel à participation",
                "newaction"         => "nouvelle proposition",
                "projectstate"      => "En projet",
                "prevalided"        => "Pré validé",
                "validproject"      => "Projet validé",
                "voted"             => "Voté",
                "finish"            => "Términé",
                "suspend"           => "Suspendu"
            ];
            $quadrantTitle = [
                'quadrant0' => 'Obsolète',
                'quadrant1' => 'Valide',
                'quadrant2' => 'Anticipé',
                'quadrant3' => 'Innovant'
            ];
            $mixedInputs = [
                'tpls.forms.ocecoform.multiDecide'           => [],
                'tpls.forms.ocecoform.financementFromBudget' => [],
            ];
            
            // search field
            if (!empty($post['textPath']) && !empty($post['text'])) {
                $text = $post['text'];
                $query[$post['textPath']] = ['$regex' => new MongoRegex("/$text/i")];
            }
            
            //years
            if (!empty($filters['answers.aapStep1.year'])) {
                $field = 'answers.aapStep1.year';
                if (is_array($filters[$field])) {
                    $query[$field] = ['$in' => []];
                    foreach ($filters[$field] as $in) {
                        $query[$field]['$in'][] = strval($in);
                    }
                } else $query[$field] = strval($filters[$field]);
            }
            
            //status
            if (!empty($filters['status']) && empty($filters['status']['$not'])) {
                $field = 'status';
                if (is_array($filters[$field])) {
                    $query[$field] = ['$in' => []];
                    foreach ($filters[$field] as $in) {
                        $query[$field]['$in'][] = $in;
                    }
                } else $query[$field] = $filters[$field];
            } else {
                $query['status'] = ['$not' => new MongoRegex('/finish/i')];
            }
            
            if (empty($query['answers.aapStep1.titre'])) {
                $query['answers.aapStep1.titre'] = ['$exists' => true];
            }
            
            foreach ($post['sortBy'] as $field => $value) {
                $post['sortBy'][$field] = intval($value);
            }
            
            if (filter_var($_POST['settings']['takePagination'], FILTER_VALIDATE_BOOLEAN)) $answers = PHDB::findAndFieldsAndSortAndLimitAndIndex(Answer::COLLECTION, $query, [
                'answers', 'status', 'project'
            ],                                                                                                                                   $post['sortBy'], intval($post['indexStep']), intval($post['indexMin']));
            else $answers = PHDB::findAndSort(Answer::COLLECTION, $query, $post['sortBy'], 0, ['answers', 'status', 'project']);
            
            $output["heads"][] = "Status";
            $db_citoyens = [];
            foreach ($answers as $answer) {
                $bodies = [];
                $statusArray = [];
                
                if (isset($answer["status"])) {
                    foreach ($answer["status"] as $status) {
                        if (isset($optionsList[$status])) {
                            $statusArray[] = $optionsList[$status];
                        }
                    }
                }
                
                $bodies["Status"] = implode(' | ', $statusArray);
                foreach ($_POST["champs"] as $champ) {
                    $aapStep = $champ['step'];
                    $index = $champ["label"];
                    
                    if (isset($answer["answers"][$aapStep][$champ["value"]])) {
                        switch ($champ["type"]) {
                            case "tpls.forms.cplx.address":
                                $addresses = $answer["answers"][$champ['step']][$champ["value"]]['address'];
                                if ($_POST['settings']['address'] == 'multicolumn') {
                                    $bodies['StreetAddress'] = $addresses['streetAddress'] ?? '';
                                    $bodies['postalCode'] = $addresses['postalCode'] ?? '';
                                    $bodies['addressLocality'] = $addresses['addressLocality'] ?? '';
                                    $bodies['addressCountry'] = $addresses['addressCountry'] ?? '';
                                } else {
                                    $bodies[$index] = implode(', ', [
                                        $addresses['streetAddress'] ?? '',
                                        $addresses['postalCode'] ?? '',
                                        $addresses['addressLocality'] ?? '',
                                        $addresses['addressCountry'] ?? ''
                                    ]);
                                }
                                break;
                            case "tpls.forms.cplx.checkboxNew":
                            case "tpls.forms.tagsFix":
                            case "tpls.forms.tags":
                                $bodies[$index] = implode(", ", $answer["answers"][$aapStep][$champ["value"]]);
                                break;
                            case 'tpls.forms.ocecoform.suiviFromBudget':
                                $totalDepense = 0;
                                foreach ($answer["answers"][$aapStep][$champ["value"]] as $depense) {
                                    if (empty($depense["price"])) {
                                        $totalDepense += intval($depense["price"]);
                                    }
                                    $bodies[$index] = $totalDepense;
                                }
                                break;
                            default:
                                $bodies[$index] = $answer["answers"][$aapStep][$champ["value"]];
                                break;
                        }
                    } else if ($aapStep === 'aapStep2' && $champ['type'] === 'tpls.forms.ocecoform.multiDecide' && !empty($evaluation_type)) {
                        switch ($evaluation_type) {
                            case 'tpls.forms.aap.selection':
                                $selections = $answer["answers"][$aapStep]['selection'] ?? [];
                                $admissibilities = $answer["answers"][$aapStep]['admissibility'] ?? [];
                                
                                // évaluation des questionnaires dans l'étape 1
                                $voters = [];
                                $keys = [];
                                foreach ($selections as $user_id => $selection) {
                                    foreach ($selection as $evaluated_field => $evaluation) {
                                        $key = !empty($mappingCriteriaEvaluationField[$evaluated_field]) ? 'Critère.' . $mappingCriteriaEvaluationField[$evaluated_field] : 'Critère.' . $evaluated_field;
                                        $value = floatval($evaluation);
                                        $bodies[$key] = !empty($bodies[$key]) ? floatval($bodies[$key]) + $value : $value;
                                        $voters[$key] = !empty($voters[$key]) ? intval($voters[$key]) + 1 : 1;
                                        if (!in_array($key, $mixedInputs[$champ['type']])) $mixedInputs[$champ['type']][] = $key;
                                        if (!in_array($key, $keys)) $keys[] = $key;
                                    }
                                }
                                foreach ($keys as $key) $bodies[$key] = $bodies[$key] . ' note' . (floatval($bodies[$key]) > 1 ? 's' : '') . '/' . $voters[$key] . ' votant' . (intval($voters[$key]) > 1 ? 's' : '');
                                
                                foreach ($admissibilities as $user_id => $admissibility) {
                                    $key = 'admissibility.' . $user_id;
                                    $bodies[$key] = $admissibility;
                                    if (!in_array($key, $mixedInputs[$champ['type']])) {
                                        $mixedInputs[$champ['type']][] = $key;
                                    }
                                }
                                break;
                            case 'tpls.forms.ocecoform.evaluation':
                                $evaluations = $answer["answers"][$aapStep]['evaluationevaluation'] ?? [];
                                // évaluation des questionnaires dans l'étape 1
                                foreach ($evaluations as $quadrant => $evaluation) {
                                    $newIndex = "$index : $quadrantTitle[$quadrant]";
                                    $bodies[$newIndex] = implode("\r\n", $evaluation);
                                    if (!in_array($newIndex, $mixedInputs[$champ['type']])) $mixedInputs[$champ['type']][] = $newIndex;
                                }
                                break;
                            case 'tpls.forms.ocecoform.pourContre':
                                $pourContres = $answer["answers"][$aapStep]['pourContre'] ?? [];
                                $userIds = [];
                                foreach (array_keys($pourContres) as $userId) {
                                    $userIds[] = new MongoId($userId);
                                }
                                $users = PHDB::find(Citoyen::COLLECTION, ['_id' => ['$in' => $userIds]], ['name']);
                                $criterions = [
                                    -1 => 'Contre',
                                    0  => 'Neutre',
                                    1  => 'Pour'
                                ];
                                $bodiesTemp = [
                                    1  => [],
                                    0  => [],
                                    -1 => []
                                ];
                                foreach ($pourContres as $user => $vote) {
                                    $bodiesTemp[$vote][] = $users[$user]['name'];
                                }
                                foreach ($bodiesTemp as $vote => $values) {
                                    $newIndex = $criterions[$vote];
                                    $bodies[$newIndex] = implode("\r\n", $values);
                                    if (!in_array($newIndex, $mixedInputs[$champ['type']])) $mixedInputs[$champ['type']][] = $newIndex;
                                }
                                break;
                        }
                    } else if ($aapStep === 'aapStep3') {
                        switch ($champ['type']) {
                            case 'tpls.forms.ocecoform.generateprojectbtn':
                                if (!empty($answer['project']['id'])) {
                                    $project = PHDB::findOneById(Project::COLLECTION, $answer['project']['id'], ['name']);
                                    $bodies[$index] = $project['name'];
                                }
                                break;
                            case 'tpls.forms.ocecoform.financementFromBudget':
                                $total = 0;
                                $depenses = $answer["answers"]['aapStep1']['depense'] ?? [];
                                foreach ($depenses as $depense) {
                                    $finances = $depense['financer'] ?? [];
                                    foreach ($finances as $finance) {
                                        $name = $finance['name'] ?? '';
                                        if (empty($finance['name'])) {
                                            if (empty($db_citoyens[$finance['user']])) $db_citoyens[$finance['user']] = PHDB::findOneById(\Person::COLLECTION, $finance['user'], ['name']);
                                            $name = $db_citoyens[$finance['user']]['name'];
                                        }
                                        $newIndex = "$index : $name";
                                        if (!empty($bodies[$newIndex])) $bodies[$newIndex] = (intval(substr($bodies[$newIndex], 0, -2)) + ($finance['amount'] ?? 0)) . ' €';
                                        else $bodies[$newIndex] = ($finance['amount'] ?? 0) . ' €';
                                        if (!in_array($newIndex, $mixedInputs[$champ['type']])) $mixedInputs[$champ['type']][] = $newIndex;
                                        $total += intval($finance['amount'] ?? 0);
                                    }
                                }
                                $newIndex = "$index : Total";
                                $bodies[$newIndex] = "$total €";
                                break;
                        }
                    } else {
                        $bodies[$index] = "";
                    }
                }
                
                // enlever les lignes vides
                foreach ($bodies as $body) {
                    if (!empty($body)) {
                        $output["bodies"][] = $bodies;
                        break;
                    }
                }
            }
            
            foreach ($_POST["champs"] as $champ) {
                switch ($champ["type"]) {
                    case "tpls.forms.cplx.address":
                        if ($_POST['settings']['address'] == 'multicolumn') $output["heads"] = array_merge($output["heads"], [
                            'StreetAddress', 'postalCode', 'addressLocality', 'addressCountry'
                        ]);
                        else $output["heads"][] = $champ['label'];
                        break;
                    case "tpls.forms.ocecoform.multiDecide":
                        $output['heads'] = array_merge($output['heads'], $mixedInputs[$champ['type']]);
                        break;
                    case 'tpls.forms.ocecoform.financementFromBudget':
                        $output['heads'] = array_merge($output['heads'], $mixedInputs[$champ['type']]);
                        $output['heads'][] = $champ['label'] . ' : Total';
                        break;
                    default:
                        $output["heads"][] = $champ["label"];
                        break;
                }
            }
            return Rest::json($output);
        }
    }
