<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\actions;
use CacheHelper;
use CAction;
use CTKException;
use Form;
use Person;
use PHDB;
use Yii;

class NewAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id,$session) {
		$ctrl=$this->getController();
		
		if ( ! Person::logguedAndValid() ) {
            if(Yii::app()->request->isAjaxRequest)
                return $ctrl->renderPartial("co2.views.default.unTpl",array("msg"=>Yii::t("common","Please Login First"),"icon"=>"fa-sign-in"));
            else
 			    return $ctrl->render("co2.views.default.unTpl",array("msg"=>Yii::t("common","Please Login First"),"icon"=>"fa-sign-in"));
        }

        //check form et session exist
        if($form = PHDB::findOne( Form::COLLECTION , array("id"=>$id))){
            if(@$form["session"][$session])
            {
                if( Form::isFinish( $form["session"][$session]["endDate"]) ){
                    return $ctrl->render("co2.views.default.unTpl",array("msg"=>"La session est terminé","icon"=>"fa-calendar"));
                } else {
                    $new =  array(
                        "id" => $id,
                        "user"=>Yii::app()->session["userId"],
                        "name"=>Yii::app()->session["user"]["name"],
                        "email"=>Yii::app()->session["userEmail"],
                        "session" => $session
                    );
                    try{
    
                        $answer = array(
                            "formId"=>$new["id"],
                            "user"=>$new["user"],
                            "session"=>$new["session"],
                            "name"=>$new["name"],
                            "email"=>$new["email"],
                            "step" => "dossier",
                            "created"=>time()
                        );
                        $costum = CacheHelper::getCostum();
                        if( isset($costum) && 
                            isset($costum["slug"]) ){
                            $answer["source"] = array(   "key" => $costum["slug"],
                                                        "keys" => array($costum["slug"]),
                                                        "insertOrign" => "costum") ;
                        }
                        Yii::app()->mongodb->selectCollection( Form::ANSWER_COLLECTION)->insert( $answer);
                         $res = array( "result" => true,
                                     "answer" => $answer );
                    } catch (CTKException $e){
                        return $e->getMessage();
                    }
                    if( $form["surveyType"] == "surveyList" ){
                        $firstId = (@$form["scenario"]) ? array_keys($form["scenario"])[0] : $form["id"];
                        return $ctrl->redirect(Yii::app()->createUrl("/survey/co/index/id/".$firstId."/session/".$session."/answer/".(string)$res["answer"]['_id']));
                    } else {
                        return $ctrl->redirect(Yii::app()->createUrl("/survey/co/index/id/".$id."/session/".$session."/answer/".(string)$res["answer"]['_id']));
                    } 
                }
            } else 
                return $ctrl->render("co2.views.default.unTpl",array("msg"=>"Session introuvable","icon"=>"fa-search"));
        } else 
            return $ctrl->render("co2.views.default.unTpl",array("msg"=>"Survey introuvable","icon"=>"fa-search"));
	}
}