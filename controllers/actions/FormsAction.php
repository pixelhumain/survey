<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\actions;

use CTKAction, PHDB, Form, MongoId, Person, Slug, Yii, Authorisation, Project;
class FormsAction extends CTKAction
{
     public function run($id,$scene=null,$aid)
    {
    	$ctrl = $this->getController();
    	$ctrl->layout = "//layouts/empty";
    	
    	$answers = PHDB::findOne( Form::ANSWER_COLLECTION , array("_id"=>new MongoId($aid)));

    	$user = Person::getById($answers["user"]);
    	$form = PHDB::findOne( Form::COLLECTION , array( "id"=> $id ) );
    	$parent = Slug::getElementBySlug($form["id"]);

    	//scenario if should be overloaded by parent scenaario set as db.forms.idxxx....
    	$formSrc = null;
		if( is_string($form["scenario"]) && stripos( $form["scenario"] , "db.") !== false)
		{
			$pathT = explode(".",$form["scenario"]); 
			$formSrc = PHDB::findOne( $pathT[1] , array( $pathT[2] => $pathT[3] ) );
			$form["scenario"] = $formSrc[$pathT[4]];
		} 
		else if( is_array($form["scenario"]) ) 
		{
	    	foreach ($form["scenario"] as $k => $v) 
	    	{
	    		if( is_string($v) && stripos( $v , "db.") !== false){
	    			$pathT = explode(".",$v); 
	    			$formSrc = PHDB::findOne( $pathT[1] , array( $pathT[2] => $pathT[3] ) );
	    			$form["scenario"][$k] = $formSrc[$k];
	    		}
	    	}
	    }

	    //affichage d'une section seule
	    if($scene != null)
	    {
			foreach ($form["scenario"] as $k => $v) 
			{
				//retirer tout les scenario pour les afficher un par un 
				if($k != $scene)
					unset($form["scenario"][$k]);
			}
		}

    	// var_dump( $form );
    	// var_dump( $answers["answers"][$id]["answers"] );
    	// var_dump(Form::canAdmin( $form["id"], $form ) );
    	// var_dump(( $user == Yii::app()->session["userId"] )); 
    	//exit;
    	if ( ! Person::logguedAndValid() ) 
			return $this->getController()->render("co2.views.default.unTpl",array("msg"=>Yii::t("common","Please Login First"),"icon"=>"fa-sign-in"));
		else if( Form::canAdmin( (string)$form["_id"]) || 
				 $user == Yii::app()->session["userId"] ||
				 Authorisation::canEditItem( Yii::app()->session["userId"], $parent["type"], $parent["id"] )
				 ){ 
			$idProject = [];
			$projects = [] ;
			$projectsDetails = array();
			

			if(!empty($idProject)){
				$projectsDetails = PHDB::find(	Project::COLLECTION, 
												array( "_id" => array('$in' => $idProject)), array("name", "shortDescription", "email") );
			}

			$params = array( "answers" => $answers, 
							 'answerCollection' => "actions",
							 'answerId' => (string)$answers["_id"] ,
							 'form' => $form ,
							 'projects' => $projects,
							 'projectsDetails' => $projectsDetails,
							 "user" => $user,
							 'scenario' => "scenario" );
			//todo apply cte customisation ???
			
 			return $ctrl->render( "forms" , $params);
		} else 
			return $this->getController()->render("co2.views.default.unTpl",array("msg"=>Yii::t("project", "Unauthorized Access."),"icon"=>"fa-lock"));
    }
}