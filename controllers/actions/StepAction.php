<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\actions;

use CAction, PHDB, Form, Person, Yii;
class StepAction extends \PixelHumain\PixelHumain\components\Action{

    public function run($id,$session="1", $view=""){

        $ctrl = $this->getController();
    	$ctrl->layout = "//layouts/empty";

        $form = PHDB::findOne( Form::COLLECTION , array("id"=>$id,"session"=>$session));
    	if ( ! Person::logguedAndValid() ) {
            return $ctrl->render("co2.views.default.unTpl",array("msg"=>Yii::t("common","Please Login First"),"icon"=>"fa-sign-in"));
        }else if(	Form::canAdmin((string)$form["_id"], $form) /*Yii::app()->session["userId"] == $form["author"] ||
					(	!empty($form["links"]["forms"][Yii::app()->session["userId"]]) && 
						!empty($form["links"]["forms"][Yii::app()->session["userId"]]["isAdmin"]) &&
						$form["links"]["forms"][Yii::app()->session["userId"]]["isAdmin"] == true) */) {
            if(!@$form["session"][$session])
                return $ctrl->render("co2.views.default.unTpl",array("msg"=>"Session introuvable sur ".$id,"icon"=>"fa-search"));
            else     
    		    return $ctrl->render("admin", array("id" => $id, "form" => $form));
		} else 
			return $ctrl->render("co2.views.default.unTpl",array("msg"=>Yii::t("project", "Unauthorized Access."),"icon"=>"fa-lock"));
    }
}