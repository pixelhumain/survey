<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\actions;

use CAction, PHDB, Form, MongoId, Slug, Person, Yii, Authorisation;
class RiskAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id,$view=null)
    {
    	$ctrl = $this->getController();
    	$ctrl->layout = "//layouts/empty"; 
    	$answer = PHDB::findOne( Form::ANSWER_COLLECTION, array("_id"=>new MongoId($id)));
    	$form = PHDB::findOne( Form::COLLECTION , array("id"=>$answer["formId"]));

		$parent = Slug::getElementBySlug($form["id"]);
		$userO = Person::getById($answer["user"]);

		//scenarios are sometimes copies set as db.forms.id.xxx.scenario
		//so we overload with the parent scenario map 
		$formSrc = null;
		if( is_string($form["scenario"]) && stripos( $form["scenario"] , "db.") !== false){
			$pathT = explode(".",$form["scenario"]); 
			$formSrc = PHDB::findOne( $pathT[1] , array( $pathT[2] => $pathT[3] ) );
			$form["scenario"] = $formSrc[$pathT[4]];
		}
		
    	if ( ! Person::logguedAndValid() ) 
			return $ctrl->render("co2.views.default.unTpl",array("msg"=>Yii::t("common","Please Login First"),"icon"=>"fa-sign-in"));
		else if( Form::canAdmin( (string)$form["_id"], $form ) || $answer["user"] == Yii::app()->session["userId"] || 
            Authorisation::canEditItem( Yii::app()->session["userId"], $parent["type"], $parent["id"] ) )
		{ 

			if(!@$form["session"][ $answer["session"] ])
	 				return $ctrl->render("co2.views.default.unTpl",array("msg"=>"Session introuvable sur ".$answer["formId"],"icon"=>"fa-search"));

	 		$this->getController()->pageTitle = @$answer["name"];
    		if( $form["surveyType"] == "oneSurvey" && @$answer["answers"] )
    		{
				$adminForm = ( Form::canAdmin((string)$form["_id"], $form) ) 
								? PHDB::findOne( Form::COLLECTION , array("id"=>$answer["formId"]."Admin" ) ) 
								: PHDB::findOne( Form::COLLECTION , array("id"=>$answer["formId"]."Admin" ), array("scenarioAdmin") ) ;

				
				if( !@$adminAnswers ){
					$adminAnswers = array(
						"formId" => $answer["formId"],
					    "user" 	 => $answer["user"],
					    "name"   => $userO["name"],
					);
					if(@$adminForm["scenarioAdmin"]  )
						$adminAnswers["step"] = array_keys( $adminForm["scenarioAdmin"] )[1];
				}
    			
    			$forms = PHDB::find( Form::COLLECTION , array("parentSurvey"=>$answer["formId"]));
    			foreach ($forms as $k => $v) {
    				$form["scenario"][$v["id"]]["form"] = $v;
    			}
    			$params = array( 
    				"session" 		=> $answer["session"],
		 			"answer" 		=> $answer,
		 			"form"    		=> $form,
		 			"user"	  		=> $userO,
		 			"adminForm" 	=> $adminForm,
		 			"roles" 		=> @Yii::app()->session["custom"]["roles"] );

    			if( in_array( @$answer["step"] , array( "risk","ficheAction" ) ) )
    			{
    				$params["riskCatalog"] = PHDB::find( "risks" , array("type"=>array('$ne'=>'riskTypes')) );
    			
    				$params["riskTypes"] = array();
    				foreach ($params["riskCatalog"] as $k => $v) {
    					if(!in_array($v["type"], $params["riskTypes"]) ) 
    						$params["riskTypes"][] = $v["type"];
    				}
    			}	
	 			return $ctrl->render( "risk" ,$params);
    		}
	 		else if( @$answer["formId"]  )
	 		{
	 			if( !$view ){
		 			$ctrl->layout = "//layouts/empty";	
		 			return $ctrl->render( "answer" ,array(
								 			"answer" => $answer,
								 			"form"   => $form ));
		 		} else {
		 			$params = array( 
		 									"session" => "1",
								 			"answer" => $answer,
								 			"form"   => $form,
								 			"parent" => $parent,
								 			"user"	  		=> $userO,
								 			"adminForm" 	=> $form );
		 			if(Yii::app()->request->isAjaxRequest)
		 				return $ctrl->renderPartial( $view ,$params);
		 			else 
		 				return $ctrl->render( $view ,$params);
		 		}
	 		}
		 	else 
		 		return $ctrl->render("co2.views.default.unTpl",array("msg"=>"Answer not found","icon"=>"fa-search"));
		} else 
			return $ctrl->render("co2.views.default.unTpl",array("msg"=>Yii::t("project", "Unauthorized Access."),"icon"=>"fa-lock"));
    }
}