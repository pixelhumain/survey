<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\actions;
use Answer;
use CAction;
use Person;
use Rest;
use Yii;

class DeleteAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id) {
		$ctrl=$this->getController();
		
		if ( ! Person::logguedAndAuthorized() ) 
            return Rest::json(array("result"=>false,"msg"=>Yii::t("common","Please Login First")));//            $ctrl->

        //check form et session exist
        $answer = Answer::getById($id);
        if(!empty($answer)) 
        {
            if( Answer::canEdit($answer, null, Yii::app()->session["userId"])){

                //var_dump($answer["answers"][$answer["formId"]]); exit;
                /* if(!empty($ctrl->costum) && 
                    !empty($answer["answers"]) && 
                    !empty($answer["formId"]) && 
                    !empty($answer["answers"][$answer["formId"]]) && 
                    !empty($answer["answers"][$answer["formId"]]["answers"]) && 
                    !empty($answer["answers"][$answer["formId"]]["answers"]["project"]) ){
                    $cteR = Slug::getElementBySlug($answer["formId"], array("name") ) ;


                    $project = $answer["answers"][$answer["formId"]]["answers"]["project"] ;
                    //disconnect cter.links.action(project)
                    Link::disconnect($cteR["id"], Project::COLLECTION, $project["id"], Project::COLLECTION,Yii::app()->session["userId"], "projects", null, null);
                    //Add notification - email label in child link
                    //disconnect project.links.cter(project)
                    Link::disconnect($project["id"], Project::COLLECTION, $cteR["id"], Project::COLLECTION, Yii::app()->session["userId"], "projects", null, null);
                    //disconnect project.links.answers
                    Link::disconnect($project["id"], Project::COLLECTION, $id, Form::ANSWER_COLLECTION, Yii::app()->session["userId"], "answers", null, null);

                    $where = array("_id" => new MongoId($project["id"]));
                    $action = array('$unset' => array('category' => ""));
                    PHDB::update( Project::COLLECTION, $where, $action );
                }*/
                //******************************************
                // @RemoveLinks permet d'enlever les liens dans les autres collections liés à cette réponse
                // Cette fonction peut être surchargée dans les models des costums 
                Answer::removeLinks($answer);
                $res=Answer::delete($id);
                if(!Yii::app()->request->isAjaxRequest)
                    $ctrl->redirect(Yii::app()->request->urlReferrer);
                else 
                    return Rest::json($res);


            } else
                return Rest::json(array("result"=>false,"msg"=>"Vous n'êtes pas autorisé⋅e à supprimer cette candidature"));//            $ctrl-> 
              //$ctrl->render("co2.views.default.unTpl",array("msg"=>"Réponse introuvable ou ","icon"=>"fa-search")); 
        } else 
            return Rest::json(array("result"=>false,"msg"=>"La réponse n'existe pas"));//            $ctrl->render("co2.views.default.unTpl",array("msg"=>"Survey introuvable","icon"=>"fa-search")); 
	}
}