<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\actions;

use CTKAction, PHDB, Form, Rest;
class FormAction extends CTKAction
{
    public function run($id,$session="1")
    {
        $form = PHDB::findOne( Form::COLLECTION ,array("id"=>$id));    	
    	return Rest::json( $form );
    }
}