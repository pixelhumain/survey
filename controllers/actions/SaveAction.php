<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\actions;
use CAction;
use Costum;
use Element;
use Form;
use Mail;
use Person;
use PHDB;
use Yii;
use function json_encode;

class SaveAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id) {
		$controller=$this->getController();
		
		//imagine a validation process

		if ( ! Person::logguedAndValid() ) 
 			return json_encode(array("result"=>false, "msg"=>Yii::t("common", "You are not loggued or do not have acces to this feature ")));
 		
 		if( $_POST["h"] != hash('sha256', $_POST["t"].Yii::app()->params["idOpenAgenda"] ) )
 			return json_encode( array( "result"=>false, "msg"=>Yii::t("common", "Bad Orgine request")));

 		unset( $_POST["t"] );
 		unset( $_POST["h"] );
 		$_POST["created"] = time();
 		$res = "Empty data cannot be saved";

		if ( !empty($_POST) ){
			$res = Form::save( $id , $_POST );

            $form = PHDB::findOne( Form::COLLECTION , array("id"=> $_POST["formId"] ) ); 
            
            $user=array(
                "id" =>Yii::app()->session["userId"],
                "name" => Yii::app()->session["user"]["name"],
                "email" => Yii::app()->session["userEmail"]
            );
            
            //mail de confirmation aux porteurs et à tout les Admin de la communauté
            //if parent defined 
        	if( @$_POST["formId"] && isset( $form["parentId"] ) && isset( $form["parentType"] )  )
            {
                $elParent = Element::getByTypeAndId($form["parentType"], $form["parentId"]);
                //title,urlLogo
                $elParent["title"] = @$elParent["name"];
                $elParent["urlLogo"] = @$elParent["profilThumbImageUrl"];
                Mail::confirmSavingSurvey($user, $elParent );

                foreach ( $elParent["links"][ Element::$connectTypes[ $form["parentType"] ] ] as $idL => $v)
                {
                    if( isset($v['isAdmin']) && $v['isAdmin'] )
                    {
                        $email=Person::getEmailById($idL);
                        Mail::sendNewAnswerToAdmin($email["email"], $user, $elParent);
                    }
                }
            } 
            //in cte parentSurvey carries the community
            else if( @$_POST["parentSurvey"] )
            {
                $countStepSurvey = Form::countStep($_POST["parentSurvey"]);
            	$surveyParent = Form::getById( $_POST["parentSurvey"]);
                
                Mail::confirmSavingSurvey($user, $surveyParent);

                //contact admins of the community 
            	if($_POST["formId"]==$surveyParent["id"].$countStepSurvey){
            		
            		$adminSurvey = array($surveyParent["author"]);
                    $link = Form::getSurveyByFormId($id, Person::COLLECTION, "isAdmin");

                    foreach ($link as $key => $value) {
                        $adminSurvey[] = $key;
                    }

            		foreach ($adminSurvey as $idS){
            			$email=Person::getEmailById($idS);
            			Mail::sendNewAnswerToAdmin($email["email"], $user, $surveyParent);
            		}
            	}
            }
		}
        
        Costum::sameFunction("surveyAfterSave", array("id"=>$id, "collection"=>Form::ANSWER_COLLECTION, "params"=>$_POST));
        // else
        // 	$res = Form::remove($type,$id, $label);
  		return json_encode(array("result"=>$res, "answers"=>$_POST, ""));
        exit;
	}
}