<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers\actions;

use CAction, Form, Yii;
class GenerateAction extends \PixelHumain\PixelHumain\components\Action{

    public function run($id,$copy,$pId,$pType ){

        $ctrl = $this->getController();
    	$ctrl->layout = "//layouts/empty";

        $res = Form::generate($id,$copy,$pId,$pType );
        if ( isset($res['render']) ) 
            return $ctrl->render($res["render"],array("msg"=>$res["msg"],"icon"=>$res["icon"]));
        else 
            return $ctrl->redirect( Yii::app()->createUrl("/survey/co/index/id/".$id."/session/1") );
		
    }
}