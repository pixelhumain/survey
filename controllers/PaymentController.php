<?php

namespace PixelHumain\PixelHumain\modules\survey\controllers;

use CommunecterController, Yii, PHDB, Form, Rest, Pdf;
/**
 * PaymentController.php
 * 
 * @author: Dady Christon <devchriston@gmail.com>
 * Date: 26/02/2021
 */
class PaymentController extends CommunecterController {

    public function beforeAction($action) {
		return parent::beforeAction($action);
  	}

	public function actionPay(){
		$mollie = new \Mollie\Api\MollieApiClient();

		$request = Yii::app()->request;
		
		$id = $request->getPost("orga");
		$mode = "";
		$inputSource = $request->getPost("inputSource");

		// payment's data
		$amount = $request->getPost("amount");
		$description = $request->getPost("description");
		$nombre = $request->getPost("nombre");

		// hooks
		$source = $request->getPost("source");
		$sourceChild = $request->getPost("sourceChild","");
		$page = $request->getPost("page");
		
		// Element parent
		$parent = PHDB::findOneById("organizations", $id);
		$tracktrace = $request->getPost("tracktrace","");

		// Get API Key
		$api_key = "";
		$params_path = explode(".", $inputSource);
		$coform_params = PHDB::findOne(Form::COLLECTION, array('id' => $params_path[0]));

		if(isset($coform_params["params"][$params_path[1]]["mollieMode"])){
			$mode = $coform_params["params"][$params_path[1]]["mollieMode"];
			$api_key = base64_decode($coform_params["params"][$params_path[1]]["api".$mode]);
		}

		//if(isset($parent["mollie_key_".$mode]) && !empty($parent["mollie_key_".$mode])){
		//	$api_key = $parent["mollie_key_".$mode];
		//}

		if($api_key!="" && $mode!=""){

			$mollie->setApiKey($api_key);
			# $mollie->setAccessToken("Here is token if needed");
				
			$orderId = time();

			$protocol = isset($_SERVER['HTTPS']) && strcasecmp('off', $_SERVER['HTTPS']) !== 0 ? "https" : "http";
	    	$hostname = $_SERVER['HTTP_HOST'];
	    	$path = dirname(isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : $_SERVER['PHP_SELF']);

	    	$payment_data = [
			    "amount" => [
			        "currency" => "EUR",
			        "value" => $amount["value"]
			    ],
			    "description" => $description,
			    "redirectUrl" => "{$protocol}://{$hostname}{$path}/check/?order_id={$orderId}&source={$source}&path={$inputSource}&page={$page}",
		        "webhookUrl" => "{$protocol}://{$hostname}{$path}/check/?order_id={$orderId}&source={$source}&path={$inputSource}&page={$page}",
		        "metadata" => [
		            "order_id" => $orderId
		        ]
			];

			if($mode=="Test" || $mode=="test"){
				$payment_data["method"] = "creditcard";
			}

			$payment = $mollie->payments->create($payment_data);

			# Keep payment in database
			$paymentArray = array(
	        		'payment' => array(
	        			'id' => $payment->id, 
	        			'currency' => "EUR",
	        			'value' => $amount["value"],
	        			'description' => $description,
	        			'nbPart' => $nombre,
	        			'isPaid' => false
	        		),
	        		'collection' => 'payments',
	        		'user' => $_SESSION["userId"],
	        		'parent' => array('id' => $id),
	        		'source' => array(
	        			'key' => $source,
	        		),
	        		'sourceChild'=> $sourceChild,
	        		'tracktrace'=> $tracktrace,
	        		'orderId' => "$orderId",
	        		'mode' => $mode
	        	);
			Yii::app()->mongodb->selectCollection("payments")->insert($paymentArray);


			return Rest::json(array('url' => $payment->getCheckoutUrl()));
		}else{
			return Rest::json(array('message' => "Une problème s'est produite. Veuillez reéssayer si le problème persiste, contacter l'administrateur."));	
		}
	}


	# Check if payment is paid
	public function actionCheck($order_id, $source, $path, $page){
		$mollie = new \Mollie\Api\MollieApiClient();
		# $mollie->setAccessToken("Here is token if needed"); 0340287138

		$p = PHDB::findOne("payments", array('orderId' => "$order_id"));
		
		$params_path = explode(".", $path);
		$coform_params = PHDB::findOne(Form::COLLECTION, array('id' => $params_path[0]));
		$api_key = "";
		if(isset($coform_params["params"][$params_path[1]]["mollieMode"])){
			$mode = $coform_params["params"][$params_path[1]]["mollieMode"];
			$api_key = base64_decode($coform_params["params"][$params_path[1]]["api".$mode]);
		}

		$parent = PHDB::findOneById("organizations", $p["parent"]["id"]);

		if($api_key!=""){

			$mollie->setApiKey($api_key);

			if(isset($p["payment"]["id"])){
				
				$payment = $mollie->payments->get($p["payment"]["id"]);

				if ($payment->isPaid()){
				    $res = array('isPaid' => true);
				    PHDB::update("payments", array('orderId' => "$order_id"), array('$set' => array('payment.isPaid' => true, 'payment.status' => $payment->status)));				    
			    	PHDB::update("answers", array('user' => $_SESSION["userId"], 'form' => "$source"), array('$set' => array('answers.isPaid' => true, 'answers.status' => $payment->status)));
				}else{
				    $res = array('isPaid' => false);
				    PHDB::update("payments", array('orderId' => "$order_id"), array('$set' => array('payment.status' => $payment->status)));				    
			    	PHDB::update("answers", array('user' => $_SESSION["userId"], 'form' => "$source"), array('$set' => array('answers.isPaid' => false, 'answers.status' => $payment->status)));
				}
			}else{
				$res = array('isPaid' => false);
			}

			return $this->redirect("$page");
		}else{
			return Rest::json(array('message' => "Payement invalide"));	
		}
	}

	public function actionGetPaymentMethods()
	{
		$mollie = new \Mollie\Api\MollieApiClient();

		$request = Yii::app()->request;
		
		$param1 = $request->getPost("param1");
		$param2 = $request->getPost("param2");

		$coform_params = PHDB::findOne(Form::COLLECTION, array('id' => $param1));
		$api_key = "";
		if(isset($coform_params["params"][array_keys($coform_params["params"])[0]]["mollieMode"])){
			$mode = $coform_params["params"][array_keys($coform_params["params"])[0]]["mollieMode"];
			$api_key = base64_decode($coform_params["params"][array_keys($coform_params["params"])[0]]["api".$mode]);
		}

		$methods = array();

		if($api_key!=""){
			$mollie->setApiKey($api_key);
			// Methods for the Payments API
			$methods = $mollie->methods->allActive();
		}

		return Rest::json($methods);
	}

	public function actionInvoice($id=null, $slug=null, $user=null, $form=null) {
		$doc = array();

		$orga = PHDB::findOne("organizations",  array('slug' => $slug ));
		$orga["siret"] = "51338183000019";

		$payment = PHDB::findOne("payments", array('user' => $user, 'source.key'=> $form));
		$customer = PHDB::findOneById("citoyens", $user);

		$params = array(
			'organization' => $orga,
			'associe' => $customer,
			'payment' => $payment,
			'currency' => "€"
		);

		$tpl = $this->renderPartial('application.views.pdf.factureOpenAtlas', $params, true);

		$doc["html"] = $tpl;

		Pdf::createPdf($doc);

		//echo $tpl;
    }
}
