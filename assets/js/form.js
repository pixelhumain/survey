// TO INIT AJOUTER DANS PARAMS CONFIG app.#search
// >> "filters":"co2.views.app.filters.search",

var formObj = {
	container : "#central-container",
	forms : {},
	el : {},
	openFormList : {},
	labels : {
		answer : trad.folder,
		answers : trad.folders,
	},
	titletype : [
		"text",
		"select",
		"tpls.forms.cplx.multitextvalidation",
		"tpls.forms.cplx.multiCheckboxPlus",
		"tpls.forms.cplx.multiRadio",
		"tpls.forms.cplx.name"
	],
	//filterSearch : {},
	init : function (pInit = null) {
		mylog.log("fObj init", pInit);
		//Init variable
		var copyFilters = jQuery.extend(true, {}, formObj);
		copyFilters.initVar(pInit);
		return copyFilters;

	},
	initVar : function (pInit) {
		// this.filterSearch = searchObj.init(this.paramsFilter);
		// this.filterSearch.header.init(this.filterSearch);
	},
	initDefaults : function (pInit) {},
	initEvents : function (pInit) {},
	initViews : function (pInit) {
		$(".central-section").removeClass("col-lg-10 col-lg-offset-1").addClass("col-lg-12");
		if (location.hash.indexOf(hashUrlPage + ".view.forms.dir.form") >= 0) {
			var hashArray = location.hash.split('.');
			this.urls.form(this, hashArray[5]);
		} else if (location.hash.indexOf(hashUrlPage + ".view.forms.dir.answer.") >= 0) {
			var hashArray = location.hash.split('.');
			mylog.log("formObj.initViews hashArray", hashArray);
			var form = null;
			var answer = null;
			var mode = null;
			var contxtId = contextId;
			var contxtType = contextType;

			if (hashArray.indexOf('answer') != -1) {
				answer = hashArray[hashArray.indexOf('answer') + 1];
			}
			if (hashArray.indexOf('form') != -1) {
				form = hashArray[hashArray.indexOf('form') + 1];
			}
			if (hashArray.indexOf('mode') != -1) {
				mode = hashArray[hashArray.indexOf('mode') + 1];
			}
			if (hashArray.indexOf('contextId') != -1) {
				contxtId = hashArray[hashArray.indexOf('contextId') + 1];
			}
			if (hashArray.indexOf('contextType') != -1) {
				contxtType = hashArray[hashArray.indexOf('contextType') + 1];
			}

			this.urls.answer(this, answer, form, mode, contxtId, contxtType);


		} else if (location.hash.indexOf(hashUrlPage + ".view.forms.dir.answers.form") >= 0) {
			var hashArray = location.hash.split('.');
			this.urls.answers(this, hashArray[6]);
		} else if (location.hash.indexOf(hashUrlPage + ".view.forms.dir.myAnswers.form") >= 0) {
			var hashArray = location.hash.split('.');
			this.urls.answers(this, hashArray[6]);
		} else if (location.hash.indexOf(hashUrlPage + ".view.forms.dir.observatory") >= 0) {
			var hashArray = location.hash.split('.');
			if (hashArray.indexOf("contextId") >= 0 && hashArray.indexOf("contextType") >= 0) {
				this.urls.observatory(this, hashArray[5], null, hashArray[hashArray.indexOf("contextId") + 1], hashArray[hashArray.indexOf("contextType") + 1]);
			} else {
				this.urls.observatory(this, hashArray[5]);
			}
		} else if (location.hash.indexOf(hashUrlPage + ".view.forms.dir.graphbuilder") >= 0) {
			var hashArray = location.hash.split('.');
			this.urls.graphbuilder(this, hashArray[5]);
		} else if (location.hash.indexOf(hashUrlPage + ".view.forms.dir.ocecoformdashboard") >= 0) {
			var hashArray = location.hash.split('.');
			this.urls.ocecoformdashboard(this, hashArray[5], null, hashArray[hashArray.indexOf("contextId") + 1], hashArray[hashArray.indexOf("contextType") + 1]);
		} else if (location.hash.indexOf(hashUrlPage + ".view.forms.dir.ocecoorgadashboard") >= 0) {
			var hashArray = location.hash.split('.');
			this.urls.ocecoorgadashboard(this, hashArray[hashArray.indexOf("contextId") + 1]);
		} else if (location.hash.indexOf(hashUrlPage + ".view.forms.dir.evaluate") >= 0) {
			var hashArray = location.hash.split('.');
			this.urls.globalEvaluation(this, hashArray[hashArray.indexOf("evaluate") + 1]);
		} else {
			this.urls.forms(this);
		}
	},
	urls : {
		forms : function (fObj) {
			ajaxPost('#central-container', baseUrl + '/survey/form/get/slug/' + contextData.slug + '/tpl/json',
				null,
				function (data) {
					var str = "";
					mylog.log("pageProfil.views.forms ajaxPost data", data);
					if (typeof data != "undefined" && data != null &&
						data.forms != "undefined" && data.forms != null) {
						fObj.el = data.el;
						fObj.openFormList = fObj.openForm.list();
						fObj.views.forms(fObj, data.forms);
					}
					history.replaceState(location.hash, "", hashUrlPage + ".view.forms");

				}, "html");
		},
		form : function (fObj, id) {
			ajaxPost(fObj.container, baseUrl + '/survey/form/edit/id/' + id,
				null,
				function () {
					history.replaceState(location.hash, "", hashUrlPage + ".view.forms.dir.form." + id);
				}, "html");
		},
		answer : function (fObj, id, form, mode, contextId, contextType) {
			mylog.log("fObj.urls.answer", fObj, id, form);
			var url = '/survey/answer/index/id/' + id;
			var addHash = ".view.forms.dir.answer." + id;
			if (typeof form != "undefined" && form != null) {
				url += '/form/' + form;
			}
			if (typeof mode != "undefined" && mode != null) {
				url += '/mode/' + mode;
				addHash += ".mode." + mode;
			}

			if (typeof contextId != "undefined" && contextType != "undefined") {
				url += '/contextId/' + contextId + '/contextType/' + contextType;
				addHash += '.contextId.' + contextId + '.contextType.' + contextType;
			}

			ajaxPost(fObj.container, baseUrl + url,
				null,
				function () {
					mylog.log("formWizard!! FORM", hashUrlPage, location.hash);

					if (id === "new" && typeof answerId != "undefined") {
						history.replaceState(location.hash, "", hashUrlPage + ".view.forms.dir.answer." + answerId + '.mode.w');
					} else {
						history.replaceState(location.hash, "", hashUrlPage + addHash);
					}
				}, "html");
		},
		answers : function (fObj, form) {
			mylog.log("formObj.urls.answers", fObj, form);
			ajaxPost(null,
				baseUrl + "/survey/answer/directory/form/" + form,
				null,
				function (data) {
					history.replaceState(location.hash, "", hashUrlPage + ".view.forms.dir.answers.form." + form);
					fObj.views.answers(fObj, data.results);
				});
		},
		// observatory : function(fObj, form){
		// 	mylog.log("formObj.urls.observatory", fObj, form);
		// 	var params = {
		// 		userId : userId
		// 	}
		// 	getAjax("#central-container", baseUrl+"/survey/sommom/dashboard/form/"+form,
		//           	function(data){
		//           		history.replaceState(location.hash, "", hashUrlPage+".view.forms.dir.observatory.form."+form);
		//           	},"html");
		//           	},"html");

		observatory : function (fObj, id, form, contextId, contextType) {
			mylog.log("fObj.urls.observatory", fObj, id, form);
			var url = '/survey/answer/dashboard/answer/' + id + '/elSlug/' + contextData.slug;
			var addHash = ".view.forms.dir.observatory." + id;
			if (typeof contextId != "undefined" && contextId != null &&
				typeof contextType != "undefined" && contextType != null) {
				url += '/contextId/' + contextId + '/contextType/' + contextType;
				addHash += '.contextId.' + contextId + '.contextType.' + contextType;
			}
			ajaxPost(fObj.container, baseUrl + url,
				null,
				function () {
					history.replaceState(location.hash, "", hashUrlPage + addHash);
				}, "html");
		},
		graphbuilder : function (fObj, id, form, contextId, contextType) {
			mylog.log("fObj.urls.graphbuilder", fObj, id, form);
			var url = '/survey/answer/graphbuilder/answer/' + id;
			var addHash = ".view.forms.dir.graphbuilder." + id;
			if (typeof contextId != "undefined" && contextId != null &&
				typeof contextType != "undefined" && contextType != null) {
				url += '/contextId/' + contextId + '/contextType/' + contextType;
				addHash += '.contextId.' + contextId + '.contextType.' + contextType;
			}
			ajaxPost(fObj.container, baseUrl + url,
				null,
				function () {
					history.replaceState(location.hash, "", hashUrlPage + addHash);
				}, "html");
		},
		ocecoformdashboard : function (fObj, id, form, contextId, contextType) {
			mylog.log("fObj.urls.ocecoformdashboard", fObj, id, form);
			var url = '/survey/answer/ocecoformdashboard/answerid/' + id;
			var addHash = ".view.forms.dir.ocecoformdashboard." + id;
			ajaxPost(fObj.container, baseUrl + url,
				null,
				function () {
					history.replaceState(location.hash, "", hashUrlPage + addHash);
				}, "html");
		},
		ocecoorgadashboard : function (fObj, contextId) {
			mylog.log("fObj.urls.ocecoorgadashboard", fObj, contextId);
			if (typeof contextId != "undefined" && contextId != null) {
				var url = '/survey/answer/ocecoorgadashboard/contextid/' + contextId;
				var addHash = ".view.forms.dir.ocecoorgadashboard." + contextId;
				ajaxPost(fObj.container, baseUrl + url,
					null,
					function () {
						history.replaceState(location.hash, "", hashUrlPage + addHash);
					}, "html");
			}
		},
		globalEvaluation : function (fObj, parentformid) {
			//coInterface.showLoader(fObj.container);
			ajaxPost("#mainDash", baseUrl + '/survey/answer/aapevaluate/parentformid/' + parentformid,
				null,
				function () {
					if (typeof hashUrlPage != "undefined") {
						history.replaceState(location.hash, "", hashUrlPage + ".view.forms.dir.evaluate." + parentformid);
					}
				}, "html");
		}
	},
	// initFilters : function(){},
	// pagination : {},
	// scroll : {},
	// header : {},
	// results : {},
	// search : {},
	views : {
		forms : function (fObj, forms) {
			var str = '';
			fObj.forms = forms;
			str += '<main class="">' +
				'<div class="col-xs-12 col-lg-10 col-lg-offset-1 no-padding">' +
				'<div class="page__title">' +
				'<h1> COform </h1>';
			/*			str +=  		`<div class="btn-group pull-right">
			 <button type="button" class="btn btn-success choosetemplateForm">Choisir un template coForm</button>
			 </div>`;*/
			str += canEdit ? `<div class="btn-group pull-right">
								<button type="button" class="btn btn-success chooseAapForm hidden">Menu template </button>
							</div>` : '';
			/*			str +=  		`<div class="btn-group pull-right">
			 <button type="button" class="btn btn-success copyForm">Copier un coForm</button>
			 </div>`;*/
			str += canEdit ? `<div class="btn-group pull-left">
								<button type="button" class="btn btn-primary createForm">` + trad.newform + `</button>
							</div>` : '';
			str += ' </div>';
			str += '<div class="col-xs-12 no-padding" role="row">';
			$.each(forms, function (idF, valForm) {
				str += fObj.views.newform(idF, valForm, fObj);
			});
			str += ' 	</div>'
			'</div>' +
			'</main>';

			$(fObj.container).html(str);
			fObj.events.form(fObj);
			fObj.events.add(fObj);
			fObj.events.share(fObj);
			fObj.events.answers(fObj);
		},
		form : function (id, form) {
			mylog.log("formProfil.views.form", id, form);
			var panelColor = "panel-primary";
			var formCostum = false;
			var urlCostum = "";
			var datatypeform = "";
			if (typeof form.source != "undefined" &&
				typeof form.source.insertOrign != "undefined" &&
				form.source.insertOrign == "costum") {
				panelColor = "panel-success";
				formCostum = true;
				urlCostum = baseUrl + '/costum/co/index/id/' + form.source.key;
			}
			form.active = (form.active === true || form.active === "true");
			form.hasObservatory = true;
			if (typeof form.openForm != "undefined" && form.openForm == true) {
				datatypeform = 'data-type="openform"';
			} else {
				datatypeform = '';
			}
			var str = '<div class="col-sm-6 col-xs-12" id="' + id + 'Form">' +
				'<div class="panel ' + panelColor + '">' +
				'<div class="panel-heading">' +
				'<h3 class="panel-title">' + form.name;
			if (typeof form.canEditForm != "undefined" && form.canEditForm === true) {
				str += '<a href="javascript:;" class="pull-right deleteFormBtn" data-id="' + id + '">' +
					'<i class="fa fa-trash text-red"></i></a>';
				if (formCostum == true) {
					str += '<a href="' + urlCostum + '#form.edit.id.' + id + '" target="_blanc" class="pull-right">' +
						'<i class="fa fa-pencil text-dark"></i>' +
						'</a>';
				} else {
					str += '<a href="javascript:;" data-id="' + id + '" class="pull-right config">' +
						'<i class="fa fa-pencil text-dark"></i>' +
						'</a>';
				}
			}
			str += '</h3>';

			str += '</div>' +
				'<div class="panel-body ">';
			if (typeof form.active != "undefined" && (form.active === true)) {
				str += '<span class="label label-success">' + trad.activate + '</span>';
			} else {
				str += '<span class="label label-danger">' + trad.notactivated + '</span>';
			}

			str += '<div class="col-xs-12">';
			str += '<a href="javascript:;" data-id="' + id + '" class="myAnswers btn btn-xs btn-primary col-sm-12 bold">' +
				'<i class="fa fa-pencil"></i> ' + trad.myanswers +
				'</a>';

			if (typeof form.active != "undefined" && form.active === true) {
				// if(formCostum == true){
				// 	str +=  '<a href="'+urlCostum+'#answer.index.id.new.form.'+id+'" target="_blanc" class="btn btn-xs btn-primary col-sm-12
				// bold">'+ '<i class="fa fa-file-text-o"></i> '+trad.applicationform+ '</a>'; } else {
				str += '<a href="javascript:;" data-id="' + id + '" ' + datatypeform + ' class=" application btn btn-xs btn-primary col-sm-12 bold">' +
					'<i class="fa fa-file-text-o"></i> ' + trad.applicationform +
					'</a>';
				//}

				if (typeof form.canEditForm != "undefined" && form.canEditForm === true) {
					str += '<a href="javascript:;" data-id="' + id + '" class="allAnswers btn btn-xs btn-primary col-sm-12 bold">' +
						'<i class="fa fa-pencil"></i> ' + trad.allanswers +
						'</a>';
				}
			}
			if (form.hasObservatory) {
				str += '<a href="javascript:;" data-id="' + id + '" ' + datatypeform + ' class="observatory btn btn-xs btn-primary col-sm-12 bold">' +
					'<i class="fa fa-pie-chart"></i> ' + trad.observatory +
					'</a>';
			}
			str += '</div>' +
				'</div>' +
				'</div>' +
				'</div>';
			return str;

		},
		newform : function (id, form, fObj) {
			mylog.log("formProfil.views.form", id, form);
			var panelColor = "panel-primary";
			var formCostum = false;
			var urlCostum = "";
			var datatypeform = "";
			var elconfigslug;
			var today = new Date();
			if (typeof form.type != "undefined" && (form.type === "aap" || form.type === "templatechild") && typeof form.config != "undefined") {

				ajaxPost(
					null,
					baseUrl + "/survey/form/getaapconfig/formid/" + form.config,
					null,
					function (data) {
						mylog.log('ireto', data);
						elconfigslug = data;
					},
					null,
					"",
					{async : false}
				);
			}

			if (typeof form.source != "undefined" &&
				typeof form.source.insertOrign != "undefined" &&
				form.source.insertOrign == "costum") {
				panelColor = "panel-success";
				formCostum = true;
				urlCostum = baseUrl + '/costum/co/index/id/' + form.source.key;
			}
			form.active = (form.active === true || form.active === "true");
			form.hasObservatory = true;
			if (typeof form.openForm != "undefined" && form.openForm == true) {
				datatypeform = 'data-type="openform"';
			}

			if (typeof form.description == "undefined" || form.description == null) {
				form.description = "<b> (" + trad.nodescription + ") </b>";
			}

			if (!notEmpty(form.startDate)) {
				if (today.getMonth() == 12) {
					form.startDate = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
				}
			}

			if (!notEmpty(form.endDate)) {
				if (today.getMonth() <= 9) {
					form.endDate = today.getDate() + '/' + (today.getMonth() + 3) + '/' + today.getFullYear();
				} else {
					form.endDate = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
				}
			}

			var str = "";
			if (typeof form.type != "undefined" && (form.type == "aapConfig")) {
				//do not qhow aapConfig
			} else {
				str += '<div class="col-md-6 col-xs-12 panel-form" data-num="1">';
				str += '<section class="widget">';
				str += '    <div class="widget-body">';
				if (typeof form.active != "undefined" && (form.active === true)) {
					str += '        <div class="widget-top-overflow windget-padding-md clearfix bg-active text-white">';
				} else {
					str += '        <div class="widget-top-overflow windget-padding-md clearfix bg-suspendu text-white">';
				}
				str += '            <h3 class="mt-lg mb-lg">' + form.name;
				if (typeof form.type != "undefined" && (form.type === "aapConfig")) {
					str += '<span class="small"> (' + trad.templatecallproposals + ') </span></h3>';
				}
				if (typeof form.type != "undefined" && (form.type === "template")) {
					str += '<span class="small"> (Template) </span></h3>';
				}
				str += '            <ul class="tags-form text-white pull-right">';
				if (typeof form.active != "undefined" && (form.active === true)) {
					str += '                <li><span><i class="fa fa-check-circle"></i> ' + trad.activate + '</span></li>';
				} else {
					str += '                <li><span><i class="fa fa-times-circle"></i> ' + trad.notactivated + '</span></li>';
				}

				mylog.log("eaz", form);

				if (typeof form.type != "undefined" && (form.type === "aap")) {
					str += '                <li><span><i class="fa fa-check-circle"></i> ' + trad.Callforproposalsform + '</span></li>';
					str += '                <li><span><i class="fa fa-check-circle"></i> ' + trad.originaltemplate + ': ' + (notNull(elconfigslug.forms) ? elconfigslug.forms.name : "") + '</span></li>';
				}

				if (typeof form.type != "undefined" && (form.type === "templatechild")) {
					str += '                <li><span><i class="fa fa-check-circle"></i> ' + trad.originaltemplate + ': ' + elconfigslug.forms.name + '</span></li>';
				}

				if (typeof form.copyable != "undefined" && (form.copyable == true)) {

				}

				str += '            </ul>';
				str += '        </div>';

				str += '        <div class="text-desc">' + (!form.description.trim() ? '<p class="text-light">' + trad.nodescription + '</p>' : dataHelper.markdownToHtml(form.description, {
					parseImgDimensions : true,
					simplifiedAutoLink : true,
					strikethrough : true,
					tables : true,
					openLinksInNewWindow : true
				})) + '</div>';
				str += '    </div>';
				str += '    <footer class="bg-body-light">';
				str += '        <ul class="post-comments mt mb-0">';
				if (typeof form.type != "undefined" && (form.type == "aapConfig")) {
				} else {
					str += '            <li class="form-date">';
					str += '                <span class="thumb-xs avatar pull-left mr-sm">';
					str += '                    <i class="fa fa-calendar"></i>';
					str += '                </span>';
					str += '                <div class="comment-body">';
					str += '                    <h6 class="author fw-semi-bold">' + trad.start + ' </h6>';
					str += '                    <p>' + (typeof form.startDate != "undefined" && form.startDate !== null ? (typeof form.startDate === 'object' && typeof form.startDate.sec === 'number' ? moment.unix(form.startDate.sec).format('DD/MM/YYYY') : form.startDate) : trad.Nodate) + '</p>';
					str += '                </div>';
					str += '            </li>';
					str += '            <li class="form-date">';
					str += '                <span class="thumb-xs avatar pull-left mr-sm">';
					str += '                    <i class="fa fa-calendar"></i>';
					str += '                </span>';
					str += '                <div class="comment-body">';
					str += '                    <h6 class="author fw-semi-bold"> ' + trad.end + ' </h6>';
					str += '                    <p>' + (typeof form.endDate != "undefined" && form.endDate !== null ? (typeof form.endDate === 'object' && typeof form.endDate.sec === 'number' ? moment.unix(form.endDate.sec).format('DD/MM/YYYY') : form.endDate) : trad.Nodate) + '</p>';
					str += '                </div>';
					str += '            </li>';
				}
				str += '            <li class="text-center form-btn-container">';
				if (typeof form.canEditForm != "undefined" && form.canEditForm === true) {
					if (formCostum == true) {
						str += ` <a href="${ urlCostum }#form.edit.id.${ id }" target="_blanc" class="btneditform btn btn-default" data-toggle="tooltip" data-placement="bottom" data-original-title="${ tradForm.EditQuestionnaire }"><i class="fa fa-pencil"></i></a>`;
					} else {
						if (typeof form.type != "undefined" && (form.type == "aapConfig" || form.type == "templatechild")) {} else if (typeof form.type != "undefined" && (form.type == "aap") && typeof elconfigslug != "undefined" && elconfigslug.parent != "undefined" && elconfigslug.parent.slug != "undefined") {
							str += `<a href="${ baseUrl }/costum/co/index/slug/${ elconfigslug.parent.slug }#configurationAap.context.${ elementData.slug }.formid.${ id }.aapview.inputlist" target="_blank" data-id="${ id }" class="btneditform btn btn-default" data-toggle="tooltip" data-placement="bottom" data-original-title="${ tradForm.EditQuestionnaire }"><i class="fa fa-pencil"></i></a>`;
						} else {
							str += `<a href="javascript:;" data-id="${ id }" class="btneditform btn btn-default config" data-toggle="tooltip" data-placement="bottom" data-original-title="${ tradForm.EditQuestionnaire }"><i class="fa fa-pencil"></i></a>`;
						}
					}
				}

				var sbsl = [];
				if (typeof form != "undefined" && typeof form.subForms != "undefined") {
					$.each(form.subForms, function (ind, val) {
						if (typeof form[val] != "undefined" && form[val] != null && typeof form[val].name != "undefined") {
							sbsl.push(form[val].name);
						}
					});
				}
				if (typeof form.canEditForm != "undefined" && form.canEditForm === true) {
					str += isUserConnected == "logged" ? '<a href="javascript:;" data-id="' + id + '" data-form=\'' + escapeHtml(JSON.stringify(form)) + '\' data-subselect="' + escapeHtml(JSON.stringify(sbsl)) + '" class="btnConfigForm btn btn-default" data-toggle="tooltip" data-placement="bottom" data-original-title="' + tradForm.SetUp + '"><i class="fa fa-cog"></i></a>' : '';
				}
				var ttsl = {};
				if (typeof form != "undefined" && typeof form.subForm != "undefined") {
					$.each(form.subForms, function (ind, val) {
						if (typeof form[val] != "undefined" && form[val] != null && typeof form[val].inputs != "undefined") {
							$.each(form[val].inputs, function (ind2, val2) {
								if (fObj.titletype.includes(val2.type)) {
									ttsl[ind2] = val2.label;
								}
							});
						}
					});
				}

				//str +=	'<a href="javascript:;" data-id="'+id+'" data-form=\''+escapeHtml(JSON.stringify(form))+'\' c
				// data-titleselect=\''+escapeHtml(JSON.stringify(ttsl))+'\' class="btnConfigForm btn btn-default" data-toggle="tooltip"
				// data-placement="bottom" data-original-title='+trad.modifyformdetailsanddesciption+'><i class="fa fa-cog"></i></a>';

				/*if(typeof form.type != "undefined" && (form.type !== "aapConfig")) {
				 str +=	'					<a href="'+baseUrl+'/costum/co/index/slug/'+elementData.slug+'"  class=" btn btn-info aapConfigCostum" > Costum ' +
				 '							<span class="pull-right tooltips " data-toggle="tooltip" data-placement="bottom" data-original-title="Créer votre costum">' +
				 '								<img src="'+assetPath+'/images/logo_costum.png" style="width: 25px;margin-top: -4px">' +
				 '                           </span>' +
				 '						</a>';
				 }*/
				if (typeof form.type != "undefined" && (form.type == "aapConfig" || form.type == "aap")) {} else {
					var newResponse = 0;
					ajaxPost("", baseUrl + '/survey/answer/countvisit/formId/' + id,
						null,
						function (res) {
							if (res.notSeenCounter) {
								if (res.notSeenCounter != 0) {
									newResponse = res.notSeenCounter;
								}
							}
						}, null, null,
						{
							async : false
						}
					);
					str += `${ newResponse > 0 ? '<span class="badge position-absolute">' + newResponse + '</span>' : '' }<a href="javascript:;" data-id="${ id }" ${ datatypeform } class="observatory margin-right-5 btn btn-default" data-toggle="tooltip" data-placement="bottom" data-original-title="${ trad.listanswers }" > <i class="fa fa-envelope"></i></a>`;
					str += form.active && isUserConnected == "logged" ? '<a href="javascript:;" data-id="' + id + '" data-parentformid="' + form._id.$id + '" ' + datatypeform + ' class="addAnswer btn btn-default" data-toggle="tooltip" data-placement="bottom" data-original-title="' + trad.answertheform + '"> <i class="fa fa-plus"></i></a>' : '';
				}

				if (typeof form.type != "undefined" && (form.type === "aap") && typeof elconfigslug != "undefined" && typeof elconfigslug.parent != "undefined" && typeof elconfigslug.parent.slug != "undefined") {
					str += '					<a href="' + baseUrl + '/costum/co/index/slug/' + elconfigslug.parent.slug + '#proposalAap.context.' + elementData.slug + '.formid.' + id + '.aappage.details" target="_blank" class=" btn btn-default aapCostum" data-toggle="tooltip" data-placement="bottom" data-original-title=' + trad.gotocallproposals + '>  ' +
						'							<span class="pull-right">' +
						'								<img src="' + assetPath + '/images/logo_costum.png" style="width: 25px;margin-top: -4px">' +
						'                           </span>' +
						'						</a>';
				}

				if (typeof elementData != "undefined" &&
					typeof elementData.costum != "undefined") {

					str += `<div class="dropdown" style="display:inline-flex;">
								<button id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" type="button" class="btn btn-default bg-active dropdown-toggle" ><i class="fa fa-plus-square-o"></i><i class="fa fa-link"></i></button>
								<ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
									<li data-id="${id}" ${datatypeform} class="copyFormLink margin-right-5 btn" data-toggle="tooltip" data-placement="bottom" data-original-title="${tradForm.LinkToAnswerStandalone}" data-clipboard-text="${baseUrl}/costum/co/index/slug/${elementData.slug}#answer.index.id.new.form.${id}.standalone.true" data-clipboard-action="copy"> Copier le lien pour une nouvelle réponse </li>
									<li data-id="${id}" ${datatypeform} class="openFormLink margin-right-5 btn" data-toggle="tooltip" data-placement="bottom" data-original-title="${tradForm.LinkToAnswerStandalone}" data-clipboard-text="${baseUrl}/costum/co/index/slug/${elementData.slug}#answer.index.id.new.form.${id}.standalone.true" data-clipboard-action="copy"> Créer une nouvelle réponse dans un nouvel onglet </li>
								</ul>
						     </div>
						    `;

				} else {
					str += `<div class="dropdown" style="display:inline-flex;">
								<button id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" type="button" class="btn btn-default bg-active dropdown-toggle" ><i class="fa fa-link"></i></button>
								<ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
									<li data-id="${id}" ${datatypeform} class="copyFormLink margin-right-5 btn" data-toggle="tooltip" data-placement="bottom" data-original-title="${tradForm.LinkToAnswerStandalone}" data-clipboard-text="${baseUrl}/${urlCostum}#answer.index.id.new.form.${id}.standalone.true" data-clipboard-action="copy"> Copier le lien pour une nouvelle réponse </a></li>
									<li data-id="${id}" ${datatypeform} class="openFormLink margin-right-5 btn" data-toggle="tooltip" data-placement="bottom" data-original-title="${tradForm.LinkToAnswerStandalone}" data-clipboard-text="${baseUrl}/${urlCostum}#answer.index.id.new.form.${id}.standalone.true" data-clipboard-action="copy"> Créer une nouvelle réponse dans un nouvel onglet </a></li>
								</ul>
						     </div>           
						    `;
				}

				if (typeof elementData != "undefined" &&
					typeof elementData.costum != "undefined") {

					str += `<div class="dropdown" style="display:inline-flex;">
								<button id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" type="button" class="btn btn-default bg-active dropdown-toggle" ><i class="fa fa-columns"></i></button>
								<ul class="dropdown-menu" aria-labelledby="dropdownMenu3">
									<li data-id="${id}" ${datatypeform} class="copyFormLink margin-right-5 btn" data-toggle="tooltip" data-placement="bottom" data-original-title="${tradForm.LinkToAnswerSlide}" data-clipboard-text="${baseUrl}/costum/co/index/slug/${elementData.slug}#answer.answer.id.new.form.${id}.view.slide" data-clipboard-action="copy"> Copier le lien pour une nouvelle réponse </li>
									<li data-id="${id}" ${datatypeform} class="openFormLink margin-right-5 btn" data-toggle="tooltip" data-placement="bottom" data-original-title="${tradForm.LinkToAnswerSlide}" data-clipboard-text="${baseUrl}/costum/co/index/slug/${elementData.slug}#answer.answer.id.new.form.${id}.view.slide" data-clipboard-action="copy"> Créer une nouvelle réponse dans un nouvel onglet </li>
								</ul>
						     </div>           
						    `;

				} else {
					str += `<div class="dropdown" style="display:inline-flex;">
								<button id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" type="button" class="btn btn-default bg-active dropdown-toggle" ><i class="fa fa-columns"></i></button>
								<ul class="dropdown-menu" aria-labelledby="dropdownMenu3">
									<li data-id="${id}" ${datatypeform} class="copyFormLink margin-right-5 btn" data-toggle="tooltip" data-placement="bottom" data-original-title="${tradForm.LinkToAnswerStandalone}" data-clipboard-text="${baseUrl}/${urlCostum}#answer.answer.id.new.form.${id}.view.slide" data-clipboard-action="copy"> Copier le lien pour une nouvelle réponse </li>
									<li data-id="${id}" ${datatypeform} class="openFormLink margin-right-5 btn" data-toggle="tooltip" data-placement="bottom" data-original-title="${tradForm.LinkToAnswerStandalone}" data-clipboard-text="${baseUrl}/${urlCostum}#answer.answer.id.new.form.${id}.view.slide" data-clipboard-action="copy"> Créer une nouvelle réponse dans un nouvel onglet </li> 
								</ul>
						     </div>           
						    `;
				}

				str += '<div class="dropdown" style="display:inline-flex;">' +
					'                <button id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" type="button" class="btn btn-default bg-active dropdown-toggle" ><i class="fa fa-plus-square-o"></i> ' + tradForm.Actions + ' <span class="caret"></span></button>' +
					'                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">' +
					'                    <li>';

				if (typeof form.canEditForm != "undefined" && form.canEditForm === true) {
					if (typeof form.type == "undefined" && form.type != "aapConfig" && form.type != "template" && form.type != "aap" && form.type != "templatechild") {
						str += '                <li><a href="javascript:;"  class="margin-right-5 small templatizeFormBtn" data-id="' + id + '" > ' + trad.Transformintotemplate + ' <i class="fa fa-copy"></i></a><li>';
					}
					str += '<li><a href="javascript:;"  class="margin-right-5 small  deleteFormBtn" data-id="' + id + '" > ' + trad.delete + ' <i class="fa fa-trash"></i></a><li>';
				}

				str += '						' +

					'                </ul>' +
					'            </div>';
				str += '           </li>';
				str += '        </ul>';
				str += '    </footer>';
				str += '</section>';
				str += '</div>';
			}
			return str;
		},
		answers : function (fObj, answers) {
			//mylog.log("formProfil.views.answers");
			var str = '';
			var ct = 0;
			var globalLinks = [];
			var gUids = [];
			var lbl = "";
			if (typeof answers != "undefined" && answers != null && Object.keys(answers).length > 0) {
				str += '<div id="allAnswersContainer" class="col-xs-12 no-padding">';
			}
			$.each(answers, function (id, ans) {
				//mylog.log("formProfil.views.answers each", id, ans);
				ct++;
				lbl = fObj.labels.answers + " " + ct;
				var address = "<i class='fa fa-map-marker'></i> ";
				var nameProp = "inconnu";
				var descrTravaux = "Aucune";
				if (typeof ans.mappingValues != "undefined" && ans.mappingValues != null) {
					if (typeof ans.mappingValues.name != "undefined") {
						lbl = ans.mappingValues.name;
					}
					address += (typeof ans.mappingValues.address != "undefined" && ans.mappingValues.address != null) ? ans.mappingValues.address[0]["name"] : " adresse non renseignée";
					if (typeof ans.mappingValues.description != "undefined" && ans.mappingValues.description != null) {
						descrTravaux = ans.mappingValues.description;
					}
					if (typeof ans.mappingValues.creatorName != "undefined" && ans.mappingValues.creatorName != null) {
						nameProp = ans.mappingValues.creatorName;
					}
				}

				var lblp = (typeof ans.answers == "undefined" || ans.answers == null) ? "no answer" : ans.percent + "%";
				var percol = "danger";
				var statecol = "danger";
				var lblstate = "Pas d'opérateur";
				var step = "ouvert";
				var icon = "folder-open-o";
				var imgAnsw = ((typeof ans.profilMediumImageUrl != "undefined") ? baseUrl + ans.profilMediumImageUrl : modules.co2.url + "/images/thumbnail-default.jpg");
				var liBg = ((ans.todo > 0) ? "style='background-color:lightGreen'" : "");
				if (ans.percent > 50) {
					percol = "warning";
				}
				if (ans.percent > 75) {
					percol = "success";
				}
				str += '<div class="answerLi col-xs-12 no-padding margin-bottom-10" ' + liBg + ' >' +
					'<div class="col-xs-4">' +
					'<img src="' + imgAnsw + '" class="img-responsive margin-auto">' +
					'</div>' +
					'<div class="col-xs-8 container-infos">' +
					'<div class="col-xs-12 no-padding">' +
					'<h3 class="margin-top-5 titleAnsw"> ' + lbl + '</h3>' +
					'</div>' +
					'<span class="info-answ bold text-dark">' + address + '</span><br/>' +
					'<span class="info-answ bold">' +
					'<i class="fa fa-calendar"></i> ' +
					moment(ans.created).local().locale("fr").format("DD/MM/YYYY HH:mm") +
					'</span>';
				if (typeof ans.updated != "undefined") {
					str += ' <span class="info-answ bold">' +
						'<i class="fa  fa-edit"></i> ' +
						moment(ans.updated).local().locale("fr").format("DD/MM/YYYY HH:mm") +
						'</span>';
				}

				str += ' <span class="info-answ col-xs-12 no-padding">' +
					'<b>Déposé par :</b> ' + nameProp + '</span>' +

					'<span class="info-answ col-xs-12 no-padding">' +
					'<b>Description :</b> ' + descrTravaux + '</span>' +

					'<span class="info-answ">' +
					'<i class="fa fa-' + icon + '"></i> ' + step + '</span>' +

					'<br/>' +

					'<span class="label label-' + percol + '">' +
					'<i class="fa fa-pencil-square-o"></i> ' + lblp + ' </span>' +

					'<span class="label label-' + statecol + ' margin-left-5">' +
					'<i class="fa fa-black-tie"></i> ' + lblstate + ' </span>' +

					'<br/>';
				if (ans.percent != 0 && ans.uids.length > 0) {
					if (ans.tasksPerson.length > 0) {
						str += '<a href="javascript:;" data-id="' + id + '" class="answerTasksBtn btn btn-xs btn-default">' +
							'<i class="fa fa-cogs "></i>Tasks ' +
							'<span class="margin-5 label label-primary">' +
							'<i class="fa fa-square-o"></i> ' + todo +
							'</span> <span class="margin-5 label label-success">' +
							'<i class="fa fa-check-square-o"></i> ' + done + ' </span>' +
							'</a>';
					}
				}
				str += '<div class="col-xs-12 no-padding">' +
					'<a href="javascript:;" data-id="' + id + '" data-mode="r" class="btnAnswer lbh margin-top-5 btn btn-open-answer">' +
					'<i class="fa fa-sign-in"></i> Lire' +
					'</a>';
				if (typeof ans.canEdit == "boolean" && ans.canEdit === true) {
					str += '<a href="javascript:;" data-id="' + id + '" data-mode="w" class="btnAnswer lbh margin-top-5 btn btn-open-answer">' +
						'<i class="fa fa-sign-in"></i> Modifer' +
						'</a>';
				}

				str += '</div>' +
					'</div>' +
					'</div>';
			});
			str += '</div>';
			$(fObj.container).html(str);
			fObj.events.answers(fObj);
		}
	},
	events : {
		add : function (fObj) {
			$(fObj.container + ' .createForm').off().click(function () {
				if (canEdit) {
					dyFObj.openForm(fObj.dynForm.form(fObj))
				}
			});

			$(fObj.container + ' .addOpenForm').off().click(function () {
				dyFObj.openForm(fObj.dynForm.openForm(fObj))
			});

			$(fObj.container + ' .generateAapConfig').off().click(function () {
				fObj.dynForm.aapConfig(fObj); // generate inputs doccument and create aapConfig in forms collection
			});
			$(fObj.container + ' .chooseAapForm').off().click(function () {
				fObj.aap.generateFormParent(fObj); // choose aapCOnfig id and set it inside generated formParent
			});
			$(fObj.container + ' .choosetemplateForm').off().click(function () {
				fObj.aap.generateFormParentTemplate(fObj); // choose template id and set it inside generated formParent
			});
			$(fObj.container + ' .copyForm').off().click(function () {
				fObj.aap.generatecopiedFormParent(fObj); // copy form from another element
			});
			$(fObj.container + ' .generateCostum').off().click(function () {
				fObj.aap.generateCostum(); // generer costum
			});

			$(fObj.container + ' .editOrNewAnswer').off().click(function () {
				ajaxPost(
					null,
					baseUrl + '/survey/answer/get/form/' + $(this).data('id') + '/userId/' + userId + '/',
					null,
					function (data) {
						//alert(JSON.stringify(data));
						//href="'+urlCostum+'#answer.index.id.new.form.'+id+'"
					},
					"json"
				);
			});
			$(fObj.container + ' .panel-form .widget-body .text-desc').moreAndLess({
				showChar : 200,
				ellipsestext : "...",
				moretext : trad.seemore,
				lesstext : trad.seeless,
				class : "letter-green"
			})
		},
		share : function (fObj) {
			$(fObj.container + ' .shareForm').off().on().click(function () {
				var btn = $(this);
				lazyLoad(baseUrl + '/plugins/clipboard/clipboard.min.js', '',
					function () {
						bootbox.dialog({
							message : '<div class="alert-white">' +
								'<br><strong>' + trad.choosehowshareform + '</strong>' +
								'<br><br>' +
								'<h5>URL : </h5><code>' + baseUrl + '/' + btn.data("urlcostum") + '#answer.index.id.new.form.' + btn.data("id") + '</code>' +
								'<h5>HTML : </h5><textarea class="form-control" rows="4">' +
								'<h5>' + fObj.forms[btn.data("id")].name + '</h5>' +
								'<p>' + fObj.forms[btn.data("id")].what + '</p>' +
								'<a href="' + baseUrl + '/' + btn.data("urlcostum") + '#answer.index.id.new.form.' + btn.data("id") + '" target="_blanc">' + trad.reply + '</a>' +
								'</textarea>' +
								'</div>'
						});
					});
			});
			lazyLoad(baseUrl + '/plugins/clipboard/clipboard.min.js', '',
				function () {
					$('.copyFormLink').on().click(function () {
						var clipboard = new ClipboardJS('.copyFormLink');
						clipboard.on('success', function (e) {
							// mylog.info('Action:', e.action);
							// mylog.info('Text:', e.text);
							// mylog.info('Trigger:', e.trigger);
							e.clearSelection();
							toastr.success(trad.copy);
						});

						clipboard.on('error', function (e) {
							// mylog.error('Action:', e.action);
							// mylog.error('Trigger:', e.trigger);
						});
					});
				});
			$('.openFormLink').on().click(function () {
				var URL = $(this).data('clipboard-text');
				window.open(URL, "_blank");
			});
		},
		answers : function (fObj) {
			$(fObj.container + ' .btnAnswer').off().on("click", function () {
				if (typeof $(this).data("type") != "undefined" && $(this).data("type") == "openform") {
					fObj.urls.answer(fObj, $(this).data("id"), null, $(this).data("mode"), contextId, contextType);
				} else {
					fObj.urls.answer(fObj, $(this).data("id"), null, $(this).data("mode"));
				}
			});

			$(fObj.container + ' .btnForm').off().on("click", function () {
				fObj.urls.form(fObj, $(this).data("id"), null, $(this).data("mode"));
			});

		},
		form : function (fObj) {

			$(fObj.container + ' .application').off().on("click", function () {
				if (typeof $(this).data("type") != "undefined" && $(this).data("type") == "openform") {
					fObj.urls.answer(fObj, "new", $(this).data("id"), null, contextId, contextType);
				} else {
					fObj.urls.answer(fObj, "new", $(this).data("id"));
				}
			});

			$(fObj.container + ' .allAnswers').off().on("click", function () {
				mylog.log("formObj.events.form .allAnswers", $(this).data("id"));
				fObj.urls.answers(fObj, $(this).data("id"));
			});

			$(fObj.container + ' .myAnswers').off().on("click", function () {
				fObj.urls.myAnswers(fObj, $(this).data("id"));
			});

			$(fObj.container + ' .observatory').off().on("click", function () {
				if (typeof $(this).data("type") != "undefined" && $(this).data("type") == "openform") {
					fObj.urls.observatory(fObj, $(this).data("id"), null, contextId, contextType);
				} else {
					fObj.urls.observatory(fObj, $(this).data("id"));
				}
			});


			$(fObj.container + ' .config').off().on("click", function () {
				var idForm = $(this).data("id");

				if (typeof $(this).data("type") != "undefined" && $(this).data("type") == "openform") {
					fObj.urls.form(fObj, idForm, null, contextId, contextType);
				} else {
					fObj.urls.form(fObj, idForm);
				}
			});

			$('.btnConfigForm').off().on("click", function () {
				var form = $(this).data("form");
				var subselect = $(this).data("subselect");
				var titleselect = $(this).data("titleselect");
				fObj.dynForm.addDescription(fObj, form, true, subselect, titleselect);
			});

			$(fObj.container + ' .addAnswer').on('click', function () {
				coInterface.showLoader(fObj.container);
				if (typeof $(this).data("contextid") !== "undefined" && typeof $(this).data("contexttype") !== "undefined") {
					ajaxPost("#mainDash", baseUrl + '/survey/answer/index/id/new/form/' + $(this).data("parentformid") + '/contextId/' + $(this).data("contextid") + '/contextType/' + $(this).data("contexttype"),
						null,
						function () {
							if (typeof hashUrlPage != "undefined") {
								// history.replaceState(location.hash, "", hashUrlPage+".view.forms.dir.graphbuilder."+$(this).data("parentformid"));
							}
						}, "html");
				} else {
					ajaxPost(fObj.container, baseUrl + '/survey/answer/index/id/new/form/' + $(this).data("parentformid"),
						null,
						function () {
							if (typeof hashUrlPage != "undefined") {
								// history.replaceState(location.hash, "", hashUrlPage+".view.forms.dir.graphbuilder."+$(this).data("parentformid"));
							}
						}, "html");
				}
			});
			// $(fObj.container+' .editFormBtn').off().click( function(){
			// 	dyFObj.openForm( fObj.dynForm.form(fObj) , null, fObj.forms[$(this).data("id")] )
			// });

			$(fObj.container + ' .deleteFormBtn').off().click(function () {
				var idF = $(this).data("id");
				bootbox.dialog({
					title : trad.confirmdelete,
					message : "<span class='text-red bold'><i class='fa fa-warning'></i> " + trad.actionirreversible + "</span>",
					buttons : [
						{
							label : "Ok",
							className : "btn btn-primary pull-left",
							callback : function () {
								getAjax("", baseUrl + "/survey/form/delete/id/" + idF, function (res) {
									if (res.result) {
										toastr.success(res.msg);
										urlCtrl.loadByHash(location.hash);
									} else {
										toastr.error(res.msg);
									}
								}, "html");
							}
						},
						{
							label : trad.cancel,
							className : "btn btn-default pull-left",
							callback : function () {}
						}
					]
				});
			});

			$(fObj.container + ' .templatizeFormBtn').off().click(function () {
				var idF = $(this).data("id");
				bootbox.dialog({
					title : trad.transformtotemplate,
					message : "<span class='text-red bold'><i class='fa fa-warning'></i>" + trad.doyouwanttransformtotemplate + "</span>",
					buttons : [
						{
							label : "Transformer",
							className : "btn btn-primary pull-left",
							callback : function () {
								getAjax("", baseUrl + "/survey/form/templatizeform/id/" + idF, function (res) {
									if (res.result) {
										toastr.success(res.msg);

									} else {
										toastr.error(res.msg);
									}
								}, "html");
							}
						},
						{
							label : trad.cancel,
							className : "btn btn-default pull-left",
							callback : function () {}
						}
					]
				});
			});
			$('[data-toggle="tooltip"]').tooltip();

		},

		graphbuilder : function (fObj) {
			$('#logoCoform').attr('src', $('#logoCoform').data("imgurl") + "coObservatoire.png");
		},

		dashboard : function (fObj) {
			$('#logoCoform').attr('src', $('#logoCoform').data("imgurl") + "coObservatoire.png");

			$(".addAnswer").on('click', function () {
				coInterface.showLoader("#mainDash");
				if (typeof $(this).data("contextid") !== "undefined" && typeof $(this).data("contexttype") !== "undefined") {
					ajaxPost("#mainDash", baseUrl + '/survey/answer/index/id/new/form/' + $(this).data("parentformid") + '/contextId/' + $(this).data("contextid") + '/contextType/' + $(this).data("contexttype"),
						null,
						function () {
							if (typeof hashUrlPage != "undefined" && typeof answerId != "undefined") {
								// history.replaceState(location.hash, "", hashUrlPage+".view.forms.dir.graphbuilder."+$(this).data("parentformid"));
								history.remplaceState(location.hash, "", hashUrlPage + ".view.forms.dir.form." + answerId);
							}
						}, "html");
				} else {
					ajaxPost("#mainDash", baseUrl + '/survey/answer/index/id/new/form/' + $(this).data("parentformid"),
						null,
						function () {
							if (typeof hashUrlPage != "undefined") {
								// history.replaceState(location.hash, "", hashUrlPage+".view.forms.dir.graphbuilder."+$(this).data("parentformid"));
							}
						}, "html");
				}
			});

			$('.getopalgraph').on('click', function () {
				coInterface.showLoader("#mainDash");
				var cid = $(this).data("contextid");
				// var ctype = $(this).data("contexttype");
				ajaxPost("#mainDash", baseUrl + '/survey/answer/ocecoorgadashboard/contextid/' + cid,
					null,
					function () {
						if (typeof hashUrlPage != "undefined") {
							history.replaceState(location.hash, "", hashUrlPage + ".view.forms.dir.ocecoorgadashboard." + cid);
						}
					}, "html");
			});

			$('.survey-container').on('click', '.deleteanswer', function () {
				var itemId = $(this).data("ansid");

				id = $(this).data("ansid");
				bootbox.dialog({
					title : trad.confirmdelete,
					message : "<span class='text-red bold'><i class='fa fa-warning'></i> " + trad.actionirreversible + "</span>",
					buttons : [
						{
							label : "Ok",
							className : "btn btn-primary pull-left",
							callback : function () {
								getAjax("", baseUrl + "/survey/co/delete/id/" + itemId, function () {
									//urlCtrl.loadByHash(location.hash);
									$('.compact-parent[data-id="' + itemId + '"]').remove();
									var currentCount = $('.folderCount').text() * 1;
									mylog.log("currentCount: " + currentCount)
									currentCount > 0 ? $('.folderCount').html(currentCount - 1) : ''
								}, "html");
							}
						},
						{
							label : trad.cancel,
							className : "btn btn-default pull-left",
							callback : function () {}
						}
					]
				});
			});


			// $(fObj.container+' .application').off().on("click",function(){
			// 	if (typeof $(this).data("type") != "undefined" && $(this).data("type") == "openform") {
			// 		fObj.urls.answer(fObj, "new", $(this).data("id"), null, contextId, contextType);
			// 	} else {
			// 		fObj.urls.answer(fObj, "new", $(this).data("id"));
			// 	}
			// });
		},
		obs : function (fObj) {
			$(".getgraphbuilder").on('click', function () {
				coInterface.showLoader("#mainDash");
				ajaxPost(fObj.container, baseUrl + '/survey/answer/graphbuilder/answer/' + $(this).data("parentformid"),
					null,
					function () {
						if (typeof hashUrlPage != "undefined") {
							history.replaceState(location.hash, "", hashUrlPage + ".view.forms.dir.graphbuilder." + $(this).data("parentformid"));
						}
					}, "html");
			});

			$(".getobservatory").on('click', function () {
				coInterface.showLoader("#mainDash");
				var parentFormId = $(this).data("parentformid");
				var week = $(this).data("week");

				ajaxPost(fObj.container, baseUrl + '/survey/answer/observatory/form/' + parentFormId + "/week/" + week,
					null,
					function () {
						if (typeof hashUrlPage != "undefined") {
							history.replaceState(location.hash, "", hashUrlPage + ".view.forms.dir.obs." + parentFormId);
						}
					}, "html");
			});

			$(".getdashboard").on('click', function () {
				coInterface.showLoader("#mainDash");
				var prntfrmid = $(this).data("parentformid");

				if (typeof $(this).data("contextid") != "undefined") {
					var cntxtid = $(this).data("contextid");
					var cntxttp = $(this).data("contexttype");

					ajaxPost("#mainDash", baseUrl + '/survey/answer/dashboard/answer/' + $(this).data("parentformid") + '/contextId/' + $(this).data("contextid") + '/contextType/' + $(this).data("contexttype"),
						null,
						function () {
							if (typeof hashUrlPage != "undefined") {
								history.replaceState(location.hash, "", hashUrlPage + ".view.forms.dir.observatory." + prntfrmid + ".contextId." + cntxtid + ".contextType." + cntxttp
								);
							}
						}, "html");
				} else {
					ajaxPost("#mainDash", baseUrl + '/survey/answer/dashboard/answer/' + $(this).data("parentformid"),
						null,
						function () {
							if (typeof hashUrlPage != "undefined") {
								history.replaceState(location.hash, "", hashUrlPage + ".view.forms.dir.observatory." + prntfrmid);
							}
						}, "html");
				}

				$('.dropdown.menu li').each(function () {
					$(this).removeClass("selected");
				})

				$('.dropdown.menu li')
			});

			$(".getformconfig").on('click', function () {
				coInterface.showLoader("#mainDash");
				var prntfrmid = $(this).data("parentformid");

				if (typeof $(this).data("contextid") != "undefined") {
					var cntxtid = $(this).data("contextid");
					var cntxttp = $(this).data("contexttype");

					ajaxPost("#mainDash", baseUrl + '/survey/form/edit/id/' + $(this).data("parentformid") + '/contextId/' + cntxtid + '/contextType/' + cntxttp,
						null,
						function () {
							if (typeof hashUrlPage != "undefined") {
								history.replaceState(location.hash, "", hashUrlPage + ".view.forms.dir.form." + prntfrmid + ".contextId." + cntxtid + ".contextType." + cntxttp);
							}
						}, "html");

				} else {
					ajaxPost("#mainDash", baseUrl + '/survey/form/edit/id/' + $(this).data("parentformid"),
						null,
						function () {
							if (typeof hashUrlPage != "undefined") {
								history.replaceState(location.hash, "", hashUrlPage + ".view.forms.dir.form." + prntfrmid);
							}
						}, "html");
				}
			});
			$('.globalEvaluate').off().on('click', function () {
				coInterface.showLoader("#mainDash");
				var parentformid = $(this).data("parentformid");
				fObj.urls.globalEvaluation(fObj, parentformid);
			});

			$("#coform-list").on('click', '.getanswer', function () {
				coInterface.showLoader("#mainDash");
				var answid = $(this).data("ansid");
				var getdatatype = "";
				if (typeof $(this).data("type") != "undefined" && $(this).data("type") == "openform") {
					ajaxPost("#mainDash", baseUrl + '/survey/answer/index/id/' + $(this).data("ansid") + '/mode/' + $(this).data("mode") + '/contextId/' + contextId + '/contextType/' + contextType,
						null,
						function () {
							if (typeof hashUrlPage != "undefined") {
								history.replaceState(location.hash, "", hashUrlPage + ".view.forms.dir.answer." + answid + ".contextId." + contextId + ".contextType." + contextType);
							}
						}, "html");
				} else {
					ajaxPost("#mainDash", baseUrl + '/survey/answer/index/id/' + $(this).data("ansid") + '/mode/' + $(this).data("mode") + '/contextId/' + contextId + '/contextType/' + contextType,
						null,
						function () {
							if (typeof hashUrlPage != "undefined") {
								history.replaceState(location.hash, "", hashUrlPage + ".view.forms.dir.answer." + answid);
							}
						}, "html");
				}
			});

			$('.getansweropal').on('click', function () {
				coInterface.showLoader("#mainDash");
				var ansid = $(this).data("ansid");
				ajaxPost("#mainDash", baseUrl + '/survey/answer/ocecoformdashboard/answerid/' + ansid,
					null,
					function () {
						if (typeof hashUrlPage != "undefined") {
							history.replaceState(location.hash, "", hashUrlPage + ".view.forms.dir.ocecoformdashboard." + $(this).data("ansid"));
						}
					}, "html");
			});
		},
		addproposition : function (fObj) {
			ajaxPost('', baseUrl + '/survey/form/get/slug/' + contextData.slug + '/tpl/json',
				null,
				function (data) {
					var str = "";
					mylog.log("pageProfil.views.forms ajaxPost data", data);
					if (typeof data != "undefined" && data != null &&
						data.forms != "undefined" && data.forms != null) {
						fObj.el = data.el;
						fObj.openFormList = fObj.openForm.list();
						fObj.forms = data.forms;
						smallMenu.openAjaxHTML(baseUrl + '/survey/answer/addproposition/contextId/' + contextId + '/contextType/' + contextType);
					}
				}, "html");

		}
	},
	manage : {},
	actions : {},
	dynForm : {
		form : function (fObj) {
			return {
				jsonSchema : {
					title : trad.createquestionnaire,
					description : trad.everythingpossiblequestions,
					icon : "fa-question",
					properties : {
						name : {label : trad.formname},
						what : {
							label : trad.nameananswer,
							placeholder : trad.proposal + ", " + trad.folders + ", " + trad.Projects + " ..."
						},
						image : dyFInputs.image(),
						formid : {
							inputType : "hidden",
							value : fObj.el.slug + Object.keys(fObj.forms).length
						},
						parent : {
							inputType : "finder",
							label : tradDynForm.whoiscarrytheproject,
							multiple : true,
							rules : {required : true, lengthMin : [1, "parent"]},
							initType : ["organizations", "projects"],
							openSearch : true
						},
						image : dyFInputs.image(),
						temporarymembercanreply : {
							inputType : "checkboxSimple",
							label : trad.availableoffline,
							subLabel : trad.availableoffline,
							params : {
								onText : trad.yes, offText : trad.no, onLabel : trad.yes, offLabel : trad.no,
								labelText : ""
							},
							checked : true
						}
						// tpl : {
						// 	inputType : "hidden",
						// 	value:"survey.views.tpls.forms.formWizard"
						// },
						// answersTpl : {
						// 	inputType : "hidden",
						// 	value:"survey.views.tpls.forms.answers"
						//}
					},
					beforeBuild : function () {
						dyFObj.setMongoId('forms', function () {
							uploadObj.gotoUrl = '#page.type.forms.id.' + uploadObj.id;
						});
					},
					afterSave : function () {
						dyFObj.commonAfterSave();
					},
					save : function (formData) {
						mylog.log('save tplCtx formData', formData)

						delete formData.collection;

						tplCtx = {
							collection : "forms",
							id : formData.id,
							value : {
								id : formData.formid,
								name : formData.name,
								parent : formData.parent,
								oneAnswerPerPers : true
							},
							setType : [
								{
									path : "oneAnswerPerPers",
									type : "boolean"
								}
							]
						};

						// tplCtx.value.id = tplCtx.value.formid;

						// delete tplCtx.value.formid;
						mylog.log("save tplCtx", tplCtx);

						if (typeof tplCtx.value == "undefined") {
							toastr.error('value cannot be empty!');
						} else {
							dataHelper.path2Value(tplCtx, function (params) {
								urlCtrl.loadByHash(location.hash);

							});
							dyFObj.closeForm();
						}

					}
				}
			}
		},

		openForm : function (fObj) {
			return {
				jsonSchema : {
					title : trad.createquestionnaire,
					description : trad.everythingpossiblequestions,
					icon : "fa-question",
					properties : {
						openform : {
							inputType : "select",
							label : "openForm à appliquer",
							options : fObj.openFormList
						}
					},
					beforeBuild : function () {
						dyFObj.setMongoId('forms', function () {
							uploadObj.gotoUrl = '#page.type.forms.id.' + uploadObj.id;
						});
					},
					save : function (formData) {
						mylog.log('save tplCtx formData', formData)

						delete formData.collection;

						tplCtx = {
							collection : "forms",
							id : formData.openform,
							value : {
								type : contextData.type,
								name : contextData.name
							},
							path : "formCommunity." + contextData.id
						};

						// tplCtx.value.id = tplCtx.value.formid;

						// delete tplCtx.value.formid;
						mylog.log("save tplCtx", tplCtx);
						if (typeof tplCtx.value == "undefined") {
							toastr.error('value cannot be empty!');
						} else {
							dataHelper.path2Value(tplCtx, function (params) {
								ajaxPost("", baseUrl + '/survey/form/generateinputs/form/' + formData.openform + '/contextId/' + contextId + '/contextType/' + contextType,
									null,
									function () {
										urlCtrl.loadByHash(location.hash);
									}, "html");

							});
						}

					}
				}
			}
		},
		aapConfig : function (fObj) {
			var currentAapConfig = fObj.aap.currentAapConfig(fObj);
			var checkboxSimpleParams = {"onText" : trad.yes, "offText" : trad.no, "onLabel" : trad.yes, "offLabel" : trad.no, "labelText" : "label"}
			jsonHelper.setValueByPath(
				currentAapConfig,
				"subForms-aapStep3-params-config-criterions",
				jsonHelper.getValueByPath(currentAapConfig, "subForms.aapStep3.params.config.criterions")
			);

			var aapConfigForm = {
				jsonSchema : {
					title : trad.configuretempatecall,
					description : "",
					icon : "fa-question",
					properties : {
						parent : {
							inputType : "finder",
							label : tradDynForm.whoiscarrytheproject,
							multiple : true,
							rules : {required : true, lengthMin : [1, "parent"]},
							initType : ["organizations", "projects"],
							openSearch : true
						},
						name : {
							inputType : "text",
							label : trad.nametemplate,
							value : (notNull(currentAapConfig) && exists(currentAapConfig.name) ? currentAapConfig.name : "")
						},
						description : {
							inputType : "textarea",
							label : trad.templatedescription,
							value : (notNull(currentAapConfig) && exists(currentAapConfig.description) ? currentAapConfig.description : "")
						},
						type : {
							inputType : "hidden",
							value : "aapConfig"
						},
						projectGeneration : {
							inputType : "select",
							label : trad.projectgenerate,
							options : {
								onStart : trad.atcreation,
								onRunning : trad.processing,
								onSubmit : trad.onsubmission
							}
						},
						active : {
							label : trad.activate,
							inputType : "checkboxSimple",
							params : checkboxSimpleParams,
							checked : (notNull(currentAapConfig) && exists(currentAapConfig.active) ? currentAapConfig.active : true)
						},
						private : {
							label : trad.private,
							inputType : "checkboxSimple",
							params : checkboxSimpleParams,
							checked : (notNull(currentAapConfig) && exists(currentAapConfig.private) ? currentAapConfig.private : true)
						},
						showMap : {
							label : trad.showmaponproject,
							inputType : "checkboxSimple",
							params : checkboxSimpleParams,
							checked : (notNull(currentAapConfig) && exists(currentAapConfig.showMap) ? currentAapConfig.showMap : true)
						}
					},
					onLoads : {
						onload : function (data) {
							$(".parentfinder,.subtepForms-aapStep4-params-config-linktext").hide();
							alignInput2(aapConfigForm.jsonSchema.properties, "subForms-aapStep1", 4, 6, null, null, trad.step + "  1 (" + trad.Proposal + " )", "#3f4e58", "");
							alignInput2(aapConfigForm.jsonSchema.properties, "subForms-aapStep2", 4, 6, null, null, trad.step + "  2 (" + trad.Characterization + " )", "#3f4e58", "");
							alignInput2(aapConfigForm.jsonSchema.properties, "subForms-aapStep3", 4, 6, null, null, trad.step + "  3 (EVALUATION)", "#3f4e58", "", {
								borderSize : "5px",
								borderType : "solid",
								borderColor : "green"
							});
							alignInput2(aapConfigForm.jsonSchema.properties, "subForms-aapStep4", 4, 6, null, null, trad.step + "  4 (" + trad.Financing + " )", "#3f4e58", "");
							alignInput2(aapConfigForm.jsonSchema.properties, "subForms-aapStep5", 4, 6, null, null, trad.step + "  5 (" + trad.Followup + " )", "#3f4e58", "");
							$('.subForms-aapStep3-params-config-criterionslists,.subForms-aapStep3-params-config-typeselect,.subForms-aapStep3-params-config-typeselect,.subForms-aapStep3-params-config-linktext').removeClass('col-md-4 col-xs-6').addClass('col-md-10 col-md-offset-1 col-xs-12')

							if (jsonHelper.getValueByPath(currentAapConfig, "subForms.aapStep3.params.config.type") == "noteCriterionBased" || jsonHelper.getValueByPath(currentAapConfig, "subForms.aapStep3.params.config.type") == "starCriterionBased") {
								$('.subForms-aapStep3-params-config-criterionslists').show();
							}
							$('#subForms-aapStep3-params-config-type').off().on('change', function () {
								if ($(this).val() == "noteCriterionBased" || $(this).val() == "starCriterionBased") {
									$('.subForms-aapStep3-params-config-criterionslists').fadeIn();
									//$("[data-entry=min]").val(0); $("[data-entry=max]").val(5);
								} else {
									$('.subForms-aapStep3-params-config-criterionslists').fadeOut();
								}
							})

							if (jsonHelper.getValueByPath(currentAapConfig, "subForms.aapStep4.params.config.type") != "ocecoform") {
								$('.subForms-aapStep4-params-config-link').show();
							}
							$('#subForms-aapStep4-params-config-type').off().on('change', function () {
								if ($(this).val() == "ocecoform") {
									$('.subForms-aapStep4-params-config-linktext').fadeOut();
								} else {
									$('.subForms-aapStep4-params-config-linktext').fadeIn();
								}
							})

							//alert(JSON.stringify(currentAapConfig));

							if (currentAapConfig) {
								$('.subForms-aapStep1-activecheckboxSimple').hide();
								$('.subForms-aapStep2-activecheckboxSimple').hide();
								$('.subForms-aapStep3-activecheckboxSimple').hide();
								$('.subForms-aapStep4-activecheckboxSimple').hide();
							}

						},
						sub : function (data) {

						}
					},
					save : function (formData) {
						mylog.log('save tplCtx formData', formData)
						tplCtx = {
							collection : "forms",
							value : {},
							format : true,
							path : "allToRoot"
						};

						if (currentAapConfig != null && currentAapConfig.exist == true) // check if aapAlready exists ans make modification only
						{
							tplCtx.id = currentAapConfig._id.$id
						}
						$.each(aapConfigForm.jsonSchema.properties, function (k, v) {
							if (k == "subForms-aapStep3-params-config-criterions") {
								tplCtx.value[k] = formData["subForms-aapStep3-params-config-criterions"];
							} else if (k.split("-").length > 1 && k.split("-")[0] != "subForms") {
								var spltid = k.split("-");

								var spltidobj = spltid.reverse().reduce(function (total, current, index) {
									var sp = {};
									if (index == 0) {
										sp[current] = $("#" + k).val();
									} else {
										sp[current] = total;
									}
									return sp;
								}, {});

								$.each(spltidobj, function (indx, val) {
									if (indx == "params") {
										$.each(val, function (indx2, val2) {
											tplCtx.value[indx][indx2] = val2;
										});
									} else {
										tplCtx.value[indx] = val;
									}
								});

							} else {
								if (v.inputType == 'tags') {
									tplCtx.value[k] = $("#" + k).val().split(',');
								} else {
									tplCtx.value[k] = $("#" + k).val();
								}
							}
						});

						tplCtx.value["mapping"] = {
							"proposition" : "answers.aapStep1.titre",
							"description" : "answers.aapStep1.opalProcess41",
							"depense" : "answers.aapStep1.depense",
							"murir" : "aapStep4"
						};
						mylog.log("goo", formData);
						tplCtx.value["parent"] = formData.parent;
						mylog.log("save tplCtx", tplCtx);

						if (typeof tplCtx.value == "undefined") {
							toastr.error('value cannot be empty!');
						} else {
							dataHelper.path2Value(tplCtx, function (params) {
								if (currentAapConfig != null && currentAapConfig.exist == true) {
									urlCtrl.loadByHash(location.hash)
								} else {
									var insertedAaapConfig = params.saved;
									var id = insertedAaapConfig._id.$id;
									delete insertedAaapConfig._id;
									var generatedInputs = fObj.aap.generateInputsDocument(fObj, {
										number : 5,
										proposition : 1,
										//caracterisation: 2,
										evaluation : 3,
										financement : 4,
										suivie : 5
									}); //generate inputs document
									$.each(generatedInputs, (k, v) => insertedAaapConfig.subForms["aapStep" + k].inputs = v._id.$id); // assing
								                                                                                                      // generated
								                                                                                                      // inputs to
								                                                                                                      // subforms
								                                                                                                      // input
									dataHelper.path2Value({ // adn update aapConfig in forms collection
										id : id,
										collection : "forms",
										path : "allToRoot",
										format : true,
										value : insertedAaapConfig
									}, () => {
										fObj.aap.generateCostum();
										/*bootbox.confirm({
										 message: "<h5 class='text-center text-success'>Voulez-vous créer le costum ?</h5>",
										 buttons: {
										 confirm: {label: 'OUI',className: 'btn-success'},
										 cancel: {label: 'NON',className: 'btn-primary'}
										 },
										 callback: function (result) {
										 if (result)
										 fObj.aap.generateCostum();
										 else
										 urlCtrl.loadByHash(location.hash);
										 }
										 })*/
									});
								}

							});
						}

					}
				}
			};

			if (notNull(currentAapConfig)) {

			}

			$.each(["Proposition", "Evaluation", "Financement", "Suivie"], function (k, v) { //common properties
				var i = k + 1;
				if (currentAapConfig) {} else {
					aapConfigForm.jsonSchema.properties['subForms-aapStep' + i + '-active'] = {
						label : trad.activatestep + " " + i,
						inputType : "checkboxSimple",
						params : checkboxSimpleParams,
						checked : jsonHelper.getValueByPath(currentAapConfig, "subForms.aapStep" + i + ".active")
					};
				}

				aapConfigForm.jsonSchema.properties["subForms-aapStep" + i + "-inputs"] = {  //get inputs 1 path from inputs collection
					inputType : "hidden",
					value : jsonHelper.getValueByPath(currentAapConfig, "subForms.aapStep" + i + ".inputs")
				};
				aapConfigForm.jsonSchema.properties["subForms-aapStep" + i + "-name"] = {
					inputType : "text",
					label : trad.stagename + " " + i,
					value : (notNull(currentAapConfig) && exists(currentAapConfig.subForms) &&
					(exists(currentAapConfig.subForms['aapStep' + i]) &&
						exists(currentAapConfig.subForms['aapStep' + i].name)) ? currentAapConfig.subForms['aapStep' + i].name : v)
				};
				aapConfigForm.jsonSchema.properties["subForms-aapStep" + i + "-params-haveEditingRules"] = {
					inputType : "checkboxSimple",
					label : trad.writingmode,
					params : checkboxSimpleParams,
					checked : jsonHelper.getValueByPath(currentAapConfig, "subForms.aapStep" + i + ".params.haveEditingRules")
				};
				aapConfigForm.jsonSchema.properties["subForms-aapStep" + i + "-params-haveReadingRules"] = {
					inputType : "checkboxSimple",
					label : trad.readingmode,
					params : checkboxSimpleParams,
					checked : jsonHelper.getValueByPath(currentAapConfig, "subForms.aapStep" + i + ".params.haveReadingRules")
				};
				aapConfigForm.jsonSchema.properties["subForms-aapStep" + i + "-params-canEdit"] = {
					inputType : "tags",
					minimumInputLength : 0,
					label : trad.whocanedit,
					values : ["Evaluateur", "Financeur"],
					value : typeof jsonHelper.getValueByPath(currentAapConfig, "subForms.aapStep" + i + ".params.canEdit") != "undefined" ?
						jsonHelper.getValueByPath(currentAapConfig, "subForms.aapStep" + i + ".params.canEdit").join() : ""
				};
				aapConfigForm.jsonSchema.properties["subForms-aapStep" + i + "-params-canRead"] = {
					inputType : "tags",
					minimumInputLength : 0,
					label : trad.whocanread,
					values : ["Evaluateur", "Financeur"],
					value : typeof jsonHelper.getValueByPath(currentAapConfig, "subForms.aapStep" + i + ".params.canRead") != "undefined" ?
						jsonHelper.getValueByPath(currentAapConfig, "subForms.aapStep" + i + ".params.canRead").join() : ""
				};
			});

			aapConfigForm.jsonSchema.properties['subForms-aapStep3-params-config-type'] = {
				inputType : "select",
				label : trad.evaluationtype,
				options : {
					"noteCriterionBased" : trad.evaluationbynote,
					"starCriterionBased" : trad.evaluationbystar,
					"forOrAgainst" : trad.fororagainst
				},
				value : typeof jsonHelper.getValueByPath(currentAapConfig, "subForms.aapStep3.params.config.type") != "undefined" ? jsonHelper.getValueByPath(currentAapConfig, "subForms.aapStep3.params.config.type") : "noteCriterionBased"
			};

			aapConfigForm.jsonSchema.properties['subForms-aapStep3-params-config-criterions'] = {
				inputType : "lists",
				label : trad.evaluationcriteria,
				"entries" : {
					"label" : {
						"type" : "text",
						"label" : trad.namecriteria + " {num}",
						"class" : "col-md-5 col-sm-5 col-xs-10"
					},
					"coeff" : {
						"label" : "Coeff",
						"type" : "text",
						"value" : "1",
						"class" : "col-md-2 col-sm-5 col-xs-10"
					},
					"note" : {
						"label" : "",
						"type" : "text",
						"value" : "0",
						"class" : "col-md-2 col-sm-5 col-xs-10 hidden"
					}

				}
			};

			aapConfigForm.jsonSchema.properties['subForms-aapStep4-params-config-type'] = {
				inputType : "select",
				label : trad.typefinancing,
				options : {
					"ocecoform" : trad.financingline,
					"crowdfunding" : trad.participativefinancing
				},
				value : typeof jsonHelper.getValueByPath(currentAapConfig, "subForms.aapStep4.params.config.type") != "undefined" ? jsonHelper.getValueByPath(currentAapConfig, "subForms.aapStep4.params.config.type") : "ocecoform"
			};
			aapConfigForm.jsonSchema.properties['subForms-aapStep4-params-config-link'] = {
				inputType : "text",
				label : trad.participativefinancinglink,
				placeholder : "http://",
				value : jsonHelper.getValueByPath(currentAapConfig, "subForms.aapStep4.params.config.link")
			};

			dyFObj.openForm(aapConfigForm, null, currentAapConfig);
		},
		addDescription : function (fObj, params, update = false, subselect, titleselect) {
			var today = new Date();

			/*debut config rules */
			var checkboxSimpleParams = {"onText" : trad.yes, "offText" : trad.no, "onLabel" : trad.yes, "offLabel" : trad.no, "labelText" : "label"}

			/*

			 /*fin config rules */

			var descriptionForm = {
				jsonSchema : {
					title : trad.configurequestionnaire,
					description : "",
					icon : "fa-question",
					properties : {
						name : {
							label : trad.formname,
							value : params.name,
							rules : {
								required : true
							}
						},
						description : {
							label : trad.questionnairedescription,
							inputType : "textarea",
							markdown : true,
							rules : {
								required : true
							}
						},
						what : {
							label : trad.nameananswer,
							placeholder : trad.proposal + ", " + trad.folders + ", " + trad.Projects + " ..."
						},
						// actionDomains : {
						// 	label : "Domaine d'actions",
						// 	inputType : "array",
						// 	value : []
						// },
						// objectives : {
						// 	label : "Objectifs",
						// 	inputType : "array",
						// 	value : []
						// },
						// image :{
						// 	inputType : "uploader",
						// 	label : "Image",
						// 	docType : "image",
						// 	contentKey : "file",
						// 	itemLimit : 1,
						// 	domElement:"image",
						// 	endPoint :"/subKey/formImg",
						// 	filetypes: ["jpeg", "jpg", "gif", "png"],
						// },
						// document :{
						// 	inputType : "uploader",
						// 	label : "Documents",
						// 	docType : "file",
						// 	contentKey : "file",
						// 	itemLimit : 3,
						// 	domElement:"document",
						// 	endPoint :"/subKey/formDoc",
						// 	filetypes: ["pdf"],
						// },
						active : {
							inputType : "checkboxSimple",
							label : trad.activateopenresponse,
							subLabel : trad.activatetoshareandopenresponsetrad,
							params : {
								onText : trad.yes, offText : trad.no, onLabel : trad.yes, offLabel : trad.no,
								labelText : trad.activate
							},
						},
						private : {
							inputType : "checkboxSimple",
							label : trad.private,
							subLabel : trad.onlycommunitycanedit,
							params : {
								onText : trad.yes, offText : trad.no, onLabel : trad.yes, offLabel : trad.no,
								labelText : trad.private
							},
						},
						canReadOtherAnswers : {
							inputType : "checkboxSimple",
							label : trad.openreadinganswers,
							subLabel : trad.areanswersopentopublic,
							params : {
								onText : trad.yes, offText : trad.no, onLabel : trad.yes, offLabel : trad.no,
								labelText : trad.openanswers
							},
						},
						/*copyable : {
						 inputType : "checkboxSimple",
						 label : "D'autre éléménts peuvent copier cette formulaire",
						 subLabel : "D'autre éléménts peuvent copier cette formulaire",
						 params : { onText : trad.yes,offText : trad.no,onLabel : trad.yes,offLabel : trad.no,
						 labelText : "Copiable"},
						 ,
						 order:11
						 },*/
						startDate : {
							inputType : "datetime",
							label : tradDynForm.startDate,
							value : ((update && params.startDate ) ? (typeof params.startDate.sec === 'number' ? moment.unix(params.startDate.sec).format('DD/MM/YYYY HH:mm') : params.startDate) : today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear()),
							rules : {
								required : true
							}
						},
						endDate : {
							inputType : "datetime",
							label : tradDynForm.endDate,
							value : ((update && params.endDate) ? (typeof params.endDate.sec === 'number' ? moment.unix(params.endDate.sec).format('DD/MM/YYYY HH:mm') : params.endDate) : today.getDate() + '/' + (today.getMonth() + 3) + '/' + today.getFullYear()),
							rules : {
								required : true
							}
						},
						useBannerImg : {
							inputType : "checkboxSimple",
							label : trad.coverPhotos,
							params : {
								onText : trad.yes, offText : trad.no, onLabel : trad.yes, offLabel : trad.no,
								labelText : trad.activate
							},
						},
						useBannerText : {
							inputType : "checkboxSimple",
							label : trad.coverText,
							params : {
								onText : trad.yes, offText : trad.no, onLabel : trad.yes, offLabel : trad.no,
								labelText : trad.activate
							},
						},
						bannerText : {
							inputType : "textarea",
							markdown : true,
							label : "Texte",
						},
						anyOnewithLinkCanAnswer : {
							inputType : "checkboxSimple",
							label : trad.sharebylink,
							subLabel : trad.havelinkbeableanswer,
							params : {
								onText : trad.yes, offText : trad.no, onLabel : trad.yes, offLabel : trad.no,
								labelText : trad.uniqueresponse
							},
						},
						oneAnswerPerPers : {
							inputType : "checkboxSimple",
							label : trad.blocktooneresponse,
							subLabel : trad.apersoncanansweronly,
							params : {
								onText : trad.yes, offText : trad.no, onLabel : trad.yes, offLabel : trad.no,
								labelText : trad.uniqueresponse
							},
						},
						canModify : {
							inputType : "checkboxSimple",
							label : trad.modifiableresponse,
							subLabel : trad.oncesubmittedreponsemodifiable,
							params : {
								onText : trad.yes, offText : trad.no, onLabel : trad.yes, offLabel : trad.no,
								labelText : trad.modifiableresponse
							},
						},
						showAnswers : {
							inputType : "checkboxSimple",
							label : trad.displayresponse,
							subLabel : trad.arepostedpublicly,
							params : {
								onText : trad.yes, offText : trad.no, onLabel : trad.yes, offLabel : trad.no,
								labelText : trad.displayresponse
							},
						},
						/*validateBtn: {
						 inputType : "checkboxSimple",
						 label : "Bouton Valider à la fin du formulaire",
						 subLabel : "Bouton Valider à la fin du formulaire",
						 params : { onText : trad.yes,offText : trad.no,onLabel : trad.yes,offLabel : trad.no,
						 labelText : "Bouton Valider"},
						 },*/

						/*feedbackBtn : {
						 inputType : "checkboxSimple",
						 label : "Bouton Valider à la fin du formulaire",
						 subLabel : "Bouton Valider à la fin du formulaire",
						 params : { onText : trad.yes,offText : trad.no,onLabel : trad.yes,offLabel : trad.no,
						 labelText : "Bouton Valider"},
						 },*/
						temporarymembercanreply : {
							inputType : "checkboxSimple",
							label : trad.availableoffline,
							subLabel : trad.availableoffline,
							params : {
								onText : trad.yes, offText : trad.no, onLabel : trad.yes, offLabel : trad.no,
								labelText : ""
							},
						},
						withconfirmation : {
							inputType : "checkboxSimple",
							label : trad.ifavailableofflinesendmail,
							subLabel : trad.availableoffline,
							params : {
								onText : trad.yes, offText : trad.no, onLabel : trad.yes, offLabel : trad.no,
								labelText : ""
							},

						},
						startDateNoconfirmation : {
							inputType : "datetime",
							label : trad.startdatetoanswerwithoummail,
							value : ((update && params.startDateNoconfirmation) ? (typeof params.startDateNoconfirmation.sec === 'number' ? moment.unix(params.startDateNoconfirmation.sec).format('DD/MM/YYYY HH:mm') : params.startDateNoconfirmation) : today.getDate() + '/' + (today.getMonth() + 3) + '/' + today.getFullYear()),
						},
						endDateNoconfirmation : {
							inputType : "datetime",
							label : trad.enddatetoanswerwithoummail,
							value : ((update && params.endDateNoconfirmation) ? (typeof params.endDateNoconfirmation.sec === 'number' ? moment.unix(params.endDateNoconfirmation.sec).format('DD/MM/YYYY HH:mm') : params.endDateNoconfirmation) : today.getDate() + '/' + (today.getMonth() + 3) + '/' + today.getFullYear()),
						},
						hasStepValidations : {
							inputType : "select",
							label : trad.lockfromstep,
							options : subselect
						},
						/*"params-inputForAnswerName" : {
						 inputType : "select",
						 label : trad.selectanswertodisplay,
						 placeholder : trad.selectanswertodisplay,
						 noOrder : true,
						 options : titleselect,
						 value: ( (typeof params.params != "undefined" && typeof params.params.inputForAnswerName != "undefined" ) ? params.params.inputForAnswerName : "" )
						 },
						 "params-checkboxNewurgency" : {
						 inputType : "hidden"
						 }*/
					},
					onLoads : {
						onload : function () {

							//alignInput2(["actionDomainlists","objectiveslists"], "upload", 6, 6, null, null, "Objectifs et Domaine d'actions",
							// "#3f4e58", "");
							trad.banner = trad.banner.charAt(0).toUpperCase() + trad.banner.slice(1)
							alignInput2(["useBannerImgcheckboxSimple", "useBannerTextcheckboxSimple", "bannerTexttextarea"], "banner", 6, 6, null, null, trad.banner, "#3f4e58", "");
							alignInput2(["hidden-btnObservatorycheckboxSimple"], "hidden", 6, 6, null, null, ucfirst(trad.hide), "#3f4e58", "");
							alignInput2(["imageuploader", "documentuploader"], "upload", 6, 6, null, null, trad.imageandpdffile, "#3f4e58", "");
							alignInput2(["startDatedate", "endDatedate"], "date", 6, 6, null, null, "Dates", "#3f4e58", "");
							alignInput2(["privatecheckboxSimple", "canReadOtherAnswerscheckboxSimple", "anyOnewithLinkCanAnswercheckboxSimple", "oneAnswerPerPerscheckboxSimple", "canModifycheckboxSimple", "showAnswerscheckboxSimple", "validateBtncheckboxSimple", "feedbackBtncheckboxSimple", "copyablecheckboxSimple"], "test", 6, 12, null, null, trad.more, "#3f4e58", "");
							alignInput2(["temporarymembercanreplycheckboxSimple", "withconfirmationcheckboxSimple", "startDateNoconfirmationdate", "endDateNoconfirmationdate"], trad.activation, 6, 6, null, null, trad.ifnotconnected, "#3f4e58", "");
							if (exists(params.subForms) && params.subForms != null) {
								var arr = ["Proposition", "Evaluation", "Financement", "Suivie"];
								$.each(params.subForms, function (k, v) {
									alignInput2(descriptionForm.jsonSchema.properties, "params-aapStep" + (k + 1), 6, 6, null, null, trad.step + "  " + arr[k], "#3f4e58", "");
								});
							}
							alignInput2(["activecheckboxSimple"], null, 6, 6, null, null, trad.activation, "#3f4e58", "");
							//$('.mainDynFormCloseBtn,.close-modal').hide();
							$('#btn-submit-form').addClass("btn-block btn-lg bg-green-k letter-white").removeClass("letter-green").css({
								"margin-left" : "0",
								"margin-right" : "5",
								"order" : "3"
							});
							$('.mainDynFormCloseBtn').addClass('btn-block btn-lg').css({'margin-right' : "5px", 'margin-top' : "0"});
							$('.form-actions').css({"display" : "flex"});
							$('.form-action hr').remove();
							$('.bannerTexttextarea').removeClass("col-xs-6 col-md-6").addClass("col-xs-12 col-md-12");
						}
					},
					beforeBuild : function () {
						mylog.log("paramsko", params);
						uploadObj.set("forms", params._id.$id);

						if (typeof params.type != "undefined" && (params.type == "aap" || params.type == "aapConfig")) {
							descriptionForm.jsonSchema.properties["params-inputForAnswerName"] = {
								inputType : "select",
								label : trad.selectanswertodisplay,
								placeholder : trad.selectanswertodisplay,
								noOrder : true,
								options : titleselect,
								value : ((typeof params.params != "undefined" && typeof params.params.inputForAnswerName != "undefined") ? params.params.inputForAnswerName : "")
							};

							descriptionForm.jsonSchema.properties["params-checkboxNewurgency"] = {
								inputType : "hidden"
							}
							if (typeof params.subType != "undefined" && params.subType == "aap") {
								descriptionForm.jsonSchema.properties["params-checkboxNewinterventionArea"] = {
									inputType : "hidden"
								}
								descriptionForm.jsonSchema.properties["params-radioNewpublicCible"] = {
									inputType : "hidden"
								}
								descriptionForm.jsonSchema.properties["params-radioNewaxesTFPB"] = {
									inputType : "hidden"
								}
							}
							descriptionForm.jsonSchema.properties["params-onlymemberaccess"] = {
								inputType : "checkboxSimple",
								label : trad.onlymemberscanaccess,
								params : checkboxSimpleParams,
								checked : typeof jsonHelper.getValueByPath(params, "params.onlymemberaccess") != "undefined" ? jsonHelper.getValueByPath(params, "params.onlymemberaccess") : true
							};
							descriptionForm.jsonSchema.properties["params-onlyauthorcanedit"] = {
								inputType : "checkboxSimple",
								label : trad.onlyauthorcanaccess,
								params : checkboxSimpleParams,
								checked : typeof jsonHelper.getValueByPath(params, "params.onlyauthorcanedit") != "undefined" ? jsonHelper.getValueByPath(params, "params.onlyauthorcanedit") : true
							};
							descriptionForm.jsonSchema.properties["params-adminabsoluteaccess"] = {
								inputType : "checkboxSimple",
								label : trad.canadminsstillaccess,
								params : checkboxSimpleParams,
								checked : typeof jsonHelper.getValueByPath(params, "params.adminabsoluteaccess") != "undefined" ? jsonHelper.getValueByPath(params, "params.adminabsoluteaccess") : true
							};

							if (params.subType == "ocecoform") {
								descriptionForm.jsonSchema.properties["stepOneOnly"] = {
									inputType : "checkboxSimple",
									label : trad.answeronlyproposalestage,
									params : checkboxSimpleParams,
									checked : typeof jsonHelper.getValueByPath(params, "params.stepOneOnly") != "undefined" ? jsonHelper.getValueByPath(params, "params.stepOneOnly") : false
								};
							}

							descriptionForm.jsonSchema.properties["hidden-btnObservatory"] = {
								inputType : "checkboxSimple",
								label : trad['Hide or show the project observatory button'],
								params : checkboxSimpleParams,
								checked : jsonHelper.getValueByPath(params, "hidden.btnObservatory")
							};

							if (exists(params.subForms) && params.subForms != null) {
								$.each(params.subForms, function (k, v) { //common properties
									var i = (parseInt(k) + 1);

									descriptionForm.jsonSchema.properties["params-aapStep" + i + "-haveEditingRules"] = {
										inputType : "checkboxSimple",
										label : trad.restrictediting,
										info : trad.onlythosewithauthorizecanedit,
										params : checkboxSimpleParams,
										checked : jsonHelper.getValueByPath(params, "params.aapStep" + i + ".haveEditingRules")
									};
									descriptionForm.jsonSchema.properties["params-aapStep" + i + "-haveReadingRules"] = {
										inputType : "checkboxSimple",
										label : trad.restrictaccess,
										info : trad.onlythosewithauthorizecanaccess,
										params : checkboxSimpleParams,
										checked : jsonHelper.getValueByPath(params, "params.aapStep" + i + ".haveReadingRules")
									};
									descriptionForm.jsonSchema.properties["params-aapStep" + i + "-canEdit"] = {
										inputType : "tags",
										minimumInputLength : 0,
										label : trad.rolescanedit,
										info : trad.ifemptyonlycreatorcanedit,
										values : ["Evaluateur", "Financeur"],
										value : typeof jsonHelper.getValueByPath(params, "params.aapStep" + i + ".canEdit") != "undefined" ?
											(Array.isArray(jsonHelper.getValueByPath(params, "params.aapStep" + i + ".canEdit")) ?
												jsonHelper.getValueByPath(params, "params.aapStep" + i + ".canEdit")
												: jsonHelper.getValueByPath(params, "params.aapStep" + i + ".canEdit").split(","))
											: ""
									};
									descriptionForm.jsonSchema.properties["params-aapStep" + i + "-canRead"] = {
										inputType : "tags",
										minimumInputLength : 0,
										label : trad.rolescanaccess,
										info : trad.ifemptyonlycreatorcanaccess,
										values : ["Evaluateur", "Financeur"],
										value : typeof jsonHelper.getValueByPath(params, "params.aapStep" + i + ".canRead") != "undefined" ?
											(Array.isArray(jsonHelper.getValueByPath(params, "params.aapStep" + i + ".canRead")) ?
												jsonHelper.getValueByPath(params, "params.aapStep" + i + ".canRead")
												: jsonHelper.getValueByPath(params, "params.aapStep" + i + ".canRead").split(","))
											: ""
									};
								});
							}
						}
					},
					save : function (formData) {
						mylog.log('save tplCtx formData', formData)
						var tplCtx = {
							id : params._id.$id,
							collection : "forms",
							value : {},
							format : true,
							path : "allToRoot",
							setType : [
								{path : "useBannerImg", type : "boolean"},
								{path : "useBannerText", type : "boolean"},
								{path : "startDate", type : "isoDate"},
								{path : "endDate", type : "isoDate"},
								{path : "startDateNoconfirmation", type : "isoDate"},
								{path : "endDateNoconfirmation", type : "isoDate"}
							]
						};

						/*if(typeof params["params"] != "undefined"){
						 tplCtx.value["params"] = params["params"];
						 }*/

						$.each(descriptionForm.jsonSchema.properties, function (k, v) {
							var kk = k.split("-").join("][");
							if (k.split("-").length > 1) {
								if (params.type == "aap" && k == "params-checkboxNewurgency") {
									if (notNull(params.params) && notNull(params.params.checkboxNewurgency) && notNull(params.params.checkboxNewurgency.list) && Array.isArray(params.params.checkboxNewurgency.list) && params.params.checkboxNewurgency.list.length != 0) {} else {
										tplCtx.value[kk] = {
											"list" : [
												"Urgent"
											]
										}
									}
								} else if (params.type == "aap" && params.subType == "aap" && k == "params-checkboxNewinterventionArea") {
									if (notNull(params.params) && notNull(params.params.checkboxNewinterventionArea) && notNull(params.params.checkboxNewinterventionArea.list) && Array.isArray(params.params.checkboxNewinterventionArea.list) && params.params.checkboxNewinterventionArea.list.length != 0) {} else {
										tplCtx.value[kk] = {
											"list" : [
												"Centre Ville", "Marcadet", "Bas-de-la-Rivière", "Butor", "Sainte-Clotilde", "Montagne 8ème", "Montagne 15 ème", "Source", "Bellepierre", "Brûlé", "Vauban", "Camélia", "Providence", "Saint-François", "Montgaillard", "Chaudron", "Primat", "Moufia 2", "Moufia", "Bois-de-Nèfle", "Bretagne", "Domenjod"
											],
										}
									}
								} else if (params.type == "aap" && params.subType == "aap" && k == "params-radioNewpublicCible") {
									if (notNull(params.params) && notNull(params.params.radioNewpublicCible) && notNull(params.params.radioNewpublicCible.list) && Array.isArray(params.params.radioNewpublicCible.list) && params.params.radioNewpublicCible.list.length != 0) {} else {
										tplCtx.value[kk] = {
											"list" : [
												"Moins de 12 ans", "De 13 ans à 18 ans", "De 18 ans à 29 ans", "De 30 ans à 49 ans", "Séniors", "Tous publics"
											],
										}
									}
								} else if (params.type == "aap" && params.subType == "aap" && k == "params-radioNewaxesTFPB") {
									if (notNull(params.params) && notNull(params.params.radioNewaxesTFPB) && notNull(params.params.radioNewaxesTFPB.list) && Array.isArray(params.params.radioNewaxesTFPB.list) && params.params.radioNewaxesTFPB.list.length != 0) {} else {
										tplCtx.value[kk] = {
											"list" : [
												"Pilier valeur de la République et citoyenneté",
												"Pilier Developpement Economique et Emploi",
												"Pilier Cadre de vie et Renouvellement urbain",
												"Pilier Cohésion Sociale",
												"Axe 1 - Renforcement de la présence de Proximité",
												"Axe 2 - Formation soutien des personnels de proximité",
												"Axe 3 - Sur-entretien",
												"Axe 4 - Gestion des déchets et encombrants",
												"Axe 5 - Tranquilité résidentielle",
												"Axe 6 - Concertation - sensibilisation des locataires",
												"Axe 7 - Animation Lien Social",
												"Axe 8 - Petits travaux d'amélioration de la qualité de service"
											],
										}
									}
								} else {
									var spltid = k.split("-");

									var spltidobj = spltid.reverse().reduce(function (total, current, index) {
										var sp = {};
										if (index == 0) {
											sp[current] = $("#" + k).val();
										} else {
											sp[current] = total;
										}
										return sp;
									}, {});


									function isObject(item) {
										return (item && typeof item === 'object' && !Array.isArray(item));
									}

									function mergeDeep(target, ...sources) {
										if (!sources.length) {
											return target;
										}
										const source = sources.shift();

										if (isObject(target) && isObject(source)) {
											for (const key in source) {
												if (isObject(source[key])) {
													if (!target[key]) {
														Object.assign(target, {[key] : {}});
													}
													mergeDeep(target[key], source[key]);
												} else {
													Object.assign(target, {[key] : source[key]});
												}
											}
										}

										return mergeDeep(target, ...sources);
									}

									/*$.each(spltidobj, function(indx, val){
									 if (indx == "params"){
									 $.each(val, function(indx2, val2){
									 tplCtx.value[indx][indx2] = val2;
									 });
									 }else{
									 tplCtx.value[indx]= val;
									 }
									 });*/
									if (spltidobj != null) {
										$.each(spltidobj, function (indx, val) {
											if (notNull(params) && notNull(params[indx])) {
												tplCtx.value[indx] = mergeDeep(params[indx], val);
											} else {
												tplCtx.value[indx] = val;
											}
										});
									}
								}

							} else {
								if (v.inputType == "array") {
									tplCtx.value[k] = getArray('.' + k + v.inputType);
								} else if (params.type == "aap" && k == "params-checkboxNewurgency") {
									tplCtx.value[k] = {
										"list" : [
											"Urgent"
										],
									}
								} else {
									tplCtx.value[k] = $("#" + k).val();
								}
							}
							/*var kk = k.split("-").join("][");

							 if (v.inputType == "array")
							 tplCtx.value[kk] = getArray('.' + k + v.inputType);
							 else if(params.type="aap" && k=="params-checkboxNewurgency")
							 tplCtx.value[kk] = {
							 "global" : {
							 "list" : [
							 "Urgent"
							 ],
							 "dependOn" : ""
							 }
							 }
							 else
							 tplCtx.value[kk] = $("#" + k).val();*/
						});
						['startDate', 'endDate', 'startDateNoconfirmation', 'endDateNoconfirmation'].forEach(function (dateField) {
							if (typeof tplCtx.value[dateField] === 'string') {
								tplCtx.value[dateField] = moment(tplCtx.value[dateField], 'DD/MM/YYYY HH:mm').format();
							}
						})

						tplCtx.value["parent"] = formData.parent;
						mylog.log("save tplCtx", $.extend(tplCtx));

						if (typeof tplCtx.value == "undefined") {
							toastr.error('value cannot be empty!');
						} else {
							dataHelper.path2Value(tplCtx, function (prms) {
								var parentId = Object.keys(prms.elt.parent)[0];
								var notifMailParams = {
									id : prms.id,
									startDate : moment(prms.elt.startDate, "DD/MM/YYYY").format("YYYY-MM-DD"),
									endDate : moment(prms.elt.endDate, "DD/MM/YYYY").format("YYYY-MM-DD"),
									name : prms.elt.name,
									description : prms.elt.description,
									parent : {
										id : parentId,
										type : prms.elt.parent[parentId].type,
										name : prms.elt.parent[parentId].name
									}
								}
								if (update === false) {
									$.post("/survey/form/sendemail/type/notifAapEnd", notifMailParams)
								}

								dyFObj.commonAfterSave(prms, function () {
									if (params.type == "aap" && params.subType == "aap") {
										dataHelper.path2Value({
											id : prms.id,
											collection : "forms",
											path : "params.principalDomaine.options",
											value : getArray('.actionDomainsarray')
										}, function () {
											toastr.success("Enregistré");
											dyFObj.closeForm();
											urlCtrl.loadByHash(location.hash);
										})
									} else {
										toastr.success("Enregistré");
										dyFObj.closeForm();
										urlCtrl.loadByHash(location.hash);
									}
								})
							});
						}

					}
				}
			};
			// if(exists(params.subType) && params.subType =="ocecoform"){
			// 	delete descriptionForm.jsonSchema.properties.actionDomains;
			// 	delete descriptionForm.jsonSchema.properties.objectives;
			// }
			if (update) {
				dyFObj.openForm(descriptionForm, null, params);
			} else {
				dyFObj.openForm(descriptionForm);
			}
		}
	},
	openForm : {
		list : function () {
			var openFormObj = {};

			$.getJSON(baseUrl + '/survey/form/getopenform', function (data) {
				if (typeof data.forms != "undefined") {
					$.each(data.forms, function (id, val) {
						if (typeof val.name != "undefined") {
							openFormObj[val["_id"]["$id"]] = val.name;
							mylog.log("azeeer", openFormObj);
						}
					});
				}
				;
			});

			return openFormObj;
		}
	},
	aap : {
		currentAapConfig : function (fObj) { //check if appConfig already exist in this element
			var currentAapConfig = {};
			$.each(fObj.forms, function (k, v) {
				if (v.type == "aapConfig") {
					existAapConfig = true;
					currentAapConfig = v;
					currentAapConfig["exist"] = true;
					return false
				}
			});
			return currentAapConfig
		},
		allAapConfigList : function () {
			var aapConfigList = null;
			$.ajax({
				type : "POST",
				url : baseUrl + "/co2/search/globalautocomplete",
				data : {
					searchType : "forms",
					filters : {type : "aapConfig"},
					fields : ["name", "type", "subForms"]
				},
				async : false,
				success : function (data) {
					aapConfigList = data.results;
				},
				dataType : "json"
			});
			return aapConfigList;
		},
		generateInputsDocument : function (fObj, options) {
			// generete inputs documents and return it lists
			var inputsList = null;
			$.ajax({
				type : "POST",
				url : baseUrl + '/survey/form/generateinputs',
				data : options,
				async : false,
				success : function (res) {
					inputsList = res.data;
				},
				dataType : "json"
			});
			return inputsList;
		},
		generateFormParent : function (fObj) {
			smallMenu.openAjaxHTML(baseUrl + '/co2/aap/getviewbypath/path/costum.views.custom.appelAProjet.templateList');
		},
		generateFormParentTemplate : function (fObj) {
			smallMenu.openAjaxHTML(baseUrl + '/co2/aap/getviewbypath/path/costum.views.custom.appelAProjet.templateFormList');
		},
		generatecopiedFormParent : function (fObj) {
			smallMenu.openAjaxHTML(baseUrl + '/co2/aap/getviewbypath/path/costum.views.custom.appelAProjet.copyableList');
		},
		generateCostum : function () {
			var params = {
				cms : [
					{
						name : "Appel à projet",
						page : "welcome",
						path : "tpls.blockCms.projects.callForProjects",
						type : "blockCopy",
						collection : "cms",
						//view : "proposition",
						haveTpl : "false"
					}
				]
			};
			params.cms[0]["parent"] = {};
			params.cms[0]["parent"][contextData.id] = {
				"type" : contextData.collection,
				"name" : contextData.name
			};
			options = {
				withbootbox : true,
				type : "aap"
			};
			if (typeof contextData.costum == "undefined") {
				costumize(false, params, options);
			} else { // just add block cms if costum exists
				var ajaxPostParams = {
					searchType : ["cms"],
					filters : {
						"path" : "tpls.blockCms.projects.callForProjects"
					}
				}
				ajaxPostParams.filters['parent.' + contextData.id] = {'$exists' : true};
				ajaxPost('', baseUrl + "/" + moduleId + "/search/globalautocomplete",
					ajaxPostParams,
					function (data) {
						if (Object.values(data.results).length == 0) {
							var tplCtx = {};
							tplCtx.collection = "cms";
							tplCtx.path = "allToRoot";
							tplCtx.value = {...params.cms[0]};

							if (typeof tplCtx.value == "undefined") {
								toastr.error("value cannot be empty!");
							} else {
								dataHelper.path2Value(tplCtx, function (pms) {
									alert(JSON.stringify(params.cms[0], null, 2));
									if (pms.result) {
										toastr.success("Bloc bien ajouter");
									} else {
										toastr.error(params.msg);
									}
								});
							}
						}
					}, null);
			}
			location.reload();
		},
	}
};
