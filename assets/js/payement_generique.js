(function (X, $) {
    if (typeof X.cos_payementlist === 'function')
        return;
    function xhr(cart) {
        var that = this;
        this._cart = cart;

        this.cart = function () {
            return (this._cart);
        }
        this.camp_fund = function () {
            return new Promise(function (resolve, reject) {
                var post, url;

                if (!that.cart().form())
                    reject({
                        code: 101,
                        msg: 'Missing form id or bad format (string only)'
                    });
                else {
                    post = {
                        form: that.cart().form()
                    };
                    url = baseUrl + '/co2/aap/funding/request/cart';
                    ajaxPost(null, url, post, function (resp) {
                        if (resp.success)
                            resolve(resp.content);
                        else
                            reject({
                                code: 400,
                                msg: resp.content
                            });
                    }, reject)
                }
            });
        }
        this.aap_checkout = function () {
            return new Promise(function () {

            });
        }
        this.aap_camp_checkout = function (args) {
            return new Promise(function (resolve, reject) {
                var url, post;

                if (typeof args !== 'object' || !args)
                    reject({
                        code: 101,
                        msg: 'Missing or incomplete arguments'
                    });
                else if (!Array.isArray(args.funds) || !args.funds.length)
                    reject({
                        code: 102,
                        msg: 'Empty values'
                    });
                else if (!that.cart().form())
                    reject({
                        code: 103,
                        msg: 'Missing form id'
                    });
                else {
                    post = {
                        funds: args.funds,
                        form: that.cart().form()
                    };
                    url = baseUrl + '/co2/aap/funding/request/camp_double';
                    ajaxPost(null, url, post, function (resp) {
                        if (resp.success)
                            resolve(resp.content);
                        else
                            reject({
                                code: 400,
                                msg: resp.content
                            });
                    }, function (arg0, arg1, arg2) {
                        reject({
                            code: 401,
                            msg: [arg0, arg1, arg2]
                        })
                    })
                }
            });
        }
        this.camp_proc_fin = function () {
            var url, post;
            return new Promise(function (resolve, reject) {
                if (!that.cart().form()) {
                    reject({
                        code: 101,
                        msg: 'Missing form id or bad format (string only)'
                    })
                }else {
                    post = {
                        page: "financer",
                        form: that.cart().form(),
                        candidat: that.cart().candidat(),
                        view : 'costum.views.custom.aap.genericpayment.financeur_list'
                    };
                    if (that.cart().campagne_process().tl())
                        post.selected = that.cart().campagne_process().tl();
                    url = baseUrl + '/co2/aap/camp-candidat-paielist-html/';
                    ajaxPost(null, url, post, function (resp) {
                        if (resp.success)
                            resolve(resp.content);
                        else
                            reject({
                                code: 400,
                                msg: resp.content
                            });
                    }, function (arg0, arg1, arg2) {
                        reject({
                            code: 401,
                            msg: [arg0, arg1, arg2]
                        })
                    })
                }
            });
        }
        this.camp_proc_commun = function (args) {
            var url, post;
            return new Promise(function (resolve, reject) {
                url = baseUrl + '/co2/aap/camp-candidat-paielist-html/';
                if (!that.cart().form())
                    reject({
                        code: 101,
                        msg: 'Missing form id or bad format (string only)'
                    });
                else if (!that.cart().campagne_process().tl())
                    reject({
                        code: 102,
                        msg: 'No tiers-lieu selected'
                    })
                else {
                    /*url += '/form/' + that.cart().form();
                    url += '/tl/' + that.cart().campagne_process().tl();
                    post = $.extend({}, post, args);*/
                    post = {
                        page:"candidatdepense",
                        form: that.cart().form(),
                        financer : that.cart().campagne_process().tl(),
                        candidat: that.cart().candidat(),
                        communs: camp_proc.communs(),
                        view : 'costum.views.custom.aap.genericpayment.candidat_list'
                    };
                    ajaxPost(null, url, post, function (resp) {
                        if (resp.success)
                            resolve(resp.content);
                        else
                            reject({
                                code: 400,
                                msg: resp.content
                            });
                    }, function (arg0, arg1, arg2) {
                        reject({
                            code: 401,
                            msg: [arg0, arg1, arg2]
                        })
                    })
                }
            });
        }
        this.camp_proc_helloasso_pay = function () {
            var url,
                post,
                camp_proc;

            camp_proc = that.cart().campagne_process();
            return new Promise(function (resolve, reject) {
                url = baseUrl + '/co2/helloAsso/campagne-tl';

                if (!camp_proc.communs().length)
                    reject({
                        code: 101,
                        msg: 'Nothing to pay'
                    });
                else if (!camp_proc.tl())
                    reject({
                        code: 102,
                        msg: 'No tiers-lieu selected'
                    })
                else if (!that.cart().form())
                    reject({
                        code: 103,
                        msg: 'Missing form id or bad format (stirng only)'
                    })
                else {
                    post = {
                        tl: camp_proc.tl(),
                        communs: camp_proc.communs(),
                        form: that.cart().form(),
                    };
                    if (camp_proc.info())
                        post.infos = camp_proc.info();
                    ajaxPost(null, url, post, function (resp) {
                        if (resp.success)
                            resolve(resp.content);
                        else
                            reject({
                                code: 400,
                                msg: resp.content
                            });
                    }, function (arg0, arg1, arg2) {
                        reject({
                            code: 401,
                            msg: [arg0, arg1, arg2]
                        })
                    })
                }
            });
        }
        this.camp_proc_stripe_pay = function () {
            var url,
                post,
                camp_proc;

            camp_proc = that.cart().campagne_process();
            return new Promise(function (resolve, reject) {
                url = baseUrl + '/co2/aap/camp-fund-validate/method/stripe';

                if (!camp_proc.communs().length)
                    reject({
                        code: 101,
                        msg: 'Nothing to pay'
                    });
                else if (!camp_proc.tl())
                    reject({
                        code: 102,
                        msg: 'No tiers-lieu selected'
                    })
                else if (!that.cart().form())
                    reject({
                        code: 103,
                        msg: 'Missing form id or bad format (stirng only)'
                    })
                else {
                    post = {
                        tl: camp_proc.tl(),
                        communs: camp_proc.communs(),
                        form: that.cart().form(),
                    };
                    if (camp_proc.info())
                        post.infos = camp_proc.info();
                    ajaxPost(null, url, post, function (resp) {
                        if (resp.success)
                            resolve(resp.content);
                        else
                            reject({
                                code: 400,
                                msg: resp.content
                            });
                    }, function (arg0, arg1, arg2) {
                        reject({
                            code: 401,
                            msg: [arg0, arg1, arg2]
                        })
                    })
                }
            });
        }
        this.camp_proc_bank_pay = function () {
            var url,
                post,
                camp_proc;

            camp_proc = that.cart().campagne_process();
            return new Promise(function (resolve, reject) {
                url = baseUrl + '/co2/aap/camp-fund-validate/method/bank';

                if (!camp_proc.communs().length)
                    reject({
                        code: 101,
                        msg: 'Nothing to pay'
                    });
                else if (!that.cart().form())
                    reject({
                        code: 102,
                        msg: 'Missing form id or bad format (string only)'
                    })
                else {
                    post = {
                        tl: camp_proc.tl(),
                        communs: camp_proc.communs(),
                        form: that.cart().form(),
                        method: "bank",
                        wait_for_validation: 1,
                    };
                    if (camp_proc.info())
                        post.infos = camp_proc.info();
                    ajaxPost(null, url, post, function (resp) {
                        if (resp.success)
                            resolve(resp.content);
                        else
                            reject({
                                code: 400,
                                msg: resp.content
                            });
                    }, function (arg0, arg1, arg2) {
                        reject({
                            code: 401,
                            msg: [arg0, arg1, arg2]
                        })
                    })
                }
            });
        }
        this.camp_proc_payment_validation = function () {
            var url,
                post,
                camp_proc;

            camp_proc = that.cart().campagne_process();
            return new Promise(function (resolve, reject) {
                url = baseUrl + '/co2/aap/funding/request/camp_double';

                if (!camp_proc.communs().length)
                    reject({
                        code: 101,
                        msg: 'Nothing to pay'
                    });
                else if (!that.cart().form())
                    reject({
                        code: 102,
                        msg: 'Missing form id or bad format (string only)'
                    })
                else {
                    post = {
                        form: that.cart().form(),
                        communs: camp_proc.communs()
                    };
                    if (camp_proc.info())
                        post.infos = camp_proc.info();
                    ajaxPost(null, url, post, function (resp) {
                        if (resp.success)
                            resolve(resp.content);
                        else
                            reject({
                                code: 400,
                                msg: resp.content
                            });
                    }, function (arg0, arg1, arg2) {
                        reject({
                            code: 401,
                            msg: [arg0, arg1, arg2]
                        })
                    })
                }
            });
        }
        this.camp_proc_info = function () {
            var url,
                tl,
                post;

            tl = that.cart().campagne_process().tl();
            return new Promise(function (resolve, reject) {
                if (!tl)
                    reject({
                        code: 101,
                        msg: 'No tl selected'
                    });
                else {
                    post = {
                        tl: tl
                    };
                    url = baseUrl + '/co2/aap/camp-cart-bill-info-html';
                    ajaxPost(null, url, post, function (resp) {
                        if (resp.success)
                            resolve(resp.content);
                        else
                            reject({
                                code: 400,
                                msg: resp.content
                            });
                    }, function (arg0, arg1, arg2) {
                        reject({
                            code: 401,
                            msg: [arg0, arg1, arg2]
                        })
                    })
                }
            });
        }
        this.camp_proc_payment_type = function () {
            var url,
                post;

            return new Promise(function (resolve, reject) {
                if (!that.cart().form())
                    reject({
                        code: 101,
                        msg: 'Missing form id or bad format (string only)'
                    })
                else {
                    post = {
                        page : "paymenttypepage",
                        form : that.cart().form(),
                        financer : that.cart().campagne_process().tl(),
                        candidat : that.cart().candidat(),
                        communs : that.cart().campagne_process().communs(),
                        view : 'costum.views.custom.aap.genericpayment.payementtype_list'
                    };

                    url = baseUrl + '/co2/aap/camp-candidat-paielist-html/';

                    ajaxPost(null, url, post, function (resp) {
                        if (resp.success)
                            resolve(resp.content);
                        else
                            reject({
                                code: 400,
                                msg: resp.content
                            });
                    }, function (arg0, arg1, arg2) {
                        reject({
                            code: 401,
                            msg: [arg0, arg1, arg2]
                        })
                    });
                }
            });
        }
        this.camp_del = function () {
            return (new Promise(function (resolve, reject) {
                var url,
                    post,
                    camp_proc;

                url = baseUrl + '/co2/aap/funding/request/delete';
                camp_proc = that.cart().campagne_process();
                if (!camp_proc.communs().length)
                    reject({
                        code: 101,
                        msg: 'Nothing to pay'
                    });
                else if (!that.cart().form())
                    reject({
                        code: 102,
                        msg: 'Missing form id or bad format (string only)'
                    })
                else {
                    post = {
                        form: that.cart().form(),
                        communs: camp_proc.communs()
                    };
                    ajaxPost(null, url, post, function (resp) {
                        if (resp.success)
                            resolve(resp.content);
                        else
                            reject({
                                code: 400,
                                msg: resp.content
                            });
                    }, function (arg0, arg1, arg2) {
                        reject({
                            code: 401,
                            msg: [arg0, arg1, arg2]
                        });
                    });
                }
            }));
        }
    }

    function modal() {
        this._on_show = null;
        this._on_hide = null;

        this.on_show = function (arg0) {
            if (!arg0)
                return (this._on_show);
            if (typeof arg0 === 'function')
                this._on_show = arg0;
            return (this);
        }
        this.on_hide = function (arg0) {
            if (!arg0)
                return (this._on_hide);
            if (typeof arg0 === 'function')
                this._on_hide = arg0;
            return (this);
        }
    }

    function campagne_process() {
        this._tl = null;
        this._candidat = null;
        this._amount = 0;
        this._communs = [];
        this._info = null;
        this._pay_meth = null;
        this._tl_len = 0;

        this.reset = function () {
            this._tl = null;
            this._candidat = null;
            this._amount = 0;
            this._communs = [];
            this._info = null;
            this._pay_meth = null;
            this._tl_len = 0;
            return (this);
        }

        this.tl_len = function (arg) {
            if (typeof arg !== 'number')
                return (this._tl_len);
            this._tl_len = arg;
            return (this);
        }

        this.tl = function (arg) {
            if (typeof arg === 'undefined')
                return (this._tl);
            this._tl = arg;
            return (this);
        }

        this.candidat = function (arg) {
            if (typeof arg === 'undefined')
                return (this._candidat);
            this._candidat = arg;
            return (this);
        }

        this.payment_method = function (arg) {
            if (typeof arg === 'undefined')
                return (this._pay_meth);
            this._pay_meth = arg;
            return (this);
        }

        this.reset_communs = function () {
            this._communs = [];
            return (this);
        }

        this.amount = function (arg) {
            if (typeof arg === 'undefined')
                return (this._amount);
            this._amount = arg;
            return (this);
        }

        this.communs = function (arg) {
            if (typeof arg === 'undefined')
                return (this._communs.slice());
            this._communs.push(arg);
            return (this);
        }

        this.info = function (arg) {
            if (typeof arg === 'undefined')
                return (this._info);
            if (this._info)
                this._info = $.extend(this._info, arg);
            else
                this._info = arg;
            return (this);
        }
    }

    function html() {
        this.tl_list = function () {

        }
    }
    X.cos_payementlist = function (menu) {
        var that = this;
        this._form = null;
        this._amount = 0;
        this._candidat = null;

        if (!menu) {
            mylog.error('Costum cart cannot be build without menu dom id');
            return;
        }
        this.set = function (arg0, arg1) {
            if (typeof arg0 === 'object' && arg0 && typeof arg1 === 'undefined')
                $.each(arg0, function (key, val) {
                    that['_' + key] = val;
                    if (typeof that[key] !== 'function')
                        that[key] = function (arg) {
                            if (typeof arg === 'undefined') {
                                if (typeof that['_' + key] === 'object') {
                                    if (Array.isArray(that['_' + key]))
                                        return (that['_' + key].slice());
                                    return (Object.assign({}, that['_' + key]));
                                }
                                return (that['_' + key]);
                            }
                            if (typeof arg !== 'undefined')
                                that['_' + key] = arg;
                            return (that);
                        }
                });
            else if (typeof arg0 === 'string' && typeof arg1 !== 'undefined') {
                that['_' + arg0] = arg1;
                if (typeof that[arg0] !== 'function')
                    that[arg0] = function (arg) {
                        if (typeof arg === 'undefined') {
                            if (typeof that['_' + arg0] === 'object') {
                                if (Array.isArray(that['_' + arg0]))
                                    return (that['_' + arg0].slice());
                                return (Object.assign({}, that['_' + arg0]));
                            }
                            return (that['_' + arg0]);
                        }
                        if (typeof arg !== 'undefined')
                            that['_' + arg0] = arg;
                        return (that);
                    }
            }
            return (this);
        }

        // construct
        this.set({ menu: menu });

        this.dom = function (selector) {
            return ($(this.menu()).find(selector));
        }

        this.form = function (arg0, arg1) {
            if (typeof arg0 === 'undefined')
                return (that._form);
            if (typeof arg0 === 'string')
                that._form = arg0;
            return (that);
        }

        this.candidat = function (arg0, arg1) {
            if (typeof arg0 === 'undefined')
                return (that._candidat);
            if (typeof arg0 === 'string')
                that._candidat = arg0;
            return (that);
        }

        this.amount = function (arg0, arg1) {
            if (typeof arg0 === 'undefined')
                return (that._amount);
            if (typeof arg0 === 'string')
                that._amount = arg0;
            return (that);
        }
        this.modal = function () {
            if (!this._modal)
                this._modal = new modal();
            return (this._modal);
        }
        this.xhr = function () {
            if (!this._xhr)
                this._xhr = new xhr(this);
            return (this._xhr);
        }

        this.campagne_process = function () {
            if (!this._camp_proc)
                this._camp_proc = new campagne_process();
            return (this._camp_proc);
        }

        this.init_dom_event = function () {
            $(document).on('click', '.paybudgetperc', function (e) {
                e.stopPropagation();
                var show_event = that.modal().on_show();
                var self = $(this);
                if (notEmpty(self.data("candidat"))) {
                    that.candidat(self.data("candidat"));
                }

                if (!self.hasClass("disabled"))
                    $.when($('.co-popup-payementlist-container').show()).then(function (x) {
                        that.menu('.co-popup-payementlist-container');
                        show_event();
                    });
            }).on('click', 'button.cos-payementlist-validate', function () {
                var args = {
                    funds: []
                };
                var self = $(this);

                if (self.prop('disabled'))
                    return;
                self.prop('disabled', true);
                self.empty().html('<i class="fa fa-spinner fa-spin"></i> Valider');
                if (that.amount()) {
                    $(that.menu()).find('input[type=checkbox]:checked').each(function () {
                        var self = $(this);

                        args.funds.push({
                            tl: self.data('tl'),
                            ans: self.data('commun'),
                            double: self.data('double')
                        });
                    });
                    that.xhr().aap_camp_checkout(args).then(function () {
                        toastr.success('Votre opération est réussie');
                        $('.modal').modal('hide');
                    }).catch(function () {
                        self.prop('disabled', false)
                            .empty()
                            .text('Valider');
                    });
                } else {
                    toastr.error('Aucune opération à valider');
                    self.prop('disabled', false)
                        .empty()
                        .text('Valider');
                }
            });
            return (this);
        }
    }
    var aap = new aapv2();
    X.g_cos_payementlist = new X.cos_payementlist('.modal.cos-payementlist');
    if (aap.url().dot('formid'))
        form = aap.url().dot('formid');
    else
        form = '6438366673d20a0de1533c77';
    X.g_cos_payementlist.form(form)
        .init_dom_event()
        .modal()
        .on_show(function () {
            // cart for campagne
            var form,
                that = X.g_cos_payementlist;
            //set orga name
            $('.co-popup-payementlist-question .title-info').html(costum.title);
            if (costum.logo)
                $('.co-popup-payementlist-question .logo-info').attr('src', costum.logo);
            $('.close-payementlist').off().click(function () {
                $('.co-popup-payementlist-container').hide();
                if (typeof customFinObj !== 'undefined' && typeof customFinObj.data !== 'undefined' && typeof customFinObj.data.lineToPay !== 'undefined') {
                    customFinObj.data.lineToPay = {}
                }
            });

            // get form id
            that.campagne_process().tl(null);

            var popup_container_dom = $(".co-popup-payementlist-menu+div");
            popup_container_dom.removeClass("co-popup-payementlist-normal")
                .addClass("co-popup-payementlist-center");
            that.dom('.modal-body.cos-payementlist')
                .empty()
                .html(X.coInterface.showCostumLoader());
            that.xhr()
                .camp_proc_fin()
                .then(function (html) {
                    that.dom('.modal-body.cos-payementlist')
                        .empty()
                        .html(html);
                })
                .catch(function (err) {
                    if (err.code === 400)
                        that.dom('.modal-body.cos-payementlist')
                            .empty()
                            .html(X.coTranslate(err.msg));
                }).finally(function () {
                popup_container_dom.addClass("co-popup-payementlist-normal")
                    .removeClass("co-popup-payementlist-center");
            });
        });
    X.refresh_camp_count = function () {
        X.g_cos_payementlist.xhr()
            .camp_fund()
            .then(function (resp) {
                var count,
                    cart_btn;

                count = 0;
                cart_btn = $(".cos-payementlist.cos-payementlist-button");
                cart_btn.find(".cart-count")
                    .empty()
                    .addClass("hide");
                $.each(resp.funds, function (_, fund) {
                    count += fund.communs.length;
                });
                if (count) {
                    cart_btn.find(".cart-count")
                        .text(count)
                        .removeClass("hide");
                    cart_btn.removeClass("disabled");
                } else
                    cart_btn.addClass("disabled");
            });
    }
    X.refresh_camp_count();
})(window, jQuery);