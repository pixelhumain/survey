if(typeof bindDnDEvent == 'undefined') {
    function bindDnDEvent() {
        if (typeof coForm_FormWizardParams != "undefined" && typeof coForm_FormWizardParams.mode != "undefined" && coForm_FormWizardParams.mode == "fa" && typeof coForm_FormWizardParams.canAdminAnswer != 'undefined' && coForm_FormWizardParams.canAdminAnswer ) {
            var currentlyScrolling = false;

            var SCROLL_AREA_HEIGHT = 40;
            $(function () {
                $(".coformstep div.questionList").off("sortable").sortable({
                    cursor: 'move',
                    cursorAt: {top: 5},
                    handle: 'h4,h3',
                    delay: 150,
                    grid: [20, 10],
                    placeholder: "buildBlockPlaceholder",
                    scroll: true,
                    start: function(e, ui) {
                        ui.placeholder.height(ui.item.height());
                        ui.placeholder.width(ui.item[0].offsetWidth);
                        ui.item.removeClass("dragged").addClass("dragged")
                    },
                    sort: function(event, ui) {
                        if (currentlyScrolling) {
                            return;
                        }
                        var windowHeight   = $(window).height();
                        var mouseYPosition = event.clientY;

                        if (mouseYPosition < SCROLL_AREA_HEIGHT) {
                            currentlyScrolling = true;

                            $('html, body').animate({
                            scrollTop: "-=" + windowHeight / 2 + "px" // Scroll up half of window height.
                            }, 
                            400, // 400ms animation.
                            function() {
                            currentlyScrolling = false;
                            });

                        } else if (mouseYPosition > (windowHeight - SCROLL_AREA_HEIGHT)) {

                            currentlyScrolling = true;
                    
                            $('html, body').animate({
                            scrollTop: "+=" + windowHeight / 2 + "px" // Scroll down half of window height.
                            }, 
                            400, // 400ms animation.
                            function() {
                            currentlyScrolling = false;
                            });
                    
                        }
                    },
                    stop: function(e, ui) {
                        ui.item.removeClass("dragged")
                    }
                });
                $(".questionDraggable").off("draggable").draggable({
                    connectToSortable: ".coformstep div.questionList",
                    cursor: "move",
                    cursorAt: {left: 5},
                    helper: "clone",
                    revert: "invalid",
                    zIndex: 2,
                    scroll: true
                });
            });
        }
        let itemsChanged = [];
        let itemsBefore = [];
        let itemsBeforeObj = {};
        let itemsChangedObj = {};
        if(typeof coForm_FormWizardParams !="undefined" && typeof coForm_FormWizardParams.isAap != "undefined"){
            $(".coformstep div.questionList").off("sortstop").on("sortstop", function (event, ui) {
                let formIdKey = $(this).attr('data-id');
                let idSubForm = $(this).attr('data-ids');
                let formSorted = $(this);
                if(notNull(coForm_FormWizardParams.steps[idSubForm]?.inputs)) {
                    formInputsfor[formIdKey] = JSON.parse(JSON.stringify(coForm_FormWizardParams.steps[idSubForm]?.inputs))
                }
                mylog.log("ul oder changed ui", ui.item);
                let inputData = {};
                let inputCt = "";
                itemsChanged = [];
                itemsChangedObj = {};
                if (ui.item.hasClass("questionDraggable")) {
                    inputCt = Date.now().toString(36) + Math.random().toString(36).substring(2);
                    let label = (`${ui.item.text()}`.replace(/[\n\r]|\r/g, '')).trim();
                    let index = -1;
                    if(coForm_FormWizardParams.isAap == false || coForm_FormWizardParams.isAap == 'false') {
                        inputData = {
                            id: idSubForm,
                            key: undefined,
                            formParentId: coForm_FormWizardParams?.parentForm?.['_id']?.['$id'],
                            collection: "forms",
                            path: `inputs.${formIdKey}${inputCt}`,
                            inputId: `${formIdKey}${inputCt}`,
                            value: {
                                label: index = label.indexOf(":") > -1 ? label.substring(index +1) : label,
                                position : 1,
                                type: ui.item.attr("data-type"),
                            }
                        }
                    } else if (coForm_FormWizardParams.isAap == true || coForm_FormWizardParams.isAap == 'true'){
                        inputData = {
                            id: idSubForm,
                            key: undefined,
                            formParentId: coForm_FormWizardParams?.parentForm?.['_id']?.['$id'],
                            collection: "inputs",
                            path: `inputs.${formIdKey}${inputCt}`,
                            inputId: `${formIdKey}${inputCt}`,
                            value: {
                                label: index = label.replace(/:/g, ' '),
                                placeholder : '',
                                info : '',
                                parentid : coForm_FormWizardParams?.parentForm?.['_id']?.['$id'],
                                positions : {
                                    [coForm_FormWizardParams?.parentForm?.['_id']?.['$id']] : 1
                                },
                                type: ui.item.attr("data-type"),
                            }
                        }
                    }
                    mylog.log("input added", inputData);
                    const inputKey = formIdKey+inputCt;
                    ui.item.replaceWith(`
                        <div id="question_${formIdKey}${inputCt}" class="coforminput questionBlock col-xs-12 ui-corner-all no-padding questionBlock" data-id="${idSubForm}" data-form="${formIdKey}" data-path="${ coForm_FormWizardParams.isAap == true || coForm_FormWizardParams.isAap == 'true' ? "inputs" : "forms" }.${inputKey}.position" data-key="${inputKey}" draggable="false" role="option" aria-grabbed="false"></div>
                    `);
                }
                if(typeof formInputsfor[formIdKey] == 'undefined')
                    formInputsfor[formIdKey] = {};

                if(itemsBefore.length == 0) {
                    itemsBefore = formInputsfor[formIdKey] ? Object.keys(formInputsfor[formIdKey]) : null
                    itemsBeforeObj = formInputsfor[formIdKey] ? JSON.parse(JSON.stringify(formInputsfor[formIdKey])) : {}
                }
                let inputsValueToSend = {
                    collection: "forms",
                    formParentId: coForm_FormWizardParams?.parentForm?.['_id']?.['$id'],
                    id: idSubForm,
                    path: "inputs",
                    updatePartial : true,
                    value: {}
                };
                if(coForm_FormWizardParams.isAap == true || coForm_FormWizardParams.isAap == 'true') {
                    inputsValueToSend = {
                        collection: "inputs",
                        formParentId: coForm_FormWizardParams?.parentForm?.['_id']?.['$id'],
                        id: idSubForm,
                        path: "inputs",
                        updatePartial : true,
                        value: {}
                    };
                }
                $(this).children().each(function (index,elem) {
                    let key = $(this).attr('data-key');
                    if (key) {
                        itemsChanged.push(key)
                        if(!formInputsfor?.[formIdKey]?.[key]) {
                            formInputsfor[formIdKey][key] = inputData.value
                        }
                        if(formInputsfor?.[formIdKey]?.[key]?.['positions']) {
                            var positionKey = null;
                            if(typeof formInputsfor?.[formIdKey]?.[key]?.['positions'] == 'object' && Object.keys(formInputsfor?.[formIdKey]?.[key]?.['positions']).length > 0) {
                                positionKey = Object.keys(formInputsfor?.[formIdKey]?.[key]?.['positions'])[0];
                            }
                            if(formInputsfor?.[formIdKey]?.[key]?.['positions']?.[coForm_FormWizardParams?.parentForm?.['_id']?.['$id']]) {
                                formInputsfor[formIdKey][key]['positions'][coForm_FormWizardParams?.parentForm?.['_id']?.['$id']] = ""+index
                            } else {
                                formInputsfor[formIdKey][key]['positions'][coForm_FormWizardParams?.parentForm?.['_id']?.['$id']] = ""+index
                            }
                        } else if (formInputsfor?.[formIdKey]?.[key] && (coForm_FormWizardParams.isAap == true || coForm_FormWizardParams.isAap == 'true')) {
                            formInputsfor[formIdKey][key]['positions'] = {
                                [coForm_FormWizardParams?.parentForm?.['_id']?.['$id']] : ""+index
                            }
                        }
                        if(formInputsfor?.[formIdKey]?.[key]?.['position']) {
                            formInputsfor[formIdKey][key]['position'] = ""+index
                        }
                    }
                    itemsChangedObj = JSON.parse(JSON.stringify(formInputsfor[formIdKey]))
                });
                let hasEdited = false;
                if(JSON.stringify(itemsBeforeObj) != JSON.stringify(itemsChangedObj)) {
                    hasEdited = true;
                } else if(JSON.stringify(itemsChangedObj).indexOf('"position":') < 0 && JSON.stringify(itemsChangedObj).indexOf('"positions":') < 0 && JSON.stringify(itemsBefore) != JSON.stringify(itemsChanged)) {
                    hasEdited = true;
                }
                // if (JSON.stringify(itemsBefore) != JSON.stringify(itemsChanged)) {
                    mylog.log("check diff", itemsBeforeObj, itemsChangedObj, itemsChanged)
                if(hasEdited) {
                    if (formIdKey) {
                        itemsChanged.forEach((elem, index) => { 
                            inputsValueToSend.value[elem] = formInputsfor[formIdKey][elem] ? (({ docinputsid, ...cloneObj }) => cloneObj)(formInputsfor[formIdKey][elem]) : inputData.value;
                            // inputsValueToSend.value[elem] = formInputsfor[formIdKey][elem] ? (formInputsfor[formIdKey][elem]) : inputData.value;
                            if(inputsValueToSend.value[elem] && inputsValueToSend.value[elem]['positions']) {
                                var positionKey = null;
                                if(typeof inputsValueToSend.value[elem]['positions'] == 'object' && Object.keys(inputsValueToSend.value[elem]['positions']).length > 0) {
                                    positionKey = Object.keys(inputsValueToSend.value[elem]['positions'])[0];
                                }
                                if(inputsValueToSend.value[elem]['positions'][coForm_FormWizardParams?.parentForm?.['_id']?.['$id']]) {
                                    inputsValueToSend.value[elem]['positions'][coForm_FormWizardParams?.parentForm?.['_id']?.['$id']] = index+1
                                }
                            }
                            if(inputsValueToSend.value[elem] && inputsValueToSend.value[elem]['position']) {
                                inputsValueToSend.value[elem]['position'] = index+1
                            }
                            if(elem == "tags") {
                                dataHelper.path2Value({
                                    collection: inputsValueToSend.collection,
                                    formParentId: inputsValueToSend.formParentId,
                                    id: inputsValueToSend.id,
                                    path: inputsValueToSend.path+"."+elem,
                                    updatePartial : true,
                                    value: inputsValueToSend.value[elem]
                                }, function (params) {
                                });
                                delete inputsValueToSend.value[elem]
                            }
                        })
                        mylog.log("inputsValueToSend",itemsBefore, itemsChanged, inputsValueToSend.value)
                        dataHelper.path2Value(inputsValueToSend, function (params) {
                            if(Object.values(inputData).every(o => o == null)) {
                                itemsBefore.length = 0;
                                // itemsBefore = Array.from(itemsChanged);
                                toastr.success('Déplacé avec succès');
                                // itemsBefore = Array.from(itemsChanged);
                                coForm_FormWizardParams.steps[idSubForm].inputs = JSON.parse(JSON.stringify(itemsChangedObj))
                            } else {
                                if (inputData.value.type == "tpls.forms.cplx.openDynform") {
                                    delete inputData.path;
                                    delete inputData.id;
                                    inputData.value = {
                                        id: `${formIdKey}${inputCt}`,
                                        name: inputData.value.label,
                                        type: "dynform"
                                    };
                                    mylog.log("create openDynform save inputData", inputData);
                                    dataHelper.path2Value(inputData, function(params) {
                                        toastr.success('Question ajouté avec succès');
                                        // reloadWizard();
                                        if(typeof reloadInput != "undefined" && typeof stepObj != "undefined") {
                                            reloadInput(inputCt, formIdKey, () => stepObj.bindEvent.inputEvents(stepObj))
                                            formSorted.find(".DnDPlaceholder").remove();
                                        }
                                    });
            
                                } else {
                                    if(inputData.value.type == "tpls.forms.cplx.multiCheckbox" || inputData.value.type == "tpls.forms.cplx.radioNew" || inputData.value.type == "tpls.forms.cplx.checkboxNew") {
                                        myKunik = inputData.value.type.split('.')[3]+inputData.inputId;
                                        sectionDyf[myKunik+'ParamsData'] = {"list" : []};
                                        sectionDyf[myKunik+'Params'] = {
                                            "jsonSchema" : {	
                                                "title" : myKunik+" config",
                                                "icon" : "fa-cog",
                                                "properties" : {
                                                    list : {
                                                        inputType : "array",
                                                        label : tradForm.checkboxList,
                                                        values :  null,
                                                        init : function() {
                                                            $(`<input type="text" class="form-control copyConfig" placeholder="vous pouvez copier le liste ici, séparé par des virgules; ou utiliser le button ajout ci-dessous"/>`).insertBefore('.listarray .inputs.array');
                                                            $(".copyConfig").off().on("blur", function() {
                                                                let textVal = $(this).val().length > 0 ? $(this).val().split(",") : [];
                                                                textVal.forEach((el, index) => {
                                                                    dyFObj.init.addfield('.listarray', el, 'list')
                                                                });
                                                                $(this).val('')
                                                            })
                                                        }
                                                    }
                                                },
                                                save : function () {  
                                                    tplCtx.value = {};
                                                    $.each( sectionDyf[myKunik+'Params'].jsonSchema.properties , function(k,val) { 
                                                        tplCtx.value[k] = getArray('.'+k+val.inputType);
                                                    });
                                                    mylog.log("save tplCtx",tplCtx);
                                    
                                                    if(tplCtx.value.list.length == 0) {
                                                        toastr.error(tradDynForm["This field is required."]);
                                                        $('#btn-submit-form').removeAttr('disabled');
                                                        $('#btn-submit-form').empty();
                                                        $('#btn-submit-form').append(tradDynForm.submit+`<i class="fa fa-arrow-circle-right"></i>`)
                                                    }
                                                    else {
                                                        dataHelper.path2Value( tplCtx, function(params) { 
                                                            $("#ajax-modal").modal('hide');
                                                            if(typeof reloadInput != "undefined" && typeof stepObj != "undefined") {
                                                                reloadInput(formIdKey+inputCt, formIdKey, () => stepObj.bindEvent.inputEvents(stepObj))
                                                                formSorted.find(".DnDPlaceholder").remove();
                                                            }
                                                        } );
                                                    }
                                    
                                                },
                                            }
                                        };
                                        tplCtx.id = coForm_FormWizardParams?.parentForm?.['_id']?.['$id'];
                                        // tplCtx.collection = coForm_FormWizardParams.isAap == true || coForm_FormWizardParams.isAap == 'true' ? inputData.collection : 'forms';
                                        tplCtx.collection = 'forms';
                                        tplCtx.path = "params."+myKunik;
                                        tplCtx.formParentId = coForm_FormWizardParams?.parentForm?.['_id']?.['$id'];
                                        dyFObj.openForm( sectionDyf[myKunik+'Params'],null, sectionDyf[myKunik+'ParamsData']);
                                        $('#ajax-modal').on('hidden.bs.modal', function (e) {
                                            if(typeof tplCtx.value != 'undefined' && tplCtx.value.list.length == 0 && myKunik) {
                                                myKunik = undefined;
                                                if(typeof reloadInput != "undefined" && typeof stepObj != "undefined") {
                                                    reloadInput(formIdKey+inputCt, formIdKey, () => stepObj.bindEvent.inputEvents(stepObj))
                                                    formSorted.find(".DnDPlaceholder").remove();
                                                }
                                            }
                                        });
                                    } else {
                                        toastr.success('Question ajouté avec succès');
                                        // reloadWizard();
                                        // urlCtrl.loadByHash(location.hash);
                                        if(typeof reloadInput != "undefined" && typeof stepObj != "undefined") {
                                            reloadInput(formIdKey+inputCt, formIdKey, () => stepObj.bindEvent.inputEvents(stepObj));
                                            formSorted.find(".DnDPlaceholder").remove();
                                        }
                                    }
                                }
                                if(coForm_FormWizardParams.steps?.[idSubForm]?.inputs != undefined) {
                                    coForm_FormWizardParams.steps[idSubForm].inputs[formIdKey+inputCt] = inputData.value
                                }
                            }
                        });
                    }
                }
            });
        }
    }
}
/* $( ".questionDraggable" ).on( "drag", function( event, ui ) {
    $(this).addClass('ondrag')
} ); */

$('.searchInputCoform').keyup(function(){
    
    var that = this, $allListElements = $('#inputsCoformList > li');

    var $matchingListElements = $allListElements.filter(function(i, li){
        var listItemText = $(li).text().toUpperCase(), searchText = that.value.toUpperCase();
        return ~listItemText.indexOf(searchText);
    });
    
    $allListElements.hide();
    $matchingListElements.show();
    
});

$('.searchBtnCoform').click(function(){
    
    var that = $(".searchInputCoform"), $allListElements = $('#inputsCoformList > li');

    var $matchingListElements = $allListElements.filter(function(i, li){
        var listItemText = $(li).text().toUpperCase(), searchText = that.val().toUpperCase();
        return ~listItemText.indexOf(searchText);
    });
    
    $allListElements.hide();
    $matchingListElements.show();
    
});