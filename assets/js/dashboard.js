 var ocecoform = {
      allData : {},

      tiles : {
        // "nombrePropValidee" : {
        //     type : "html",
        //     datapath : "validFinal.valid",
        //     getandsetDatafunc : function(ocecoform){
        //       var returnObj = countReducedData(allData, "validFinal.valid", "pathtoattribute");
        //       if(typeof returnObj.validated !== "undefined"){
        //           ocecoform.arData["nombrePropValidee"] = returnObj.validated;
        //       }
        //     },
        // },
        "financementgraph" : {
          type : "barchart",
          datapath : "",
          getandsetDatafunc : function(ocecoform){
              var totalfi = sumReducedData(sumReduceData(allData, "financer.amount", "roottoarray"));

              var totaldepense = sumReducedData(sumReduceData(allData, "price", "root"));

              var reste = totaldepense - totalfi;

              var fibarChartData = [
                  {
                      label: 'Financé',
                      backgroundColor: "rgba(0, 0, 0, 1)",
                      borderColor: "rgba(0, 0, 0, 1)",
                      data: [totalfi]
                  }, {
                      label: 'Total à financer',
                      backgroundColor: "rgba(46, 204, 113, 1)",
                      borderColor: "rgba(46, 204, 113, 1)",
                      data: [totaldepense]
                  },
                  {
                      label: 'Reste',
                      backgroundColor: "rgba(221, 221, 221, 1)",
                      borderColor : 'rgba(221, 221, 221, 1)',
                      data: [reste]
                  }
              ];

              var fibarChartDataLabels = ["Total financement", "financé", "Reste"];

              arData['fibarChartData'] = {
                  labels : fibarChartDataLabels,
                  datasets : fibarChartData
              };
          }
        }
      },

      arData : {
        "nombrePropValidee" : 0,
      },

      init : function(pInit = null){
          var copyFilters = jQuery.extend(true, {}, formObj);
          copyFilters.initVar(pInit);
          return copyFilters;
      },

      initvalues : function(ocecoform){
          $.each( ocecoform.arData , function( dataId, dataValue ) {
              if (typeof ocecoform.arData[dataId] !== null) {
                 ocecoform.tiles[dataId].getandsetDatafunc(ocecoform);
              }
          });
      },

      initviews : function(ocecoform){
          $.each( ocecoform.tiles , function( tilesId, tilesValue ) {
              if (typeof ocecoform.arData[tilesId] !== null) {
                  if(tilesValue.type == "html"){
                      $("#"+tilesId).html(ocecoform.arData[tilesId]);
                  } else if (tilesValue.type == "html") {
                      ajaxPost('#fibarChart', baseUrl+'/graph/co/dash/g/graph.views.co.ocecoform.bar'+"/id/fibarChart", null, function(){},"html");
                  }
              } 
          });
      },

      updateviewData : function(arData, chart){
        if (typeof tiles[chart] !== "undefined" && arData[chart]){
            if (typeof tiles[chart]["type"] == "html" ) {
                $('#'.chart).html(arData[chart]);
            }
        }
      },

      countReducedData : function(allD,datakey,type){
                  var r;
        if (type == "root") {

            r = allD.reduce(function(sums,entry){
               sums[entry[datakey]] = (sums[entry[datakey]] || 0) + 1;
               return sums;
            },{});
            return r;

        } else if (type == "pathtoattribute"){

            var str = datakey.split(".");
            r = allD.reduce(function(sums,entry){
              if (typeof entry[str[0]] !== "undefined" && typeof entry[str[0]][str[1]] !== "undefined") {
                    sums[entry[str[0]][str[1]]] = (sums[entry[str[0]][str[1]]] || 0) + 1;
                }
               return sums;
            },{});
            return r;

        } else if (type == "pathtoattribu"){

        }
      },

      sumReduceData : function(allD,datakey,type){
        if (type == "root") {

            r = allD.reduce(function(sums,entry){
                if (entry[datakey] != "") {
                    sums[entry[datakey]] = (sums[entry[datakey]] || 0) + 1;
                }
               return sums;
            },{});
            return r;

        } else if (type == "pathtoattribute"){

            var str = datakey.split(".");
            r = allD.reduce(function(sums,entry){
              if (typeof entry[str[0]] !== "undefined" && typeof entry[str[0]][str[1]] !== "undefined") {
                    sums[entry[str[0]][str[1]]] = (sums[entry[str[0]][str[1]]] || 0) + 1;
                }
               return sums;
            },{});
            return r;

        } else if (type == "roottoarray"){

            r = allD.reduce(function(sums,entry){
                var str = datakey.split(".");
                if (typeof entry[str[0]] !== "undefined") {
                    sums2 = sumReduceData(entry[str[0]], str[1], "root");
                    sums = sumObjectsByKey(sums,sums2);
                }
               return sums;
            },{});
            return r;

        }
      },

      sumObjectsByKey : function(...objs) {
        return objs.reduce((a, b) => {
            for (let k in b) {
                if (b.hasOwnProperty(k))
                    a[k] = (a[k] || 0) + b[k];
            }
        return a;
        }, {});
      },

      havAtleastOne : function(countReducedData, objkey){
          if (objkey in countReducedData)
            return true;
      },

      cumulReduceData: function(sumReducedData, datakey){

        sumReducedData.reduce(function(accumulator, entry){
          // return entry
        });
      },

      sumReductedData: function(Data){
          var sum = 0;
          $.each( Data , function( dataId, dataValue ) {
                sum = sum + ( parseInt(dataId) * dataValue);
          });
      }

};
