/*
#todo change color when filtered or sorted

*/
var financementlogObj = {
    var : {
        containerId : "#financementTable",
        containerFilteredId : "#financementFilTable",
        containerStatId : "#financementStat",
        fiterBtnId : "#fiterBtnGroup",
        answerId : "",
        formId : "",
        stepFinancerId : "",
        data : [],
        groupData : [],
        dataparams : {
            'date du financement' : {
                read : date => date?.sec ? new Date(date.sec* 1000 ).toLocaleDateString() : "",
                editable : false,
            },
            'statut' : {
                //read : statut => statut != undefined && statut == "paid" ? "payée" : "en attente de payement",
                read : function (status){
                    if(status == "pending"){
                        return "en attente de payement";
                    }else if(status == "paid"){
                        return "payée";
                    }else if(status == "distributed"){
                        return "rédistribué"
                    }
                },
                headerClasses : 'filter-select',
                editable : false,
            },
            'data-answerid' : {
                read : id => typeof id == 'object' ? id.$id : id
            },
            'financeur' : {
                headerClasses : 'filter-select',
                editable : false,
            },
            'commun' : {
                headerClasses : 'filter-select editable',
                editable : true,
            }
        },
        params : {
            mode : 'read',
            isAdmin : false,
            canEdit : false
        },
        headers : {},
        tableHeader : {}
    },
    action : {
        init : function (pInit = null) {
            var copyFilters = jQuery.extend(true, {}, financementlogObj);
            return copyFilters;
        },
        initVar : function (financementlogObj, data){
            var transformed = Object.values(data["tableAnswers"]);
            financementlogObj.var.formId = data["form"];
            financementlogObj.var.stepFinancerId = data["stepFinancerId"];
            financementlogObj.action.applyTransformationsWithParams(financementlogObj,transformed);
            financementlogObj.var.data = transformed;

            var transformedGrp = Object.values(data["tableFinancersFiltered"]);
            financementlogObj.action.applyTransformationsWithParams(financementlogObj,transformedGrp);
            financementlogObj.var.groupData = transformedGrp;

            var tableStat = Object.values(data["tableStat"]);
            financementlogObj.action.applyTransformationsWithParams(financementlogObj,tableStat);
            financementlogObj.var.stat = tableStat;

            if(typeof financementlogObj.var.data[0] != "undefined") {
                financementlogObj.var.headers[Object.keys(financementlogObj.var.data[0]).indexOf('date du financement')] = {sorter: 'date'};
                financementlogObj.var.tableHeader = Object.keys(financementlogObj.var.data[0]);
            }
            if(notEmpty(data.autorisation)){
                Object.keys(data.autorisation).forEach(function (key) {
                    financementlogObj.var.params[key] = data.autorisation[key];
                });
            }/*
            financementlogObj.var.enteteobj = data["enteteobj"];*/
        },
        initBtnGroup: function (financementlogObj){
            $(financementlogObj.var.fiterBtnId).append(financementlogObj.view.tableloading(financementlogObj));
            if($('#switchTable').length <= 0) {
                $(financementlogObj.var.fiterBtnId).append(financementlogObj.view.switchtablebtn(financementlogObj));
            }
            if($('#resetTable').length <= 0) {
                $(financementlogObj.var.fiterBtnId).append(financementlogObj.view.resetsortfilterbtn(financementlogObj));
            }
            if($('#downloadCsv').length <= 0) {
                $(financementlogObj.var.fiterBtnId).append(financementlogObj.view.downloaddropdown(financementlogObj));
            }
            if($('#headerselectbtn').length <= 0) {
                $(financementlogObj.var.fiterBtnId).append(financementlogObj.view.headerselect(financementlogObj));
            }
            //$(financementlogObj.var.fiterBtnId).append(financementlogObj.view.toogleeditbtn(financementlogObj));
        },
        initSelect2Sortable : function (financementlogObj , enteteobj , onChange) {
            const $select = $("#select2v4");
            let selectedOrder = enteteobj.selectedOrder ;
            const lockedFirst = enteteobj.lockedFirst;
            const lockedLast = enteteobj.lockedLast;

            if ($.fn.select2) {
                $('#monSelect').select2('destroy');
                delete $.fn.select2;
            }

            $.getScript("/plugins/select2/select2v4/select2v4.min.js", function () {
                $('#select2v4').select2({
                    dropdownParent: $select.parent(),
                    data: enteteobj.options,
                    multiple: true,
                    width: '100%'
                });

                $select.val(selectedOrder).trigger("change.select2");

                function updateDisplayOrder() {
                    $(".select2-selection__choice").each((index, element) => {
                        const text = $(element).attr("title");
                        const id = enteteobj.options.find(opt => opt.text === text)?.id;
                        if (id === lockedFirst) {
                            $(element).addClass("locked").html(`<span>🔒</span> ${text}`);
                        } else if (lockedLast.includes(id)) {
                            $(element).addClass("locked").html(`${text} <span>🔒</span>`);
                        } else {
                            $(element).html(`<span class="drag-handle">≡</span> ${index + 1}. ${text}`);
                        }
                    });
                }

                function initSortable() {
                    const selectionContainer = document.querySelector(".select2-selection__rendered");
                    if (!selectionContainer) return;

                    Sortable.create(selectionContainer, {
                        animation: 150,
                        filter: '.select2-search__field',
                        onEnd: function (evt) {
                            let sortedValues = Array.from(selectionContainer.children)
                                .filter(el => el.classList.contains("select2-selection__choice"))
                                .map(el => $(el).attr("title"));

                            let sortedIds = sortedValues.map(text => {
                                return enteteobj.options.find(opt => opt.text === text)?.id;
                            }).filter(id => id !== undefined);

                            sortedIds = sortedIds.filter(id => id !== lockedFirst);
                            sortedIds.unshift(lockedFirst);

                            lockedLast.forEach(id => {
                                sortedIds = sortedIds.filter(i => i !== id);
                                sortedIds.push(id);
                            });

                            selectedOrder = sortedIds;
                            $select.val(sortedIds).trigger("change.select2");
                            setTimeout(() => {
                                updateDisplayOrder();
                                onChange(sortedIds);
                            }, 100);
                        }
                    });
                }

                $select.on("select2:select select2:unselect", function (e) {
                    const id = e.params.data.id;
                    if (id === lockedFirst || lockedLast.includes(id)) {
                        $select.val(selectedOrder).trigger("change.select2");
                        return;
                    }
                    setTimeout(() => {
                        updateDisplayOrder();
                        initSortable();
                    }, 100);
                });

                $select.on("select2:open", function () {
                    setTimeout(initSortable, 100);
                });

                // Initialisation du tri
                initSortable();
                updateDisplayOrder();
            });
        },
        initTable: function (financementlogObj,dataArray,container = null ){
            var $tableHtml = financementlogObj.action.generateTable(financementlogObj,dataArray);
            if(container == null){
                container = financementlogObj.var.containerId;
            }
            $(container).html($tableHtml);

            financementlogObj.action.actualisefinancerlog(financementlogObj, container);

            $.tablesorter.addParser({
                id: 'date', // Identifiant du parser
                is: function(s) {
                    return false; // ne pas tester, nous allons toujours trier
                },
                format: function(s) {
                    // Convertir la date de format dd/mm/yyyy à un objet Date
                    var parts = s.split('/');
                    // Note: parts[2] est l'année, parts[1] est le mois (0-indexé), parts[0] est le jour
                    return new Date(parts[2], parts[1] - 1, parts[0]).getTime();
                },
                type: 'numeric' // Type de tri
            });

            /*$.tablesorter.themes.bootstrap = {
                // these classes are added to the table. To see other table classes available,
                // look here: http://getbootstrap.com/css/#tables
                table        : 'table table-bordered table-striped',
                caption      : 'caption',
                // header class names
                header       : 'bootstrap-header', // give the header a gradient background (theme.bootstrap_2.css)
                sortNone     : '',
                sortAsc      : '',
                sortDesc     : '',
                active       : '', // applied when column is sorted
                hover        : '', // custom css required - a defined bootstrap style may not override other classes
                // icon class names
                icons        : '', // add "bootstrap-icon-white" to make them white; this icon class is added to the <i> in the header
                iconSortNone : 'bootstrap-icon-unsorted', // class name added to icon when column is not sorted
                iconSortAsc  : 'glyphicon glyphicon-chevron-up', // class name added to icon when column has ascending sort
                iconSortDesc : 'glyphicon glyphicon-chevron-down', // class name added to icon when column has descending sort
                filterRow    : '', // filter row class; use widgetOptions.filter_cssFilter for the input/select element
                footerRow    : '',
                footerCells  : '',
                even         : '', // even row zebra striping
                odd          : ''  // odd row zebra striping
            };
    */
            $(`${container} table`).bind("filterStart" , function (e,t) {
                $('.infofiltre').show();
            }).bind("sortStart" , function (e,t) {
                $('.infofiltre').show();
            }).bind("filterEnd" , function (e,t) {
                financementlogObj.action.actualisefinancerlog(financementlogObj , container);
                $('.infofiltre').hide();
            }).bind("sortEnd" , function (e,t) {
                financementlogObj.action.unsetRowSpanFin(financementlogObj , container);
                $('.infofiltre').hide();
            })
                .tablesorter({
                    // this will apply the bootstrap theme if "uitheme" widget is included
                    // the widgetOptions.uitheme is no longer required to be set
                    theme : "ice",

                    widthFixed: true,

                    headerTemplate : '{content} {icon}', // new in v2.7. Needed to add the bootstrap icon!

                    // widget code contained in the jquery.tablesorter.widgets.js file
                    // use the zebra stripe widget if you plan on hiding any rows (filter widget)
                    widgets : [ "uitheme", "filter", "columns", "zebra" , "print" ],

                    widgetOptions : {
                        columns: [ "primary", "secondary", "tertiary" ],
                        print_now        : true,
                        filter_reset : ".reset",
                        filter_cssFilter: "form-control",
                        filter_childRows  : true,
                        filter_columnFilters   : true,
                        headerTitle_useAria    : true,
                        filter_functions: {
                            2: {

                            }
                        },


                    },

                    headers : financementlogObj.var.headers,

                    on_filters_loaded: function(o){
                        o.SetFilterValue(paramCol, paramCriteria);
                        o.Filter();
                    },



                });
            //5555
            $('td.distpayement').each(function() {
                var parent = $(this);
                var dropdown = `<div class="dropdown">
                                        <a class="dropdown-toggle coremudrbtn coremubtnerror dropdown-toggle" type="button"
                                           id="dropdownMenu1" data-toggle="dropdown">
                                            payée
                                            <i class="fa fa-add-user"></i> <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu dropdowntablebtn" role="menu" aria-labelledby="dropdownMenu1">
                                            <li role="presentation"><a data-depenseid="${parent.data('depenseid')}" data-answerid="${parent.data('answerid')}" data-financeid="${parent.data('financeid')}"  class="redistline">Rédistribuer</a></li>
                                        </ul>
                                    </div>`;

                // Sélectionner l'option actuelle
                //dropdown.val(currentCountry);

                // Remplacer le texte par le dropdown
                $(this).html(dropdown);
            });

            $('td.distcommun').each(function() {
                var parent = $(this);
                var dropdown = `<a target="_blank" class="" href="${baseUrl}/costum/co/index/slug/${costum.slug}#detail-un-commun.communId.${parent.data('answerid')}">
                                            ${parent.text()}
                                        </a>`;

                $(this).html(dropdown);
            });

            $(container+' td.distfinftl').each(function() {
                var parent = $(this);
                var dropdown = `${parent.text()} `;
                if(($(parent).text() == "" || $(parent).text() == " ") && $(this).data('financertype') == "tl"){
                    dropdown += `<button class="btn btn-small financerlogsmbtn activedoublbtn pull-right"><i class="fa fa-copy"></i></button>`;
                }
                else if($(parent).text() != "" && $(parent).text() != " "){
                    dropdown += `<button class="btn btn-small financerlogsmbtn desactivedoublbtn pull-right"><i class="fa fa-times"></i></button>`;
                }

                $(this).html(dropdown);
            });

            $('td.distfinanceur').each(function() {
                var parent = $(this);
                const originaltext = parent.text();
                var specialcolor = '';
                if(notEmpty($(this).data('financertype')) && $(this).data('financertype') == "tl"){
                    specialcolor = "tlcolor";
                }

                var needbtn = '';
                if(notEmpty($(this).data('needbtn')) && ($(this).data('needbtn') == "true" || $(this).data('needbtn') == true)){
                    needbtn = `<button class="btn btn-small needftlbtn pull-right"><span>Définir comme tl</span></button>`;
                }

                const dropdown = `<a href="#page.type.organizations.id.${parent.data('financeurid')}" class="${specialcolor} parent-link lbh-preview-element"> ${originaltext} </a> ${needbtn}`;

                // Remplacer le texte par le dropdown
                if($(this).html() == originaltext) {
                    // if(this.querySelector('button') == null) {
                    $(this).html(dropdown);
                }
            });

            coInterface.bindLBHLinks();
        },
        setRowSpanFin: function (tableId, col) {
            var rowSpanMapff0 = {};
            var rowSpanMapff1 = {};
            var rowSpanMapffB = {};
            var rowSpanMapffB2 = {};
            var rowSpanMapf = {};

            $(tableId).find('tr').each(function () {
                if (col == 0){
                    var valueOfTheSpannableCell0 = $($(this).children('td')[0]).text();
                    $($(this).children('td')[0]).attr('data-original-value0', valueOfTheSpannableCell0.replace(/"/g, "&quot;"));
                    rowSpanMapff0[valueOfTheSpannableCell0] = true;
                }else {

                    var valueOfTheSpannableCell1 = $($(this).children('td')[1]).text();
                    $($(this).children('td')[1]).attr('data-original-value1', valueOfTheSpannableCell1);
                    rowSpanMapff1[valueOfTheSpannableCell1] = true;

                    if (col != 1) {
                        var valueOfTheSpannableCellBefore = $($(this).children('td')[col - 1]).text();
                        $($(this).children('td')[col]).attr('data-original-valueB', valueOfTheSpannableCellBefore);
                        rowSpanMapffB[valueOfTheSpannableCellBefore] = true;

                        var valueOfTheSpannableCell = $($(this).children('td')[col]).text();
                        $($(this).children('td')[col]).attr('data-original-value' + col, valueOfTheSpannableCell);
                        rowSpanMapf[valueOfTheSpannableCell] = true;
                    }
                }

            });

            if(col == 0) {
                $.each(rowSpanMapff0 , function(row, rowGroup) {
                    var $cellsToSpan0 = $(tableId+` td[data-original-value0="${row.replace(/"/g, "&quot;")}"]:visible`);
                    var numberOfRowsToSpan0 = $cellsToSpan0.length;
                    $cellsToSpan0.each(function (index) {
                        if (index == 0) {
                            $(this).attr('data-rs', numberOfRowsToSpan0);
                            $(this).attr('rowspan', numberOfRowsToSpan0);
                        } else {
                            $(this).attr('data-rs', 'hh');
                            $(this).hide();
                        }
                    });
                });
            }else if(col == 1){
                $.each(rowSpanMapff1 , function(row, rowGroup) {
                    var $cellsToSpan1 = $(tableId+` td[data-original-value1="${row.replace(/"/g, "&quot;")}"]:visible`);
                    var numberOfRowsToSpan1 = $cellsToSpan1.length;
                    $cellsToSpan1.each(function (index) {
                        if (index == 0) {
                            $(this).attr('data-rs', numberOfRowsToSpan1);
                            $(this).attr('rowspan', numberOfRowsToSpan1);
                        } else {
                            $(this).attr('data-rs', 'hh');
                            $(this).hide();
                        }
                    });
                });
            }else{
                $.each(rowSpanMapffB , function(rowB, rowGroupB) {
                    $.each(rowSpanMapf , function(row, rowGroup) {
                        var $cellsToSpan = $(tableId+` td[data-original-valueB="${rowB.replace(/"/g, "&quot;")}"][data-original-value${col}="${row.replace(/"/g, "&quot;")}"]:visible`);
                        var numberOfRowsToSpan = $cellsToSpan.length;
                        $cellsToSpan.each(function(index){
                            if(index==0){
                                $(this).attr('rowspan', numberOfRowsToSpan);
                            }else{
                                $(this).hide();
                            }
                        });
                    });
                });
            }

        },
        generateTable: function(financementlogObj,dataArray){
            if (!Array.isArray(dataArray) || dataArray.length === 0) {
                return ''; // Retourner une chaîne vide si le tableau d'objets est vide ou non valide
            }

            // Créer la table
            var $table = $('<table>').addClass('generated-table');
            var $thead = $('<thead>');
            var $tbody = $('<tbody>');

            // Créer l'en-tête du tableau (les clés des objets deviennent des th avec des classes si spécifiées)
            var $trHead = $('<tr>');
            var keys = Object.keys(dataArray[0]);

            keys.forEach(function (key) {
                if (!key.startsWith('data-')) { // On ignore les clés qui commencent par 'data-'
                    var $th = $('<th>').text(key); // Crée l'élément <th> pour chaque clé

                    $th.attr('data-column', key);
                    // Vérifie si la clé existe dans l'objet `headerClasses` et applique les classes correspondantes
                    if (financementlogObj.var.dataparams[key] && financementlogObj.var.dataparams[key].headerClasses) {
                        $th.addClass(financementlogObj.var.dataparams[key].headerClasses);
                    }

                    $trHead.append($th);
                }
            });

            $thead.append($trHead);
            $table.append($thead);

            // Créer les lignes du tableau (corps)
            dataArray.forEach(function (item) {
                var $trBody = $('<tr>');
                $trBody.attr('data-statut', item['statut']);
                keys.forEach(function (key) {
                    if (!key.startsWith('data-')) { // Ignorer les clés 'data-' dans les cellules
                        var $td = $('<td>').html(item[key] || '');

                        $td.attr('data-column', key);

                        if(item[key] == "payée"){
                            $td.addClass('distpayement');
                        }
                        if(key == "commun"){
                            $td.addClass('distcommun');
                        }

                        if(key == "financements FTL"){
                            $td.addClass('distfinftl');
                        }

                        if(key == "financement FTL" && item[key] !== ''){
                            $td.attr('contenteditable',true);
                            $td.addClass('distfinftl_editable');
                        }

                        if(key == "financeur"){
                            $td.addClass('distfinanceur');
                        }

                        var attr = {};
                        // Ajouter les attributs data-* aux cellules correspondantes
                        Object.keys(item).forEach(function (dataKey) {
                            if (dataKey.startsWith('data-')) {
                                $td.attr(dataKey, item[dataKey]); // Appliquer les data-* à la cellule
                                if(dataKey = "data-needbtn" && (item[dataKey] == "true" || item[dataKey] == "true")){
                                    $td.addClass('needftlbtn');
                                }
                            }
                        });

                        //Ajouter les groupes de boutons
                        if(typeof userId != "undefined" && key == 'poste de depense' && (item["data-user"] == userId || financementlogObj.var.params.isAdmin)){
                            //$td.append(financementlogObj.view.actionbtn(financementlogObj , item));
                        }

                        $trBody.append($td); // Ajouter la cellule à la ligne
                    }
                });

                $tbody.append($trBody); // Ajouter la ligne au tbody
            });

            $table.append($tbody);

            // Retourner la table HTML en tant qu'élément jQuery
            return $table;
        },
        actualisefinancerlog : function(financementlogObj , container = null){
            if(container == null){
                container = financementlogObj.var.containerId
            }
            var loaderPromise = $.Deferred();
            financementlogObj.action.unsetRowSpanFin(financementlogObj, container);
            loaderPromise.resolve();
            $.when(loaderPromise).then(function() {
                if ((container == '#financementTable' || container == '#financementFilTable') && notEmpty(financementlogObj.var.tableHeader) && financementlogObj.var.tableHeader.indexOf('commun') != -1) {
                    financementlogObj.action.setRowSpanFin(`${container} table`, financementlogObj.var.tableHeader.indexOf('commun'));
                }

                if (container == '#financementTable' && typeof financementlogObj.var.tableHeader != "undefined" && notEmpty(financementlogObj.var.tableHeader) && financementlogObj.var.tableHeader.indexOf("poste de depense") != -1) {
                    financementlogObj.action.setRowSpanFin(`${container} table`, financementlogObj.var.tableHeader.indexOf("poste de depense"));
                }

                if (container == '#financementTable' && typeof financementlogObj.var.tableHeader != "undefined" && notEmpty(financementlogObj.var.tableHeader) && financementlogObj.var.tableHeader.indexOf("depense") != -1) {
                    financementlogObj.action.setRowSpanFin(`${container} table`, financementlogObj.var.tableHeader.indexOf("depense"));
                }


                /*var restr = ["financeur" , "financement" ,"financements FTL"];
                if (container == '#financementFilTable' && typeof financementlogObj.var.tableHeader != "undefined" && notEmpty(financementlogObj.var.tableHeader)) {
                    financementlogObj.action.setRowSpanFin(`${container} table`, financementlogObj.var.enteteobj.selectedOrder.indexOf("commun"));
                    $.each(financementlogObj.var.enteteobj.selectedOrder, function (i,head){
                        if (restr.indexOf(head) == -1){
                            //financementlogObj.action.setRowSpanFin(`${container} table`, financementlogObj.var.enteteobj.selectedOrder.indexOf(head));
                        }
                    });
                }*/
            });
        },
        unsetRowSpanFin : function(financementlogObj, container = null){
            if(container == null){
                container = financementlogObj.var.containerId
            };
            $(`${container} table td`).show();
            $(`${container} table td`).attr('rowspan',false);
            return true;
        },
        applyTransformationsWithParams : function(financementlogObj,arrayOfObjects) {
            arrayOfObjects.forEach(obj => {
                Object.keys(obj).forEach(key => {
                    if (financementlogObj.var.dataparams.hasOwnProperty(key) && typeof financementlogObj.var.dataparams[key].read != "undefined") {
                        // Appliquer la transformation spécifiée dans `params` si elle existe
                        obj[key] = financementlogObj.var.dataparams[key].read(obj[key], obj);
                    }
                });
            });
        },
        loadData  : function(financementlogObj , answer= null,form=null){
            if(!notEmpty(answer)){
                answer = financementlogObj.var.answerId;
            }

            if(!notEmpty(form)){
                form = financementlogObj.var.formId;
            }

            //coInterface.showLoader(financementlogObj.var.containerId);
            coInterface.showCostumLoader(financementlogObj.var.containerId);
            coInterface.showCostumLoader(financementlogObj.var.containerFilteredId);

            linkparams = "";
            if(notEmpty(form)) {
                linkparams ="form/"+form;
            }else if(notEmpty(answer)){
                linkparams ="answer/"+answer;
            }
            ajaxPost("", baseUrl + '/survey/form/financerlogs/' + linkparams, {}, function (data) {
                // var loaderPromise = $.Deferred();
                financementlogObj.action.initVar(financementlogObj, data );
                // loaderPromise.resolve();

                // $.when(loaderPromise).then(function() {
                financementlogObj.action.initBtnGroup(financementlogObj);

                /*financementlogObj.action.initSelect2Sortable(financementlogObj , financementlogObj.var.enteteobj , selectedSortedValues => {
                    console.log(selectedSortedValues);
                });*/

                financementlogObj.action.initTable(
                    financementlogObj,
                    financementlogObj.var.groupData,
                    financementlogObj.var.containerFilteredId
                );
                financementlogObj.action.initTable(
                    financementlogObj,
                    financementlogObj.var.data,
                    financementlogObj.var.containerId
                );
                financementlogObj.action.initTable(
                    financementlogObj,
                    financementlogObj.var.stat,
                    financementlogObj.var.containerStatId
                );
                financementlogObj.event.onTableLoad(financementlogObj);
                // });
            }, null, "json", {
                async: true
            });
        },
        turnOnEditMode : function(financementlogObj){
            if(financementlogObj.var.params.isAdmin) {
                $('.tableDL-entete-action').addClass('modeEdit');
            }else if(financementlogObj.var.params.canEdit){
                $(`.tableDL-entete-action[data-user="${userId}"]`).addClass('modeEdit');
                $(`.tableDL-entete-action[data-canedit="${userId}"]`).addClass('modeEdit');
            }
        },
        turnOffEditMode : function(financementlogObj){
            $('.tableDL-entete-action').removeClass('modeEdit');
        }
    },
    view : {
        actionbtn:function (financementlogObj,attr) {
            var attribut = '';
            $.each(attr, function (index,value){
                if(index.includes("data-")){
                    attribut += ` ${index} = "${value}" `;
                }
            });
            html = '<div class="tableDL-entete-action">';
            html = `<div class="tableDL-entete-action">
                         <!--<button ${attribut} class="btn depbtn-tab depmodal" data-content="modifier">
                                <i class="fa fa fa-pencil"></i>
                         </button>
                         <button ${attribut} class="btn depbtn-tab depmodal" data-content="com">
                                <i class="fa fa fa-comment"></i>
                         </button>
                         <button ${attribut} class="btn depbtn-tab depmodal" data-content="del">
                                <i class="fa fa fa-trash"></i>
                         </button> -->`;
            /*if(typeof attr.include != "undefined" && attr.include == false ) {
                html += `<button ${attribut} class="btn depbtn-tab depmodal" data-content="show">
                                <i class="fa fa fa-eye"></i> Montrer
                         </button>`;
            }else{
                html += `<button data-uid="0" class="btn depbtn-tab depmodal" data-content="hide">
                                <i class="fa fa fa-eye-slash"></i> Cacher
                         </button>`;*/
            //}
            html += `</div>`;

            return html;
        },
        editbtn: function (financementlogObj) {
            var html = '';
            html = `<button id="toogleeditbtn" class="btn margin-right-20" data-type="read"><i class="fa fa-lock"></i> modification verrouillé</button>`;
            return html;
        },
        toogleeditbtn: function (financementlogObj) {
            var html = '';
            if(financementlogObj.var.params.canEdit){
                html = `<button id="toogleeditbtn" class="btn margin-right-20" data-type="read"><i class="fa fa-lock"></i> modification verrouillé</button>`;
            }
            return html;
        },
        toogleeditbtnlabel: function (financementlogObj) {
            var html = '';
            if(financementlogObj.var.params.mode == "read"){
                html = `<i class="fa fa-lock"></i> modification verrouillé`;
            }else{
                html = `<i class="fa fa-unlock"></i> mode édition`;
            }
            return html;
        },
        resetsortfilterbtn: function (financementlogObj) {
            var html = '';
            html = `
            <div class="">
                <button id="resetTable" class="btn margin-right-20 financerlogbtn">Réinitialiser le tri et les filtres</button>
            </div>`;
            return html;
        },
        downloaddropdown: function (financementlogObj) {
            var html = '';
            html = `<div class="dropdown pull-right">
                        <a class="dropdown-toggle btn financerlogbtn" type="button"
                           id="menucsvdownload" data-toggle="dropdown">
                            Télécharger
                            <i class="fa fa-add-user"></i> <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdowntablebtn" role="menu" aria-labelledby="menucsvdownload">
                            ${financementlogObj.view.downloadcsvbtn(financementlogObj)}
                            ${financementlogObj.view.downloadcsvallbtn(financementlogObj)}
                            ${financementlogObj.view.downloadcsvalldbtn(financementlogObj)}
                            ${financementlogObj.view.downloadcsvallsbtn(financementlogObj)}
                        </ul>
                    </div>`;
            return html;
        },
        downloadcsvbtn: function (financementlogObj) {
            var html = '';
            html = `
                      <li role="presentation"><a id="downloadCsv" class="">Télécharger le tableau des rédistribution</a></li>
                   `;
            return html;
        },
        downloadcsvallbtn: function (financementlogObj) {
            var html = '';
            html = `
                      <li role="presentation"><a id="downloadCsvall" class="">Télécharger le tableau groupé par commun en csv</a></li>
                   `;
            return html;
        },
        downloadcsvalldbtn: function (financementlogObj) {
            var html = '';
            html = `
                      <li role="presentation"><a id="downloadCsvdall" class="">Télécharger le tableau détaillé en csv</a></li>
                   `;
            return html;
        },
        downloadcsvallsbtn: function (financementlogObj) {
            var html = '';
            html = `
                      <li role="presentation"><a id="downloadCsvsall" class="">Télécharger le tableau des statistiques en csv</a></li>
                   `;
            return html;
        },
        switchtablebtn: function (financementlogObj) {
            var html = '';
            html = `<div class="btn-group margin-right-20">
                        <button data-container="financementFilTable" class="switchTable btn financerlogbtn active"><i class="fa fa-trello"></i> <span>financement groupé</span></button>
                        <button data-container="financementTable" class="switchTable btn financerlogbtn"><i class="fa fa-list"></i> <span>financement détailé</span></button>
                        <button data-container="financementStat" class="switchTable btn financerlogbtn"><i class="fa fa-sort-numeric-asc"></i> <span>statistique</span></button>
                    </div>`;
            return html;
        },
        tableloading: function (financementlogObj) {
            var html = '';
            html = `<h3 class="infofiltre" style="display: none">Chargement...</h3>`;
            return html;
        },
        headerselect: function (financementlogObj) {
            var html = '';
            html = `
            <!--<div class="">
                <button id="headerselectbtn" class="btn margin-right-20 financerlogbtn"><i class="fa fa-cog"></i>  Réorganiser l'entête</button>
            </div>-->`;
            return html;
        },
    },
    event : {
        onTableLoad : function (financementlogObj) {
            financementlogObj.event.initResetsortfilterbtnEvent(financementlogObj);
            financementlogObj.event.initToogleeditbtnEvent(financementlogObj);
            financementlogObj.event.initActionEvent(financementlogObj);
        },
        initResetsortfilterbtnEvent : function (financementlogObj) {
            $('#resetTable').click(function() {
                $("#financementFilTable table").trigger('filterReset').trigger('sortReset');
                financementlogObj.action.actualisefinancerlog(financementlogObj , '#financementFilTable');
                return false;
            });
        },
        initActionEvent : function (financementlogObj) {
            $('.depmodal[data-content="show"]').off().on('click' , function (){
                var depenseIndex = $(this).data('depenseid');
                var params = {
                    id: $(this).data('answerid'),
                    collection: 'answers',
                    path: 'answers.aapStep1.depense.' + depenseIndex + '.include',
                    value: 'false',
                    setType : 'bool'
                }
                $.confirm({
                    title: "Inclure ce ligne de depense et ses financements",
                    content: `Cette action va inclure ce ligne de depense et ses financements dans les données pris en compte par la plateforme`,
                    buttons: {
                        yes: {
                            text: 'Inclure',
                            btnClass: 'btn btn-danger',
                            action: function() {
                                mylog.log(params, '_response')
                                dataHelper.path2Value(params, function(__response) {
                                    if (__response.result && __response.result === true) {
                                       financementlogObj.action.loadData(financementlogObj);
                                    }
                                });
                            }
                        },
                        no: {
                            text: coTranslate('cancel'),
                            btnClass: 'btn btn-default'
                        }
                    }
                });
            });

            $('.depmodal[data-content="hide"]').off().on('click' , function (){

                var depenseIndex = $(this).data('depenseid');
                var params = {
                    id: $(this).data('answerid'),
                    collection: 'answers',
                    path: 'answers.aapStep1.depense.' + depenseIndex + '.include',
                    value: 'true',
                    setType : 'bool'
                }
                $.confirm({
                    title: "Cacher momentanément ce ligne de depense et ses financements",
                    content: `Cette action va cacher momentanément ce ligne de depense et ses financements dans les données pris en compte par la plateforme."`,
                    buttons: {
                        yes: {
                            text: 'Cacher',
                            btnClass: 'btn btn-danger',
                            action: function() {
                                mylog.log(params, '_response')
                                dataHelper.path2Value(params, function(__response) {
                                    if (__response.result && __response.result === true) {
                                        financementlogObj.action.loadData(financementlogObj);
                                    }
                                });
                            }
                        },
                        no: {
                            text: coTranslate('cancel'),
                            btnClass: 'btn btn-default'
                        }
                    }
                });
            });

            $('.redistline').off().on('click' , function (){
                var answerIndex = $(this).data('answerid');
                var financerIndex = $(this).data('financeid');
                var depenseIndex = $(this).data('depenseid');
                var params = {
                    id: answerIndex,
                    collection: 'answers',
                    path: 'answers.aapStep1.depense.' + depenseIndex + '.financer.'+ financerIndex + '.status',
                    value: 'distributed'
                }
                $.confirm({
                    title: "Ré-distribuer le montant",
                    content: `Ré-distribuer le montant pour ce financement`,
                    buttons: {
                        yes: {
                            text: 'Ré-distribuer',
                            btnClass: 'btn btn-danger',
                            action: function() {
                                mylog.log(params, '_response')
                                dataHelper.path2Value(params, function(__response) {
                                    if (__response.result && __response.result === true) {
                                        //financementlogObj.action.loadData(financementlogObj);
                                        $(`td[data-answerid='${answerIndex}'][data-financeid='${financerIndex}'][data-depenseid='${depenseIndex}']:nth-last-child(1)`).html('rédistribué');
                                    }
                                });
                            }
                        },
                        no: {
                            text: coTranslate('cancel'),
                            btnClass: 'btn btn-default'
                        }
                    }
                });
            });

            $('.depmodal[data-content="com"]').off().on('click' , function () {
                $('#modal-preview-comment').addClass('comment-modal-aac-financement')
                var thisbtn = $(this);
                commentObj.openPreview('forms', financementlogObj.var.formId, `${financementlogObj.var.formId}.${financementlogObj.var.stepFinancerId}.${financementlogObj.var.answerId}.financer.question_financer.comment.financement.depensekey.aapStep1-depense.depenseindex.${thisbtn.data('uid')}`, 'Commentaire')
                commentObj.closePreview = function(){
                    $('#modal-preview-comment').removeClass('comment-modal-aac-financement')
                    $(".main-container").off();
                    $("#modal-preview-comment").css("display", "none");
                }
                // commentObj.openPreview('answers',answerObj._id.$id,thisbtn.data('uid'), 'aapStep1','null','null',{notCloseOpenModal : true})
            });

            $('#downloadCsv').on('click', function() {
                //$(financementlogObj.var.containerId + ' table').trigger('outputTable');
                let csvContent = "data:text/csv;charset=utf-8,";

                var total = 0;
                var commmun = "";
                var soustotal = 0;
                $(financementlogObj.var.containerId + ' table thead tr').each(function(index) {
                    const row = [];

                    $(this).find('th').each(function() {
                        if(!$(this).text().includes("Rédistribuer") && (index == 0 || index == 1 || index == 3 || index == 5)) {
                            row.push($(this).text().toUpperCase());
                        }
                    });
                    csvContent += row.join(";") + "\n";
                });

                $(financementlogObj.var.containerId + ' table tbody tr[data-statut="payée"]').each(function(ind) {
                    const row = [];
                    var extrarow = "";

                    if(ind == 0){
                        commmun = $(this).find('td').first().text();
                    }
                    if(ind != 0 && (commmun != $(this).find('td').first().text() || (ind + 1) == $(financementlogObj.var.containerId + ' table tbody tr[data-statut="payée"]').length)){
                        soustotal += parseInt($(this).find('td').eq(5).text());
                        total += parseInt($(this).find('td').eq(5).text());
                        extrarow = commmun+";sous-total;;"+soustotal+ "\n";
                        commmun = $(this).find('td').first().text();
                    }else{
                        soustotal += parseInt($(this).find('td').eq(5).text());
                        total += parseInt($(this).find('td').eq(5).text());
                    }

                    $(this).find('td').each(function(index) {
                        if(!$(this).text().includes("Rédistribuer") && (index == 0 || index == 1 || index == 3 || index == 5)) {
                            row.push($(this).text());
                        }
                    });
                    csvContent += row.join(";") + "\n";
                    csvContent += extrarow;
                });
                csvContent += "Total;;;"+total;

                const encodedUri = encodeURI(csvContent);
                const link = document.createElement("a");
                link.setAttribute("href", encodedUri);
                link.setAttribute("download", "redistribution_"+new Date().toLocaleDateString()+".csv");
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            });

            $('#downloadCsvall').on('click', function() {
                let csvContent = "data:text/csv;charset=utf-8,";

                csvContent += "commun;financement;financements FTL;financeur;etat;email\n";
                let totalFinancement = 0;
                let totalFinancementFTL = 0;

                financementlogObj.var.groupData.forEach(item => {
                    let row = [];
                    let financement = item.financement || 0;
                    let financementFTL = item["financements FTL"] || 0;

                    totalFinancement += financement;
                    totalFinancementFTL += financementFTL;

                    row.push(item.commun, financement, financementFTL, item.financeur, item.etat, item.email);
                    csvContent += row.join(";") + "\n";
                });
                csvContent += "Total;;"+totalFinancement+ ";"+totalFinancementFTL;

                const encodedUri = encodeURI(csvContent);
                const link = document.createElement("a");
                link.setAttribute("href", encodedUri);
                link.setAttribute("download", "commun_"+new Date().toLocaleDateString()+".csv");
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            });

            $('#downloadCsvdall').on('click', function() {
                let csvContent = "data:text/csv;charset=utf-8,";

                csvContent += "commun;date du financement;depense;financement FTL;financement TL;financeur;poste de depense;statut\n";
                let totalFinancement = 0;
                let totalFinancementFTL = 0;

                financementlogObj.var.data.forEach(item => {
                    let row = [];
                    let financement = item["financement TL"] || 0;
                    let financementFTL = item["financements FTL"] || 0;

                    totalFinancement += financement;
                    totalFinancementFTL += financementFTL;

                    row.push(item.commun , item["date du financement"] , item.depense , financementFTL , financement , item.financeur , item["poste de depense"] , item.statut);
                    csvContent += row.join(";") + "\n";
                });
                csvContent += "Total;;"+totalFinancement+ ";"+totalFinancementFTL;

                const encodedUri = encodeURI(csvContent);
                const link = document.createElement("a");
                link.setAttribute("href", encodedUri);
                link.setAttribute("download", "financement_"+new Date().toLocaleDateString()+".csv");
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            });

            $('#downloadCsvsall').on('click', function() {
                let csvContent = "data:text/csv;charset=utf-8,";

                csvContent += "commun;financement citoyen;financement ftl;financement organisation;financement tl;financement total;nb compte qui ont visité;nb contributeurs;nb tiers-lieux;nb votes\n";
                let totalFinancement = 0;
                let totalFinancementFTL = 0;

                financementlogObj.var.stat.forEach(item => {
                    let row = [];
                    let financement = item["financement total"] || 0;
                    row.push(item["commun"],item["financement citoyen"],item["financement ftl"],item["financement organisation"],item["financement tl"],item["financement total"],item["nb compte qui ont visité"],item["nb contributeurs"],item["nb tiers-lieux"],item["nb votes"]);
                    csvContent += row.join(";") + "\n";
                });

                const encodedUri = encodeURI(csvContent);
                const link = document.createElement("a");
                link.setAttribute("href", encodedUri);
                link.setAttribute("download", "stat_"+new Date().toLocaleDateString()+".csv");
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            });

            $('.switchTable').on('click', function(){
                var thisbtn = $(this);

                $('.switchTable').removeClass('active');
                thisbtn.addClass('active');

                $('.financertable').hide('fast');
                $('#'+thisbtn.data('container')).show('fast');
            });

            $('#headerselectbtn').on('click', function(){
                var thisbtn = $(this);
                $('#headerselect').toggle("slow");
            });

            $('.financerlogsmbtn').on('click', function(){
                var thisbtn = $(this);
                var activate = true ;
                var financeurid = thisbtn.parent().data('financeurid');
                var answerid = thisbtn.parent().data('answerid');
                var dialog = 'Voulez-vous activer le financement FTL pour ce tiers-lieux';
                if(thisbtn.hasClass('desactivedoublbtn')){
                    activate = false;
                    dialog = 'Voulez-vous enlever le financement FTL pour ce tiers-lieux';
                }

                bootbox.dialog({
                    title: 'Financement FTL',
                    //message: "<span class='text-red bold'><i class='fa fa-warning'></i> "+trad.actionirreversible+"</span>",
                    message : dialog,
                    buttons: [
                        {
                            label: "Ok",
                            className: "btn btn-primary pull-left",
                            callback: function() {
                                var loaderPromise = $.Deferred();

                                coInterface.showCostumLoader(financementlogObj.var.containerId);
                                coInterface.showCostumLoader(financementlogObj.var.containerFilteredId);

                                loaderPromise.resolve();
                                $.when(loaderPromise).then(function() {
                                    ajaxPost("", baseUrl + '/survey/form/duplicatefinance/',
                                        {
                                            answer : answerid,
                                            financeurid : financeurid,
                                            action : activate
                                        },
                                        function (data) {
                                            toastr.success("Effectué");
                                            financementlogObj.action.loadData(financementlogObj);
                                        }, null, "json");
                                });
                            }
                        },
                        {
                            label: "Annuler",
                            className: "btn btn-default pull-left",
                            callback: function() {}
                        }
                    ]
                });
            });

            $('.needftlbtn').on('click', function(){
                var thisbtn = $(this);
                var activate = true ;
                var financeurid = thisbtn.parent().data('financeurid');
                var answerid = thisbtn.parent().data('answerid');

                bootbox.dialog({
                    title: "Changer le type de financeur",
                    message: "Voulez-vous vraiment définir le type du financeur en tant que 'tiers-lieu' au lieu de simple organisation de communecter ? Cette action va permettre à ce financement de béneficier le cofinancement FTL",
                    buttons: [
                        {
                            label: "Ok",
                            className: "btn btn-primary pull-left",
                            callback: function() {
                                var loaderPromise = $.Deferred();
                                //coInterface.showLoader(financementlogObj.var.containerFilteredId);

                                coInterface.showCostumLoader(financementlogObj.var.containerId);
                                coInterface.showCostumLoader(financementlogObj.var.containerFilteredId);
                                loaderPromise.resolve();
                                $.when(loaderPromise).then(function() {
                                    ajaxPost("", baseUrl + '/survey/form/changefintype/',
                                        {
                                            answer : answerid,
                                            financeurid : financeurid,
                                            action : activate
                                        },
                                        function (data) {
                                            toastr.success("Effectué");
                                            financementlogObj.action.loadData(financementlogObj);
                                        }, null, "json");
                                });
                            }
                        },
                        {
                            label: "Annuler",
                            className: "btn btn-default pull-left",
                            callback: function() {}
                        }
                    ]
                });
            });

            $('.distfinftl_editable').on('blur', function(){
                var thisbtn = $(this);

                var financeurid = thisbtn.data('financeftlid');
                var answerid = thisbtn.data('answerid');
                var depenseid = thisbtn.data('depenseid');

                var formQ = {
                    value : thisbtn.html(),
                    collection : "answers",
                    id : answerid,
                    path : "answers.aapStep1.depense."+depenseid+".financer."+financeurid+".amount"
                };

                if(thisbtn.html() != "") {
                    dataHelper.path2Value(formQ, function (params) {
                        toastr.success("Modifié");
                    });
                }
            });

            /*$('.depmodal[data-content="delete"]').off().on("click",function() {
                var thisbtn = $(this);
                bootbox.dialog({
                    title: trad.confirmdelete,
                    message: "<span class='text-red bold'><i class='fa fa-warning'></i> "+trad.actionirreversible+"</span>",
                    buttons: [
                        {
                            label: "Ok",
                            className: "btn btn-primary pull-left",
                            callback: function() {
                                var formQ = {
                                    value:null,
                                    formParentId : answerObj.form,
                                    collection : "answers",
                                    id : answerObj._id.$id,
                                    pull : "answers.aapStep1.depense",
                                    path : "answers.aapStep1.depense."+thisbtn.data('uid')
                                };

                                if (typeof parentfId != "undefined") {
                                    formQ["formParentId"] = parentfId;
                                }

                                dataHelper.path2Value( formQ , function(params) {
                                    showLoader('#question_financer');
                                    showLoader('#question_depense');
                                    reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>" );
                                } );
                            }
                        },
                        {
                            label: "Annuler",
                            className: "btn btn-default pull-left",
                            callback: function() {}
                        }
                    ]
                });
            });

            $('.depmodal[data-content="edit"]').off().on("click",function() {
                var thisbtn = $(this);
                tplCtx = {
                    id : answerObj._id.$id ,
                    collection : "answers",
                    form : "aapStep1",
                    updatePartial: true,
                    setType : [
                        {
                            "path": "price",
                            "type": "int"
                        },
                        {
                            "path": "date",
                            "type": "isoDate"
                        }
                    ]
                }

                delete tplCtx.arrayForm;
                tplCtx.path = "answers.aapStep1.depense." + thisbtn.data('uid');
                if (answerObj?.answers?.aapStep1?.depense?.[thisbtn.data('uid')]?.imageKey && newdepenseDataImg[answerObj?.answers?.aapStep1?.depense?.[thisbtn.data('uid')]?.imageKey]) {
                    var currentdepenseImg = JSON.parse(JSON.stringify(newdepenseDataImg[answerObj?.answers?.aapStep1?.depense?.[thisbtn.data('uid')]?.imageKey]))
                    currentdepenseImg.title ? delete currentdepenseImg.title : "";
                    currentdepenseImg.subKey ? delete currentdepenseImg.subKey : "";
                    currentdepenseImg.metaData ? delete currentdepenseImg.metaData : "";

                    sectionDyf.depenseDyf.jsonSchema.properties.image.initList = [currentdepenseImg];
                    tplCtx.lastKeygen = currentdepenseImg.imageKey;
                } else if (sectionDyf.depenseDyf.jsonSchema.properties.image.initList) {
                    delete sectionDyf.depenseDyf.jsonSchema.properties.image.initList
                }
                dyFObj.openForm( sectionDyf.depenseDyf,null, answerObj.answers.aapStep1.depense[thisbtn.data('uid')],null,null,{
                    type : "bootbox",
                    notCloseOpenModal : true,
                });
            });*/
        },
        initToogleeditbtnEvent : function (financementlogObj) {
            $('#toogleeditbtn').click(function() {
                var thisbtn = $(this);
                if(thisbtn.data('type') == 'read'){
                    thisbtn.data('type' , 'edit');
                    $(financementlogObj.var.containerId + ' table').addClass('editmode');
                    //financementlogObj.var.params.mode = 'edit';
                    //financementlogObj.action.turnOnEditMode(financementlogObj);
                }else{
                    thisbtn.data('type' , 'read');
                    $(financementlogObj.var.containerId + ' table').removeClass('editmode');
                    //financementlogObj.var.params.mode = 'read';
                    //financementlogObj.action.turnOffEditMode(financementlogObj);
                }
                thisbtn.html(financementlogObj.view.toogleeditbtnlabel(financementlogObj));
            });
        }
    }
}