$(document).ready(function () {
    var currentlyScrolling = false;

    var SCROLL_AREA_HEIGHT = 40;
    $(".questionDraggable").draggable({
        connectToSortable: ".questionList",
        cursor: "move",
        cursorAt: {left: 5},
        helper: "clone",
        revert: "invalid",
        zIndex: 2,
        scroll: true
    });

    if (typeof canEditForm != "undefined" && canEditForm) {
        $("#questionList" + formKey).off("sortable").sortable({
            cursor: 'move',
            cursorAt: {top: 5},
            handle: 'h4',
            delay: 150,
            grid: [20, 10],
            placeholder: "buildBlockPlaceholder",
            scroll: true,
            start: function(e, ui) {
                ui.placeholder.height(ui.item.height());
                ui.placeholder.width(ui.item[0].offsetWidth);
            },
            sort: function(event, ui) {
                if (currentlyScrolling) {
                    return;
                }
                var windowHeight   = $(window).height();
                var mouseYPosition = event.clientY;

                if (mouseYPosition < SCROLL_AREA_HEIGHT) {
                    currentlyScrolling = true;

                    $('html, body').animate({
                    scrollTop: "-=" + windowHeight / 2 + "px" // Scroll up half of window height.
                    }, 
                    400, // 400ms animation.
                    function() {
                    currentlyScrolling = false;
                    });

                } else if (mouseYPosition > (windowHeight - SCROLL_AREA_HEIGHT)) {

                    currentlyScrolling = true;
            
                    $('html, body').animate({
                    scrollTop: "+=" + windowHeight / 2 + "px" // Scroll down half of window height.
                    }, 
                    400, // 400ms animation.
                    function() {
                    currentlyScrolling = false;
                    });
            
                }
            },
        });
    }
    /*$ (".questionDraggable").on("dragstop", function(event, ui) {
        mylog.log("drag stoped")
    }) */
});

let itemsChanged = [];
let itemsBefore = [];

/* $( ".questionDraggable" ).on( "drag", function( event, ui ) {
    $(this).addClass('ondrag')
} ); */

if(typeof formKey !="undefined" && typeof formIsCoform != "undefined"){
    $("#questionList" + formKey).off("sortstop").on("sortstop", function (event, ui) {
        let formIdKey = $(this).attr('data-id');
        let idSubForm = $(this).attr('data-ids');
        let inputData = {};
        let inputCt = "";
        let formSorted = $(this);
        if (ui.item.hasClass("questionDraggable")) {
            inputCt = Date.now().toString(36) + Math.random().toString(36).substring(2);
            let label = (`${ui.item.text()}`.replace(/[\n\r]|\r/g, '')).trim();
            let index = -1;
            if(formIsCoform == true || formIsCoform == 'true') {
                inputData = {
                    id: idSubForm,
                    key: undefined,
                    formParentId: idParentForm,
                    collection: "forms",
                    path: `inputs.${formIdKey}${inputCt}`,
                    inputId: `${formIdKey}${inputCt}`,
                    value: {
                        label: index = label.indexOf(":") > -1 ? label.substring(index +1) : label,
                        position : 1,
                        type: ui.item.attr("data-type"),
                    }
                }
            } else if (formIsCoform == false || formIsCoform == 'false'){
                inputData = {
                    id: idSubForm,
                    key: undefined,
                    formParentId: idParentForm,
                    collection: "inputs",
                    path: `inputs.${formIdKey}${inputCt}`,
                    inputId: `${formIdKey}${inputCt}`,
                    value: {
                        label: index = label.replace(/:/g, ' '),
                        placeholder : '',
                        info : '',
                        parentid : idParentForm,
                        positions : {
                            [idParentForm] : 1
                        },
                        type: ui.item.attr("data-type"),
                    }
                }
            }
            mylog.log("input added", inputData);
            ui.item.replaceWith(`
                <li id="question${formIdKey}${inputCt}" class="col-xs-12 no-padding questionBlock ui-sortable-handle" data-key="${formIdKey}${inputCt}" draggable="false" role="option" aria-grabbed="false"></li>
            `);
        }
        
        itemsChanged = [];
        if(itemsBefore.length == 0) {
            itemsBefore = formInputsfor[formIdKey] ? Object.keys(formInputsfor[formIdKey]) : null
        }
        let inputsValueToSend = {
            collection: "forms",
            formParentId: idParentForm,
            id: idSubForm,
            path: "inputs",
            value: {}
        };
        if(formIsCoform == false || formIsCoform == 'false') {
            inputsValueToSend = {
                collection: "inputs",
                formParentId: idParentForm,
                id: idSubForm,
                path: "inputs",
                value: {}
            };
        }
        $(this).children().each(function () {
            let key = $(this).attr('data-key');
            if (key) itemsChanged.push(key)
        });
        if (JSON.stringify(itemsBefore) != JSON.stringify(itemsChanged)) {
            if (formIdKey) {
                itemsChanged.forEach((elem, index) => { 
                    inputsValueToSend.value[elem] = formInputsfor[formIdKey][elem] ? (({ docinputsid, ...cloneObj }) => cloneObj)(formInputsfor[formIdKey][elem]) : inputData.value;
                    // inputsValueToSend.value[elem] = formInputsfor[formIdKey][elem] ? (formInputsfor[formIdKey][elem]) : inputData.value;
                    if(inputsValueToSend.value[elem] && inputsValueToSend.value[elem]['positions']) {
                        var positionKey = null;
                        if(typeof inputsValueToSend.value[elem]['positions'] == 'object' && Object.keys(inputsValueToSend.value[elem]['positions']).length > 0) {
                            positionKey = Object.keys(inputsValueToSend.value[elem]['positions'])[0];
                        }
                        if(inputsValueToSend.value[elem]['positions'][idParentForm]) {
                            inputsValueToSend.value[elem]['positions'][idParentForm] = index+1
                        }
                    }
                    // if(inputsValueToSend.value[elem] && inputsValueToSend.value[elem]['position']) {
                    if(formIsCoform == true || formIsCoform == 'true') {
                        inputsValueToSend.value[elem]['position'] = index+1
                    }
                })
                dataHelper.path2Value(inputsValueToSend, function (params) {
                    if(Object.values(inputData).every(o => o == null)) {
                        itemsBefore.length = 0;
                        itemsBefore = Array.from(itemsChanged);
                        toastr.success('Déplacé avec succès');
                        itemsBefore = Array.from(itemsChanged);
                    } else {
                        if (inputData.value.type == "tpls.forms.cplx.openDynform") {
                            delete inputData.path;
                            delete inputData.id;
                            inputData.value = {
                                id: `${formIdKey}${inputCt}`,
                                name: inputData.value.label,
                                type: "dynform"
                            };
                            mylog.log("create openDynform save inputData", inputData);
                            dataHelper.path2Value(inputData, function(params) {
                                toastr.success('Question ajouté avec succès');
                                // reloadWizard();
                                // if(typeof reloadInput != "undefined" && typeof stepObj != "undefined") {
                                if(typeof reloadInput != "undefined") {
                                    reloadInput(inputCt, formIdKey, () => {
                                        if(typeof bindBtnEditEvent != "undefined")
                                            bindBtnEditEvent()
                                    }, true, {position : inputData.value.position, idSubForm : idSubForm})
                                    formSorted.find(".DnDPlaceholder").remove();
                                }
                            });
    
                        } else {
                            if(inputData.value.type == "tpls.forms.cplx.multiCheckbox" || inputData.value.type == "tpls.forms.cplx.radioNew" || inputData.value.type == "tpls.forms.cplx.checkboxNew") {
                                myKunik = inputData.value.type.split('.')[3]+inputData.inputId;
                                sectionDyf[myKunik+'ParamsData'] = {"list" : []};
                                sectionDyf[myKunik+'Params'] = {
                                    "jsonSchema" : {	
                                        "title" : myKunik+" config",
                                        "icon" : "fa-cog",
                                        "properties" : {
                                            list : {
                                                inputType : "array",
                                                label : tradForm.checkboxList,
                                                values :  null,
                                                init : function() {
                                                    $(`<input type="text" class="form-control copyConfig" placeholder="vous pouvez copier le liste ici, séparé par des virgules; ou utiliser le button ajout ci-dessous"/>`).insertBefore('.listarray .inputs.array');
                                                    $(".copyConfig").off().on("blur", function() {
                                                        let textVal = $(this).val().length > 0 ? $(this).val().split(",") : [];
                                                        textVal.forEach((el, index) => {
                                                            dyFObj.init.addfield('.listarray', el, 'list')
                                                        });
                                                        $(this).val('')
                                                    })
                                                }
                                            }
                                        },
                                        save : function () {  
                                            tplCtx.value = {};
                                            $.each( sectionDyf[myKunik+'Params'].jsonSchema.properties , function(k,val) { 
                                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                                            });
                                            mylog.log("save tplCtx",tplCtx);
                            
                                            if(tplCtx.value.list.length == 0) {
                                                toastr.error(tradDynForm["This field is required."]);
                                                $('#btn-submit-form').removeAttr('disabled');
                                                $('#btn-submit-form').empty();
                                                $('#btn-submit-form').append(tradDynForm.submit+`<i class="fa fa-arrow-circle-right"></i>`)
                                            }
                                            else {
                                                dataHelper.path2Value( tplCtx, function(params) { 
                                                    $("#ajax-modal").modal('hide');
                                                    urlCtrl.loadByHash(location.hash);
                                                } );
                                            }
                            
                                        },
                                    }
                                };
                                tplCtx.id = idParentForm;
                                tplCtx.collection = formIsCoform == true || formIsCoform == 'true' ? inputData.collection : 'forms';
                                tplCtx.path = "params."+myKunik;
                                tplCtx.formParentId = idParentForm;
                                dyFObj.openForm( sectionDyf[myKunik+'Params'],null, sectionDyf[myKunik+'ParamsData']);
                                $('#ajax-modal').on('hidden.bs.modal', function (e) {
                                    if(typeof tplCtx.value != 'undefined' && tplCtx.value.list.length == 0 && myKunik) {
                                        myKunik = undefined;
                                        reloadWizard();
                                    }
                                });
                            } else {
                                toastr.success('Question ajouté avec succès');
                                // reloadWizard();
                                // urlCtrl.loadByHash(location.hash);
                                if(typeof reloadInput != "undefined") {
                                    reloadInput(`${formIdKey}${inputCt}`, formIdKey, () => {
                                        if(typeof bindBtnEditEvent != "undefined") {
                                            bindBtnEditEvent()
                                        }
                                    }, true, {position : inputData.value.position, idSubForm : idSubForm})
                                    formSorted.find(".DnDPlaceholder").remove();
                                }
                            }
                        }
                        if(formInputsfor[formIdKey] != undefined) {
                            formInputsfor[formIdKey][formIdKey+inputCt] = inputData.value
                        }
                        if(typeof inputsList != "undefined" && inputsList[formIdKey]) {
                            inputsList[formIdKey][formIdKey+inputCt] = inputData.value
                        }
                    }
                });
            }
        }
    });
}

$('.searchInputCoform').keyup(function(){
    
    var that = this, $allListElements = $('#inputsCoformList > li');

    var $matchingListElements = $allListElements.filter(function(i, li){
        var listItemText = $(li).text().toUpperCase(), searchText = that.value.toUpperCase();
        return ~listItemText.indexOf(searchText);
    });
    
    $allListElements.hide();
    $matchingListElements.show();
    
});

$('.searchBtnCoform').click(function(){
    
    var that = $(".searchInputCoform"), $allListElements = $('#inputsCoformList > li');

    var $matchingListElements = $allListElements.filter(function(i, li){
        var listItemText = $(li).text().toUpperCase(), searchText = that.val().toUpperCase();
        return ~listItemText.indexOf(searchText);
    });
    
    $allListElements.hide();
    $matchingListElements.show();
    
});