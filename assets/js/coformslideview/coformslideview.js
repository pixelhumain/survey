var notShowInput = [
    "tpls.forms.cplx.validateStep"
];

var coformSlideObj = {
    initialized : false,
    coformData : {},
    init : function(pInit){
        var copyObj = {};
        if (pInit != null && pInit.parentForm != null && pInit.context != null) {
            copyObj = jQuery.extend(true, {}, coformSlideObj);
            coformSlideObj.initVar(pInit);
            coformSlideObj.views.init(coformSlideObj);
        }
        return copyObj;
    },
    initVar : function(pInit){
        this.coformData = exists(pInit) ? pInit : {};
    },
    views: {
        init : function(coformSlideObj){
            $('.co-popup-slide').append(coformSlideObj.views.createWelcomePanel(coformSlideObj));
            $('.co-popup-slide').append(coformSlideObj.views.createPanel(coformSlideObj));
            if(notEmpty(userConnected)) {
                $('.slide-financer-bg').append(coformSlideObj.views.profilPicDropdown(coformSlideObj));
            }
            coformSlideObj.events.init(coformSlideObj);
            if(notEmpty(userConnected)) {
                var stepinputinfo = coformSlideObj.actions.getStepInputInfo(coformSlideObj, true);
                coformSlideObj.events.loadStep(coformSlideObj, '.swiper-slide.startnew', stepinputinfo.actualStep, stepinputinfo.actualInput, true);

                if (typeof coformSlideObj.coformData.answer.slide != "undefined") {
                    coformSlideObj.events.loadStep(coformSlideObj, '.swiper-slide.resume', coformSlideObj.coformData.answer.slide.step, coformSlideObj.coformData.answer.slide.input, true);
                }
            }
            if(notEmpty(location.href.split('.').indexOf('view'))) {
                $('.main-container').prepend('<input class="coformslideradio hide" type="checkbox">');
            }
            $('body').off().on('keydown' ,  function (e){
                if (e.which === 13 || e.which === 39){
                    if ($(".ap-slide-next").attr('disabled') != 'disabled') {
                        $(".ap-slide-next").trigger('click');
                    }
                    if(e.which === 13 && $('.ap-starter-btn-email').length > 0 && $('.ap-starter-btn-email').val() != ""){
                        $('.ap-starter-btn-email').trigger('click');
                    }
                } else if (e.which === 37) {
                    if ($(".ap-slide-prev").attr('disabled') != 'disabled') {
                        $(".ap-slide-prev").trigger('click');
                    }
                }
            });
        },
        createWelcomePanel : function(coformSlideObj){
            if( notEmpty(coformSlideObj.coformData)) {
                return `
                    <div class="co-popup-slide-content">
                        <div class="co-popup-slide-header">
                            <div class="co-popup-slide-header-logo margin-bottom-20">
                                <img class="logo-info" src="${typeof coformSlideObj.coformData.context.profilThumbImageUrl != "undefined" ? baseUrl + coformSlideObj.coformData.context.profilThumbImageUrl : baseUrl + defaultImage}">
                            </div>
                            <span class="co-popup-slide-header-h2 co-popup-slide-title margin-top"> Bienvenue sur </span>
                            <h2 class="co-popup-slide-header-h2 co-popup-slide-title aap-primary-color"> ${ typeof coformSlideObj.coformData.parentForm.name != "undefined" ? coformSlideObj.coformData.parentForm.name : ""} </h2> 
                            <div class="formDescrdiv"><p class="co-popup-preconfig-header-p formDescr"> ${ typeof coformSlideObj.coformData.parentForm.description != "undefined" ? coformSlideObj.coformData.parentForm.description : ""} </p></div>
                        </div>
                        ${coformSlideObj.views.createStartNavButton(coformSlideObj)}
                    </div>
                `;
            } else {
                return coformSlideObj.views.dataLoaderError(coformSlideObj);
            }
        },
        createStartNavButton : function(coformSlideObj) {
            var returnHtml = `<div class="co-popup-slide-header-action margin-top-20">`;
            if (notEmpty(userConnected) && coformSlideObj.coformData.canSeeAnswer && coformSlideObj.coformData.isOpenToTempAcount){
                returnHtml +=  `<ul class="co-popup-slide-actions">`;
                returnHtml +=  `<li class="li-start-slide">
                                        <a href="javascript:;" class="ap-starter-btn-start-slide">
                                            Démarrer
                                        </a>
                                   </li>
                                `;
                if(typeof coformSlideObj.coformData.answer.slide != "undefined"){

                    returnHtml +=  `<li class="li-resume-slide">
                                        <a href="javascript:;" class="ap-starter-btn-resume">
                                        <span class="resume1" >Reprendre</span>
                                        <span class="resume1" ><small >Etape ${coformSlideObj.coformData.answer.slide.step != false ? coformSlideObj.coformData.parentForm.subForms.indexOf(coformSlideObj.coformData.answer.slide.step) + 1 : ""} question ${ coformSlideObj.coformData.answer.slide.input != false ? Object.keys(Object.values(coformSlideObj.coformData.steps)[ coformSlideObj.coformData.parentForm.subForms.indexOf(coformSlideObj.coformData.answer.slide.step) ].inputs).indexOf(coformSlideObj.coformData.answer.slide.input) + 1 : ""} </small> </span>
                                        </a>
                                   </li>
                                `;
                }
                returnHtml +=  `</ul>`;
            }else{
                var conxErrrInfo = "";
                var emailBtn = "";
                var connectBtn = "";
                if(coformSlideObj.coformData.parentForm.temporarymembercanreply) {
                    conxErrrInfo = "Pour repondre à ce formulaire. Veuillez vous connecter à votre compte ou entrer votre adresse e-mail, qui va recevoir une invitation à rejoindre la plateforme";
                    connectBtn =`
                        <li class="li-start-slide">
                            <a href="javascript:;" class="ap-starter-btn-connect" data-target="#modalLogin" data-toggle="modal" data-dismiss="modal"><span><i class="fa fa-sign-in" aria-hidden="true"></i></span> <span class="connect-label"> Se connecter </span> </a>
                            <input class="inputemail form-control" id="name" type="text" />
                        </li> `;
                    emailBtn = `<li class="li-abord-slide"><a href="javascript:;" class="ap-starter-btn-email" data-type="toogle"> Répondre avec un email </a></li>`;
                } else {
                    connectBtn = `<li class="li-start-slide"><a href="javascript:;" class="ap-starter-btn-connect" data-target="#modalLogin" data-toggle="modal" data-dismiss="modal"><span><i
                        class="fa fa-sign-in" aria-hidden="true"></i></span><span class="connect-label"> Se connecter </span></a></li>`;
                    conxErrrInfo = "Pour repondre à ce formulaire, veuillez vous connecter à votre compte";
                }

                if(!coformSlideObj.coformData.isOpenToTempAcount){
                    connectBtn = "";
                    emailBtn = "";
                    conxErrrInfo = "Ce formulaire n'est pas ouvert à de nouvelle réponse";
                }
                returnHtml +=  `
                    <div class="">
                         <label class="padding-10 center"> <i class="fa fa-exclamation-triangle"></i> ${ conxErrrInfo} </label>
                    </div>
                    <ul class="co-popup-slide-actions">
                                ${ connectBtn }
                                ${ emailBtn }
                    </ul>
                `;
            }
            returnHtml += `</div>`;
            return returnHtml;
        },
        dataLoaderError : function(coformSlideObj) {
            return  ` Data error `;
        },
        createEmailInput : function(coformSlideObj) {
            return `<div class="input-group">
                        <input type="text" class="inputemail" placeholder="Votre adresse" name="email" >
                            <span class="input-group-addon"><i class="fa fa-email"></i> </span>
                    </div>`;
        },
        createNavButton : function(coformSlideObj , stepinputinfo) {

            var returnHtml = `<div class="co-popup-slide-formnav margin-top-20" id="nav${stepinputinfo.actualInput}">`;
            if(stepinputinfo.prevInput != false) {
                returnHtml += `            <li class="li-start-slide prev">
                                                <button href="javascript:;" class="ap-slide-prev prev">
                                                    Precedent
                                                </button>
                                            </li>
                                `;
            }
            if(stepinputinfo.nextInput != false) {
                returnHtml += `            <li class="li-start-slide next">
                                                <button href="javascript:;" class="ap-slide-next next btnnext_${ stepinputinfo.actualInput}"  ${ (stepinputinfo.isRequired ) ?  "disabled" : "" }>
                                                    Suivant
                                                </button>
                                            </li>
                                `;
            }
            returnHtml += `         </div>`;
            return returnHtml;
        },
        createPanel : function(coformSlideObj) {
            var returnHtml = `
            <div class="co-popup-slide-question" style="display : none">
                <div class="co-popup-slide-question-container">
                    <div class="padding-10 co-popup-slide-projectswiper" >
                        <div class="swiper" id="swiper">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide startnew" >
                                </div>
                                <div class="swiper-slide resume" >

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>`;
            return returnHtml;
        },
        profilPicDropdown : function(){
            var profilPic = "";
            if(notEmpty(userConnected) && notEmpty(userConnected.profilImageUrl)){
                profilPic = userConnected.profilImageUrl;
            } else {
                profilPic = defaultImage;
            }
            return `<div class="pull-right dropleft form-user-profil">
                            <a href="javascript:;" type="button" class="drpdwn pull-right btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src="${baseUrl + profilPic}" alt="${ userConnected.name }" class="custom-fin-img currentUserimg">
                            </a>
                            <div class="dropdown-menu padding-20 border-radius-10">
                                <div class="aap-primary-color center"> email : </div>
                                <div class="center currentUseremail"> ${ userConnected.email } </div>
                                <div class="center"> <a href="javascript:;"  class="next btn btn-aap-secondary center border-radius-20 preconfig-btn-next modifyfinbtn formLogoutBtn" data-nextstep="selectfin" data-actualstep="selectAction" data-next="2"> Se déconnecter </a> </div>
                            </div>
                        </div>`;
        },
        createBreadCrumb : function(coformSlideObj , stepinputinfo) {
            var actualStep = stepinputinfo.actualStep;
            var actualInput= stepinputinfo.actualInput;

            var returnHtml = `<div class="br-container margin-top-20" id="br${actualInput}">`;
                returnHtml += `            <li class=" ">
                                                <div class="">
                                                    <small class="small"> Etape ${coformSlideObj.coformData.parentForm.subForms.indexOf(actualStep) + 1} / ${coformSlideObj.coformData.parentForm.subForms.length} </small>
                                                </div>
                                                <div class="">
                                                    <span class="">${ coformSlideObj.coformData.steps[actualStep].name }</span>
                                                </div>
                                            </li>
                                            <li class="">
                                                <div class="">
                                                    <small class="small"> Question ${Object.keys(Object.values(coformSlideObj.coformData.steps)[ coformSlideObj.coformData.parentForm.subForms.indexOf(actualStep) ].inputs).indexOf(actualInput) + 1} / ${ Object.keys(Object.values(coformSlideObj.coformData.steps)[ coformSlideObj.coformData.parentForm.subForms.indexOf(actualStep) ].inputs).length }</small>
                                                </div>
                                            </li>
                                `;
            returnHtml += `         </div>`;
            return returnHtml;
        },
    },
    events: {
        init: function(coformSlideObj) {

            coformSlideObj.events.btnEvent();
        },
        btnEvent : function() {
            $(".ap-starter-btn-email").off().click(function(){
                if($(this).attr('data-type' ) == "toogle") {
                    if (!$(this).hasClass('send')) {
                        $(this).html('Envoyer');
                        $(this).attr('data-type', 'send');
                        $(".ap-starter-btn-connect").addClass('email');
                        $(".inputemail").addClass('active');
                        $(".inputemail").focus();
                    }
                } else if ($(this).attr('data-type' ) == "send") {
                    if($(".inputemail").val() == ""){
                        $(".inputemail").addClass('error');
                    }else{
                        $this = $(this);
                        var params = {};
                        ajaxPost(
                            null,
                            baseUrl + `/survey/answer/answerwithemail/`,
                            {
                                id : typeof coformSlideObj.coformData.answer._id != "undefined" ? coformSlideObj.coformData.answer._id.$id : "new",
                                form : coformSlideObj.coformData.parentForm._id.$id,
                                email : $(".inputemail").val()
                            },
                            function(res){
                                location.reload();
                            });
                    }

                }
            });

            $(".ap-starter-btn-start-slide").off().click(function(){
                $(".checkbox-slide-start").trigger('click');
                $(".checkbox-slide-start2").trigger('click');
                $('.swiper-slide.resume').remove();

                var stepinputinfo = coformSlideObj.actions.getStepInputInfo(coformSlideObj , true);

                coformSlideObj.coformData.answer.slide = {
                    input : stepinputinfo.actualInput,
                    step   : stepinputinfo.actualStep
                }

                $('.swiper-slide.startnew').attr("id" , "slide"+stepinputinfo.actualInput);
                $('.co-popup-slide-container').append(coformSlideObj.views.createNavButton(coformSlideObj , stepinputinfo));
                $('.co-popup-slide-container').prepend(coformSlideObj.views.createBreadCrumb(coformSlideObj, stepinputinfo));

                mylog.log("eeeeeeee", stepinputinfo);
                $('.swiper-wrapper').append(`<div class="swiper-slide" id="slide${ stepinputinfo.nextInput }" style="display: none"></div>`);
                coformSlideObj.events.loadStep(coformSlideObj , `#slide${ stepinputinfo.nextInput }` , stepinputinfo.nextInputStep , stepinputinfo.nextInput , true);

                coformSlideObj.actions.updateSlideStory(coformSlideObj , stepinputinfo.actualStep , stepinputinfo.actualInput , function(){});

                coformSlideObj.events.btnEvent();
                setTimeout( function() {
                    $('.co-popup-slide-content').hide();
                    $('.co-popup-slide-question').show();
                    coformSlideObj.events.afterload();
                }, "700");
            });

            $(".formLogoutBtn").off().on("click", function(){
                url=baseUrl+'/'+moduleId+'/person/logout';
                ajaxPost(null, url,
                    null,
                    function(data){
                        location.reload();
                    });
            });

            $('.inputemail').off().on("blur", function(){
                $(this).removeClass('error');
            });

            $('.ap-starter-btn-resume').off().on("click", function(){
                $(".checkbox-slide-start").trigger('click');
                $(".checkbox-slide-start2").trigger('click');
                $('.swiper-slide.startnew').remove();
                setTimeout( function() {
                    $('.co-popup-slide-content').hide();
                    $('.co-popup-slide-question').show();
                }, "700");

                var stepinputinfo = coformSlideObj.actions.getStepInputInfo(coformSlideObj);
                $('.swiper-slide.resume').attr("id" , "slide"+stepinputinfo.actualInput);
                $('.co-popup-slide-container').append(coformSlideObj.views.createNavButton(coformSlideObj , stepinputinfo));
                $('.co-popup-slide-container').prepend(coformSlideObj.views.createBreadCrumb(coformSlideObj, stepinputinfo));


                $('.swiper-wrapper').append(`<div class="swiper-slide" id="slide${ stepinputinfo.nextInput }"></div>`);
                coformSlideObj.events.loadStep(coformSlideObj , `#slide${ stepinputinfo.nextInput }` , stepinputinfo.nextInputStep , stepinputinfo.nextInput , true);

                $('.swiper-wrapper').append(`<div class="swiper-slide" style="display : none" id="slide${ stepinputinfo.prevInput }"></div>`);
                coformSlideObj.events.loadStep(coformSlideObj , `#slide${ stepinputinfo.prevInput }` , stepinputinfo.prevInputStep , stepinputinfo.prevInput , true);

                coformSlideObj.events.btnEvent();
            });

            $(".ap-slide-next").off().click(function(e){
                e.preventDefault();
                var stepinputinfo = coformSlideObj.actions.getStepInputInfo(coformSlideObj);
                $(`.co-popup-slide-formnav`).remove();
                $(`.br-container`).remove();

                $(`#slide${stepinputinfo.nextInput}`).show();
                $(`#slide${stepinputinfo.actualInput}`).hide();

                coformSlideObj.coformData.answer.slide = {
                    input : stepinputinfo.nextInput,
                    step   : stepinputinfo.nextInputStep
                }

                coformSlideObj.actions.updateSlideStory(coformSlideObj , stepinputinfo.nextInputStep , stepinputinfo.nextInput , function(){});

                stepinputinfo = coformSlideObj.actions.getStepInputInfo(coformSlideObj);

                $('.co-popup-slide-container').append(coformSlideObj.views.createNavButton(coformSlideObj , stepinputinfo));
                $('.co-popup-slide-container').prepend(coformSlideObj.views.createBreadCrumb(coformSlideObj, stepinputinfo));

                $('.swiper-wrapper').append(`<div class="swiper-slide" id="slide${ stepinputinfo.nextInput }" style="display: none"></div>`);
                coformSlideObj.events.loadStep(coformSlideObj , `#slide${ stepinputinfo.nextInput }` , stepinputinfo.nextInputStep , stepinputinfo.nextInput , true);

                coformSlideObj.events.btnEvent();
            });

            $(".ap-slide-prev").off().click(function(){
                var stepinputinfo = coformSlideObj.actions.getStepInputInfo(coformSlideObj);
                $(`.co-popup-slide-formnav`).remove();
                $(`.br-container`).remove();
                $(`.swiper-slide#slide${ stepinputinfo.actualInput }`).remove();
                $(`.swiper-slide#slide${ stepinputinfo.nextInput }`).remove();
                if($(`.swiper-slide#slide${ stepinputinfo.prevInput }`).length > 0){
                    $(`.swiper-slide#slide${ stepinputinfo.prevInput }`).show();
                }else{
                    $('.swiper-wrapper').append(`<div class="swiper-slide" id="slide${ stepinputinfo.prevInput }"></div>`);
                    coformSlideObj.events.loadStep(coformSlideObj , `#slide${ stepinputinfo.prevInput }` , stepinputinfo.prevInputStep , stepinputinfo.prevInput , false);
                }

                coformSlideObj.events.afterload();

                coformSlideObj.coformData.answer.slide = {
                    input : stepinputinfo.prevInput,
                    step   : stepinputinfo.prevInputStep
                }

                coformSlideObj.actions.updateSlideStory(coformSlideObj , stepinputinfo.prevInputStep , stepinputinfo.prevInput , function(){});

                stepinputinfo = coformSlideObj.actions.getStepInputInfo(coformSlideObj);


                $('.co-popup-slide-container').append(coformSlideObj.views.createNavButton(coformSlideObj , stepinputinfo));
                $('.co-popup-slide-container').prepend(coformSlideObj.views.createBreadCrumb(coformSlideObj, stepinputinfo));

                $('.swiper-wrapper').append(`<div class="swiper-slide" id="slide${ stepinputinfo.nextInput }" style="display: none"></div>`);
                coformSlideObj.events.loadStep(coformSlideObj , `#slide${ stepinputinfo.nextInput }` , stepinputinfo.nextInputStep , stepinputinfo.nextInput , true);

                coformSlideObj.events.btnEvent();
            });
        },
        loadStep : function(coformSlideObj , container,  step  , input , async = true){
            if(coformSlideObj.coformData.isOpenToTempAcount) {
                showLoader(container);
                ajaxPost(container, baseUrl + `/survey/answer/answer/id/${coformSlideObj.coformData.answer._id.$id}/form/${coformSlideObj.coformData.parentForm._id.$id}/step/${step}/isinsideform/false/input/${input}`, null, function (res) {
                    $(container + ' center').hide();
                    coformSlideObj.events.afterload();
                }, null, "html", {async: async});
            }
        },
        afterload : function (){
            if($('input:visible:first')){
                var focusval = $('input:visible:first').val();
                $('input:visible:first').focus().val("").val(focusval);
            }
            $('input:visible:first').focus();


            var actIn = coformSlideObj.actions.getStepInputInfo(coformSlideObj).actualInput;
            var actSt = coformSlideObj.actions.getStepInputInfo(coformSlideObj).actualStep;
            if($(`.btnnext_${actIn}`).attr('disabled') == 'disabled'){
                $('input.saveOneByOne').on("keyup", function(){
                    var thisinput = $(this);
                    if(thisinput.val() != "" && $(`.btnnext_${thisinput.attr('id')}`).attr('disabled') == 'disabled'){
                        $(`.btnnext_${thisinput.attr('id')}`).attr('disabled', false)
                    }
                });
                $('textarea.saveOneByOne').on('DOMSubtreeModified' , function (){
                    var thisinput = $(this);
                    if(thisinput.val() != "" && $(`.btnnext_${thisinput.attr('id')}`).attr('disabled') == 'disabled'){
                        $(`.btnnext_${thisinput.attr('id')}`).attr('disabled', false)
                    }
                });
                $('#question_'+actIn).on('DOMSubtreeModified' , function (){
                    ajaxPost(
                        null,
                        baseUrl + '/co2/aap/refreshanswer',
                        {
                            id : coForm_FormWizardParams.answer?.["_id"]?.["$id"] ? coForm_FormWizardParams.answer?.["_id"]?.["$id"] : ""
                        },
                        function (res) {
                            var actIn = coformSlideObj.actions.getStepInputInfo(coformSlideObj).actualInput;
                            var actSt = coformSlideObj.actions.getStepInputInfo(coformSlideObj).actualStep;
                            if(notEmpty(res.answers[actSt][actIn])){
                                $(`.btnnext_${actIn}`).attr('disabled', false);
                            }
                        }
                    );
                });
            }

            $('body').off().on('keydown' ,  function (e){
                if (e.which === 13 || e.which === 39){
                    if ($(".ap-slide-next").attr('disabled') != 'disabled') {
                        $(".ap-slide-next").trigger('click');
                    }
                    if(e.which === 13 && $('.ap-starter-btn-email').length > 0 && $('.ap-starter-btn-email').val() != ""){
                        $('.ap-starter-btn-email').trigger('click');
                    }
                } else if (e.which === 37) {
                    if ($(".ap-slide-prev").attr('disabled') != 'disabled') {
                        $(".ap-slide-prev").trigger('click');
                    }
                }
            });
            coformSlideObj.actions.formatDescr();
            //$('input:visible:first').attr('autofocus', 'true');
        }
    },
    actions: {
        formatDescr : function (){
            const viewMoreText = "...<b>Voir plus </b></a>";
            const viewLessText = "<b> Voir moins</b></a>";

            var paragraph = document.querySelectorAll(".formDescr")[0];

            var paragraphIsOverflowing = paragraph.clientHeight < paragraph.scrollHeight ? true : false;
            if (!paragraphIsOverflowing || $('.toggle-formedsc').length > 0){
                return;
            } else {
                var theToggleLink = document.createElement("A");
                theToggleLink.classList.add("toggle-formedsc");
                theToggleLink.classList.add("toggle");
                theToggleLink.innerHTML = viewMoreText;

                theToggleLink.onclick = () => {
                    if (paragraph.classList.contains("max-lines-visible")) {
                        paragraph.classList.remove("max-lines-visible");
                        theToggleLink.innerHTML = viewMoreText;
                        return;
                    }
                    paragraph.classList.add("max-lines-visible");
                    theToggleLink.innerHTML = viewLessText;
                };

                paragraph.closest('.formDescrdiv').append(theToggleLink);
            }
        },
        updateSlideStory : function(coformSlideObj, step , input , callback = function(){}){
            var tplCtx = {
                id : coformSlideObj.coformData.answer._id.$id,
                form : coformSlideObj.coformData.parentForm._id.$id,
                path : "slide",
                collection : "answers"
            }
            var today = new Date();
            today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();
            tplCtx.value = {
                input : input,
                step   : step,
                lastChange   : today
            }
            dataHelper.path2Value(tplCtx, function (params) {
                callback();
            });
        },
        getStepInputInfo : function (coformSlideObj , isnew = false) {
            var actualStep = false;
            var actualInput = false;
            var isRequired = false;
            Object.filter = (obj, predicate) => Object.keys(obj)
                .filter(key => predicate(obj[key]))
                .reduce((res, key) => (res[key] = obj[key] , res), {});

            if (typeof coformSlideObj.coformData.parentForm.subForms != "undefined") {

                $.each(coformSlideObj.coformData.steps, function (index, value) {
                    if (typeof coformSlideObj.coformData.steps[index] != "undefined"
                        && typeof coformSlideObj.coformData.steps[index].inputs != "undefined"
                    ) {
                        coformSlideObj.coformData.steps[index].inputs = Object.filter(value.inputs, input => !notShowInput.includes(input.type));
                        /*coformSlideObj.coformData.steps[index].input = Object.values(value.inputs).filter(function (item){
                            return !notShowInput.includes(item.type);
                        });*/
                    }else{
                        coformSlideObj.coformData.steps[index].inputs = {};
                    }
                });

                if (typeof coformSlideObj.coformData.answer.slide == "undefined" ||
                    isnew
                ) {
                    actualStep = coformSlideObj.coformData.parentForm.subForms[0];
                    //actualInput = Object.keys(coformSlideObj.coformData.steps[coformSlideObj.coformData.parentForm.subForms[0]].inputs)[0];
                    if (typeof coformSlideObj.coformData.parentForm.subForms != "undefined" && typeof coformSlideObj.coformData.steps[coformSlideObj.coformData.parentForm.subForms[0]].inputs != "undefined")
                        actualInput = Object.keys(coformSlideObj.coformData.steps[coformSlideObj.coformData.parentForm.subForms[0]].inputs).find(key => coformSlideObj.coformData.steps[coformSlideObj.coformData.parentForm.subForms[0]].inputs[key].position === "1");
                    if (typeof actualInput == "undefined") {
                        actualInput = Object.keys(coformSlideObj.coformData.steps[coformSlideObj.coformData.parentForm.subForms[0]].inputs)[0];
                    }
                } else {
                    actualStep = coformSlideObj.coformData.answer.slide.step;
                    actualInput = coformSlideObj.coformData.answer.slide.input;
                }

                if(typeof coformSlideObj.coformData.steps[actualStep].inputs != "undefined"){
                    isRequired = typeof coformSlideObj.coformData.steps[actualStep].inputs[actualInput].isRequired != "undefined" ? JSON.parse(coformSlideObj.coformData.steps[actualStep].inputs[actualInput].isRequired) : false;
                }

                var nextStep = false;
                var nextInput = false;
                var nextInputStep = false;
                var prevStep = false;
                var prevInput = false;
                var prevInputStep = false;

                var subformArray = coformSlideObj.coformData.parentForm.subForms;
                if (
                    notNull(subformArray[subformArray.indexOf(actualStep) + 1])
                ) {
                    nextStep = subformArray[subformArray.indexOf(actualStep) + 1];
                }
                if (notNull(subformArray[subformArray.indexOf(actualStep) - 1])
                ) {
                    prevStep = subformArray[subformArray.indexOf(actualStep) - 1];
                }

                //var inputArray = Object.keys(Object.values(coformSlideObj.coformData.steps)[ subformArray.indexOf(actualStep) ].inputs);
                var inputArray = Object.keys(coformSlideObj.coformData.steps[coformSlideObj.coformData.parentForm.subForms[subformArray.indexOf(actualStep)]].inputs)
                    .sort(function (a, b) {
                        return parseInt(coformSlideObj.coformData.steps[coformSlideObj.coformData.parentForm.subForms[subformArray.indexOf(actualStep)]].inputs[a].position) -
                            parseInt(coformSlideObj.coformData.steps[coformSlideObj.coformData.parentForm.subForms[subformArray.indexOf(actualStep)]].inputs[b].position)
                    });


                var lastStepInputArray = false;
                var nextStepInputArray = false;

                if (prevStep != false) {
                    lastStepInputArray = Object.keys(coformSlideObj.coformData.steps[coformSlideObj.coformData.parentForm.subForms[subformArray.indexOf(actualStep) - 1]].inputs)
                        .sort(function (a, b) {
                            return parseInt(coformSlideObj.coformData.steps[coformSlideObj.coformData.parentForm.subForms[subformArray.indexOf(actualStep) - 1]].inputs[a].position) -
                                parseInt(coformSlideObj.coformData.steps[coformSlideObj.coformData.parentForm.subForms[subformArray.indexOf(actualStep) - 1]].inputs[b].position)
                        });
                }
                if (nextStep != false) {
                    nextStepInputArray = Object.keys(coformSlideObj.coformData.steps[coformSlideObj.coformData.parentForm.subForms[subformArray.indexOf(actualStep) + 1]].inputs)
                        .sort(function (a, b) {
                            return parseInt(coformSlideObj.coformData.steps[coformSlideObj.coformData.parentForm.subForms[subformArray.indexOf(actualStep) + 1]].inputs[a].position) -
                                parseInt(coformSlideObj.coformData.steps[coformSlideObj.coformData.parentForm.subForms[subformArray.indexOf(actualStep) + 1]].inputs[b].position)
                        });
                }

                //actualstep == notlast
                if (notNull(inputArray[inputArray.indexOf(actualInput) + 1])) {
                    nextInput = inputArray[inputArray.indexOf(actualInput) + 1];
                    nextInputStep = actualStep;
                }
                //actualstep == last && nextStep contains inputs
                else if (inputArray.length == inputArray.indexOf(actualInput) + 1
                    &&
                    nextStep != false
                ) {
                    nextInput = nextStepInputArray[0];
                    nextInputStep = nextStep;
                }

                //actualstep == notfirst
                if (notNull(inputArray[inputArray.indexOf(actualInput) - 1])) {
                    prevInput = inputArray[inputArray.indexOf(actualInput) - 1];
                    prevInputStep = actualStep;
                }
                //actualstep == first && nextStep contains inputs
                else if (inputArray.indexOf(actualInput) == 0
                    &&
                    prevStep != false
                ) {
                    prevInput = lastStepInputArray[lastStepInputArray.length - 1];
                    prevInputStep = prevStep;
                }
            }

            return {
                actualStep : actualStep,
                actualInput : actualInput,
                nextStep : nextStep,
                nextInput : nextInput,
                prevStep : prevStep,
                prevInput : prevInput,
                nextInputStep : nextInputStep,
                prevInputStep : prevInputStep,
                isRequired : isRequired
            }
        }
    }
};