function pushListRoles(links){
    //Members
    if( typeof links.members != "undefined" ){
        $.each(links.members, function(e,v){
            if(typeof v.roles != "undefined" && v.roles != null){
                $.each(v.roles, function(i,data){
                    if(data != "" && !rolesList.includes(data)){
                        rolesList.push(data);
                    }
                });
            }
        });
    }

    //Contributors
    if(typeof links.contributors != "undefined"){
        $.each(links.contributors, function(e,v){
            if(typeof v.roles != "undefined" && v.roles != null){
                $.each(v.roles, function(i,data){
                    if(data != "" && !rolesList.includes(data)){
                        rolesList.push(data);
                    }
                });
            }
        });
    }

    //Attendees
    if(typeof links.attendees != "undefined"){
        $.each(links.attendees, function(e,v){
            if(typeof v.roles != "undefined" && v.roles != null){
                $.each(v.roles, function(i,data){
                    if(data != "" && !rolesList.includes(data)){
                        rolesList.push(data);
                    }
                });
            }
        });
    }
}
var pageProfil = {
    params:{},
    affixPageMenu : 0,
    form : null,
    init : function(){
        mylog.log("pageProfil.init");
        coInterface.showLoader("#central-container");
        pageProfil.form = null;
        pageProfil.initDateHeaderPage();
        pageProfil.initView();
        pageProfil.initMetaPage(contextDataAap.name,contextDataAap.shortDescription,contextDataAap.profilImageUrl);
        if(typeof contextDataAap.name !="undefined")
            setTitle("", "", contextDataAap.name);
        pageProfil.bindButtonMenu();
        if(typeof pageProfil.initCallB == "function")
            pageProfil.initCallB();
        //coInterface.initHtmlPosition();
        lazyImgLoading(["#headerBand img","#social-header #contentBanner img"], function(){coInterface.initHtmlPosition()});

    },

    initDateHeaderPage : function(){
        mylog.log("pageProfil.initDateHeaderPage");
        if(contextDataAap.type == "organizations" && typeof contextDataAap.openingHours != "undefined"){
            var str = directory.isOpenedStr(contextDataAap);
        }else{
            var str = directory.getDateFormated(contextDataAap);
        }
        $(".event-infos-header").html(str);
    },
    initView(){
        mylog.log("pageProfil.initView");
        if(pageProfil.params.view!=""){
            if(!notEmpty(pageProfil.params.dir))
                $(".ssmla[data-view='"+pageProfil.params.view+"']").addClass("active");
            else
                $(".ssmla[data-view='"+pageProfil.params.view+"'][data-dir='"+pageProfil.params.dir+"']").addClass("active");
            //if(pageProfil.params.view=="coop"){
            //	pageProfil.views.newspaper(false);
            //	pageProfil.actions.cospace();
            //}
            //else	
            pageProfil.views[pageProfil.params.view]();
            $("#central-container").attr("data-active-view", pageProfil.params.view);
        }else{
            $(".ssmla[data-view='newspaper']").addClass("active");
            $("#central-container").attr("data-active-view", "newspaper");
            pageProfil.views.newspaper(false);
        }
    },
    initMetaPage(title, description, image){
        mylog.log("pageProfil.initMetaPage", title, description, image);
        if(title != ""){
            $("meta[name='title']").attr("content",title);
            $("meta[property='og:title']").attr("content",title);
        }
        if(description != ""){
            $("meta[name='description']").attr("content",description);
            $("meta[property='og:description']").attr("content",description);
        }
        if(image != ""){
            $("meta[name='image']").attr("content",baseUrl+image);
            $("meta[property='og:image']").attr("content",baseUrl+image);
        }
    },
    setPosition : function(){
        //alert($("#headerBand").height());
        setTimeout(function(){
            pageProfil.affixPageMenu=$("#headerBand").height()+$("#mainNav").height()+$("#social-header").height()+$("#social-sub-header").outerHeight();//-$('.sub-menu-social').outerHeight();
            pageProfil.menuTopHeight=$("#mainNav").outerHeight()+$(".sub-menu-social").outerHeight();
            $('.sub-menu-social').removeClass('affix affix-top affix-bottom').removeData();
            $('.sub-menu-social').affix({
                offset: {
                    top: Number(pageProfil.affixPageMenu)
                }
            }).on('affixed.bs.affix', function(){
                $(this).css({"top":$("#mainNav").outerHeight()});
                $(".central-section").css({"padding-top":(pageProfil.menuTopHeight)});
                if($("#menu-left-container").length > 0 && $("#menu-left-container").is(":visible")){
                    $("#menu-left-container").css({"position": "fixed", "left" : $("#menuLeft").outerWidth(), "top": (pageProfil.menuTopHeight+15)});
                    $(".central-section").addClass("col-lg-offset-2 col-md-offset-3 col-sm-offset-3");
                }

            }).on('affixed-top.bs.affix', function(){
                $(this).css({"top":"initial"});
                $(".central-section").css({"padding-top":"initial"});
                if($("#menu-left-container").length > 0 && $("#menu-left-container").is(":visible")){
                    $("#menu-left-container").css({"position": "relative", "left" : "initial", "top": "initial"});
                    $(".central-section").removeClass("col-lg-offset-2 col-md-offset-3 col-sm-offset-3");
                }
            });
            marginTopMenuTop=($(".social-sub-header").length > 0) ? $(".social-sub-header").outerHeight() : 0;
            $(".sub-menu-social").css({"margin-top": marginTopMenuTop});
            $("#menu-top-profil-social .menu-xs-container").css("top",$("#menu-top-profil-social").outerHeight());
        },600);
        /*if($("#central-container .headerSearchIncommunity").hasClass("affix")){
            $("#central-container .headerSearchIncommunity").css("top",heightPos);
            heightPos=heightPos+$("#central-container .headerSearchIncommunity").outerHeight(); 
        }else{
            $("#central-container .headerSearchIncommunity").css("top","inherit");
        }*/
        //if("#central-container .headerSearchIncommunity").
    },
    bindViewActionEvent:function(){
        mylog.log("pageProfil.bindViewActionEvent");
        $(".ssmla").off().on("click", function(){
            pageProfil.responsiveMenuLeft();
            pageProfil.params.action = $(this).data("action");
            coInterface.initHtmlPosition();
            coInterface.simpleScroll((pageProfil.affixPageMenu+5));
            $("#div-select-create").hide(200);
            if(notEmpty($(this).data("view"))){
                pageProfil.params.view = $(this).data("view");
                pageProfil.params.dir = $(this).data("dir");
                pageProfil.params.folderId = $(this).data("folderId");
                coInterface.showLoader("#central-container");
                $("#central-container").attr("data-active-view", pageProfil.params.view);
                $(".ssmla").removeClass("active");
                $(this).addClass("active");
                onchangeClick=false;
                url=hashUrlPage+".view."+$(this).data("view");
                url+=(notNull(pageProfil.params.dir)) ? ".dir."+pageProfil.params.dir : "";
                if(pageProfil.params.view=="directory" && !notEmpty(pageProfil.params.dir)){
                    url+=".dir."+links.connectType[contextDataAap.type];
                }
                location.hash=url;

                if(pageProfil.params.view =="cospace"  && $(".central-section").hasClass("col-lg-10"))
                    $(".central-section").removeClass("col-lg-10 col-lg-offset-1").addClass("col-lg-12");
                else if($(".central-section").hasClass("col-lg-12"))
                    $(".central-section").removeClass("col-lg-12").addClass("col-lg-10 col-lg-offset-1");

                if( pageProfil.params.view=="forms")
                    $(".central-section").removeClass("col-lg-10 col-lg-offset-1").addClass("col-lg-12");

                pageProfil.views[pageProfil.params.view]($(this));
            }else if(notEmpty(pageProfil.params.action)){
                pageProfil.actions[pageProfil.params.action]();
            }
        });
    },
    bindButtonMenu : function(){
        mylog.log("pageProfil.bindButtonMenu");
        pageProfil.bindViewActionEvent();
        $("#paramsMenu").click(function(){
            $(".dropdown-profil-menu-params").addClass("open");
        });
        $(".dropdown-profil-menu-params .dropdown-menu").mouseleave(function(){ //alert("dropdown-user mouseleave");
            setTimeout(function(){
                //if(!$(".dropdown-profil-menu-params").is(":hover"))
                $(".dropdown-profil-menu-params").removeClass("open");
            }, 200);
        });
        $("#btnHeaderEditInfos").click(function(){
            $("#btn-start-detail").trigger("click");
        });

        $("#btn-close-select-create").click(function(){
            $("#div-select-create").hide(200);
        });

        coInterface.bindButtonOpenForm();

    },
    responsiveMenuLeft: function(menuTop){
        mylog.log("pageProfil.responsiveMenuLeft", menuTop);
        if($(".element-xs-menu").is(":visible")){
            $(".open-xs-menu.close-menu").trigger("click");
        }
        //if($(window).width()<768)
        //	pageProfil.menuLeftShow();
        if(menuTop){
            if($(window).width()>768)
                $(".ssmla").removeClass('active');
        }
    },
    menuLeftShow : function(){
        mylog.log("pageProfil.menuLeftShow");
        if($("#menu-left-container").hasClass("hidden-xs"))
            $("#menu-left-container").removeClass("hidden-xs");
        else
            $("#menu-left-container").addClass("hidden-xs");
    },
    views : {
        newspaper: function(){
            mylog.log("pageProfil.views.newspaper");
            //coInterface.scrollTo("#profil_imgPreview");
            setTimeout(function(){ //attend que le scroll retourn en haut (coInterface.scrollTo)
                ajaxPost('#central-container', baseUrl+"/news/co/index/type/"+typeItem+"/id/"+contextDataAap.id,
                    {nbCol:1},
                    function(){});
            }, 700);
        },
        preferences: function(){
            mylog.log("pageProfil.views.preferences");
            var url = "pod/preferences";
            ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url,
                null,
                function(){});
        },
        gallery: function(){
            mylog.log("pageProfil.views.gallery");
            var url = "gallery/index/type/"+typeItem+"/id/"+contextDataAap.id;
            if(notNull(pageProfil.params.dir))
                url+="/docType/"+pageProfil.params.dir;

            if(notNull(pageProfil.params.key))
                url+="/contentKey/"+pageProfil.params.key;

            if(notNull(pageProfil.params.folderId))
                url+="/folderId/"+pageProfil.params.folderId;
            ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url,
                null,
                function(){});
        },
        coop : function(){
            mylog.log("pageProfil.views.coop");
            $("#modalCoop").modal("show");
            pageProfil.actions.cospace();
            pageProfil.views.newspaper(false);
        },
        photos : function(){
            mylog.log("pageProfil.views.photos");
            var url = "gallery/index/type/"+typeItem+"/id/"+contextDataAap.id+"/docType/image";
            ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url,
                null,
                function(){});
        },
        mediawiki : function() {
            mylog.log("pageProfil.views.mediawiki");
            //$("#central-container").append(loading);
            ajaxPost("#central-container",
                baseUrl+"/interop/mediawiki/index",
                {id: contextDataAap.id, name: contextDataAap.name, type: contextDataAap.type , for:"index"},
                function(){}, "html");
        },
        bookmarks : function(){
            mylog.log("pageProfil.views.bookmarks");
            var url = "gallery/index/type/"+typeItem+"/id/"+contextDataAap.id+"/docType/bookmark";
            ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url,
                null,
                function(){});
        },
        library: function(){
            mylog.log("pageProfil.views.library");
            var url = "gallery/index/type/"+typeItem+"/id/"+contextDataAap.id+"/docType/file";
            ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url,
                null,
                function(){});
        },
        notifications : function(){
            mylog.log("pageProfil.views.notifications");
            var url = "element/notifications/type/"+typeItem+"/id/"+contextDataAap.id;
            ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url,
                null,
                function(){});
        },
        history : function (){
            mylog.log("pageProfil.views.history");
            var url = "pod/activitylist/type/"+typeItem+"/id/"+contextDataAap.id;
            ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url,
                null,
                function(){});
        },
        community : function(){
            //pageProfil.params.dir=(!notEmpty(pageProfil.params.dir)) ? links.connectType[contextDataAap.type] : pageProfil.params.dir;


            pageProfil.views.directory();
        },
        directory : function(callBack){
            mylog.log("pageProfil.views.directory");
            //var dataIcon = (!notEmpty(pageProfil.params.dir)) ? "users" : $(".smma[data-type-dir="+pageProfil.params.dir+"]").data("icon");
            pageProfil.params.dir=(!notEmpty(pageProfil.params.dir)) ? links.connectType[contextDataAap.type] : pageProfil.params.dir;
            //var sub=(!notEmpty(pageProfil.params.sub)) ? "" : "/sub/"+pageProfil.params.sub;
            pageProfil.directory.init();


        },
        calendar : function(){
            var params={searchP : {filters :{}}};
            params.searchP.filters["$or"]={};
            params.searchP.filters["$or"]["parent."+contextDataAap.id] = {'$exists' :true};
            params.searchP.filters["$or"]["links.attendees."+contextDataAap.id] = {'$exists' :true};

            ajaxPost('#central-container', baseUrl+'/'+moduleId+'/app/calendar',
                params,
                function(){

                });
        },
        chart: function(){
            mylog.log("pageProfil.views.chart");
            var url = "chart/header/type/"+typeItem+"/id/"+contextDataAap.id;
            ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url,
                null,
                function(){});
        },
        editChart: function(){
            mylog.log("pageProfil.views.editChart");
            var url = "chart/addchartsv/type/"+contextDataAap.type+"/id/"+contextDataAap.id;
            ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url,
                null,
                function(){});
        },
        detail : function(){
            mylog.log("pageProfil.views.detail");
            var url = "element/about/type/"+contextDataAap.type+"/id/"+contextDataAap.id;
            ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url+'?tpl=ficheInfoElement', null, function(){});
        },
        newhome : function(){
            mylog.log("pageProfil.views.newhome");
            var url = "element/newhome/type/"+contextDataAap.type+"/id/"+contextDataAap.id;
            ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url+'?tpl=ficheInfoElement', null, function(){});

        },
        urls : function(){
            mylog.log("pageProfil.views.urls");
            getAjax('', baseUrl+'/'+moduleId+'/element/geturls/type/'+contextDataAap.type+
                '/id/'+contextDataAap.id,
                function(data){
                    displayInTheContainer(data, "urls", "external-link", "urls");
                }
                ,"html");
        },
        networks: function(){
            mylog.log("pageProfil.views.networks");
            getAjax('', baseUrl+'/'+moduleId+'/element/getnetworks/type/'+contextDataAap.type+
                '/id/'+contextDataAap.id,
                function(data){
                    mylog.log("loadNetworks success", data, canEdit);
                    displayInTheContainer(data, "networks", "external-link", "networks",canEdit);
                }
                ,"html");
        },
        curiculum: function(){
            mylog.log("pageProfil.views.curiculum");
            getAjax('', baseUrl+'/'+moduleId+'/element/getcuriculum/type/'+contextDataAap.type+
                '/id/'+contextDataAap.id,
                function(html){
                    mylog.log("loadCuriculum success", html);
                    $("#central-container").html(html);
                }
                ,"html");
        },
        settings: function(){
            mylog.log("pageProfil.views.settings");
            getAjax('', baseUrl+'/'+moduleId+'/settings/index',
                function(html){
                    $("#central-container").html(html);
                }
                ,"html");//urlCtrl.loadByHash("#settings.page.myAccount");
        },
        settingsCommunity : function(){
            mylog.log("pageProfil.views.settingsCommunity");
            getAjax('', baseUrl+'/'+moduleId+'/settings/confidentiality/type/'+contextDataAap.type+'/id/'+contextDataAap.id,
                function(html){
                    $("#central-container").html("<div class='col-xs-12 bg-white'>"+html+"</div>");
                }
                ,"html");
        },
        contacts: function(){
            mylog.log("pageProfil.views.contacts");
            getAjax('', baseUrl+'/'+moduleId+'/element/getcontacts/type/'+contextDataAap.type+
                '/id/'+contextDataAap.id,
                function(data){
                    mylog.log("loadContacts", data);
                    //displayInTheContainer(data, "contacts", "envelope", "contacts");
                    var str = '';
                    var countContacts = data.length;
                    str += "<div class='col-md-12 col-sm-12 col-xs-12 labelTitleDir margin-bottom-15'>";
                    str += '<i class="fa fa-envelope fa-2x margin-right-10"></i>';
                    str += '<i class="fa fa-angle-down"></i>';
                    str += "<span class='Montserrat' id='name-lbl-title'> "+contextDataAap.name+"</span>";
                    if(countContacts>0){
                        str += 	"<span> a <b>"+countContacts+"</b></span>";
                        if(countContacts==1){
                            str += 	"<span> contact</span>";
                        }
                        else {
                            str += 	"<span> contacts</span>";
                        }
                    }
                    else{
                        str += 	"<span> n'a aucun contact</span>";
                    }
                    if( (typeof openEdition != "undefined" && openEdition == true) || (typeof canEdit != "undefined" && canEdit == true) ){
                        str += '<a class="btn btn-sm btn-link bg-green-k pull-right " href="javascript:;" onclick="dyFObj.openForm ( \'contactPoint\',\'sub\')">';
                        str +=	'<i class="fa fa-plus"></i> '+trad["Add contact"]+'</a>' ;
                    }
                    str += "</div>";
                    $.each(data,function(key,v){
                        str += '<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 margin-bottom-10 ">';
                        str += '<div class="searchEntity contactPanelHtml">';
                        str += '<div class="panel-heading border-light col-lg-12 col-xs-12">';
                        if(notEmpty(v.idContact)){
                            str += '<a href="#page.type.citoyens.id.'+v.idContact+'" class="lbh" >';
                            str += (notEmpty(v.name) ? '<h4 class="panel-title text-dark pull-left">'+v.name+'</h4><br/>' : '')+'</a>';
                        }
                        else
                            str += (notEmpty(v.name) ? '<h4 class="panel-title text-dark pull-left">'+v.name+'</h4><br/>' : '');
                        str += (notEmpty(v.role) ? '<span class="" style="font-size: 13px !important;">'+v.role+'</span><br/>' : '');
                        //str += (notEmpty(v.email) ? '<a href="javascript:;" onclick="dyFObj.openForm("formContact", "init")" style="font-size: 11px !important;">'+v.email+'</a><br/>' : '');
                        str += (notEmpty(v.email) ? '<span class="" style="font-size: 12px !important;">'+v.email+'</span><br/>' : '');
                        str += (notEmpty(v.telephone) ? '<span class="" style="font-size: 12px !important;">'+v.telephone+'</span>' : '');
                        str += '</div>';
                        if(typeof userId != 'undefined' && userId != ''){
                            str += '<ul class="nav navbar-nav margin-5 col-md-12">';
                            if(notEmpty(v.email)){
                                str += '<li class="text-left pull-left">';
                                str += '<a href="javascript:;" class="tooltips btn btn-default btn-sm openFormContact" '+
                                    'data-id-receiver="'+key +'" '+
                                    'data-email="'+(notEmpty(v.email) ? v.email : '') +'" '+
                                    'data-name="'+(notEmpty(v.name) ? v.name : '') +'">';
                                str += '<i class="fa fa-envelope"></i> Envoyer un e-mail';
                                str += '</a>';
                                str += '</li>';
                            }
                            if ((typeof v.openEdition != 'undefined' && v.openEdition == true) || (typeof v.edit != 'undefined' && v.edit == true) ) {
                                str += '<li class="text-red pull-right">';
                                str += '<a href="javascript:;" onclick="removeContact(\''+key+'\');" '+
                                    'class="margin-left-5 bg-white tooltips btn btn-link btn-sm" '+
                                    'data-toggle="tooltip" data-placement="top" data-original-title="'+trad['delete']+'" >';
                                str += '<i class="fa fa-trash"></i>';
                                str += '</a>';
                                str += '</li>';
                                str += '<li class="text-left pull-right">';

                                str += '<a href="javascript:" ' +
                                    'class="bg-white tooltips btn btn-link btn-sm btn-update-contact" '+

                                    'data-contact-key="'+key+'" data-contact-name="'+v.name+'" '+
                                    'data-contact-email="'+v.email+'" data-contact-role="'+v.role+'" '+
                                    'data-contact-telephone="'+v.telephone+'"'+

                                    'data-toggle="tooltip" data-placement="top" '+
                                    'data-original-title="'+trad['update']+'" >';
                                str += '<i class="fa fa-pencil"></i>';
                                str += '</a>';
                                str += '</li>';
                            }

                            str += '</ul>';
                        }
                        str += '</div>';
                        str += '</div>';
                        // return str;

                    });
                    $("#central-container").html(str);
                    $(".btn-update-contact").click(function(){
                        updateContact($(this).data("contact-key"),$(this).data("contact-name"), $(this).data("contact-email"),
                            $(this).data("contact-role"),$(this).data("contact-telephone"));
                    });
                    $(".openFormContact").click(function(){
                        var idReceiver = $(this).data("id-receiver");
                        var idReceiverParent = contextDataAap.id;
                        var typeReceiverParent = contextDataAap.type;

                        var contactMail = $(this).data("email");
                        var contactName = $(this).data("name");
                        //mylog.log('contactMail', contactMail);
                        $("#formContact .contact-email").html(contactMail);
                        $("#formContact #contact-name").html(contactName);

                        $("#formContact #emailSender").val(userConnected.email);
                        $("#formContact #name").val(userConnected.name);

                        $("#formContact #form-control").val("");

                        $("#formContact #idReceiver").val(idReceiver);
                        $("#formContact #idReceiverParent").val(idReceiverParent);
                        $("#formContact #typeReceiverParent").val(typeReceiverParent);

                        $("#conf-fail-mail, #conf-send-mail, #form-fail").addClass("hidden");
                        $("#form-group-contact").removeClass("hidden");
                        $("#formContact").modal("show");
                    });
                }
                ,"html");
        },
        forms: function(){
            mylog.log("pageProfil.views.forms");
            pageProfil.form = formObj.init({});
            pageProfil.form.initViews({});
            // formProfil.events.form(formProfil);
            // formProfil.events.add(formProfil);

        },
        dashboard: function(){
            mylog.log("pageProfil.views.dashboard");
            //alert( contextDataAap.costum.dashbordTpl );
            var dTpl = "";
            var dType = "";
            if( notNull(contextDataAap.costum.dashboard ) ){
                if( notNull( contextDataAap.costum.dashboard.tpl ) )
                    dTpl = "/tpl/"+contextDataAap.costum.dashboard.tpl;
                if ( notNull( contextDataAap.costum.dashboard.type ) )
                    dType = "/type/"+contextDataAap.costum.dashboard.type;
            }
            getAjax('#central-container', baseUrl+'/costum/co/dashboard/sk/'+contextDataAap.slug+dTpl+dType,
                function(data){
                    //alert("loaded dashboard ")
                }
                ,"html");

        },
        mediawiki : function() {
            mylog.log("pageProfil.views.mediawiki");
            //$("#central-container").append(loading);
            // let test = {id: contextId, name: contextName, type: contextType , for:"index"};
            // console.log(test);
            ajaxPost(".central-section",
                baseUrl+"/interop/mediawiki/index",
                {id: contextId, name: contextName, type: contextType , for:"index"},
                null, "html");
        },
        md : function(){
            mylog.log("pageProfil.views.md");
            console.log("contextDataAap",contextDataAap);
            typeElement = "person";
            if (contextType == "organizations")
                typeElement= "organization";
            else if (contextType == "projects")
                typeElement= "project";
            else if (contextType == "events")
                typeElement= "event";


            getAjax('', baseUrl+'/api/'+typeElement+'/get/id/'+contextDataAap.id+"/format/md",
                function(data){
                    descHtml = dataHelper.markdownToHtml(data) ;
                    $('#central-container').html(descHtml);
                    coInterface.bindLBHLinks();
                }
                ,"html");
        },
        cospace: function(){
            mylog.log("pageProfil.views.cospace");
            /*getAjax('', baseUrl+'/'+moduleId+'/element/getcospace/type/'+contextDataAap.type+
                        '/id/'+contextDataAap.id,
                        function(html){
                            mylog.log("loadCospace success", html);
                            $("#central-container").html(html);
                        }
            ,"html");*/
            //$("#modalCoop").modal("show");

            uiCoop.startUI();
            $("#central-container").html("<div id='modalCoop'>"+$("#modalCoop").html()+"</div>");
        }
    },
    actions : {
        delete: function(){
            mylog.log("Delete Element");
            $("#modal-delete-element").modal("show");
        },
        share : function(){
            //directory.bindBtnShareElt();
            //$(".btn-share").trigger("click");
            //if(html == "" && type !="news" && type!="activityStream" && typeof contextDataAap != "undefined"){
            directory.showShareModal(contextDataAap.type, contextDataAap.id);
            /*$("#modal-share").modal("show");
             html = directory.showResultsDirectoryHtml(new Array(contextDataAap), contextDataAap.type);
            //} 
            
            $("#modal-share #htmlElementToShare").html(html);
            $("#modal-share #btn-share-it").attr("data-id", contextDataAap.id);
            $("#modal-share #btn-share-it").attr("data-type", contextDataAap.type);
            $("#modal-share #btn-share-it").off().click(function(){
                  directory.shareIt("#modal-share #btn-share-it");
            });*/
        },
        cospace : function(){
            onchangeClick=false;
            location.hash=hashUrlPage+".view.coop";
            uiCoop.startUI();
        },
        create : function(){
            pageProfil.responsiveMenuLeft(true);
            $("#div-select-create").show(200);
        },
        chat : function(){
            hasRc=(typeof contextDataAap.hasRC != "undefined" || contextDataAap.type=="citoyens" ) ? true : false;
            rcObj.loadChat(contextDataAap.slug, contextDataAap.type, (params && params.element && params.element.preferences && params.element.preferences.private && params.element.preferences.private === true) ? false : true,hasRc, contextDataAap);
        },
        chatmanager: function () {
            var hasRc = (typeof contextDataAap.hasRC != "undefined" || contextDataAap.type == "citoyens") ? true : false;
            if (contextDataAap.type == "citoyens") {
                rcObj.loadChat(contextDataAap.slug, contextDataAap.type, (params && params.element && params.element.preferences && params.element.preferences.private && params.element.preferences.private === true) ? false : true, hasRc, contextDataAap);
            } else {
                var subChat = (contextDataAap && contextDataAap.rocketchatMultiEnabled && contextDataAap.tools && contextDataAap.tools.chat && contextDataAap.tools.chat.int && contextDataAap.tools.chat.int.length > 0 || contextDataAap && contextDataAap.tools && contextDataAap.tools.chat && contextDataAap.tools.chat.ext && contextDataAap.tools.chat.ext.length > 0) ? true : false;
                if (subChat){
                    mylog.log("pageProfil.views.chatManager");
                    getAjax('', baseUrl + '/' + moduleId + '/settings/chatmanager/type/' + contextDataAap.type + '/id/' + contextDataAap.id,
                        function (html) {
                            $("#central-container").html("<div class='col-xs-12 bg-white'>" + html + "</div>");
                        }
                        , "html");
                } else {
                    rcObj.loadChat(contextDataAap.slug, contextDataAap.type, (params && params.element && params.element.preferences && params.element.preferences.private && params.element.preferences.private === true) ? false : true, hasRc, contextDataAap);
                }
            }
        },
        mindmap : function(){
            co.mind();
        },
        graph:function(){
            window.location.href = baseUrl+"/"+moduleId+"/graph/d3/id/585bdfdaf6ca47b6118b4583/type/citoyen";
        },
        chatSettings : function(){
            rcObj.settings();
        },
        qrCode : function(){
            showDefinition('qrCodeContainerCl',true);
        },
        updateSlug : function(){
            updateSlug();
        },
        settingsAccount: function(){
            urlCtrl.loadByHash("#settings.page.myAccount");
        },
        confidentialityCommunity : function(){
            urlCtrl.loadByHash("#settings.page.confidentialityCommunity?slug="+contextDataAap.slug);
        },
        notificationsCommunity:function(){
            urlCtrl.loadByHash("#settings.page.notificationsCommunity?slug="+contextDataAap.slug);
        },
        downloadData : function(){
            extraParamsAjax={
                async:false,
                crossDomain:true
            };
            ajaxPost(
                null,
                baseUrl+"/"+moduleId+"/data/get/type/"+contextDataAap.type+"/id/"+contextDataAap.id,
                param,
                function(obj){
                    mylog.log("obj", obj);
                    $("<a/>", {
                        "download": "profil.json",
                        "href" : "data:application/json," + encodeURIComponent(JSON.stringify(obj))
                    }).appendTo("body").click(function() {
                        $(this).remove()
                    })[0].click() ;
                }
            );
        },
        printOut : function(){
            javascript:window.print();
        }
    },
    directory : {
        init : function(){
            pageProfil.directory.initView("#central-container");
            var contextDataAapByLink=pageProfil.directory.communityLinks[pageProfil.params.dir];
            var paramsFilter= {
                container : "#filterContainer",
                header : {
                    dom : ".headerSearchIncommunity"
                },
                defaults : {
                    notSourceKey : true,
                    types : contextDataAapByLink.types,
                    forced:{
                        filters:{}
                    },
                    filters:{
                    }
                },
                results : {
                    smartGrid : true,
                    renderView : "directory."+((exists(contextDataAapByLink.render)) ? contextDataAapByLink.render : "elementPanelHtml" ),
                    community : {
                        links : (typeof contextDataAap.links != "undefined") ? contextDataAap.links : null,
                        connectType : pageProfil.params.dir,
                        edit : true
                    }
                },
                filters : {
                    text : true
                }
            };
            // Si canSee on voit les elt privée car on appartient à la communauté

            if(typeof canParticipate != "undefined" && canParticipate){
                paramsFilter.urlData = baseUrl+"/co2/search/globalautocompleteadmin/type/"+contextDataAap.type+"/id/"+contextDataAap.id+"/canSee/true";
            }
            if(typeof contextDataAapByLink.links != "undefined"){
                var linksConnectRequest = (typeof contextDataAapByLink.links == "string") ? contextDataAapByLink.links : contextDataAapByLink.links[contextDataAap.type];
            }
            if(typeof linksConnectRequest != "undefined"
                && typeof contextDataAapByLink.parent != "undefined"){
                paramsFilter.defaults.forced.filters["$or"]={};
                paramsFilter.defaults.forced.filters["$or"]["links."+linksConnectRequest+"."+contextDataAap.id]={'$exists' :true};
                paramsFilter.defaults.forced.filters["$or"]["parent."+contextDataAap.id] = {'$exists' :true};
            }
            if(typeof contextDataAapByLink.organizer != "undefined"){
                paramsFilter.defaults.forced.filters["$or"]["organizer."+contextDataAap.id] = {'$exists' :true};
            }
            else if(typeof linksConnectRequest != "undefined"){
                paramsFilter.defaults.forced.filters["links."+linksConnectRequest+"."+contextDataAap.id] = {'$exists' :true};
            }else if(typeof contextDataAapByLink.parent != "undefined"){
                paramsFilter.defaults.forced.filters["parent."+contextDataAap.id] = {'$exists' :true};
            }
            if(contextDataAapByLink.types.length > 1){
                paramsFilter.filters.types = {
                    lists : contextDataAapByLink.types
                };
            }
            if(typeof contextDataAapByLink.loadEvent != "undefined"){
                paramsFilter.loadEvent=contextDataAapByLink.loadEvent;
            }
            if(typeof contextDataAapByLink.type != "undefined"){
                paramsFilter.defaults.forced.filters["type"]=contextDataAapByLink.type;
            }

            if(typeof contextDataAapByLink.urlData != "undefined")
                paramsFilter.urlData =contextDataAapByLink.urlData;
            if(typeof contextDataAapByLink.filters != "undefined"){
                if(typeof contextDataAapByLink.filters == "object" && !Array.isArray(contextDataAapByLink.filters)){
                    if(typeof contextDataAapByLink.initList != "undefined"){
                        $.each(contextDataAapByLink.initList, function(e,v){
                            contextDataAapByLink.filters[v].list=contextListFilter[v];
                        });
                        paramsFilter.filters=$.extend( contextDataAapByLink.filters , paramsFilter.filters );
                    }else
                        $.extend( paramsFilter.filters, contextDataAapByLink.filters );
                }
                else{
                    if($.inArray("roles", contextDataAapByLink.filters) >= 0){
                        paramsFilter.filters["roles"] = {
                            view : "horizontalList",
                            type : "filters",
                            dom : "#listRoles",
                            field :"links."+linksConnectRequest+"."+contextDataAap.id+".roles",
                            name : "<i class='fa fa-filter'></i> "+trad.sortbyrole+":",
                            active : true,
                            typeList : "object",
                            event : "inArray",
                            classList : "pull-left favElBtn btn",
                            list :  contextDataAap.rolesLists
                        }
                        //$("#central-container #listRoles").hide();
                    }
                    if($.inArray( "status", contextDataAapByLink.filters) >= 0){
                        paramsFilter.filters["status"] = {
                            view : "dropdownList",
                            type : "filters",
                            name : "Statuts",
                            action : "filters",
                            typeList : "object",
                            event : "exists",
                            list :  {
                                "admin" : {
                                    label: trad["administrator"],
                                    field : "links."+linksConnectRequest+"."+contextDataAap.id+".isAdmin",
                                    value : true,
                                    count : (exists(contextDataAap.counts) && exists(contextDataAap.counts.admin)) ? contextDataAap.counts.admin : 0
                                },
                                "members" : {
                                    label: trad[pageProfil.params.dir+"Active"],
                                    field : "links."+linksConnectRequest+"."+contextDataAap.id+".isInviting&&links."+linksConnectRequest+"."+contextDataAap.id+".toBeValidated",
                                    value : false,
                                    count : (exists(contextDataAap.counts) && exists(contextDataAap.counts[pageProfil.params.dir+"Active"])) ? contextDataAap.counts[pageProfil.params.dir+"Active"]: 0
                                },
                                "isInviting": {
                                    label : trad["unconfirmedinvitation"],
                                    field : "links."+linksConnectRequest+"."+contextDataAap.id+".isInviting",
                                    value : true,
                                    count : (exists(contextDataAap.counts) && exists(contextDataAap.counts.isInviting)) ? contextDataAap.counts.isInviting : 0
                                },
                                "toBeValidated": {
                                    label:trad["waitingValidation"],
                                    field : "links."+linksConnectRequest+"."+contextDataAap.id+".toBeValidated",
                                    value : true,
                                    count : (exists(contextDataAap.counts) && exists(contextDataAap.counts.toBeValidated)) ? contextDataAap.counts.toBeValidated : 0
                                }
                            }
                        }
                    }
                }

            }
            filterGroup = searchObj.init(paramsFilter);
            filterGroup.search.init(filterGroup);
            pageProfil.directory.initEvents();
        },
        initView : function(dom){
            $(dom+" .processingLoader").remove();
            var communityStr="";
            if(typeof pageProfil.directory.menuOutCommunity[pageProfil.params.dir] != "undefined"){
                if(typeof pageProfil.directory.menuOutCommunity[pageProfil.params.dir].sameAs != "undefined")
                    var menuCommunity=pageProfil.directory.menuOutCommunity[pageProfil.directory.menuOutCommunity[pageProfil.params.dir].sameAs];
                else
                    var menuCommunity=pageProfil.directory.menuOutCommunity[pageProfil.params.dir];
            }else if(typeof pageProfil.directory.communityMenu[contextDataAap.type] != "undefined"
                && typeof pageProfil.directory.communityMenu[contextDataAap.type][pageProfil.params.dir] != "undefined")
                var menuCommunity=pageProfil.directory.communityMenu[contextDataAap.type];

            if($(dom+" #menuCommunity").length <=0 && typeof menuCommunity != "undefined"){
                communityStr="<div id='menuCommunity' class='col-md-12 col-sm-12 col-xs-12 padding-20'>";
                $.each(menuCommunity, function(e,v){
                    var countCommunity=0;
                    if(typeof contextDataAap.counts != "undefined" && typeof contextDataAap.counts[e] != "undefined")
                        countCommunity=contextDataAap.counts[e];
                    else if(typeof contextDataAap.links != "undefined" && typeof contextDataAap.links[e] != "undefined")
                        countCommunity=Object.keys(contextDataAap.links[e]).length;
                    communityStr+=	'<a href="javascript:;" class="uppercase load-community';
                    if(pageProfil.params.dir==e)
                        communityStr+=		' active';
                    communityStr+=		'" data-type-dir="'+e+'">';
                    communityStr+=			'<i class="fa fa-'+v.icon+'"></i> <span class="hidden-xs">'+v.label+'</span>';

                    communityStr +=	 "<span class='badge'>"+countCommunity+"</span>";
                    communityStr +=	'</a>';
                });
                communityStr+="</div>";
            }
            var searchHtml="";
            if($(dom+" #communitySearchContent").length <= 0)
                searchHtml+="<div id='communitySearchContent' class='col-xs-12 no-padding'>";
            searchHtml+=	"<div id='listRoles' class='col-xs-12 no-padding'></div>"+
                "<div id='filterContainer' class='searchObjCSS'></div>"+
                "<div class='headerSearchIncommunity no-padding col-xs-12'></div>"+
                "<div class='bodySearchContainer margin-top-10'>"+
                "<div class='no-padding col-xs-12' id='dropdown_search'>"+
                "</div>"+
                "<div class='no-padding col-xs-12 text-left footerSearchContainer smartgrid-slide-element'></div>"+
                "</div>";
            if($(dom+" #communitySearchContent").length <= 0)
                searchHtml+="</div>";
            if($(dom+" #communitySearchContent").length <= 0)
                $(dom).append(communityStr+searchHtml);
            else
                $(dom+" #communitySearchContent").html(communityStr+searchHtml);
            //$(dom+" #listRoles").hide();
        },
        initEvents: function(){
            $(".load-community").off().on("click",function(){
                //responsiveMenuLeft();
                $(".load-community").removeClass("active");
                $(this).addClass("active");
                pageProfil.params.dir = $(this).data("type-dir");
                coInterface.showLoader("#communitySearchContent");
                //history.pushState remplace le location.hash car il recharge la page .
                //location.hash=hashUrlPage+".view.directory.dir."+pageProfil.params.dir;
                history.pushState(null, null, hashUrlPage+".view.directory.dir."+pageProfil.params.dir);
                pageProfil.views.directory();
                $(dom+' .btn-show-map').hide()
            });
        },
        menuOutCommunity : {
            projects : {
                projects:{
                    label : trad.projects,
                    icon : "lightbulb-o"
                },
                poi:{
                    label : trad.poi,
                    icon : "map-marker"
                },
                crowdfunding:{
                    label : trad.crowdfunding,
                    icon : "money"
                }
            },
            poi : {
                sameAs : "projects"
            },
            crowdfunding:{
                sameAs : "projects"
            }
        },
        communityMenu:{
            citoyens: {
                friends:{
                    label : trad.friends,
                    icon : "user"
                },
                memberOf:{
                    label : trad.organizations,
                    icon : "users"
                },
                follows : {
                    label : trad.follows,
                    icon : "links"
                },
                followers: {
                    label:trad.followers,
                    icon : "rss"
                }
            },
            organizations: {
                members : {
                    label : trad.members,
                    icon : "users"
                },
                memberOf:{
                    label : trad.organizations,
                    icon : "circle"
                },
                followers:{
                    label : trad.followers,
                    icon : "rss"
                }
            },
            projects: {
                contributors : {
                    label : trad.contributors,
                    icon : "users"
                },
                followers:{
                    label : trad.followers,
                    icon : "rss"
                }
            }/*,
			events:{
				"attendees":{
					label : trad.attendees,
					icon : "users"
				},
				"guests":{
					label : trad.guests,
					icon : "envelope"
				}
			}*/
        },
        communityLinks : {
            members:{
                types:[
                    "citoyens",
                    "NGO",
                    "LocalBusiness",
                    "Group",
                    "GovernmentOrganization",
                    "Cooperative"
                ],
                links : "memberOf",
                filters : [
                    "roles",
                    "status"
                ]
            },
            memberOf:{
                types:[
                    "NGO",
                    "LocalBusiness",
                    "Group",
                    "GovernmentOrganization",
                    "Cooperative"
                ],
                links : "members"
            },
            contributors : {
                types:[
                    "citoyens",
                    "NGO",
                    "LocalBusiness",
                    "Group",
                    "GovernmentOrganization",
                    "Cooperative"
                ],
                links : "projects",
                filters : [
                    "roles",
                    "status"
                ]
            },
            attendees : {
                types:[
                    "citoyens",
                    "NGO",
                    "LocalBusiness",
                    "Group",
                    "GovernmentOrganization",
                    "Cooperative"
                ],
                links : "events",
                filters : [
                    "roles",
                    "status"
                ]
            },
            events : {
                types:[
                    "events"
                ],
                filters : {
                    type : {
                        name : trad.category,
                        view : "dropdownList",
                        event : "selectList",
                        type : "type",
                        keyValue : false,
                        list : eventTypes
                    }
                },
                links : "attendees",
                parent : true,
                organizer : true,
                render : "eventPanelHtml",
                loadEvent : {
                    default	: "agenda"
                },
                urlData: baseUrl+"/"+moduleId+"/search/agenda"
            },
            projects : {
                types:[
                    "projects"
                ],
                parent : true,
                links : {
                    "citoyens":	"contributors",
                    "organizations":"contributors",
                    "projects":"projects"
                }
            },
            classifieds : {
                types:[
                    "classifieds"
                ],
                initList :["classifiedsType"],
                filters : {
                    classifiedsType : {
                        view : "horizontalList",
                        event : "selectList",
                        typeList : "object",
                        classDom : "text-center",
                        active : true,
                        type : "type",
                        list : {}
                    }
                },
                parent : true,
                render : "classifiedPanelHtml"
            },
            follows:{
                types:[
                    "citoyens"
                ],
                links : "followers"
            },
            friends:{
                types:[
                    "citoyens"
                ],
                links : "friends"
            },
            followers:{
                types:[
                    "citoyens"
                ],
                links : "follows"
            },
            poi : {
                types:[
                    "poi"
                ],
                parent : true
            },
            crowdfunding : {
                types : [
                    "crowdfunding"
                ],
                type : "campaign",
                render : "crowdfundingPanelHtml",
                parent : true,
                loadEvent : {
                    default : "crowdfunding"
                }
                // 			,
                // 			filters : {
                // 				type : {
                // 		view : "buttonList",
                //         field :"type",
                //         name : "<i class='fa fa-filter'></i> type :",
                //         typeList : "object",
                //         event : "filters",
                //         classList : "pull-left favElBtn btn", 
                //         list :  {
                //         	"Promesse de don" : "pledge",
                //         	"Don" : "donation"
                //         }
                //     }
                // }    
            }
        }
    }
}
function bindButtonMenu(){
    $("#btn-survey").click(function(){
        //$(".ssmla").removeClass('active');
        responsiveMenuLeft(true);
        location.hash=hashUrlPage+".view.survey";
        loadAnswers(false);
        uiCoop.closeUI(false);
    });
}

function getLabelTitleDir(dataName, dataIcon, countData, n){
    mylog.log("getLabelTitleDir", dataName, dataIcon, countData, n, trad);
    var elementName = "<span class='Montserrat' id='name-lbl-title'>"+contextDataAap.name+"</span>";

    var s = (n>1) ? "s" : "";

    //if(countData=='Aucun')
    //	countData=tradException.no;
    var html = "<i class='fa fa-"+dataIcon+" fa-2x margin-right-10'></i> <i class='fa fa-angle-down'></i> ";
    if(dataName == "follows")	{ html += elementName + " "+trad.isfollowing+" " + countData + " "+trad["page"+s]+""; }
    else if(dataName == "followers")	{ html += countData + " <b>"+trad["follower"+s]+"</b> "+trad.to+" "+ elementName; }
    else if(dataName == "members")		{ html += elementName + " "+trad.iscomposedof+" " + countData + " <b>"+trad["member"+s]+"</b>"; }
    else if(dataName == "attendees")	{ html += countData + " <b>"+trad["attendee"+s]+"</b> "+trad.toevent+" " + elementName; }
    else if(dataName == "guests")		{ html += countData + " <b>"+trad["guest"+s]+"</b> "+trad.on+" " + elementName; }
    else if(dataName == "contributors")	{ html += countData + " <b>"+trad["contributor"+s]+"</b> "+trad.toproject+" " + elementName; }
    else if(dataName == "friends")		{ html += elementName + " "+trad["has"]+" " + countData + " <b>"+trad["friend"+s]+"<b>"; }

    else if(dataName == "events"){
        if(contextDataAap.type == "events"){
            html += elementName + " "+trad.iscomposedof+" " + countData+" <b> "+trad["subevent"+s];
        }else{
            html += elementName + " "+trad.takepart+" " + countData+" <b> "+trad["event"+s];
        }
    }
    else if(dataName == "organizations"){ html += elementName + " "+trad.ismemberof+" "+ countData+" <b>"+trad["organization"+s]; }
    else if(dataName == "projects")		{ html += elementName + " "+trad.contributeto+" " + countData+" <b>"+trad["project"+s] }

    else if(dataName == "collections")	{ html += elementName+" "+trad.hasgot+" "+countData+" <b>"+trad["collection"+s]+"</b>"; }
    else if(dataName == "poi")			{ html += countData+" <b>"+trad["point"+s+"interest"+s]+"</b> "+trad['createdby'+s]+" " + elementName; }
    else if(dataName == "crowdfunding")			{ html += countData+" <b>"+trad["crowdfunding"+s+"campaign"+s]+"</b> "+trad['createdby'+s]+" " + elementName; }
    else if(dataName == "classifieds")	{ html += countData+" <b>"+trad["classified"+s]+"</b> "+trad['createdby'+s]+" " + elementName; }
    else if(dataName == "ressources")	{ html += countData+" <b>ressource"+s+"</b> "+trad['createdby'+s]+" " + elementName; }
    else if(dataName == "jobs")	{ html += countData+" <b>"+trad.job+s+"</b> "+trad['createdby'+s]+" " + elementName; }

    else if(dataName == "needs")	{ html += countData+" <b>"+trad[need+s]+"</b> "+trad.of+" " + elementName; }

    else if(dataName == "vote")		{ html += countData+" <b>"+trad[proposal+s]+"</b> "+trad.of+" " + elementName; }
    else if(dataName == "discuss")	{ html += countData+" <b>"+trad.discussion+s+"</b> "+trad.of+" " + elementName; }
    else if(dataName == "actions")	{ html += countData+" <b>"+trad.action+s+"</b> "+trad.of+" " + elementName; }

    else if(dataName == "surveys")	{ html += countData+" <b>"+trad.survey+s+"</b> "+trad['createdby'+s]+" " + elementName; }

    else if(dataName == "actionRooms")	{ html += countData+" <b>espace de décision"+s+"</b> de " + elementName; }
    else if(dataName == "networks")		{ html += countData+" <b>"+trad.map+s+"</b> "+trad.of+" " + elementName;
        if( (typeof openEdition != "undefined" && openEdition == true) || (typeof canEdit != "undefined" && canEdit == true) ){
            html += '<a class="btn btn-sm btn-link bg-green-k pull-right " href="javascript:;" onclick="dyFObj.openForm ( \'network\', \'sub\')">';
            html +=	'<i class="fa fa-plus"></i> '+tradDynForm["Add map"]+'</a>' ;
        }
    }

    if( openEdition || canEdit ){
        if( $.inArray( dataName, ["events","projects","organizations","poi","classifieds", "jobs","collections","actionRooms", "ressources"] ) >= 0 ){
            if(dataName == "collections"){
                html += '<a class="btn btn-sm btn-link bg-green-k pull-right " href="javascript:;" onclick="collection.crud()">';
                html +=	'<i class="fa fa-plus"></i> '+trad.createcollection+'</a>' ;
            }
            else {
                var elemSpec = dyFInputs.get(dataName);
                var formInputBtn='data-form-type="'+elemSpec.ctrl+'"';
                var labelCreate=trad["create"+elemSpec.ctrl];
                if($.inArray( dataName , ["ressources","classifieds","jobs"] ) >= 0){
                    formInputBtn='data-form-type="'+dataName+'"';
                    labelCreate=trad["create"+dataName];
                }


                html += '<button class="btn btn-sm btn-link bg-green-k pull-right btn-open-form" '+formInputBtn+' data-dismiss="modal">';
                html +=	'<i class="fa fa-plus"></i> '+labelCreate+'</button>' ;
            }
        }
    }
    if(dataName != "contacts" && dataName != "collections" &&
        ( costum == null ||
            typeof costum.htmlConstruct.element == "undefined" ||
            typeof costum.htmlConstruct.element.viewMode == "undefined" ||
            costum.htmlConstruct.element.viewMode == true ) ){
        html+='<div class="col-xs-12 text-right no-padding margin-top-5">'+
            '<button class="btn switchDirectoryView ';
        if(directory.viewMode=="list") html+='active ';
        html+=     'margin-right-5" data-value="list"><i class="fa fa-bars"></i></button>'+
            '<button class="btn switchDirectoryView ';
        if(directory.viewMode=="block") html+='active ';
        html+=     '" data-value="block"><i class="fa fa-th-large"></i></button>'+
            '</div>';
    }
    return html;
}



//todo add count on each tag
function getfilterRoles(roles) {
    mylog.log("getfilterRoles roles",roles);
    if(typeof roles == "undefined") {
        $("#listRoles").hide();
        return;
    }

    var nRole = 0;
    $.each( roles,function(k,o){ nRole++; } );
    if(nRole == 0){
        $("#listRoles").hide();
        return;
    }
    $("#listRoles").show(300);
    $("#listRoles").html("<i class='fa fa-filter'></i> "+trad.sortbyrole+": ");
    $("#listRoles").append("<a class='btn btn-link btn-sm letter-blue favElBtn favAllBtn' "+
        "href='javascript:directory.toggleEmptyParentSection(\".favSection\",null,\".searchEntityContainer, .entityLight\",1)'>"+
        " <i class='fa fa-refresh'></i> <b>"+trad["seeall"]+"</b></a>");
    $.each( roles,function(k,o){
        $("#listRoles").append("<a class='btn btn-link btn-sm favElBtn letter-blue "+slugify(k)+"Btn' "+
            "data-tag='"+slugify(k)+"' "+
            "href='javascript:directory.toggleEmptyParentSection(\".favSection\",\"."+slugify(k)+"\",\".searchEntityContainer, .entityLight\",1)'>"+
            k+" <span class='badge'>"+o.count+"</span>"+
            "</a>");
    });
}
function displayInTheContainer(data, dataName, dataIcon, contextType, edit){
    mylog.log("displayInTheContainer",data, dataName, dataIcon, contextType, edit)
    var n=0;
    listRoles={};

    $.each(data, function(key, val){
        mylog.log("rolesShox",key, val);
        if(typeof key != "undefined" && ( (typeof val.id != "undefined" || typeof val["_id"] != "undefined") || contextType == "contacts") || dataName=="collections" ) n++;
        if(typeof val.rolesLink != "undefined"){
            mylog.log(val.rolesLink);
            $.each(val.rolesLink, function(i,v){
                //Push new roles in rolesList
                if(v != "" && !rolesList.includes(v))
                    rolesList.push(v);
                //Incrément and push roles in filter array
                if(v != ""){
                    if(typeof listRoles[v] != "undefined")
                        listRoles[v].count++;
                    else
                        listRoles[v]={"count": 1}
                }
            });
        }
    });
    var communityStr="";
    //console.log(dataName);
    if($.inArray( dataName , ["follows","followers","organizations","members","guests","attendees","contributors","friends"] ) >= 0){
        communityStr+="<div id='menuCommunity' class='col-md-12 col-sm-12 col-xs-12 padding-20'>";
        if(contextDataAap.type == "citoyens" ) {
            communityStr+='<a href="javascript:" class="ssmla uppercase load-coummunity';
            if(dataName=="follows")
                communityStr+=' active';
            communityStr+='" data-type-dir="follows" data-icon="link">'+
                '<i class="fa fa-link"></i> <span class="hidden-xs">'+trad.follows+'</span>';
            if(typeof contextDataAap.links != "undefined" && typeof contextDataAap.links.follows != "undefined")
                communityStr += "<span class='badge'>"+Object.keys(contextDataAap.links.follows).length+"</span>";
            communityStr +=	'</a>';
        }
        countHtml="";
        if(typeof contextDataAap.links != "undefined" && typeof contextDataAap.links[connectTypeElement] != "undefined"){
            countLinks=0;
            countGuests=0;
            $.each(contextDataAap.links[connectTypeElement], function(e,v){
                if(typeof v.isInviting == "undefined")
                    countLinks++;
                else
                    countGuests++;
            });
            countHtml += "<span class='badge'>"+countLinks+"</span>";
        }
        communityStr+=	'<a href="javascript:" class="ssmla uppercase load-coummunity';
        if(dataName==connectTypeElement)
            communityStr+=' active';
        communityStr+='" data-type-dir="'+connectTypeElement+'" data-icon="user">'+
            '<i class="fa fa-user"></i> <span class="hidden-xs">'+trad[connectTypeElement]+"</span>"+
            countHtml+
            '</a>';

        if(contextDataAap.type != "citoyens" && contextDataAap.type != "events") {
            communityStr+='<a href="javascript:" class="ssmla uppercase load-coummunity';
            if(dataName=="followers")
                communityStr+=' active';
            communityStr+='" data-type-dir="followers" data-icon="link">'+
                '<i class="fa fa-link"></i> <span class="hidden-xs">'+trad.followers+'</span>';
            if(typeof contextDataAap.links != "undefined" && typeof contextDataAap.links.followers != "undefined")
                communityStr += "<span class='badge'>"+Object.keys(contextDataAap.links.followers).length+"</span>";
            communityStr +=	'</a>';
        }
        if(contextDataAap.type != "citoyens") {
            communityStr+='<a href="javascript:" class="ssmla uppercase load-coummunity';
            if(dataName=="guests")
                communityStr+=' active';
            communityStr+='" data-type-dir="guests" data-icon="send">'+
                '<i class="fa fa-send"></i> <span class="hidden-xs">'+trad.guests+'</span>';
            if(typeof countGuests != "undefined")
                communityStr += "<span class='badge'>"+countGuests+"</span>";
            communityStr +=	'</a>';
        }
        if (contextDataAap.type=="citoyens" || contextDataAap.type=="places" ){
            communityStr+='<a href="javascript:" class="ssmla uppercase load-coummunity';
            if(dataName=="organizations")
                communityStr+=' active';
            communityStr+='" data-type-dir="organizations" data-icon="group"> '+
                '<i class="fa fa-group"></i> <span class="hidden-xs">'+trad.organizations+'</span>';
            if(typeof contextDataAap.links != "undefined" && typeof contextDataAap.links.memberOf != "undefined")
                communityStr += "<span class='badge'>"+Object.keys(contextDataAap.links.memberOf).length+"</span>";
            communityStr += '</a>';

        }
        communityStr+="</div>";
    }/* TODO REFACTOR COMMUNITY AND MUTUALIZE ALL CLASSIFIEDS
	else if($.inArray( dataName , ["classifieds", "jobs", "ressources"])>=0){
		communityStr+="<div id='menuCommunity' class='col-md-12 col-sm-12 col-xs-12 padding-20'>";
		if(contextDataAap.type == "citoyens" ) {
		communityStr+='<a href="javascript:" class="ssmla uppercase load-coummunity';
			if(dataName=="follows")
				communityStr+=' active';		
		communityStr+='" data-type-dir="follows" data-icon="link">'+
				'<i class="fa fa-link"></i> <span class="hidden-xs">'+trad.follows+'</span>';
				if(typeof contextDataAap.links != "undefined" && typeof contextDataAap.links.follows != "undefined")
		communityStr += "<span class='badge'></span>";
		communityStr +=	'</a>';
		}
		communityStr +='</div>';
	}*/

    mylog.log("displayInTheContainer communityStr", n, communityStr);

    if(n>0){
        var thisTitle = getLabelTitleDir(dataName, dataIcon, parseInt(n), n);

        var html = "";
        html+=communityStr;
        var btnMap = '<button class="btn btn-default btn-sm hidden-xs btn-show-onmap inline" id="btn-show-links-onmap">'+
            '<i class="fa fa-map-marker"></i>'+
            '</button>';

        if(dataName == "networks" || dataName == "surveys") btnMap = "";

        html += "<div class='col-md-12 col-xs-12 margin-bottom-15 labelTitleDir'>";

        mylog.log("eztrzeter", dataName);
        if(dataName != "urls" && dataName != "contacts")
            html += btnMap;

        html +=	thisTitle;
        html += "<div id='listRoles' class='shadow2 col-xs-12'></div>"+
            "<hr>";
        html +=	"</div>";

        if(dataName=="events")
            html += "<div class='col-md-12 col-sm-12 col-xs-12 margin-bottom-10'>"+
                "<a href='javascript:;' id='showHideCalendar' class='text-azure' data-hidden='0'><i class='fa fa-caret-up'></i> Hide calendar</a>"+
                "</div>"+
                "<div id='profil-content-calendar' class='col-md-12 col-sm-12 col-xs-12 margin-bottom-20'></div>";
        mapElements = [];

        mylog.log("listRoles",listRoles);
        //if(dataName != "collections"){
        previewResults="directory.elementPanelHtml";
        if($.inArray(dataName, ["classifieds", "ressources", "jobs"]) >= 0) previewResults="directory.classifiedPanelHtml";
        if(dataName=="events") previewResults="directory.eventPanelHtml";
        if(mapElements.length==0) mapElements = data;
        else $.extend(mapElements, data);
        mylog.log("edit2", edit);
        //data.edit=edit;
        html +="<div id='content-results-profil' class='col-xs-12 col-sm-12 no-padding'>";
        html +="</div>";
        $("#central-container").html(html);
        var smartGridInElt=null;
        if(dataName != "events"){
            smartGridInElt=true;
            var $gridElt=$("#content-results-profil").masonry({
                itemSelector: '.smartgrid-slide-element'
            });
        }
        results=directory.showResultsDirectoryHtml(data, previewResults, smartGridInElt);
        if(smartGridInElt){
            $str=$(results);
            $gridElt.append($str).masonry( 'appended', $str );
            callbackResultsInEltCommunity($gridElt, 0);
        }else{
            $("#content-results-profil").html(results);
        }

        const arr = document.querySelectorAll('img.lzy_img')
        arr.forEach((v) => {
            v.dom = "#content-results-profil";
            imageObserver.observe(v);
        });
        //}

        //$("#centarl-container #content-results-profil").html();

        /*}else{
            $.each(data, function(col, val){
                colName=col;
                if(col=="favorites")
                    colName="favoris";
                html += "<a class='btn btn-default col-xs-12 shadow2 padding-10 margin-bottom-20' onclick='$(\"."+colName+"\").toggleClass(\"hide\")' ><h2><i class='fa fa-star'></i> "+colName+" ("+Object.keys(val.list).length+")</h2></a>"+
                        "<div class='"+colName+" col-sm-12 col-xs-12  hide'>";
                mylog.log("list", val);
                if(val.count==0)
                    html +="<span class='col-xs-12 text-dark margin-bottom-20'>"+trad.noelementinthiscollection+"</span>";
                else{
                    $.each(val.list, function(key, elements){ 
                        if(mapElements.length==0) mapElements = elements;
                        else $.extend(mapElements, elements);
                        html += directory.showResultsDirectoryHtml(elements, "directory.elementPanelHtml");
                    });
                }
                html += "</div>";
            });
        }*/
        toogleNotif(false);
        mylog.log("displayInTheContainer html",html);
        //if(dataName != "collections" && directory.viewMode=="block")
        //  setTimeout(function(){ directory.checkImage(data);}, 300);
        if(dataName == "events"){
            //init calendar view
            calendar.init("#profil-content-calendar");
            mylog.log("calendar data", data);
            calendar.addEvents(data);
            //$(window).on('resize', function(){
            //	$("#profil-content-calendar").fullCalendar('destroy');
            //	calendar.showCalendar("#profil-content-calendar", data, "month");
            //});
            /*$(".fc-button").on("click", function(e){
                 calendar.setCategoryColor();
            })*/
        }
        directory.bindBtnElement();
        directory.bindEventAdmin();

        getfilterRoles(listRoles);
        var dataToMap = data;
        /*if(dataName == "collections"){
            dataToMap = new Array();
            $.each(data, function(key, val){
                $.each(val.list, function(type, list){
                    mylog.log("collection", type, list);
                    $.each(list, function(id, el){
                        dataToMap.push(el);
                    });
                });
            });
        }*/
        mylog.log("displayInTheContainer here");
        mylog.log("dataToMap", dataToMap);
        $("#btn-show-links-onmap").off().click(function(){
            //Sig.showMapElements(Sig.map, dataToMap, "", thisTitle);
            //mapCO.addElts(dataToMap);
            if(typeof mapCO != "undefined")
                showMap();
        });

    }else{
        //var nothing = tradException.no;
        //if(dataName == "organizations" || dataName == "collections" || dataName == "follows")
        //	nothing = tradException.nofem;
        var html =  communityStr+"<div class='col-md-12 col-sm-12 col-xs-12 labelTitleDir margin-bottom-15'>"+
            getLabelTitleDir(dataName, dataIcon, parseInt(n), n)+
            "</div>";
        mylog.log("displayInTheContainer html2", html);
        $("#central-container").html(html + "<span class='col-md-12 alert bold bg-white'>"+
            "<i class='fa fa-ban'></i> "+trad.nodata+
            "</span>");
        toogleNotif(false);
    }
    coInterface.bindButtonOpenForm();
    if(communityStr != ""){
        $(".load-coummunity").off().on("click",function(){
            //responsiveMenuLeft();
            $(".load-coummunity").removeClass("active");
            $(this).addClass("active");
            pageProfil.params.dir = $(this).data("type-dir");
            //history.pushState remplace le location.hash car il recharge la page .
            //location.hash=hashUrlPage+".view.directory.dir."+pageProfil.params.dir;
            history.pushState(null, null, hashUrlPage+".view.directory.dir."+pageProfil.params.dir);
            pageProfil.views.directory();
        });
    }

}


var colNotifOpen = true;
/*function callbackResultsInEltCommunity($grid, timeOut){
	if($(".smartgrid-slide-element img.isLoading").length <= 0){
		$grid.masonry({itemSelector: ".smartgrid-slide-element"});
		//$(" .smartgrid-slide-element").removeClass("last-item-loading");
	}else{
		setTimeout(function(){
			callbackResultsInEltCommunity($grid, timeout+100);
		}, (timeout+100));
	}	
}*/
function toogleNotif(open){
    if(typeof open == "undefined") open = false;

    if(open==false){
        $('#notif-column').removeClass("col-md-3 col-sm-3 col-lg-3").addClass("hidden");
        $('#central-container').removeClass("col-md-9 col-lg-9").addClass("col-md-12 col-lg-12");
    }else{
        $('#notif-column').addClass("col-md-3 col-sm-3 col-lg-3").removeClass("hidden");
        $('#central-container').addClass("col-sm-12 col-md-9 col-lg-9").removeClass("col-md-12 col-lg-12");
    }

    colNotifOpen = open;
}

function descHtmlToMarkdown() {
    mylog.log("htmlToMarkdown");
    if(typeof contextDataAap.descriptionHTML != "undefined" && contextDataAap.descriptionHTML == true) {
        mylog.log("htmlToMarkdown");
        if( $("#descriptionAbout").html() != "" ){
            var paramSpan = {
                filter: ['span'],
                replacement: function(innerHTML, node) {
                    return innerHTML;
                }
            }
            var paramDiv = {
                filter: ['div'],
                replacement: function(innerHTML, node) {
                    return innerHTML;
                }
            }
            mylog.log("htmlToMarkdown2");
            var converters = { converters: [paramSpan, paramDiv] };
            var descToMarkdown = toMarkdown( $("#descriptionMarkdown").html(), converters ) ;
            mylog.log("descToMarkdown", descToMarkdown);
            $("descriptionMarkdown").html(descToMarkdown);
            var param = new Object;
            param.name = "description";
            param.value = descToMarkdown;
            param.id = contextDataAap.id;
            param.typeElement = contextDataAap.type;
            param.block = "toMarkdown";
            ajaxPost(
                null,
                baseUrl+"/"+moduleId+"/element/updateblock/",
                param,
                function(data){ }
            );
            mylog.log("param", param);
        }
    }
}


function removeAddress(form){
    var msg = trad.suredeletelocality ;
    if(!form && contextDataAap.type == personCOLLECTION)
        msg = trad.suredeletepersonlocality ;

    bootbox.confirm({
        message: msg + "<span class='text-red'></span>",
        buttons: {
            confirm: {
                label: trad.yes,
                className: 'btn-success'
            },
            cancel: {
                label: trad.no,
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (!result) {
                return;
            } else {
                param = new Object;
                param.name = "locality";
                param.value = "";
                param.pk = contextDataAap.id;
                ajaxPost(
                    null,
                    baseUrl+"/"+moduleId+"/element/updatefields/type/"+contextDataAap.type,
                    param,
                    function(data){
                        if(data.result && !form){
                            if(contextDataAap.type == personCOLLECTION) {
                                //Menu Left
                                $("#btn-geoloc-auto-menu").attr("href", "javascript:");
                                $('#btn-geoloc-auto-menu > span.lbl-btn-menu').html("Communectez-vous");
                                $("#btn-geoloc-auto-menu").attr("onclick", "communecterUser()");
                                $("#btn-geoloc-auto-menu").off().removeClass("lbh");
                                //Dashbord
                                $("#btn-menuSmall-mycity").attr("href", "javascript:");
                                $("#btn-menuSmall-citizenCouncil").attr("href", "javascript:");
                                //Multiscope
                                $(".msg-scope-co").html("<i class='fa fa-cogs'></i> Paramétrer mon code postal</a>");
                                //MenuSmall
                                $(".hide-communected").show();
                                $(".visible-communected").hide();

                                $(".communecter-btn").removeClass("hidden");
                            }
                            myScopes.communexion={};
                            localStorage.setItem("myScopes",JSON.stringify(myScopes));
                            toastr.success(data.msg);
                            urlCtrl.loadByHash("#page.type."+contextDataAap.type+".id."+contextDataAap.id+".view.detail");
                        }
                    }
                );
            }
        }
    });
}
