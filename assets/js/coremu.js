const documentReadyPromise = new Promise(function (resolve) {
    if (document.readyState === 'complete') {
        resolve();
    } else {
        function onReady() {
            resolve();
            document.removeEventListener('DOMContentLoaded', onReady, true);
            window.removeEventListener('load', onReady, true);
        }
        document.addEventListener('DOMContentLoaded', onReady, true);
        window.addEventListener('load', onReady, true);
    }
});
var coremuObj = {

    answerId : '',
    userId :'',
    budgetContainer: "#coremutbody",
    financementContainer : '#coremufinancementbody',
    sankeyfinancementContainer : '#sankeyfinancement',
    financementDistrPieContainer : '#financementDistrPie',
    financementTagsPieContainer : '#financementTagsPie',
    candidatePieContainer : '#candidatePieContainer',
    bootBcontainer: "#bootBcontainer",
    cache : {},
    dashboardCache : {},
    answerObjdepense : {},
    users : {},
    userRoles : {},
    community : [],
    status : "proposition",
    isUpdating : false,
    actualcommentId : "",
    init : async function (coremuObj , answerId ){
        if(documentReadyPromise) {
            await documentReadyPromise;
        }

        coremuObj.answerId = answerId;
        if($.isEmptyObject(coremuObj.cache)) {
            coremuObj.loadCommunity(coremuObj);
            var answerData = coremuObj.getData(coremuObj, answerId, false);

            if(notNull(answerData["project"]) && notNull(answerData["project"]["id"])){
                coremuObj.status = "project";
            }
            coremuObj.cache = coremuObj.arrageData(answerData);
            coremuObj.userId = userId;
            if ($(coremuObj.budgetContainer).length > 0) {
                coremuObj.loadBudgetTable(coremuObj);
            }
            if ($(coremuObj.financementContainer).length > 0) {
                coremuObj.loadFinancementTable(coremuObj);
            }
            if ($(coremuObj.sankeyfinancementContainer).length > 0) {
                coremuObj.loadFinancementSankey(coremuObj);
            }
            if ($(coremuObj.financementDistrPieContainer).length > 0) {
                coremuObj.loadFinancementDistrPie(coremuObj);
            }
            if ($(coremuObj.financementTagsPieContainer).length > 0) {
                coremuObj.loadFinancementTagsPie(coremuObj);
            }
            if ($(coremuObj.candidatePieContainer).length > 0) {
                coremuObj.loadCandidatePie(coremuObj);
            }
            setTimeout(() => {
                coremuObj.event.init(coremuObj);
            }, "2000")
            coremuObj.event.onReadyCallbacks(coremuObj);
            coremuObj.event.initWebSocket(coremuObj, coWsConfig, answerId);
        }
        commentObj.afterSaveReload = function(){
            return null;
        }
    },

    loadCommunity : function (coremuObj) {
        if (notNull(coremuObj.answerId) && coremuObj.community.length == 0) {
            if ($.isEmptyObject(coremuObj.community)){
                var coremuData = coremuObj.getData(coremuObj, answerId , false);
                if(
                    notNull(coremuData.parent) &&
                    notNull(coremuData.parent.links) &&
                    notNull(coremuData.parent.links.members)
                ) {
                    coremuObj.community = coremuData.parent.links.members
                }
                $.each(coremuObj.community, function(index, value){
                    if (value.type == "citoyens" && !notNull(coremuObj.users[index])){
                        /*ajaxPost(
                            '',
                            baseUrl + "/co2/element/get/type/citoyens/id/" + index,
                            null,
                            function (data) {
                                if (
                                    typeof data.map != "undefined"
                                ) {
                                    coremuObj.users[index] = data.map;
                                }
                            },
                            null,
                            null,
                            {"async": true}
                        );*/
                    }
                    else if (value.type != "citoyens" ){
                        ajaxPost(
                            '',
                            baseUrl + "/co2/element/get/type/"+value.type+"/id/" + index,
                            null,
                            function (data) {
                                if (
                                    typeof data.map != "undefined"
                                ) {
                                    coremuObj.community[index] = data.map;
                                    if(notNull(value.roles)){
                                        coremuObj.community[index].roles = value.roles;
                                    }
                                }
                            },
                            null,
                            null,
                            {"async": false}
                        );
                    }
                });
            }
        }
    },

    loadBudgetTable : function (coremuObj){
        if (notNull(coremuObj.cache.arrageDepense)) {
            $.each(coremuObj.cache.arrageDepense, function (index, value) {
                var $card = $(coremuObj.createTrLineBudget(coremuObj, index));

                //$card.css({ opacity: 0, scale: 0.5 }).velocity({ opacity: 1, scale: 1 }, { duration: 500 });

                $(coremuObj.budgetContainer).append($card);
            });

            var $cardfooter = $(coremuObj.createTrFooterBudget(coremuObj));
            var $cardfootervalidee = $(coremuObj.createTrFooterBudgetValid(coremuObj));

            //$cardfooter.css({ opacity: 0, scale: 0.5 }).velocity({ opacity: 1, scale: 1 }, { duration: 500 });
            //$cardfootervalidee.css({ opacity: 0, scale: 0.5 }).velocity({ opacity: 1, scale: 1 }, { duration: 500 });

            $(coremuObj.budgetContainer).append($cardfooter);
            $(coremuObj.budgetContainer).append($cardfootervalidee);

        }
    },

    loadFinancementTable : function (coremuObj){
        if (notNull(coremuObj.cache.arrageDepense)) {
            $.each(coremuObj.cache.arrageDepense, function (index, value) {
                var $card = $(coremuObj.createTrLineFinancement(coremuObj, index));

                //$card.css({ opacity: 0, scale: 0.5 }).velocity({ opacity: 1, scale: 1 }, { duration: 500 });

                $(coremuObj.financementContainer).append($card);

            });
        }
        var $cardfooter = $(coremuObj.createTrFooterFinancement(coremuObj));
        $(coremuObj.financementContainer).append($cardfooter);

    },

    loadFinancementSankey : function (coremuObj){
        if (notNull(coremuObj.financementSankey)) {
            coremuObj.block.sankey(coremuObj , coremuObj.sankeyfinancementContainer , null , coremuObj.financementSankey , ["Financeur" , "Depenses" , "Candidats"]);
        }
    },

    loadFinancementDistrPie : function (coremuObj){
        if (notNull(coremuObj.financementDistrPie)) {
            coremuObj.block.pie(coremuObj , coremuObj.financementDistrPieContainer , null , coremuObj.financementDistrPie , "€");
        }
    },

    loadFinancementTagsPie : function (coremuObj){
        if (notNull(coremuObj.financementTagsPie)) {
            coremuObj.block.pie(coremuObj , coremuObj.financementTagsPieContainer , null , coremuObj.financementTagsPie ,"€");
        }
    },

    loadCandidatePie : function (coremuObj){
        if (notNull(coremuObj.candidatePie)) {
            coremuObj.block.pie(coremuObj , coremuObj.candidatePieContainer , null , coremuObj.candidatePie);
        }
    },

    loadGlobalDashboard : function(coremuObj){
        var numberWithCommas = function(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")+" €";
        };

        if(notNull(allDataAns) && notNull(aapObj)) {
            var arData = coremuObj.getDataDash(coremuObj , null , allDataAns );
            var queryParams = aapObj.common.getQuery(aapObj);
            if (notNull(arData.number)) {
                $("#dashnumber").html(arData.number);
            }
            if (notNull(arData.total)) {
                $("#dashtotal").html(numberWithCommas(arData.total));
            }
            if (notNull(arData.totalFinancement)) {
                $("#dashtotalFinancement").html(numberWithCommas(arData.totalFinancement));
            }
            if (notNull(arData.totalValidee)) {
                $("#dashtotalValidee").html(numberWithCommas(arData.totalValidee));
            }
            if (notNull(arData.totalansweraction)) {
                $("#dashtotalansweraction").html(arData.totalansweraction);
            }
            if (notNull(arData.totalcandidattovalid)) {
                $("#dashtotalcandidattovalid").html(arData.totalcandidattovalid);
            }
            if (notNull(arData.totalleft)) {
                $("#dashtotalleft").html(numberWithCommas(arData.totalleft));
            }
            if (notNull(arData.totalneedcandidat)) {
                $("#dashtotalneedcandidat").html(arData.totalneedcandidat);
            }
            if (notNull(arData.totalpricetovalid)) {
                $("#dashtotalpricetovalid").html(arData.totalpricetovalid);
            }
            if (notNull(arData.financementTagsPie)) {
                $("#dashfinancementTagsPie").html("");
                coremuObj.block.pie(coremuObj, "#dashfinancementTagsPie", null, arData.financementTagsPie , " €");
            }
            if (notNull(arData.financementDistrPie)) {
                $("#dashfinancementDistrPie").html("");
                coremuObj.block.pie(coremuObj, "#dashfinancementDistrPie", null, arData.financementDistrPie , " €");
            }
            if (notNull(arData.candidatePie)) {
                $("#dashcandidatePie").html("");
                coremuObj.block.pie(coremuObj, "#dashcandidatePie", null, arData.candidatePie , " €");
            }
            if (notNull(arData.financementSankey)) {
                $("#dashfinancementSankey").html("");
                coremuObj.block.sankey(coremuObj, "#dashfinancementSankey", null, arData.financementSankey , ["Financeur" , "Proposition" ]);
            }
            if (notNull(arData.candidateHour)) {
                $("#dashcandidateHour").html("");
                coremuObj.block.pie(coremuObj, "#dashcandidateHour", null, arData.candidateHour , " heure(s)");
            }

            if (notNull(arData.projecttotal)) {
                $("#dashprojecttotal").html("sur "+arData.projecttotal + " projet(s)");
            }
            if (notNull(arData.projecttotalFinancement)) {
                $("#dashprojecttotalFinancement").html("sur "+arData.projecttotalFinancement + " projet(s)");
            }
            if (notNull(arData.projecttotalValidee)) {
                $("#dashprojecttotalValidee").html("sur "+arData.projecttotalValidee + " projet(s)");
            }
            if (notNull(arData.projecttotalFinancement)) {
                $("#dashprojecttotalFinancement").html("sur "+arData.projecttotalFinancement + " projet(s)");
            }
            if (notNull(arData.projecttotalansweraction)) {
                $("#dashprojecttotalansweraction").html("sur "+arData.projecttotalansweraction + " projet(s)");
            }
            if (notNull(arData.projecttotalFinancement)) {
                $("#dashprojecttotalFinancement").html("sur "+arData.projecttotalFinancement + " projet(s)");
            }
            if (notNull(arData.projecttotalcandidattovalid)) {
                $("#dashprojecttotalcandidattovalid").html("sur "+arData.projecttotalcandidattovalid + " projet(s)");
            }
            if (notNull(arData.projecttotalneedcandidat)) {
                $("#dashprojecttotalneedcandidat").html("sur "+arData.projecttotalneedcandidat + " projet(s)");
            }
            if (notNull(arData.projecttotalpricetovalid)) {
                $("#dashprojecttotalpricetovalid").html("sur "+arData.projecttotalpricetovalid + " projet(s)");
            }
            if (notNull(arData.projecttotalm)) {
                $("#dashprojecttotalm").html("sur "+arData.projecttotalm + " projet(s)");
            }

            if (notNull(arData.projecttotalname)) {
                $("#dashprojecttotalname").html("");
                $.each(arData.projecttotalname, function(i, name) {
                     if(name != "" && name != " " ) {
                        $("#dashprojecttotal").append('<li class="openproposalmodal" data-answer="'+`${arData.dashname[name]}`+'"  data-url="'+`#detailProposalAap.context.${aapObj.context.slug}.formid.${aapObj.form._id.$id}.answerId.${queryParams?.answerId}.projectId.${queryParams.projectId}`+'" data-formid = "' + fp["_id"]["$id"] + '" data-id="' + arData.dashname[name] + '"> ' + name + ' </li>');
                    }
                });
            }
            if (notNull(arData.projecttotalFinancementname)) {
                $("#dashprojecttotalFinancementname").html("");
                $.each(arData.projecttotalFinancementname, function(i, name) {
                     if(name != "" && name != " " ) {
                        $("#dashprojecttotalFinancementname").append('<li class="openproposalmodal" data-answer="'+`${arData.dashname[name]}`+'"  data-url="'+`#detailProposalAap.context.${aapObj.context.slug}.formid.${aapObj.form._id.$id}.answerId.${queryParams?.answerId}.projectId.${queryParams.projectId}`+'" data-formid = "' + fp["_id"]["$id"] + '" data-id="' + arData.dashname[name] + '"> ' + name + ' </li>');
                    }
                });
            }
            if (notNull(arData.projecttotalValideename)) {
                $("#dashprojecttotalValideename").html("");
                $.each(arData.projecttotalValideename, function(i, name) {
                     if(name != "" && name != " " ) {
                        $("#dashprojecttotalValideename").append('<li class="openproposalmodal" data-answer="'+`${arData.dashname[name]}`+'"  data-url="'+`#detailProposalAap.context.${aapObj.context.slug}.formid.${aapObj.form._id.$id}.answerId.${queryParams?.answerId}.projectId.${queryParams.projectId}`+'" data-formid = "' + fp["_id"]["$id"] + '" data-id="' + arData.dashname[name] + '"> ' + name + ' </li>');
                    }
                });
            }
            if (notNull(arData.projecttotalFinancementname)) {
                $("#dashprojecttotalFinancementname").html("");
                $.each(arData.projecttotalFinancementname, function(i, name) {
                     if(name != "" && name != " " ) {
                        $("#dashprojecttotalFinancementname").append('<li class="openproposalmodal" data-answer="'+`${arData.dashname[name]}`+'"  data-url="'+`#detailProposalAap.context.${aapObj.context.slug}.formid.${aapObj.form._id.$id}.answerId.${queryParams?.answerId}.projectId.${queryParams.projectId}`+'" data-formid = "' + fp["_id"]["$id"] + '" data-id="' + arData.dashname[name] + '"> ' + name + ' </li>');
                    }
                });
            }
            if (notNull(arData.projecttotalansweractionname)) {
                $("#dashprojecttotalansweractionname").html("");
                $.each(arData.projecttotalansweractionname, function(i, name) {
                     if(name != "" && name != " " ) {
                        $("#dashprojecttotalansweractionname").append('<li class="openproposalmodal" data-answer="'+`${arData.dashname[name]}`+'"  data-url="'+`#detailProposalAap.context.${aapObj.context.slug}.formid.${aapObj.form._id.$id}.answerId.${queryParams?.answerId}.projectId.${queryParams.projectId}`+'" data-formid = "' + fp["_id"]["$id"] + '" data-id="' + arData.dashname[name] + '"> ' + name + ' </li>');
                    }
                });
            }
            if (notNull(arData.projecttotalFinancementname)) {
                $("#dashprojecttotalFinancementname").html("");
                $.each(arData.projecttotalFinancementname, function(i, name) {
                     if(name != "" && name != " " ) {
                        $("#dashprojecttotalFinancementname").append('<li class="openproposalmodal" data-answer="'+`${arData.dashname[name]}`+'"  data-url="'+`#detailProposalAap.context.${aapObj.context.slug}.formid.${aapObj.form._id.$id}.answerId.${queryParams?.answerId}.projectId.${queryParams.projectId}`+'" data-formid = "' + fp["_id"]["$id"] + '" data-id="' + arData.dashname[name] + '"> ' + name + ' </li>');
                    }
                });
            }
            if (notNull(arData.projecttotalcandidattovalidname)) {
                $("#dashprojecttotalcandidattovalidname").html("");
                $.each(arData.projecttotalcandidattovalidname, function(i, name) {
                     if(name != "" && name != " " ) {
                        $("#dashprojecttotalcandidattovalidname").append('<li class="openproposalmodal" data-answer="'+`${arData.dashname[name]}`+'"  data-url="'+`#detailProposalAap.context.${aapObj.context.slug}.formid.${aapObj.form._id.$id}.answerId.${queryParams?.answerId}.projectId.${queryParams.projectId}`+'" data-formid = "' + fp["_id"]["$id"] + '" data-id="' + arData.dashname[name] + '"> ' + name + ' </li>');
                    }
                });
            }
            if (notNull(arData.projecttotalneedcandidatname)) {
                $("#dashprojecttotalneedcandidatname").html("");
                $.each(arData.projecttotalneedcandidatname, function(i, name) {
                      if(name != "" && name != " " ) {
                         $("#dashprojecttotalneedcandidatname").append('<li class="openproposalmodal" data-answer="'+`${arData.dashname[name]}`+'"  data-url="'+`#detailProposalAap.context.${aapObj.context.slug}.formid.${aapObj.form._id.$id}.answerId.${queryParams?.answerId}.projectId.${queryParams.projectId}`+'" data-formid = "' + fp["_id"]["$id"] + '" data-id="' + arData.dashname[name] + '"> ' + name + ' </li>');
                     }
                });
            }
            if (notNull(arData.projecttotalpricetovalidname)) {
                $("#dashprojecttotalpricetovalidname").html("");
                $.each(arData.projecttotalpricetovalidname, function(i, name) {
                    if(name != "" && name != " " ) {
                        $("#dashprojecttotalpricetovalidname").append('<li class="openproposalmodal" data-answer="'+`${arData.dashname[name]}`+'"  data-url="'+`#detailProposalAap.context.${aapObj.context.slug}.formid.${aapObj.form._id.$id}.answerId.${queryParams?.answerId}.projectId.${queryParams.projectId}`+'" data-formid = "' + fp["_id"]["$id"] + '" data-id="' + arData.dashname[name] + '"> ' + name + ' </li>');
                    }
                });
            }
            if (notNull(arData.projecttotalmname)) {
                $("#dashprojecttotalmname").html("");
                $.each(arData.projecttotalmname, function(i, name) {
                    if(name != "" && name != " " ) {
                        $("#dashprojecttotalmname").append('<li class="openproposalmodal" data-answer="'+`${arData.dashname[name]}`+'"  data-url="'+`#detailProposalAap.context.${aapObj.context.slug}.formid.${aapObj.form._id.$id}.answerId.${queryParams?.answerId}.projectId.${queryParams.projectId}`+'" data-formid = "' + fp["_id"]["$id"] + '" data-id="' + arData.dashname[name] + '"> ' + name + ' </li>');
                    }
                });
            }
            if (notNull(arData.totalname)) {
                $("#dashtotalname").html("");
                $.each(arData.totalname, function(i, name) {
                    if(name != "" && name != " " ) {
                        $("#dashtotalname").append('<li class="openproposalmodal" data-answer="'+`${arData.dashname[name]}`+'"  data-url="'+`#detailProposalAap.context.${aapObj.context.slug}.formid.${aapObj.form._id.$id}.answerId.${queryParams?.answerId}.projectId.${queryParams.projectId}`+'" data-formid = "' + fp["_id"]["$id"] + '" data-id="' + arData.dashname[name] + '"> ' + name + ' </li>');
                    }
                });
            }
        }
    },

    getData : function(coremuObj , answerid , iSanswerSaving){
        var coremuData = {
                "depense": {},
                "form": {},
                "parent": {},
                "badgeList": {},
                "project": {},
                "comment": {},
                "titre": ""
            };

        ajaxPost(
            '',
            baseUrl + "/co2/element/get/type/answers/id/" + answerid,
            null,
            function (data) {
                if (
                    typeof data.map != "undefined" &&
                    typeof data.map.answers != "undefined" &&
                    typeof data.map.answers.aapStep1 != "undefined" &&
                    typeof data.map.answers.aapStep1.titre != "undefined"
                ){
                    coremuData.titre = data.map.answers.aapStep1.titre;
                }
                if (
                    typeof data.map != "undefined" &&
                    typeof data.map.answers != "undefined" &&
                    typeof data.map.answers.aapStep1 != "undefined" &&
                    typeof data.map.answers.aapStep1.depense != "undefined"
                ){
                    coremuData["depense"] =  data.map.answers.aapStep1.depense;
                    if(!iSanswerSaving){
                        $.each(coremuData.depense, function(index, value){
                            ajaxPost(
                                null,
                                baseUrl+'/'+moduleId+"/comment/countcommentsfrom",
                                {
                                    "type" : "answers",
                                    "id" : answerId,
                                    "path" : answerId+"depense"+index
                                },
                                function (data){
                                    if (typeof data.count != "undefined") {
                                        coremuData.comment[answerId+"depense"+index] = data.count;
                                    }
                                },
                                null,
                                null,
                                {"async" : false}
                            );
                        });
                    }else{
                        coremuData["comment"] = coremuObj.cache.comment;
                    }
                }

                if (!iSanswerSaving) {
                    if (
                        typeof data.map != "undefined" &&
                        typeof data.map.project != "undefined"
                    ) {
                        coremuData.project = data.map.project;
                    }

                    if (
                        typeof data.map != "undefined" &&
                        typeof data.map.form != "undefined"
                    ) {
                        ajaxPost(
                            '',
                            baseUrl + "/co2/element/get/type/forms/id/" + data.map.form,
                            null,
                            function (data) {
                                if (
                                    typeof data.map != "undefined" &&
                                    typeof data.map.parent != "undefined"
                                ) {
                                    coremuData["form"] = data.map;
                                    var parent = {};
                                    ajaxPost(
                                        '',
                                        baseUrl + "/co2/element/get/type/" + data.map.parent[Object.keys(data.map.parent)[0]].type + "/id/" + Object.keys(data.map.parent)[0],
                                        null,
                                        function (data) {
                                            if (
                                                typeof data.map != "undefined"
                                            ) {
                                                coremuData["parent"] = data.map
                                            }
                                        },
                                        null,
                                        null,
                                        {"async": false}
                                    );

                                    var filtersText = {};

                                    filtersText["parent." + Object.keys(data.map.parent)[0]] = {'$exists': true};

                                    ajaxPost(
                                        null,
                                        baseUrl + "/" + moduleId + "/search/globalautocomplete",
                                        {
                                            notSourceKey: true,
                                            "searchType": ["badges"],
                                            filters: filtersText
                                        },
                                        function (data) {
                                            coremuData["badgeList"] = data;
                                        },
                                        null,
                                        null,
                                        {"async": false}
                                    );
                                }
                            },
                            null,
                            null,
                            {"async": false}
                        );
                    }

                } else {
                    coremuData["form"] = coremuObj.cache.form;
                    coremuData["parent"] = coremuObj.cache.parent;
                    coremuData["badgeList"] = coremuObj.cache.badgeList;
                    coremuData["project"] = coremuObj.cache.project;
                }

            },
            null,
            null,
            {"async" : false}
        );

        coremuObj.answerObjdepense = coremuData;

        return coremuData;

    },

    getDataDash : function(coremuObj , formid , allAnswer = null ){
        var coremuData = {
            depense : {},
            number : 0,
            total : 0,
            totalValidee : 0,
            totalFinancement : 0,
            totalValidee : 0,
            totalFinancement : 0,
            totalansweraction : 0,
            totalcandidattovalid : 0,
            totalleft : 0,
            totalneedcandidat : 0,
            totalpricetovalid : 0,
            financementDistrPie : [],
            financementTagsPie : [],
            candidatePie : [],
            financementSankey : {
                "nodes" : [], "links" : []
            },
            candidateHour : [],

            projecttotal : 0,
            projecttotalFinancement : 0,
            projecttotalValidee : 0,
            projecttotalFinancement : 0,
            projecttotalansweraction : 0,
            projecttotalcandidattovalid : 0,
            projecttotalleft : 0,
            projecttotalneedcandidat : 0,
            projecttotalpricetovalid : 0,
            projecttotalm : 0,

            totalname : [],
            projecttotalname : [],
            projecttotalFinancementname : [],
            projecttotalValideename : [],
            projecttotalFinancementname : [],
            projecttotalansweractionname : [],
            projecttotalcandidattovalidname : [],
            projecttotalleftname : [],
            projecttotalneedcandidatname : [],
            projecttotalpricetovalidname : [],
            projecttotalmname : [],

            dashname : {}



        };

        $.each(allAnswer, function(index, value){
            coremuData.number ++;
            var titre = "";
            if(Array.isArray(value) && value.length > 0 ){
                titre = value[0]["titre"]
            }else if(!Array.isArray(value)){
                var firstValue = Object.keys(value);
                titre = value[firstValue[0]]["titre"]
            }
            coremuData.depense[index] = coremuObj.arrageData({
                "depense" : value ,
                "titre" : titre ,
                "parent" : parentDashboard
            });

            coremuData.dashname[titre] = index;

            coremuData.totalname.push(titre);

            if(!isNaN(coremuData.depense[index].total)) {
                coremuData.total += coremuData.depense[index].total;
            }
            if(!isNaN(coremuData.depense[index].totalFinancement)) {
                coremuData.totalFinancement += coremuData.depense[index].totalFinancement;
            }
            if(!isNaN(coremuData.depense[index].totalValidee)) {
                coremuData.totalValidee += coremuData.depense[index].totalValidee;
            }
            if(!isNaN(coremuData.depense[index].totalFinancement)) {
                coremuData.totalFinancement += coremuData.depense[index].totalFinancement;
            }
            if(!isNaN(coremuData.depense[index].totalansweraction)) {
                coremuData.totalansweraction += coremuData.depense[index].totalansweraction;
            }
            if(!isNaN(coremuData.depense[index].totalcandidattovalid)) {
                coremuData.totalcandidattovalid += coremuData.depense[index].totalcandidattovalid;
            }
            if(!isNaN(coremuData.depense[index].totalleft)) {
                coremuData.totalleft += coremuData.depense[index].totalleft;
            }
            if(!isNaN(coremuData.depense[index].totalneedcandidat)) {
                coremuData.totalneedcandidat += coremuData.depense[index].totalneedcandidat;
            }
            if(!isNaN(coremuData.depense[index].totalpricetovalid)) {
                coremuData.totalpricetovalid += coremuData.depense[index].totalpricetovalid;
            }

            $.each(coremuData.depense[index].arrageDepense , function(index2,value2) {
                coremuData.number++;
            });

            coremuData.projecttotal++;

            if(coremuData.depense[index].total != 0){
                coremuData.projecttotalm++;
                coremuData.projecttotalmname.push(titre);
            }

            if(coremuData.depense[index].totalFinancement != 0 ){
                coremuData.projecttotalFinancement++;
                coremuData.projecttotalFinancementname.push(titre);
            }
            if(coremuData.depense[index].totalValidee != 0){
                coremuData.projecttotalValidee++;
                coremuData.projecttotalValideename.push(titre);
            }
            if(coremuData.depense[index].totalFinancement != 0){
                coremuData.projecttotalFinancement++;
                coremuData.projecttotalFinancementname.push(titre);
            }
            if(coremuData.depense[index].totalansweraction != 0){
                coremuData.projecttotalansweraction++;
                coremuData.projecttotalansweractionname.push(titre);
            }
            if(coremuData.depense[index].totalcandidattovalid != 0){
                coremuData.projecttotalcandidattovalid++;
                coremuData.projecttotalcandidattovalidname.push(titre);
            }
            if(coremuData.depense[index].totalleft != 0){
                coremuData.projecttotalleft++;
                coremuData.projecttotalleftname.push(titre);
            }
            if(coremuData.depense[index].totalneedcandidat != 0){
                coremuData.projecttotalneedcandidat++;
                coremuData.projecttotalneedcandidatname.push(titre);
            }
            if(coremuData.depense[index].totalpricetovalid != 0){
                coremuData.projecttotalpricetovalid++;
                coremuData.projecttotalpricetovalidname.push(titre);
            }

            $.each(coremuObj.candidatePie , function(index2,value2){
                if(!coremuData.candidatePie.some(node => node.name === value2.name)){
                    coremuData.candidatePie.push(value2)
                }else {
                    var indexOfNodes = coremuObj.candidatePie.map(function (el) {
                                        return el.name;
                    });
                    coremuObj.candidatePie[indexOfNodes.indexOf(value2.name)]["value"] += parseInt(value2.value);
                }
            });

            $.each(coremuObj.financementDistrPie , function(index2,value2){
                if(!coremuData.financementDistrPie.some(node => node.name === value2.name)){
                    coremuData.financementDistrPie.push(value2)
                }else {
                    var indexOfNodes = coremuObj.financementDistrPie.map(function (el) {
                        return el.name;
                    });
                    coremuObj.financementDistrPie[indexOfNodes.indexOf(value2.name)]["value"] += parseInt(value2.value);
                }
            });

            $.each(coremuObj.financementTagsPie , function(index2,value2){
                if(!coremuData.financementTagsPie.some(node => node.name === value2.name)){
                    coremuData.financementTagsPie.push(value2)
                }else {
                    var indexOfNodes = coremuObj.financementTagsPie.map(function (el) {
                        return el.name;
                    });
                    coremuObj.financementTagsPie[indexOfNodes.indexOf(value2.name)]["value"] += parseInt(value2.value);
                }
            });

            if(
                notNull(fp) &&
                notNull(fp.parent)
            ) {
                ajaxPost(
                    '',
                    baseUrl + "/co2/element/get/type/"+fp.parent[Object.keys(fp.parent)[0]].type+"/id/" + Object.keys(fp.parent)[0],
                    null,
                    function (data) {
                        if (
                            typeof data.map != "undefined"
                            && typeof data.map.links != "undefined"
                            && typeof data.map.links.members != "undefined"
                        ) {
                            coremuObj.community = data.map.links.members
                        }
                    },
                    null,
                    null,
                    {"async": false}
                );
            }
            $.each(coremuObj.community, function(index, value){
                if (value.type == "citoyens" && !notNull(coremuObj.users[index])){
                    ajaxPost(
                        '',
                        baseUrl + "/co2/element/get/type/citoyens/id/" + index,
                        null,
                        function (data) {
                            if (
                                typeof data.map != "undefined"
                            ) {
                                coremuObj.users[index] = data.map;
                            }
                        },
                        null,
                        null,
                        {"async": false}
                    );
                }
                else if (value.type != "citoyens" ){
                    ajaxPost(
                        '',
                        baseUrl + "/co2/element/get/type/"+value.type+"/id/" + index,
                        null,
                        function (data) {
                            if (
                                typeof data.map != "undefined"
                            ) {
                                coremuObj.community[index] = data.map;
                                if(notNull(value.roles)){
                                    coremuObj.community[index].roles = value.roles;
                                }
                            }
                        },
                        null,
                        null,
                        {"async": false}
                    );
                }
            });

            if(notNull(coremuData.depense[index].arrageDepense)) {
                $.each(coremuData.depense[index].arrageDepense, function (index2, value2) {
                    $.each(value2.candidateEstimate, function (candidateId, can){
                        if(typeof can.AssignBudgetArray != "undefined") {
                            $.each(can.AssignBudgetArray, function (abudgId, abudg) {
                                if(typeof abudg.hour != "undefined" && typeof can.name != "undefined") {

                                    if (!coremuData.candidateHour.some(node => node.name === can.name.trimStart())) {
                                        coremuData.candidateHour.push({
                                            "name": can.name.trimStart(),
                                            "value": parseInt(abudg.hour)
                                        });
                                    } else {
                                        var indexOfNodes = coremuData.candidateHour.map(function (el) {
                                            return el.name;
                                        });
                                        coremuData.candidateHour[indexOfNodes.indexOf(can.name.trimStart())]["value"] += parseInt(abudg.hour);
                                    }
                                }
                            });
                        }
                    });
                    $.each(value2.financement, function (financementId, fin) {
                        if (typeof fin.amount != 'undefined' && $.isNumeric(fin.amount)) {

                            var financername = "";
                            if(notNull(fin.name)){
                                financername = fin.name
                            }else if (notNull(parentDashboard) && notNull(parentDashboard.name)) {
                                financername = parentDashboard.name;
                            }

                            if (!coremuData.financementSankey.nodes.some(node => node.name === financername)) {
                                coremuData.financementSankey.nodes.push({"name": financername, "level": 0})
                            }

                            if (!coremuData.financementSankey.nodes.some(node => node.name === titre)) {
                                coremuData.financementSankey.nodes.push({
                                    "name": titre,
                                    "level": 1
                                })
                            }

                            var indexOfNodes = coremuData.financementSankey.nodes.map(function (el) {
                                return el.name;
                            });

                            coremuData.financementSankey.links.push({
                                "source": financername,
                                "target": coremuData.depense[index].titre,
                                "value": parseInt(fin.amount)
                            })
                        }
                    });
                });
            }
        });
        var links2 = [];
        var indexOfNodes = coremuData.financementSankey.nodes.map(function (el) {
            return el.name;
        });
        $.each(coremuData.financementSankey.links , function(index3,value3){
            links2.push({
                "source" : indexOfNodes.indexOf(value3.source),
                "target" : indexOfNodes.indexOf(value3.target),
                "value" : value3.value
            });
        });
        coremuData.financementSankey.links = coremuObj.fusionSankeyLinks(links2);

        return coremuData;

    },

    isValidMongoId: function(id) {
        return id.length === 24 &&  !isNaN(Number('0x' + id))
    },

    arrageData : function (data) {

        var titre = "";
        if(notNull(data.titre)){
            titre = data.titre;
        }
        var total = 0;
        var totalFinancement = 0;
        var totalValidee = 0;
        var arrageDepense = {};
        var badgeList = {};
        var form = {};
        var parent = {};
        var comment = {};

        var totalcandidattovalid = 0;
        var totalpricetovalid = 0;
        var totalneedcandidat = 0;

        var totalleft = 0;
        var nextLinePath = 0;

        var financementSankey = {
            nodes : [],
            links : []
        };

        var financementDistrPie = [];

        var financementTagsPie = [];

        var candidatePie = [];

        if (typeof data.depense != 'undefined') {
            $.each(data.depense, function (depenseId, depense) {
                if (parseInt(depenseId) > parseInt(nextLinePath)){
                    nextLinePath = parseInt(depenseId);
                }

                var poste = (typeof depense["poste"] != "undefined") ? depense["poste"] : "";
                var description = (typeof depense["description"] != "undefined") ? depense["description"] : "";
                var tags = (typeof depense["tags"] != "undefined") ? depense["tags"] : [];
                var price = notNull( depense["price"]) ? depense["price"] : 0;
                total += parseInt(price);
                var asssignationType = (typeof depense["switchAmountAttr"] != "undefined") ? depense["switchAmountAttr"] : "assignBudget";
                var lock = (typeof depense["lock"] != "undefined") ? depense["lock"] : false;
                var badge = (typeof depense["badge"] != "undefined") ? depense["badge"] : [];
                var candidateNumber = (typeof depense["candidateNumber"] != "undefined") ? depense["candidateNumber"] : 0;

                var candidateEstimate = {};
                var priceEstimate = {};
                var deniedCandidate = {};
                var isdeniedCandidate = {};

                var financement = {};
                var left = 0;

                var totalConfirmed = 0;
                var totalRowFinanced = 0;
                var totalEnveloppe = 0;
                var valideRow = false;

                var candidattovalid = 0;
                var pricetovalid = 0;
                var needcandidat = 0;
                var candidatvalid = 0;

                var estimateIdPrice = null;

                var percentageConfirmed = 0;
                var percentageFinanced = 0;
                var percentageNotFinanced = 0;

                if (typeof depense["estimates"] != "undefined") {

                    candidateEstimate = Object.keys(depense["estimates"])
                        .filter((key) => (coremuObj.isValidMongoId(key) && ( !notNull(depense["estimates"][key].deny) || depense["estimates"][key].deny != true)))
                        .reduce((obj, key) => {
                            return Object.assign(obj, {
                                [key]: depense["estimates"][key]
                            });
                        }, {});

                    deniedCandidate = Object.keys(depense["estimates"])
                        .filter((key) => (coremuObj.isValidMongoId(key) && ( notNull(depense["estimates"][key].deny) && depense["estimates"][key].deny == true)))
                        .reduce((obj, key) => {
                            return Object.assign(obj, {
                                [key]: depense["estimates"][key]
                            });
                        }, {});

                    isdeniedCandidate = Object.keys(depense["estimates"])
                        .filter((key) => (key == coremuObj.userId && notNull(depense["estimates"][key].deny) && depense["estimates"][key].deny == true))
                        .reduce((obj, key) => {
                            return Object.assign(obj, {
                                [key]: depense["estimates"][key]
                            });
                        }, {});

                    priceEstimate = Object.keys(depense["estimates"])
                        .filter((key) => !coremuObj.isValidMongoId(key))
                        .reduce((obj, key) => {
                            return Object.assign(obj, {
                                [key]: depense["estimates"][key]
                            });
                        }, {});

                }

                if (typeof depense["financer"] != "undefined") {
                    financement = Object.keys(depense["financer"])
                        .reduce((obj, key) => {
                            return Object.assign(obj, {
                                [key]: depense["financer"][key]
                            });
                        }, {});
                }

                if (
                    Object.keys(candidateEstimate).length == 0 ||
                    (candidateNumber != 0 && parseInt(candidateNumber) > Object.keys(candidateEstimate).length)
                ) {
                    needcandidat ++;
                    totalneedcandidat++;
                }

                if (totalConfirmed == price && price != 0){
                    valideRow = true;
                }

                if(!financementSankey.nodes.some(node => node.name === poste)){
                    financementSankey.nodes.push({"name": poste , "level":1})
                }

                $.each(candidateEstimate, function (estimateId, estimate) {
                    if(!notNull(coremuObj.users[estimateId]) ){
                        ajaxPost(
                            '',
                            baseUrl + "/co2/element/get/type/citoyens/id/" + estimateId,
                            null,
                            function (data) {
                                if (
                                    typeof data.map != "undefined"
                                ) {
                                    user = data.map;
                                    coremuObj.users[estimateId] = data.map;
                                }
                            },
                            null,
                            null,
                            {"async": false}
                        );
                    }

                    if (!notNull(estimate["deny"]) || estimate["deny"] != true) {
                        var totalestimate = 0;
                        var totalestimateConfirmed = 0;
                        var assignpricelist = [];
                        var percentage = 0;
                        var phour = 0;

                        if ((typeof estimate["validate"] == "undefined" || estimate["validate"] != "validated") && (typeof estimate["deny"] == "undefined" || estimate["deny"] != true)) {
                            candidattovalid++;
                            totalcandidattovalid++;
                        }

                        if (typeof estimate["validate"] != "undefined" && estimate["validate"] == "validated"){
                            candidatvalid++;
                        }

                        if (asssignationType == "assignBudget") {
                            if (typeof estimate['AssignBudgetArray'] != "undefined") {
                                if(!Array.isArray(estimate['AssignBudgetArray'])){
                                    assignpricelist = Object.values(estimate['AssignBudgetArray']);
                                }else {
                                    assignpricelist = estimate['AssignBudgetArray'];
                                }
                                $.each(estimate['AssignBudgetArray'], function (bAid, bA) {
                                    totalestimate += parseInt(bA["price"]);
                                    totalEnveloppe += parseInt(bA["price"]);

                                    if (candidatePie.some(distr => distr.name === coremuObj.users[estimateId].name)) {
                                        var indexOfNodes = candidatePie.map(function (el) {
                                            return el.name;
                                        });
                                        candidatePie[indexOfNodes.indexOf(coremuObj.users[estimateId].name)]["value"] += parseInt(bA["price"]);
                                    } else {
                                        candidatePie.push({"name": coremuObj.users[estimateId].name, "value": parseInt(bA["price"])})
                                    }

                                    if(notNull(bA["hour"])){
                                        phour += parseInt(bA["hour"]);
                                    }
                                    if (typeof estimate["validate"] != "undefined" && estimate["validate"] == "validated" && typeof bA["check"] != "undefined" && (bA["check"] == "true" || bA["check"] == true)) {
                                        totalestimateConfirmed += parseInt(bA["price"]);
                                        totalConfirmed += parseInt(bA["price"]);
                                        totalValidee += parseInt(bA["price"]);

                                        if(notNull(coremuObj.users[estimateId])) {
                                            if(!financementSankey.nodes.some(node => node.name === poste)){
                                                financementSankey.nodes.push({"name": poste , "level":1})
                                            }

                                            if(!financementSankey.nodes.some(node => node.name === coremuObj.users[estimateId].name)){
                                                financementSankey.nodes.push({"name": coremuObj.users[estimateId].name , "level" : 2})
                                            }

                                            var indexOfNodes = financementSankey.nodes.map(function (el) { return el.name; });
                                            financementSankey.links.push({
                                                "source": indexOfNodes.indexOf(poste),
                                                "target": indexOfNodes.indexOf(coremuObj.users[estimateId].name),
                                                "value": parseInt(bA["price"])
                                            });
                                        }
                                    }

                                    if (notNull(price) && typeof estimate["validate"] != "undefined" && estimate["validate"] == "validated" && (typeof bA["check"] == "undefined" || (bA["check"] != "true" && bA["check"] != true))) {
                                        pricetovalid++;
                                        totalpricetovalid++;
                                    }

                                });
                            }
                        } else {
                            percentage = 100 / Object.keys(candidateEstimate).length;
                            if (typeof estimate['percentage'] != "undefined") {
                                percentage = estimate['percentage'];
                            }
                            if (price != 0) {
                                totalestimate += (price * percentage) / 100;
                                totalEnveloppe += (price * percentage) / 100;
                                if (typeof estimate["validate"] != "undefined" && estimate["validate"] == "validated" && typeof estimate['percentageState'] != "undefined" && estimate['percentageState'] == "validated") {
                                    totalConfirmed += (price * percentage) / 100;
                                    totalestimateConfirmed += (price * percentage) / 100;
                                    totalValidee += (price * percentage) / 100;

                                    if (candidatePie.some(distr => distr.name === coremuObj.users[estimateId].name)) {
                                        var indexOfNodes = candidatePie.map(function (el) {
                                            return el.name;
                                        });
                                        candidatePie[indexOfNodes.indexOf(coremuObj.users[estimateId].name)]["value"] += ((price * percentage) / 100);
                                    } else {
                                        candidatePie.push({"name": coremuObj.users[estimateId].name, "value": ((price * percentage) / 100)});
                                    }

                                    if(notNull(coremuObj.users[estimateId])) {
                                        if(!financementSankey.nodes.some(node => node.name === poste)){
                                            financementSankey.nodes.push({"name": poste , "level":1})
                                        }

                                        if(!financementSankey.nodes.some(node => node.name === coremuObj.users[estimateId].name)){
                                            financementSankey.nodes.push({"name": coremuObj.users[estimateId].name , "level" : 2})
                                        }

                                        var indexOfNodes = financementSankey.nodes.map(function (el) { return el.name; });
                                        financementSankey.links.push({
                                            "source": indexOfNodes.indexOf(poste),
                                            "target": indexOfNodes.indexOf(coremuObj.users[estimateId].name),
                                            "value": (price * percentage) / 100
                                        });

                                    }
                                }

                                if (notNull(price) && typeof estimate["validate"] != "undefined" && estimate["validate"] == "validated" && (typeof estimate['percentageState'] == "undefined" || estimate['percentageState'] != "validated")) {
                                    pricetovalid++;
                                    totalpricetovalid++;
                                }
                            }
                        }

                        candidateEstimate[estimateId]["assignpricelist"] = assignpricelist;
                        candidateEstimate[estimateId]["totalestimate"] = totalestimate;
                        candidateEstimate[estimateId]["totalestimateConfirmed"] = totalestimateConfirmed;
                        candidateEstimate[estimateId]["percentage"] = percentage;
                        candidateEstimate[estimateId]["hour"] = phour;
                    }
                });

                $.each(priceEstimate, function (priceEstimateId, priceEstimate){
                    if (notNull(priceEstimate) && notNull(priceEstimate.ispriceselected) && priceEstimate.ispriceselected == true){
                        estimateIdPrice = priceEstimateId;
                    }
                });

                left = parseInt(price);
                totalleft += parseInt(price);

                $.each(financement, function (financementId, fin){
                    if(typeof fin.amount != 'undefined' && $.isNumeric(fin.amount)){
                        totalRowFinanced += parseInt(fin.amount);
                        totalFinancement += parseInt(fin.amount);
                        left -= parseInt(fin.amount);
                        totalleft -= parseInt(fin.amount);

                        var financername = "";
                        if(notNull(fin.name)){
                            financername = fin.name
                        }else if (notNull(coremuObj.cache.parent) && notNull(coremuObj.cache.parent.name)) {
                            financername = coremuObj.cache.parent.name;
                        }else if (notNull(data["parent"]) && notNull(data["parent"]["name"])){
                            financername = data["parent"]["name"];
                        }

                        if(financementDistrPie.some(distr => distr.name === financername)) {
                            var indexOfNodesf = financementDistrPie.map(function (el) { return el.name; });
                            financementDistrPie[indexOfNodesf.indexOf(financername)]["value"] += parseInt(fin.amount);
                        }else if(notNull(financername) && financername != 0){
                            financementDistrPie.push({"name": financername , "value" : parseInt(fin.amount)})
                        }

                        if(notNull(tags) && typeof tags == "string") {
                            $.each(tags.split(','), function (id , tag) {
                                if (financementTagsPie.some(distr => distr.name === tag)) {
                                    var indexOfNodes = financementTagsPie.map(function (el) {
                                        return el.name;
                                    });
                                    financementTagsPie[indexOfNodes.indexOf(tag)]["value"] += parseInt(fin.amount);
                                } else {
                                    financementTagsPie.push({"name": tag, "value": parseInt(fin.amount)})
                                }
                            });
                        }

                        if(!financementSankey.nodes.some(node => node.name === financername)){
                            financementSankey.nodes.push({"name": financername , "level" : 0})
                        }

                        if(!financementSankey.nodes.some(node => node.name === poste)){
                                financementSankey.nodes.push({"name": poste , "level" : 1})
                        }

                        var indexOfNodes = financementSankey.nodes.map(function (el) { return el.name; });

                        financementSankey.links.push({
                            "source" : indexOfNodes.indexOf(financername),
                            "target" : indexOfNodes.indexOf(poste),
                            "value" : parseInt(fin.amount)
                        })
                    }
                });

                percentageConfirmed = parseInt((totalConfirmed * 100) / price) ;
                percentageFinanced = parseInt((totalRowFinanced * 100) / price);
                percentageNotFinanced = parseInt(((totalConfirmed + totalRowFinanced) * 100) / price);

                var totalNotif = candidattovalid + pricetovalid + needcandidat;
                var isactive = ( totalNotif > 0 ? "active" : "");
                var tocandidatebtn = false;
                if (lock != "locked" &&
                    typeof  candidateEstimate[userId] == "undefined" &&
                    typeof  deniedCandidate[userId] == "undefined" &&
                    coremuObj.roles.canCandidate(coremuObj) &&
                    (
                        candidateNumber == 0 ||
                        (
                            candidateNumber != 0 &&
                            parseInt(candidateNumber) > candidatvalid
                        )
                    ) &&
                    valideRow != true

                ){
                    tocandidatebtn = true;
                }

                arrageDepense[depenseId] = {
                    "titre" : titre,
                    "poste": poste,
                    "description": description,
                    "tags": tags,
                    "price": price,
                    "estimateIdPrice" : estimateIdPrice,
                    "asssignationType": asssignationType,
                    "lock": lock,
                    "badge": badge,
                    "candidateEstimate": candidateEstimate,
                    "priceEstimate": priceEstimate,
                    "deniedCandidate" : deniedCandidate,
                    "isdeniedCandidate" : isdeniedCandidate,
                    "candidateNumber": candidateNumber,
                    "totalConfirmed": totalConfirmed,
                    "totalEnveloppe": totalEnveloppe,
                    "valideRow": valideRow,
                    "needcandidat" : needcandidat,
                    "pricetovalid" : pricetovalid,
                    "candidattovalid" : candidattovalid,
                    "isactive" : isactive,
                    "totalNotif" : totalNotif,
                    "tocandidatebtn" : tocandidatebtn,
                    "totalRowFinanced" : totalRowFinanced,
                    "percentageConfirmed" : percentageConfirmed,
                    "percentageFinanced" : percentageFinanced,
                    "percentageNotFinanced" : percentageNotFinanced,
                    "financement" : financement,
                    "left" : left,
                    "candidatvalid" : candidatvalid
                };
            });
            if(!$.isEmptyObject(data.depense)){
                nextLinePath = parseInt(nextLinePath) + 1;
            }
        }

        if (typeof data.badgeList != "undefined") {
            badgeList = data.badgeList.results;
        }

        if (typeof data.form != "undefined") {
            form = data.form;
        }

        if (typeof data.parent != "undefined") {
            parent = data.parent;
        }

        if (typeof data.comment != "undefined") {
            comment = data.comment;
        }

        var totalansweraction = totalneedcandidat + totalpricetovalid + totalcandidattovalid;

        coremuObj.financementDistrPie = financementDistrPie;
        financementSankey["links"] = coremuObj.fusionSankeyLinks(financementSankey["links"]);
        financementSankey["nodes"] = coremuObj.cleanSankeyNodes(financementSankey);
        coremuObj.financementSankey = financementSankey;
        coremuObj.financementTagsPie = financementTagsPie;
        coremuObj.candidatePie = candidatePie;

        return {
            "arrageDepense" : arrageDepense,
            "total": total,
            "totalFinancement" : totalFinancement,
            "totalleft" : totalleft,
            "totalValidee": totalValidee,
            "badgeList": badgeList,
            "form" : form,
            "parent" : parent,
            "totalansweraction" : totalansweraction,
            "totalneedcandidat" : totalneedcandidat,
            "totalpricetovalid" : totalpricetovalid,
            "totalcandidattovalid" : totalcandidattovalid,
            "nextLinePath" : nextLinePath,
            "comment" : comment,
            "titre" : titre
        }

    },

    checkBadgeMatch : function (array1, array2) {
        // Check if arrays have no matches
        if (!notNull(array1) || array1.length == 0 || array2.length == 0) {
            return "";
        }

        if (array1.every(elem => !array2.includes(elem))) {
            return "Badge(s) non acqui(s)";
        }

        // Check if arrays have all matches
        if (array1.every(elem => array2.includes(elem))) {
            return "Badge(s) acqui(s)";
        }

        // If none of the above conditions are met, return "Some matches"
        return "Badges partielement acquis";
    },

    update: function (container , arrangeData , path , animation){
        $(container).html(coremuObj.getValue(arrangeData , path));
    },

    updateGraph : {
        sankey : function (data) {
            sankey.nodes(data.nodes)
                .links(data.links)
                .layout(32);

            var link = svg.selectAll(".link")
                .data(data.links);

            link.enter().append("path")
                .attr("class", "link")
                .attr("d", path)
                .style("stroke-width", function(d) { return Math.max(1, d.value); })
                .style("opacity", 0)
                .transition()
                .duration(750)
                .style("opacity", 1).append("title")
                .append("title")
                .text(function(d) {
                    return d.source.name + " → " + d.target.name + "\n" + format(d.value);
                });

            link.transition()
                .duration(750)
                .attr("d", path)
                .style("stroke-width", function(d) { return Math.max(1, d.value); });

            link.exit().remove();

            var node = svg.selectAll(".node")
                .data(data.nodes);

            var nodeEnter = node.enter().append("g")
                .attr("class", "node");

            nodeEnter.append("rect")
                .attr("height", function(d) { return d.dy; })
                .attr("width", sankey.nodeWidth())
                .style("fill", function(d) { return d.color; })
                .style("opacity", 0)
                .transition()
                .duration(750)
                .style("opacity", 1);

            nodeEnter.append("text")
                .attr("x", -6)
                .attr("y", function(d) { return d.dy / 2; })
                .attr("dy", ".35em")
                .attr("text-anchor", "end")
                .attr("transform", null)
                .text(function(d) { return d.name; })
                .filter(function(d) { return d.x < 400; })
                .attr("x", 6 + sankey.nodeWidth())
                .attr("text-anchor", "start");

            node.transition()
                .duration(750)
                .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

            node.exit().remove();
        }


    },

    getValue: function (obj, path) {
        path = path.replace(/\[(\w+)\]/g, '.$1')
        path = path.replace(/^\./, '')
        var a = path.split('.')
        var o = obj
        while (a.length) {
            var n = a.shift()
            if (!(n in o)) return
            o = o[n]
        }
        return o
    },

    createTrLineBudget : function (coremuObj, pos){
        var html = "";
        if (notNull(coremuObj.cache.arrageDepense[pos])) {
            html += ' <tr class="haveroundbadge entiereline entiereline' + pos + '" data-id="' + pos + '" data-count="1">' +
                '   <td data-field="poste">' +
                '      <div class="smallspan lockeditsuprbtn'+pos+'" data-active="always">' +
                coremuObj.block.lockeditsuprbtn(coremuObj, pos) +
                '      </div>' +
                '      <span style="font-size: 22px" class=" poste'+pos+'" data-html="true" data-placement="right" data-microtip-position="top" role="tooltip" aria-label="' + coremuObj.cache.arrageDepense[pos].description + '">' +
                coremuObj.cache.arrageDepense[pos].poste +
                '      </span>' +
                '<div class="tags'+pos+'">' +
                    coremuObj.block.tags(coremuObj, pos) +
                '</div>' ;
                html += '      <div class="smallspan montantheader' + pos + '" data-active="always">' +
                coremuObj.block.montantgroupbtn(coremuObj, pos) +
                '      </div>' +
                '      <div style="display: inline-flex;">' +
                '         <span class="nowrap price'+answerId+pos+'" id="price' + pos + '" style="font-size: 22px"> ' + coremuObj.cache.arrageDepense[pos].price + '</span><span style="font-size: 22px">€ ( </span>' +
                '         <span class="nowrap totalConfirmed'+answerId+pos+'" id="totalConfirmed' + pos + '" style="font-size: 22px"> ' + coremuObj.cache.arrageDepense[pos].totalConfirmed + '</span><span style="font-size: 22px">€ depensé)<span>' +
                '      </div>' +
                '      <div>';

            html += '      </div>';
                if(coremuObj.cache.arrageDepense[pos].description != "") {
                    html += ' <div class="smallspan toogle-maximum" data-active="always">' +
                        '         <span> <b> Description </b></span>' +
                        '      </div>';
                }
            html += '      <div class="toogle-maximum smallspan-description description'+pos+'" style="font-size: 16px">' +
                coremuObj.cache.arrageDepense[pos].description +
                '      </div>' +
                '      <div class=" statustags-container no-padding toogle-maximum">' +
                '         <div class="smallspan editbadge'+pos+'">' +
                coremuObj.block.editbadge(coremuObj, pos) +
                '         </div>' +
                coremuObj.block.selectbadge(coremuObj, pos) +

                '         <div class="col-xs-12 no-padding">' +
                '         </div>' +
                '      </div>' +
                '       <div class="arrageDepense' + pos + 'valideRowContainer">';
            if (coremuObj.cache.arrageDepense[pos].valideRow) {
                html += coremuObj.block.valideRow(coremuObj, pos);
            }
            html += '       </div>' +
                ' </td>' +

                '   <td data-field="price">' +
                '      <div class="dropdown actiondropdown">' +
                '         <button class="numberAction dropdown-toggle arrageDepense' + pos + 'isactive ' + coremuObj.cache.arrageDepense[pos].isactive + '  " data-placement="top" data-microtip-position="top" role="tooltip" aria-label="Action en attente de d\'interaction" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                '         <i class="fa fa-exclamation"></i> <span class="numberAction arrageDepense' + pos + 'totalNotif"> ' + coremuObj.cache.arrageDepense[pos].totalNotif + ' </span>' +
                '         </button>' +
                '         <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">' +
                '            <div class="dropdown-menu-item">' +
                '               <span class="">Appel à candidature </span> <span class=" actionnumber pull-right arrageDepense' + pos + 'needcandidat"> ' + coremuObj.cache.arrageDepense[pos].needcandidat + ' </span>' +
                '            </div>' +
                '            <div class="dropdown-menu-item">' +
                '               <span class="">Budget à valider </span> <span class=" actionnumber pull-right arrageDepense' + pos + 'pricetovalid"> ' + coremuObj.cache.arrageDepense[pos].pricetovalid + ' </span>' +
                '            </div>' +
                '            <div class="dropdown-menu-item">' +
                '               <span class="">Candidat à valider </span> <span class=" actionnumber pull-right arrageDepense' + pos + 'candidattovalid"> ' + coremuObj.cache.arrageDepense[pos].candidattovalid + ' </span>' +
                '            </div>' +
                '         </div>' +
                '      </div>' +
                '      <div class="smallspan candidatheader' + pos + '" data-type="list" data-active="true" data-pos="' + pos + '">' +
                coremuObj.block.candidatbtn(coremuObj, pos) +
                '      </div>' +
                '      <div class="smallspan" data-type="deny" data-active="false" data-pos="' + pos + '">' +
                '         <span class="text-danger"> <b> Candidat recalé </b>  </span>' +
                '         <div class="pull-right ">' +
                '            <button class="btn btn-xs btn-danger close-smallspan" data-pos="' + pos + '" type="button"><i class="fa fa-times-circle"></i></button>' +
                '         </div>' +
                '      </div>' +
                '      <div class="smallspan" data-type="price" data-active="false" data-pos="' + pos + '">' +
                '         <span class="text-danger"> <b> Estimation de prix </b>  </span>' +
                '         <div class="pull-right ">' +
                '            <button class="btn btn-xs btn-danger close-smallspan" data-pos="' + pos + '" type="button"><i class="fa fa-times-circle"></i></button>' +
                '         </div>' +
                '      </div>' +
                '      <div class="toogle-maximum" data-pos="' + pos + '">' +
                '         <main class="leaderboard__profiles arrageDepense' + pos + 'candidateEstimateContainer arrageDepense' + pos + 'priceEstimateContainer arrageDepense' + pos + 'deniedCandidateContainer" data-pos="' + pos + '">';
            $.each(coremuObj.cache.arrageDepense[pos].priceEstimate, function (index, value) {
                html += coremuObj.block.estimateprice(coremuObj, pos, index);
            });
            $.each(coremuObj.cache.arrageDepense[pos].deniedCandidate, function (index, value) {
                html += coremuObj.block.deniedcandidatecard(coremuObj, pos, index);
            });
            $.each(coremuObj.cache.arrageDepense[pos].candidateEstimate, function (index, value) {
                html += coremuObj.block.candidatecard(coremuObj, pos, index)
            });
            $.each(coremuObj.cache.arrageDepense[pos].isdeniedCandidate, function (index, value) {
                html += coremuObj.block.isdeniedCandidate(coremuObj, pos, index)
            });
            html += '         </main>';
                '      </div>';
            html += '   <div class="arrageDepense' + pos + 'tocandidatebtnContainer candidatebtn-div" data-pos="' + pos + '">';
            html += coremuObj.block.tocandidatebtn(coremuObj, pos);
            html += '           </div>';
                '   </td>' +
                '   <td class="roweditbtn td-action hide" style="display: none">' +
                '   </td>' +
                '</tr>';
        }
        return html;
    },

    createTrFooterBudget: function(coremuObj){
        var html = "";

        html += '<tr style="background-color: gainsboro;" class="trtotal totaltrline">' +
            '         <td class="hidden"></td>' +
            '         <td class="hidden"></td>' +
            '         <td class="text-bold"><span class="pull-right">Total : </span></td>' +
            '         <td class="text-bold coremuObj-cache-total"><span class="total">'+coremuObj.cache.total+'</span>€</td>' +
            '    </tr>';
        return html;
    },

    createTrFooterBudgetValid: function(coremuObj){
        var html = "";

        html += '    <tr style="background-color: gainsboro;" class="trtotal ">' +
            '         <td class="hidden">&nbsp;</td>' +
            '         <td class="hidden">&nbsp;</td>' +
            '         <td class="text-bold"><span class="pull-right">Validée : </span></td>' +
            '         <td class="text-bold" id="totalValideeContainer"><span class="totalValidee">'+coremuObj.cache.totalValidee+'</span>€</td>' +
            '   </tr>';
        return html;
    },

    createTrLineFinancement: function (coremuObj, pos){
        var html = "";
        var percentageFinanced = coremuObj.cache.arrageDepense[pos].percentageFinanced;
        var percentageConfirmed = coremuObj.cache.arrageDepense[pos].percentageConfirmed;
        var percentageNotFinanced = coremuObj.cache.arrageDepense[pos].percentageConfirmed;

        if (notNull(coremuObj.cache.arrageDepense[pos])) {
            html += '<div class="col-md-12 col-sm-12 col-xs-12 contentInformation entierelinefin entierelinefin' + pos + ' no-padding  line-dark" data-id="' + pos + '">' +
                '   <div class="col-md-5 col-sm-6" style="padding-top: 20px">' +
                '      <h3 class="widget-title poste'+pos+'" style="font-size: 21px">' +
                coremuObj.cache.arrageDepense[pos].poste +
                '      </h3>' +
                '           <span class="lockeditsuprbtn'+pos+'" style="display: inline-flex">' +
                coremuObj.block.lockeditsuprbtn(coremuObj, pos , false) +
                '           </span>' +
                '       <button style="" type="button" id="btnFinancer'+pos+'" data-id="'+answerId+'" data-budgetpath="depense" data-form="aapStep1" data-pos="'+pos+'" data-name="'+coremuObj.cache.arrageDepense[pos].poste+'" class="btn btn-xs rebtnFinancer financementbadgebtn">Financer cette ligne</button>'+
                '   </div>' +
                '   <div class="col-md-7 col-sm-6" style="padding-top: 20px;">' +
                '      <h3 class="widget-title" style="font-size: 17px;">' +
                '           <span style="font-size: 17px;" class="nowrap totalRowFinanced'+pos+'" id="totalRowFinanced' + pos + '" > ' + coremuObj.cache.arrageDepense[pos].totalRowFinanced + '</span>' +
                '           <i class="fa fa-euro"></i> financé sur' +
                '           <span class="nowrap price'+answerId+pos+'" id="price' + pos + '" > ' + coremuObj.cache.arrageDepense[pos].price + '</span>' +
                '           <i class="fa fa-euro"></i> et' +
                '         <span style="color: #6c7a89">' +
                '         ' +
                '           <span class="nowrap totalConfirmed'+pos+'" id="totalConfirmed' + pos + '" > ' + coremuObj.cache.arrageDepense[pos].totalConfirmed + '</span>' +
                '           <i class="fa fa-euro"></i> dépensé' +
                '         </span>' +
                '      </h3>' +
                '      <div class="">' +
                '         <div class="">' +
                '            <div>' +
                '            </div>' +
                '            <div style="clear: both;"> </div>' +
                '            <div class="progressbarfin'+pos+'" style="max-height: 72px;">' +
                coremuObj.block.progressbarfin(coremuObj, pos ) +
                '            </div>' +
                '            <div class="widget-controls hidden">' +
                '               <a href="#" class="widget-control-right"><span class="fa fa-times"></span></a>' +
                '            </div>' +
                '         </div>' +
                '      </div>' +
                '   </div>' +
                '   <div class="col-md-12 col-sm-12  padding-10">' +
                '      <div style="" class="pr_div '+answerId+'fitable-'+pos+'">' +
                '         <table class="table table-bordered table-hover  directoryTable oceco-styled-table">' +
                '            <tbody class="oceco-blanc-table financementtable'+pos+'" data_id="'+pos+'">';
            html += coremuObj.block.financerTable(coremuObj, pos);

                html += '            </tbody>' +
                '         </table>' +
                '      </div>' +
                '   </div>' +
                '</div>';
        }
        return html;
    },

    createTrFooterFinancement: function(coremuObj){
        var html = "";

        html += '<div style="background-color: gainsboro;" class="trfintotal totaltrfinline">' +
            '         <span class="text-bold"><span class="">Total : </span></span>' +
            '         <span class="text-bold coremuObj-cache-totalFinancement"><span class="totalFinancement"> '+coremuObj.cache.totalFinancement+'</span> € financé sur <span class="total"> '+coremuObj.cache.total+'</span> € et <span class="totalValidee"> '+coremuObj.cache.totalValidee+' </span> € dépensé </span>' +
            '    </div>';
        return html;
    },

    roles : {
        isFormAdmin : function (coremuObj){
            if (notNull(coremuObj.userRoles.isFormAdmin)){
                return coremuObj.userRoles.isFormAdmin
            } else {
                var returnValue = {};
                if(notNull(answerId)) {
                    ajaxPost(
                        '',
                        baseUrl + '/survey/form/getaaprole/answerId/' + answerId,
                        null,
                        function (data) {
                            if (
                                typeof data.isFormAdmin != "undefined"
                            ) {
                                returnValue = data.isFormAdmin;
                                coremuObj.userRoles = data;
                            }
                        },
                        null,
                        null,
                        {"async": false}
                    );
                }
            }

            return returnValue;
        },

        canCandidate : function (coremuObj){
            var returnValue = {};

            if (notNull(coremuObj.userRoles.canCandidate)){
                return coremuObj.userRoles.canCandidate
            } else {
                var returnValue = {};
                if(notNull(answerId)) {
                    ajaxPost(
                        '',
                        baseUrl + '/survey/form/getaaprole/answerId/' + answerId,
                        null,
                        function (data) {
                            if (
                                typeof data.canCandidate != "undefined"
                            ) {
                                returnValue = data.canCandidate;
                                coremuObj.userRoles = data;
                            }
                        },
                        null,
                        null,
                        {"async": false}
                    );
                }
            }

            return returnValue;
        },

        /*canEditProposition : function (coremuObj , pos){
            if (notNull(coremuObj.userRoles.pos) && notNull(coremuObj.userRoles.pos.canEditProposition)){
                return coremuObj.userRoles.pos.canEditProposition
            } else {
                var returnValue = {};
                ajaxPost(
                    '',
                    baseUrl+'/survey/form/getaaprole/answerId/'+ answerId + '/depenseposition/' + pos ,
                    null,
                    function (data) {
                        if (
                            typeof data.canEditProposition != "undefined"
                        ) {
                            returnValue = data.canEditProposition;
                            coremuObj.userRoles.pos = data;
                        }
                    },
                    null,
                    null,
                    {"async": false}
                );
            }

            return returnValue;
        },*/

        /*canEditCandidature : function (coremuObj , pos , candidate){
            var returnValue = {};

            if (notNull(coremuObj.userRoles.pos) && notNull(coremuObj.userRoles.pos[candidate]) && notNull(coremuObj.userRoles.pos[candidate].canEditProposition)){
                return coremuObj.userRoles.pos[candidate].canEditProposition;
            } else {
                var returnValue = {};
                ajaxPost(
                    '',
                    baseUrl+'/survey/form/getaaprole/answerId/'+ answerId + '/depenseposition/' + pos + '/candidateId/' + candidate ,
                    null,
                    function (data) {
                        if (
                            typeof data.canEditProposition != "undefined"
                        ) {
                            returnValue = data.canEditProposition;
                            coremuObj.userRoles.pos = data;
                        }
                    },
                    null,
                    null,
                    {"async": false}
                );
            }

            return returnValue;
        }*/

    },

    block: {
        lockeditsuprbtn: function (coremuObj, pos, withtitle = true) {
            var html = '';
            if (withtitle) {
                html += "         <span> <b> Poste de depense </b></span>";
            }

            var locklinestate = "";
            var target = "";
            var tooltips = "";
            var icon = "";

            if (coremuObj.roles.isFormAdmin(coremuObj)) {
                locklinestate = "locklinestate";
            }
            if (
                typeof coremuObj.cache.arrageDepense != "undefined" &&
                typeof coremuObj.cache.arrageDepense[pos] != "undefined" &&
                typeof coremuObj.cache.arrageDepense[pos].lock != "undefined" &&
                coremuObj.cache.arrageDepense[pos].lock == "locked"
            ) {
                target = "unlocked"
                tooltips = "En état verrouillé";
                icon = "fa-lock";
            } else {
                target = "locked";
                tooltips = "En état déverrouillé";
                icon = "fa-unlock";
            }

            if (withtitle) {
                html += '<button class=" btn btn-xs ' + locklinestate + ' budgetbadgebtn " data-id="' + answerId + '" data-key="depense" data-form="aapStep1" data-pos="' + pos + '" type="button" data-target="' + target + '" data-active="false" data-placement="top" data-microtip-position="top" role="tooltip" aria-label="' + tooltips + '">' +
                    '<i class="fa ' + icon + '"></i>' +
                    '</button>';
                html += '<button class=" btn btn-xs locklinestatehide hide budgetbadgebtn " data-id="' + answerId + '" data-key="depense" data-form="aapStep1" data-pos="' + pos + '" type="button" data-target="' + target + '" data-active="false" data-placement="top" data-microtip-position="top" role="tooltip" aria-label="' + tooltips + '">' +
                    '<i class="fa ' + icon + '"></i>' +
                    '</button>';
                html += '<button class=" btn btn-xs unlocklinestatehide hide budgetbadgebtn " data-id="' + answerId + '" data-key="depense" data-form="aapStep1" data-pos="' + pos + '" type="button" data-active="false" data-placement="top" data-microtip-position="top" role="tooltip" aria-label="' + tooltips + '">' +
                    '<i class="fa ' + icon + '"></i>' +
                    '</button>';
            }

            html += '<button data-pos="'+pos+'"  class="opencoremucomment btn btn-xs bd-enable-btnbudgetbudget budgetbadgebtn" style="visibility: visible; opacity: 1 ;display: inline-block;" data-placement="top" data-microtip-position="top" role="tooltip" aria-label="Commenter cette ligne">' +
                '<i class="fa fa-commenting"></i>' +
                '<span class="numberAction depense'+pos+'comment">'+coremuObj.cache.comment[answerId+"depense"+pos]+' </span>' +
                '</button>';

            if (
                (
                    typeof coremuObj.cache.arrageDepense == "undefined" ||
                    typeof coremuObj.cache.arrageDepense[pos] == "undefined" ||
                    typeof coremuObj.cache.arrageDepense[pos].lock == "undefined" ||
                    coremuObj.cache.arrageDepense[pos].lock != "locked"
                ) &&
                coremuObj.roles.isFormAdmin(coremuObj)
            ) {
                html += '<a href="javascript:;" data-actionid="" data-id="' + answerId + '" data-collection="answers" data-key="' + pos + '" data-path="answers.aapStep1.depense.' + pos + '" class="editbudgetdepense previewTpl budgetbadgebtn btn btn-xs bd-enable-btnbudgetbudget" data-placement="top" data-microtip-position="top" role="tooltip" aria-label="Modifier cette ligne">' +
                    '<i class="fa fa-pencil"></i>' +
                    '</a> ' +
                    '<a href="javascript:;" data-id="' + answerId + '" data-collection="answers" data-key="' + pos + '" data-path="answers.aapStep1.depense.' + pos + '" class="inputdeleteLine previewTpl btn btn-xs bd-enable-btnbudgetbudget budgetbadgebtn" data-placement="top" data-microtip-position="top" role="tooltip" aria-label="Supprimer cette ligne">' +
                    '<i class="fa fa-times"></i>' +
                    '</a>'
            }

            return html;
        },

        editbadge: function (coremuObj, pos) {
            html = "            <span> <b> Badge(s) requis(s) </b></span>";
            if (
                (
                    typeof coremuObj.cache.arrageDepense == "undefined" ||
                    typeof coremuObj.cache.arrageDepense[pos] == "undefined" ||
                    typeof coremuObj.cache.arrageDepense[pos].lock == "undefined" ||
                    coremuObj.cache.arrageDepense[pos].lock != "locked"
                ) &&
                coremuObj.roles.isFormAdmin(coremuObj)
            ) {
                html += '<a href="javascript:;" class="btn btn-xs budgetbadgebtn bd-enable-btnbudgetdepense" data-loc="edit" data-id="badge' + answerId + pos + '" data-key="depense" data-pos="' + pos + '" data-answerid="' + answerId + '" type="button" data-microtip-position="top" role="tooltip" aria-label="Modifier badge" title="">' +
                    '       <i class="fa fa-pencil"></i>' +
                    '   </a>';
            }
            return html;
        },

        montantbtn: function (coremuObj, pos) {
            var attribution = "";
            var tooltips = "";
            var active = "";
            var pricelenght = 0;
            var disable = "disabled";
            var html = "";

            if (coremuObj.roles.isFormAdmin(coremuObj)) {
                disable = "";
            }
            if (typeof coremuObj.cache.arrageDepense[pos] != "undefined") {
                if (coremuObj.cache.arrageDepense[pos].asssignationType == "assignBudget") {
                    attribution = "assignBudget";
                    tooltip = "En mode attribution manuel";
                    active = "checked";
                } else {
                    attribution = "depense";
                    tooltip = "En mode attribution pourcentage";
                    active = "";
                }
                pricelenght = Object.keys(coremuObj.cache.arrageDepense[pos].priceEstimate).length - 1;
            }

            if (
                typeof coremuObj.cache.arrageDepense == "undefined" ||
                typeof coremuObj.cache.arrageDepense[pos] == "undefined" ||
                typeof coremuObj.cache.arrageDepense[pos].lock == "undefined" ||
                coremuObj.cache.arrageDepense[pos].lock != "locked"
            ) {
                /*html += '<div class="budgetbadgebtn btn btn-xs" ' + disable + ' data-placement="top" data-microtip-position="top" role="tooltip" aria-label="' + tooltips + '">' +
                    '   <button type="button" class="btn btn-xs btn-toggle switchAmountAttr ' + active + ' " data-id="' + answerId + '" data-collection="answers" data-key="' + pos + '" data-path="answers.aapStep1.depense.' + pos + '.switchAmountAttr" data-val="' + attribution + '" data-toggle="button tooltip" aria-pressed="false" autocomplete="off" data-placement="top" data-microtip-position="top" role="tooltip" aria-label="' + tooltips + '">' +
                    '      <div class="handle"></div>' +
                    '   </button>' +
                    '</div>';*/

                html += '<div class="budgetbadgebtn btn btn-xs " ' + disable + ' data-placement="top" data-microtip-position="top" role="tooltip" aria-label="' + tooltip + '">' +
                    '       <input type="checkbox" ' + active + ' class="toggle switchAmountAttr switchAmountAttr' + pos + '" data-id="' + answerId + '" data-collection="answers" data-key="' + pos + '" data-path="answers.aapStep1.depense.' + pos + '.switchAmountAttr" data-val="' + attribution + '">' +
                    '   </div>';


                if (coremuObj.roles.canCandidate(coremuObj)) {
                    html += '<a href="javascript:;" data-id="' + answerId + '" data-key="depense" data-form="aapStep1" data-kunik="budgetdepense" data-pos="' + pos + '" type="button" class="btn btn-xs newbtnestimate_price propositionAction budgetbadgebtn" data-loc="edit" data-placement="top" data-microtip-position="top" role="tooltip" aria-label="Proposer un prix">' +
                        '   <i class="fa fa-money"></i>' +
                        '</a>';
                }

                if (pricelenght > 0) {
                    html += '<button class="btn-toogle-list btn btn-xs btn-custombadge action-list-toggle budgetbadgebtn" data-position="bottom" data-microtip-position="top" role="tooltip" aria-label="Choisir un prix" data-pos="' + pos + '" type="button" id="about-us" data-target="price" data-active="false">' +
                        '   <i class="fa fa-bars"></i>' +
                        '   <span class="number">' + pricelenght + '</span>' +
                        '</button>';
                }
            }

            return html;

        },

        candidatbtn: function (coremuObj, pos) {
            var html = "";

            html += '<span> <b> Candidat </b> <small> ( ' + (Object.keys(coremuObj.cache.arrageDepense[pos].candidateEstimate).length - parseInt(coremuObj.cache.arrageDepense[pos].candidattovalid)) + ' candidat(s) validé(s) ' + (coremuObj.cache.arrageDepense[pos].candidateNumber != 0 ? 'sur ' + coremuObj.cache.arrageDepense[pos].candidateNumber + "requis" : "") + ')</small></span>';

            if (
                (
                    typeof coremuObj.cache.arrageDepense == "undefined" ||
                    typeof coremuObj.cache.arrageDepense[pos] == "undefined" ||
                    typeof coremuObj.cache.arrageDepense[pos].lock == "undefined" ||
                    coremuObj.cache.arrageDepense[pos].lock != "locked"
                ) &&
                coremuObj.roles.isFormAdmin(coremuObj)
            ) {
                html += '<a href="javascript:;" data-id="' + answerId + '" data-key="depense" data-form="aapStep1" data-kunik="budgetdepense" data-exclus=\'' + JSON.stringify(Object.keys(coremuObj.cache.arrageDepense[pos].candidateEstimate)) + '\' data-pos="' + pos + '" type="button" class="btn btn-xs newbtnestimate_candidate propositionAction budgetbadgebtn" data-loc="edit" data-toggle="tooltip" data-placement="top" data-microtip-position="top" role="tooltip" aria-label="Assigner des candidats">' +
                    '   <i class="fa fa-group"></i>' +
                    '</a>' +
                    '<a href="javascript:;" data-id="' + answerId + '" data-min="' + Object.keys(coremuObj.cache.arrageDepense[pos].candidateEstimate).lenght + '" data-candidatenumber="' + coremuObj.cache.arrageDepense[pos].candidateNumber + '" data-key="depense" data-form="aapStep1" data-pos="' + pos + '" type="button" class="btn candidatenumber  btn-xs budgetbadgebtn" data-loc="edit" data-placement="top" data-microtip-position="top" role="tooltip" aria-label="Nombre de personne sur cette proposition">' +
                    '   <i class="fa fa-list-ol"></i>' +
                    '</a>';

                if (Object.keys(coremuObj.cache.arrageDepense[pos].deniedCandidate).length > 0) {
                    html += '<button class="btn-toogle-list btn btn-xs btn-custombadge action-list-toggle budgetbadgebtn candidat-times" data-pos="' + pos + '" type="button" id="about-us" data-target="deny" data-active="false" data-placement="top"  data-microtip-position="top" role="tooltip" aria-label="Candidat recalé">' +
                        '        <i style="color: #860100" class="fa fa-times"></i> ' +
                        '        <span class="number">' + Object.keys(coremuObj.cache.arrageDepense[pos].deniedCandidate).length + '</span>' +
                        '    </button>';
                }

            }
            return html;
        },

        selectbadge: function (coremuObj, pos) {
            var html = "";
            html += '<select data-id="badge' + answerId + pos + '" class="statustags statustags1" multiple disabled >';
            if (notNull(coremuObj.cache.badgeList)) {
                $.each(coremuObj.cache.badgeList, function (index, value) {
                    var selected = "";
                    var image = "";
                    if (coremuObj.cache.arrageDepense[pos].badge.includes(index)) {
                        selected = "selected";
                    }
                    if (typeof value.profilThumbImageUrl != "undefined") {
                        image = "selected";
                    }
                    html += '<option ' + selected + ' data-image="' + image + '" value="' + index + '">' + value.name + '</option>'
                });
            }
            html += '</select>'

            return html;
        },

        estimateprice: function (coremuObj, pos, candidate) {
            var html = "";
            if (notNull(coremuObj.cache.arrageDepense[pos].priceEstimate[candidate])) {
                var ispriceselected = (typeof coremuObj.cache.arrageDepense[pos].priceEstimate[candidate].ispriceselected != "undefined") ? coremuObj.cache.arrageDepense[pos].priceEstimate[candidate].ispriceselected : false;
                var selectedlabel = "";
                var user = {};
                var pricecomment = '<small style=\'color:grey\' >(commentaire vide)</small>';
                var price = (typeof coremuObj.cache.arrageDepense[pos].priceEstimate[candidate].price != "undefined") ? coremuObj.cache.arrageDepense[pos].priceEstimate[candidate].price : false;

                if (notNull(coremuObj.cache.arrageDepense[pos].priceEstimate[candidate].ispriceselected)) {
                    ispriceselected = coremuObj.cache.arrageDepense[pos].priceEstimate[candidate].ispriceselected;
                }

                if (ispriceselected) {
                    selectedlabel = "séléctionnée";
                }

                if (notNull(coremuObj.cache.arrageDepense[pos].priceEstimate[candidate].pricecomment)) {
                    pricecomment = coremuObj.cache.arrageDepense[pos].priceEstimate[candidate].pricecomment;
                }


                if (notNull(coremuObj.cache.arrageDepense[pos].priceEstimate[candidate].addedBy)) {
                    if (!notNull(coremuObj.users[coremuObj.cache.arrageDepense[pos].priceEstimate[candidate].addedBy])) {
                        ajaxPost(
                            '',
                            baseUrl + "/co2/element/get/type/citoyens/id/" + coremuObj.cache.arrageDepense[pos].priceEstimate[candidate].addedBy,
                            null,
                            function (data) {
                                if (
                                    typeof data.map != "undefined"
                                ) {
                                    user = data.map;
                                    coremuObj.users[coremuObj.cache.arrageDepense[pos].priceEstimate[candidate].addedBy] = data.map;
                                }
                            },
                            null,
                            null,
                            {"async": false}
                        );
                    } else {
                        user = coremuObj.users[coremuObj.cache.arrageDepense[pos].priceEstimate[candidate].addedBy];
                    }
                }

                html += '<div class="leaderboard__container arrageDepense' + pos + 'estimatePrice' + candidate + ' arrageDepense' + pos + 'priceEstimate' + candidate + '" data-type="price"  data-active = "false"  data-pos="' + pos + '">' +
                    '         <article class="leaderboard__profile block-candidate-price btn-deny-toogle-comment candidateprice' + pos + candidate + ' ' + (ispriceselected ? "check-estimates" : "") + '" data-pos="' + pos + '" data-uid="' + candidate + '">';
                html += '         <span class="leaderboard__name nowrap">' + price + '<i class="fa fa-euro"></i> </span>' +
                    '             <span class="leaderboard__value"><small>Proposé par :</small>' + (notNull(user.name) ? user.name : "") + '</span>' +
                    '         </article>' +
                    '         <div class="arrow-down-comment" data-active="true" data-pos="' + pos + '" data-uid="' + candidate + '"></div>' +
                    '             <div class="denycomment pricecomment' + pos + candidate + '" data-active="true" data-pos="' + pos + '" data-uid="' + candidate + '">' +
                    pricecomment +
                    '             </div>' +
                    '             <span style="">' +
                    '                 <div class=" action-btn-group estimatepricecardbtn' + pos + candidate + '">' +
                    coremuObj.block.estimatepricecardbtn(coremuObj, pos, candidate) +
                    '                 </div>' +
                    '             </span>' +
                    '         </div>';

            }

            return html;
        },

        estimatepricecardbtn: function (coremuObj, pos, candidate) {
            var html = '';
            var estimateIdPrice = '';
            var pricecomment = '';
            var price = '';
            var ispriceselected = false;

            if (notNull(coremuObj.cache.arrageDepense[pos].estimateIdPrice)) {
                estimateIdPrice = coremuObj.cache.arrageDepense[pos].estimateIdPrice;
            }

            if (notNull(coremuObj.cache.arrageDepense[pos].priceEstimate[candidate]) && notNull(coremuObj.cache.arrageDepense[pos].priceEstimate[candidate].ispriceselected)) {
                ispriceselected = coremuObj.cache.arrageDepense[pos].priceEstimate[candidate].ispriceselected;
            }

            if (notNull(coremuObj.cache.arrageDepense[pos].priceEstimate[candidate].price)) {
                price = coremuObj.cache.arrageDepense[pos].priceEstimate[candidate].price;
            }

            if (notNull(coremuObj.cache.arrageDepense[pos].priceEstimate[candidate].pricecomment)) {
                pricecomment = coremuObj.cache.arrageDepense[pos].priceEstimate[candidate].pricecomment;
            }

            if (coremuObj.roles.canCandidate(coremuObj)) {
                if (
                    typeof coremuObj.cache.arrageDepense == "undefined" ||
                    typeof coremuObj.cache.arrageDepense[pos] == "undefined" ||
                    typeof coremuObj.cache.arrageDepense[pos].lock == "undefined" ||
                    coremuObj.cache.arrageDepense[pos].lock != "locked"
                ) {

                    if (!ispriceselected) {
                        html += '<button type="button" class="btn leaderboard__editbtn  estibtndelete"  data-placement="top" data-microtip-position="top" role="tooltip" aria-label="Supprimer cette estimation" data-id="' + answerId + '" data-uid="' + candidate + '" data-key="depense" data-form="aapStep1" data-pos="' + pos + '" ><i class="fa fa-trash"></i></button>';

                        html += '        <button type="button" class="btn leaderboard__editbtn  btnEstimateSelected"  data-placement="top" data-microtip-position="top" role="tooltip" aria-label="Seléctionner cette estimation" data-id="' + answerId + '" data-uid="' + candidate + '" data-price="' + price + '" data-key="' + pos + '" data-form="aapStep1" data-pos="' + pos + '" data-spid="' + estimateIdPrice + '" ><i class="fa fa-hand-pointer-o"></i></button>';

                        html += '        <button type="button"  class="btn leaderboard__editbtn  btnmodifypricecomment" data-pricecomment="' + pricecomment + '"  data-placement="top" data-microtip-position="top" role="tooltip" aria-label="Modifier le commentaire"" data-id="' + answerId + '" data-uid="' + candidate + '" data-price="' + price + '" data-key="' + pos + '" data-form="aapStep1" data-pos="' + pos + '" ><i class="fa fa-comment"></i></button>';
                    }


                }
            }

            return html;
        },

        deniedcandidatecard: function (coremuObj, pos, candidate) {
            var html = "";
            var user = {};

            if (notNull(candidate)) {
                if (!notNull(coremuObj.users[candidate])) {
                    ajaxPost(
                        '',
                        baseUrl + "/co2/element/get/type/citoyens/id/" + candidate,
                        null,
                        function (data) {
                            if (
                                typeof data.map != "undefined"
                            ) {
                                user = data.map;
                                coremuObj.users[candidate] = data.map;
                            }
                        },

                        null,
                        null,
                        {"async": false}
                    );
                } else {
                    user = coremuObj.users[candidate];
                }
            }

            html += '<div class="leaderboard__container arrageDepense' + pos + 'deniedCandidate' + candidate + '" data-type="deny" data-active="false" data-pos="' + pos + '">' +
                '   <article class="leaderboard__profile block-candidate-deny " data-pos="' + pos + '" data-uid="' + candidate + '">' +
                '      <img src="' + (notNull(user.profilImageUrl) ? user.profilImageUrl : modules.co2.url + "/images/thumb/default_citoyens.png") + '" alt="." class="leaderboard__picture">' +
                '      <span class="leaderboard__name">' + user.name + '<small>(récalé)<i class="fa fa-exclamation-triangle"></i></small></span>' +
                '   </article>' +
                '   <div class="arrow-down-comment" data-active="false" data-pos="' + pos + '" data-uid="' + candidate + '"></div>' +
                '   <div class="denycomment" data-active="false" data-pos="' + pos + '" data-uid="' + candidate + '">' +
                '   </div>' +
                '   <span style="">' +
                '      <div class=" action-btn-group deniedcandidatecardbtn' + pos + '">' +
                coremuObj.block.deniedcandidatecardbtn(coremuObj, pos, candidate) +
                '      </div>' +
                '   </span>' +
                '</div>';

            return html;
        },

        deniedcandidatecardbtn: function (coremuObj, pos, candidate) {
            var html = '';

            if (coremuObj.roles.canCandidate(coremuObj)) {
                if (
                    typeof coremuObj.cache.arrageDepense == "undefined" ||
                    typeof coremuObj.cache.arrageDepense[pos] == "undefined" ||
                    typeof coremuObj.cache.arrageDepense[pos].lock == "undefined" ||
                    coremuObj.cache.arrageDepense[pos].lock != "locked"
                ) {

                    html += ' <button type="button" class="btn leaderboard__editbtn  estibtndelete" data-placement="top" data-microtip-position="top" role="tooltip" aria-label="Supprimer ce candidat" data-id="' + answerId + '" data-uid="' + candidate + '" data-key="depense" data-form="aapStep1" data-pos="' + pos + '"><i class="fa fa-trash"></i></button>' +
                        '         <button type="button" class="btn leaderboard__editbtn  btncanceldeny" data-placement="top" data-microtip-position="top" role="tooltip" aria-label="Ré-integrer ce candidat" data-id="' + answerId + '" data-uid="' + candidate + '" data-key="depense" data-form="aapStep1" data-pos="' + pos + '"><i class="fa fa-user-plus"></i></button>' +
                        '         <button type="button" class="btn leaderboard__editbtn  btnmodifydenycomment" data-denycomment="" data-placement="top" data-microtip-position="top" role="tooltip" aria-label="Modifier le commentaire" data-id="' + answerId + '" data-uid="' + candidate + '" data-key="depense" data-form="aapStep1" data-pos="' + pos + '"><i class="fa fa-comment"></i></button>';

                }
            }

            return html;
        },

        candidatecard: function (coremuObj, pos, candidate) {
            var html = "";
            var user = {};
            var haveBadge = false;

            if (notNull(candidate)) {
                if (!notNull(coremuObj.users[candidate])) {
                    ajaxPost(
                        '',
                        baseUrl + "/co2/element/get/type/citoyens/id/" + candidate,
                        null,
                        function (data) {
                            if (
                                typeof data.map != "undefined"
                            ) {
                                user = data.map;
                                coremuObj.users[candidate] = data.map;
                            }
                        },

                        null,
                        null,
                        {"async": false}
                    );
                } else {
                    user = coremuObj.users[candidate];
                }
            }

            html += '<div class="leaderboard__container arrageDepense' + pos + 'candidateEstimate' + candidate + '" data-type="list" data-active="true" data-pos="' + pos + '">' +
                '   <article class="arrageDepense' + pos + 'candidateEstimate' + candidate + 'validate leaderboard__profile ' + (notNull(coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].validate) && coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].validate == "validated" ? "check-estimates" : "") + ' btn-deny-toogle-comment " data-active="false" data-pos="' + pos + '" data-uid="' + candidate + '">' +
                '      <img src="' + (notNull(user.profilImageUrl) ? user.profilImageUrl : modules.co2.url + "/images/thumb/default_citoyens.png") + '" alt="." class="leaderboard__picture">' +
                '      <span class="leaderboard__name"> ' + user.name + ' ';
            if (notNull(user.badges) && notNull(coremuObj.cache.arrageDepense[pos].badge) && coremuObj.cache.arrageDepense[pos].badge.length > 0) {
                html += '<br><span class=""><small>' +
                    '        <i class="fa fa-id-badge"></i>  ' + coremuObj.checkBadgeMatch(Object.keys(coremuObj.cache.arrageDepense[pos].badge), Object.keys(user.badges)) + '</span>' +
                    '     </small></span>';
            }
            html += '   </span>';

            html += '      <span class="leaderboard__value nowrap">';
            if (coremuObj.cache.arrageDepense[pos].asssignationType != "percentage" && coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].hour != 0) {
                html += '               <span class="arrageDepense' + pos + 'candidateEstimate' + candidate + 'hour' + '" >' +
                    '                   ' + coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].hour +
                    '               </span> ' +
                    '               <span >' +
                    '                   h <i class="fa fa-clock-o"></i>' +
                    '               </span> - ';
            }

            html +=    '        <span class="arrageDepense' + pos + 'candidateEstimate' + candidate + 'totalestimateConfirmed' + '" >' +
                '                   ' + coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].totalestimateConfirmed +
                '               </span>' +
                '               <span>€</span> / ' +
                '               <span class="arrageDepense' + pos + 'candidateEstimate' + candidate + 'totalestimate' + '" >' +
                                    coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].totalestimate +
                '               </span>' +
                '               <span>€</span>' +
                '           </span>';

            html += '   </article>' +
                '   <div class="arrow-down-comment" data-active="false" data-pos="' + pos + '" data-uid="' + candidate + '"></div>' +
                '   <div class="denycomment" data-active="false" data-pos="' + pos + '" data-uid="' + candidate + '">' +
                '      <table class="table table-striped">' +
                '         <tbody class="candidatetablecomment' + pos + candidate + 'Container">' +
                coremuObj.block.candidatetablecomment(coremuObj, pos, candidate) +
                '         </tbody>' +
                '      </table>' +
                '   </div>' +
                '   <span style="">' +
                '      <div class=" action-btn-group candidatecardbtn' + pos + candidate + 'Container">' +
                coremuObj.block.candidatecardbtn(coremuObj, pos, candidate) +
                '      </div>' +
                '   </span>' +
                '</div>';

            return html;
        },

        candidatecardbtn: function (coremuObj, pos, candidate) {
            var html = '';
            var attribution = '';
            var btnclass = '';
            var tooltips = '';
            var data = '';
            var assignicon = '';

            var tooltipsvalidate = "";
            var dataval = "";

            var tooltipsdeny = "";
            var datadenyval = "";
            var candidatASBArray = [];
            var max = 0;
            var iconValidate = "";
            var candidatassignBudget = 0;
            var isDisabled = "disabled";

            if (coremuObj.roles.canCandidate(coremuObj)) {
                if (typeof coremuObj.cache.arrageDepense[pos] != "undefined") {

                    if (typeof coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate] != "undefined"
                        && typeof coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].assignpricelist != "undefined"
                    ) {
                        if(!Array.isArray(coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].assignpricelist)){
                            candidatASBArray = Object.values(coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].assignpricelist);
                        }else {
                            candidatASBArray = coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].assignpricelist;
                        }
                    }
                    ;

                    if (coremuObj.cache.arrageDepense[pos].asssignationType == "assignBudget") {
                        attribution = "assignBudget";
                        btnclass = "btnAssignBudgetArray";
                        tooltips = "Demander une CoRénumeration";
                        assignicon = "fa-money";
                        max = coremuObj.cache.arrageDepense[pos].price - coremuObj.cache.arrageDepense[pos].totalEnveloppe + coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].totalestimate
                        data = 'data-max="' + max + '" data-assignpricelist=\'' + JSON.stringify(candidatASBArray) + '\'';
                    } else {
                        attribution = "depense";
                        btnclass = "btnaddpercent";
                        tooltips = "Ajouter une pourcentage de budget à ce candidat";
                        assignicon = "fa-percent";
                        data = 'data-percentage="' + coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].percentage + '"';
                    }

                    if (coremuObj.status == "project") {
                        isDisabled = "";
                    }else{
                        tooltips = "Le proposition n'est pas encore en projet";
                    }

                    if (
                        notNull(coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].validate) &&
                        coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].validate == "validated"
                    ) {
                        tooltipsvalidate = "Invalider ce candidat";
                        dataval = "";
                        iconValidate = "fa-times";

                    } else {
                        tooltipsvalidate = "Valider ce candidat";
                        dataval = "validated";
                        iconValidate = 'fa-check';
                    }

                    if (
                        typeof coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].deny != "undefined" &&
                        (coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].deny == "true" || coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].deny == true)
                    ) {
                        tooltipsdeny = "";
                        denydataval = "";
                    } else {
                        tooltipsdeny = "Récaler ce candidat";
                        denydataval = "true";
                    }

                }

                if (
                    typeof coremuObj.cache.arrageDepense == "undefined" ||
                    typeof coremuObj.cache.arrageDepense[pos] == "undefined" ||
                    typeof coremuObj.cache.arrageDepense[pos].lock == "undefined" ||
                    coremuObj.cache.arrageDepense[pos].lock != "locked"
                ) {

                    html += '<button ' +
                        '       type="button" class="btn leaderboard__editbtn  estibtndelete" data-microtip-position="top" role="tooltip" aria-label="Supprimer ce candidat" ' +
                        '       data-id="' + answerId + '" data-uid="' + candidate + '" ' +
                        '       data-uname="' + coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].name + '" data-price="" data-key="depense" data-form="aapStep1" data-pos="' + pos + '">' +
                        '           <i class="fa fa-trash"></i>' +
                        '   </button>';

                    html += '<button ' + isDisabled +
                        '       type="button" class="btn leaderboard__editbtn  ' + btnclass + '" data-placement="top" ' +
                        '       data-microtip-position="top" role="tooltip" aria-label="' + tooltips + '" ' + data + ' data-id="' + answerId + '" data-uid="' + candidate + '" data-uname="' + coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].name + '" data-price="" data-days="" data-key="depense" data-form="aapStep1" data-pos="' + pos + '"><i class="fa ' + assignicon + '"></i></button>';

                    html += '<button type="button" class="btn leaderboard__editbtn  btncheckestimate" data-placement="top" data-microtip-position="top" role="tooltip" aria-label="' + tooltipsvalidate + '" data-id="' + answerId + '" data-uid="' + candidate + '"  data-key="depense" data-form="aapStep1" data-pos="' + pos + '" data-val="' + dataval + '"><i class="fa ' + iconValidate + '"></i></button>';

                    html += '<button type="button" class="btn leaderboard__editbtn  btncheckdeny" data-placement="top" data-microtip-position="top" role="tooltip" aria-label="' + tooltipsdeny + '" data-id="' + answerId + '" data-uid="' + candidate + '" data-key="depense" data-form="aapStep1" data-pos="' + pos + '" data-val="' + denydataval + '"><i class="fa fa-user-times"></i></button>';

                }
            }

            return html;
        },

        candidatetablecomment: function (coremuObj, pos, candidate) {
            var html = "";
            var icon
            var label = [];
            var price = [];
            var ischecked = [];
            var checkAssignclass = "";
            var dataAction = "";
            var checklabel = [];
            var phour = [];
            var hourlyRate = [];

            var candidatASBArray = [];
            if (typeof coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate] != "undefined"
                && typeof coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].assignpricelist != "undefined"
            ) {
                candidatASBArray = coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].assignpricelist;
            }
            ;

            if (coremuObj.cache.arrageDepense[pos].asssignationType == "percentage") {
                phour[0] = "";
                hourlyRate[0] = "";
                if (notNull(coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].totalestimate)) {
                    if (isNaN(coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].percentage)) {
                        label[0] = " pourcentage attribué : 0%";
                    }else{
                        label[0] = " pourcentage attribué :" + coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].percentage + "%";
                    }
                    price[0] = coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].totalestimate + '€';
                }
                if (notNull(coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].percentageState) && coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].percentageState == "validated") {
                    ischecked[0] = true;
                    dataAction[0] = "";
                    checklabel[0] = "checked";
                } else {
                    ischecked[0] = false;
                    dataAction = " validated";
                    checklabel[0] = "";
                }
                checkAssignclass = "checkpercentageState";
            } else {
                if (notNull(coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].assignpricelist)) {
                    $.each(coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].assignpricelist, function (index, value) {
                        label.push(value.label);
                        price.push(value.price) + "€" ;
                        if (typeof value.hour != "undefind") {
                            phour.push(value.hour);
                        } else {
                            phour.push("0€");
                        }
                        if (typeof value.hourlyRate != "undefind") {
                            hourlyRate.push(value.hourlyRate);
                        } else {
                            hourlyRate.push("0");
                        }
                        if (typeof value.check != "undefind" && (value.check == true || value.check == "true")) {
                            ischecked.push(true);
                            dataAction = "false";
                            checklabel.push("checked");
                        } else {
                            ischecked.push(false);
                            dataAction = "true";
                            checklabel.push("");
                        }
                    });
                }
                checkAssignclass = "checkAssignBudgetArray";

            }

            $.each(label, function (index, value) {
                html += '<tr>' +
                    '               <td>' + label[index] + '</td>' +
                    '               <td>' + price[index] + '</td>';

                if (notNull(phour[index]) && phour[index] != 0){
                    html += '               <td>' + phour[index] + ' h </td>' ;
                }else{
                    html += '               <td></td>' ;
                }
                if ( notNull(hourlyRate[index]) && hourlyRate[index] != 0){
                    html += '               <td>' + hourlyRate[index] + '€/h </td>' ;
                }else{
                    html += '               <td></td>' ;
                }
                if (
                    coremuObj.roles.isFormAdmin(coremuObj)
                    && (
                        typeof coremuObj.cache.arrageDepense == "undefined" ||
                        typeof coremuObj.cache.arrageDepense[pos] == "undefined" ||
                        typeof coremuObj.cache.arrageDepense[pos].lock == "undefined" ||
                        coremuObj.cache.arrageDepense[pos].lock != "locked"
                    )
                    && (
                        typeof coremuObj.cache.arrageDepense != "undefined" &&
                        typeof coremuObj.cache.arrageDepense[pos] != "undefined" &&
                        typeof coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate] != "undefined" &&
                        coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].validate == "validated" &&
                        typeof price != "undefined" &&
                        typeof price[index] != "undefined"
                    )
                ) {
                    html += '<td class="">' +
                        '        <input ' + checklabel[index] + ' class="' + checkAssignclass + '"  data-assignpricelist=\'' + JSON.stringify(candidatASBArray) + '\' data-action="' + dataAction + '" data-id="' + answerId + '" data-uid="' + candidate + '" data-uprice="' + price[index].toString() + '" data-unumber="' + index + '" data-price="' + price[index].toString() + '"  data-key="depense" data-form="aapStep1" data-pos="' + pos + '" type="checkbox" ">' +
                        '    </td>';
                    //html += '    <td><button class="btn btn-success btn-xs checkAssignBudgetArray" type="button" data-assignpricelist=\'' + JSON.stringify(candidatASBArray) + '\' data-action="' + !ischecked[index].toString() + '" data-id="' + answerId + '" data-uid="' + candidate + '" data-uprice="' + price[index].toString() + '" data-unumber="' + index + '" data-price="' + price[index].toString() + '"  data-key="depense" data-form="aapStep1" data-pos="' + pos + '"><i class="fa '+(ischecked[0] ? "fa-check" : "fa-circle-o")+'"></i> </button> </td>';

                } else if (
                    coremuObj.roles.isFormAdmin(coremuObj)
                    && (
                        typeof coremuObj.cache.arrageDepense == "undefined" ||
                        typeof coremuObj.cache.arrageDepense[pos] == "undefined" ||
                        typeof coremuObj.cache.arrageDepense[pos].lock == "undefined" ||
                        coremuObj.cache.arrageDepense[pos].lock != "locked"
                    )
                    && (
                        typeof coremuObj.cache.arrageDepense == "undefined" ||
                        typeof coremuObj.cache.arrageDepense[pos] == "undefined" ||
                        typeof coremuObj.cache.arrageDepense[pos].validate == "undefined" ||
                        coremuObj.cache.arrageDepense[pos].validate != "validated"
                    )
                ) {
                    html += '<td> <span class="text-success"> <i class="fa ' + (ischecked[0] ? "fa-check" : "fa-exlamation") + '"></i> </span> </td>'
                } else {
                    html += '<td> <span class="text-success"> <i class="fa ' + (ischecked[0] ? "fa-check" : "fa-exlamation") + '"></i>  </span> </td>'
                }
                html += '</tr>';
            })

            if (
                coremuObj.cache.arrageDepense[pos].asssignationType != "percentage"
                &&
                (
                    !notNull(coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].assignpricelist)
                    || coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].assignpricelist.length == 0
                )
                ) {
                    html += "(pas de corénumeration)";
                }
            return html;
        },

        tocandidatebtn: function (coremuObj, pos) {
            var html = "";
            if (coremuObj.cache.arrageDepense[pos].tocandidatebtn) {
                html += '<div class="toogle-maximum arrageDepense' + pos + 'tocandidatebtn" >' +
                    '      <button type="button" data-id="' + answerId + '" data-key="depense" data-form="aapStep1" data-kunik="budgetdepense" data-pos="' + pos + '" class="newbtncandidate btn">' +
                    '           Candidater                                                            ' +
                    '       </button>' +
                    '</div>';
            }

            return html;
        },

        valideRow: function (coremuObj, pos) {
            var html = "";
            html += '           <div class="validerow arrageDepense' + pos + 'valideRow">' +
                '               <label class=""><i class="fa fa-check"></i> Budget consomé </label>' +
                '           </div>';
            return html;
        },

        isdeniedCandidate: function (coremuObj, pos, candidate) {
            var html = "";
            var user = {};
            var haveBadge = false;

            if (notNull(candidate)) {
                if (!notNull(coremuObj.users[candidate])) {
                    ajaxPost(
                        '',
                        baseUrl + "/co2/element/get/type/citoyens/id/" + candidate,
                        null,
                        function (data) {
                            if (
                                typeof data.map != "undefined"
                            ) {
                                user = data.map;
                                coremuObj.users[candidate] = data.map;
                            }
                        },

                        null,
                        null,
                        {"async": false}
                    );
                } else {
                    user = coremuObj.users[candidate];
                }
            }

            html += '<div class="leaderboard__container arrageDepense' + pos + 'candidateEstimate' + candidate + '" data-type="list" data-active="true" data-pos="' + pos + '">' +
                '   <article class="arrageDepense' + pos + 'candidateEstimate' + candidate + 'validate leaderboard__profile ' + (notNull(coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].validate) && coremuObj.cache.arrageDepense[pos].candidateEstimate[candidate].validate == "validated" ? "check-estimates" : "") + ' btn-deny-toogle-comment " data-active="false" data-pos="' + pos + '" data-uid="' + candidate + '">' +
                '      <img src="' + (notNull(user.profilImageUrl) ? user.profilImageUrl : modules.co2.url + "/images/thumb/default_citoyens.png") + '" alt="." class="leaderboard__picture">' +
                '      <span class="leaderboard__name"> ' + user.name + ' ' +
                '           <br> <small>(vous avez étés récalé de cette ligne de depense)</small>';

            html += '   </span>';

            html += '   </article>' +
                '   <div class="arrow-down-comment" data-active="false" data-pos="' + pos + '" data-uid="' + candidate + '"></div>' +
                '   <div class="denycomment" data-active="false" data-pos="' + pos + '" data-uid="' + candidate + '">' +
                '   </div>' +
                '   <span style="">' +
                '      <div class=" action-btn-group">' +
                coremuObj.block.deniedcandidatecardbtn(coremuObj, pos, candidate) +
                '      </div>' +
                '   </span>' +
                '</div>';

            return html;
        },

        montantgroupbtn: function (coremuObj, pos) {
            var html = "";
            html += '<span> <b> Montant </b></span>' +
                coremuObj.block.montantbtn(coremuObj, pos);
            return html;
        },

        financerTable: function (coremuObj, pos) {
            var html = "";
            $.each(coremuObj.cache.arrageDepense[pos].financement, function (index, value) {
                html += coremuObj.block.financementline(coremuObj, pos, index);
            });
            return html;
        },

        financementline: function (coremuObj, pos, lineId) {
            var html = '';

            var financer = '';

            if (typeof coremuObj.cache.arrageDepense[pos].financement[lineId].name != 'undefined') {
                financer = coremuObj.cache.arrageDepense[pos].financement[lineId].name;
            } else if (notNull(coremuObj.cache.parent) && notNull(coremuObj.cache.parent.name)) {
                financer = coremuObj.cache.parent.name;
            }

            if(isNaN(parseInt((coremuObj.cache.arrageDepense[pos].financement[lineId].amount * 100) / coremuObj.cache.arrageDepense[pos].price))) {
                html += '          <tr class="financement' + pos + lineId + '">' +
                    '                  <td class="col-xs-4">' +
                    '                     <h5>' + financer + '</h5>' +
                    '                  </td>' +
                    '                  <td class="col-xs-5"></td>' +
                    '                  <td class="col-xs-2">' + coremuObj.cache.arrageDepense[pos].financement[lineId].amount + '</td>' +
                    '                  <td class="col-xs-2"> <span class="pull-right label label-primary">0%</span></td>' +
                    '                  <td class="py-3">' +
                    '                     <div class="esti-btn">' +
                    coremuObj.block.financementlinebtn(coremuObj, pos, lineId) +
                    '                     </div>' +
                    '                  </td>' +
                    '               </tr>';
            }else{
                html += '          <tr class="financement' + pos + lineId + '">' +
                    '                  <td class="col-xs-4">' +
                    '                     <h5>' + financer + '</h5>' +
                    '                  </td>' +
                    '                  <td class="col-xs-5"></td>' +
                    '                  <td class="col-xs-2">' + coremuObj.cache.arrageDepense[pos].financement[lineId].amount + '</td>' +
                    '                  <td class="col-xs-2"> <span class="pull-right label label-primary">' + (parseInt((coremuObj.cache.arrageDepense[pos].financement[lineId].amount * 100) / coremuObj.cache.arrageDepense[pos].price)) + '%</span></td>' +
                    '                  <td class="py-3">' +
                    '                     <div class="esti-btn">' +
                    coremuObj.block.financementlinebtn(coremuObj, pos, lineId) +
                    '                     </div>' +
                    '                  </td>' +
                    '               </tr>';
            }

            return html;
        },

        financementlinebtn: function (coremuObj, pos, lineId) {
            var html = '';

            var datafinid = '';
            var dataname = '';
            var dataemail = '';

            if (typeof coremuObj.cache.arrageDepense[pos].financement[lineId].id != 'undefined') {
                datafinid = ' data-finid="' + coremuObj.cache.arrageDepense[pos].financement[lineId].id + '" ';
            } else if (typeof coremuObj.cache.arrageDepense[pos].financement[lineId].email != 'undefined') {
                dataemail = ' data-email="' + coremuObj.cache.arrageDepense[pos].financement[lineId].email + '" ';
            } else if (typeof coremuObj.cache.arrageDepense[pos].financement[lineId].name != 'undefined') {
                dataname = ' data-name="' + coremuObj.cache.arrageDepense[pos].financement[lineId].name + '" ';
            }

            html += '<button ' + datafinid + dataemail + dataname + ' type="button" class="td-datetimelabel2  fibtnedit" data-id="' + answerId + '" data-uid="' + pos + '" data-email="' + coremuObj.cache.arrageDepense[pos].financement[lineId].email + '" data-name="' + coremuObj.cache.arrageDepense[pos].financement[lineId].name + '"  data-finamount="' + coremuObj.cache.arrageDepense[pos].financement[lineId].amount + '" data-key="depense" data-form="aapStep1" data-pos="' + 0 + '"><i class="fa fa-pencil"></i></button><span class="dv"></span>' +
                '    <button type="button" class="td-datetimelabel2  fibtndelete" data-id="' + answerId + '" data-uid="' + pos + '" data-key="depense" data-form="aapStep1" data-pos="' + pos + '"><i class="fa fa-trash"></i></button>';


            return html;
        },

        progressbarfin: function (coremuObj, pos) {
            var html = '';
            var percentageFinanced = isNaN(coremuObj.cache.arrageDepense[pos].percentageFinanced) ? 0 : coremuObj.cache.arrageDepense[pos].percentageFinanced;
            var percentageConfirmed = isNaN(coremuObj.cache.arrageDepense[pos].percentageConfirmed) ? 0 : coremuObj.cache.arrageDepense[pos].percentageConfirmed;
            var percentageNotFinanced = isNaN(coremuObj.cache.arrageDepense[pos].percentageConfirmed) ? 0 : percentageFinanced - percentageConfirmed;

            html += '               <div class="progress" style="height: 23px">';
            if (percentageFinanced > percentageConfirmed) {
                html += '             <div class="progress-bar progress-bar-success2 " role="progressbar" style="width:' + percentageConfirmed + '%">' +
                    '                     <span style="font-size: 13px">Dépensé ' + percentageConfirmed + ' %</span>' +
                    '                  </div>' +
                    '                  <div class="progress-bar progress-bar-success" role="progressbar" style="width:' + percentageFinanced + '%">' +
                    '                     <span style="font-size: 13px">Financé ' + percentageFinanced + ' %</span>' +
                    '                  </div>';
            } else {
                html += '                  <div class="progress-bar progress-bar-success" role="progressbar" style="width:' + percentageFinanced + '%">' +
                    '                     <span style="font-size: 13px">Financé ' + percentageFinanced + ' %</span>' +
                    '                  </div>' +
                    '                  <div class="progress-bar progress-bar-success2 " role="progressbar" style="width:100%">' +
                    '                     <span style="font-size: 13px">Dépensé ' + percentageConfirmed + ' %</span>' +
                    '                  </div>';
            }

            html += '              <div class="progress-bar progress-bar-nf" role="progressbar" style="width:' + percentageNotFinanced + '%">' +
                '                     <span style="font-size: 13px">Non financé ' + percentageNotFinanced + ' %</span>' +
                '                  </div>' +
                '               </div>';

            return html;
        },

        newFormFinancer : function (coremuObj , pos ) {
           html ="";

           html += '<div id="container" class="new-form-financer">' +
               '            <main class="mainc">' +
               '                <div id="switchcom">' +
               '                    <div class="btn-group depenseoceco">' +
               '                        <button type="button" id="dcombtn" class="dcombtn switchcombtn btn active" data-switchactbtn="dcombtn" data-removebtn="icombtn" data-switchactdiv="dcomdiv" data-removediv="icomdiv" >Depuis la communauté </button>' +
               '                        <button type="button" id="icombtn" class="icombtn switchcombtn btn " data-switchactbtn="icombtn" data-removebtn="dcombtn" data-switchactdiv="icomdiv" data-removediv="dcomdiv">Inexistant dans la communauté</button>' +
               '                    </div>' +
               '                    <div style="clear: both;"></div>' +
               '                </div>' +
               '                <div class="dcomdiv">' +
               '                    <label class="label-esti">Financeur <small class="small" style="color: #4b4e56"> (laisser vide si autofinancement) </small></label>' +
               '                </div>' +
               '                <div id="dcomdiv" class="dcomdiv switchcomdiv">' +
               '                    <div class="col-sm-12 col-md-9">' +
               '                        <select id="financer" class="col-md-4 form-control fi-financer todoinput">' +
               '                            <option value=0 >Quel financeur</option>';
                                           $.each(coremuObj.community , function(index, community){
                                               if(notNull(community.name) && notNull(community.roles) && !$.isEmptyObject(community.roles) && $.inArray("Financeur" , community.roles) !== -1){
                                                   html += '<option value="'+index+'">'+community.name+'</option>';
                                               }
                                           });
               html +='                 </select>' +
               '                    </div>' +
               '                    <div class="col-sm-12 col-md-3">' +
               '                        <button class="btn btn-default sup-btn " >Ajouter financeur</button>' +
               '                    </div>' +
               '                    <div style="clear: both;"></div>' +
               '                </div>' +
               '                <div class="icomdiv ocecowebview  hide">' +
               '                    <label class="label-esti">Nom</label>' +
               '                    <label class="label-esti">Email</label>' +
               '                </div>' +
               '                <div class="icomdiv hide">' +
               '                    <!-- <label class=" spanfi-name">Nom</label><label class=" spanfi-mail">Email</label> -->' +
               '                    <label class="label-mobi">Nom</label>' +
               '                    <input id="" class="form-control todoinput fi-name" type="text" spellcheck="false" placeholder="Nom" onfocus="this.placeholder=\'\'" onblur="this.placeholder=\'Nom\'" />' +
               '                    <label class="label-mobi">Email</label>' +
               '                    <input id="new-credit-value" class="form-control fi-mail todoinput" type="text" spellcheck="false" placeholder="Email" onfocus="this.placeholder=\'\'" onblur="this.placeholder=\'Email\'" />' +
               '                    <div style="clear: both;"></div>' +
               '                </div>' +
               '                <div class="envdiv" >' +
               '                    <label class="label-esti enveloppelabel"><i class="fa fa-info-circle"></i> Enveloppe pour ce financeur : </label>' +
               '                    <label class="label-esti fi-Env enveloppelabelnum"> </label>' +
               '                </div>' +
               '                <div class="envdiv">' +
               '                    <label class="label-esti enveloppelabel"><i class="fa fa-info-circle"></i> Reste dans l\'enveloppe pour ce financeur : </label>' +
               '                    <label class="label-esti fi-resteEnv enveloppelabelnum"> </label>' +
               '                </div>' +
               '                <div class="envdiverror" >' +
               '                    <label class=" enveloppewarning label-esti enveloppelabel"> <span class="restEnvWarning"> </span> </label>' +
               '                </div>' +
               '                <div class="">' +
               '                    <label class="label-mobi">Libélé du financement</label>' +
               '                    <input id="" class="form-control todoinput fi-fond" type="text" spellcheck="false" placeholder="Libélé du financement" onfocus="this.placeholder=\'\'" onblur="this.placeholder=\'Libélé du financement\'" />' +
               '                    <label class="label-mobi">Montant Financé</label>' +
               '                    <input data-max="'+parseInt(coremuObj.cache.arrageDepense[pos].left)+'" id="new-credit-value" class="form-control fi-montant todoinput" type="number" spellcheck="false" placeholder="Montant Financé" onfocus="this.placeholder=\'\'" onblur="this.placeholder=\'Montant Financé\'" />' +
               '                    <div style="clear: both;"></div>' +
               '                </div>' +
               '                <div id="single-line"></div>' +
               '                <div>' +
               '                    <table class=\'table table-bordered table-hover  directoryTable oceco-styled-table\'>' +
               '                        <tr>' +
               '                            <th class="thead-light">  </th>' +
               '                            <th class="thead-light"> Coût </th>' +
               '                            <th class="thead-light"> Reste à financer </th>' +
               '                            <th class="thead-light"> montant completé par la somme saisi </th>' +
               '                        </tr>';
                           //$.each(coremuObj.cache.arrageDepense, function(index , line){
                               html+= '<tr data-id="'+pos+'">' +
                               '            <td>'+pos+'</td>' +
                               '            <td>'+coremuObj.cache.arrageDepense[pos].price+'</td>' +
                               '            <td>'+coremuObj.cache.arrageDepense[pos].left.toString()+'</td>' +
                               '            <td> 0 </td>' +
                               '       </tr>';
                           //})
               html +='            </table>' +
               '                </div>' +
               '                <div id="single-line"></div>' +
               '                <ul id="financer-list">' +
               '                </ul>' +
               '            </main>' +
               '        </div>';

              return html;
       },

        newFormFinancerAll : function (coremuObj , pos ) {
            html ="";

            html += '<div id="container" class="new-form-financeallline">' +
                '            <main class="mainc">' +
                '                <div id="switchcom">' +
                '                    <div class="btn-group depenseoceco">' +
                '                        <button type="button" id="dcombtn" class="dcombtn switchcombtn btn active" data-switchactbtn="dcombtn" data-removebtn="icombtn" data-switchactdiv="dcomdiv" data-removediv="icomdiv" >Depuis la communauté </button>' +
                '                        <button type="button" id="icombtn" class="icombtn switchcombtn btn " data-switchactbtn="icombtn" data-removebtn="dcombtn" data-switchactdiv="icomdiv" data-removediv="dcomdiv">Inexistant dans la communauté</button>' +
                '                    </div>' +
                '                    <div style="clear: both;"></div>' +
                '                </div>' +
                '                <div class="dcomdiv">' +
                '                    <label class="label-esti">Financeur <small class="small" style="color: #4b4e56"> (laisser vide si autofinancement) </small></label>' +
                '                </div>' +
                '                <div id="dcomdiv" class="dcomdiv switchcomdiv">' +
                '                    <div class="col-sm-12 col-md-9">' +
                '                        <select id="financer" class="col-md-4 form-control fi-financer todoinput">' +
                '                            <option value=0 >Quel financeur</option>';
                                                    $.each(coremuObj.community , function(index, community){
                                                        if(notNull(community.name) && notNull(community.roles) && !$.isEmptyObject(community.roles) && $.inArray("Financeur" , community.roles) !== -1){
                                                            html += '<option value="'+index+'">'+community.name+'</option>';
                                                        }
                                                    });
                html +='                 </select>' +
                '                    </div>' +
                '                    <div class="col-sm-12 col-md-3">' +
                '                        <button class="btn btn-default sup-btn " style=" display: block;">Ajouter financeur</button>' +
                '                    </div>' +
                '                    <div style="clear: both;"></div>' +
                '                </div>' +
                '                <div class="icomdiv ocecowebview  hide">' +
                '                    <label class="label-esti">Nom</label>' +
                '                    <label class="label-esti">Email</label>' +
                '                </div>' +
                '                <div class="icomdiv hide">' +
                '                    <!-- <label class=" spanfi-name">Nom</label><label class=" spanfi-mail">Email</label> -->' +
                '                    <label class="label-mobi">Nom</label>' +
                '                    <input id="" class="form-control todoinput fi-name" type="text" spellcheck="false" placeholder="Nom" onfocus="this.placeholder=\'\'" onblur="this.placeholder=\'Nom\'" />' +
                '                    <label class="label-mobi">Email</label>' +
                '                    <input id="new-credit-value" class="form-control fi-mail todoinput" type="text" spellcheck="false" placeholder="Email" onfocus="this.placeholder=\'\'" onblur="this.placeholder=\'Email\'" />' +
                '                    <div style="clear: both;"></div>' +
                '                    <!--  <label class=" spanfi-fond">Libélé du financement</label><label class=" spanfi-montant">Montant Financé</label><label class=" spanfi-btn"></label> -->' +
                '                </div>' +
                '                <div class="envdiv" >' +
                '                    <label class="label-esti enveloppelabel"><i class="fa fa-info-circle"></i> Enveloppe : </label>' +
                '                    <label class="label-esti fi-Env enveloppelabelnum"> </label>' +
                '                </div>' +
                '                <div class="envdiv">' +
                '                    <label class="label-esti enveloppelabel"><i class="fa fa-info-circle"></i> Reste dans l\'enveloppe : </label>' +
                '                    <label class="label-esti fi-resteEnv enveloppelabelnum"> </label>' +
                '                </div>' +
                '                <div class="envdiverror" >' +
                '                    <label class=" enveloppewarning label-esti enveloppelabel"><span class="restEnvWarning">  </span> </label>' +
                '                </div>' +
                '                <div class="">' +
                '                    <label class="label-mobi">Libélé du financement</label>' +
                '                    <input id="" class="form-control todoinput fi-fond" type="text" spellcheck="false" placeholder="Libélé du financement" onfocus="this.placeholder=\'\'" onblur="this.placeholder=\'Libélé du financement\'" />' +
                '                    <label class="label-mobi">Montant Financé</label>' +
                '                    <input data-max="'+parseInt(coremuObj.cache.totalleft)+'" max="'+parseInt(coremuObj.cache.totalleft)+'" id="new-credit-value" class="form-control fi-montant todoinput" type="number" spellcheck="false" placeholder="Montant Financé" onfocus="this.placeholder=\'\'" onblur="this.placeholder=\'Montant Financé\'" />' +
                '                    <div style="clear: both;"></div>' +
                '                </div>' +
                '                <div id="single-line"></div>' +
                '                <div>' +
                '                    <label> Reste à financer total : <b> <?= $totalReste ?> </b></label>' +
                '                    <table class=\'table table-bordered table-hover  directoryTable oceco-styled-table\'>' +
                '                        <tr>' +
                '                            <th class="thead-light">  </th>' +
                '                            <th class="thead-light"> Coût </th>' +
                '                            <th class="thead-light"> Reste à financer </th>' +
                '                            <th class="thead-light"> montant completé par la somme saisi </th>' +
                '                        </tr>';
                                                        $.each(coremuObj.cache.arrageDepense, function(index , line){
                                                            html+= '<tr data-id="'+index+'">' +
                                                                '            <td>'+index+'</td>' +
                                                                '            <td>'+line.price+'</td>' +
                                                                '            <td>'+line.left+'</td>' +
                                                                '            <td> 0 </td>' +
                                                                '       </tr>';
                                                        })
                html += '<tr data-id="total">' +
                    '            <td>total</td>' +
                    '            <td>'+coremuObj.cache.total+'</td>' +
                    '            <td>'+coremuObj.cache.totalleft+'</td>' +
                    '            <td> 0 </td>' +
                    '       </tr>' +
                    '          </table>' +
                '                </div>' +
                '                <div id="single-line"></div>' +
                '                <ul id="financer-list">' +
                '                </ul>' +
                '            </main>' +
                '        </div>';

            return html;

        },

        tags : function(coremuObj, pos){
            var html = "";
            if (notNull(coremuObj.cache.arrageDepense[pos].tags) && typeof coremuObj.cache.arrageDepense[pos].tags == 'string' && coremuObj.cache.arrageDepense[pos].tags.split(',').length > 0) {
                $.each(coremuObj.cache.arrageDepense[pos].tags.split(','), function (index, value) {
                    if(value != "") {
                        html += '  <span class="lddtag">' + value + '</span>';
                    }
                });
            }
            return html;
        },

        sankey : function(coremuObj , containerId , params , sankeyData , columnNames = []) {
            const container = d3
                .select(containerId);

            const tooltip = container
                .append("div")
                .style("opacity", 0)
                .attr("class", "tooltip")
                .style("background-color", "white")
                .style("border", "solid")
                .style("border-width", "2px")
                .style("border-radius", "5px")
                .style("padding", "5px");

            var mouseover = function(d) {
                tooltip
                    .style("opacity", 1)
                d3.select(this)
                    .style("stroke", "black")
                    .style("opacity", 1)
            }
            var mousemove = function(d) {
                tooltip
                    .html(d.source.name + " ➔ " + d.target.name + " : " + d.value + " €")
                    .style("left", (d3.mouse(this)[0]+70) + "px")
                    .style("top", (d3.mouse(this)[1]) + "px");
            }
            var mouseleave = function(d) {
                tooltip
                    .style("opacity", 0)
                d3.select(this)
                    .style("stroke", "none")
                    .style("opacity", 0.8)
            }

            // SVG frame
            // the same margin, width and height are used for both visualizations
            var margin = {
                top: 20,
                right: 20,
                bottom: 20,
                left: 20,
            };

            if(notNull(params) && notNull(params.margin)){
                margin = params.margin;
            }

            var sankeyContainerWidth = 1000;
            var sankeyContainerHeight = 600;

            if(notNull(params) && notNull(params.width)){
                sankeyContainerWidth.width;
            }

            if(notNull(params) && notNull(params.height)){
                sankeyContainerWidth.height;
            }

            const width = sankeyContainerWidth + (margin.left + margin.right);
            const height = sankeyContainerHeight + (margin.top + margin.bottom);

            const containerFrame = container
                .append('svg')
                .attr('viewBox', `0 0 ${width + (margin.left + margin.right)} ${height + (margin.top + margin.bottom)}`)
                .append('g')
                .attr('transform', `translate(${margin.left}, ${margin.top})`);

            // ZOOM feature
            // include a rectangle spanning the entire container, as to allow a translation on the wrapping group
            containerFrame
                .append('rect')
                .attr('x', 0)
                .attr('y', 0)
                .attr('width', width)
                .attr('height', height)
                .attr('fill', 'transparent');

            if (columnNames.length == 2){
                var distanceArr = [0, width - 120];
            } else {
                var distanceArr = [0, (width/2) - 50,  width - 120];
            }

            $.each(columnNames , function(cnid, cn){
                containerFrame.append("text")
                    .attr("x", distanceArr[cnid])
                    .attr("y", 10)
                    .style("fill", "#506db4")
                    .style("font-family", "Montserrat")
                    .style("font-size", "17px")
                    .style("z-index", "1000")
                    .style("font-weight", "bold")
                    .text(cn)
                    .raise();
            });

            function zoomFunction() {
                const { x, y, k } = d3.event.transform;
                containerFrame.attr('transform', `translate(${x} ${y}) scale(${k})`);
            }

            const zoom = d3
                .zoom()
                .scaleExtent([1, 5])
                .on('zoom', zoomFunction);

            containerFrame
                .call(zoom);

            // detail a color scale
            var color = d3
                .scaleOrdinal(d3.schemeSet3);

            if(notNull(params) && notNull(params.color)){
                color = params.color;
            }

            // detail the sankey function
            const sankey = d3
                .sankey()
                //.nodeAlign(d3.sankeyLeft)
                .nodeAlign(function (node) {
                    // you may specify the horizatonal location here
                    // i.e. if your data structure contain node.horizontalPosition (an integer)
                    // you can return node.horizontalPosition
                    return node.level; //align left
                })
                .extent([[1, 1], [width - 1, height - 6]]);
                // limit the nodes and links within the containing group
                //.extent([[0, 10], [width, height]]);

            // destructure the two arrays for the nodes and links in two variables
            const { nodes, links } = sankey(sankeyData);

            // detail in a defs block one linear gradient for each link
            // detail a unique identifier as to later call the id with the specified index
            const defs = containerFrame
                .append('defs');

            const linearGradients = defs
                .selectAll('linearGradient')
                .data(links)
                .enter()
                .append('linearGradient')
                .attr('id', d => `gradient${d.index}`)
                .attr("gradientUnits", "userSpaceOnUse")
                .attr("x1", d => d.source.x1)
                .attr("x2", d => d.target.x0);

            // linear gradient going from left to right and detailing a color based on the source and target values
            linearGradients
                .append('stop')
                .attr('offset', '0%')
                .attr('stop-color', d => color(d.source.index));

            linearGradients
                .append('stop')
                .attr('offset', '100%')
                .attr('stop-color', d => color(d.target.index));

            // detail a generator function for the links
            const sankeyLinks = d3
                .sankeyLinkHorizontal();

            // append a path element for each link
            // using the generator function
            containerFrame
                .selectAll('path.link')
                .data(links)
                .enter()
                .append('path')
                .attr('class', 'link')
                .attr('d', sankeyLinks)
                //.attr('fill', 'none')
                // stroke using the gradient
                .attr('stroke', d => `url(#gradient${d.index})`)
                // stroke width based on the width of each data point
                .attr('stroke-width', d => d.width)
                // alter the opacity on hover
                // detail also the data through a simple tooltip
                .attr('opacity', 0.5)
                .on('mouseenter', function (d) {
                    d3
                        .select(this)
                        .transition()
                        .attr('opacity', 1);
                    mouseover();
                })
                .on('mouseout', function () {
                    d3
                        .select(this)
                        .transition()
                        .attr('opacity', 0.5);
                    mouseleave();
                })
                .on("mousemove", mousemove)
                ;

            var nodePosY = d3.scaleLinear()
                .domain([0, 2])
                .range([0, height]);

            // append a rectangle for each node
            // using the fabricated values and the color based on the index
            containerFrame
                .selectAll('rect.node')
                .data(nodes)
                .enter()
                .append('rect')
                .attr('class', 'node')
                .attr('x', d => d.x0)
                .attr('y', d => d.y0)
                .attr('width', d => (d.x1 - d.x0))
                .attr('height', d => (d.y1 - d.y0))
                .attr('pointer-events', 'none')
                .attr('stroke', '#555')
                .attr('stroke-width', '0px')
                .attr("transform", function(d) {
                    return "translate(" + d.x + "," + 10 * d.level + ")";
                })
                .attr('fill', d => color(d.index));


            // for each node append also a text element, detailing the respective value
            // horizontally position the text after or before the rectangle elements for each node
            containerFrame
                .selectAll('text.node')
                .data(nodes)
                .enter()
                .append('text')
                .text(d => d.name)
                .attr('font-size', '15px')
                .attr('fill', '#111')
                .attr('x', (d) => {
                    if (d.sourceLinks.length > 0) {
                        return d.x0 + sankey.nodeWidth() + 5;
                    }
                    return d.x0 - 5;
                })
                .attr('y', d => (d.y1 + d.y0) / 2)
                .attr('pointer-events', 'none')
                .attr('alignment-baseline', 'middle')
                .attr('text-anchor', d => ((d.sourceLinks.length > 0) ? 'start' : 'end'));
        },

        pie : function(coremuObj , containerId , params , dataset , montant = ""){
            var width = 750;
            var height = 250;
            var thickness = 40;
            var duration = 750;
            var padding = 10;
            var opacity = .8;
            var opacityHover = 1;
            var otherOpacityOnHover = .8;
            var tooltipMargin = 13;

            var radius = Math.min(width-padding, height-padding) / 2;
            var color = d3.scaleOrdinal(d3.schemeSet3);
            var color2 = d3.scaleOrdinal(d3.schemeCategory20c);

            // legend dimensions
            var legendRectSize = 25; // defines the size of the colored squares in legend
            var legendSpacing = 6; // defines spacing between squares

            var svg = d3.select(containerId) // select element in the DOM with id 'chart'
                .append('svg') // append an svg element to the element we've selected
                .attr('width', width) // set the width of the svg element we just added
                .attr('height', height) // set the height of the svg element we just added
                .append('g') // append 'g' element to the svg element
                .attr('transform', 'translate(' + (width / 4) + ',' + (height / 2) + ')'); // our reference is now to the 'g' element. centerting the 'g' element to the svg element

            var arc = d3.arc()
                .innerRadius(0) // none for pie chart
                .outerRadius(radius); // size of overall chart

            var pie = d3.pie() // start and end angles of the segments
                .value(function(d) { return d.value; }) // how to extract the numerical data from each entry in our dataset
                .sort(null); // by default, data sorts in oescending value. this will mess with our animation so we set it to null

            // define tooltip
            var tooltip = d3.select(containerId) // select element in the DOM with id 'chart'
                .append("div")
                .style("opacity", 0)
                .attr("class", "tooltip")
                .style("background-color", "white")
                .style("border", "solid")
                .style("border-width", "2px")
                .style("border-radius", "5px")
                .style("padding", "5px");

            tooltip.append('span') // add divs to the tooltip defined above
                .attr('class', 'namepie'); // add class 'name' on the selection
            tooltip.append('br');
            tooltip.append('span') // add divs to the tooltip defined above
                .attr('class', 'valuepie'); // add class 'value' on the selection
            tooltip.append('br');
            tooltip.append('span') // add divs to the tooltip defined above
                .attr('class', 'percentpie'); // add class 'percent' on the selection

            dataset.forEach(function(d) {
                d.value = +d.value; // calculate value as we iterate through the data
                d.enabled = true; // add enabled property to track which entries are checked
            });

            // creating the chart
            var path = svg.selectAll('path') // select all path elements inside the svg. specifically the 'g' element. they don't exist yet but they will be created below
                .data(pie(dataset)) //associate dataset wit he path elements we're about to create. must pass through the pie function. it magically knows how to extract values and bakes it into the pie
                .enter() //creates placeholder nodes for each of the values
                .append('path') // replace placeholders with path elements
                .attr('d', arc)
                .style('stroke', '#555555')
                .style('stroke-width', 2)
                .attr('fill', function(d) { return color(d.index); }) // use color scale to define fill of each name in dataset
                .each(function(d) { this._current - d; })
            ; // creates a smooth animation for each track

            // mouse event handlers are attached to path so they need to come after its definition
            path.on('mouseover', function(d) {  // when mouse enters div
                var total = d3.sum(dataset.map(function(d) { // calculate the total number of tickets in the dataset
                    return (d.enabled) ? d.value : 0; // checking to see if the entry is enabled. if it isn't, we return 0 and cause other percentages to increase
                }));
                var percent = Math.round(1000 * d.data.value / total) / 10; // calculate percent
                tooltip.select('.namepie').html(d.data.name); // set current name
                tooltip.select('.valuepie').html(d.data.value+montant); // set current value
                tooltip.select('.percentpie').html((isNaN(percent) ? 0 : percent) +"%"); // set percent calculated above
                tooltip.style('opacity', 1); // set display
            });

            path.on('mouseout', function() { // when mouse leaves div
                tooltip.style('opacity', 0); // hide tooltip for that element
            });

            path.on('mousemove', function(d) { // when mouse moves
                tooltip.style('top', (d3.event.layerY + 10) + 'px') // always 10px below the cursor
                    .style('left', (d3.event.layerX + 10) + 'px'); // always 10px to the right of the mouse
            });

            // define legend
            var legend = svg.selectAll('.legend') // selecting elements with class 'legend'
                .data(color.domain()) // refers to an array of name from our dataset
                .enter() // creates placeholder
                .append('g') // replace placeholders with g elements
                .attr('class', 'legend') // each g is given a legend class
                .attr('transform', function(d, i) {
                    var height = legendRectSize + legendSpacing; // height of element is the height of the colored square plus the spacing
                    var offset =  height * color.domain().length / 2; // vertical offset of the entire legend = height of a single element & half the total number of elements
                    var horz =  8 * legendRectSize; // the legend is shifted to the left to make room for the text
                    var vert = i * height - offset; // the top of the element is hifted up or down from the center using the offset defiend earlier and the index of the current element 'i'
                    return 'translate(' + horz + ',' + vert + ')'; //return translation
                });

            // adding colored squares to legend
            legend.append('rect') // append rectangle squares to legend
                .attr('width', legendRectSize) // width of rect size is defined above
                .attr('height', legendRectSize) // height of rect size is defined above
                .style('fill', color) // each fill is passed a color
                .style('stroke', color) // each stroke is passed a color
                .style('stroke', '#555555')
                .style('stroke-width', 2)
                .on('click', function(name) {
                    var rect = d3.select(this); // this refers to the colored squared just clicked
                    var enabled = true; // set enabled true to default
                    var totalEnabled = d3.sum(dataset.map(function(d) { // can't disable all options
                        return (d.enabled) ? 1 : 0; // return 1 for each enabled entry. and summing it up
                    }));

                    if (rect.attr('class') === 'disabled') { // if class is disabled
                        rect.attr('class', ''); // remove class disabled
                    } else { // else
                        if (totalEnabled < 2) return; // if less than two name are flagged, exit
                        rect.attr('class', 'disabled'); // otherwise flag the square disabled
                        enabled = false; // set enabled to false
                    }

                    pie.value(function(d) {
                        if (d.name === name) d.enabled = enabled; // if entry name matches legend name
                        return (d.enabled) ? d.value : 0; // update enabled property and return value or 0 based on the entry's status
                    });

                    path = path.data(pie(dataset)); // update pie with new data

                    path.transition() // transition of redrawn pie
                        .duration(750) //
                        .attrTween('d', function(d) { // 'd' specifies the d attribute that we'll be animating
                            var interpolate = d3.interpolate(this._current, d); // this = current path element
                            this._current = interpolate(0); // interpolate between current value and the new value of 'd'
                            return function(t) {
                                return arc(interpolate(t));
                            };
                        });
                });

            // adding text to legend
            legend.append('text')
                .attr('x', legendRectSize + legendSpacing)
                .attr('y', legendRectSize - legendSpacing)
                .attr('font-size', '15px')
                .attr('fill', '#111')
                .text(function(d) {
                    if (dataset[d]["name"] !=  "") {
                        return dataset[d]["name"] + " " + dataset[d]["value"] +montant;
                    }else{
                        return "(label vide) " + dataset[d]["value"]+montant
                    }
                }); // return name

        }

    },

    deepDiffMapper : function () {
        return {
            VALUE_CREATED: 'created',
            VALUE_UPDATED: 'updated',
            VALUE_DELETED: 'deleted',
            VALUE_UNCHANGED: 'unchanged',
            map: function(obj1, obj2) {
                if (this.isFunction(obj1) || this.isFunction(obj2)) {
                    throw 'Invalid argument. Function given, object expected.';
                }
                if (this.isValue(obj1) || this.isValue(obj2)) {
                    return {
                        type: this.compareValues(obj1, obj2),
                        data: obj1 === undefined ? obj2 : obj1
                    };
                }

                var diff = {};
                for (var key in obj1) {
                    if (this.isFunction(obj1[key])) {
                        continue;
                    }

                    var value2 = undefined;
                    if (obj2[key] !== undefined) {
                        value2 = obj2[key];
                    }

                    diff[key] = this.map(obj1[key], value2);
                }
                for (var key in obj2) {
                    if (this.isFunction(obj2[key]) || diff[key] !== undefined) {
                        continue;
                    }

                    diff[key] = this.map(undefined, obj2[key]);
                }

                return diff;

            },
            compareValues: function (value1, value2) {
                if (value1 === value2) {
                    return this.VALUE_UNCHANGED;
                }
                if (this.isDate(value1) && this.isDate(value2) && value1.getTime() === value2.getTime()) {
                    return this.VALUE_UNCHANGED;
                }
                if (value1 === undefined) {
                    return this.VALUE_CREATED;
                }
                if (value2 === undefined) {
                    return this.VALUE_DELETED;
                }
                return this.VALUE_UPDATED;
            },
            isFunction: function (x) {
                return Object.prototype.toString.call(x) === '[object Function]';
            },
            isArray: function (x) {
                return Object.prototype.toString.call(x) === '[object Array]';
            },
            isDate: function (x) {
                return Object.prototype.toString.call(x) === '[object Date]';
            },
            isObject: function (x) {
                return Object.prototype.toString.call(x) === '[object Object]';
            },
            isValue: function (x) {
                return !this.isObject(x) && !this.isArray(x);
            }
        }
    }(),

    getPaths : function (coremuObj, obj, value, path = []) {
        const paths = [];
        if (obj) {
            for (const [key, val] of Object.entries(obj)) {
                if (val === value) {
                    paths.push([...path, key]);
                } else if (typeof val === 'object') {
                    paths.push(...coremuObj.getPaths(coremuObj, val, value, [...path, key]));
                }
            }
        }

        return paths;
    },

    updateNumber : function (countContainer , initial , final){
        //if(notNull(initial) && initial != "" &&  notNull(final) && final != "") {
            $(countContainer).prop('Counter', parseInt(initial)).animate({
                Counter: parseInt(final)
            }, {
                duration: 500,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        //}
    },

    updateColorClass: function (countContainer , initial , final){
        if (final == "") {
            $(countContainer).removeClass(initial);
        }else if(initial == ""){
            $(countContainer).addClass(final);
        }else{
            if ($(countContainer).hasClass(initial)) {
                $(countContainer).removeClass(initial).addClass(final);
            } else {
                $(countContainer).removeClass(final).addClass(initial);
            }
        }
    },

    showHideHtml: function (cardClass ,cardContainerClass , card , type="hide") {
        if (type == "show") {
            $(cardContainerClass).html(card);

            //$(cardClass).velocity('transition.slideUpIn', {delay: 3100, stagger: 100});
        } else {
            //$(cardClass).velocity('transition.slideDownOut', {delay: 3100, stagger: 100});

            $(cardClass).remove();
        }
    },

    reloadHtml: function (cardClass ,cardContainerClass , card ) {
        if(notNull(cardClass)) {
            //$(cardClass).velocity('transition.slideDownOut', {delay: 3100, stagger: 100});

            $(cardClass).remove();

            $(cardContainerClass).html(card);

            //$(cardClass).velocity('transition.slideUpIn', {delay: 3100, stagger: 100});
        }else {
            $(cardContainerClass).empty();

            $(cardContainerClass).html(card);

            //$(cardClass).velocity('transition.slideUpIn', {delay: 3100, stagger: 100});
        }
    },

    showAppend: function (cardClass ,cardContainerClass , card ) {
        if(!$(cardContainerClass).has(cardClass).length) {
            $(cardContainerClass).append(card);
        }
    },

    hideRemove: function (cardClass) {
        //$(cardClass).velocity('transition.slideUpOut', {delay: 3100, stagger: 100});
        $(cardClass).remove();
    },

    findValue : function(obj, value) {
        for (let key in obj) {
            if (obj[key] === value) {
                return [key];
            } else if (typeof obj[key] === "object") {
                let path = findValue(obj[key], value);
                if (path) {
                    return [key].concat(path);
                }
            }
        }
        return null;
    },

    fusionSankeyLinks : function(links) {
        const merged = links.reduce((acc, cur) => {
            const found = acc.find((item) => (item.source === cur.source && item.target === cur.target));
            if (found) {
                found.value += cur.value;
            } else {
                acc.push({ target : cur.target, source : cur.source, value : cur.value });
            }
            return acc;
        }
        , []);

        return merged;
    },

    cleanSankeyNodes : function(data) {
        var modified = [];
        $.each(data.links , function(i, link) {
            modified.push({ "target" : data.nodes[link.target] , "source" : data.nodes[link.source]})
        });
        const merged = data.nodes.reduce((acc, cur) => {
                const found = modified.find((item) => (item.source.name === cur.name || item.target.name === cur.name));
                if (found) {
                    acc.push(cur);
                }else{
                    acc.push({"name" : " "});
                }
                return acc;
            }
            , []);

        return merged;
    },

    event : {
        init : function (coremuObj){
            coremuObj.event.initSelect2(coremuObj);
        },

        initSelect2 : function (coremuObj){
            let optionSelect1 = {
                formatSelection: function (state) {
                    if (!state.id) { return state.text; }

                    var $state = jQuery(
                        /*
                                            '<div class="badgetag"><img width="25" height="25" alt="."  class="img-circle" src=" ' + baseUrl+state.element[0].attributes[1].value + '"/> ' + state.text + '</div>' +
                        */
                        '<div class="leaderboard__container" data-active="always">'+
                        '    <article class="leaderboard__profile">' +
                        '    <img src="'+ baseUrl+state.element[0].attributes[1].value +'" alt="." class="leaderboard__picture">' +
                        '    <span class="leaderboard__name">' + state.text + '</span>' +
                        '    <span class="leaderboard__value">' +
                        '        <span style="display: inline-block;">' +

                        '        </span>'+
                        '    </span>'+
                        '    </article>'+
                        '</div>'
                    );

                    return $state;
                },
                closeOnSelect: false,
                //width: '80%',
                placeholder: "aucune",
                containerCssClass : 'aapstatus-container',
                dropdownCssClass: "aapstatus-dropdown"
            };
            let $select1 = $("select.statustags1").select2(optionSelect1);
        },

        initWebSocket : function(coremuObj , coWsConfig , answerId){

            if(!notNull(wsCO) || !wsCO.hasListeners('refresh-coform-answer')) {
                var wsCO = null

                var socketConfigurationEnabled = coWsConfig.enable && coWsConfig.serverUrl && coWsConfig.pingRefreshCoformAnswerUrl;
                if (socketConfigurationEnabled) {
                    wsCO = io(coWsConfig.serverUrl, {
                        query: {
                            answerId: answerId
                        }
                    });

                    wsCO.on('refresh-coform-answer', function () {
                        coremuObj.event.update(coremuObj);
                    });
                }
            }

        },

        callbacks : {
            onUpdateValue : {
                check : function (path) {
                    return null;
                },
                totalestimateConfirmed : function (coremuObj, path , difference , newData ) {
                    coremuObj.updateNumber('.'+path.slice(0, -1).join('') , difference[path[0]][path[1]][path[2]][path[3]][path[4]].data , newData[path[0]][path[1]][path[2]][path[3]][path[4]]);
                },
                assignpricelist : function (coremuObj, path , difference , newData ) {
                    var apba = "";
                    if(!Array.isArray(newData.arrageDepense[path[1]].candidateEstimate[path[3]].assignpricelist)){
                        apba = JSON.stringify(Object.values(newData.arrageDepense[path[1]].candidateEstimate[path[3]].assignpricelist));
                    }else {
                        apba = JSON.stringify(newData.arrageDepense[path[1]].candidateEstimate[path[3]].assignpricelist);
                    }
                    $('[data-assignpricelist][data-pos="'+path[1]+'"][data-uid="'+path[3]+'"]').attr('data-assignpricelist' , apba);
                },
                totalestimate : function (coremuObj, path , difference , newData ) {
                    coremuObj.updateNumber('.'+path.slice(0, -1).join('') , difference[path[0]][path[1]][path[2]][path[3]][path[4]].data , newData[path[0]][path[1]][path[2]][path[3]][path[4]]);
                },
                totalValidee : function (coremuObj, path , difference , newData ) {
                    coremuObj.updateNumber('.'+path.slice(0, -1).join('') , difference[path[0]].data , newData[path[0]]);
                },
                total : function (coremuObj, path , difference , newData ) {
                    coremuObj.updateNumber('.'+path.slice(0, -1).join('') , difference[path[0]].data , newData[path[0]]);
                },
                totalFinancement : function (coremuObj, path , difference , newData ) {
                    coremuObj.updateNumber('.'+path.slice(0, -1).join('') , difference[path[0]].data , newData[path[0]]);
                },
                isactive : function (coremuObj, path , difference , newData ) {
                    coremuObj.updateColorClass('.'+path.slice(0, -1).join('') , newData[path[0]][path[1]][path[2]] , difference[path[0]][path[1]][path[2]].data );
                },
                totalNotif : function (coremuObj, path , difference , newData ) {
                    coremuObj.updateNumber('.'+path.slice(0, -1).join('') , difference[path[0]][path[1]][path[2]].data , newData[path[0]][path[1]][path[2]]);
                },
                needcandidat : function (coremuObj, path , difference , newData ) {
                    coremuObj.updateNumber('.'+path.slice(0, -1).join('') , difference[path[0]][path[1]][path[2]].data , newData[path[0]][path[1]][path[2]]);
                },
                pricetovalid : function (coremuObj, path , difference , newData ) {
                    coremuObj.updateNumber('.'+path.slice(0, -1).join('') , difference[path[0]][path[1]][path[2]].data , newData[path[0]][path[1]][path[2]]);
                },
                candidattovalid : function (coremuObj, path , difference , newData ) {
                    coremuObj.updateNumber('.'+path.slice(0, -1).join('') , difference[path[0]][path[1]][path[2]].data , newData[path[0]][path[1]][path[2]]);
                },
                valideRow : function (coremuObj, path , difference , newData ) {
                    var html = "";
                    var type = "hide";
                    if (newData[path[0]][path[1]][path[2]] == true){
                        type = "show";
                        html = coremuObj.block.valideRow(coremuObj, path[1]);
                    }
                    coremuObj.showHideHtml('.'+path.slice(0, -1).join('') , '.'+path.slice(0, -1).join('')+'Container',  html, type);
                },
                tocandidatebtn : function (coremuObj, path , difference , newData ) {
                    var html = "";
                    var type = "hide";
                    if (newData[path[0]][path[1]][path[2]] == true){
                        type = "show";
                        html = coremuObj.block.tocandidatebtn(coremuObj, path[1]);
                    }
                    coremuObj.showHideHtml('.'+path.slice(0, -1).join('') , '.'+path.slice(0, -1).join('')+'Container',  html, type);
                },
                totalansweraction : function (coremuObj, path , difference , newData ) {
                    coremuObj.updateNumber('.'+path.slice(0, -1).join('') , difference[path[0]].data , newData[path[0]]);
                },
                totalneedcandidat : function (coremuObj, path , difference , newData ) {
                    coremuObj.updateNumber('.'+path.slice(0, -1).join('') , difference[path[0]].data , newData[path[0]]);
                },
                totalpricetovalid : function (coremuObj, path , difference , newData ) {
                    coremuObj.updateNumber('.'+path.slice(0, -1).join('') , difference[path[0]].data , newData[path[0]]);
                },
                totalcandidattovalid : function (coremuObj, path , difference , newData ) {
                    coremuObj.updateNumber('.'+path.slice(0, -1).join('') , difference[path[0]].data , newData[path[0]]);
                },
                totalEnveloppe : function (coremuObj, path , difference , newData ) {
                    if(notNull(newData.arrageDepense[path[1]].candidateEstimate)) {
                        $.each(Object.keys(newData.arrageDepense[path[1]].candidateEstimate), function (index, value) {
                            var html = "";
                            html = coremuObj.block.candidatecardbtn(coremuObj, path[1], value);

                            coremuObj.reloadHtml(null , '.candidatecardbtn'+path[1]+value+'Container' , html);

                        });
                    }
                    coremuObj.event.afterRender.candidatecard();
                    coremuObj.event.afterRender.card();
                },
                candidateNumber : function (coremuObj , path , difference , newData) {
                    var html2 = "";
                    html2 = coremuObj.block.candidatbtn(coremuObj, path[1]);

                    coremuObj.reloadHtml(null , '.candidatheader'+path[1] , html2);

                    coremuObj.event.afterRender.card();

                    coremuObj.event.afterRender.trLine();
                },
                pricecomment : function (coremuObj , path , difference , newData) {
                    $('.pricecomment'+path[1]+path[3]).html(newData[path[0]][path[1]][path[2]][path[3]][path[4]]);
                },
                price : function (coremuObj , path , difference , newData) {
                    if(notNull(newData[path[0]][path[1]][path[2]]) && $.isNumeric(newData[path[0]][path[1]][path[2]])) {
                        $('.price' + answerId + path[1]).html(newData[path[0]][path[1]][path[2]]);
                    }

                    var html2 = "";
                    html2 = coremuObj.block.progressbarfin(coremuObj, path[1]);

                    coremuObj.reloadHtml(null , '.progressbarfin'+path[1] , html2);
                },
                asssignationType : function (coremuObj , path , difference , newData) {

                    $.each(Object.keys(newData.arrageDepense[path[1]].candidateEstimate) , function(i, candidate){
                        var html = coremuObj.block.candidatetablecomment(coremuObj, path[1] , candidate);
                        coremuObj.reloadHtml(null , '.candidatetablecomment'+path[1]+candidate+'Container' , html);
                    });

                },
                badge : function(coremuObj , path , difference , newData){
                    $('[data-id="badge'+answerId+path[1]+'"]').val(coremuObj.cache.arrageDepense[path[1]].badge).trigger('change');
                },
                lock : function(coremuObj , path , difference , newData){
                    var html1 = "";
                    html1 = coremuObj.block.lockeditsuprbtn(coremuObj, path[1]);

                    coremuObj.reloadHtml(null , '.lockeditsuprbtn'+path[1] , html1);

                    var html2 = "";
                    html2 = coremuObj.block.editbadge(coremuObj, path[1]);

                    coremuObj.reloadHtml(null , '.editbadge'+path[1] , html2);

                    var html3 = "";
                    html3 = coremuObj.block.montantgroupbtn(coremuObj, path[1]);

                    coremuObj.reloadHtml(null , '.montantheader'+path[1] , html3);

                    var html4 = "";
                    html4 = coremuObj.block.candidatbtn(coremuObj, path[1]);

                    coremuObj.reloadHtml(null , '.candidatheader'+path[1] , html4);

                    $.each(Object.keys(newData.arrageDepense[path[1]].candidateEstimate) , function(i, candidate){
                        var html = coremuObj.block.candidatetablecomment(coremuObj, path[1] , candidate);
                        coremuObj.reloadHtml(null , '.candidatetablecomment'+path[1]+candidate+'Container' , html);
                    });

                    $.each(Object.keys(newData.arrageDepense[path[1]].candidateEstimate) , function(i, candidate){
                        var html = coremuObj.block.candidatecardbtn(coremuObj, path[1] , candidate);
                        coremuObj.reloadHtml(null , '.candidatecardbtn'+path[1]+candidate+'Container' , html);
                    });

                    $.each(Object.keys(newData.arrageDepense[path[1]].deniedCandidate) , function(i, candidate){
                        var html = coremuObj.block.deniedcandidatecardbtn(coremuObj, path[1] , candidate);
                        coremuObj.reloadHtml(null , '.deniedcandidatecardbtn'+path[1]+candidate , html);
                    });

                    coremuObj.event.afterRender.candidatecard();
                    coremuObj.event.afterRender.card();
                    coremuObj.event.afterRender.denycard();
                    coremuObj.event.afterRender.pricecard();
                    coremuObj.event.afterRender.trLine();
                    coremuObj.event.afterRender.addcandidate();
                },
                totalConfirmed : function(coremuObj , path , difference , newData){
                    coremuObj.updateNumber('.totalConfirmed'+path[1] , difference[path[0]][path[1]][path[2]].data , newData[path[0]][path[1]][path[2]]);
                    coremuObj.updateNumber('.totalConfirmed'+answerId+path[1] , difference[path[0]][path[1]][path[2]].data , newData[path[0]][path[1]][path[2]]);
                    var html2 = "";
                    html2 = coremuObj.block.progressbarfin(coremuObj, path[1]);

                    coremuObj.reloadHtml(null , '.progressbarfin'+path[1] , html2);
                },
                totalRowFinanced : function(coremuObj , path , difference , newData) {
                    coremuObj.updateNumber('.totalRowFinanced' + path[1], difference[path[0]][path[1]][path[2]].data, newData[path[0]][path[1]][path[2]]);
                    var html2 = "";
                    html2 = coremuObj.block.progressbarfin(coremuObj, path[1]);

                    coremuObj.reloadHtml(null , '.progressbarfin'+path[1] , html2);
                },
                poste : function(coremuObj , path , difference , newData) {
                    coremuObj.reloadHtml(null , '.poste'+path[1] , coremuObj.cache.arrageDepense[path[1]].poste);
                },
                tags : function(coremuObj , path , difference , newData) {
                    coremuObj.reloadHtml(null , '.tags'+path[1] , coremuObj.block.tags(coremuObj , path[1]));
                },
                description : function(coremuObj , path , difference , newData) {
                    coremuObj.reloadHtml(null , '.description'+path[1] , coremuObj.cache.arrageDepense[path[1]].description);
                },
                financement : function(coremuObj , path , difference , newData){
                    var html = coremuObj.block.financerTable(coremuObj, path[1]);
                    $('.financementtable'+path[1]).html(html);
                    coremuObj.event.afterRender.financementLine();
                },
                AssignBudgetArray : function (coremuObj, path , difference , newData ) {
                    var html = "";
                    html = coremuObj.block.candidatetablecomment(coremuObj, path[1] , path[3]);
                    coremuObj.reloadHtml(null , '.candidatetablecomment'+path[1]+path[3]+'Container' , html);
                    coremuObj.event.afterRender.candidatecard();
                    coremuObj.event.afterRender.card();
                    var apba = "";
                    if(!Array.isArray(newData.arrageDepense[path[1]].candidateEstimate[path[3]].assignpricelist)){
                        apba = JSON.stringify(Object.values(newData.arrageDepense[path[1]].candidateEstimate[path[3]].assignpricelist));
                    }else {
                        apba = JSON.stringify(newData.arrageDepense[path[1]].candidateEstimate[path[3]].assignpricelist);
                    }
                    $('[data-assignpricelist][data-pos="'+path[1]+'"][data-uid="'+path[3]+'"]').attr('data-assignpricelist' , apba );
                },
            },
            onCreateValue : {
                candidateEstimate : function (coremuObj, path , difference , newData ) {
                    var html = "";
                    html = coremuObj.block.candidatecard(coremuObj, path[1] , path[3]);
                    coremuObj.showAppend('.'+path.slice(0, -2).join('')+path[3] , '.'+path.slice(0, -2).join('')+'Container',  html);
                    coremuObj.event.afterRender.candidatecard();
                    coremuObj.event.afterRender.card();
                     if ( $('.smallspan[data-type="list"][data-active="true"][data-pos="'+path[1]+'"]').length == 0 ) {
                         $('.leaderboard__container[data-type="list"]').attr("data-active", false);
                     }
                },
                assignpricelist : function (coremuObj, path , difference , newData ) {
                    var apba = "";
                    if(!Array.isArray(newData.arrageDepense[path[1]].candidateEstimate[path[3]].assignpricelist)){
                        apba = JSON.stringify(Object.values(newData.arrageDepense[path[1]].candidateEstimate[path[3]].assignpricelist));
                    }else {
                        apba = JSON.stringify(newData.arrageDepense[path[1]].candidateEstimate[path[3]].assignpricelist);
                    }
                    $('[data-assignpricelist][data-pos="'+path[1]+'"][data-uid="'+path[3]+'"]').attr('data-assignpricelist' , apba );
                },
                AssignBudgetArray : function (coremuObj, path , difference , newData ) {
                    var html = "";
                    html = coremuObj.block.candidatetablecomment(coremuObj, path[1] , path[3]);
                    coremuObj.reloadHtml(null , '.candidatetablecomment'+path[1]+path[3]+'Container' , html);
                    coremuObj.event.afterRender.candidatecard();
                    coremuObj.event.afterRender.card();
                    var apba = "";
                    if(!Array.isArray(newData.arrageDepense[path[1]].candidateEstimate[path[3]].assignpricelist)){
                        apba = JSON.stringify(Object.values(newData.arrageDepense[path[1]].candidateEstimate[path[3]].assignpricelist));
                    }else {
                        apba = JSON.stringify(newData.arrageDepense[path[1]].candidateEstimate[path[3]].assignpricelist);
                    }
                    $('[data-assignpricelist][data-pos="'+path[1]+'"][data-uid="'+path[3]+'"]').attr('data-assignpricelist' , apba );
                },
                validate : function (coremuObj, path , difference , newData ) {
                    var initial = "" ;
                    var final = "" ;
                    if (notNull(newData[path[0]][path[1]][path[2]][path[3]][path[4]]) && newData[path[0]][path[1]][path[2]][path[3]][path[4]] == "validated" ){
                        initial = "";
                        final = "check-estimates";
                    }else {
                        initial = "check-estimates";
                        final = "";
                    }
                    var html1 = "";
                    var html2 = "";
                    html1 = coremuObj.block.candidatetablecomment(coremuObj, path[1] , path[3]);
                    html2 = coremuObj.block.candidatbtn(coremuObj, path[1]);
                    html3 = coremuObj.block.candidatecardbtn(coremuObj, path[1], path[3]);

                    coremuObj.updateColorClass('.'+path.slice(0, -1).join('') , initial, final);
                    coremuObj.reloadHtml(null , '.candidatetablecomment'+path[1]+path[3]+'Container' , html1);
                    coremuObj.reloadHtml(null , '.candidatheader'+path[1] , html2);
                    coremuObj.reloadHtml(null , '.candidatecardbtn'+path[1]+path[3]+'Container' , html3);
                    coremuObj.event.afterRender.candidatecard();
                    coremuObj.event.afterRender.card();
                    coremuObj.event.afterRender.trLine();
                },
                deniedCandidate : function (coremuObj, path , difference , newData ){
                    var html = "";
                    html = coremuObj.block.deniedcandidatecard(coremuObj, path[1] , path[3]);
                    coremuObj.showAppend('.'+path.slice(0, -2).join('')+path[3] , '.'+path.slice(0, -2).join('')+'Container',  html);
                    html2 = coremuObj.block.candidatbtn(coremuObj, path[1]);
                    coremuObj.reloadHtml(null , '.candidatheader'+path[1] , html2);
                    coremuObj.event.afterRender.denycard();
                    coremuObj.event.afterRender.card();
                    coremuObj.event.afterRender.toogleList();
                    coremuObj.event.afterRender.trLine();
                },
                priceEstimate : function (coremuObj, path , difference , newData ){
                    var html = "";
                    html = coremuObj.block.estimateprice(coremuObj, path[1] , path[3]);
                    coremuObj.showAppend('.'+path.slice(0, -2).join('')+path[3] , '.'+path.slice(0, -2).join('')+'Container',  html);
                    var html2 = coremuObj.block.montantgroupbtn(coremuObj, path[1]);
                    coremuObj.reloadHtml(null , '.montantheader'+path[1] , html2);
                    coremuObj.event.afterRender.trLine();
                    coremuObj.event.afterRender.pricecard();
                    coremuObj.event.afterRender.card();
                },
                createEntiereline : function (coremuObj , pos){

                    let answerDataDepense = Object.keys(coremuObj.answerObjdepense.depense);

                    var $card = $(coremuObj.createTrLineBudget(coremuObj, pos));
                    if( !$(coremuObj.budgetContainer).has( '.entiereline'+pos ).length ) {
                        $($card).insertBefore(".totaltrline");
                    }

                    var $card2 = $(coremuObj.createTrLineFinancement(coremuObj, pos));
                    if( !$(coremuObj.financementContainer).has( 'entierelinefin'+pos ).length ) {
                        $($card2).insertBefore(".totaltrfinline");
                    }
                    coremuObj.event.afterRender.trLine();
                    coremuObj.event.afterRender.card();
                    coremuObj.event.initSelect2(coremuObj);
                },
                pricecomment : function (coremuObj , path , difference , newData) {
                    $('.pricecomment'+path[1]+path[3]).html(newData[path[0]][path[1]][path[2]][path[3]][path[4]]);
                },
                ispriceselected : function (coremuObj , path , difference , newData){
                    if(notNull(newData[path[0]][path[1]][path[2]][path[3]][path[4]]) && newData[path[0]][path[1]][path[2]][path[3]][path[4]] == true) {
                        $('.candidateprice'+path[1]+path[3]).addClass('check-estimates');
                    } else {
                        $('.candidateprice'+path[1]+path[3]).removeClass('check-estimates');
                    }
                    var html2 = coremuObj.block.estimatepricecardbtn(coremuObj, path[1], path[3]);
                    $('.estimatepricecardbtn'+path[1]+path[3]).html(html2);
                    coremuObj.event.afterRender.pricecard();
                    coremuObj.event.afterRender.card();

                },
                badge : function(coremuObj , path , difference , newData){
                    $('[data-id="badge'+answerId+path[1]+'"]').val(coremuObj.cache.arrageDepense[path[1]].badge).trigger('change');
                },
                financement : function(coremuObj , path , difference , newData){
                    var html = coremuObj.block.financerTable(coremuObj, path[1]);
                    $('.financementtable'+path[1]).html(html);
                    coremuObj.event.afterRender.financementLine();
                }
            },
            onDeleteValue : {
                financement : function(coremuObj , path , difference , newData){
                    var html = coremuObj.block.financerTable(coremuObj, path[1]);
                    $('.financementtable'+path[1]).html(html);
                    coremuObj.event.afterRender.financementLine();
                },
                candidateEstimate : function (coremuObj, path ) {
                    coremuObj.hideRemove('.'+path.slice(0, -2).join('')+path[3] );
                    coremuObj.event.afterRender.trLine();
                    coremuObj.event.afterRender.card();
                },
                assignpricelist : function (coremuObj, path , difference , newData ) {
                    var apba = "";
                    if(!Array.isArray(newData.arrageDepense[path[1]].candidateEstimate[path[3]].assignpricelist)){
                        apba = JSON.stringify(Object.values(newData.arrageDepense[path[1]].candidateEstimate[path[3]].assignpricelist));
                    }else {
                        apba = JSON.stringify(newData.arrageDepense[path[1]].candidateEstimate[path[3]].assignpricelist);
                    }
                    $('[data-assignpricelist][data-pos="'+path[1]+'"][data-uid="'+path[3]+'"]').attr('data-assignpricelist' , apba );
                },
                priceEstimate : function (coremuObj, path ) {
                    coremuObj.hideRemove('.'+path.slice(0, -2).join('')+path[3] );
                    var html2 = coremuObj.block.montantgroupbtn(coremuObj, path[1]);
                    coremuObj.reloadHtml(null , '.montantheader'+path[1] , html2);
                    coremuObj.event.afterRender.card();
                },
                deniedCandidate : function (coremuObj, path ) {
                    coremuObj.hideRemove('.'+path.slice(0, -1).join('') );
                    var html2 = coremuObj.block.candidatbtn(coremuObj, path[1]);
                    coremuObj.reloadHtml(null , '.candidatheader'+path[1] , html2);
                    coremuObj.event.afterRender.toogleList();
                    coremuObj.event.afterRender.trLine();
                },
                AssignBudgetArray : function (coremuObj, path , difference , newData ) {
                    var html = "";
                    html = coremuObj.block.candidatetablecomment(coremuObj, path[1] , path[3]);
                    coremuObj.reloadHtml(null , '.candidatetablecomment'+path[1]+path[3]+'Container' , html);
                    coremuObj.event.afterRender.candidatecard();
                    coremuObj.event.afterRender.card();
                    var apba = "";
                    if(!Array.isArray(newData.arrageDepense[path[1]].candidateEstimate[path[3]].assignpricelist)){
                        apba = JSON.stringify(Object.values(newData.arrageDepense[path[1]].candidateEstimate[path[3]].assignpricelist));
                    }else {
                        apba = JSON.stringify(newData.arrageDepense[path[1]].candidateEstimate[path[3]].assignpricelist);
                    }
                    $('[data-assignpricelist][data-pos="'+path[1]+'"][data-uid="'+path[3]+'"]').attr('data-assignpricelist' , apba );
                },
                validate : function (coremuObj, path , difference , newData ) {
                    var initial = "" ;
                    var final = "" ;
                    if (notNull(newData[path[0]][path[1]][path[2]][path[3]][path[4]]) && newData[path[0]][path[1]][path[2]][path[3]][path[4]] == "validated" ){
                        initial = "";
                        final = "check-estimates";
                    }else{
                        initial = "check-estimates";
                        final = "";
                    }
                    var html1 = "";
                    var html2 = "";
                    html1 = coremuObj.block.candidatetablecomment(coremuObj, path[1] , path[3]);
                    html2 = coremuObj.block.candidatbtn(coremuObj, path[1]);
                    html3 = coremuObj.block.candidatecardbtn(coremuObj, path[1], path[3]);

                    coremuObj.updateColorClass('.'+path.slice(0, -1).join('') , initial, final);
                    coremuObj.reloadHtml(null , '.candidatetablecomment'+path[1]+path[3]+'Container' , html1);
                    coremuObj.reloadHtml(null , '.candidatheader'+path[1] , html2);
                    coremuObj.reloadHtml(null , '.candidatecardbtn'+path[1]+path[3]+'Container' , html3);
                    coremuObj.event.afterRender.candidatecard();
                    coremuObj.event.afterRender.card();
                    coremuObj.event.afterRender.trLine();
                },
                deleteEntiereline : function (pos){
                    coremuObj.hideRemove('tr.entiereline[data-id="'+pos+'"]');
                    coremuObj.hideRemove('div.entierelinefin[data-id="'+pos+'"]');
                },
                ispriceselected : function (coremuObj , path , difference , newData){
                    if(notNull(newData[path[0]][path[1]][path[2]][path[3]][path[4]]) && newData[path[0]][path[1]][path[2]][path[3]][path[4]] == true) {
                        $('.candidateprice'+path[1]+path[3]).addClass('check-estimates');
                    } else {
                        $('.candidateprice'+path[1]+path[3]).removeClass('check-estimates');
                    }
                    var html = coremuObj.block.estimatepricecardbtn(coremuObj, path[1], path[3]);
                    $('.estimatepricecardbtn'+path[1]+path[3]).html(html);
                    coremuObj.event.afterRender.pricecard();
                    coremuObj.event.afterRender.card();

                },
                badge : function(coremuObj , path , difference , newData){
                    $('[data-id="badge'+answerId+path[1]+'"]').val(coremuObj.cache.arrageDepense[path[1]].badge).trigger('change');
                }
            }
        },

        afterRender : {
            candidatecard : function (){
                $('.btnAssignBudgetArray').off().click(function() {
                    var apl = $(this).data("assignpricelist");
                    tplCtx.pos = $(this).data("pos");
                    tplCtx.uid = $(this).data("uid");
                    tplCtx.collection = "answers";
                    tplCtx.id = $(this).data("id");
                    tplCtx.key = $(this).data("key");
                    tplCtx.form = $(this).data("form");
                    tplCtx.assignpricelist = apl;
                    tplCtx.max = $(this).data("max");
                    tplCtx.path = "answers."+tplCtx.form+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+tplCtx.uid+".";

                    var tplCtx2 = {};
                    tplCtx2.pos = $(this).data("pos");
                    tplCtx2.uid = $(this).data("uid");
                    tplCtx2.collection = "answers";
                    tplCtx2.id = $(this).data("id");
                    tplCtx2.key = $(this).data("key");
                    tplCtx2.form = $(this).data("form");
                    tplCtx2.assignpricelist = apl;
                    tplCtx2.path = "answers."+tplCtx.form+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+tplCtx.uid+".";

                    dyFObj.openForm( paramsAssignBudgetArray ,null, { "AssignBudgetArray" : apl},  null , null, {
                        type : "bootbox",
                        notCloseOpenModal : true,
                    });
                });

                $('.btncheckestimate').off().click(function() {
                    tplCtx.pos = $(this).data("pos");
                    tplCtx.uid = $(this).data("uid");
                    tplCtx.collection = "answers";
                    tplCtx.id = $(this).data("id");
                    tplCtx.key = $(this).data("key");
                    tplCtx.key = $(this).data("key");
                    tplCtx.form = $(this).data("form");
                    tplCtx.percentage = $(this).data("percentage");
                    tplCtx.path = "answers."+tplCtx.form+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+tplCtx.uid+".validate";
                    tplCtx.value = $(this).data("val");

                    if (typeof tplCtx.setType != "undefined"){
                        delete tplCtx.setType;
                    }

                    dataHelper.path2Value( tplCtx, function(){
                        coremuObj.event.update(coremuObj);
                        ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});
                    });
                });

                $('.btnaddpercent').off().click(function() {
                    tplCtx.pos = $(this).data("pos");
                    tplCtx.uid = $(this).data("uid");
                    tplCtx.collection = "answers";
                    tplCtx.id = $(this).data("id");
                    tplCtx.key = $(this).data("key");
                    tplCtx.form = $(this).data("form");
                    tplCtx.percentage = $(this).data("percentage");
                    tplCtx.path = "answers."+tplCtx.form+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+tplCtx.uid+".percentage";

                    dyFObj.openForm( paramsPercentage ,null, { "percentage" : parseInt(tplCtx.percentage)}, null , null , {
                        type : "bootbox",
                        notCloseOpenModal : true,
                    });
                });

                $('.checkAssignBudgetArray').off().change(function() {
                    var $this = $(this);
                    tplCtx.pos = $(this).data("pos");
                    tplCtx.uid = $(this).data("uid");
                    tplCtx.collection = "answers";
                    tplCtx.id = $(this).data("id");
                    tplCtx.key = $(this).data("key");
                    tplCtx.form = $(this).data("form");
                    tplCtx.assignBudget = $(this).data("assignpricelist");
                    tplCtx.unumber = $(this).data("unumber");
                    tplCtx.action = $(this).data("action");
                    if($this.is(':checked')) {
                        tplCtx.value = "true";
                    }else{
                        tplCtx.value = "false";
                    }
                    tplCtx.path = "answers."+tplCtx.form+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+tplCtx.uid+".AssignBudgetArray."+tplCtx.unumber+".check";

                    dataHelper.path2Value( tplCtx, function() {
                        coremuObj.event.update(coremuObj);
                        ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});
                    });

                });

                $('.checkpercentageState').off().change(function() {
                    var $this = $(this);
                    tplCtx.pos = $(this).data("pos");
                    tplCtx.uid = $(this).data("uid");
                    tplCtx.collection = "answers";
                    tplCtx.id = $(this).data("id");
                    tplCtx.key = $(this).data("key");
                    tplCtx.form = $(this).data("form");
                    tplCtx.unumber = $(this).data("unumber");
                    if($this.is(':checked')) {
                        tplCtx.value = "validated";
                    }else{
                        tplCtx.value = null;
                    }
                    tplCtx.path = "answers."+tplCtx.form+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+tplCtx.uid+".percentageState";

                    dataHelper.path2Value( tplCtx, function(){
                        coremuObj.event.update(coremuObj);
                        ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});
                    });

                });

                $('.btncheckdeny').off().click(function() {
                    var thisbtn = $(this);
                    tplCtx = {};
                    tplCtx.pos = thisbtn.data("pos");
                    tplCtx.uid = thisbtn.data("uid");
                    tplCtx.collection = "answers";
                    tplCtx.id = thisbtn.data("id");
                    tplCtx.key = thisbtn.data("key");
                    tplCtx.form = thisbtn.data("form");
                    tplCtx.path = "answers."+tplCtx.form+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+tplCtx.uid+".deny";

                    tplCtx.value = thisbtn.data("val");

                    if (typeof tplCtx.setType != "undefined"){
                        delete tplCtx.setType;
                    }

                    dataHelper.path2Value( tplCtx, function(){

                        tplCtx = {};
                        tplCtx.pos = thisbtn.data("pos");
                        tplCtx.uid = thisbtn.data("uid");
                        tplCtx.collection = "answers";
                        tplCtx.id = thisbtn.data("id");
                        tplCtx.key = thisbtn.data("key");
                        tplCtx.form = thisbtn.data("form");
                        tplCtx.path = "answers."+tplCtx.form+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+tplCtx.uid+".percentage";
                        tplCtx.value = null;
                        dataHelper.path2Value( tplCtx, function(){});

                        tplCtx = {};
                        tplCtx.pos = thisbtn.data("pos");
                        tplCtx.uid = thisbtn.data("uid");
                        tplCtx.collection = "answers";
                        tplCtx.id = thisbtn.data("id");
                        tplCtx.key = thisbtn.data("key");
                        tplCtx.form = thisbtn.data("form");
                        tplCtx.path = "answers."+tplCtx.form+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+tplCtx.uid+".AssignBudgetArray";
                        tplCtx.value = null;
                        dataHelper.path2Value( tplCtx, function(){});

                        tplCtx = {};
                        tplCtx.pos = thisbtn.data("pos");
                        tplCtx.uid = thisbtn.data("uid");
                        tplCtx.collection = "answers";
                        tplCtx.id = thisbtn.data("id");
                        tplCtx.key = thisbtn.data("key");
                        tplCtx.form = thisbtn.data("form");
                        tplCtx.path = "answers."+tplCtx.form+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+tplCtx.uid+".denycomment";

                        dyFObj.openForm( paramsDenycomment , null, null , null, null, {
                            type : "bootbox",
                            notCloseOpenModal : true,
                        });

                        coremuObj.event.update(coremuObj);
                        ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});


                    });
                });
            } ,

            denycard: function(){
                $('.btncanceldeny').off().click(function() {
                    tplCtx.pos = $(this).data("pos");
                    tplCtx.uid = $(this).data("uid");
                    tplCtx.collection = "answers";
                    tplCtx.id = $(this).data("id");
                    tplCtx.key = $(this).data("key");
                    tplCtx.form = $(this).data("form");
                    tplCtx.path = "answers."+tplCtx.form+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+tplCtx.uid+".deny";
                    tplCtx.value = $(this).data("val");

                    if (typeof tplCtx.setType != "undefined"){
                        delete tplCtx.setType;
                    }

                    dataHelper.path2Value( tplCtx, function(){
                        coremuObj.event.update(coremuObj);
                        ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});
                    });
                });

                $('.btnmodifydenycomment').off().click(function() {
                    let thisbtn = $(this);
                    tplCtx = {};
                    tplCtx.pos = thisbtn.data("pos");
                    tplCtx.uid = thisbtn.data("uid");
                    tplCtx.collection = "answers";
                    tplCtx.id = thisbtn.data("id");
                    tplCtx.key = thisbtn.data("key");
                    tplCtx.form = thisbtn.data("form");
                    dyFObj.openForm(paramsDenycomment, null, { "denycomment" : thisbtn.data("denycomment")} , null , null , {
                        type : "bootbox",
                        notCloseOpenModal : true,
                    } );
                });
            },

            pricecard : function (){
                $('.btnEstimateSelected').off().click(function() {

                    var thisbtn = $(this);
                    var btnpos = thisbtn.data("pos");
                    tplCtx.form = thisbtn.data("form");
                    tplCtx.pathBase = "answers."+tplCtx.form;
                    tplCtx.pos = thisbtn.data("pos");
                    tplCtx.uid = thisbtn.data("uid");
                    tplCtx.collection = "answers";
                    tplCtx.id = thisbtn.data("id");
                    tplCtx.key = thisbtn.data("key");

                    tplCtx.price = thisbtn.data("price");

                    if (typeof tplCtx.setType != "undefined"){
                        delete tplCtx.setType;
                    }

                    tplCtx.path = "answers.aapStep1.depense."+tplCtx.pos+".price";
                    tplCtx.value = tplCtx.price;

                    dataHelper.path2Value( tplCtx, function(){
                        coremuObj.event.update(coremuObj);
   ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});

                        $.each(Object.keys(coremuObj.cache.arrageDepense[btnpos].candidateEstimate) , function(index, value){
                            tplCtx = {};
                            tplCtx.pos = thisbtn.data("pos");
                            tplCtx.uid = thisbtn.data("uid");
                            tplCtx.collection = "answers";
                            tplCtx.id = thisbtn.data("id");
                            tplCtx.key = thisbtn.data("key");
                            tplCtx.form = thisbtn.data("form");
                            tplCtx.path = "answers.aapStep1.depense."+tplCtx.pos+".estimates."+value+".AssignBudgetArray";
                            tplCtx.value = null;
                            dataHelper.path2Value( tplCtx, function(){});
                        });

                        $.each(Object.keys(coremuObj.cache.arrageDepense[btnpos].priceEstimate) , function(index, value){
                            tplCtx = {};
                            tplCtx.pos = thisbtn.data("pos");
                            tplCtx.uid = thisbtn.data("uid");
                            tplCtx.collection = "answers";
                            tplCtx.id = thisbtn.data("id");
                            tplCtx.key = thisbtn.data("key");
                            tplCtx.form = thisbtn.data("form");
                            tplCtx.path = "answers.aapStep1.depense."+tplCtx.pos+".estimates."+value+".ispriceselected";
                            tplCtx.value = false;
                            tplCtx.setType = "boolean";
                            dataHelper.path2Value( tplCtx, function(){});
                        });

                        tplCtx = {};

                        tplCtx.form = thisbtn.data("form");
                        tplCtx.pathBase = "answers."+tplCtx.form;
                        tplCtx.pos = thisbtn.data("pos");
                        tplCtx.uid = thisbtn.data("uid");
                        tplCtx.collection = "answers";
                        tplCtx.id = thisbtn.data("id");
                        tplCtx.key = thisbtn.data("key");

                        tplCtx.path = "answers.aapStep1.depense."+tplCtx.pos+".estimates."+tplCtx.uid+".ispriceselected";
                        tplCtx.value = true;

                        tplCtx.setType = "boolean";

                        dataHelper.path2Value( tplCtx, function(){});
                        coremuObj.event.update(coremuObj);
   ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});

                    } );
                });

                $('.btnmodifypricecomment').off().click(function() {
                    let thisbtn = $(this);
                    tplCtx = {};
                    tplCtx.pos = thisbtn.data("pos");
                    tplCtx.uid = thisbtn.data("uid");
                    tplCtx.collection = "answers";
                    tplCtx.id = thisbtn.data("id");
                    tplCtx.key = thisbtn.data("key");
                    tplCtx.form = thisbtn.data("form");
                    dyFObj.openForm(paramsPricecomment, null, { "pricecomment" : thisbtn.data("pricecomment")} , null , null , {
                        type : "bootbox",
                        notCloseOpenModal : true,
                    } );
                });
            },

            card : function (){
                $('.estibtndelete').off().click(function() {
                    tplCtx.pos = $(this).data("pos");
                    tplCtx.collection = "answers";
                    tplCtx.id = $(this).data("id");
                    tplCtx.key = $(this).data("key");
                    tplCtx.form = $(this).data("form");
                    tplCtx.uid = $(this).data("uid");

                    prioModal = bootbox.dialog({
                        title: trad.confirmdelete,
                        show: false,
                        message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                        buttons: [
                            {
                                label: "Ok",
                                className: "btn btn-primary pull-left",
                                callback: function() {
                                    /*var esti_pos = FiData[tplCtx.pos]["estimates"];
                                    delete esti_pos[tplCtx.uid];*/

                                    tplCtx.path = "answers";
                                    // if( notNull(formInputs [tplCtx.form]) )
                                    tplCtx.path = "answers."+tplCtx.form;

                                    tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+tplCtx.uid;

                                    tplCtx.value = null;

                                    mylog.log("btnEstimate save",tplCtx);

                                    if (typeof tplCtx.setType != "undefined"){
                                        delete tplCtx.setType;
                                    }

                                    dataHelper.path2Value( tplCtx, function(){
                                        coremuObj.event.update(coremuObj);
   ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});

                                    } );
                                }
                            },
                            {
                                label: "Annuler",
                                className: "btn btn-default pull-left",
                                callback: function() {}
                            }
                        ]
                    });

                    prioModal.modal("show");
                });

                $('.btn-deny-toogle-comment').off().click(function() {

                    let thisbtn = $(this);
                    if (thisbtn.attr("data-active") == "false") {
                        thisbtn.attr("data-active" , "true");

                        $('.arrow-down-comment[data-pos="'+thisbtn.attr("data-pos")+'"][data-uid="'+thisbtn.attr("data-uid")+'"] , .denycomment[data-pos="'+thisbtn.attr("data-pos")+'"][data-uid="'+thisbtn.attr("data-uid")+'"]').each(function (index) {
                            $(this).attr("data-active", "true");
                        });
                    } else {

                        thisbtn.attr("data-active" , "false");

                        $('.arrow-down-comment[data-pos="'+thisbtn.attr("data-pos")+'"][data-uid="'+thisbtn.attr("data-uid")+'"] , .denycomment[data-pos="'+thisbtn.attr("data-pos")+'"][data-uid="'+thisbtn.attr("data-uid")+'"]').each(function (index) {
                            $(this).attr("data-active", "false");
                        });
                    }
                });
            },

            trLine : function(){
                coremuObj.event.afterRender.toogleList();

                $('.newbtncandidate[data-kunik="budgetdepense"]').off().click(function() {
                    tplCtx.pos = $(this).data("pos");
                    tplCtx.collection = "answers";
                    tplCtx.id = $(this).data("id");
                    tplCtx.key = $(this).data("key");
                    tplCtx.form = $(this).data("form");
                    var currentValue = [];
                    tplCtx.value = {
                        "name" : typeof userConnected.name != "undefined" ? userConnected.name : userId,
                        "addedBy" : userId
                    };

                    tplCtx.path = "answers";
                    // if( notNull(formInputs [tplCtx.form]) )
                    tplCtx.path = "answers."+tplCtx.form;

                    //tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+currentUserId;
                    tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+userId;

                    tplCtx.setType = [
                        {
                            "path": "price",
                            "type": "int"
                        },
                        {
                            "path": "date",
                            "type": "isoDate"
                        }
                    ];

                    var today = new Date();
                    today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();

                    tplCtx.value.date = today;

                    dataHelper.path2Value( tplCtx, function(){
                        coremuObj.event.update(coremuObj);
   ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});

                        var params = {
                            parentId : answerId,
                            parentType : "answers",
                            listInvite : {
                                citoyens :{},
                                organizations:{}
                            }
                        }

                        params.listInvite.citoyens[userId] = typeof userConnected.name != "undefined" ? userConnected.name : userId;

                        ajaxPost("",baseUrl+'/'+moduleId+"/link/multiconnect",params,function(data){
                            ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/addperson/answerid/' + answerId,
                                {
                                    pers : params.listInvite.citoyens,
                                    url : window.location.href
                                },
                                function (data) {
                                    coremuObj.event.update(coremuObj);
   ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});

                                }, "html");
                        });

                    } );
                });

                $(".switchAmountAttr").off().change(function() {
                    tplCtx.id = $(this).data("id");
                    tplCtx.collection = $(this).data("collection");
                    tplCtx.path = $(this).data("path");
                    if($(this).is(':checked')) {
                        tplCtx.value = "assignBudget";
                    } else {
                        tplCtx.value = "percentage";
                    }

                    if (typeof tplCtx.setType != "undefined"){
                        delete tplCtx.setType;
                    }
                    dataHelper.path2Value( tplCtx , function(params) {
                        coremuObj.event.update(coremuObj);
   ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});

                    });
                });

                $(".bd-enable-btnbudgetdepense").off().click(function() {

                    var thisbtn = $(this);

                    $("select[data-id='" + thisbtn.data('id') + "']").prop("disabled", false);

                    thisbtn.attr("data-loc" , "save");

                    setTimeout(() => {
                        $(document).one("click",function(event) {
                            var container = $("tr[data-id='" + thisbtn.data('pos') + "'] .select2-container.select2-container-multi.statustags.statustags1.aapstatus-container");
                            if(typeof container != undefined) {
                                if (!container.is(event.target) && !container.has(event.target).length) {
                                    $("select[data-id='" + thisbtn.data('id') + "']").prop("disabled", true);
                                    var tplCtx = {};

                                    tplCtx.id = thisbtn.data('answerid');
                                    tplCtx.key = thisbtn.data('key');
                                    tplCtx.pos = thisbtn.data('pos');
                                    tplCtx.form = "aapStep1";

                                    tplCtx.collection = "answers";

                                    tplCtx.path = "answers";

                                    tplCtx.path = "answers." + tplCtx.form;

                                    tplCtx.path = tplCtx.path + "." + tplCtx.key + "." + tplCtx.pos + ".badge";

                                    tplCtx.value = $("select[data-id='" + thisbtn.data('id') + "'").val();

                                    dataHelper.path2Value(tplCtx, function (params) {
                                        coremuObj.event.update(coremuObj);
   ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});
                                        toastr.success('badge requis sauvegardé');
                                        thisbtn.attr("data-loc" , "edit");

                                    });
                                }
                            }
                        });
                    }, "1000")
                });

                $('.close-smallspan').off().click(function() {

                    let thisbtn = $(this);
                    $('.candidatebtn-div[data-pos="'+thisbtn.attr("data-pos")+'"]').show();
                    $('.btn-toogle-list').attr("data-active" , "false");
                    $('.leaderboard__container[data-pos="'+thisbtn.attr("data-pos")+'"]').each(function (index) {
                        $(this).attr("data-active" , false);
                    });
                    $('.leaderboard__container[data-pos="'+thisbtn.attr("data-pos")+'"][data-type="list"]').each(function (index) {
                        $(this).attr("data-active" , true);
                    });
                    $('.smallspan[data-pos="'+thisbtn.attr("data-pos")+'"]').each(function (index) {
                        if ($(this).attr("data-active") != "always") {
                            $(this).attr("data-active", false);
                        }
                    });
                    $('.smallspan[data-pos="'+thisbtn.attr("data-pos")+'"][data-type="list"').each(function (index) {
                        $(this).attr("data-active" , true);
                    });

                    if($('.toogle-maximum[data-pos="'+thisbtn.attr("data-pos")+'"]').hasClass("toogledeny")) {
                        $('.toogle-maximum[data-pos="'+thisbtn.attr("data-pos")+'"]').removeClass('toogledeny');
                        $('.leaderboard__profiles[data-pos="'+thisbtn.attr("data-pos")+'"]').removeClass('toogledeny');
                    }
                    if($('.toogle-maximum[data-pos="'+thisbtn.attr("data-pos")+'"]').hasClass("toogleprice")) {
                        $('.toogle-maximum[data-pos="'+thisbtn.attr("data-pos")+'"]').removeClass('toogleprice');
                        $('.leaderboard__profiles[data-pos="'+thisbtn.attr("data-pos")+'"]').removeClass('toogleprice');
                    }

                });

                $(".toogle-minimal").off().click(function(){
                    $(".toogle-maximum").toggle("slow");
                    setTimeout(() => {
                        if($(".toogle-maximum").is(":hidden")){
                            localStorage.setItem("coremuTableToogle", "hidden");
                        }else{
                            localStorage.setItem("coremuTableToogle", "show");
                        }
                    }, "2000");
                });

                $(".badgeparams").off().click(function() {
                    tplCtx.id = $(this).data("id");
                    tplCtx.collection = $(this).data("collection");
                    tplCtx.path = $(this).data("path");
                    //mylog.log(".editbudgetdepenseParams",tplCtx,sectionDyf.budgetdepenseParamsData);
                    dyFObj.openForm( sectionDyf.budgetdepensebadgeParams,null, sectionDyf.budgetdepenseParamsData , null , null , {
                        type : "bootbox",
                        notCloseOpenModal : true,
                    });
                });

                $('.openAnswersComment').off().click(function() {
                    setTimeout(
                        function() {
                            $('.footer-comments').on('DOMSubtreeModified', function () {

                            })
                        }
                        , 2000
                    );
                });

                $('.inputdeleteLine').off().click( function(){
                    formId = $(this).data("id");
                    key = $(this).data("key");
                    pathLine = $(this).data("path");
                    collection = $(this).data("collection");
                    if (typeof $(this).data("parentid") != "undefined") {
                        var parentfId = $(this).data("parentid");
                    }
                    bootbox.dialog({
                        title: trad.confirmdelete,
                        message: "<span class='text-red bold'><i class='fa fa-warning'></i> "+trad.actionirreversible+"</span>",
                        buttons: [
                            {
                                label: "Ok",
                                className: "btn btn-primary pull-left",
                                callback: function() {
                                    var formQ = {
                                        value:null,
                                        collection : collection,
                                        id : formId,
                                        path : pathLine
                                    };

                                    if (typeof parentfId != "undefined") {
                                        formQ["formParentId"] = parentfId;
                                    }

                                    if (typeof tplCtx.setType != "undefined"){
                                        delete tplCtx.setType;
                                    }

                                    dataHelper.path2Value( formQ , function(params) {
                                        coremuObj.event.update(coremuObj);
   ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});

                                    } );
                                }
                            },
                            {
                                label: "Annuler",
                                className: "btn btn-default pull-left",
                                callback: function() {}
                            }
                        ]
                    });
                });

                $('.newbtnestimate_price[data-kunik="budgetdepense"]').off().click(function() {
                    tplCtx.pos = $(this).data("pos");
                    tplCtx.collection = "answers";
                    tplCtx.id = $(this).data("id");
                    tplCtx.key = $(this).data("key");
                    tplCtx.form = $(this).data("form");

                    var msgform = '<div id="" class="newform-estimate-price"  data-formestimate="'+pos+'" >' +
                        '              <div class="form-group">' +
                        '                   <label for="e-price">Proposer un prix</label>' +
                        '                   <input required="required" type="number" style="width: 90%" class="form-control  e-price" id="e-price">' +
                        '              </div>' +
                        '              <div class="form-group">' +
                        '                   <label for="e-price">Commentaire</label>' +
                        '                   <input type="text" style="width: 90%" class="form-control  e-pricecomment" id="e-pricecomment">' +
                        '              </div>' +
                        '          </div>';

                    prioModal = bootbox.dialog({
                        message: msgform,
                        show: false,
                        size: "large",
                        className: 'estimatedialog',
                        buttons: {
                            success: {
                                label: trad.save,
                                className: "btn-primary",
                                callback: function () {
                                    if(typeof $('.bootbox .search-person[data-formestimate="'+ tplCtx.pos +'"]').val() != "undefined" && $('.bootbox .search-person[data-formestimate="'+ tplCtx.pos +'"]').val() != null && $('.bootbox .search-person[data-formestimate="'+ tplCtx.pos +'"]').val().length > 0) {
                                        $.each($('.bootbox .search-person[data-formestimate="'+ tplCtx.pos +'"]').val(), function (indexselect, elementselect){
                                            tplCtx.path = "answers";
                                            // if( notNull(formInputs [tplCtx.form]) )
                                            tplCtx.path = "answers."+tplCtx.form;

                                            tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+elementselect;

                                            tplCtx.setType = [
                                                {
                                                    "path": "price",
                                                    "type": "int"
                                                },
                                                {
                                                    "path": "date",
                                                    "type": "isoDate"
                                                }
                                            ];

                                            var today = new Date();
                                            today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();
                                            tplCtx.value = {
                                                price : $(".bootbox .e-price").val(),
                                                days : $(".bootbox .e-days").val(),
                                                name :  $('.bootbox option[value="'+$('.bootbox .search-person[data-formestimate="'+tplCtx.pos+'"]').val()[indexselect]+'"]').text(),
                                                date : today,
                                                addedBy : userId
                                            };

                                            mylog.log("btnEstimate save",tplCtx);
                                            if($(".bootbox .e-price").val() != "") {
                                                dataHelper.path2Value(tplCtx, function () {
                                                    coremuObj.event.update(coremuObj);
   ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});

                                                    prioModal.modal('hide');
                                                });
                                            }

                                        });
                                    }
                                    else {
                                        tplCtx.path = "answers";
                                        // if( notNull(formInputs [tplCtx.form]) )
                                        tplCtx.path = "answers."+tplCtx.form;

                                        tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+Math.floor(Math.random() * 100000);

                                        tplCtx.setType = [
                                            {
                                                "path": "price",
                                                "type": "int"
                                            },
                                            {
                                                "path": "date",
                                                "type": "isoDate"
                                            }
                                        ];

                                        var today = new Date();
                                        today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();
                                        tplCtx.value = {
                                            price : $(".bootbox .e-price").val(),
                                            days : $(".bootbox .e-days").val(),
                                            name :  " ",
                                            date : today,
                                            addedBy : userId
                                        };

                                        if($(".bootbox .e-pricecomment").val() != "undefined" && $(".bootbox .e-pricecomment").val() != "") {
                                            tplCtx.value.pricecomment = $(".bootbox .e-pricecomment").val();
                                        }

                                        mylog.log("btnEstimate save",tplCtx);

                                        if($(".bootbox .e-price").val() != "") {
                                            dataHelper.path2Value(tplCtx, function () {
                                                coremuObj.event.update(coremuObj);
   ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});

                                                prioModal.modal('hide');

                                            });
                                        }
                                    }
                                }
                            },
                            cancel: {
                                label: trad.cancel,
                                className: "btn-secondary",
                                callback: function(){
                                    prioModal.modal('hide');
                                }
                            }
                        },
                        onEscape: function(){
                            prioModal.modal('hide');
                        }
                    });

                    var selectedErow = tplCtx.pos;

                    prioModal.on('shown.bs.modal', function (e) {

                        $('.estimatedialog #add-button').data("pos", selectedErow );

                    });

                    prioModal.on('shown.bs.modal', function (e) {
                        var employerStateSelector = $(".bootbox select.search-person").select2("destroy");

                        employerStateSelector.select2({
                            formatSelection: function (state) {

                                if (!state.id) { return state.text; }
                                var $state = jQuery(
                                    '<div class="leaderboard__container">'+
                                    '    <span class="leaderboard__name">' + state.text + baseUrl+state.element[0].attributes[1].value +' </span>' +
                                    '</div>'
                                );

                                return $state;
                            },

                            closeOnSelect: false,
                            //width: '80%',
                            placeholder: "aucune",
                            containerCssClass : 'aapstatus-container',
                            dropdownCssClass: "aapstatus-dropdown"
                        });
                    })

                    prioModal.modal("show");
                });

                $('.newbtnestimate_candidate[data-kunik="budgetdepense"]').off().click(function() {
                    tplCtx.pos = $(this).data("pos");
                    tplCtx.collection = "answers";
                    tplCtx.id = $(this).data("id");
                    tplCtx.key = $(this).data("key");
                    tplCtx.form = $(this).data("form");

                    var exclus = $.merge( Object.keys(coremuObj.cache.arrageDepense[tplCtx.pos].candidateEstimate) , Object.keys(coremuObj.cache.arrageDepense[tplCtx.pos].deniedCandidate) ) ;

                    var msgform = '   <div id="" class="newform-estimate-candidate" data-formestimate="'+pos+'" >' +
                        '       <div class="form-group">' +
                        '           <label for="new-credit-value">Proposer des personnes</label>' +
                        '               <select id="new-credit-value" class="e-who search-person" data-formestimate="'+pos+'" multiple name="new-credit-value">';

                        if (notNull(coremuObj.community)){
                            $.each(coremuObj.community , function(index , value){
                                if(!exclus.includes(index)){
                                    msgform += '<option value="'+index+'" class="" > '+coremuObj.users[index].name+' </option>' ;
                                }
                            });
                        }

                    msgform +='         </select>' +
                        '       </div>' +
                        '  </div>';

                    prioModal = bootbox.dialog({
                        message: msgform,
                        show: false,
                        size: "large",
                        className: 'estimatedialog',
                        buttons: {
                            success: {
                                label: trad.save,
                                className: "btn-primary",
                                callback: function () {
                                    if(typeof $('.bootbox .search-person[data-formestimate="'+ tplCtx.pos +'"]').val() != "undefined" && $('.bootbox .search-person[data-formestimate="'+ tplCtx.pos +'"]').val() != null && $('.bootbox .search-person[data-formestimate="'+ tplCtx.pos +'"]').val().length > 0) {
                                        $.each($('.bootbox .search-person[data-formestimate="'+ tplCtx.pos +'"]').val(), function (indexselect, elementselect){
                                            tplCtx.path = "answers";
                                            // if( notNull(formInputs [tplCtx.form]) )
                                            tplCtx.path = "answers."+tplCtx.form;

                                            tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+elementselect;

                                            tplCtx.setType = [
                                                {
                                                    "path": "price",
                                                    "type": "int"
                                                },
                                                {
                                                    "path": "date",
                                                    "type": "isoDate"
                                                }
                                            ];

                                            var today = new Date();
                                            today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();
                                            tplCtx.value = {
                                                price : $(".bootbox .e-price").val(),
                                                days : $(".bootbox .e-days").val(),
                                                name :  $('.bootbox option[value="'+$('.bootbox .search-person[data-formestimate="'+tplCtx.pos+'"]').val()[indexselect]+'"]').text(),
                                                date : today,
                                                addedBy : userId
                                            };

                                            mylog.log("btnEstimate save",tplCtx);

                                            if($(".bootbox .e-price").val() != "") {
                                                dataHelper.path2Value(tplCtx, function () {
                                                    coremuObj.event.update(coremuObj);
   ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});

                                                    prioModal.modal('hide');

                                                });
                                            }

                                        });
                                    }
                                    else {
                                        tplCtx.path = "answers";
                                        // if( notNull(formInputs [tplCtx.form]) )
                                        tplCtx.path = "answers."+tplCtx.form;

                                        tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+userId+"es";

                                        tplCtx.setType = [
                                            {
                                                "path": "price",
                                                "type": "int"
                                            },
                                            {
                                                "path": "date",
                                                "type": "isoDate"
                                            }
                                        ];

                                        var today = new Date();
                                        today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();
                                        tplCtx.value = {
                                            price : $(".bootbox .e-price").val(),
                                            days : $(".bootbox .e-days").val(),
                                            name :  " ",
                                            date : today,
                                            addedBy : userId
                                        };

                                        mylog.log("btnEstimate save",tplCtx);

                                        if($(".bootbox .e-price").val() != "") {
                                            dataHelper.path2Value(tplCtx, function () {
                                                coremuObj.event.update(coremuObj);
   ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});

                                                prioModal.modal('hide');

                                            });
                                        }
                                    }
                                }
                            },
                            cancel: {
                                label: trad.cancel,
                                className: "btn-secondary",
                                callback: function(){
                                    prioModal.modal('hide');
                                }
                            }
                        },
                        onEscape: function(){
                            prioModal.modal('hide');
                        }
                    });

                    var selectedErow = tplCtx.pos;

                    prioModal.on('shown.bs.modal', function (e) {

                        $('.estimatedialog #add-button').data("pos", selectedErow );

                    });

                    prioModal.on('shown.bs.modal', function (e) {
                        var employerStateSelector = $(".bootbox select.search-person").select2("destroy");

                        employerStateSelector.select2({
                            formatSelection: function (state) {

                                if (!state.id) { return state.text; }
                                var $state = jQuery(
                                    '<div class="leaderboard__container">'+
                                    '    <span class="leaderboard__name">' + state.text + baseUrl+state.element[0].attributes[1].value +' </span>' +
                                    '</div>'
                                );

                                return $state;
                            },

                            closeOnSelect: false,
                            //width: '80%',
                            placeholder: "aucune",
                            containerCssClass : 'aapstatus-container',
                            dropdownCssClass: "aapstatus-dropdown"
                        });
                    })

                    prioModal.modal("show");
                });

                $('.candidatenumber').off().click(function() {
                    tplCtx.pos = $(this).data("pos");
                    tplCtx.collection = "answers";
                    tplCtx.id = $(this).data("id");
                    tplCtx.key = $(this).data("key");
                    tplCtx.form = $(this).data("form");
                    tplCtx.min = $(this).data("min");
                    tplCtx.candidatenumber = $(this).data("candidatenumber");
                    tplCtx.path = "answers."+tplCtx.form+"."+tplCtx.key+"."+tplCtx.pos+".candidateNumber";

                    dyFObj.openForm( paramsCandidateNumber,null, { "candidateNumber" : tplCtx.candidatenumber} , null , null , {
                        type : "bootbox",
                        notCloseOpenModal : true,
                    });
                });

                $('.locklinestate').off().click(function() {
                    tplCtx = {};
                    tplCtx.pos = $(this).data("pos");
                    tplCtx.collection = "answers";
                    tplCtx.id = $(this).data("id");
                    tplCtx.key = $(this).data("key");
                    tplCtx.form = $(this).data("form");
                    tplCtx.value = $(this).data("target");
                    tplCtx.path = "answers."+tplCtx.form+"."+tplCtx.key+"."+tplCtx.pos+".lock";
                    dataHelper.path2Value( tplCtx, function(){
                        coremuObj.event.update(coremuObj);
   ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});

                    });
                });

                if (localStorage.getItem("coremuTableToogle") == "show"){
                    $(".toogle-maximum").show();
                }else if (localStorage.getItem("coremuTableToogle") == "hidden"){
                    $(".toogle-maximum").hide();
                }

                $(".toogle-action").off().click(function(){
                    $(".td-action").toggle("slow");
                    setTimeout(() => {
                        if($(".td-action").is(":hidden")){
                            localStorage.setItem("coremuTdactionToogle", "hidden");
                        }else{
                            localStorage.setItem("coremuTdactionToogle", "show");
                        }
                    }, "2000");
                });

                if (localStorage.getItem("coremuTdactionToogle") == "show"){
                    $(".td-action").show();
                }else if (localStorage.getItem("coremuTdactionToogle") == "hidden"){
                    $(".td-action").hide();
                }

                $(".toogle-minimal-action").off().click(function(){
                    $("ul.propositionAction").toggleClass("td-action-maximum" , 500);
                    setTimeout(() => {
                        if($("ul.propositionAction").hasClass("td-action-maximum")){
                            localStorage.setItem("coremuTdactionClass", "hasClass");
                        }else{
                            localStorage.setItem("coremuTdactionClass", "hasnotClass");
                        }
                    }, "2000");
                });

                if (localStorage.getItem("coremuTdactionClass") == "hasClass"){
                    $("ul.propositionAction").addClass("td-action-maximum");
                }else if (localStorage.getItem("coremuTdactionClass") == "hasnotClass"){
                    $("ul.propositionAction").removeClass("td-action-maximum");
                }

                $('.estibtnedit[data-kunik="budgetdepense"]').off().click(function() {
                    var btn = $(this);
                    tplCtx.pos = $(this).data("pos");
                    tplCtx.collection = "answers";
                    tplCtx.id = $(this).data("id");
                    tplCtx.key = $(this).data("key");
                    tplCtx.form = $(this).data("form");
                    tplCtx.price = $(this).data("price");
                    tplCtx.uid = $(this).data("uid");
                    tplCtx.uname = $(this).data("uname");

                    var days = $(this).data("days");
                    var price = $(this).data("price");
                    var currentValue = [{id:tplCtx.uid,text:tplCtx.uname}];
                    var toDelete = "";
                    tplCtx.value = {};
                    prioModal = bootbox.dialog({
                        message: $(".newform-estimate").html(),
                        show: false,
                        size: "large",
                        className: 'estimatedialog',
                        buttons: {
                            success: {
                                label: trad.save,
                                className: "btn-primary",
                                callback: function () {

                                    tplCtx.path = "answers";
                                    // if( notNull(formInputs [tplCtx.form]) )
                                    tplCtx.path = "answers."+tplCtx.form;

                                    tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".estimates";
                                    tplCtx.setType = [
                                        {
                                            "path": "price",
                                            "type": "int"
                                        },
                                        {
                                            "path": "date",
                                            "type": "isoDate"
                                        }
                                    ];
                                    tplCtx.updatePartial = true;
                                    var today = new Date();
                                    today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();

                                    mylog.log("btnEstimate save",tplCtx);
                                    dataHelper.path2Value( tplCtx, function(){
                                        coremuObj.event.update(coremuObj);
                                        ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});
                                        if(currentValue.length != 0){
                                            var params = {
                                                parentId : answerId,
                                                parentType : "answers",
                                                listInvite : {
                                                    citoyens :{},
                                                    organizations:{}
                                                }
                                            };
                                            $.each(currentValue,function(k,v){
                                                if(exists(v.id) && v.text){
                                                    params.listInvite.citoyens[v.id] = v.text
                                                }
                                            });
                                            ajaxPost("",baseUrl+'/'+moduleId+"/link/multiconnect",params,function(data){
                                                ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/addperson/answerid/' + answerId,
                                                    {
                                                        pers : params.listInvite.citoyens,
                                                        url : window.location.href
                                                    },
                                                    function (data) {

                                                    }, "html");
                                            });
                                        }

                                        if(toDelete != ""){
                                            tplCtx.path = "answers."+tplCtx.form+"."+tplCtx.key+"."+btn.data("pos")+".estimates."+toDelete;
                                            mylog.log(tplCtx.path,"toDelete")
                                            tplCtx.value = null;
                                            delete tplCtx.setType;
                                            delete tplCtx.updatePartial;
                                            dataHelper.path2Value( tplCtx, function(){
                                                coremuObj.event.update(coremuObj);
   ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});
                                                links.disconnect('answers',answerId,toDelete,'citoyens','contributors')
                                                var person = {};
                                                person[toDelete] = "";
                                                ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/rmperson/answerid/' + answerId,
                                                    {
                                                        pers : person,
                                                        url : window.location.href
                                                    },
                                                    function (data) {

                                                    }, "html");
                                            })
                                        }
                                        saveLinks(answerObj._id.$id,"estimated",userId);
                                        prioModal.modal('hide');

                                    } );
                                }
                            },
                            cancel: {
                                label: trad.cancel,
                                className: "btn-secondary",
                                callback: function(){
                                    prioModal.modal('hide');
                                }
                            }
                        },
                        onEscape: function(){
                            prioModal.modal('hide');
                        }
                    });

                    prioModal.on('shown.bs.modal', function (e) {
                        $(".bootbox .e-days,.bootbox .e-price").on('blur',function(){
                            $(".search-person").trigger('change');
                        })
                        setTimeout(() => {
                            $(".search-person").trigger('change');
                        }, 700);
                        $(".e-price").val(price);

                        var employerStateSelector = $(".bootbox select.search-person").select2("destroy");

                        employerStateSelector.select2({
                            formatSelection: function (state) {
                                if (!state.id) { return state.text; }

                                var $state = jQuery(
                                    '<div class="leaderboard__container">'+
                                    '    <span class="leaderboard__name">' + state.text + baseUrl+state.element[0].attributes[1].value +'</span>' +
                                    '</div>'
                                );

                                return $state;
                            },
                            closeOnSelect: false,
                            //width: '80%',
                            placeholder: "aucune",
                            containerCssClass : 'aapstatus-container',
                            dropdownCssClass: "aapstatus-dropdown"
                        });
                    });

                    prioModal.modal("show");

                });

                $(".editbudgetdepense").off().click(function() {
                    tplCtx.id = $(this).data("id");
                    tplCtx.collection = $(this).data("collection");
                    tplCtx.path = $(this).data("path");
                    tplCtx.actionid = $(this).data("actionid");
                    tplCtx.key = $(this).data("key");
                    dyFObj.openForm( sectionDyf.budgetdepense,null, coremuObj.answerObjdepense.depense[tplCtx.key] , null , null , {
                        type : "bootbox",
                        notCloseOpenModal : true,
                    });
                });

                $('.locklinestatehide').off().click(function() {
                    tplCtx = {};
                    tplCtx.pos = $(this).data("pos");
                    tplCtx.collection = "answers";
                    tplCtx.id = $(this).data("id");
                    tplCtx.key = $(this).data("key");
                    tplCtx.form = $(this).data("form");
                    tplCtx.value = $(this).data("target");
                    tplCtx.path = "answers."+tplCtx.form+"."+tplCtx.key+"."+tplCtx.pos+".lock";
                    dataHelper.path2Value( tplCtx, function(){});
                });

                $('.unlocklinestatehide').off().click(function() {
                    tplCtx = {};
                    tplCtx.pos = $(this).data("pos");
                    tplCtx.collection = "answers";
                    tplCtx.id = $(this).data("id");
                    tplCtx.key = $(this).data("key");
                    tplCtx.form = $(this).data("form");
                    tplCtx.value = null;
                    tplCtx.path = "answers."+tplCtx.form+"."+tplCtx.key+"."+tplCtx.pos+".lock";
                    dataHelper.path2Value( tplCtx, function(){});
                });

                $('.opencoremucomment').off().click(function(event) {
                    event.preventDefault();
                    coremuObj.actualcommentId = $(this).data('pos');
                    commentObj.afterSaveReload = function (){
                            ajaxPost(
                                null,
                                baseUrl + '/' + moduleId + "/comment/countcommentsfrom",
                                {
                                    "type": "answers",
                                    "id": answerId,
                                    "path": answerId + "depense" + coremuObj.actualcommentId
                                },
                                function (data) {
                                    if (typeof data.count != "undefined") {
                                        coremuObj.cache.comment[answerId + "depense" + coremuObj.actualcommentId] = data.count;
                                        $('.depense'+coremuObj.actualcommentId+'comment').html(data.count);
                                    }
                                },
                                null,
                                null,
                                {"async": false}
                            );
                        coremuObj.actualcommentId = "";
                    };
                    commentObj.openPreview(
                    'answers',
                    answerId,
                    answerId + 'depense' + $(this).data("pos") ,
                    '',
                    '',
                        '',
                    {notCloseOpenModal : true}
                    );
                    return false
                });

            },

            toogleList : function(){
                $('.btn-toogle-list').off().click(function() {
                    let thisbtn = $(this);
                    if (thisbtn.attr("data-active") == "false") {
                        $('.candidatebtn-div[data-pos="'+thisbtn.attr("data-pos")+'"]').hide();
                        $('.btn-toogle-list').each(function (index) {
                            $(this).attr("data-active" , "false");
                        });

                        thisbtn.attr("data-active" , "true");

                        $('.leaderboard__container[data-pos="'+thisbtn.attr("data-pos")+'"]').each(function (index) {
                            $(this).attr("data-active", "false");
                        });
                        $('.leaderboard__container[data-pos="'+thisbtn.attr("data-pos")+'"][data-type="' + thisbtn.data("target") + '"').each(function (index) {
                            $(this).attr("data-active", "true");
                        });
                        $('.smallspan[data-pos="'+thisbtn.attr("data-pos")+'"]').each(function (index) {
                            if ($(this).attr("data-active") != "always") {
                                $(this).attr("data-active", false);
                            }
                        });
                        $('.smallspan[data-pos="'+thisbtn.attr("data-pos")+'"][data-type="' + thisbtn.data("target") + '"').each(function (index) {
                            $(this).attr("data-active", true);
                        });

                        if(thisbtn.attr("data-target") == "deny") {
                            $('.toogle-maximum[data-pos="'+thisbtn.attr("data-pos")+'"]').addClass('toogledeny');
                            $('.leaderboard__profiles[data-pos="'+thisbtn.attr("data-pos")+'"]').addClass('toogledeny')
                        }
                        if(thisbtn.attr("data-target") == "price") {
                            $('.toogle-maximum[data-pos="'+thisbtn.attr("data-pos")+'"]').addClass('toogleprice');
                            $('.leaderboard__profiles[data-pos="'+thisbtn.attr("data-pos")+'"]').addClass('toogleprice');
                        }
                    } else {
                        $('.close-smallspan[data-pos="'+thisbtn.attr("data-pos")+'"]').trigger("click");
                    }
                });
            },

            financementLine : function () {
                $('.rebtnFinancer').off().click(function() {
                    tplCtx.pos = $(this).data("pos");
                    var rercpos =  tplCtx.pos;
                    tplCtx.budgetpath = $(this).data("budgetpath");
                    tplCtx.collection = "answers";
                    tplCtx.id = $(this).data("id");
                    tplCtx.form = $(this).data("form");
                    prioModal = bootbox.dialog({
                        message: coremuObj.block.newFormFinancer(coremuObj , tplCtx.pos),
                        title: "Ajouter un Financeur sur une ligne",
                        className: 'financerdialog',
                        show: false,
                        size: "large",
                        buttons: {
                            success: {
                                label: trad.save,
                                className: "btn-primary save-finance",
                                callback: function () {

                                    tplCtx.setType = [
                                        {
                                            "path": "amount",
                                            "type": "int"
                                        },
                                        {
                                            "path": "date",
                                            "type": "isoDate"
                                        }
                                    ];

                                    tplCtx.path = "answers.aapStep1";

                                    tplCtx.path = tplCtx.path+"."+tplCtx.budgetpath+"."+tplCtx.pos+".financer."+Object.keys(coremuObj.cache.arrageDepense[tplCtx.pos].financement).length;

                                    var today = new Date();
                                    today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();
                                    tplCtx.value = {
                                        line   : $(".financerdialog .fi-fond").val(),
                                        amount : $(".financerdialog .fi-montant").val(),
                                        user   : userId,
                                        date   : today
                                    };

                                    if($(".financerdialog .fi-financer").val() != 0 ){
                                        tplCtx.value.id = $(".financerdialog .fi-financer").val();
                                        tplCtx.value.name = $(".financerdialog .fi-financer option:selected").text();
                                    }else if($(".financerdialog .fi-name").val() != "" ){
                                        tplCtx.value.name = $(".financerdialog .fi-name").val();
                                        tplCtx.value.email = $(".financerdialog .fi-mail").val();
                                    }

                                    delete tplCtx.pos;
                                    delete tplCtx.budgetpath;
                                    mylog.log("btnFinancer save",tplCtx);
                                    dataHelper.path2Value( tplCtx, function(params) {

                                        ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/newfinancement/answerid/' + answerId,
                                            {
                                                pos : rercpos,
                                                url : window.location.href
                                            },
                                            function (data) {

                                            }, "html");
                                        
                                        ajaxPost("", baseUrl + "/co2/aap/commonaap/action/notifyAddFinance", {
                                            answerId:tplCtx.id,
                                            status : tplCtx.value.line+' : '+ tplCtx.value.amount+' Euro'
                                        }, function(data){}, function(error){
                                            mylog.log("ajaxPost error", error)
                                        }, "json")

                                        prioModal.modal('hide');
                                        coremuObj.event.update(coremuObj);
                                    } );

                                    delete tplCtx.setType;
                                }
                            },
                            cancel: {
                                label: trad.cancel,
                                className: "btn-secondary",
                                callback: function() {
                                }
                            }
                        },
                        onEscape: function() {
                            prioModal.modal("hide");
                        }
                    });
                    prioModal.modal("show");

                    prioModal.on('shown.bs.modal', function (e) {

                        $(".fi-fond").autocomplete(optionsfinancementfinancer);
                        $(".envdiv").hide();

                        $(".sup-btn").off().on("click",function() {
                            prioModal.modal("hide");
                            dyFObj.openForm("organization",null,null,null,
                                {
                                    onLoads : {
                                        onload: function () {
                                            $(".financerdialog").css("z-index" , "100000");

                                        }
                                    },
                                    afterSave : function(data) {
                                        coremuObj.community[data.id] = data.map;
                                        coremuObj.community[data.id]["roles"] = ["Financeur"];
                                        var sendDataM = {
                                            parentId : cntxtId,
                                            parentType : cntxtType,
                                            listInvite : {
                                                organizations : {}
                                            }
                                        };
                                        sendDataM.listInvite.organizations[data.id] = {};
                                        sendDataM.listInvite.organizations[data.id]["name"] = data.map.name;
                                        sendDataM.listInvite.organizations[data.id]["roles"] = ["Financeur"];

                                        ajaxPost("",
                                            baseUrl+"/co2/link/multiconnect",
                                            sendDataM,
                                            function() {
                                                dyFObj.closeForm();
                                                coremuObj.event.update(coremuObj);
                                            },
                                            null,
                                            "json"
                                        );
                                    },
                                    type : "bootbox",
                                    notCloseOpenModal : true
                                },
                                {
                                    onLoads : {
                                        onload: function () {
                                            $(".financerdialog").css("z-index" , "100000");

                                        }
                                    },
                                    afterSave : function(data) {
                                        var sendDataM = {
                                            parentId : cntxtId,
                                            parentType : cntxtType,
                                            listInvite : {
                                                organizations : {}
                                            }
                                        };
                                        sendDataM.listInvite.organizations[data.id] = {};
                                        sendDataM.listInvite.organizations[data.id]["name"] = data.map.name;
                                        sendDataM.listInvite.organizations[data.id]["roles"] = ["Financeur"];

                                        ajaxPost("",
                                            baseUrl+"/co2/link/multiconnect",
                                            sendDataM,
                                            function(data) {
                                                dyFObj.closeForm();
                                                coremuObj.event.update(coremuObj);
                                            },
                                            null,
                                            "json"
                                        );
                                    },
                                    type : "bootbox",
                                    notCloseOpenModal : true
                                }
                            );
                        });

                        $(".switchcombtn").on("click",function() {
                            $(".financerdialog").find("."+$(this).data("switchactbtn")).addClass("active");
                            $(".financerdialog").find("."+$(this).data("removebtn")).removeClass("active");
                            $(".financerdialog").find("."+$(this).data("switchactdiv")).removeClass("hide");
                            $(".financerdialog").find("."+$(this).data("removediv")).addClass("hide");
                        });

                        $(".financerdialog .fi-montant.todoinput").keyup( function( event ) {
                            if($(".financerdialog .fi-montant.todoinput").val() > 0) {

                                if (parseInt($(".financerdialog .fi-montant.todoinput").val()) > parseInt($(".financerdialog .fi-montant.todoinput").attr("max"))) {
                                    $(".financerdialog .fi-montant.todoinput").val($(".financerdialog .fi-montant.todoinput").attr("max"));
                                }

                                /*if (parseInt($(".financerdialog .fi-montant.todoinput").val()) > parseInt($(".financerdialog .fi-resteEnv").data("value"))){
                                    //$(".financerdialog .fi-montant.todoinput").val($(".financerdialog .fi-resteEnv").data("value"));
                                    $(".restEnvWarning").html("<i class='fa fa-info-circle'></i> Le montant à financer depasse la valeur restant dans l'enveloppe du financeur")
                                }else{
                                    $(".restEnvWarning").html("");
                                }*/

                                var entry = parseInt($(".financerdialog .fi-montant.todoinput").val());
                                var left = parseInt(coremuObj.cache.arrageDepense[rercpos].left);

                                if (coremuObj.cache.arrageDepense[rercpos].left > 0) {
                                    if (entry < left) {
                                        $('.financerdialog tr td:nth-child(4)').html(entry);
                                    } else {
                                        $('.financerdialog tr td:nth-child(4)').html(left);
                                    }
                                }
                            }
                        });

                        $(".financerdialog .fi-name.todoinput").keyup( function( event ) {
                            var thisbtn = $(this);
                            if (sectionDyf.financementfinancerParamsData.envelope == true){

                                if((typeof orgsfitotal[thisbtn.val()] != "undefined" &&
                                sectionDyf.financementfinancerenvelopeData[new Date().getFullYear()][thisbtn.val()] != "undefined")){
                                    $(".envdiv").show();
                                    $(".financerdialog .fi-Env").html(sectionDyf.financementfinancerenvelopeData[new Date().getFullYear()][thisbtn.val()].toString() );
                                    $(".financerdialog .fi-resteEnv").html((sectionDyf.financementfinancerenvelopeData[new Date().getFullYear()][thisbtn.val()] - orgsfitotal[thisbtn.val()]).toString());
                                    $(".financerdialog .fi-resteEnv").data("value" , (sectionDyf.financementfinancerenvelopeData[new Date().getFullYear()][$(".financerdialog .fi-financer.todoinput option:selected").text()] - orgsfitotal[$(".financerdialog .fi-financer.todoinput option:selected").text()]) );


                                }else{
                                    $(".envdiv").hide();
                                }

                            }
                        });

                        $(".financerdialog .fi-financer.todoinput").on('change' , function( event ) {
                            var thisbtn = $(this);
                            if (sectionDyf.financementfinancerParamsData.envelope == true){

                                if((typeof orgsfitotal[$(".financerdialog .fi-financer.todoinput option:selected").text()] != "undefined" &&
                                sectionDyf.financementfinancerenvelopeData[new Date().getFullYear()][$(".financerdialog .fi-financer.todoinput option:selected").text()] != "undefined")){
                                    $(".envdiv").show();
                                    $(".financerdialog .fi-Env").html(sectionDyf.financementfinancerenvelopeData[new Date().getFullYear()][$(".financerdialog .fi-financer.todoinput option:selected").text()].toString() );
                                    $(".financerdialog .fi-resteEnv").html((sectionDyf.financementfinancerenvelopeData[new Date().getFullYear()][$(".financerdialog .fi-financer.todoinput option:selected").text()] - orgsfitotal[$(".financerdialog .fi-financer.todoinput option:selected").text()]).toString());
                                    $(".financerdialog .fi-resteEnv").data("value" , (sectionDyf.financementfinancerenvelopeData[new Date().getFullYear()][$(".financerdialog .fi-financer.todoinput option:selected").text()] - orgsfitotal[$(".financerdialog .fi-financer.todoinput option:selected").text()]) );

                                }else{
                                    $(".envdiv").hide();
                                }

                            }
                        });

                        $('.save-finance').attr('disabled','disabled');
                        $('.fi-montant').on('blur', function(){
                            if($(this).val() != ""){
                                $('.save-finance').removeAttr('disabled');
                            }
                        });

                    });
                });

                $('.FinanceAllLine').off().click(function() {
                    var bfibudgetpath = $(this).data("budgetpath"),
                        bficollection = "answers" ,
                        bfiid = $(this).data("id") ,
                        bfiform = $(this).data("form");

                    prioModal = bootbox.dialog({
                        message: coremuObj.block.newFormFinancerAll(coremuObj),
                        title: "Ajouter un Financeur sur plusieurs lignes",
                        className: 'financerdialog',
                        show: false,
                        size: "large",
                        buttons: {
                            success: {
                                label: trad.save,
                                className: "btn-primary save-finance",
                                callback: function () {
                                    var fpos = 0;
                                    var fposarray = Object.keys(answerObj.answers.aapStep1.depense) ;
                                    $.each(financementobj, function (ind, val) {
                                        if (parseInt(val.val) > 0) {

                                            tplCtx.setType = [
                                                {
                                                    "path": "amount",
                                                    "type": "int"
                                                },
                                                {
                                                    "path": "date",
                                                    "type": "isoDate"
                                                }
                                            ];

                                            tplCtx.budgetpath = bfibudgetpath;
                                            tplCtx.collection = bficollection;
                                            tplCtx.id = bfiid;
                                            tplCtx.form = bfiform;

                                            tplCtx.path = "answers.aapStep1";

                                            if (!Array.isArray(financerList[fpos]) && typeof financerList[fpos] != "undefined"){
                                                financerList[fpos] = Object.values(financerList[fpos]);
                                            }
                                            tplCtx.path = tplCtx.path + "." + tplCtx.budgetpath + "." + fposarray[fpos] + ".financer." + financerList[fpos].length;

                                            var today = new Date();
                                            today = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
                                            tplCtx.value = {
                                                line: $(".financerdialog .fi-fond").val(),
                                                amount: val.val,
                                                user: userId,
                                                date: today
                                            };

                                            if ($(".financerdialog .fi-financer").val() != 0) {
                                                tplCtx.value.id = $(".financerdialog .fi-financer").val();
                                                tplCtx.value.name = $(".financerdialog .fi-financer option:selected").text();
                                            } else if ($(".financerdialog .fi-name").val() != "") {
                                                tplCtx.value.name = $(".financerdialog .fi-name").val();
                                                tplCtx.value.email = $(".financerdialog .fi-mail").val();
                                            }

                                            delete tplCtx.budgetpath;
                                            mylog.log("btnFinancer save", tplCtx);
                                            $.ajax({
                                                type : 'POST',
                                                data : {pos : fpos,
                                                    url : window.location.href},
                                                url : baseUrl + '/survey/answer/rcnotification/action/newfinancement/answerid/' + answerId,
                                                dataType : "json",
                                                async : false,

                                                success : function(data){}
                                            });
                                            dataHelper.path2Value(tplCtx, function (params) {

                                                prioModal.modal('hide');

                                                coremuObj.event.update(coremuObj);
                                            });

                                            delete tplCtx.setType;
                                        }
                                        fpos++;
                                    });
                                }
                            },
                            cancel: {
                                label: trad.cancel,
                                className: "btn-secondary",
                                callback: function() {
                                }
                            }
                        },
                        onEscape: function() {
                            prioModal.modal("hide");
                        }
                    });
                    prioModal.modal("show");

                    var thisbtn = $(this);

                    prioModal.on('shown.bs.modal', function (e) {

                        $('.save-finance').attr('disabled','disabled');
                        $('.fi-montant').on(' ', function(){
                            if($(this).val() != ""){
                                $('.save-finance').removeAttr('disabled');
                            }
                        });

                        $(".fi-fond").autocomplete(optionsfinancementfinancer);
                        $(".envdiv").hide();

                        $(".sup-btn").off().on("click",function() {
                            prioModal.modal("hide");
                            dyFObj.openForm("organization",function(){ },null,null,
                                {
                                    afterSave : function(data) {
                                        var sendDataM = {
                                            parentId : cntxtId,
                                            parentType : cntxtType,
                                            listInvite : {
                                                organizations : {}
                                            }
                                        };
                                        sendDataM.listInvite.organizations[data.id] = {};
                                        sendDataM.listInvite.organizations[data.id]["name"] = data.map.name;
                                        sendDataM.listInvite.organizations[data.id]["roles"] = ["Financeur"];

                                        ajaxPost("",
                                            baseUrl+"/co2/link/multiconnect",
                                            sendDataM,
                                            function(data) {
                                                dyFObj.closeForm();
                                                // yyyyy
                                            },
                                            null,
                                            "json"
                                        );
                                    }
                                }
                            );
                        });

                        $(".switchcombtn").on("click",function() {
                            $(".financerdialog").find("."+$(this).data("switchactbtn")).addClass("active");
                            $(".financerdialog").find("."+$(this).data("removebtn")).removeClass("active");
                            $(".financerdialog").find("."+$(this).data("switchactdiv")).removeClass("hide");
                            $(".financerdialog").find("."+$(this).data("removediv")).addClass("hide");
                        });

                        $(".financerdialog .fi-montant.todoinput").keyup( function( event ) {
                            if($(".financerdialog .fi-montant.todoinput").val() > 0) {

                                if (parseInt($(".financerdialog .fi-montant.todoinput").val()) > parseInt($(".financerdialog .fi-montant.todoinput").attr("max"))){
                                    $(".financerdialog .fi-montant.todoinput").val($(".financerdialog .fi-montant.todoinput").attr("max"));
                                }

                                if (parseInt($(".financerdialog .fi-montant.todoinput").val()) > parseInt($(".financerdialog .fi-resteEnv").data("value"))){
                                    //$(".financerdialog .fi-montant.todoinput").val($(".financerdialog .fi-resteEnv").data("value"));
                                    $(".restEnvWarning").html("<i class='fa fa-info-circle'></i> Le montant à financer depasse la valeur restant dans l'enveloppe du financeur")
                                }else{
                                    $(".restEnvWarning").html("");
                                }

                                var entry = parseInt($(".financerdialog .fi-montant.todoinput").val());
                                $('.financerdialog tr[data-id="total"] td:nth-child(4)').html(entry);
                                var left = 0;
                                $.each(coremuObj.cache.arrageDepense, function (index, val) {
                                        if (entry > 0){
                                            left = parseInt(val.left);
                                            if (entry < left){
                                                $('.financerdialog tr[data-id="' + index + '"] td:nth-child(4)').html(entry);
                                                entry -= left;
                                            } else {
                                                $('.financerdialog tr[data-id="' + index + '"] td:nth-child(4)').html(reste);
                                                entry = 0;
                                            }
                                        }
                                });

                            }
                        });

                        $(".financerdialog .fi-name.todoinput").keyup( function( event ) {
                            var thisbtn = $(this);
                            if (sectionDyf.financementfinancerParamsData.envelope == true){

                                if((typeof orgsfitotal[thisbtn.val()] != "undefined" &&
                                sectionDyf.financementfinancerenvelopeData[thisbtn.val()] != "undefined")){
                                    $(".envdiv").show();
                                    $(".financerdialog .fi-Env").html(sectionDyf.financementfinancerenvelopeData[new Date().getFullYear()][thisbtn.val()].toString() );
                                    $(".financerdialog .fi-resteEnv").html((sectionDyf.financementfinancerenvelopeData[new Date().getFullYear()][thisbtn.val()] - orgsfitotal[thisbtn.val()]).toString());
                                    $(".financerdialog .fi-resteEnv").data("value" , (sectionDyf.financementfinancerenvelopeData[new Date().getFullYear()][$(".financerdialog .fi-financer.todoinput option:selected").text()] - orgsfitotal[$(".financerdialog .fi-financer.todoinput option:selected").text()]) );


                                }else{
                                    $(".envdiv").hide();
                                }

                            }
                        });

                        $(".financerdialog .fi-financer.todoinput").on('change' , function( event ) {
                            var thisbtn = $(this);
                            if (sectionDyf.financementfinancerParamsData.envelope == true){

                                if((typeof orgsfitotal[$(".financerdialog .fi-financer.todoinput option:selected").text()] != "undefined" &&
                                sectionDyf.financementfinancerenvelopeData[$(".financerdialog .fi-financer.todoinput option:selected").text()] != "undefined")){
                                    $(".envdiv").show();
                                    $(".financerdialog .fi-Env").html(sectionDyf.financementfinancerenvelopeData[new Date().getFullYear()][$(".financerdialog .fi-financer.todoinput option:selected").text()].toString() );
                                    $(".financerdialog .fi-resteEnv").html((sectionDyf.financementfinancerenvelopeData[new Date().getFullYear()][$(".financerdialog .fi-financer.todoinput option:selected").text()] - orgsfitotal[$(".financerdialog .fi-financer.todoinput option:selected").text()]).toString());
                                    $(".financerdialog .fi-resteEnv").data("value" , (sectionDyf.financementfinancerenvelopeData[new Date().getFullYear()][$(".financerdialog .fi-financer.todoinput option:selected").text()] - orgsfitotal[$(".financerdialog .fi-financer.todoinput option:selected").text()]) );

                                }else{
                                    $(".envdiv").hide();
                                }

                            }
                        });
                    });

                });

                $('.fibtnedit').off().click(function() {
                    tplCtx.pos = $(this).data("pos");
                    tplCtx.key = $(this).data("key");
                    tplCtx.collection = "answers";
                    tplCtx.id = $(this).data("id");
                    tplCtx.form = $(this).data("form");
                    tplCtx.uid = $(this).data("uid");

                    if (typeof $(this).data("finid") != "undefined") {
                        var finid = $(this).data("finid");
                    }else {
                        var finname = $(this).data("finname");
                        var finmail = $(this).data("finmail");
                    }

                    var finfond = $(this).data("finfond");
                    var finmontant = $(this).data("finamount");

                    prioModal = bootbox.dialog({
                        message: $(".new-form-financer").html(),
                        title: "Modifier un financement",
                        className: 'financerdialog',
                        show: false,
                        size: "large",
                        buttons: {
                            success: {
                                label: trad.save,
                                className: "btn-primary",
                                callback: function () {

                                    //var formInputsHere = formInputs;
                                    // var financersCount = ( typeof eval("answerObj.answers."+tplCtx.budgetpath+"["+tplCtx.pos+"].financer") != "undefined" ) ? eval("answerObj.answers."+tplCtx.budgetpath+"["+tplCtx.pos+"].financer").length : 0;

                                    tplCtx.path = "answers."+tplCtx.form;

                                    tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".financer."+tplCtx.uid;

                                    // if( notNull(formInputs [tplCtx.form]) )
                                    // 	tplCtx.path = "answers."+tplCtx.budgetpath+"."+tplCtx.pos+".financer."+financersCount;

                                    var today = new Date();
                                    today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();
                                    tplCtx.value = {
                                        line   : $(".financerdialog .fi-fond").val(),
                                        amount : $(".financerdialog .fi-montant").val(),
                                        user   : userId,
                                        date   : today
                                    };

                                    if($(".financerdialog .fi-financer").val() != 0 ){
                                        tplCtx.value.id = $(".financerdialog .fi-financer").val();
                                        tplCtx.value.name = $(".financerdialog .fi-financer option:selected").text();
                                    }else if($(".financerdialog .fi-name").val() != "" ){
                                        tplCtx.value.name = $(".financerdialog .fi-name").val();
                                        tplCtx.value.email = $(".financerdialog .fi-mail").val();
                                    }

                                    if(tplCtx.value.name == "" )

                                        delete tplCtx.pos;
                                    delete tplCtx.budgetpath;
                                    mylog.log("btnFinancer save",tplCtx);
                                    dataHelper.path2Value( tplCtx, function(params) {
                                        prioModal.modal('hide');
                                        coremuObj.event.update(coremuObj);
                                    } );
                                }
                            },
                            cancel: {
                                label: trad.cancel,
                                className: "btn-secondary",
                                callback: function() {
                                }
                            }
                        },
                        onEscape: function() {
                            prioModal.modal("hide");
                        }
                    });
                    prioModal.modal("show");

                    prioModal.on('shown.bs.modal', function (e) {
                        if (typeof finid != "undefined") {
                            $('.fi-financer option[value="' + finid +'"]').prop("selected", true);

                            $(".financerdialog").find(".dcombtn").addClass("active");
                            $(".financerdialog").find(".icombtn").removeClass("active");
                            $(".financerdialog").find(".dcomdiv").removeClass("hide");
                            $(".financerdialog").find(".icomdiv").addClass("hide");
                        }else {
                            $(".fi-name").val(finname);
                            $(".fi-mail").val(finmail);

                            $(".financerdialog").find(".icombtn").addClass("active");
                            $(".financerdialog").find(".dcombtn").removeClass("active");
                            $(".financerdialog").find(".icomdiv").removeClass("hide");
                            $(".financerdialog").find(".dcomdiv").addClass("hide");
                        }
                        $(".fi-fond").val(finfond);
                        $(".fi-montant").val(finmontant);
                    });

                    $(".switchcombtn").on("click",function() {
                        $(".financerdialog").find("."+$(this).data("switchactbtn")).addClass("active");
                        $(".financerdialog").find("."+$(this).data("removebtn")).removeClass("active");
                        $(".financerdialog").find("."+$(this).data("switchactdiv")).removeClass("hide");
                        $(".financerdialog").find("."+$(this).data("removediv")).addClass("hide");
                    });

                });

                $('.fibtndelete').off().click(function() {
                    tplCtx.pos = $(this).data("pos");
                    tplCtx.collection = "answers";
                    tplCtx.id = $(this).data("id");
                    tplCtx.key = $(this).data("key");
                    tplCtx.form = $(this).data("form");
                    tplCtx.uid = $(this).data("uid");

                    prioModal = bootbox.dialog({
                        title: trad.confirmdelete,
                        show: false,
                        message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                        buttons: [
                            {
                                label: "Ok",
                                className: "btn btn-primary pull-left",
                                callback: function() {
                                    var fi_pos = FiData[tplCtx.pos]["financer"];

                                    // delete fi_pos[tplCtx.uid];
                                    fi_pos.splice(parseInt(tplCtx.uid), 1);

                                    tplCtx.path = "answers";
                                    // if( notNull(formInputs [tplCtx.form]) )
                                    tplCtx.path = "answers."+tplCtx.form;

                                    tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".financer";

                                    tplCtx.value = fi_pos;

                                    mylog.log("btnEstimate save",tplCtx);
                                    dataHelper.path2Value( tplCtx, function(){
                                        prioModal.modal('hide');
                                        coremuObj.event.update(coremuObj);
                                    } );
                                }
                            },
                            {
                                label: "Annuler",
                                className: "btn btn-default pull-left",
                                callback: function() {}
                            }
                        ]
                    });

                    prioModal.modal("show");
                });
            },

            d3 : {

                zoomFunction: function (containerFrame) {
                    if (notNull(d3) && notNull(d3.event) && notNull(d3.event.transform)) {
                        const {x, y, k} = d3.event.transform;
                        containerFrame.attr('transform', `translate(${x} ${y}) scale(${k})`);
                    }
                }

            }
        },

        update: function(coremuObj){
            /*if(coremuObj.isUpdating){
                return;
            }*/
            coremuObj.isUpdating = true;
            var newData = coremuObj.arrageData(coremuObj.getData(coremuObj, answerId , true));
            var difference = coremuObj.deepDiffMapper.map(coremuObj.cache, newData);
            var updatedValue = coremuObj.getPaths(coremuObj,difference,'updated');
            var createdValue = coremuObj.getPaths(coremuObj,difference,'created');
            var deletedValue = coremuObj.getPaths(coremuObj,difference,'deleted');

            console.log("wwwwwwww" , difference , updatedValue , createdValue , deletedValue);

            coremuObj.cache = newData;
            $.each(updatedValue, function(index, value) {
                if(notNull(value) && notNull(coremuObj.event.callbacks.onUpdateValue[value[value.length - 1]])){
                    coremuObj.event.callbacks.onUpdateValue[value[value.length - 1]](coremuObj, value , difference , newData );
                }
                if(notNull(value) && notNull(coremuObj.event.callbacks.onUpdateValue[value[value.length - 2]])){
                    coremuObj.event.callbacks.onUpdateValue[value[value.length - 2]](coremuObj, value , difference , newData );
                }
                if(notNull(value) && notNull(coremuObj.event.callbacks.onUpdateValue[value[value.length - 3]])){
                    coremuObj.event.callbacks.onUpdateValue[value[value.length - 3]](coremuObj, value , difference , newData );
                }
                if(notNull(value) && notNull(coremuObj.event.callbacks.onUpdateValue[value[value.length - 4]])){
                    coremuObj.event.callbacks.onUpdateValue[value[value.length - 4]](coremuObj, value , difference , newData );
                }
            });
            $.each(createdValue, function(index, value) {
                if(notNull(value) && notNull(coremuObj.event.callbacks.onCreateValue[value[value.length - 1]])){
                    coremuObj.event.callbacks.onCreateValue[value[value.length - 1]](coremuObj, value , difference , newData );
                }
                if(notNull(value) && notNull(coremuObj.event.callbacks.onCreateValue[value[value.length - 2]])){
                    coremuObj.event.callbacks.onCreateValue[value[value.length - 2]](coremuObj, value , difference , newData );
                }
                if(notNull(value) && notNull(coremuObj.event.callbacks.onCreateValue[value[value.length - 3]])){
                    coremuObj.event.callbacks.onCreateValue[value[value.length - 3]](coremuObj, value , difference , newData );
                }
                if(notNull(value) && notNull(coremuObj.event.callbacks.onCreateValue[value[value.length - 4]])){
                    coremuObj.event.callbacks.onCreateValue[value[value.length - 4]](coremuObj, value , difference , newData );
                }
                if(value.length == 3 && $.isNumeric(value[1])){
                    coremuObj.event.callbacks.onCreateValue.createEntiereline(coremuObj , value[1]);
                }
            });
            $.each(deletedValue, function(index, value) {
                if(notNull(value) && notNull(coremuObj.event.callbacks.onCreateValue[value[value.length - 1]])){
                    coremuObj.event.callbacks.onDeleteValue[value[value.length - 1]](coremuObj, value , difference , newData );
                }
                if(notNull(value) && notNull(coremuObj.event.callbacks.onCreateValue[value[value.length - 2]])){
                    coremuObj.event.callbacks.onDeleteValue[value[value.length - 2]](coremuObj, value , difference , newData );
                }
                if(notNull(value) && notNull(coremuObj.event.callbacks.onCreateValue[value[value.length - 3]])){
                    coremuObj.event.callbacks.onDeleteValue[value[value.length - 3]](coremuObj, value , difference , newData );
                }
                if(notNull(value) && notNull(coremuObj.event.callbacks.onCreateValue[value[value.length - 4]])){
                    coremuObj.event.callbacks.onDeleteValue[value[value.length - 4]](coremuObj, value , difference , newData );
                }
                if(value.length == 3 && $.isNumeric(value[1])){
                    coremuObj.event.callbacks.onDeleteValue.deleteEntiereline(value[1]);
                }
            });

            if(updatedValue.length != 0 || createdValue.length != 0 || deletedValue.length != 0){
                ajaxPost("", baseUrl+'/survey/form/coremudashboard/tableonly/true/jsononly/true/depenseid/'+answerId , {},
                    function(data) {
                        tableData = data["tableAnswers"];
                        createPage (data["tableAnswers"]);
                    } , "json"
                );
                ajaxPost("", baseUrl+'/survey/form/coremufinancement/tableonly/true/jsononly/true/depenseid/'+answerId , {},
                    function(data) {
                        tableData = data["tableAnswers"];
                        if(typeof createPageFin != "undefined") {
                            createPageFin (data["tableAnswers"]);
                        }
                    } , "json"
                );
            }
            $(".tooltips").tooltip();
            coremuObj.isUpdating = false;

        },

        onReadyCallbacks : function(coremuObj) {
            $(document).ready(function () {
                coremuObj.event.afterRender.candidatecard();
                coremuObj.event.afterRender.card();
                coremuObj.event.afterRender.denycard();
                coremuObj.event.afterRender.pricecard();
                coremuObj.event.afterRender.trLine();
                coremuObj.event.afterRender.financementLine();

                //adds a line into answer
                $( ".addbudgetdepense" ).off().on("click",function() {
                    tplCtx.id = $(this).data("id");
                    tplCtx.collection = $(this).data("collection");
                    tplCtx.path = $(this).data("path")+coremuObj.cache.nextLinePath;
                    dyFObj.openForm( sectionDyf.budgetdepense , null, null, null , null, {
                        type : "bootbox",
                        notCloseOpenModal : true,
                    } );
                });
            });
        }

    }
}
