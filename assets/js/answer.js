   		
var answerObject={
	delete : function(id, callB){
		bootbox.confirm(trad.areyousuretodelete,
          function(result) 
          {
	          if (!result) {
	            if(typeof btnClick !="undefined" && btnClick != null)
	              btnClick.empty().html('<i class="fa fa-trash"></i>');
	            return;
	          } else {
	    		getAjax("",baseUrl+"/survey/co/delete/id/"+id,function(){
	            	if(notNull(callB))
	            		callB();
	            	else{
	            		toastr.success("La candidature a été supprimée avec succès");
	            		$("#allAnswersList .ansline"+id).remove();
	            	}
	            },"html");
	          }
          });	    
    },
    mailConfig : {
    	inputs : {},
    	setContactList: function(obj){
			str='<span class="col-xs-12 bold">Email</span> ' +
			    '<input type="text" id="contact-email" class="col-xs-12" value="" placeholder="Entrer un email"/>';
			if(typeof obj.selectOptByGroup != "undefined"){
				str='<span class="col-xs-12 bold no-padding">Sélectionner les contacts dans la liste suivante</span> ';
				str+='<div class="col-xs-12 no-padding checkboxAnswerContact">';
				$.each(obj.selectOptByGroup, function(e,v){
					if(Object.keys(v).length > 0){
						str+="<span class='col-xs-12 no-padding'><i class='fa fa-caret-down'></i> "+obj.selectContact[e]+" :</span>";
						$.each(v, function(id, data){
							img= (notEmpty(data.profilThumbImageUrl)) ? baseUrl + data.profilThumbImageUrl : modules.co2.url + "/images/thumb/default_"+data.collection+".png";
		
							str+="<div class='population-elt-finder contact-id-"+id+" col-xs-12' data-value='"+id+"' data-collection='"+data.collection+"'>"+
									'<div class="checkbox-content pull-left">'+
										'<label>'+
						    				'<input type="checkbox" class="check-population-finder checkbox-info" data-collection="'+data.collection+'" data-value="'+id+'">'+
						    				'<span class="cr"><i class="cr-icon fa fa-check"></i></span>'+
										'</label>'+
									'</div>'+
									"<div class='element-finder element-finder-"+id+"'>"+
										'<img src="'+ img+'" class="thumb-send-to pull-left img-circle" height="40" width="40">'+
										'<span class="info-contact pull-left margin-left-20">' +
											'<span class="name-element text-dark text-bold" data-id="'+id+'">' + data.name + '</span>'+
											'<br/>'+
											'<span class="type-element text-light pull-left">' + trad[data.collection]+ '</span>'+
										'</span>' +
									"</div>"+
								"</div>";
						});
					}
				});
				str+="</div>";
			}
			return str;
		},
		setObject:function(obj){
			ansName=(typeof answerValues.mappingValues != "undefined" && typeof answerValues.mappingValues.name != "undefined") ? answerValues.mappingValues.name : "";
			str='<input type="text" id="object-email" class="col-xs-12" value="Notification sur votre dossier "'+ansName+'">';
			if(typeof obj.object != "undefined"){
				if(typeof obj.object== "string")
					str+='<input type="text" id="object-email" class="col-xs-12" value="'+obj.object+'"/>';
			    else if(typeof obj.object== "array"){
			    	str+='<select id="object-email" path="links.operators" style="width:100%;" placeholder="Sélectionner un objet dans la liste">';
			    	$.each(obj.object, function(e, v){
			    		str+="<option value='"+v+"'>"+v+"</option>";
			    	});
			    	str+="</select>";
			    }
			}
			return str;
		},
		defaultMessage:function(obj){
			if(typeof obj.message != "undefined") return obj.message;
			else return "";
		}
    },
    mailTo : {
    	subject: "Votre candidature a été validée",
    	validation : function(id, step){
    		var msg="<span style='white-space: pre-line;'>"+$("#send-mail-admin #message-email").text()+"<br/></span>"+
			        aObj.mailTo.defaultRedirect(elt, type, id);
			      	  
			var params={
			        tplMail : $("#send-mail-admin #contact-email").val(),
			        tpl : "basic",
			        tplObject : answerObject.mailTo.subject,
			        html: msg
			};
			ajaxPost(
			null,
			baseUrl+"/survey/answer/mail/validation/",
			params,
			function(data){ 
			        toastr.success("Le mail a été envoyé avec succès");
			        }
			); 
    	},
		initMessage : true,
		initObject : true,
		initMail: function(elt){
			if(typeof elt.creator != "undefined" && elt.creator.email)
			        return elt.creator.email;
			else
			        return "";
		},
		defaultObject : function(elt){
			return "La réponse demande d'être approfondi";
		},
		defaultRedirect : function(elt, id){
			//urlMail=(notNull(costum)) ? 
			//return "<a href='"+baseUrl+"#answer.index.id."+id+"' target='_blank'>Retrouvez la page "+elt.name+" en cliquant sur ce lien</a>";
		},
		defaultMessage : function(elt, type, id){
			var nameContact=(typeof elt.creator != "undefined" && elt.creator.name) ? elt.creator.name : "";
			var str="Bonjour"+((notEmpty(nameContact)) ? " "+nameContact : "")+",\n"+
			        "Le contenu que vous avez publié demande d'être approfindi : \n"+
			        " - Référencer l'adresse\n"+
			        " - Ajouter la description, des mots clés, une image\n"+
			        " - Les dates ne sont pas cohérentes\n"+
			        "Encore un petit effort et vos points seront attribués";
			return str;
		},
		bootbox : function(aObj, elt, type, id){
			bootbox.dialog({
				onEscape: function() {},
				message: '<div id="send-mail-admin" class="row">  ' +
				    '<div class="col-xs-12"> ' +
				        '<span>Email</span> ' +
				        '<input type="text" id="contact-email" class="col-xs-12" value="'+aObj.mailTo.initMail(elt)+'"/>'+
				    '</div>'+
				    '<div class="col-xs-12"> ' +
				        '<span>Object</span> ' +
				        '<input type="text" id="object-email" class="col-xs-12" value="'+aObj.mailTo.defaultObject(elt)+'"/>'+
				    '</div>'+
				    '<div class="col-xs-12"> ' +
				        '<span>Message</span> ' +
				        '<textarea id="message-email" class="col-xs-12 text-dark" style="min-height:250px;">'+aObj.mailTo.defaultMessage(elt, type, id)+'</textarea>'+
				    '</div>'+
				    '</div>',
				buttons: {
				    success: {
				        label: "Ok",
				        className: "btn-primary",
				        callback: function () {
				                aObj.mailTo.sendMail(aObj, elt, type, id);
				        }
				    },
				    cancel: {
				        label: trad["cancel"],
				        className: "btn-secondary",
				        callback: function() {}
				    }
				}
			});
		},
		validate : function(aObj, elt, type, id){
			
		}
	},
	formLog : {
		manageLog: function (data={}, action="", type=null) {
			var results = {};
			if (typeof data == "object" && Object.keys(data).length > 0 && action != "") {
				ajaxPost(
					null, 
					baseUrl + '/survey/answer/managelog/action/' + action,
					data,
					function (param) { 
						if (action == "get" && param.result == true)
							results = param.data;
					},
					null,
					type,
					{ async: false }
				)	
			}
			return results;
		},
		saveLog: function(data) {
			if (data.result) {
				data.time = new Date().getTime();
				answerObject.formLog.manageLog(data, "save");
			}
				
		}
	}
}
// ajout de modification dans le log si c'est un answer
dataHelper.path2Value = function ( params, callback, asyncParam, options={} )
{
	var url = options.url ? option.url : baseUrl;
	asyncParam=(asyncParam==false) ? {async:false}  : {}  ;
	dyFObj.path2Value = {"params" : params};
	mylog.log( "path2Value", url+"/"+moduleId+"/element/updatepathvalue", params);
	if( !notEmpty(params.collection))
		alert("collection cannot be empty");
	else
		ajaxPost(
		null,
		url+"/"+moduleId+"/element/updatepathvalue",
		params,
		function(data){ 
				mylog.log("success path2Value : ",data);
				mylog.log("params path blix: ",params);
			dyFObj.path2Value.result = data;
			if (jsonHelper.pathExists("answerObject.formLog.saveLog") && 
				typeof jsonHelper.getValueByPath(params, "path") == "string" &&
				jsonHelper.getValueByPath(params, "path").includes("answers") && 
				typeof jsonHelper.getValueByPath(params, "collection") == "string" &&
				jsonHelper.getValueByPath(params, "collection").includes("answers")) {
				data.path = params.path;	
				if (localStorage.getItem("lastLogPath") == null || localStorage.getItem("lastLogPath") == undefined) 
					localStorage.setItem("lastLogPath", "");
				if (localStorage.getItem("lastLogPath").split("criteria")[0] != data.path.split("criteria")[0]) {
					answerObject.formLog.saveLog(data);
					localStorage.setItem("lastLogPath", data.path);
				}
					
			}
			if(data.result){
				if(typeof callback == "function")
					callback(data);
				else{
					toastr.success(data.msg); 
					urlCtrl.loadByHash(location.hash);
				}
			}else{
				toastr.error(data.msg);
			}
			},
			function(data){
				toastr.error("Something went really bad ! Please contact the administrator.");
			},
			null,
			asyncParam
		); 
}