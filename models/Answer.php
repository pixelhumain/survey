<?php

use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;

class Answer {
	const COLLECTION = "answers";
	const CONTROLLER = "answer";
	const ICON = "fa-calendar-check-o";
	public $parentFo;

	public static function getById($id, $fields = array()) {
		return PHDB::findOne(self::COLLECTION, array("_id" => new MongoId((string)$id)), $fields);
	}


	public static function generateAnswer($form, $draft = false, $context = null) {
		$costum = CacheHelper::getCostum();
		//Rest::json($costum); exit;
		$ans = [
			"collection" => self::COLLECTION,
			"user" => Yii::app()->session["userId"],
			"links" => [
				"answered" => [Yii::app()->session["userId"]],
				//proposed,particpated,decision,financed,financed,referenced,commented,worked,payed,tested,validated
				//each actor is connectable to his activitystream
			],
			"created" => time(),
			"cterSlug" => Slug::getSlugByIdAndCol(array_keys($form["parent"])[0], $form["parent"][array_keys($form["parent"])[0]]["type"])
		];

		$ans["context"] = $form["parent"];
		if (!empty($context)) {
			foreach ($context as $keyC => $valC) {
				if (empty($ans["context"][$keyC])) {
					$ans["context"][$keyC] = $valC;
				}
			}
		}
		if (isset($form["type"]) && $form["type"] == "aap")
			$ans["acceptation"] = "pending";

		if ($draft === true)
			$ans["draft"] =  true;

		$formList = null;
		if (!empty($form)) {
			$ans["form"] = (string)$form["_id"];
			if (!empty($form["subForms"])) {
				$formList = $form["subForms"];
				if (!is_array($formList)) {
					$ans["formId"] = implode("|", $formList);
				} else {
					$ans["formId"] = array_keys($formList);
				}

				$ans["formList"] = count($formList);
			}
			//$ans["step"] = $formList[0];
		}
		//Rest::json($costum); exit;
		if (!empty($costum) && !empty($costum["slug"])) {
			$ans["source"] = Costum::getSource($costum["slug"]);
		}
		//Rest::json($ans); exit;
		if (Costum::isSameFunction("generateAnswerBeforeSave")) {
			$paramBeforeSave = Costum::sameFunction(
				"generateAnswerBeforeSave",
				array("answer" => $ans, "formList" => $formList)
			);
			$ans = $paramBeforeSave["answer"];
		}

		if (!empty($ans) && !empty($ans["form"])) {
			$parentForm = PHDB::findOneById(Form::COLLECTION, $ans["form"]);
			if (isset($parentForm["type"]) && $parentForm["type"] == "aap") {
				$ans["updated"] = time();
			}
			if (isset($parentForm["params"]["budgetdepense"]["prefilledDepense"])) {
				$depense = [];
				foreach ($parentForm["params"]["budgetdepense"]["prefilledDepense"] as $prefid => $pref) {
					array_push($depense, array("poste" => $pref));
				}
				$ans["answers"]["aapStep1"]["depense"] = $depense;
			}

			if (isset($parentForm["params"]["budgetbudget"]["prefilledDepense"])) {
				$budget = [];
				foreach ($parentForm["params"]["budgetbudget"]["prefilledDepense"] as $prefid => $pref) {
					array_push($budget, array("poste" => $pref));
				}
				$ans["answers"]["aapStep1"]["budget"] = $budget;
			}
		}

		Yii::app()->mongodb->selectCollection(Answer::COLLECTION)->insert($ans);
		$answer = PHDB::findOne(Answer::COLLECTION, ["_id" => new MongoId($ans["_id"])]);
		return $answer;
	}

	public static function delete($id) {
		try {
			Document::removeAllForElement($id, self::COLLECTION);
			PHDB::remove(
				self::COLLECTION,
				array("_id" => new MongoId((string)$id))
			);
			return array("result" => true, "msg" => Yii::t("common", "The answer has been well deleted"));
		} catch (CTKException $e) {
			return $e->getMessage();
		}
	}
	// @RemoveLinks permet d'enlever les liens dans les autres collections liés à cette réponse
	// Cette fonction peut être surchargée dans les models des costums
	public static function removeLinks($answer) {
		if (Costum::isSameFunction("removeLinksAnswer", array("answer" => $answer)))
			Costum::sameFunction("removeLinksAnswer", array("answer" => $answer));
		return true;
	}

	public static function canAdmin($ans, $form = null) {
		if (!empty($form) && Form::canAdmin((string)$form["_id"], $form))
			return true;
		else if (Costum::isSameFunction("canAdminAnswer", array("answer" => $ans)))
			return Costum::sameFunction("canAdminAnswer", array("answer" => $ans));

		return false;
	}

	public static function canEdit($ans, $form = null, $userId = null, $parent = null) {
		if (Authorisation::isInterfaceAdmin())
			return true;
		if (ctype_xdigit($ans)) {
			$ans = self::getById($ans);
		}
		// Si auteur de la réponse
		if (isset(Yii::app()->session["userId"]) && $ans["user"] == Yii::app()->session["userId"])
			return true;
		// Si administrateur du context via l'attr parentSlug (peut correspondre au parent du form ou au context dans lequel la réponse est rempli)
		if (isset($ans["parentSlug"])) {
			$parent = Slug::getElementBySlug($ans["parentSlug"], array("name"));
			if (Authorisation::isElementAdmin($parent["id"], $parent["type"], Yii::app()->session["userId"])) {
				return true;
			}
		}
		//Si l'answer a un entrée context, on regarde dedans
		// [] Elle contient le parent du formulaire lié à la réponse
		// [] Et le contexte dans laquelle la réponse a été écrite (ex : élément projet, orga, etc)
		if (isset($ans["context"]) && !empty($ans["context"])) {
			$res = false;
			foreach ($ans["context"] as $k => $v) {
				if (Authorisation::isElementAdmin($k, $v["type"], Yii::app()->session["userId"])) {
					$res = true;
					break;
				}
			}
			if ($res)
				return true;
		}
		// TODO  check Form::Admin : Pas commenté sur master
		// !!! Vérifier que bien utile en plus du parentSlug check et du context (condition ci-dessus) !!!!
		if (!empty($ans["form"]) && Form::canAdmin($ans["form"]))
			return true;

		if (Costum::isSameFunction("canEditAnswer", array("answer" => $ans)))
			return Costum::sameFunction("canEditAnswer", array("answer" => $ans));

		return false;
	}

	public static function canAccess($answer, $form = null, $userId = null, $parentForm = null) {
		if (ctype_xdigit($answer)) {
			$answer = self::getById($answer);
			$form = Form::getByIdMongo($answer["form"]);
			$parent = Slug::getElementBySlug($form["id"]);
		}
		if (self::canEdit($answer, $form, $userId, $parentForm))
			return true;
		else if (Costum::isSameFunction("canAccessAnswer", array("answer" => $answer)))
			return Costum::sameFunction("canAccessAnswer", array("answer" => $answer));
		return false;
	}
	public static function getListBy($form = null, $costum = null, $userId = null, $cond = null) {
		$where =  array();
		$lists = array();
		if (!empty($form))
			$where["form"] = $form;
		if (!empty($costum))
			$where["source.keys"] = array('$in' => array($costum));
		if (!empty($userId))
			$where["user"] = $userId;
		if (!empty($cond)) {
			$where = array('$and' => array($where, $cond));
		}
		if (!empty($where))
			$lists = PHDB::find(Answer::COLLECTION, $where);
		return $lists;
	}

	public static function globalAutocomplete($form, $searchParams) {
		$searchParams["indexMin"] = (isset($searchParams["indexMin"])) ? $searchParams["indexMin"] : 0;
		$searchParams["indexStep"] = (isset($searchParams["indexStep"])) ? $searchParams["indexStep"] : 100;
		$mappingData = (isset($form["mapping"])) ? $form["mapping"] : array("name" => "name", "address" => "address", "answers.aapStep1.titre" => "answers.aapStep1.titre");
		$query = array();
		if ($searchParams["searchType"][0] == "answers" && empty($searchParams["filters"]["sousOrganisation"]))
			$query = array("form" => (string)@$form["_id"]);

		if (!empty($searchParams["filters"]["notSeen"]) && !empty($searchParams["filters"]["form"])) {
			$views = PHDB::find("views", array("form" => $searchParams["filters"]["form"], "user" => ['$ne' => Yii::app()->session['userId']]));
			$answersId = [];
			foreach ($views as $key => $value) {
				$answersId[] = new MongoId($value["parentId"]);
			}
			$query = SearchNew::addQuery($query, array("_id" => ['$nin' => $answersId]));
			unset($searchParams["filters"]["notSeen"]);
		}

		if (!empty($searchParams["filters"]["sousOrganisation"]) && !empty($searchParams["filters"]["form"])) {
			$prms = explode("-", $searchParams["filters"]["sousOrganisation"][0]);
			$prms[] = $searchParams["filters"]["form"];

			$query = array("form" => ['$in' => $prms]);
			unset($searchParams["filters"]["sousOrganisation"]);
			if (!empty($searchParams["filters"]["allSousOrganisation"]))
				unset($searchParams["filters"]["allSousOrganisation"]);
			unset($searchParams["filters"]["form"]);
		}

		if (!empty($searchParams["filters"]["allSousOrganisation"])) {
			$prms = explode("-", $searchParams["filters"]["allSousOrganisation"][0]);
			$prms[] = $searchParams["filters"]["form"];

			$query = array("form" => ['$in' => $prms]);
			unset($searchParams["filters"]["allSousOrganisation"]);
			unset($searchParams["filters"]["form"]);
		}


		if (!empty($searchParams["userId"]))
			$query = SearchNew::addQuery($query, array("user" => $searchParams["userId"]));

		if (!empty($searchParams["name"])) {
			if (!empty($searchParams["textPath"]))
				$query = SearchNew::searchText($searchParams["name"], $query, array("textPath" => $searchParams["textPath"]));
			else
				$query = SearchNew::searchText($searchParams["name"], $query);
		}
		if (!empty($searchParams["filters"]["address"])) {
			$pathToAddress = (isset($mappingData["address"]["path"])) ? $mappingData["address"]["path"] : $mappingData["address"];
			$searchRegExp = SearchNew::accentToRegex(trim(urldecode($searchParams["filters"]['address'])));
			$query = SearchNew::addQuery(
				$query,
				array(
					'$or' => array(
						array($pathToAddress . ".postalCode" => new MongoRegex("/.*{$searchRegExp}.*/i")),
						array($pathToAddress . ".name" => new MongoRegex("/.*{$searchRegExp}.*/i"))
					)
				)
			);
			unset($searchParams["filters"]["address"]);
		}

		if (
			!empty($searchParams["searchTags"]) &&
			(count($searchParams["searchTags"]) > 1  || count($searchParams["searchTags"]) == 1 && $searchParams["searchTags"][0] != "")
		) {
			$operator = (!empty($options) && isset($options["tags"]) && isset($options["tags"]["verb"])) ? $options["tags"]["verb"] : '$in';
			if (!empty($searchParams["tagsPath"])) {
				$query = SearchNew::searchTags($searchParams["searchTags"], $operator, $query, $searchParams["tagsPath"]);
				unset($searchParams["tagsPath"]);
			} else
				$query = SearchNew::searchTags($searchParams["searchTags"], $operator, $query);
			unset($searchParams["searchTags"]);
		}

		if (!empty($searchParams['filters'])) {
			$query = SearchNew::searchFilters($searchParams['filters'], $query);
		}

		if (!empty($searchParams['sortBy'])) {
			if (Api::isAssociativeArray($searchParams['sortBy'])) {
				$sortBy = array();
				foreach ($searchParams["sortBy"] as $key => $value) {
					$sortBy[$key] = (int)$value;
				}
				$searchParams["sortBy"] = $sortBy;
			} else {
				$sortBy = array();
				foreach ($searchParams["sortBy"] as $key => $value) {
					$sortBy[$value] = 1;
				}
				$searchParams["sortBy"] = $sortBy;
			}
		} else
			$searchParams['sortBy'] = array("updated" => -1);

		if (!isset($searchParams['fields']))
			$searchParams['fields'] = [];

		$res = array();

		$res["results"] = PHDB::findAndFieldsAndSortAndLimitAndIndex($searchParams["searchType"][0], $query, $searchParams["fields"], $searchParams['sortBy'], $searchParams["indexStep"], $searchParams["indexMin"]);
		if (isset($searchParams["count"]))
			$res["count"][$searchParams["searchType"][0]] = PHDB::count($searchParams["searchType"][0], $query);

		return $res;
	}

	public static function getMappingValues($mapping, $ans) {
		foreach ($mapping as $k => $path) {
			if (is_array($path) && isset($path["path"]))
				$path = $path["path"];
			$mapping[$k] = self::getValueByPath($path, $ans);
		}
		return $mapping;
	}
	public static function getValueByPath($indexes, $arrayToAccess) {
		if (is_string($indexes))
			$indexes = explode(".", $indexes);
		if (isset($arrayToAccess[$indexes[0]])) {
			if (count($indexes) > 1)
				return self::getValueByPath(array_slice($indexes, 1), $arrayToAccess[$indexes[0]]);
			else
				return $arrayToAccess[$indexes[0]];
		}
	}


	public static function getDataAnswers($answerList, $subForms, $form) {
		$globalLinks = [];
		$gUids = [];
		if (!empty($answerList)) {
			foreach ($answerList as $key => $ans) {
				if (isset($form["mapping"])) {
					$answerList[$key]["mappingValues"] = Answer::getMappingValues($form["mapping"], $ans);
				}
				$answerList[$key]["countComment"] = PHDB::count(Comment::COLLECTION, array("contextId" => $key, "contextType" => Form::ANSWER_COLLECTION));

				$localLinks = [];
				$uids = [];
				$todo = 0;
				$done = 0;
				$tasksPerson = [];
				if (!isset($ans["answers"])) {
					$percent = 0;
				} else {
					$totalInputs = 0;
					$answeredInputs = 0;
					foreach ($subForms as $fid => $f) {
						$totalInputs += count(@$f["inputs"]);
						if (isset($ans["answers"][$fid])) {
							$answeredInputs += count($ans["answers"][$fid]);
						}

						//todo lists are on depense for the moment
						//todo genereaclly not with a fixed input ID
						if (isset($ans["answers"][$fid]["depense"])) {
							foreach ($ans["answers"][$fid]["depense"]  as $ix => $dep) {
								if (isset($dep["todo"])) {
									foreach ($dep["todo"] as $ixx => $t) {
										if (!isset($t["done"]) || $t["done"] == "0") {
											$todo++;
											$whos = (is_array($t["who"])) ? $t["who"] : explode(",", $t["who"]);
											foreach ($whos as $whoix => $who) {
												if (!isset($tasksPerson[$who]))
													$tasksPerson[$who] = [];
												$tasksPerson[$who][] = $t["what"];
											}
										} else
											$done++;
									}
								}
							}
						}
					}
					if (isset($ans["links"])) {
						foreach ($ans["links"] as $type => $ls) {
							if (!isset($localLinks[$type]))
								$localLinks[$type] = [];

							if (!isset($globalLinks[$type]))
								$globalLinks[$type] = [];

							foreach ($ls as $uid => $time) {
								if (is_string($uid) && strlen($uid) == 24 && ctype_xdigit($uid)) {
									if (!in_array($uid, $localLinks[$type]))
										$localLinks[$type][] = $uid;
									if (!in_array($uid, $uids))
										$uids[] = new MongoId($uid);
									if (!in_array($uid, $globalLinks[$type]))
										$globalLinks[$type][] = $uid;
									if (!in_array($uid, $gUids))
										$gUids[] = new MongoId($uid);
								}
							}
						}
					}
					$percent = floor($answeredInputs * 100 / $totalInputs);
				}
				$answerList[$key]["percent"] = $percent;
				$answerList[$key]["uids"] = $uids;
				$answerList[$key]["todo"] = $todo;
				$answerList[$key]["done"] = $done;
				$answerList[$key]["tasksPerson"] = $tasksPerson;
				$answerList[$key]["canEdit"] = self::canEdit($ans, $form, Yii::app()->session["userId"]);
			}
		}
		return $answerList;
	}

	public static function csv($elements, $idElt, $valElt, $forms) {
		$elements[$idElt]["id"] = $idElt;
		$elements[$idElt]["user"] = $valElt["user"];
		$elements[$idElt]["created"] = date("d/m/Y", $valElt["created"]);
		if (!empty($valElt["answers"])) {
			foreach ($forms as $keyF => $valF) {
				if (!empty($valF["id"]) && !empty($valElt["answers"][$valF["id"]])) {
					foreach ($valElt["answers"][$valF["id"]] as $keyAns => $valAns) {
						if (!empty($valF["inputs"]) && !empty($valF["inputs"][$keyAns]) && !empty($valF["inputs"][$keyAns]["label"])) {
							$elements[$idElt][$valF["inputs"][$keyAns]["label"]] = $valAns;
						}
					}
				}
			}
		}
		return $elements;
	}

	public static $mailConfig = array(
		"validation" => ["tpl" => "answer.validation", "subject" => "La candidature {what} a été validée"],
		"rejected" => ["tpl" => "answer.rejected", "subject" => "La candidature {what} a été rejetée"],
		"default" => ["tpl" => "answer.default", "subject" => ""]
	);

	public static function getCommunityToNotify($answer, $entry = null) {
		$arrayMail = array();
		if (!empty($answer["links"])) {
			foreach ($answer["links"] as $k => $v) {
				if ($k == "answered" && (empty($entry) || in_array("answered", $entry))) {
					foreach ($v as $id) {
						$people = Element::getElementById($id, Person::COLLECTION, null, array("email", "name", "slug"));
						array_push($arrayMail, $people);
					}
				}
			}
			if (Costum::isSameFunction("getAnswerCommunityToNotify")) {
				// CARREFUL $costumUserArray need to be return as result : return $params["costumUserArray"]
				array_merge($arrayMail, Costum::sameFunction("getAnswerCommunityToNotify", array(
					"answer" => $answer
				)));
			}
		}
		return $arrayMail;
	}
	public static function mailProcess($id, $tpl, $step, $infos) {
		$costum = CacheHelper::getCostum();
		$answer = self::getById($id);
		$mailsCommunity = array();
		if (isset($infos["emails"]) && !empty($infos["emails"])) {
			foreach ($infos["emails"] as $v) {
				array_push($mailsCommunity, Element::getElementByWhere(Person::COLLECTION, array("email" => $v), array("email", "name", "slug")));
			}
		}
		if (isset($infos["community"]) && !empty($infos["community"])) {
			$mailsCommunity = array();
			foreach ($infos["community"] as $k => $v) {
				if ($v["type"] == Person::COLLECTION) {
					array_push($mailsCommunity, Element::getElementById($k, Person::COLLECTION, null, array("email", "name", "slug")));
				} else if (in_array($v["type"], [Organization::COLLECTION, Project::COLLECTION])) {
					$community = Element::getCommunityByTypeAndId($v["type"], $k, Person::COLLECTION, "isAdmin");
					foreach ($community as $e => $val) {
						array_push($mailsCommunity, Element::getElementById($e, Person::COLLECTION, null, array("email", "name", "slug")));
					}
				}
			}
		}
		if (empty($mailsCommunity))
			$mailsCommunity = self::getCommunityToNotify($answer);
		$answer["mappingValues"] = array();
		$form = Form::getByIdMongo($answer["form"]);
		if (isset($form["mapping"]))
			$answer["mappingValues"] = self::getMappingValues($form["mapping"], $answer);
		$nameAnsw = (isset($answer["mappingValues"]["name"])) ? $answer["mappingValues"]["name"] : "";
		$objMail = "[" . Mail::getAppName() . "] " . Yii::t("mail", self::$mailConfig[$tpl]["subject"], array("{what}" => $nameAnsw));
		if (isset($infos["tplObject"]))
			$objMail = "[" . Mail::getAppName() . "] " . $infos["tplObject"];
		foreach ($mailsCommunity as $c) {
			if (isset($c["email"])) {
				//$checkHasCostumConfig = self::$mailConfig[$tpl]["tpl"];
				$params = array(
					"type" => Cron::TYPE_MAIL,
					"tpl" => self::$mailConfig[$tpl]["tpl"],
					"subject" => $objMail,
					"from" => Yii::app()->params['adminEmail'],
					"to" => $c["email"],
					"tplParams" => array(
						"user" => $c,
						"title" => Mail::getAppName(),
						"answer" => $answer,
						"form" => $form,
						"msg" => @$infos
					)
				);
				if (isset($infos["msg"]))
					$params["tplParams"]["msg"] = $infos["msg"];
				if (isset($infos["html"]))
					$params["tplParams"]["html"] = $infos["html"];

				$params = Mail::getCustomMail($params);
				Mail::schedule($params);
			}
		}
		if (Costum::isSameFunction("AnswerMailProcess")) {
			// CARREFUL $costumUserArray need to be return as result : return $params["costumUserArray"]
			Costum::sameFunction(
				"AnswerMailProcess",
				array(
					"tpl" => $tpl,
					"step" => $step,
					"infos" => $infos,
					"form" => $form,
					"answer" => $answer
				)
			);
		}
	}

	public static function validate($params) {
		//Rest::json($params); exit;
		$costum = CacheHelper::getCostum();
		$set = array();
		$res = array('result' => false, "msg" => "Erreur");
		if (!empty($params["answerId"]))
			$answer = PHDB::findOneById(Answer::COLLECTION, $params["answerId"]);

		if (!empty($answer)) {
			//Rest::json($answer); exit;
			if (!empty($params["input"]["validated"]) && ($params["input"]["validated"] === true || $params["input"]["validated"] === "true"))
				$set["validated"] = true;

			if (!empty($params["input"]["generateElement"])) {
				$mapping = array();
				$import = array(
					'file' => array(json_encode(array($answer))),
					'nameFile' => "test",
					'typeFile' => 'json',
					"warnings" => "false"
				);
				//Rest::json($import); exit;
				foreach ($params["input"]["generateElement"] as $type => $map) {
					$mapping = array();
					foreach ($map as $k => $v) {
						$mapping[] = array(
							'idHeadCSV' => $v["from"],
							'valueAttributeElt' => $v["to"]
						);
					}
					$import['infoCreateData'] = $mapping;
					$import['typeElement'] = $type;
					//Rest::json($import); exit;
					$resImport = Import::previewData($import, true, true, true);
					return Rest::json($resImport);
					exit;
					foreach ($resImport["elementsObj"] as $keyElt => $elt) {
						$elt = Import::checkElement($elt, $type);
						if (!empty($elt["msgError"]))
							unset($elt["msgError"]);
						$elt["collection"] = $type;
						$elt["key"] = $type;
						if (Costum::isSameFunction("generateElementBeforeSave")) {
							$elt = Costum::sameFunction("generateElementBeforeSave", $elt);
						}
						$save = Element::save($elt);
						//Rest::json($save); exit;
						Link::connect($save["id"], $type, $params["answerId"], Answer::COLLECTION, Yii::app()->session["userId"], "answers", false, false, false, false);
						Link::connect($params["answerId"], Answer::COLLECTION, $save["id"], $type, Yii::app()->session["userId"], $type, false, false, false, false);
					}
					$res = array('result' => true, "msg" => "Valider");
					//Rest::json($resImport); exit;
				}
			}


			if (Costum::isSameFunction("answerValidate")) {
				$paramBeforeValidate = Costum::sameFunction("answerValidate", $params);
			}

			if (!empty($set)) {
				PHDB::update(
					Answer::COLLECTION,
					array("_id" => new MongoId((string)$params["answerId"])),
					array('$set' => $set)
				);

				$res = array('result' => true, "msg" => "Valider");
			}


			if ($res["result"] === true) {
				$res["html"] = $params["controller"]->renderPartial('survey.views.tpls.forms.finish', $res, true);
			}
		}

		return $res;
	}

	public static function is_true($val, $return_null = false) {
		$boolval = (is_string($val) ? filter_var($val, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) : (bool) $val);
		return ($boolval === null && !$return_null ? false : $boolval);
	}

	public static function GenerateProjectFromAnswer($name, $descr, $links = [], $financer = [], $parent, $action, $answer, $images = null) {
		if (is_string($answer))
			$answer = PHDB::findOneById(Form::ANSWER_COLLECTION, $answer);
		if (!empty($answer["project"]["id"])) {
			$project = PHDB::findOneById(Project::COLLECTION, (string)$answer["project"]["id"]);
			if (!empty($answer["project"]["room"]))
				$room = PHDB::findOneById(Room::COLLECTION, $answer["project"]["room"]);
			$p_id = "";
			$p_type = "";
			foreach ($parent as $keyp => $valuep) {
				if (isset($valuep["type"])) {
					$p_id = $keyp;
					$p_type = $valuep["type"];
				}
			}
			if (empty($room)) {
				$room = [
					"collection" => Room::COLLECTION,
					"name" => $name,
					"description" => $descr,
					"parentId" => (string)$p_id,
					"parentType" => $p_type,
					"status" => "open",
					"created" => time(),
					"creator" => Yii::app()->session["userId"]
				];
				Yii::app()->mongodb->selectCollection(Room::COLLECTION)->insert($room);
			}
		} else {
			$project = [
				"collection" => Project::COLLECTION,
				"name" => $name,
				"shortDescription" => $descr,
				"created" => time(),
				"properties" => [
					"avancement" => "idea"
				],
				"creator" => Yii::app()->session["userId"]
			];
			$p_id = "";
			$p_type = "";
			foreach ($parent as $keyp => $valuep) {
				if (isset($valuep["type"])) {
					$p_id = $keyp;
					$p_type = $valuep["type"];
				}
			}

			$room = [
				"collection" => Room::COLLECTION,
				"name" => $name,
				"description" => $descr,
				"parentId" => (string)$p_id,
				"parentType" => $p_type,
				"status" => "open",
				"created" => time(),
				"creator" => Yii::app()->session["userId"]
			];

			$project["parent"] = $parent;
			$image_fields = ['profilImageUrl', 'profilMarkerImageUrl', 'profilMediumImageUrl', 'profilThumbImageUrl'];
			foreach ($image_fields as $image_field) {
				if (!empty($images[$image_field]))
					$project[$image_field] = $images[$image_field];
			}
			Yii::app()->mongodb->selectCollection(Project::COLLECTION)->insert($project);
			$slugpro = Slug::checkAndCreateSlug($project["name"], Project::COLLECTION, (string)$project["_id"]);
			//var_dump($slug);
			Slug::save(Project::COLLECTION, (string)$project["_id"], $slugpro);
			$params["slug"] = $slugpro;
			Element::updateField(Project::COLLECTION, (string)$project["_id"], "slug", $slugpro);
			Yii::app()->mongodb->selectCollection(Room::COLLECTION)->insert($room);
		}
		$child = [];
		$child["childType"] = Project::COLLECTION;
		$child["childId"] = $project["_id"];
		Link::connectParentToChild($p_id, $p_type, $child, false, Yii::app()->session["userId"]);
		foreach ($links as $valuelink) {
			$child = [];
			$child["childType"] = Person::COLLECTION;
			$child["childId"] = $valuelink;
			Link::connectParentToChild($project["_id"], Project::COLLECTION, $child, false, Yii::app()->session["userId"], "");
		}
		foreach ($financer as $valuef) {
			$child = [];
			$child["childType"] = Organization::COLLECTION;
			$child["childId"] = $valuef;
			Link::connectParentToChild($project["_id"], Project::COLLECTION, $child, false, Yii::app()->session["userId"], ["Financeur"]);
		}
		//add action
		if (!empty($answer["project"]["id"])) {
			foreach ($action as $nkey => $nvalue) {
				if (isset($nvalue["actionid"])) {
					array_splice($action, $nkey, 1);
				}
			}
		}
		foreach ($action as $avalue) {
			$action = [
				"name" => @$avalue["name"],
				"credits" => @$avalue["credits"],
				"parentId" => (string)$project["_id"],
				"parentType" => Project::COLLECTION,
				"answerId" => (string)$answer["_id"],
				"status" => "todo",
				"idUserAuthor" => Yii::app()->session["userId"],
				"idParentRoom" => (string)$room["_id"],
				"options" => [
					"creditAddPorteur" => false,
					"creditSharePorteur" => false,
					"possibleStartActionBeforeStartDate" => false
				],
				"max" => 1,
				"min" => 1,
				"noStartDate" => true,
			];

			Yii::app()->mongodb->selectCollection(Action::COLLECTION)->insert($action);

			if (isset($avalue["depenseKey"]) && isset($action["_id"])) {
				Yii::app()->mongodb->selectCollection(Answer::COLLECTION)->update(
					["_id" => $answer['_id']],
					['$set' => ['answers.aapStep1.depense.' . $avalue["depenseKey"] . '.actionid' => (string)$action["_id"]]]
				);
			}
			$paramsaction = [
				"id" => $action["_id"]
			];

			$paramsaction["child"] = [
				"id" => $avalue["user"],
				"type" => Person::COLLECTION
			];

			ActionRoom::assignPeople($paramsaction);
		}
		return ["projet" => (string)$project["_id"], "room" => (string)$room["_id"]];
	}

	public static function ResultVoteMajoritaire($parentForm, $formId, $key, $paramsData = []) {
		$allFormAnswers = PHDB::find(Form::ANSWER_COLLECTION, array("form" => (string)$parentForm['_id'], "updated" => ['$exists' => 1]));
		$reslutView = array();
		$nbVote = 0;
		foreach ($allFormAnswers as $elem) {
			if (isset($elem['answers'][$formId][$key])) {
				if (count($elem['answers'][$formId][$key])) {
					foreach ($elem['answers'][$formId][$key] as $index => $item) {
						if (isset($reslutView[$item['propos']])) {
							$keyFound = array_search($item['niveau'], array_column(json_decode(json_encode($reslutView[$item['propos']]), TRUE), 'niveau'));
							if ($keyFound > -1) {
								$reslutView[$item['propos']][$keyFound]['count']++;
							} else {
								array_push($reslutView[$item['propos']], array('niveau' => $item['niveau'], 'count' => 1));
							}
						} else {
							$reslutView[$item['propos']][0] = array('niveau' => $item['niveau'], 'count' => 1);
						}
					}
				}
			}
			$nbVote++;
		}

		$htmlResult = '
		<div class="col-xs-12">';

		if (count($reslutView)) {
			$htmlResult .= '
				<div class="col-xs-12 p-0 card" style="width: 100%; min-width: 550px">
					<div class="card-header">
						<h4>Profil de mérites</h4>
					</div>
					<div class="card-body">
						<div class="">
							<table class="profiles table-merite">';
			foreach ($paramsData['list'] as $index => $item) {
				if ($index == 0) {
					$htmlResult .= '
											<thead>
                                            	<tr>
                                                	<th></th>';
					foreach ($paramsData['mentions'] as $i => $mentItem) {
						$htmlResult .= '
																		<th>
																			<small class="tab-mieuxvoter n' . $i . ' badge">' . $mentItem . '</small>
																		</th>';
					}
					$htmlResult .= '</tr>
											</thead>
											<tbody>';
				}
				$htmlResult .= '<tr>
												<th>' . $item . '</th>';
				if (isset($reslutView[$item])) {
					foreach ($paramsData['mentions'] as $idMent => $mentItem) {
						$indexFound = array_search($idMent, array_column(json_decode(json_encode($reslutView[$item]), TRUE), 'niveau'));
						if ($indexFound > -1) {
							$htmlResult .= '
																		<td>
                                                                            ' . round(($reslutView[$item][$indexFound]['count'] * 100) / $nbVote, 2, PHP_ROUND_HALF_EVEN) . '
                                                                            %
                                                                        </td>';
						} else {
							$htmlResult .= '<td>0%</td>';
						}
					}
				}
				$htmlResult .= '</tr>';
			}
			$htmlResult .= '</tbody>
							</table>
						</div>
					</div>     
						<span class="ml-1 text-nowrap">Nombre de votes: <span class="text-bold">' . $nbVote . '</span></span>
					</div>';
		}
		$htmlResult .= '
		</div>';

		return $htmlResult;
	}

	public static function ResultCoDate($parentForm, $coDateKunik, $inputKey, $inputValue, $paramsData = [], $isAdmin = false) {
		$allFormAnswers = PHDB::find(Form::ANSWER_COLLECTION, array("form" => (string)$parentForm['_id'], "updated" => ['$exists' => 1]), ['answers', 'user', 'updated']);
		$reslutView = array();
		$nbVote = 0;
		$userConcerned = null;
		$bodyUsersAnswers = '';
		$tabViewAllResult = '<table class="codate table-merite" style="margin-bottom:0; width: unset;"><thead><tr><th rowspan="2"></th>';
		$trSubBlock = '';
		$bodyBlock = '';
		$iterationCount = 0;
		$footerBlock = '';
		$maxChoose = 0;
		$ownAnswerBlock = '';
		$valideVoters = 0;
		$label = $inputValue['label'];
		if (count($allFormAnswers) > 0) {
			foreach ($allFormAnswers as $iter => $elem) {
				if ($iterationCount == 0) {
					foreach ($paramsData['alldates'] as $indexDateParam => $itemDateParam) {
						$tabViewAllResult .= '
							<th colspan="' . count($itemDateParam) . '">' . $indexDateParam . '</th>
						';
						foreach ($itemDateParam as $itemIter => $timeValue) {
							$colUnikId = uniqid();
							$createEventBtn = $isAdmin ? '<a href="javascript:;" id="' . $colUnikId . '" class="createEvent" data-id="' . new MongoId() . '" data-formid="' . (string)$parentForm['_id'] . '" data-date="' . $indexDateParam . '" data-time="' . $timeValue . '" data-titleQuestion="' . $label . '" data-eventduration="' . $paramsData["eventduration"] . '"><i class="fa fa-calendar"><small> Create event</small></i></a>' : '';
							$trSubBlock .= '
							<th>' . $timeValue . '<br>' . $createEventBtn . '</th>
						';
						}
					}
				}
				if (isset($elem['updated'])) {
					$valideVoters++;
					$userConcerned = PHDB::findOneById(Citoyen::COLLECTION, $elem['user'], ['name', 'username', "roles"]);
					if (isset($userConcerned['name'])) {
						if (isset($userConcerned["roles"])) {
							if (isset($userConcerned["roles"]["tobeactivated"])) {
								isset($elem["name"]) ? $bodyBlock .= '<tr><th class="bg-name">' . $elem['name'] . '</th>' : $bodyBlock .= '<tr><th class="bg-name">' . $userConcerned['name'] . '</th>';
							} else $bodyBlock .= '<tr><th class="bg-name">' . $userConcerned['name'] . '</th>';
						} else {
							$bodyBlock .= '<tr><th class="bg-name">' . $userConcerned['name'] . '</th>';
						}
					} else   $bodyBlock .= '<tr><th class="bg-name">' . 'Not logged' . '</th>';

					foreach ($paramsData['alldates'] as $indexKey => $item) {
						// if($iterationCount == 0) {
						//     $tabViewAllResult .= '
						//             <th colspan="'.count($item).'">'.$indexKey.'</th>
						//         ';
						// }
						foreach ($item as $itemIter => $timeValue) {
							$colUnikId = uniqid();

							if ($iterationCount == 0) {
								// $createEventBtn = '<a href="javascript:;" id="'.$colUnikId.'" class="createEvent" data-id="'.new MongoId().'" data-date="'.$indexKey.'" data-time="'.$timeValue.'" data-titleQuestion="'.$label.'"><i class="fa fa-calendar"></i></a>';
								// $trSubBlock .= '
								//     <th>'.$timeValue.'</th>
								// ';
								$reslutView[$indexKey][$timeValue] = 0;
							}
							$valChecked = '';
							if (isset($elem['answers'][$inputValue['subFormId']][$inputKey])) {
								if (isset($elem['answers'][$inputValue['subFormId']][$inputKey][$indexKey])) {
									if (array_search($timeValue, $elem['answers'][$inputValue['subFormId']][$inputKey][$indexKey]) > -1) {
										$valChecked =  "checked";
										if (isset($reslutView[$indexKey][$timeValue])) $reslutView[$indexKey][$timeValue]++;
										else $reslutView[$indexKey][$timeValue] = 1;
										if ($reslutView[$indexKey][$timeValue] > $maxChoose) $maxChoose = $reslutView[$indexKey][$timeValue];
									}
								}
							}
							$bodyBlock .= '
							<td>
							<input type="checkbox" id="cbr_' . $colUnikId . '" name="codateR' . $coDateKunik . '" class="animate-check" disabled ' . $valChecked . ' />
								<label for="cbr_' . $colUnikId . '" class="check-box"></label> 
							</td>
						';
						}
					}
					$bodyBlock .= '
					</tr>';
				}
				if (count($allFormAnswers) - 1 == $iterationCount) {
					foreach ($paramsData['alldates'] as $indexKey => $item) {
						foreach ($item as $itemIter => $timeValue) {
							$classColMax = '';
							$maxIcon = '';
							if (isset($reslutView[$indexKey])) {
								if (isset($reslutView[$indexKey][$timeValue])) {
									if ($reslutView[$indexKey][$timeValue] == $maxChoose && $maxChoose != 0) {
										$classColMax = 'max-votant';
										$maxIcon = '<i class="fa fa-star"></i>';
										// $maxChoose = 99999999999;
									}
									$footerBlock .= '
										<td class="' . $classColMax . '" >' . $maxIcon . ' ' . $reslutView[$indexKey][$timeValue] . '</td>
									';
								} else {
									$footerBlock .= '
										<td class="fix-width ' . $classColMax . '" > 0</td>
									';
								}
							} else {
								$footerBlock .= '
									<td class="fix-width ' . $classColMax . '" > 0</td>
								';
							}
						}
					}
				}
				$iterationCount++;
			}
		}
		return $tabViewAllResult .= '
			</tr>
			<tr class="fs">
				' . $trSubBlock . '
			</tr>
			</thead>
			<tbody class="fix-width">
				' . $bodyBlock . '
			</tbody>
			<tfoot class="fix-width">
				<tr>
					<th class="bg-name">' . Yii::t("common", "Total voters ") . $valideVoters . '</th>
					' . $footerBlock . '
				</tr>
			</tfoot>
			</table>
		';
	}

	public static function ResultScore($controller, $parentForm, $show) {
		$allFormAnswers = PHDB::find(Form::ANSWER_COLLECTION, array("form" => (string)$parentForm['_id'], "updated" => ['$exists' => 1]), ['answers', 'user', 'updated']);
		return $controller->renderPartial("costum.views.custom.appelAProjet.callForProjects.scoreTable", array("allAnswers" => $allFormAnswers, "show" => $show));
	}

	public function sortByPosOrder($a, $b) {
		if (isset($a['positions']) && isset($b['positions']))
			return (int)@$a['positions'][(string)$this->parentFo['_id']] - (int)@$b['positions'][(string)$this->parentFo['_id']];
	}
}
