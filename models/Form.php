<?php

use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;

class Form
{
	const COLLECTION = "forms";
	const CONTROLLER = "forms";
	const ANSWER_COLLECTION = "answers";
	const INPUTS_COLLECTION = "inputs";
    const PAYMENTCONFIG_COLLECTION = "paymentconfigs";

	const ANSWER_CONTROLLER = "answer";
	const ICON = "fa-list-alt";
	const ICON_ANSWER = "fa-calendar-check-o";

	public static $dataBinding = array(
		"id" => array("name" => "id"),
		"type" => array("name" => "type"),
		"name" => array("name" => "name"),
		"inputs" => array("name" => "inputs"),
		"template" => array("name" => "template"),
		"collection" => array("name" => "collection"),
		"what" => array("name" => "what"),
		"tpl" => array("name" => "tpl"),
		"form" => array("name" => "form"),
		"image" => array("name" => "image"),
		"formid" => array("name" => "formid"),
		"preferences" => array("name" => "preferences"),
		"description" => array("name" => "description"),
		"parent" => array("name" => "parent"),
		"media" => array("name" => "media"),
		"tags" => array("name" => "tags"),
		"shortDescription" => array("name" => "shortDescription"),
		"modified" => array("name" => "modified"),
		"source" => array("name" => "source"),
		"updated" => array("name" => "updated"),
		"creator" => array("name" => "creator"),
		"created" => array("name" => "created"),
		"isSpecific" => array("name" => "isSpecific"),
		"subForm" => array("name" => "subForm"),
		"payement" => array("name" => "payement")
	);

	public static $riskWeight = array(
		"11" => array("w" => 1, "c" => "lightGreen"),
		"12" => array("w" => 2, "c" => "lightGreen"),
		"13" => array("w" => 3, "c" => "lightGreen"),
		"14" => array("w" => 4, "c" => "orange"),
		"21" => array("w" => 5, "c" => "lightGreen"),
		"22" => array("w" => 6, "c" => "lightGreen"),
		"23" => array("w" => 7, "c" => "orange"),
		"24" => array("w" => 8, "c" => "red"),
		"31" => array("w" => 9, "c" => "lightGreen"),
		"32" => array("w" => 10, "c" => "orange"),
		"33" => array("w" => 11, "c" => "red"),
		"34" => array("w" => 12, "c" => "red"),
		"41" => array("w" => 13, "c" => "orange"),
		"42" => array("w" => 14, "c" => "red"),
		"43" => array("w" => 15, "c" => "red"),
		"44" => array("w" => 16, "c" => "red")
	);

	public static $cplx = [
		"multiRadio",
		"multiCheckboxPlus",
		"calendar",
		"partner",
		"budget",
		"financement",
		"financementFromBudget",
		"suiviFromBudget",
		"indicateurs",
		"element",
		"stepValidation",
		"validateStep",
		"generatePDF",
		"address",
		"regle",
		"multiCheckbox",
		"evaluation",
		"evaluationEditable",
		"listing",
		"subList",
		"list",
		"multiList",
		"openDynform",
		"simpleTable",
		"rangeTwoSliders",
		"multitextvalidation",
		"radioNew",
		"typeEtDroitAdhesion",
		"onLinePayement",
		"checkboxNew",
		"categorizedCheckbox",
		"finder",
		"searchExist",
		"framadate",
		"mieuxVoter",
	];

	public static $coformconfiguration = [
		"configform" => [
			"titre" => [
				"label" => "Titre",
				"type" => "text",
				"path" => "",
				"elementId" => "",
				"elementType" => "",
				"configType" => ""
			]
		]
	];

	public static function ocecoTools()
	{
		return array(
			array(
				"id" => "evaluation",
				"label" => "Evaluation",
				"path" => "tpls.forms.evaluation.evaluation",
				"logo" => Yii::app()->getModule("co2")->assetsUrl . '/images/ocecotools/evaluation.png',
				"defaultParams" => []
			),
			array(
				"id" => "sondageDate",
				"label" => Yii::t("common", "Survey by date"),
				"path" => "tpls.forms.cplx.sondageDate",
				"logo" => Yii::app()->getModule("co2")->assetsUrl . '/images/ocecotools/Calendrier.png',
				"defaultParams" => [],
			),
			array(
				"id" => "mieuxVoter",
				"label" => Yii::t("common", "Majority vote"),
				"path" => "tpls.forms.cplx.mieuxVoter",
				"logo" => Yii::app()->getModule("co2")->assetsUrl . '/images/ocecotools/vote_majoritaire.png'
			),
			array(
				"id" => "happinessCalculator",
				"label" => Yii::t("common", "Happiness evaluation"),
				"path" => "tpls.forms.evaluation.happinessCalculator",
				"logo" => Yii::app()->getModule("co2")->assetsUrl . '/images/ocecotools/Calculateur bonheur.png'
			),
			array(
				"id" => "timeEvaluation",
				"label" => "Calculateur de temps",
				"path" => "tpls.forms.evaluation.evaluation",
				"logo" => Yii::app()->getModule("co2")->assetsUrl . '/images/ocecotools/Calculateur temps.png',
				"defaultParams" => []
			),
			array(
				"id" => "evaluationEditable",
				"label" => "Evaluation ariane",
				"path" => "tpls.forms.cplx.evaluationEditable",
				"logo" => Yii::app()->getModule("co2")->assetsUrl . '/images/ocecotools/evaluation_ariane.jpg',
				"defaultParams" => []
			),
			array(
				"id" => "createNewForm",
				"label" => Yii::t("common", "New form"),
				"path" => "",
				"logo" => Yii::app()->getModule("co2")->assetsUrl . '/images/logo-coform.png',
				"defaultParams" => []
			),
			array(
				"id" => "createNewOcecoForm",
				"label" => Yii::t("common", "New oceco"),
				"path" => "",
				"logo" => Yii::app()->getModule("co2")->assetsUrl . '/images/oceco.png',
				"defaultParams" => []
			),
			[
				"id" => "createCoevent",
				"label" => Yii::t("common", "New coevent"),
				"path" => "",
				"logo" => Yii::app()->getModule("co2")->assetsUrl . "/images/oceco.png",
				"defaultParams" => []
			],
			[
				"id" => "createAac",
				"label" => "Creér un appel à commun",
				"path" => "",
				"logo" => Yii::app()->getModule("co2")->assetsUrl . "/images/logo-coform.png",
				"defaultParams" => []
			]
		);
	}

	public static function ocecoToolsItems()
	{
		return [
			"createNewForm" => array(
				"id" => "createNewForm",
				"label" => Yii::t("common", "New form"),
				"path" => "",
				"logo" => Yii::app()->getModule("co2")->assetsUrl . '/images/logo-coform.png',
				"defaultParams" => []
			),
			"createNewOcecoForm" => array(
				"id" => "createNewOcecoForm",
				"label" => Yii::t("common", "New oceco"),
				"path" => "",
				"logo" => Yii::app()->getModule("co2")->assetsUrl . '/images/oceco.png',
				"defaultParams" => []
			),
			"createAac" => array(
				"id" => "createAac",
				"label" => "Creér un appel à commun",
				"path" => "",
				"logo" => Yii::app()->getModule("co2")->assetsUrl . '/images/logo-coform.png',
				"defaultParams" => []
			),

		];
	}

	public static function inputTypes()
	{
		return $inputTypes = [
			"text" => Yii::t("survey", "Text : short text"),
			"textarea" => Yii::t("survey", "Textarea : Large text area"),
			"date" => Yii::t("survey", "Date : Select a date"),
			"number" => Yii::t("survey", "Choose a number"),
			"tpls.forms.cplx.radioNew" => Yii::t("survey", "Radio button: Single choice"),
			"tpls.forms.cplx.checkboxNew" => Yii::t("survey", "Checkbox : Multiple choice"),
			"tpls.forms.cplx.categorizedCheckbox" => Yii::t("survey", "Checkbox : Categorized choice"),
			"tpls.forms.cplx.multiRadio" => Yii::t("survey", "Radio with additional and conditional options"),
			"tpls.forms.cplx.multiCheckboxPlus" => Yii::t("survey", "Checkbox with additional and conditional options"),
			"tpls.forms.select" => Yii::t("survey", "Select : add a choice list"),
			"tpls.forms.cplx.address" => Yii::t("survey", "Geocode an address"),
			"tpls.forms.uploader" => Yii::t("survey", "File(s)"),
			"tpls.forms.emailUser" => Yii::t("survey", "Respondent's e-mail address"),
			"tpls.forms.cplx.multitextvalidation" => Yii::t("survey", "Text: short text Configurable"),
			"tpls.forms.tags" => Yii::t("survey", "Tags : Add tags"),
			"tpls.forms.aap.invite" => Yii::t("common", "Add contributor"),
			"tpls.forms.sectionTitle" => Yii::t("survey", "Section Separator with Title"),
			"tpls.forms.sectionDescription" => Yii::t("survey", "Section separator with description"),
			"tpls.forms.evaluation.commonTableV2" => Yii::t("survey", "Digital tools table"),
			"tpls.forms.evaluation.commonTable" => Yii::t("survey", "Common table"),
			"tpls.forms.cplx.finder" => Yii::t("survey", "Configurable finder"),
			"tpls.forms.cplx.video" => Yii::t("survey", "Video"),
			"color" => Yii::t("survey", "Color : Select a color"),
			"month" => Yii::t("survey", "Choose a month"),
			"week" => Yii::t("survey", "Week : Choose a week"),
			"time" => Yii::t("survey", "Time : Add a time"),
			"email" => Yii::t("survey", "Fill out an e-mail"),
			"tel" => Yii::t("survey", "Tel : Add a Phone"),
			"url" => Yii::t("survey", "Url : Add an internet link"),
			"tpls.forms.cplx.rangeTwoSliders" => Yii::t("survey", "Interval (Ex: age range)"),
			"tpls.forms.cplx.element" => Yii::t("survey", "Create an item Person , Organization, Project,..."),
			"tpls.forms.cplx.searchExist" => Yii::t("survey", "Text field with search for existing"),
			"tpls.forms.cplx.scoreInput" => Yii::t("survey", "Input that displays the total score"),
			"tpls.forms.cplx.scoreCheckboxInput" => Yii::t("survey", "Input that displays the number of checked boxes (checkbox)"),
			"tpls.forms.evaluation.evaluation" => Yii::t("survey", "Evaluation table"),
			"tpls.forms.evaluation.happinessCalculator" => Yii::t("survey", "Happiness Calculator"),
			"tpls.forms.cplx.mieuxVoter" => Yii::t("survey", "Majority Voting"),
			"tpls.forms.cplx.sondageDate" => Yii::t("survey", "Date polling"),
			"tpls.forms.cplx.listing" => Yii::t("survey", "Liste : input Listing"),
			// "tpls.forms.cplx.repondants" => Yii::t("survey", "List of respondents"),
			"tpls.forms.cplx.simpleTable" => Yii::t("survey", "Simple two-entry table"),
			"tpls.forms.cplx.calendar" => Yii::t("survey", "List of key actions with dates"),
			"tpls.forms.cplx.indicateurs" => Yii::t("survey", "List of Indicator"),
			"tpls.forms.cplx.validateStep" => Yii::t("survey", "Input Validation and confirmation via email"),
			"tpls.forms.cplx.typeEtDroitAdhesion" => Yii::t("survey", "Payment with mollie, Amount by type"),
			"tpls.forms.cplx.onLinePayement" => Yii::t("survey", "Payment with mollie"),
			"tpls.forms.cplx.evaluationEditable" => Yii::t("survey", "Multiple quadrant (Ex. evaluation)") . ' V0.2',
			//"button"    => Yii::t("survey", "button"),
			// "tpls.forms.checkbox"  => Yii::t("survey", "Checkbox : Checkbox"),
			// "datetime-local" => Yii::t("survey", "Datetime-local : Select a date and time"),
			//"image" 	=> Yii::t("survey", "Ajouter une image"),
			//"radio" 	=> Yii::t("survey", "radio : Choix unique"),
			//"range" 	=> Yii::t("survey", "range : "),
			// "tpls.forms.cplx.partner" => Yii::t("survey", "The partners and their mutual commitments"),
			// "tpls.forms.ocecoform.budget"	=> Yii::t("survey", "List of expenses"),
			// "tpls.forms.cplx.financement" => Yii::t("survey", "List of Financing"),
			// "tpls.forms.ocecoform.budget" => Yii::t("survey", "Oceco expense line"),
			// "tpls.forms.ocecoform.financementFromBudget"=> Yii::t("survey", "Oceco financing line"),
			// "tpls.forms.cplx.financementFromBudget" => Yii::t("survey", "List of Financing re-using budgets"),
			// "tpls.forms.cplx.suiviFromBudget" => Yii::t("survey", "Follow-up of works based on budgets"),
			// "tpls.forms.cplx.stepValidation" => Yii::t("survey", "Step Validation by Roles"),
			// "tpls.forms.cplx.generatePDF" => Yii::t("survey", "Generate PDF"),
			// "tpls.forms.cplx.regle" => Yii::t("survey", "Rule of attribution"),
			// "tpls.forms.cplx.multiCheckbox" => Yii::t("survey", "List of checkboxes"),
			// "tpls.forms.cplx.evaluation" => Yii::t("survey", "Multiple quadrant (Ex. evaluation)"),
			// "tpls.forms.cplx.subList" 		=> Yii::t("survey", "Sublist : sublist"),
			// "tpls.forms.cplx.list" 		=> Yii::t("survey", "Liste : liste"),
			// "tpls.forms.cplx.multiList" 		=> Yii::t("survey", "MultiList : multiList"),
			// "tpls.forms.cplx.openDynform" => Yii::t("survey", "Table or list of answers (Open Dynamic Form)"),
		];
	}

	public static $coformQuestionIcons = [
		"text" => "text.png",
		"textarea" => "textarea.png",
		"tpls.forms.checkbox" => "icones_checkbox.png",
		"color" => "icones_color picker.png",
		"email" => "icones_email.png",
		"tpls.forms.cplx.address" => "icones_geocode.png",
		"url" => "icones_lien.png",
		"tpls.forms.cplx.calendar" => "icones_list actions date.png",
		"tpls.forms.cplx.multiCheckboxPlus" => "icones_liste choix.png",
		"tpls.forms.cplx.multiCheckbox" => "icones_liste choix.png",
		"tpls.forms.cplx.checkboxNew" => "icones_liste choix.png",
		"tpls.forms.cplx.categorizedCheckbox" => "icones_liste choix.png",
		"tpls.forms.cplx.financement" => "icones_liste financement.png",
		"tpls.forms.ocecoform.budget" => "icones_listes dépenses.png",
		"tpls.forms.cplx.multiRadio" => "icones_radio button.png",
		"tpls.forms.cplx.radioNew" => "icones_radio button.png",
		"tpls.forms.tags" => "icones_tags.png",
		"tel" => "icones_telephone.png",
		"tpls.forms.emailUser" => "icones_email.png",
		"date" => "simple_input.png",
		"date" => "simple_input.png",
		"datetime-local" => "simple_input.png",
		"month" => "simple_input.png",
		"week" => "simple_input.png",
		"time" => "simple_input.png",
	];

	public static $coformDesc = [
		"text" => "c’est un input de type texte, avec lequel on peut saisir des textes courtes",
	];

	public static $dynformInputTypes = [
		"text" => "texte simple",
		"textarea" => "textarea",
		"markdown" => "markdown",
		"finder" => "finder",

		//"wysiwyg" => "wysiwyg",
		//"checkbox" => "checkbox",
		"checkboxSimple" => "checkboxSimple",
		"radio" => "radio",

		"select" => "select",
		"selectMultiple" => "selectMultiple",

		"formLocality" => "formLocality",
		"location" => "location",

		"uploader" => "uploader",
		//"folder" => "folder",

		"lists" => "lists",
		"tags" => "tags",
		//2 "scope" => "scope",

		//"password" => "password",
		//"openingHours" => "openingHours",
		"hidden" => "hidden",
		//"timestamp" => "timestamp",
		"colorpicker" => "colorpicker",
		"date" => "date",
		"datetime" => "datetime",
		"time" => "time",
		//"link" => "link",
		//"captcha" => "captcha",
		//"tagList" => "tagList",

		//2 "postalcode" => "postalcode",
		// "arrayForm" => "arrayForm",
		"array" => "array",
		"properties" => "properties"
	];

	/*
   [X] author view pod
   [X] add element
     [X] create new element
     [X] limit to fixed number of elements
     [X] connect existing one
     [X] connect with the answer in an afterSave
     [X] multiple element block in page with different types
   [X] text [[~d/modules/costum/views/tpls/forms/text.php]]
   [X] select [[~d/modules/costum/views/tpls/forms/select.php]]
     * TODO : dynform add key value question for list / or value list
   [X] tags  [[~d/modules/costum/views/tpls/forms/text.php]]
     * BUG design
     * TODO : dynform add key value question for list / or value list
   [X] calendar :oneByone: SAve [[~d/modules/costum/views/tpls/forms/cplx/calendar.php]]
   * BUG cannot have 2 calendars in the same time
   [X] multi checkbox  : multicheckbox
   [ ] finder
     [ ] communityList
   * link to openForm and connect to answer
   [ ] open select
     * new options can be added to the list ,
     * other peoples answers become options,
     * or my old options become answers
   [ ] wysiwyg
   [ ] markdown
   [ ] locality
   [ ] image
   [ ] tagList
   [ ] postalcode
   [ ] arrayForm
   [ ] array
   [ ] properties
    */

	public static function save($id, $data)
	{
		try
		{
			$step = $data["formId"];
			unset($data["formId"]);
			unset($data["parentSurvey"]);
			$data["created"] = time();
			return PHDB::update(
				self::ANSWER_COLLECTION,
				array("_id" => new MongoId((string)$id)),
				array('$set' => array("answers." . $step => $data))
			);
		}
		catch (CTKException $e)
		{
			return $e->getMessage();
		}
	}

	// TODO : COMMENT AND TO DELETE [FUNCTION MIGRER DANS LE MODEL ANSWER.PHP]
	/*public static function delAnswer($id)
    {
		try {
			Document::removeAllForElement($id, self::ANSWER_COLLECTION);
			return PHDB::remove( self::ANSWER_COLLECTION,
                    array( "_id" => new MongoId((string)$id)));
		} catch (CTKException $e){
   			return $e->getMessage();
  		}
    }*/

	public static function canBeDeleted($idform)
	{
		$form = PHDB::find(Form::COLLECTION, ['$and' => [["config" => $idform], ['$or' => [["type" => "templatechild"], ["type" => "aap"]]]]]);
		if (!empty($form) && sizeof($form) >= 2)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	public static function getTemplateChildAdmins($idtemplate)
	{
		$forms = PHDB::find(Form::COLLECTION, ['$and' => [["config" => $idtemplate], ['$or' => [["type" => "templatechild"], ["type" => "aap"]]]]]);
		$communuty = [];
		foreach ($forms as $formid => $form)
		{
			$communutychild = Element::getCommunityByParentTypeAndId($form["parent"]);
			return $communutychild;
		}
	}

	public static function countStep($idParent)
	{
		return PHDB::count(self::COLLECTION, array("parentForm" => $idParent));
	}

	public static function getBySourceAndId($source = null, $id = null)
	{
		$res = array();
		if (!empty($source))
		{
			$queryForm = array("source.keys" => array('$in' => [$source]));
		}
		if (!empty($id))
		{
			$queryForm = array("_id" => new MongoId($id));
		}
		$res["form"] = PHDB::findOne(Form::COLLECTION, $queryForm);
		$res["forms"] = [];
		if (!empty($res["form"]["subForms"]))
		{
			foreach ($res["form"]["subForms"] as $ix => $formId)
			{
				$f = PHDB::findOne(Form::COLLECTION, ["id" => $formId]);
				$res["forms"][$formId] = $f;
			}
		}
		return $res;
	}

	public static function getById($parentForm, $fields = array())
	{
		return PHDB::findOne(self::COLLECTION, array("id" => $parentForm), $fields);
	}

	public static function getByIdMongo($id, $fields = array())
	{
		return PHDB::findOneById(self::COLLECTION, $id, $fields);
	}


	public static function getAnswerById($id, $fields = array())
	{
		return PHDB::findOneById(self::ANSWER_COLLECTION, $id, $fields);
	}

	public static function getLinksById($id)
	{
		return self::getByIdMongo($id, array("links"));
	}

	public static function getLinksFormsByFormId($id, $type = "all", $role = null)
	{
		$res = array();

		$form = self::getLinksById($id);

		if (empty($form))
		{
			throw new CTKException(Yii::t("form", "The form id is unkown : contact your admin"));
		}
		if (isset($form) && isset($form["links"]) && isset($form["links"]["Form"]))
		{
			$members = array();
			foreach ($form["links"]["Form"] as $key => $member)
			{
				if (!@$member["toBeValidated"] && !@$member["isInviting"])
					$members[$key] = $member;
			}
			//No filter needed
			if ($type == "all")
			{
				return $members;
			}
			else
			{
				foreach ($form["links"]["Form"] as $key => $member)
				{
					if ($member['type'] == $type)
					{
						if (!empty($role) && @$member[$role] == true)
						{

							if ($role == "isAdmin")
							{
								if (!@$member["isAdminPending"] && !@$member["toBeValidated"] && !@$member["isInviting"] && $member["isAdmin"] == true)
									$res[$key] = $member;
							}
							else
							{
								$res[$key] = $member;
							}
						}
						else if (empty($role) && !@$member["toBeValidated"] && !@$member["isInviting"])
						{
							$res[$key] = $member;
						}
					}
				}
			}
		}
		return $res;
	}
	// TODO OCEATOON : DEPRACTED !?
	/*	public static function listForAdminNews($form, $answers = array() ){
            $results = array();
            $uniq = array();
            $uniqO = array();
            $uniqP = array();
            $uniqE = array();

            $scenario = array();


            foreach ( $form["scenario"] as $key => $value) {
                $scenario[$key] = false;
            }

            //Rest::json($answers);exit ;
            foreach ( $answers as $key => $value) {
                if(empty($results[ $value["user"] ]))
                    $results[ $value["user"] ] = array("userId" => $value["user"]);

                if( !empty($value["answers"]) &&
                    !empty($value["answers"][Organization::CONTROLLER]) &&
                    !in_array( $value["answers"][Organization::CONTROLLER]["id"], $uniqO )  &&
                    ( 	empty($results[$value["user"]]) ||
                        (!empty($results[$value["user"]]) && empty($results[$value["user"]]["parentId"]) ) ) ) {

                        $orga = Element::getElementById($value["answers"][Organization::CONTROLLER]["id"], Organization::COLLECTION, null, array("name", "email"));
                        $ans["parentId"] = $value["answers"][Organization::CONTROLLER]["id"];
                        $ans["parentType"] = Organization::COLLECTION;
                        $ans["parentName"] = $orga["name"];
                        $ans["userId"] = $value["user"];
                        $results[$value["user"]] = $ans;

                    $uniqO[] = $value["answers"][Organization::CONTROLLER]["id"];
                }

                if( !empty($value["answers"]) &&
                    !empty($value["answers"][Project::CONTROLLER]) &&
                    !in_array( $value["answers"][Project::CONTROLLER]["id"], $uniqP ) ){

                    $orga = Element::getElementById($value["answers"][Project::CONTROLLER]["id"], Project::COLLECTION, null, array("name", "email", "shortDescription", "shortDescription"));
                    $orga["id"] = $value["answers"][Project::CONTROLLER]["id"];
                    $orga["type"] = Project::COLLECTION;

                    if(!empty($value["answers"][Project::CONTROLLER]["parentId"])){
                        $orga["parentId"] = $value["answers"][Project::CONTROLLER]["parentId"];
                        $orga["parentType"] = Element::getCollectionByControler($value["answers"][Project::CONTROLLER]["parentType"]);
                        $parent = Element::getSimpleByTypeAndId($orga["parentType"], $orga["parentId"]);
                        $orga["parentName"] = $parent["name"];
                    }else{
                        $answersParent = PHDB::findOne( Form::ANSWER_COLLECTION ,
                                            array("parentSurvey"=>@$value["parentSurvey"],
                                                    "answers.organization" => array('$exists' => 1),
                                                    "user" => $value["user"]) );

                        if( !empty($value["answers"]) &&
                            !empty($value["answers"][Organization::CONTROLLER]) &&
                            !in_array( $value["answers"][Organization::CONTROLLER]["id"], $uniqO )  &&
                            ( 	empty($results[$value["user"]]) ||
                                (!empty($results[$value["user"]]) && empty($results[$value["user"]]["parentId"]) ) ) ) {

                                $orga = Element::getElementById($value["answers"][Organization::CONTROLLER]["id"], Organization::COLLECTION, null, array("name", "email"));
                                $ans["parentId"] = $value["answers"][Organization::CONTROLLER]["id"];
                                $ans["parentType"] = Organization::COLLECTION;
                                $ans["parentName"] = $orga["name"];
                                $ans["userId"] = $value["user"];
                                $results[$value["user"]] = $ans;

                            $uniqO[] = $value["answers"][Organization::CONTROLLER]["id"];
                        }

                        if( !empty($value["answers"]) &&
                            !empty($value["answers"][Project::CONTROLLER]) &&
                            !in_array( $value["answers"][Project::CONTROLLER]["id"], $uniqP ) ){

                            $orga = Element::getElementById($value["answers"][Project::CONTROLLER]["id"], Project::COLLECTION, null, array("name", "email"));
                            $orga["id"] = $value["answers"][Project::CONTROLLER]["id"];
                            $orga["type"] = Project::COLLECTION;

                            if(!empty($value["answers"][Project::CONTROLLER]["parentId"])){
                                $orga["parentId"] = $value["answers"][Project::CONTROLLER]["parentId"];
                                $orga["parentType"] = Element::getCollectionByControler($value["answers"][Project::CONTROLLER]["parentType"]);
                                $parent = Element::getSimpleByTypeAndId($orga["parentType"], $orga["parentId"]);
                                $orga["parentName"] = $parent["name"];
                            }else{
                                $answersParent = PHDB::findOne( Form::ANSWER_COLLECTION ,
                                                    array("parentSurvey"=>@$value["parentSurvey"],
                                                            "answers.organization" => array('$exists' => 1),
                                                            "user" => $value["user"]) );

                                $orga["parentId"] = $answersParent["answers"][Organization::CONTROLLER]["id"];
                                $orga["parentType"] = Organization::COLLECTION;
                                $orga["parentName"] = $answersParent["answers"][Organization::CONTROLLER]["name"];
                            }

                            $orga["userId"] = $value["user"];
                            $orga["userName"] = $value["name"];

                            $results[ $value["user"] ]["id"] = @$orga["id"];
                            $results[ $value["user"] ]["type"] = @$orga["type"];
                            $results[ $value["user"] ]["name"] = @$orga["name"];
                            $results[ $value["user"] ]["email"] = @$orga["email"];
                            $results[ $value["user"] ]["parentId"] = @$orga["parentType"];
                            $results[ $value["user"] ]["parentName"] = @$orga["parentName"];
                            $results[ $value["user"] ]["userId"] = @$orga["userId"];
                            $results[ $value["user"] ]["userName"] = @$orga["userName"];

                            $uniqP[] = $value["answers"][Project::CONTROLLER]["id"];
                        }


                        if ( !empty($results[$value["user"]]) ) {
                            if ( empty($results[$value["user"]]["scenario"]) )
                                $results[$value["user"]]["scenario"] = $scenario;
                            //var_dump($results[$value["user"]]); echo "</br></br>";
                            if ( isset($results[$value["user"]]["scenario"][$value["formId"]]) )
                                $results[$value["user"]]["scenario"][$value["formId"]] = true;
                        }

                    }

                    $orga["userId"] = $value["user"];
                    $orga["userName"] = $value["name"];

                    $results[ $value["user"] ]["id"] = @$orga["id"];
                    $results[ $value["user"]]["type"] = @$orga["type"];
                    $results[ $value["user"]]["name"] = @$orga["name"];
                    $results[ $value["user"]]["email"] = @$orga["email"];
                    $results[ $value["user"]]["parentId"] = @$orga["parentType"];
                    $results[ $value["user"]]["parentName"] = @$orga["parentName"];
                    $results[ $value["user"]]["userId"] = @$orga["userId"];
                    $results[ $value["user"]]["userName"] = @$orga["userName"];

                    if(!empty($orga["shortDescription"]) )
                        $results[ $value["user"]]["desc"] = $orga["shortDescription"];
                    else if(!empty($orga["description"]) )
                            $results[ $value["user"]]["desc"] = $orga["description"];

                    $uniqP[] = $value["answers"][Project::CONTROLLER]["id"];
                }

                //var_dump($value["name"]);echo "<br/>";
                if ( !empty($results[$value["user"]]) ) {

                    if ( empty($results[$value["user"]]["scenario"]) )
                        $results[$value["user"]]["scenario"] = $scenario;

                    if ( isset( $results[$value["user"]]["scenario"][$value["formId"]] ) )
                        $results[$value["user"]]["scenario"][$value["formId"]] = true;
                }
            }
            // exit;
            // Rest::json($results);exit ;
            return $results ;
        }*/

	//rebuild answerList
	// by  adding organizing organization (id,type) and project (id,type)
	public static function listForAdmin($answers)
	{
		//Rest::json($answers); exit ;
		$uniq = array();
		$res = array();
		if (!empty($answers))
		{
			foreach ($answers as $key => $value)
			{
				$new = $value;
				if (@$value["answers"])
				{
					foreach ($value["answers"] as $keyA => $valA)
					{


						if (!empty($valA["answers"][Organization::CONTROLLER]) && !empty($valA["answers"][Organization::CONTROLLER]["id"]))
						{
							$orga = Element::getElementById($valA["answers"][Organization::CONTROLLER]["id"], Organization::COLLECTION, null, array("name", "email", "shortDescription"));
							$orga["id"] = $valA["answers"][Organization::CONTROLLER]["id"];
							$orga["type"] = Organization::COLLECTION;
							$new[Organization::CONTROLLER] = $orga;
						}

						if (!empty($valA["answers"][Project::CONTROLLER]) && !empty($valA["answers"][Project::CONTROLLER]["id"]))
						{
							$project = Element::getElementById($valA["answers"][Project::CONTROLLER]["id"], Project::COLLECTION, null, array("name", "email", "shortDescription"));
							$project["id"] = $valA["answers"][Project::CONTROLLER]["id"];
							$project["type"] = Project::COLLECTION;
							$new[Project::CONTROLLER] = $project;
						}
					}
				}
				$res[$key] = $new;
			}
		}

		//Rest::json($res); exit ;
		return $res;
	}

	public static function canAdmin($id, $form = array())
	{
		if (empty($form) && @$id)
			$form = PHDB::findOneById(Form::COLLECTION, $id);

		$res = false;
		if (!empty(Yii::app()->session["userId"]))
		{
			if (
				Yii::app()->session["userId"] == @$form["author"] ||
				(!empty($form["links"]["members"][Yii::app()->session["userId"]]) &&
					!empty($form["links"]["members"][Yii::app()->session["userId"]]["isAdmin"]) &&
					$form["links"]["members"][Yii::app()->session["userId"]]["isAdmin"] == true /*&&
					 !empty($form["links"]["members"][Yii::app()->session["userId"]]["roles"]) &&
					in_array("TCO", $form["links"]["members"][Yii::app()->session["userId"]]["roles"]) */)
				||
				(!empty($form["links"]["contributors"][Yii::app()->session["userId"]]) &&
					!empty($form["links"]["contributors"][Yii::app()->session["userId"]]["isAdmin"]) &&
					$form["links"]["contributors"][Yii::app()->session["userId"]]["isAdmin"] == true)
			)
			{
				$res = true;
			}
			else if (Authorisation::isInterfaceAdmin())
			{
				$res = true;
			}
			else if (Authorisation::isParentAdmin($id, Form::COLLECTION, Yii::app()->session["userId"], $form))
			{
				$res = true;
			}
		}

		return $res;
	}

	public static function canAdminRoles($id, $role, $form = array())
	{
		if (empty($form))
			$form = PHDB::findOneById(Form::COLLECTION, $id);

		$res = false;
		if (
			!empty($form["links"]) &&
			!empty($form["links"]["members"]) &&
			!empty($form["links"]["members"][Yii::app()->session["userId"]]) &&
			!empty($form["links"]["members"][Yii::app()->session["userId"]]["isAdmin"]) &&
			$form["links"]["members"][Yii::app()->session["userId"]]["isAdmin"] == true &&
			!empty($form["links"]["members"][Yii::app()->session["userId"]]["roles"]) &&
			in_array($role, $form["links"]["members"][Yii::app()->session["userId"]]["roles"])
		)
		{
			$res = true;
		}
		else if (Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"])))
		{
			$res = true;
		}
		//Rest::json($res); exit ;
		return $res;
	}

	public static function canSuperAdmin($id, $session, $form = array(), $formAdmin = array())
	{
		if (empty($form))
			$form = PHDB::findOne(Form::COLLECTION, array("id" => $id));

		if (empty($formAdmin))
			$formAdmin = PHDB::findOne(Form::COLLECTION, array("id" => $id . "Admin", "session" => $session));

		if (@$formAdmin["adminRole"])
			$res = self::canAdminRoles((string)$form["_id"], $formAdmin["adminRole"], $form);
		else
			$res = false;
		return $res;
	}

	public static function updatePriorisation($params)
	{

		$res = Link::removeRole($params["contextId"], $params["contextType"], $params["childId"], $params["childType"], @$params["roles"], Yii::app()->session['userId'], $params["connectType"]);

		$answer = PHDB::findOneById(self::ANSWER_COLLECTION, $params["answer"]);
		$roles = explode(",", $params["roles"]);
		$pourcentage = round(100 / count($roles), 2);
		$categories = array();
		$priorisation = array();

		foreach ($roles as $key => $value)
		{
			$slug = InflectorHelper::slugify($value);
			if (!empty($answer["categories"][$slug]))
			{
				$categories[$slug] = $answer["categories"][$slug];
				if (!empty($answer["answers"]["priorisation"][$slug]))
					$priorisation[$slug] = $answer["answers"]["priorisation"][$slug];
			}
			else
			{
				$categories[$slug] = array("name" => $value, "pourcentage" => $pourcentage);
			}
		}

		PHDB::update(
			self::ANSWER_COLLECTION,
			array("_id" => new MongoId($params["answer"])),
			array('$set' => array("categories" => $categories, "answers.priorisation" => $priorisation))
		);

		return $res;
	}

	public static function isFinish($endDate)
	{
		$res = false;
		$today = date(DateTime::ISO8601, strtotime("now"));
		if (!empty($endDate))
		{
			$endDate = date(DateTime::ISO8601, $endDate->sec);
			if ($endDate < $today)
				$res = true;
		}
		return $res;
	}

	public static function notOpen($d)
	{
		$res = false;
		$today = date(DateTime::ISO8601, strtotime("now"));
		if (!empty($d))
		{
			$d = date(DateTime::ISO8601, $d->sec);
			if ($d > $today)
				$res = true;
		}
		return $res;
	}

	public static function createNotificationAnswer($comment)
	{
		$answer = Form::getAnswerById($comment["contextId"]);
		$cter = Slug::getElementBySlug($answer["cterSlug"]);
		$cter = $cter["el"];
		$projectName = (isset($answer["answers"]["action"]["project"][0]["name"])) ? $answer["answers"]["action"]["project"][0]["name"] . " " : "";

		if ($answer["user"] == Yii::app()->session["userId"])
		{
			//Notify admin and if answer

			$tplObject = "[CTE] Un candidat a laissé un message";
			$messages = "<p>un message à été envoyer sur le projet : " . $projectName . ":</p>";
			//$messages="<p>".Yii::app()->session["user"]["name"]." a envoyé un message sur son projet ".$projectName.":</p>";
		}
		else
		{
			$tplObject = "[CTE] Vous avez reçu un message";
			$messages = "<p>" . Yii::app()->session["user"]["name"] . " a envoyé un message sur votre projet " . $projectName . ":</p>";
			$mails = [];
			if (isset($cter["links"]["members"]))
			{
				foreach ($cter["links"]["members"] as $key => $v)
				{
					if (@$v["isAdmin"] && $key != Yii::app()->session["userId"])
					{
						$email = Person::getEmailById($key);
						array_push($mails, $email["email"]);
					}
				}
			}
			//$mails=[$answer["email"]];
		}
		$messages .= "<br/><br/><p style='padding:10px 20px;margin:1%;border:1px solid lightgray; font-style:italic; border-radius:10px; width:90%;white-space: pre-line;'>" . $comment["text"] . "</p>" .
			"<br/><br/><div style='text-align:center'><a href='" . Yii::app()->getRequest()->getBaseUrl(true) . (isset(Yii::app()->session["custom"]["url"]) ? Yii::app()->session["custom"]["url"] : null) . "' target='_blank' style='padding:7px 10px; border-radius:5px; background-color:#00b795;color:white;font-weight:800;font-variant:small-caps;'>Répondre</a></div>";
		$params = array(
			'formId' => $answer["formId"],
			'session' => @$answer["session"],
			'answerId' => (string)$answer["_id"],
			'answerUser' => @$answer["name"],
			"tpl" => "eligibilite",
			"tplObject" => $tplObject,
			"messages" => $messages,
			"tplMail" => ""
		);
		if (!empty($mails))
		{
			foreach ($mails as $email)
			{
				$params["tplMail"] = $email;
				Mail::createAndSend($params);
			}
		}
	}

	public static function generate($id, $copy, $pId, $pType)
	{
		$res = null;
		$copyEl = PHDB::findOne(Form::COLLECTION, array("id" => $copy));
		$checkExist = PHDB::findOne(Form::COLLECTION, array("id" => $id));
		if (!Person::logguedAndValid())
		{
			$res = array("render" => "co2.views.default.unTpl", "msg" => Yii::t("common", "Please Login First"), "icon" => "fa-sign-in");
		}
		else if ($checkExist)
		{
			$res = array("msg" => Yii::t("common", "Success"));
		}
		else if ($copyEl)
		{
			unset($copyEl["_id"]);
			unset($copyEl["modified"]);
			unset($copyEl["created"]);
			unset($copyEl["links"]);
			$copyEl["id"] = $id;
			$copyEl["copied"] = $copy;
			// reuse and not duplicate scenarios and dynform definitions
			$copyEl["scenario"] = "db.forms.id." . $copy . ".scenario";

			//parent elemnet information
			$copyEl["parentId"] = $pId;
			$copyEl["parentType"] = $pType;

			$el = Slug::getElementBySlug($id);
			if (!empty($el["el"]["profilThumbImageUrl"]))
				$copyEl['custom']["logo"] = $el["el"]["profilThumbImageUrl"];

			$copyEl['title'] = $el["el"]["name"];
			$copyEl['collection'] = self::COLLECTION;
			$copyEl["created"] = time();
			$copyEl["updated"] = time();
			$copyEl["modified"] = new MongoDate(time());

			// var_dump($copyEl);
			// exit;
			Yii::app()->mongodb->selectCollection(Form::COLLECTION)->insert($copyEl);
			PHDB::update(
				$pType,
				array("_id" => new MongoId($pId)),
				array('$set' => array("links." . self::COLLECTION . "." . (string)$copyEl["_id"] => array("type" => self::COLLECTION, "copyForm" => $copy)))
			);
			$res = array("msg" => Yii::t("common", "Success"));
		}
		else
			$res = array("render" => "co2.views.default.unTpl", "msg" => Yii::t("project", "Source Copy doesn't exist."), "icon" => "fa-lock");
		return $res;
	}

	public static function canAccess($rules, $formParents = null)
	{
		//WARNING la c'est ce qui est lier à l'user du costum connecté
		if (Authorisation::isInterfaceAdmin())
			return true;
		if (!empty($formParents))
		{
			foreach ($formParents as $k => $v)
			{
				if (Link::hasRoles($k, $v["type"], $rules["roles"]))
					return true;
			}
		}
		else
		{
			$costum = CacheHelper::getCostum();
			if (
				isset($rules["roles"])
				&& isset(Yii::app()->session["costum"][$costum["slug"]]["hasRoles"])
				&& !empty(array_intersect(Yii::app()->session["costum"][$costum["slug"]]["hasRoles"], $rules["roles"]))
			)
				return true;
		}
		return false;
	}

	public static function getDataForm($params)
	{
		if (isset($_GET["form"]) || !empty($params["form"]))
		{

			if (!empty($params["form"]))
				$form = $params["form"];
			else
				$form = PHDB::findOneById(Form::COLLECTION, $_GET["form"]);

			$params["parentForm"] = $form;
			$communityLinks = array();
			if (!empty($form["parent"]))
			{
				foreach ($form["parent"] as $key => $value)
				{
					$cl = Element::getCommunityByTypeAndId($value["type"], $key);
					if (!empty($cl))
						$communityLinks = array_merge($communityLinks, $cl);
				}
			}
			$params["parentForm"]["communityLinks"] = $communityLinks;
			if (!isset($params["el"]) && !empty($form) && isset($form["parent"]))
			{
				$params["el"] = self::getFirstParentForm($form);
				$allforms = [];
				if (isset($form) && !empty($form["subForms"]))
				{

					if (!is_array($form["subForms"]))
					{
						$params["formId"] = implode("|", $form["subForms"]);
					}
					else
					{
						$params["formId"] = array_keys($form["subForms"]);
					}

					//$params = self::getFormData($params);
					if (isset($form["rules"]))
					{
						foreach ($form["subForms"] as $k => $value)
						{
							if (isset($form["rules"][$value]))
							{
								if (!self::canAccess($form["rules"][$value], $form["parent"]))
								{
									array_splice($params["parentForm"]["subForms"], $k, -1);
								}
							}
						}
					}
					$budgetInputList = [];

					$stockBudgetInputs = false;
					if (!empty($form["subForms"]))
					{
						foreach ($form["subForms"] as $i => $formId)
						{
							$f = PHDB::findOne(Form::COLLECTION, ["id" => $formId]);
							$allforms[$formId] = $f;
							if (isset($f["inputs"]))
							{
								foreach ($f["inputs"] as $key => $inp)
								{
									if (in_array($inp["type"], ["tpls.forms.cplx.budget"]))
										$budgetInputList[$formId . "." . $key] = $formId . "." . $key;
									if (in_array($inp["type"], [
										"tpls.forms.cplx.tpls.forms.cplx.financementFromBudget",
										"tpls.forms.cplx.tpls.forms.cplx.suiviFromBudget",
										"tpls.forms.cplx.tpls.forms.cplx.decideFromBudget"
									]))
										$stockBudgetInputs = true;
								}
							}
						}
					}
					if (count($budgetInputList))
						Yii::app()->session["budgetInputList"] = $budgetInputList;
				}
				$params["forms"] = $allforms;
				$params["showForm"] = true;
			}
		}
		return $params;
	}

	public static function getParamsForm($id, $form, $renderStep = null, $renderInput = null, $mode = "w", $project = null, $include = false)
	{
		$params = [
			//state
			"isNew" => false,
			"isAap" => false,
			"isCoremu" => false,
			"isSeasonal" => false,
			"wizard" => true,

			//element
			"parentForm" => [],
			"configForm" => [],
			"projects" => [],
			"actions" => [],
			"steps" => [],
			"userData" => [],
			"userAnswers" => [],
			"answer" => [],
			"context" => [],
			"contextConfig" => [],
			"accountBadge" => [],

			//links
			"parentCommunity" => [],
			"communityOrgaGroup" => [],
			"communityCitoyenGroup" => [],
			"parentProjects" => [],
			"poiList" => [],

			//view tpls
			"tpl" => "survey.views.tpls.forms.newFormWizard",
			"tplStandalone" => "survey.views.tpls.forms.standalone.newFormWizard",
			"tplCustom" => "",
			"tplSlide" => "survey.views.tpls.forms.coformslideview.coformslideview",
			"tplOffline" => "survey.views.tpls.forms.offlineWizard",
			"tplFeedback" => "survey.views.tpls.forms.feedback.formWizard",
			"tplconnexionmode" => "survey.views.default.newconnexionmode",
			"tplIdnotFound" => "co2.views.default.unTpl",
			"msg" => "",

			//access
			"canSeeAnswer" => false,
			"canEdit" => false,
			"canAdminAnswer" => false,
			"canEditForm" => false,
			"canAdminConfig" => false,
			"isSuperAdmin" => Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]),
		];

		if (!empty($renderInput))
		{
			$params["tplCustom"] = "survey.views.tpls.forms.custom." . $renderInput;
		}

		if (!empty($id) && empty($project) && $id != "new")
		{
            if($renderStep == "aapStep3" && $renderInput == "financer" && $include){
                $params["answer"] = Aap::filterdepense($id,null);
            }else {
                $params["answer"] = PHDB::findOneById(Form::ANSWER_COLLECTION, $id);
            }
		}
		elseif (!empty($project) && $id != "new")
		{
			$params["answer"] = PHDB::findOne(Form::ANSWER_COLLECTION, ["project.id" => $project]);
		}

		if (isset($form) && !empty($form))
			$params["parentForm"] = PHDB::findOneById(Form::COLLECTION, $form);
		if (!empty($form) && !empty(Yii::app()->session["userId"]))
		{
			if (isset($form["session"]) && $form["session"] == true && isset($form["sessionMapping"]) && isset($form["actualSession"]))
			{
				$params["userAnswers"] = PHDB::find(Form::ANSWER_COLLECTION, array("form" => (string)$params["parentForm"]["_id"], "user" => Yii::app()->session["userId"], $params["parentForm"]["sessionMapping"] => $params["parentForm"]["actualSession"]));
			}
			else
			{
				$params["userAnswers"] = PHDB::find(Form::ANSWER_COLLECTION, array("form" => (string)$params["parentForm"]["_id"], "user" => Yii::app()->session["userId"]));
			}
		}

		if (empty($form))
		{
			$form = $params["answer"]["form"];
		}
		if (empty($params["parentForm"]))
		{
			if (empty($params["answer"]["form"]))
			{
				$params["parentForm"] = PHDB::findOneById(Form::COLLECTION, $form);
			}
			else
			{
				$params["parentForm"] = PHDB::findOneById(Form::COLLECTION, $params["answer"]["form"]);
			}
		}

		if ($id == "new")
		{
			$params["isNew"] = true;
		}
		else
		{
			$params["isNew"] = false;

			if (empty($form))
			{
				$form = $params["answer"]["form"];
			}
			if (empty($params["parentForm"]))
			{
				if (empty($params["userAnswers"]))
				{
					$params["userAnswers"] = PHDB::findAndSort(Form::ANSWER_COLLECTION, array("user" => Yii::app()->session["userId"], "form" => $params["answer"]["form"]), array("created" => -1));
				}
			}
			if (!empty($params["answer"]["project"]["id"]))
			{
				$params["projects"] = PHDB::findOneById(Project::COLLECTION, $params["answer"]["project"]["id"]);
				/*$params["actions"] = PHDB::find(Actions::COLLECTION, array('$or' =>[
                    array("parentId" => (string) $params["projects"]["_id"] , "parentType" => Project::COLLECTION),
                    array("answerId" => (string) $params["answer"]["_id"])
                ]));*/
			}
			if (!empty($params["answer"]["user"]))
			{
				$params["userData"] = PHDB::findOneById(Citoyen::COLLECTION, $params["answer"]["user"]);
			}
		}

		$contextId = array_keys($params["parentForm"]["parent"])[0];
		$contextType = $params["parentForm"]["parent"][$contextId]["type"];
		if (isset($params["parentForm"]["type"]) && $params["parentForm"]["type"] == "aap")
		{
			$params["isAap"] = true;
			$params["steps"] = PHDB::find(Form::INPUTS_COLLECTION, array("formParent" => $form));
			$params["configForm"] = PHDB::findOneById(Form::COLLECTION, $params["parentForm"]["config"]);

			foreach ($params["steps"] as $stepId => $step)
			{
				$params["steps"][$stepId]["name"] = $params["configForm"]["subForms"][$step["step"]]["name"];
				$params["steps"][$stepId]["id"] = $step["step"];
				if (isset($step["inputs"])) {
					$params["steps"][$stepId]["hasMultiEval"] = self::hasMultiEval($step["inputs"]);
				}
				if (!empty($renderStep) && $step["step"] == $renderStep)
				{
					$params["step"] = $params["steps"][$stepId];
					$params["step"]["id"] = $renderStep;
					$params["step"]["name"] = $params["configForm"]["subForms"][$step["step"]]["name"];
				}
			}
		}
		else
		{
			$params["mongoidsteps"] = PHDB::find(Form::COLLECTION, array("id" => array('$in' => $params["parentForm"]["subForms"])));
			foreach ($params["mongoidsteps"] as $mongoid => $step)
			{
				$params["steps"][$step["id"]] = $step;
			}
			if (!empty($renderStep))
			{
				$rslstep = PHDB::find(Form::COLLECTION, array("id" => $renderStep));
				if (isset(array_values($rslstep)[0]["inputs"]))
				{
					$params["step"] = array_values($rslstep)[0];
					$params["step"]["step"] = $renderStep;
				}
				else
				{
					$params["step"] = $renderStep;
					$params["step"]["step"] = $renderStep;
				}
			}
		}

		if ($id == "new" && $params["isNew"] == true && isset($params["parentForm"]["aapType"]) && $params["parentForm"]["aapType"] == "aac") {
			$params["myOthersCommuns"] = [];
			if (isset($params["parentForm"]["_id"])) {
				$queryParams = Aap::myOthersCommunsQuery([
					"currentForm" => (string) $params["parentForm"]["_id"]
				]);
				$searchParams = $queryParams["searchParams"];
				$res["results"] = PHDB::findAndFieldsAndSortAndLimitAndIndex(
					Answer::COLLECTION,
					$queryParams["query"],
					$searchParams["fields"],
					$searchParams['sortBy'],
					$searchParams["indexStep"],
					$searchParams["indexMin"]
				);
				$params["myOthersCommuns"] = Aap::parseCommunData($res)["results"];
			}
		}

		if (!empty($params["parentForm"]["tplformWizard"]))
		{
			$params["tplformWizard"] = $params["parentForm"]["tplformWizard"];
		}

		if (!empty($params["parentForm"]["tpl"]) && $renderStep == null)
		{
			$params["tpl"] = $params["parentForm"]["tpl"];
		}

		if (!empty($params["parentForm"]["tplformconnexionmode"]))
		{
			$params["tplconnexionmode"] = $params["parentForm"]["tplformconnexionmode"];
		}

		if (!empty($params["parentForm"]["tplSlide"]))
		{
			$params["tplSlide"] = $params["parentForm"]["tplSlide"];
		}

		if (!empty($contextId) && !empty($contextType))
		{
			$params["context"] = PHDB::findOneById($contextType, $contextId);
			if ($params["isAap"] && $params["parentForm"]["parent"] == $params["configForm"]["parent"])
			{
				$params["contextConfig"] = $params["context"];
			}
			elseif ($params["isAap"])
			{
				$contextConfigId = array_keys($params["configForm"]["parent"])[0];
				$params["contextConfig"] = PHDB::findOneById($params["configForm"]["parent"][$contextConfigId]["type"], $contextConfigId);
			}
			if ($params["parentForm"]["parent"][array_key_first($params["parentForm"]["parent"])]["type"] != "citoyens")
			{
				$params["parentCommunity"] = Element::getCommunityByParentTypeAndId($params["parentForm"]["parent"]);
				$params["communityOrgaGroup"] = Link::groupFindByType(Organization::COLLECTION, $params["parentCommunity"]);
				$params["communityCitoyenGroup"] = isset($params["context"]["links"]["members"]) ? Link::groupFindByType(Citoyen::COLLECTION, $params["context"]["links"]["members"]) : [];
			}
			if (!empty($params["context"]["links"]["projects"]))
			{
				$projectKeys = array_keys($params["context"]["links"]["projects"]);
				$projectKeys = array_map(function ($v)
				{
					return new MongoId($v);
				}, $projectKeys);

				$params["parentProjects"] = PHDB::find(Project::COLLECTION, array("_id" => array('$in' => $projectKeys)), ["name"]);
			}
		}

		if (!empty($contextType) && !empty($contextId))
		{
			$poiList = PHDB::find(
				Poi::COLLECTION,
				array(
					"parent." . $contextId => array('$exists' => 1),
					"parent." . $contextId . ".type" => $contextType,
					"type" => "cms"
				)
			);
		}

		if (!empty(Yii::app()->session["userId"]))
		{
			if (
				$params["isSuperAdmin"] ||
				(!empty($params["context"]["links"]["members"][Yii::app()->session["userId"]]["isAdmin"]) && $params["context"]["links"]["members"][Yii::app()->session["userId"]]["isAdmin"] == true)
			)
			{
				$params["canAdminAnswer"] = true;
				/*if($params["mode"] == "fa") {
                    $params["canEditForm"] = true;
                }*/
			}

			if (
				$params["isSuperAdmin"] ||
				(!empty($params["contextConfig"]["links"]["members"][Yii::app()->session["userId"]]["isAdmin"]) && $params["contextConfig"]["links"]["members"][Yii::app()->session["userId"]]["isAdmin"] == true)
			)
			{
				$params["canAdminConfig"] = true;
			}

			if (!empty($params["answer"]))
			{
				$params["canEdit"] = self::getAnswerAuthorisation($params["answer"], $params["parentForm"], $params["context"])["canSeeAnswer"];
				$params["canSeeAnswer"] = self::getAnswerAuthorisation($params["answer"], $params["parentForm"], $params["context"])["canSeeAnswer"];
			}
			else
			{
				$params["canEdit"] = self::getAnswerAuthorisation("new", $params["parentForm"], $params["context"])["canSeeAnswer"];
				$params["canSeeAnswer"] = self::getAnswerAuthorisation("new", $params["parentForm"], $params["context"])["canSeeAnswer"];
			}
			$params["canEdit"] = self::getAnswerAuthorisation($params["answer"], $params["parentForm"], $params["context"])["canSeeAnswer"];

			$params["canSeeAnswer"] = self::getAnswerAuthorisation($params["answer"], $params["parentForm"], $params["context"])["canSeeAnswer"];
		}
		if (!empty($renderInput))
		{
			$params["input"] = $params["step"]["inputs"][$renderInput];
			$params["input"]["tpl"] = "";
			$params["input"]["id"] = $renderInput;
			if (in_array($params["input"]["type"], ["textarea", "markdown", "wysiwyg"]))
			{
				$params["input"]["tpl"] = "tpls.forms.textarea";
			}
			else if (empty($params["input"]["type"]) || in_array($params["input"]["type"], ["text", "button", "color", "date", "datetime-local", "email", "image", "month", "number", "radio", "range", "tel", "time", "url", "week", "tags", "hidden"]))
			{
				$params["input"]["tpl"] = "tpls.forms.text";
			}
			else if (in_array("multiDecide", explode(".", $params["input"]["type"])) && isset($params["parentForm"]["inputConfig"]["multiDecide"]))
			{
				$kunikDecide = explode(".", $params["parentForm"]["inputConfig"]["multiDecide"]);
				$kunikDecideName = $kunikDecide[count($kunikDecide) - 1];
				$params["step"]["inputs"][$kunikDecideName] = $params["input"];
				$params["input"]["tpl"] = $params["parentForm"]["inputConfig"]["multiDecide"];
				$params["input"]["border"] = false;
				$params["step"]["inputs"][$kunikDecideName] = $params["steps"][$renderStep]["inputs"][$kunikDecideName] = $params["input"];
			}
			else
			{
				$params["input"]["tpl"] = $params["input"]["type"];
			}

			if (stripos($params["input"]["type"], "tpls.forms.cplx") !== false)
			{
				$params["saveOneByOne"] = false;
			}
			else
			{
				$params["saveOneByOne"] = true;
			}
			$kunikT = explode(".", @$params["input"]["type"]);
			$params["keyTpl"] = (count($kunikT) > 1) ? $kunikT[count($kunikT) - 1] : @$params["input"]["type"];
			$params["kunik"] = $params["keyTpl"] . $renderInput;
			$params["formId"] = $params["step"]["id"];
			$params["form"] = $params["step"];
			$params["key"] = $renderInput;
			$params["type"] = $params["input"]["type"];
			$params["label"] = @$params["input"]["label"];
			$params["info"] = @$params["input"]["info"];
			$params["inpClass"] = " ";
			$params["placeholder"] = @$params["input"]["placeholder"];
			$params["titleColor"] = (isset($params["parentForm"]["params"]["colors"]["title"])) ? $params["parentForm"]["params"]["colors"]["title"] : "#3f4e58";
			$params["answerPath"] = "answers." . $params["formId"] . "." . $params["key"] . ".";
			isset($params["answer"]["_id"]) ? $answer = PHDB::findOneById(self::ANSWER_COLLECTION, $params["answer"]["_id"]) : "";
			if (!empty($answer["answers"][$params["formId"]][$params["key"]]))
			{
				$params["answers"] = $answer["answers"][$params["formId"]][$params["key"]];
			}
			else
			{
				$params["answers"] = null;
			}
			$commentCount = 0;
            if($mode == "w" && isset($input["activeComments"]) && $input["activeComments"] == true) {
                $where = array("path" => (isset($params["form"]["formParent"]) ? $params["form"]["formParent"] : ""). "." .(isset($params["form"]["_id"]) ? (string) $params["form"]["_id"] : ""). '.' . (isset($params["answer"]["_id"]) ? (string) $params["answer"]["_id"] : "") . '.' .($params["step"]["inputs"][$renderInput]).".question_".($params["step"]["inputs"][$renderInput]).".comment");
                $commentCount = PHDB::count(Comment::COLLECTION, $where);
            }
			$params["editQuestionBtn"] = isset($params["step"]["inputs"][$renderInput]['isRequired']) && json_encode($params["step"]["inputs"][$renderInput]['isRequired']) == "true" ? " * " : "";
			$params["editQuestionBtn"] .= ($mode == "w" || $mode == "r") && isset($params["step"]["inputs"][$renderInput]["activeComments"]) && $params["step"]["inputs"][$renderInput]["activeComments"] == true ? " <a class='btn btn-xs btn-tertiaire btn-question-comment' href='javascript:;' data-id='".(isset($params["form"]["_id"]) ? (string)$params["form"]["_id"] : "")."' data-answer='". (isset($params["answer"]["_id"]) ? (string) $params["answer"]["_id"] : "") ."'  data-inputkey='".$renderInput."' data-activevcomments='". ( json_encode($params["step"]["inputs"][$renderInput]["activeComments"] ?? false)) ."' data-key='question_".$renderInput."' data-parentid='".(isset($params["form"]["formParent"]) ? $params["form"]["formParent"] : "")."'><i class='fa fa-comments fa-2x'></i><span class='comment-pastil'>".$commentCount."<span></a></a>" : ""; 
			if ($params["canAdminAnswer"] && $mode == "fa")
			{
				$params["canEditForm"] = true;
				if ($params["isAap"])
				{
					$params["editQuestionBtn"] .= " <a class='btn btn-xs btn-danger descSec" . $renderInput . " editQuestionaap' href='javascript:;' data-isrequired ='" . (isset($params["step"]["inputs"][$renderInput]['isRequired']) ? json_encode($params["step"]["inputs"][$renderInput]['isRequired']) : "false") . "' data-position='" . (isset($params["step"]["inputs"][$renderInput]['position']) ? $params["step"]["inputs"][$renderInput]['position'] : "") . "' data-positions=\"" . (isset($params["step"]["inputs"][$renderInput]['positions']) ? json_encode($params["step"]["inputs"][$renderInput]['positions']) : "") . "\" data-collection='" . Form::INPUTS_COLLECTION . "' data-key='" . $renderInput . "' data-path='inputs." . $renderInput . "' data-id='" . (isset($params["form"]["_id"]) ? (string)$params["form"]["_id"] : "") . "' data-parentid='" . (isset($params["form"]["formParent"]) ? $params["form"]["formParent"] : "") . "' data-formid=\"" . $params["formId"] . "\" data-label=\"" . (isset($params["step"]["inputs"][$renderInput]["label"]) ? $params["step"]["inputs"][$renderInput]["label"] : "") . "\" data-info=\"" . (isset($params["step"]["inputs"][$renderInput]["info"]) ? $params["step"]["inputs"][$renderInput]["info"] : "") . "\" data-placeholder=\"" . (isset($params["step"]["inputs"][$renderInput]["placeholder"]) ? $params["step"]["inputs"][$renderInput]["placeholder"] : "") . "\" data-type='" . $params["step"]["inputs"][$renderInput]["type"] . "' data-activemultieval='". ( json_encode($params["step"]["inputs"][$renderInput]["activeMultieval"] ?? false)) ."' data-evaluationkey='". ( isset($params["step"]["inputs"][$renderInput]["evaluationKey"]) ? $params["step"]["inputs"][$renderInput]["evaluationKey"] : 'A' ) ."' data-activecomments='". ( json_encode($params["step"]["inputs"][$renderInput]["activeComments"] ?? false)) ."'><i class='fa fa-pencil'></i></a>" .
						" <a class='btn btn-xs btn-danger deleteLine' href='javascript:;'  data-id='" . (isset($params["form"]["_id"]) ? (string)$params["form"]["_id"] : "") . "' data-collection='" . Form::INPUTS_COLLECTION . "' data-inputkey='" . $renderInput . "' data-key='question_" . $renderInput . "' data-path='inputs." . $renderInput . "' data-parentid='" . (isset($params["form"]["formParent"]) ? $params["form"]["formParent"] : "") . "'><i class='fa fa-trash'></i></a>";
				}
			}
		}

		foreach ($params["steps"] as $stepId => $step)
		{
			$params["steps"][$stepId]["sections"] = [];
			if (isset($step["inputs"]))
			{
				foreach ($step["inputs"] as $inputId => $input)
				{
					if (isset($input["type"]) && $input["type"] == "tpls.forms.titleSeparator")
					{
						$params["steps"][$stepId]["sections"][$inputId] = $input;
					}
				}
			}
		}

		if (!empty($renderStep))
		{
			if (isset($params["step"]["inputs"]))
			{
				foreach ($params["step"]["inputs"] as $inputId => $input)
				{
					if (isset($input["type"]) && $input["type"] == "tpls.forms.titleSeparator")
					{
						$params["step"]["sections"][$inputId] = $input;
					}
				}
			}
		}

		if (!empty($params["parentForm"]["coremu"]) && $params["parentForm"]["coremu"] == true)
		{
			$params["isCoremu"] = $params["parentForm"]["coremu"];
			$params["accountBadge"] = Badge::getBagdes(Yii::app()->session["userId"], Person::COLLECTION);
			$params["allBadge"] = PHDB::find(Badge::COLLECTION, ['parent.' . $contextId => array('$exists' => 1)]);
			$params["allBadge"] = array_merge($params["allBadge"], PHDB::find(Badge::COLLECTION, ['issuer.' . $contextId => array('$exists' => 1)]));
			if (!empty($params["answer"]["answers"]["aapStep1"]["depense"]))
			{
				$params["depenseAction"] = Aap::getDepenseAction($params["answer"]["answers"]["aapStep1"]["depense"]);
			}
			else
			{
				$params["depenseAction"] = array(
					'totalcandidattovalid' => 0,
					'totalpricetovalid' => 0,
					'totalneedcandidat' => 0,
					'candidattovalid' => array(),
					'pricetovalid' => array(),
					'needcandidat' => array(),
				);
			}
		}


		return $params;
	}

	public static function getParamsWizard($form, $answer, $standAlone = false, $config = null)
	{
		$params = [
			"parentForm" => [],
			"isAap" => false,
			"configForm" => [],
			"steps" => [],
			"disabledSteps" => [],
			"disabledStepsId" => [],
			"hiddenSteps" => [],
			"hiddenStepsId" => [],
			"canAdminAnswer" => false,
		];

		if (ctype_xdigit($form))
		{
			$params["parentForm"] = PHDB::findOneById(Form::COLLECTION, $form);
		}
		else
		{
			$params["parentForm"] = $form;
		}

		if (empty($params['parentForm']['onlymemberaccess']))
		{
			$params['parentForm']['onlymemberaccess'] = true;
		}

		if (ctype_xdigit($answer) && !empty($answer))
		{
			$params["answer"] = PHDB::findOneById(Form::ANSWER_COLLECTION, $answer);
		}
		elseif (!empty($answer))
		{
			$params["answer"] = $answer;
		}

		if (isset($params["parentForm"]["type"]) && $params["parentForm"]["type"] == "aap")
		{
			$params["isAap"] = true;
			$params["steps"] = PHDB::find(Form::INPUTS_COLLECTION, array("formParent" => (string)$params["parentForm"]["_id"]));
			if (ctype_xdigit($config) || empty($config))
			{
				$params["configForm"] = PHDB::findOneById(Form::COLLECTION, $params["parentForm"]["config"]);
			}
			else
			{
				$params["configForm"] = $config;
			}
			foreach ($params["steps"] as $stepId => $step)
			{
				$params["steps"][$stepId]["name"] = $params["configForm"]["subForms"][$step["step"]]["name"];
				$params["steps"][$stepId]["id"] = $step["step"];
				if (isset($params["configForm"]["subForms"][$step["step"]]["lockStep"]))
				{
					$params["steps"][$stepId]["lockStep"] = $params["configForm"]["subForms"][$step["step"]]["lockStep"];
				}
				else
				{
					$params["steps"][$stepId]["lockStep"] = false;
				}
				if (isset($params["configForm"]["subForms"][$step["step"]]["hideStep"]))
				{
					$params["steps"][$stepId]["hideStep"] = $params["configForm"]["subForms"][$step["step"]]["hideStep"];
				}
				else
				{
					$params["steps"][$stepId]["hideStep"] = false;
				}
				if (isset($params["configForm"]["subForms"][$step["step"]]["hideStep"]))
				{
					$params["steps"][$stepId]["hideStep"] = $params["configForm"]["subForms"][$step["step"]]["hideStep"];
				}
				else
				{
					$params["steps"][$stepId]["hideStep"] = false;
				}
				if (isset($params["configForm"]["subForms"][$step["step"]]["hideStepStandalone"]))
				{
					$params["steps"][$stepId]["hideStepStandalone"] = $params["configForm"]["subForms"][$step["step"]]["hideStepStandalone"];
				}
				else
				{
					$params["steps"][$stepId]["hideStepStandalone"] = false;
				}
			}
		}
		else
		{
			$params["steps"] = PHDB::find(Form::COLLECTION, array("id" => array('$in' => $params["parentForm"]["subForms"])));
		}


		if (!empty($params["answer"]))
		{
			$access = self::getAnswerAuthorisation($params["answer"], $params["parentForm"], array_keys($params["parentForm"]["parent"])[0]);
			$params["canAdminAnswer"] = $access["canAdminAnswer"];
			$arrayKey = array_keys($params["steps"]);
			$n = 0;

			if (!empty($params["answer"]))
			{
				foreach ($params["steps"] as $stepId => $step)
				{
					$params["steps"][$stepId]["step"] = $stepId;
					if (isset($step["lockStep"]) && $step["lockStep"])
					{
						if (
							!$params["canAdminAnswer"] &&
							(
								(empty($params["answer"]["unlockedSteps"]) || !in_array($step["id"], $params["answer"]["unlockedSteps"])) ||
								(empty($params["answer"]["step"]) || array_search($step["id"], $arrayKey) > array_search($params["answer"]["step"], $arrayKey))
							)
						)
						{
							array_push($params["disabledSteps"], $n);
							array_push($params["disabledStepsId"], $step["id"]);
						}
					}

					if (
						(isset($step["hideStep"]) && $step["hideStep"]) ||
						(isset($params["configForm"]["subForms"][$step["id"]]["params"]["haveReadingRules"]) && $params["configForm"]["subForms"][$step["id"]]["params"]["haveReadingRules"])

					)
					{
						if (
							!$params["canAdminAnswer"] &&
							(
								(
									!empty($params["configForm"]["subForms"][$step["id"]]["params"]["canRead"]) ||
									empty($params["configForm"]["subForms"][$step["id"]]["params"]["canRead"][0])
								) &&
								sizeof(array_intersect($access["roles"], @$params["configForm"]["subForms"][$step["id"]]["params"]["canRead"])) <= 0
							)
						)
						{
							array_push($params["hiddenSteps"], $n);
							array_push($params["hiddenStepsId"], $step["id"]);
						}
					}

					if ($standAlone == true && $step["hideStepStandalone"])
					{
						array_push($params["hiddenSteps"], $n);
						array_push($params["hiddenStepsId"], $step["id"]);
					}
					$n++;
				}
			}
		}

		if (isset($params["parentForm"]["type"]) && $params["parentForm"]["type"] == "aap")
		{
			$params["isAap"] = true;
			$params["steps"] = PHDB::find(Form::INPUTS_COLLECTION, array("formParent" => (string)$params["parentForm"]["_id"]));
			if (ctype_xdigit($config) || empty($config))
			{
				$params["configForm"] = PHDB::findOneById(Form::COLLECTION, $params["parentForm"]["config"]);
			}
			else
			{
				$params["configForm"] = $config;
			}
			foreach ($params["steps"] as $stepId => $step)
			{
				$params["steps"][$stepId]["name"] = $params["configForm"]["subForms"][$step["step"]]["name"];
				$params["steps"][$stepId]["id"] = $step["step"];
				if (isset($params["configForm"]["subForms"][$step["step"]]["lockStep"]))
				{
					$params["steps"][$stepId]["lockStep"] = $params["configForm"]["subForms"][$step["step"]]["lockStep"];
				}
				else
				{
					$params["steps"][$stepId]["lockStep"] = false;
				}
				if (isset($params["configForm"]["subForms"][$step["step"]]["hideStep"]))
				{
					$params["steps"][$stepId]["hideStep"] = $params["configForm"]["subForms"][$step["step"]]["hideStep"];
				}
				else
				{
					$params["steps"][$stepId]["hideStep"] = false;
				}
				if (isset($params["configForm"]["subForms"][$step["step"]]["hideStep"]))
				{
					$params["steps"][$stepId]["hideStep"] = $params["configForm"]["subForms"][$step["step"]]["hideStep"];
				}
				else
				{
					$params["steps"][$stepId]["hideStep"] = false;
				}
				if (isset($params["configForm"]["subForms"][$step["step"]]["hideStepStandalone"]))
				{
					$params["steps"][$stepId]["hideStepStandalone"] = $params["configForm"]["subForms"][$step["step"]]["hideStepStandalone"];
				}
				else
				{
					$params["steps"][$stepId]["hideStepStandalone"] = false;
				}
			}
		}
		else
		{
			$params["steps"] = PHDB::find(Form::COLLECTION, array("id" => array('$in' => $params["parentForm"]["subForms"])));
		}

		return $params;
	}

	public static function validateStep($params, $step)
	{
		$isStepValid = false;
		$obl = 0;
		$validated = 0;

		foreach (array_values($params["steps"])[$step]["inputs"] as $inputId => $input)
		{
			if (!empty($input["isRequired"]) && $input["isRequired"])
			{
				$obl++;
				if (!empty($params["answer"]["answers"][array_values($params["steps"])[$step]][$inputId]))
				{
					$validated++;
				}
			}
		}

		if (Costum::isSameFunction("elementBeforeSave"))
		{
			$isStepValid = ($obl == $validated) && Costum::sameFunction("isStepValid", ["params" => $params, "step" => $step]);
		}
		else
		{
			$isStepValid = $obl == $validated;
		}

		if ($isStepValid)
		{
			PHDB::update(
				self::ANSWER_COLLECTION,
				array("_id" => new MongoId((string)$params["answer"]["_id"])),
				array('$set' => array("step" => array_values($params["steps"])[$step]))
			);
		}

		return $isStepValid;
	}

	public static function getFirstParentForm($form)
	{
		$parent = null;
		if (!empty($form) && isset($form["parent"]))
		{
			foreach ($form["parent"] as $keyP => $valP)
			{
				$parent = Element::getElementById($keyP, $valP["type"], null, array());
				if (!empty($parent))
					return $parent;
			}
		}
		return $parent;
	}

	public static function getAnswerAuthorisation($answer = null, $form = null, $formParent = null, $formParenType = "organizations")
	{
		$return = [
			"canSeeAnswer" => false,
			"canAdminAnswer" => false,
			"roles" => []
		];

		if (ctype_xdigit($answer) && !empty($answer))
		{
			$answer = PHDB::findOneById(Form::ANSWER_COLLECTION, $answer);
		}
		if (ctype_xdigit($form))
		{
			$form = self::getById($form);
		}
		if (ctype_xdigit($formParent))
		{
			$formParent = PHDB::findOneById($formParenType, $formParent);
		}

		if (
			isset(Yii::app()->session["userId"]) &&
			(
				Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) ||
				(isset($formParent["links"]["members"][Yii::app()->session["userId"]]["isAdmin"]) && $formParent["links"]["members"][Yii::app()->session["userId"]]["isAdmin"] == true)
			)
		)
		{
			$return["canAdminAnswer"] = true;
		}
		else
		{
			$return["canAdminAnswer"] = false;
		}

		if (isset($formParent["links"]["members"][Yii::app()->session["userId"]]["roles"]))
		{
			$return["roles"] = $formParent["links"]["members"][Yii::app()->session["userId"]]["roles"];
		}

		$canread = false;

		if ($return["canAdminAnswer"])
		{
			$canread = true;
		}
		elseif (isset(Yii::app()->session["userId"]))
		{
			if (
				empty($form["onlymemberaccess"]) ||
				filter_var($form["onlymemberaccess"], FILTER_VALIDATE_BOOLEAN) == false ||
				(filter_var($form["onlymemberaccess"], FILTER_VALIDATE_BOOLEAN) == true && isset($formParent["links"]["members"][Yii::app()->session["userId"]]))
			)
			{
				if (
					$answer = "new" ||
					empty($form["onlyusercanread"]) ||
					filter_var($form["onlyusercanread"], FILTER_VALIDATE_BOOLEAN) == false ||
					(filter_var($form["onlyusercanread"], FILTER_VALIDATE_BOOLEAN) == true && !empty($answer["user"]) && $answer["user"] == Yii::app()->session["userId"])
				)
				{
					$canread = true;
				}
			}

            if (isset($answer["links"]["canEdit"][Yii::app()->session["userId"]]["roles"]))
            {
                $canread = true;
            }
		}

		$return["canSeeAnswer"] = $canread;

		return $return;
	}

	public static function isFormAdmin($id)
	{
		$form = PHDB::findOneById(Form::COLLECTION, $id);
		$res = false;

		if (!empty(Yii::app()->session["userId"]))
		{
			if (
				Yii::app()->session["userId"] == @$form["author"] ||
				(!empty($form["links"]["members"][Yii::app()->session["userId"]]) &&
					!empty($form["links"]["members"][Yii::app()->session["userId"]]["isAdmin"]) &&
					$form["links"]["members"][Yii::app()->session["userId"]]["isAdmin"] == true)
				||
				(!empty($form["links"]["contributors"][Yii::app()->session["userId"]]) &&
					!empty($form["links"]["contributors"][Yii::app()->session["userId"]]["isAdmin"]) &&
					$form["links"]["contributors"][Yii::app()->session["userId"]]["isAdmin"] == true)
			)
			{
				$res = true;
			}
			else if (isset($form["parent"]) && !empty($form["parent"]))
			{
				foreach ($form["parent"] as $k => $v)
				{
					if ($v["type"] == Person::COLLECTION)
					{
						if ($k == Yii::app()->session["userId"])
							$res = true;
					}
					else
					{
						$res = self::isElementAdmin($k, $v["type"], Yii::app()->session["userId"], true);
					}
				}
			}
		}
		return $res;
	}

	public static function isElementAdmin($elementId, $elementType, $userId, $checkParent = false)
	{
		$res = false;
		if (Authorisation::isUserSuperAdmin($userId))
		{
			return true;
		}
		if (in_array($elementType, [Event::COLLECTION, Project::COLLECTION, Organization::COLLECTION]))
		{
			$eltAdmin = Element::getElementById($elementId, $elementType, null, array("name", "links", "parent", "organizer"));

			if (!empty($eltAdmin))
			{
				// CHECK FIRST IN ELEMENT DIRECTLY IF USER IS ADMIN
				if (
					isset($eltAdmin["links"])
					&& !empty(Link::$linksTypes[$elementType])
					&& isset($eltAdmin["links"][Link::$linksTypes[$elementType][Person::COLLECTION]])
					&& isset($eltAdmin["links"][Link::$linksTypes[$elementType][Person::COLLECTION]][$userId])
					&& isset($eltAdmin["links"][Link::$linksTypes[$elementType][Person::COLLECTION]][$userId]["isAdmin"])
					&& !isset($eltAdmin["links"][Link::$linksTypes[$elementType][Person::COLLECTION]][$userId]["isAdminPending"])
					&& !isset($eltAdmin["links"][Link::$linksTypes[$elementType][Person::COLLECTION]][$userId]["toBeValidated"])
				)
				{
					return true;
					// CHECK THEN IF USER IS ADMIN OF PARENT OF THIS ELEMENT
				}
				else if (isset($eltAdmin["parent"]) && !empty($eltAdmin["parent"]) && empty($checkParent))
				{
					foreach ($eltAdmin["parent"] as $k => $v)
					{
						if ($v["type"] == Person::COLLECTION)
						{
							if ($k == Yii::app()->session["userId"])
								$res = true;
						}
						else
						{
							$res = self::isElementAdmin($k, $v["type"], Yii::app()->session["userId"], true);
						}
						if (!empty($res))
							return $res;
					}
				}
				else if (isset($eltAdmin["organizer"]) && !empty($eltAdmin["organizer"]) && empty($checkParent))
				{
					foreach ($eltAdmin["organizer"] as $k => $v)
					{
						if ($v["type"] == Person::COLLECTION)
						{
							if ($k == Yii::app()->session["userId"])
								$res = true;
						}
						else
						{
							$res = self::isElementAdmin($k, $v["type"], Yii::app()->session["userId"], true);
						}
						if (!empty($res))
							return $res;
					}
				}
			}
		}
		if (empty($checkParent))
		{
			if (Costum::isSameFunction("isElementAdmin"))
			{
				$res = Costum::sameFunction("isElementAdmin", array("elementId" => $elementId, "elementType" => $elementType, "userId" => $userId));
			}
		}
	}

	public static function switchcoremu($form, $active = null)
	{
		if ($active != null)
		{
			PHDB::update(
				self::COLLECTION,
				array("_id" => new MongoId($form)),
				array('$set' => array("coremu" => filter_var($active, FILTER_VALIDATE_BOOLEAN)))
			);
		}
		else
		{
			$formParent = PHDB::findOneById(Form::COLLECTION, $form);
			$active = isset($formParent["coremu"]) ? filter_var($formParent["coremu"], FILTER_VALIDATE_BOOLEAN) : false;
		}

		$budgetinput = PHDB::findOne(Form::INPUTS_COLLECTION, array('formParent' => $form, "step" => "aapStep1"));
		$financementinput = PHDB::findOne(Form::INPUTS_COLLECTION, array('formParent' => $form, "step" => "aapStep3"));

		if (!empty($budgetinput) && !empty($financementinput))
		{
			if (filter_var($active, FILTER_VALIDATE_BOOLEAN))
			{
				$budgetinputtype = "tpls.forms.ocecoform.coremu.budget";
				$financementinputtype = "tpls.forms.ocecoform.coremu.financement";
			}
			else
			{
				$budgetinputtype = "tpls.forms.ocecoform.budget";
				$financementinputtype = "tpls.forms.ocecoform.financementFromBudget";
			}
			PHDB::update(
				self::INPUTS_COLLECTION,
				array("_id" => new MongoId((string)$budgetinput["_id"])),
				array('$set' => array("inputs.depense.type" => $budgetinputtype))
			);
			PHDB::update(
				self::INPUTS_COLLECTION,
				array("_id" => new MongoId((string)$financementinput["_id"])),
				array('$set' => array("inputs.financer.type" => $financementinputtype))
			);
			return true;
		}
		else
		{
			return false;
		}
	}

	public static function getFormTotalFinancement($form , $parentForm = null , $context = null)
	{
		//$result = PHDB::find(Form::ANSWER_COLLECTION, array("form" => $form , "answers.aapStep1.titre" => array('$exists' => 1)));
		if(empty($parentForm)){
            $parentForm = PHDB::findOneById(Form::COLLECTION, $form);
        }
        if(empty($context)){
            $context = PHDB::findOneById($parentForm["parent"][array_keys($parentForm["parent"])[0]]["type"], array_keys($parentForm["parent"])[0]);
        }

		$groupCommunity = Element::getCommunityByParentTypeAndId($parentForm["parent"]);
		$parentCommunity = array_merge(Link::groupFindByType(Organization::COLLECTION, $groupCommunity), Link::groupFindByType(Citoyen::COLLECTION, $context["links"]["members"]));

		$query =
			array(
				array(
					'$match' => array(
						"form" => $form,
						"answers.aapStep1.titre" => ['$exists' => true],
						"answers.aapStep1.depense.financer" => ['$exists' => true]
					)
				),
				array('$unwind' => '$answers.aapStep1.depense'),
				array('$unwind' => '$answers.aapStep1.depense.financer'),
				[
					'$project' => [
						"id" => '$answers.aapStep1.depense.financer.id',
						"name" => '$answers.aapStep1.depense.financer.name',
						"email" => '$answers.aapStep1.depense.financer.name',
						'amount' => '$answers.aapStep1.depense.financer.amount',
                        'include' => '$answers.aapStep1.depense.include'
					],

				]
			);
		$result = [];

		$proposals = PHDB::aggregate(Form::ANSWER_COLLECTION, $query);
		$result = Form::getFinancerAmount($proposals["result"], $context["name"], $parentCommunity, null);

		return $result;
	}

	public static function getFinancerAmount($proposals, $context, $parentCommunity , $getId = false)
	{
		$result = [];
        foreach ($proposals as $k => $v)
		{
			if (!empty($v["amount"]))
			{
				$type = isset($v["type"]) ? $v["type"] : "";
                $name = !empty($v["name"]) ? $v["name"] : '';
                $id = !empty($v["id"]) ? $v["id"] : '';
                $profilImageUrl = '';
                if(!empty($v["id"]) && empty($parentCommunity[$v["id"]])){
                    $parentCommunity[$v["id"]] = PHDB::findOneById(Organization::COLLECTION,$v["id"]);
                }
                if(!empty($v["id"]) && !empty($parentCommunity[$v["id"]])){
                    if(!empty($parentCommunity[$v["id"]]['name'])) {
                        $name = $parentCommunity[$v["id"]]['name'];
                    }elseif(empty($name) && !empty($parentCommunity[$v["id"]]['name'])){
                        $name = $context;
                    }
                    if(empty($profilImageUrl) && !empty($parentCommunity[$v["id"]]['profilImageUrl'])) {
                        $profilImageUrl = $parentCommunity[$v["id"]]['profilImageUrl'];
                    }elseif(empty($name) && !empty($parentCommunity[$v["id"]]['profilImageUrl'])){
                        $profilImageUrl = $context;
                    }
                }
                if($getId){
                    if(!isset($result[trim($name)])){
                        $result[trim($name)] = [
                            "name" => trim($name),
                            "id" => $id,
                            "profilImageUrl" => $profilImageUrl,
                            "amount" => floatval($v["amount"]),
							"type" => $type
                        ];
                    }else{
                        $result[trim($name)] = [
                            "name" => trim($name),
                            "id" => $id,
                            "profilImageUrl" => $profilImageUrl,
                            "amount" => floatval($result[trim($name)]["amount"]) + floatval($v["amount"]),
							"type" => $type
                        ];
                    }
                }else{
                    if(empty($result[$name])){
                        $result[trim($name)] = $v["amount"];
                    }else{
                        $result[trim($name)] = $result[trim($name)] + $v["amount"];
                    }
                }
			}
		}
		return $result;
	}

	public static function createFormConfigPanel (Array $configs){
		$html = "";
		foreach ($configs as $kpanel => $config) {
			if(!empty($config)){
				$html .= '<div class="panel panel-default">';
					$html .= '<div class="panel-body" style="position:relative">';
					$html .="<h5>".$kpanel."</h5>";
						foreach ($config as $k => $v) {
							if(!$v["notShow"]){
								$html .= '<div class="form-group">';
									$html .= '<div class="checkbox checbox-switch switch-success">';
											$html .= '<label>'.$v["label"].'</label>';
											$html .= '<label class="pull-right">';
	
												if($v["input"]["type"]==="checkbox"){
													$html .= '<input 
														data-collection="'.$v["input"]['collection'].'" 
														data-path="'.$v["input"]['path'].'"
														data-id="'.$v["input"]["idCollection"].'" 
														class="aapchecboxconfig" 
														id="'.$v["input"]["inputId"].'"
														type="checkbox" 
														'.$v["input"]["isChecked"].'
													/><span></span>';
												}
												
											$html .= '</label>';
									$html .= '</div>';
									$html .= '<small id="" class="text-muted">'.$v["info"].' </small>';
								$html .= '</div>';
							}
						}
					$html .= '</div>';
				$html .= '</div>';
			}
		}
		return $html;
	}

	public static function getAllFormContext($contextId, $is_only_aap = false)
	{
		$res = [];
		$where = [
			'$and' => [
				[
					"parent.$contextId" => [
						'$exists' => 1
					]
				]
			]
		];
		if ($is_only_aap)
			$where['$and'][] = ['type' => 'aap'];
		else
			$where['$and'][] =
				[
					'$or' => [
						[
							"subForms" => [
								'$exists' => true
							],
							"type" => [
								'$nin' => ["aapConfig", "template"]
							]
						],
						[
							"subForms" => [
								'$exists' => false
							],
							"type" => [
								'$eq' => "aap"
							]
						]
					]
				];
		if (!empty($contextId))
		{
			$res = PHDB::find(
				Form::COLLECTION,
				$where,
				[
					"name",
					"type",
					"subForms",
					"params",
					"parent"
				]
			);
		}
		return $res;
	}

	public static function setFtlInput($form)
	{
		try
		{
			$stepDepense = PHDB::findOne(self::INPUTS_COLLECTION, ["formParent" => $form, "step" => "aapStep1"]);
			$stepFinancer = PHDB::findOne(self::INPUTS_COLLECTION, ["formParent" => $form, "step" => "aapStep3"]);
			PHDB::update(
				self::COLLECTION,
				array("_id" => new MongoId((string)$stepDepense["_id"])),
				array(
					'$set' => array(
						"inputs.financer.type" => "tpls.forms.ocecoform.newDepenseList",
						"inputs.financer.subType" => "depense"
					)
				)
			);
			return PHDB::update(
				self::COLLECTION,
				array("_id" => new MongoId((string)$stepFinancer["_id"])),
				array(
					'$set' => array(
						"inputs.financer.type" => "tpls.forms.ocecoform.newDepenseList",
						"inputs.financer.subType" => "finance"
					)
				)
			);
		}
		catch (CTKException $e)
		{
			return $e->getMessage();
		}
	}

	public static function hasMultiEval($inputs) {
		$isMultiEvalStep = false;
		foreach ($inputs as $input) {
			if (isset($input["activeMultieval"]) && $input["activeMultieval"]) {
				$isMultiEvalStep = true;
				break;
			}
		}
		return $isMultiEvalStep;
	}

	public static function getCommentedQuestionParams($id, $form = null, $getAll = true, $commentIndex = null, $extraParams = [])
	{
		$params = [

			//element
			"parentForm" => [],
			"configForm" => [],
			"steps" => [],
			"userData" => [],
			"answer" => [],
			"context" => [],
			"contextConfig" => [],
			"orderedQuestion" => [],
			"toBeSelectedComment" => $commentIndex,
			"toHighlightComments" => [
				"allComments" => []
			],
			"newComment" => [],
			"canEditDiscussion" => false,
			"referentUsersPath" => "answers.aapStep1.finderaapStep1m0w402djpsq3i44017",
		];

		$citoyenReferentInputId = "answers.aapStep1.finderaapStep1m0w402djpsq3i44017";

		if (!empty($id) && $id != "new")
		{
			$params["answer"] = PHDB::findOneById(Form::ANSWER_COLLECTION, $id, ["collection", "links", "created", "cterSlug", "form", "context", "source", "answers", "moreDiscussions", "user"]);
			if(isset($params["answer"]["_id"])) {
				$params["answer"]["profilImageUrl"] = Aap::getPropositionThumbnail((string)$params["answer"]["_id"]);
			}
		}
		$formFields = ["collection", "name", "config", "parent", "subForms", "type", "subType", "answersMapping", "created", "creator", "active", "endDate", "endDateNoconfirmation", "oneAnswerPerPers", "private", "showAnswers", "startDate", "startDateNoconfirmation", "temporarymembercanreply", "inputConfig", "onlymemberaccess"];

		if (isset($form) && !empty($form))
			$params["parentForm"] = PHDB::findOneById(Form::COLLECTION, $form, $formFields);

		if (empty($form) && !empty($params["answer"]["form"]))
		{
			$form = $params["answer"]["form"];
		}
		if (empty($params["parentForm"]))
		{
			if (!empty($form)) {
				$params["parentForm"] = PHDB::findOneById(Form::COLLECTION, $form, $formFields);
			}
			else if (!empty($params["answer"]["form"]))
			{
				$params["parentForm"] = PHDB::findOneById(Form::COLLECTION, $params["answer"]["form"], $formFields);
			}
		}

		if (!empty($params["answer"]["user"]))
		{
			$params["userData"] = PHDB::findOneById(Citoyen::COLLECTION, $params["answer"]["user"]);
		}

		if (isset($params["parentForm"]["answersMapping"]["referentUsersPath"])) {
			$params["referentUsersPath"] = $params["parentForm"]["answersMapping"]["referentUsersPath"];
		}
		$contextId = array_keys($params["parentForm"]["parent"])[0];
		$contextType = $params["parentForm"]["parent"][$contextId]["type"];
		if (isset($params["parentForm"]["type"]) && $params["parentForm"]["type"] == "aap")
		{
			$params["isAap"] = true;
			$params["steps"] = PHDB::find(Form::INPUTS_COLLECTION, array("formParent" => $form), ["isSpecific", "formParent", "step", "inputs"]);
			$params["configForm"] = PHDB::findOneById(Form::COLLECTION, $params["parentForm"]["config"], ["collection", "parent", "name", "type", "subType", "subForms"]);

			foreach ($params["steps"] as $stepId => $step)
			{
				$params["steps"][$stepId]["name"] = $params["configForm"]["subForms"][$step["step"]]["name"];
				$params["steps"][$stepId]["id"] = $step["step"];
				if (!empty($renderStep) && $step["step"] == $renderStep)
				{
					$params["step"] = $params["steps"][$stepId];
					$params["step"]["id"] = $renderStep;
					$params["step"]["name"] = $params["configForm"]["subForms"][$step["step"]]["name"];
				}
			}
		}
		else
		{
			$params["mongoidsteps"] = PHDB::find(Form::COLLECTION, array("id" => array('$in' => $params["parentForm"]["subForms"])));
			foreach ($params["mongoidsteps"] as $mongoid => $step)
			{
				$params["steps"][$step["id"]] = $step;
			}
			if (!empty($renderStep))
			{
				$rslstep = PHDB::find(Form::COLLECTION, array("id" => $renderStep));
				if (isset(array_values($rslstep)[0]["inputs"]))
				{
					$params["step"] = array_values($rslstep)[0];
					$params["step"]["step"] = $renderStep;
				}
				else
				{
					$params["step"] = $renderStep;
					$params["step"]["step"] = $renderStep;
				}
			}
		}

		if (!empty($contextId) && !empty($contextType))
		{
			$params["context"] = PHDB::findOneById($contextType, $contextId, ["collection", "costumId", "costumType", "created", "creator", "name", "parent", "profilBannerUrl", "profilImageUrl", "profilMarkerImageUrl", "profilMediumImageUrl", "profilRealBannerUrl", "profilThumbImageUrl", "slug", "source", "type"]);
			if ($params["isAap"] && $params["parentForm"]["parent"] == $params["configForm"]["parent"])
			{
				$params["contextConfig"] = $params["context"];
			}
			elseif ($params["isAap"])
			{
				$contextConfigId = array_keys($params["configForm"]["parent"])[0];
				$params["contextConfig"] = PHDB::findOneById($params["configForm"]["parent"][$contextConfigId]["type"], $contextConfigId);
			}
		}

		$paramsPathRegex = [];
		foreach ($params["steps"] as $stepId => $step)
		{
			$params["allFormComments"] = [];
			if (isset($step["inputs"]) && isset($params["parentForm"]["_id"]) && isset($params["answer"]["_id"]))
			{
				foreach ($step["inputs"] as $inputId => $input)
				{
					$paramsPathRegex[] = [ "path" => new MongoRegex('/'. ((string) $params["parentForm"]["_id"] . '\.' . $stepId . '\.' . (string) $params["answer"]["_id"] . '\.' . $inputId . '\.question') .'.*/')];
				}
			}
		}
		$communReferentValue = UtilsHelper::array_val_at_path($params["answer"], $citoyenReferentInputId, ".");
		$allReferentId = [];
		if (!empty($communReferentValue)) {
			$allReferentId = array_keys($communReferentValue);
		}
		if (
			(
				isset($params["context"]["_id"]) && isset(Yii::app()->session["userId"]) &&
				Authorisation::isElementAdmin((string) $params["context"]["_id"], $params["context"]["collection"], Yii::app()->session["userId"])
			) || 
			(
				isset(Yii::app()->session["userId"]) && isset($params["answer"]["user"]) &&
				Yii::app()->session["userId"] == $params["answer"]["user"]
			) ||
			(
				!empty($allReferentId) &&
				isset(Yii::app()->session["userId"]) &&
				in_array(Yii::app()->session["userId"], $allReferentId)
			)
		) {
			$params["canEditDiscussion"] = true;
		}
		if (isset($params["answer"]["moreDiscussions"])) {
			foreach ($params["answer"]["moreDiscussions"] as $discuId => $discu) {
				$toShowInList = false;
				if (isset($discu["privilege"]) && $discu["privilege"] == "private") {
					if (isset($params["context"]["_id"]) && Authorisation::isElementAdmin((string) $params["context"]["_id"], $params["context"]["collection"], Yii::app()->session["userId"])) {
						$toShowInList = true;
					}
				} else if (isset($discu["privilege"]) && $discu["privilege"] == "authorAdmin") {
					if (
						$params["canEditDiscussion"]
					) {
						$toShowInList = true;
					}
				} else if (isset($discu["privilege"]) && $discu["privilege"] == "authorAll") {
					$toShowInList = true;
				}
				if (isset($discu["path"]) && isset($discu["stepId"]) && $toShowInList) {
					$params["steps"][$discu["stepId"]][$discuId] = [
						"id" => $discuId,
						"formParent" => (string) $params["parentForm"]["_id"],
						"inputs" => [
							$discuId => [
								"label" => $discu["name"],
								"showAnyway" => true,
								"commentPath" => $discu["path"],
							]
						],
						"name" => $discu["name"],
						"step" => $discuId,
						"_id" => [
							'$id' => $discuId,
						]
					];
					$otherDiscuSplittedPath = explode(".", $discu["path"]);
					$pathRegex = "/";
					foreach ($otherDiscuSplittedPath as $key => $value) {
						if ($key == 0) {
							$pathRegex .= $value;
						} else {
							$pathRegex .= '\.' . $value;
						}
					}
					$pathRegex .= '.*/';
					$paramsPathRegex[] = [ "path" => new MongoRegex($pathRegex)];
				} else {
					unset($params["answer"]["moreDiscussions"][$discuId]);
				}
			}
		}
		$isAlreadySelected = false;
		$alreadyInList = [];
		$commentFields = ["contextId", "contextType", "text", "created", "author", "collection", "tags", "argval", "path", "status", "views.".Yii::app()->session["userId"], "parentId"];
		if (isset($params["parentForm"]["_id"])) {
			$params["allQuestionComments"] = PHDB::find(Comment::COLLECTION, [
				"contextId" =>  (string) $params["parentForm"]["_id"],
				"contextType" => Form::COLLECTION
			], $commentFields);
		}
		if (!empty($paramsPathRegex))
		{
			$params["allFormComments"] = PHDB::findAndSort(Comment::COLLECTION, [
				"contextId" =>  (string) $params["parentForm"]["_id"],
				"contextType" => Form::COLLECTION,
				'$or' => $paramsPathRegex
			], ["created" => -1], 0, $commentFields);
			if (!empty($params['allFormComments'])) {
				foreach ($params['allFormComments'] as $commentId => $commentVal) {
					$hasParent = self::checkCommentParent($params["allQuestionComments"], $commentVal);
					if (!empty($extraParams["newCommentId"]) && $extraParams["newCommentId"] == $commentId && $hasParent) {
						$params["newComment"] = $commentVal;
					}
					if (!empty($commentVal["path"]) && $hasParent) {
						$commentPath = explode(".", $commentVal["path"]);
						if (strpos($commentVal["path"], ".discu_") !== false ) {
							$moreDiscuStepId = $commentPath[1];
							$moreDiscuId = $commentPath[3];
							if (!isset($params["toHighlightComments"][$moreDiscuId])) {
								$params["toHighlightComments"][$moreDiscuId] = [];
							}
							if (!empty($moreDiscuId) && isset($params["answer"]["moreDiscussions"][$moreDiscuId])) {
								if (!isset($params["orderedQuestion"][$moreDiscuStepId]["inputs"][$moreDiscuId]) && isset($params["steps"][$moreDiscuStepId][$moreDiscuId])) {
									$params["orderedQuestion"][$moreDiscuStepId]["formParent"] = $params["steps"][$moreDiscuStepId][$moreDiscuId]["formParent"] ?? null;
									$params["orderedQuestion"][$moreDiscuStepId]["id"] = $params["steps"][$moreDiscuStepId][$moreDiscuId]["id"] ?? null;
									$params["orderedQuestion"][$moreDiscuStepId]["name"] = $params["steps"][$moreDiscuStepId][$moreDiscuId]["name"] ?? null;
									$params["orderedQuestion"][$moreDiscuStepId]["step"] = $params["steps"][$moreDiscuStepId][$moreDiscuId]["step"] ?? null;
									$params["orderedQuestion"][$moreDiscuStepId]["_id"] = $params["steps"][$moreDiscuStepId][$moreDiscuId]["_id"] ?? null;
									$params["orderedQuestion"][$moreDiscuStepId]["inputs"][$moreDiscuId] = $params["steps"][$moreDiscuStepId][$moreDiscuId]["inputs"][$moreDiscuId];
									$alreadyInList[] = $moreDiscuId;
									if ($commentIndex == $commentId) {
										$params["orderedQuestion"][$moreDiscuStepId]["inputs"][$moreDiscuId]["toBeSelected"] = true;
										$isAlreadySelected = true;
									}
								} else if (isset($params["orderedQuestion"][$moreDiscuStepId]["inputs"][$moreDiscuId]) && $commentIndex == $commentId) {
									$params["orderedQuestion"][$moreDiscuStepId]["inputs"][$moreDiscuId]["toBeSelected"] = true;
									$isAlreadySelected = true;
								}
							} else if (isset($params["orderedQuestion"][$moreDiscuStepId]["inputs"][$moreDiscuId]) && $commentIndex == $commentId) {
								$params["orderedQuestion"][$moreDiscuStepId]["inputs"][$moreDiscuId]["toBeSelected"] = true;
								$isAlreadySelected = true;
							}
							if (isset(Yii::app()->session["userId"]) && !isset($commentVal["views"][Yii::app()->session["userId"]])) {
								isset($params["orderedQuestion"][$moreDiscuStepId]["inputs"][$moreDiscuId]["notSeenComments"]) ? $params["orderedQuestion"][$moreDiscuStepId]["inputs"][$moreDiscuId]["notSeenComments"]++ : $params["orderedQuestion"][$moreDiscuStepId]["inputs"][$moreDiscuId]["notSeenComments"] = 1;
								UtilsHelper::push_array_if_not_exists($commentId, $params["toHighlightComments"][$moreDiscuId]);
							}
						} else {
							$stepId = $commentPath[1];
							$inputId = $commentPath[3];
							$depenseIndexId = array_search("depenseindex", $commentPath, true);
							$showAnyway = strpos($commentVal["path"], "comment.financement.") !== false ? true : false;
							$inputIdSuffix = $showAnyway == true ? "_financement" . ($depenseIndexId !== false && isset($commentPath[$depenseIndexId + 1]) ? "_".$commentPath[$depenseIndexId + 1] : "") : "";
							if (!isset($params["toHighlightComments"][$inputId.$inputIdSuffix])) {
								$params["toHighlightComments"][$inputId.$inputIdSuffix] = [];
							}
							if (!isset($params["orderedQuestion"][$stepId]["inputs"][$inputId.$inputIdSuffix]) && isset($params["steps"][$stepId]["inputs"][$inputId])) {
								$params["orderedQuestion"][$stepId]["formParent"] = $params["steps"][$stepId]["formParent"] ?? null;
								$params["orderedQuestion"][$stepId]["id"] = $params["steps"][$stepId]["id"] ?? null;
								$params["orderedQuestion"][$stepId]["name"] = $params["steps"][$stepId]["name"] ?? null;
								$params["orderedQuestion"][$stepId]["step"] = $params["steps"][$stepId]["step"] ?? null;
								$params["orderedQuestion"][$stepId]["_id"] = $params["steps"][$stepId]["_id"] ?? null;
								$params["orderedQuestion"][$stepId]["inputs"][$inputId.$inputIdSuffix] = $params["steps"][$stepId]["inputs"][$inputId];
								$params["orderedQuestion"][$stepId]["inputs"][$inputId.$inputIdSuffix]["showAnyway"] = $showAnyway;
								if ($showAnyway == true) {
									$depenseKeyIndex = array_search("depensekey", $commentPath, true);
									$params["orderedQuestion"][$stepId]["inputs"][$inputId.$inputIdSuffix]["commentPath"] = $commentVal["path"];
									if ($depenseKeyIndex !== false && isset($commentPath[$depenseKeyIndex + 1]) && $depenseIndexId !== false && isset($commentPath[$depenseIndexId + 1]) && isset($params["answer"]["answers"])) {
										$depensePath = implode(".", explode("-", $commentPath[$depenseKeyIndex + 1])) . "." . $commentPath[$depenseIndexId + 1];
										$depenseVal = UtilsHelper::array_val_at_path($params["answer"]["answers"], $depensePath, ".");
										if (!empty($depenseVal) && isset($depenseVal["poste"])) {
											$params["orderedQuestion"][$stepId]["inputs"][$inputId.$inputIdSuffix]["label"] = $depenseVal["poste"] . " (Financement)";
										}
									}
								}
								if ($commentIndex == $commentId) {
									$params["orderedQuestion"][$stepId]["inputs"][$inputId.$inputIdSuffix]["toBeSelected"] = true;
									$isAlreadySelected = true;
								}
							} else if (isset($params["orderedQuestion"][$stepId]["inputs"][$inputId.$inputIdSuffix]) && $commentIndex == $commentId) {
								$params["orderedQuestion"][$stepId]["inputs"][$inputId.$inputIdSuffix]["toBeSelected"] = true;
								$isAlreadySelected = true;
							}
							if (isset(Yii::app()->session["userId"]) && !isset($commentVal["views"][Yii::app()->session["userId"]])) {
								isset($params["orderedQuestion"][$stepId]["inputs"][$inputId.$inputIdSuffix]["notSeenComments"]) ? $params["orderedQuestion"][$stepId]["inputs"][$inputId.$inputIdSuffix]["notSeenComments"]++ : $params["orderedQuestion"][$stepId]["inputs"][$inputId.$inputIdSuffix]["notSeenComments"] = 1;
								UtilsHelper::push_array_if_not_exists($commentId, $params["toHighlightComments"][$inputId.$inputIdSuffix]);
							}
						}
					}
				}
				unset($params['allFormComments']);
			}
		}
		if (isset($params["answer"]["moreDiscussions"])) {
			foreach ($params["answer"]["moreDiscussions"] as $discuId => $discu) {
				if (array_search($discuId, $alreadyInList) === false) {
					$params["orderedQuestion"][$discu["stepId"]]["id"] = $discuId;
					$params["orderedQuestion"][$discu["stepId"]]["formParent"] = (string) $params["parentForm"]["_id"];
					$params["orderedQuestion"][$discu["stepId"]]["name"] = $discu["name"];
					$params["orderedQuestion"][$discu["stepId"]]["step"] = $discuId;
					$params["orderedQuestion"][$discu["stepId"]]["_id"] = [
						'$id' => $discuId,
					];
					$params["orderedQuestion"][$discu["stepId"]]["inputs"][$discuId] = [
						"label" => $discu["name"],
						"showAnyway" => true,
						"commentPath" => $discu["path"],
					];
				}
			}
			unset($params["answer"]["moreDiscussions"]);
		}
		if ($getAll && isset($params["answer"]["_id"])) {
			$params["allComments"] = PHDB::find(Comment::COLLECTION, [
				"contextId" =>  (string) $params["answer"]["_id"],
				"contextType" => Answer::COLLECTION
			], $commentFields);
			if (!empty($params['allComments'])) {
				foreach ($params['allComments'] as $commentId => $commentVal) {
					$hasParent = self::checkCommentParent($params["allComments"], $commentVal);
					if (!empty($extraParams["newCommentId"]) && $extraParams["newCommentId"] == $commentId && $hasParent) {
						$params["newComment"] = $commentVal;
					}
					if ($hasParent && isset(Yii::app()->session["userId"]) && !isset($commentVal["views"][Yii::app()->session["userId"]])) {
						isset($params["allDiverNotSeenComments"]) ? $params["allDiverNotSeenComments"]++ : $params["allDiverNotSeenComments"] = 1;
						UtilsHelper::push_array_if_not_exists($commentId, $params["toHighlightComments"]["allComments"]);
					}
				}
				unset($params['allComments']);
			}
			if (!$isAlreadySelected) {
				$params["toBeSelectedAll"] = true;
			}
		}
		if (!empty($params["newComment"]["author"])) {
			$params["newComment"]["authorData"] = PHDB::findOneById(Citoyen::COLLECTION, $params["newComment"]["author"], ["name", "profilThumbImageUrl"]);
		}
		if (isset($params["allQuestionComments"])) {
			unset($params["allQuestionComments"]);
		}
		if (empty($params["steps"])) {
			$params["steps"] = new stdClass();
		}
		if (empty($params["orderedQuestion"])) {
			$params["orderedQuestion"] = new stdClass();
		}
		return $params;
	}

	public static function checkCommentParent($allComents, $comment) {
		if (!empty($allComents) && !empty($comment)) {
			if (isset($comment["parentId"]) && !empty($comment["parentId"]) && isset($allComents[$comment["parentId"]])) {
				return self::checkCommentParent($allComents, $allComents[$comment["parentId"]]);
			} elseif (isset($comment["parentId"]) && empty($comment["parentId"])) {
				return true;
			} elseif (isset($comment["parentId"]) && !empty($comment["parentId"]) && !isset($allComents[$comment["parentId"]])) {
				return false;
			}
		} else {
			return false;
		}
	}

	public static function get_aap_inputs($form, $step = 'aapStep1')
	{
		$db_input = PHDB::findOne(self::INPUTS_COLLECTION, ['formParent' => $form, 'step' => $step], ['inputs']);
		return ($db_input['inputs']);
	}

	public static function formConfigFromContext($parent_id, $parent_type, $is_aap = 0) {
		$db_config = null;
		$db_where = ["parent.$parent_id.type" => $parent_type];
		if ($is_aap)
			$db_where['type'] = 'aap';
		$db_form = PHDB::findOne(self::COLLECTION, $db_where, ['config']);
		if (!empty($db_form['config']))
			$db_config = PHDB::findOneById(self::COLLECTION, $db_form['config']);
		return ($db_config);
	}

	public static function getConfig($f_parent, $fields = []) {
		$db_form = PHDB::findOneById(self::COLLECTION, $f_parent, ['config']);
		if (!empty($db_form['config']))
			return (PHDB::findOneById(self::COLLECTION, $db_form['config'], $fields));
		return (null);
	}

    public static function setPayementConfig($config) {
        $return = [];
        if($config["type"] == "iban") {
            $return = [
                'iban' => self::encryptData($config["iban"], Yii::app()->session['userId']),
                'bic' => self::encryptData($config["bic"], Yii::app()->session['userId'])
            ];
        }elseif($config["type"] == "api_key_stripe"){
            $return = [
                'api_key_stripe' => self::encryptData($config["api_key_stripe"], Yii::app()->session['userId']),
                'webhook' => self::encryptData($config["webhook"], Yii::app()->session['userId']),
            ];
        }
        $return['type'] = $config["type"];
        $return['user'] = $config["user"];
        $return['addedAt'] = time();

        return $return;
    }

    public static function getPayementPartialConfig() {
        $return = [];
        if(Person::logguedAndValid()){
            $pc = PHDB::findOne(Form::PAYMENTCONFIG_COLLECTION , ["user" => Yii::app()->session['userId']]);
            if(isset($pc["payement_list"])) {
                foreach ($pc["payement_list"] as $key => $value) {
                    if ($key == "iban") {
                        $return[] = [
                            'type' => 'iban',
                            'iban' => self::censorData($value["iban"], $pc["user"]),
                            'bic' => self::censorData($value["bic"], $pc["user"], 3),
                            'addedAt' => $value["addedAt"]
                        ];
                    } elseif ($key== "api_key_stripe") {
                        $return[] = [
                            'type' => 'api_key_stripe',
                            'api_key_stripe' => self::censorData($value["api_key_stripe"], $pc["user"]),
                            'webhook' => self::censorData($value["webhook"], $pc["user"]),
                            'addedAt' => $value["addedAt"]
                        ];
                    }
                }
            }
        }

        return [
            "favorite" => $pc["favorite"] ?? false,
            "payement_list" => $return
        ];
    }

    public static function censorData($encryptedData, $key, $visibleChars = 5) {
        $decrypted = self::decryptData($encryptedData, $key);
        $masked = str_repeat('*', strlen($decrypted) - $visibleChars);
        return $masked . substr($decrypted, -$visibleChars); // Censure dynamique
    }

    public static function encryptData($data, $key) {
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
        $encrypted = openssl_encrypt($data, 'aes-256-cbc', $key, 0, $iv);
        return base64_encode($encrypted . '::' . $iv); // Concaténer le texte chiffré et le IV
    }

    public static function decryptData($encryptedData, $key) {
        list($encrypted, $iv) = explode('::', base64_decode($encryptedData), 2);
        return openssl_decrypt($encrypted, 'aes-256-cbc', $key, 0, $iv);
    }

    public static function savePayementPartialConfig($payment) {
        if(Person::logguedAndValid()){
            $pc = PHDB::findOne(Form::PAYMENTCONFIG_COLLECTION , ["user" => Yii::app()->session['userId']]);

            if($pc == null){
                $save = [
                    "user" => Yii::app()->session['userId'],
                    "created" => time(),
                    "payement_list" => []
                ];
                $save["payement_list"][$payment["type"]] = self::setPayementConfig($payment);
                return Yii::app()->mongodb->selectCollection(Form::PAYMENTCONFIG_COLLECTION)->insert($save);
            }else{
                $payment["user"] = Yii::app()->session['userId'];
                return PHDB::update(
                    Form::PAYMENTCONFIG_COLLECTION,
                    array("user" => Yii::app()->session['userId']),
                    array('$set' => array("payement_list." . $payment["type"] => self::setPayementConfig($payment) ))
                );
            }
        }else{
            return [];
        }
    }

    public static function savePayementFavorite($favorite) {
        if(Person::logguedAndValidb()) {
            return PHDB::update(
                Form::PAYMENTCONFIG_COLLECTION,
                array("user" => Yii::app()->session['userId']),
                array('$set' => array("favorite" => $favorite))
            );
        }
    }

    public static function deletePayementConfig($type){
        if(Person::logguedAndValid()) {
            $pc = PHDB::findOne(Form::PAYMENTCONFIG_COLLECTION , ["user" => Yii::app()->session['userId']]);
            if(isset($pc["payement_list"][$type])) {
                unset($pc["payement_list"][$type]);
            }

            if(empty($pc["payement_list"]))
                $pc["payement_list"] = new stdClass();

            return PHDB::update(
                Form::PAYMENTCONFIG_COLLECTION,
                array("user" => Yii::app()->session['userId']),
                array('$set' => array("payement_list"  => $pc["payement_list"] ))
            );
        }
    }
}
