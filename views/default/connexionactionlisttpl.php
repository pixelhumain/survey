<?php
HtmlHelper::registerCssAndScriptsFiles(array(
// '/css/dashboard.css',
//    '/js/form.js'
), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );

?>
<style type="text/css">
    .formconexmode .container {
        text-align: center;
    }

    .formconexmode .haveaccountbtn {
        width: 250px;
        height: 50px;
        background: #9fbd38;
        border: 0;
        display: inline-block;
        color: #fff;
        font-size: 18px;
        margin: 10px;
    }

    .formconexmode .givemailbtn {
        width: 250px;
        height: 50px;
        background: #333;
        border: 0;
        display: inline-block;
        margin-right: 10px;
        color: #fff;
        font-size: 17px;
        margin: 10px;
    }

    .formconexmode h2 {
        font-weight: 100 !important;
        color: #777;
        font-size: 48px;
    }

    .formconexmode h4 {
        font-weight: 100;
        color: #777;
    }

    .formconexmode p {
        font-size: 15px !important;
    }


</style>

<div class="row formconexmode">
    <div class="container">
            <h2>Identifiez-vous</h2>
            <h4>pour interagir avec les actions</h4>

            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <button class="haveaccountbtn " data-target="#modalLogin" data-toggle="modal" data-dismiss="modal" >J'ai déjà un compte</button>
                    <p>Acceder en se connectant avec un compte communecter</p>
                </div>
                <div class="col-md-6 col-sm-12">
                    <button class="givemailbtn">Acceder avec un email</button>
                    <p>Acceder en fournissant un mail, qui va recevoir une invitation à rejoindre la plateforme </p>
                </div>
            </div>

        </div>
</div>

<script type="text/javascript">
    var params = <?php echo json_encode($params); ?>;
    $(function(){

        $('.givemailbtn').off().on('click',function(){
            $this = $(this);

            bootbox.prompt({
                title: "Votre adresse Email",

                callback: function (result) {
                    if(result){
                        ajaxPost(
                            null,
                            baseUrl+'/survey/answer/actionlist',
                            {
                                id : params ,
                                email : result
                            }
                            , function(res){

                                if(res.status && notNull(res.location)){
                                    //alert(res.location);

                                    setTimeout(function(){
                                            window.location.assign(res.location);
                                            window.location.reload();
                                        }
                                        ,2000);


                                }else{
                                    toastr.error(res.msg);
                                }
                            })
                    }
                }
            });
        });
    })
</script>
