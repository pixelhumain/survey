<?php
HtmlHelper::registerCssAndScriptsFiles(array(
// '/css/dashboard.css',
    '/js/form.js'
), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );

if(isset($form) && !isset($parentForm)){
    $parentForm = $form;
}
?>
<style type="text/css">
    #currentModalHeader {
        position: relative !important;
        width: 100% !important;
    }
    .modal-custom .modal-content {
        overflow-x: hidden;
    }
    .formconexmode {
        width: 100%;
    }
    .formconexmode .containere {
        text-align: center;
    }

    .formconexmode .haveaccountbtn {
        /* width: 250px;
        height: 50px; */
        background: #43C9B7;
        border: 3px solid #43C9B7;
        display: inline-block;
        color: #fff;
        font-size: 18px;
        font-weight: 600;
        margin: 10px 0;
        border-radius: 10px;
        transition: all 0.4s ease;
    }
    .formconexmode .haveaccountbtn:hover {
        background: transparent;
        color: #43C9B7;
    }

    .formconexmode .givemailbtn {
        /* width: 250px;
        height: 50px; */
        background: #4623c9;
        border: 3px solid #4623c9;
        display: inline-block;
        color: #fff;
        font-size: 18px;
        font-weight: 600;
        margin: 10px 0;
        border-radius: 10px;
        transition: all 0.4s ease;
    }

    .formconexmode .givemailbtn:hover {
        background: transparent;
        color: #4623c9;
    }
    .formconexmode h2 {
        font-weight: 100 !important;
        color: #777;
        font-size: 48px;
    }

    .formconexmode h4 {
        font-weight: 100;
        color: #777;
    }

    .formconexmode p {
        font-size: 15px !important;
    }

    @media (max-width: 500px) {
        .formconexmode h2 {
            font-size: 30px;
        }
        .formconexmode h4 {
            font-size: 16px !important;
        }
        .formconexmode p {
            font-size: 12px !important;
        }
        .formconexmode .haveaccountbtn, .formconexmode .givemailbtn {
            font-size: 16px;
        }
        .modal-title.text-center {
            text-align: left;
            font-size: 16px;
        }
        
    }

</style>
<div class="formconexmode">
    <div class="row">
        <div class="containere">
            <h2>Identifiez-vous</h2>
            <h4>pour répondre au formulaire</h4>

            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <button class="haveaccountbtn btn" data-target="#modalLogin" data-toggle="modal" data-dismiss="modal" >J'ai déjà un compte</button>
                    <p>Répondre en se connectant avec un compte communecter</p>
                </div>
                <div class="col-md-6 col-sm-12">
                    <button class="givemailbtn btn">Repondre sans avoir un compte</button>
                    <p>Répondre en fournissant un mail, qui va recevoir une invitation à rejoindre la plateforme </p>
                </div>
            </div>

        </div>
    </div>
</div>
<?php
if(empty($answer)){
    $id = "new";
}else{
    $id = (string)$answer["_id"];
}
$connexionmodeParams = [
    "id" => $id,
    "answer" => $answer,
    "mode" => "w",
    "form" => (string)$parentForm["_id"]
];

//var_dump($form);
?>

<script type="text/javascript">
    $(function(){
        var  copyFormObj =formObj.init();
        $('.givemailbtn').off().on('click',function(){
            $this = $(this);
            var params = <?php echo json_encode($connexionmodeParams); ?>;
            mylog.log("params.form",params.form);
            bootbox.prompt({
                title: " votre adresse Email",
                /*message: '<p>Please select an option below:</p>',*/
                callback: function (result) {
                    if(result){
                        ajaxPost(
                            null,
                            baseUrl+`/survey/answer/answerwithemail/`,
                            {
                                id : params.id,
                                form : params.form,
                                email : result
                            }
                            , function(res){

                                if(res.status && notNull(res.location)){
                                    //alert(res.location);

                                    setTimeout(function(){
                                            window.location.assign(res.location);
                                            window.location.reload();
                                        }
                                        ,2000);


                                }else{
                                    toastr.error(res.msg);
                                }
                            })
                    }
                }
            });
            var emailLastVal = "";
            var lastStat = false;
            var bootboxModal = setInterval(function() {
                if($('.bootbox.modal.fade.bootbox-prompt').is(':visible')) {
                    const emailRegex = new RegExp(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g);
                    $('.bootbox-input.bootbox-input-text.form-control').on('keyup', delay(function(e) {
                        var self = this;
                        if($(this).val().match(emailRegex) && emailLastVal != $(this).val()) {
                            emailLastVal = $(this).val();
                            lastStat = true;
                            var resVal = false;
                            ajaxPost(
                                null,
                                baseUrl+`/survey/answer/checkemailuser/`,
                                {
                                    id : params.id,
                                    form : params.form,
                                    email : $(self).val()
                                },
                                function(res){
                                    if(res && res.duplicated) {
                                        lastStat = false;
                                        $('.btn.btn-primary.bootbox-accept').prop('disabled', true);
                                        var msgError = trad.AnActiveAccountExists;
                                        var btnHTML = `
                                            <a href="javascript:;" class="btn btn-default bg-white letter-blue bold margin-top-10" data-target="#modalLogin" data-toggle="modal" data-dismiss="modal">
                                                <i class="fa fa-sign-in"></i>
                                                ${tradCms.login}
                                            </a>`;
                                        if(res && res.tobeactivated) {
                                            lastStat = false;
                                            msgError = trad.ATemporaryAccountExists;
                                            var btnHTML = `
                                                <a href="javascript:;" class="btn btn-default bg-white letter-blue bold margin-top-10 openModalResendEmail" data-toggle="modal" data-target="#modalSendActivation" onclick="${$('#modalSendActivation #email2').val($(self).val())};">
                                                    <i class="fa fa-envelope"></i>
                                                    ${trad.ReceiveAnotherValidationEmail}
                                                </a>
                                            `;
                                        }
                                        $('.bootbox-form-error').remove();
                                        $('.bootbox-form').append(`
                                            <div class="bootbox-form-error text-center">
                                                <span class="bg-white alert-danger margin-top-10">${msgError}</span><br />
                                                ${btnHTML}
                                            </div>
                                        `).animate({ opacity:1 }, 300 );
                                        $('.openModalResendEmail').off().on('click', function() {
                                            $('.bootbox.modal.fade.bootbox-prompt').hide()
                                        });
                                        resVal = e.which !== 13;
                                    } else if(res && !res.duplicated) {
                                        $('.btn.btn-primary.bootbox-accept').prop('disabled', false);
                                        $('.bootbox-form-error').remove();
                                        resVal = true
                                    }
                                },
                                null,
                                null,
                                {
                                    async:false
                                }
                            );
                            return resVal;
                        } else {
                            if(lastStat) {
                                $('.btn.btn-primary.bootbox-accept').prop('disabled', false);
                                $('.bootbox-form-error').remove();
                                return lastStat
                            } else {
                                $('.btn.btn-primary.bootbox-accept').prop('disabled', true);
                                return e.which !== 13;
                            }
                        }
                    }, 350));
                    $('.bootbox-input.bootbox-input-text.form-control').on('blur', function() {
                        var self = this;
                        if($(self).val() == '') {
                            $('.btn.btn-primary.bootbox-accept').prop('disabled', true)
                        } else if($(self).val().match(emailRegex) && emailLastVal != $(self).val()) {
                            $('.btn.btn-primary.bootbox-accept').prop('disabled', true);
                            emailLastVal = $(self).val();
                            lastStat = true;
                            ajaxPost(
                                null,
                                baseUrl+`/survey/answer/checkemailuser/`,
                                {
                                    id : params.id,
                                    form : params.form,
                                    email : $(self).val()
                                }
                                , function(res){
                                    if(res && res.duplicated) {
                                        $('.btn.btn-primary.bootbox-accept').prop('disabled', true);
                                        lastStat = false;
                                        var msgError = trad.AnActiveAccountExists;
                                        var btnHTML = `
                                            <a href="javascript:;" class="btn btn-default bg-white letter-blue bold margin-top-10" data-target="#modalLogin" data-toggle="modal" data-dismiss="modal">
                                                <i class="fa fa-sign-in"></i>
                                                ${tradCms.login}
                                            </a>`;
                                        if(res && res.tobeactivated) {
                                            msgError = trad.ATemporaryAccountExists;
                                            lastStat = false;
                                            var btnHTML = `
                                                <a href="javascript:;" class="btn btn-default bg-white letter-blue bold margin-top-10 openModalResendEmail" data-toggle="modal" data-target="#modalSendActivation" onclick="${$('#modalSendActivation #email2').val($(self).val())};">
                                                    <i class="fa fa-envelope"></i>
                                                    ${trad.ReceiveAnotherValidationEmail}
                                                </a>
                                            `;
                                        }
                                        $('.bootbox-form-error').remove();
                                        $('.bootbox-form').append(`
                                            <div class="bootbox-form-error text-center">
                                                <span class="bg-white alert-danger margin-top-10">${msgError}</span><br />
                                                ${btnHTML}
                                            </div>
                                        `).animate({ opacity:1 }, 300 );
                                        $('.openModalResendEmail').off().on('click', function() {
                                            $('.bootbox.modal.fade.bootbox-prompt').hide()
                                        });
                                    } else if(res && !res.duplicated) {
                                        $('.btn.btn-primary.bootbox-accept').prop('disabled', false);
                                        $('.bootbox-form-error').remove();
                                    }
                                }
                            );
                        } else {
                            if(lastStat) {
                                $('.btn.btn-primary.bootbox-accept').prop('disabled', false);
                                $('.bootbox-form-error').remove();
                            } else
                                $('.btn.btn-primary.bootbox-accept').prop('disabled', true)
                        }
                    })
                    clearInterval(bootboxModal)
                }
            },200)
        });
    })
</script>
