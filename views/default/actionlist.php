<style>
    .container.fixed-breadcrumb-filters,
    .container.fixed-breadcrumb-filters,
    .container.aap-footer,
    .headerSearchleft .countResults span{
        display: none !important;
    }
    .list-aap-container.container{
        margin-bottom : 50px
    }
</style>
<?php
if (!empty(Yii::app()->session["userId"])){ ?>
    <script type="text/javascript">
        var formStandalone = true;
        var checkboxSimpleParams = {
            "onText" : trad.yes,
            "offText" : trad.no,
            "onLabel" : trad.yes,
            "offLabel" : trad.no,
            "labelText" : tradCms.label
        }
        jQuery(document).ready(function() {
            $("#wizardcontainer").removeClass('container').addClass("container-fluid");

        });
    </script>

    <?php
        echo $this->renderPartial("costum.views.custom.appelAProjet.callForProjects.main",
            array(
                //"paramsData" => $paramsData,
                //"blockCms"=>$blockCms,
                //"blockKey" => $blockKey,
                "kunik" => "kunik".rand(),
                "clienturi" => Yii::app()->getRequest()->getBaseUrl(true)."/co/index/slug/".$slug."#welcome.slug.".$el["slug"].".formid.".(string)$form["_id"].".page.list?view=compact",
                "formStandalone" => true,
                "slug" => $slug, //$slug,
                //"clienturi" => Yii::app()->getRequest()->getBaseUrl(true)."/co/index/slug/".$slug."#welcome.slug.".$el["slug"].".formid.".(string)$parentForm["_id"].".page.list?view=compact",
                )
        );
    ?>
<?php } else {
    echo "<h2 class='text-center' style='font-size: 2em;font-weight: 100;'>".Yii::t("common","Connect you")."</h2>";
        if (isset($form["temporarymembercanreply"]) && ($form["temporarymembercanreply"] == true || $form["temporarymembercanreply"]=="true")){
    ?>
            <p class='text-center'>
                <button  type="button" id="connectuserbtn" class="btn btn-primary">
                    intéragir
                </button>
            </p>
            <script type="text/javascript">
                $(function() {
                    var form = <?php echo json_encode($form); ?>;
                    smallMenu.openAjaxHTML(baseUrl+'/co2/aap/getviewbypath/path/survey.views.default.connexionactionlisttpl/params/'+form["_id"]["$id"]);
                    $('#connectuserbtn').off().on('click', function () {
                        smallMenu.openAjaxHTML(baseUrl+'/co2/aap/getviewbypath/path/survey.views.default.connexionactionlisttpl/params/'+form["_id"]["$id"]);
                    });
                });

            </script>
    <?php
        }
}
?>
