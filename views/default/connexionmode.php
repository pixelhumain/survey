<?php
HtmlHelper::registerCssAndScriptsFiles(array(
// '/css/dashboard.css',
'/js/form.js'
), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );

$parentform = $form;
?>
<style type="text/css">
    .formconexmode .container {
        text-align: center;
    }

    .formconexmode .haveaccountbtn {
        width: 250px;
        height: 50px;
        background: #9fbd38;
        border: 0;
        display: inline-block;
        color: #fff;
        font-size: 18px;
        margin: 10px;
    }

    .formconexmode .givemailbtn {
        width: 250px;
        height: 50px;
        background: #333;
        border: 0;
        display: inline-block;
        margin-right: 10px;
        color: #fff;
        font-size: 17px;
        margin: 10px;
    }

    .formconexmode h2 {
        font-weight: 100 !important;
        color: #777;
        font-size: 48px;
    }

    .formconexmode h4 {
        font-weight: 100;
        color: #777;
    }

    .formconexmode p {
        font-size: 15px !important;
    }


</style>

<div class="row formconexmode">
    <?php
    use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;
    $timezone = !empty(Yii::app()->session["timezone"]) ? Yii::app()->session["timezone"] : "UTC";
    date_default_timezone_set($timezone);
    $startDate = !empty($form["startDateNoconfirmation"]) ? $form["startDateNoconfirmation"]->toDateTime()->format('c') : null;
    $endDate = !empty($form["endDateNoconfirmation"]) ? $form["endDateNoconfirmation"]->toDateTime()->format('c') : null;
    $now = date('d-m-Y H:i', time());
    $checkDate = Api::checkDateStatus($now,$startDate,$endDate);
    if(
        (
            ( !isset($form["session"]) || $form["session"] == false ) &&
            ($checkDate == "include" )
        ) ||
        (
            (isset($form["session"]) && $form["session"] == true && isset($form["sessionMapping"]) && isset($form["actualSession"])) &&
            (
                    (empty($answer) || !is_array($answer)) ||
                    (!empty($answer) || is_array($answer) && $answerApi::path_get_value($answer, $form["sessionMapping"] , '.') == null || Api::path_get_value($answer, $form["sessionMapping"] , '.') == $form["actualSession"])
            ) &&
            ($checkDate == "include" )
        )
    )
    {
    ?>
    <div class="container">
        <h2>Identifiez-vous</h2>
        <h4>pour répondre au formulaire</h4>

        <div class="row">
            <div class="col-md-6 col-sm-12">
                <button class="haveaccountbtn " data-target="#modalLogin" data-toggle="modal" data-dismiss="modal" >J'ai déjà un compte</button>
                <p>Répondre en se connectant avec un compte communecter</p>
            </div>
            <div class="col-md-6 col-sm-12">
                <button class="givemailbtn">Repondre sans avoir un compte</button>
                <p>Répondre en fournissant un mail, qui va recevoir une invitation à rejoindre la plateforme </p>
            </div>
        </div>

    </div>
    <?php
    } else {
        if(time() < strtotime(str_replace("/","-",$form["startDateNoconfirmation"])) )
            echo "<div class='container'><h3>Les réponses commenceront le ".$form["startDateNoconfirmation"]."</h3></div>";
        else if ( time() > strtotime(str_replace("/","-",$form["endDateNoconfirmation"])))
            echo "<div class='container'><h3>Le temps de réponses est écoulé </h3></div>";
    }
    ?>
</div>

<?php
$form=(is_array($form) && isset($form["_id"])) ? (string)$form["_id"] : (string)$form;
$connexionmodeParams = [
    "id" => $id,
    "answer" => $answer,
    "mode" => $mode,
    "form" => $form
];
if(isset($redirect)) {
    $connexionmodeParams['redirect'] = $redirect;
}

//var_dump($form);
?>

<script type="text/javascript">
    $(function(){
        var  copyFormObj =formObj.init();
        $('.givemailbtn').off().on('click',function(){
            $this = $(this);
            var params = <?php echo json_encode($connexionmodeParams); ?>;
            mylog.log("params.form",params.form);
            bootbox.prompt({
                title: " votre adresse Email",
                /*message: '<p>Please select an option below:</p>',*/
                callback: function (result) {
                    if(result){
                        ajaxPost(
                            null,
                            baseUrl+`/survey/answer/answerwithemail/`,
                            {
                                id : params.id,
                                form : params.form,
                                email : result,
                                resLocation : params['redirect'] ? params['redirect'] : ""
                            }
                            , function(res){

                            if(res.status && notNull(res.location)){
                               window.location.assign(res.location);
                               window.location.reload();
                            }else{
                                toastr.error(res.msg);
                            }
                            },
                            null,
                            null,
                            {"async": false}
                        )
                    }
                }
            });
            var emailLastVal = "";
            var lastStat = false;
            var bootboxModal = setInterval(function() {
                if($('.bootbox.modal.fade.bootbox-prompt').is(':visible')) {
                    const emailRegex = new RegExp(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g);
                    $('.bootbox-input.bootbox-input-text.form-control').on('keyup', delay(function(e) {
                        var self = this;
                        if($(this).val().match(emailRegex) && emailLastVal != $(this).val()) {
                            emailLastVal = $(this).val();
                            lastStat = true;
                            var resVal = false;
                            ajaxPost(
                                null,
                                baseUrl+`/survey/answer/checkemailuser/`,
                                {
                                    id : params.id,
                                    form : params.form,
                                    email : $(self).val()
                                },
                                function(res){
                                    if(res && res.duplicated) {
                                        lastStat = false;
                                        $('.btn.btn-primary.bootbox-accept').prop('disabled', true);
                                        var msgError = trad.AnActiveAccountExists;
                                        var btnHTML = `
                                            <a href="javascript:;" class="btn btn-default bg-white letter-blue bold margin-top-10" data-target="#modalLogin" data-toggle="modal" data-dismiss="modal">
                                                <i class="fa fa-sign-in"></i>
                                                ${tradCms.login}
                                            </a>`;
                                        if(res && res.tobeactivated) {
                                            lastStat = false;
                                            msgError = trad.ATemporaryAccountExists;
                                            var btnHTML = `
                                                <a href="javascript:;" class="btn btn-default bg-white letter-blue bold margin-top-10 openModalResendEmail" data-toggle="modal" data-target="#modalSendActivation" onclick="${$('#modalSendActivation #email2').val($(self).val())};">
                                                    <i class="fa fa-envelope"></i>
                                                    ${trad.ReceiveAnotherValidationEmail}
                                                </a>
                                            `;
                                        }
                                        $('.bootbox-form-error').remove();
                                        $('.bootbox-form').append(`
                                            <div class="bootbox-form-error text-center">
                                                <span class="bg-white alert-danger margin-top-10">${msgError}</span><br />
                                                ${btnHTML}
                                            </div>
                                        `).animate({ opacity:1 }, 300 );
                                        $('.openModalResendEmail').off().on('click', function() {
                                            $('.bootbox.modal.fade.bootbox-prompt').hide()
                                        });
                                        resVal = e.which !== 13;
                                    } else if(res && !res.duplicated) {
                                        $('.btn.btn-primary.bootbox-accept').prop('disabled', false);
                                        $('.bootbox-form-error').remove();
                                        resVal = true
                                    }
                                },
                                null,
                                null,
                                {
                                    async:false
                                }
                            );
                            return resVal;
                        } else {
                            if(lastStat) {
                                $('.btn.btn-primary.bootbox-accept').prop('disabled', false);
                                $('.bootbox-form-error').remove();
                                return lastStat
                            } else {
                                $('.btn.btn-primary.bootbox-accept').prop('disabled', true);
                                return e.which !== 13;
                            }
                        }
                    }, 350));
                    $('.bootbox-input.bootbox-input-text.form-control').on('blur', function() {
                        var self = this;
                        if($(self).val() == '') {
                            $('.btn.btn-primary.bootbox-accept').prop('disabled', true)
                        } else if($(self).val().match(emailRegex) && emailLastVal != $(self).val()) {
                            $('.btn.btn-primary.bootbox-accept').prop('disabled', true);
                            emailLastVal = $(self).val();
                            lastStat = true;
                            ajaxPost(
                                null,
                                baseUrl+`/survey/answer/checkemailuser/`,
                                {
                                    id : params.id,
                                    form : params.form,
                                    email : $(self).val()
                                }
                                , function(res){
                                    if(res && res.duplicated) {
                                        $('.btn.btn-primary.bootbox-accept').prop('disabled', true);
                                        lastStat = false;
                                        var msgError = trad.AnActiveAccountExists;
                                        var btnHTML = `
                                            <a href="javascript:;" class="btn btn-default bg-white letter-blue bold margin-top-10" data-target="#modalLogin" data-toggle="modal" data-dismiss="modal">
                                                <i class="fa fa-sign-in"></i>
                                                ${tradCms.login}
                                            </a>`;
                                        if(res && res.tobeactivated) {
                                            msgError = trad.ATemporaryAccountExists;
                                            lastStat = false;
                                            var btnHTML = `
                                                <a href="javascript:;" class="btn btn-default bg-white letter-blue bold margin-top-10 openModalResendEmail" data-toggle="modal" data-target="#modalSendActivation" onclick="${$('#modalSendActivation #email2').val($(self).val())};">
                                                    <i class="fa fa-envelope"></i>
                                                    ${trad.ReceiveAnotherValidationEmail}
                                                </a>
                                            `;
                                        }
                                        $('.bootbox-form-error').remove();
                                        $('.bootbox-form').append(`
                                            <div class="bootbox-form-error text-center">
                                                <span class="bg-white alert-danger margin-top-10">${msgError}</span><br />
                                                ${btnHTML}
                                            </div>
                                        `).animate({ opacity:1 }, 300 );
                                        $('.openModalResendEmail').off().on('click', function() {
                                            $('.bootbox.modal.fade.bootbox-prompt').hide()
                                        });
                                    } else if(res && !res.duplicated) {
                                        $('.btn.btn-primary.bootbox-accept').prop('disabled', false);
                                        $('.bootbox-form-error').remove();
                                    }
                                }
                            );
                        } else {
                            if(lastStat) {
                                $('.btn.btn-primary.bootbox-accept').prop('disabled', false);
                                $('.bootbox-form-error').remove();
                            } else
                                $('.btn.btn-primary.bootbox-accept').prop('disabled', true)
                        }
                    })
                    clearInterval(bootboxModal)
                }
            },200)
        });
    })
</script>
