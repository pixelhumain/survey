<?php $cssJS = array(
    '/plugins/jquery.dynForm.js',
    
    '/plugins/jQuery-Knob/js/jquery.knob.js',
    '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
    '/plugins/jquery.dynSurvey/jquery.dynSurvey.js',

	'/plugins/jquery-validation/dist/jquery.validate.min.js',
    '/plugins/select2/select2.min.js' , 
    '/plugins/moment/min/moment.min.js' ,
    '/plugins/moment/min/moment-with-locales.min.js',

    // '/plugins/bootbox/bootbox.min.js' , 
    // '/plugins/blockUI/jquery.blockUI.js' , 
    
    '/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js' , 
    '/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css',
    '/plugins/jquery-cookieDirective/jquery.cookiesdirective.js' , 
    '/plugins/ladda-bootstrap/dist/spin.min.js' , 
    '/plugins/ladda-bootstrap/dist/ladda.min.js' , 
    '/plugins/ladda-bootstrap/dist/ladda.min.css',
    '/plugins/ladda-bootstrap/dist/ladda-themeless.min.css',
    '/plugins/animate.css/animate.min.css',
    // SHOWDOWN
	'/plugins/showdown/showdown.min.js',
	//MARKDOWN
	'/plugins/to-markdown/to-markdown.js',
	'/plugins/select2/select2.min.js' ,
	'/plugins/select2/select2.css'
);

HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);
$cssJS = array(
    '/js/dataHelpers.js',
    '/js/sig/geoloc.js',
    '/js/sig/findAddressGeoPos.js',
    '/js/default/loginRegister.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl() );
$cssJS = array(
'/assets/css/default/dynForm.css',
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->theme->baseUrl);

?>

<?php 
/* ---------------------------------------------
ETAPE DU SCENARIO
used when the answers carries many forms in the scenario
---------------------------------------------- */
foreach ( $form[ "scenario" ] as $step => $dynF ) {
	//echo count(array_keys( $dynF["form"] ));
	
	//prepares dummy data with empty values if none exists for each field of the given scene
	if( !@$answers[$step] )
	{
		$answers["answers"] = array();
		$answers["answers"] = array();
		if( @$f["json"]['jsonSchema']["properties"] )
		{
			foreach ( $f["json"]['jsonSchema']["properties"] as $propkey => $prop ) 
			{
				if (@$prop["properties"]){
					$answers[$step]["answers"][$propkey] = []; 
					$tmp = array();
					foreach ($prop["properties"] as $propki => $propvi) {
						$tmp[$propki] = "";
					}
				}
				else 
					$answers[$step]["answers"][$propkey] = "";
			}
		}
		$answers[$step]["created"] = time();
	} 


	if(@$answers[$step]){  
		$editBtn = "";
		if( isset($dynF["saveElement"]) )
			$editBtn = "<a href='javascript:'  data-form='".$step."' data-step='".$step."' data-type='".$answers[$step]["type"]."' data-id='".$answers[$step]["id"]."' class='editStep btn btn-default'><i class='fa fa-pencil'></i></a>";
		else if(!isset($dynF["arrayForm"]))
			$editBtn = ( !empty($user) && (string)$user["_id"] == Yii::app()->session["userId"] ) 
				? "<a href='javascript:'  data-form='".$step."' data-step='".$step."' class='editStep btn btn-default'><i class='fa fa-pencil'></i></a>" 
				: "";

		$titleIcon = ( isset($dynF['icon']) )? "<i class='fa ".@$dynF['icon']." ".@$dynF['titleClass']."'></i>" : "";

		?>
		
		<div class=" titleBlock col-xs-12 text-center" style="cursor:pointer; background-color: <?php echo (@$form["custom"]["color"]) ? $form["custom"]["color"] : "LightSeaGreen" ; ?>"  >
			<h1> 
			<?php echo $dynF["title"]; ?><i class="fa pull-right <?php echo @$dynF["icon"]; ?>"></i>
			<?php echo $editBtn; ?>
			</h1>
			
		</div>

		<div class='col-xs-12' id='<?php echo $step; ?>'>

		<?php 

			

			echo "<div class='col-xs-12'>";
			
			$head =  '<thead><tr>'.
						'<th>'.Yii::t("common","Question").'</th>'.
						'<th>Réponse</th>'.
					'</tr></thead>';

			if(@@$dynF["arrayForm"]  )
			{
				$arrayKi = (isset($dynF["key"]) )
					? $dynF["key"] 
					: array_keys($dynF["json"]["jsonSchema"]["properties"])[0];

				$head =  '<thead><tr>'.
						'<th>Ajouter une ligne</th>'.
						"<th><a href='javascript:;' data-form='".$step."' data-step='".$step."' data-q='".$arrayKi."' class='addAF btn btn-primary'><i class='fa fa-plus'></i> Ajouter</a></th>".
					'</tr></thead>';

				if( count($answerVa[$arrayKi]) > 0 )
					$head = "";
			}

			echo '<table class="table table-striped table-bordered table-hover  directoryTable" id="panelAdmin">'.
				$head.
				'<tbody class="directoryLines">';
			if( isset($dynF["json"]) || isset($dynF["arrayForm"] ))
			{
				
				$formQ = @$dynF["json"]["jsonSchema"]["properties"];

				foreach ($answers[$step] as $q => $a) 
				{
					if( @$formQ[$q]["inputType"] == "arrayForm" || 
						@$dynF["key"] == $q ){
						
						//Tout les titre du tableau de réponses 
						echo '<tr>';
							if(isset($dynF["properties"]))
								$props = $dynF["properties"];
							else 
								$props = $formQ[$q]["properties"];

							foreach ( $props as $ik => $iv) {
								echo "<th>".( ( is_string($iv) ) ? $iv : $iv["placeholder"] )."</th>";
							}

							echo "<th><a href='javascript:;' data-form='".$step."' data-step='".$step."' data-q='".$q."' class='addAF btn btn-primary'><i class='fa fa-plus'></i> Ajouter</a></th>";
						echo '</tr>';

						//Toutes les réponses du tableau 
						foreach ($a as $sq => $sa) {
							echo '<tr>';
								
								foreach ($props as $ik => $iv) {
									//chaque propriété a sa réponse 
									echo "<td>".@$sa[$ik]."</td>";
								}
								echo "<td>".
									"<a href='javascript:;' data-form='".$step."' data-step='".$step."' data-q='".$q."' data-pos='".$sq."' class='editAF btn btn-default'><i class='fa fa-pencil'></i></a> ".
									"<a href='javascript:;' data-form='".$step."' data-step='".$step."' data-q='".$q."' data-pos='".$sq."' class='deleteAF btn btn-danger'><i class='fa fa-times'></i></a>".
								"</td>";
							echo '</tr>';
						}
					}
					else if(is_string($a)){
						echo '<tr>';
							echo "<td>".@$formQ[ $q ]["placeholder"]."</td>";
							$markdown = (strpos(@$formQ[ $q ]["class"], 'markdown') !== false) ? 'markdown' : "";
							echo "<td class='".$markdown."'>".$a."</td>";
						echo '</tr>';
					}else if( isset($a["type"]) && $a["type"]==Document::COLLECTION){

						$document=Document::getById($a["id"]);
						//var_dump($answers[$step]["answers"][$step]);
						$answers[$step]["answers"][$step]["files"]=$document;
						$path=Yii::app()->getRequest()->getBaseUrl(true)."/upload/communecter/".$document["folder"]."/".$document["name"];
						echo '<tr>';
							echo "<td>".@$formQ[ $q ]["placeholder"]."</td>";
							echo "<td>";
								echo "<a href='".$path."' target='_blank'><i class='fa fa-file-pdf-o text-red'></i> ".$document["name"]."</a>";
							echo "</td>";
						echo '</tr>';
					}else if(is_array( $a )){
						echo '<tr>';
							echo "<td>".@$formQ[ $q ]["placeholder"]."</td>";
							echo "<td>";
							$sep = "";
							foreach ($a as $sa => $sv) {
								if( is_array($sv) )
									$sv = implode(",", $sv);
								echo $sv.$sep;
								$sep = ", ";
							}
							echo "</td>";
						echo '</tr>';
						
					}
				}
			}
			//the scene is dynform.js : ex: project, organization
			else if ( isset($dynF["saveElement"]) )
			{
				$el = Element::getByTypeAndId( $answers[$step]["type"] , $answers[$step]["id"] );

				echo '<tr>';
					echo "<td> ".Yii::t("common","Name")."</td>";
					echo "<td> <a target='_blank' class='btn btn-default' href='".Yii::app()->createUrl("#@".@$el["slug"]).".view.detail'>".$el["name"]."</a></td>";
				echo '</tr>';

				if(isset($el["type"])){
					echo '<tr>';
						echo "<td>".Yii::t("common","Type")."</td>";
						echo "<td>".$el["type"]."</td>";
					echo '</tr>';
				}

				if(isset($el["description"])){
					echo '<tr>';
						echo "<td>".Yii::t("common", "Description")."</td>";
						echo "<td>".$el["description"]."</td>";
					echo '</tr>';
				}

				if(isset($el["tags"])){
					echo '<tr>';
						echo "<td>".Yii::t("common","Tags")."</td>";
						echo "<td>";
						$it=0;
						foreach($el["tags"] as $tags){
							if($it>0)
								echo ", ";
							echo "<span class='text-red'>#".$tags."</span>";
							$it++;
						}
						echo "</td>";
					echo '</tr>';
				}

				if(isset($el["shortDescription"])){
					echo '<tr>';
						echo "<td>".Yii::t("common","Short description")."</td>";
						echo "<td>".$el["shortDescription"]."</td>";
					echo '</tr>';
				}

				if(isset($el["email"])){
					echo '<tr>';
						echo "<td>".Yii::t("common","Email")."</td>";
						echo "<td>".$el["email"]."</td>";
					echo '</tr>';
				}
				
				if(!empty($el["profilImageUrl"])){
					echo '<tr>';
						echo "<td>".Yii::t("common","Profil image")." </td>";
						echo "<td><img src='".Yii::app()->createUrl($el["profilImageUrl"])."' class='img-responsive'/></td>";
					echo '</tr>';
				}

				if(isset($el["url"])){
					echo '<tr>';
						echo "<td>".Yii::t("common","Website URL")."</td>";
						echo "<td><a href='".$el["url"]."'>".$el["url"]."</a></td>";
					echo '</tr>';
				}

				echo '<tr>';
					echo "<td>".Yii::t("common","Edit")."</td>";
					echo "<td>".$editBtn."</td>";
				echo '</tr>';
			}
			echo "</tbody></table></div>";
		
	} else { ?>
	<div class="bg-red col-xs-12 text-center text-large text-white margin-bottom-20">
		<h1> <?php echo $step;?> </h1>
	<?php 
		echo "<h3 style='' class=''> <i class='fa fa-2x fa-exclamation-triangle'></i> ".Yii::t("surveys","This step {num} hasn't been filed yet",array('{num}'=>$step))."</h3>".
			"<a href='".Yii::app()->createUrl('survey/co/index/id/'.$step)."' class='btn btn-success margin-bottom-10'>".Yii::t("surveys","Go back to this form")."</a>";

	}
	echo "</div>";

//var_dump($projectsDetails);
?>

<script type="text/javascript">
//if(typeof form == "undefined ")
var form = <?php echo json_encode($form); ?>;
var formSession = "<?php //echo $_GET["session"]; ?>";
//if(typeof answers == "undefined ")
var answers  = <?php echo json_encode($answers); ?>;
var projects  = <?php echo json_encode(@$projects); ?>;
var projectsDetails  = <?php echo json_encode(@$projectsDetails); ?>;
var projectsList = {};
var projectsLink = {};
var scenarioKey = "scenario";
var answerCollection = "<?php echo @$answerCollection ?>";
var answerIdx = "<?php echo $_GET["id"] ?>";

$(document).ready(function() { 
      mylog.log("render","/modules/survey/views/co/answerScenario.php")
  

	if(projects != null){
		$.each(projects,function(i,el) {
			if(typeof answers.links != "undefined" &&
				typeof answers.links.projects != "undefined" &&
				typeof answers.links.projects[i] != "undefined")
				projectsLink[i] = el;
			else
				projectsList[i] = el;
		});
	}
	
	
	
	$('#doc').html( dataHelper.markdownToHtml( $('#doc').html() ) );
	
	$.each($('.markdown'),function(i,el) { 
		$(this).html( dataHelper.markdownToHtml( $(this).html() ) );	
	});

	$('.editStep').off().click(function() { 
		alert('.editStep : '+$(this).data("form"));
		//editing typed elements like projects, organizations
		if( $(this).data("type") )
		{
			mylog.log('.editStep 1',$(this).data("form"),$(this).data("type"),$(this).data("id"));
			var dfCostum = ( form[scenarioKey][ $(this).data("form") ]['costum']) ? form[scenarioKey][ $(this).data("form") ]['costum']['dynFormCostum'] : null;

			dyFObj.editElement( $(this).data("type"), $(this).data("id"), $(this).data("type"),dfCostum );
		}
		else 
		{
			mylog.log('.editStep 2',$(this).data("form"),$(this).data("type"),$(this).data("step"));
			updateForm = {
				form : $(this).data("form"),
				step : $(this).data("step")	
			};

			mylog.log("path",scenarioKey,$(this).data("form"));
			var editForm = form[scenarioKey][$(this).data("form")].json;

			editForm.jsonSchema.onLoads = {
				onload : function(){
					dyFInputs.setHeader("bg-dark");
					$('.form-group div').removeClass("text-white");
					dataHelper.activateMarkdown(".form-control.markdown");
				}
			};

			editForm.jsonSchema.save = function(){
				alert("save");
				data = {
	    			formId : $(this).data("form"),
	    			answerId : answerIdx,
	    			//session : formSession,
	    			answerSection : "answers."+form.id+".answers."+$(this).data("form") ,
	    			answers : arrayForm.getAnswers(editForm , true)
	    		};
	    		
	    		var urlPath = baseUrl+"/survey/co/update";
	    		if(answerCollection) {
	    			//use case when answers are in a different collection than answers
	    			//ex : ficheAction
	    			data.collection = answerCollection;
	    			data.id = data.answerId;
	    			urlPath = baseUrl+"/survey/co/update2";
	    			//alert("answerCollection : "+answerCollection+data.id)
	    		}
	    		mylog.log("editStep save!!!",data);

	    		$.ajax({ type: "POST",
			        url: urlPath,
			        data: data,
					type: "POST",
			    }).done(function (data) {
			    	//alert("done")
			    	if( $('.fine-uploader-manual-trigger').fineUploader('getUploads').length == 0 ){
				    	updateForm = null;
				    }
				    window.location.reload();
			    });
			    
			};
			var editData = answers[$(this).data("form")];

			
			dyFObj.editStep( editForm , editData);	
		}
	});

	$('.deleteAF').off().click(function() { 
		arrayForm.del($(this).data("form"),$(this).data("step"),$(this).data("q"),$(this).data("pos"));
	});

	$('.addAF').off().click(function() { 
		arrayForm.add( $(this).data("form"),$(this).data("step"),$(this).data("q"));
	});

	$('.editAF').off().click(function() { 
		arrayForm.edit($(this).data("form"),$(this).data("step"),$(this).data("q"),$(this).data("pos"));
	});
});

</script>

<?php } ?>