<?php $cssJS = array(
    
    '/plugins/jquery.dynForm.js',
    
    '/plugins/jQuery-Knob/js/jquery.knob.js',
    '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
    //'/plugins/SmartWizard/dist/js/jquery.smartWizard.js',
    //'/plugins/SmartWizard/dist/css/smart_wizard.min.css',
    '/plugins/jquery.dynSurvey/jquery.dynSurvey.js',

	'/plugins/jquery-validation/dist/jquery.validate.min.js',
    '/plugins/moment/min/moment.min.js' ,
    '/plugins/moment/min/moment-with-locales.min.js',

    // '/plugins/bootbox/bootbox.min.js' , 
    // '/plugins/blockUI/jquery.blockUI.js' , 
    
    '/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js' , 
    '/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css',
    '/plugins/jquery-cookieDirective/jquery.cookiesdirective.js' , 
    '/plugins/ladda-bootstrap/dist/spin.min.js' , 
    '/plugins/ladda-bootstrap/dist/ladda.min.js' , 
    '/plugins/ladda-bootstrap/dist/ladda.min.css',
    '/plugins/ladda-bootstrap/dist/ladda-themeless.min.css',
    '/plugins/animate.css/animate.min.css',
    // SHOWDOWN
	'/plugins/showdown/showdown.min.js',
	//MARKDOWN
	'/plugins/to-markdown/to-markdown.js',
	'/plugins/select2/select2.min.js' ,
	'/plugins/select2/select2.css',
);

HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);
$cssJS = array(
    '/js/dataHelpers.js',
    '/js/sig/geoloc.js',
    '/js/sig/findAddressGeoPos.js',
    '/js/default/loginRegister.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl() );
$cssJS = array(
'/assets/css/default/dynForm.css',

);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->theme->baseUrl);



if( $this->layout != "//layouts/empty"){
	$layoutPath = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.';
	echo $this->renderPartial($layoutPath.'header',array("page"=>"ressource","layoutPath"=>$layoutPath));
}

$canSuperAdmin = Form::canSuperAdmin($form["id"],$session, $form, $adminForm);
$showStyle = ( $canAdmin ) ? "display:none; " : "";

$pageParams = array(
	"adminAnswers"=>$answer,
	"adminForm"=>$adminForm,
	"form" => $form,
	"parent" => $parent,
	"user" => $user,
	"prioKey" => @$adminForm['key'],
	"canAdmin" => $canAdmin,
	"canSuperAdmin" => $canSuperAdmin,
	"session"=>$session
); 
if(isset($answer['answers']))
	$pageParams["answers"] = $answer['answers'][$answer["formId"]]['answers'];

$ct = 0;
$showHide = "";
echo "<div style='margin-top:60px'>";
	$page = (isset($form["costum"]["editForm"])) ? $form["costum"]["editForm"] : "dossierEdit2";
	$page = "survey.views.custom.ctenat.dossierEditCTENat";
	echo $this->renderPartial( $page , $pageParams, true ); 
echo "</div>";
?>

	


<?php 
if(@$form["custom"]['footer']){
	echo $this->renderPartial( $form["custom"]["footer"],array("form"=>$form,"answers"=>$answer['answers']),true);
}

$canSuperAdmin = Form::canSuperAdmin($form["id"],$form["session"],$form, $adminForm);
?>

<script type="text/javascript">
var form = <?php echo json_encode($form); ?>;
var formSession = "<?php echo $session; ?>";

var adminForm = <?php echo json_encode($adminForm); ?>;

var adminAnswers  = <?php echo json_encode($answer); ?>;
var rolesListCustom = <?php echo json_encode(@$roles); ?>;
var canAdmin = "<?php echo $canAdmin; ?>";
var canSuperAdmin = "<?php echo $canSuperAdmin; ?>";
var updateForm = null;

$(document).ready(function() { 
	mylog.log("render","/dev/modules/survey/views/co/answerEdit.php");
	
	uploadObj.formId = form.id;
	uploadObj.answerId = "<?php echo $_GET['id']; ?>";	
	

});

function showTableOrForm(key,type){
	$("."+key+"_DataTables").hide();
	$("#"+key+"_"+type+"Table").show();
}



</script>

 