<?php
    $form = $form ?? null;
    $userSessionId = strval(Yii::app()->session['userId']);
    $config = PHDB::findOneById(Form::COLLECTION, $form, ['name', "config", 'subForms']);
    $subForms = PHDB::findOne(Form::COLLECTION, ["_id" => new MongoId($config['config'])], ["subForms"]);
    
    $steps = [];
    $not = [];
    // types d'inputs à exclure de l'exportation
    $input_exclusions = [
        'tpls.forms.titleSeparator',
        'sectionTitle',
        'tpls.forms.sectionDescription',
        'tpls.forms.cplx.list',
        'tpls.forms.costum.cressReunion.element',
        'tpls.forms.cplx.validateStep'
    ];
    
    // reordonner les étapes et ne prendre que les champs intéressants
    foreach ($subForms['subForms'] as $subFormKey => $subFormValue) {
        $tempStep = [
            "name" => $subFormValue["name"],
            'id'   => $subFormKey
        ];
        $inputs = PHDB::findOne(Form::INPUTS_COLLECTION, ["formParent" => $form, "step" => $subFormKey], ["inputs"]);
        $tempInputs = [];
        $tempOrderedInputs = [];
        
        $inputs["inputs"] = $inputs["inputs"] ?? [];
        foreach ($inputs["inputs"] as $inputKey => $input) {
            if (isset($input["type"])) {
                if (isset($input["positions"][$form])) {
                    // sélectionner la position de l'élément dans le formulaire
                    $position = intval($input["positions"][$form]);
                    
                    $tempOrderedInputs[] = [
                        'id'         => (string)$inputs['_id'],
                        "clef"       => $inputKey,
                        "label"      => isset($input['csvfield']) && isset($input['csvfield'][$userSessionId]) ? $input['csvfield'][$userSessionId] : (!empty($input["label"]) ? $input["label"] : $inputKey),
                        "position"   => $position,
                        "step"       => $subFormKey,
                        "type"       => $input["type"],
                        'exportable' => !in_array($input["type"], $input_exclusions)
                    ];
                } else {
                    $tempInputs[] = [
                        'id'         => (string)$inputs['_id'],
                        "clef"       => $inputKey,
                        "label"      => isset($input['csvfield']) && isset($input['csvfield'][$userSessionId]) ? $input['csvfield'][$userSessionId] : (!empty($input["label"]) ? $input["label"] : $inputKey),
                        "position"   => -1,
                        "step"       => $subFormKey,
                        "type"       => $input["type"],
                        'exportable' => !in_array($input["type"], $input_exclusions)
                    ];
                }
            }
        }
        
        $permutation = true;
        // reordonner selon les ordres fournis dans la base de données
        do {
            $permutation = false;
            $length = count($tempOrderedInputs);
            for ($i = 0; $i < $length - 1; $i++) {
                if ($tempOrderedInputs[$i]["position"] > $tempOrderedInputs[$i + 1]["position"]) {
                    $permutation = true;
                    $_temp = $tempOrderedInputs[$i];
                    $tempOrderedInputs[$i] = $tempOrderedInputs[$i + 1];
                    $tempOrderedInputs[$i + 1] = $_temp;
                }
            }
        } while ($permutation);
        
        $tempInputs = array_merge($tempOrderedInputs, $tempInputs);
        $tempStep["inputs"] = $tempInputs;
        $steps[] = $tempStep;
    }
?>
<style>
    .modal {
        z-index: 100000;
    }

    .tab-pane {
        padding: 5px;
        border: 1px rgba(0, 0, 0, .1) solid;
        border-top: transparent;
    }

    .reorderable-input {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        border: 1px solid transparent;
        box-sizing: border-box;
        border-radius: 6px;
        padding: 5px;
        margin: 0;
    }

    .reorderable-input:hover {
        background-color: rgba(0, 0, 0, .05);
    }

    .reorderable-input:hover .dragger,
    .reorderable-input:hover .editor {
        display: inline-block;
        margin-left: 5px;
    }

    .input-value,
    .dragger,
    .editor {
        align-self: center;
    }

    .dragger,
    .editor {
        display: none;
        justify-self: flex-end;
        padding: 5px;
        border-radius: 5px;
    }

    .dragger {
        background-color: rgba(0, 0, 0, .2);
    }

    .dragger:hover {
        cursor: grab;
    }

    .editor {
        background-color: #28a745;
        color: white;
        cursor: pointer;
    }

    .ghost-element {
        border: 3px rgba(0, 0, 0, .5) dashed;
        padding: 5px;
        border-radius: 6px;
    }

    .action-btn {
        display: flex;
        flex-direction: row;
        align-items: flex-start;
    }

    label.input-value {
        -moz-user-select: none;
        -webkit-user-select: none;
    }
</style>
<script>
    'use strict';

    function load_events() {
        // debut champs de remplacement pour l'édition de champ
        const input_group_field_editor = document.createElement('div');
        input_group_field_editor.classList.add('input-group');
        input_group_field_editor.style.flex = '1';

        const input_field_container = document.createElement('input');
        input_field_container.classList.add('form-control');
        input_field_container.classList.add('export-field-name-editor');
        input_field_container.type = 'text';
        const editor_button_container = document.createElement('div');
        editor_button_container.classList.add('input-group-btn');
        const cancel_editor = document.createElement('button');
        const confirm_editor = document.createElement('button');
        cancel_editor.classList.add('btn');
        cancel_editor.classList.add('btn-danger');

        const cancel_icon = document.createElement('i');
        cancel_icon.classList.add('fa');
        cancel_icon.classList.add('fa-times');
        cancel_editor.appendChild(cancel_icon);

        confirm_editor.classList.add('btn');
        confirm_editor.classList.add('btn-primary');

        const save_icon = document.createElement('i');
        save_icon.classList.add('fa');
        save_icon.classList.add('fa-save');
        confirm_editor.appendChild(save_icon);

        editor_button_container.appendChild(cancel_editor);
        editor_button_container.appendChild(confirm_editor);
        input_group_field_editor.appendChild(input_field_container);
        input_group_field_editor.appendChild(editor_button_container);
        // fin champs de remplacement pour l'édition de champ

        // début block fictif pour le déplacement d'un élément
        const ghost_reordable_input = document.createElement('div');
        ghost_reordable_input.classList.add('ghost-element');
        // fin block fictif pour le déplacement d'un élément

        const tab_pane_elements = [...document.querySelectorAll('.tab-pane')];
        const reorderable_input_containers = [...document.querySelectorAll('.reorderable-input')];
        const reorderable_inputs = [...document.querySelectorAll('.reorderable-input input[type=checkbox]:not(:disabled)')];
        const editor_button_elements = [...document.querySelectorAll('.editor')];
        const dragger_elements = [...document.querySelectorAll('.dragger')];
        const select_all_elements = [...document.querySelectorAll('.select-all')];
        const csv_delimiter = document.getElementById('csv-delimiter');

        let current_selected_tab = null;
        let y_mouse_location = 0;
        let after_element = null;

        reorderable_input_containers.forEach(reorderable_input => {
            reorderable_input.addEventListener('dragstart', function (e1) {
                reorderable_input.classList.add('dragging');
            });
            reorderable_input.addEventListener('dragend', function (e1) {
                current_selected_tab.removeChild(ghost_reordable_input);
                dragger_elements.forEach(dragger => dragger.removeAttribute('style'));
                reorderable_input_containers.forEach(element => element.removeAttribute('style'));
                reorderable_input.removeAttribute('draggable');
                reorderable_input.classList.remove('dragging');
                if (after_element == null) current_selected_tab.appendChild(this);
                else current_selected_tab.insertBefore(this, after_element);
                save_csv_exportation_settings();
            });
        });

        reorderable_inputs.forEach(reorderable_input => {
            reorderable_input.addEventListener('change', function (e) {
                const is_checked = this.checked;
                const its_parent = document.querySelector(`[data-target="${this.dataset.parent}"]`);
                const children = [...document.querySelectorAll(`.csv-field-export[data-parent="${this.dataset.parent}"]:not(:disabled)`)];
                const something_is_different = children.find(child => child.checked !== is_checked);
                if (typeof something_is_different === 'undefined') {
                    its_parent.checked = is_checked;
                    its_parent.indeterminate = false;
                } else {
                    its_parent.checked = false;
                    its_parent.indeterminate = true;
                }
                save_csv_exportation_settings();
            });
        });

        tab_pane_elements.forEach(tab_pane => {
            tab_pane.addEventListener('dragover', function (e1) {
                e1.preventDefault();
                current_selected_tab = tab_pane;
                y_mouse_location = e1.clientY;
                after_element = get_drag_after_element(current_selected_tab, y_mouse_location);
                const dragging_element = document.querySelector('.dragging');
                reorderable_input_containers.forEach(reorderable_input => {
                    reorderable_input.style.background = 'transparent';
                    [...reorderable_input.childNodes].forEach(function (child) {
                        if (typeof child.classList !== 'undefined' && child.classList.contains('dragger')) {
                            child.style.display = 'none';
                        }
                    });
                });
                dragging_element.style.display = 'flex';
                ghost_reordable_input.style.height = dragging_element.offsetHeight + 'px';
                ghost_reordable_input.style.width = dragging_element.offsetWidth + 'px';
                dragging_element.style.display = 'none';
                if (after_element == null) tab_pane.appendChild(ghost_reordable_input);
                else tab_pane.insertBefore(ghost_reordable_input, after_element);
            });
        });

        function get_drag_after_element(container, y) {
            const draggable_elements = [...container.querySelectorAll('.reorderable-input:not(.dragging)')]

            return draggable_elements.reduce((closest, child) => {
                const box = child.getBoundingClientRect()
                const offset = y - box.top - box.height / 2
                if (offset < 0 && offset > closest.offset) {
                    return {
                        offset : offset,
                        element: child
                    }
                } else {
                    return closest
                }
            }, {
                offset: Number.NEGATIVE_INFINITY
            }).element
        }


        dragger_elements.forEach(dragger => {
            dragger.addEventListener('mousedown', () => {
                const parent = dragger.parentNode.parentNode;
                if (!parent.hasAttribute('draggable')) parent.setAttribute('draggable', true);
            });

            dragger.addEventListener('mouseup', () => {
                const parent = dragger.parentNode.parentNode;
                if (parent.hasAttribute('draggable')) parent.removeAttribute('draggable');
            });
        });

        // column editor button
        editor_button_elements.forEach(editor => {
            editor.addEventListener('click', function (e) {
                const inputs = [...this.parentElement.parentElement.childNodes];
                const parent = this.parentElement.parentElement;
                const checkbox = this.parentElement.parentElement.firstElementChild.firstElementChild;
                inputs.forEach(input => {
                    input.remove();
                });

                input_field_container.value = checkbox.dataset.label;
                parent.appendChild(input_group_field_editor);
                input_field_container.focus();
                editor_button_elements.forEach(editorElement => editorElement.style.display = 'none');

                function handle_cancel_edition() {
                    input_group_field_editor.remove();
                    inputs.forEach(input => {
                        input_field_container.value = '';
                        parent.appendChild(input);
                    });
                    cancel_editor.removeEventListener('click', handle_cancel_edition);
                    confirm_editor.removeEventListener('click', handle_save_edition);
                    input_group_field_editor.removeEventListener('keydown', handle_input_keydown);
                    editor_button_elements.forEach(editorElement => editorElement.removeAttribute('style'));
                }

                function handle_save_edition() {
                    const updateData = {
                        id        : checkbox.dataset.id,
                        collection: "inputs",
                        path      : `inputs.${checkbox.value}.csvfield`,
                        value     : {
                            '<?= $userSessionId ?>': input_field_container.value
                        }
                    };
                    dataHelper.path2Value(updateData, function (data) {
                        inputs.forEach(input => {
                            checkbox.dataset.label = input_field_container.value;
                            checkbox.nextSibling.textContent = input_field_container.value;
                            input_group_field_editor.remove();
                            parent.appendChild(input);
                        });
                        cancel_editor.removeEventListener('click', handle_cancel_edition);
                        confirm_editor.removeEventListener('click', handle_save_edition);
                        input_group_field_editor.removeEventListener('keydown', handle_input_keydown);
                        editor_button_elements.forEach(editorElement => editorElement.removeAttribute('style'));
                        save_csv_exportation_settings();
                    });
                }

                function handle_input_keydown(event) {
                    if (event.code === 'Enter')
                        handle_save_edition();
                    else if (event.code === 'Escape')
                        handle_cancel_edition();
                }

                cancel_editor.removeEventListener('click', handle_cancel_edition);
                confirm_editor.removeEventListener('click', handle_save_edition);
                input_group_field_editor.removeEventListener('keydown', handle_input_keydown);


                cancel_editor.addEventListener('click', handle_cancel_edition);
                confirm_editor.addEventListener('click', handle_save_edition);
                input_group_field_editor.addEventListener('keydown', handle_input_keydown);
            });
        });

        select_all_elements.forEach(select_all_element => {
            select_all_element.addEventListener('change', function (e) {
                const parent = this.dataset.target;
                const targets = [...document.querySelectorAll(`[data-parent="${parent}"]:not(:disabled)`)];
                targets.forEach(target => target.checked = this.checked);
                save_csv_exportation_settings();
            });
        });

        function handle_change_delimiter() {
            save_csv_exportation_settings();
        }

        csv_delimiter.removeEventListener('change', handle_change_delimiter);
        csv_delimiter.addEventListener('change', handle_change_delimiter);
    }

    function save_csv_exportation_settings() {
        const form = '<?= $form ?>';
        const default_setting = {
            delimiter: document.getElementById('csv-delimiter').value,
            fields   : []
        };
        const settings = localStorage.getItem(form) == null ? default_setting : JSON.parse(localStorage.getItem(form));
        settings.fields = [];
        [...document.querySelectorAll('.csv-field-export')].forEach(champ => {
            settings.fields.push({
                id        : champ.dataset.id,
                parent    : champ.dataset.parent,
                step      : champ.dataset.step,
                label     : champ.dataset.label,
                type      : champ.dataset.type,
                exportable: !champ.hasAttribute('disabled'),
                value     : champ.value,
                checked   : champ.checked,
            });
        });
        settings.delimiter = document.getElementById('csv-delimiter').value;
        settings.exportation_name = document.getElementById('csv-exportation-name').value;
        settings.take_pagination = document.getElementById('csv-take-pagination').checked;
        localStorage.setItem(form, JSON.stringify(settings));
        toastr.success('Le paramètre est enregistré');
        return false;
    }

    function load_settings() {
        const form = '<?= $form ?>';
        const default_setting = {
            delimiter: document.getElementById('csv-delimiter').value,
            fields   : []
        };
        const steps = JSON.parse(`<?= json_encode($steps) ?>`);
        const temporary_settings = [];
        for (const step of steps) {
            for (const input of step.inputs) {
                temporary_settings.push({
                    id        : input.id,
                    parent    : `csv-${step['id']}`,
                    step      : input.step,
                    label     : input.label,
                    type      : input.type,
                    checked   : true,
                    exportable: input.exportable,
                    value     : input.clef,
                });
            }
        }

        const settings = localStorage.getItem(form) == null ? default_setting : JSON.parse(localStorage.getItem(form));

        const longest = settings.fields.length >= temporary_settings.length ? settings.fields : temporary_settings;
        const to_removes = [];
        const to_adds = [];

        for (let temporary of longest) {
            const setting_find = settings.fields.find(setting => setting.value === temporary.value);
            const temporary_find = temporary_settings.find(setting => setting.value === temporary.value);
            if (typeof setting_find === 'undefined') to_adds.push(temporary);
            if (typeof temporary_find === 'undefined') to_removes.push(temporary);
        }

        for (let to_add of to_adds) {
            const nearest = settings.fields.findIndex(setting => setting['parent'] === to_add['parent']);
            settings.fields.splice(nearest + 1, 0, to_add);
        }

        for (let to_remove of to_removes) {
            const index_to_remove = settings.fields.findIndex(field => field.value === to_remove.value);
            settings.fields.splice(index_to_remove, 1);
        }

        let html = '';
        let last_parent = '';
        const status = {};
        settings.fields.forEach(field => {
            if (last_parent === '') {
                last_parent = field.parent;
                status[last_parent] = [];
                html += `
                <div class="checkbox">
                    <label class="input-value">
                        <input type="checkbox" class="select-all" data-target="${last_parent}">
                        Sélectionner tout
                    </label>
                </div>
                `;
            }
            if (field.parent !== last_parent) {
                const parentElement = document.getElementById(`${last_parent}-tabulation-csv`);
                if (parentElement == null) {
                    localStorage.clear();
                    window.location.reload();
                }
                parentElement.innerHTML = html;
                const check = status[last_parent].filter(checked => checked);
                const select_all_element = document.querySelector(`[data-target="${last_parent}"]`);
                if (check.length === status[last_parent].length) select_all_element.setAttribute('checked', 'checked');
                else select_all_element.indeterminate = check.length != 0 && check.length !== status[last_parent].length;
                last_parent = field.parent;
                status[last_parent] = [];
                html = `
                <div class="checkbox">
                    <label class="input-value">
                        <input type="checkbox" class="select-all" data-target="${last_parent}">
                        Sélectionner tout
                    </label>
                </div>
                `;
            }
            html += `
                <div class="checkbox reorderable-input">
                    <label class="input-value ${!field.exportable ? 'text-danger' : ''}">
                        <input class="csv-field-export" data-parent="${field.parent}" data-step="${field.step}" data-label="${field.label}" data-type="${field.type}" value="${field.value}" data-id="${field.id}" type="checkbox" ${!field.exportable ? 'disabled' : field.checked ? 'checked' : ''}>
                        ${field.label}
                    </label>
                    <div class="action-btn">
                        <i class="fa fa-edit editor"></i>
                        <i class="fa fa-arrows dragger"></i>
                    </div>
                </div>
            `;
            status[last_parent].push(!field.exportable || field.checked);
        });

        if (typeof last_parent !== 'undefined' && last_parent !== '') {
            const parentElement = document.getElementById(`${last_parent}-tabulation-csv`);
            if (parentElement == null) {
                localStorage.clear();
                window.location.reload();
            }
            parentElement.innerHTML = html;
            const check = status[last_parent].filter(checked => checked);
            const select_all_element = document.querySelector(`[data-target="${last_parent}"]`);
            if (check.length === status[last_parent].length) select_all_element.setAttribute('checked', 'checked');
            else select_all_element.indeterminate = check.length != 0 && check.length !== status[last_parent].length;
            html = '';
        }
        document.getElementById('csv-delimiter').value = settings.delimiter;
        document.getElementById('csv-exportation-name').value = typeof settings.exportation_name === 'undefined' ? `<?= $config['name'] ?>` : settings.exportation_name;
        if (typeof settings.address_view !== 'undefined') {
            [...document.querySelectorAll('[name=csv-address-view]')].forEach(element => {
                element.checked = settings.address_view === element.value;
            });
        }
        document.getElementById('csv-take-pagination').checked = typeof settings.take_pagination !== 'undefined' && settings.take_pagination;
    }

    function perform_csv_exportation(element) {
        const spinner = element.firstElementChild;
        spinner.classList.add('fa');
        spinner.classList.add('fa-spinner');
        spinner.classList.add('fa-spin');
        const query = aapObj.filterSearch.proposal.search.obj;
        const data = {
            settings: {
                address       : document.querySelector('[name=csv-address-view]:checked').value,
                takePagination: document.getElementById('csv-take-pagination').checked
            },
            champs  : [],
            query
        };

        [...document.querySelectorAll('.csv-field-export')].forEach(champ => {
            if (champ.checked) {
                data.champs.push({
                    step : champ.dataset.step,
                    label: champ.dataset.label,
                    type : champ.dataset.type,
                    value: champ.value
                });
            }
        });

        $.ajax({
            url     : `${baseUrl}/survey/exportation/csv-aap`,
            method  : 'post',
            dataType: 'json',
            data,
            success : response => {
                spinner.className = '';
                const {
                    Parser
                } = json2csv;
                const fields = response.heads;
                const json2csvParser = new Parser({
                    fields,
                    delimiter: document.getElementById('csv-delimiter').value
                });
                const csv = json2csvParser.parse(response.bodies);
                let typed_name = document.getElementById('csv-exportation-name').value;
                typed_name = typed_name === '' ? `<?= $config['name'] ?>` : typed_name;
                const typed_name_splited = typed_name.split('.');

                var exportedFilenmae = typed_name_splited.length > 1 && typed_name_splited[typed_name_splited.length - 1].toLowerCase() === 'csv' ? typed_name : `${typed_name}.csv`;
                //var universalBOM = "\uFEFF";
                var blob = new Blob([new Uint8Array([0xEF, 0xBB, 0xBF]), csv], {
                    type: 'text/csv;charset=utf-8;'
                });
                if (navigator.msSaveBlob) { // IE 10+
                    navigator.msSaveBlob(blob, exportedFilenmae);
                } else {
                    var link = document.createElement("a");
                    if (link.download !== undefined) { // feature detection
                        // Browsers that support HTML5 download attribute
                        var url = URL.createObjectURL(blob);
                        link.setAttribute("href", url);
                        link.setAttribute("download", exportedFilenmae);
                        link.style.visibility = 'hidden';
                        document.body.appendChild(link);
                        link.click();
                        document.body.removeChild(link);
                    }
                }
            },
            error   : function () {
                spinner.className = '';
                toastr.error("Une erreur s'est produite lors de l'exportation");
            }
        });
        return false;
    }
</script>

<div class="modal fade" id="csv-exportation" style="margin-top: 50px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Exportation des données en CSV</h4>
            </div>
            <div class="modal-body">
                <!-- entete des tabs -->
                <ul class="nav nav-tabs" role="tablist" id="csv-export-headers">
                    <?php
                        foreach ($steps as $index => $step) { ?>
                            <li <?= $index == 0 ? "class=\"active\"" : "" ?>>
                                <a href="#csv-<?= $step["id"] ?>-tabulation-csv" role="tab" data-toggle="tab"><?= $step["name"] ?></a>
                            </li>
                        <?php
                        } ?>
                    <li <?= count($steps) == 0 ? "class=\"active\"" : "" ?>>
                        <a href="#setting-tabulation-csv" role="tab" data-toggle="tab"><i class="fa fa-cog"></i></a>
                    </li>
                </ul>
                <div class="tab-content" id="csv-export-content">
                    <?php
                        foreach ($steps as $index => $step) { ?>
                            <div class="tab-pane <?= $index == 0 ? "active" : "" ?>" id="csv-<?= $step["id"] ?>-tabulation-csv">
                                <div class="checkbox">
                                    <label class="input-value">
                                        <input type="checkbox" class="select-all"
                                               data-target="csv-<?= $step["id"] ?>" <?= count($step["inputs"]) > 0 ? 'checked' : 'disabled' ?>>
                                        Sélectionner tout
                                    </label>
                                </div>
                                <?php
                                    foreach ($step["inputs"] as $step_value) { ?>
                                        <div class="checkbox reorderable-input">
                                            <label class="input-value <?= !$step_value["exportable"] ? 'text-danger' : '' ?>">
                                                <input class="csv-field-export" data-parent="csv-<?= $step["id"] ?>"
                                                       data-step="<?= $step_value["step"] ?>" data-label="<?= $step_value["label"] ?>"
                                                       data-type="<?= $step_value["type"] ?>" data-id="<?= $step_value['id'] ?>"
                                                       value="<?= $step_value["clef"] ?>"
                                                       type="checkbox" <?= !$step_value["exportable"] ? 'disabled' : 'checked' ?>>
                                                <?= $step_value["label"] ?>
                                            </label>
                                            <div class="action-btn">
                                                <i class="fa fa-edit editor"></i>
                                                <i class="fa fa-arrows dragger"></i>
                                            </div>
                                        </div>
                                    <?php
                                    } ?>
                            </div>
                        <?php
                        } ?>
                    <div class="tab-pane<?= count($steps) == 0 ? ' active' : '' ?>" id="setting-tabulation-csv">
                        <div class="form-group">
                            <label for="csv-delimiter">Délimiteur</label>
                            <select class="form-control" id="csv-delimiter">
                                <option value="," selected>Virgule</option>
                                <option value=";">Point virgule</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="csv-exportation-name">Nom du fichier:</label>
                            <input type="text" class="form-control" id="csv-exportation-name" value="<?= $config['name'] ?>"
                                   onblur="save_csv_exportation_settings()">
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="csv-take-pagination" onchange="save_csv_exportation_settings()"> <b>Prendre en compte la
                                    pagination</b>
                            </label>
                        </div>
                        <span>Paramètre de vue des champs</span>
                        <div class="form-group">
                            <label>Adresse:</label>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="csv-address-view" value="multicolumn" onchange="save_csv_exportation_settings()"
                                           checked> Multicolonne
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="csv-address-view" value="monocolumn" onchange="save_csv_exportation_settings()"> Colonne
                                    unique
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="return perform_csv_exportation(this)"><span></span> Exporter</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
    $(document).ready(function () {
        load_settings();
        load_events();
    })
</script>