<?php
    $form = $form ?? null;
    $user_connecter = strval(Yii::app()->session['userId']);
    $config = PHDB::findOneById(Form::COLLECTION, $form, ['name', 'config', 'subForms']);
    $sub_forms = $config['subForms'] ?? [];
    
    $steps = [];
    $not = [];
    // types d'inputs à exclure de l'exportation
    $input_exclusions = [
        'tpls.forms.titleSeparator',
        'sectionTitle',
        'tpls.forms.sectionDescription',
        'tpls.forms.cplx.list',
        'tpls.forms.costum.cressReunion.element',
        'tpls.forms.cplx.validateStep',
        'tpls.forms.costum.franceTierslieux.validateStepFtl',
        'tpls.forms.costum.franceTierslieux.validateStep',
        'tpls.forms.sectionTitle',
    ];
    
    // reordonner les étapes et ne prendre que les champs intéressants
    foreach ($sub_forms as $sub_form) {
        $db_sub_form = PHDB::findOne(Form::COLLECTION, ['id' => $sub_form], ['id', 'name', 'inputs']);
        $temp_step = [
            'name' => $db_sub_form['name'],
            'id'   => $sub_form
        ];
        $inputs = $db_sub_form['inputs'] ?? [];
        $temp_inputs = [];
        $ordered_inputs = [];
        
        foreach ($inputs as $key => $input) {
            if (isset($input['type'])) {
                $label = $key;
                
                if (!empty($input['label'])) $label = $input['label'];
                else if (!empty($input['info'])) $label = $input['info'];
                
                $label = htmlspecialchars($label);
                
                if (isset($input['position'])) {
                    // sélectionner la position de l'élément dans le formulaire
                    $position = intval($input["position"]);
                    
                    $ordered_inputs[] = [
                        'id'         => (string)$db_sub_form['_id'],
                        'clef'       => $key,
                        'label'      => $label,
                        'position'   => $position,
                        'step'       => $sub_form,
                        'type'       => $input["type"],
                        'exportable' => !in_array($input['type'], $input_exclusions)
                    ];
                } else {
                    $temp_inputs[] = [
                        'id'         => (string)$db_sub_form['_id'],
                        'clef'       => $key,
                        'label'      => $label,
                        'position'   => -1,
                        'step'       => $sub_form,
                        'type'       => $input['type'],
                        'exportable' => !in_array($input['type'], $input_exclusions)
                    ];
                }
            }
        }
        
        $permutation = true;
        // reordonner selon les ordres fournis dans la base de données
        do {
            $permutation = false;
            $length = count($ordered_inputs);
            for ($i = 0; $i < $length - 1; $i++) {
                if ($ordered_inputs[$i]['position'] > $ordered_inputs[$i + 1]['position']) {
                    $permutation = true;
                    [$ordered_inputs[$i], $ordered_inputs[$i + 1]] = [$ordered_inputs[$i + 1], $ordered_inputs[$i]];
                }
            }
        } while ($permutation);
        
        $temp_step["inputs"] = [...$ordered_inputs, ...$temp_inputs];
        $steps[] = $temp_step;
    }
?>
<style>
    .modal {
        z-index: 100000;
    }

    @media only screen and (min-width: 768px) {
        #csv-exportation .modal-dialog {
            width: 600px !important;
            margin: 30px auto !important;
        }
    }

    .tab-pane {
        padding: 5px;
        border: 1px rgba(0, 0, 0, .1) solid;
        border-top: transparent;
    }

    .reorderable-input {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        border: 1px solid transparent;
        box-sizing: border-box;
        border-radius: 6px;
        padding: 5px;
        margin: 0;
    }

    .reorderable-input:hover {
        background-color: rgba(0, 0, 0, .05);
    }

    .reorderable-input:hover .dragger,
    .reorderable-input:hover .editor {
        display: inline-block;
        margin-left: 5px;
    }

    .input-value,
    .dragger,
    .editor {
        align-self: center;
    }

    .dragger,
    .editor {
        display: none;
        justify-self: flex-end;
        padding: 5px;
        border-radius: 5px;
    }

    .dragger {
        background-color: rgba(0, 0, 0, .2);
    }

    .dragger:hover {
        cursor: grab;
    }

    .editor {
        background-color: #28a745;
        color: white;
        cursor: pointer;
    }

    .ghost-element {
        border: 3px rgba(0, 0, 0, .5) dashed;
        padding: 5px;
        border-radius: 6px;
    }

    .action-btn {
        display: flex;
        flex-direction: row;
        align-items: flex-start;
    }

    label.input-value {
        user-select: none;
        -moz-user-select: none;
        -webkit-user-select: none;
    }
    
    .nav-tab-inline {
        display: flex;
        flex-direction: row;
        overflow: auto hidden;
    }

    .nav-tab-inline li {
        white-space: nowrap;
    }
</style>
<div class="modal fade" id="csv-exportation" style="margin-top: 50px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Exportation des données en CSV</h4>
            </div>
            <div class="modal-body">
                <!-- entete des tabs -->
                <ul class="nav nav-tabs" role="tablist" id="csv-export-headers">
                    <?php
                        foreach ($steps as $index => $step) { ?>
                            <li <?= $index == 0 ? "class=\"active\"" : "" ?>>
                                <a href="#csv-<?= $step["id"] ?>-tabulation-csv" role="tab" data-toggle="tab"><?= $step["name"] ?></a>
                            </li>
                            <?php
                        } ?>
                    <li <?= count($steps) == 0 ? "class=\"active\"" : "" ?>>
                        <a href="#setting-tabulation-csv" role="tab" data-toggle="tab"><i class="fa fa-cog"></i></a>
                    </li>
                </ul>
                <div class="tab-content" id="csv-export-content">
                    <?php
                        foreach ($steps as $index => $step) { ?>
                            <div class="tab-pane <?= $index == 0 ? "active" : "" ?>" id="csv-<?= $step["id"] ?>-tabulation-csv">
                                <div class="checkbox">
                                    <label class="input-value">
                                        <input type="checkbox" class="select-all"
                                               data-target="csv-<?= $step["id"] ?>" <?= count($step["inputs"]) > 0 ? 'checked' : 'disabled' ?>>
                                        Sélectionner tout
                                    </label>
                                </div>
                                <?php
                                    foreach ($step["inputs"] as $step_value) { ?>
                                        <div class="checkbox reorderable-input">
                                            <label class="input-value <?= !$step_value["exportable"] ? 'text-danger' : '' ?>">
                                                <input class="csv-field-export" data-parent="csv-<?= $step["id"] ?>"
                                                       data-step="<?= $step_value["step"] ?>" data-label="<?= $step_value["label"] ?>"
                                                       data-type="<?= $step_value["type"] ?>" data-id="<?= $step_value['id'] ?>"
                                                       value="<?= $step_value["clef"] ?>"
                                                       type="checkbox" <?= !$step_value["exportable"] ? 'disabled' : 'checked' ?>>
                                                <?= $step_value["label"] ?>
                                            </label>
                                            <div class="action-btn">
                                                <i class="fa fa-edit editor"></i>
                                                <i class="fa fa-arrows dragger"></i>
                                            </div>
                                        </div>
                                        <?php
                                    } ?>
                            </div>
                            <?php
                        } ?>
                    <div class="tab-pane<?= count($steps) == 0 ? ' active' : '' ?>" id="setting-tabulation-csv">
                        <div class="form-group">
                            <label for="csv_delimiter">Délimiteur</label>
                            <select class="form-control" id="csv_delimiter">
                                <option value="," selected>Virgule</option>
                                <option value=";">Point virgule</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="csv_exportation_name">Nom du fichier:</label>
                            <input type="text" class="form-control" id="csv_exportation_name" value="<?= $config['name'] ?>">
                        </div>
                        <h3>Paramètre de vue des champs :</h3>
                        <div class="form-group">
                            <label>Adresse:</label>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="address_view" value="multicolumn" checked>
                                    Multicolonne
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="address_view" value="monocolumn"> Colonne unique
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Question au choix multiple:</label>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="checkbox_view" value="multicolumn" data-target="custom_checkbox_aside_view" checked>
                                    Une réponse par colonne
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="checkbox_view" value="monocolumn" data-target="custom_checkbox_aside_view"> Colonne
                                    unique
                                </label>
                            </div>
                        </div>
                        <div class="checkbox" id="custom_checkbox_aside_view" data-activation-condition="monocolumn" style="display: none">
                            <label>
                                <input type="checkbox" name="custom_checkbox_aside_view"> Mettre les champs personalisés à part
                            </label>
                        </div>
                        <div class="form-group">
                            <label>Structure d'organisation/projet:</label>
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="finder_view" value="complete" data-target="finder_column_view" checked>
                                            Information complète
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="finder_view" value="nameonly" data-target="finder_column_view"> Nom
                                            seulement
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12" id="finder_column_view" data-activation-condition="nameonly" style="display: none;">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="finder_column_view" value="multicolumn" checked> Une structure par colonne
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="finder_column_view" value="monocolumn"> Colonne unique
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h3>Autres :</h3>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="export_following_answer_view" data-target="second_answer_params"> Exporter la deuxième
                                réponse
                            </label>
                        </div>
                        <div class="form-group" id="second_answer_params" data-activation-condition="true" style="display: none;">
                            <label for="second_answer_attribute">Attribut liant à la deuxième réponse</label>
                            <input type="text" class="form-control" id="second_answer_attribute" placeholder="Attribut"
                                   value="followingAnswerId">
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="export_dynform_view" data-target="insert-second-after-selection"> Exporter les
                                informations liées aux organisations
                            </label>
                        </div>
                        <div class="form-group" id="insert-second-after-selection" data-activation-condition="true" style="display: none">
                            <label for="insert-second-after">Insérer le formulaire après :</label>
                            <select id="insert-second-after" class="form-control">
                                <?php
                                    $count = count($steps);
                                    foreach ($steps as $step) {
                                        if (count($step['inputs']) > 0) {
                                            ?>
                                            <optgroup label="<?= $step['name'] ?>">
                                                <?php
                                                    foreach ($step['inputs'] as $step_input) {
                                                        if ($step_input['exportable']) { ?>
                                                            <option value="<?= $step_input['clef'] ?>">
                                                                <?= $step_input['label'] ?>
                                                            </option>
                                                            <?php
                                                        }
                                                    }
                                                ?>
                                            </optgroup>
                                            <?php
                                        }
                                    } ?>
                            </select>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="completed_answers_only_view"> Exporter uniquement les réponses complètes (dernière étape
                                validée)
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="export_user_email_view"> Exporter l'email du répondant
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="export_answer_id_view"> Exporter l'identifiant de la réponse
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="split_steps_view"> Mettre les différentes étapes dans des fichiers séparés
                            </label>
                        </div>
                        <div class="form-group">
			                <label for="fileImport">Mapping intitulé des colonnes et options</label>
			                <input type="file" id="fileImport" name="fileImport" accept=".csv">
		                </div>
                        <div id="additional-settings"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="csv-export"><span></span> Exporter</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    var coform_csv_setting_extension = function(__forms = {}) {
        let html = '';
        $.each(__forms, function (__id, __input) {
            html += `<div class="form-group" data-type="${__input.type}" data-name="${__id}">`;
            if(['text', 'number', 'date', 'email'].includes(__input.type)) {
                html += '';
            } else if(__input.type === 'select') {
                const keys = Object.keys(__input.options);
                const values = __input.value ? [__input.value] : (__input.values ? (__input.is_multiple ? __input.values : __input.values[__input.values.length - 1]) : (keys.length && !__input.is_multiple ? [keys[0]] : []));
                html += `
                    <label >${__input.label ? `${__input.label} :` : 'Choix :'}</label>
                    <select id="${__id}" name="${__id}" class="form-control" ${__input.is_multiple ? 'multiple' : ''}>${keys.map(__key => `<option value="${__key}" ${values.includes(__key) ? 'selected' : ''}>${__input.options[__key]}</option>`).join('')}</select>
                `;
            } else if(__input.type === 'checkbox') {
                const keys = Object.keys(__input.options);
                const values = __input.value ? [__input.value] : (__input.values ? __input.values : []);
                html += `
                    <label >${__input.label ? `${__input.label} :` : 'Choix :'}</label>
                    <div class="row">
                `;
                $.each(keys, function(__index, __key) {
                    if(__index % 2 === 0 && __index !== 0) html += '</div><div class="row">'
                    html += `
                        <div class="col-md-6 col-sm-12">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="${__id}" ${values.includes(__key) ? 'checked' : ''} value="${__key}"> ${__input.options[__key]}
                                </label>
                            </div>
                        </div>
                    `;
                });
                html += '</div>';
            }
            html += '</div>';
        });
        $('#additional-settings').empty().html(html);
    };
    
    (function () {
        /**
         * @typedef Field
         * @type {Object}
         * @property {string} id Identifiant du champ
         * @property {string} parent Nom du parent
         * @property {string} step Nom du step
         * @property {string} label Libellé du champ à exporter
         * @property {string} type Type du champ
         * @property {boolean} checked Coché pour l'export ou pas
         * @property {boolean} exportable Peut être exporté ou se trouve dans les listes des champs à exclure
         * @property {boolean} value Valeur du champ
         */
        /**
         * @typedef Storage
         * @type {Object}
         * @property {string} delimiter Délimiteur lors de l'éxport
         * @property {string} second_answer_attribute Non de l'attribut qui permet de lier à la deuxième réponse
         * @property {string} exportation_name Nom du fichier d'exportation
         * @property {boolean} is_exporting_answer_id Exporter ou pas l'identifiant de l'answer
         * @property {boolean} is_exporting_second_answer Exporter ou non la deuxième réponse liée à la réponse principale
         * @property {boolean} export_dynform_view Exporter ou non le dynform lié à la première réponse
         * @property {string} insert_dynform_after Nom du champ qui précède le dynform de la réponse
         * @property {boolean} export_answer_id_view Exporter ou non l'identifiant de l'answer
         * @property {boolean} completed_answers_only requête uniquement les réponses dont la dernière étape est validée
         * @property {boolean} export_user_email_view Exporter ou non l'email du répondant
         * @property {boolean} custom_checkbox_aside_view Mettre les champs complexes à part
         * @property {boolean} split_steps Séparer les différents étapes dans différents fichiers
         * @property {Field[]} fields
         */
        const view_settings = ['address', 'checkbox', 'finder', 'finder_column'];
        const conditional_checkboxes = ['export_following_answer', 'export_dynform', 'export_answer_id', 'export_user_email', 'custom_checkbox_aside', 'split_steps', 'completed_answers_only'];
        const storage_version = 3.0;

        /**
         * Vérifier si la variable n'existe pas, n'est pas définie ou est vide
         * @param {any} __input Variable à évaluer
         * @returns {boolean}
         */
        function is_empty(__input) {
            return typeof __input==='undefined' || typeof __input==='object' && (__input===null || __input instanceof Array && __input.length===0) || typeof __input==='string' && __input==='';
        }

        /**
         * Créer la clé de stockage
         * @return {string}
         */
        function get_storage_key() {
            const form = '<?= $form ?>';
            return `${form}v${storage_version}`;
        }

        /**
         * Récupérer le paramètre stocké en mémoire
         * @return {Storage}
         */
        function get_storage() {
            const key = get_storage_key();
            const storage = localStorage.getItem(key);
            return typeof storage==='string' && storage.length ? JSON.parse(storage) : {
                fields : [],
                delimiter : document.getElementById('csv_delimiter').value,
                exportation_name : document.getElementById('csv_exportation_name').value,
                is_exporting_second_answer : false,
                second_answer_attribute : 'followingAnswerId',
                is_exporting_answer_id : false,
                export_dynform_view : false,
                export_answer_id_view : false,
                completed_answers_only : false,
                export_user_email_view : false,
                custom_checkbox_aside_view : false,
                split_steps : false,
                insert_dynform_after : ''
            };
        }

        /**
         * Sauver les paramètres dans le navigateur
         * @param {Object} __input Valeur à enregistrer
         * @return {Promise.<Storage>}
         */
        function set_storage(__input) {
            return new Promise(function (__resolve) {
                const key = get_storage_key();
                const storage = get_storage();
                const values = $.extend(true, {}, storage, __input);
                localStorage.setItem(key, JSON.stringify(values));
                __resolve(values);
            });
        }

        function load_events() {
            // debut champs de remplacement pour l'édition de champ
            const input_group_field_editor = document.createElement('div');
            input_group_field_editor.classList.add('input-group');
            input_group_field_editor.style.flex = '1';

            const input_field_container = document.createElement('input');
            input_field_container.classList.add('form-control');
            input_field_container.classList.add('export-field-name-editor');
            input_field_container.type = 'text';
            const editor_button_container = document.createElement('div');
            editor_button_container.classList.add('input-group-btn');
            const cancel_editor = document.createElement('button');
            const confirm_editor = document.createElement('button');
            cancel_editor.classList.add('btn');
            cancel_editor.classList.add('btn-danger');

            const cancel_icon = document.createElement('i');
            cancel_icon.classList.add('fa');
            cancel_icon.classList.add('fa-times');
            cancel_editor.appendChild(cancel_icon);

            confirm_editor.classList.add('btn');
            confirm_editor.classList.add('btn-primary');

            const save_icon = document.createElement('i');
            save_icon.classList.add('fa');
            save_icon.classList.add('fa-save');
            confirm_editor.appendChild(save_icon);

            editor_button_container.appendChild(cancel_editor);
            editor_button_container.appendChild(confirm_editor);
            input_group_field_editor.appendChild(input_field_container);
            input_group_field_editor.appendChild(editor_button_container);
            // fin champs de remplacement pour l'édition de champ

            // début block fictif pour le déplacement d'un élément
            const ghost_reordable_input = document.createElement('div');
            ghost_reordable_input.classList.add('ghost-element');
            // fin block fictif pour le déplacement d'un élément

            const tab_pane_elements = [...document.querySelectorAll('.tab-pane')];
            const reorderable_input_containers = [...document.querySelectorAll('.reorderable-input')];
            const reorderable_inputs = [...document.querySelectorAll('.reorderable-input input[type=checkbox]:not(:disabled)')];
            const editor_button_elements = [...document.querySelectorAll('.editor')];
            const dragger_elements = [...document.querySelectorAll('.dragger')];
            const select_all_elements = [...document.querySelectorAll('.select-all')];
            const csv_delimiter = document.getElementById('csv_delimiter');

            let current_selected_tab = null;
            let y_mouse_location = 0;
            let after_element = null;

            reorderable_input_containers.forEach(reorderable_input => {
                reorderable_input.addEventListener('dragstart', function (e1) {
                    reorderable_input.classList.add('dragging');
                });
                reorderable_input.addEventListener('dragend', function (e1) {
                    current_selected_tab.removeChild(ghost_reordable_input);
                    dragger_elements.forEach(dragger => dragger.removeAttribute('style'));
                    reorderable_input_containers.forEach(element => element.removeAttribute('style'));
                    reorderable_input.removeAttribute('draggable');
                    reorderable_input.classList.remove('dragging');
                    if (after_element==null) current_selected_tab.appendChild(this);
                    else current_selected_tab.insertBefore(this, after_element);
                    save_csv_exportation_settings();
                });
            });

            reorderable_inputs.forEach(reorderable_input => {
                reorderable_input.addEventListener('change', function () {
                    const is_checked = this.checked;
                    const its_parent = document.querySelector(`[data-target="${this.dataset.parent}"]`);
                    const children = [...document.querySelectorAll(`.csv-field-export[data-parent="${this.dataset.parent}"]:not(:disabled)`)];
                    const something_is_different = children.find(child => child.checked!==is_checked);
                    if (typeof something_is_different==='undefined') {
                        its_parent.checked = is_checked;
                        its_parent.indeterminate = false;
                    } else {
                        its_parent.checked = false;
                        its_parent.indeterminate = true;
                    }
                    save_csv_exportation_settings();
                });
            });

            tab_pane_elements.forEach(tab_pane => {
                tab_pane.addEventListener('dragover', function (e1) {
                    e1.preventDefault();
                    current_selected_tab = tab_pane;
                    y_mouse_location = e1.clientY;
                    after_element = get_drag_after_element(current_selected_tab, y_mouse_location);
                    const dragging_element = document.querySelector('.dragging');
                    reorderable_input_containers.forEach(reorderable_input => {
                        reorderable_input.style.background = 'transparent';
                        [...reorderable_input.childNodes].forEach(function (child) {
                            if (typeof child.classList!=='undefined' && child.classList.contains('dragger')) {
                                child.style.display = 'none';
                            }
                        });
                    });
                    dragging_element.style.display = 'flex';
                    ghost_reordable_input.style.height = dragging_element.offsetHeight + 'px';
                    ghost_reordable_input.style.width = dragging_element.offsetWidth + 'px';
                    dragging_element.style.display = 'none';
                    if (after_element==null) tab_pane.appendChild(ghost_reordable_input);
                    else tab_pane.insertBefore(ghost_reordable_input, after_element);
                });
            });

            function get_drag_after_element(container, y) {
                const draggable_elements = [...container.querySelectorAll('.reorderable-input:not(.dragging)')]

                return draggable_elements.reduce((closest, child) => {
                    const box = child.getBoundingClientRect()
                    const offset = y - box.top - box.height / 2
                    if (offset<0 && offset>closest.offset) {
                        return {
                            offset : offset,
                            element : child
                        }
                    } else {
                        return closest
                    }
                }, {
                    offset : Number.NEGATIVE_INFINITY
                }).element
            }


            dragger_elements.forEach(dragger => {
                dragger.addEventListener('mousedown', () => {
                    const parent = dragger.parentNode.parentNode;
                    if (!parent.hasAttribute('draggable')) parent.setAttribute('draggable', true);
                });

                dragger.addEventListener('mouseup', () => {
                    const parent = dragger.parentNode.parentNode;
                    if (parent.hasAttribute('draggable')) parent.removeAttribute('draggable');
                });
            });

            // column editor button
            editor_button_elements.forEach(editor => {
                editor.addEventListener('click', function (e) {
                    const inputs = [...this.parentElement.parentElement.childNodes];
                    const parent = this.parentElement.parentElement;
                    const checkbox = this.parentElement.parentElement.firstElementChild.firstElementChild;
                    inputs.forEach(input => {
                        input.remove();
                    });

                    input_field_container.value = checkbox.dataset.label;
                    parent.appendChild(input_group_field_editor);
                    input_field_container.focus();
                    editor_button_elements.forEach(editorElement => editorElement.style.display = 'none');

                    function handle_cancel_edition() {
                        input_group_field_editor.remove();
                        inputs.forEach(input => {
                            input_field_container.value = '';
                            parent.appendChild(input);
                        });
                        cancel_editor.removeEventListener('click', handle_cancel_edition);
                        confirm_editor.removeEventListener('click', handle_save_edition);
                        input_group_field_editor.removeEventListener('keydown', handle_input_keydown);
                        editor_button_elements.forEach(editorElement => editorElement.removeAttribute('style'));
                    }

                    function get_input_info(id, input) {
                        return new Promise(function (resolve) {
                            ajaxPost(null, `${baseUrl}/survey/exportation/csv-coform/get_input/${input}`, {
                                id
                            }, function (result) {
                                resolve(result)
                            }, null, 'JSON')
                        });
                    }

                    async function handle_save_edition() {
                        var input = await get_input_info(checkbox.dataset.id, checkbox.value);
                        input['csvfield'] = input['csvfield'] || {};
                        input['csvfield'][userConnected['_id']['$id']] = input_field_container.value;

                        var updateData = {
                            id : checkbox.dataset.id,
                            collection : 'forms',
                            path : `inputs.${checkbox.value}`,
                            value : input
                        };

                        updateData['value']['csvfield'] = {};
                        updateData['value']['csvfield'][userConnected['_id']['$id']] = input_field_container.value;

                        dataHelper.path2Value(updateData, function (data) {
                            inputs.forEach(input => {
                                checkbox.dataset.label = input_field_container.value;
                                checkbox.nextSibling.textContent = input_field_container.value;
                                input_group_field_editor.remove();
                                parent.appendChild(input);
                            });
                            cancel_editor.removeEventListener('click', handle_cancel_edition);
                            confirm_editor.removeEventListener('click', handle_save_edition);
                            input_group_field_editor.removeEventListener('keydown', handle_input_keydown);
                            editor_button_elements.forEach(editorElement => editorElement.removeAttribute('style'));
                            toastr.success('Mise à jour enregistrée');
                        });
                    }

                    function handle_input_keydown(event) {
                        if (event.code==='Enter')
                            handle_save_edition();
                        else if (event.code==='Escape')
                            handle_cancel_edition();
                    }

                    cancel_editor.removeEventListener('click', handle_cancel_edition);
                    confirm_editor.removeEventListener('click', handle_save_edition);
                    input_group_field_editor.removeEventListener('keydown', handle_input_keydown);


                    cancel_editor.addEventListener('click', handle_cancel_edition);
                    confirm_editor.addEventListener('click', handle_save_edition);
                    input_group_field_editor.addEventListener('keydown', handle_input_keydown);
                });
            });

            select_all_elements.forEach(select_all_element => {
                select_all_element.addEventListener('change', function (e) {
                    const parent = this.dataset.target;
                    const targets = [...document.querySelectorAll(`[data-parent="${parent}"]:not(:disabled)`)];
                    console.log(targets);
                    targets.forEach(target => target.checked = this.checked);
                    save_csv_exportation_settings();
                });
            });

            function handle_change_delimiter() {
                save_csv_exportation_settings();
            }

            csv_delimiter.removeEventListener('change', handle_change_delimiter);
            csv_delimiter.addEventListener('change', handle_change_delimiter);
        }

        function save_radio_option_settings(element) {
            const target = document.getElementById(element.dataset.target);
            if (element['value']===target.dataset.activationCondition) target.style.removeProperty('display');
            else target.style.display = 'none';
            save_csv_exportation_settings();
        }

        /**
         * @param {HTMLInputElement} __element
         */
        function manage_conditional_dom_and_save_changes(__element) {
            if (!is_empty(__element.dataset.target)) {
                const targets = __element.dataset.target.split(',');
                $.each(targets, function (__, __target) {
                    const target = document.getElementById(__target);
                    if (__element.checked) target.style.removeProperty('display');
                    else target.style.display = 'none';
                });
            }
        }

        function html_special_chars(text) {
            return text
                .replace(/&/g, "&amp;")
                .replace(/</g, "&lt;")
                .replace(/>/g, "&gt;")
                .replace(/"/g, "&quot;")
                .replace(/'/g, "&#039;");
        }

        function save_csv_exportation_settings() {
            const default_setting = {
                delimiter : document.getElementById('csv_delimiter').value,
                fields : []
            };
            view_settings.forEach(view_setting => default_setting[`${view_setting}_view`] = document.querySelector(`[name=${view_setting}_view]:checked`).value);
            $.each(conditional_checkboxes, function (__, __checkbox) {
                default_setting[`${__checkbox}_view`] = $(`${__checkbox}_view`).is(':checked');
            });
            const settings = get_storage();
            settings.fields = [];
            $('.csv-field-export').each(function (__, __field) {
                settings.fields.push({
                    id : __field.dataset.id,
                    parent : __field.dataset.parent,
                    step : __field.dataset.step,
                    label : html_special_chars(__field.dataset.label),
                    type : __field.dataset.type,
                    exportable : !__field.hasAttribute('disabled'),
                    value : __field.value,
                    checked : __field.checked,
                })
            });

            settings.delimiter = document.getElementById('csv_delimiter').value;
            settings.exportation_name = document.getElementById('csv_exportation_name').value;
            settings.second_answer_attribute = document.getElementById('second_answer_attribute').value;

            view_settings.forEach(view_setting => settings[`${view_setting}_view`] = default_setting[`${view_setting}_view`]);

            set_storage(settings);
            toastr.success('Le paramètre est enregistré');
            return false;
        }

        function load_settings() {
            const default_setting = {
                delimiter : document.getElementById('csv_delimiter').value,
                fields : []
            };
            view_settings.forEach(view_setting => default_setting[`${view_setting}_view`] = document.querySelector(`[name=${view_setting}_view]:checked`).value);

            const steps = JSON.parse(JSON.stringify(<?= json_encode($steps) ?>));
            const temporary_settings = [];
            for (const step of steps) {
                for (const input of step.inputs) {
                    temporary_settings.push({
                        id : input.id,
                        parent : `csv-${step['id']}`,
                        step : input.step,
                        label : input.label,
                        type : input.type,
                        checked : true,
                        exportable : input.exportable,
                        value : input.clef,
                    });
                }
            }

            const settings = get_storage();

            const longest = settings.fields.length>=temporary_settings.length ? settings.fields : temporary_settings;
            const to_removes = [];
            const to_adds = [];

            for (const temporary of longest) {
                const setting_find = settings.fields.find(setting => setting.value===temporary.value);
                const temporary_find = temporary_settings.find(setting => setting.value===temporary.value);
                if (typeof setting_find==='undefined') to_adds.push(temporary);
                if (typeof temporary_find==='undefined') to_removes.push(temporary);
            }

            for (const to_add of to_adds) {
                const nearest = settings.fields.indexOf(setting => setting['parent']===to_add['parent']);
                if (nearest<0) settings.fields.push(to_add);
                else settings.fields.splice(nearest + 1, 0, to_add);
            }

            for (const to_remove of to_removes) {
                const index_to_remove = settings.fields.find(field => field.value===to_remove.value);
                settings.fields.splice(settings.fields.indexOf(index_to_remove), 1);
            }

            let html = '';
            let last_parent = '';
            const status = {};
            settings.fields.forEach(field => {
                if (last_parent==='') {
                    last_parent = field.parent;
                    status[last_parent] = [];
                    html += `
                <div class="checkbox">
                    <label class="input-value">
                        <input type="checkbox" class="select-all" data-target="${last_parent}">
                        Sélectionner tout
                    </label>
                    <div class="action-btn">
                        <i class="fa fa-edit editor"></i>
                        <i class="fa fa-arrows dragger"></i>
                    </div>
                </div>
                `;
                }
                if (field.parent!==last_parent) {
                    console.log(`${last_parent}-tabulation-csv`);
                    document.getElementById(`${last_parent}-tabulation-csv`).innerHTML = html;
                    const check = status[last_parent].filter(checked => checked);
                    const select_all_element = document.querySelector(`[data-target="${last_parent}"]`);
                    if (check.length===status[last_parent].length) select_all_element.setAttribute('checked', 'checked');
                    else select_all_element.indeterminate = check.length!=0 && check.length!==status[last_parent].length;
                    last_parent = field.parent;
                    status[last_parent] = [];
                    html = `
                <div class="checkbox">
                    <label class="input-value">
                        <input type="checkbox" class="select-all" data-target="${last_parent}">
                        Sélectionner tout
                    </label>
                    <div class="action-btn">
                        <i class="fa fa-edit editor"></i>
                        <i class="fa fa-arrows dragger"></i>
                    </div>
                </div>
                `;
                }
                html += `
                <div class="checkbox reorderable-input">
                    <label class="input-value ${!field.exportable ? 'text-danger' : ''}">
                        <input class="csv-field-export" data-parent="${field.parent}" data-step="${field.step}" data-label="${field.label}" data-type="${field.type}" value="${field.value}" data-id="${field.id}" type="checkbox" ${!field.exportable ? 'disabled' : field.checked ? 'checked' : ''}>
                        ${field.label}
                    </label>
                    <div class="action-btn">
                        <i class="fa fa-edit editor"></i>
                        <i class="fa fa-arrows dragger"></i>
                    </div>
                </div>
            `;
                status[last_parent].push(!field.exportable || field.checked);
            });

            if (typeof last_parent!=='undefined' && last_parent!=='') {
                document.getElementById(`${last_parent}-tabulation-csv`).innerHTML = html;
                const check = status[last_parent].filter(checked => checked);
                const select_all_element = document.querySelector(`[data-target="${last_parent}"]`);
                if (check.length===status[last_parent].length) select_all_element.setAttribute('checked', 'checked');
                else select_all_element.indeterminate = check.length!=0 && check.length!==status[last_parent].length;
                html = '';
            }
            document.getElementById('csv_delimiter').value = settings.delimiter;
            document.getElementById('second_answer_attribute').value = settings.second_answer_attribute;
            document.getElementById('csv_exportation_name').value = is_empty(settings.exportation_name) ? `<?= $config['name'] ?>` : settings.exportation_name;
            document.getElementById('insert-second-after').value = is_empty(settings.insert_dynform_after) ? document.getElementById('insert-second-after').value : settings.insert_dynform_after;

            $.each(conditional_checkboxes, function (__, __checkbox) {
                const view = `${__checkbox}_view`;
                settings[view] = !is_empty(settings[view]) ? settings[view] : default_setting[view];
                const element = $(`[name=${view}]`).prop('checked', settings[view]);
                const target = !is_empty(element.data('target')) ? element.data('target') : '';
                const targets = (!is_empty(target) ? target.split(',') : []).map(function (__target) {
                    return $(`#${__target}`);
                });
                $.each(targets, function (__, __target) {
                    const activation = JSON.parse(__target.data('activation-condition'));
                    if (activation===element.is(':checked')) __target.css('display', '');
                });
            });
            view_settings.forEach(view => {
                settings[`${view}_view`] = !is_empty(settings[`${view}_view`]) ? settings[`${view}_view`] : default_setting[`${view}_view`];
                [...document.querySelectorAll(`[name=${view}_view]`)].forEach(element => {
                    element.checked = element['value']===settings[`${view}_view`];
                    if (!is_empty(element.dataset.target)) {
                        const targets = element.dataset.target.split(',');
                        $.each(targets, function (__, __target) {
                            const target = document.getElementById(__target);
                            if (element.checked && target.dataset.activationCondition===element.value) target.style.removeProperty('display');
                        });
                    }
                });
            });
        }

        $("#fileImport").off().on("change",function(e){
            var domElem=$(this);
            mylog.log("domElem",domElem);
            var mapping={};
            if (e.target.files != undefined) {
            mylog.log("event object",e);
			var reader = new FileReader();
			reader.onload = function(evt) {
                mylog.log("event onload",evt);
				file = evt.target.result;
				file=dataHelper.csvToArray(file,';','"');
				var mapping=file.reduce((acc,[k,v])=>(acc[k]=v,acc),{});
				mylog.log("file ok",file);
				mylog.log("mapping ok",mapping);
                // this.dataset["mapping"]={};

                var mappingStr=JSON.stringify(mapping);
                mylog.log("mapping",mappingStr);
                document.getElementById('fileImport').dataset.mapping=mappingStr;
			};
			reader.readAsText(e.target.files.item(0));
			// mylog.log("reader ok",file);
		    }
            // domElem.data("mapping",mapping);
            // var file=$(this).files[0];

            //     reader = new FileReader();

            //     var mapping={};

            //     reader.onload = function() {
            //        file=reader.result;
            //        file=dataHelper.csvToArray(file,';','"');
	        //        mapping=file.reduce((acc,[k,v])=>(acc[k]=v,acc),{});
	        //     // mylog.log("file ok",file);
	        //        mylog.log("mapping ok",mapping);
            //        $(this).data("mapping",mapping);
            //     };
            //     reader.onerror = function() {
            //         console.log(reader.error);
            //     };
                
            //     reader.readAsText(file);

                // return mapping;
        });
                


            

        function perform_csv_exportation(element) {
            const spinner = element.firstElementChild;
            spinner.classList.add('fa');
            spinner.classList.add('fa-spinner');
            spinner.classList.add('fa-spin');
            const query = {
                form : `<?= $form ?>`,
                answers : {
                    '$exists' : true
                }
            };

            const post = {
                excluded_inputs : JSON.parse(JSON.stringify(<?= json_encode($input_exclusions) ?>)),
                is_exporting_following_answer : document.querySelector('[name=export_following_answer_view]').checked,
                following_answer_id : document.getElementById('second_answer_attribute').value,
                is_exporting_dynform : document.querySelector('[name=export_dynform_view]').checked,
                export_dynform_after : document.getElementById('insert-second-after').value,
                is_exporting_answer_id : document.querySelector('[name=export_answer_id_view]').checked,
                completed_answers_only : document.querySelector('[name=completed_answers_only_view]').checked,
                is_exporting_user_email : document.querySelector('[name=export_user_email_view]').checked,
                is_custom_checkbox_aside : document.querySelector('[name=custom_checkbox_aside_view]').checked,
                split_steps : document.querySelector('[name=split_steps_view]').checked,
                settings : {},
                champs : [],
                steps : {},
                query,
                mappingLabel : typeof document.getElementById('fileImport').dataset.mapping=="string" ? JSON.parse(document.getElementById('fileImport').dataset.mapping) : null

            };

            // mylog.log("mappingLabel sent",JSON.parse(document.getElementById('fileImport').dataset.mapping));


            // post.mappingLabel = getMappingFromFile(document.getElementById('fileImport'));

            mylog.log("post what",post);

            $('#additional-settings').find('.form-group').each(function() {
                
                const self = $(this);
                const data_type = self.data('type');
                const data_name = self.data('name');
                if(['text', 'number', 'date', 'email'].includes(data_type)) {
                
                } else if(data_type === 'select') {
                
                }else if(data_type === 'checkbox') {
                    post[data_name] = [];
                    self.find('input[type=checkbox]:checked').each(function () {post[data_name].push(this.value)});
                }
            });

            view_settings.forEach(view_setting => post['settings'][view_setting] = document.querySelector(`[name=${view_setting}_view]:checked`).value);

            $('.csv-field-export').each(function () {
                const champ = this;
                if (champ.checked) {
                    post.steps[champ.dataset.step] = post.steps[champ.dataset.step] ? post.steps[champ.dataset.step] : [];
                    post.steps[champ.dataset.step].push({
                        value : champ.value,
                        label : champ.dataset.label,
                        type : champ.dataset.type,
                    });

                    // lately to delete
                    post.champs.push({
                        step : champ.dataset.step,
                        label : champ.dataset.label,
                        type : champ.dataset.type,
                        value : champ.value
                    });
                }
            })
            request_exportation(post);

            return false;
        }

        /**
         * Envoyer une requête pour l'export
         * @param {Object} __post Données de paramètrage
         */
        function request_exportation(__post) {
            const url = `${baseUrl}/survey/exportation/${__post.split_steps ? 'multiple-file-csv-coform' : 'csv-coform'}`;
            ajaxPost(null, url, __post, function (__response) {
                $('#csv-export span').removeClass();
                if (__post.split_steps) {
                    let time_manager = 0;
                    $.each(__response, (__, __csv) => {
                        const {heads, bodies, name} = __csv;
                        mylog.log("bodies",bodies,"name",name);
                        if (!is_empty(heads) && !is_empty(bodies)) {
                            setTimeout(() => {
                                export_file(heads, bodies, name);
                            }, time_manager * 500);
                            time_manager += 1;
                        }
                    });
                } else {
                    const {heads, bodies} = __response;
                    export_file(heads, bodies);
                }
            });
        }

        /**
         * Créer un processus d'exportation
         * @param {string[]} __headers Liste d'entêtes
         * @param {Object[]} __body Les contenues du fichier
         * @param {stirng} __specification Différenciation des fichiers générés
         */
        function export_file(__headers, __body, __specification = '') {
            if (!is_empty(json2csv) && !is_empty(json2csv.Parser)) {
                const parser = json2csv.Parser;
                const fields = __headers;
                const exportation_preparation = new parser({fields, delimiter : document.getElementById('csv_delimiter').value});
                const csv = exportation_preparation.parse(__body);
                let filename = document.getElementById('csv_exportation_name').value;
                if (!is_empty(filename)) {
                    if (!is_empty(__specification)) filename = filename+"_"+__specification;
                    const splited_filename = filename.split('.');
                    const file_signature = moment().format('YYYYMMDD-HHmm');
                    filename = filename.length>200 ? filename.substring(0, 200) + '...' : filename;
                    filename = splited_filename.length>1 && splited_filename[splited_filename.length - 1].toLowerCase()==='csv' ? `${splited_filename.slice(-1).join('')}_${file_signature}.csv` : `${filename} ${file_signature}.csv`;

                    //var universalBOM = "\uFEFF";
                    const blob = new Blob([new Uint8Array([0xEF, 0xBB, 0xBF]), csv], {
                        type : 'text/csv;charset=utf-8;'
                    });
                    if (!is_empty(navigator) && !is_empty(navigator.msSaveBlob)) { // IE 10+
                        navigator.msSaveBlob(blob, filename);
                    } else {
                        const link = document.createElement('a');
                        if (link.download!==undefined) { // feature detection
                            // Browsers that support HTML5 download attribute
                            const url = URL.createObjectURL(blob);
                            link.href = url;
                            link.download = filename;
                            link.style.display = 'hidden';
                            document.body.appendChild(link);
                            link.dispatchEvent(
                                new MouseEvent('click', {
                                    bubbles : true,
                                    cancelable : true,
                                    view : window
                                })
                            );
                            document.body.removeChild(link);
                            URL.revokeObjectURL(url);
                        }
                    }
                } else {
                    toastr.error('Vous devez spécifier un nom pour votre fichier dans l\'onglet de paramètre');
                }
            }
        }

        $(function () {
            $('#insert-second-after').val($('#insert-second-after>optgroup:last-child>option:last-child').attr('value'));
            load_settings();
            load_events();

            $('[name=address_view], [name=finder_column_view]').on('change', function () {
                save_csv_exportation_settings();
            });
            $('#csv_exportation_name, #second_answer_attribute').on('blur', function () {
                save_csv_exportation_settings();
            });
            $('[name=finder_view], [name=checkbox_view]').on('change', function (__e) {
                save_radio_option_settings(__e.target);
            });

            $.each(conditional_checkboxes, function (__, __dom) {
                const self = $(`[name=${__dom}_view]`);
                self.on('change', function () {
                    manage_conditional_dom_and_save_changes(this);
                    set_storage({[self.attr('name')] : this.checked}).then(function () {
                        toastr.success('Le paramètre est enregistré');
                    });
                })
            });
            $('#insert-second-after').on('change', function () {
                const self = $(this);
                set_storage({insert_dynform_after : self.val()}).then(function () {
                    toastr.success('Le paramètre est enregistré');
                });
            });
            $('#csv-export').on('click', function (__e) {
                __e.preventDefault();
                perform_csv_exportation(__e.target);
            });
        })
    })();
</script>