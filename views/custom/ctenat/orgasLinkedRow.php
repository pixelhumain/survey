<table class="table table-bordered table-hover  directoryTable" id="panelAdmin<?php echo $key?>">
	<tbody class="directoryLines">	

<?php 
	$editBtnL = ($canAdmin) ? " <a href='javascript:;' data-form='".$step."' data-step='".$step."' data-q='".$key."' class='addAF btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";
?>	
<thead>
	<tr>
		<td colspan=5>
			<h3 style="color:#16A9B1">Structure porteuse associée <?php echo $editBtnL?></h3>
		</td>
	</tr>	
	<?php if(count($answers)>0){ ?>
	<tr>
		<th></th>
		<th>Nom de l'organisation</th>
		<th>Type</th>
		<th>Référant</th>
	</tr>
	<?php } ?>
</thead>

<tr>
	<?php 
	$pos = 0;
	$ids = [];
	foreach ($answers as $i => $o) {
		$ids[] = $o["id"];
	}
	
	$orgas = Link::groupFindByType( Organization::COLLECTION,$ids, ["name","type","referantName","profilThumbImageUrl"] );
	
	foreach ($orgas as $id => $a) {
		$img = ( !empty( $a["profilThumbImageUrl"] ) ) ? "<img src='".Yii::app()->createUrl( $a["profilThumbImageUrl"] )."'  style='width:70px'/> " : "";
		echo "<tr>".
			"<td style='width:80px' >".$img."</td>".
			"<td><a class='lbh-preview-element' data-hash='#page.type.organizations.id.".$id."' data-type='organizations' data-id='".$id."' href='#page.type.organizations.id.".$id."'>".@$a["name"]."</a></td>".
			"<td>".@Yii::t("category",Organization::$types[$a["type"]])."</td>".
			"<td>".@$a["referantName"]."</td>";
	?>
		<td>
			<?php if ($canAdmin){ ?>
			<a href='javascript:;' data-form='<?php echo $step?>' data-step='<?php echo $step?>' data-q='<?php echo $key?>' data-pos='<?php echo $pos?>' class='editAF btn btn-xs btn-default'><i class='fa fa-pencil'></i></a>
			<a href='javascript:;' data-form='<?php echo $step?>' data-step='<?php echo $step?>' data-q='<?php echo $key?>' data-pos='<?php echo $pos?>' class='deleteAF btn btn-xs btn-danger'><i class='fa fa-times'></i></a>
			<?php } ?>
		</td>
	<?php 
		$pos++;
		echo "</tr>";
	}
	 ?>
	</tbody>
</table>

<script type="text/javascript">
	
$(document).ready(function() { 
    mylog.log("render","/modules/survey/views/custom/ctenat/calendarRow.php");
});
</script>