<div id='explain' class='col-sm-offset-1 col-sm-10 stepperContent ' style='margin-top:30px; padding-bottom:40px'>
	<div class='col-xs-12 text-center'>
		<a href="javascript:showStepForm('#action')" class="btn btn-primary">C'est parti</a>
	</div>
	<div class='col-xs-12 margin-top-20'>
		<div class='col-xs-2 text-center'>
			<b style="color:#0A2F62">Jalon 1</b>
		</div>
		<div class='col-xs-10' style="border-bottom:1px solid #666;margin-bottom: 20px;padding-bottom: 10px;">
			<b style="color:#0A2F62">Décrire l’action et sa structure porteuse</b> : identifier une ou plusieurs structures porteuses et décrire sommairement l’action. <btr/>
			<br>
			A ce stade, votre action existe et votre territoire la voit apparaître dans son tableau de bord.
		</div>
	</div>
	<br><br>
	<div class='col-xs-12'>
		<div class='col-xs-2 text-center'>
			<b style="color:#0A2F62">Jalon 2</b>
		</div>
		<div class='col-xs-10' style="border-bottom:1px solid #666;margin-bottom: 20px;padding-bottom: 10px;">
			<b style="color:#0A2F62">Caractériser l’action</b> : renseigner des étiquettes présentant des « domaines d’action » et des « objectifs/cibles de développement durable ». Cette caractérisation sera utilisée dans les moteurs de recherche de la plateforme et pour vous aider à évaluer votre action.

		</div>
	</div>
<br><br>
	<div class='col-xs-12'>
		<div class='col-xs-2 text-center'>
			<b style="color:#0A2F62">Jalon 3</b>
		</div>
		<div class='col-xs-10' style="border-bottom:1px solid #666;margin-bottom: 20px;padding-bottom: 10px;">
			<b style="color:#0A2F62">Mûrir l’action</b> : préciser tous les éléments opérationnels de l’action (calendrier, partenaires, financements, indicateurs d’évaluation…). 
			<br/>Cette phase est collaborative : les partenaires techniques, financiers et administratifs de l’action sont conviés à analyser les aspects de leur champ de compétences et à donner leur avis pour permettre à l’action d’aboutir.

			<br><br>
			A ce stade, ces partenaires ont accès à votre dossier et peuvent interagir avec vous, commenter vos propositions et vous faire des suggestions pour faire évoluer l’action.


		</div>
	</div>
<br><br>
	<div class='col-xs-12'>
		<div class='col-xs-2 text-center'>
			<b style="color:#0A2F62">Jalon 4</b>
		</div>
		<div class='col-xs-10' style="border-bottom:1px solid #666;margin-bottom: 20px;padding-bottom: 10px;">
			<b style="color:#0A2F62">Contractualiser l'action</b>: Une fois les étapes précédentes complétées, l’action est validée
.
			<br><br>
			Ce jalon n’est accessible qu’au porteur de l’action, aux financeurs, à la collectivité et l’État local. Une fois l’action validée, elle est publique et donc visible par l’ensemble de la communauté.


		</div>
	</div>
<br><br>
	<div class='col-xs-12'>
		<div class='col-xs-2 text-center'>
			<b style="color:#0A2F62">Jalon 5</b>
		</div>
		<div class='col-xs-10' style="margin-bottom: 20px;">
			<b style="color:#0A2F62">Suivre l'action</b> : renseigner les indicateurs de suivi et faire évoluer, le cas échéant, les informations de l’action.
			<br><br>
			Cette partie n’est ouverte qu’aux services instructeurs et au porteur de projet.
		</div>
	</div>
</div>

<script type="text/javascript">
	
$(document).ready(function() { 
    mylog.log("render","/modules/survey/views/custom/ctenat/explainRow.php");
});
</script>