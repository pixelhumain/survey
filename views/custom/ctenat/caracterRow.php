<div id='caracter' class='col-sm-12 stepperContent hide' style='padding-bottom:40px'>
	<div class='col-xs-12'>
		
		<div class='col-xs-12'>
			<h3 class="text-center" style="color:#16A9B1;padding-top:20px;">Caractérisation 
			<?php if($canAdmin){ ?><a href='javascript:'  data-form='caracter' data-step='caracter' class='editStep btn btn-default'><i class='fa fa-pencil'></i></a><?php } ?></h3>
			<div class='col-xs-12 col-sm-offset-1 col-sm-10 margin-bottom-20' style="border-bottom:1px solid #666;padding-bottom: 20px;margin-bottom: 20px;">
			La caractérisation de l’action permet à l’ensemble des actions des CTE de s’inscrire dans un référentiel commun permettant ensuite le classement des actions dans le moteur de recherche, dans les graphiques du tableau de bord... Il vous est demandé de choisir parmis les propositions le périmètre d’application de l’action ainsi que les finalités de transition écologique (cibles développement durable) jugées prioritaires. Vos choix nous permettront par ailleurs de vous proposer des indicateurs de suivi. <!-- <a href="javascript:;" class="modalInfo" data-poi="5d1e033e40bb4ed6549bf68d"><i style="color: #16A9B1" class="fa fa-info-circle fa-2x"></i></a> -->
			</div>
		</div>

		
		<div class='col-xs-12'>
			<h3 class="text-center" style="color:#24A9B1;padding-top:20px;">SELECTION DES DOMAINES D’ACTION</h3>
			<div class='col-xs-12 col-sm-offset-1 col-sm-10 margin-bottom-20' style="border-bottom:1px solid #666;padding-bottom: 20px;margin-bottom: 20px;">
				Les domaines d’action indiquent la nature technique de l’action. Il peut s’agir de grands corps de métier. Une même action peut couvrir plusieurs domaines d’action. Le domaine d’action principal correspond au “coeur de métier” de l’action. Les domaines secondaires qualifient des facettes moins centrales de l’action.


				<style type="text/css">
					.action{
						font-size:1.5em; 
						font-weight:bold;
						background-color:#3D85C6;
						border-radius: 5px;
						padding:10px;
						margin: 20px;
						}
					.actionSub{
						font-size:1.5em; 
						font-weight:bold;
						border: 1px solid #3D85C6;
						border-radius: 5px;
						padding:10px;
						margin: 20px;
						}
					.wimg{
						width : 50px;
						margin:5px;
					}
				</style>

				<h4 class="margin-top-20">Domaine d’action principal (obligatoire)</h4>
				<?php 
				$beflink="";
				$aflink="";
				if($canAdmin){ 
					$beflink = "<a href='javascript:'  data-form='caracter' data-step='caracter' class='editStep'>";
					$aflink = "</a>";
				}

				if(isset($answers["actionPrincipal"])){		
					$img = "";
					foreach (@$this->costum["badges"]["domainAction"] as $ki => $vi) {
						if( $vi["name"] == $answers["actionPrincipal"] && isset($vi["profilThumbImageUrl"]) )
							$img = "<img class='wimg' src='".Yii::app()->createUrl($vi["profilThumbImageUrl"])."'/>";
					}
					
					echo "<div class='text-center text-white action'>".
							$beflink.$answers["actionPrincipal"].$aflink.
						"</div>"; 
				}
				?>

				<h4 class="margin-top-20">Domaine(s) d’action secondaire(s)</h4>
				<?php 
				if(!empty($answers["actionSecondaire"])){
					foreach ($answers["actionSecondaire"] as $k => $v ) {
						$img = "";
						foreach (@$this->costum["badges"]["domainAction"] as $ki => $vi) 
						{
							if( $vi["name"] == $v && isset($vi["profilThumbImageUrl"]) )
								$img = "<img class='wimg' src='".Yii::app()->createUrl($vi["profilThumbImageUrl"])."'/>";
						}
						echo "<div class='text-center actionSub'>".
							$beflink.$img.$v.$aflink.
						"</div>";
					}
				}?>
			</div>
		</div>


		<div class='col-xs-12'>
			<h3 class="text-center" style="color:#24A9B1;padding-top:20px;">SELECTION DES OBJECTIFS</h3>
			<div class='col-sm-offset-2 col-sm-8 margin-bottom-20'>
				Les objectifs de l’action qui vous sont proposés sont structurées autour des 5 finalités des agenda 21.
				
				<h4 class="margin-top-20">Objectif principal (obligatoire)</h4>
				<?php 
				if(isset($answers["cibleDDPrincipal"])){	
					$img = "";
					foreach (@$this->costum["badges"]["cibleDD"] as $ki => $vi) {
						if( $vi["name"] == $answers["cibleDDPrincipal"] && isset($vi["profilThumbImageUrl"]) )
							$img = "<img class='wimg' src='".Yii::app()->createUrl($vi["profilThumbImageUrl"])."'/>";
					}
					if(is_array($answers["cibleDDPrincipal"])){
						echo "<div class='text-center text-white action'>".
							$beflink.$img.$answers["cibleDDPrincipal"][0].$aflink.
						"</div>";
					} else {
						echo "<div class='text-center text-white action'>".
							$beflink.$img.$answers["cibleDDPrincipal"].$aflink.
						"</div>";
					}
					
				}
				?>

				<h4 class="margin-top-20">Objectif(s) secondaire(s)</h4>
				<?php  
				if(isset($answers["cibleDDSecondaire"])){
					foreach ($answers["cibleDDSecondaire"] as $k => $v ) {
						$img = "";
						foreach (@$this->costum["badges"]["cibleDD"] as $ki => $vi) {
							if( $vi["name"] == $v && isset($vi["profilThumbImageUrl"]) )
								$img = "<img class='wimg' src='".Yii::app()->createUrl($vi["profilThumbImageUrl"])."'/>";
						}
						echo "<div class='text-center text-white actionSub'>".$beflink.$img.$v.$aflink."</div>";
					}
				} ?>
			</div>
		</div>


		
	</div>
</div>

<script type="text/javascript">
documentation.poiId = null;
$(document).ready(function() { 

    mylog.log("render","/modules/survey/views/custom/ctenat/caracterRow.php");

    $(".modalInfo").off().on("click",function (){
    	documentation.poiId = $(this).data('poi');
    	smallMenu.open("",null,null,function(poiId){
    		documentation.getBuildPoiDoc( documentation.poiId,"#openModalContent",true );
    	});
    	
		
	});
    
});
</script>