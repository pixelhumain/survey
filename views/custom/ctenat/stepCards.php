<div class="col-xs-12 text-center">
	Vous souhaitez proposer une action pour le CTE de votre territoire.
	<br/><br/>
	<h3>ETAPE 1 :</h3>
	Dans le formulaire suivant vous devrez renseigner l’organisme porteur, l’intitulé de l’action et une brève description. On vous demandera également de caractériser brièvement votre action.
</div>

<div class="col-xs-12 text-center">
	<img class='img-responsive' style='margin:30px auto 0px auto;' src='<?php echo Yii::app()->getModule("costum")->assetsUrl?>/images/ctenat/stepper.png'>
</div>

<div class="col-xs-12 text-center">
	Cette première étape réalisée, <b>votre page action sera automatiquement créée</b> sur la plateforme et vous pourrez y accéder en tant qu’administrateur pour la modifier et la compléter avec d’autres informations (financements, partenaires, indicateurs de suivi…), en vous rendant sur <b>l’onglet “candidature”</b> de votre page action.
</div>

<div class="col-xs-12 text-center">
	<img class='img-responsive' style='margin:30px auto 0px auto;' src='<?php echo Yii::app()->getModule("costum")->assetsUrl?>/images/ctenat/menuTabs.png'>
</div>

<div class="col-xs-12 text-center">
	A ce stade, votre page action ne sera visible que de vous et des référents du CTE du territoire (la collectivité, les services de l’Etat et leurs partenaires locaux).
</div>


<br/>