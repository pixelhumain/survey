<table class="table table-bordered table-hover  directoryTable" id="panelAdmin<?php echo $key?>">
	
<?php 
// $indicateurs = array(
//     "auto" => "Autofinancement (porteur)",
//     "public" => "Communes / COMCOM /Syndicats Publics",
//     "departement" => "Département",
//     "region" => "Région",
//     "private" => "Secteur privé",
//     "state" => "État"
// );

$indicateurs = Ctenat::getIndicator();
$editBtnL = ($canAdmin) ? " <a href='javascript:;' data-form='".$step."' data-step='".$step."' data-q='".$key."' class='addAF btn btn-default margin-top-5 margin-bottom-5'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";
?>	
<style type="text/css">
	.bgGrey{background-color: grey;}
</style>
<thead>
	<tr>
		<td colspan='7' >
			<h3 style="color:#16A9B1;width:100%;"><?php echo $df["title"]?></h3>
			<span>Choisissez des indicateurs parmi ceux déjà proposés par les territoires (essayez dans la mesure du possible d’utiliser des indicateurs existant)</span><br/>
			<?php echo $editBtnL ?>
			<br/>
			<span>Au besoin, vous pouvez déclarer un nouvel indicateur</span><br/>
			<a href='javascript:;' onclick="dyFObj.openForm('indicator');" class='btn btn-default margin-top-5 margin-bottom-5'><i class='fa fa-plus'></i> Ajouter un nouvel indicateur </a>
<?php //echo @$df["text"]?>
		</td>
	</tr>	
	<tr>
		<th>Indicateur</th>
		<th>Objectif / Réalisé</th>
		<th>Réf. 2018</th>
		<th>Résultat 2019</th>
		<th>Résultat 2020</th>
		<th>Résultat 2021</th>
		<th>Résultat 2022</th>
		<th></th>
	</tr>
</thead>
<tbody class="directoryLines">	
	<!-- <tr> -->
	<?php 
	$ct = 0;
	//always show indicateur emploi 
	$emploiExists = false;
	foreach ($answers as $q => $a) {
		if( isset($a["indicateur"]) && is_array($a["indicateur"]) &&  in_array("5d7fa1c540bb4e8f7b496afe", $a["indicateur"]) )
			$emploiExists = true;
		else if( isset($a["indicateur"]) && "5d7fa1c540bb4e8f7b496afe" == $a["indicateur"] )
			$emploiExists = true;
	}

	if( !$emploiExists ){
		$emploiIndic = [ "indicateur" => "5d7fa1c540bb4e8f7b496afe" ];
		PHDB::update( Form::ANSWER_COLLECTION, [ "_id"=>new MongoId($id) ], 
						array('$push' => array("answers.".$answer['formId'].".answers.".$step.".".$key=>$emploiIndic)));
		$answers[] = $emploiIndic;
	}
	//var_dump($indicateurs); //exit;
	foreach ($answers as $q => $a) {
		if(isset($a["indicateur"])){
			//foreach ($a["indicateur"] as $kA => $valA) {
			echo "<tr>".
				//"<td id='indic".$ct."' rowspan=2 style='vertical-align : middle;text-align:center;'>".( !empty($indicateurs[$valA]) ?  $indicateurs[$valA] : "" )."</td>".
				"<td id='indic".$ct."' rowspan=2 style='vertical-align : middle;text-align:center;'>".( !empty($indicateurs[$a["indicateur"]]) ?  $indicateurs[$a["indicateur"]] : "" )."</td>".
				"<td>Objectif</td>".
				"<td style='vertical-align : middle;text-align:center;background-color:grey;'></td>".
				"<td class='editContent' data-key='res2019'  data-indic='indic".$ct."' data-pos='".$q."' data-type='objectif'>".(isset($a["objectif"]["res2019"])?$a["objectif"]["res2019"]:"")."</td>".
				"<td class='editContent' data-key='res2020'  data-indic='indic".$ct."' data-pos='".$q."' data-type='objectif'>".(isset($a["objectif"]["res2020"])?$a["objectif"]["res2020"]:"")."</td>".
				"<td class='editContent' data-key='res2021'  data-indic='indic".$ct."' data-pos='".$q."' data-type='objectif'>".(isset($a["objectif"]["res2021"])?$a["objectif"]["res2021"]:"")."</td>".
				"<td class='editContent' data-key='res2022'  data-indic='indic".$ct."' data-pos='".$q."' data-type='objectif'>".(isset($a["objectif"]["res2022"])?$a["objectif"]["res2022"]:"")."</td>"; ?>
				
				<td rowspan=2 >
					<?php if ($canAdmin){ ?>
					<a href='javascript:;' data-form='<?php echo $step?>' data-step='<?php echo $step?>' data-q='<?php echo $key?>' data-pos='<?php echo $ct?>' class='editAF btn btn-xs btn-default'><i class='fa fa-pencil'></i></a>
					<a href='javascript:;' data-form='<?php echo $step?>' data-step='<?php echo $step?>' data-q='<?php echo $key?>' data-pos='<?php echo $ct?>' class='deleteAF btn btn-xs btn-danger'><i class='fa fa-times'></i></a>
					<!-- $step.$key.$ct -->
					<?php } ?>
					<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $id?>','<?php echo $step.$key.$ct ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$id,"contextType"=>"answers", "path"=>$step.$key.$ct))?> <i class='fa fa-commenting'></i></a>
				</td>
			
			<?php echo "</tr>";
			$editableClass = ($isSuivi) ? "editContent" : "bgGrey" ;
			echo "<tr>".
				"<td>Réalisé</td>".	
				"<td class='editContent' data-key='res2018'  data-indic='indic".$ct."' data-pos='".$q."' data-type='reality'>".(isset($a["reality"]["res2018"]) ? $a["reality"]["res2018"]:"")."</td>".
				"<td class='".$editableClass."' data-key='res2019'  data-indic='indic".$ct."' data-pos='".$q."' data-type='reality'>".(isset($a["reality"]["res2019"]) ? $a["reality"]["res2019"]:"")."</td>".
				"<td class='".$editableClass."'  data-key='res2020'  data-indic='indic".$ct."' data-pos='".$q."' data-type='reality'>".(isset($a["reality"]["res2020"]) ? $a["reality"]["res2020"]:"")."</td>".
				"<td class='".$editableClass."'  data-key='res2021'  data-indic='indic".$ct."' data-pos='".$q."' data-type='reality'>".(isset($a["reality"]["res2021"]) ? $a["reality"]["res2021"]:"")."</td>".
				"<td class='".$editableClass."'  data-key='res2022'  data-indic='indic".$ct."' data-pos='".$q."' data-type='reality'>".(isset($a["reality"]["res2022"]) ? $a["reality"]["res2022"]:"")."</td>";
			echo "</tr>";


			$ct++;
			//}
		}
	}

	 ?>

	</tbody>
</table>

<script type="text/javascript">
var rTmp = null;
$(document).ready(function() { 
	mylog.log("render","/modules/survey/views/custom/ctenat/resultsRow.php");
	$('.editContent').off().click(function() {
		
		rTmp = {
			pos : $(this).data('pos'),
			type : $(this).data('type'),
			key : $(this).data('key')
		};
		var indic = $('#'+$(this).data('indic')).html();
		bootbox.prompt({
				inputType: 'number',
	            title: "Valeur ? <span style='color:red'>(uniquement des chiffres)</span><br/>"+indic, 
	            callback : function(result){ 
	                if (result === null) {
				    	//alert("null");
				    } 
				    else 
				    {
		                data = {
			    			collection : "answers",
			    			id : answerIdx,
			    			path : "answers."+form.id+".answers.murir.results."+rTmp.pos+"."+rTmp.type+"."+rTmp.key,
			    			value : result
			    		}
			    		mylog.log(".editContent", "results","data",data);
						
						dataHelper.path2Value( data, function(params) { 
							dyFObj.commonAfterSave(null,function(){
								answerAfterSave();
							});
						} );
					}
	            }
	        });

	 	});

    });
  	</script>
