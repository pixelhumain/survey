<?php 

$cssJS = array(

    '/plugins/jquery.dynForm.js',
    
    '/plugins/jQuery-Knob/js/jquery.knob.js',
    '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
    '/plugins/jquery.dynSurvey/jquery.dynSurvey.js',

	'/plugins/jquery-validation/dist/jquery.validate.min.js',
    '/plugins/select2/select2.min.js' , 
    '/plugins/moment/min/moment.min.js' ,
    '/plugins/moment/min/moment-with-locales.min.js',

    // '/plugins/bootbox/bootbox.min.js' , 
    // '/plugins/blockUI/jquery.blockUI.js' , 
    
    '/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js' , 
    '/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css',
    '/plugins/jquery-cookieDirective/jquery.cookiesdirective.js' , 
    '/plugins/ladda-bootstrap/dist/spin.min.js' , 
    '/plugins/ladda-bootstrap/dist/ladda.min.js' , 
    '/plugins/ladda-bootstrap/dist/ladda.min.css',
    '/plugins/ladda-bootstrap/dist/ladda-themeless.min.css',
    '/plugins/animate.css/animate.min.css',
    // SHOWDOWN
	'/plugins/showdown/showdown.min.js',
	//MARKDOWN
	'/plugins/to-markdown/to-markdown.js',
	'/plugins/select2/select2.min.js' ,
	'/plugins/select2/select2.css'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);

$cssJS = array(
    '/js/dataHelpers.js',
    '/js/sig/geoloc.js',
    '/js/sig/findAddressGeoPos.js',
    '/js/default/loginRegister.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl() );

$cssJS = array(
'/assets/css/default/dynForm.css',
'/assets/js/comments.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->theme->baseUrl);
?>

<style type="text/css">
	#central-container {padding-top: 0px;}
</style>
<?php 


/* ?>
<div class="col-xs-12 text-center margin-bottom-20" >
	<div class="col-xs-3 formStep text-center">
	<?php
	echo "<h4>AUTEUR</h4>";
	echo "<a target='_blank' href='".Yii::app()->createUrl("costum/co/index/id/ctenat#@".@$user["username"])."'>";
	echo "<img src='".Yii::app()->createUrl($user["profilThumbImageUrl"])."' class='img-circle'/>";
	echo "<h4>".$user["name"]."</h4></a>";
	?>
	</div>

	<div class="col-xs-3 formStep text-center">
	<?php
	if( isset($answers["organization"]) ){
		$orga = Element::getByTypeAndId( $answers["organization"]["type"] , $answers["organization"]["id"] );
		echo "<a target='_blank' href='".Yii::app()->createUrl("costum/co/index/id/ctenat#@".@$orga["slug"])."'>";
		echo "<h4>ORGANISATION</h4>";
		if(!empty($orga["profilThumbImageUrl"]))
			echo "<img src='".Yii::app()->createUrl($orga["profilThumbImageUrl"])."' class='img-circle'/>";
		else 
			echo "<i class='fa fa-4x fa-users'></i>";
		echo "<h4>".$orga["name"]."</h4></a>";
	} else {
		echo "<h4>ORGANISATION<br/></h4>";
		echo "<i class='fa fa-4x fa-question-circle-o'></i>";
	}

	?>
	</div>
	
	<div class="col-xs-3 formStep text-center">
	<?php 
	if(isset($answers["project"])){
		$project = Element::getByTypeAndId( $answers["project"]["type"] , $answers["project"]["id"] );
		echo "<a target='_blank' href='".Yii::app()->createUrl("costum/co/index/id/ctenat#@".@$project["slug"])."'>";
		echo "<h4>PROJET</h4>";
		if(!empty($project["profilThumbImageUrl"]))
			echo "<img src='".Yii::app()->createUrl($project["profilThumbImageUrl"])."' class='img-circle'/>";
		else 
			echo "<i class='fa fa-4x fa-lightbulb-o'></i>";
		echo "<h4>".$project["name"]."</h4></a>";
	}else {
		echo "<h4>PROJET<br/></h4>";
		echo "<i class='fa fa-4x fa-question-circle-o'></i>";
	}
	?>
	</div>
	
	<div class="col-xs-3 formStep text-center">
	<?php
	$tags = array();	
	if(isset($answers["caracter"]) ){
		echo "<h4>CARACTERISATION<br/></h4>";
		
		if(is_array(@$answers["caracter"]["actionPrincipal"]) && count(@$answers["caracter"]["actionPrincipal"]))
			$tags = array_merge($tags,$answers["caracter"]["actionPrincipal"]);
		else if(!empty($answers["caracter"]["actionPrincipal"]))
			$tags[] = $answers["caracter"]["actionPrincipal"];
		
		if(is_array(@$answers["caracter"]["actionSecondaire"])  && count(@$answers["caracter"]["actionSecondaire"]))
			$tags = array_merge($tags,@$answers["caracter"]["actionSecondaire"]);
		
		if(is_array(@$answers["caracter"]["cibleDDPrincipal"]) && count(@$answers["caracter"]["cibleDDPrincipal"]))
			$tags = array_merge($tags,@$answers["caracter"]["cibleDDPrincipal"]);
		
		if(is_array(@$answers["caracter"]["cibleDDSecondaire"]) && count(@$answers["caracter"]["cibleDDSecondaire"])>0 )
			$tags = array_merge($tags,$answers["caracter"]["cibleDDSecondaire"]);
		else if(!empty($answers["caracter"]["cibleDDSecondaire"]))
			$tags[] = @$answers["caracter"]["cibleDDSecondaire"];
		

		foreach ($tags as $i => $t) {
			echo "<span class='badge bg-red' style='margin-bottom:3px;' >#".@$t."</span><br/>";
		}
	}else {
		echo "<h4>CARACTERISATION<br/></h4>";
		echo "<i class='fa fa-4x fa-question-circle-o'></i>";
	}
	?>
	</div>
</div>

<?php */ 

$newKey = [ array(
		"after" => "organization",
		"key" => "organizations",
		"value"=> array (
		    "title" => "Porteurs Associés",
		    "description" => "liste des organisations associés à cette fiche action",
		    "arrayForm" => true ,
		    "icon" => "fa-lightbulb-o",
		    "json" => array (
		        "jsonSchema" => array (
		            "title" => "Projets associés",
		            "icon" => "fa-lightbulb-o",
		            "properties" => array (
		                "orgasLinked" => array (
		                    "inputType" => "arrayForm",
		                    "label" => "Liste des co-porteurs",
		                    "properties" => array (
		                        "type" => array (
		                            "placeholder" => "Type"
		                        ),
		                        "id" => array (
		                            "placeholder" => "ID"
		                        ),
		                        "name" => array (
		                            "placeholder" => "Name"
		                        )
		                    )
		                )
		            )
		        )
		    )
		)
	)
];

$tmpForm =array();
$posCt = 0;
//introduce one or more steps in the process
foreach ( $form["scenario"] as $key => $value) {
	//echo $key."|".@$newKey[ $posCt ]["after"]."<br/>";
	$tmpForm[$key] = $value;
	if( $key ==  @$newKey[ $posCt ]["after"] ) {
		//echo " > ".$newKey[ $posCt ]["key"];
		$tmpForm[ $newKey[ $posCt ]["key"] ] = $newKey[ $posCt ][ "value" ];
		$posCt++;
	}
}
$form["scenario"] = $tmpForm;

$defaultColor = "#62A5B2"; 
$bleu = "#0A2F62";
?>
<h1 class='text-center' style="color:<?php echo $bleu ?>" >Montage et suivi des actions</h1>
<div style="margin-bottom: 30px">
Bienvenue dans l’espace collaboratif de suivi de votre dossier. Cet espace vous guide pas à pas depuis la déclaration de votre action jusqu’à son suivi dans le temps une fois réalisée. Une fois les premières étapes réalisées, cet espace devient visible des services de la collectivité, financeurs et partenaires institutionnels et de tout partenaire de votre territoire que vous souhaiterez convier à contribuer.
</div>

<div id="wizard" class="swMain">

    <style type="text/css">
        .swMain ul li > a.done .stepNumber {
            border-color: <?php echo $bleu ?>;
            background-color: #16A9B1; 
        }

        swMain > ul li > a.selected .stepDesc, .swMain li > a.done .stepDesc {
         color: <?php echo $bleu ?>;  
         font-weight: bolder; 
        }

        .swMain > ul li > a.selected::before, .swMain li > a.done::before{
          border-color: <?php echo $bleu ?>;      
        }
    </style>

 






<ul id="wizardLinks">
	<li>
        <a onclick="showStep('#userStep')" href="javascript:;" class="done"   >
        <div class="stepNumber">0</div>
        <span class="stepDesc">Auteur</span></a>
	</li>
	<?php 
	$count = 1;
	foreach ( $form[ "scenario" ] as $step => $dynF ) {
		//echo count(array_keys( $dynF["form"] ));
		echo "<li>";
	        echo '<a onclick="showStep(\'#'.$step.'\')" href="javascript:;" class="done" >';
	        echo '<div class="stepNumber">'.$count.'</div>';
	        echo '<span class="stepDesc">'.$dynF["title"].'</span></a>';
	    echo "</li>";  
	    $count++;
	}?>
	
	<li>
        <a onclick="showStep('#contractualiser')" href="javascript:;"  >
        <div class="stepNumber"><?php echo count($form[ "scenario" ])+1 ?></div>
        <span class="stepDesc">Contractualiser</span></a>
	</li>
	<li>
        <a onclick="showStep('#suivre')" href="javascript:;"  >
        <div class="stepNumber"><?php echo count($form[ "scenario" ])+2 ?></div>
        <span class="stepDesc">Suivre</span></a>
	</li>
</ul>

<div id='userStep' class='col-sm-12 stepperContent hide' style='padding-bottom:40px'>
	<div class='col-xs-12'>
		
		<h1>Auteur</h1>
		<table id="by"  class="table table-striped table-bordered table-hover  directoryTable" id="panelAdmin">
			
			<tbody>
				<tr>
					<td rowspan=2>
						<?php 
						$imgPath = (@$user["profilMediumImageUrl"] && !empty($user["profilMediumImageUrl"])) ? Yii::app()->createUrl('/'.$user["profilMediumImageUrl"]) : $this->module->getParentAssetsUrl().'/images/thumb/default_citoyens.png'; ?>  
						<img class="img-circle" height=70 width="70" src="<?php echo $imgPath ?>">
					</td>
					<td><a href="@<?php echo @$user["username"]; ?>"><?php echo @$user["name"]; ?></a></td>
				</tr>
		
				<tr>
					<td><?php echo @$user["email"]; ?></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div id='explain' class='col-sm-offset-1 col-sm-10 stepperContent hide' style='margin-top:30px; padding-bottom:40px'>
	<div class='col-xs-12'>
		<div class='col-xs-2 text-center'>
			<b style="color:<?php echo $bleu ?>">Jalon 1</b>
		</div>
		<div class='col-xs-10' style="border-bottom:1px solid #666;margin-bottom: 20px;padding-bottom: 10px;">
			<b style="color:<?php echo $bleu ?>">Déclarer et décrire l’action</b> : lors de la création initiale, le formulaire de déclaration a permis de déclarer un premier référent (personne qui a validé le formulaire), d’identifier une ou plusieurs structures porteuses et de décrire sommairement l’action. <br>
			<br>
			A ce stade, votre action existe et votre territoire la voit apparaître dans son tableau de bord.
		</div>
	</div>
	<br><br>
	<div class='col-xs-12'>
		<div class='col-xs-2 text-center'>
			<b style="color:<?php echo $bleu ?>">Jalon 2</b>
		</div>
		<div class='col-xs-10' style="border-bottom:1px solid #666;margin-bottom: 20px;padding-bottom: 10px;">
			<b style="color:<?php echo $bleu ?>">Caractériser et référencer l’action</b> : des étiquettes présentant des “domaines d’action” et des “cibles de développement durable” vous sont proposées et vous permettent ainsi de référencer votre action parmi toutes les actions du CTE. Cette caractérisation sera également utilisée pour vous aider à évaluer votre action.
			<br><br>
			A ce stade, votre action est référencée, dès que vous validez le jalon, votre action apparaît visible dans la base CTE (sa description, son portage et ses thématique clé sont visibles de tous avec la mention “action candidate”)

		</div>
	</div>
<br><br>
	<div class='col-xs-12'>
		<div class='col-xs-2 text-center'>
			<b style="color:<?php echo $bleu ?>">Jalon 3</b>
		</div>
		<div class='col-xs-10' style="border-bottom:1px solid #666;margin-bottom: 20px;padding-bottom: 10px;">
			<b style="color:<?php echo $bleu ?>">Collaborer et faire mûrir l’action</b> : Y sont précisés tous les éléments opérationnels de l’action (grandes tâches, planning, budget prévisionnel, demande de financement, recherche de partenariats, choix d’indicateurs d’évaluation…). Cette phase est collaborative : les services instructeurs, les têtes de réseaux, collèges de financeurs sont conviés à analyser les aspects techniques et financiers et à donner leur avis pour permettre à l’action d’aboutir. 
			<br><br>
			A ce stade, vos partenaires et les institutions référentes du territoire ont accès à votre dossier et peuvent interragir avec vous, commenter vos propositions et vous faire des suggestions

		</div>
	</div>
<br><br>
	<div class='col-xs-12'>
		<div class='col-xs-2 text-center'>
			<b style="color:<?php echo $bleu ?>">Jalon 4</b>
		</div>
		<div class='col-xs-10' style="border-bottom:1px solid #666;margin-bottom: 20px;padding-bottom: 10px;">
			<b style="color:<?php echo $bleu ?>">Contractualiser</b>: Sur la base des éléments renseignés en étape 3, l’ensemble des services instructeurs concernés valident les différents volets du dossier pour que la “fiche action” puisse-t-être annexée au contrat de CTE.
			<br><br>
			Cette partie n’est ouverte qu’aux services instructeurs et au porteur de projet.

		</div>
	</div>
<br><br>
	<div class='col-xs-12'>
		<div class='col-xs-2 text-center'>
			<b style="color:<?php echo $bleu ?>">Jalon 5</b>
		</div>
		<div class='col-xs-10' style="margin-bottom: 20px;">
			<b style="color:<?php echo $bleu ?>">Suivre</b> : Année après année, le porteur renseigne les indicateurs retenus
			<br><br>
			Cette partie n’est ouverte qu’aux services instructeurs et au porteur de projet.

		</div>
	</div>
</div>

<div id='contractualiser' class='col-sm-offset-1 col-sm-10 stepperContent hide' style='padding-bottom:40px'>
	<div class='col-xs-12'>
	
		<h1>Contractualiser</h1>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

	</div>

</div>

<div id='suivre' class='col-sm-offset-1 col-sm-10 stepperContent hide' style='padding-bottom:40px'>
	<div class='col-xs-12'>
	
		<h1>Suivre</h1>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

	</div>

</div>










<?php 

/* ---------------------------------------------
ETAPE DU SCENARIO
used when the answers carries many forms in the scenario
---------------------------------------------- */
$count = 1;
$log = false;
foreach ( $form[ "scenario" ] as $step => $dynF ) {
	//echo count(array_keys( $dynF["form"] ));
	$hide = ($count==3) ? "" : "hide";
	echo "<div id='".$step."' class='col-sm-12 stepperContent hide' style='padding-bottom:40px'>";

	$count++;
	//prepares dummy data with empty values if none exists for each field of the given scene
	if( !@$answers[$step] )
	{
		$answers["answers"] = array();
		if( $dynF["json"]['jsonSchema']["properties"] )
		{
			foreach ( $dynF["json"]['jsonSchema']["properties"] as $propkey => $prop ) 
			{
				if (isset($prop["properties"])){
					$answers[$step][$propkey] = []; 
				}
				else if ( isset($prop["arrayForm"]) && $prop["arrayForm"] == true)
					$answers[$step][$propkey] = array();
				else if (isset($prop["json"]['jsonSchema']["properties"]) ){
					$answers[$step][$propkey] = []; 
					foreach ($prop["json"]['jsonSchema']["properties"] as $propki => $propvi) {
						$answers[$step][$propkey][$propki] = "";
					}
				}
				else 
					$answers[$step][$propkey] = "";
			}
		}
	} 

//if($log)var_dump($answers[$step]); exit;
	if(isset($answers[$step]) ){  

		$editBtn = "";
		//when the content of this step is an Element 
		if(!isset($dynF["arrayForm"])) 
		{
			$editBtn = ( !empty($user) && (string)$user["_id"] == Yii::app()->session["userId"] ) 
				? "<a href='javascript:'  data-form='".$step."' data-step='".$step."' class='editStep btn btn-default'><i class='fa fa-pencil'></i></a>" 
				: "";
		}

		$titleIcon = ( isset($dynF['icon']) )? "<i class='fa ".@$dynF['icon']." ".@$dynF['titleClass']."'></i>" : "";
		?>
		
		<div class=" titleBlock col-xs-12 text-center"  >
			<h1 style="color:<?php echo $bleu ?>"> 
			<?php echo $dynF["title"]; ?>
			</h1>
		</div>

		<div class='col-xs-12' id='<?php echo $step; ?>'>

		<?php 

			echo "<div class='col-xs-12'>";
			
			if (isset($dynF["arrayForm"]) && $dynF["arrayForm"]==true)
			{
				//can contain many steps in one arrayForm
				//for each section 
				//get properties 
				// and build 
				
				$formQ = @$dynF["json"]["jsonSchema"]["properties"];
				foreach ( $formQ as $ik => $iv) 
				{
					// var_dump($iv);
					 
					echo '<table class="table table-bordered table-hover  directoryTable" id="panelAdmin'.$ik.'"><tbody class="directoryLines">';	
					$currentAnswers = array();

					if( count(array_keys($formQ))>1 ){
						if(isset($answers[$step][$ik]))
							$currentAnswers = $answers[$step][$ik];
					}
					else if($answers[$step])
						$currentAnswers = $answers[$step];

					 // var_dump($ik);
					 // var_dump($iv);
					 // var_dump($currentAnswers);
					//if( isset($iv["arrayForm"]) && $iv["arrayForm"]  ){
					
					if(isset($iv["answerTpl"]))
							echo $this->renderPartial( $iv["answerTpl"], array( "id"=>$_GET["id"],
																		  "answers"=>$currentAnswers,
																		  "df"=>$iv,
																		  "step"=>$step,
																		  "key"=>$ik));
					else {
						renderArrayFormPropertiesHeader ( $iv, $ik,$step ) ;
						$ct = 0;
						foreach ($currentAnswers as $q => $a) 
						{
							renderArrayFormPropertiesRows( $ik, $a, $step, $ct );
							$ct++;
						}
					}
					// } else 
						//when a non arrayForm is inside an arrayForm
						//to repair
					// 	renderAnswer( $props, $step,$answers[$step][$ik] );
					echo "</tbody></table>";
				}
			}
			//the scene is dynform.js : ex: project, organization
			// else if(is_array( $answers[$step] )) {
			// 	$formQ = @$dynF["json"]["jsonSchema"]["properties"];
			// echo '<table class="table table-striped table-bordered table-hover  directoryTable" id="panelAdmin'.$step.'"><tbody class="directoryLines">';	
			// 	renderArrayAnswer( $formQ, $step, $answers[$step] );
			// echo "</tbody></table>";
			// }

			else if ( isset($dynF["saveElement"]) )
			{
				//if($log) echo "render saveElement<br/>";
				if(isset($dynF["answerTpl"]))
				{
					//if($log)echo $dynF["answerTpl"];
					echo '<table class="table table-striped table-bordered table-hover  directoryTable" id="panelAdmin'.$step.'"><tbody class="directoryLines">';	
					echo $this->renderPartial( $dynF["answerTpl"], array("answers"=>$answers,"step"=>$step));
					echo "</tbody></table>";
				}
				else 
				{
					//if($log)echo "render automatic<br/>";
					echo '<table class="table table-striped table-bordered table-hover  directoryTable" id="panelAdmin'.$step.'"><tbody class="directoryLines">';	
					renderElementAnswers($answers,$step);
					echo "</tbody></table>";
				}
			}
			else if( isset($dynF["json"]) )
			{
				$props = @$dynF["json"]["jsonSchema"]["properties"];
				echo '<table class="table table-striped table-bordered table-hover  directoryTable" id="panelAdmin'.$step.'"><tbody class="directoryLines">';	
				renderAnswer( $props,$answers[$step] );
				echo "</tbody></table>";
			} 
			echo "</div>";
			
		
	} else { ?>
	<div class="bg-red col-xs-12 text-center text-large text-white margin-bottom-20">
		<h1> <?php echo $step;?> </h1>
	<?php 
		echo "<h3 style='' class=''> <i class='fa fa-2x fa-exclamation-triangle'></i> ".Yii::t("surveys","This step {num} hasn't been filed yet",array('{num}'=>$step))."</h3>".
			"<a href='".Yii::app()->createUrl('survey/co/index/id/'.$step)."' class='btn btn-success margin-bottom-10'>".Yii::t("surveys","Go back to this form")."</a>";

	}
	echo "</div>";

	echo "</div>";
 } 








function renderAnswer ( $prop, $a ) {	
	//echo "<br/>renderAnswer";
	foreach ($a as $sa => $sv) {
		echo '<tr>';
			echo "<td>".((isset($prop[$sa]["placeholder"])) ? $prop[$sa]["placeholder"] : $sa)."</td>";
			echo "<td>";
			if(is_array(@$sv))
				echo implode(",",$sv); 
			else 
				echo $sv;
			echo "</td>";
		echo '</tr>';
	}
}

function renderArrayFormPropertiesHeader($df,$key, $step){

	$props =  (isset($df["properties"])) ? $df["properties"] : array();
	$title = (isset($df["title"])) ? @$df["title"] : "";
	$editBtnL = " <a href='javascript:;' data-form='".$step."' data-step='".$step."' data-q='".$key."' class='addAF btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne</a>";
	
	echo '<thead><tr><td colspan='.(count($props)+1).' ><h3 style="color:#16A9B1">'.$title.$editBtnL.'</h3></td></tr>';
	echo '<tr>';
		foreach ( $props as $iik => $iiv) {
			echo "<th>".( ( isset($iiv["placeholder"]) ) ? $iiv["placeholder"] : $iik )."</th>";
		}
		echo "<th></th>";
	echo '</tr>';
	echo '</thead>';
}

function renderArrayFormPropertiesRows($key,$answer , $step,$ct)
{
	//echo "<br/>renderArrayFormPropertiesRows : ".$step."|".$key;
	//var_dump($answer);
	if(@$answer && is_array($answer))
	{
		echo '<tr>';
			foreach ($answer as $sa => $sv) {
				echo "<td>";
				if(is_array($sv))
					echo implode(",",$sv);
				else
					echo $sv;

				echo "</td>";
			}
			//TODO 
			// repair edit for the entries 
			// add link to partner
			echo "<td>".
				" <a href='javascript:;' data-form='".$step."' data-step='".$step."' data-q='".$key."' data-pos='".$ct."' class='editAF btn btn-xs btn-default'><i class='fa fa-pencil'></i></a>".
				" <a href='javascript:;' data-form='".$step."' data-step='".$step."' data-q='".$key."' data-pos='".$ct."' class='deleteAF btn btn-xs btn-danger'><i class='fa fa-times'></i></a>".
				' <a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentAnswer(\''.$key.$_GET["id"].'\')">'.PHDB::count(Comment::COLLECTION, array("contextId"=>$key.$_GET["id"],"contextType"=>Form::ANSWER_COLLECTION))." <i class='fa fa-commenting'></i></a></td>";
		echo '</tr>';
	}
}

function renderArrayAnswer($formQ, $q, $a){
	//echo "<br/>renderArrayAnswer:".$q;
	echo '<tr>';
		echo "<td>".@$formQ[ $q ]["placeholder"]."</td>";
		echo "<td>";
		$sep = "";
		foreach ($a as $sa => $sv) {
			if( is_array($sv) )
				$sv = implode(",", $sv);
			echo $sv.$sep;
			$sep = ", ";
		}
		echo "xxx</td>";
	echo '</tr>';
}

function renderElementAnswers($answers , $step){
	//if($log)echo "renderElementAnswers<br/>";
	if( isset($answers[$step]["type"] ) ) 
	{
		$editBtn = "<a href='javascript:'  data-form='".$step."' data-step='".$step."' data-type='".$answers[$step]["type"]."' data-id='".$answers[$step]["id"]."' class='editStep btn btn-default'><i class='fa fa-pencil'></i></a>"; 
		$editBtnL = "<div class='text-center'><a href='javascript:'  data-form='".$step."' data-step='".$step."' data-type='".$answers[$step]["type"]."' data-id='".$answers[$step]["id"]."' class='editStep btn btn-primary text-center'><i class='fa fa-pencil'></i> ".Yii::t("common","Edit")."</a></div>"; 
	}
	else {
		$editBtn = "<a href='javascript:'  data-form='".$step."' data-step='".$step."' class='createStep btn btn-default'><i class='fa fa-pencil'></i></a>";
		$editBtnL = "<div class='text-center'><a href='javascript:'  data-form='".$step."' data-step='".$step."' class='createStep btn btn-primary '><i class='fa fa-pencil'></i> ".Yii::t("common","Edit")."</a></div>";
	}

	if( isset($answers[$step]["type"])) {
		$el = Element::getByTypeAndId( $answers[$step]["type"] , $answers[$step]["id"] );

		echo '<tr>';
			echo "<td> ".Yii::t("common","Name")."</td>";
			echo "<td> <a target='_blank' class='btn btn-default lbh' href='#@".@$el["slug"]."'>".$el["name"]."</a></td>";
		echo '</tr>';

		if(isset($el["type"])){
			echo '<tr>';
				echo "<td>".Yii::t("common","Type")."</td>";
				echo "<td>".$el["type"]."</td>";
			echo '</tr>';
		}

		if(isset($el["description"])){
			echo '<tr>';
				echo "<td>".Yii::t("common", "Description")."</td>";
				echo "<td>".$el["description"]."</td>";
			echo '</tr>';
		}

		if(isset($el["tags"])){
			echo '<tr>';
				echo "<td>".Yii::t("common","Tags")."</td>";
				echo "<td>";
				$it=0;
				foreach($el["tags"] as $tags){
					if($it>0)
						echo ", ";
					echo "<span class='text-red'>#".$tags."</span>";
					$it++;
				}
				echo "</td>";
			echo '</tr>';
		}

		if(isset($el["shortDescription"])){
			echo '<tr>';
				echo "<td>".Yii::t("common","Short description")."</td>";
				echo "<td>".$el["shortDescription"]."</td>";
			echo '</tr>';
		}

		if(isset($el["email"])){
			echo '<tr>';
				echo "<td>".Yii::t("common","Email")."</td>";
				echo "<td>".$el["email"]."</td>";
			echo '</tr>';
		}
		
		if(!empty($el["profilThumbImageUrl"])){
			echo '<tr>';
				echo "<td>".Yii::t("common","Profil image")." </td>";
				echo "<td><img src='".Yii::app()->createUrl($el["profilThumbImageUrl"])."' class='img-responsive'/></td>";
			echo '</tr>';
		}

		if(isset($el["url"])){
			echo '<tr>';
				echo "<td>".Yii::t("common","Website URL")."</td>";
				echo "<td><a href='".$el["url"]."'>".$el["url"]."</a></td>";
			echo '</tr>';
		}

		
	}
	echo $editBtnL;
}

 ?>
	
</div>











<script type="text/javascript">
//if(typeof form == "undefined ")
var form = <?php echo json_encode($form); ?>;
var formSession = "<?php //echo $_GET["session"]; ?>";
//if(typeof answers == "undefined ")
var answers  = <?php echo json_encode($answers); ?>;
var projects  = <?php echo json_encode(@$projects); ?>;
var projectsDetails  = <?php echo json_encode(@$projectsDetails); ?>;
var projectsList = {};
var projectsLink = {};
var scenarioKey = "scenario";
var answerCollection = "answers";
var answerIdx = "<?php echo $_GET["id"] ?>";
var answerSectionBase = "answers."+form.id+".answers";
var answerSection = "";



$(document).ready(function() { 

    mylog.log("render","/modules/survey/views/custom/ctenat/dossierEdit.php")
  	coInterface.bindLBHLinks();
	if(projects != null){
		$.each(projects,function(i,el) {
			if(typeof answers.links != "undefined" &&
				typeof answers.links.projects != "undefined" &&
				typeof answers.links.projects[i] != "undefined")
				projectsLink[i] = el;
			else
				projectsList[i] = el;
		});
	}
	
		
	
	$('#doc').html( dataHelper.markdownToHtml( $('#doc').html() ) );
	
	$.each($('.markdown'),function(i,el) { 
		$(this).html( dataHelper.markdownToHtml( $(this).html() ) );	
	});

	$('.createStep').off().click(function() { 
		mylog.log('createStep ',$(this).data("form"));
		var dfCostum = ( form.scenario[ $(this).data("form") ]['costum']) ? form.scenario[ $(this).data("form") ]['costum']['dynFormCostum'] : null;
		updateForm = {
				form : $(this).data("form")
			};

		//il faut mettre a jour les answer avec les data de l'orga ou du project
		dfCostum.afterSave = function(elem){
			mylog.log("createStep afterSave Costum",elem);
			data = {
				collection : "answers",
				id : answerIdx,
				path : "answers."+form.id+".answers."+updateForm.form,
				value : {
					"type" : updateForm.form ,
                    "id"   : elem.id ,
                    "name" : elem.map.name
				}
			}
			mylog.log('createStep ',data);
			globalCtx = data;

			dataHelper.path2Value( data, function(params) { 
				dyFObj.commonAfterSave(data,function(){
					answerAfterSave();						
				});
			} );
		}

		dyFObj.openForm( $(this).data("form"), null, true, null,dfCostum );

		
	});
	$('.editStep').off().click(function() { 
		//alert('.editStep : '+$(this).data("form"));
		//editing typed elements like projects, organizations
		if( $(this).data("type") )
		{

			var dfCostum = ( form.scenario[ $(this).data("form") ]['costum']) ? form.scenario[ $(this).data("form") ]['costum']['dynFormCostum'] : null;
			mylog.log('.editStep','.editStep 1',$(this).data("form"),$(this).data("type"),$(this).data("id"),"dfCostum",dfCostum );
			dfCostum.afterSave = function(elem){
				 dyFObj.commonAfterSave(null,function(){
				 	answerAfterSave();
				});
			};
			dyFObj.editElement( $(this).data("type"), $(this).data("id"), $(this).data("type"),dfCostum );
		}
		else 
		{
			mylog.log('.editStep','.editStep 2',$(this).data("form"),$(this).data("type"),$(this).data("step"));

			//used for image upload 
			updateForm = {
				form : $(this).data("form"),
				step : $(this).data("step")	
			};

			mylog.log('.editStep',"path",$(this).data("form"));
			var editForm = form.scenario[$(this).data("form")].json;

			editForm.jsonSchema.onLoads = {
				onload : function(){
					dyFInputs.setHeader("bg-dark");
					$('.form-group div').removeClass("text-white");
					dataHelper.activateMarkdown(".form-control.markdown");
				}
			};
			

			editForm.jsonSchema.save = function(){
				//alert("save");
				data = {
	    			collection : "answers",
	    			id : answerIdx,
	    			path : "answers."+form.id+".answers."+updateForm.form,
	    			value : arrayForm.getAnswers(editForm , true)
	    		}
	    		mylog.log(".editStep","data",data);
				globalCtx = data;

				dataHelper.path2Value( data, function(params) { 
					dyFObj.commonAfterSave(null,function(){
						answerAfterSave();
					});
				} );
			    
			};
			var editData = answers[$(this).data("form")];

			
			dyFObj.editStep( editForm , editData);	
		}
	});

	$('.deleteAF').off().click(function() { 
		answerSection = answerSectionBase+"."+$(this).data("step")+"."+$(this).data("q");
		arrayForm.del($(this).data("form"),$(this).data("step"),$(this).data("q"),$(this).data("pos"));
	});

	$('.addAF').off().click(function() { 
		answerSection = answerSectionBase+"."+$(this).data("step")+"."+$(this).data("q");
		dyFObj.afterAfterSave = answerAfterSave;
		arrayForm.add( $(this).data("form"),$(this).data("step"),$(this).data("q"));
	});

	$('.editAF').off().click(function() { 
		answerSection = answerSectionBase+"."+$(this).data("step")+"."+$(this).data("q");
		dyFObj.afterAfterSave = answerAfterSave;
		arrayForm.edit($(this).data("form"),$(this).data("step"),$(this).data("q"),$(this).data("pos"));
	});

});

function showStep(id){
	
    $(".stepperContent").addClass("hide");
    $(id).removeClass("hide");
}

ctxDynForms = {
		organizations : {
			organizations : {
				orgasLinked : {
					title : "Porteurs associés",
		            icon : "fa-users",
					properties : "organization",
					costum : {
	                    "beforeBuild" : {
	                        "properties" : {
	                            "category" : {
	                                "inputType" : "hidden",
	                                "value" : "ficheAction"
	                            },
	                            "links[answers]" : {
	                                "inputType" : "hidden"
	                            },
	                            "links[projects]" : {
	                                "inputType" : "hidden"
	                            }
	                        }
	                    },
	                    "onload" : {
	                        "actions" : {
	                            "html" : {
	                            	"modal-title" : "Ajouter un porteur associé",
	                            	"infocustom" : "partenaires, CoPorteurs, Collaborateur de l'action, Enregistré et responsable d'une partie de l'action",
	                                "nametext > label" : "Noms du Porteurs associés"
	                            },
	                            "addClass" :{
	                            	"modal-header":"bg-dark"
	                            },
	                            "removeClass" :{
	                            	"modal-header":"bg-green"
	                            },
	                            "presetValue" : {
	                                "links[answers]" : answerIdx,
	                                "links[projects]" : {
	                                    "eval" : "answers.project.id"
	                                }
	                            },
	                            "hide" : {
	                                "urltext" : 1,
	                                "parentfinder" : 1,
	                                "publiccheckboxSimple" : 1
	                            }
	                        }
	                    },
	                    afterSave : function(elem){
							mylog.log("createStep afterSave Costum","answerIdx",answerIdx,"elem",elem);
							data = {
								collection : "answers",
								id : answerIdx,
								path : "answers."+form.id+".answers.organizations.orgasLinked."+elem.id,
								value : {
									"type" : "organization" ,
				                    "id"   : elem.id ,
				                    "name" : elem.map.name
								}
							}
							mylog.log('createStep ',data);
							globalCtx = data;

							dataHelper.path2Value( data, function(params) { 
								dyFObj.commonAfterSave(data,function(){
									answerAfterSave();	
								});
							} );
						}
	                }
					
				}
			}
		}
	}
function commentAnswer(answerId, pos){
	var modal = bootbox.dialog({
	        message: '<div class="content-risk-comment-tree"></div>',
	        title: "Fil de commentaire du risque",
	        buttons: [
	        
	          {
	            label: "Annuler",
	            className: "btn btn-default pull-left",
	            callback: function() {
	              
	            }
	          }
	        ],
	        onEscape: function() {
	          modal.modal("hide");
	        }
	    });
		modal.on("shown.bs.modal", function() {
		  $.unblockUI();
		  	getAjax(".content-risk-comment-tree",baseUrl+"/"+moduleId+"/comment/index/type/answers/id/"+answerId+"/path/"+location.hash+":answers.SigneLe9Avril2019.answers.murir.calendar."+pos,
			function(){  //$(".commentCount").html( $(".nbComments").html() ); 
			},"html");

		  //bindEventTextAreaNews('#textarea-edit-news'+idNews, idNews, updateNews[idNews]);
		});
	   // modal.modal("show");
	//}
}

function answerAfterSave () { 
	location.hash = location.hash.split(".")[0]+"."+location.hash.split(".")[1]+"."+location.hash.split(".")[2]+".subview."+$(".stepperContent:not(.hide").attr("id");
	pageProfil.params.subview = $(".stepperContent:not(.hide").attr("id");
	pageProfil.views.answers();
		//urlCtrl.loadByHash(location.hash);
	dyFObj.closeForm();
}

</script>
