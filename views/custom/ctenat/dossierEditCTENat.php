<style type="text/css">
	#central-container {padding-top: 0px;}
	table.table-bordered{
	    border:1px solid #ccc;
	    margin-top:20px;
	  }
table.table-bordered > thead > tr > th{
    border:1px solid #ccc;
}
table.table-bordered > tbody > tr > td{
    border:1px solid #ccc;
}
</style>
<?php 

$cssJS = array('/js/docs/docs.js');
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl());

$newKey = [ array(
		"after" => "organization",
		"key" => "organizations",
		"value"=> array (
		    "title" => "Porteurs Associés",
		    "description" => "liste des organisations associés à cette fiche action",
		    "arrayForm" => true ,
		    "icon" => "fa-lightbulb-o",
		    "json" => array (
		        "jsonSchema" => array (
		            "title" => "Projets associés",
		            "icon" => "fa-lightbulb-o",
		            "properties" => array (
		                "orgasLinked" => array (
		                    "inputType" => "arrayForm",
		                    "label" => "Liste des co-porteurs",
		                    "properties" => array (
		                        "type" => array (
		                            "placeholder" => "Type"
		                        ),
		                        "id" => array (
		                            "placeholder" => "ID"
		                        ),
		                        "name" => array (
		                            "placeholder" => "Name"
		                        )
		                    )
		                )
		            )
		        )
		    )
		)
	)
];

$tmpForm =array();
$posCt = 0;
//introduce one or more steps in the process
foreach ( $form["scenario"] as $key => $value) {
	//echo $key."|".@$newKey[ $posCt ]["after"]."<br/>";
	$tmpForm[$key] = $value;
	if( $key ==  @$newKey[ $posCt ]["after"] ) {
		//echo " > ".$newKey[ $posCt ]["key"];
		$tmpForm[ $newKey[ $posCt ]["key"] ] = $newKey[ $posCt ][ "value" ];
		$posCt++;
	}
}
$form["scenario"] = $tmpForm;

$defaultColor = "#62A5B2"; 
$bleu = "#0A2F62";

$communityLinks = Element::getCommunityByTypeAndId(Project::COLLECTION,$parent["id"]);
foreach ($communityLinks as $id => $co) {
	if( !isset($co["roles"]) || !in_array("Financeur", $co["roles"]) )
		unset( $communityLinks[$id] );		
}
$orgaFinancers = Link::groupFindByType(Organization::COLLECTION,$communityLinks,["name"] );
$orgs = [];
foreach ($orgaFinancers as $id => $or) {
	$orgs[$id] = $or["name"];
}

$listLabels = array_merge(Ctenat::$financerTypeList,$orgs);

?>
<div class='col-xs-12 col-sm-10 col-sm-offset-1' style="margin-bottom: 30px">
Bienvenue dans l’espace collaboratif de suivi de votre action.<br/>
Cet espace vous permet de renseigner pas à pas l’ensemble des informations relatives à votre action. A partir de l’étape 3, ces informations permettront aux partenaires publics techniques et financiers (collectivité(s), services de l’Etat, ADEME, banque des territoires, CEREMA…) de vous apporter leur avis sous forme de commentaires.<br/>
Enfin, vous pourrez suivre et évaluer votre action.
 <a href="javascript:;" onclick="showStep('#explain')"><i class="fa fa-info-circle fa-2x"></i></a>
</div>

<div id="wizard" class="swMain">

    <style type="text/css">
        .swMain ul li > a.done .stepNumber {
            border-color: <?php echo $bleu ?>;
            background-color: #16A9B1; 
        }

        swMain > ul li > a.selected .stepDesc, .swMain li > a.done .stepDesc {
         color: <?php echo $bleu ?>;  
         font-weight: bolder; 
        }

        .swMain > ul li > a.selected::before, .swMain li > a.done::before{
          border-color: <?php echo $bleu ?>;      
        }
    </style>

 






<ul id="wizardLinks">
	<li>
        <a onclick="showStep('#project')" href="javascript:;" class="done"   >
        <div class="stepNumber">1</div>
        <span class="stepDesc">Décrire l’action</span></a>
	</li>

	<li>
        <a onclick="showStep('#caracter')" href="javascript:;" class="done"   >
        <div class="stepNumber">2</div>
        <span class="stepDesc">Caractériser l'action</span></a>
	</li>

<?php 
$stepNum = 3;
if( !isset($adminAnswers["validation"]) || 
		  !isset($adminAnswers["validation"]["ctenat"]) ||  
		  !isset($adminAnswers["validation"]["cter"]) ||		  
		  !( isset($adminAnswers["validation"]["ctenat"]) && 
		  	in_array( @$adminAnswers["validation"]["ctenat"]["valid"], ["valid", "validReserve"]) &&  
		  	isset($adminAnswers["validation"]["cter"]) &&
		  	in_array( @$adminAnswers["validation"]["cter"]["valid"], ["valid", "validReserve"]) )  ) { ?>

	<li  id="murirBtn">
        <a onclick="showStep('#murir')" href="javascript:;" class="done"   >
        <div class="stepNumber"><?php echo $stepNum ?></div>
        <span class="stepDesc">Mûrir l’action</span></a>
	</li>

<?php } ?>

	<li>
		<?php 
		$prioClass = "";
		$isSuivi = false;
		if( isset($adminAnswers["validation"]) &&
		  isset($adminAnswers["validation"]["ctenat"]) &&
		  isset($adminAnswers["validation"]["ctenat"]["valid"]) &&
		  isset($adminAnswers["validation"]["cter"]) &&
		  isset($adminAnswers["validation"]["cter"]["valid"]) &&
//<<<<<<< HEAD
		  in_array( $adminAnswers["validation"]["ctenat"]["valid"], ["valid", "validReserve"]) &&
		  isset($adminAnswers["validation"]["cter"]["valid"]) && 
		  in_array( $adminAnswers["validation"]["cter"]["valid"], ["valid", "validReserve"])  ) { 
// =======
// 		  in_array( @$adminAnswers["validation"]["ctenat"]["valid"], ["valid", "validReserve"]) &&
// 		  in_array( @$adminAnswers["validation"]["cter"]["valid"], ["valid", "validReserve"])  ) { 
// >>>>>>> master
		  	$prioClass = "done";
		  	$isSuivi = true;
		}
		$stepNum++;
		?>
        <a onclick="showStep('#contractualiser')" href="javascript:;" class="<?php echo $prioClass ?>">
        <div class="stepNumber"><?php echo $stepNum ?></div>
        <span class="stepDesc">Contractualiser l’action</span></a>
	</li>

	<?php $stepNum++; ?>
	<li>
        <a onclick="showStep('#suivre')" href="javascript:;" class="<?php echo $prioClass ?>"  >
        <div class="stepNumber"><?php echo $stepNum ?></div>
        <span class="stepDesc">Suivre l’action</span></a>
	</li>
</ul>






<!-- *******************************
					PROJECT -->
<div id='project' class='col-sm-12 stepperContent hide' style='padding-bottom:40px'>
	<div class='col-xs-12'>
		<div class='col-xs-12 col-sm-offset-1 col-sm-10 margin-bottom-20'>
			<b style="color:<?php echo $bleu ?>">Décrire l’action et sa structure porteuse </b>: identifier une ou plusieurs structures porteuses et décrire sommairement l’action. 
			<br><br>
			A ce stade, votre action existe et votre territoire la voit apparaître dans son tableau de bord.
		</div>

		<!-- *******************************
				AUTEUR  -->
		<div class='col-xs-12'>
			<h3 style="color:#16A9B1">Auteur</h3>
			<table class="table table-bordered table-hover  ">
				<thead>
					<tr>
						<th></th>
						<th>Nom</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td rowspan=2>
							<?php 
							$imgPath = (@$user["profilMediumImageUrl"] && !empty($user["profilMediumImageUrl"])) ? Yii::app()->createUrl('/'.$user["profilMediumImageUrl"]) : $this->module->getParentAssetsUrl().'/images/thumb/default_citoyens.png'; ?>  
							<img class="img-circle" height=70 width="70" src="<?php echo $imgPath ?>">
						</td>
						<td><a class="lbh-preview-element" data-hash='#page.type.citoyens.id.<?php echo (string)$user["_id"] ?>' href="@<?php echo Yii::app()->createUrl($user["username"]); ?>"><?php echo @$user["name"]; ?></a></td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class='col-xs-12'>
			<!-- *******************************
				Structure porteuse -->
			<table class="table table-bordered table-hover  " id="panelAdminorganization">
				<?php 
				$editBtn="";
				if($canAdmin){
					$editBtn = ( isset($answers["organization"]["type"] ) && PHDB::findOneById(Organization::COLLECTION,$answers["organization"]["id"], ["_id"]) ) ? 
						"<a href='javascript:'  data-form='organization' data-step='organization' data-type='".$answers["organization"]["type"]."' data-id='".$answers["organization"]["id"]."' class='editStep btn btn-default'><i class='fa fa-pencil'></i> ".Yii::t("common","Edit")."</a>" 
						: "<a href='javascript:'  data-form='organization' data-step='organization' class='createStep btn btn-default'><i class='fa fa-pencil'></i></a>";
					if( isset($answers["organization"]["id"] ) && PHDB::findOneById(Organization::COLLECTION,$answers["organization"]["id"], ["_id"]))
						$editBtn = "";
				}
				?>
				<thead>
					<tr>
						<td colspan=5><h3 style="color:#16A9B1">Structure porteuse <?php echo $editBtn?></h3></td>
					</tr>	
					<tr>
						<th></th>
						<th>Nom de l'organisation</th>
						<th>Type</th>
					</tr>
				</thead>
				<tbody class="">
				<tr>
				<?php 
					if(isset($answers["organization"])){
						$orga = PHDB::findOne( Organization::COLLECTION, 
												array( "_id" => new MongoId( $answers["organization"]["id"] )), ["name","type","referantName","profilThumbImageUrl"] );
						$img = ( !empty( $orga["profilThumbImageUrl"] ) ) ? "<img src='".Yii::app()->createUrl( $orga["profilThumbImageUrl"] )."'  style='width:70px'/> " : "";
						echo "<tr>".
							"<td style='width:80px' >".$img."</td>".
							"<td><a class='lbh-preview-element' data-hash='#page.type.organizations.id.".$answers["organization"]["id"]."' data-type='organizations' data-id='".$answers["organization"]["id"]."' href='#page.type.organizations.id.".$answers["organization"]["id"]."' >".@$answers["organization"]["name"]."</a></td>".
							"<td>".@Yii::t("category",Organization::$types[$orga["type"]])."</td>".
						"</tr>";
					}
				?>
				</tr>
				</tbody>
			</table>			
		</div>

		<div class='col-xs-12'>
		<!-- *******************************
					Structure associé -->
		<?php 
			echo $this->renderPartial( 	"survey.views.custom.ctenat.orgasLinkedRow" , 
								[ 	"id" 		=> $_GET["id"],
								  	"answers" 	=> (isset($answers["organizations"]["orgasLinked"])) ? $answers["organizations"]["orgasLinked"] : array() ,
									"step"		=> "organizations",
									"key"		=> "orgasLinked", 
									"canAdmin"	=> $canAdmin ] );
		?>
		</div>

		<div class='col-xs-12'>
		<!-- *******************************
					Action -->
		<?php 
			renderElementAnswers($answers,"project",true,"Action", $canAdmin);
		?>
		</div>
	</div>
</div>



<!-- *******************************
					MURIR -->

<div id='murir' class='col-sm-12 stepperContent hide' style='padding-bottom:40px'>
	<div class='col-xs-12'>
		
		<div class='col-sm-offset-2 col-sm-8 margin-bottom-20' style="color:white; border:4px solid #0B5394; background-color: #3D85C6;">
			<b>Nota Bene</b> : les informations contenues dans cette étape ne sont pas publiques, elle seront visibles uniquement par les partenaires publics du CTE (collectivité(s) porteuse(s), services de l’Etat, ADEME, banque des territoires, CEREMA…) et par les partenaires que vous auriez vous-même ajouté (onglet “Administration”). Ces partenaires sont visibles dans l’onglet “Communauté” de l’action.<br/><br/>
			Une fois l’étape validée, tous ces partenaires pourront vous donner leur avis et vous apporter leur aide sous forme de commentaires.

		</div>
		
		<?php if(isset($canAdmin) && !empty($canAdmin)){ ?>
			<div class="col-xs-12 text-center padding-10">
				<button id="generateCopil" class="generateCopil  btn btn-primary" data-id="<?php echo $_GET["id"] ?>" data-date="<?php echo date("Y-m-d") ?>">Générer un copil</button>
			</div>
		<?php 	} ?>
		
		<div class="col-xs-12">
			<?php $countComment=PHDB::count(Comment::COLLECTION, array("contextId"=>$_GET["id"],"contextType"=>Form::ANSWER_COLLECTION, "path"=>"murir")); ?>
			<h3 style="color:#16A9B1">Commentaire général sur l'action</h3>
			<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('<?php echo Form::ANSWER_COLLECTION?>','<?php echo $_GET["id"] ?>','murir', 'Commentaires générales de la candidature')"><i class="fa fa-commenting"></i> <?php echo $countComment ?> commentaire<?php if($countComment>1) echo "s" ?></a>
		</div>
		<div class='col-xs-12'>
		<!-- *******************************
					calendar -->
		<?php  
			echo $this->renderPartial( "survey.views.custom.ctenat.calendarRow" , array( 
									"id" => $_GET["id"],
								  	"answers" => (isset($answers["murir"]["calendar"])) ? $answers["murir"]["calendar"] : array() ,
									"df" => $form["scenario"]["murir"]["json"]["jsonSchema"]["properties"]["calendar"],
									"step"=>"murir",
									"key"=>"calendar", 
									"canAdmin"=>$canAdmin));
		?>
		</div>


		<div class='col-xs-12'>
		<!-- *******************************
					partenaires -->
		<?php 
			renderArrayFormPropertiesHeader ( $form["scenario"]["murir"]["json"]["jsonSchema"]["properties"]["partenaires"], "murir","partenaires",true, $canAdmin ) ;
			$ct = 0;
			$data = (isset($answers["murir"]["partenaires"])) ? $answers["murir"]["partenaires"] : array(); 
			foreach ($data as $q => $a) 
			{
				renderArrayFormPropertiesRows( "murir", "partenaires", $a , $ct, $canAdmin );
				$ct++;
			}
			renderArrayFormPropertiesHeaderClose();
		?>
		</div>

		<div class='col-xs-12'>
		<!-- *******************************
					budget -->
		<?php 
			renderArrayFormPropertiesHeader ( $form["scenario"]["murir"]["json"]["jsonSchema"]["properties"]["budget"], "murir","budget",true, $canAdmin) ;
			$ct = 0;
			$data = (isset($answers["murir"]["budget"])) ? $answers["murir"]["budget"] : array(); 
			foreach ($data as $q => $a) 
			{
				renderArrayFormPropertiesRows( "murir", "budget", $a , $ct, $canAdmin);
				$ct++;
			}
			echo $this->renderPartial( "survey.views.custom.ctenat.totalRow" , 
									array( "data" => $data, 
										   "label"=> "Budget",
										   "span" => 4 ) );
			renderArrayFormPropertiesHeaderClose();
			$totalBudg = 0;
			foreach ($data as $q => $a) {
				$t = 0;
				if(!isset($a["valid"]) || $a["valid"]!="refused"){	
					if( isset($a["amount2019"]) )
						$t = $a["amount2019"];
					if( isset($a["amount2020"]) )
						$t = $t + $a["amount2020"];
					if( isset($a["amount2021"]) )
						$t = $t + $a["amount2021"];
					if( isset($a["amount2022"]) )
						$t = $t + $a["amount2022"];
				}
				$totalBudg = $totalBudg + $t;
			}
		?>
		</div>


		<div class='col-xs-12'>
		<!-- *******************************
					planFinancement -->
		<?php 
			renderArrayFormPropertiesHeader ( $form["scenario"]["murir"]["json"]["jsonSchema"]["properties"]["planFinancement"], "murir","planFinancement",true, $canAdmin,2 ) ;
			$ct = 0;
			$data = (isset($answers["murir"]["planFinancement"])) ? $answers["murir"]["planFinancement"] : array(); 
			foreach ($data as $q => $a) 
			{
				renderArrayFormPropertiesRows( "murir", "planFinancement", $a , $ct, $canAdmin,$listLabels,Organization::COLLECTION );
				$ct++;
			}
			echo $this->renderPartial( "survey.views.custom.ctenat.totalRow" , 
									array(  "data"  => $data,
											"label" => "Financement",
											//"decal"=>"<td></td>",
										   	"span"  => 3) );
			$totalFin = 0;
			foreach ($data as $q => $a) {
				if(!isset($a["valid"]) || $a["valid"]!="refused"){
					$t = 0;
					if( isset($a["amount2019"]) )
						$t = $a["amount2019"];
					if( isset($a["amount2020"]) )
						$t = $t + $a["amount2020"];
					if( isset($a["amount2021"]) )
						$t = $t + $a["amount2021"];
					if( isset($a["amount2022"]) )
						$t = $t + $a["amount2022"];
					$totalFin = $totalFin + $t;
				}
			}
			
			$percent = ($totalBudg) ? $totalFin*100/$totalBudg : 0;
			$delta = $totalFin - $totalBudg ;
			$col = "";
			$percol = "primary";
			if($delta > 0){
				$col = "text-green";
				$percol = "success";
			}
			else if($delta < 0 ){
				$col = "text-red";
				$percol = "danger";

			}
			renderArrayFormPropertiesHeaderClose();
		?>
		</div>

		<div class='col-xs-12 ' id="equilibreBudget">
			<h3 style="color:#16A9B1">EQUILIBRE BUDGETAIRE</h3>
			L'équilibre budgétaire doit être atteint pour pouvoir passer à la phase de contractualisation
			<div class="progress">
			  <div class="progress-bar progress-bar-<?php echo $percol ?>" style="width: <?php echo $percent ?>%">
			    <span class="sr-only">35% Complete (success)</span>
			  </div>
			</div>
			<table class="table table-bordered table-hover  ">
				<tbody class="">
					<tr>
						<td>BUDGET prévisionnel</td>
						<td><?php echo trim(strrev(chunk_split(strrev($totalBudg),3, ' '))) ?>€</td>
					</tr>
					<tr>
						<td>Financements acquis</td>
						<td><?php echo trim(strrev(chunk_split(strrev($totalFin),3, ' '))) ?> €</td>
					</tr>
					<tr>
						<td>Delta</td>
						<td><?php echo "<span class='".$col."'>".trim(strrev(chunk_split(strrev($delta),3, ' ')))." € </span>" ?> </td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class='col-xs-12'>
		<!-- *******************************
					results -->
		<?php 
			echo $this->renderPartial( "survey.views.custom.ctenat.resultsRow" , array( 
									"id" => $_GET["id"],
								  	"answers" => (isset($answers["murir"]["results"])) ? $answers["murir"]["results"] : array() ,
								  	"df" => $form["scenario"]["murir"]["json"]["jsonSchema"]["properties"]["results"],
								  	"step"=>"murir",
								  	"key"=>"results", 
								  	"isSuivi"=>$isSuivi,
								  	"answer" => $adminAnswers,
								  	"canAdmin"=>$canAdmin));
		?>
		</div>


		
		<div id="copilFile" class="col-xs-12">
			<div class="col-xs-12 table table-bordered table-hover  ">
				<h3 style="color:#16A9B1">Compte-rendu de COPIL</h3>
			<?php $files=Document::getListDocumentsWhere(array("id"=>$_GET["id"], "type"=>Form::ANSWER_COLLECTION, "doctype"=>"file", "subKey"=>array('$exists'=>false)),"file");
				if(!empty($files)){ ?>
					<div class="content-copil">
					<?php foreach($files as $k => $v){ ?>
						<div class='col-xs-12 padding-5 shadow2 margin-top-5 margin-bottom-5' id="<?php echo $k ?>">
							<a href='<?php echo $v["docPath"] ?>' target='_blank' class="link-files"><i class="fa fa-file-pdf-o text-red"></i> <?php echo $v["name"] ?></a>
							<?php if($canAdmin){ ?>
								<a href='javascript:;' class="pull-right text-red btn-remove-document" data-id="<?php echo $k ?>"><i class="fa fa-trash"></i> <?php echo Yii::t("common","Delete") ?></a>
							<?php } ?>
						</div>
				<?php	} ?>
					</div>
			<?php	}else{
					echo '<div class="content-copil">';
					echo "<span class='italic noCopilFile'>Aucun copil n'a été généré</span>";
					echo "</div>";
				}
			?>
			</div>
		</div>
	</div>
	<div class="form-prioritize" style="display:none;">
	  <form class="inputProritary" role="form">

		<a href="javascript:;" data-value="waiting" class="validateStatusFinance btn" style="width: 100%;">Attente de validation</a><br/><br/>
		<a href="javascript:;" data-value="validated" class="validateStatusFinance btn btn-success" style="width: 100%;">Validé sans réserve</a><br/><br/>
		<a href="javascript:;" data-value="reserved" class="validateStatusFinance btn btn-warning" style="width: 100%;">Validé avec réserves</a><br/><br/>
		<a href="javascript:;" data-value="refused"  class="validateStatusFinance btn btn-danger" style="width: 100%;">Non validé</a><br/><br/>
		<br/><br/>
	  </form>
	</div>
</div>


<!-- *******************************
					CARACTER -->
<?php 
//$props = $form["scenario"]["caracter"]["json"]["jsonSchema"]["properties"];
//renderAnswer( $props,@$answers["caracter"],"caracter",true,null ,$canAdmin );
echo $this->renderPartial( "survey.views.custom.ctenat.caracterRow",array("answers"=>@$answers["caracter"],"canAdmin"=>$canAdmin));
?>




<!-- *******************************
					EXPLAIN -->
<?php 
echo $this->renderPartial( "survey.views.custom.ctenat.explainRow",array("bleu"=>$bleu));
?>



<!-- *******************************
					CONTRACTUALISER -->
<div id='contractualiser' class='col-sm-offset-1 col-sm-10 stepperContent hide' style='padding-bottom:40px'>
	<div class='col-xs-12'>
	
		<h1>Contractualiser</h1>
		<?php 
		$canAdminCter = Authorisation::isElementAdmin( $parent["id"] , $parent["type"] , Yii::app()->session["userId"] );
		echo $this->renderPartial( "survey.views.custom.ctenat.contractualiser",
					array( 
						"canAdminCter" => $canAdminCter,
						"name"=>(!empty($answers["project"]) && !empty($answers["project"]["id"])) ? $answers["project"]["name"] : "",
						"validation"=>(isset($adminAnswers["validation"])) ? $adminAnswers["validation"] : null,
						"priorisation" => (isset($adminAnswers["priorisation"])) ? $adminAnswers["priorisation"] : null))	;?>

	</div>

</div>

<div id='suivre' class='col-sm-offset-1 col-sm-10 stepperContent hide' style='padding-bottom:40px'>
	<div class='col-xs-12'>
	
		<h1>Suivre</h1>
		Cette étape ne sera accessible qu’une fois l’action complétée et validée.

	</div>

</div>

<?php 

function renderAnswer ( $prop, $a,$step, $table=null,$title=null, $canAdmin=false ) 
{	
	$editBtn = ( $canAdmin )
		? "<a href='javascript:'  data-form='".$step."' data-step='".$step."' class='editStep btn btn-default'><i class='fa fa-pencil'></i> ".Yii::t("common","Edit")."</a>" 
		: "";
	if(isset($title))
		echo '<h3 style="color:#16A9B1;padding-top:20px;">'.$title.' '.$editBtn.'</h3>xxxxxxxxxxxxxxx<br>';

	if(isset($table))
		echo '<table class="table table-bordered table-hover  " id="panelAdmin'.$step.'"><tbody class="">';	
	
	//var_dump($a);exit;
	if(!empty($a)){
		foreach ($a as $sa => $sv) {
			//var_dump($sv);exit;
			echo '<tr>';
				echo "<td>".((isset($prop[$sa]["placeholder"])) ? $prop[$sa]["placeholder"] : $sa)."</td>";
				//echo "<td>";
				if(!empty($sv)){
					if(is_array($sv) && !is_object($sv))
						echo multi_implode($sv, ",");
					else 
						echo $sv;
				}
				echo "</td>";
			echo '</tr>';
		}
	}
	if(isset($table))
		echo "</tbody></table>";
}

function multi_implode($array, $glue) {
    $ret = '';

    foreach ($array as $item) {
        if (is_array($item)) {
            $ret .= multi_implode($item, $glue) . $glue;
        } else {
            $ret .= $item . $glue;
        }
    }

    $ret = substr($ret, 0, 0-strlen($glue));

    return $ret;
}

function renderArrayFormPropertiesHeader($df, $step,$key,$table=null, $canAdmin=false, $validate=false ) {
	if(isset($table))
		echo '<table class="table table-bordered table-hover  " id="panelAdmin'.$step.'">';	
	$props =  (isset($df["properties"])) ? $df["properties"] : array();
	$title = (isset($df["title"])) ? @$df["title"] : "";
	$editBtnL="";
	if($canAdmin){
		$btnclass = ($key == "planFinancement") ? "addAFAndNotify" : "addAF";
		$editBtnL = " <a href='javascript:;' data-form='".$step."' data-step='".$step."' data-notify='".$key."' data-q='".$key."' class='".$btnclass." btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne</a>";
	}
	$text = (isset($df["text"])) ? "<br>".$df["text"] : "" ;
	echo '<thead><tr><td colspan='.(count($props)+1).' ><h3 style="color:#16A9B1">'.$title.$editBtnL.'</h3>'.$text.'</td></tr>';
	
	echo '<tr style="border:none">';
		$ct = 0;
		foreach ( $props as $iik => $iiv) {
			$ct++;
			echo "<th>".( ( isset($iiv["placeholder"]) ) ? $iiv["placeholder"] : $iik )."</th>";
			//if($validate && $validate == $ct)
			//	echo "<th>Valider</th>";
			
		}
		echo "<th></th>";
	echo '</tr>';
	echo '</thead><tbody>';
}

function renderArrayFormPropertiesHeaderClose(){
	echo "</tbody></table>";
}


function renderArrayFormPropertiesRows($step,$key,$answer , $pos, $canAdmin=false,$listLabels=[],$type=null, $validate=false )
{
	//echo "<br/>renderArrayFormPropertiesRows : ".$step."|".$key;
	//var_dump($answer);
	if(isset($answer) && is_array($answer))
	{
		echo '<tr>';
			
			$titleComment = $key." ".(intval($pos)+1);
			$cot = 0;
			$once = true;
			if( $key == "planFinancement" ) {
				$financerType= ( ( isset($answer["financerType"]) && !empty($answer["financerType"]) && !empty($listLabels[$answer["financerType"]]) ) ? $listLabels[$answer["financerType"]] : "" );
				echo "<td>".$financerType."</td>";
				$finName = "";
				if( isset($answer["financer"])){
					$finName = $answer["financer"];
					if( is_string($answer["financer"]) && strlen($answer["financer"]) == 24 && ctype_xdigit($answer["financer"])){
						$org = PHDB::findOne(Organization::COLLECTION,["_id"=>new MongoId($answer["financer"])]);
						$finName = "<a class='lbh-preview-element'  data-hash='#page.type.organizations.id.".$answer["financer"]."' data-id='".$answer["financer"]."' data-type='organization' href='#page.type.organizations.id.".$answer["financer"]."'>".$org["name"]."</a>";
					} 
				}
				echo "<td>".$finName."</td>";
				echo "<td>".$answer["title"]."</td>";
			}
			//if(isset($answer["financerType"]))
			foreach ( $answer as $sa => $sv ) 
			{
				$cot++;
				$validTD = "";

				if(!in_array($sa,["valid", "financerType", "financer"]) && ($key!="planFinancement" || $sa!="title")){
					echo "<td>";

					//if($sa == "valid")
					//	continue;//dont do anything
					if(is_array($sv))
						echo implode(",",$sv);
					else if ( isset( $listLabels[ $sv ] ) ){
						if( is_string($sv) && strlen($sv) == 24 && ctype_xdigit($sv)){
							echo "<a class='lbh-preview-element'  data-hash='#page.type.".$type.".id.".$sv."' data-id='".$sv."' data-type='".$type."' href='#page.type.".$type.".id.".$sv."'>".$listLabels[ $sv ]."</a>";
							/*if($validate){
								$validTD = "<td class='text-center'>";
								$color = "default";
								$valbl = "?";
								if( isset($answer["valid"]["valid"])){
									if( $answer["valid"]["valid"] == "valid" ){
										$color = "success";
										$valbl = "V";
									} else if( $answer["valid"]["valid"] == "validReserve" ){
										$color = "warning";
										$valbl = "R";
									} else if( $answer["valid"]["valid"] == "notValid" ){
										$color = "danger";
										$valbl = "NV";
									}
								}
								$validTD .= "<a href='javascript:;' data-type='".$type."' data-id='".$sv."' data-pos='".$pos."'  class='btnValidFinance btn btn-xs btn-".$color."'>".$valbl."</a>";
								
								$validTD .= "</td>";
							}*/
						}
						else {
							echo $listLabels[ $sv ];
						}
					}
					else {
						$txt = $sv;
						if( $validate && $validate == $cot && $once){
							$once = false;
							$txt = "</td><td>".$txt;
						}
						echo $txt;
					}

					if(in_array($sa, ["poste", "qui"]))
						$titleComment .=" : ".$sv;
				//if(!in_array($sa,["valid"]))
					echo "</td>";	
				//echo $validTD;
				}
			}
			echo "<td>";

			if($canAdmin){
				echo " <a href='javascript:;' data-form='".$step."' data-step='".$step."' data-q='".$key."' data-pos='".$pos."' class='editAF btn btn-xs btn-default'><i class='fa fa-pencil'></i></a>".
					" <a href='javascript:;' data-form='".$step."' data-step='".$step."' data-q='".$key."' data-pos='".$pos."' class='deleteAF btn btn-xs btn-danger'><i class='fa fa-times'></i></a>";
			}
			echo '<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview(\''.Form::ANSWER_COLLECTION.'\',\''.$_GET["id"].'\',\''.$step.$key.$pos.'\', \''.$titleComment.'\')">'.PHDB::count(Comment::COLLECTION, array("contextId"=>$_GET["id"],"contextType"=>Form::ANSWER_COLLECTION, "path"=>$step.$key.$pos)).' <i class="fa fa-commenting"></i></a>';

				if($key=="planFinancement"){
					$color = "default";
					$valbl = "?";
					$tool= "En attente de validation";
					if( isset($answer["valid"]) ){
						if( $answer["valid"] == "validated" ){
							$color = "success";
							$valbl = "V";
							$tool="Validé sans réserve";
						} else if( $answer["valid"] == "reserved" ){
							$color = "warning";
							$valbl = "R";
							$tool="Validé avec réserves";
						} else if( $answer["valid"] == "refused" ){
							$color = "danger";
							$valbl = "NV";
							$tool="Non validé";
						}
					}
					echo "<a href='javascript:;' data-type='".$type."' data-id='".$sv."' data-pos='".$pos."'  class='btnValidFinance btn btn-xs btn-".$color." margin-left-5 padding-10 tooltips' data-toggle='tooltip' data-placement='left' data-original-title='".$tool."'>".$valbl."</a>";
				}
			echo "</td>";
		echo '</tr>';
	}
}

function renderArrayAnswer($formQ, $q, $a){
	//echo "<br/>renderArrayAnswer:".$q;
	echo '<tr>';
		echo "<td>".@$formQ[ $q ]["placeholder"]."</td>";
		echo "<td>";
		$sep = "";
		foreach ($a as $sa => $sv) {
			if( is_array($sv) )
				$sv = implode(",", $sv);
			echo $sv.$sep;
			$sep = ", ";
		}
		echo "</td>";
	echo '</tr>';
}



function renderElementAnswers($answers , $step, $table=null,$title=null, $canAdmin=false){
	//if($log)echo "renderElementAnswers<br/>";
	
	$editBtn = ( isset($answers[$step]["type"] ) ) 
			? "<a href='javascript:'  data-form='".$step."' data-step='".$step."' data-type='".$answers[$step]["type"]."' data-id='".$answers[$step]["id"]."' class='editStep btn btn-default'><i class='fa fa-pencil'></i> ".Yii::t("common","Edit")."</a>" 
			: "<a href='javascript:'  data-form='".$step."' data-step='".$step."' class='createStep btn btn-default'><i class='fa fa-pencil'></i></a>";
	if(empty($canAdmin))
		$editBtn="";
	if(isset($title))
		echo '<h3 style="color:#16A9B1;padding-top:20px;">'.$title." ".$editBtn.'</h3>';
	else 
		echo "<div class='text-center'>".$editBtn."</div>";

	if(isset($table))
		echo '<table class="table table-bordered table-hover  " id="panelAdmin'.$step.'"><tbody class="">';	
	
	if( isset($answers[$step]["type"])) {
		$el = Element::getByTypeAndId( $answers[$step]["type"] , $answers[$step]["id"] );

		echo '<tr>';
			echo "<td> ".Yii::t("common","Name")."</td>";
			echo "<td> <a class='lbh-preview-element' data-hash='#page.type.".$answers[$step]["type"]."s.id.".$answers[$step]["id"]."' href='#@".@$el["slug"]."'>".$el["name"]."</a></td>";
		echo '</tr>';

		if(isset($el["shortDescription"])){
			echo '<tr>';
				echo "<td>".Yii::t("common","Short description")."</td>";
				echo "<td>".$el["shortDescription"]."</td>";
			echo '</tr>';
		}


		if(isset($el["description"])){
			echo '<tr>';
				echo "<td >".Yii::t("common", "Description")."</td>";
				echo "<td class='markdown'>".$el["description"]."</td>";
			echo '</tr>';
		}

		if(isset($el["expected"])){
			echo '<tr>';
				echo "<td>".Yii::t("common", "Attente")."</td>";
				echo "<td class='markdown'>".$el["expected"]."</td>";
			echo '</tr>';
		}

		if(isset($el["tags"])){
			echo '<tr>';
				echo "<td>".Yii::t("common","Tags")."</td>";
				echo "<td>";
				$it=0;
				foreach($el["tags"] as $tags){
					if($it>0)
						echo ", ";
					echo "<span class='text-red'>#".$tags."</span>";
					$it++;
				}
				echo "</td>";
			echo '</tr>';
		}

		

		if(isset($el["email"])){
			echo '<tr>';
				echo "<td>".Yii::t("common","Email")."</td>";
				echo "<td>".$el["email"]."</td>";
			echo '</tr>';
		}
		
		if(!empty($el["profilThumbImageUrl"])){
			echo '<tr>';
				echo "<td>".Yii::t("common","Profil image")." </td>";
				echo "<td><img src='".Yii::app()->createUrl($el["profilThumbImageUrl"])."' class='img-responsive'/></td>";
			echo '</tr>';
		}

		if(isset($el["url"])){
			echo '<tr>';
				echo "<td>".Yii::t("common","Website URL")."</td>";
				echo "<td><a href='".$el["url"]."'>".$el["url"]."</a></td>";
			echo '</tr>';
		}

		
	}
	

	if(isset($table))
		echo "</tbody></table>";
}
$actionsIndicator = array();
$cibleIndicator = array();
if(!empty($answers["caracter"]["actionPrincipal"]))
	$actionsIndicator[] = $answers["caracter"]["actionPrincipal"];

if(!empty($answers["caracter"]["actionSecondaire"]))
	$actionsIndicator = array_merge($actionsIndicator, $answers["caracter"]["actionSecondaire"]);

if(!empty($answers["caracter"]["cibleDDPrincipal"]))
	$cibleIndicator[] =  $answers["caracter"]["cibleDDPrincipal"];

if(!empty($answers["caracter"]["cibleDDSecondaire"]))
	$cibleIndicator = array_merge($cibleIndicator, $answers["caracter"]["cibleDDSecondaire"]);

$pId = null;
if(!empty($answers["project"]) && !empty($answers["project"]["id"]))
	$pId = $answers["project"]["id"];
$indicators = Ctenat::getIndicator($actionsIndicator, $cibleIndicator, array("name","unity1"), $pId);
 ?>
	
</div>


<script type="text/javascript">
//if(typeof form == "undefined ")
var form = <?php echo json_encode($form); ?>;

var formSession = "<?php //echo $_GET["session"]; ?>";
//if(typeof answers == "undefined ")
var answers  = <?php echo json_encode($answers); ?>;
var projects  = <?php echo json_encode(@$projects); ?>;
var projectsDetails  = <?php echo json_encode(@$projectsDetails); ?>;
var projectsList = {};
var projectsLink = {};
var scenarioKey = "scenario";
var answerCollection = "answers";
var answerIdx = "<?php echo $_GET["id"] ?>";
var answerSectionBase = "answers."+form.id+".answers";
var answerSection = "";

var actionsIndicator = <?php echo json_encode($actionsIndicator); ?>;
var cibleIndicator = <?php echo json_encode($cibleIndicator); ?>;
var indicators = <?php echo json_encode($indicators); ?>;
var dialog = null;

$(document).ready(function() { 
	costum.lists.financerTypeList = <?php echo json_encode(Ctenat::$financerTypeList); ?>;
	costum.lists.financersList = <?php echo json_encode($orgs); ?>;
	costum.lists["indicators"] = indicators;
    mylog.log("render","/modules/survey/views/custom/ctenat/dossierEditCTENat")

  	coInterface.bindLBHLinks();
  	coInterface.bindTooltips();

  	<?php if( isset($adminAnswers["validation"]) &&
		  isset($adminAnswers["validation"]["ctenat"]) &&
		  isset($adminAnswers["validation"]["cter"]) &&
		  in_array( @$adminAnswers["validation"]["ctenat"]["valid"], ["valid", "validReserve"]) &&
		  in_array( @$adminAnswers["validation"]["cter"]["valid"], ["valid", "validReserve"])  ) { ?>
  	$("#suivre").html($("#murir").html())
	<?php } ?>

  	documentManager.bindEvents();
  	$("#copyEquilibreBudgetaire").html($("#equilibreBudget").html());
	if(projects != null){
		$.each(projects,function(i,el) {
			if(typeof answers.links != "undefined" &&
				typeof answers.links.projects != "undefined" &&
				typeof answers.links.projects[i] != "undefined")
				projectsLink[i] = el;
			else
				projectsList[i] = el;
		});
	}
	
	$(".generateCopil").click(function(){
		idAnswer=$(this).data("id"),
		dataNow=$(this).data("date");
		var titleCopil=$(this).data("title");
		var subKey=$(this).data("key")
		//window.open(baseUrl+"/costum/ctenat/generatecopil/id/"+idAnswer+"/title/COPIL010203/date/"+dataNow);
        msgBoot='<div class="row">  ' +
                      '<div class="col-md-12"> ' +
                        '<form class="form-horizontal"> ' ;
                          if(!notNull(titleCopil)){
                         msgBoot+= '<label class="col-md-12 no-padding" for="awesomeness">Nom du copil : </label><br> ' +                        
                          '<input type="text" id="nameFavorite" class="nameSearch wysiwygInput form-control" value="" style="width: 100%" placeholder="'+tradDynForm.addname+'..."></input>'
                      		}
                         msgBoot += "<br>"+
                    		'<label class="col-md-12 no-padding" for="awesomeness">Noms de participants : </label><br> ' +
                          	'<textarea class="col-xs-12" id="participantsCopil"></textarea><br>'+
                          	'<label class="col-md-12 no-padding" for="awesomeness">Date du copil à récupérer : </label><br> ' +
                          	'<input id="dateCopil" type="date" value="'+dataNow+'">'+
                    		'</div> </div>' +
                    '</form></div></div>';            
		bootbox.dialog({
            title: "Remplissez les informations du COPIL",
            message: msgBoot,
            buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {
                    	
                    	var dialog = bootbox.dialog ({
						    message: '<p class="text-center mb-0 loadingPdf"></p>',
						    closeButton: false
						});
            			
            			coInterface.showLoader(".loadingPdf", "Création du copil en cours");
            			$(".loadingPdf .processingLoader").removeClass("col-xs-12 margin-top-50");
                        
                        var formData={
                          "title": (notNull(titleCopil)) ? titleCopil : $("#nameFavorite").val(),
                          "participants":$("#participantsCopil").val(),
                          "date":$("#dateCopil").val(),
                          "copil":true
                        };
                        if(notNull(subKey))
                        	formData.subKey=subKey;
                        
                        $.ajax({
						  type: "POST",
						  url: baseUrl+"/costum/ctenat/generatecopil/id/"+idAnswer,
						  data: formData,
						  success: function(data){
						  		toastr.success("Le copil a été créé avec succès");
						  		targetDom=(notNull(subKey))? "#copilFile"+subKey+" .content-copil": "#copilFile .content-copil";
						  		$(targetDom+ ".noCopilFile").remove();
						  		$(targetDom).prepend("<div class='col-xs-12 padding-5 shadow2 margin-top-5 margin-bottom-5'>"+"<a href='"+baseUrl+"/"+data.docPath+"' target='_blank' class='link-files'><i class='fa fa-file-pdf-o text-red'></i> "+ data.fileName+"</a>"+
											"</div>");
						  		coInterface.scrollTo("#copilFile");
								// do something in the background
								dialog.modal('hide');

						  },
						  dataType: "json"
						});
                    }
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: function() {
                  }
                }
              }
      	});
	});

	$(".generateFicheAction").click(function(){
		idAnswer=$(this).data("id"),
		dataNow=$(this).data("date");
		var titleFA=$(this).data("title");
		var subKey=$(this).data("key")
		//window.open(baseUrl+"/costum/ctenat/generatecopil/id/"+idAnswer+"/title/COPIL010203/date/"+dataNow);
        msgBoot='<div class="row">' +
                  '<div class="col-md-12"> Générer le contenu de cette fiche action en l\'état' +    
                '</div></div>';            
		bootbox.dialog({
            title: "Figer cette fiche action",
            message: msgBoot,
            buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {
                    	var dialog = bootbox.dialog ({
						    message: '<p class="text-center mb-0 loadingPdf"></p>',
						    closeButton: false
						});
            			coInterface.showLoader(".loadingPdf", "Création de la fiche action en cours");
            			$(".loadingPdf .processingLoader").removeClass("col-xs-12 margin-top-50");
                        var formData = {
                          "title": titleFA,
                          "date":dataNow,
                          "save":true
                        };
                        if(notNull(subKey))
                        	formData.subKey=subKey;
                        $.ajax({
						  type: "POST",
						  url: baseUrl+"/co2/export/pdfelement/id/"+idAnswer+"/type/answers/",
						  data: formData,
						  success: function(data){
						  		toastr.success("La fiche action a été créé avec succès");
						  		$(".modal").modal('hide');
						  		urlCtrl.loadByHash(location.hash);
						  },

						  dataType: "json"
						});
                    }
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: function() {
                  }
                }
              }
      	});
	});
	
	$('#doc').html( dataHelper.markdownToHtml( $('#doc').html() ) );
	
	$.each($('.markdown'),function(i,el) { 
		$(this).html( dataHelper.markdownToHtml( $(this).html() ) );	
	});

	$('.btnValidFinance').off().click(function() { 
		var posFinance = $(this).data("pos");
		prioModal = bootbox.dialog({
	        message: $(".form-prioritize").html(),
	        title: "État du Financement",
	        show: false,
	        onEscape: function() {
	          prioModal.modal("hide");
	        }
	    });
	    prioModal.modal("show");

	    $(".validateStatusFinance").off().on("click",function() { 
		
            if($(this).data("value")!="waiting"){
	        	//var today = new Date();
				//today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();
	            var formData = {
		    		id : answerIdx,
		    		collection : "answers",
		    		path : "answers.<?php echo $adminAnswers["formId"] ?>.answers.murir.planFinancement."+posFinance+".valid",
	        		value : $(this).data("value")/*{
	        			valid : $(this).data("value"),
	        			user : userId,
	        			date : today
	        		}*/
	            };
	                        
		    	

		  	 	dataHelper.path2Value( formData, function(params) { 
		  	 		prioModal.modal('hide');
		  	 		urlCtrl.loadByHash(location.hash);
		  	 	} );
		  	 }else
		  	 	prioModal.modal('hide');
		  	
      	});
	});
	$('.createStep').off().click(function() { 
		mylog.log('createStep ',$(this).data("form"));
		var dfCostum = ( form.scenario[ $(this).data("form") ]['costum']) ? form.scenario[ $(this).data( "form" ) ]['costum']['dynFormCostum'] : null;
		updateForm = {
			form : $(this).data("form")
		};

		//il faut mettre a jour les answer avec les data de l'orga ou du project
		dfCostum.afterSave = function(elem){
			mylog.log("createStep afterSave Costum",elem);
			data = {
				collection : "answers",
				id : answerIdx,
				path : "answers."+form.id+".answers."+updateForm.form,
				value : {
					"type" : updateForm.form ,
                    "id"   : elem.id ,
                    "name" : elem.map.name
				}
			}
			mylog.log('createStep ',data);
			globalCtx = data;

			dataHelper.path2Value( data, function(params) { 
				dyFObj.commonAfterSave(data,function(){
					answerAfterSave();						
				});
			} );
		}

		dyFObj.openForm( $(this).data("form"), null, true, null,dfCostum );



		dataHelper.path2Value( {
			collection : contextData.type,
			id : contextData.id,
			path : "showAAP",
			value : true
		}, function(){} );
		
	});
	$('.editStep').off().click(function() { 
		//alert('.editStep : '+$(this).data("form"));
		//editing typed elements like projects, organizations
		if( $(this).data("type") )
		{

			var dfCostum = ( form.scenario[ $(this).data("form") ]['costum']) ? form.scenario[ $(this).data("form") ]['costum']['dynFormCostum'] : null;
			mylog.log('.editStep','.editStep 1',$(this).data("form"),$(this).data("type"),$(this).data("id"),"dfCostum",dfCostum );
			dfCostum.afterSave = function(elem){
				 dyFObj.commonAfterSave(null,function(){
				 	answerAfterSave();
				});
			};
			dyFObj.editElement( $(this).data("type"), $(this).data("id"), $(this).data("type"),dfCostum );
		}
		else 
		{
			mylog.log('.editStep','.editStep 2',$(this).data("form"),$(this).data("type"),$(this).data("step"));

			//used for image upload 
			updateForm = {
				form : $(this).data("form"),
				step : $(this).data("step")	
			};

			mylog.log('.editStep',"path",$(this).data("form"));
			var editForm = form.scenario[$(this).data("form")].json;

			editForm.jsonSchema.onLoads = {
				onload : function(){
					dyFInputs.setHeader("bg-dark");
					$('.form-group div').removeClass("text-white");
					dataHelper.activateMarkdown(".form-control.markdown");
				}
			};
			

			editForm.jsonSchema.save = function(){
				//alert("save");
				data = {
	    			collection : "answers",
	    			id : answerIdx,
	    			path : "answers."+form.id+".answers."+updateForm.form,
	    			value : arrayForm.getAnswers(editForm , true)
	    		}
	    		mylog.log(".editStep","data",data);
				globalCtx = data;

				dataHelper.path2Value( data, function(params) { 
					dyFObj.commonAfterSave(null,function(){
						answerAfterSave();
					});
				} );
			    
			};
			var editData = answers[$(this).data("form")];

			
			dyFObj.editStep( editForm , editData);	
		}
	});

	$('.deleteAF').off().click(function() { 
		answerSection = answerSectionBase+"."+$(this).data("step")+"."+$(this).data("q");
		arrayForm.del($(this).data("form"),$(this).data("step"),$(this).data("q"),$(this).data("pos"));
	});


	$('.addAF').off().click(function() { 
		answerSection = answerSectionBase+"."+$(this).data("step")+"."+$(this).data("q");
		dyFObj.afterAfterSave = answerAfterSave;
		arrayForm.add( $(this).data("form"),$(this).data("step"),$(this).data("q"));
	});

	$('.addAFAndNotify').off().click(function() { 
		answerSection = answerSectionBase+"."+$(this).data("step")+"."+$(this).data("q");
		dyFObj.afterAfterSave = notify;
		notifyType = $(this).data("notify");
		arrayForm.add( $(this).data("form"),$(this).data("step"),$(this).data("q"));
	});

	$('.editAF').off().click(function() { 
		answerSection = answerSectionBase+"."+$(this).data("step")+"."+$(this).data("q");
		dyFObj.afterAfterSave = answerAfterSave;
		arrayForm.edit($(this).data("form"),$(this).data("step"),$(this).data("q"),$(this).data("pos"));
	});
});
var notifyType = null;
function connectToAnswer( type,id,name, slug, email )  {  
	mylog.log( "connectToAnswer",type,id,name, slug, email );
	if(type == "organizations"){
		data = {
			collection : "answers",
			id : answerIdx,
			path : "answers."+form.id+".answers.organizations.orgasLinked",
			arrayForm : true,
			value : {
                "type" : type,
                "id" : id,
                "name" : name
            }
		}

		mylog.log("connectToAnswer","data",data);
		globalCtx = data;
		dataHelper.path2Value( data, function(params) { 
			dyFObj.closeForm();
			urlCtrl.loadByHash(location.hash);
		} );
	}
}

function showStep(id){
    $(".stepperContent").addClass("hide");
    $(id).removeClass("hide");
    //alert(location.hash.split(".")[0]+"."+location.hash.split(".")[1]+"."+location.hash.split(".")[2]+".subview."+$(".stepperContent:not(.hide").attr("id"));
    history.pushState(null, null, location.hash.split(".")[0]+"."+location.hash.split(".")[1]+"."+location.hash.split(".")[2]+".subview."+$(".stepperContent:not(.hide").attr("id"));
}

var ctxDynForms = {
	organizations : {
		organizations : {
			orgasLinked : {
				title : "Ajouter un porteur",
	            icon : "fa-users",
				properties : "organization",
				costum : {
                    "beforeBuild" : {
                        "properties" : {
                        	"type" : {
                                "label" : "Type d'organisation",
                                "inputType" : "select",
                                "placeholder" : "---- Type d'organisation ----",
                                "options" : {
                                    "NGO":"Association",
                                    "LocalBusiness":"Entreprise",
                                    "GovernmentOrganization":"Service Public"
                                }
                            },
                            
                            "category" : {
                                "inputType" : "hidden",
                                "value" : "ficheAction"
                            },
                            "links[answers]" : {
                                "inputType" : "hidden"
                            },
                            "links[projects]" : {
                                "inputType" : "hidden"
                            },
                            "roles" : {
                                "inputType" : "hidden"
                            },
                            "actionPrincipal" : {
	                            "inputType" : "select",
	                            "label" : "Domaines d’action de votre organisation",
	                            "placeholder" : "Choisir un domaine d'action principal",
	                            "groupOptions" : true,
	                            "groupSelected" : false,
	                            "list" : "domainAction",
	                            "select2" : {
	                                "multiple" : true
	                            },
	                            "order" : 6
	                        },
	                        "shortDescription" : {
                                "inputType" : "textarea",
                                "label" : "Description courte de l’orga",
                            },
                            "sectionReferant" : {
				                "inputType" : "custom",
				                "html":"<hr/>",
				                 "order" : 12
				            },
	                        "sectionReferant" : {
				                "inputType" : "custom",
				                "html":"<hr/><div class='margin-top-20'><b>Informations relative à l’administration de cette organisation</b> (la personne recevra une notification par mail)</div>",
				                 "order" : 13
				            },
				            "referantName" : {
                                "inputType" : "text",
                                "label" : "Nom de l’administrateur de cette organisation",
                                "order" : 14
                            },
	                        "email" : {
	                        	"inputType" : "text",
						    	"label" : "Email de l’administrateur ",
						    	"placeholder" : "exemple@mail.com" ,
	                        	"order" : 15
	                        }
                        }
                    },
                    "onload" : {
                        "actions" : {
                            "html" : {
                            	"modal-title" : "Ajouter un porteur",
                                "nametext > label" : "Nom de l’organisation"
                            },
                            "addClass" :{
                            	"modal-header":"bg-dark"
                            },
                            "removeClass" :{
                            	"modal-header":"bg-green"
                            },
                            "presetValue" : {
                                "links[answers]" : answerIdx,
                                "links[projects]" : {
                                    "eval" : "contextData.id"
                                },
                                "role" : "creator",
                                "roles":"Porteur d\'action"
                            },
                            "hide" : {
                                "urltext" : 1,
                                "tagstags" : 1,
                                "parentfinder" : 1,
                                "publiccheckboxSimple" : 1,
                                "roleselect" : 1,
                                "infocustom":1
                            }
                        }
                    },
                    afterSave : function(elem){
						mylog.log("createStep afterSave Costum","answerIdx",answerIdx,"elem",elem);
						data = {
							collection : "answers",
							id : answerIdx,
							arrayForm : true,
							path : "answers."+form.id+".answers.organizations.orgasLinked",
							value : {
								"type" : "organization" ,
			                    "id"   : elem.id ,
			                    "name" : elem.map.name
							}
						}
						mylog.log('createStep ',data);
						globalCtx = data;

						dataHelper.path2Value( data, function(params) { 
							dyFObj.commonAfterSave(data,function(){
								answerAfterSave();	
							});
						} );
					}
                }
				
			}
		}
	}
}

function answerAfterSave () { 
	location.hash = location.hash.split(".")[0]+"."+location.hash.split(".")[1]+"."+location.hash.split(".")[2]+".subview."+$(".stepperContent:not(.hide").attr("id");
	pageProfil.params.subview = $(".stepperContent:not(.hide").attr("id");
	pageProfil.views.answers();
		//urlCtrl.loadByHash(location.hash);
	dyFObj.closeForm();
}

function notify () { 
	
	answerAfterSave ();

	if(notifyType == "planFinancement" && dyFObj.path2Value && notEmpty(dyFObj.path2Value.params.value.financer)) {	
		

		//check financer is part of community and role Financer
		//launch action linking project to orga with role Financeur + orga to project
		// and createNotification to the financer
		//alert("ajax"+form.id+":"+dyFObj.path2Value.params.value.financer);
		ajaxPost( '' , baseUrl+'/costum/ctenat/notifinancer', { 
		  	cter : form.id,
		  	financer : dyFObj.path2Value.params.value.financer,
		  	project : answers.project.id
		  }, function(){toastr.success("Le copil a été créé avec succès");}, "json");
		
	}
}

</script>
