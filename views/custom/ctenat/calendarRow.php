<table class="table table-bordered table-hover  directoryTable" id="panelAdmin<?php echo $key?>">
	

<?php 
	$dateSections = array("01/01/2018","01/07/2018","01/01/2019","01/07/2019","01/01/2020","01/07/2020","01/01/2021","01/07/2021","01/01/2022","01/07/2022","01/01/2023");

	$editBtnL = ($canAdmin) ? " <a href='javascript:;' data-form='".$step."' data-step='".$step."' data-q='".$key."' class='addAF btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";
?>	
<thead>
	<tr>
		<td colspan='<?php echo count( $dateSections)+2?>' ><h3 style="color:#16A9B1"><?php echo $df["title"].$editBtnL?></h3>
			Indiquez ici le calendrier prévisionnel du projet et de ses différentes étapes. Par exemple, faut-il une étude préalable ou un appel d’offres et le cas échéant quand sont-ils prévus ? Quelles sont les échéances prévues pour le lancement des travaux ? l’achèvement des travaux?
		</td>
	</tr>	
	<?php if(count($answers)>0){ ?>
	<tr>
		<th>Actions<br/>
		</th>
		<th>1er<br/>Sem<br/>2018</th>
		<th>2ème<br/>Sem<br/>2018</th>
		<th>1er<br/>Sem<br/>2019</th>
		<th>2ème<br/>Sem<br/>2019</th>
		<th>1er<br/>Sem<br/>2020</th>
		<th>2ème<br/>Sem<br/>2020</th>
		<th>1er<br/>Sem<br/>2021</th>
		<th>2ème<br/>Sem<br/>2021</th>
		<th>1er<br/>Sem<br/>2022</th>
		<th>2ème<br/>Sem<br/>2022</th>
		<th></th>
	</tr>
	<?php } ?>
</thead>
<tbody class="directoryLines">	
	<?php 
	$ct = 0;
	
	
	foreach ($answers as $q => $a) {

		echo "<tr>".
			"<td>".@$a["step"]."</td>";
		$bgColor = "white";
		$td = "";
		foreach ($dateSections as $sa => $sv) {
			if( $bgColor == "white"){
				if( isset( $dateSections[$sa+1] ) && strtotime(str_replace('/', '-',@$a["startDate"])) < strtotime(str_replace('/', '-',$dateSections[$sa+1]))  ) {
					$bgColor = "#5EAC87";
					$td = "&bull;";
				}
			} else if( $bgColor == "#5EAC87"){
				if( strtotime(str_replace('/', '-',@$a["endDate"])) < strtotime(str_replace('/', '-',$sv))  ) {
					$bgColor = "";
					$td = "";
				}
			}
			if( $sa != sizeof( $dateSections ) - 1 )
				echo "<td class='text-center' style='border: 1px solid #ddd;background-color:".$bgColor."'>".$td."</td>";
		}
	?>
	<td>
		<?php if ($canAdmin){ ?>
		<a href='javascript:;' data-form='<?php echo $step?>' data-step='<?php echo $step?>' data-q='<?php echo $key?>' data-pos='<?php echo $ct?>' class='editAF btn btn-xs btn-default'><i class='fa fa-pencil'></i></a>
		<a href='javascript:;' data-form='<?php echo $step?>' data-step='<?php echo $step?>' data-q='<?php echo $key?>' data-pos='<?php echo $ct?>' class='deleteAF btn btn-xs btn-danger'><i class='fa fa-times'></i></a>
		<!-- $step.$key.$ct -->
		<?php } ?>
		<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $id?>','<?php echo $step.$key.$ct ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$id,"contextType"=>"answers", "path"=>$step.$key.$ct))?> <i class='fa fa-commenting'></i></a>
	</td>
	<?php 
		$ct++;
		echo "</tr>";
	}
	 ?>
	</tbody>
</table>

<script type="text/javascript">
	
$(document).ready(function() { 
    mylog.log("render","/modules/survey/views/custom/ctenat/calendarRow.php");
});
</script>