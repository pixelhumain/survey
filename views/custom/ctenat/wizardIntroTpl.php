<div class="col-xs-12 col-sm-10 col-sm-offset-1" style="margin-bottom: 30px">
Bienvenue dans l’espace collaboratif de suivi de votre action.<br>
Cet espace vous permet de renseigner pas à pas l’ensemble des informations relatives à votre action. A partir de l’étape 3, ces informations permettront aux partenaires publics techniques et financiers (collectivité(s), services de l’Etat, ADEME, banque des territoires, CEREMA…) de vous apporter leur avis sous forme de commentaires.<br>
Enfin, vous pourrez suivre et évaluer votre action.<br>
 <a href="#forum" class="lbh"> Pour poser vos questions, rendez vous sur le forum <i class="fa fa-info-circle fa-2x"></i></a>
</div>