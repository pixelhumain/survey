	<?php 

$total2019 = 0;
$total2020 = 0;
$total2021 = 0;
$total2022 = 0;
foreach ($data as $q => $a) {
	if(!isset($a["valid"]) || $a["valid"]!="refused"){		
		if(isset($a["amount2019"]))
			$total2019 = $total2019 + $a["amount2019"];
		if(isset($a["amount2020"]))
			$total2020 = $total2020 + $a["amount2020"];
		if(isset($a["amount2021"]))
			$total2021 = $total2021 + $a["amount2021"];
		if(isset($a["amount2022"]))
			$total2022 = $total2022 + $a["amount2022"];
	}
}
$budgetTotal = $total2019 + $total2020 + $total2021 + $total2022;
if($budgetTotal> 0){
	echo "<tr class='bold'>".
		"<td></td>";
		if($label=="Financement")
			echo "<td></td>";
	echo (( isset($decal)) ? $decal : "" ).
		"<td>TOTAL : </td>".
		"<td>".(($total2019 == 0) ? "" : trim(strrev(chunk_split(strrev($total2019),3, ' ')))."€")."</td>".
		"<td>".(($total2020 == 0) ? "" : trim(strrev(chunk_split(strrev($total2020),3, ' ')))."€")."</td>".
		"<td>".(($total2021 == 0) ? "" : trim(strrev(chunk_split(strrev($total2021),3, ' ')))."€")."</td>".
		"<td>".(($total2022 == 0) ? "" : trim(strrev(chunk_split(strrev($total2022),3, ' ')))."€")."</td>".
		"<td></td>".
	"</tr>";

}

	echo "<tr class='bold'>".
		"<td></td>".
		"<td colspan=".$span." style='text-align:right'>".strtoupper($label)." TOTAL : </td>".
		"<td colspan=3>".trim(strrev(chunk_split(strrev($budgetTotal),3, ' ')))." €</td>".
	"</tr>";

?>