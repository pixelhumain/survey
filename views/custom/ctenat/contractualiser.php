<?php 
//$canAdminCter = true;
if($canAdminCter || isset($priorisation) && $priorisation == Ctenat::STATUT_ACTION_CONTRACT ){ ?>

<div id="copyEquilibreBudgetaire"></div>

<table class="table table-bordered table-hover  directoryTable" >
	<tbody class="directoryLines">	
	
		<tr>
			<td colspan='2' ><h3 style="color:#16A9B1">Validation en Copil - réunion de finalisation</h3>
			</td>
		</tr>
		<?php if(isset($validation["cter"])){ ?>
		<tr>
			<td colspan='2' align="center">
				<button id="generateCopil2" class="generateCopil btn btn-primary" data-id="<?php echo $_GET["id"] ?>" data-date="<?php echo date("Y-m-d") ?>" data-title="Compte Rendu de la réunion de finalisation" data-key="copilReunionFinalisation">Générer le copil de la réunion finale</button>
		<?php $ficheAction=Document::getListDocumentsWhere([ "id"=>$_GET["id"], "type"=>Form::ANSWER_COLLECTION, "doctype"=>"file", "subKey"=>"ficheActionReunionFinalisation" ] ,"file");
			if(empty($ficheAction))
			{ ?>
				<button id="generateFicheAction" class="generateFicheAction btn btn-primary" data-id="<?php echo $_GET["id"] ?>" data-date="<?php echo date("Y-m-d") ?>" data-copil="ficheAction" data-title="Fiche Action Finale <?php echo $name ?>" data-key="ficheActionReunionFinalisation">Générer la version finale de la fiche action</button>	
			<?php } ?>
			</td>
		</tr>
		<?php } ?>
		<?php if(isset($validation["cter"])){ 
			$color = "danger";
			$lbl = "Non validé";
			if(isset($validation["cter"]["valid"])){
				if( $validation["cter"]["valid"] == "valid" ){
					$color = "success";
					$lbl = "Validé sans réserves";
				} else if( $validation["cter"]["valid"] == "validReserve" ){
					$color = "warning";
					$lbl = "Validé avec réserves";
				}
			}

			$editState = ( isset( $validation["ctenat"] ) ) ? "" : " <a href='javascript:;' data-type='cter' class='validEdit btn btn-default btn-xs'><i class='fa fa-pencil'></i></a>";
			

			?>
		<tr>
			<td>Avis</td>
			<td><h4 class="label label-<?php echo $color?>"><?php echo $lbl?></h4> <?php echo $editState ?></td>
		</tr>
		<tr>
			<td>Commentaire global</td>
			<td><?php if( isset($validation["cter"]["description"] ) ) echo $validation["cter"]["description"]; ?></td>
		</tr>
		<tr>
			<td>Date</td>
			<td><?php if( isset($validation["cter"]["date"] ) ) echo $validation["cter"]["date"]; ?></td>
		</tr>
		<?php 
		if(!empty($ficheAction))
			{ 
				//$ficheAction = array_splice($ficheAction, count($ficheAction)-1);
				$histo = 0;
				foreach($ficheAction as $k => $v){ 
					if($histo < 1){?>
					<tr>
						<td>Fiche Action Finale</td>
						<td><a href='<?php echo $v["docPath"] ?>' target='_blank' class="link-files"><i class="fa fa-file-pdf-o text-red"></i> <?php echo $v["name"] ?></a></td>
					</tr>
				<?php	$histo++; }
				} 
			} ?>

		<?php }  ?>
		<tr>
			<td colspan='2' class="text-center" >
				<a href="javascript:;" data-type="cter" class="validEdit btn btn-primary">Valider cette action</a>				
			</td>
		</tr>
	</tbody>
</table>

<?php if(isset($validation["cter"])){ ?>
<div id="copilFilecopilReunionFinalisation" class="col-xs-12">
	<div class="col-xs-12 table table-bordered table-hover  ">
		<h3 style="color:#16A9B1">Compte-rendu de COPIL</h3>
	<?php $files=Document::getListDocumentsWhere([ "id"=>$_GET["id"], "type"=>Form::ANSWER_COLLECTION, "doctype"=>"file", "subKey"=>"copilReunionFinalisation" ] ,"file");
		if(!empty($files))
		{ ?>
			<div class="content-copil">
			<?php foreach($files as $k => $v){ ?>
				<div class='col-xs-12 padding-5 shadow2 margin-top-5 margin-bottom-5' id="<?php echo $k ?>">
					<a href='<?php echo $v["docPath"] ?>' target='_blank' class="link-files"><i class="fa fa-file-pdf-o text-red"></i> <?php echo $v["name"] ?></a>
					<a href='javascript:;' class="pull-right text-red btn-remove-document" data-id="<?php echo $k ?>"><i class="fa fa-trash"></i> <?php echo Yii::t("common","Delete") ?></a>
				</div>
		<?php	} ?>
			</div>
	<?php } else {
			echo '<div class="content-copil">';
			echo "<span class='italic noCopilFile'>Aucun copil n'a été généré</span>";
			echo "</div>";
		}
	?>
	</div>
</div>
<?php } ?>


<table class="table table-bordered table-hover  directoryTable" >
	<tbody class="directoryLines">	
		<tr>
			<td colspan='2' ><h3 style="color:#16A9B1">Avis - Etat Regional</h3>
			</td>
		</tr>
		<?php if(isset($validation["etat"])){ ?>
		<tr>
			<td colspan='2' align="center" >
				<button id="generateCopil2" class="generateCopil btn btn-primary" data-id="<?php echo $_GET["id"] ?>" data-date="<?php echo date("Y-m-d") ?>" data-title="AVIS de l’Etat Régional" data-key="copilEtatRegional">Générer le copil de l'Etat régional</button>
			</td>
		</tr>
		<?php } ?>
		<?php if(isset($validation["etat"])){ 
			$color = "danger";
			$lbl = "Non validé";
			if(isset($validation["etat"]["valid"])){
				if( $validation["etat"]["valid"] == "valid" ){
					$color = "success";
					$lbl = "Validé sans réserves";
				} else if( $validation["etat"]["valid"] == "validReserve" ){
					$color = "warning";
					$lbl = "Validé avec réserves";
				}
			}
			?>
		<tr>
			<td>Avis</td>
			<td><h4 class="label label-<?php echo $color?>"><?php echo $lbl?></h4></td>
		</tr>
		<tr>
			<td>Commentaire</td>
			<td><?php if( isset($validation["etat"]["description"] ) ) echo $validation["etat"]["description"]; ?></td>
		</tr>
		<tr>
			<td>Date</td>
			<td><?php if( isset($validation["etat"]["date"] ) ) echo $validation["etat"]["date"]; ?></td>
		</tr>
		<?php } ?>
		<tr>
			<td colspan='2' class="text-center" >
				<a href="javascript:;" data-type="etat" class="validEdit btn btn-default">Votre avis sur cette action</a>
			</td>
		</tr>
	</tbody>
</table>
<?php if(isset($validation["etat"])){ ?>
<div id="copilFilecopilEtatRegional" class="col-xs-12">
	<div class="col-xs-12 table table-bordered table-hover  ">
		<h3 style="color:#16A9B1">Compte-rendu de COPIL</h3>
	<?php $files=Document::getListDocumentsWhere(array("id"=>$_GET["id"], "type"=>Form::ANSWER_COLLECTION, "doctype"=>"file", "subKey"=>"copilEtatRegional"),"file");
		if(!empty($files)){ ?>
			<div class="content-copil">
			<?php foreach($files as $k => $v){ ?>
				<div class='col-xs-12 padding-5 shadow2 margin-top-5 margin-bottom-5' id="<?php echo $k ?>">
					<a href='<?php echo $v["docPath"] ?>' target='_blank' class="link-files"><i class="fa fa-file-pdf-o text-red"></i> <?php echo $v["name"] ?></a>
					<a href='javascript:;' class="pull-right text-red btn-remove-document" data-id="<?php echo $k ?>"><i class="fa fa-trash"></i> <?php echo Yii::t("common","Delete") ?></a>
				</div>
		<?php	} ?>
			</div>
	<?php	}else{
			echo '<div class="content-copil">';
			echo "<span class='italic noCopilFile'>Aucun copil n'a été généré</span>";
			echo "</div>";
		}
	?>
	</div>
</div>
<?php } ?>

<table class="table table-bordered table-hover  directoryTable" >
	<tbody class="directoryLines">	
		<tr>
			<td colspan='2' ><h3 style="color:#16A9B1">Validation equipe CTE national</h3>
			</td>
		</tr>
		<?php if(isset($validation["ctenat"])){ ?>
		<tr>
			<td colspan='2' align="center">
				<button id="generateCopil2" class="generateCopil btn btn-primary" data-id="<?php echo $_GET["id"] ?>" data-date="<?php echo date("Y-m-d") ?>" data-title="Validation de l’Equipe Nationale" data-key="copilCTENat">Générer le copil de l'Equipe Nationale</button>
			</td>
		</tr>
		<?php } ?>
		<?php if(isset($validation["ctenat"])){ 
			$color = "danger";
			$lbl = "Non validé";
			if(isset($validation["ctenat"]["valid"])){
				if( $validation["ctenat"]["valid"] == "valid" ){
					$color = "success";
					$lbl = "Validé sans réserves";
				} else if( $validation["ctenat"]["valid"] == "validReserve" ){
					$color = "warning";
					$lbl = "Validé avec réserves";
				}
			}
			?>
		<tr>
			<td>Avis</td>
			<td><h4 class="label label-<?php echo $color?>"><?php echo $lbl?></h4></td>
		</tr>
		<tr>
			<td>Commentaire</td>
			<td><?php if( isset($validation["ctenat"]["description"] ) ) echo $validation["ctenat"]["description"]; ?></td>
		</tr>
		<tr>
			<td>Date</td>
			<td><?php if( isset($validation["ctenat"]["date"] ) ) echo $validation["ctenat"]["date"]; ?></td>
		</tr>
		<?php } ?>
		<tr>
			<td colspan='2' class="text-center" >
				<a href="javascript:;" data-type="ctenat" class="validEdit btn btn-primary">Valider cette action</a>
			</td>
		</tr>
		
	</tbody>
</table>
<?php if(isset($validation["ctenat"])){ ?>
<div id="copilFilecopilCTENat" class="col-xs-12">
	<div class="col-xs-12 table table-bordered table-hover  ">
		<h3 style="color:#16A9B1">Compte-rendu de COPIL</h3>
	<?php $files=Document::getListDocumentsWhere(array("id"=>$_GET["id"], "type"=>Form::ANSWER_COLLECTION, "doctype"=>"file", "subKey"=>"copilCTENat"),"file");
		if(!empty($files)){ ?>
			<div class="content-copil">
			<?php foreach($files as $k => $v){ ?>
				<div class='col-xs-12 padding-5 shadow2 margin-top-5 margin-bottom-5' id="<?php echo $k ?>">
					<a href='<?php echo $v["docPath"] ?>' target='_blank' class="link-files"><i class="fa fa-file-pdf-o text-red"></i> <?php echo $v["name"] ?></a>
					<a href='javascript:;' class="pull-right text-red btn-remove-document" data-id="<?php echo $k ?>"><i class="fa fa-trash"></i> <?php echo Yii::t("common","Delete") ?></a>
				</div>
		<?php	} ?>
			</div>
	<?php	}else{
			echo '<div class="content-copil">';		
			echo "<span class='italic noCopilFile'>Aucun copil n'a été généré</span>";
			echo "</div>";
		}
	?>
	</div>
</div>
<?php } ?>

<?php if( isset($validation["ctenat"]) && 
		  isset($validation["cter"]) ){

		if( isset($validation["cter"]["valid"]) && 
			isset($validation["ctenat"]["valid"]) && 
			in_array( $validation["ctenat"]["valid"], ["valid", "validReserve"]) && 
		  in_array( $validation["cter"]["valid"], ["valid", "validReserve"])  ) { ?>
		
<table class="table table-bordered table-hover  directoryTable" >
	<tbody class="directoryLines">	
	<?php if( $priorisation != Ctenat::STATUT_ACTION_CONTRACT ){ ?>
	<thead>
		<tr>
			<td><h3 style="color:red"><i class='fa fa-gavel'></i> Finaliser le Contrat </h3>
			</td>
		</tr>	
	</thead>
	<?php } ?>

	<body>
		<tr style="background-color: #eee">
			<td class="text-center">
				<?php if( $priorisation == Ctenat::STATUT_ACTION_VALID ){ ?>
					<h1 style="color:#24284D"><?php echo Ctenat::STATUT_ACTION_VALID ?></h1><br/>
					<img width=150 src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/planClimat.png">
					<img width=150 src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/Logo_republique-francaise.png">
				<?php } else {?>
					<a href="javascript:;" data-type="ctenat" class="changerStatus btn btn-primary">Contractualiser</a>
				<?php } ?>
			</td>
		</tr>		
	</tbody>
</table>
<?php } else { ?>
<div class="col-xs-12 text-center">
	<h1 style="color:#24284D"><?php echo Ctenat::STATUT_ACTION_REFUSE ?></h1><br/>
	<img width=150 src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/planClimat.png">
	<img width=150 src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/Logo_republique-francaise.png">
</div>
<?php }
 }?>

<?php } else {?>
	<div class="col-xs-12">
		Seuls les référents du territoire, le porteur de l’action et l’équipe nationale ont accès à cette étape.
	</div>
<?php } ?>


<script type="text/javascript">

$(document).ready(function() { 

    mylog.log("render","/modules/survey/views/custom/ctenat/contractualiser.php");
    documentManager.bindEvents();
    typeObj.validation = {
		    jsonSchema : {
			    title : "Validation",
			    icon : "gavel",
			    onLoads : {
			    	sub : function(){
		    		 	dyFInputs.setSub("bg-red");
		    		},
			    },
			    save : function() { 	    	
			   
					var formData = $("#ajaxFormModal").serializeFormJSON();
					var today = new Date();
					today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();
			    	data = {
			    		id : answerIdx,
			    		collection : "answers",
			    		path : "validation."+formData.who,
                		value : {
                			//who : formData.who,
                			valid : formData.valid,
                			description : formData.description,
                			date : today
                		}
			    	};
			    	
			    	

			  	 	dataHelper.path2Value( data, function(params) { 
			  	 		dyFObj.closeForm();
			  	 		urlCtrl.loadByHash(location.hash);
			  	 	} );

						
			    },
			    properties : {
			    	info : {
		                inputType : "custom",
		                html:"<p></p>",
		            },
		            valid : {
                        "label" : "Valider",
                        "inputType" : "select",
                        "placeholder" : "---- Selectionner Valider ----",
                        "options" : {
                        	"notValid":"Non validé",
                            "validReserve" : "Validé avec réserves",
                            "valid" : "Validé sans réserve"
                        }
                    },
		            description : dyFInputs.textarea( tradDynForm["description"], "..." ),
		      		//  date : {
				    // 	inputType : "date",
				    // 	label : "Date",
				    // 	placeholder : "Date de Validation",
				    // 	rules : { 
				    // 		required : true,
				    // 		greaterThanNow : ["DD/MM/YYYY"]
				    // 	}
				    // },
		            who : dyFInputs.inputHidden()
			    }
			}
		};
    $('.validEdit').off().on("click", function() {
    	var setVal = { who : $(this).data("type")};
		dyFObj.openForm( typeObj.validation, null, setVal );
	});

	$('.changerStatus').off().on("click", function() {
		data = {
    		id : answerIdx,
    		collection : "answers",
    		path : "priorisation",
    		value : "<?php echo Ctenat::STATUT_ACTION_VALID ?>"
    	};

    	

  	 	dataHelper.path2Value( data, function(params) { 
  	 		urlCtrl.loadByHash(location.hash);
  	 	} );
	});
    $("#copyEquilibreBudgetaire").html($("#equilibreBudget").html());
});
</script>