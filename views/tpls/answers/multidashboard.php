<?php
$cssJS = array(
    // '/plugins/gridstack/css/gridstack.min.css',
    // '/plugins/gridstack/js/gridstack.js',
    // '/plugins/gridstack/js/gridstack.jQueryUI.min.js'

);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);


HtmlHelper::registerCssAndScriptsFiles(array(
    '/css/graphbuilder.css',
    '/js/form.js',
    '/js/dashboard.js',
    '/css/aap/aapGlobalDashboard.css'
), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );

$modalrender = true;
$renderUrl = @$urlR;

$exp_url = explode("/", $renderUrl);
$el_slug = null;
$el_form_id = "";

if (isset($this->costum["slug"])){
    $slug = $this->costum["slug"];
}

if (
    in_array("slug", $exp_url) &&
    isset($exp_url[array_search('slug', $exp_url) + 1]) &&
    is_string($exp_url[array_search('slug', $exp_url) + 1]) &&
    $exp_url[array_search('slug', $exp_url) + 1] != ""
){
    $el_slug = $exp_url[array_search('slug', $exp_url) + 1];
}

if (
    in_array("formid", $exp_url) &&
    isset($exp_url[array_search('formid', $exp_url) + 1]) &&
    is_string($exp_url[array_search('formid', $exp_url) + 1]) &&
    $exp_url[array_search('formid', $exp_url) + 1] != ""
){
    $el_form_id = $exp_url[array_search('formid', $exp_url) + 1];
    $elform = PHDB::findOneById(Form::COLLECTION, $el_form_id);
    if(!empty($elform)){
        $el_configform = PHDB::findOneById(Form::COLLECTION, $elform["config"]);
        $elanswers = PHDB::find(Form::ANSWER_COLLECTION,array( "form" => (string)$elform["_id"],"answers.aapStep1.titre"=>['$exists' => true]));
    }
}
?>

<style>
    .btn_cont {
        width: 550px;
        height: 60px;
        position: relative;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -1%);
        padding-top: 20px;
    }
    .btn_cont_right {
        width: 550px;
        height: 60px;
        position: relative;
        left: 20px;
        top: 50%;
        padding-top: 20px;
    }
    .edit_mode {
        float: left;
        width: 135px;
        margin-top: 10px;
    }
    .edit_mode label {
        float: left;
    }
    .edit_mode .toggle_div {
        float: right;
    }
    .move_all {
        float: left;
    }
    .clear_grid, .ser_grid {
        float: right;
        margin: 0 5px 0 5px;
    }

    #btnAllQPV{
        border:1px solid #90BE21;
        border-radius: 40px;
        padding: 0.5em 2em;
        font-size: 14pt !important;
    }
</style>

<?php
$allData = json_decode(json_encode($allData));
/*if (isset($el["oceco"]["subOrganization"]))
{*/
?>
<div class="col-md-offset-1 col-md-10 col-xs-12 aapdashboard margin-top-5">
    <button type="button" class="btn aapgoto" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.dashboard">dashboard</button>
</div>
<?php
/*}*/
?>


<div class="col-md-offset-1 col-md-10 col-xs-12 aapdashboard margin-top-5">
    <div id="ocecofiltercontainer" class="searchObjCSS menuFilters menuFilters-vertical col-xs-12 bg-light text-center"></div>
    <div class="col-md-12 col-sm-12 col-xs-12 aaptilescont">
        <div class="col-md-12 col-sm-12 col-xs-12 aaptiles padding-top-20 padding-bottom-20">
            <div class=" col-md-12 aaptilestitles2 margin-bottom-20">
                <h4>Fiches actions </h4>
            </div>
            <div class=" col-md-2 " style="">
                <div class="col-md-12 aapnumbertiles" id="actionRealisee">
                    0
                </div>
                <div class=" aaptilestitles1">Actions réalisées</div>
            </div>
            <div class=" col-md-3 " style="">

                <div class="col-md-12 aapnumbertiles" id="actionFinancee">
                    0
                </div>
                <div class=" aaptilestitles1">Financés</div>
            </div>
            <div class=" col-md-2 " style="">
                <div class="col-md-12 aapnumbertiles" id="nbQuartier">
                    0
                </div>
                <div class="aaptilestitles1">Quartiers</div>
            </div>
            <div class=" col-md-1 " style="">
                <div class="col-md-12 aapnumbertiles" id="nbQPV">
                    14
                </div>
                <div class="aaptilestitles1">QPV</div>
            </div>
            <div class=" col-md-2 " style="">

                <div class="col-md-12 aapnumbertiles" id="habitantTouchee">
                    0
                </div>
                <div class="aaptilestitles1">Habitants touchées</div>
            </div>

            <div class=" col-md-2 " style="">
                <div class="col-md-12 aapnumbertiles" id="personneImpliquee">
                    0
                </div>
                <div class="aaptilestitles1">Personnes impliqués</div>
            </div>
            <!--<span class="aaptilestitles"> <i class="fa fa-tag"></i> Action en retard </span>-->
            <!--<hr role="separator" aria-orientation="horizontal" class="v-divider theme--light">
            <div class="center">
                <button class="center btn btn-sm btn-secondary aapdashcallback" data-aapdashcallback="viewinfo" data-containerid="totalLateProject"> <i class="fa fa-info-circle"></i> details </button>
            </div>-->
        </div>
    </div>

</div>

<div class="col-md-offset-1 col-md-10 col-xs-12 aapdashboard">

    <div class="col-md-12 col-sm-12 col-xs-12 aaptilescont">
        <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
            <div class="aaptilestitles3 "> <h5> Flux du financement <!--<button class="btn btn-sm voirbtn aapevent aapdashcallback " data-aapdashcallback="viewinfo" data-containerid="totalLateProject">voir</button>--> </h5> </div>

            <div class="aapinnertiles3 col-md-12" id="financersankey2" data-aapdashcallback="viewinfo" style="padding: 50px 0 50px 0; overflow: scroll;">


            </div>


        </div>
    </div>

</div>

<div class="col-md-offset-1 col-md-10 col-xs-12 aapdashboard">

    <div class="col-md-12 col-sm-12 col-xs-12 aaptilescont">
        <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
            <span class="aaptilestitles3"> <h5> Total financé par financeur </h5> </span>

            <div class="aapinnertiles3 col-md-12" id="totparfinanceur" data-aapdashcallback="viewinfo" style="padding: 50px 0 50px 0;">

            </div>

        </div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12 aaptilescont">
        <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
            <span class="aaptilestitles3"> <h5> Total financé par quartier </h5> </span>

            <div class="aapinnertiles3 col-md-12" id="totparquartier" data-aapdashcallback="viewinfo" style="padding: 50px 0 50px 0;">

            </div>

        </div>
    </div>

</div>

<div class="col-md-offset-1 col-md-10 col-xs-12 aapdashboard">

    <div class="col-md-6 col-sm-12 col-xs-12 aaptilescont">
        <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
            <span class="aaptilestitles3"> <h5> Actions par thématique </h5> </span>

            <div class="aapinnertiles3 col-md-12" id="actionparthematique" data-aapdashcallback="viewinfo" style="padding: 50px 0 50px 0;">

            </div>

        </div>
    </div>

    <div class="col-md-6 col-sm-12 col-xs-12 aaptilescont">
        <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
            <span class="aaptilestitles3"> <h5> Financement par thématique  </h5> </span>

            <div class="aapinnertiles3 col-md-12" id="finparthematique" data-aapdashcallback="viewinfo" style="padding: 50px 0 50px 0;">

            </div>

        </div>
    </div>

</div>

<div class="col-md-offset-1 col-md-10 col-xs-12 aapdashboard">

    <div class="col-md-6 col-sm-12 col-xs-12 aaptilescont">
        <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
            <span class="aaptilestitles3"> <h5> Actions par pilier </h5> </span>

            <div class="aapinnertiles3 col-md-12" id="actionparpilier" data-aapdashcallback="viewinfo" style="padding: 50px 0 50px 0;">

            </div>

        </div>
    </div>

    <div class="col-md-6 col-sm-12 col-xs-12 aaptilescont">
        <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
            <span class="aaptilestitles3"> <h5> Financement par pilier </h5> </span>

            <div class="aapinnertiles3 col-md-12" id="finparpilier" data-aapdashcallback="viewinfo" style="padding: 50px 0 50px 0;">

            </div>

        </div>
    </div>

</div>

<script type="text/javascript">
    var gridHeight;

    var grid = $('#grid_stack_cont').data('gridstack');

    function aapgraphcallback(graphkey, index){
        if (typeof $('#'+graphkey).data("aapdashcallback") != "undefined"){
            var aapdashcallback = $('#'+graphkey).data("aapdashcallback");
            ocecoform.tiles[graphkey][aapdashcallback](index);
        }
    }

    jQuery(document).ready(function() {
        $('.aapdashcallback').click(function(){
            var aapdashcontainerid = $(this).data("containerid");
            var aapdashcallback = $(this).data("aapdashcallback");
            ocecoform.tiles[aapdashcontainerid][aapdashcallback]();
        });

        $(".aapgoto").off().on("click", function(){
            if(isUserConnected == "unlogged")
                return $("#modalLogin").modal();
            mylog.log("azeee", $(this).data("url"));
            window.location.href = $(this).data("url");
            urlCtrl.loadByHash(location.hash);
        });
    });

    var standartcolors = [
        'rgba(46, 204, 113, 1)',
        'rgba(224, 224, 0, 1)',
        'rgba(221, 221, 221, 1)'
    ];

    var standartcolors = [
        'rgba(46, 204, 113, 1)',
        'rgba(224, 224, 0, 1)',
        'rgba(221, 221, 221, 1)'
    ];

    var mybarcolor = [
        "#b30040",
        "#50bd60",
        "#7c00e4",
        "#b67994",
        "#00c5e6",
        "#006b0f",
        "#f06f29",
    ]

    var bigstandartcolors = [
        // "#F7464A",
        'rgba(46, 204, 113, 1)',
        "#46BFBD",
        "#949FB1",
        "#4D5360",
    ];

    var defaultimgprofil = '<?php echo Yii::app()->getModule('co2')->assetsUrl.'/images/thumb/default_citoyens.png'; ?>';

    var groupe = ["Feature" , "Costume" , "Chef de projet" , "Data" , "Maintenance"];

    var qpv = ["Bas de la Rivière", "Camélias","Bellepierre","Moufia","Chaudron","Sainte Clotilde","Marcadet","Vauban","Butor","La Source","Centre Ville","Domenjod","Montgaillard","Primat"]

    var smhtml = '<h1 class="text-center"></h1>' +
        '<style type="text/css">' +
        '</style>' +
        '<div class="col-xs-12 col-sm-10 col-sm-offset-1">' +
        '    <div class="no-padding col-xs-12 text-left headerSearchContainer"></div>' +
        '' +
        '    <div id="filterCMS"></div>' +
        '    <div id="appCmsCurrent">' +
        '    </div>' +
        '    <div class="no-padding col-xs-12 text-left footerSearchContainer smartgrid-slide-element"></div>' +
        '</div>';

    var fp = <?php echo json_encode($formparent) ?>;
    var parent = <?php echo json_encode($parent) ?>;

    var ocecoform = {
        allData : <?php echo json_encode($allData) ?> ,

        allDataAns : <?php echo json_encode($allDataAns) ?> ,

        project : <?php echo json_encode($project) ?>,

        actions : <?php echo json_encode($action) ?> ,

        title : "<?php echo $title; ?>",

        tiles : {

            "actionRealisee" : {
                type : "html",
                getandsetDatafunc : function(ocecoform){
                    ocecoform.tiles["actionRealisee"].values = Object.values(ocecoform.allDataAns).length;
                },
                values : 0
            },
            "actionFinancee" : {
                type : "html",
                getandsetDatafunc : function(ocecoform){
                    let  totalAmountFin = 0;
                    $.each(ocecoform.allDataAns, (index, ans) => {
                        $.each(ans, (i, line) => {
                            if(line.financer){
                                $.each(line.financer, (i, fin) => {
                                    if(fin.amount){
                                        totalAmountFin+=parseFloat(fin.amount);
                                    }
                                });
                            }
                        });
                    });
                    ocecoform.tiles["actionFinancee"].values = ocecoform.dataProcessorFunction.formatToKorM(totalAmountFin)+" €";
                },
                values : "3.353M"
            },
            "nbQuartier" : {
                type : "html",
                getandsetDatafunc : function(ocecoform){
                    let  quartiers = [];
                    $.each(ocecoform.allData, (index, ans) => {
                        if(exists(ans.answers.aapStep1["multiCheckboxPlusinterventionArea"])){
                            $.each(ans.answers.aapStep1["multiCheckboxPlusinterventionArea"], (i, quartier) => {
                                quartiers.push(...Object.keys(quartier));
                            });
                        }
                    });

                    let quertierSet = Array.from(new Set(quartiers));
                    if(ocecoform.filters.activeFilters["multiCheckboxPlusinterventionArea"].length!=0){
                        quertierSet = ocecoform.filters.activeFilters["multiCheckboxPlusinterventionArea"];
                    }

                    ocecoform.tiles["nbQuartier"].values = (quertierSet.includes("Tous quartiers") && quertierSet.length!=1)? quertierSet.length-1 : quertierSet.length;
                },
                values : 21
            },
            "habitantTouchee" : {
                type : "html",
                getandsetDatafunc : function(ocecoform){
                    ocecoform.tiles["habitantTouchee"].values = ocecoform.dataProcessorFunction.formatToKorM(114280);
                },
                values : 114280
            },
            "personneImpliquee" : {
                type : "html",
                getandsetDatafunc : function(ocecoform){
                },
                values : 45
            },
            "financersankey2" : {
                type : "chart",
                totaltasks : [],
                userindex : [],
                getandsetDatafunc : function(ocecoform){
                    var dataniv1 = ocecoform.dataProcessorFunction.reformatwithid(ocecoform.allDataAns , "idorigin" , 2 );
                    var dataniv2 = ocecoform.dataProcessorFunction.ungroup(dataniv1);
                    var links = [];
                    var nodes = [];

                    $.each(dataniv2, function (indxx , valxx) {
                        if (typeof valxx.financer != "undefined") {
                            $.each(valxx.financer, function (indxx2 , valxx2) {

                                if (valxx2.amount != "undefined" && $.isNumeric(valxx2.amount)){
                                    if (typeof valxx.multiCheckboxPlusinterventionArea != "undefined"){
                                        $.each(valxx.multiCheckboxPlusinterventionArea, function( chid , chval) {
                                            links.push({
                                                "source": (typeof valxx2.name == "undefined" || valxx2.name == "") ? parent.name : valxx2.name ,
                                                "target": Object.keys(chval)[0],
                                                "value": parseInt(valxx2.amount)
                                            });

                                            if (!nodes.some(node => node.name === Object.keys(chval)[0])){
                                                nodes.push({
                                                    "name": Object.keys(chval)[0],
                                                    "level" : 1
                                                });
                                            }

                                        });
                                    } else {
                                        links.push({
                                            "source": valxx2.name,
                                            "target": "Pas de quartiers",
                                            "value": parseInt(valxx2.amount)
                                        });
                                    }
                                }

                                if (!nodes.some(node => node.name === parent.name )) {
                                    nodes.push({
                                        "name": parent.name,
                                        "level": 0
                                    });
                                }

                                if (!nodes.some(node => node.name === "Pas de quartiers" )) {
                                    nodes.push({
                                        "name": "Pas de quartiers",
                                        "level": 1
                                    });
                                }

                                if (typeof valxx2.name != "undefined" && !nodes.some(node => node.name === valxx2.name )){
                                    nodes.push({
                                        "name": valxx2.name,
                                        "level": 0
                                    });
                                }

                            });
                        }
                    });

                    var links2 = [];

                    $.each(nodes, function (ndid , nd){
                        if (nd.level == 0){
                            $.each(nodes, function (ndid2 , nd2){
                                if (nd2.level == 1){
                                    var itr = {
                                        "source" : nd.name,
                                        "target" : nd2.name,
                                        "value" : 0
                                    };
                                    $.each(links, function (lkid , lk){
                                        if (lk.source == nd.name && lk.target == nd2.name){
                                            console.log(lk);
                                            itr.value += lk.value;
                                        }
                                    });
                                    if (itr.value > 0){
                                        links2.push(itr);
                                    }
                                }
                            });
                        }
                    });

                    ocecoform.tiles["financersankey2"].values.data = { "links" : JSON.stringify(links2), "nodes" : JSON.stringify(nodes) };

                },
                values : {
                    id : "financerd3sankey",
                    g : 'graph.views.co.ocecoform.sankey',
                    colors : mybarcolor,
                    data : {
                        "links": "",
                        "nodes": ""
                    },
                    labels : ["FINANCEURS", "PROPOSITIONS", "EXCECUTANTS"]
                },
                toogleview :function () {

                }
            },
            "totparfinanceur" : {
                type : "chart",
                /*totaltaskslate : [],
                totaltasksnotlate : [],
                totaltasksfinished : [],
                userindex : [],*/
                getandsetDatafunc : function(ocecoform){
                    let  totalPerFinancer = {};
                    let colors = [];
                    $.each(ocecoform.allDataAns, (index, ans) => {
                        $.each(ans, (i, line) => {
                            if(line.financer){
                                $.each(line.financer, (i, fin) => {
                                    if(fin.amount && fin.name){
                                        if(!exists(totalPerFinancer[fin.name])){
                                            totalPerFinancer[fin.name] = 0;
                                        }
                                        totalPerFinancer[fin.name] += parseFloat(fin.amount);
                                    }
                                });
                            }
                        });
                    });
                    ocecoform.tiles["totparfinanceur"].values.data = Object.values(totalPerFinancer);
                    ocecoform.tiles["totparfinanceur"].values.labels = Object.keys(totalPerFinancer);
                    ocecoform.tiles["totparfinanceur"].values.colors = mybarcolor;
                },
                values : {
                    id : "graphtasks",
                    data : [209000, 123000, 98300],
                    label : 'a financé au total',
                    labels : ["label 1","label 2","label 3"],
                    g : 'graph.views.co.ocecoform.bar',
                    colors : mybarcolor,
                    unity : "€"
                },
                viewinfo :function (ind) {

                }
            },

            "totparquartier" : {
                type : "chart",
                totaltaskslate : [],
                totaltasksnotlate : [],
                totaltasksfinished : [],
                userindex : [],
                getandsetDatafunc : function(ocecoform){
                    var dataniv1 = ocecoform.dataProcessorFunction.reformatwithid(ocecoform.allDataAns , "idorigin" , 2 );
                    var dataniv2 = ocecoform.dataProcessorFunction.ungroup(dataniv1);
                    var links = [];
                    var nodes = [];

                    var labels = [];
                    var label = [];
                    var fvalues = [];

                    $.each(dataniv2, function (indxx , valxx) {
                        if (typeof valxx.financer != "undefined") {
                            $.each(valxx.financer, function (indxx2 , valxx2) {

                                if (valxx2.amount != "undefined" && $.isNumeric(valxx2.amount)){
                                    if (typeof valxx.multiCheckboxPlusinterventionArea != "undefined"){
                                        $.each(valxx.multiCheckboxPlusinterventionArea, function( chid , chval) {
                                            links.push({
                                                "source": (typeof valxx2.name == "undefined" || valxx2.name == "") ? parent.name : valxx2.name ,
                                                "target": Object.keys(chval)[0],
                                                "value": parseInt(valxx2.amount)
                                            });

                                            if (!nodes.some(node => node.name === Object.keys(chval)[0])){
                                                nodes.push({
                                                    "name": Object.keys(chval)[0],
                                                    "level" : 1
                                                });
                                            }

                                        });
                                    } else {
                                        links.push({
                                            "source": valxx2.name,
                                            "target": "Pas de quartiers",
                                            "value": parseInt(valxx2.amount)
                                        });
                                    }
                                }

                                if (!nodes.some(node => node.name === parent.name )) {
                                    nodes.push({
                                        "name": parent.name,
                                        "level": 0
                                    });
                                }

                                if (!nodes.some(node => node.name === "Pas de quartiers" )) {
                                    nodes.push({
                                        "name": "Pas de quartiers",
                                        "level": 1
                                    });
                                }

                                if (typeof valxx2.name != "undefined" && !nodes.some(node => node.name === valxx2.name )){
                                    nodes.push({
                                        "name": valxx2.name,
                                        "level": 0
                                    });
                                }

                            });
                        }
                    });

                    var links2 = [];

                    $.each(nodes, function (ndid , nd){
                        if (nd.level == 0){
                            $.each(nodes, function (ndid2 , nd2){
                                if (nd2.level == 1){
                                    var itr = {
                                        "source" : nd.name,
                                        "target" : nd2.name,
                                        "value" : 0
                                    };
                                    $.each(links, function (lkid , lk){
                                        if (lk.source == nd.name && lk.target == nd2.name){
                                            console.log(lk);
                                            itr.value += lk.value;
                                        }
                                    });
                                    if (itr.value > 0){
                                        links2.push(itr);
                                    }
                                }
                            });
                        }
                    });

                    $.each(links2, function (lk2id , lk2){
                        if(labels.indexOf(lk2.source) == -1)
                        {
                            labels.push(lk2.source);
                            fvalues[labels.indexOf(lk2.source)] = [parseInt(lk2.value)]
                            if(label.indexOf(lk2.target) == -1){
                                label.push(lk2.target);
                            }
                        }else{
                            if(label.indexOf(lk2.target) == -1){
                                label.push(lk2.target);
                            }
                            fvalues[labels.indexOf(lk2.source)].push(parseInt(lk2.value));
                        }
                    });

                    $.each(fvalues, function (fvid , fv){
                        $.each(label, function (llid , ll){
                            if (typeof fvalues[fvid][llid] == "undefined"){
                                fvalues[fvid][llid] = 0;
                            }
                        });
                    });

                    finalarray = [];

                    $.each(fvalues, function (fid , fdata){
                        finalarray.push(
                            {
                                label : labels[fid],
                                data: fdata,
                                backgroundColor: mybarcolor[fid],
                                stack : "stack 0"
                            }
                        );
                    });

                    ocecoform.tiles["totparquartier"].values.data = finalarray;
                    ocecoform.tiles["totparquartier"].values.labels = label;

                },
                values : {
                    id : "totparquartier",
                    data : [
                        {
                            label: 'Dataset 1',
                            data: [451, 45, 45],
                            backgroundColor: "#ef5154",
                            stack: 'Stack 0',
                        },
                        {
                            label: 'Dataset 2',
                            data: [451, 45, 45],
                            backgroundColor: "#4268f9",
                            stack: 'Stack 0',
                        },
                        {
                            label: 'Dataset 3',
                            data: [451, 45, 45],
                            backgroundColor: "#fabb44",
                            stack: 'Stack 1',
                        },
                    ],
                    label : 'Actions par thématique',
                    labels : ["","",""],
                    g : 'graph.views.co.ocecoform.barmulti',
                    colors : mybarcolor
                },
                viewinfo :function (ind) {

                }
            },

            "actionparthematique" : {
                type : "chart",
                dataLabel : {},
                getandsetDatafunc : function(ocecoform){
                    var dataLabel = {};
                    $.each(ocecoform.allData,function(k,v){
                        if(exists(v.answers.aapStep1.tags)){
                            $.each(v.answers.aapStep1.tags,function(key,value){
                                if(!exists(dataLabel[value]))
                                    dataLabel[value] = 1
                                else if(exists(dataLabel[value]))
                                    dataLabel[value] ++ ;
                            })
                        }
                    })
                    ocecoform.tiles.actionparthematique.dataLabel = dataLabel;
                    ocecoform.tiles.actionparthematique.values.labels = Object.keys(ocecoform.tiles.actionparthematique.dataLabel);
                    ocecoform.tiles.actionparthematique.values.data = Object.values(ocecoform.tiles.actionparthematique.dataLabel);
                    ocecoform.tiles.actionparthematique.values.colors = ocecoform.dataProcessorFunction.generateColorsByLabels(Object.keys(ocecoform.tiles.actionparthematique.dataLabel));
                    mylog.log("datalabel actionparthematique",dataLabel);
                },
                values : {
                    id : "actionparthematique",
                    labels : [],
                    data : [],
                    label : 'Actions par thématique',
                    g : 'graph.views.co.ocecoform.pie',
                    colors : bigstandartcolors
                },
                viewinfo :function (ind) {

                }
            },

            "finparthematique" : {
                type : "chart",
                totaltaskslate : [],
                totaltasksnotlate : [],
                totaltasksfinished : [],
                userindex : [],
                getandsetDatafunc : function(ocecoform){
                    var dataniv1 = ocecoform.dataProcessorFunction.reformatwithid(ocecoform.allDataAns , "idorigin" , 2 );
                    var dataniv2 = ocecoform.dataProcessorFunction.ungroup(dataniv1);
                    var labels = [];
                    var values = [];

                    $.each(dataniv2, function (indxx , valxx) {
                        if (typeof valxx.financer != "undefined") {
                            $.each(valxx.financer, function (indxx2 , valxx2) {

                                if (valxx2.amount != "undefined" && $.isNumeric(valxx2.amount)){
                                    if (typeof valxx.tags != "undefined"){
                                        $.each(valxx.tags, function( tid , tval) {
                                            if(labels.indexOf(tval) !== -1)
                                            {
                                                values[labels.indexOf(tval)] += parseInt(valxx2.amount);
                                            }else{
                                                labels.push(tval);
                                                values.push(parseInt(valxx2.amount));
                                            }

                                        });
                                    }
                                }

                            });
                        }
                    });

                    ocecoform.tiles["finparthematique"].values.data = values ;
                    ocecoform.tiles["finparthematique"].values.labels = labels ;
                    ocecoform.tiles["finparthematique"].values.colors = ocecoform.dataProcessorFunction.generateColorsByLabels(labels);

                },
                values : {
                    id : "finparthematique",
                    data : [254, 114, 150],
                    label : 'Actions par thématique',
                    labels : ["aaaa","bbbb","cccc"],
                    g : 'graph.views.co.ocecoform.pie',
                    colors : bigstandartcolors,
                    unity : "€"
                },
                viewinfo :function (ind) {

                }
            },

            "actionparpilier" : {
                type : "chart",
                dataLabel : {},
                getandsetDatafunc : function(ocecoform){
                    var dataLabel = {};
                    $.each(ocecoform.allData,function(k,v){
                        if(exists(v.answers.aapStep1.multiRadioaxesTFPB) && exists(v.answers.aapStep1.multiRadioaxesTFPB.value)){
                            var value = v.answers.aapStep1.multiRadioaxesTFPB.value;
                            if(!exists(dataLabel[value]))
                                dataLabel[value] = 1
                            else if(exists(dataLabel[value]))
                                dataLabel[value] ++ ;
                        }
                    })
                    ocecoform.tiles.actionparpilier.dataLabel = dataLabel;
                    ocecoform.tiles.actionparpilier.values.labels = Object.keys(ocecoform.tiles.actionparpilier.dataLabel);
                    ocecoform.tiles.actionparpilier.values.data = Object.values(ocecoform.tiles.actionparpilier.dataLabel);
                    ocecoform.tiles.actionparpilier.values.colors = ocecoform.dataProcessorFunction.generateColorsByLabels(Object.keys(ocecoform.tiles.actionparpilier.dataLabel));
                    mylog.log("datalabel actionparpilier",dataLabel);
                },
                values : {
                    id : "actionparpilier",
                    data : [254, 114, 150],
                    label : 'Actions par pilier',
                    labels : ["aaaa","bbbb","cccc"],
                    g : 'graph.views.co.ocecoform.pie',
                    colors : bigstandartcolors
                },
                viewinfo :function (ind) {

                }
            },

            "finparpilier" : {
                type : "chart",
                totaltaskslate : [],
                totaltasksnotlate : [],
                totaltasksfinished : [],
                userindex : [],
                getandsetDatafunc : function(ocecoform){
                    var dataniv1 = ocecoform.dataProcessorFunction.reformatwithid(ocecoform.allDataAns , "idorigin" , 2 );
                    var dataniv2 = ocecoform.dataProcessorFunction.ungroup(dataniv1);
                    var labels = [];
                    var values = [];

                    $.each(dataniv2, function (indxx , valxx) {
                        if (typeof valxx.financer != "undefined") {
                            $.each(valxx.financer, function (indxx2 , valxx2) {

                                if (valxx2.amount != "undefined" && $.isNumeric(valxx2.amount)){
                                    if (typeof valxx.multiRadioaxesTFPB != "undefined"){
                                        //$.each(valxx.multiRadioaxesTFPB.value, function( tid , tval) {
                                        if(labels.indexOf(valxx.multiRadioaxesTFPB.value) !== -1)
                                        {
                                            values[labels.indexOf(valxx.multiRadioaxesTFPB.value)] += parseInt(valxx2.amount);
                                        }else{
                                            labels.push(valxx.multiRadioaxesTFPB.value);
                                            values.push(parseInt(valxx2.amount));
                                        }

                                        //});
                                    }
                                }

                            });
                        }
                    });

                    ocecoform.tiles["finparpilier"].values.data = values ;
                    ocecoform.tiles["finparpilier"].values.labels = labels ;
                    ocecoform.tiles["finparpilier"].values.colors = ocecoform.dataProcessorFunction.generateColorsByLabels(labels);
                },
                values : {
                    id : "finparpilier",
                    data : [254, 114, 150],
                    label : 'Actions par thématique',
                    labels : ["aaaa","bbbb","cccc"],
                    g : 'graph.views.co.ocecoform.pie',
                    colors : bigstandartcolors,
                    unity : "€"
                },
                viewinfo :function (ind) {

                }
            },

            "actionparobj" : {
                type : "chart",
                totaltaskslate : [],
                totaltasksnotlate : [],
                totaltasksfinished : [],
                userindex : [],
                getandsetDatafunc : function(ocecoform){

                },
                values : {
                    id : "actionparobj",
                    data : [254, 114, 150],
                    label : 'Actions par thématique',
                    labels : ["aaaa","bbbb","cccc"],
                    g : 'graph.views.co.ocecoform.pie',
                    colors : bigstandartcolors
                },
                viewinfo :function (ind) {

                }
            },

            "finparobjectif" : {
                type : "chart",
                totaltaskslate : [],
                totaltasksnotlate : [],
                totaltasksfinished : [],
                userindex : [],
                getandsetDatafunc : function(ocecoform){
                    var dataniv1 = ocecoform.dataProcessorFunction.reformatwithid(ocecoform.allDataAns , "idorigin" , 2 );
                    var dataniv2 = ocecoform.dataProcessorFunction.ungroup(dataniv1);
                    var labels = [];
                    var values = [];

                    $.each(dataniv2, function (indxx , valxx) {
                        if (typeof valxx.financer != "undefined") {
                            $.each(valxx.financer, function (indxx2 , valxx2) {

                                if (valxx2.amount != "undefined" && $.isNumeric(valxx2.amount)){
                                    if (typeof valxx.mainObjectives != "undefined"){
                                        $.each(valxx.mainObjectives, function( tid , tval) {
                                            if(labels.indexOf(tval) !== -1)
                                            {
                                                values[labels.indexOf(tval)] += parseInt(valxx2.amount);
                                            }else{
                                                labels.push(tval);
                                                values.push(parseInt(valxx2.amount));
                                            }

                                        });
                                    }
                                }

                            });
                        }
                    });

                    ocecoform.tiles["finparobjectif"].values.data = values ;
                    ocecoform.tiles["finparobjectif"].values.labels = labels ;
                },
                values : {
                    id : "finparobjectif",
                    data : [254, 114, 150],
                    label : 'Actions par thématique',
                    labels : ["aaaa","bbbb","cccc"],
                    g : 'graph.views.co.ocecoform.pie',
                    colors : bigstandartcolors,
                    unity : "€"
                },
                viewinfo :function (ind) {

                }
            }

        },

        init : function(pInit = null){
            var copyFilters = jQuery.extend(true, {}, formObj);
            copyFilters.initVar(pInit);
            return copyFilters;
        },

        initvalues : function(ocecoform){
            $.each( ocecoform.tiles , function( dataId, dataValue ) {
                ocecoform.tiles[dataId].getandsetDatafunc(ocecoform);
            });
        },

        initviews : function(ocecoform){
            $.each( ocecoform.tiles , function( tilesId, tilesValue ) {
                if(tilesValue.type == "html"){
                    $("#"+tilesId).html(ocecoform.tiles[tilesId].values);
                    $("#"+tilesId).data("value", 0);

                    $("#"+tilesId).prop('Counter', 0).animate(
                        {
                            Counter: $(this).data("value")
                        },
                        {
                            duration: 3000,
                            easing: 'swing',
                            step: function(now) {
                                $(this).text(Math.ceil(this.Counter));
                            }
                        }
                    )

                } else if (tilesValue.type == "chart") {
                    if (typeof tilesValue.callback != "undefined") {
                        var cb = tilesValue.callback;
                    } else {
                        var cb = function(){};
                    }
                    ajaxPost("#"+tilesId, baseUrl+'/graph/co/chart/', ocecoform.tiles[tilesId].values, cb ,"html");
                } else if (tilesValue.type == "list") {
                    $.each(ocecoform.tiles[tilesId].values, function(tId, tVal){
                        $("#"+tilesId).append(ocecoform.tiles[tilesId].template(tVal));
                    })
                }
            });
        },
        filters : {
            initialData: [],
            initialDataAns: [],
            activeFilters:{"multiCheckboxPlusinterventionArea":[], "context" : [] , "year" : ["2022"]},
            init: function(ocecoform){

                if(ocecoform.filters.initialData.length==0){
                    ocecoform.filters.initialData = ocecoform.allData;
                    ocecoform.filters.initialDataAns = ocecoform.allDataAns;
                }
                let  quartiers = [];
                $.each(ocecoform.filters.initialData, (index, data) => {
                    if(exists(data.answers.aapStep1["multiCheckboxPlusinterventionArea"])){
                        $.each(data.answers.aapStep1["multiCheckboxPlusinterventionArea"], (i, quartier) => {
                            quartiers.push(...Object.keys(quartier));
                        });
                    }
                });

                let quartierColumn = {};
                $.each(Array.from(new Set(quartiers)).sort(), function(i, quartier) {
                    let isQPV = "";
                    if(!quartierColumn["q-"+i%4]){
                        quartierColumn["q-"+i%4]=[];
                    }
                    quartierColumn["q-"+i%4].push(quartier+isQPV);
                });

                var paramsFilter= {
                    container : "#ocecofiltercontainer",
                    defaults : {
                        types : ["answers"],
                        indexStep:1
                    },
                    filters : {
                        theme : {
                            view : "megaMenuDropdown",
                            type : "filters",
                            remove0: true,
                            field:"multiCheckboxPlusinterventionArea",
                            countResults: true,
                            name : "Fitre par quartier",
                            event : "filters",
                            keyValue: true,
                            list : quartierColumn
                        },
                        year : {
                            view : "dropdownList",
                            type : "filters",
                            field : "year",
                            name : trad.year,
                            event : "filters",
                            list : ["2020","2021","2022"]
                        }
                    }
                }

                ocecoFilter = searchObj;
                //ocecoFilter.search.autocomplete = function(fObj){}
                ocecoFilter.init(paramsFilter);

                $(".dropdown-title").remove();
                $(".badge-theme-count").remove();

                $(".btn-filters-select").each(function(){
                    if(qpv.includes($(this).data("value"))){
                        $(this).text( $(this).text()+" (QPV)")
                    }
                });

                $(".container-filters-menu").append("<button id='btnAllQPV' data-active='false' class='btn btn-lg btn-theme rounded-pill padding-right-20 padding-left-20 bg-white'> Activer Tout les QPV </button>")

                $(document).on("click", ".theme[data-type='filters']", function(e){
                    ocecoform.filters.byQuartier(ocecoform, $(this), false);
                    ocecoform.initvalues(ocecoform);
                    ocecoform.initviews(ocecoform);
                });

                $(document).on("click", ".year[data-type='filters']", function(e){
                    ocecoform.filters.byYear(ocecoform, $(this), false);
                    ocecoform.initvalues(ocecoform);
                    ocecoform.initviews(ocecoform);
                });

                $(document).on("click",".filters-activate[data-filterk='theme']", function(){
                    ocecoform.filters.byQuartier(ocecoform, $(this), true);
                    ocecoform.initvalues(ocecoform);
                    ocecoform.initviews(ocecoform);
                });

                $(document).on("click",".filters-activate[data-filterk='year']", function(){
                    ocecoform.filters.byYear(ocecoform, $(this), true);
                    ocecoform.initvalues(ocecoform);
                    ocecoform.initviews(ocecoform);
                });

                $(document).on("click", "#btnAllQPV", function(){
                    let selector = ".btn-filters-select";
                    if($(this).data("active")=="true"){
                        selector = ".filters-activate";
                        $(this).data("active", "false");
                        $(this).text(" Activer Tout les QPV ");
                    }else{
                        $(this).data("active", "true");
                        $(this).text(" Désactiver Tout les QPV ");

                    }

                    $(selector).each(function(){
                        if(qpv.includes($(this).data("value"))){
                            $(this).click();
                        }
                    });
                });
            },

            byQuartier:function(ocecoform, element, removeActive=false){

                if(removeActive){
                    ocecoform.filters.activeFilters[ element.data("field") ] = ocecoform.filters.activeFilters[ element.data("field") ].filter(function(value, index, arr){
                        return value!=element.data("value");
                    });
                }else{
                    if(!ocecoform.filters.activeFilters[ element.data("field") ].includes(element.data("value"))){
                        ocecoform.filters.activeFilters[ element.data("field") ].push( element.data("value") );
                    }
                }

                if(ocecoform.filters.activeFilters[ element.data("field") ].length==0){
                    ocecoform.allData = ocecoform.filters.initialData;
                    ocecoform.allDataAns = ocecoform.filters.initialDataAns;
                }else{
                    let entriesAllData = Object.entries(ocecoform.filters.initialData).filter(([key, data])=>{
                        let value = false;
                        if(exists(data.answers.aapStep1[element.data("field")])){
                            $.each(data.answers.aapStep1[element.data("field")], (i, quartier) => {
                                $.each(ocecoform.filters.activeFilters[ element.data("field") ], (k, v) => {
                                    const tempV =exists(quartier[v]) || value;
                                    value = tempV;
                                });
                            });
                        }
                        return value;
                    });

                    let entriesAllDataAns = Object.entries(ocecoform.filters.initialDataAns).filter(([key, data])=>{
                        let value = false;
                        if(data[0] && exists(data[0][element.data("field")])){
                            $.each(data[0][element.data("field")], (i, quartier) => {
                                $.each(ocecoform.filters.activeFilters[ element.data("field") ], (k, v) => {
                                    const tempV =exists(quartier[v]) || value;
                                    value = tempV;
                                });
                            });
                        }
                        return value;
                    });

                    ocecoform.allData = Object.fromEntries(entriesAllData);
                    ocecoform.allDataAns = Object.fromEntries(entriesAllDataAns);
                }
            }

            byYear:function(ocecoform, element, removeActive=false){
                if(removeActive){
                    ocecoform.filters.activeFilters[element.data("field")] = ocecoform.filters.activeFilters[ element.data("field") ].filter(function(value, index, arr){
                        return value!=element.data("value");
                    });
                }else{
                    if(!ocecoform.filters.activeFilters[ element.data("field") ].includes(element.data("value"))){
                        ocecoform.filters.activeFilters[ element.data("field") ].push( element.data("value") );
                    }
                }
                if(ocecoform.filters.activeFilters[ element.data("field") ].length==0){
                    ocecoform.allData = ocecoform.filters.initialData;
                    ocecoform.allDataAns = ocecoform.filters.initialDataAns;
                }else{
                    let entriesAllData = Object.entries(ocecoform.filters.initialData).filter(([key, data])=>{
                        let value = false;
                        if(exists(data.answers.aapStep1["year"])){
                            value = ocecoform.filters.activeFilters[ element.data("field") ] == data.answers.aapStep1["year"];

                        }
                        return value;
                    });

                    let entriesAllDataAns = Object.entries(ocecoform.filters.initialDataAns).filter(([key, data])=>{
                        let value = false;
                        if(data[0] && exists(data[0]["year"])){
                            value = ocecoform.filters.activeFilters[ element.data("field") ] == data[0]["year"];
                        }
                        return value;
                    });

                    ocecoform.allData = Object.fromEntries(entriesAllData);
                    ocecoform.allDataAns = Object.fromEntries(entriesAllDataAns);
                }
            }
        },

        updateviewData : function(arData, chart){
            if (typeof tiles[chart] !== "undefined" && arData[chart]){
                if (typeof tiles[chart]["type"] == "html" ) {
                    $('#'.chart).html(arData[chart]);
                }
            }
        },

        dataProcessorFunction : {
            mergeArray : function(arr, type, path){
                if(notNull(arr)) {
                    var r = arr.reduce(function (accumulator, item) {
                        if (type == "root" && typeof item != "undefined" && item != null) {
                            accumulator = accumulator.concat(item);
                            return accumulator;
                        } else if (type == "path" && typeof item != "undefined" && typeof item[path] != "undefined") {
                            accumulator = accumulator.concat(item[path]);
                            return accumulator;
                            // }
                            // else if(type == "path" && typeof item != "undefined" && typeof item[path] != "undefined") {

                        } else {
                            return accumulator;
                        }
                    }, []);
                    return r;
                }else{
                    return [];
                }
            },

            reformatwithid : function(obj , keyname="idorigin", level = 1 , idorn = null){
                if (level == 1){
                    var temp = {};
                    $.each(obj , function(index , value){
                        if (notNull(idorn)){
                            temp[index] = value;
                            temp[index][keyname] = idorn;
                        } else {
                            temp[index] = value;
                            temp[index][keyname] = index;
                        }
                    });
                    return Object.values(temp);
                } else {
                    //if (Array.isArray(obj)){
                    var temp2 = {};
                    $.each(obj , function(index2 , value2){

                        temp2[index2] = ocecoform.dataProcessorFunction.reformatwithid(value2, "idorigin", level - 1, index2);

                    });
                    return Object.values(temp2);

                    //}
                }

            },

            sankeytizer : function(arr, path){
                return arr;
            },

            getpropo : function(id){
                if (typeof ocecoform.allData[id] != "undefined" &&
                    typeof ocecoform.allData[id]["answers"] != "undefined" &&
                    typeof ocecoform.allData[id]["answers"]["aapStep1"] != "undefined" &&
                    typeof ocecoform.allData[id]["answers"]["aapStep1"]["titre"] != "undefined"
                ){
                    return ocecoform.allData[id]["answers"]["aapStep1"]["titre"];
                }else {
                    return "";
                }
            },

            selectOccur : function(arr, key, path, limit = null){
                if(notNull(arr)) {

                    if (key != "null/false" && key != "outdate" ) {
                        if (typeof arr != "undefined") {
                            var r = arr.reduce(function (accumulator, item) {
                                if (typeof item != "undefined" && item[path] == key) {
                                    accumulator = accumulator.concat(item);
                                }
                                return accumulator;
                            }, []);
                        } else {
                            return [];
                        }
                        return r;
                    } else if (key == "null/false"){
                        var r = arr.reduce(function (accumulator, item) {
                            if (typeof item[path] == "undefined" || item[path] == null || item[path] == 'false' || item[path] == false) {
                                accumulator = accumulator.concat(item);
                            }
                            return accumulator;
                        }, []);
                        return r;
                    } else if (key == "outdate"){
                        if (typeof arr != "undefined") {
                            var r = arr.reduce(function (accumulator, item) {
                                if (typeof item != "undefined" && (typeof item[path] != "undefined")) {
                                    if (!ocecoform.dataProcessorFunction.verifydatevalue([item[path]])){
                                        item[path] = ocecoform.dataProcessorFunction.convertDate(item[path]);
                                    }
                                    if (ocecoform.dataProcessorFunction.countDays(item[path], new Date) < 0) {
                                        accumulator = accumulator.concat(item);
                                    }
                                }
                                return accumulator;
                            }, []);
                        } else {
                            return [];
                        }
                        return r;
                    }

                }else{
                    return [];
                }
            },

            countOccur : function(arr, key){
                if(notNull(arr)) {

                    if (typeof arr != "undefined") {
                        var r = arr.reduce(function (accumulator, item) {
                            if (typeof item != "undefined" && item == key) {
                                accumulator++;
                            }
                            return accumulator;
                        }, 0);
                    } else {
                        return 0;
                    }
                    return r;

                }else{
                    return 0;
                }
            },
            convertDate : function(dateString){
                if(notNull(dateString) && typeof dateString == "string"){
                    var dateParts = dateString.split("/");

                    var dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
                    return dateObject;
                }else {
                    return null;
                }
            },
            countDays : function(startDate, endDate){
                if (ocecoform.dataProcessorFunction.verifydatevalue([startDate, endDate])) {
                    var difference_In_Time = startDate.getTime() - endDate.getTime();
                    var difference_In_Days = difference_In_Time / (1000 * 3600 * 24);
                    return Math.round(difference_In_Days);
                }else{
                    return null;
                }
            },
            countToday : function(arr, path){
                if(notNull(arr)) {
                    arr = arr.filter(function (va) {
                        return (new Date(va[path]).getDay() == new Date().getDay()) && (new Date(va[path]).getMonth() == new Date().getMonth()) && (new Date(va[path]).getYear() == new Date().getYear());
                    });
                    return arr.length;
                } else {
                    return 0;
                }
            },
            countWeek : function(arr, path){
                if (notNull(arr)) {
                    arr = arr.filter(function (va) {
                        return ocecoform.dataProcessorFunction.getWeekStart(new Date(va[path])) == ocecoform.dataProcessorFunction.getWeekStart(new Date());
                    });
                    return arr.length;
                }else{
                    return 0;
                }
            },
            verifydatevalue : function (date) {
                var clean = true;
                $.each(date , function (index, value){
                    if (typeof value == "undefined" || value == null || !value.getTime){
                        clean = false;
                    }
                });
                return clean;
            },

            sortbyDate : function(arr, path, isISO, direction = "asc"){
                if (notNull(arr)) {
                    var operators = {
                        'asc': function (a, b) {
                            return a - b
                        },
                        'desc': function (a, b) {
                            return b - a
                        }
                    };

                    if (path != "root") {
                        arr = arr.filter(function (va) {
                            return typeof va[path] !== 'undefined';
                        });
                        if (isISO) {
                            return arr.sort(function (a, b) {
                                return operators[direction](new Date(b[path]), new Date(a[path]));
                            });
                        } else {
                            return arr.sort(function (a, b) {
                                return operators[direction](ocecoform.dataProcessorFunction.convertDate(b[path]), ocecoform.dataProcessorFunction.convertDate(a[path]));
                            });
                        }
                    } else {
                        if (isISO) {
                            return arr.sort(function (a, b) {
                                return operators[direction](new Date(b), new Date(a));
                            });
                        } else {
                            return arr.sort(function (a, b) {
                                return operators[direction](ocecoform.dataProcessorFunction.convertDate(b), ocecoform.dataProcessorFunction.convertDate(a));
                            });
                        }
                    }
                } else {
                    return [];
                }
            },

            getAttr : function(arr, path){
                if (notNull(arr)) {
                    var r = arr.reduce(function (accumulator, item) {
                        if (typeof item != "undefined" && typeof item[path] != "undefined") {
                            accumulator = accumulator.concat([item]);
                        }
                        return accumulator;
                    }, []);
                    return r;
                }else{
                    return [];
                }
            },

            getWeekStart : function (date) {
                var offset = new Date(date).getDay();
                return new Date(new Date(date) - offset * 24 * 60 * 60 * 1000)
                    .toISOString()
                    .slice(0, 10);
            },

            groupWeeks : function (dates, path, countpath, type, countreverse = 0, isISO) {

                const groupsByWeekNumber = dates.reduce(function(acc, item) {
                    var jsdate = item[path];
                    if (!isISO) {
                        jsdate = ocecoform.dataProcessorFunction.convertDate(item[path])
                    }
                    const today = new Date(jsdate);
                    const weekNumber = today.getWeek();

                    // check if the week number exists
                    if (typeof acc[weekNumber] === 'undefined') {
                        acc[weekNumber] = [];
                    }

                    acc[weekNumber].push(item);

                    return acc;
                }, []);

                if (type == "countpath" || type == "") {


                    return groupsByWeekNumber.map(function(group) {
                        return {
                            weekStart: ocecoform.dataProcessorFunction.getWeekStart(ocecoform.dataProcessorFunction.convertDate(group[0][path])),
                            count: group.reduce(function(acc, item) {
                                return acc + parseInt(item[countpath]);
                            }, 0)
                        };
                    });

                } else if (type == "count"){

                    if (!isISO) {
                        return groupsByWeekNumber.map(function(group) {
                            return {
                                weekStart: ocecoform.dataProcessorFunction.getWeekStart(ocecoform.dataProcessorFunction.convertDate(group[0][path])),
                                count: group.reduce(function(acc, item) {
                                    return acc = acc + 1;
                                }, 0)
                            };
                        });
                    } else {
                        return groupsByWeekNumber.map(function(group) {
                            return {
                                weekStart: ocecoform.dataProcessorFunction.getWeekStart(group[0][path]),
                                count: group.reduce(function(acc, item) {
                                    return acc = acc + 1;
                                }, 0)
                            };
                        });
                    }

                } else if (type == "countreverse"){

                    if (!isISO) {
                        return groupsByWeekNumber.map(function(group) {
                            return {
                                weekStart: ocecoform.dataProcessorFunction.getWeekStart(ocecoform.dataProcessorFunction.convertDate(group[0][path])),
                                count: group.reduce(function(acc, item) {
                                    return countreverse - 1;
                                }, 0)
                            };
                        });
                    } else {
                        return groupsByWeekNumber.map(function(group) {
                            return {
                                weekStart: ocecoform.dataProcessorFunction.getWeekStart(group[0][path]),
                                count: group.reduce(function(acc, item) {
                                    return countreverse = countreverse - 1;
                                }, 0)
                            };
                        });
                    }

                }

            },

            sumInt : function(arr){
                if(notNull(arr)) {
                    var sum = 0;
                    for(i = 0 ; i < arr.length ; i++){
                        if($.isNumeric(arr[i])){
                            sum = sum + parseInt(arr[i]);
                        }
                    }
                    return sum;
                }else{
                    return [];
                }
            },

            sumCumul : function(arr){
                if (notNull(arr)) {

                    const accumulate = arr => arr.map((sum => value => sum += value)(0));

                    return accumulate(arr);
                }else {
                    return [];
                }
            },

            getUserData : function(id) {
                var returnUserData;
                ajaxPost("",
                    baseUrl+"/co2/element/get/type/citoyens/id/"+id,
                    null,
                    function(data) {

                        if(typeof data != "undefined"){
                            returnUserData = data;
                        }
                    },
                    null,
                    "json",
                    {async : false}
                );
                return returnUserData;

            },

            getOrgaData : function(id) {
                var returnUserData;
                ajaxPost("",
                    baseUrl+"/co2/element/get/type/organizations/id/"+id,
                    null,
                    function(data) {

                        if(typeof data != "undefined"){
                            returnUserData = data;
                        }
                    },
                    null,
                    "json",
                    {async : false}
                );
                return returnUserData;

            },

            group : function(arr, path){
                if (notNull(arr)) {

                    var gr = arr.reduce(function (res, obj) {
                        res[obj[path]] = {
                            count: (obj[path] in res ? res[obj[path]].count : 0) + 1
                        }
                        return res;
                    }, []);
                    return gr;
                }else {
                    return [];
                }
            },

            ungroup : function(arr){
                if (notNull(arr)) {
                    var gr = arr.reduce(function (res, obj) {
                            arrM = ocecoform.dataProcessorFunction.mergeArray(Object.values(obj), "root", "");
                            res = res.concat(arrM);
                            return res;
                        }
                        , []);
                    return gr;
                }else {
                    return [];
                }
            },

            formatToKorM: function(num) {
                if(num > 999 && num < 1000000){
                    return (num/1000).toFixed(1) + 'K';
                }else if(num > 1000000){
                    return (num/1000000).toFixed(1) + 'M';
                }else if(num < 900){
                    return num;
                }
            },

            generateColors: function(dataLength){
                let colors = [];
                for (var i = 0; i < dataLength; i++) {
                    let letters = '0123456789ABCDEF';
                    let color = '#';
                    for (var k = 0; k < 6; k++) {
                        color += letters[Math.floor(Math.random() * 16)];
                    }
                    colors[i]= color;
                }
                return colors;
            },
            generateColorsByLabels: function(labels = []){
                let colors = [];
                $.each(labels,function(klab,vlab){
                    var hash = 0;
                    for (var i = 0; i < vlab.length; i++) {
                        hash = str.charCodeAt(i) + ((hash << 5) - hash);
                    }
                    var c = (hash & 0x00FFFFFF).toString(16).toUpperCase();
                    var color=  "00000".substring(0, 6 - c.length) + c;
                    colors.push("#"+color);
                })
                return colors
            }
        },

        html : {
            action : function (value) {
                var str = '';
                str += '<div class="row actioncard">';
                str +='         <div class="col-md-4 actioninfo">';
                str +='         <span class="actionname">'+value.name+'</span>';
                /*str +='         <span>'+value.status+'</span>';*/
                str +='         </div>';

                str +='         <div class="col-md-8 actiontaskcontainer">';

                if (typeof value.tasks != "undefined"){
                    $.each(value.tasks, function (index2, value2) {

                        str +='     <div class="col-md-3 actiontask">';
                        str +='          <div class="taskname">'+value2.task+'</div>';
                        str +='          <div><i style="color: orange" class="fa fa-warning"></i> <span style="color: orange"> Fin : </span> '+value2.endDate.toLocaleDateString()+'</div>';

                        if (typeof value2.contributors != "undefined"){
                            str += '     <div>';

                            $.each(value2.contributors, function (index3, value3) {
                                var contr = ocecoform.dataProcessorFunction.getUserData(index3);
                                str += '     <div>';
                                if (typeof contr.map.name != "undefined") {

                                    var profilpc = "";
                                    if("undefined" != typeof contr.map.profilImageUrl && contr.map.profilImageUrl != ""){
                                        profilpc = "<img width='25' height='25' alt='' class='img-circle' src='"+baseUrl+contr.map.profilThumbImageUrl+"'/>";
                                    } else {
                                        profilpc ="<i class='fa fa-user'></i>";
                                    }

                                    str += '             <span>'+  profilpc + '</span>';

                                    str += '             <span>' + contr.map.name + '</span>';
                                }
                                str += '     </div>';
                            });

                            str += '      </div>';

                        };
                        str +='     </div>';
                    });
                }

                str +='         </div>';
                str +='    </div>';

                return str;
            }
        },

        compileDataHtml : function ( data , view){
            var str = "";

            if (typeof ocecoform.html[view] != "undefind" && notNull(data) ) {
                $.each(data, function (index, value) {
                    str += ocecoform.html[view](value);
                });
            }

            return str;
        },

        createTable : function (arrList, labelkey=null) {
            let table = document.createElement('table');
            table.className = "aap-styled-table";
            let headerRow = document.createElement('tr');

            if (labelkey != null){
                $.each(labelkey, function(index, value){
                    let header = document.createElement('th');
                    let textNode = document.createTextNode(value);
                    header.appendChild(textNode);
                    headerRow.appendChild(header);
                });
            }else{
                labelkey = {};

                $.each(arrList, function (index, value){
                    var klist = Object.keys(value);

                    $.each(klist, function(ind, val){
                        //console.log("eeer", val, klist);
                        if (!(val in labelkey)){
                            labelkey[val] = val;
                        }
                    });
                });

                $.each(labelkey, function(index, value){
                    let header = document.createElement('th');
                    let textNode = document.createTextNode(value);
                    header.appendChild(textNode);
                    headerRow.appendChild(header);
                });
            }

            table.appendChild(headerRow);

            $.each(arrList, function(indexarr, valuearr){
                let row = document.createElement('tr');
                if (labelkey != null) {
                    $.each(labelkey, function (indexlb, valuelb) {
                        let cell = document.createElement('td');
                        var textNode;
                        if (typeof valuearr[indexlb] != "undefined" && valuearr[indexlb] != null) {
                            if ( Object.prototype.toString.call(valuearr[indexlb]) === '[object Array]') {
                                textNode = ocecoform.createTable(valuearr[indexlb]);
                                console.log("bebebe", valuearr[indexlb]);
                            } else if (typeof valuearr[indexlb] == "string") {
                                textNode = document.createTextNode(valuearr[indexlb]);
                            } else {
                                textNode = document.createTextNode("");
                            }
                        } else {
                            textNode = document.createTextNode("");
                        }
                        cell.appendChild(textNode);
                        row.appendChild(cell);
                    });
                }
                table.appendChild(row);
            });

            return table;
        },

        getModal : function (htmlbody) {
            prioModal = bootbox.dialog({
                message: htmlbody,
                title: "",
                show: false,
                size: "large",
                buttons: {
                    cancel: {
                        label: trad.cancel,
                        className: "btn-secondary"
                    }
                }
            });
            prioModal.modal("show");
        }

    };

    Date.prototype.getWeek = function(dowOffset) {
        /*getWeek() was developed by Nick Baicoianu at MeanFreePath: http://www.epoch-calendar.com */

        dowOffset = typeof dowOffset == 'int' ? dowOffset : 0; //default dowOffset to zero
        var newYear = new Date(this.getFullYear(), 0, 1);
        var day = newYear.getDay() - dowOffset; //the day of week the year begins on
        day = day >= 0 ? day : day + 7;
        var daynum =
            Math.floor(
                (this.getTime() -
                    newYear.getTime() -
                    (this.getTimezoneOffset() - newYear.getTimezoneOffset()) * 60000) /
                86400000
            ) + 1;
        var weeknum;
        //if the year starts before the middle of a week
        if (day < 4) {
            weeknum = Math.floor((daynum + day - 1) / 7) + 1;
            if (weeknum > 52) {
                nYear = new Date(this.getFullYear() + 1, 0, 1);
                nday = nYear.getDay() - dowOffset;
                nday = nday >= 0 ? nday : nday + 7;
                /*if the next year starts before the middle of
                     the week, it is week #1 of that year*/
                weeknum = nday < 4 ? 1 : 53;
            }
        } else {
            weeknum = Math.floor((daynum + day - 1) / 7);
        }
        return weeknum;
    };

    ocecoform.initvalues(ocecoform);
    ocecoform.initviews(ocecoform);
    ocecoform.filters.init(ocecoform);

    function noduplmergeArr(...arrays) {
        let jointArray = []

        arrays.forEach(array => {
            jointArray = [...jointArray, ...array]
        })
        const uniqueArray = jointArray.filter((item,index) => jointArray.indexOf(item) === index)
        return uniqueArray
    }

</script>
