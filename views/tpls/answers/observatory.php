<?php 
$cssJS = array(
    // '/plugins/gridstack/css/gridstack.min.css',
    // '/plugins/gridstack/js/gridstack.js',
    // '/plugins/gridstack/js/gridstack.jQueryUI.min.js'

);

HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);


HtmlHelper::registerCssAndScriptsFiles(array( 
  '/css/graphbuilder.css',
  '/js/form.js'
  ), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );

echo $this->renderPartial("survey.views.tpls.answers.header",["parentFormId" => $allanswers['parentFormId'], "adminRight" => $adminRight] ,true );
?>
<style>
    .btn.active{
        background: #9FBD38;
        color:white;
    }
    .btn.btn-default, select.selectZone{
        font-weight:bold;
    }
/* 
    #calendar-history h3 {
        text-align: center;
    } */
    #calendar-history {
        display: flex;
        justify-items: center;
        flex-wrap: wrap;
        justify-content: space-around;
    }
    .month-history {
        border-radius: 5px;
        overflow: hidden;
    }
    .days-history {
      display: grid;
      grid-template-columns: repeat(7, 1fr);
      gap: 5px;
      padding: 0px 10px;
    }
    .day-history {
      text-align: center;
      color: transparent;
      border-radius: 8px;
      padding: 3px;
      border: 1px solid #ccc;
    }
    .history-content-div {
        display: flex;
        justify-content: center;
        width: 70%;
        margin: 2% auto;
        flex-wrap: wrap;
    }
    .history-line {
        border-radius: 11px;
        box-shadow: -1px 3px 13px 0px rgba(0, 0, 0, 0.1);
        width: 80%;
        margin: 3% auto;
    }
    .default-empty-text {
        margin: 20px auto;
        padding: 20px;
    }

    .timeline-history-section .event img {
    width: 100%;
    } 
    .timeline-history-section .event p {
        position: relative;
    }
    .timeline-history-section pstrong {
    font-weight: 600;
    }
    .timeline-history-section h1 {
    font-family: 'Raleway', sans-serif;
    letter-spacing: 1.5px;
    color: #393939;
    font-weight: 100;
    font-size: 2.4em;
    }
    ul.timeline-site {
    padding-bottom: 0px;
    }
    .timeline-history-section {
    height: inherit !important;
    text-align: center;
    }
    /* Timeline */
    .timeline-site {
        border-left: 2px solid #8cb9ea;
        border-bottom-right-radius: 4px;
        border-top-right-radius: 4px;
        background: rgba(255, 255, 255, 0.03);
        color: #393939;
        font-family: "Source Sans Pro", sans-serif;
        margin: 0% 0% 0% 40%;
        letter-spacing: 0.5px;
        position: relative;
        line-height: 1.4em;
        font-size: 1.03em;
        padding: 50px;
        list-style: none;
        text-align: left;
        font-weight: 100;
        max-width: 50%;
    }
    .timeline-site h1,
    .timeline-site h2,
    .timeline-site h3 {
        font-family: 'Raleway', sans-serif;
        letter-spacing: 1.5px;
        font-weight: 100;
        font-size: 1.4em;
        color: #393939;
    }
    .timeline-site .event {
        border-bottom: 1px dashed rgba(255, 255, 255, 0.1);
        padding-bottom: 25px;
        margin-bottom: 50px;
        position: relative;
        font-weight: 400;
        font-size: 20px;
        line-height: 25px;
        animation: slideInDownSmall 1s forwards;
    }
    .timeline-site .event:last-of-type {
    padding-bottom: 0;
    margin-bottom: 0;
    border: none;
    }
    .timeline-site .event:before,
    .timeline-site .event:after {
        position: absolute;
        display: block;
        top: 0;
    }
    .timeline-site .event:before {
        left: -215px;
        font-weight: bold;
        color: #393939;
        content: attr(data-date);
        text-align: right;
        font-size: 16px;
        font-family: 'Roboto', sans-serif;
        padding-bottom: 0px;
        margin-bottom: 5px;
        min-width: 60px;
    }
    .timeline-site .event:after {
        box-shadow: 0 0 0 2px #8cb9ea;
        left: -56px;
        background: #fff;
        border-radius: 50%;
        height: 11px;
        width: 11px;
        content: "";
        top: 5px;
    }
    
    @media only screen and (max-width: 725px) {
        .history-line {
            box-shadow: none;
        }	
    }

    @media only screen and (max-width: 768px) {
        .timeline-site {
            max-width: 100%;
            margin-left: 30%;
        }	
    }
    @media only screen and (max-width: 480px) {
        .timeline-site {
            max-width: 100%;
            margin-left: 0%;
        }		
        .timeline-site .event:before {
            left: 0;
            top: -40px;
        }
        .timeline-site .event:after {
            top: -33px;
        }
    }
    /* Extra small landscape devices (most landscape phones, >=480px and <= 767 and landscape) mobile-l */
    @media only screen and (min-width: 480px) and (max-width: 767px) and (orientation: landscape) {
    
        .timeline-site {
            max-width: 100%;
            margin-left: 0%;
        }		
        .timeline-site .event:before {
            left: 0;
            top: -40px;
        }
        .timeline-site .event:after {
            top: -33px;
        }
    }
    span.date-span {
        font-size: 25px;
        color: #8DBC21;
    }
    .user-browser {
        font-size: 13px;
    }
    .justify-content {
        display: flex;
    }
</style>
<div id="mainDash" class="mainDash padding-10">
    <div class="row">
        <h4 class="pull-left obs-initial" style="margin-left: 110px;"> <?php echo Yii::t("survey","Observatory")." : <span style='color:#A4C044'>".$coform['name']."</span>" ?> </h4> 
        <h4 class="pull-left obs-history" style="margin-left: 110px;" hidden> <?php echo "Historique : <span style='color:#A4C044'>".$coform['name']."</span>" ?> </h4> 
        <a class="pull-right observatory-btn" data-mode="observatory" style="margin-right: 12.5%; cursor: pointer; display: none;"> <span class="fa fa-line-chart"></span> Mode observatoire </a>
        <a class="pull-right observatory-btn" data-mode="history" style="margin-right: 12.5%; cursor: pointer;"> <span class="fa fa-history"></span> Mode historique </a>
    </div>

    <div style="margin-left: 100px;" class="padding-right-10 observatory-mode-content">
        <p>
            Cette observatoire vous permet d'avoir une visualisation statistique des réponses sur votre formulaire / sondage.
        </p>
        
        <div class="row">
            <div class="col-md-4">
                <h4>Nombre Total de Réponses : 
                    <span class="bg-white text-success text-center padding-right-10 padding-left-10" style="border-radius:40px;">
                         <?=$allanswers['count'] ?>
                    </span>
                </h4>
            </div>
            <div  class="col-md-8">
                <div class="text-center">
                    <select name="region" id="regions" class="form-control selectZone" style="display:inline !important; width:150px">
                        <?php 
                            echo '<option value="">Tout les régions</option>';
                            foreach ($zones as $key => $value) {
                                echo '<option value="'.$value["name"].'" '.(($activeZone==$value["name"])?"selected":"").'>'.$value["name"].'</option>';
                            }
                        ?>
                    </select>
                    <a href="javascript:;" class="getobservatory btn btn-default <?= ($currentWeek=="allWeeks")?"active":"" ?>" data-parentformid='<?= $allanswers['parentFormId'] ?>' data-week="allWeeks">Tout</a>
                    <a href="javascript:;" class="getobservatory btn btn-default <?= (date("W")==$currentWeek)?"active":"" ?>" data-parentformid='<?= $allanswers['parentFormId'] ?>' data-week="<?= date("W") ?>">Cette Semaine</a>
                    <a href="javascript:;" class="getobservatory btn btn-default <?= ($currentWeek!="allWeeks" && date("W")>$currentWeek)?"active":"" ?>" data-parentformid='<?= $allanswers['parentFormId'] ?>' data-week="<?= ($currentWeek!="allWeeks")?($currentWeek-1):date("W") ?>"><i class="fa fa-arrow-left"></i> <?= ($currentWeek!="allWeeks" && date("W")-$currentWeek>1)?(date("W")-$currentWeek)."ème":"" ?> Semaine Précedante</a>
                    <a href="javascript:;" class="getobservatory btn btn-default <?= ($currentWeek!="allWeeks" && date("W")<$currentWeek)?"active":"" ?>" data-parentformid='<?= $allanswers['parentFormId'] ?>' data-week="<?= ($currentWeek!="allWeeks")?($currentWeek+1):date("W") ?>"><?= ($currentWeek!="allWeeks" && $currentWeek-date("W")>1)?($currentWeek-date("W")."ème"):"" ?> Semaine Suivante <i class="fa fa-arrow-right"></i></a>
                </div>
            </div>
            <!--div class="col-md-12" style="position: -webkit-sticky; /* Safari */ position: sticky; top: 100;">
                Filtre : (active one) (active dos)
            </div-->
        </div>
    </div>
    
    <div class="container-fluid bg-white margin-top-10 observatory-mode-content" style="border-radius:5px;" >
        <br>
        <div style="margin-left: 60px;" class="padding-right-30">
            <div class="row" id="bodyDash">
                <?php foreach ($dataSets as $dsKey => $dsValue) { ?>
                    <div class="margin-bottom-10 col-md-<?=$dsValue["col"]??"6"?>">
                    <fieldset class="text-center" style="border:2px solid #ddd; border-radius: 5px">
                        <legend style="border:2px solid #ddd; border-radius:30px; width:auto; display:inline" class="margin-left-5 padding-right-10 padding-left-10"><h6><?= $dsValue["title"] ?></h6></legend>
                        <canvas id="<?= $dsKey ?>" style="width: 100%; height: <?=$dsValue["height"]??"200"?>px; margin-bottom:20px" height="<?=$dsValue["height"]??"200"?>"></canvas>
                    </fieldset>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="container-fluid bg-white margin-top-10 history-mode-content" style="display: none">
        <div class="history-content-div">
            <div id="calendar-history"></div>
        </div>
        <div class="history-line-content">
        <h4 class="text-center">Veuillez sélectionner une case.</h4>
            <div class="history-line" style="display: none;">
                <!-- <p class="default-empty-text text-center"><b>Il n'y a pas d'activité enregistrée</b></p> -->
            </div>
        </div>
    </div>

</div>

<script src="/plugins/Chart-2.8.0/Chart.min.js"></script>
<script type="text/javascript">
    cfbObj = formObj.init({});
    cfbObj.container = ".pageContent"
    cfbObj.events.graphbuilder(cfbObj);
    cfbObj.events.obs(cfbObj);
    
    var data = <?php echo json_encode($dataSets)?>;
    var effectif= <?=$effectif?>;
    var formParent = "<?= $allanswers['parentFormId'] ?>";
    var currentWeek = "<?= $currentWeek ?>";
    
    const groupByKey = function(arr, key){
        let arrayFromKeyOfObj = [];
        $.each(arr, function(k, v){
            let value = v[key];
            /*if(typeof value == "string"){
                value = value.substr(0, 20)+((value.length>20)?"...":"");
            }*/
            arrayFromKeyOfObj.push(value);
        })
        return arrayFromKeyOfObj;
    }

    const toPercent = function(values, total){
        let percentValues = [];
        if(Array.isArray(values)){
            $.each(values, function(k, v){
                percentValues.push(Math.ceil((parseInt(v)*100)/total));
            });
        }
        return percentValues;
    }

    const generateColor = function(d){
        var generatedColor = [];
        if(d){
            generatedColor = Object.keys(d).map(function(v, i){
                //let hash = 0;
                /*for (let k = 0; k < v.length; k++) {
                    hash = v.charCodeAt(k) + ((hash << 16) - hash);
                    //hash = hash & hash;
                }*/
                let per = (i)*100/(Object.keys(d).length);
                return `hsl(${i+74.50}, ${Math.round(per)}%, ${Math.round(per)}%)`;// uni-color
                //return `hsl(${Math.round(per)}, ${50}%, ${50}%)`;
            });
        }
        return generatedColor;
    }

    $("select.selectZone").on("change", function(){
        coInterface.showLoader("#mainDash");
        var requestString = "";
        if(typeof formParent != "undefined"){
            requestString+="/form/"+formParent;
        }
        if(typeof currentWeek != "undefined"){
            requestString+="/week/"+currentWeek;
        }
        /*if($(this).val()!=""){
            requestString+="/zone/"+$(this).val(); //alert($(this).val());
        }*/
        
        ajaxPost(cfbObj.container, baseUrl+'/survey/answer/observatory'+requestString, 
            {zone:$(this).val()},
            function(){
                if (typeof hashUrlPage != "undefined") {
                    history.replaceState(location.hash, "", hashUrlPage+".view.forms.dir.obs."+formParent);
                }
            },"html");
    });

    $.each( data, function(dk, dataset){
        var ctx = document.getElementById(dk).getContext('2d');

        var dataFormated = {
            labels: groupByKey(dataset["data"], 'label'),
            datasets: [
                {
                    backgroundColor: ((dataset.graphType && dataset.graphType!="line")?'#A4C044':'transparent'),//"rgba(255,99,132,0.2)",
                    data: groupByKey(dataset["data"], 'value'),
                }
            ]
        };

        let chartOptions = {
            legend:{
                display:false
            },
            scales: {}
        }

        const isPercentValue = (typeof dataset.percent !="undefined" && dataset.percent);

        // pie chart specific
        if(!["pie", "doughnut"].includes(dataset.graphType)){
            dataFormated.datasets[0] = {
                ...dataFormated.datasets[0],
                borderColor:  '#A4C044',
                borderWidth: 2,
                hoverBackgroundColor:  '#A4C044',
                hoverBorderColor:  '#4A5158',
            }
            chartOptions.scales["yAxes"] = [{
                ticks: {
                    beginAtZero: true,
                    suggestedMax: Math.max(...groupByKey(dataset["data"], 'value'))+1
                }
            }]
        }else{
            chartOptions.legend = {
                display:true,
                position:"right"
            }

            dataFormated.datasets[0] = {
                ...dataFormated.datasets[0],
                borderWidth: 1,
                hoverBackgroundColor:  generateColor(dataset.data),
                hoverBorderColor:  generateColor(dataset.data),
                backgroundColor : generateColor(dataset.data)
            }
        }

        // percentage y axis

        if(["bar"].includes(dataset.graphType) && isPercentValue){
            chartOptions.scales.yAxes[0].ticks.min=0;
            chartOptions.scales.yAxes[0].ticks.max=100;
            chartOptions.scales.yAxes[0].ticks["callback"] = function (value) {
                return value + '%'; // convert it to percentage
            }
        }

        // percentage value
        if(["bar", "pie", "doughnut"].includes(dataset.graphType)){
            let percent = 100;
            if(["pie", "doughnut"].includes(dataset.graphType)){
                percent = 360;
            }
            dataFormated.datasets[0].data=groupByKey(dataset["data"], 'value');
            if(isPercentValue){
                dataFormated.datasets[0].data=toPercent(groupByKey(dataset["data"], 'value'), effectif);
                chartOptions["tooltips"] = {
                    callbacks: {
                        label: function (ti, data) {
                            var indice = ti.index;                 
                            return  data.labels[indice] +': '+data.datasets[0].data[indice] + ' %';
                            //return ti.yLabel+" %";
                        }
                    }
                }
            }
            
        }

        window[dk] = new Chart(ctx,{
            type: dataset.graphType||"line",
            data:dataFormated,
            options: chartOptions
        });
    })

    $(function() {
        var historyLog = {
            logsData: {},
            init: function() {
                this.events.init();
                this.actions.init();
            },
            views: {
                historyListView: function(params) {
                    var date = new Date(Number(params.created));
                    var userLocale = window.navigator.language;
                    var dateFormatter = new Intl.DateTimeFormat(userLocale, { timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone, timeStyle: 'medium' });
                    var formattedTime = dateFormatter.format(date);
                    var hours = formattedTime.split(":")[0];
                    var minutes = formattedTime.split(":")[1];
                    var seconds = formattedTime.split(":")[2];
                    var period = hours >= 12 ? 'PM' : 'AM';
                    hours = hours % 12 || 12;  
                    var formattedTime = hours + 'h ' + minutes + 'min ' + seconds + 's  ' + period;
                    var str = "";
                    str += `
                        <ul class="timeline-site">
                            <li class="event wow" data-date="${formattedTime}">
                                <div class="justify-content">
                                    <i class="fa fa-user margin-right-5"></i>
                                    <p class="user-log">${params.userName}</p>
                                </div>
                                <div class="justify-content">
                                    <i class="fa fa-desktop margin-right-5"></i> 
                                    <p class="user-browser">${historyLog.actions.userAgentInfo(params.browser)}</p>
                                </div>`;
                    if (params.stepName != undefined) {
                        str += `<div class="justify-content">
                                    <i class="fa fa-file-text margin-right-5"></i> 
                                    <p class="user-browser">${params.stepName}</p>
                                </div> `;
                    }
                    if (params.inputName != undefined) {
                        str += `<div class="justify-content">
                                    <i class='fa fa-pencil margin-right-5'></i>
                                    <p class="user-browser"> dans "${params.inputName}"</p>
                                </div> `;
                    }

                    str +=  `</li>
                        </ul>
                    `;
                    return str;
                }
            },
            events: {
                init: function() {
                    historyLog.events.clickModeBtn();
                },
                clickModeBtn: function() {
                    $("a.observatory-btn").click(function() {
                        var mode = $(this).data("mode");
                        $('#mainDash').removeClass("padding-10");
                        if (mode == "observatory") {
                            $('[data-mode="observatory"], div.history-mode-content, h4.obs-history').hide();
                            $('[data-mode="history"], div.observatory-mode-content, h4.obs-initial').fadeIn();
                            $('#mainDash').addClass("padding-10");
                        } else if (mode == "history") {
                            $('[data-mode="history"], div.observatory-mode-content, h4.obs-initial').hide();
                            $('[data-mode="observatory"], div.history-mode-content, h4.obs-history').fadeIn();
                            $('#mainDash').addClass("padding-top-10");
                        }
                    });
                },
                clickHistoryCase: function(date, countUpdate=0) {
                    var text = "Il n'y a pas de modification enregistrée le " + date + ".";
                    text = (countUpdate > 0) ? "les modifications du formulaire '<?= $coform['name'] ?>' le " + date + ":" : text;
                    $(".history-line-content h4").text(text).css("color", "#A4C044");
                    if (countUpdate > 0) {
                        $(".history-line-content, .history-line").fadeIn();
                        historyLog.actions.listResults(date);
                    }
                    else 
                        $(".history-line").hide();
                }
            },
            actions: {
                init: function() {
                    historyLog.actions.generateCalendar();
                    $('[data-toggle="popover"]').popover();
                },
                userAgentInfo: function(userAgentString) {
                    var browserRegex = /(Chrome|Firefox|Safari|Edge|IE|Opera)\//i;
                    var versionRegex = /(\d+(\.\d+)*)/;
                    var browserMatch = userAgentString.match(browserRegex);
                    var browser = browserMatch ? browserMatch[1] : "Inconnu";
                    var versionMatch = userAgentString.match(versionRegex);
                    var version = versionMatch ? versionMatch[1] : "Inconnue";
                    return browser + "/ version:" + version;
                },
                listResults: function(date) {
                    const sortedRecord = {};
                    var condition = {};
                    var allRecords = {};
                    var listHtml = "";
                    condition["logContent"] = "allRecordsLog";
                    condition["sessionDate"] = date;
                    condition["formId"] = "<?= (string)$coform["_id"] ?>";
                    var data = historyLog.actions.getLog(condition);
                    var lineHistory = document.querySelector(".history-line");
                    Object.values(data).map(function(value){ allRecords = {...allRecords, ...value.records}})
                    Object.keys(allRecords).sort((a, b) => parseInt(b) - parseInt(a)).forEach(key => { sortedRecord[key] = allRecords[key];});
                    for(const[key, value] of Object.entries(sortedRecord)) { 
                        value.created = key;
                        listHtml += historyLog.views.historyListView(value);
                    }
                    lineHistory.innerHTML = listHtml;
                    listHtml = "";
                },
                manageLog: function (data={}, action="", type=null) {
                    var results = {};
                    if (typeof data == "object" && Object.keys(data).length > 0 && action != "") {
                        ajaxPost(
                            null, 
                            baseUrl + '/survey/answer/managelog/action/' + action,
                            data,
                            function (param) { 
                                if (action == "get" && param.result == true)
                                    results = param.data;
                            },
                            null,
                            type,
                            { async: false }
                        )	
                    }
                    return results;
                },
                getLog: function(where={}){
                    var results = {}
                    if (typeof where == "object" && Object.keys(where).length > 0) 
                        results = historyLog.actions.manageLog({filter: where}, "get", "json");
                    return results;
                }, 
                generateCalendar: function() {
                    const today = new Date();
                    const calendarContainer = document.getElementById("calendar-history");
                    var condition = {};
                    condition["$or"] = [];
                    condition["formId"] = "<?= (string)$coform["_id"] ?>";
                    condition["logContent"] = "headerLog";
                    condition['$or'].push({monthRef: (today.getMonth() + 1).toString().padStart(2, '0')});
                    condition['$or'].push({monthRef: ((today.getMonth() - 1 + 12) % 12 + 1).toString().padStart(2, '0')});
                    condition['$or'].push({monthRef: ((today.getMonth() - 2 + 12) % 12 + 1).toString().padStart(2, '0')});
                    historyLog.logsData = historyLog.actions.getLog(condition);
                    historyLog.actions.newLogHeaderFormat();
                    for (let i = -2; i <= 0; i++) {
                        const month = new Date(today.getFullYear(), today.getMonth() + i, 1);
                        const monthContainer = historyLog.actions.createMonthContainer(month);
                        calendarContainer.appendChild(monthContainer);
                    }
                }, 
                createMonthContainer: function(monthNameParam) {
                    const currentDate = new Date(monthNameParam.getFullYear(), monthNameParam.getMonth() + 1, 0);
                    const daysInMonth = currentDate.getDate();
                    const monthContainer = document.createElement("div");
                    monthContainer.classList.add("month-history");
                    const monthName = monthNameParam.toLocaleString('default', { month: 'long' });
                    const header = document.createElement("h6");
                    const text = document.createElement("span");
                    const icon = document.createElement("i");
                    text.textContent = " " + monthName;
                    icon.setAttribute("class", "fa fa-calendar-o");
                    header.appendChild(icon);
                    header.appendChild(text);
                    header.style.textAlign = "center";
                    monthContainer.appendChild(header);
                    const daysContainer = document.createElement("div");
                    daysContainer.classList.add("days-history");
                    monthContainer.appendChild(daysContainer);
                    for (let day = 1; day <= daysInMonth; day++) {
                        const dateReference = day.toString().padStart(2, '0') + "/" +
                                              (monthNameParam.getMonth() + 1).toString().padStart(2, '0') + "/" + 
                                              monthNameParam.getFullYear().toString().slice(-2);   
                        const dayElement = document.createElement("div");
                        historyLog.logsData[dateReference] = (historyLog.logsData[dateReference] == undefined) ? {} : historyLog.logsData[dateReference];
                        historyLog.logsData[dateReference]["color"] = (historyLog.logsData[dateReference]["color"] == undefined) ? "#f4f4f4" : historyLog.logsData[dateReference]["color"];
                        historyLog.logsData[dateReference]["count"] = (historyLog.logsData[dateReference]["count"] == undefined) ? 0 : historyLog.logsData[dateReference]["count"];
                        dayElement.classList.add("day-history");
                        dayElement.setAttribute("id", dateReference);
                        dayElement.setAttribute("data-trigger", "hover");
                        dayElement.setAttribute("data-toggle", "popover");
                        dayElement.setAttribute("data-content", dateReference + ": "+ historyLog.logsData[dateReference].count + " modification(s)");
                        dayElement.textContent = day;
                        dayElement.style.cursor = "pointer";
                        dayElement.style.backgroundColor = historyLog.logsData[dateReference].color;
                        dayElement.onclick = function(){historyLog.events.clickHistoryCase(dateReference, historyLog.logsData[dateReference].count)};
                        daysContainer.appendChild(dayElement);
                    }
                    return monthContainer;
                },
                newLogHeaderFormat: function() {
                    var newLogDataStructure = {};
                    if (Object.keys(historyLog.logsData).length > 0) {
                        for(const[key, value] of Object.entries(historyLog.logsData)) {
                            newLogDataStructure[value["sessionDate"]] = {};
                            newLogDataStructure[value["sessionDate"]]["count"] = value["countUpdate"];
                            newLogDataStructure[value["sessionDate"]]["color"] = historyLog.actions.generateGreenShadeColor(value["countUpdate"]);
                        }
                        historyLog.logsData = newLogDataStructure
                    }
                },
                generateGreenShadeColor: function(number) {
                    number = (number > 100) ? 100 : ((number < 0) ? 10 : 90 - number);
                    const limitedNumber = Math.min(Math.max(number, 0), 100);
                    const greenShade = Math.round((limitedNumber / 100) * 255);
                    const greenHex = greenShade.toString(16).padStart(2, '0');
                    const colorHex = `#${greenHex}FF${greenHex}`;
                    return colorHex;
                },
            }

        }
        historyLog.init();
    })

</script>
    
</script>
