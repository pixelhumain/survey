<?php
HtmlHelper::registerCssAndScriptsFiles(['/plugins/d3/d3-v5/d3.min.js'], Yii::app()->request->baseUrl);
HtmlHelper::registerCssAndScriptsFiles(['/plugins/d3/sankey/d3-sankey.min.js'], Yii::app()->request->baseUrl);

HtmlHelper::registerCssAndScriptsFiles(array(
    '/css/graphbuilder.css',
    '/js/form.js',
    '/js/dashboard.js',
    '/css/aap/aapGlobalDashboard.css',
    '/js/coremu.js'
), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );

$modalrender = true;
$renderUrl = @$urlR;

$exp_url = explode("/", $renderUrl);
$el_slug = null;
$el_form_id = "";

if (isset($this->costum["slug"])){
    $slug = $this->costum["slug"];
}

if (
    in_array("slug", $exp_url) &&
    isset($exp_url[array_search('slug', $exp_url) + 1]) &&
    is_string($exp_url[array_search('slug', $exp_url) + 1]) &&
    $exp_url[array_search('slug', $exp_url) + 1] != ""
){
    $el_slug = $exp_url[array_search('slug', $exp_url) + 1];
}

if (
    in_array("formid", $exp_url) &&
    isset($exp_url[array_search('formid', $exp_url) + 1]) &&
    is_string($exp_url[array_search('formid', $exp_url) + 1]) &&
    $exp_url[array_search('formid', $exp_url) + 1] != ""
){
    $el_form_id = $exp_url[array_search('formid', $exp_url) + 1];
    $elform = PHDB::findOneById(Form::COLLECTION, $el_form_id);
    if(!empty($elform)){
        $el_configform = PHDB::findOneById(Form::COLLECTION, $elform["config"]);
        $elanswers = PHDB::find(Form::ANSWER_COLLECTION,array( "form" => (string)$elform["_id"],"answers.aapStep1.titre"=>['$exists' => true]));
    }
}

if (isset($this->costum["slug"])){
    $slug = $this->costum["slug"];
}
$el = null;
if (!empty($el_slug)) {
    $el = Slug::getElementBySlug($el_slug);
}
?>

    <style>
        .dr-coremu {
            padding: 10px;
            height: 300px;
            overflow-y: auto;
            font-family: Raleway, sans-serif;
            color : #48b;
        }

        .dr-coremu ::marker {
            content: ">";
            color: #48b;
        }

        .dr-coremu li {
            padding: 5px;
            scroll-snap-align: start;
            border-bottom : 1px solid #b0a5a5
        }

        .dr-coremu li:hover {
            color: #48b;
            cursor : pointer;
        }

        .dr-coremu li:hover {
            font-weight: 700;
        }

        .dr-coremu li:hover::marker {
            font-weight: 700;
        }
        .dr-coremu li:hover::after {
            opacity: 1;
            transition-delay: 0.2s;
        }

        .dr-coremu li:first-child {
            border: 0;
        }

        .btn_cont {
            width: 550px;
            height: 60px;
            position: relative;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -1%);
            padding-top: 20px;
        }
        .btn_cont_right {
            width: 550px;
            height: 60px;
            position: relative;
            left: 20px;
            top: 50%;
            padding-top: 20px;
        }
        .edit_mode {
            float: left;
            width: 135px;
            margin-top: 10px;
        }
        .edit_mode label {
            float: left;
        }
        .edit_mode .toggle_div {
            float: right;
        }
        .move_all {
            float: left;
        }
        .clear_grid, .ser_grid {
            float: right;
            margin: 0 5px 0 5px;
        }

/*        .aaptilescont  .aapinnertiles1 {
            background-color: #a2d17f;
        }*/

        #coremudashboardcompact {
            height: 500px;
            overflow-y: scroll;
        }

        .legend {
            font-size: 14px;
        }
        rect {
            cursor: pointer;
            stroke-width: 2;
        }
        rect.disabled {
            fill: transparent !important;
        }

        .portfolio-modal.modal.vertical .modal-content,
        .portfolio-modal.modal.vertical #openModalContent,
        .portfolio-modal.modal.vertical .list-aap-container.aappageform.container,
        .portfolio-modal.modal.vertical #customHeader,
        .portfolio-modal.modal.vertical .col-xs-12.text-center.bg-white{
            background-color: #00000000 !important;
        }
        .portfolio-modal.modal.vertical{
            background-color: #000000a3 !important;
        }
        .portfolio-modal.modal.vertical #wizardcontainer{
            background-color: #fff !important;
            border-radius: 5px;
        }
        .portfolio-modal.modal .aap-footer,
        .portfolio-modal.modal.vertical .col-xs-12.text-center.bg-white hr,
        .portfolio-modal.modal.vertical #modeSwitch{
            display: none;
        }
        .portfolio-modal.vertical .close-modal .lr .rl,
        .portfolio-modal.vertical .close-modal .lr {
            background-color: #fff ;
        }

    </style>

    <?php
    $allData = json_decode(json_encode($allData));

    if ($hv_parent)
    {
        ?>

        <div class="col-md-offset-1 col-md-10 col-xs-12 margin-top-5">
            <button type="button" class="btn aapgoto" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_parent_slug; ?>.formid.<?php echo $el_parent_form_id ?>.page.multidashboard">Multidashboard</button>
        </div>
        <?php
    }
    elseif (isset($el["el"]["oceco"]["subOrganization"]) && filter_var(    $el["el"]["oceco"]["subOrganization"], FILTER_VALIDATE_BOOLEAN)  && !$hv_parent)
    {
        ?>

        <div class="col-md-offset-1 col-md-10 col-xs-12 margin-top-5">
            <button type="button" class="btn aapgoto" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.multidashboard">Multidashboard</button>
        </div>
        <?php
    }
    ?>
    <div id="ocecofiltercontainer" class="searchObjCSS menuFilters menuFilters-vertical col-xs-12 bg-light text-center"></div>
    <div class="col-md-12 col-xs-12 aapdashboard">
        <div class="col-md-3 col-sm-12 col-xs-6 aaptilescont ">
            <div class="col-md-12 col-sm-12 col-xs-12 aaptiles1">
                <span class="aaptilestitles1"> <h5> total des propositions </h5> </span>
                <div class="aapinnertiles1 col-md-12 " style="">
                    <div class="col-md-12 aapnumbertiles " id="dashnumber">

                    </div>
                    <div class="col-md-12 aapnumbertiles " id="">
                        <div class="dropdown">
                            <button id="dashprojecttotal" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" type="button" class="dropdown-toggle btn btn-sm voirbtn aapevent aapdashcallback" data-aapdashcallback="viewinfo" data-containerid="totalLateProject">voir</button>
                            <div id="dashtotalname" class="dropdown-menu dr-coremu" aria-labelledby="dropdownMenuButton">

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-6 aaptilescont">
            <div class="col-md-12 col-sm-12 col-xs-12 aaptiles1">
                <span class="aaptilestitles1"> <h5> total des montants  </h5> </span>
                <div class="aapinnertiles1 col-md-12 " style="">
                    <div class="col-md-12 aapnumbertiles " id="dashtotal">

                    </div>
                    <div class="col-md-12 aapnumbertiles " id="">
                        <div class="dropdown">
                            <button id="dashprojecttotalm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" type="button" class="dropdown-toggle btn btn-sm voirbtn aapevent aapdashcallback" data-aapdashcallback="viewinfo" data-containerid="totalLateProject">voir</button>
                            <div id="dashprojecttotalmname" class="dropdown-menu dr-coremu" aria-labelledby="dropdownMenuButton">

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-6 aaptilescont">
            <div class="col-md-12 col-sm-12 col-xs-12 aaptiles1">
                <span class="aaptilestitles1"> <h5> total consommé </h5> </span>
                <div class="aapinnertiles1 col-md-12 " style="">
                    <div class="col-md-12 aapnumbertiles " id="dashtotalValidee">

                    </div>
                    <div class="col-md-12 aapnumbertiles " id="">
                        <div class="dropdown">
                            <button id="dashprojecttotalValidee" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" type="button" class="dropdown-toggle btn btn-sm voirbtn aapevent aapdashcallback" data-aapdashcallback="viewinfo" data-containerid="totalLateProject">voir</button>
                            <div id="dashprojecttotalValideename" class="dropdown-menu dr-coremu"  aria-labelledby="dropdownMenuButton">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-6 aaptilescont">
            <div class="col-md-12 col-sm-12 col-xs-12 aaptiles1">
                <span class="aaptilestitles1"> <h5> total financé </h5> </span>
                <div class="aapinnertiles1 col-md-12 " style="">
                    <div class="col-md-12 aapnumbertiles " id="dashtotalFinancement">

                    </div>
                    <div class="col-md-12 aapnumbertiles " id="">
                        <div class="dropdown">
                            <button id="dashprojecttotalFinancement" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" type="button" class="dropdown-toggle btn btn-sm voirbtn aapevent aapdashcallback" data-aapdashcallback="viewinfo" data-containerid="totalLateProject">voir</button>
                            <div id="dashprojecttotalFinancementname" class="dropdown-menu dr-coremu"  aria-labelledby="dropdownMenuButton">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-xs-12 aapdashboard">
        <div class="col-md-3 col-sm-12 col-xs-6 aaptilescont ">
            <div class="col-md-12 col-sm-12 col-xs-12 aaptiles1">
                <span class="aaptilestitles1"> <h5> depense en attente de candidature </h5> </span>
                <div class="aapinnertiles1 col-md-12 " style="">
                    <div class="col-md-12 aapnumbertiles " id="dashtotalneedcandidat">

                    </div>
                    <div class="col-md-12 aapnumbertiles " id="">
                        <div class="dropdown">
                            <button id="dashprojecttotalneedcandidat" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" type="button" class="dropdown-toggle btn btn-sm voirbtn aapevent aapdashcallback" data-aapdashcallback="viewinfo" data-containerid="totalLateProject">voir</button>
                            <div id="dashprojecttotalneedcandidatname" class="dropdown-menu dr-coremu"  aria-labelledby="dropdownMenuButton">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-6 aaptilescont">
            <div class="col-md-12 col-sm-12 col-xs-12 aaptiles1">
                <span class="aaptilestitles1"> <h5> candidature en attente de validation  </h5> </span>
                <div class="aapinnertiles1 col-md-12 " style="">
                    <div class="col-md-12 aapnumbertiles " id="dashtotalcandidattovalid">

                    </div>
                    <div class="col-md-12 aapnumbertiles " id="">
                        <div class="dropdown">
                            <button id="dashprojecttotalcandidattovalid" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" type="button" class="dropdown-toggle btn btn-sm voirbtn aapevent aapdashcallback" data-aapdashcallback="viewinfo" data-containerid="totalLateProject">voir</button>
                            <div id="dashprojecttotalcandidattovalidname" class="dropdown-menu dr-coremu"  aria-labelledby="dropdownMenuButton">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-6 aaptilescont">
            <div class="col-md-12 col-sm-12 col-xs-12 aaptiles1">
                <span class="aaptilestitles1"> <h5> coremu en attente de validation </h5> </span>
                <div class="aapinnertiles1 col-md-12 " style="">
                    <div class="col-md-12 aapnumbertiles " id="dashtotalpricetovalid">

                    </div>
                    <div class="col-md-12 aapnumbertiles " id="">
                        <div class="dropdown">
                            <button id="dashprojecttotalpricetovalid" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" type="button" class="dropdown-toggle btn btn-sm voirbtn aapevent aapdashcallback" data-aapdashcallback="viewinfo" data-containerid="totalLateProject">voir</button>
                            <div id="dashprojecttotalpricetovalidname" class="dropdown-menu dr-coremu"  aria-labelledby="dropdownMenuButton">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-6 aaptilescont">
            <div class="col-md-12 col-sm-12 col-xs-12 aaptiles1">
                <span class="aaptilestitles1"> <h5> action restant total <br>  </h5> </span>
                <div class="aapinnertiles1 col-md-12 " style="">
                    <div class="col-md-12 aapnumbertiles " id="dashtotalansweraction">

                    </div>
                    <div class="col-md-12 aapnumbertiles " id="">
                        <div class="dropdown">
                            <button id="dashprojecttotalansweraction" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" type="button" class="dropdown-toggle btn btn-sm voirbtn aapevent aapdashcallback" data-aapdashcallback="viewinfo" data-containerid="totalLateProject">voir</button>
                            <div id="dashprojecttotalansweractionname" class="dropdown-menu dr-coremu"  aria-labelledby="dropdownMenuButton">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-xs-12 aapdashboard">
        <div class="col-md-12 col-sm-12 col-xs-12 ">
            <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
                <span class="aaptilestitles3"> <h5> Sankey</h5> </span>

                <div class="aapinnertiles3 col-md-12" id="dashfinancementSankey" data-aapdashcallback="viewinfo" style="padding: 50px 0 50px 0;">

                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-xs-12 aapdashboard">
        <div class="col-md-6 col-sm-12 col-xs-12 ">
            <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
                <span class="aaptilestitles3"> <h5> Distribution par financeur  </h5> </span>

                <div class="aapinnertiles3 col-md-12" id="dashfinancementDistrPie" data-aapdashcallback="viewinfo" style="padding: 50px 0 50px 0;">
                </div>
            </div>
        </div>

        <div class="col-md-6 col-sm-12 col-xs-12 ">
            <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
                <span class="aaptilestitles3"> <h5> Distribution par tags  </h5> </span>

                <div class="aapinnertiles3 col-md-12" id="dashfinancementTagsPie" data-aapdashcallback="viewinfo" style="padding: 50px 0 50px 0;">

                </div>
            </div>
        </div>

        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
                <span class="aaptilestitles3"> <h5> Distribution par candidat  </h5> </span>

                <div class="aapinnertiles3 col-md-12" id="dashcandidatePie" data-aapdashcallback="viewinfo" style="padding: 50px 0 50px 0;">

                 </div>
            </div>
        </div>

        <div class="col-md-6  col-sm-12 col-xs-12 ">
            <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
                <span class="aaptilestitles3"> <h5> Nombre d'heure coRémunéré </h5> </span>

                <div class="aapinnertiles3 col-md-12" id="dashcandidateHour" data-aapdashcallback="viewinfo" style="padding: 50px 0 50px 0;">
                </div>
            </div>
        </div>
    </div>

    <div class="portfolio-modal modal vertical fade" id="aapViewFormModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content padding-top-15">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class=" container ">
                <h1 class="text-center"><i class="fa fa-spinner fa-spin"></i></h1>
            </div>
        </div>
    </div>

    <script>
        var allDataAns = <?php echo json_encode($allDataAns) ?>;
        var allData = <?php echo json_encode($allData) ?>;
        var parentDashboard = <?php echo json_encode($parent) ?>;
        var fp = <?php echo json_encode($formparent) ?>;
        coInterface.showLoader("#coremudashboardcompact");

        function initFilter(){
            if(filters.initialData.length==0){
                filters.initialData = allData;
                filters.initialDataAns = allDataAns;
            }

            var paramsFilter = {
                container : "#ocecofiltercontainer",
                defaults : {
                    types : ["answers"],
                    indexStep:1
                }
            }

            let  thematiques = [];
            $.each(filters.initialData, (index, data) => {
                if(exists(data.answers.aapStep1["tags"])){
                    $.each(data.answers.aapStep1["tags"], (i, thematique) => {
                        thematiques.push(thematique);
                    });
                }
            });

            let thematiqueColumn = {};
            $.each(Array.from(new Set(thematiques)).sort(), function(i, thematique) {
                if(!thematiqueColumn["q-"+i%4]){
                    thematiqueColumn["q-"+i%4]=[];
                }
                thematiqueColumn["q-"+i%4].push(thematique);
            });

            let  years = [];
            $.each(filters.initialData, (index, data) => {
                if(exists(data.answers.aapStep1["year"])){
                    years.push(data.answers.aapStep1["year"]);
                }
            });

            let  yearColumn = {};
            $.each(Array.from(new Set(years)).sort(), function(i, year) {
                if(!thematiqueColumn["q-"+i%4]){
                    thematiqueColumn["q-"+i%4]=[];
                }
                thematiqueColumn["q-"+i%4].push(thematique);
            });


            paramsFilter["filters"] = {

            }

            ocecoFilter = searchObj;
            //ocecoFilter.search.autocomplete = function(fObj){}
            ocecoFilter.init(paramsFilter);

            $(".dropdown-title").remove();
            $(".badge-theme-count").remove();

            $(document).on("click", ".thematique[data-type='filters']", function(e){
                filters.byThematique( $(this), false);
                coremuObj.loadGlobalDashboard(coremuObj);
            });

            $(document).on("click",".filters-activate[data-filterk='thematique']", function(){
                filters.byThematique( $(this), true);
                coremuObj.loadGlobalDashboard(coremuObj);
            });

            $(document).on("click", ".year[data-type='filters']", function(e){
                filters.byYear( $(this), false);
                coremuObj.loadGlobalDashboard(coremuObj);
            });

            $(document).on("click",".filters-activate[data-filterk='year']", function(){
                filters.byYear( $(this), true);
                coremuObj.loadGlobalDashboard(coremuObj);
            });
        }

        var filters = {
            initialData: [],
            initialDataAns: [],
            activeFilters: {"titre": [], "tags": [] , "year" : []},
            byThematique:function (element, removeActive=false){

                if(removeActive){
                    filters.activeFilters[ element.data("field") ] = filters.activeFilters[ element.data("field") ].filter(function(value, index, arr){
                        return value!=element.data("value");
                    });
                }else{
                    if(!filters.activeFilters[ element.data("field") ].includes(element.data("value"))){
                        filters.activeFilters[ element.data("field") ].push( element.data("value") );
                    }
                }

                if(filters.activeFilters[ element.data("field") ].length==0){
                    allData = filters.initialData;
                    allDataAns = filters.initialDataAns;
                }else{
                    let entriesAllData = Object.entries(filters.initialData).filter(([key, data])=>{
                        let value = false;
                        if(exists(data.answers.aapStep1[element.data("field")])){
                            $.each(data.answers.aapStep1[element.data("field")], (i, thematique) => {
                                $.each(filters.activeFilters[ element.data("field") ], (k, v) => {
                                    const tempV = (thematique == v) || value;
                                    value = tempV;
                                });
                            });
                        }
                        return value;
                    });

                    let entriesAllDataAns = Object.entries(filters.initialDataAns).filter(([key, data])=>{
                        let value = false;
                        console.log("aaa", data);
                        if(data && exists(data[element.data("field")])){
                            $.each(data[element.data("field")], (i, thematique) => {
                                $.each(filters.activeFilters[ element.data("field") ], (k, v) => {
                                    const tempV = (thematique == v) || value;
                                    value = tempV;
                                });
                            });
                        }
                        return value;
                    });

                    allData = Object.fromEntries(entriesAllData);
                    allDataAns = Object.fromEntries(entriesAllDataAns);
                }
            } ,
            byYear:function (element, removeActive=false){

                if(removeActive){
                    filters.activeFilters[ element.data("field") ] = filters.activeFilters[ element.data("field") ].filter(function(value, index, arr){
                        return value!=element.data("value");
                    });
                }else{
                    if(!filters.activeFilters[ element.data("field") ].includes(element.data("value"))){
                        filters.activeFilters[ element.data("field") ].push( element.data("value") );
                    }
                }

                if(filters.activeFilters[ element.data("field") ].length==0){
                    allData = filters.initialData;
                    allDataAns = filters.initialDataAns;
                }else{
                    let entriesAllData = Object.entries(filters.initialData).filter(([key, data])=>{
                        let value = false;
                        if(exists(data.answers.aapStep1[element.data("field")])){
                            $.each(filters.activeFilters[ element.data("field") ], (k, v) => {
                                const tempV = (data.answers.aapStep1[element.data("field")] == v) || value;
                                value = tempV;
                            });
                        }
                        return value;
                    });

                    let entriesAllDataAns = Object.entries(filters.initialDataAns).filter(([key, data])=>{
                        let value = false;

                        if(data && exists(data[element.data("field")])){
                            $.each(filters.activeFilters[ element.data("field") ], (k, v) => {
                                const tempV = (data[element.data("field")] == v) || value;
                                value = tempV;
                            });
                        }
                        return value;
                    });

                    allData = Object.fromEntries(entriesAllData);
                    allDataAns = Object.fromEntries(entriesAllDataAns);
                }
            }
        }

        jQuery(document).ready(function() {
            coremuObj.loadGlobalDashboard(coremuObj);
            initFilter();

            $('.openproposalmodal').off().on('click', function (e) {
                const thisbtn = $(this);
                const answerId = thisbtn.data('answer');
                const url = document.location+".answerId."+answerId+".aapview.summary";
                history.pushState({}, "", url);
                const modalurl = baseUrl + '/co2/aap/getviewbypath/path/co2.views.aap.detailProposal';
                const post = {
                    answerId : answerId,
                    showFilter : false,
                };
                ajaxPost(null, modalurl, post,
                    function (data) {
                        smallMenu.open(data);
                        $("#openModal").css({
                            cssText : `
                        top: 0 !important;
                        display: block !important;
                        overflow: hidden !important;
                        display: block !important;
                        background-color: rgba(3, 3, 3, 0.62) !important;`
                        });
                        aapObj.common.loadProposalDetail(aapObj, answerId);
                        $("nav.nav-tabs li:first").addClass("active");
                        $(".tab-pane#proposition-summary").addClass("active");
                        $("#openModal .modal-content").addClass("aap-modal-md");
                        $("#openModal .modal-content .close-modal").addClass("aap-close-modal");
                        $('#openModalContent').removeClass("container").addClass("container-fluid");
                        $(' #openModal .propsDetailPanel [role=tab]').on('shown.bs.tab', function () {
                            $('body.index.modal-open').append(`
                            <div class="modal-backdrop my-backdrop fade in"></div>
                        `)
                            /*const view_target = $(this).attr('href').substring(13);
                            const answer = $(this).data('answer');
                            aapObj.common.loadProposalDetail(aapObj, answer, view_target);*/
                            aapObj.common.loadProposalDetail(aapObj, answerId);
                            $(".nav.nav-tabs li:first").addClass("active");
                            $(".nav.nav-tabs li:not(:first)").hide();
                            $(".tab-pane#proposition-summary").addClass("active");
                        });
                        $('#openModal').on('hidden.bs.modal', function () {
                            /*$('#openModalContent').empty();
                            const query = aapObj.common.getQuery(aapObj);
                            const new_hash = `proposalAap.context.${query.context}.formid.${query.formid}${document.location.href.indexOf('?') > -1 ? '?' + document.location.href.split('?')[1] : ''}`;
                            const title = aapObj.pageDetail["#" + aapObj.page + "Aap"].subdomainName;
                            history.pushState({}, title, `${document.location.origin}${document.location.pathname}#${new_hash}`);
                            document.title = title;
                            $('body.index.modal-open .modal-backdrop.my-backdrop.fade.in').remove();
                            aapObj.directory.reloadAapProposalItem(aapObj, answerId);*/
                            const url = document.location.href.slice(0, -50);
                            history.pushState({}, "", url);
                        })
                    }, null, null, {
                        async : false
                    }
                );
            });
        });
    </script>
