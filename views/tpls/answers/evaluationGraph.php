
<style>
    .coform-chart-container .chart-container .chart-canvas {
        display: flex;
        justify-content: center;
        flex-wrap: wrap;
    }
    .coform-chart-container {
        font-family: 'Montserrat';
    }
    .coform-chart-container .user-item .user-thumb {
        border-radius: 50%;
        margin-right: 10px;
    }
    .coform-chart-container .users-list {
        list-style: none;
    }
    .coform-chart-container .users-list .user-item span {
        font-family: 'Montserrat';
        font-weight: 100;
    }
    .coform-chart-container .modal-dialog .modal-header {
        display: flex;
        justify-content: space-between;
    }
    .coform-chart-container .modal-dialog .modal-header .close {
        right: 1vw;
        position: absolute;
    }
    .coform-chart-container .modal-dialog .modal-body .answer-date,
    .coform-chart-container .modal-dialog .modal-body .is-autoeval {
        font-size: 12px;
        color: #838383;
    }
</style>
<div class="chart-container">
    <span class="all-users cursor-pointer"   id="showUsersModal<?= $containerId ?>" data-toggle="modal" data-target="#usersModal<?= $containerId ?>">
        <i class="fa fa-users"></i>
        Toutes les participants
    </span>
    <div class="chart-canvas bs-mx-auto" style="position: relative; width: <?= $chartWidth ?>; height: <?= $chartHeight ?>;">
        <canvas id="coformChartContainer<?= $containerId ?>"></canvas>
    </div>
    <div id="customLegendContainer<?= $containerId ?>"></div>
    <!-- Modal Structure -->
    <div id="usersModal<?= $containerId ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="usersModalLabel<?= $containerId ?>">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="usersModalLabel<?= $containerId ?>">Toutes les participants</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul id="usersList<?= $containerId ?>" class="users-list">
                        <?php 
                        $usersPerPage = 10;
                        $totalUsers = count($users);
                        $totalPages = ceil($totalUsers / $usersPerPage);
                        $currentPage = 1;
                        $start = ($currentPage - 1) * $usersPerPage;
                        $end = min($start + $usersPerPage, $totalUsers);
                        $currentUsers = array_slice($users, $start, $usersPerPage, true);

                        foreach ($currentUsers as $userId => $user) { ?>
                            <li class="user-item bs-mb-2">
                                <img src="<?= isset($user["profilThumbImageUrl"]) ? $user["profilThumbImageUrl"] : "" ?>" width="30" height="30" alt="" class="user-thumb">
                                <span class="user-name"><?= $user["name"] ?></span>
                                <span class="is-autoeval"><?= isset($user["isAutoEval"]) ? "(Auto-évaluation)" : "" ?></span>
                                <span class="answer-date pull-right bs-pt-2"><?= isset($user["dateAnswer"]) ? date("d/m/Y H:i", strtotime($user["dateAnswer"])) : "" ?></span>
                            </li>
                        <?php } ?>
                    </ul>
                    <?php if ($totalPages > 1) { ?>
                        <button id="loadMoreUsers<?= $containerId ?>" class="btn btn-primary" onclick="loadMoreUsers()">Afficher plus</button>
                    <?php } ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>
</div>


</canvas>
<script type="text/javascript">
    // Function to dynamically load a script
    const _phpParams = {
        containerId: "<?= $containerId ?>",
        chartType: "<?= $chartType ?>",
        chartData: JSON.parse(JSON.stringify(<?= json_encode($chartData) ?>)),
        minDatasetValue: <?= json_encode($minDatasetValue) ?>,
        maxDatasetValue: <?= json_encode($maxDatasetValue) ?>,
        chartLegend: JSON.parse(JSON.stringify(<?= json_encode($chartLegend) ?>)),
        axePointsLabel: JSON.parse(JSON.stringify(<?= json_encode($axePointsLabel) ?>)),
    }
    function loadScript(url, callback) {
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.src = url;
        script.onload = callback;
        document.head.appendChild(script);
    }
    let currentPage = 1;
    const totalPages = <?= $totalPages ?>;
    const usersPerPage = <?= $usersPerPage ?>;
    const users = <?= json_encode($users) ?>;
    const containerId = "<?= $containerId ?>";

    function loadMoreUsers() {
        currentPage++;
        if (currentPage <= totalPages) {
            const start = (currentPage - 1) * usersPerPage;
            const end = Math.min(start + usersPerPage, users.length);
            const usersList = document.getElementById('usersList' + containerId);

            for (let i = start; i < end; i++) {
                const user = users[i];
                const userItem = document.createElement('li');
                userItem.innerHTML = `<li class="user-item bs-mb-2">
                    <img src="${user.profilThumbImageUrl || ''}" width="30" height="30" alt="" class="user-thumb">
                    <span class="user-name">${user.name}</span>
                `;
                userItem.className = 'user-item bs-mb-2';
                usersList.appendChild(userItem);
            }

            if (currentPage === totalPages) {
                document.getElementById('loadMoreUsers' + containerId).style.display = 'none';
            }
        }
    }

    // Check if Chart.js 4.4.0 is already loaded
    if (typeof Chart === 'undefined' || Chart.version !== '4.4.0') {
        loadScript('<?= Yii::app()->request->baseUrl ?>/plugins/Chart.js/chart.4.4.0.min.js', function() {
            initializeChart();
        });
    } else {
        initializeChart();
    }

    function initializeChart() {
        const config = {
            type: _phpParams.chartType,
            data: _phpParams.chartData,
            options: {
                maintainAspectRatio: true,
                aspectRatio: 1,
                scales: {
                    r: {
                        min: _phpParams.minDatasetValue > 0 ? _phpParams.minDatasetValue - 1 : 0,
                        max: _phpParams.maxDatasetValue + 1,
                        angleLines: {
                            color: '#c0bebe'
                        },
                        grid: {
                            circular: true
                        },
                        ticks: {
                            stepSize: 1
                        }
                    }
                },
                elements: {
                    line: {
                        tension: 0.4 // Smooth the lines
                    }
                },
                plugins: {
                    tooltip: {
                        callbacks: {
                            label: function(context) {
                                const label = context.dataset.label || '';
                                const value = context.raw;
                                const description = _phpParams.axePointsLabel?.[context.label]?.[value - 1] ? _phpParams.axePointsLabel[context.label][value - 1] : ucfirst(trad.empty);
                                return `${description}`;
                            },
                            pointStyle: false
                        }
                    },
                    legend: {
                        display: true,
                        position: 'bottom',
                        labels: {
                            generateLabels: function(chart) {
                                const data = chart.data;
                                return data.datasets.map((dataset, i) => {
                                    return {
                                        text: dataset.label,
                                        fontColor: "#838383",
                                        fillStyle: dataset.borderColor, // Use borderColor for background
                                        strokeStyle: dataset.borderColor, // Use borderColor for border
                                        lineWidth: 2,
                                        hidden: !chart.isDatasetVisible(i),
                                        index: i
                                    };
                                });
                            }
                        },
                        onClick: function(e, legendItem) {
                            const index = legendItem.index;
                            const ci = this.chart;
                            const meta = ci.getDatasetMeta(index);

                            // Toggle the visibility of the dataset
                            meta.hidden = meta.hidden === null ? !ci.data.datasets[index].hidden : null;

                            // Update the chart with animation
                            ci.update({
                                duration: 800,
                                easing: 'easeInOutQuad'
                            });
                        }
                    },
                },
            }
        };
        const myChart = new Chart(
            document.getElementById('coformChartContainer<?= $containerId ?>'),
            config
        );
        const lengendHtml = _phpParams.chartData.labels.map(label => {
            const description = _phpParams.chartLegend[label];
            return `<tr><td style="max-width: 15%; min-width: 5%; vertical-align: text-top;" class="bs-pr-2"><strong>${label} </strong></td> <td class="bs-pb-2" style="width: 84%">: ${description}</td></tr>`;
        }).join('');
        $("#customLegendContainer"+_phpParams.containerId).html(
            `
                <table>
                    <tbody>
                        ${lengendHtml}
                    </tbody>
                </table>
            `
        )
    }

</script>