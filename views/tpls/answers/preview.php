<?php 
HtmlHelper::registerCssAndScriptsFiles(array( 
  '/js/answer.js',
  ), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );

?>
<style type="text/css">
    .toolsMenu{
        box-shadow: 0px 1px 3px 0px #a3a3a3;
    }
    #modal-preview-coop{
        overflow : scroll;
    }

    .blockFontPreview {
		font-size: 14px;
		color: #ec5d85;
	}

	h2 {
		font-size: unset;
	}

	#description{
		text-align: justify;
  		text-justify: inter-word;
	}

	#socialNetwork {
		font-size: 20px;
	}

	p.community-text {
		margin: auto 5vh;
		font-size: 21px;
    	margin-top: 8vh;
	}

	.media-body {
		width: auto !important;
	}

	.m-title-desc {
		margin: auto 8vh;
		margin-top: 8vh;
	}

	.mx-1 {
		margin: auto 1vh;
	}

	.space-top-prev {
		margin-top: 8vh;
	}

	.faw-size {
		font-size: xx-large;
	}

	.flex-disp, #socialNetwork {
		display: flex;
		justify-content: center;
	}

	.font-21 {
		font-size: 21px;
	}

	.btn-network {
		display: inline-block;
		width: 50px;
		height: 50px;
		background: #fff/*#f1f1f1*/;
		margin: 5px;
		border-radius: 30%;
		color: #1da1f2;
		overflow: hidden;
		position: relative;
		border: 1px solid #1d3b58;
	}

	.btn-network i {
		line-height: 50px;
		font-size: 25px;
		transition: 0.2s linear;
	}

	@media (max-width: 628px) {
		.media-body {
			width: auto !important;
			display: flex;
			margin-top: 20px
		}

		.media-object {
			margin: auto 20%;
			width: 200px !important;
			height: 200px !important;
		}

		.media {
			margin: auto 8vh;
		}

		p.community-text {
			font-size: initial;
		}

		.media-left {
			display: inherit !important;
		}

		.space-top-prev {
			margin-top: 4vh;
		}
	}


	.banner-outer {
		background-color: #f9f9f9;
		background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-right-bg.png' ?>"); 
		background-position: bottom right;
		background-repeat: no-repeat;
		background-size: 25% auto;
	}

	@media screen and (min-width: 1360px) {
		.basic-banner {
			background-size: auto auto;
		}
	}
	@media screen and (min-width: 768px) {
		.basic-banner {
			background-size: 15% auto;
		}
	}

	.basic-banner {
		padding-top: 80px;
		padding-bottom: 100px;
		background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-left-bg.png' ?>");
		background-position: top left;
		background-repeat: no-repeat;
		background-size: 25% auto;
	}

	.container-banner.max-950 {
		max-width: 980px;
	}

	.container-banner:before {
		display: table;
		content: " ";
	}

	.text-center {
		text-align: center;
	}

	.container-banner:after {
		clear: both;
		display: table;
    	content: " ";
	}

	@media (min-width: 1200px) {
		.container-banner {
			width: 1170px;
		}
	}

	@media (min-width: 992px) {
		.container-banner {
			width: 970px;
		}
	}

	@media (min-width: 768px) {
		.container-banner {
			width: 750px;
		}
	}

	.basic-banner-inner {
		display: flex;
    	justify-content: center;
	}

	.title-banner h1::before {
		content: "";
		position: absolute;
		/* left: 37%; */
		left: 40%;
		top: 100%;
		/* bottom: 50%; */
		z-index: -1;
		margin-left: -1em;
		width: 6em;
		height: 6em;
		background-size: 25% auto;
		background-image: url("<?php echo (!empty(Yii::app()->createUrl('/' . $element["profilMediumImageUrl"])) ? Yii::app()->createUrl('/' . $element["profilMediumImageUrl"]) : Yii::app()->getModule( "co2" )->assetsUrl."/images/thumbnail-default.jpg") ?>");
		background-repeat: no-repeat;
		background-color: transparent;
		border-radius: 50%;
		background-position-x: center;
		background-position-y: center;
		background-size: contain;
		border-radius: 0%;
	}

	.title-section h3::before {
		content: "";
		position: absolute;
		
		top: 15%;
		z-index: -1;
		margin-left: -1em;
		width: 2em;
		height: 2em;
		background-color: #F9B000;
		border-radius: 50%;
	}

	.title-section.m-title-desc h3::before  {
		left: 14%;
	}

	.title-section.m-title-contact h3::before  {
		left: 7%;
	}

	.title-section {
		position: absolute;
	}

	.font-weight-100 {
		font-weight: 100;
	}

	.title-banner {
		position: sticky;
	}

	.m-title-contact {
		margin: auto 8vh;
    	margin-top: 2vh;
	}

	.dv-contact-content {
		margin-top: 3vh;
		margin-bottom: 10vh;
	}
</style>
<script type="text/javascript">
    var sectionDyf = {};
    var formInputs = {};
    var answerObj = <?php echo (!empty($element)) ? json_encode( $element ) : "null"; ?>;
          
</script>

<div id="preview-elt-<?php echo $type ?>-<?php echo (string)$element["_id"] ?>">
    <div class="col-xs-12 padding-10 toolsMenu">
        <button class="btn btn-default pull-right btn-close-preview">
            <i class="fa fa-times"></i>
        </button>
        <a href="#answer.index.id.<?php echo (string)$element["_id"] ?>" class="lbh btn btn-primary pull-right margin-right-10"><?php echo Yii::t("common", "Open") ?></a>
    </div>
    <div class="col-xs-12 no-padding container-preview">
        <div class="banner-outer ">
            <div class="basic-banner">
                <div class="basic-banner-inner">
                    <div class="container-banner max-950"> 
                        <div class="basic-info text-center">
                            <div class="title-banner">
                                <h1 class="font-weight-100"><?php echo $element["name"] ?></h1>
                            </div>	                                      
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php 
    $params = [
        "form"=> PHDB::findOneById( Form::COLLECTION , $element["form"]),
        "canEdit" => @$canEdit,
        "answer"=>$element,
        "mode" => (isset($_GET["mode"])) ? $_GET["mode"] : "r",
        "showWizard"=>false,
        "preview"=>true,
        "wizid"=> $element["form"],
        "color1"=>"",
        "color2"=>""
    ];
    if(isset($params["form"]["subForms"]) && count($params["form"]["subForms"]) > 1)
        $params["form"]["subForms"]=[$params["form"]["subForms"][0]];
    $params = Form::getDataForm($params);
    $params["parentForm"]=$params["form"];
    $params["canEditForm"]=false;
    $params["canAdminAnswer"] = false;

    //var_dump($params);
    echo $this->renderPartial("survey.views.tpls.forms.wizard",$params); 
?>
    </div>
</div>