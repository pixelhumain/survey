
<?php
    if($form["form"]["type"] == "aap")
        echo $this->renderPartial("survey.views.tpls.answers.header",["parentFormId" => $parentFormId],true);
?>
<style>
    .aap-evaluation td,.aap-evaluation tr{
        /*text-align: left;
        font-size: 14px;*/
    }
    th.rotated-text {
        /*position:relative;
        height: 140px;
        white-space: nowrap;
        padding: 0 !important;*/
    }

    th.rotated-text > .th-label {
        /*transform:
            translate(13px, 0px)
            rotate(270deg);
        width: 30px;*/
    }
    th.rotated-text > .sort-icon{
        /*display: block;
        position: absolute;
        bottom: 0;
        right: 12px;*/
    }
    th.rotated-text > div > span {
        padding: 5px 10px;
    }
    .aap-evaluation .dropdown{
        display: inline;
    }
    .aap-evaluation.table thead{
        /*height: 186px;*/
        background: #9fbd39;
    }
    .aap-evaluation.table{
        margin-bottom: 70px;
    }
    .aap-evaluation .dropdown{
        display: inline-block !important;
    }
    .aap-evaluation .dropdown .fa{
        cursor:pointer
    }
    .aap-evaluation .dropdown-menu {
        text-align: center;
    }
    .aap-evaluation .dropdown-menu li:not(:first-child){
        display: inline;
    }
    .aap-table-container{
        display: flex;
        flex-direction: column;
    }
    .mainDash {
      background-color : #fff;
    }

    .TriSea-technologies-Switch > input[type="checkbox"] {
        display: none;   
    }

    .TriSea-technologies-Switch > label {
        cursor: pointer;
        height: 0px;
        position: relative; 
        width: 40px;  
    }

    .TriSea-technologies-Switch > label::before {
        background: rgb(0, 0, 0);
        box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
        border-radius: 8px;
        content: '';
        height: 16px;
        margin-top: -8px;
        position:absolute;
        opacity: 0.3;
        transition: all 0.4s ease-in-out;
        width: 40px;
    }
    .TriSea-technologies-Switch > label::after {
        background: rgb(255, 255, 255);
        border-radius: 16px;
        box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
        content: '';
        height: 24px;
        left: -4px;
        margin-top: -8px;
        position: absolute;
        top: -4px;
        transition: all 0.3s ease-in-out;
        width: 24px;
    }
    .TriSea-technologies-Switch > input[type="checkbox"]:checked + label::before {
        background: inherit;
        opacity: 0.5;
    }
    .TriSea-technologies-Switch > input[type="checkbox"]:checked + label::after {
        background: inherit;
        left: 20px;
    }
    .aap-table-container .star-rating .star-value {
        background: url(<?= Yii::app()->getModule('costum')->assetsUrl . "/images/blockCmsImg/green-star.png" ?>) !important;
        background-size: 15px !important;
        background-repeat-y: no-repeat !important;
    }
</style>

<?php

    $step = "aapStep2";
    if(isset($formConfig["subForms"]["aapStep2"]["params"]["config"]["criterions"]))
        $step = "aapStep2";
    elseif(isset($formConfig["subForms"]["aapStep3"]["params"]["config"]["criterions"]))
        $step = "aapStep3";
    
    //surcharge criteria from form
    if(isset($form["form"]["evaluationCriteria"]["activateLocalCriteria"]) && ($form["form"]["evaluationCriteria"]["activateLocalCriteria"] == true || $form["form"]["evaluationCriteria"]["activateLocalCriteria"] == "true")){
        if(isset($form["form"]["evaluationCriteria"]["type"]))
            $formConfig["subForms"][$step]["params"]["config"]["type"] = $form["form"]["evaluationCriteria"]["type"];
        if(isset($form["form"]["evaluationCriteria"]["criterions"]))
            $formConfig["subForms"][$step]["params"]["config"]["criterions"] = $form["form"]["evaluationCriteria"]["criterions"];
            
    }

    $allowedRoles = isset($formConfig["subForms"][$step]["params"]["canEdit"]) ? $formConfig["subForms"][$step]["params"]["canEdit"] : array();
    if(isset($form["form"]["evaluationCriteria"]["activateLocalCriteria"]) && isset($form["form"]["evaluationCriteria"]["whoCanEvaluate"]) && is_array($form["form"]["evaluationCriteria"]["whoCanEvaluate"]) &&  ($form["form"]["evaluationCriteria"]["activateLocalCriteria"] == true))
        $allowedRoles = $form["form"]["evaluationCriteria"]["whoCanEvaluate"];
    $allowedRoles[] = "Evaluateur";
    $parentElement = array_keys($form["form"]["parent"])[0];
    $me = isset(Yii::app()->session['userId']) ? Person::getById(Yii::app()->session['userId']) : null;
    if(@$form["form"]["parent"][$parentElement]["type"]=="organizations"){
        $roles = isset($me["links"]["memberOf"][$parentElement]["roles"]) ? $me["links"]["memberOf"][$parentElement]["roles"] : null;
        $isAdmin = isset($me["links"]["memberOf"][$parentElement]["isAdmin"]) ? $me["links"]["memberOf"][$parentElement]["isAdmin"] :false;
    }elseif(@$form["form"]["parent"][$parentElement]["type"]=="projects"){
        $roles = isset($me["links"]["projects"][$parentElement]["roles"]) ? $me["links"]["projects"][$parentElement]["roles"] : null;
        $isAdmin = isset($me["links"]["projects"][$parentElement]["isAdmin"]) ? $me["links"]["projects"][$parentElement]["isAdmin"] :false;
    }

    $canEvaluate =  false;
    $intersectRoles = array_intersect($allowedRoles,$roles);
    if(count($intersectRoles) != 0 || $isAdmin)
        $canEvaluate =true;
    $cumulGlobal = [];
    $type =  $form["form"]["parent"][$parentElement]["type"];
    $communityWithAdmin = Element::getCommunityByTypeAndId($type, $parentElement, "all", "isAdmin",null, null, ["name", "profilMediumImageUrl"]);
    $community = Element::getCommunityByTypeAndId($type, $parentElement, "all", null,$allowedRoles, null, ["name", "profilMediumImageUrl"]);
    $community = array_unique(array_merge($communityWithAdmin,$community),SORT_REGULAR);

    $creator = Person::getById($form["form"]["creator"]);

    //$community[(string)$creator["_id"]] = $creator;
    $sumCoeff = 0;
    if(!empty($formConfig["subForms"][$step]["params"]["config"]["criterions"])) {
        foreach ($formConfig["subForms"][$step]["params"]["config"]["criterions"] as $key => $value) {
            $sumCoeff = $sumCoeff + (float)$value["coeff"];
        }
    }

?>

<?php
    if(empty(Yii::app()->session['userId'])){
        echo "<h1>Veillez se connecter</h1>";
    }elseif(empty($formConfig["subForms"][$step]["params"]["config"]["criterions"])){
        echo "<h1>Aucun critère d'évaluation</h1>";
    }else{ ?>
    <?php if($isAdmin == true){ ?>
        <ul class="list-group">
            <li class="list-group-item bg-green">
                L'administrateur peut modifier toutes les évaluations
                <div class="TriSea-technologies-Switch pull-right">
                    <input id="TriSeaSuccess" name="TriSea1" type="checkbox" <?= @$form["form"]["adminCanEditAllEvaluation"] == true ? "checked" :"" ?>/>
                    <label for="TriSeaSuccess" class="label-warning"></label>
                </div>
            </li>
        </ul>
    <?php } ?>
    <div class="aap-table-container">
        <?php  
            foreach ($community as $kcommunity => $vcommunity) {
                $haveMadeAvote = false;
                foreach ($allAnswers as $kallans => $vallans) {
                    if(isset($vallans["answers"][$step]["evaluation"][$kcommunity]))
                        $haveMadeAvote = true;
                }
                if($haveMadeAvote == false)
                    unset($community[$kcommunity]);
            }
            foreach ($community as $kcommunity => $vcommunity) {
                $totalA = 0;
                $totalB = 0;
                $answerPath = "answers.".$step.".evaluation.".$kcommunity;
                $max =0;
                //check if community have not voted yet
                foreach ($allAnswers as $kallans => $vallans) {
                    if(isset($vallans["answers"])){
                        if( !isset($vallans["answers"][$step]["evaluation"][$kcommunity])){
                            $allAnswers[$kallans]["answers"][$step]["evaluation"][$kcommunity] = $formConfig["subForms"][$step]["params"]["config"]["criterions"];
                        }
                        foreach ($formConfig["subForms"][$step]["params"]["config"]["criterions"] as $kcr => $vcr) {
                            if( !isset($allAnswers[$kallans]["answers"][$step]["evaluation"][$kcommunity][$kcr])){
                                $allAnswers[$kallans]["answers"][$step]["evaluation"][$kcommunity][$kcr] = $formConfig["subForms"][$step]["params"]["config"]["criterions"][$kcr];
                            }
                        }
                    }
                }
                
                if(isset($sortBycriteriaIndex) && isset($order)){
                    if(isset($sortBycriteriaIndex) && $sortBycriteriaIndex=="subTotal"){
                        usort($allAnswers, function($a, $b) use($order,$kcommunity,$totalA,$totalB,$step,$formConfig) {
                            foreach (@$a["answers"][$step]["evaluation"][$kcommunity] as $ka => $va) {
                                $totalA = (float) $totalA + ((float) $va['note'] * (float) $formConfig["subForms"][$step]["params"]["config"]["criterions"][$ka]);
                            }
                            foreach (@$b["answers"][$step]["evaluation"][$kcommunity] as $kb => $vb) {
                                $totalB = (float) $totalB + ((float) $vb['note'] * (float) $formConfig["subForms"][$step]["params"]["config"]["criterions"][$kb]);
                            }

                            if($order =="ascending")
                                return (float) $totalA <= (float) $totalB ? -1 : 1;
                            if($order =="descending")
                                return (float) $totalA  >= (float) $totalB ? -1 : 1;
                        });
                    }elseif(isset($sortBycriteriaIndex) && $sortBycriteriaIndex !="subTotal" && $sortBycriteriaIndex !="cumulSubTotal"){
                        if($order =="ascending")
                            usort($allAnswers, function($a, $b) use($sortBycriteriaIndex,$kcommunity,$step){
                                return ((float)@$a["answers"][$step]["evaluation"][$kcommunity][$sortBycriteriaIndex]['note'] <= (float)@$b["answers"][$step]["evaluation"][$kcommunity][$sortBycriteriaIndex]['note']) ? -1 : 1;
                            });
                        if($order =="descending")
                            usort($allAnswers, function($a, $b) use($sortBycriteriaIndex,$kcommunity,$step){
                                return ((float)@$a["answers"][$step]["evaluation"][$kcommunity][$sortBycriteriaIndex]['note'] >= (float)@$b["answers"][$step]["evaluation"][$kcommunity][$sortBycriteriaIndex]['note']) ? -1 : 1;
                            });
                    }
                }
            ?>
            <?php if(!empty($formConfig["subForms"][$step]["params"]["config"]["criterions"])){ ?>
                <div class="table-responsive">
                    <?php echo "<h3 class='text-dark'>".$vcommunity["name"]." <i class='fa fa-chevron-down'></i></h3>".(@(string)$me['_id'] == @(string)$vcommunity['_id'] ? '<h6 class="letter-green-k">Vous pouvez évaluer ici </h6>' : (@$form["form"]["adminCanEditAllEvaluation"] == true && $isAdmin ? '<h6 class="letter-green-k">Vous pouvez évaluer ici </h6>':'')) ;?>
                    <table class="aap-evaluation table table-hover table-condensed table-bordered table-responsive text-dark">
                        <thead>
                            <tr>
                                <?php if(!empty($form["params"]["config"]["type"])) ?>
                                <th class="rotated-text"><div><span></span></div></th>
                                <?php foreach (@$formConfig["subForms"][$step]["params"]["config"]["criterions"] as $kcr => $vcr) { ?>
                                    <th class="rotated-text text-center"><div class="th-label"><span><?= $vcr["label"] ?>   <small class="label label-default">Coeff  <?= $vcr["coeff"] ?></small> </span></div>
                                    <div class="sort-icon">
                                        <i class="fa fa-caret-down sort-by-criteria" data-key="<?= $kcr ?>" data-order="descending" data-clps="table-<?=(string)@$vcommunity['_id']?>"></i>
                                        <i class="fa fa-caret-up sort-by-criteria" data-key="<?= $kcr ?>" data-order="ascending" data-clps="table-<?=(string)@$vcommunity['_id']?>"></i>
                                    </div>
                                    </th>
                                <?php } ?>
                                <th>
                                    <div class="th-label text-center" >
                                        <span>Moyenne /
                                            <?= @$formConfig["subForms"][$step]["params"]["config"]["type"]=="noteCriterionBased" ? "10" : "5"?>
                                        </span>
                                    <div class="sort-icon">
                                        <i class="fa fa-caret-down sort-by-criteria" data-key="subTotal" data-order="descending" data-clps="table-<?=(string)@$vcommunity['_id']?>" ></i>
                                        <i class="fa fa-caret-up sort-by-criteria" data-key="subTotal" data-order="ascending" data-clps="table-<?=(string)@$vcommunity['_id']?>" ></i>
                                    </div>
                                    </div>
                                </th>
                                <?php if($canEvaluate && (string)$me["_id"] == $kcommunity){ ?>
                                    <!-- <th class="rotated-text">
                                    <div class="th-label"><span>Validation </span></div>
                                    </th> -->
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($allAnswers as $kans => $vans) { ?>
                                <?php if(isset($vans["answers"])){   
                                    $subTotal = 0;
                                    $subTotalWithCoeff = 0;
                                ?>  
                                <tr>
                                    <th class="uppercase">
                                        <a href="javascript:;" data-url = '<?php echo Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo @$el_slug; ?>.formid.<?= (string) @$form["form"]["_id"]; ?>.page.sheet.answer.<?php echo @(string)$vans["_id"] ?>' class="aap_plusdinfo">
                                            <?php echo !empty(@$vans["answers"]["aapStep1"]["titre"]) ? @$vans["answers"]["aapStep1"]["titre"] : @$vans["answers"]["aapStep1"]["titre"] ?> 
                                        </a>
                                    </th>

                                    <?php if(@$formConfig["subForms"][$step]["params"]["config"]["type"]=="noteCriterionBased"){ 
                                            ksort($vans["answers"][$step]["evaluation"][$kcommunity]);
                                            foreach (@$vans["answers"][$step]["evaluation"][$kcommunity] as $kev => $vev) {
                                                $critLabel = @$formConfig["subForms"][$step]["params"]["config"]["criterions"][$kev]["label"];
                                                $vev["coeff"] = $critLabel == @$vev["label"] ?  @$formConfig["subForms"][$step]["params"]["config"]["criterions"][$kev]["coeff"] : 1;
                                                $vev["note"] = $critLabel == @$vev["label"] ? (float)$vev["note"] : 0;
                                                if(isset($vev["note"]) && isset($vev["coeff"])){
                                                    $subTotal = $subTotal + ((float) $vev["note"]);
                                                    $subTotalWithCoeff = $subTotalWithCoeff + ((float) $vev["note"] * (float) $vev["coeff"]);
                                                    $cumulGlobal[@$vans["answers"]["aapStep1"]["titre"]]["id"] = $kans;
                                                    if(!isset($cumulGlobal[@$vans["answers"]["aapStep1"]["titre"]]["data"][$kev]))
                                                        $cumulGlobal[@$vans["answers"]["aapStep1"]["titre"]]["data"][$kev]=array();
                                                        $cumulGlobal[@$vans["answers"]["aapStep1"]["titre"]]["data"][$kev]["note"][]= $critLabel == @$vev["label"] ? @$vev["note"] :0;
                                                        $cumulGlobal[@$vans["answers"]["aapStep1"]["titre"]]["data"][$kev]["label"]=$vev["label"];
                                                        $cumulGlobal[@$vans["answers"]["aapStep1"]["titre"]]["data"][$kev]["id"][]= @$kans;
                                                        $cumulGlobal[@$vans["answers"]["aapStep1"]["titre"]]["data"][$kev]["coeff"] = (float) $vev["coeff"];
                                                    
                                                }
                                                ?>
                                                <td <?= ( (string)$me["_id"]==$kcommunity) ? 'contenteditable="true"' : (@$form["form"]["adminCanEditAllEvaluation"] == true && $isAdmin ? 'contenteditable="true"':'')  ?> 
                                                    class="evaluateField"
                                                    data-id="<?= (string)@$vans["_id"] ?>" 
                                                    data-path="<?= $answerPath.'.'.$kev ?>"
                                                    data-label="<?= @$formConfig["subForms"][$step]["params"]["config"]["criterions"][$kev]["label"]?>"
                                                    data-coeff = "<?= @$formConfig["subForms"][$step]["params"]["config"]["criterions"][$kev]["coeff"]?>"
                                                    >
                                                    <?= isset($vev["note"]) ? $vev["note"] : 0 ?>
                                                </td>
                                            <?php } ?>
                                            <?php 
                                                $max = max($max,$subTotalWithCoeff/$sumCoeff);
                                            ?>
                                        <td>
                                            <strong><?php echo round($subTotalWithCoeff/$sumCoeff,2) ?></strong>  <!-- <span class="pull-right"><?= $sumCoeff ?></span> -->
                                        </td>
                                    <?php } ?>
                                    
                                    <?php if(@$formConfig["subForms"][$step]["params"]["config"]["type"]=="starCriterionBased"){ ?>
                                        <?php 
                                            ksort($vans["answers"][$step]["evaluation"][$kcommunity]);
                                            $subTotal = 0;
                                            foreach (@$vans["answers"][$step]["evaluation"][$kcommunity] as $kev => $vev) {
                                                $critLabel = @$formConfig["subForms"][$step]["params"]["config"]["criterions"][$kev]["label"];
                                                $vev["coeff"] = $critLabel == @$vev["label"] ?  @$formConfig["subForms"][$step]["params"]["config"]["criterions"][$kev]["coeff"] : 1;
                                                $vev["note"] = $critLabel == @$vev["label"] ? (float)$vev["note"] : 0;
                                                $starIcon = "";
                                                if(isset($vev["note"]) && isset($vev["coeff"])){
                                                    $subTotal = $subTotal + ((float) $vev["note"]);
                                                    $subTotalWithCoeff = $subTotalWithCoeff + ((float) $vev["note"] * (float) $vev["coeff"]);
                                                    $cumulGlobal[@$vans["answers"]["aapStep1"]["titre"]]["id"] = $kans;
                                                    if(!isset($cumulGlobal[@$vans["answers"]["aapStep1"]["titre"]]["data"][$kev]))
                                                        $cumulGlobal[@$vans["answers"]["aapStep1"]["titre"]]["data"][$kev]=array();
                                                        $cumulGlobal[@$vans["answers"]["aapStep1"]["titre"]]["data"][$kev]["note"][]= $critLabel == @$vev["label"] ? @$vev["note"] :0;
                                                        $cumulGlobal[@$vans["answers"]["aapStep1"]["titre"]]["data"][$kev]["label"]=$vev["label"];
                                                        $cumulGlobal[@$vans["answers"]["aapStep1"]["titre"]]["data"][$kev]["id"][]= @$kans;
                                                        $cumulGlobal[@$vans["answers"]["aapStep1"]["titre"]]["data"][$kev]["coeff"] = (float) $vev["coeff"];
                                                    
                                                }
                                            ?>
                                            <td>
                                                <div class="rater-evaluator" <?= ((string)@$me["_id"]==$kcommunity) ? '' : (@$form["form"]["adminCanEditAllEvaluation"] == true && $isAdmin ? '' : 'data-status="readonly"')  ?> 
                                                    data-rating="<?= $vev["note"] ?>"
                                                    data-id="<?= (string)@$vans["_id"] ?>" 
                                                    data-path="<?= $answerPath.'.'.$kev ?>" 
                                                    data-label="<?= @$formConfig["subForms"][$step]["params"]["config"]["criterions"][$kev]["label"]?>"
                                                    data-coeff="<?= @$formConfig["subForms"][$step]["params"]["config"]["criterions"][$kev]["coeff"]?>">
                                                </div>
                                               <?php  echo $vev["note"] ?>
                                            </td>
                                        <?php } ?>
                                            <td>
                                                <div class="rater-evaluator" data-rating="<?= ($subTotalWithCoeff/$sumCoeff) ?>" data-status="readonly"></div>
                                                <small><?= round($subTotalWithCoeff/$sumCoeff,6) ?></small>
                                            <?php 
                                                $max = max($max,$subTotalWithCoeff/$sumCoeff); 
                                            ?> 
                                            </td> 
                                            <?php if($canEvaluate && (string)$me["_id"] == $kcommunity){ ?>
                                                <!-- <td>
                                                    <input 
                                                        <?= (isset($vans["answers"][$step]["validated"]) && $vans["answers"][$step]["validated"]== true) ? "checked" : ""  ?>
                                                        type="checkbox" 
                                                        class="validationCheckbox"
                                                        data-id="<?= (string)$vans["_id"] ?>" 
                                                        data-path="answers.aapStep3.validated"
                                                        value="false"
                                                    >
                                                </td> -->
                                            <?php } ?>
                                    <?php } ?>
                                </tr>
                                <?php } ?>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            <?php }else{ ?>
                <div class="alert alert-danger">
                    Aucun critère d'évaluation
                </div>
            <?php } ?>
        <?php } ?>

        <div class="table-responsive" style="order:-1">
                <?php
                if(isset($order))
                    uasort($cumulGlobal,function($x,$y) use($formConfig,$order,$step){
                        $totalx = 0;
                        $totaly = 0;
                        foreach($x["data"] as $kx => $vx){
                            
                            $totalx = $totalx + ((array_sum(@$vx["note"])/count(@$vx["note"])) * 
                                @$formConfig["subForms"][$step]["params"]["config"]["criterions"][$kx]["coeff"]);
                        }
                        foreach($y["data"] as $ky => $vy){
                            $totaly = $totaly + ((array_sum(@$vy["note"])/count(@$vy["note"])) *
                                @$formConfig["subForms"][$step]["params"]["config"]["criterions"][$ky]["coeff"]);
                        }
                        if($order =="ascending")
                                return (float) $totalx <= (float) $totaly ? -1 : 1;
                        elseif($order =="descending")
                                return (float) $totalx  >= (float) $totaly ? -1 : 1;
                        
                    });
                ?>
            <?php if(count($community) != 0){ ?>
                <h2 class="text-center text-dark">CUMMULE DES EVALUATIONS</h2>
                <table class="table table-bordered table-hover table-striped text-dark">
                    <thead>
                        <tr>
                            <?php if(!empty($form["params"]["config"]["type"])) ?>
                            <th class="rotated-text"><div><span></span></div></th>
                            <?php foreach (@$formConfig["subForms"][$step]["params"]["config"]["criterions"] as $kcr => $vcr) { ?>
                                <th class="rotated-text text-center"><div class="th-label"><span><?= $vcr["label"] ?>  <br> <small class="label label-default">Coeff  <?= $vcr["coeff"] ?></small> </span></div>
                                <!-- <div class="sort-icon">
                                    <i class="fa fa-caret-down sort-by-criteria" data-key="<?= $kcr ?>" data-order="descending"></i>
                                    <i class="fa fa-caret-up sort-by-criteria" data-key="<?= $kcr ?>" data-order="ascending"></i>
                                </div> -->
                                </th>
                            <?php } ?>
                            <th>
                                <div class="th-label text-center" >
                                    <span>Moyenne /
                                        <?= @$formConfig["subForms"][$step]["params"]["config"]["type"]=="noteCriterionBased" ? "10" : "5"?>
                                    </span>
                                <div class="sort-icon">
                                    <i class="fa fa-caret-down sort-by-criteria" data-key="cumulSubTotal" data-order="descending"></i>
                                    <i class="fa fa-caret-up sort-by-criteria" data-key="cumulSubTotal" data-order="ascending"></i>
                                </div>
                                </div>
                            </th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php foreach ($cumulGlobal as $kcummul => $vcummul) {
                            ksort($vcummul);
                            $subTotalWithCoeffCummul =0;
                            /*var_dump($vcummul);*/ ?>
                            <tr>
                                <td class="uppercase text-bold">
                                        <a href="javascript:;" data-url = '<?php echo Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo @$el_slug; ?>.formid.<?= (string) @$form["form"]["_id"]; ?>.page.sheet.answer.<?= $vcummul["id"] ?>' class="aap_plusdinfo">
                                            <?= $kcummul ?>
                                        </a>
                                </td>
                                <?php if($formConfig["subForms"][$step]["params"]["config"]["type"]=="noteCriterionBased"){ 
                                    foreach($vcummul["data"] as $kcriteria => $vcriteria){
                                        $subTotalWithCoeffCummul = $subTotalWithCoeffCummul + ((array_sum($vcriteria["note"])/count($vcriteria["note"])) * @$formConfig["subForms"][$step]["params"]["config"]["criterions"][$kcriteria]["coeff"])
                                        ?>
                                        <td><?= array_sum($vcriteria["note"])/count($vcriteria["note"]) ?></td>
                                    <?php } ?>
                                    <td>
                                        <?= round($subTotalWithCoeffCummul/$sumCoeff,2) ?>
                                    </td>
                                <?php } ?>

                                <?php if($formConfig["subForms"][$step]["params"]["config"]["type"]=="starCriterionBased"){ 
                                    foreach($vcummul["data"] as $kcriteria => $vcriteria){
                                        $starIcon = "";
                                        $starValueFloat = !empty(array_sum($vcriteria["note"])/count($vcriteria["note"])) ? array_sum($vcriteria["note"])/count($vcriteria["note"]) : 0;
                                        $starValueInt = !empty(array_sum($vcriteria["note"])/count($vcriteria["note"])) ? array_sum($vcriteria["note"])/count($vcriteria["note"]) : 0;
                                        $subTotalWithCoeffCummul = $subTotalWithCoeffCummul + ((array_sum($vcriteria["note"])/count($vcriteria["note"])) * @$formConfig["subForms"][$step]["params"]["config"]["criterions"][$kcriteria]["coeff"])
                                        ?>
                                        <td><?php //echo array_sum($vcriteria["note"])/count($vcriteria["note"]) ?>
                                            <div class="rater-evaluator" data-rating="<?= ($starValueFloat) ?>" data-status="readonly"></div>
                                        </td>
                                    <?php } ?>
                                    <td>
                                    <div class="rater-evaluator" data-rating="<?= ($subTotalWithCoeffCummul/$sumCoeff) ?>" data-status="readonly"></div>
                                        <small><?= round($subTotalWithCoeffCummul/$sumCoeff,2) ?></small>
                                    </td>
                                <?php } ?>   
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } ?>
            <?php if(count($community) == 0){ ?>
                <div class="alert alert-warning">
                    Personne n'a voté, la note de chaque projet est toujours à zéro.
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>


<script>
    $(function(){
        var parentformid = "<?= $parentFormId ?>";
        var basedCriteria = "<?= @$formConfig["subForms"][$step]["params"]["config"]["type"]?>";
        var canEvaluate = <?= $canEvaluate ? "true" : "false" ?>;
        <?php if($canEvaluate){ ?>
            $('.evaluateField').off().on('blur',function(){
                if(basedCriteria == "noteCriterionBased" && $(this).text().trim()>10 ){
                    $(this).text(0);
                    return  toastr.error("Donne une note entre 0 à 10. Merci");
                }
                    
                var id = $(this).data('id');
                var path = $(this).data('path');
                var label = $(this).data('label');
                var coeff = $(this).data('coeff');
                var note = $(this).text().trim();

                var tplCtx = {
                    id : id ,
                    collection : "answers",
                    path : path,
                    value : {
                        label : label,
                        note : note,
                        coeff : coeff
                    }
                };
                dataHelper.path2Value( tplCtx, function(params){
                    toastr.success("Enregistré");
                    ajaxPost("#mainDash", baseUrl+'/survey/answer/aapevaluate/parentformid/'+parentformid, 
                    null,
                    function(){
                        if (typeof hashUrlPage != "undefined") {
                            history.replaceState(location.hash, "", hashUrlPage+".view.forms.dir.evaluate."+parentformid);
                        }
                    },"html");
                })
            })
        <?php } ?>

        $('.sort-by-criteria').off().on('click',function(){
            var sortBycriteriaIndex = $(this).data("key").toString();
            var order = $(this).data("order");
            ajaxPost("#mainDash", baseUrl+'/survey/answer/aapevaluate/parentformid/'+parentformid, 
                {
                    sortBycriteriaIndex : sortBycriteriaIndex,
                    order : order
                },
                function(){
                    if (typeof hashUrlPage != "undefined") {
                        history.replaceState(location.hash, "", hashUrlPage+".view.forms.dir.evaluate."+parentformid);
                    }
                },"html");
        })

        $('.rater-evaluator').each(function(i, obj) {
                var opts = {
                    rating : $(obj).data("rating"),
                    starSize:15,
                    step:0.5,
                    element:obj,
                    rateCallback : function rateCallback(rating, done) {
                        var $this = this;
                        this.setRating(rating); 
                            var tplCtx ={
                                id : $(obj).data("id"),
                                collection : "answers",
                                path : $(obj).data("path"),
                                value : {
                                    label : $(obj).data("label"),
                                    note : rating,
                                    coeff : $(obj).data("coeff")
                                }
                            }
                            dataHelper.path2Value(tplCtx, function (params) {
                                if(params.result){
                                    toastr.success("Enregistré");
                                    ajaxPost("#mainDash", baseUrl+'/survey/answer/aapevaluate/parentformid/'+parentformid, 
                                    null,
                                    function(){
                                        if (typeof hashUrlPage != "undefined") {
                                            history.replaceState(location.hash, "", hashUrlPage+".view.forms.dir.evaluate."+parentformid);
                                        }
                                    },"html");
                                }
                            });
                        done(); 
                    }
                };
                if($(obj).data("status")=="readonly"){
                    opts["readOnly"] = true;
                    delete opts.rateCallback;
                }
                var starRating = raterJs(opts);
        });
        $('#TriSeaSuccess').on("click",function(){
            var prms = {
                id: parentformid,
                collection : "forms",
                path : "adminCanEditAllEvaluation",
                format:true,
            }

            if($(this).is(":checked"))
                prms.value="true"
            else
                prms.value="false"

            dataHelper.path2Value(prms, function (res) {
                if(res.result)
                ajaxPost("#mainDash", baseUrl+'/survey/answer/aapevaluate/parentformid/'+parentformid, 
                null,
                function(){
                    if (typeof hashUrlPage != "undefined") {
                        history.replaceState(location.hash, "", hashUrlPage+".view.forms.dir.evaluate."+parentformid);
                    }
                },"html");
            });
        })
    })
</script>