<style type="text/css">
	#allAnswersList{ list-style: none }
	#allAnswersList li{ padding:5px; border-bottom: 1px solid #ccc;  }
</style>

<div class="col-xs-12 text-center dash-answers">
     <a href="javascript:;" class='btn btn- btn-default' id="showAnswerBtn<?php echo @$viewDom ?>"><i class="fa fa-bars"></i> Les <?php echo $what ?></a>
     <a href="#dashboard" class='lbh btn  btn-default'><i class="fa  fa-area-chart"></i> Observatoire Global</a>
     <a href="#community" class='lbh btn  btn-default'><i class="fa  fa-group"></i> Communauté</a>
     <a href="#answer.new" class='btn  btn-primary'><i class="fa fa-plus"></i>  Ajouter <?php echo $what ?></a>
</div>

<div id="allAnswersContainer<?php echo @$viewDom ?>" class="hide col-xs-12 col-lg-offset-2 col-lg-8 margin-top-20">
	<ul id="allAnswersList">
	<?php 
	$lbl = $what." ";
	$ct = 0;
	if(!empty($allAnswers)){ 
		foreach ($allAnswers as $k => $ans) {
			$ct++;
			?>

		<li class="answerLi col-xs-12 ansline<?php echo $k ?>" >
			<div class="col-xs-3 text-center">
				<a href="#answer.index.id.<?php echo $ans["_id"] ?>.mode.w" class="lbh"> <?php echo $lbl." ".$ct ?></a> 
			</div>
				<?php 
				$lblp = "";
				$percol = "danger";
				
				if(!isset($ans["answers"])) {
					$lblp = "no answers" ;
					$percent = 0;
				} else {
					$totalInputs = 0;
					$answeredInputs = 0;
					// foreach (Yii::app()->session["forms"] as $ix => $f) 
					// {
					// 	$totalInputs += count($f["inputs"]);
					// 	//echo "|".$f['id']."-fi=".count($f["inputs"]);
					// 	if( isset( $ans["answers"][$f['id']] ) ){
					// 		$answeredInputs += count( $ans["answers"][$f['id']] );
					// 		//echo "|".$f['id']."-ai=".count( $ans["answers"][$f['id']] )."<br/>";
					// 	}
					// }
					//echo "tot".$totalInputs."-ans".$answeredInputs;
					$totalInputs = 1;
					$percent = floor($answeredInputs*100/$totalInputs);
					$percol = "primary";
					$lblp = $percent."%";
				}

				if( $percent > 50 )
					$percol = "warning";
				if( $percent > 75 )
					$percol = "success";
				?>
				<div class="col-xs-8">
					<span class="margin-5" style="font-size:0.8em"> <i class="fa fa-calendar"></i> <?php echo date("d/m/y H:i",$ans["created"]); ?></span>

					<?php 
					$step = "instruction";
					$icon = "folder-open-o";
					if ( $percent!= 0 && isset($ans["validation"]) ) {
						if ( isset($ans["validation"]["deal2"]["valid"]) && in_array($ans["validation"]["deal2"]["valid"], ["valid", "validReserve"] ) ) {
							$step = "financement";
							$icon = "money";
						}
						if ( isset($ans["validation"]["deal3"]["valid"]) &&  in_array($ans["validation"]["deal3"]["valid"], ["valid", "validReserve"] ) ){
							$step = "suivi";
							$icon = "cogs";
						}
					}	?>
					<span class="margin-5 " style="font-size:0.8em"> <i class="fa fa-<?php echo $icon; ?>"></i> <?php echo $step; ?></span>
					
					<br/>
					
					<span class="margin-5 label label-<?php echo $percol ?>"> <i class="fa fa-pencil-square-o"></i> <?php echo $lblp ?> </span>

					
					<br/>
					<?php if ( $percent!= 0) {?>
					<a href="#dashboard.answer.<?php echo $ans["_id"] ?>" class='margin-5 lbh btn btn-default '> <i class="fa fa-group "></i>Communauté <span class="margin-5  label label-primary"> 3 </span></a>
					
					<a href="#dashboard.answer.<?php echo $ans["_id"] ?>" class='margin-5  lbh btn btn-default '> <i class="fa fa-pie-chart "></i> Observatoire Local</a>
					<?php } ?>
				</div>

				<div class="col-xs-1">
					<a class='text-red pull-right deleteAnswer' data-id="<?php echo $k ?>" href="javascript:;"><i class="fa  fa-trash"></i> </a> 
				</div>
			
		</li>
		
	<?php } 
	}?>
		<li class="text-center"><a href="#answer.new" class="btn btn-xs btn-primary lbh-menu-app"><i class="fa fa-plus"></i>  <?php echo Yii::t("common","Add") ?></a></li>
	</ul>
</div>
<script type="text/javascript">

jQuery(document).ready(function() {

  mylog.log("render","/modules/survey/views/tpls/answers/allAnswers.php");
 $('#showAnswerBtn<?php echo @$viewDom ?>').on("click",function() { 
    $("#allAnswersContainer<?php echo @$viewDom ?>").toggleClass("hide");
    $('#wizardForm').toggleClass("hide");
    //getAjax(".pageContent", baseUrl+"/costum/co/index/slug/<?php echo $el['slug'] ?>/page/answerList",null,"html");
   })

<?php if($canEdit) { ?>
	

	  $('.deleteAnswer').off().click( function(){
      id = $(this).data("id");
      var $this=$(this);
      bootbox.dialog({
          title: trad.confirmdelete,
          message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
          buttons: [
            {
              label: "Ok",
              className: "btn btn-primary pull-left",
              callback: function() {
              	$this.find("i").removeClass("fa-trash").addClass("fa-spin fa-spinner");
                getAjax("",baseUrl+"/survey/co/delete/id/"+id,function(){
                	toastr.success("La candidature a été supprimée avec succès");
                	$("#allAnswersList .ansline"+id).remove();
                },"html");
              }
            },
            {
              label: "Annuler",
              className: "btn btn-default pull-left",
              callback: function() {}
            }
          ]
      });
    });


<?php } ?>
  
  

});


</script>