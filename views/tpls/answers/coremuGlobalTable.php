<?php
HtmlHelper::registerCssAndScriptsFiles(array(
    '/js/coremu.js'
), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );

$modalrender = true;
$renderUrl = @$urlR;

$exp_url = explode("/", $renderUrl);
$el_slug = null;
$el_form_id = "";

if (isset($this->costum["slug"])){
    $slug = $this->costum["slug"];
}

if (
    in_array("slug", $exp_url) &&
    isset($exp_url[array_search('slug', $exp_url) + 1]) &&
    is_string($exp_url[array_search('slug', $exp_url) + 1]) &&
    $exp_url[array_search('slug', $exp_url) + 1] != ""
){
    $el_slug = $exp_url[array_search('slug', $exp_url) + 1];
}

if (
    in_array("formid", $exp_url) &&
    isset($exp_url[array_search('formid', $exp_url) + 1]) &&
    is_string($exp_url[array_search('formid', $exp_url) + 1]) &&
    $exp_url[array_search('formid', $exp_url) + 1] != ""
){
    $el_form_id = $exp_url[array_search('formid', $exp_url) + 1];
    $elform = PHDB::findOneById(Form::COLLECTION, $el_form_id);
    if(!empty($elform)){
        $el_configform = PHDB::findOneById(Form::COLLECTION, $elform["config"]);
        $elanswers = PHDB::find(Form::ANSWER_COLLECTION,array( "form" => (string)$elform["_id"],"answers.aapStep1.titre"=>['$exists' => true]));
    }
}

if (isset($this->costum["slug"])){
    $slug = $this->costum["slug"];
}
$el = null;
if (!empty($el_slug)) {
    $el = Slug::getElementBySlug($el_slug);
}
?>

<style>
    .btn_cont {
        width: 550px;
        height: 60px;
        position: relative;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -1%);
        padding-top: 20px;
    }
    .btn_cont_right {
        width: 550px;
        height: 60px;
        position: relative;
        left: 20px;
        top: 50%;
        padding-top: 20px;
    }
    .edit_mode {
        float: left;
        width: 135px;
        margin-top: 10px;
    }
    .edit_mode label {
        float: left;
    }
    .edit_mode .toggle_div {
        float: right;
    }
    .move_all {
        float: left;
    }
    .clear_grid, .ser_grid {
        float: right;
        margin: 0 5px 0 5px;
    }

    .aaptilescont  .aapinnertiles1 {
        background-color: #a2d17f;
    }
</style>

<?php
$allData = json_decode(json_encode($allData));

if ($hv_parent)
{
    ?>

    <div class="col-md-offset-1 col-md-10 col-xs-12 margin-top-5">
        <button type="button" class="btn aapgoto" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_parent_slug; ?>.formid.<?php echo $el_parent_form_id ?>.page.multidashboard">Multidashboard</button>
    </div>
    <?php
}
elseif (isset($el["el"]["oceco"]["subOrganization"]) && filter_var(    $el["el"]["oceco"]["subOrganization"], FILTER_VALIDATE_BOOLEAN)  && !$hv_parent)
{
    ?>

    <div class="col-md-offset-1 col-md-10 col-xs-12 margin-top-5">
        <button type="button" class="btn aapgoto" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.multidashboard">Multidashboard</button>
    </div>
    <?php
}
?>
<div id="ocecofiltercontainer" class="searchObjCSS menuFilters menuFilters-vertical col-xs-12 bg-light text-center"></div>

<div class="col-md-12 col-xs-12 aapdashboard" id="coremudashboardcompact">

</div>

<script>
    var allDataAns = <?php echo json_encode($allDataAns) ?>;
    var allData = <?php echo json_encode($allData) ?>;
    var parentDashboard = <?php echo json_encode($parent) ?>;
    var fp = <?php echo json_encode($formparent) ?>;
    coInterface.showLoader("#coremudashboardcompact");
    jQuery(document).ready(function() {
        ajaxPost("#coremudashboardcompact", baseUrl + '/survey/form/coremudashboard/tableonly/true/el_form_id/' + fp["_id"]["$id"],
            null,
            function () {
                /* $("#coremudashboard .aapdashboard").removeClass("col-md-offset-1")
                     .removeClass("col-md-10")
                     .addClass("col-md-12")
                     .css("margin-top" , "0px");*/
            }, "html");
    });
</script>
