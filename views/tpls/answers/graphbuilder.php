<?php 
$cssJS = array(
    // '/plugins/gridstack/css/gridstack.min.css',
    // '/plugins/gridstack/js/gridstack.js',
    // '/plugins/gridstack/js/gridstack.jQueryUI.min.js'

);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);


HtmlHelper::registerCssAndScriptsFiles(array( 
  '/css/graphbuilder.css',
  '/js/form.js'
  ), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );
?>

<?php 
	$inputTypes = [
		"count" => "nombre de réponses", //total , d'une question, choix d'un option radio/checkbox
		"cumul" => "opération sur un champ nombre", // label + number
		"sorting" => "Le tri à plat", //  label + one datasets [bar/pie/line]
		"crossing" => "Le tri croisé", // label + multiple datasets [radar/bar/line]
		"map" => "Carte"
    //mindmap
	];

  $inputSubTypes = [
    "count" => [
        "totalanswer" => "nombre de réponse total",
        "totalquestion" => "nombre de réponse total à un champ spécifique",
    ],
    "cumul" => [
        "cumulreponse" => "cumulé d'un champ de type nombre",
        "moyenne" => "moyenne des réponse d'un champ nombre"
    ],
    "sorting" => [
        "pie" => "pie",
        "bar" => "bar"
    ],
    "crossing" => [
        "barMany" => "bar",
        "radar" => "radar"
    ],
    "map" => [
        "simpleMap" => "Carte simple"
    ]
  ];

      $titlevalue = "";
      $titleparams = [];
      foreach ($allanswers["titleparams"] as $key => $value) {
       $titleparams = array_merge($titleparams, array($value["id"] => $value["title"]));
      }

      echo $this->renderPartial("survey.views.tpls.answers.header",["parentFormId" => $allanswers['parentFormId'], "adminRight" => $adminRight] ,true );

?>

<!-- <div id="feedback">
    <a href="javascript:;" class="shmap">Afficher la carte</a>
</div>

 <div class="navdash">
	<section style="padding-top: 0px !important;">
	  	<nav>
		    <ul class="menuItems">
		      	<li><a href='javascript:;' class=" getliste" data-item='Liste'>Liste</a></li>
		      	<li class="active"><a href='javascript:;' class="active" data-item='Graph'>Graph</a></li>
		    </ul>
	  	</nav>

	</section>
</div>  -->

<div id="mainDash" class="mainDash">

  <div class="row buttonListDash">
    <h2 class = "pull-left" style="margin-left: 50px; text-transform: none; font-family: 'Lato', sans-serif !important;"> Graphbuilder 
    </h2>
<?php if ($adminRight) { ?>

    <div class="btn-group pull-right">
      <button class="btn btn-outline-dark titleparams"><i class="fa fa-cog"></i> Paramètre </button>
      <button class="btn btn-outline-dark addgraph"><i class="fa fa-plus"></i> Ajouter </button>
    </div>
  </div> 
  
<?php } ?>

  <div class="" id="bodygraph">
      <div class="surveys grid survey-grid col-md-11 col-sd-11 grid-stack" style="display: inline-block;">
    <?php 
      for ($i=0; $i < sizeof($allanswers['index']); $i++) {

        if ($allanswers['type'][$i] != "" and ($allanswers['type'][$i] == "count" or $allanswers['type'][$i] == "cumul")) {
    ?>
          <div class="survey-item smallTile visiblehovercontainer grid-stack-item" data-gs-width="4" data-gs-height="2" style="height: 100px;" id="<?php echo $allanswers["index"][$i] ?>">
              <div class="grid-stack-item-content" >
                  <span class="survey-name responsedoctitle" style="font-family: 'Raleway', sans-serif;">
                    <span class="lstick "></span>
                      <?php echo $allanswers["title"][$i] ?>
                  </span>

                  <?php if ($adminRight) { ?>
                  <div class="pull-right">
          
                      <span class="survey-stage" style="top: 0px">
                              <button 
                                style="margin: 5px 5px 0px;  border-radius: 10px;" 
                                class="btn btn-outline-dark btn-xs dropdown-toggle visiblehover params<?php echo $allanswers['index'][$i].$allanswers['type'][$i] ?>" 
                                type="button"
                                data-index="<?php echo $allanswers["index"][$i] ?>"
                                > 
                                <i class="fa fa-cog"></i>
                                        
                                </button>
                                <button 
                                style="margin: 5px 5px 0px;  border-radius: 10px;" 
                                class="btn btn-outline-dark btn-xs dropdown-toggle visiblehover deleteGraph" 
                                type="button"
                                data-index="<?php echo $allanswers["index"][$i] ?>"
                                data-key="<?php echo $allanswers["index"][$i] ?>"
                                data-path = "graph.<?php echo $allanswers['index'][$i]; ?>"
                                > 
                                <i class="fa fa-trash" style="color: red"></i>
                                </button>
                      </span>
                  </div>
                  <?php } ?>

                  <div id="<?php echo $allanswers['index'][$i] ?>" style="    font-family: 'Nunito', 'Raleway', sans-serif; font-size: 25px; color: #74b976;">
                    <?php 
                      if(isset($allanswers["render"][$i])){
                          echo $allanswers["render"][$i]; 
                      }
                    ?>
                  </div>
              </div>
          </div>



    <?php
          
        }
        elseif($allanswers['type'][$i] != "" and $allanswers['type'][$i] != "count" and $allanswers['type'][$i] != "cumul"){

          $height = "middleTile";
          if($allanswers['height'][$i] == ""){
            $height = "middleTile";
          } elseif ($allanswers['height'][$i] == "small") {
            $height = "smallTile";
          } elseif ($allanswers['height'][$i] == "normal") {
            $height = "middleTile";
          } elseif ($allanswers['height'][$i] == "high") {
            $height = "highTile";
          } elseif ($allanswers['height'][$i] == "full") {
            $height = "fullTile";
          }


    ?>

          <div class="survey-item <?php echo $height; ?> id="<?php echo $allanswers["index"][$i] ?>"">
              
              <span class="survey-name responsedoctitle" style="font-family: 'Raleway', sans-serif;">
                  <?php echo $allanswers["title"][$i] ?>
              </span>

             <?php if ($adminRight) { ?>
              <div class="pull-right">
      
                  
                  
                  <span class="survey-stage">
                          <button 
                            style="margin: 5px 5px 0px;  border-radius: 10px; " 
                            class="btn btn-outline-dark btn-xs dropdown-toggle params<?php echo $allanswers['index'][$i].$allanswers['type'][$i] ?>" 
                            type="button"
                            data-index="<?php echo $allanswers["index"][$i] ?>"
                            > 
                            <i class="fa fa-cog"></i>
                            </button>
                            <button 
                            style="margin: 5px 5px 0px;  border-radius: 10px; " 
                            class="btn btn-outline-dark btn-xs dropdown-toggle deleteGraph" 
                                type="button"
                                data-index="<?php echo $allanswers["index"][$i] ?>"
                                data-key="<?php echo $allanswers["index"][$i] ?>"
                                data-path = "graph.<?php echo $allanswers['index'][$i]; ?>"
                            > 
                            <i class="fa fa-trash" style="color: red"></i>
                            </button>
                  </span>
              </div>
              <?php  } ?>

              <div id="<?php echo $allanswers['index'][$i] ?>" style="margin-top: 20px">
                
              </div>

          </div>


      
    <?php 
        } else if($allanswers['type'][$i] != "" && $allanswers['type'][$i] == "counter"){
    ?>


    <?php 
        }
    ?>


    <?php 
      }
     ?>
  </div>
</div>
</div>

<?php
	$num = sizeof($allanswers['index']) +1;

	$heightbloc = [
		"small" => "petite",
		"normal" => "moyenne",
		"high" => "grande",
		"full" => "toute la largeur disponible"
	];

?>

<script type="text/javascript">
  cfbObj = formObj.init({});
  cfbObj.container = ".pageContent"
  cfbObj.events.graphbuilder(cfbObj);
  cfbObj.events.obs(cfbObj);
  <?php

      foreach ($inputTypes as $o => $p) {
      ?>
          var <?php echo $o?> = {
                    subtype : {
                            inputType : "select",
                            label : "Sélectionnez le type de présentation",
                            placeholder : "Sélectionnez le type de présentation",
                            noOrder : true,
                            options : <?php echo json_encode($inputSubTypes[$o]); ?>,
                            rules : { required : true },
                            value :  ""
                        },

          <?php  
          if ($o != "crossing") {
          ?>
                          question : {
                                inputType : "select",
                                label : "Sélectionnez un champ",
                                placeholder : "Sélectionnez un champ",
                                noOrder : true,
                                options : <?php echo json_encode($titleparams); ?>,
                                rules : { required : true },
                                value :  ""
                            }

          <?php
          } else {
          ?>
                          label : {
                                inputType : "select",
                                label : "Sélectionnez le label",
                                placeholder : "Sélectionnez un champ",
                                noOrder : true,
                                options : <?php echo json_encode($titleparams); ?>,
                                rules : { required : true },
                                value :  ""
                            },
                            var1 : {
                                inputType : "select",
                                label : "Sélectionnez le champ à croiser",
                                placeholder : "Sélectionnez un champ",
                                noOrder : true,
                                options : <?php echo json_encode($titleparams); ?>,
                                rules : { required : true },
                                value :  ""
                            }

          <?php
          } 
          ?>

        };

      <?php
      }
      ?>




	<?php 
	 	for ($i=0; $i < sizeof($allanswers['index']); $i++) { 
	 		if($allanswers['type'][$i] != "" and $allanswers['type'][$i] != "count" and $allanswers['type'][$i] != "cumul"){

	 ?>
	 			var <?php echo $allanswers['index'][$i]."Data" ?> = <?php echo json_encode($allanswers['render'][$i]); ?>;

	<?php 
			}
		}
	?>


	$(document).ready(function() {

	<?php 
	 	for ($i=0; $i < sizeof($allanswers['index']); $i++) { 
	 		if($allanswers['type'][$i] != "" and $allanswers['type'][$i] != "count" and $allanswers['type'][$i] != "cumul"){

	 ?>
	 			ajaxPost('#<?php echo $allanswers['index'][$i]?>', baseUrl+'/graph/co/dash/g/graph.views.co.observatory.<?php echo $allanswers['render'][$i]["type"] ?>'+"/id/<?php echo $allanswers['index'][$i] ?>", null, function(){},"html");

	<?php 
			}
		}
	?>

	});


$(document).ready(function() {
  //   $(function () {
  //   $('.grid-stack').gridstack({
  //     cellHeight: 100,
  //     verticalMargin: 10,
  //     horizontalMargin: 10,
  //     disableResize: true,
  //     removable: true
  //   });
  // }); 
});



	$(".getliste").on('click', function() {
      ajaxPost("#central-container", baseUrl+'/survey/answer/dashboard/answer/<?php echo $allanswers['parentFormId']; ?>', 
          null,
          function(){
              history.replaceState(location.hash, "", hashUrlPage+".view.forms.dir.observatory.<?php echo $allanswers['parentFormId']; ?>");
          },"html");
  })
/*

tiles /dynform
export
tableau


*/

	$(document).ready(function() {

        $(".addgraph").off().on("click",function() { 
            sectionDyfObservatory = {
                "jsonSchema" : {
                    "title" : "Ajouter un graphique",
                    "icon" : "fa-cog",
                    "text" : "Ajouter un graphique",
                    "properties" : {
                      	label : { label : "Titre du graphique" },
	                    placeholder : { label : "Texte dans le graphique" },
	                    info : { 
	                        inputType : "textarea",
	                        label : "Information complémentaire" },
	                    type : { label : "Type",
	                             inputType : "select",
	                             options : <?php echo json_encode($inputTypes); ?>,
	                             value : "bar" 
	                    },
	                    height : { label : "Taille",
	                             inputType : "select",
	                             options : <?php echo json_encode($heightbloc); ?>,
	                             value : "bar" 
	                    }
	                },
                    save : function () {
                    	var tplCtx = {};

                        tplCtx.id = "<?php echo $allanswers['parentFormId']; ?>",
                        tplCtx.path = "graph.<?php echo $allanswers['name'].$num; ?>",
                        tplCtx.collection = "<?php echo Form::COLLECTION ?>";  
                        var today = new Date();
                        tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
                        
                        $.each( sectionDyfObservatory.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                        });

                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                dyFObj.closeForm();
                                urlCtrl.loadByHash(location.hash);
                            } );
                        }

                    }
                }
            };

            dyFObj.openForm( sectionDyfObservatory );

		});

        <?php
      for ($i=0; $i < sizeof($allanswers['index']); $i++) {

        ?>


    $(".params<?php echo $allanswers['index'][$i].$allanswers['type'][$i] ?>").off().on("click",function() { 
            sectionDyfObservatory = {
                "jsonSchema" : {
                    "title" : "Ajouter un graphique",
                    "icon" : "fa-cog",
                    "text" : "Ajouter un graphique",
                    "properties" : <?php echo $allanswers['type'][$i]; ?>
                  ,
                    save : function () {
                      var tplCtx = {};

                        tplCtx.id = "<?php echo $allanswers['parentFormId']; ?>",
                        tplCtx.path = "graph.<?php echo $allanswers['index'][$i]; ?>.render",
                        tplCtx.collection = "<?php echo Form::COLLECTION ?>";  
                        var today = new Date();

                        tplCtx.value = {};
                        
                        $.each( sectionDyfObservatory.jsonSchema.properties , function(k,val) {
                            if(k == "subtype"){
                                tplCtx.value[k] = $("#"+k).val();
                            }else {
                              if(typeof tplCtx.value["variables"] == "undefined"){
                                  tplCtx.value["variables"] = {};
                              } 
                              tplCtx.value["variables"][k] = $("#"+k).val();
                            }
                        });

                        tplCtx.value["func"] = { "datasets" : ["label", "var1"] };

                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                //location.reload();
                            } );
                        };

                    }
                }
            };

            dyFObj.openForm( sectionDyfObservatory );

    });

    $('.deleteGraph').off().click( function(){
      formId = "<?php echo $allanswers['parentFormId']; ?>";
      key = $(this).data("key");
      pathLine = $(this).data("path");
      collection = "<?php echo Form::COLLECTION ?>";
      bootbox.dialog({
          title: trad.confirmdelete,
          message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
          buttons: [
            {
              label: "Ok",
              className: "btn btn-primary pull-left",
              callback: function() {
                var formQ = {
            value:null,
            collection : collection,
            id : formId,
            path : pathLine
          };
          
          dataHelper.path2Value( formQ , function(params) { 
            $("#"+key).remove();
                        //location.reload();
          } );
              }
            },
            {
              label: "Annuler",
              className: "btn btn-default pull-left",
              callback: function() {}
            }
          ]
      });
    });

    <?php 
      }
    ?>

    });
</script>
