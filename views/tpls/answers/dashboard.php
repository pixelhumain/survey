<?php
// ini_set('memory_limit', '-1');
$cssJS = array(
	'/plugins/jlist/js/jplist.core.min.js',
	'/plugins/jlist/js/jplist.bootstrap-sort-dropdown.min.js',
	'/plugins/jlist/js/jplist.pagination-bundle.min.js',
	'/plugins/jlist/js/jplist.textbox-filter.min.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);

HtmlHelper::registerCssAndScriptsFiles(array(
	// '/css/dashboard.css',
	'/js/form.js',
	'/css/inputs-style.css',
	'/css/space-layout.css',
	'/css/codate/codate.css'
), Yii::app()->getModule(Survey::MODULE)->getAssetsUrl());
HtmlHelper::registerCssAndScriptsFiles(array(
	'/js/aap/aap.js',
	'/js/aap/aap-overload.js'
), Yii::app()->getModule("co2")->getAssetsUrl());
?>

<style type="text/css">
	@charset "UTF-8";
	@import url(https://fonts.googleapis.com/css?family=Lato:300,400,700);
	@import url(http://weloveiconfonts.com/api/?family=entypo);

	/* entypo */
	[class*=entypo-]:before {
		font-family: "entypo", sans-serif;
	}

	.answer-submited {
		font-size: 15px;
		display: block;
		border: 2px solid #525f68;
		width: fit-content;
		padding: 2px 10px;
		border-radius: 15px;
		margin: 2px 5px;
	}

	.bodydashboard {
		box-sizing: border-box;
	}

	.bodydashboard {
		background: #f5f5f5;
		max-width: 1200px;
		margin: 0 auto;
		padding: 10px;
		font-family: "Lato", sans-serif;
		text-shadow: 0 0 1px rgba(255, 255, 255, 0.004);
		font-size: 100%;
		font-weight: 400;
	}

	.toggler {
		color: #A1A1A4;
		font-size: 1.25em;
		margin-left: 8px;
		text-align: center;
		cursor: pointer;
	}

	.toggler.active {
		color: #000;
	}

	.surveys {
		list-style: none;
		margin: 0;
		padding: 0;
	}

	.survey-item {
		display: block;
		margin-top: 10px;
		padding: 20px;
		border-radius: 2px;
		background: white;
		/*box-shadow: 0 2px 6px rgb(0 0 0 / 18%);*/
	}

	.survey-name {
		font-weight: 400;
	}

	.list .survey-item {
		position: relative;
		padding: 0;
		font-size: 14px;
		line-height: 40px;
	}

	.list .survey-name {
		width: 30%;
		white-space: nowrap;
		overflow: hidden;
		display: inline-block;
	}

	.list .survey-item .pull-right {
		position: absolute;
		right: 0;
		top: 0;
	}

	@media screen and (max-width: 800px) {
		.list .survey-item .stage:not(.active) {
			display: none;
		}
	}

	@media screen and (max-width: 700px) {
		.list .survey-item .survey-progress-bg {
			display: none;
		}
	}

	@media screen and (max-width: 600px) {
		.list .survey-item .pull-right {
			position: static;
			line-height: 20px;
			padding-bottom: 10px;
		}
	}

	.list .survey-country,
	.list .survey-progress,
	.list .survey-completes,
	.list .survey-end-date {
		color: #A1A1A4;
	}

	.list .survey-country,
	.list .survey-completes,
	.list .survey-end-date,
	.list .survey-name,
	.list .survey-stage {
		margin: 0 10px;
	}

	.list .survey-country {
		margin-right: 0;
	}

	.list .survey-end-date,
	.list .survey-completes,
	.list .survey-country,
	.list .survey-name {
		vertical-align: middle;
	}

	.list .survey-end-date {
		display: inline-block;
		width: 100px;
		white-space: nowrap;
		overflow: hidden;
	}

	.survey-stage .stage {
		display: inline-block;
		vertical-align: middle;
		width: 16px;
		height: 16px;
		overflow: hidden;
		border-radius: 50%;
		padding: 0;
		margin: 0 2px;
		background: #f2f2f2;
		text-indent: -9999px;
		color: transparent;
		line-height: 16px;
	}

	.survey-stage .stage.active {
		background: #A1A1A4;
	}

	.list .list-only {
		display: auto;
	}

	.list .grid-only {
		display: none !important;
	}

	.list .social-links {
		display: inline-flex;
	}

	.grid .grid-only {
		display: auto;
	}

	.grid .list-only {
		display: none !important;
	}

	.grid .survey-item {
		position: relative;
		display: inline-block;
		vertical-align: top;
		/*height: 200px;*/
		width: 250px;
		margin: 10px;
		min-height: 260px;
		padding: 4rem 1rem 7rem 1rem;
		background-color: #dadada;
		position: relative;
		border: 0px !important;
	}

	/*By nicoss*/
	.grid .survey-item:after {
		content: "";
		display: block;
		width: 0px;
		height: 0px;
		background-color: skyblue;
		top: 0px;
		right: 0px;
		border-bottom: 20px solid #dadada;
		border-left: 20px solid #dadada;
		border-right: 20px solid #f8f8f8;
		border-top: 20px solid #f8f8f8;
		position: absolute;
		-webkit-filter: drop-shadow(-5px 5px 3px rgba(0, 0, 0, 0.5));
		filter: drop-shadow(-5px 5px 3px rgba(0, 0, 0, 0.5));
	}

	.grid .survey-item:before {
		content: "";
		display: block;
		width: 0px;
		height: 0px;
		border-top: 40px solid #dadada;
		border-right: 40px solid #dadada;
		border-left: 40px solid #f8f8f8;
		border-bottom: 40px solid #f8f8f8;
		bottom: 0px;
		left: 0px;
		position: absolute;
		-webkit-filter: drop-shadow(7px -7px 5px rgba(0, 0, 0, 0.5));
		filter: drop-shadow(7px -7px 5px rgba(0, 0, 0, 0.5));
		margin-right: 10%;
	}

	/*By nicoss*/
	@media screen and (max-width: 767px) {
		.grid .survey-item {
			display: block;
			width: auto;
			height: 150px;
			margin: 10px auto;
			min-width: 190px;
		}
	}

	.grid .survey-name {
		display: block;
		max-width: 80%;
		font-size: 16px;
		line-height: 20px;
		white-space: nowrap;
		overflow: hidden;
	}

	.grid .survey-country {
		font-size: 11px;
		line-height: 16px;
		text-transform: uppercase;
	}

	.grid .survey-end-date {
		color: #A1A1A4;
	}

	.grid .survey-country.term {
		color: green;
	}

	.grid .survey-country.aban {
		color: red;
	}

	.grid .survey-country.encours {
		color: blue;
	}


	.grid .survey-end-date:before {
		content: "Ends ";
	}

	.grid .survey-end-date.ended:before {
		content: "<?php echo Yii::t('survey', 'Modified on') ?>";
	}

	.grid .survey-progress {
		display: block;
		position: absolute;
		/*bottom: 0;*/
		bottom: 80px;
		left: 0;
		right: 0;
		width: 100%;
		padding: 20px;
		border-top: 1px solid #eee;
		font-size: 13px;
	}

	.grid .survey-progress-bg {
		width: 40%;
		display: block;
	}

	@media screen and (max-width: 200px) {
		.grid .survey-progress-bg {
			display: none;
		}
	}

	.grid .survey-progress-labels {
		position: absolute;
		right: 20px;
		top: 0;
		line-height: 40px;
	}

	@media screen and (max-width: 200px) {
		.grid .survey-progress-labels {
			right: auto;
			left: 10px;
		}
	}

	.grid .survey-progress-label {
		line-height: 21px;
		font-size: 13px;
		font-weight: 400;
	}

	.grid .survey-completes {
		line-height: 21px;
		font-size: 13px;
		vertical-align: middle;
	}

	.grid .survey-stage {
		position: absolute;
		top: 10px;
		right: 52px;
	}

	.grid .survey-stage .stage {
		display: none;
	}

	.grid .survey-stage .active {
		display: block;
	}

	.grid .survey-end-date {
		font-size: 12px;
		line-height: 20px;
	}

	.survey-progress-label {
		vertical-align: middle;
		margin: 0 10px;
		color: #8DC63F;
	}

	.survey-progress-bg {
		display: inline-block;
		vertical-align: middle;
		position: relative;
		width: 100px;
		height: 4px;
		border-radius: 2px;
		overflow: hidden;
		background: #eee;
	}

	.survey-progress-fg {
		position: absolute;
		top: 0;
		bottom: 0;
		height: 100%;
		left: 0;
		margin: 0;
		background: #8DC63F;
	}

	#navigation ul {
		margin: 0;
		padding: 0;
	}

	#navigation ul li {
		list-style-type: none;
		display: inline;
	}

	#navigation li:not(:first-child):before {
		content: " | ";
	}

	.view-icon {
		font-size: 23px;
		color: #8f8f8f;
	}

	.view-icon:hover {
		color: #8DC63F;
	}

	.view-icon.s {
		font-size: 18px;
	}

	#displayOptions a {
		cursor: pointer;
	}

	#displayOptions a.active i {
		color: #8DC63F;
	}

	#displayOptions button,
	.inner-addon.right-addon input.form-control {
		height: 34px;
		box-shadow: inset 0 -3px 0 0 #9fbd38;
		background-color: #f0f0f0;
		color: #1e1e1e;
		border: 1px solid #f0f0f0;
		border-radius: 10px;
		margin: 0px 5px;
	}

	#displayOptions .btn-outline-dark:not(:disabled):not(.disabled).active,
	#displayOptions .btn-outline-dark:not(:disabled):not(.disabled):active,
	#displayOptions .show>.btn-outline-dark.dropdown-toggle,
	#displayOptions button:hover {
		background-color: #8DC63F;
		border-color: #8DC63F;
		color: #fff;
		box-shadow: inset 0 -3px 0 0 #2c3e50;
	}

	#displayOptions .view-icon svg {
		width: 19px;
	}

	#displayOptions .view-icon .view-icon-svg {
		fill: #8f8f8f;
	}

	#displayOptions .view-icon.active .view-icon-svg,
	#displayOptions .view-icon:hover .view-icon-svg {
		fill: #8DC63F;
	}

	.mobileShow {
		display: none;
	}

	@media only screen and (max-device-width : 580px) {
		.mobileShow {
			display: inline;
		}
	}

	.obstopbtn {
		padding: 3px;
	}

	.obstopcnttr {
		display: inline-flex;
	}

	.obsorderby {
		padding-left: 10px;
		display: inline-flex;
	}

	@media only screen and (min-device-width : 600px) {
		.obsorderby {
			display: inline;
		}
	}

	.flex-center {
		display: flex;
		justify-content: center;
		align-items: center;
	}

	.social-links {
		display: flex;
	}

	.social-btn {
		cursor: pointer;
		height: 40px;
		width: 40px;
		font-family: 'Titillium Web', sans-serif;
		color: #333;
		border-radius: 10px;
		box-shadow: 0px 10px 10px rgba(0, 0, 0, 0.1);
		background: white;
		margin: 5px;
		transition: 1s;
		border: 1px solid #eee;
	}

	.social-btn span {
		width: 0px;
		overflow: hidden;
		transition: 1s;
		text-align: center;
	}

	.social-btn:hover {
		width: 100px;
		border-radius: 5px;
	}

	.social-btn:hover span {
		padding: 2px;
		width: max-content;
	}

	.editdeleteicon {
		font-size: 25px;
		color: #9fbd38;
	}


	[data-jplist-group] {
		display: flex;
		font-size: 16px;
		flex-wrap: nowrap;
		flex-direction: column;
		align-items: center;
		width: 100%;
		box-sizing: border-box;
	}

	[data-jplist-item] {
		border-bottom: 1px dotted #ccc;
		width: 100%;
		text-align: center;
		padding: 10px 0;
		box-sizing: border-box;
	}

	.buttons {
		display: flex;
		justify-content: center;
		align-items: center;
		margin: 20px 0;
	}

	.inner-addon {
		position: relative;
	}

	.inner-addon .glyphicon {
		position: absolute;
		padding: 7px;
		pointer-events: none;
		right: 0px;
	}

	.jplist-pagination,
	.jplist-pagingprev,
	.jplist-pagingnext {
		display: inline-flex;
	}

	.jplist-pagination button {
		border-color: #9fbd38 !important;
		border: 1px;
		height: 20px !important;
		width: 20px !important;
		margin: 0px 5px;
		border-radius: 20px;

	}

	.navdash {
		display: flex;
		flex-direction: column;
		justify-content: center;
		align-items: center;
		position: relative;
		font-family: Hack, monospace;
		border: #dee2e6 solid 1px;
	}

	.navdash {
		margin: 25px;
		background: #fff;
		padding: 16px;
	}

	.navdash .menuItems {
		list-style: none;
		display: flex;
	}

	.navdash .menuItems li {
		margin-left: 20px;
	}

	.navdash .menuItems li a {
		text-decoration: none;
		color: #8f8f8f;
		font-size: 24px;
		font-weight: 400;
		transition: all 0.5s ease-in-out;
		position: relative;
		text-transform: uppercase;
	}

	/*.navdash .menuItems li a::before {
    content: attr(data-item);
    transition: 0.5s;
    color: #8254ff;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    width: 0;
    overflow: hidden;
  }
  .navdash .menuItems li a:hover::before {
    width: 100%;
    transition: all 0.5s ease-in-out;
  }
  
  .navdash .menuItems li a::before .active {
    width: 100%;
    transition: all 0.5s ease-in-out;
  }*/


	/*.navdash .menuItems li .active a::before {
    width: 100%;
    transition: all 0.5s ease-in-out;*/

	.menuItems a::before {
		position: absolute;
		content: '';
		/* border-radius: 3px; */
		width: 16px;
		height: 16px;
		transform: rotate(45deg);
		visibility: hidden;
		opacity: 0;
		transition: all .2s;
		left: 50%;
		margin-left: -8px;
		bottom: -30px;
		box-shadow: -1px -1px 1px 0px rgba(222, 226, 230, 0.9);
	}

	.menuItems li.active a::before {
		opacity: 1;
		visibility: visible;
		bottom: -38px;
		background-color: #f8f8f8;
	}

	.menuItems li.active a {
		color: #9c27b0;
	}

	#central-container .mainDash {
		/*background-color : #dadada;*/
		background-color: #f8f8f8;
	}

	.buttonListDash {
		margin: 10px;
		padding: 90px 35px 5px 10px;
	}

	#bodydashboard {
		padding: 5px 35px 10px 35px;
	}

	.titleparams {
		color: rgb(153, 102, 255);
		margin-top: 20px;
		margin-bottom: 10px;
	}

	.application {
		margin-top: 20px;
		margin-bottom: 10px;
		color: #000;
	}

	.list.survey-grid-dash-container {
		/*display: inline-block; */
	}

	.grid.survey-grid-dash-container {
		display: grid;
		grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
		grid-gap: 1em;
		margin: 1em 0 3em;
		justify-items: center;
		width: 100%;
	}

	#customHeader {
		background-color: #F8F8F8 !important;
		/*rgba(0,0,0,0.2)!important;*/
		/*padding-top: 100px !important;*/
		padding-top: 10px !important;
	}

	.coFormbody {
		background-color: #fff;
	}

	.survey-container {
		padding-top: 40px;
	}

	.social-btn {
		/* display: none; */
	}

	.ocecobadge-btn {
		display: flex;
	}

	li:hover .social-btn {
		display: flex;
	}

	li:hover .ocecobadge-btn {
		display: none;
	}

	.mainDash .wrap {
		display: none;
	}

	@media (max-width: 767px) {
		#bodydashboard {
			padding: 5px;
		}
	}

	/*New style*/
	.btn-group button.titleparams,
	.btn-group button.application {
		position: relative;
		margin: 0px;
		padding: 4px 6px;
		color: #fff;
		text-align: center;
		border: 0;
		border-bottom: 2px solid #3f4e58;
		cursor: pointer;
		-webkit-box-shadow: inset 0 -3px #fff;
		box-shadow: inset 0 -2px #fff;
		background-color: #9fbd38;
	}

	.btn-group button.titleparams:hover,
	.btn-group button.application:hover,
	.btn-group button.titleparams:active,
	.btn-group button.application:active {
		background: #3f4e58;
		border-bottom: 2px solid #7da53d;
		color: #fff;
	}

	span.text {
		display: none;
	}

	.survey-container .list {
		/* background-color: rgb(255, 255, 255);
          background-clip: border-box;
          border-width: 1px;
          border-style: solid;
          border-color: rgba(0, 0, 0, 0.125);
          border-image: initial;
          border-radius: 6px;
          box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%);
          padding: 0 10px 0 10px; */
		display: none;
	}

	.survey-container .tabulator.list {
		display: block !important;
	}

	.survey-container .tabulator.grid {
		display: none !important;
	}

	.d-none {
		display: none;
	}

	.compact-container {
		width: 100%;
		height: 167px;
		position: relative;
		border: 1px solid #80808000;
		box-shadow: 0 2px 5px 0 rgb(0 0 0 / 16%), 0 2px 10px 0 rgb(0 0 0 / 12%);
	}

	.padding-compact-coform {
		margin-bottom: 20px;
		display: flex;
		justify-content: center;
		align-items: center;
		flex-direction: column;
		padding: 110px 30px !important;
	}

	.ctnr-txtarea textarea {
		height: 40px !important;
	}

	.border-none {
		border: none;
	}

	.openAnswersComment {
		font-size: 58% !important;
		border: 0;
		padding: 2px 5px !important;
		min-width: 38px;
		width: 38px;
		max-width: 38px;
	}

	.openAnswersComment i.fa-2x {
		font-size: 2.4rem !important;
	}

	.unseen-proposition {
		box-shadow: inset 0 0 3px 3px #dcdc9a !important;
	}

	.btn-proposal-favorite {
		background-color: transparent !important;
	}

	#coform-list #dropdown_search {
		min-height: inherit !important;
	}

	#show-filters-lg .filter-xs-count {
		position: relative !important;
		float: right !important;
		margin-right: -2rem !important;
		margin-top: -1rem !important;
	}

	#filterContainerInside #activeFilters {
		display: none !important;
	}

	.depositor-image {
		display: inline-block;
		overflow: hidden;
		width: 25px;
		height: 25px;
		border-radius: 50%;
		vertical-align: middle;
	}

	.fa-2xx {
		font-size: 20px;
	}

	.survey-container .list .survey-item .w-100 {
		width: 100%;
	}

	.list .survey-item .col-schu-12,
	.list .survey-item .col-schu-4,
	.list .survey-item .col-schu-2 {
		position: relative;
		min-height: 1px;
		padding-right: 15px;
		padding-left: 15px;
	}

	@media screen and (max-width: 768px) {
		.filterXs.visible-xs.menu-xs-cplx.pull-left {
			position: relative;
			z-index: 11;
		}

		.filterXs .aap-filter {
			border: 1px solid #ccc;
			border-top: 0px;
			border-bottom: 0px;
			height: 45px !important;
			line-height: 35px;
			margin-top: 0px;
			border-radius: 0px;
			width: 100%;
		}

		#menuTopLeft .aap-filter .visible-xs .showHide-filters-xs,
		#menuTopLeft #show-list-xs,
		.filterXs .aap-filter .visible-xs .showHide-filters-xs {
			border: none !important;
			background-color: transparent;
			font-size: 2.5rem;
			margin: 2px;
		}

		#menuTopLeft #show-filters-xs .filter-xs-count,
		.filterXs #show-filters-xs .filter-xs-count {
			right: -3rem !important;
		}

		.filterXs .count-result-xs.visible-xs {
			text-align: start;
		}

		#menuTopLeft .aap-filter .visible-xs .searchBarInMenu.pull-right,
		.filterXs .aap-filter .visible-xs .searchBarInMenu.pull-right {
			display: none !important;
		}

		#menuTopLeft .aap-filter,
		.filterXs .aap-filter {
			position: unset;
			float: left;
			padding-left: 4px;
		}

		#menuTopLeft #filterContainerInside,
		.filterXs #filterContainerInside {
			left: -18rem;
			right: 0px;
			top: 6vh;
			display: block;
			background-color: rgb(255, 255, 255);
			position: absolute;
			max-height: 60vh;
			min-height: unset !important;
			width: 98vw;
			padding: 10px;
			overflow-y: auto;
			box-shadow: 0 0 10px rgba(0, 0, 0, .2);
		}

		.filterXs .btn.saveActiveFilter,
		.filterXs .accordion-row {
			border: none;
		}
	}

	.accordion-row {
		font-size: 12px !important;
		background: none !important;
		padding: 5px !important;
		border: none !important;
		line-height: 1.4 !important;
		transition: all 0.3s;
		text-align: left;
	}

	.accordion-row:hover {
		background-color: #70b73a !important;
		color: white;
		font-weight: bold;
	}

	#menu-top-btn-group #filterContainerL .showHide-filters-xs {
		width: 24px;
		margin-right: 5px;
		background-color: transparent;
	}

	.list .survey-item .col-schu-4 {
		width: 33.33333333%;
		float: left;
	}

	.list .survey-item .col-schu-2 {
		width: 16.66666667%;
	}

	.list .survey-item .schu-text-center {
		text-align: center;
	}

	.list .survey-item .w-20 {
		width: 20%;
	}

	.list .survey-item .schu-nowrap {
		white-space: nowrap;
	}

	.list .survey-item .schu-content-center {
		display: flex;
		justify-content: center;
	}

	.codate-result .panel-heading {
		padding: 10px 15px 0;
		border-bottom: none !important;
		border-top-left-radius: 3px;
		border-top-right-radius: 3px;
	}

	.d-none {
		display: none;
	}
</style>
<?php
HtmlHelper::registerCssAndScriptsFiles(array(
	'/css/tabulator.min.css',
	'/js/tabulator.min.js',
), Yii::app()->getModule(Survey::MODULE)->getAssetsUrl());
HtmlHelper::registerCssAndScriptsFiles([
	//    "/js/appelAProjet/aap.js",
	//    "/css/aap/aap.css"
], Yii::app()->getModule('costum')->assetsUrl);
?>

<?php
$dossier = $allanswers["dossier"];
$titlevalue = "";
$titleparams = [];

$answerStateOptionColor = [
	"1" => "encours",

	"2" => "aban",

	"0" => "term"
];

$badgelabel = [
	"progress"     => "En cours",
	"vote"         => "En evaluation",
	"finance"      => "En financement",
	"call"         => "Appel à participation",
	"newaction"    => "nouvelle proposition",
	"projectstate" => "En projet",
	"prevalided"   => "Pré validé",
	"validproject" => "Projet validé",
	"voted"        => "Voté"


];

foreach ($allanswers["titleparams"] as $key => $value) {
	$titleparams[$value["id"]] = $value["title"];
	if ($value["id"] == $allanswers["selectedtitleparams"]) {
		$titlevalue = $value["id"];
	}
}

$opalContextId = null;
$opalContextType = null;
if ($contextId != null && $contextType != null) {
	$opalContextId = $contextId;
	$opalContextType = $contextType;
}
$allQuestions = array();
$formParams = PHDB::findOneById(Form::COLLECTION, $allanswers['parentFormId']);
$coDateParentFormId = isset($allanswers['parentFormId']) ? $allanswers['parentFormId'] : '';
$parentForm = PHDB::findOneById(Form::COLLECTION, $coDateParentFormId);
$formInputs = array();
if (isset($formParams) && isset($formParams['subForms'])) {
	foreach ($formParams['subForms'] as $i => $item) {
		$subFormQuest = PHDB::find(Form::COLLECTION, array("id" => $item));
		if (isset($subFormQuest)) {
			if (!empty($subFormQuest) && isset($subFormQuest["_id"])) {
				$formInputs[(string) $subFormQuest["_id"]] = $subFormQuest;
			}
			$allQuestions[$item] = $subFormQuest;
		}
	}
}
$queryParams = Yii::$app->request->getQueryParams();
$listView = isset($queryParams['view']) ? $queryParams['view'] : 'compact';
$el = null;
$el_slug = @$el_slug;
if (!empty($el_slug)) {
	$el = Slug::getElementBySlug($el_slug, array("name", "type", "collection", "profilImageUrl", "slug", "oceco", "links"));
}

echo $this->renderPartial("survey.views.tpls.answers.header", [
	"parentFormId"    => $allanswers['parentFormId'],
	"adminRight" => $adminRight,
	"opalContextId" => $opalContextId,
	"opalContextType" => $opalContextType
],                        true);
?>

<div id="mainDash" class="mainDash">

	<div class="row buttonListDash">
		<?php
		$allInputs = array();
		$indexFound = -1;
		$indexMieuxVoter = -1;
		$indexTabCommun = -1;
		$indexTabCommunV2 = -1;
		if (isset($formParams) && isset($formParams['subForms'])) {
			foreach ($formParams['subForms'] as $i => $item) {
				if (isset($allQuestions[$item])) {
					$inputs = array_column(json_decode(json_encode($allQuestions[$item]), TRUE), 'inputs');
					if (isset($inputs[0])) {
						$inputKeys = array_keys($inputs[0]);
						foreach ($inputKeys as $k => $kVal) {
							$inputs[0][$kVal]['subFormId'] = $item;
						}
						$allInputs = array_merge($allInputs, array_combine($inputKeys, $inputs[0]));
					}
					// var_dump(array_search('tpls.forms.cplx.sondageDate', array_column(json_decode(json_encode($inputs[0]), TRUE), 'type')));
				}
			}
			$indexFound = array_search('tpls.forms.cplx.sondageDate', array_column(json_decode(json_encode($allInputs), TRUE), 'type'));
			$indexMieuxVoter = array_search('tpls.forms.cplx.tpls.forms.cplx.mieuxVoter', array_column(json_decode(json_encode($allInputs), TRUE), 'type'));
			$indexTabCommun = array_search('tpls.forms.evaluation.commonTable', array_column(json_decode(json_encode($allInputs), TRUE), 'type'));
			$indexTabCommunV2 = array_search('tpls.forms.evaluation.commonTableV2', array_column(json_decode(json_encode($allInputs), TRUE), 'type'));
		}

		if ($indexTabCommun > -1 && isset(Yii::app()->session["userId"])) {
		?>
			<div class="col-xs-12 row codate-result" id="">
				<h3 class=""
					style="margin-left: 50px; text-transform: none; font-family: 'Lato', sans-serif !important;"><?php echo Yii::t("survey", "All results"); ?>
					Tableau commun</h3>
				<div class="panel with-nav-tabs panel-success">
					<?php
					$panelHead = '<div class="panel-heading">' .
						'<ul class="nav nav-tabs">';
					$panelBody = '<div class="panel-body" style="max-height: 620px; overflow-y: auto">' .
						'<div class="tab-content">';
					$indexCount = 0;
					foreach ($allInputs as $inputKey => $inputValue) {
						if ($inputValue['type'] == 'tpls.forms.evaluation.commonTable') {
							$divId = uniqid();
							$classActive = $indexCount == 0 ? "in active" : "";
							$panelHead .= '<li class="changeTab ' . $classActive . '"><a href="#' . $divId . '" data-toggle="tab">' . $inputValue['label'] . '</a></li>';
							$communTabParams = [
								"key"        => $inputKey,
								"parentForm" => $formParams,
								"kunik"      => 'commonTable' . $inputKey,
								"resultMode" => true,
								"el"         => $el ? $el : (isset($elBySlug) ? $elBySlug : null),
								"mode"       => 'w',
								"form"       => isset($allQuestions[$inputValue['subFormId']]) ? array_values($allQuestions[$inputValue['subFormId']])[0] : null
							];
							$panelBody .= '
                      <div class=" fade d-none ' . $classActive . ' tabAllResult table-responsive" id="' . $divId . '">
                        <button type="button" class="btn btn-sm btn-link codate scroll-left hidden-print disabled" title="Faire défiler à gauche" data-id="' . $divId . '" aria-hidden="true">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </button>
                        <button type="button" class="btn  btn-sm btn-link codate scroll-right hidden-print" title="Faire défiler à droite" data-id="' . $divId . '" aria-hidden="true">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </button>
                        ' . $this->renderPartial("survey.views.tpls.forms.evaluation.commonTable", $communTabParams) .
								'</div>';
						}
						$indexCount++;
					}
					$panelHead .= '</ul>' .
						'</div>';
					$panelBody .= '</div>' .
						'</div>';
					echo $panelHead;
					echo $panelBody;
					?>
				</div>
			</div>
		<?php
		}

		if ($indexTabCommunV2 > -1 && isset(Yii::app()->session["userId"])) {
		?>
			<div class="col-xs-12 row codate-result" id="">
				<h3 class=""
					style="margin-left: 50px; text-transform: none; font-family: 'Lato', sans-serif !important;"><?php echo Yii::t("survey", "All results"); ?>
					Tableau commun</h3>
				<div class="panel with-nav-tabs panel-success">
					<?php
					$panelHead = '<div class="panel-heading">' .
						'<ul class="nav nav-tabs">';
					$panelBody = '<div class="panel-body" style="max-height: 620px; overflow-y: auto">' .
						'<div class="tab-content">';
					$indexCount = 0;
					foreach ($allInputs as $inputKey => $inputValue) {
						if ($inputValue['type'] == 'tpls.forms.evaluation.commonTableV2') {
							$divId = uniqid();
							$classActive = $indexCount == 0 ? "in active" : "";
							$panelHead .= '<li class="changeTab ' . $classActive . '"><a href="#' . $divId . '" data-toggle="tab">' . $inputValue['label'] . '</a></li>';
							$communTabParams = [
								"key"        => $inputKey,
								"parentForm" => $formParams,
								"kunik"      => 'commonTableV2' . $inputKey,
								"resultMode" => true,
								"el"         => $el ? $el : (isset($elBySlug) ? $elBySlug : null),
								"mode"       => 'w',
								"form"       => isset($allQuestions[$inputValue['subFormId']]) ? array_values($allQuestions[$inputValue['subFormId']])[0] : null
							];
							$panelBody .= '
                    <div class=" fade d-none ' . $classActive . ' tabAllResult table-responsive" id="' . $divId . '">
                      <button type="button" class="btn btn-sm btn-link codate scroll-left hidden-print disabled" title="Faire défiler à gauche" data-id="' . $divId . '" aria-hidden="true">
                          <span class="glyphicon glyphicon-chevron-left"></span>
                      </button>
                      <button type="button" class="btn  btn-sm btn-link codate scroll-right hidden-print" title="Faire défiler à droite" data-id="' . $divId . '" aria-hidden="true">
                          <span class="glyphicon glyphicon-chevron-right"></span>
                      </button>
                      ' . $this->renderPartial("survey.views.tpls.forms.evaluation.commonTableV2", $communTabParams) .
								'</div>';
						}
						$indexCount++;
					}
					$panelHead .= '</ul>' .
						'</div>';
					$panelBody .= '</div>' .
						'</div>';
					echo $panelHead;
					echo $panelBody;
					?>
				</div>
			</div>
		<?php
		}

		if (($indexFound > -1 || $indexMieuxVoter > -1) && isset(Yii::app()->session["userId"])) {
		?>
			<div class="col-xs-12 row codate-result" id="">
				<h3 class=""
					style="margin-left: 50px; text-transform: none; font-family: 'Lato', sans-serif !important;"><?php echo Yii::t("survey", "All results"); ?>
					CODate <?php echo Yii::t("common", "and") ?> Vote Majoritaire</h3>
				<div class="panel with-nav-tabs panel-success">
					<?php
					$indexCount = 0;
					$countInput = 0;
					$panelHead = '<div class="panel-heading">' .
						'<ul class="nav nav-tabs">';
					$panelBody = '<div class="panel-body" style="max-height: 620px">' .
						'<div class="tab-content">';

					foreach ($allInputs as $inputKey => $inputValue) {
						switch ($inputValue['type']) {
								// case 'tpls.forms.cplx.scoreCheckboxInput':
								// case 'tpls.forms.cplx.scoreInput':
							case 'tpls.forms.cplx.radioNew':
							case 'tpls.forms.cplx.checkboxNew':
								$countInput++;
								break;
							default:
						}
					}

					foreach ($allInputs as $inputKey => $inputValue) {
						$divId = uniqid();
						$classActive = $indexCount == 0 ? "in active" : "";
						if ($inputValue['type'] == 'tpls.forms.cplx.sondageDate') {
							$panelHead .= '<li class="changeTab ' . $classActive . '"><a href="#' . $divId . '" data-toggle="tab">' . $inputValue['label'] . ' </a></li>';
							if ($countInput > 0) {
								$panelHead .= '<li class="changeTab ' . $classActive . '"><a href="#score' . $divId . '" data-toggle="tab">Scores </a></li>';
							}
							$paramsData = [
								"alldates"      => new stdClass,
								"eventduration" => ''
							];
							$coDateKunik = 'sondageDate' . $inputKey;

							if (isset($parentForm["params"][$coDateKunik])) {
								if (isset($parentForm["params"][$coDateKunik]["alldates"])) {
									$paramsData["alldates"] = $parentForm["params"][$coDateKunik]["alldates"];
								}
								if (isset($parentForm["params"][$coDateKunik]["eventduration"])) $paramsData["eventduration"] = $parentForm["params"][$coDateKunik]["eventduration"];
							}
							$showTableScore = ($countInput > 0) ? "score" : "";
							$panelBody .= '
                    <div class=" fade d-none ' . $classActive . ' codate tabAllResult table-responsive" id="' . $divId . '">
                      <button type="button" class="btn btn-sm btn-link codate scroll-left hidden-print disabled" title="Faire défiler à gauche" data-id="' . $divId . '" aria-hidden="true">
                          <span class="glyphicon glyphicon-chevron-left"></span>
                      </button>
                      <button type="button" class="btn  btn-sm btn-link codate scroll-right hidden-print" title="Faire défiler à droite" data-id="' . $divId . '" aria-hidden="true">
                          <span class="glyphicon glyphicon-chevron-right"></span>
                      </button>
                      ' . Answer::ResultCoDate($formParams, $coDateKunik, $inputKey, $inputValue, $paramsData, $adminRight) .
								'</div>';

							// $panelBody .= '
							// <div class=" fade d-none '.$classActive.' codate tabAllResult table-responsive" id="score'.$divId.'">
							//   '. Answer::ResultScore($this ,$formParams , $showTableScore).
							// '</div>';
						} else if ($inputValue['type'] == 'tpls.forms.cplx.mieuxVoter') {
							$mieuxVoterKunik = 'mieuxVoter' . $inputKey;
							$paramsDataMieuxVoter = array();
							if (isset($parentForm["params"][$mieuxVoterKunik])) {
								$paramsDataMieuxVoter = $parentForm["params"][$mieuxVoterKunik];
							}
							if (!empty($paramsDataMieuxVoter)) {
								$divId = uniqid();
								$classActive = $indexCount == 0 ? "in active" : "";
								$panelHead .= '<li class="changeTab ' . $classActive . '"><a href="#' . $divId . '" data-toggle="tab">' . $inputValue['label'] . '</a></li>';
								$paramsDataMieuxVoter["global"]["mentions"] = [
									"0" => "Excellent",
									"1" => "Très bien",
									"2" => "Bien",
									"3" => "Assez bien",
									"4" => "Passable",
									"5" => "Insuffisant",
									"6" => "À rejeter"
								];
								$panelBody .= '
                      <div class=" fade d-none ' . $classActive . ' tabAllResult table-responsive" id="' . $divId . '">
                        <button type="button" class="btn btn-sm btn-link codate scroll-left hidden-print disabled" title="Faire défiler à gauche" data-id="' . $divId . '" aria-hidden="true">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </button>
                        <button type="button" class="btn  btn-sm btn-link codate scroll-right hidden-print" title="Faire défiler à droite" data-id="' . $divId . '" aria-hidden="true">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </button>
                        ' . Answer::ResultVoteMajoritaire($formParams, $inputValue['subFormId'], $inputKey, $paramsDataMieuxVoter["global"]) .
									'</div>';
							}
						}
						$indexCount++;
					}
					$panelHead .= '</ul>' .
						'</div>';
					$panelBody .= '</div>' .
						'</div>';
					echo $panelHead;
					echo $panelBody;
					?>
				</div>
			</div>
		<?php
		}
		?>
		<h2 class="pull-left"
			style="margin-left: 50px; text-transform: none; font-family: 'Lato', sans-serif !important;"> <?php echo Yii::t('survey', 'List of') ?> <?php echo $allanswers["dossier"]; ?>
			<span style=" padding-left: 5px; font-size: 20px; color: grey"> </span>
		</h2>


		<div class="btn-group pull-right" id="btn-actiongroup" style="margin-top: 20px; margin-bottom: 10px;">
			<?php if ($adminRight) { ?>
				<button class="btn btn-outline-dark titleparams"><i class="fa fa-cog"></i> <?php echo Yii::t('common', 'Settings') ?> </button>
			<?php
			}
			if ($canAddAnswer || $adminRight) {
				if ($contextId != null && $contextType != null) {
					$btndatacontext = " data-contextid='" . $contextId . "' data-contexttype='" . $contextType . "' data-type='openform'";
				} else {
					$btndatacontext = "";
				}
			?>
				<button class="btn btn-outline-dark application addAnswer" data-id='<?php echo $allanswers['parentFormId'] ?>'
					data-parentformid='<?php echo $allanswers['parentFormId'] ?>' <?php echo $btndatacontext ?>><i
						class="fa fa-plus"></i> <?php echo Yii::t('common', 'Add') ?> </button>
				<button class="btn btn-outline-dark application" data-toggle="modal" data-target="#csv-exportation"><i class="fa fa-file"></i>
					Exporter en CSV
				</button>
			<?php } ?>
		</div>
	</div>
	<?= $this->renderPartial("survey.views.custom.exportation-csv.coform", ["form" => $allanswers['parentFormId']]) ?>


	<div class="" id="bodydashboard">

		<button class="btn btn-link mobileShow" data-toggle="collapse" data-target="#displayOptions">
			<?php echo Yii::t('survey', 'Display options') ?>
			<ul class="d-flex mt-2" style="list-style: none;">
				<li class="btn-breadcrumb" data-toggle="tooltip" data-placement="bottom" data-original-title="3 par ligne">
					<a href="javascript:;" class="change-list-view-coform margin-right-5" data-action="compact"><i class="fa fa-th"
							aria-hidden="true"></i></a>
				</li>
				<!-- <li class="btn-breadcrumb" data-toggle="tooltip" data-placement="bottom" data-original-title="Liste">
                  <a href="javascript:;" class="change-list-view-coform margin-right-5" data-action="minimalist"><i class="fa fa-align-justify" aria-hidden="true"></i></a>
                </li>
                <li class="btn-breadcrumb" data-toggle="tooltip" data-placement="bottom" data-original-title="Complet">
                  <a href="javascript:;" class="change-list-view-coform margin-right-5" data-action="detailed"><i class="fa fa-picture-o" aria-hidden="true"></i></a>
                </li> -->
			</ul>
		</button>

		<ul class="d-flex mt-2" style="list-style: none; position: absolute; right: 6rem; z-index: 2">
			<li class="btn-breadcrumb" data-toggle="tooltip" data-placement="bottom" data-original-title="3 par ligne">
				<a href="javascript:;" class="change-list-view-coform margin-right-5" data-view="coform-compact"><i class="fa fa-th"
						aria-hidden="true"></i></a>
			</li>
			<!-- <li class="btn-breadcrumb" data-toggle="tooltip" data-placement="bottom" data-original-title="Tableau">
                <a href="javascript:;" class="change-list-view margin-right-5" data-view="table"><i class="fa fa-table" aria-hidden="true"></i></a>
            </li> -->
		</ul>
		<div id="displayOptions" class="jplist-panel " style="">

			<ul class="d-flex mt-2 d-none" style="list-style: none;">
				<li class="btn-breadcrumb" data-toggle="tooltip" data-placement="bottom" data-original-title="3 par ligne">
					<a href="javascript:;" class="change-list-view-coform margin-right-5" data-action="compact"><i class="fa fa-th" aria-hidden="true"></i></a>
				</li>
				<li class="btn-breadcrumb" data-toggle="tooltip" data-placement="bottom" data-original-title="Tableau">
					<a href="javascript:;" class="change-list-view margin-right-5" data-action="table"><i class="fa fa-table" aria-hidden="true"></i></a>
				</li>
			</ul>
			<div id="displayOptions" class="jplist-panel " style="">
				<div class="pull-left" style="display: inline-flex;">

				</div>
			</div>
			<div class="survey-container pt-0">
				<div id="answerList" class="tabulator grid"></div>
				<div class="surveys col-xs-12 survey-grid survey-grid-dash-container no-padding">
					<div id="coform-load"></div>
					<div id="coform-list" class="no-padding m-0">
						<div class="col-xs-12 no-padding row">
							<div class="col-md-12 no-padding">
								<div class="col-xs-3 menu-filters-lg filter" id="filterContainerAapOceco" style="display: none;">
									<div id='filterContainerL' class='aap-filter menu-verticale searchObjCSS'></div>
								</div>
								<div class="col-xs-12 col-md-12 filter-container">
									<button type="button" class="btn btn-default showHide-filters-xs btn-aap-tertiary hidden-xs" style="height: fit-content;"
										id="show-filters-lg">
										<span class="filter-xs-count topbar-badge badge animated bounceIn badge-tranparent badge-success"></span>
										<i class="fa fa-sliders"></i>
										<small class="pl-2"><?php echo Yii::t("form", "Filters") ?></small>
									</button>
									<div class='headerSearchContainerL no-padding col-xs-12'></div>
									<div class='bodySearchContainer margin-top-30'>
										<div class='col-xs-12 no-padding' id='dropdown_search'>
										</div>
										<div class='padding-top-20 col-xs-12 text-left footerSearchContainerL'></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<script type="text/javascript">
					cfbObj = formObj.init({});
					cfbObj.container = ".pageContent"
					cfbObj.events.dashboard(cfbObj);
					cfbObj.events.obs(cfbObj);
					const dataAllAnswers = <?php if (!empty($allanswers)) echo json_encode($allanswers);
											else echo json_encode(null) ?>;
					$(document).ready(function() {
						var ownParentForm = <?php echo json_encode($parentForm) ?>;
						const dataFormAnswers = <?php echo json_encode($formanswers); ?>;
						const adminRigth = <?php echo json_encode($adminRight); ?>;
						const canEditEachOtherAnswer = <?php echo json_encode($canEditEachotherAnswer); ?>;
						const userAnswer = false; //<?php /* echo $useranswer ? json_encode($useranswer) : false; */ ?>;
						var allQuestions = <?php echo json_encode($allQuestions); ?>;
						const formParams = <?php echo json_encode($formParams); ?>;
						const formInputs = <?php echo json_encode($formInputs); ?>

						var hash = location.hash.split("?")[1];
						var viewParams = {
							actionOf: userId,
							projectOf: "",
							actionByproject: false,
							actionByStatus: false,
							notSeen: false,
							view: "coform-compact"
						}

						$.fn.hasScrollWidthBar = function() {
							return this.get(0) ? this.get(0).scrollWidth > this.width() : false;
						}

						function checkIfCanScrollLeftOrRight(idContainer) {
							let $container = $(idContainer);
							if (($container.width() + $container.scrollLeft()) == $container.width()) {
								return 'tooleft'
							}
							if (($container.width() + $container.scrollLeft()) >= $container[0].scrollWidth) {
								return 'tooright'
							}
						}

						if (exists(hash)) {
							var arrHash = hash.split("&");
							$.each(arrHash, function(k, v) {
								k = v.split('=')[0];
								v = v.split("=")[1];
								if (k == "aapview") viewParams.view = v
							})
							if (location.hash.indexOf("actionOf=") != -1) viewParams.view = "kanban-personal-action";
						}
						setTimeout(() => {
							$.each($(".tabAllResult.table-responsive"), function() {
								if ($(this).hasScrollWidthBar()) {
									let that = this;
									$.each($(that).find("button.codate"), function() {
										$(this).show();
									})
								} else {
									let that = this;
									$.each($(that).find("button.codate"), function() {
										$(this).hide();
									})
								}
							})
						}, 756);
						$('a[data-toggle="tab"]').on("shown.bs.tab", function(event) {
							// event.stopImmediatePropagation();
							$.each($(".tabAllResult.table-responsive"), function() {
								if ($(this).hasScrollWidthBar()) {
									let that = this;
									$('.scroll-left').addClass('disabled');
									$('.scroll-right').removeClass('disabled');
									$.each($(that).find("button.codate"), function() {
										$(this).show();
									})
								} else {
									let that = this;
									$.each($(that).find("button.codate"), function() {
										$(this).hide();
									})
								}
							})
						});
						$('.scroll-right').on("click", function(event) {
							event.stopImmediatePropagation();
							const pos = $("#" + $(this).attr("data-id")).scrollLeft();
							const toScroll = $(window).width() < 576 ? 100 : 200;
							$("#" + $(this).attr("data-id")).animate({
								scrollLeft: pos + toScroll
							}, 800);
							$("#" + $(this).attr("data-id")).find(".scroll-left").removeClass("disabled");
							if (checkIfCanScrollLeftOrRight("#" + $(this).attr("data-id")) == 'tooright') $(this).addClass("disabled")
						});

						$('.scroll-left').on("click", function(event) {
							event.stopImmediatePropagation();
							const pos = $("#" + $(this).attr("data-id")).scrollLeft();
							const toScroll = $(window).width() < 576 ? 100 : 200;
							$("#" + $(this).attr("data-id")).animate({
								scrollLeft: pos - toScroll
							}, 800);
							$("#" + $(this).attr("data-id")).find(".scroll-right").removeClass("disabled");
							if (checkIfCanScrollLeftOrRight("#" + $(this).attr("data-id")) == 'tooleft') $(this).addClass("disabled")
						});
						$(".tabAllResult.table-responsive").on("scroll", function(event) {
							event.stopImmediatePropagation();
							if (checkIfCanScrollLeftOrRight("#" + $(this).attr("id")) == 'tooleft')
								$("#" + $(this).attr("id")).find(".scroll-left").addClass("disabled");
							else $("#" + $(this).attr("id")).find(".scroll-left").removeClass("disabled");
							if (checkIfCanScrollLeftOrRight("#" + $(this).attr("id")) == 'tooright')
								$("#" + $(this).attr("id")).find(".scroll-right").addClass("disabled");
							else $("#" + $(this).attr("id")).find(".scroll-right").removeClass("disabled")
						})
						$('.svgtoogler').on('click', function() {
							var toggle;
							toggle = $(this).addClass('active').attr('data-toggle');
							$(this).siblings('.svgtoogler').removeClass('active');
							$('.tabulator').removeClass('grid list').addClass(toggle)
							return $('.surveys').removeClass('grid list').addClass(toggle);
						});

						$('.change-list-view-coform').off().on('click', function(e) {
							var action = $(this).data('view');
							e.stopPropagation();
							viewParams.view = action
							if (location.hash.indexOf('?') != -1 && location.hash.indexOf('actionOf=') == -1) {
								location.hash = location.hash.split('?')[0]
								history.pushState(null, null, location.hash + ".aapview=" + action);
							} else {
								location.hash = location.hash.split('?')[0]
								history.pushState(null, null, location.hash + ".aapview=" + action);
							}
							urlCtrl.loadByHash(location.hash);
						});

						$.each(["coform-compact", "table"], function(k, v) {
							if (viewParams.view == v) {
								$(`.change-list-view-coform[data-view="${v}"]`).addClass("active-view");
							}

						})

						var params = {};
						//refactor-aap/co2/app/aap/context/coSindniSmarterre/formid/620a5e534f94bf26f34c5be8/aapview/detailed/page/proposal;
						ajaxPost("", baseUrl + `/co2/app/aap/context/${contextData.slug}/formid/${ formParams?._id?.$id }/aapview/detailed/page/proposal/isForAap/false`,
							null,
							function(res) {
								params = res;
								$('#coform-list').removeClass("d-none").addClass("d-flex");
								$("#coform-load").hide()
							}, null, null, {
								beforeSend: () => {
									$('#coform-list').removeClass("d-flex").addClass("d-none");
									$("#coform-load").html('<p class="text-center"><i class="fa fa-3x fa-spinner fa-spin"></i></p>')
								},
								async: false
							}
						);

						params.aapview = viewParams?.view ? viewParams.view : "coform-compact";
						params.answerId = null;
						params.canEdit = <?php echo json_encode($adminRight) ?>;
						params.canParticipate = <?php echo json_encode($canEditEachotherAnswer) ?> || <?php echo json_encode($adminRight) ?>;
						params.canSee = <?php echo json_encode($canReadEachOtherAnswer) ?>;
						params.context = contextData;
						params.form = formParams;
						params.inputs = formInputs;
						params.page = "proposal";
						params.pageDetail = {};
						aapObj = aapObj.init(params)

						function proposition_init_filters() {
							var filterPrms = aapObj.paramsFilters.proposal(aapObj, "#filterContainerL", false);
							aapObj.filterSearch.proposal = searchObj.init(filterPrms);
							aapObj.filterSearch.proposal.search.init(aapObj.filterSearch.proposal);
						}

						$("#dropdown_search").empty();
						proposition_init_filters();
						aapObj.events.common(aapObj);
						aapObj.common.switchFilter()
						// var fObjj = formObj.init({});
						// fObjj.container = "block-container-callForProjects60dd6a3860e5d2695e342d2b";
						// fObjj.events.form(fObjj);
						// mylog.log("check fObj", fObjj);

						toastr.options.closeButton = true;
						toastr.options.preventDuplicates = true;
						toastr.options.progressBar = true;
						var answerInfo = {
							id: '',
							form: ''
						}
						let columns = [],
							tableData = [];

						$(".createEvent").on("click", function(event) {
							// dyFObj.openForm('event')
							let self = this;
							event.stopImmediatePropagation();
							const addHours = function(hours, date = new Date()) {
								const dateCopy = new Date(date.getTime());
								dateCopy.setTime(dateCopy.getTime() + hours * 60 * 60 * 1000);
								return dateCopy != "Invalid Date" ? dateCopy.toLocaleString('fr', {
									day: 'numeric',
									month: 'numeric',
									year: 'numeric',
									hour: 'numeric',
									minute: 'numeric'
								}) : null;
							}
							let startDate = `${$(self).attr("data-date")} ${$(self).attr("data-time")}`;
							const [d, m, y] = $(self).attr("data-date").split("/");
							const sDate = new Date(`${m}-${d}-${y} ${$(self).attr("data-time")}`);
							let endDate = addHours($(self).attr("data-eventduration"), sDate); //`${$(self).attr("data-date")} 23:59`;
							const eDate = endDate.indexOf(":") > -1 ? endDate.substring(0, endDate.indexOf(":") - 2) : '';
							const eTime = endDate.indexOf(":") > -1 ? endDate.substring(endDate.indexOf(":") - 2) : '';
							bootbox.dialog({
								title: trad.confirm,
								message: `<span class='text-success bold'><i class='fa fa-info'></i> ${tradForm.wouldYouLikeToCreateTheEvent} ${$(self).attr("data-titleQuestion")}<br><span class="text-black">${trad.fromdate} ${$(self).attr("data-date")} ${trad.to} ${$(self).attr("data-time")} ${trad.until} ${eDate} ${trad.to} ${eTime}</span> </span>`,
								buttons: [{
										label: "Ok",
										className: "btn btn-primary pull-left",
										callback: function() {
											const allParent = ownParentForm["parent"] ? ownParentForm["parent"] : undefined;
											const oneParent = allParent ? {
												[Object.keys(allParent)[0]]: allParent[[Object.keys(allParent)[0]]]
											} : null
											const [day, month, year] = $(self).attr("data-date") ? $(self).attr("data-date").split("/") : null;
											let dataToSend = {
												id: $(self).attr("data-id"),
												name: $(self).attr("data-titleQuestion"),
												type: 'workshop',
												organizer: oneParent,
												startDate: startDate,
												endDate: endDate,
												collection: "events",
												key: "event",
												public: true,
												timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
												preferences: {
													isOpenData: true,
													isOpenEdition: true
												},
											};
											// mylog.log("create event to send", dataToSend);

											ajaxPost(
												null,
												baseUrl + "/co2/search/globalautocomplete", {
													name: $(self).attr("data-titleQuestion"),
													searchType: 'events',
													indexMin: 0,
													indexMax: 50
												},
												function(res) {
													if (res) {
														if (Object.values(res.results).length > 0) {
															toastr.warning(trad.checkItemAlreadyExist);
														} else {
															ajaxPost(null, baseUrl + "/co2/element/save", dataToSend, function(data) {
																if (data.result) toastr.success(tradForm.modificationSave)
															}, "json");
														}
													}
												}
											)
											// ajaxPost(null, baseUrl+"/co2/element/save", dataToSend, function(data) {mylog.log(data)}, "json");
											// dataHelper.path2Value(dataToSend, function() {

											// });
											// mylog.log("data create event to send", dataToSend)
										}
									},
									{
										label: "Annuler",
										className: "btn btn-default pull-left",
										callback: function() {}
									}
								]
							});
						})
					});

					$('document').ready(function() {
						$('#bodydashboard').jplist({
							itemsBox: '.survey-grid',
							itemPath: '.survey-item',
							panelPath: '.jplist-panel',
							effect: 'fade'
						});

						$(".section .toggle").click(function() {
							var elem = $(this).text();
							if (elem == "Voir plus") {
								$(this).text("Lire moins");
								$(this).parent().find('.text').slideDown();
								$(this).parent().find('.sample-text').hide();
							} else {
								$(this).text("Voir plus");
								$(this).parent().find('.text').slideUp();
								$(this).parent().find('.sample-text').show();
							}
						});
					});

					$(document).ready(function() {
						$(".titleparams").off().on("click", function() {
							sectionDyfObservatory = {
								"jsonSchema": {
									"title": "Configuration des titres",
									"icon": "fa-cog",
									"text": "Sélectionnez la réponse d'un champ text pour réprésenter la réponse en tant que titre d'un element",
									"properties": {
										title: {
											inputType: "select",
											label: "Sélectionnez la réponse d'un champ text pour réprésenter la réponse en tant que titre d'un element",
											placeholder: "Sélectionnez la réponse d'un champ text pour réprésenter la réponse en tant que titre d'un element",
											noOrder: true,
											options: <?php echo json_encode($titleparams); ?>,
											rules: {
												required: true
											},
											value: "<?php echo $titlevalue; ?>"
										}
									},
									save: function() {
										const today = new Date();
										let dataToSend = {
											id: dataAllAnswers["parentFormId"],
											path: "params.inputForAnswerName",
											collection: <?php echo json_encode(Form::COLLECTION) ?>,
											value: {
												date: today.toLocaleString('fr', {
													day: 'numeric',
													month: 'numeric',
													year: 'numeric'
												})
											}
										}
										// tplCtx.id = dataAllAnswers["parentFormId"];
										/* tplCtx.path = "params.inputForAnswerName";
										 // tplCtx.collection = "<?php /* echo Form::COLLECTION */ ?>";
										 var today = new Date();
										 tplCtx.value = {
										 date: today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear()
										 }; */
										$.each(sectionDyfObservatory.jsonSchema.properties, function(k, val) {
											dataToSend.value[k] = $("#" + k).val();
										});

										mylog.log("save tplCtx", dataToSend);
										if (typeof dataToSend.value == "undefined") {
											toastr.error('value cannot be empty!');
										} else {
											dataHelper.path2Value(dataToSend, function(params) {
												location.reload();
											});
										}

									}
								}
							};

							dyFObj.openForm(sectionDyfObservatory);
						});
					});
					// jplist.init();
				</script>