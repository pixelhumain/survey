<style type="text/css">
    #transferbutton:disabled {
        cursor: not-allowed;
        pointer-events: all !important;
    }
</style>
<div>
    <div class="col-md-8 col-md-offset-2 ">
        <div class="form-group">
            <label for="">
                <h4>
                    Choisir formulaire de destination
                </h4>
            </label>
            <br/>
            <select name="" id="transferselect" class="form-control">
                <option value=""> choisir </option>

                <?php if (isset($formaap)){
                    foreach ($formaap as $fid => $faap){
                        ?>
                        <option value="<?php echo $fid ?>" data-answerid = "<?php echo $answerid?>" data-contextid = "<?php echo array_keys($faap["parent"])[0]?>" data-contexttype = "<?php echo $faap["parent"][array_keys($faap["parent"])[0]]["type"]?>" data-contextname = "<?php echo $faap["parent"][array_keys($faap["parent"])[0]]["name"]?>"> <?php echo $faap["name"] ?> ( <?php echo $faap["parent"][array_keys($faap["parent"])[0]]["name"]?> ) </option>
                        <?php
                    }
                }
                ?>

            </select>
            <small id="Help" class="form-text text-muted">

            </small>

        </div>
        <div id="">
            <button type="button" id="transferbutton" class="btn btn-primary" disabled ="true" >Transferer</button>
        </div>

    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        $('#transferselect').on('change' , function(){
           if ($(this).val() != ""){
               $('#transferbutton').prop('disabled', false);
           } else {
               $('#transferbutton').prop('disabled', true);
           }
        });

        $('#transferbutton').on('click' , function(){
            var answerid = $('#transferselect option:selected').data("answerid");
            var formid = $("#transferselect").val();

            var contextid = $('#transferselect option:selected').data("contextid");
            var contexttype = $('#transferselect option:selected').data("contexttype");
            var contextname = $('#transferselect option:selected').data("contextname");
            var answer1 = {
                collection : "answers",
                id : answerid,
            };

            var answer2 = {
                collection : "answers",
                id : answerid,
            };

            answer1["path"] = "form";
            answer2["path"] = "context";

            answer1["value"] = formid;

            answer2["value"] = {};
            answer2["value"][contextid] = {
                type : contexttype,
                name : contextname
            };

            console.log("azee", answer1 , answer2);

            dataHelper.path2Value( answer1 , function(params) {
                dataHelper.path2Value( answer2 , function(params) {
                    toastr.success("Proposition transferé avec succès !");
                    urlCtrl.loadByHash(location.hash);
                } );
            } );

        });
    });
</script>
