<?php
$cssJS = array(
    // '/plugins/gridstack/css/gridstack.min.css',
    // '/plugins/gridstack/js/gridstack.js',
    // '/plugins/gridstack/js/gridstack.jQueryUI.min.js'

);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);


HtmlHelper::registerCssAndScriptsFiles(array(
    '/css/graphbuilder.css',
    '/js/form.js',
    '/js/dashboard.js',
    '/css/aap/aapGlobalDashboard.css'
), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );

if (isset($this->costum["slug"])){
    $slug = $this->costum["slug"];
}
$el = null;
?>

<style>
    .btn_cont {
        width: 550px;
        height: 60px;
        position: relative;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -1%);
        padding-top: 20px;
    }
    .btn_cont_right {
        width: 550px;
        height: 60px;
        position: relative;
        left: 20px;
        top: 50%;
        padding-top: 20px;
    }
    .edit_mode {
        float: left;
        width: 135px;
        margin-top: 10px;
    }
    .edit_mode label {
        float: left;
    }
    .edit_mode .toggle_div {
        float: right;
    }
    .move_all {
        float: left;
    }
    .clear_grid, .ser_grid {
        float: right;
        margin: 0 5px 0 5px;
    }

</style>

<?php
    $allData = json_decode(json_encode($allData));

if ($hv_parent)
{
    ?>
    <div class="col-md-12 margin-top-5">
        <button type="button" class="btn aapgoto" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_parent_slug; ?>.formid.<?php echo $el_parent_form_id ?>.page.multidashboard">Multidashboard</button>
    </div>
    <?php
}
elseif (isset($el["el"]["oceco"]["subOrganization"]) && filter_var(    $el["el"]["oceco"]["subOrganization"], FILTER_VALIDATE_BOOLEAN)  && !$hv_parent)
{
    ?>
    <div class="col-md-12 margin-top-5">
        <button type="button" class="btn aapgoto" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.multidashboard">Multidashboard</button>
    </div>
    <?php
}
?>

<div class="col-md-12 aapdashboard">
    <div id="ocecofiltercontainer" class="searchObjCSS menuFilters menuFilters-vertical col-xs-12 bg-light text-center"></div>
    <div class="col-md-2 col-sm-12 col-xs-12 aaptilescont">
        <div class="col-md-12 col-sm-12 col-xs-12 aaptiles1">
            <span class="aaptilestitles1"> <h5> Action en retard </h5> </span>
            <div class="aapinnertiles1 col-md-12 " style="">
                <div class="col-md-12 aapicontiles ocecotitle">

                </div>
                <div class="col-md-12 aapnumbertiles " id="totalLateProject">
                    0
                </div>
                <div class="col-md-12 aapnumbertiles " id="">
                    <button class="btn btn-sm voirbtn aapevent aapdashcallback" data-aapdashcallback="viewinfo" data-containerid="totalLateProject">voir</button>
                </div>

            </div>
        </div>
    </div>
    <!-- Proposition----------------------- -->
    <div class="col-md-8 col-sm-12 col-xs-12 aaptilescont">
        <div class="col-md-12 col-sm-12 col-xs-12 aaptiles">
            <div class=" col-md-12 aaptilestitles2">
                <h4>Propositions </h4>
            </div>
            <div class=" col-md-2 col-md-offset-1" style="">
                <div class="col-md-12 aapnumbertiles" id="totalProp">
                    0
                </div>
                <div class=" aaptilestitles1">Au total</div>
            </div>
            <div class=" col-md-2 col-md-offset-1" style="">

                <div class="col-md-12 aapnumbertiles" id="inVoteProp">
                    0
                </div>
                <div class=" aaptilestitles1">à voter</div>
            </div>
            <div class=" col-md-2 col-md-offset-1" style="">

                <div class="col-md-12 aapnumbertiles" id="toFinanceProp">
                    0
                </div>
                <div class="aaptilestitles1">à financer</div>
            </div>
            <div class=" col-md-2 col-md-offset-1" style="">

                <div class="col-md-12 aapnumbertiles" id="inProgressProp">
                    0
                </div>
                <div class="aaptilestitles1">En cours</div>
            </div>
            <!--<span class="aaptilestitles"> <i class="fa fa-tag"></i> Action en retard </span>-->
            <!--<hr role="separator" aria-orientation="horizontal" class="v-divider theme--light">
            <div class="center">
                <button class="center btn btn-sm btn-secondary aapdashcallback" data-aapdashcallback="viewinfo" data-containerid="totalLateProject"> <i class="fa fa-info-circle"></i> details </button>
            </div>-->
        </div>
    </div>
    <!-- end propositions -->
    <div class="col-md-2 col-sm-12 col-xs-12 aaptilescont">
        <div class="col-md-12 col-sm-12 col-xs-12 aaptiles1">
            <span class="aaptilestitles1"> <h5> Action proche du deadline </h5> </span>
            <div class="aapinnertiles1 col-md-12 " style="">
                <div class="col-md-12 aapicontiles ocecotitle">

                </div>
                <div class="col-md-12 aapnumbertiles " id="totalNearProject">
                    0
                </div>
                <div class="col-md-12 aapnumbertiles " id="">
                    <button class="btn btn-sm voirbtn aapevent aapdashcallback" data-aapdashcallback="viewinfo" data-containerid="totalNearProject">voir</button><button class="btn btn-sm cogvoirbtn aapevent aapdashcallback" data-aapdashcallback="configviewinfo" data-containerid="totalNearProject"><i class="fa fa-cog"></i></button>
                </div>

            </div>
        </div>
    </div>
</div>

<!--------------------------- Projects----------------- -->
<div class="col-md-12 aapdashboard">
    <div class="col-md-12 col-sm-12 col-xs-12 aaptilescont">
        <div class="col-md-12 col-sm-12 col-xs-12 aaptiles">
            <div class=" col-md-12 aaptilestitles2">
                <h4>Projets </h4>
            </div>
            <div class=" col-md-2 col-md-offset-1" style="">
                <div class="col-md-12 aapnumbertiles" id="totalProject">
                    0
                </div>
                <div class=" aaptilestitles1">Au total</div>
            </div>
            <div class=" col-md-2 col-md-offset-1" style="">
                <div class="col-md-12 aapnumbertiles" id="todoProject">
                    0
                </div>
                <div class=" aaptilestitles1">À faire</div>
            </div>
            <div class=" col-md-2 col-md-offset-1" style="">
                <div class="col-md-12 aapnumbertiles" id="doneProject">
                    0
                </div>
                <div class=" aaptilestitles1">Terminé</div>
            </div>
            <div class=" col-md-2 col-md-offset-1" style="">
                <div class="col-md-12 aapnumbertiles" id="inProgressProject">
                    0
                </div>
                <div class="aaptilestitles1">En cours</div>
            </div>
        </div>
    </div>
</div>
<!---------------------------End Projects-------------- -->

<!--------------------------- Actions----------------- -->
<div class="col-md-12 aapdashboard">
    <div class="col-md-12 col-sm-12 col-xs-12 aaptilescont">
        <div class="col-md-12 col-sm-12 col-xs-12 aaptiles">
            <div class=" col-md-12 aaptilestitles2">
                <h4>Actions </h4>
            </div>
            <div class=" col-md-2 col-md-offset-1" style="">
                <div class="col-md-12 aapnumbertiles" id="totalAction">
                    0
                </div>
                <div class=" aaptilestitles1">Au total</div>
            </div>
            <div class=" col-md-2 col-md-offset-1" style="">
                <div class="col-md-12 aapnumbertiles" id="todoAction">
                    0
                </div>
                <div class=" aaptilestitles1">À faire</div>
            </div>
            <div class=" col-md-2 col-md-offset-1" style="">
                <div class="col-md-12 aapnumbertiles" id="doneAction">
                    0
                </div>
                <div class=" aaptilestitles1">Terminé</div>
            </div>
            <div class=" col-md-2 col-md-offset-1" style="">
                <div class="col-md-12 aapnumbertiles" id="inProgressAction">
                    0
                </div>
                <div class="aaptilestitles1">En cours</div>
            </div>
        </div>
    </div>
</div>
<!---------------------------End Actions-------------- -->
<div class="col-md-12 aapdashboard">

    <div class="col-md-12 col-sm-12 col-xs-12 aaptilescont">
        <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
            <div class="aaptilestitles3 "> <h5> Flux du financement <!--<button class="btn btn-sm voirbtn aapevent aapdashcallback " data-aapdashcallback="viewinfo" data-containerid="totalLateProject">voir</button>--> </h5> </div>

            <div class="aapinnertiles3 col-md-12" id="financersankey" data-aapdashcallback="viewinfo" style="padding-top: 20px;">


            </div>


        </div>
    </div>
</div>

<div class="col-md-12 aapdashboard">
    <div class="col-md-12 col-sm-12 col-xs-12 aaptilescont">
        <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
            <span class="aaptilestitles3"> <h5> Tache en à faire par personne </h5> </span>

            <div class="aapinnertiles3 col-md-12" id="todotasksperperson" data-aapdashcallback="viewinfo" style="padding-top: 20px;">

            </div>

        </div>
    </div>
</div>

<div class="col-md-12 aapdashboard">
    <div class="col-md-12 col-sm-12 col-xs-12 aaptilescont">
        <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
            <span class="aaptilestitles3"> <h5> Tache en cours par personne </h5> </span>

            <div class="aapinnertiles3 col-md-12" id="inprogressTaskPerPerson" data-aapdashcallback="viewinfo" style="padding-top: 20px;">

            </div>

        </div>
    </div>
</div>

<div class="col-md-12 aapdashboard">
    <div class="col-md-6 col-sm-12 col-xs-12 aaptilescont">
        <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
            <span class="aaptilestitles3"> <h5> Taches par categorie </h5> </span>

            <div class="aapinnertiles3 col-md-12" id="graphtasks" data-aapdashcallback="viewinfo" style="padding-top: 20px;">

            </div>

        </div>
    </div>

</div>

<div class="col-md-12 aapdashboard">
    <div class="col-md-6 col-sm-12 col-xs-12 aaptilescont">
        <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
            <span class="aaptilestitles3"> <h5> Financement par type de travaux </h5> </span>

            <div class="aapinnertiles3 col-md-12" id="finpertype" style="padding-top: 20px;">

            </div>

        </div>
    </div>

    <div class="col-md-6 col-sm-12 col-xs-12 aaptilescont">
        <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
            <span class="aaptilestitles3"> <h5> Evolution des payement </h5> </span>

            <div class="aapinnertiles3 col-md-12" id="financerline" style="padding-top: 20px;">

            </div>

        </div>
    </div>

    <div class="col-md-6 aaptilescont">
        <div class="col-md-12 col-sm-12 col-xs-12 aaptiles">
            <div class="aapinnertiles col-md-12" id="" style="">
                <div class="ocecoval-item">

                    <div class="ocecoval-desc">
                        <span class="timelinetitle">Montant demandé à financer</span>
                        <p class="timelinetext" id="amounttofin">0</p>
                    </div>
                </div>

                <div class="ocecoval-item ">

                    <div class="ocecoval-desc">
                        <span class="timelinetitle">Montant Financer dépensé </span>
                        <p class="timelinetext" id="amountinfin">0</p>
                    </div>
                </div>

                <div class="ocecoval-item">

                    <div class="ocecoval-desc">
                        <span class="timelinetitle">Montant Financer en cours de réalisation </span>
                        <p class="timelinetext" id="amountfin">0</p>
                    </div>
                </div>
            </div>

            <!--<hr role="separator" aria-orientation="horizontal" class="v-divider theme--light">
            <div class="center">
                <button class="center btn btn-sm btn-secondary aapdashcallback" data-aapdashcallback="viewinfo" data-containerid="totalLateProject"> <i class="fa fa-info-circle"></i> details </button>
            </div>-->
        </div>
    </div>

</div>

<script type="text/javascript">
    var gridHeight;

    var grid = $('#grid_stack_cont').data('gridstack');

    function aapgraphcallback(graphkey, index){
        if (typeof $('#'+graphkey).data("aapdashcallback") != "undefined"){
            var aapdashcallback = $('#'+graphkey).data("aapdashcallback");
            ocecoform.tiles[graphkey][aapdashcallback](index);
        }
    }

    jQuery(document).ready(function() {
        $('.aapdashcallback').click(function(){
            var aapdashcontainerid = $(this).data("containerid");
            var aapdashcallback = $(this).data("aapdashcallback");
            ocecoform.tiles[aapdashcontainerid][aapdashcallback]();
        });

        $(".aapgoto").off().on("click", function(){
            if(isUserConnected == "unlogged")
                return $("#modalLogin").modal();
            mylog.log("azeee", $(this).data("url"));
            window.location.href = $(this).data("url");
            urlCtrl.loadByHash(location.hash);
        });
    });

    var standartcolors = [
        'rgba(46, 204, 113, 1)',
        'rgba(224, 224, 0, 1)',
        'rgba(221, 221, 221, 1)'
    ];

    var standartcolors = [
        'rgba(46, 204, 113, 1)',
        'rgba(224, 224, 0, 1)',
        'rgba(221, 221, 221, 1)'
    ];

    var bigstandartcolors = [
        // "#F7464A",
        'rgba(46, 204, 113, 1)',
        "#46BFBD",
        "#949FB1",
        "#4D5360",
    ];

    var defaultimgprofil = '<?php echo Yii::app()->getModule('co2')->assetsUrl.'/images/thumb/default_citoyens.png'; ?>';

    var groupe = ["Feature" , "Costume" , "Chef de projet" , "Data" , "Maintenance"];

    var multiorga = <?php echo (isset($multiorga) ? $multiorga : "false") ?>;

    var sousorga = <?php echo (isset($sousorga) ? json_encode($sousorga) : "{}" ); ?>;

    var smhtml = '<h1 class="text-center"></h1>' +
        '<style type="text/css">' +
        '</style>' +
        '<div class="col-xs-12 col-sm-10 col-sm-offset-1">' +
        '    <div class="no-padding col-xs-12 text-left headerSearchContainer"></div>' +
        '' +
        '    <div id="filterCMS"></div>' +
        '    <div id="appCmsCurrent">' +
        '    </div>' +
        '    <div class="no-padding col-xs-12 text-left footerSearchContainer smartgrid-slide-element"></div>' +
        '</div>';

    var fp = <?php echo json_encode($formparent) ?>;
    var parent = <?php echo json_encode($parent) ?>;

    var numberWithCommas = function(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")+" €";
    };

    var ocecoform = {
        allData : <?php echo json_encode($allData) ?> ,
        allDataAns : <?php echo json_encode($allDataAns) ?> ,
        project : <?php echo json_encode($project) ?>,
        actions : <?php echo json_encode($action) ?> ,
        contributors : <?php echo json_encode($contributors) ?> ,
        title : "<?php echo $title; ?>",
        tiles : {
            "graphtasks" : {
                type : "chart",
                totaltaskslate : [],
                totaltasksnotlate : [],
                totaltasksfinished : [],
                userindex : [],
                getandsetDatafunc : function(ocecoform){
                    var actionsM = ocecoform.dataProcessorFunction.ungroup(Object.values(ocecoform.actions));
                    var tasks = ocecoform.dataProcessorFunction.mergeArray(actionsM, "path", "tasks");

                    var nctasks = ocecoform.dataProcessorFunction.selectOccur(tasks, "null/false", "checked");

                    var ntlatetasks = [];
                    var latetasks = [];
                    var cktasks = [];

                    $.each(tasks, function(i, v){
                        var index = nctasks.indexOf(v);
                        if (index == -1) {
                            cktasks.push(v);
                        }
                    });

                    ocecoform.tiles.graphtasks.totaltasksfinished = cktasks;

                    $.each(nctasks , function(tsId, tsVl) {
                        if (ocecoform.dataProcessorFunction.countDays(ocecoform.dataProcessorFunction.convertDate(tsVl.endDate), new Date()) < 0) {
                            latetasks.push(tsVl);
                        }else {
                            ntlatetasks.push(tsVl);
                        }
                    });

                    ocecoform.tiles.graphtasks.totaltasksnotlate = ntlatetasks;
                    ocecoform.tiles.graphtasks.totaltaskslate = latetasks;

                    var todata = [cktasks.length, ntlatetasks.length, latetasks.length];
                    var tolabel = ["Completé", "dans les temps", "En retard"];

                    ocecoform.tiles["graphtasks"].values.data = todata;
                    ocecoform.tiles["graphtasks"].values.labels = tolabel;

                },
                values : {
                    id : "graphtasks",
                    data : [0, 0, 0],
                    label : 'nombre de taches',
                    labels : ["","",""],
                    g : 'graph.views.co.ocecoform.pie',
                    colors : bigstandartcolors
                },
                viewinfo :function (ind) {
                    var totaltasksuserclick = [ocecoform.tiles.graphtasks.totaltasksfinished , ocecoform.tiles.graphtasks.totaltasksnotlate, ocecoform.tiles.graphtasks.totaltaskslate ];
                    var tableHeadvf = {
                        "task" : "tâche",
                        "credits" : "credits",
                        "createdAt" : "date de creation"
                    };
                    ocecoform.getModal(ocecoform.createTable(totaltasksuserclick[ind], tableHeadvf));

                }
            },
            "totalLateProject" : {
                type : "html",
                getandsetDatafunc : function(ocecoform){
                    var actionsM = ocecoform.dataProcessorFunction.ungroup(Object.values(ocecoform.actions));

                    var prlatelenght = 0;

                    var prlatelist = [];

                    $.each(actionsM , function(tsId, tsVl){

                        var acttasks = ocecoform.dataProcessorFunction.selectOccur(tsVl.tasks, "null/false", "checked");

                        var acttasks2 = ocecoform.dataProcessorFunction.selectOccur(acttasks, "outdate", "endDate");
                        if (acttasks2.length != 0){
                            prlatelenght++;
                            tsVl.tasks = acttasks2;
                            prlatelist.push(tsVl);
                        }

                    });

                    ocecoform.tiles["totalLateProject"].values = prlatelenght;
                    ocecoform.tiles["totalLateProject"].prlatelenght = prlatelist;
                },
                values : 0,
                prlatelenght : [],
                viewinfo : function () {
                    // ocecoform.getTableModal(ocecoform.tiles["totalLateProject"].prlatelenght);
                    var tableHeadvf = {
                        "credits" : "credits",
                        "name" : "nom",
                        "tasks" : "taches"
                    };
                    //ocecoform.getModal(ocecoform.createTable(ocecoform.tiles.totalLateProject.prlatelenght, tableHeadvf));
                    //smallMenu.openAjaxHTML(baseUrl+'/co2/aap/getviewbypath/path/costum.views.custom.appelAProjet.templateList');
                    //ocecoform.getModal();
                    smallMenu.open(ocecoform.compileDataHtml(ocecoform.tiles.totalLateProject.prlatelenght, "action"));

                }
            },
            "totalProp" : {
                type : "html",
                getandsetDatafunc : function(ocecoform){
                    ocecoform.tiles["totalProp"].values = Object.values(ocecoform.allData).length;
                },
                values : 0
            },
            "inVoteProp" : {
                type : "html",
                getandsetDatafunc : function(ocecoform){
                    ocecoform.tiles["inVoteProp"].values = ocecoform.dataProcessorFunction.countOccur(ocecoform.dataProcessorFunction.mergeArray(Object.values(ocecoform.allData), "path", "status"),"vote");
                },
                values : 0
            },
            "toFinanceProp" : {
                type : "html",
                getandsetDatafunc : function(ocecoform){
                    ocecoform.tiles["toFinanceProp"].values = ocecoform.dataProcessorFunction.countOccur(ocecoform.dataProcessorFunction.mergeArray(Object.values(ocecoform.allData), "path", "status"),"finance");
                },
                values : 0
            },
            "inProgressProp" : {
                type : "html",
                getandsetDatafunc : function(ocecoform){
                    ocecoform.tiles["inProgressProp"].values = ocecoform.dataProcessorFunction.countOccur(ocecoform.dataProcessorFunction.mergeArray(Object.values(ocecoform.allData), "path", "status"),"progress");
                },
                values : 0
            },
            "totalProject" : {
                type : "html",
                getandsetDatafunc : function(ocecoform){
                    ocecoform.tiles["totalProject"].values = Object.values(ocecoform.project).length;
                },
                values : 0
            },
            "totalAction" : {
                type : "html",
                getandsetDatafunc : function(ocecoform){
                    ocecoform.tiles["totalAction"].values = Object.values(ocecoform.actions).length;
                },
                values : 0
            },
            "todoAction" : {
                type : "html",
                getandsetDatafunc : function(ocecoform){
                    var count = 0;
                    $.each(ocecoform.actions,function(k,v){
                        if(exists(v.status) && v.status == "todo")
                            count++;
                    })
                    ocecoform.tiles["todoAction"].values = count;
                },
                values : 0
            },
            "doneAction" : {
                type : "html",
                getandsetDatafunc : function(ocecoform){
                    var count = 0;
                    $.each(ocecoform.actions,function(k,v){
                        if(exists(v.status) && v.status == "done")
                            count++;
                    })
                    ocecoform.tiles["doneAction"].values = count;
                },
                values : 0
            },
            "inProgressAction" : {
                type : "html",
                getandsetDatafunc : function(ocecoform){
                    var count = 0;
                    $.each(ocecoform.actions,function(k,v){
                        if(exists(v.tracking) && v.tracking)
                            count++;
                    })
                    ocecoform.tiles["inProgressAction"].values = count;
                },
                values : 0
            },
            "totalNearProject" : {
                type : "html",
                getandsetDatafunc : function(ocecoform){
                    var actionsM = ocecoform.dataProcessorFunction.ungroup(Object.values(ocecoform.actions));

                    var prlatelenght = 0;

                    var prlatelist = [];

                    $.each(actionsM , function(tsId, tsVl){

                        var acttasks = ocecoform.dataProcessorFunction.selectOccur(tsVl.tasks, "null/false", "checked");

                        var orderedTasks = ocecoform.dataProcessorFunction.sortbyDate(acttasks, "endDate", false);

                        if (typeof orderedTasks[0] != "undefined" &&
                            typeof orderedTasks[0].endDate != "undefined" &&
                            ocecoform.dataProcessorFunction.countDays(orderedTasks[0].endDate, new Date) > 0 &&
                            ocecoform.dataProcessorFunction.countDays(orderedTasks[0].endDate, new Date) < ocecoform.tiles["totalNearProject"].daycnt()
                        ){
                            prlatelenght++;
                            prlatelist.push(tsVl);
                        }

                    });

                    ocecoform.tiles["totalNearProject"].values = prlatelenght;
                    ocecoform.tiles["totalNearProject"].prlatelenght = prlatelist;
                },
                values : 0,
                prlatelenght : [],
                viewinfo : function () {
                    var tableHeadvf = {
                        "credits" : "credits",
                        "name" : "nom",
                        "tasks" : "taches"
                    };
                    smallMenu.open(ocecoform.compileDataHtml(ocecoform.tiles.totalNearProject.prlatelenght, "action"));

                },
                daycnt : function () {
                    var arrayvar = window.location.href.split('.');
                    var daycnt = 10;
                    if (typeof localStorage["aap_"+arrayvar[arrayvar.indexOf('formid') + 1]+"_totalNearProject_configviewinfo"] != "undefined"){
                        daycnt = localStorage["aap_"+arrayvar[arrayvar.indexOf('formid') + 1]+"_totalNearProject_configviewinfo"];
                    }
                    return daycnt;
                },
                configviewinfo : function () {
                    prioModal = bootbox.dialog({
                        message: '<div class="input-group">' +
                            '<label>limite du nombre de jour aprés ce jour que le filtre va prendre en compte(seul la sous-tâche la plus lointaine de l\'action est pris en compte pour evaluer la date de fin de l\'action)</label>' +
                            '<input class="input" type="number" value="'+ocecoform.tiles["totalNearProject"].daycnt()+'" id="totalNearProjectconfigviewinfo">' +
                            '</div>',
                        show: false,
                        buttons: {
                            success: {
                                label: trad.save,
                                className: "btn-primary",
                                callback: function () {
                                    var arrayvar = window.location.href.split('.');
                                    localStorage.setItem("aap_"+arrayvar[arrayvar.indexOf('formid') + 1]+"_totalNearProject_configviewinfo", $('#totalNearProjectconfigviewinfo').val());
                                    urlCtrl.loadByHash(location.hash);
                                }
                            },
                            cancel: {
                                label: trad.cancel,
                                className: "btn-secondary",
                                callback: function(){
                                    prioModal.modal('hide');
                                }
                            }
                        },
                        onEscape: function(){
                            prioModal.modal('hide');
                        }
                    });
                    prioModal.modal('show');
                }
            },
            "financersankey" : {
                type : "chart",
                totaltasks : [],
                userindex : [],
                getandsetDatafunc : function(ocecoform){
                    var dataniv1 = ocecoform.dataProcessorFunction.reformatwithid(ocecoform.allDataAns , "idorigin" , 2 );
                    var dataniv2 = ocecoform.dataProcessorFunction.ungroup(dataniv1);
                    var links = [];
                    var nodes = [];
                    var benef = [];

                    var havepayement = false;

                    $.each(dataniv2, function (indxx , valxx) {
                        if (typeof valxx.financer != "undefined") {
                            $.each(valxx.financer, function (indxx2 , valxx2) {
                                if (typeof valxx2.name != "undefined" && typeof valxx.idorigin != "undefined" && typeof valxx2.amount != "undefined" && $.isNumeric(valxx2.amount)) {

                                    links.push({
                                        "source": valxx2.name,
                                        "target": ocecoform.dataProcessorFunction.getpropo(valxx.idorigin),
                                        "value": parseInt(valxx2.amount)
                                    });
                                }
                                else if ((typeof valxx2.name == "undefined" || valxx2.name == "") && typeof valxx.idorigin != "undefined" && typeof valxx2.amount != "undefined" && $.isNumeric(valxx2.amount)) {
                                    links.push({
                                        "source": parent.name,
                                        "target": ocecoform.dataProcessorFunction.getpropo(valxx.idorigin),
                                        "value": parseInt(valxx2.amount)
                                    });
                                }

                                if (!nodes.some(node => node.name === parent.name )) {
                                    nodes.push({
                                        "name": parent.name,
                                        "level": 0
                                    });
                                }

                                if (typeof valxx2.name != "undefined" && !nodes.some(node => node.name === valxx2.name )){
                                    nodes.push({
                                        "name": valxx2.name,
                                        "level": 0
                                    });
                                }


                                if (typeof valxx.idorigin != "undefined" && !nodes.some(node => node.name === ocecoform.dataProcessorFunction.getpropo(valxx.idorigin))){
                                    nodes.push({
                                        "name": ocecoform.dataProcessorFunction.getpropo(valxx.idorigin),
                                        "level" : 1
                                    });
                                }

                            });
                        }
                        if (typeof valxx.payement != "undefined") {
                            $.each(valxx.payement, function (indxx2 , valxx2) {
                                havepayement = true;
                                if (typeof valxx2.beneficiary != "undefined" && typeof valxx.idorigin != "undefined") {

                                    if(Object.prototype.toString.call(valxx2.beneficiary) === '[object Array]') {
                                        $.each(valxx2.beneficiary, function (it, vt) {
                                            links.push({
                                                "target": vt,
                                                "source": ocecoform.dataProcessorFunction.getpropo(valxx.idorigin),
                                                "value": parseInt(valxx2.amount) / valxx2.beneficiary.length
                                            });
                                        });
                                    }else{
                                        links.push({
                                                "target": valxx2.beneficiary.name,
                                                "source": ocecoform.dataProcessorFunction.getpropo(valxx.idorigin),
                                                "value": parseInt(valxx2.amount) / valxx2.beneficiary.length
                                            });
                                    }

                                }

                                if(Object.prototype.toString.call(valxx2.beneficiary) === '[object Array]') {
                                    $.each(valxx2.beneficiary, function (it, vt) {
                                        if (typeof valxx2.beneficiary != "undefined" && !nodes.some(node => node.name === vt)) {

                                            nodes.push({
                                                "name": vt,
                                                "level": 2
                                            });
                                        }
                                    });
                                }else{
                                    if (typeof valxx2.beneficiary != "undefined" && typeof valxx2.beneficiary.name != "undefined" && !nodes.some(node => node.name === valxx2.beneficiary.name)) {

                                            nodes.push({
                                                "name": valxx2.beneficiary.name,
                                                "level": 2
                                            });
                                        }
                                }

                                if (typeof valxx.idorigin != "undefined" && !nodes.some(node => node.name === ocecoform.dataProcessorFunction.getpropo(valxx.idorigin))){
                                    nodes.push({
                                        "name": ocecoform.dataProcessorFunction.getpropo(valxx.idorigin),
                                        "level" : 1
                                    });
                                }

                            });
                        }
                    });

                    $.each(dataniv2, function (indxx , valxx) {
                        if (typeof valxx.payement != "undefined") {
                            $.each(valxx.payement, function (indxx2 , valxx2) {
                                havepayement = true;
                                if (typeof valxx2.beneficiary != "undefined" && typeof valxx.idorigin != "undefined" && typeof valxx2.amount != "undefined" && $.isNumeric(valxx2.amount)) {

                                    if(Object.prototype.toString.call(valxx2.beneficiary) === '[object Array]') {
                                        $.each(valxx2.beneficiary, function (ix, vx) {
                                            links.push({
                                                "target": vx,
                                                "source": ocecoform.dataProcessorFunction.getpropo(valxx.idorigin),
                                                "value": parseInt(valxx2.amount)
                                            });
                                        });
                                    }else{
                                        links.push({
                                                "target": valxx2.beneficiary.name,
                                                "source": ocecoform.dataProcessorFunction.getpropo(valxx.idorigin),
                                                "value": parseInt(valxx2.amount)
                                            });
                                    }
                                }

                                if(Object.prototype.toString.call(valxx2.beneficiary) === '[object Array]') {
                                    $.each(valxx2.beneficiary, function (ix, vx) {
                                        if (!nodes.some(node => node.name === vx)) {
                                            nodes.push({
                                                "name": vx
                                            });
                                        }
                                        if (!benef.includes(vx)) {
                                            benef.push(vx);
                                        }
                                    });
                                }else{
                                    if (!nodes.some(node => node.name === valxx2.beneficiary.name)) {
                                            nodes.push({
                                                "name": valxx2.beneficiary.name
                                            });
                                        }
                                        if (!benef.includes(valxx2.beneficiary.name)) {
                                            benef.push(valxx2.beneficiary.name);
                                        }
                                }

                                if (typeof valxx.idorigin != "undefined" && !nodes.some(node => node.name === ocecoform.dataProcessorFunction.getpropo(valxx.idorigin))){
                                    nodes.push({
                                        "name": ocecoform.dataProcessorFunction.getpropo(valxx.idorigin)
                                    });
                                }

                            });
                        }
                    });
                    if(!havepayement){
                        ocecoform.tiles["financersankey"].values.labels = ["FINANCEURS", "PROPOSITIONS"];
                    }

                    ocecoform.tiles["financersankey"].values.data = { "links" : JSON.stringify(links), "nodes" : JSON.stringify(nodes) };

                },
                values : {
                    id : "financerd3sankey",
                    data : {
                        "links": [

                        ] ,
                        "nodes": [

                        ] },
                    g : 'graph.views.co.ocecoform.sankey',
                    colors : bigstandartcolors,
                    labels : ["FINANCEURS", "PROPOSITIONS", "EXCECUTANTS"]
                },
                toogleview :function () {

                }
            },
            "currentTasks" : {
                type : "html",
                getandsetDatafunc : function(ocecoform){
                    var actionsM = ocecoform.dataProcessorFunction.ungroup(Object.values(ocecoform.actions));
                    var tasks = ocecoform.dataProcessorFunction.mergeArray(actionsM, "path", "tasks");
                    var com = ocecoform.dataProcessorFunction.selectOccur(tasks, "true", "checked");
                    ocecoform.tiles["currentTasks"].values = tasks.length - com.length;
                },
                values : 0
            },
            "todotasksperperson" : {
                type : "chart",
                totaltasks : [],
                userindex : [],
                getandsetDatafunc : function(ocecoform){
                    var dataTodo = {};
                    var dataName = {};
                    $.each(ocecoform.contributors,function(kcontr,vcontr){
                        dataTodo[kcontr] = 0;
                        dataName[kcontr] = vcontr["name"];
                    })
                    $.each(ocecoform.actions,function(kaction,vaction){
                        if(vaction?.status =="todo" && (vaction?.tracking == false || !exists(vaction.tracking))){
                            if(!exists(vaction.tasks)){
                                if(exists(vaction?.links?.contributors)){
                                    $.each(vaction?.links?.contributors,function(kcontr,vcontr){
                                        dataTodo[kcontr]++;
                                    })
                                }
                            }
                            if(exists(vaction.tasks)){
                                $.each(vaction.tasks,function(ktask,vtask){
                                    if(exists(vtask.contributors)){
                                        $.each(vtask.contributors,function(kcontr,vcontr){
                                            dataTodo[kcontr]++;
                                        })
                                    }
                                })
                            }
                        }
                    })
                    $.each(dataTodo,function(k,v){
                        if(v == 0){
                            delete dataTodo[k];
                            delete dataName[k];
                        }
                    })
                    ocecoform.tiles["todotasksperperson"].values.data = Object.values(dataTodo);
                    ocecoform.tiles["todotasksperperson"].values.labels = Object.values(dataName);
                    ocecoform.tiles["todotasksperperson"].values.colors = ocecoform.dataProcessorFunction.generateColorsByLabels(Object.values(dataName));
                    

                },
                values : {
                    id : "todotasksperperson",
                    data : [0, 0, 0],
                    label : 'nombre de taches',
                    labels : ["","",""],
                    g : 'graph.views.co.ocecoform.bar',
                    colors : bigstandartcolors
                },
                viewinfo :function (ind) {
                    /*var userclick = ocecoform.tiles.tasksperperson.userindex[ind];
                    var totaltasksuserclick = [];
                    $.each(ocecoform.tiles.tasksperperson.totaltasks, function(it , vt){
                        if (userclick in vt.contributors){
                            totaltasksuserclick.push(vt);
                        }
                    });
                    var tableHeadvf = {
                        "task" : "tâche",
                        "credits" : "credits",
                        "createdAt" : "date de creation"
                    };
                    ocecoform.getModal(ocecoform.createTable(totaltasksuserclick, tableHeadvf));*/
                }
            },
            "inprogressTaskPerPerson" : {
                type : "chart",
                totaltasks : [],
                userindex : [],
                getandsetDatafunc : function(ocecoform){
                    var dataInProgress = {};
                    var dataName = {};
                    $.each(ocecoform.contributors,function(kcontr,vcontr){
                        dataInProgress[kcontr] = 0;
                        dataName[kcontr] = vcontr["name"];
                    })
                    $.each(ocecoform.actions,function(kaction,vaction){
                        if(vaction?.status =="todo" && (vaction?.tracking == true)){
                            if(!exists(vaction.tasks)){
                                if(exists(vaction?.links?.contributors)){
                                    $.each(vaction?.links?.contributors,function(kcontr,vcontr){
                                        dataInProgress[kcontr]++;
                                    })
                                }
                            }
                            if(exists(vaction.tasks)){
                                $.each(vaction.tasks,function(ktask,vtask){
                                    if(exists(vtask.contributors)){
                                        $.each(vtask.contributors,function(kcontr,vcontr){
                                            dataInProgress[kcontr]++;
                                        })
                                    }
                                })
                            }
                        }
                    })
                    $.each(dataInProgress,function(k,v){
                        if(v == 0){
                            delete dataInProgress[k];
                            delete dataName[k];
                        }
                    })
                    ocecoform.tiles["inprogressTaskPerPerson"].values.data = Object.values(dataInProgress);
                    ocecoform.tiles["inprogressTaskPerPerson"].values.labels = Object.values(dataName);
                    ocecoform.tiles["inprogressTaskPerPerson"].values.colors = ocecoform.dataProcessorFunction.generateColorsByLabels(Object.values(dataName));
                    

                },
                values : {
                    id : "inprogressTaskPerPerson",
                    data : [0, 0, 0],
                    label : 'nombre de taches',
                    labels : ["","",""],
                    g : 'graph.views.co.ocecoform.bar',
                    colors : bigstandartcolors
                },
                viewinfo :function (ind) {
                }
            },
            "outdatetaskscard" : {
                type : "list",
                getandsetDatafunc : function(ocecoform){
                    var actionsM = ocecoform.dataProcessorFunction.ungroup(Object.values(ocecoform.actions));
                    var tasks = ocecoform.dataProcessorFunction.mergeArray(actionsM, "path", "tasks");
                    tasks = ocecoform.dataProcessorFunction.selectOccur(tasks, "null/false", "checked");
                    var newtasks = [];

                    $.each(tasks , function(tsId, tsVl){
                        if (ocecoform.dataProcessorFunction.countDays(ocecoform.dataProcessorFunction.convertDate(tsVl.endDate), new Date()) < 0) {

                            var profilpicture = "";
                            if (typeof tsVl.contributors != "undefined") {

                                $.each(tsVl.contributors , function(tsIdcon, tsVlcon){

                                    var infodata = [];

                                    infodata = ocecoform.dataProcessorFunction.getUserData(tsIdcon);

                                    if (
                                        typeof infodata != "undefined" &&
                                        typeof infodata.map != "undefined" &&
                                        typeof infodata.map.profilImageUrl != "undefined" &&
                                        infodata.map.profilImageUrl != ""
                                    ) {
                                        profilpicture += '<a href="javascript:;" class="oceco-avatar" data-bs-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="'+infodata.map.name+'"><img  id="menu-thumb-profil"  src="'+infodata.map.profilImageUrl+'" alt="image"></a>';
                                    }else{
                                        profilpicture += '<a href="javascript:;" class="oceco-avatar" data-bs-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="'+infodata.map.name+'"><img class="" id="menu-thumb-profil" src="'+defaultimgprofil+'" alt="image"></a>';
                                    }
                                });
                            }
                            tasks[tsId]["profilpicture"] = profilpicture;

                            newtasks.push(tasks[tsId]);
                        }
                    })
                    var orderedTasks = ocecoform.dataProcessorFunction.sortbyDate(newtasks, "endDate", true);
                    ocecoform.tiles["outdatetaskscard"].values = orderedTasks;
                },
                template : function(listvalue){
                    // return '<div class="ocecocard-item">'+
                    //         // '<div class="ocecocard-icon">'+
                    //         //     '<i class="fa fa-circle"></i>'+
                    //         // '</div>'+
                    //         '<div class="ocecocard-desc">'+
                    //             '<span class="timelinetitle">'+listvalue.task+'</span>'+
                    //             '<p class="timelinetext">'+new Date().getDate()+'/'+new Date().getMonth()+'/'+new Date().getYear()+'</p>'+
                    //         '</div>'+
                    //         '<div class="ocecocard-icon">'+
                    //              listvalue.profilpicture+
                    //         '</div>'+
                    //       '</div>';
                    return '<tr class="ocecotable-tr">'+

                        '<td>'+
                        '<div class="ocecotable-task-div">'+
                        listvalue.task+
                        '</div>'+
                        '</td>'+
                        '<td>'+
                        '<div class="oceco-avatar-group">'+
                        listvalue.profilpicture+
                        '</div>'+
                        '</td>'+
                        '<td >'+
                        '<div class="">'+
                        '<div class="ocecotable-d-div" >'+
                        ocecoform.dataProcessorFunction.convertDate(listvalue.endDate).getDay() + '/' + ocecoform.dataProcessorFunction.convertDate(listvalue.endDate).getMonth()+ '/' +  ocecoform.dataProcessorFunction.convertDate(listvalue.endDate).getFullYear()+
                        '</div>'+
                        '</div>'+
                        '</td>'+
                        '</tr>';
                },
                values : {

                }
            } ,
            "outdatetasklenght" : {
                type : "html",
                getandsetDatafunc : function(ocecoform){
                    var actionsM = ocecoform.dataProcessorFunction.ungroup(Object.values(ocecoform.actions));
                    var tasks = ocecoform.dataProcessorFunction.mergeArray(actionsM, "path", "tasks");
                    tasks = ocecoform.dataProcessorFunction.selectOccur(tasks, "null/false", "checked");
                    var newtasks = [];

                    $.each(tasks , function(tsId, tsVl){
                        if (ocecoform.dataProcessorFunction.countDays(ocecoform.dataProcessorFunction.convertDate(tsVl.endDate), new Date()) < 0) {

                            var profilpicture = "";
                            if (typeof tsVl.contributors != "undefined") {

                                $.each(tsVl.contributors , function(tsIdcon, tsVlcon){

                                    var infodata = [];

                                    infodata = ocecoform.dataProcessorFunction.getUserData(tsIdcon);

                                    if (
                                        typeof infodata != "undefined" &&
                                        typeof infodata.map != "undefined" &&
                                        typeof infodata.map.profilImageUrl != "undefined" &&
                                        infodata.map.profilImageUrl != ""
                                    ) {
                                        profilpicture += '<a href="javascript:;" class="oceco-avatar" data-bs-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="'+infodata.map.name+'"><img  id="menu-thumb-profil"  src="'+infodata.map.profilImageUrl+'" alt="image"></a>';
                                    }else{
                                        profilpicture += '<a href="javascript:;" class="oceco-avatar" data-bs-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="'+infodata.map.name+'"><img class="" id="menu-thumb-profil" src="'+defaultimgprofil+'" alt="image"></a>';
                                    }
                                });
                            }
                            tasks[tsId]["profilpicture"] = profilpicture;

                            newtasks.push(tasks[tsId]);
                        }
                    })
                    var orderedTasks = ocecoform.dataProcessorFunction.sortbyDate(newtasks, "endDate", true);
                    ocecoform.tiles["outdatetaskscard"].values = orderedTasks;
                },
                value : 0
            },
            "completetaskstimeline" : {
                type : "list",
                getandsetDatafunc : function(ocecoform){
                    var actionsM = ocecoform.dataProcessorFunction.ungroup(Object.values(ocecoform.actions));
                    var tasks = ocecoform.dataProcessorFunction.mergeArray(actionsM, "path", "tasks");

                    var newtasks = [];

                    $.each(tasks , function(tsId, tsVl){
                        if (typeof tsVl.endDate != "undefined") {
                            if (ocecoform.dataProcessorFunction.countDays(ocecoform.dataProcessorFunction.convertDate(tsVl.endDate), new Date()) < 0) {

                                var profilpicture = "";
                                if (typeof tsVl.contributors != "undefined") {

                                    $.each(tsVl.contributors , function(tsIdcon, tsVlcon){

                                        var infodata = [];

                                        infodata = ocecoform.dataProcessorFunction.getUserData(tsIdcon);

                                        if (
                                            typeof infodata != "undefined" &&
                                            typeof infodata.map != "undefined" &&
                                            typeof infodata.map.profilImageUrl != "undefined" &&
                                            infodata.map.profilImageUrl != ""
                                        ) {
                                            profilpicture += '<a href="javascript:;" class="oceco-avatar" data-bs-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="'+infodata.map.name+'"><img  id="menu-thumb-profil"  src="'+infodata.map.profilImageUrl+'" alt="image"></a>';
                                        }else{
                                            profilpicture += '<a href="javascript:;" class="oceco-avatar" data-bs-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="'+infodata.map.name+'"><img class="" id="menu-thumb-profil" src="'+defaultimgprofil+'" alt="image"></a>';
                                        }
                                    });
                                }
                                tasks[tsId]["profilpicture"] = profilpicture;

                                newtasks.push(tasks[tsId]);
                            }
                        }
                    });

                    var orderedTasks = ocecoform.dataProcessorFunction.sortbyDate(newtasks, "checkedAt", true);
                    ocecoform.tiles["completetaskstimeline"].values = orderedTasks;
                },
                template : function(listvalue){
                    // return '<div class="ocecotimeline-item">'+
                    //         '<div class="ocecotimeline-icon">'+
                    //             '<i class="fa fa-circle"></i>'+
                    //         '</div>'+
                    //         '<div class="ocecotimeline-desc">'+
                    //             '<span class="timelinetitle">'+listvalue.task+'</span>'+
                    //             '<p class="timelinetext">'+new Date().getHours()+':'+new Date().getMinutes()+' '+new Date().getDate()+'/'+new Date().getMonth()+'/'+new Date().getYear()+'</p>'+
                    //         '</div>'+
                    //       '</div>';
                    return '<tr class="ocecotable-tr">'+
                        '<td >'+
                        '<div class="">'+
                        '<div class="ocecotable-h-div">'+
                        new Date(listvalue.checkedAt).getHours() + ':' + new Date(listvalue.checkedAt).getMinutes()+
                        '</div>'+
                        '<div class="ocecotable-d-div" >'+
                        new Date(listvalue.checkedAt).getDay() + '/' + new Date(listvalue.checkedAt).getMonth()+ '/' +  new Date(listvalue.checkedAt).getFullYear()+
                        '</div>'+
                        '</div>'+
                        '</td>'+
                        '<td>'+
                        '<div class="ocecotable-task-div">'+
                        listvalue.task+
                        '</div>'+
                        '</td>'+
                        '<td>'+
                        '<div class="oceco-avatar-group">'+
                        listvalue.profilpicture+
                        '</div>'+
                        '</td>'+
                        '</tr>';
                },
                values : {

                }
            } ,
            "financerline" : {
                type : "chart",
                getandsetDatafunc : function(ocecoform){
                    var finn = ocecoform.dataProcessorFunction.mergeArray(ocecoform.dataProcessorFunction.ungroup(Object.values(ocecoform.allDataAns)), "path", "financer");
                    var kkkd = ocecoform.dataProcessorFunction.groupWeeks(finn, "date" ,"amount", "countpath", 0, false);
                    ocecoform.tiles["financerline"].values.data[0] = ocecoform.dataProcessorFunction.mergeArray(kkkd, "path", "count" );
                    ocecoform.tiles["financerline"].values.labels[0] = ocecoform.dataProcessorFunction.mergeArray(kkkd, "path", "weekStart" );


                    var payy = ocecoform.dataProcessorFunction.mergeArray(ocecoform.dataProcessorFunction.ungroup(Object.values(ocecoform.allDataAns)), "path", "payement");
                    var pppa = ocecoform.dataProcessorFunction.groupWeeks(payy, "date" ,"amount", "countpath",0, false);
                    ocecoform.tiles["financerline"].values.data[1] = ocecoform.dataProcessorFunction.mergeArray(pppa, "path", "count" );
                    ocecoform.tiles["financerline"].values.labels[1] = ocecoform.dataProcessorFunction.mergeArray(pppa, "path", "weekStart" );

                    var ghg = [...new Set([...ocecoform.tiles["financerline"].values.labels[1],...ocecoform.tiles["financerline"].values.labels[0]])];

                    var finamount = [];
                    var finamount2 = [];

                    $.each(ghg, function(ih, vh){

                        if (ocecoform.tiles["financerline"].values.labels[0].indexOf(vh) != -1 ) {

                            finamount.push(ocecoform.tiles["financerline"].values.data[0][ocecoform.tiles["financerline"].values.labels[0].indexOf(vh)]);
                        }else{
                            finamount.push(0);
                        }

                        if (ocecoform.tiles["financerline"].values.labels[1].indexOf(vh) != -1 ) {

                            finamount2.push(ocecoform.tiles["financerline"].values.data[1][ocecoform.tiles["financerline"].values.labels[1].indexOf(vh)]);
                        }else{
                            finamount2.push(0);
                        }

                    });

                    finamount = ocecoform.dataProcessorFunction.sumCumul(finamount);
                    finamount2 = ocecoform.dataProcessorFunction.sumCumul(finamount2);

                    ocecoform.tiles["financerline"].values.labels[0] = ocecoform.tiles["financerline"].values.labels[1] = ghg;

                    ocecoform.tiles["financerline"].values.data[0] = finamount;

                    ocecoform.tiles["financerline"].values.data[1] = finamount2;

                    var price = ocecoform.dataProcessorFunction.mergeArray(Object.values(ocecoform.allData), "path", "price");
                    ocecoform.tiles["financerline"].values.objectif = ocecoform.dataProcessorFunction.sumInt(price);
                },
                values : {
                    id : "financerline",
                    data : [[0,0], [0,0]],
                    label : '# of Votes',
                    labels : [["0","0"],["0","0"]],
                    objectif : 0,
                    g : 'graph.views.co.ocecoform.multiline',
                    colors : standartcolors,
                    withlabel : true
                }
            } ,
            "finpertype" : {
                type : "chart",
                getandsetDatafunc : function(ocecoform){
                    var tot = ocecoform.dataProcessorFunction.ungroup(Object.values(ocecoform.allDataAns));

                    var labels = [];
                    var data = [];

                    $.each(tot, function (indxx , valxx) {
                            if (typeof valxx.financer != "undefined" && typeof valxx.group != "undefined") {
                                var group = valxx.group.split(',')
                                $.each(group, function (ind , val) {
                                    if (labels.indexOf(val) !== -1){
                                        $.each(valxx.financer, function (indxx2 , valxx2) {
                                            if (typeof valxx2.amount != "undefined") {
                                                data[labels.indexOf(val)] += parseInt(valxx2.amount);
                                            }
                                        });
                                    }else{
                                        labels.push(val)
                                        $.each(valxx.financer, function (indxx2 , valxx2) {
                                            if (typeof valxx2.amount != "undefined") {
                                                data.push(parseInt(valxx2.amount));
                                            }
                                        });
                                    }
                                });
                            }
                    });

                     ocecoform.tiles["finpertype"].values.data = data;
                     ocecoform.tiles["finpertype"].values.labels = labels;


                },
                values : {
                    id : "finpertype",
                    data : [0,0, 0,0,0],
                    label : '# of Votes',
                    labels : groupe,
                    g : 'graph.views.co.ocecoform.pie',
                    colors : bigstandartcolors
                }
            },
            "amounttofin" : {
                type : "html",
                getandsetDatafunc : function(ocecoform){
                    var price = ocecoform.dataProcessorFunction.mergeArray(ocecoform.dataProcessorFunction.ungroup(Object.values(ocecoform.allDataAns)), "path", "price");
                    ocecoform.tiles["amounttofin"].values = numberWithCommas(ocecoform.dataProcessorFunction.sumInt(price));
                },
                values : 0
            },

            "amountfin" : {
                type : "html",
                getandsetDatafunc : function(ocecoform){

                    var tt = ocecoform.dataProcessorFunction.mergeArray(ocecoform.dataProcessorFunction.ungroup(Object.values(ocecoform.allDataAns)), "path", "financer");
                    var ttjk = ocecoform.dataProcessorFunction.mergeArray(tt, "path", "amount");
                    ocecoform.tiles["amountfin"].values = numberWithCommas(ocecoform.dataProcessorFunction.sumInt(ttjk));

                },
                values : 0
            },

            "amountinfin" : {
                type : "html",
                getandsetDatafunc : function(ocecoform){

                    var tt = ocecoform.dataProcessorFunction.mergeArray(ocecoform.dataProcessorFunction.ungroup(Object.values(ocecoform.allDataAns)), "path", "payement");
                    var ttjk = ocecoform.dataProcessorFunction.mergeArray(tt, "path", "amount");
                    ocecoform.tiles["amountinfin"].values = numberWithCommas(ocecoform.dataProcessorFunction.sumInt(ttjk));

                },
                values : 0
            }

        },

        init : function(pInit = null){
            var copyFilters = jQuery.extend(true, {}, formObj);
            copyFilters.initVar(pInit);
            return copyFilters;
        },

        initvalues : function(ocecoform){
            $.each( ocecoform.tiles , function( dataId, dataValue ) {
                ocecoform.tiles[dataId].getandsetDatafunc(ocecoform);
            });
        },

        initviews : function(ocecoform){
            $.each( ocecoform.tiles , function( tilesId, tilesValue ) {
                if(tilesValue.type == "html"){
                    $("#"+tilesId).html(ocecoform.tiles[tilesId].values);
                } else if (tilesValue.type == "chart") {
                    if (typeof tilesValue.callback != "undefined") {
                        var cb = tilesValue.callback;
                    } else {
                        var cb = function(){};
                    }
                    if(typeof window['myBarr'+ocecoform.tiles[tilesId].values] != "undefined"){
                        if (typeof ocecoform.tiles[tilesId].typechart == "undefined" || ocecoform.tiles[tilesId].typechart != "multi"){
                            $.each(ocecoform.tiles[tilesId].values.data , function(dataid, data){
                                window['myBarr'+ocecoform.tiles[tilesId].values].data.datasets[0].data[dataid] = data;
                            });
                        }else{
                            $.each(ocecoform.tiles[tilesId].values.data , function(dataid, data){
                                window['myBarr'+ocecoform.tiles[tilesId].values].data.datasets[0].data[dataid] = data;
                            });
                        }

                    } else {
                        ajaxPost("#"+tilesId, baseUrl+'/graph/co/chart/', ocecoform.tiles[tilesId].values, cb ,"html");
                    }
                    window['myBarr'+ocecoform.tiles[tilesId].values]
                    //ajaxPost("#"+tilesId, baseUrl+'/graph/co/chart/', ocecoform.tiles[tilesId].values, cb ,"html");
                } else if (tilesValue.type == "list") {
                    $.each(ocecoform.tiles[tilesId].values, function(tId, tVal){
                        $("#"+tilesId).append(ocecoform.tiles[tilesId].template(tVal));
                    })
                }
            });
        },

        filters : {
            initialData: [],
            initialDataAns: [],
            activeFilters:{"multiCheckboxPlusinterventionArea":[], "context" : []},
            init: function(ocecoform){

                if(ocecoform.filters.initialData.length==0){
                    ocecoform.filters.initialData = ocecoform.allData;
                    ocecoform.filters.initialDataAns = ocecoform.allDataAns;
                }

                var paramsFilter= {
                    container : "#ocecofiltercontainer",
                    defaults : {
                        types : ["answers"],
                        indexStep:1
                    }
                }

                if (multiorga){


                    var sousorgaColumn = {};

                    var i = 0;
                    $.each(sousorga, function(orid, or) {

                        if(!sousorgaColumn["q-"+i%4]){
                            sousorgaColumn["q-"+i%4]=[];
                        }
                        sousorgaColumn["q-"+i%4].push(orid);
                        i++;
                    });


                    paramsFilter["filters"] = {
                        orga : {
                            view : "megaMenuDropdown",
                            type : "filters",
                            remove0: true,
                            field:"context",
                            countResults: true,
                            name : "Fitre par organisation",
                            event : "filters",
                            keyValue: true,
                            list : sousorgaColumn
                        }
                    }
                }

                ocecoFilter = searchObj;
                //ocecoFilter.search.autocomplete = function(fObj){}
                ocecoFilter.init(paramsFilter);

                $(".dropdown-title").remove();
                $(".badge-theme-count").remove();

                $(document).on("click", ".orga[data-type='filters']", function(e){
                    ocecoform.filters.byOrga(ocecoform, $(this), false);
                    ocecoform.initvalues(ocecoform);
                    ocecoform.initviews(ocecoform);
                });

                $(document).on("click",".filters-activate[data-filterk='orga']", function(){
                    ocecoform.filters.byOrga(ocecoform, $(this), true);
                    ocecoform.initvalues(ocecoform);
                    ocecoform.initviews(ocecoform);
                });

            },

            byOrga:function(ocecoform, element, removeActive=false){
                if(removeActive){
                    ocecoform.filters.activeFilters[ element.data("field") ] = ocecoform.filters.activeFilters[ element.data("field") ].filter(function(value, index, arr){
                        return value!=element.data("value");
                    });
                }else{
                    if(!ocecoform.filters.activeFilters[ element.data("field") ].includes(element.data("value"))){
                        ocecoform.filters.activeFilters[ element.data("field") ].push( element.data("value") );
                    }
                }

                if(ocecoform.filters.activeFilters[ element.data("field") ].length==0){
                    ocecoform.allData = ocecoform.filters.initialData;
                    ocecoform.allDataAns = ocecoform.filters.initialDataAns;
                }else{
                    let entriesAllData = Object.entries(ocecoform.filters.initialData).filter(([key, data])=>{
                        let value = false;
                        if(exists(data[element.data("field")])){
                            $.each(data[element.data("field")], (i, orga) => {
                                $.each(ocecoform.filters.activeFilters[ element.data("field") ], (k, v) => {
                                    const tempV = (i == sousorga[v])|| value;
                                    value = tempV;
                                });
                            });
                        }

                        return value;
                    });

                    let entriesAllDataAns = Object.entries(ocecoform.filters.initialDataAns).filter(([key, data])=>{
                        let value = false;
                        if(data[0] && exists(data[0][element.data("field")])){
                            $.each(data[0][element.data("field")], (i, orga) => {
                                $.each(ocecoform.filters.activeFilters[ element.data("field") ], (k, v) => {
                                    const tempV = (i == sousorga[v])|| value;
                                    value = tempV;
                                });
                            });
                        }
                        return value;
                    });

                    ocecoform.allData = Object.fromEntries(entriesAllData);
                    ocecoform.allDataAns = Object.fromEntries(entriesAllDataAns);

                    console.log("iiii", Object.fromEntries(entriesAllData));
                }

            }

        },

        updateviewData : function(arData, chart){
            if (typeof tiles[chart] !== "undefined" && arData[chart]){
                if (typeof tiles[chart]["type"] == "html" ) {
                    $('#'.chart).html(arData[chart]);
                }
            }
        },

        dataProcessorFunction : {
            mergeArray : function(arr, type, path){
                if(notNull(arr)) {
                    var r = arr.reduce(function (accumulator, item) {
                        if (type == "root" && typeof item != "undefined" && item != null) {
                            accumulator = accumulator.concat(item);
                            return accumulator;
                        } else if (type == "path" && typeof item != "undefined" && typeof item[path] != "undefined") {
                            accumulator = accumulator.concat(item[path]);
                            return accumulator;
                            // }
                            // else if(type == "path" && typeof item != "undefined" && typeof item[path] != "undefined") {

                        } else {
                            return accumulator;
                        }
                    }, []);
                    return r;
                }else{
                    return [];
                }
            },

            reformatwithid : function(obj , keyname="idorigin", level = 1 , idorn = null){
                if (level == 1){
                    var temp = {};
                    $.each(obj , function(index , value){
                        if (notNull(idorn)){
                            temp[index] = value;
                            temp[index][keyname] = idorn;
                        } else {
                            temp[index] = value;
                            temp[index][keyname] = index;
                        }
                    });
                    return Object.values(temp);
                } else {
                    //if (Array.isArray(obj)){
                        var temp2 = {};
                        $.each(obj , function(index2 , value2){

                            temp2[index2] = ocecoform.dataProcessorFunction.reformatwithid(value2, "idorigin", level - 1, index2);

                        });
                        return Object.values(temp2);

                    //}
                }

            },

            sankeytizer : function(arr, path){
                return arr;
            },

            getpropo : function(id){
                if (typeof ocecoform.allData[id] != "undefined" &&
                    typeof ocecoform.allData[id]["answers"] != "undefined" &&
                    typeof ocecoform.allData[id]["answers"]["aapStep1"] != "undefined" &&
                    typeof ocecoform.allData[id]["answers"]["aapStep1"]["titre"] != "undefined"
                ){
                    return ocecoform.allData[id]["answers"]["aapStep1"]["titre"];
                }else {
                    return "";
                }
            },

            selectOccur : function(arr, key, path, limit = null){
                if(notNull(arr)) {

                    if (key != "null/false" && key != "outdate" ) {
                        if (typeof arr != "undefined") {
                            var r = arr.reduce(function (accumulator, item) {
                                if (typeof item != "undefined" && item[path] == key) {
                                    accumulator = accumulator.concat(item);
                                }
                                return accumulator;
                            }, []);
                        } else {
                            return [];
                        }
                        return r;
                    } else if (key == "null/false"){
                        var r = arr.reduce(function (accumulator, item) {
                            if (typeof item[path] == "undefined" || item[path] == null || item[path] == 'false' || item[path] == false) {
                                accumulator = accumulator.concat(item);
                            }
                            return accumulator;
                        }, []);
                        return r;
                    } else if (key == "outdate"){
                        if (typeof arr != "undefined") {
                            var r = arr.reduce(function (accumulator, item) {
                                if (typeof item != "undefined" && (typeof item[path] != "undefined")) {
                                    if (!ocecoform.dataProcessorFunction.verifydatevalue([item[path]])){
                                        item[path] = ocecoform.dataProcessorFunction.convertDate(item[path]);
                                    }
                                    if (ocecoform.dataProcessorFunction.countDays(item[path], new Date) < 0) {
                                        accumulator = accumulator.concat(item);
                                    }
                                }
                                return accumulator;
                            }, []);
                        } else {
                            return [];
                        }
                        return r;
                    }

                }else{
                    return [];
                }
            },

            countOccur : function(arr, key){
                if(notNull(arr)) {

                    if (typeof arr != "undefined") {
                        var r = arr.reduce(function (accumulator, item) {
                            if (typeof item != "undefined" && item == key) {
                                accumulator++;
                            }
                            return accumulator;
                        }, 0);
                    } else {
                        return 0;
                    }
                    return r;

                }else{
                    return 0;
                }
            },
            convertDate : function(dateString){
                if(notNull(dateString) && typeof dateString == "string" ){
                    var dateParts = dateString.split("/");

                    var dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
                    return dateObject;
                }else {
                    return null;
                }
            },
            countDays : function(startDate, endDate){
                if (ocecoform.dataProcessorFunction.verifydatevalue([startDate, endDate])) {
                    var difference_In_Time = startDate.getTime() - endDate.getTime();
                    var difference_In_Days = difference_In_Time / (1000 * 3600 * 24);
                    return Math.round(difference_In_Days);
                }else{
                    return null;
                }
            },
            countToday : function(arr, path){
                if(notNull(arr)) {
                    arr = arr.filter(function (va) {
                        return (new Date(va[path]).getDay() == new Date().getDay()) && (new Date(va[path]).getMonth() == new Date().getMonth()) && (new Date(va[path]).getYear() == new Date().getYear());
                    });
                    return arr.length;
                } else {
                    return 0;
                }
            },
            countWeek : function(arr, path){
                if (notNull(arr)) {
                    arr = arr.filter(function (va) {
                        return ocecoform.dataProcessorFunction.getWeekStart(new Date(va[path])) == ocecoform.dataProcessorFunction.getWeekStart(new Date());
                    });
                    return arr.length;
                }else{
                    return 0;
                }
            },
            verifydatevalue : function (date) {
                var clean = true;
                $.each(date , function (index, value){
                    if (typeof value == "undefined" || value == null || !value.getTime){
                        clean = false;
                    }
                });
                return clean;
            },

            sortbyDate : function(arr, path, isISO, direction = "asc"){
                if (notNull(arr)) {
                    var operators = {
                        'asc': function (a, b) {
                            return a - b
                        },
                        'desc': function (a, b) {
                            return b - a
                        }
                    };

                    if (path != "root") {
                        arr = arr.filter(function (va) {
                            return typeof va[path] !== 'undefined';
                        });
                        if (isISO) {
                            return arr.sort(function (a, b) {
                                return operators[direction](new Date(b[path]), new Date(a[path]));
                            });
                        } else {
                            return arr.sort(function (a, b) {
                                return operators[direction](ocecoform.dataProcessorFunction.convertDate(b[path]), ocecoform.dataProcessorFunction.convertDate(a[path]));
                            });
                        }
                    } else {
                        if (isISO) {
                            return arr.sort(function (a, b) {
                                return operators[direction](new Date(b), new Date(a));
                            });
                        } else {
                            return arr.sort(function (a, b) {
                                return operators[direction](ocecoform.dataProcessorFunction.convertDate(b), ocecoform.dataProcessorFunction.convertDate(a));
                            });
                        }
                    }
                } else {
                    return [];
                }
            },

            getAttr : function(arr, path){
                if (notNull(arr)) {
                    var r = arr.reduce(function (accumulator, item) {
                        if (typeof item != "undefined" && typeof item[path] != "undefined") {
                            accumulator = accumulator.concat([item]);
                        }
                        return accumulator;
                    }, []);
                    return r;
                }else{
                    return [];
                }
            },

            getWeekStart : function (date) {
                var offset = new Date(date).getDay();
                return new Date(new Date(date) - offset * 24 * 60 * 60 * 1000)
                    .toISOString()
                    .slice(0, 10);
            },

            groupWeeks : function (dates, path, countpath, type, countreverse = 0, isISO) {

                const groupsByWeekNumber = dates.reduce(function(acc, item) {
                    var jsdate = item[path];
                    /*if (!isISO) {
                        jsdate = ocecoform.dataProcessorFunction.convertDate(item[path])
                    }*/
                    if (typeof item[path] == "string"){
                        if (item[path].includes("/")){
                            jsdate = ocecoform.dataProcessorFunction.convertDate(item[path])
                        }else{
                            var datestamp = item[path].split(' ')[1];
                            jsdate = new Date(datestamp * 1000);
                        }
                    }

                    const today = new Date(jsdate);

                    const weekNumber = today.getWeek();

                    // check if the week number exists
                    if (typeof acc[weekNumber] === 'undefined') {
                        acc[weekNumber] = [];
                    }

                    acc[weekNumber].push(item);

                    return acc;
                }, []);

                if (type == "countpath" || type == "") {


                    return groupsByWeekNumber.map(function(group) {
                        if (typeof group[0][path] == "string"){
                            if (group[0][path].includes("/")){
                                return {
                                    weekStart: ocecoform.dataProcessorFunction.getWeekStart(ocecoform.dataProcessorFunction.convertDate(group[0][path])),
                                    count: group.reduce(function(acc, item) {
                                        return acc + parseInt(item[countpath]);
                                    }, 0)
                                };
                            }else{
                                var datestamp2 = group[0][path].split(' ')[1];
                                jsdate2 = new Date(datestamp2 * 1000);

                                return {
                                    weekStart: ocecoform.dataProcessorFunction.getWeekStart(jsdate2),
                                    count: group.reduce(function(acc, item) {
                                        return acc + parseInt(item[countpath]);
                                    }, 0)
                                };


                            }
                        }

                    });

                } else if (type == "count"){

                    if (!isISO) {
                        return groupsByWeekNumber.map(function(group) {
                            if (typeof group[0][path] == "string"){
                                if (group[0][path].includes("/")){
                                    return {
                                        weekStart: ocecoform.dataProcessorFunction.getWeekStart(ocecoform.dataProcessorFunction.convertDate(group[0][path])),
                                        count: group.reduce(function(acc, item) {
                                            return acc = acc + 1;
                                        }, 0)
                                    };
                                }else{
                                    var datestamp2 = group[0][path].split(' ')[1];
                                    jsdate2 = new Date(datestamp2 * 1000);

                                    return {
                                        weekStart: ocecoform.dataProcessorFunction.getWeekStart(jsdate2),
                                        count: group.reduce(function(acc, item) {
                                            return acc = acc + 1;
                                        }, 0)
                                    };


                                }
                            }
                        });
                    } else {
                        return groupsByWeekNumber.map(function(group) {

                            if (typeof group[0][path] == "string"){
                                if (group[0][path].includes("/")){
                                    return {
                                        weekStart: ocecoform.dataProcessorFunction.getWeekStart(ocecoform.dataProcessorFunction.convertDate(group[0][path])),
                                        count: group.reduce(function(acc, item) {
                                            return acc = acc + 1;
                                        }, 0)
                                    };
                                }else{
                                    var datestamp2 = group[0][path].split(' ')[1];
                                    jsdate2 = new Date(datestamp2 * 1000);

                                    return {
                                        weekStart: ocecoform.dataProcessorFunction.getWeekStart(jsdate2),
                                        count: group.reduce(function(acc, item) {
                                            return acc = acc + 1;
                                        }, 0)
                                    };


                                }
                            }
                        });
                    }

                } else if (type == "countreverse"){

                    if (!isISO) {
                        return groupsByWeekNumber.map(function(group) {
                            if (typeof group[0][path] == "string"){
                                if (group[0][path].includes("/")){
                                    return {
                                        weekStart: ocecoform.dataProcessorFunction.getWeekStart(ocecoform.dataProcessorFunction.convertDate(group[0][path])),
                                        count: group.reduce(function(acc, item) {
                                            return countreverse - 1;
                                        }, 0)
                                    };
                                }else{
                                    var datestamp2 = group[0][path].split(' ')[1];
                                    jsdate2 = new Date(datestamp2 * 1000);

                                    return {
                                        weekStart: ocecoform.dataProcessorFunction.getWeekStart(jsdate2),
                                        count: group.reduce(function(acc, item) {
                                            return countreverse - 1;
                                        }, 0)
                                    };


                                }
                            }
                        });
                    } else {
                        return groupsByWeekNumber.map(function(group) {
                            if (typeof group[0][path] == "string"){
                                if (group[0][path].includes("/")){
                                    return {
                                        weekStart: ocecoform.dataProcessorFunction.getWeekStart(ocecoform.dataProcessorFunction.convertDate(group[0][path])),
                                        count: group.reduce(function(acc, item) {
                                            return countreverse = countreverse - 1;
                                        }, 0)
                                    };
                                }else{
                                    var datestamp2 = group[0][path].split(' ')[1];
                                    jsdate2 = new Date(datestamp2 * 1000);

                                    return {
                                        weekStart: ocecoform.dataProcessorFunction.getWeekStart(jsdate2),
                                        count: group.reduce(function(acc, item) {
                                            return countreverse = countreverse - 1;
                                        }, 0)
                                    };


                                }
                            }
                        });
                    }

                }

            },

            sumInt : function(arr){
                if(notNull(arr)) {
                    var sum = 0;
                    for(i = 0 ; i < arr.length ; i++){
                        if($.isNumeric(arr[i])){
                            sum = sum + parseInt(arr[i]);
                        }
                    }
                    return sum;
                }else{
                    return [];
                }
            },

            sumCumul : function(arr){
                if (notNull(arr)) {

                    const accumulate = arr => arr.map((sum => value => sum += value)(0));

                    return accumulate(arr);
                }else {
                    return [];
                }
            },

            getUserData : function(id) {
                var returnUserData;
                ajaxPost("",
                    baseUrl+"/co2/element/get/type/citoyens/id/"+id,
                    null,
                    function(data) {

                        if(typeof data != "undefined"){
                            returnUserData = data;
                        }
                    },
                    null,
                    "json",
                    {async : false}
                );
                return returnUserData;

            },

            getOrgaData : function(id) {
                var returnUserData;
                ajaxPost("",
                    baseUrl+"/co2/element/get/type/organizations/id/"+id,
                    null,
                    function(data) {

                        if(typeof data != "undefined"){
                            returnUserData = data;
                        }
                    },
                    null,
                    "json",
                    {async : false}
                );
                return returnUserData;

            },

            group : function(arr, path){
                if (notNull(arr)) {

                    var gr = arr.reduce(function (res, obj) {
                        res[obj[path]] = {
                            count: (obj[path] in res ? res[obj[path]].count : 0) + 1
                        }
                        return res;
                    }, []);
                    return gr;
                }else {
                    return [];
                }
            },

            ungroup : function(arr){
                if (notNull(arr)) {
                    var gr = arr.reduce(function (res, obj) {
                            arrM = ocecoform.dataProcessorFunction.mergeArray(Object.values(obj), "root", "");
                            res = res.concat(arrM);
                            return res;
                        }
                        , []);
                    return gr;
                }else {
                    return [];
                }
            },
            generateColorsByLabels: function(labels = []){
                let colors = [];
                $.each(labels,function(klab,vlab){
                    var hash = 0;
                    for (var i = 0; i < vlab.length; i++) {
                        hash = str.charCodeAt(i) + ((hash << 5) - hash);
                    }
                    var c = (hash & 0x00FFFFFF).toString(16).toUpperCase();
                    var color=  "00000".substring(0, 6 - c.length) + c;
                    colors.push("#"+color);
                })
                return colors
            }
        },

        html : {
            action : function (value) {
                var str = '';
                str += '<div class="row actioncard">';
                str +='         <div class="col-md-4 actioninfo">';
                str +='         <span class="actionname">'+value.name+'</span>';
                /*str +='         <span>'+value.status+'</span>';*/
                str +='         </div>';

                str +='         <div class="col-md-8 actiontaskcontainer">';

                if (typeof value.tasks != "undefined"){
                    $.each(value.tasks, function (index2, value2) {

                        str +='     <div class="col-md-3 actiontask">';
                        str +='          <div class="taskname">'+value2.task+'</div>';
                        str +='          <div><i style="color: orange" class="fa fa-warning"></i> <span style="color: orange"> Fin : </span> '+value2.endDate.toLocaleDateString()+'</div>';

                        if (typeof value2.contributors != "undefined"){
                            str += '     <div>';

                            $.each(value2.contributors, function (index3, value3) {
                                var contr = ocecoform.dataProcessorFunction.getUserData(index3);
                                str += '     <div>';
                                if (typeof contr.map.name != "undefined") {

                                    var profilpc = "";
                                    if("undefined" != typeof contr.map.profilImageUrl && contr.map.profilImageUrl != ""){
                                        profilpc = "<img width='25' height='25' alt='' class='img-circle' src='"+baseUrl+contr.map.profilThumbImageUrl+"'/>";
                                    } else {
                                        profilpc ="<i class='fa fa-user'></i>";
                                    }

                                    str += '             <span>'+  profilpc + '</span>';

                                    str += '             <span>' + contr.map.name + '</span>';
                                }
                                str += '     </div>';
                            });

                            str += '      </div>';

                        };
                        str +='     </div>';
                    });
                }

                str +='         </div>';
                str +='    </div>';

                return str;
            }
        },

        compileDataHtml : function ( data , view){
            var str = "";

            if (typeof ocecoform.html[view] != "undefind" && notNull(data) ) {
                $.each(data, function (index, value) {
                    str += ocecoform.html[view](value);
                });
            }

            return str;
        },

        createTable : function (arrList, labelkey=null) {
            let table = document.createElement('table');
            table.className = "aap-styled-table";
            let headerRow = document.createElement('tr');

            if (labelkey != null){
                $.each(labelkey, function(index, value){
                    let header = document.createElement('th');
                    let textNode = document.createTextNode(value);
                    header.appendChild(textNode);
                    headerRow.appendChild(header);
                });
            }else{
                labelkey = {};

                $.each(arrList, function (index, value){
                    var klist = Object.keys(value);

                    $.each(klist, function(ind, val){
                        //console.log("eeer", val, klist);
                        if (!(val in labelkey)){
                            labelkey[val] = val;
                        }
                    });
                });

                $.each(labelkey, function(index, value){
                    let header = document.createElement('th');
                    let textNode = document.createTextNode(value);
                    header.appendChild(textNode);
                    headerRow.appendChild(header);
                });
            }

            table.appendChild(headerRow);

            $.each(arrList, function(indexarr, valuearr){
                let row = document.createElement('tr');
                if (labelkey != null) {
                    $.each(labelkey, function (indexlb, valuelb) {
                        let cell = document.createElement('td');
                        var textNode;
                        if (typeof valuearr[indexlb] != "undefined" && valuearr[indexlb] != null) {
                            if ( Object.prototype.toString.call(valuearr[indexlb]) === '[object Array]') {
                                textNode = ocecoform.createTable(valuearr[indexlb]);
                                console.log("bebebe", valuearr[indexlb]);
                            } else if (typeof valuearr[indexlb] == "string") {
                                textNode = document.createTextNode(valuearr[indexlb]);
                            } else {
                                textNode = document.createTextNode("");
                            }
                        } else {
                            textNode = document.createTextNode("");
                        }
                        cell.appendChild(textNode);
                        row.appendChild(cell);
                    });
                }
                table.appendChild(row);
            });

            return table;
        },

        getModal : function (htmlbody) {
            prioModal = bootbox.dialog({
                message: htmlbody,
                title: "",
                show: false,
                size: "large",
                buttons: {
                    cancel: {
                        label: trad.cancel,
                        className: "btn-secondary"
                    }
                }
            });
            prioModal.modal("show");
        }

    };

    Date.prototype.getWeek = function(dowOffset) {
        /*getWeek() was developed by Nick Baicoianu at MeanFreePath: http://www.epoch-calendar.com */

        dowOffset = typeof dowOffset == 'int' ? dowOffset : 0; //default dowOffset to zero
        var newYear = new Date(this.getFullYear(), 0, 1);
        var day = newYear.getDay() - dowOffset; //the day of week the year begins on
        day = day >= 0 ? day : day + 7;
        var daynum =
            Math.floor(
                (this.getTime() -
                    newYear.getTime() -
                    (this.getTimezoneOffset() - newYear.getTimezoneOffset()) * 60000) /
                86400000
            ) + 1;
        var weeknum;
        //if the year starts before the middle of a week
        if (day < 4) {
            weeknum = Math.floor((daynum + day - 1) / 7) + 1;
            if (weeknum > 52) {
                nYear = new Date(this.getFullYear() + 1, 0, 1);
                nday = nYear.getDay() - dowOffset;
                nday = nday >= 0 ? nday : nday + 7;
                /*if the next year starts before the middle of
                     the week, it is week #1 of that year*/
                weeknum = nday < 4 ? 1 : 53;
            }
        } else {
            weeknum = Math.floor((daynum + day - 1) / 7);
        }
        return weeknum;
    };

    ocecoform.initvalues(ocecoform);
    ocecoform.initviews(ocecoform);
    ocecoform.filters.init(ocecoform);

    function noduplmergeArr(...arrays) {
        let jointArray = []

        arrays.forEach(array => {
            jointArray = [...jointArray, ...array]
        })
        const uniqueArray = jointArray.filter((item,index) => jointArray.indexOf(item) === index)
        return uniqueArray
    }

</script>
