<?php
HtmlHelper::registerCssAndScriptsFiles(array( 
  '/css/header.css',
  ), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );
?>

<div class="wrap">
    <header id="header" style="position: relative;">
        <div class="container-fluid">
            <div class="col-md-12">
                <button id="primary-nav-button" type="button">Menu</button>
                <!-- <div class="logo">
                    <a ><img data-imgurl="<?php echo Yii::app()->getModule('survey')->assetsUrl ?>/images/" alt="Logo" id="logoCoform"></a>
                </div> -->
                <nav id="primary-nav" class="dropdown cf">
                    <ul class="dropdown menu">
                    <?php 
                        if (isset($adminRight) && $adminRight) {
                            if (isset($opalContextId) && $opalContextType != null) {
                    ?>
                        <li id="btn-configform"><a href='javascript:;' class="getformconfig" data-item='Graph' data-parentformid='<?php echo $parentFormId ?>' data-contextid='<?php echo $opalContextId ?>' data-contexttype='<?php echo $opalContextType ?>' ><i class="fa fa-cogs fa-formheader" aria-hidden="true"></i><?php echo Yii::t('survey','Configure form') ?> </a></li>
                    
                    <?php 
                            } else {
                    ?> 
                        <li id="btn-configform"><a href='javascript:;' class="getformconfig" data-item='Graph' data-parentformid='<?php echo $parentFormId ?>'><i class="fa fa-cogs fa-formheader" aria-hidden="true"></i><?php echo Yii::t('survey','Configure form') ?> </a></li>

                    <?php 
                            }
                        }
                    ?>   

                    <?php 
                        if (isset($opalContextId) && $opalContextType != null) {
                    ?>
                        <li ><a class='active getdashboard' href='javascript:;' data-item='Liste' data-parentformid='<?php echo $parentFormId ?>' data-contextid='<?php echo $opalContextId ?>' data-contexttype='<?php echo $opalContextType ?>' ><i class="fa fa-list-alt fa-formheader" aria-hidden="true"></i> <?php echo Yii::t('survey','List of answers') ?>  </a></li>
                    <?php 
                        } else {
                    ?> 
                        <li ><a class='active getdashboard' href='javascript:;' data-item='Liste' data-parentformid='<?php echo $parentFormId ?>' ><i class="fa fa-list-alt fa-formheader" aria-hidden="true"></i> <?php echo Yii::t('survey','List of answers') ?> </a></li>
                    <?php 
                        }
                    ?> 

                    <?php 
                        if (isset($opalContextId) && $opalContextId != null) {
                    ?>
                        <!--li id="btn-stat"><a href='javascript:;' class="getopalstat" data-item='Stat' data-parentformid='<?php echo $parentFormId ?>' data-contextid='<?php echo $opalContextId ?>' data-contexttype='<?php echo $opalContextType ?>' ><i class="fa fa-tachometer" aria-hidden="true"></i><?php echo Yii::t("survey","Observatory") ?></a></li-->
                    <?php 
                        } else {
                    ?> 
                        <li id="btn-stat"><a href='javascript:;' class="getobservatory" data-item='Stat' data-parentformid='<?php echo $parentFormId ?>' data-week='<?= date("W") ?>'><i class="fa fa-tachometer fa-formheader" aria-hidden="true"></i><?php echo Yii::t("survey","Observatory") ?></a></li>
                    <?php 
                        }
                    ?> 

                    <?php 
                        if (isset($opalContextId) && $opalContextId != null) {
                    ?>
                        <li id="btn-graph"><a href='javascript:;' class="getopalgraph" data-item='Graph' data-parentformid='<?php echo $parentFormId ?>' data-contextid='<?php echo $opalContextId ?>' data-contexttype='<?php echo $opalContextType ?>' ><i class="fa fa-area-chart fa-formheader" aria-hidden="true"></i><?php echo Yii::t('survey','Opal Dashboard') ?></a></li>
                    <?php 
                        } else {
                    ?> 
                        <li id="btn-graph"><a href='javascript:;' class="getgraphbuilder" data-item='Graph' data-parentformid='<?php echo $parentFormId ?>'><i class="fa fa-area-chart fa-formheader" aria-hidden="true"></i><?php echo Yii::t('survey','Graph') ?></a></li>
                    <?php 
                        }
                    ?> 

                    <?php if (isset($parentFormId) && $parentFormId != null) { ?>
                        <!-- <li>
                            <a href='javascript:;' class="globalEvaluate"  data-parentformid='<?= $parentFormId ?>'><i class="fa fa-check-square fa-formheader" aria-hidden="true"></i><?php echo Yii::t('survey','Evaluate the projects') ?></a>
                        </li> -->
                    <?php } ?>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
</div>