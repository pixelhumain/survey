<style type="text/css">

.firstinfo {
    display: flex;
}


.card-user-about {
    min-height: 100px;
    padding: 20px;
    border-radius: 3px;
    background-color: white;
    box-shadow: 0px 10px 20px #354C57;
    position: relative;
    overflow: hidden;
}

.card-user-about:after {
    content: '';
    display: block;
    width: 190px;
    height: 350px;
    background: <?php echo ($titleColor) ? $titleColor : "cadetblue"; ?>;
    position: absolute;
    animation: rotatemagic 0.75s cubic-bezier(0.425, 1.04, 0.47, 1.105) 1s both;
}

.card-user-about .firstinfo {
    flex-direction: row;
    z-index: 2;
    position: relative;
}

.card-user-about .firstinfo img {
    border-radius: 50%;
    width: 100px;
    height: 100px;
    border: 3px solid #fff;
    box-shadow: 0 3px 6px rgb(0 0 0 / 16%), 0 3px 6px rgb(0 0 0 / 23%);
}

.card-user-about .firstinfo .profileinfo {
    padding: 0px 20px;
}

.card-user-about .firstinfo .profileinfo h1 {
    font-size: 24px;
    text-transform: none;
    font-weight: 400;
}

.card-user-about .firstinfo .profileinfo h4 {
    font-size: 20px;
    text-transform: none;
    font-weight: 400;
}


@keyframes animatop {
    0% {
        opacity: 0;
        bottom: -500px;
    }
    100% {
        opacity: 1;
        bottom: 0px;
    }
}

@keyframes animainfos {
    0% {
        bottom: 10px;
    }
    100% {
        bottom: -42px;
    }
}

@keyframes rotatemagic {
    0% {
        opacity: 0;
        transform: rotate(0deg);
        top: -24px;
        left: -253px;
    }
    100% {
        transform: rotate(-30deg);
        top: -24px;
        left: -78px;
    }
}

    @media (max-width: 767px) {
        .card-user-about .firstinfo img {
            width: 80px;
            height: 80px;
        }
        .card-user-about .card:after {
            width: 150px;
            height: 240px;
        }
        .card-user-about .firstinfo .profileinfo h1 {
    		font-size: 18px;
    	}
    }
    
</style>

<div class="form-group">
	


	<div class="card-user-about col-xs-12">
		<?php 
			$user = PHDB::findOne(Person::COLLECTION,["_id"=>new MongoId($answer["user"])]);
			$imgPath = (@$user["profilMediumImageUrl"] && !empty($user["profilMediumImageUrl"])) ? Yii::app()->createUrl('/'.$user["profilMediumImageUrl"]) : $this->module->getParentAssetsUrl().'/images/thumb/default_citoyens.png'; 
		?>
        <!-- <div class="firstinfo"><img src="https://bootdey.com/img/Content/avatar/avatar6.png" /> -->
        <div class="firstinfo"><img src="<?php echo $imgPath ?>" />
            <div class="profileinfo">
                <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label?> :</h4>
                <h1><a class="lbh-preview-element" data-hash='#page.type.<?php echo Person::COLLECTION ?>.id.<?php echo (string)$answer["user"] ?>' href="@<?php echo Yii::app()->createUrl($user["username"]); ?>">
                	<?php echo @$user["name"]; ?>
                </a></h1>
            </div>
        </div>
    </div>
</div>

