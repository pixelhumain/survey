<?php 
$cssJS = array(
    '/plugins/jQuery-Knob/js/jquery.knob.js',
    '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
    //'/plugins/jQuery-Smart-Wizard/styles/smart_wizard.css',
  // SHOWDOWN
  '/plugins/showdown/showdown.min.js',
  // MARKDOWN
  '/plugins/to-markdown/to-markdown.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);
$poiList = array();

if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
    $poiList = PHDB::find(Poi::COLLECTION, 
                    array( "parent.".$this->costum["contextId"] => array('$exists'=>1), 
                           "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"],
                           "type"=>"cms") );
} 
?>

<style type="text/css">
  @font-face{
      font-family: "montserrat";
       src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/montserrat-regular.woff") format("woff"),
       url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/montserrat-regular.ttf") format("ttf")
  }.mst{font-family: 'montserrat'!important;}

  @font-face{
      font-family: "CoveredByYourGrace";
      src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/CoveredByYourGrace.ttf")
  }.cbyg{font-family: 'CoveredByYourGrace'!important;}
    
    
  #customHeader{
    margin-top: 0px;
  }
  #costumBanner{
   /* max-height: 375px; */
  }
  #costumBanner h1{
    position: absolute;
    color: white;
    background-color: rgba(0,0,0,0.4);
    font-size: 29px;
    bottom: 0px;
    padding: 20px;
  }
  #costumBanner h1 span{
    color: #eeeeee;
    font-style: italic;
  }
  #costumBanner img{
    min-width: 100%;
  }
  .btn-main-menu{
    background: <?php echo $this->costum["colors"]["pink"]; ?>;
    border-radius: 10px;
    padding: 10px !important;
    color: white;
    cursor: pointer;
    border:3px solid transparent;
    font-size: 1.5em
    /*min-height:100px;*/
  }
  .btn-main-menuW{
    background: white;
    color: <?php echo $this->costum["colors"]["pink"]; ?>;
    border:none;
    cursor:text ;
  }
  .btn-main-menu:hover{
    border:2px solid <?php echo $this->costum["colors"]["pink"]; ?>;
    background-color: white;
    color: <?php echo $this->costum["colors"]["pink"]; ?>;
  }
  .btn-main-menuW:hover{
    border:none;
  }
  @media screen and (min-width: 450px) and (max-width: 1024px) {
    .logoDescription{
      width: 60%;
      margin:auto;
    }
  }

  @media (max-width: 1024px){
    #customHeader{
      margin-top: -1px;
    }
  }
  #customHeader #newsstream .loader{
    display: none;
  }

</style>
<script type="text/javascript">
    var sectionDyf = {};
		
</script>
<?php
  // $params = [  "tpl" => $this->costum["slug"],
  //       "slug"=>$el["slug"],
  //       "canEdit"=>$canEdit,
  //       "el"=>$el  ];
  // echo $this->renderPartial("survey.views.tpls.acceptAndAdmin", $params, true ); 
?>  

<div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">
<?php
$color1 = "#E63458";
if(isset($this->costum["cms"]["color1"]))
	$color1 = $this->costum["cms"]["color1"];
// if($canEdit)
//   echo "<a class='btn btn-xs btn-danger editBtn' href='javascript:;' data-key='color1' data-type='color'  data-path='costum.cms.color1' data-label='Couleur Principale '><i class='fa fa-pencil'></i></a>";
?>

	<div class="col-xs-12 no-padding" >
		<!-- style="background-color:<?php //echo $this->costum["colors"]["grey"]; ?>; max-width:100%;"> -->

		<div class="col-xs-12 no-padding" style="">
		<style type="text/css">
			.monTitle{
				border-top: 1px dashed <?php echo $this->costum["colors"]["pink"]; ?>; 
				border-bottom: 1px dashed <?php echo $this->costum["colors"]["pink"]; ?>;
				    margin-top: -20px;
			}
		</style>
	<?php $formSmallSize =  12; ?>
			<div class="col-md-12 col-lg-<?php echo $formSmallSize?> no-padding "><br/>

				<div class="col-xs-12 no-padding" style="background-color: white; ">

					<div class="col-xs-12 no-padding">

						<script type="text/javascript">
						var formInputs = {};
						var answerObj = <?php echo (!empty($answer)) ? json_encode( $answer ) : "null"; ?>;
						</script>
						<div class="col-xs-12 margin-top-20">
						<?php 
						$wizardUid = "wizardForm";
						$params = [
							"el" => $el,
							"color1" => $color1,
							"canEdit" => $canEdit,
							"allAnswers"=>@$allAnswers,
							"what" => "dossiers"
						];
						echo $this->renderPartial("survey.views.tpls.answers.allAnswers",$params); 
		                
						if($showForm){
							$params = [
								"parentForm"=>$parentForm,
                "form"=>$form,
								"el" => $el,
								"active" => "all",
								"color1" => $color1,
								"canEdit" => false,
								"answer"=>$answer,
								"showForm" => $showForm,
								"mode" => $mode,
								"wizid"=> $wizardUid
							];
							echo $this->renderPartial("survey.views.tpls.forms.wizard",$params); 

						} else {
							echo "<h4 class='text-center' style='color:".$color1."'><i class='fa fa-warning'></i> Une seul réponse n'est possible.</h4>";
							echo "<a class='btn btn-primary' href='/costum/co/index/slug/".$el["slug"]."/answer/".@$myAnswers[0]."'>Votre réponse</a>";
						}
						?>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


</div>

<script type="text/javascript">
//to edit costum page pieces 
var configDynForm = <?php echo json_encode($this->costum['dynForm']); ?>;
var answerId = <?php echo json_encode((String)$answer['_id']); ?>;
var mode = <?php echo json_encode($mode); ?>;
//information and structure of the form in this page  
var tplCtx = {};
if(location.hash.indexOf("#answer.index.id.new")>=0){
    history.replaceState("#answer.index.id.new", "", "#answer.index.id."+answerId+".mode."+mode);
}

jQuery(document).ready(function() {

	mylog.log("render","/modules/costum/views/custom/co/formWizard.php");
	contextData = {
		id : "<?php echo $this->costum["contextId"] ?>",
		type : "<?php echo $this->costum["contextType"] ?>",
		name : "<?php echo htmlspecialchars($el['name']) ?>",
		profilThumbImageUrl : "http://127.0.0.1/ph/themes/CO2/assets/img/LOGOS/CO2/logo-min.png"
	};
	getAjax("community", 
			baseUrl+"/"+moduleId+"/element/getdatadetail/type/"+costum.contextType+"/id/"+costum.contextId+"/dataName/members",
			function(data){
				//displayInTheContainer(data,"members","group",null,true);
				mylog.log("#community",data);
			},
			"html");

});


</script>

