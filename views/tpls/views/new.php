<?php 
$costum=CacheHelper::getCostum();
$pageUrl=$_SERVER['REQUEST_URI'];
$hash = explode('/', $pageUrl);
$hash = array_pop($hash);
$config= isset($costum["app"]["#".$hash]) ? $costum["app"]["#".$hash] : null;
$title = isset($config["newPage"]["title"]) ? $config["newPage"]["title"] : "Déposer une candidature" ;
$subtitle = isset($config["newPage"]["subtitle"]) ? $config["newPage"]["subtitle"] : "Voici la liste des formulaires disponibles" ;
$button = isset($config["newPage"]["button"]) ? $config["newPage"]["button"] : "Déposer une candidature" ;

?> 
<div class="col-xs-12">
	<h2 class="text-center"><?php echo $title ?></h2>

	<div class="col-sm-10 col-sm-offset-1">
		<div class="col-xs-12">
			<?php echo $subtitle ?>
		</div>
		<div class="col-xs-12 padding-10">
			<?php
			if(!empty($forms)){
				foreach ($forms as $key => $value) { ?>
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title"><?php echo @$value["name"] ; ?></h3>
						</div>
						<div class="panel-body">
							<?php if(!empty(Yii::app()->session['userId'])){ 

								$url = "#answer.index.id.new.form.".$key;
								if(!empty($contextId) && !empty($contextType))
									$url .= ".contextId.".$contextId.".contextType.".$contextType;
								?>
								<a class="lbh btn btn-success" href="<?php echo $url; ?>" style="font-size: 17px;"> <?php echo $button ?></a>
					       <?php } else { ?>
								<button class="btn btn-default bg-white letter-green bold" data-toggle="modal" data-target="#modalLogin" style="font-size: 17px;">
									<i class="fa fa-sign-in"></i> 
									<span class="hidden-xs"><?php echo Yii::t("login", "Login") ?></span>
								</button>
								<button class="btn btn-default bg-white letter-blue bold" data-toggle="modal" data-target="#modalRegister" style="font-size: 17px;">
				                        <i class="fa fa-plus-circle bold"></i> 
				                        <span class="hidden-xs"><?php echo Yii::t("login", "I create my account") ?></span>
				                </button>
					       <?php } ?>
						</div>
					
			       </div>
				<?php
				}
			}else{
				echo "<h5 style='display:flex; justify-content:center; margin-top:30px;'>".Yii::t("survey","No form has been shared for the moment")."</h5>.";
			} ?>
			
		</div>
	</div>
</div>
