<script type="text/javascript">
	//contains all dynform definitions for complexe inputs
	//it also carries sub dynforms, when an input needs extra parameters to 
	var sectionDyf = {};
	var tplCtx = {};
	
	var DFdata = {
		'tpl'  : '<?php echo $tpl ?>',
		"id"   : "<?php echo Yii::app()->session["costum"]["contextId"] ?>"
	};
	costum.col = "<?php echo Yii::app()->session["costum"]["contextType"]  ?>";
	costum.ctrl = "<?php echo Element::getControlerByCollection(Yii::app()->session["costum"]["contextType"]) ?>";

	var configDynForm = <?php echo json_encode($this->costum['dynForm']); ?>;
	var tplsList = <?php echo json_encode((isset($this->costum['tpls'])) ? $this->costum['tpls']:null); ?>;

		//contains all dynform definitions for complexe inputs
		//it also carries sub dynforms, when an input needs extra parameters to 
		//var sectionDyf = {};
		var tplCtx = {};
		
		var DFdata = {
			'tpl'  : '<?php echo $tpl ?>',
			"id"   : "<?php echo $this->costum["contextId"] ?>"
		};
		costum.col = "<?php echo $this->costum["contextType"]  ?>";
		costum.ctrl = "<?php echo Element::getControlerByCollection($this->costum["contextType"]) ?>";

		var configDynForm = <?php echo json_encode($this->costum['dynForm']); ?>;
		var tplsList = <?php echo json_encode((isset($this->costum['tpls'])) ? $this->costum['tpls']:null); ?>;

</script>
		
<?php 
if( !isset( $el["costum"]["slug"] ) || (isset( $el["costum"]["slug"]) && $el["costum"]["slug"] != $tpl ) ) 
  $test=$tpl;
if( isset($test) || $canEdit ) 
{
?>

<script type="text/javascript">

	
	function saveThisTpl (slug) { 
		data = {
			collection : costum.contextType,
			id : costum.contextId,
			path : "costum.slug",
			value : '<?php echo $tpl ?>'
		}
		mylog.log(".saveThisTpl","data",data);
		// http://127.0.0.1/costum/co/index/slug/administration-opal/test/campagne#
		dataHelper.path2Value( data, function(params) { 
			var s = location.href;
			var a = s.split('/');
			s = s.replace(a[a.length-2] + '/', '');
			s = s.replace(a[a.length-1], '');
			location.href = s;
		} );
	}
	function previewTpl () { 
		$('#acceptAndAdmin,.editBtn,.editQuestion,.addQuestion,.deleteLine, .previewTpl').fadeOut(); 
	}

jQuery(document).ready(function() {


    mylog.log("render","/modules/costum/views/tpls/acceptAndAdmin.php");
        $(".editBtn").off().on("click",function() { 
            var activeForm = {
                "jsonSchema" : {
                    "title" : "Template config",
                    "type" : "object",
                    "properties" : {
                        
                    }
                }
            };
            if(configDynForm.jsonSchema.properties[ $(this).data("key") ])
            	activeForm.jsonSchema.properties[ $(this).data("key") ] = configDynForm.jsonSchema.properties[ $(this).data("key") ];
            else
            	activeForm.jsonSchema.properties[ $(this).data("key") ] = { label : $(this).data("label") };
            
            if($(this).data("label"))
            	activeForm.jsonSchema.properties[ $(this).data("key") ].label = $(this).data("label");
            if($(this).data("type")){
            	activeForm.jsonSchema.properties[ $(this).data("key") ].inputType = $(this).data("type");
            	if($(this).data("type") == "textarea" && $(this).data("markdown") )
            		activeForm.jsonSchema.properties[ $(this).data("key") ].markdown = true;
            }

            
            tplCtx.id = contextData.id;
			tplCtx.collection = contextData.type;
            tplCtx.key = $(this).data("key");
            tplCtx.path = $(this).data("path");
            
            activeForm.jsonSchema.save = function () {  
                tplCtx.value = $( "#"+tplCtx.key ).val();
                    //alert("#"+tplCtx.key+" : "+$( "#"+tplCtx.key ).val());
                console.log("activeForm save tplCtx",tplCtx);
                if(typeof tplCtx.value == "undefined")
                	toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        location.reload();
                    } );
                }

        	}
            dyFObj.openForm( activeForm );
    });

    $('.deleteLine').off().click( function(){
      formId = $(this).data("id");
      key = $(this).data("key");
      pathLine = $(this).data("path");
      collection = $(this).data("collection");
      bootbox.dialog({
          title: trad.confirmdelete,
          message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
          buttons: [
            {
              label: "Ok",
              className: "btn btn-primary pull-left",
              callback: function() {
                var formQ = {
				  	value:null,
                    pull:pathLine,
				  	collection : collection,
				  	id : formId,
				  	path : pathLine
				  };
				  
				  dataHelper.path2Value( formQ , function(params) { 
						$("#"+key).remove();
                        //location.reload();
					} );
              }
            },
            {
              label: "Annuler",
              className: "btn btn-default pull-left",
              callback: function() {}
            }
          ]
      });
    });

	$(".editTpl").off().on("click",function() { 
		var activeForm = {
            "jsonSchema" : {
                "title" : "Edit Question config",
                "type" : "object",
                "properties" : {}
            }
        };
    
    	$.each( formTpls [ $(this).data("key") ] , function(k,val) { 
    		console.log("formTpls",k,val); 
    		activeForm.jsonSchema.properties[ k ] = {
    			label : k,
    			value : val
    		};
    	 });
        
        console.log("formTpls activeForm.jsonSchema.properties",activeForm.jsonSchema.properties); 
        tplCtx.id = $(this).data("id");
        tplCtx.key = $(this).data("key");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path");
        
        activeForm.jsonSchema.save = function () {  
            tplCtx.value = {};
            $.each( formTpls [ tplCtx.key ] , function(k,val) { 
        		tplCtx.value[k] = $("#"+k).val();
        	 });
                //alert("#"+tplCtx.key+" : "+$( "#"+tplCtx.key ).val());
            console.log("save tplCtx",tplCtx);
            if(typeof tplCtx.value == "undefined")
            	toastr.error('value cannot be empty!');
            else {
                dataHelper.path2Value( tplCtx, function(params) { 
                    $("#ajax-modal").modal('hide');
                    location.reload();
                } );
            }

    	}
        dyFObj.openForm( activeForm );
    });

    $(".addTpl").off().on("click",function() { 
        var tplCtx = {};
        var activeForm = {
            "jsonSchema" : {
                "title" : "Ajouter une section",
                "type" : "object",
                "properties" : {
                    type : { label : "Choisir le template",
                    		 inputType : "select",
                    		 options : {
								"blockevent"    : "Les blocs d'évènements",
                                "eventsCarousel" : "Les évènements en carousel",
								"blockwithimg"  : "Un bloc avec un texte et une image",
								"communityblock"     : "Les blocs communautés",
								"communityCaroussel"      : "Communauté en carousel",
                                "news" : "L'actualité"
                    		 },
                    		 value : "text" }
                }
            }
        };          
        
        tplCtx.id = $(this).data("id");
        tplCtx.key = $(this).data("key");
        tplCtx.collection = $(this).data("collection");
        
        activeForm.jsonSchema.save = function () {  
            tplCtx.path = "costum.tpls."+$("#type").val();
            tplCtx.value = {
                defaultcolor : "white"
            };
            console.log("activeForm save tplCtx",tplCtx);
            if(typeof tplCtx.value == "undefined")
            	toastr.error('value cannot be empty!');
            else {
                dataHelper.path2Value( tplCtx, function(params) { 
                    $("#ajax-modal").modal('hide');
                    location.reload();
                } );
            }

    	}

        dyFObj.openForm( activeForm );
    });

    
});
</script>

<div class="alert alert-danger text-center"  id="acceptAndAdmin" >
  
  	<?php //echo $this->costum["contextId"]."<br/>" ?>
	<?php //echo $this->costum["contextType"]."<br/>" ?>
	<?php //echo $this->costum["contextSlug"]."<br/>" ?>
	<?php //var_dump($this->costum) ?>

  	<?php if(isset($test)  ){ ?>
  	<strong>Hey!</strong> Is this what you want ? 
	<a href="javascript:;" onclick=" saveThisTpl('<?php echo $tpl ?>') " class="btn btn-danger"> Save This Template</a> <a href="/costum/co/index/slug/<?php echo $slug ?>/test/costumBuilder" class="btn btn-default"> Try another Template </a>
	
	<?php } else if(isset($this->costum["dynForm"])) {
        ?>
	<!-- <strong>Configure</strong> your Costum Template <a href="/costum/co/config/slug/<?php //echo $slug ?>" class="btn btn-danger"><i class="fa fa-pencil"></i> here </a> -->
	<a class="btn btn-default" href="javascript:previewTpl();"><i class="fa fa-eye"></i> Preview</a>
    <a class="btn btn-default" href="#@<?= $this->costum["contextSlug"] ?>" data-hash="#page.type.<?= $this->costum["contextType"] ?>.id.<?= $this->costum["contextId"] ?>" target="_blank"><i class="fa fa-id-card-o" aria-hidden="true"></i>
Element </a>
    <?php echo $this->renderPartial("costum.views.tpls.blocCss", array("canEdit" => $canEdit));   ?>
    <?php echo $this->renderPartial("costum.views.tpls.blocApp", array("canEdit" => $canEdit));   ?>
	<?php } else  { ?>
	<strong>hey petit pixel</strong> rajoute une balise dynform sur ton template pour le rendre editable. 
	<?php } ?>
</div>

<?php } ?>