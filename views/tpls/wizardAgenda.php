<?php
$defaultColor = "#354C57"; 
$structField = "structags";
?>
<style type="text/css">
.circle {
  font-weight: bold;
  padding: 15px 20px;
  border-radius: 50%;
  background-color: #71b62c;
  color: white;
  max-height: 50px;
  z-index: 2;
}
.circle.active{
      background: #8a2f88;
    border: inset 3px #8a2f88;
    max-height: 70px;
    height: 70px;
    font-size: 25px;
    width: 70px;
}
.timeline-ctc h2{
 text-align: center;
    padding: 105px 0px 60px 0px !important;
    background: #8a2f88;
    font-size: 40px;
    color: white;

}
.how-it-works.row {
  display: flex;
}
.row .timeline{
  display: flex;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  margin-right: -15px;
  margin-left: -15px;
  padding:0;
  margin-bottom: 0;
}
.how-it-works.row .col-2 {
  display: inline-flex;
  align-self: stretch;
  position: relative;
  align-items: center;
  justify-content: center;
}
.how-it-works.row .col-2::after {
  content: "";
  position: absolute;
  border-left: 3px solid #cfc2ae;
  z-index: 1;
}
.how-it-works.row .col-2.bottom::after {
  height: 50%;
  left: 50%;
  top: 50%;
}
.how-it-works.row.justify-content-end .col-2.full::after {
  height: 100%;
  left: calc(50% - 3px);
}
.how-it-works.row .col-2.full::after {
    height: 100%;
    left: calc(50% - 0px);
}
.how-it-works.row .col-2.top::after {
  height: 50%;
  left: 50%;
  top: 0;
}

.timeline-ctc .timeline div {
  padding: 0;
  height: 40px;
}
.timeline-ctc .timeline hr {
  border-top: 3px solid #cfc2ae;
  margin: 0;
  top: 17px;
  position: relative;
}
.timeline-ctc .timeline .col-2 {
  display: flex;
  overflow: hidden;
  flex: 0 0 16.666667%;
    max-width: 16.666667%;
}
.align-items-center {
    -ms-flex-align: center !important;
    align-items: center !important;
}
.justify-content-end {
    -ms-flex-pack: end !important;
    justify-content: flex-end !important;
}
.row {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: 15px;
    margin-left: -15px;
}
.how-it-works.row .col-6 p{
  color: #444;
}
.how-it-works.row .col-6 h5{
font-size: 17px;
    text-transform: inherit;
}
.col-2 {
    -ms-flex: 0 0 16.666667%;
    flex: 0 0 16.666667%;
    max-width: 16.666667%;
}
.col-6 {
    -ms-flex: 0 0 50%;
    flex: 0 0 50%;
    max-width: 50%;
}
.timeline-ctc .timeline .col-8 {  
    flex: 0 0 66.666667%;
    max-width: 66.666667%;
}
.timeline-ctc .timeline .corner {
  border: 3px solid #cfc2ae;
  width: 100%;
  position: relative;
  border-radius: 15px;
}

.timeline-ctc .timeline .top-right {
  left: 50%;
  top: -50%;
}
.timeline-ctc .timeline .left-bottom {
  left: -50%;
  top: calc(50% - 3px);
}
.timeline-ctc .timeline .top-left {
  left: -50%;
  top: -50%;
}
.timeline-ctc .timeline .right-bottom {
  left: 50%;
  top: calc(50% - 3px);
}
	
</style>
<div id="wizardAgenda" class="waMain">
    <?php
    foreach($listSteps as $k => $v){
        $n = "todo";
        $p = null;

        if( count(Poi::getPoiByStruct($poiList,"wastep".$v,$structField))!=0 ){
            $p = Poi::getPoiByStruct($poiList, "wastep".$v,$structField )[0];
            $n =  $p["name"];

        // La verification de plein chose
        if($v == 1) {
            // first section
            ?>
            <div class="row align-items-center how-it-works">
                <div class="col-2 text-center bottom">
                    <div class="circle"><?= $v; ?></div>
                </div>
                <div class="col-6">
                    <h5><?= @$p["name"]; ?></h5>
                    <p class='markdown'><?= @$p["description"]; ?>
                    <?php $edit ="update"; ?></p>
                   
                </div>
            </div>
        <?php }
        else if($v%2 == 0) {
            //Si c'est pair ?>

            <div class="row timeline">
                    <div class="col-2">
                      <div class="corner top-right"></div>
                    </div>
                    <div class="col-8">
                      <hr>
                    </div>
                    <div class="col-2">
                      <div class="corner left-bottom"></div>
                    </div>
            </div>

            <div class="row align-items-center justify-content-end how-it-works">
                <div class="col-6 text-right">
                    <h5><?= @$p["name"]; ?></h5>
                    <p class='markdown'> <?= @$p["description"]; ?>
                    <?php $edit ="update"; ?>
                </p>
                    
                </div>
                <div class="col-2 text-center full">
                    <div class="circle"><?= $v; ?></div>
                </div>
            </div>
        <?php
        }
        else {
            //Si c'est impair
            ?>
            <div class="row timeline">
                    <div class="col-2">
                      <div class="corner right-bottom"></div>
                    </div>
                    <div class="col-8">
                      <hr>
                    </div>
                    <div class="col-2">
                      <div class="corner top-left"></div>
                    </div>
            </div>
            
            <div class="row align-items-center how-it-works">
                    <div class="col-2 text-center full">
                      <div class="circle">3</div>
                    </div>
                    <div class="col-6">
                      <h5><?= @$p["name"]; ?></h5>
                      <p class='markdown'><?= @$p["description"]; ?>
                    </p>
                    <p>  <?php $edit ="update"; ?></p>
                      
                    </div>
                  </div>
            <?php
        }

        
    }
    else 
        { ?>
        <br />
        <?php  
        $edit ="create";
        }

        echo $this->renderPartial("costum.views.tpls.openFormBtn",
                                array(
                                    'edit' => $edit,
                                    'tag' => 'wastep'.$v,
                                    'id' => (string)@$p["_id"]
                                 ),true);
        
        

        ?>
<?php 
}
echo $this->renderPartial("costum.views.tpls.dynFormCostumCMS"); 
    ?>

<script type="text/javascript">
jQuery(document).ready(function() {
    mylog.log("render","costum.views.tpls.wizard");
    $.each($(".markdown"), function(k,v){
        descHtml = dataHelper.markdownToHtml($(v).html()); 
        $(v).html(descHtml);
    });
});
        

        //OPTIM : ce code est répété autant de fois qu'il y a de btn 
    //il devrait etre sur l'appelant mais du il sera répété un peu partout 
    
    
    
    $(".editThisBtn").off().on("click",function (){
        mylog.log("editThisBtn");
        var id = $(this).data("id");
        var type = $(this).data("type");
        dyFObj.editElement(type,id,null,dynFormCostumCMS)
    });
    $(".createBlockBtn").off().on("click",function (){
        mylog.log("createBtn");
        dyFObj.openForm('poi',null,{structags:$(this).data("tag") ,type:'cms'},null,dynFormCostumCMS)
    });

    $(".deleteThisBtn").off().on("click",function (){
        mylog.log("deleteThisBtn click");
          $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
          var btnClick = $(this);
          var id = $(this).data("id");
          var type = $(this).data("type");
          var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
          
          bootbox.confirm(trad.areyousuretodelete,
            function(result) 
            {
                if (!result) {
                  btnClick.empty().html('<i class="fa fa-trash"></i>');
                  return;
                } else {
                  $.ajax({
                        type: "POST",
                        url: urlToSend,
                        dataType : "json"
                    })
                    .done(function (data) {
                        if ( data && data.result ) {
                          toastr.info("élément effacé");
                          $("#"+type+id).remove();
                        } else {
                           toastr.error("something went wrong!! please try again.");
                        }
                    });
                }
            });

    });
    </script>
</div>