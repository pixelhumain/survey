
<?php
$initAnswerFiles=Document::getListDocumentsWhere(array(
    "id"=>(string)$answer["_id"],
    "type"=>'answers'), "image");

$depimg = [];
foreach ($initAnswerFiles as $key4 => $d4) {
    if(isset($d4["imageKey"])) {
        $depimg[$d4["imageKey"]] = $d4;
    }
}

$colspanplus  = 0 ;
if( $mode != "pdf" && ( $canAdminAnswer === true || $canEdit === true ) )
    $colspanplus  = 2;

?>
<style type="text/css">
    .funding_body_depense {
        background-color: #f5f5f5;
        padding: 20px;
    }

    .answer-depense-img {
        height: 140px;
    }

    .funding_body_table {
        background-color: #fff;
    }

    .funding_body_table_thead {
        color: #7da53d;
    }

    .btn-group.depenseoceco {
        display: flex;
    }

    .depenseoceco .btn {
        flex: 1
    }

    .pr_div {
        display: flex;
    }

    .pr_div table {
        flex: 1
    }


    .mr-3, .mx-3 {
        margin-right: 1rem !important;
    }

    .media-body {
        -webkit-box-flex: 1;
        -ms-flex: 1;
        flex: 1;
    }

    .mb-0, .my-0 {
        margin-bottom: 0 !important;
    }

    .align-items-center {
        -webkit-box-align: center !important;
        -ms-flex-align: center !important;
        align-items: center !important;
    }
    .selected-price {
        background-color:#e5ffe5;
    }

</style>

<div class="single_funding ">
    <div class="funding_header padding-15">
        <h4> <?php echo !empty($label) ? $label : "Ligne de dépense" ?>
            <span class="dropdown margin-left-30 budgetdropdown">
                <button class="btn btn-secondary dropdown-toggle margin-bottom-10" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-ellipsis-v "></i>
                </button>
                <div class="dropdown-menu pull-right" aria-labelledby="dropdownMenuButton">
                    <?php echo $editBtnL.$editParamsBtn ?>
                </div>
            </span>

            <span>
                <i class="fa fa-euro"></i>
            </span>
        </h4>
    </div>
    <div class="funding_body funding_body_depense">
        <div class="panel-body no-padding">
            <div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding ">
                <div class="alert alert-success alert-content-information hidden">

                </div>
                <table class="table table-bordered funding_body_table" id="editableTable">
                    <?php if (!empty($answer["answers"]["aapStep1"][$key])) { ?>
                        <thead class="funding_body_table_thead">
                        <tr class="line-dark">
                            <?php

                            foreach ($properties as $i => $inp) {
                                echo "<th>".$inp["placeholder"]."</th>";
                            } ?>

                            <?php

                            if(	$mode != "pdf" && !empty($canAdminAnswer) && $canAdminAnswer === true ){?>
                                <th></th>
                            <?php } ?>
                        </tr>
                        </thead>
                    <?php } ?>
                    <tbody style="color: #3f4e58;">
                    <?php
                    $totalPrice = 0;
                    $totalSubvention = 0;
                    $showSubvetion = false;
                    if (!empty($answer["answers"]["aapStep1"][$key]))
                    {

                        $icont = 1;
                        foreach ($answer["answers"]["aapStep1"][$key] as $depId => $dep)
                        {
                            if(!empty($dep["price"]))
                                $totalPrice += (float) $dep["price"];
                            ?>
                            <tr data-id="<?= $kunik ?><?= $icont ?>">
                                <?php
                                foreach ($properties as $i => $inp) {
                                    ?>
                                    <td data-field="<?= $i ?>">
                                        <?php
                                        if( $i == "price" && !empty($dep[$i]) && is_numeric($dep[$i])  ) {
                                            ?>
                                            <span id='price<?= $depId ?>' > <?php echo trim(strrev(chunk_split(strrev($dep["price"]),3, ' '))) ?>€</span>

                                            <?php
                                            $tds = "";
                                            if( $paramsData["estimate"] )
                                            {
                                                $estc = 0;
                                                $labelest = "aucun proposition";
                                                if( isset($dep["estimates"] ))
                                                {
                                                    $estc = sizeof($dep["estimates"]);
                                                    if ($estc != 0) {
                                                        $labelest = 'Voir les proposition('.$estc.')';
                                                    }
                                                }
                                                // $tds .= "<a href='javascript:;' data-id='".$answer["_id"]."' data-key='".$key."' data-form='".$form["id"]."' data-pos='".$q."'  class='btn btn-xs btn-primary btnEstimate margin-left-5 padding-10'><i class='fa fa-plus'></i></a>";

                                                $tds .= '<div class="btn-group depenseoceco">';

                                                if ($canEdit && ( $mode == "w" || $mode == "fa") ) {
                                                    $tds .= '<button data-id="' . $answer['_id'] . '" data-key="' . $key . '" data-form="' . $form['id'] . '" data-kunik="' . $kunik . '" data-pos="' . $depId . '" type="button" class="btn btn-primary newbtnestimate  btn-xs"> Ajouter </button>';
                                                }
                                                $tds .= '<button type="button" class="btn btn-secondary btn-xs espull-me" data-kunik="' . $kunik . '" data-len="'.$estc.'" data-pull="'.$answer["_id"].'estable-'.$depId.$kunik.'" >'.$labelest.'</button>
								</div>';

                                                if( isset($dep["estimates"] ))
                                                {
                                                    $tds .= '<div style="display:none"  class="pr_div '.$answer["_id"].'estable-'.$depId.$kunik.'"> <table ><tbody class="oceco-blanc-table">';
                                                    foreach ( $dep["estimates"] as $uid => $esti )
                                                    {

                                                        $selected = ( isset($esti["selected"]) && $esti["selected"] === true ) ? "success" : "default";
                                                        $btn_label_s = ( isset($esti["selected"]) && $esti["selected"] === true ) ? '<i class="fa fa-check-circle checkedvalue pull-left success" aria-hidden="true"></i>' : "choisir";
                                                        $trcolor = ( isset($esti["selected"]) && $esti["selected"] === true ) ? "selected-price" : "";
                                                        $tds .= '<tr class="'.$trcolor.'">
										<td class="py-3">
											<div class="media align-items-center">
																
												<div class="media-body">
												<h4 class="font-weight-normal mb-0">'.trim(strrev(chunk_split(strrev($esti["price"]),3, ' '))).'€ </h4>
												<small class="text-muted">'.$esti["name"].'</small>
												</div>
											</div>
                          				</td>
                          <td class="py-3">'.@$esti["days"].'j</td>';
                                                        if (($mode == "fa" || $mode == "w")	&& $canAdminAnswer) {
                                                            $tds .= '<td class="py-3"><a href="javascript:;" data-id="' . $answer['_id'] . '" data-uid="' . $uid . '" data-price="' . $esti["price"] . '" data-key="' . $key . '" data-form="' . $form["id"] . '" data-pos="' . $depId . '"  class="btn btn-xs btn-' . $selected . ' btnEstimateSelected margin-left-5 padding-10">' . $btn_label_s . '</a></td>';
                                                        } elseif (isset($esti["selected"]) && $esti["selected"] == true){
                                                            $tds .= '<td class="py-3"><i class="fa fa-check-circle success" aria-hidden="true"></i></td>';
                                                        } else {
                                                            $tds .= '<td></td>';
                                                        }
                                                        if ((Yii::app()->session["userId"] == $uid || (isset($esti["addedBy"]) && Yii::app()->session["userId"]== $esti["addedBy"]))  && ($mode == "fa" || $mode == "w")) {
                                                            $tds .= '<td class="py-3"><div class="esti-btn">
                          		<button type="button"  class="td-datetimelabel2  estibtnedit" data-id="'.$answer['_id'].'" data-uid="'.$uid.'" data-uname="'.$esti["name"].'" data-price="'.$esti["price"].'" data-days="'.@$esti["days"].'" data-key="'.$key.'" data-form="'.$form["id"].'" data-pos="'.$depId.'" ><i class="fa fa-pencil"></i></button><span class = "dv"></span>
                          		<button type="button" class="td-datetimelabel2  estibtndelete" data-id="'.$answer['_id'].'" data-uid="'.$uid.'" data-uname="'.$esti["name"].'" data-price="'.$esti["price"].'" data-key="'.$key.'" data-form="'.$form["id"].'" data-pos="'.$depId.'" ><i class="fa fa-trash"></i></button>
	                            </div></td>';
                                                        }else {
                                                            $tds .= '<td class="py-3"><div class="esti-btn"></div></td>';
                                                        }

                                                        $tds .= '</tr>';


                                                    }
                                                    $tds .= '</tbody></table></div>';
                                                }
                                            }
                                            echo $tds;

                                        }elseif($i == "image") {

                                            if(!empty($dep[$i]) && !empty($depimg[$dep[$i]])){
                                            ?>
                                                <div style="width: 100%;" class='col-xs-12 col-sm-4 padding-5 shadow2 margin-top-5 margin-bottom-5' >
                                                    <img class="answer-depense-img" height="140" src="<?php echo $depimg[$dep[$i]]["imagePath"] ?>" >
                                                </div>
                                            <?php
                                            } else {
                                                ?>
                                                <div style="width: 100%;" class='col-xs-12 col-sm-4 padding-5 shadow2 margin-top-5 margin-bottom-5' >
                                                    <img height="140" src="<?php echo Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl()."/images/thumbnail-default.jpg"; ?>" class="answer-depense-img">
                                                </div>
                                            <?php
                                            }
                                        }elseif(isset($dep[$i])){
                                            if($i == "poste" && (strpos(strtolower($dep[$i]), "subvention aap politique de la ville") !== false )){
                                                $showSubvetion = true;
                                                $totalSubvention += (float) @$answer["answers"]["aapStep1"][$key][$depId]["price"];
                                            }
                                            ?>
                                            <?= $dep[$i] ?>

                                            <?php
                                        }
                                        ?>

                                    </td>
                                    <?php
                                }
                                ?>
                                <td>
                                    <?php
                                    if($mode == "w" && ( $canAdminAnswer === true || $canEdit === true ) ){
                                        echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
                                            "canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
                                            "id" => (string) $answer["_id"],
                                            "collection" => Form::ANSWER_COLLECTION,
                                            "q" => $depId,
                                            "path" => /*$answerPath.$depId,*/"answers.aapStep1.".$key.".".$depId,
                                            "kunik"=>$kunik,
                                            "deleteClass"=> "deleteLineBudget",
                                            "actionid"=>@$actions[$lastkeyactions]["_id"]
                                        ] );
                                    } ?>

                                    <a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$depId ?>', '<?php echo @$dep['step'] ?>','null','null',{notCloseOpenModal : true})">
                                        <?php
                                        echo PHDB::count(Comment::COLLECTION, array("contextId"=>(string)$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$depId)); ?>
                                        <i class='fa fa-commenting'></i></a>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    <tr style="background-color: gainsboro;">
                        <td class="<?= isset($properties["group"]) ? "":"hidden"?>">&nbsp;</td>
                        <td class="<?= isset($properties["nature"]) ? "":"hidden"?>">&nbsp;</td>
                        <td class="text-bold"> <span class="pull-right">Total : </span></td>
                        <td class="text-bold"><?= trim(strrev(chunk_split(strrev($totalPrice),3, ' '))) ?>€</td>
                    </tr>
                    <?php if($showSubvetion){ ?>
                        <tr>
                            <td colspan="4" class="text-bold">
                                <i class="pull-left alert alert-success" style="width: 100%;">L'association sollicite une subvention totale de <span class="text-bold"><?= $totalSubvention ?> € </span></i>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>


            </div>

        </div>
    </div>

</div>

<script type="text/javascript">
    $(document).ready(function() {

        <?php if (empty($loadbysuivi) || (!empty($loadbysuivi) && !$loadbysuivi)){ ?>

        $( '.espull-me[data-kunik="<?= $kunik ?>"]' ).each(function( index ) {

            if (typeof $('.'+$(this).data('pull'))[0] != "undefined") {
                $('.'+$(this).data('pull'))[0].style.display = localStorage[$(this).data('pull')];
            }

            if (typeof $('.'+$(this).data('pull'))[0] != "undefined") {
                if (($('.'+$(this).data('pull'))[0].style.display == "flex" || $('.'+$(this).data('pull'))[0].style.display == "") && parseInt($(this).data('len')) > 0) {
                    $(this).html("Reduire les propositions("+$(this).data('len')+")");
                }else if ($('.'+$(this).data('pull'))[0].style.display == "none"  && parseInt($(this).data('len')) > 0) {
                    $(this).html("Voir les propositions("+$(this).data('len')+")");
                } else {
                    $(this).html("aucun resultat");
                }
            }

        });


        $('.espull-me[data-kunik="<?= $kunik ?>"]').off().click(function(e) {
            e.stopPropagation();
            var  thisdatapull = $(this).data('pull');
            var thisbtnpull = $(this);

            var  thisdatapull = $(this).data('pull');
            $('.'+$(this).data('pull')).slideToggle("fast", function(){
                localStorage.setItem(thisdatapull, $('.'+thisdatapull)[0].style.display);

                if (($('.'+thisbtnpull.data('pull'))[0].style.display == "flex" || $('.'+thisbtnpull.data('pull'))[0].style.display == "") && parseInt(thisbtnpull.data('len')) > 0) {
                    thisbtnpull.html("Reduire les propositions("+thisbtnpull.data('len')+")");
                } else if ($('.'+thisbtnpull.data('pull'))[0].style.display == "none" && parseInt(thisbtnpull.data('len')) > 0) {
                    thisbtnpull.html("Voir les propositions("+thisbtnpull.data('len')+")");
                } else {
                    thisbtnpull.html("aucun resultat");
                }
            });
        });

        <?php } ?>
    });

    /*    (function($, window, document, undefined) {
            var pluginName = "editable",
                defaults = {
                    keyboard: true,
                    dblclick: true,
                    button: true,
                    buttonSelector: ".edit",
                    maintainWidth: true,
                    dropdowns: {},
                    edit: function() {},
                    save: function() {},
                    cancel: function() {}
                };

            function editable(element, options) {
                this.element = element;
                this.options = $.extend({}, defaults, options);

                this._defaults = defaults;
                this._name = pluginName;

                this.init();
            }

            editable.prototype = {
                init: function() {
                    this.editing = false;

                    if (this.options.dblclick) {
                        $(this.element)
                            .css('cursor', 'pointer')
                            .bind('dblclick', this.toggle.bind(this));
                    }

                    if (this.options.button) {
                        $(this.options.buttonSelector, this.element)
                            .bind('click', this.toggle.bind(this));
                    }
                },

                toggle: function(e) {
                    e.preventDefault();

                    this.editing = !this.editing;

                    if (this.editing) {
                        this.edit();
                    } else {
                        this.save();
                    }
                },

                edit: function() {
                    var instance = this,
                        values = {};

                    $('td[data-field]', this.element).each(function() {
                        var input,
                            field = $(this).data('field'),
                            value = $(this).text(),
                            width = $(this).width();

                        values[field] = value;

                        $(this).empty();

                        if (instance.options.maintainWidth) {
                            $(this).width(width);
                        }

                        if (field in instance.options.dropdowns) {
                            input = $('<select></select>');

                            for (var i = 0; i < instance.options.dropdowns[field].length; i++) {
                                $('<option></option>')
                                    .text(instance.options.dropdowns[field][i])
                                    .appendTo(input);
                            };

                            input.val(value)
                                .data('old-value', value)
                                .dblclick(instance._captureEvent);
                        } else {
                            input = $('<input type="text" />')
                                .val(value)
                                .data('old-value', value)
                                .dblclick(instance._captureEvent);
                        }

                        input.appendTo(this);

                        if (instance.options.keyboard) {
                            input.keydown(instance._captureKey.bind(instance));
                        }
                    });

                    this.options.edit.bind(this.element)(values);
                },

                save: function() {
                    var instance = this,
                        values = {};

                    $('td[data-field]', this.element).each(function() {
                        var value = $(':input', this).val();

                        values[$(this).data('field')] = value;

                        $(this).empty()
                            .text(value);
                    });

                    this.options.save.bind(this.element)(values);
                },

                cancel: function() {
                    var instance = this,
                        values = {};

                    $('td[data-field]', this.element).each(function() {
                        var value = $(':input', this).data('old-value');

                        values[$(this).data('field')] = value;

                        $(this).empty()
                            .text(value);
                    });

                    this.options.cancel.bind(this.element)(values);
                },

                _captureEvent: function(e) {
                    e.stopPropagation();
                },

                _captureKey: function(e) {
                    if (e.which === 13) {
                        this.editing = false;
                        this.save();
                    } else if (e.which === 27) {
                        this.editing = false;
                        this.cancel();
                    }
                }
            };

            $.fn[pluginName] = function(options) {
                return this.each(function() {
                    if (!$.data(this, "plugin_" + pluginName)) {
                        $.data(this, "plugin_" + pluginName,
                            new editable(this, options));
                    }
                });
            };

        })(jQuery, window, document);

        editTable();

        //custome editable starts
        function editTable(){

            $(function() {
                var pickers = {};

                $('table tr').editable({
                    dropdowns: {
                        sex: ['Male', 'Female']
                    },
                    edit: function(values) {
                        $(".edit i", this)
                            .removeClass('fa-pencil')
                            .addClass('fa-save')
                            .attr('title', 'Save');

                        pickers[this] = new Pikaday({
                            field: $("td[data-field=birthday] input", this)[0],
                            format: 'MMM D, YYYY'
                        });
                    },
                    save: function(values) {
                        $(".edit i", this)
                            .removeClass('fa-save')
                            .addClass('fa-pencil')
                            .attr('title', 'Edit');

                        if (this in pickers) {
                            pickers[this].destroy();
                            delete pickers[this];
                        }
                    },
                    cancel: function(values) {
                        $(".edit i", this)
                            .removeClass('fa-save')
                            .addClass('fa-pencil')
                            .attr('title', 'Edit');

                        if (this in pickers) {
                            pickers[this].destroy();
                            delete pickers[this];
                        }
                    }
                });
            });

        }

        $(".add-row").click(function(){
            $("#editableTable").find("tbody tr:first").before("<tr><td data-field='name'></td><td data-field='name'></td><td data-field='name'></td><td data-field='name'></td><td><a class='button button-small edit' title='Edit'><i class='fa fa-pencil'></i></a> <a class='button button-small' title='Delete'><i class='fa fa-trash'></i></a></td></tr>");
            editTable();
            setTimeout(function(){
                $("#editableTable").find("tbody tr:first td:last a[title='Edit']").click();
            }, 200);

            setTimeout(function(){
                $("#editableTable").find("tbody tr:first td:first input[type='text']").focus();
            }, 300);

            $("#editableTable").find("a[title='Delete']").unbind('click').click(function(e){
                $(this).closest("tr").remove();
            });

        });

        function myFunction() {

        }

        $("#editableTable").find("a[title='Delete']").click(function(e){
            var x;
            if (confirm("Are you sure you want to delete entire row?") == true) {
                $(this).closest("tr").remove();
            } else {

            }
        });*/

</script>
