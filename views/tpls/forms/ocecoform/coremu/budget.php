<?php
HtmlHelper::registerCssAndScriptsFiles(["/css/aap/proposition.css"], Yii::app()->getModule('costum')->assetsUrl);
HtmlHelper::registerCssAndScriptsFiles(
    [
        '/plugins/d3/d3-v5/d3.min.js',
        '/plugins/d3/sankey/d3-sankey.min.js',
        '/plugins/velocity/jquery.velocity.js',
        '/plugins/microtip/microtip.css',
    ],
    Yii::app()->request->baseUrl);

 HtmlHelper::registerCssAndScriptsFiles(array(
     '/js/coremu.js',
     '/js/payement_generique.js',
 ), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );

if ($answer)
{
    $isAapProject = $isAap;
    $contextId = $ctxtid = $context["_id"];
    $contextType = $ctxttype = $parentForm["parent"][(string)$context["_id"]]["type"];

    $debug = false;
    $editBtnL = ($canEdit == true && ($mode == "w" || $mode == "fa")) ? " <a href='javascript:;' data-id='" . $answer["_id"] . "' data-collection='" . Form::ANSWER_COLLECTION . "' data-path='" . $answerPath . "' class='add" . $kunik . " btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a> <button id='addfromcsv' data-id='" . $answer["_id"] . "' data-collection='" . Form::ANSWER_COLLECTION . "' data-path='" . $answerPath . "' class=' btn btn-default'><i class='fa fa-plus'></i> Ajouter ligne via csv </button>" : "";

    $editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='" . $parentForm["_id"] . "' data-collection='" . Form::COLLECTION . "' data-path='params." . $kunik . "' class='previewTpl edit" . $kunik . "Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>".$editQuestionBtn : "";

    $paramsData = [
        "group" => ["Feature", "Costum", "Chef de Projet", "Data", "Mantenance"],
        "nature" => ["investissement", "fonctionnement"],
        "amounts" => ["price" => "Montant"],
        "estimate" => true,
        "activeGroup" => false,
        "activeNature" => false,
        "fielLabel" => "Ligne de dépense",
        "prefilledDepense" => array(),
        "parentForm" => $parentForm
    ];

    if (isset($parentForm["params"][$kunik]["group"])) $paramsData["group"] = $parentForm["params"][$kunik]["group"];
    if (isset($parentForm["params"][$kunik]["nature"])) $paramsData["nature"] = $parentForm["params"][$kunik]["nature"];
    if (isset($parentForm["params"][$kunik]["amounts"])) $paramsData["amounts"] = $parentForm["params"][$kunik]["amounts"];
    if (isset($parentForm["params"][$kunik]["estimate"])) $paramsData["estimate"] = Answer::is_true($parentForm["params"][$kunik]["estimate"]);
    if (isset($parentForm["params"][$kunik]["activeGroup"])) $paramsData["activeGroup"] = Answer::is_true($parentForm["params"][$kunik]["activeGroup"]);
    if (isset($parentForm["params"][$kunik]["activeNature"])) $paramsData["activeNature"] = Answer::is_true($parentForm["params"][$kunik]["activeNature"]);
    if (isset($input["label"])) $paramsData["fielLabel"] = $input["label"];
    if (isset($parentForm["params"][$kunik]["prefilledDepense"])) $paramsData["prefilledDepense"] = $parentForm["params"][$kunik]["prefilledDepense"];


    $properties = ["group" => ["placeholder" => "Groupé", "inputType" => "tags", "tagsList" => "budgetgroupe", "rules" => ["required" => true], "maximumSelectionLength" => 3], "nature" => ["placeholder" => "Nature de l’action", "inputType" => "tags", "tagsList" => "budgetnature", "rules" => ["required" => true], "maximumSelectionLength" => 3], "poste" => ["inputType" => "text", "label" => "Poste de dépense", "placeholder" => "Poste de dépense", "rules" => ["required" => true]] , "tags" => ["placeholder" => "tags", "inputType" => "tags",  "maximumSelectionLength" => 3], "description" => ["placeholder" => "description", "inputType" => "textarea"]];
    if(!$paramsData["activeGroup"]) unset($properties["group"]);
    if(!$paramsData["activeNature"]) unset($properties["nature"]);
    foreach ($paramsData["amounts"] as $k => $l)
    {
        $properties[$k] = ["inputType" => "number", "label" => $l, "propType" => "amount", "placeholder" => $l,
        ];
    }
    if ($debug) var_dump($answers);
    if ($debug) var_dump($paramsData);

    $ignore = array('_file_', '_params_', '_obInitialLevel_' ,'ignore');
    $params = array_diff_key(get_defined_vars() + array_flip($ignore), array_flip($ignore));

    echo $this->renderPartial("survey.views.tpls.forms.ocecoform.coremu.budgetTable", $params , true);
?>
    <style type="text/css">
        :root {
            --coremu-primary-orange-color : #f2aa2e;
        }

        .co-popup-payementlist-menu {
            background-color: #d8f4f0 !important;
            color: #000;
        }

        .co-popup-payementlist-container {
            width: 100%;
            height: 100vh;
            position: fixed;
            top: 0;
            left: 0;
            background-color: rgba(0, 0, 0, .5);
            display: flex;
            justify-content: center;
            align-items: center;
            color: #2c3e50;
            z-index: 9999999;
            padding: 1% 20%;
            margin-left: 0 !important;
        }

        .co-popup-cofinance-container {
            width: 100%;
            height: 100vh;
            position: fixed;
            top: 0;
            left: 0;
            background-color: rgba(0, 0, 0, .5);
            display: flex;
            justify-content: center;
            align-items: center;
            color: #2c3e50;
            z-index: 199999;
        }

        .co-popup-payementlist {
            /* min-width: 700px; */
            min-height: 400px;
            background-color: white;
            border-radius: 10px;
            position: relative;
            font-family: "montserrat" !important;
            width: 100%;
        }

        .co-popup-payementlist-content, .co-popup-payementlist-finish {
            transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
            width: 700px;
            height: 387px;
        }

        .co-popup-payementlist-header {
            padding-top: 20px;
            text-align: center;
        }

        .co-popup-payementlist-header h2 {
            font-weight: bold;
        }

        .co-popup-payementlist-header p {
            font-size: 14px;
            line-height: 1.8;
        }

        .co-popup-payementlist-header p a {
            text-decoration: none;
            font-weight: bold;
            color: #46c6b5;
            text-decoration: underline;
        }

        .co-popup-payementlist-actions {
            display: flex;
            flex-direction: row;
            padding-top: 50px;
            align-items: center;
            justify-content: center;
            margin: 0;
            padding: 0;
            margin-top: 20px;
        }

        .co-popup-payementlist-actions li {
            display: flex;
            margin-bottom: 5px;
        }

        .co-popup-payementlist-actions li a {
            text-decoration: none;
            padding: 10px 20px;
            border-radius: 4px;
            color: #2c3e50;
            font-size: 18px;
        }

        .co-popup-payementlist-actions li a span {
            padding-right: 10px;
        }

        .co-popup-payementlist-actions li:first-child a {
            background-color: #9fbd38;
            color: white;
            font-weight: bold;
        }

        .co-popup-payementlist-actions button {
            width: fit-content;
            margin-top: 10px;
            background-color: transparent;
            font-size: 18px;
            color: #2c3e50;
        }

        .co-popup-payementlist-background {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }

        .co-popup-payementlist-content, .co-popup-payementlist-finish {
            transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
            position: relative;
            padding: 20px;
        }

        .co-popup-payementlist-particle {
            width: 100%;
            height: 100%;
        }

        .co-popup-payementlist-container .ap-starter-btn-close {
            background-color: #2c3e50;
            color: white !important;
            margin-left: 5px;
        }

        .co-popup-payementlist-header {
            background-color: #fff !important;
        }

        @media (max-width: 720px) {
            .co-popup-payementlist-actions {
                flex-direction: column;
            }
        }

        .co-popup-payementlist-question .wrapper {
            display: flex;
            flex-direction: column;
        }

        .co-popup-payementlist-question .grid-row {
            margin-bottom: 1em
        }

        .co-popup-payementlist-question .grid-row, .grid-header {
            display: flex;
            /*   flex: 1 0 auto; */
            /*   height: auto; */
        }

        .co-popup-payementlist-question .grid-header {
            align-items: flex-end;
        }

        .co-popup-payementlist-question .header-item {
            width: 100px;
            text-align: center;
            /*   border:1px solid transparent; */
        }

        .co-popup-payementlist-question .header-item:nth-child(1) {
            width: 180px;
        }

        .co-popup-payementlist-question .subtitle {
            font-size: 0.7em;
        }

        .co-popup-payementlist-question .flex-item:before {
            content: '';
            padding-top: 26%;
        }

        .co-popup-payementlist-question .flex-item {
            display: flex;
            /*   flex-basis:25%; */
            width: 100px;
            border-bottom: 1px solid #ccc;
            justify-content: center;
            align-items: center;
            /*   text-align:left; */
            font-size: 1em;
            font-weight: normal;
            color: #999;
        }

        .co-popup-payementlist-question .flex-item:nth-child(1) {
            border: none;
            font-size: 1.15em;
            color: #000;
            width: 180px;
            justify-content: left;
        }

        .co-popup-payementlist-question [type="radio"]:not([hidden]), .co-popup-payementlist-question [type="checkbox"]:not([hidden]) {
            border: 0;
            clip: rect(0 0 0 0);
            height: 1px;
            margin: -1px;
            overflow: hidden;
            padding: 0;
            position: absolute;
            width: 1px;
        }

        .co-popup-payementlist-question label {
            /*display: block;*/
            cursor: pointer;
        }

        .co-popup-payementlist-question [type="radio"]:not([hidden])+span:before {
            content: '';
            display: inline-block;
            width: 1.5em;
            height: 1.5em;
            vertical-align: -0.25em;
            border-radius: 50%;
            border: 0.125em solid #fff;
            box-shadow: 0 0 0 0.15em #555;
            transition: 0.5s ease all;
        }

        .co-popup-payementlist-question [type="checkbox"]:not([hidden])+span:before {
            content: '';
            display: inline-block;
            width: 1.5em;
            height: 1.5em;
            vertical-align: -0.25em;
            border-radius: .25em;
            border: 0.125em solid #fff;
            box-shadow: 0 0 0 0.15em #555;
            transition: 0.5s ease all;
        }

        .co-popup-payementlist-question [type="checkbox"]:not([hidden])+span:before,
        .co-popup-payementlist-question [type="radio"]:not([hidden])+span:before {
            margin-right: 0.75em;
        }

        .co-popup-payementlist-question [type="radio"]:not([hidden]):checked+span:before,
        .co-popup-payementlist-question [type="checkbox"]:not([hidden]):checked+span:before {
            background: #46c6b5;
            box-shadow: 0 0 0 0.25em #666;
        }

        .co-popup-payementlist-question [type="radio"]:not([hidden]):focus span:after {
            content: '\0020\2190';
            font-size: 1.5em;
            line-height: 1;
            vertical-align: -0.125em;
        }

        .co-popup-payementlist-question fieldset {
            font-size: 1em;
            border: 2px solid #000;
            padding: 2em;
            border-radius: 0.5em;
            margin-bottom: 20px;
        }

        .swiper-slide-next {
            display: none !important;
        }

        .co-popup-payementlist-menu {
            background-color: #46c6b5;
            height: 100%;
            border-top-left-radius: 10px;
            border-bottom-left-radius: 10px;
        }

        .co-popup-payementlist-question-container {
            height: 600px;
            /* width: 1000px; */
            position: relative;
        }

        /*payementlist Timeline */
        ul.timeline.aap-payementlistListStepSwipping {
            transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
            list-style-type: none;
            position: relative;
            padding: 0px;
        }

        ul.timeline.aap-payementlistListStepSwipping .active .name-step {
            color: #43c9b7 !important;
        }

        ul.timeline.aap-payementlistListStepSwipping:before {
            content: ' ';
            background: #fff;
            display: inline-block;
            position: absolute;
            left: 10px;
            width: 2px;
            height: 100%;
            z-index: 400;
        }

        ul.timeline.aap-payementlistListStepSwipping>li {
            margin: 10px 0;
            padding-left: 30px;
            float: none;
        }

        ul.timeline.aap-payementlistListStepSwipping>li:before {
            content: ' ';
            background: #fff;
            display: inline-block;
            position: absolute;
            border-radius: 50%;
            border: 3px solid #46c6b5;
            left: 1px;
            width: 18px;
            height: 18px;
            z-index: 400;
        }

        ul.timeline.aap-payementlistListStepSwipping>li.active:before {
            content: ' ';
            background: #46c6b5;
            display: inline-block;
            position: absolute;
            border-radius: 50%;
            border: 3px solid var(--aap-secondary-color);
            left: 1px;
            width: 18px;
            height: 18px;
            z-index: 400;
        }


        .timeline.aap-payementlistListStepSwipping>li .name-step {
            display: block;
            max-width: 100%;
            margin-bottom: 5px;
            font-weight: 700;
            font-size: 16px;
            color: #000;
            text-align: left;
        }

        .timeline.aap-payementlistListStepSwipping>li>p {
            font-size: 14px;
            color: #eee;
            grid-template-rows: min-content 1fr;
        }

        .timeline.aap-payementlistListStepSwipping>li>hr {
            width: 100%;
            margin-bottom: 0px;
        }

        .aap-payementlistcontain-info-costum {
            margin-top: 30px;
            margin-bottom: 30px;
        }

        .aap-payementlistcontain-info-costum img.logo-info {
            width: 75px;
            height: 75px;
            border: solid 2px rgba(255, 255, 255);
            background: white;
            border-radius: 50%;
        }

        .aap-payementlistcontain-info-costum .title-info {
            font-size: 18px;
            color: #43c9b7;
        }

        .co-popup-payementlist-question .swiper-slide-:not(:last-child) {
            visibility: hidden;
        }

        .co-popup-payementlist-question .swiper-slide-:not(:last-child) div {
            display: none;
        }

        .li-start-payementlist {
            background-color: var(--aap-primary-color);
            border-radius: 4px;
        }

        .co-popup-payementlist-container input.checkbox-payementlist-start:checked+div div+div ul .li-start-payementlist {
            transition: all 0.7s cubic-bezier(0.5, 0.005, 0.075, 0.985);
            width: 100%;
            left: 0;
            height: 100%;
        }

        .co-popup-payementlist-container .li-start-payementlist {
            transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
            width: 260px;
            left: 0;
            height: 100%;
        }

        .co-popup-payementlist-container input.checkbox-payementlist-start:checked+div div+div ul .li-abord-payementlist {
            transition: all 0.7s cubic-bezier(0.5, 0.005, 0.075, 0.985);
            width: 0%;
            opacity: 0;
        }

        .co-popup-payementlist-container .li-abord-payementlist {
            transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
            width: 190px;
            opacity: 1;
        }

        .co-popup-payementlist-container input.checkbox-payementlist-start:checked+div.co-popup-payementlist-content {
            transition: all 0.7s cubic-bezier(0.5, 0.005, 0.075, 0.985);
            width: 1000px;
            height: 600px;
            padding: 0;
        }

        .co-popup-payementlist-container .li-abord-payementlist {
            transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
            width: 190px;
            opacity: 1;
        }

        input.checkbox-payementlist-start:checked+div div+div ul .li-start-payementlist a,
        input.checkbox-payementlist-start:checked+div div+div ul .li-abord-payementlist,
        input.checkbox-payementlist-start:checked+div div .co-popup-payementlist-header-h2,
        input.checkbox-payementlist-start:checked+div div .co-popup-payementlist-header-h4,
        input.checkbox-payementlist-start:checked+div div .co-popup-payementlist-header-p {
            transition: all 0.7s cubic-bezier(0.5, 0.005, 0.075, 0.985);
            opacity: 0;
        }

        input.checkbox-payementlist-start:checked+div div .co-popup-payementlist-header-logo {
            transition: all 0.7s cubic-bezier(0.5, 0.005, 0.075, 0.985);
            float: left;
            margin-left: 130px;
        }

        .li-start-payementlist a,
        .li-abord-payementlist,
        .co-popup-payementlist-header-h2,
        .co-popup-payementlist-header-h4,
        .co-popup-payementlist-header-p {
            transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
            opacity: 1;
        }

        .co-popup-payementlist-container input.checkbox-payementlist-start:checked+div div+div .co-popup-payementlist-actions {
            transition: all 0.7s cubic-bezier(0.5, 0.005, 0.075, 0.985);
            width: 33.33333333%;
            left: 0;
            height: 100%;
            margin: 0;
        }

        .co-popup-payementlist-container input.checkbox-payementlist-start:checked+div div+div.co-popup-payementlist-header-action {
            transition: all 0.7s cubic-bezier(0.5, 0.005, 0.075, 0.985);
            height: 100%;
        }

        .co-popup-payementlist-container co-popup-payementlist-header-action {
            height: 45px;
        }

        .co-popup-payementlist-container input.checkbox-payementlist-start:checked+div div.co-popup-payementlist-header {
            transition: all 0.7s cubic-bezier(0.5, 0.005, 0.075, 0.985);
            background-color: transparent !important;
            position: absolute;
        }

        .co-popup-payementlist-container .co-popup-payementlist-actions {
            transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
            width: 100%;
            left: 0;
            height: 100%;
        }

        .co-popup-payementlist-container .co-popup-payementlist-header-logo img.logo-info {
            width: 75px;
            height: 75px;
            border: solid 2px var(--aap-primary-color);
            border-radius: 50%;
        }

        .co-popup-payementlist-container .co-popup-payementlist-menu legend {
            font-size: 25px;
        }

        .co-popup-payementlist-container .co-popup-payementlist-center {
            margin: 0;
            top: 50%;
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
        }

        .co-popup-payementlist-container .co-popup-payementlist-normal {
            max-height: 470px;
            overflow-y: auto;
        }

        .co-popup-payementlist-container .swiper-slide .wrapper {
            padding: 20px 50px;
            text-align: left;
        }

        .co-popup-payementlist-container .co-popup-payementlist-title {
            font-size: 30px;
            font-weight: bold;
            color: var(--aap-secondary-color)
        }

        .co-popup-payementlist-container .co-popup-payementlist-subtitle {
            font-size: 18px;
            font-weight: bold;
            color: var(--aap-secondary-color)
        }

        .payementlist-info-has-tooltip:hover .payementlist-info-tooltip,
        .payementlist-info-has-tooltip:focus .payementlist-info-tooltip,
        .payementlist-info-has-tooltip.hover .payementlist-info-tooltip {
            opacity: 1;
            rotate: 0deg;
            pointer-events: inherit;
        }

        .co-popup-payementlist-container .payementlist-info-tooltip {
            display: block;
            position: absolute;
            padding: 10px 30px;
            border-radius: 5px;
            background: var(--aap-primary-color);
            text-align: center;
            color: white;
            opacity: 0;
            pointer-events: none;
            z-index: 5;
            bottom: 30%;
            transform: translate(-50%, 0px);
        }

        .co-popup-payementlist-container .payementlist-info-tooltip:hover {
            opacity: 1;
            pointer-events: inherit;
        }

        .co-popup-payementlist-container .payementlist-info-tooltip img {
            max-height: 200px;
        }

        .co-popup-payementlist-container .payementlist-info-tooltip:after {
            content: '';
            display: block;
            margin: 0 auto;
            widtH: 0;
            height: 0;
            border: 5px solid transparent;
            border-top: 5px solid rgba(0, 0, 0, 0.75);
            position: absolute;
            left: 50%;
        }

        .co-popup-payementlist-container .createActivationStepRow {
            padding: 0px 30px;
        }

        .co-popup-payementlist-container .close-payementlist-container, .camp-cart-btn {
            position: absolute;
            /* top: -50%; */
            right: 0;
        }

        .clickable[data-actualstep]:hover {
            cursor: pointer;
        }

        .label-group {
            margin-top: 5px;
        }

        .kanb-filter-tags {
            border-radius: 8px;
            margin-right: 5px;
            font-size: 1em;
            margin-top: 5px;
            cursor: pointer;
            display: inline-block;
        }

        .kanb-filter-tags.label-primary {
            background-color: var(--aap-primary-color);
        }

        .co-popup-payementconfig-container {
            width: 100%;
            height: 100vh;
            position: fixed;
            top: 0;
            left: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            color: #2c3e50;
            z-index: 199999;
            border-radius: 5px;
        }

        .co-popup-payementconfig-container .form-group {
            margin-bottom: 15px;
        }

        .co-popup-payementconfig-container label {
            font-weight: bold;
            display: block;
            margin-bottom: 5px;
        }

        .co-popup-payementconfig-container select, .co-popup-payementconfig-container input {
            width: 100%;
            padding: 10px;
            font-size: 1rem;
            border: 1px solid #ccc;
            border-radius: 4px;
        }

        .co-popup-payementconfig-container .btnpc {
            display: block;
            width: 100%;
            background: #007bff;
            color: #fff;
            font-size: 1rem;
            padding: 10px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            transition: background 0.3s ease;
        }

        .co-popup-payementconfig-container .btnpc:hover {
            background: #0056b3;
        }

        .co-popup-payementconfig-container #methodList li {
            margin-bottom: 10px;
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 4px;
            background: #f9f9f9;
            justify-content: space-between;
            align-items: center;
        }

        .co-popup-payementconfig-container #methodList li button.deletepaymethod {
            background: #d66e6e;
            color: white;
            border: none;
            padding: 5px 10px;
            border-radius: 4px;
            cursor: pointer;
        }

        .co-popup-payementconfig-container #methodList li button.deletepaymethod:hover {
            background: #f25959;
        }

        .co-popup-payementconfig-container .co-popup-cofinance {
            padding: 20px;
            border: #0a0a0b solid;
        }

        .co-popup-payementconfig-container .co-popup-payementconfig-container-close {
            right: 0;
        }

        .co-popup-payementconfig-container .co-popup-cofinance {
            background-color: #fff;
            max-height: 90%;
        }

        .co-popup-payementconfig-container #methodList {
            height: 150px;
            overflow-y: auto;
            list-style: none;
            padding: 0px;
        }

        .co-popup-payementconfig-container .co-popup-cofinance h2 , #paymentList h2 {
            color: #7da53d;
            font-size: 20px;
            display: inline-flex;
            align-items: center;
        }

        .methodtitle {
            font-size: 15px;
            color: #7da53d;
        }

        .methoddetail {
            font-size: 13px;
        }

        .methoddetail-cont {
            display: inline-flex;
            width: 100%;
            justify-content: space-between;
        }

        .co-popup-payementconfig-container #methodList::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.1);
            background-color: #F5F5F5;
            border-radius: 10px;
        }

        .co-popup-payementconfig-container #methodList::-webkit-scrollbar {
            width: 10px;
            background-color: #F5F5F5;
        }

        .co-popup-payementconfig-container #methodList::-webkit-scrollbar-thumb {
            border-radius: 10px;
            background-color: #FFF;
            background-image: -webkit-gradient(linear,
            40% 0%,
            75% 84%,
            from(#bceab5),
            to(#7ee281),
            color-stop(.6, #9eeaa5))
        }

        span.methodactionbtn span {
            color : var(--coremu-primary-orange-color) ;
            font-size: 12px;
        }

        .favoritepayconfigbtn {
            color : var(--coremu-primary-orange-color) ;
            background-color: #fff;
            border: 1px solid;
            padding: 4px;
            font-size: 12px;
            border-radius: 5px;
        }

        #d66e6e !important

        .methodaction {
            display: inline-flex;
            align-content: space-between;
            flex-direction: column;
        }

        .deletepaymethod {
            font-size: 12px;
            background-color: #fff;
            border : solid 1px #d66e6e;
        }

        .methodtitle-cont h3{
            margin: 0;
        }

        .methodtitle-cont{
            display: inline-flex;
            width: 100%;
            justify-content: space-between;
        }

        .methodactiondate {
            font-size: 12px;
        }
    </style>

    <div class="form-group col-xs-12 addfromcsvdiv" style="display: none" >
        <div>
            <label class="info text-center text-dark-blue"> <h4> Importer des lignes de depense depuis un fichier csv </h4></label>
        </div>
        <label for="inputcsvdata" > <input type="file" id="inputcsvdata" class="inputcsvdata" name="inputcsvdata" accept=".csv"> </label>
        <div>
            <div>
                <label class="info text-blue"> Le delimiteur utilisé est le point-virgule <code>;</code></label>
            </div>
            <div>
                <label class="info text-blue"> Le delimiteur de fin de ligne utilisé est le retour à la ligne <code>/n</code></label>
            </div>
            <div>
                <label class="info text-blue"> Exemple </label>
                <div>
                    <code>
                        Data,Feature;fonctionnement;value1;200
                        Data;fonctionnement,improuvement;value2;350
                    </code>
                </div>
            </div>
        </div>

        <div class="addfromcsvdivrs">  </div>
    </div>

    <div class="co-popup-payementlist-container" style="display: none">
        <div class="co-popup-payementlist">
            <input type="checkbox" checked class="checkbox-payementlist-start hide">
            <div class="co-popup-payementlist-question">
                <div class="co-popup-payementlist-question-container" >
                    <div class="cart-stepper-header">
                        <h5 class="">Votre panier</h5>
                        <button class="cart-stepper-close btn btn-default close-payementlist">
                            <i class="fa fa-times"></i> <span class="hidden-xs">Fermer</span>
                        </button>
                    </div>
                    <div class="camp-cart-btn margin-20" style="bottom: 0">
                        <button class="btn btn-default cos-payementlist-next"> Suivant <i class="fa fa-long-arrow-right"></i> </button>
                    </div>
                    <div class="camp-cart-btn margin-20 preced">
                        <button class="btn btn-default cos-payementlist-prev" style="display: none"> <i class="fa fa-long-arrow-left"></i> Précédent </button>
                    </div>
                    <div class="col-lg-4 hidden-md hidden-xs co-popup-payementlist-menu "">
                    <div class="center aap-payementlistcontain-info-costum">
                        <img class="logo-info" src="">
                        <div class="title-info"></div>
                    </div>
                    <ul class="timeline aap-payementlistListStepSwipping">
                        <li class="active" data-method="camp_proc_tl" data-actualstep="tl-list">
                            <div class="name-step">Mon organisation </div>
                            <p> </p>
                        </li>
                        <li data-method="camp_proc_commun" data-actualstep="commun-list">
                            <div class="name-step">Les Coremus</div>
                            <p> </p>
                        </li>
                        <li data-method="camp_proc_payment_method" data-actualstep="payment-method" class="cart-hide-before-date">
                            <div class="name-step">Méthode de paiement</div>
                            <p> </p>
                        </li>
                        <li data-method="camp_proc_info" data-actualstep="bill-info" class="cart-hide-before-date">
                            <div class="name-step">Information de facturation</div>
                            <p> </p>
                        </li>
                        <li data-actualstep="payment" class="cart-hide-before-date">
                            <div class="name-step">Paiement</div>
                            <p> </p>
                        </li>
                    </ul>
                </div>
                <div class="col-md-12 col-lg-8 col-xs-12 co-popup-payementlist-normal" >
                    <div class="" id="">
                        <div class="">
                            <div class="">
                                <div class="modal-body cos-payementlist">
                                    <i class="fa fa-spinner fa-spin"></i> Chargement
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
        $(document.documentElement).on("keyup", function(e) {
            if (typeof e.key === "string" && e.key.toLowerCase() === "escape")
                $(".close-payement").trigger("click");
        });
        $(".co-popup-payement-container").off("click").on("click", function(e) {
            var self;
            self = $(this);
            if ($(e.target).is(self))
                $(".close-payement").trigger("click");
        });
    </script>
    
    <?php
    if ($mode != "r" && $mode != "pdf")
    {
        ?>

        <script type="text/javascript">
            var contextlinks = <?php echo json_encode((isset($get_links)) ? $get_links : null); ?>;
            var <?php echo $kunik ?>Data = <?php echo json_encode((isset($answers)) ? $answers : null); ?>;
            sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;

            function htmlasignprice (labelvalue , pricevalue , number , maxprice , checkvalue , nh , th ) {
                let setmax  ="";
                if (maxprice !== null) {
                    setmax = 'max="'+maxprice+'"'
                }
                if(th == 0){
                    th = 30;
                }

                return '<tr class="padding-10 rowprice" data-line="'+number+'">' +
                    '            <td class="">' +
                    '                    <input type="text" value="'+labelvalue+'" class="form-control arrayprice-label" aria-label="label">' +
                    '             </td>' +
                    '            <td class="">' +
                    '                    <input type="number" data-line="'+number+'" min="0" value="'+th+'" class="form-control arrayprice-th" aria-label="price">' +
                    '            </td>' +
                    '            <td class="">' +
                    '                    <input type="number" data-line="'+number+'" '+setmax+' min="0" value="'+nh+'" class="form-control arrayprice-nh" aria-label="Nombre d\'heure">' +
                    '            </td>' +
                    '            <td class="">' +
                    '                    <input disabled type="number" data-line="'+number+'" '+setmax+' min="0" value="'+pricevalue+'" data-original="'+pricevalue+'" class="form-control arrayprice-price" aria-label="price">' +
                    '            </td>' +
                    '            <td class="">' +
                    '                    <button class="btn btn-danger deletepriceline" type="button" data-line="'+number+'"><i class="fa fa-trash"></i></button>' +
                    '            </td>' +
                    '            <td><input class="arrayprice-check" type="text" hidden="true" value="'+checkvalue+'"></td>' +
                    '        </tr>';

            }

            function sumInputs($inputs) {
                var sum = 0;

                $inputs.each(function() {
                    sum += parseInt($(this).val(), 0);
                });

                return sum;
            }

            paramsPercentage = {
                "jsonSchema" : {
                    "title" : "Pourcentage",
                    "properties" : {
                        "percentage" : {
                            "label": "Poucentage du budget atribué à l'utilisateur ",
                            "placeholder": "",
                            "inputType": "number"
                        },
                    },
                    save : function () {
                        tplCtx.value = $("#percentage").val();
                        if (typeof tplCtx.setType != "undefined"){
                            delete tplCtx.setType;
                        }
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.closeForm();
                            ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});

                        });
                    },
                    afterSave : function(data){
                        /*let percentageLeft = 100 - $("#percentage").val();
                        let estimatetotal = Object.keys(answerObj.answers.aapStep1.[tplCtx.key][$(this).data("pos")].estimates).length;
                        let estimateleft = estimatetotal - 1;
                        if (estimateleft > 0){
                            $.each(answerObj.answers.aapStep1.[tplCtx.key][$(this).data("pos")].estimates , function(estId , estData){
                                if(estId != tplCtx.uid && ){
                                    tplCtx.value = percentageLeft / estimateleft;
                                    tplCtx.path = "answers."+tplCtx.form+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+estId+".percentage";
                                    dataHelper.path2Value( tplCtx, function(params) {});
                                }
                            });
                        }*/
                        /*tplCtx.pos = $(this).data("pos");
                        tplCtx.uid = $(this).data("uid");
                        tplCtx.collection = "answers";
                        tplCtx.id = $(this).data("id");
                        tplCtx.key = $(this).data("key");
                        tplCtx.form = $(this).data("form");
                        tplCtx.percentage = $(this).data("percentage");
                        tplCtx.path = "answers."+tplCtx.form+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+tplCtx.uid+".percentage";

                        if(
                            typeof answerObj. != "undefined" &&
                        ){

                        }*/
                    }
                }
            };

            paramsDenycomment = {
                "jsonSchema" : {
                    "title" : "Commentaire (facultatif)",
                    "properties" : {
                        "denycomment" : {
                            "label": "Commentaire (facultatif)",
                            "placeholder": "",
                            "inputType": "text"
                        },
                    },
                    save : function () {
                        tplCtx.value = $("#denycomment").val();
                        if (typeof tplCtx.setType != "undefined"){
                            delete tplCtx.setType;
                        }
                        tplCtx.path = "answers."+tplCtx.form+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+tplCtx.uid+".denycomment";
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.closeForm();
                            ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});


                        });
                    },
                    cancel: function(){
                        ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});
                    }
                }
            };

            paramsPricecomment = {
                "jsonSchema" : {
                    "title" : "Commentaire (facultatif)",
                    "properties" : {
                        "pricecomment" : {
                            "label": "Commentaire (facultatif)",
                            "placeholder": "",
                            "inputType": "text"
                        },
                    },
                    save : function () {
                        tplCtx.value = $("#pricecomment").val();
                        if (typeof tplCtx.setType != "undefined"){
                            delete tplCtx.setType;
                        }
                        tplCtx.path = "answers.aapStep1.depense."+tplCtx.pos+".estimates."+tplCtx.uid+".pricecomment";
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.closeForm();
                            ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});
                        });
                    },
                    cancel: function(){
                        ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});
                    }
                }
            };

            paramsAssignBudget = {
                "jsonSchema" : {
                    "title" : "Demander une CoRénumeration",
                    "properties" : {
                        "assignBudget" : {
                            "label": "Budget atribué à l'utilisateur ",
                            "placeholder": "",
                            "inputType": "number"
                        },
                    },
                    save : function () {
                        tplCtx.value = $("#assignBudget").val();
                        if (typeof tplCtx.setType != "undefined"){
                            delete tplCtx.setType;
                        }
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.closeForm();
                            ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});
                        });
                    }
                }
            };

            paramsCandidateNumber = {
                "jsonSchema" : {
                    "title" : "Nombre de candidat",
                    "properties" : {
                        "candidateNumber" : {
                            "label": "Nombre de candidat",
                            "placeholder": "",
                            "inputType": "number"
                        },
                    },
                    onLoads:{
                        onload : function(){
                            $('#candidateNumber').attr("type", "number");
                            $('#candidateNumber').attr("min", tplCtx.min);
                        }
                    },
                    save : function () {
                        tplCtx.value = $("#candidateNumber").val();
                        if (typeof tplCtx.setType != "undefined"){
                            delete tplCtx.setType;
                        }
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.closeForm();
                            ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});
                        });
                    }
                }
            };

            paramsAssignBudgetArray = {
                "jsonSchema" : {
                    "title" : "Demander une CoRénumeration",
                    "properties" : {
                        "AssignBudgetArray" : {
                            "label": "Budgets attribués à ce candidat",
                            "placeholder": "",
                            "inputType": "custom",
                            "html" : '<table id="" class="container assignpricelabel">' +
                                        '<tr>' +
                                            '<td>Label</td>' +
                                            '<td>Taux horaire</td>' +
                                            '<td>Nombre d heure</td>' +
                                            '<td>Prix total</td>' +
                                            '<td></td>' +
                                        '</tr>        ' +
                                '    </table>' +
                                '<div class="row center"><button class="btn btn-secondary addpricelistline" type="button"><i class="fa fa-plus"></i></button></div>'
                        },
                    },
                    onLoads:{
                        onload : function(){
                            if (notNull(coremuObj.cache.arrageDepense[tplCtx.pos].candidateEstimate[tplCtx.uid].assignpricelist)) {
                                 $.each(coremuObj.cache.arrageDepense[tplCtx.pos].candidateEstimate[tplCtx.uid].assignpricelist, function (index, val) {
                                     valCheck = 'false';
                                     if (typeof val.check != 'undefined') {
                                         valCheck = val.check;
                                     }
                                     let th = 0;
                                     let nh = 0;
                                     if (typeof val.hourlyRate != 'undefined') {
                                         th = val.hourlyRate;
                                     }
                                     if (typeof val.hour != 'undefined') {
                                         nh = val.hour;
                                     }
                                     $('.assignpricelabel').append(htmlasignprice(val.label, val.price , index , tplCtx.max , valCheck , nh , th));
                                 });
                            }else{
                                $('.assignpricelabel').append(htmlasignprice("" , "" , $('.rowprice').size() , tplCtx.max , 'false' , 0 , 0));
                                $('.deletepriceline').off().click(function() {
                                    let thisbtn = $(this);
                                    $('.rowprice[data-line="'+thisbtn.data('line')+'"]').remove();
                                });
                                $('input.arrayprice-label').focus();
                            }

                            $('.addpricelistline').off().click(function() {
                                $('.assignpricelabel').append(htmlasignprice("" , "" , $('.rowprice').size() , tplCtx.max , 'false' , 0 , 0));
                                $('input.arrayprice-label').last().focus();
                                $('.deletepriceline').off().click(function() {
                                    let thisbtn = $(this);
                                    $('.rowprice[data-line="'+thisbtn.data('line')+'"]').remove();
                                });
                                var max = tplCtx.max;
                                var $inputs = $('.arrayprice-price');
                                $inputs.on('input change', function(e) {
                                    var $this = $(this);
                                    var originalvalue = $this.attr('data-original');
                                    max += originalvalue;
                                    var sum = sumInputs($inputs.not(function(i, el) {
                                        return el === e.target;
                                    }));
                                    var value = parseInt($this.val(), 10) || 0;
                                    if(sum + value > max) $this.val(max - sum);
                                });
                                $('.arrayprice-nh , .arrayprice-th').on('input change', function(e) {
                                    let line = parseInt($(this).data('line'));
                                    let nh = parseInt($('.arrayprice-nh[data-line="'+line+'"]').val());
                                    let th = parseInt($('.arrayprice-th[data-line="'+line+'"]').val());
                                    if(nh > 0 && th > 0){
                                        $('.arrayprice-price[data-line="'+line+'"]').val(nh * th);
                                    }
                                });
                            });

                            $('.deletepriceline').off().click(function() {
                                let thisbtn = $(this);
                                $('.rowprice[data-line="'+thisbtn.data('line')+'"]').remove();
                            });

                            var max = tplCtx.max;
                            var $inputs = $('.arrayprice-price');

                            $inputs.on('keyup', function(e) {
                                var $this = $(this);
                                var originalvalue = $this.attr('data-original');
                                var sum = sumInputs($inputs.not(function(i, el) {
                                    return el === e.target;
                                }));
                                var value = parseInt($this.val(), 10) || 0;

                                if(sum + value > (max + parseInt(originalvalue))) $this.val(max - sum);
                            });

                            $('.arrayprice-nh , .arrayprice-th').on('keyup', function(e) {
                                let nh = parseInt($('.arrayprice-nh').val());
                                let th = parseInt($('.arrayprice-th').val());
                                if(nh > 0 && th > 0){
                                    $('.arrayprice-price').val(nh * th);
                                }
                            });

                        }
                    },
                    save : function () {
                        let priceobj = [];
                        $('.rowprice').each(function(){
                            let priceobjline = {};
                            if (($(this).find('.arrayprice-label').val() || $(this).find('.arrayprice-price').val()) && $('.arrayprice-price').val() != "") {
                                if ($(this).find('.arrayprice-label').val()) {
                                    priceobjline["label"] = $(this).find('.arrayprice-label').val();
                                } else {
                                    priceobjline["label"] = " ";
                                }
                                if ($(this).find('.arrayprice-label').val()) {
                                    priceobjline["price"] = $(this).find('.arrayprice-price').val();
                                } else {
                                    priceobjline["price"] = 0;
                                }
                                if ($(this).find('.arrayprice-nh').val()) {
                                    priceobjline["hour"] = $(this).find('.arrayprice-nh').val();
                                } else {
                                    priceobjline["hour"] = 0;
                                }
                                if ($(this).find('.arrayprice-th').val()) {
                                    priceobjline["hourlyRate"] = $(this).find('.arrayprice-th').val();
                                } else {
                                    priceobjline["hourlyRate"] = 0;
                                }
                                if ($(this).find('.arrayprice-label').val()) {
                                    priceobjline["check"] = $(this).find('.arrayprice-check').val();
                                } else {
                                    priceobjline["check"] = 0;
                                }

                                priceobj.push(priceobjline);
                            }
                        });

                        tplCtx.value = priceobj;
                        if (typeof tplCtx.setType != "undefined"){
                            delete tplCtx.setType;
                        }
                        var path2 = tplCtx.path;
                        tplCtx.path = tplCtx.path + "AssignBudgetArray";
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.closeForm();
                            /*tplCtx.path = path2 + "validate";
                            tplCtx.value = null;
                            dataHelper.path2Value( tplCtx, function(params) {*/
                                ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});
                            /*});*/
                            coInterface.showCostumLoader(".funding_body.funding_body_depense");
                            reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>" );
                        });
                    },

                }
            };

            var FiData = <?php echo json_encode((isset($answer["answers"]["aapStep1"][$key])) ? $answer["answers"]["aapStep1"][$key] : []); ?>;

            var projectId = "<?php echo (!empty($answer["project"]["id"]) ? $answer["project"]["id"] : "null"); ?>";

            var parentId = "<?php echo (string)$answer["form"]; ?>";

            var statusproposition = <?php echo ((isset($answers["status"]) && is_array($answers["status"])) ? json_encode($answers["status"]) : "[]"); ?>;

            var answerId = "<?php echo (string)$answer["_id"]; ?>";
            var cntxtId = "<?php echo $contextId; ?>";
            var cntxtType = "<?php echo $contextType; ?>";

            if (FiData != null) {
                if ($.isPlainObject(FiData)){
                    FiData = Object.values(FiData);
                }
                if (Array.isArray(FiData)){
                    var estimateList = FiData.reduce(function(accumulator, item){
                        if (item != null && typeof item["estimates"] != "undefined") {
                            accumulator.push(item["estimates"]);
                            return accumulator;
                        } else {
                            accumulator.push([]);
                            return accumulator;
                        }
                    },[]);
                }
            } else {
                FiData = [];
                var estimateList = {};
            }

            var budgetgroupe = <?php echo (!empty($paramsData["group"]) ? json_encode($paramsData["group"]) : "[]"); ?>;
            var budgetnature = <?php echo (!empty($paramsData["nature"]) ? json_encode($paramsData["nature"]) : "[]"); ?>;

            if(typeof costum != "undefined" && costum != null){
                costum["budgetgroupe"] = budgetgroupe;
                costum["budgetnature"] = budgetnature;
            }

            $(document).ready(function() {
                if(!coremuObj.isInit) {
                    coremuObj.init(coremuObj, answerId);
                }
                ajaxPost(
                    null,
                    baseUrl+'/survey/answer/countvisit/answerId/'+answerId+'/',
                    null,
                    function(data){
                        if (data && data.new == true){
                            dataHelper.path2Value( data.upvalue, function(){
                                //$('#countv').html(data.upvalue.value.count);
                                //propostionObj.refreshNewCounter(answerId);
                            });
                        }
                    },
                    "json"
                );

                sectionDyf.<?php echo $kunik ?> = {
                    jsonSchema : {
                        title : "Budget prévisionnel",
                        icon : "fa-money",
                        text : "Décrire ici les principaux postes de dépenses : à quoi correspondent les coûts ? <br/>Préciser ce qui relève des dépenses de fonctionnement et des dépenses d’investissement.<br/>Les coûts doivent être en <b>hors taxe</b>.",
                        properties : <?php echo json_encode($properties); ?>,
                        save : function () {
                            var today = new Date();
                            tplCtx.setType = [
                                {
                                    "path": "price",
                                    "type": "int"
                                },
                                {
                                    "path": "date",
                                    "type": "isoDate"
                                }
                            ];

                            if(notNull(tplCtx.key) && notNull(coremuObj.answerObjdepense.depense[tplCtx.key])){
                                tplCtx.value = coremuObj.answerObjdepense.depense[tplCtx.key];
                            } else {
                                tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
                            }

                            $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) {
                                tplCtx.value[k] = $("#ajaxFormModal #"+k).val();
                            });

                            tplCtx.value["user"] = userId;

                            var estimateId = Math.floor(Math.random() * 100000);

                            if ( typeof tplCtx.value.price != "undefined" && tplCtx.value.price != 0){
                                if(!notNull(coremuObj.answerObjdepense.depense[tplCtx.key])) {
                                    tplCtx.value["estimates"] = {};

                                    tplCtx.value["estimates"][estimateId] = {
                                        "price": tplCtx.value["price"],
                                        "name": " ",
                                        "date": today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear(),
                                        "addedBy": userId,
                                        "pricecomment": "Original",
                                        "ispriceselected": true
                                    }

                                    tplCtx.setType.push({
                                        "path": "estimates." + estimateId + ".price",
                                        "type": "int"
                                    });
                                    tplCtx.setType.push({
                                        "path": "estimates." + estimateId + ".date",
                                        "type": "isoDate"
                                    });
                                    tplCtx.setType.push({
                                        "path": "estimates." + estimateId + ".ispriceselected",
                                        "type": "boolean"
                                    });
                                }
                            }

                            if (typeof tplCtx.value.group != "undefined") {
                                tplCtxgroup = {
                                    id: parentId,
                                    collection: "forms",
                                    path: "params.<?php echo $kunik ?>.group"
                                }
                                tplCtxgroup.value = sectionDyf.<?php echo $kunik ?>ParamsData.group;
                                $.each(tplCtx.value.group.split(",") , function (ind , val) {
                                    if(jQuery.inArray(val, tplCtxgroup.value) == -1) {
                                        tplCtxgroup.value.push(val);
                                    }
                                });

                                //no need to delete setType
                                dataHelper.path2Value(tplCtxgroup, function (params) {});
                            }
                            if (typeof tplCtx.value.nature != "undefined") {
                                tplCtxnature = {
                                    id: parentId,
                                    collection: "forms",
                                    path: "params.<?php echo $kunik ?>.nature"
                                }
                                tplCtxnature.value = sectionDyf.<?php echo $kunik ?>ParamsData.nature;
                                $.each(tplCtx.value.nature.split(",") , function (ind , val) {
                                    if(jQuery.inArray(val, tplCtxnature.value) == -1) {
                                        tplCtxnature.value.push(val);
                                    }
                                });
                                //no need to delete setType
                                dataHelper.path2Value(tplCtxnature, function (params) {});
                            }

                            var connectedData = ["financer","todo","payed","progress","worker","validFinal","votes"];
                            $.each( connectedData , function(k,attr) {
                                if(notNull("answerObj."+tplCtx.path+"."+attr))
                                    tplCtx.value[attr] = jsonHelper.getValueByPath(answerObj,tplCtx.path+"."+attr);
                            });

                            mylog.log("save tplCtx",tplCtx);
                            if(typeof tplCtx.value == "undefined"){}
                            else {
                                dataHelper.path2Value( tplCtx, function(params) {
                                    dyFObj.closeForm();
                                    var answerId = "<?php echo (string)$answer["_id"]; ?>";
                                    ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/newdepense/answerid/' + answerId,
                                        {
                                            pos : coremuObj.cache.nextLinePath,
                                            url : window.location.href
                                        },
                                        function (data) {

                                        }, "html");
                                    ajaxPost("", baseUrl + "/co2/aap/commonaap/action/notifyAddDepenseLine", {
                                        answerId:answerId,
                                        depense: tplCtx.value.poste +": "+  tplCtx.value.price + " Euro"
                                    }, function(data){}, function(error){}, "json")

                                    answerId

                                    var tplCtxproject = {};

                                    if (projectId != null && projectId != "null") {

                                        if (notNull(tplCtx.actionid)) {
                                            tplCtxproject.id = tplCtx.actionid;
                                            tplCtxproject.collection = "actions";
                                            tplCtxproject.path = "name"
                                            tplCtxproject.value = tplCtx.value.poste;
                                            if(exists(tplCtxproject.value))
                                                dataHelper.path2Value(tplCtxproject, function () {});
                                        }

                                        var answerId = "<?php echo (string)$answer["_id"]; ?>";
                                        var cntxtId = "<?php echo $ctxtid; ?>";
                                        var cntxtType = "<?php echo $ctxttype; ?>";

                                        ajaxPost("", baseUrl + '/survey/form/generateproject/answerId/' + answerId + '/parentId/' + cntxtId + '/parentType/' + cntxtType,
                                            null,
                                            function (data) {
                                            });
                                    }
                                });

                                delete tplCtx.setType;

                                if ($.inArray("newaction", statusproposition) == -1){
                                    statusproposition.push("newaction");
                                    tplCtx.value = statusproposition;
                                }else{
                                    tplCtx.value = statusproposition;
                                }

                                tplCtx.path = "status";

                                ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});

                            }
                        },
                        beforeBuild : function(data){
                            if(typeof costum != "undefined" && costum != null){
                                costum["budgetgroupe"] = budgetgroupe;
                                costum["budgetnature"] = budgetnature;
                            }
                        },
                    }
                };

                sectionDyf.<?php echo $kunik ?>Params = {
                    jsonSchema : {
                        title : "<?php echo $kunik ?> config",
                        description : "Liste de question possible",
                        icon : "fa-cog",
                        properties : {
                            fielLabel : {
                                inputType : "text",
                                label : "Label du champ",
                                values :  sectionDyf.<?php echo $kunik ?>ParamsData.fielLabel
                            },
                            group : {
                                inputType : "array",
                                label : "Liste des groups",
                                values :  sectionDyf.<?php echo $kunik ?>ParamsData.group
                            },
                            activeGroup : {
                                inputType : "checkboxSimple",
                                label : "Activer le groupe",
                                params : {
                                    onText : "Oui",offText : "Non",onLabel : "Oui",offLabel : "Non",labelText : "Activer les groupes"
                                },
                                checked : sectionDyf.<?php echo $kunik ?>ParamsData.activeGroup
                            },
                            nature : {
                                inputType : "array",
                                labelKey : "Clef",
                                label : "Liste des natures possibles",
                                values :  sectionDyf.<?php echo $kunik ?>ParamsData.nature
                            },
                            activeNature : {
                                inputType : "checkboxSimple",
                                label : "Activer la nature",
                                params : {
                                    onText : "Oui",offText : "Non",onLabel : "Oui",offLabel : "Non",labelText : "Activer les groupes"
                                },
                                checked : sectionDyf.<?php echo $kunik ?>ParamsData.activeNature
                            },
                            amounts : {
                                inputType : "properties",
                                info : "veuillez garder la clef du coût à << price >> pour etre pris en compte dans les futurs calcul",
                                labelKey : "Clef",
                                labelValue : "Label affiché",
                                label : "Liste des prix(ex:par année)",
                                values :  sectionDyf.<?php echo $kunik ?>ParamsData.amounts
                            },
                            estimate : {
                                inputType : "checkboxSimple",
                                label : "estimate Prices",
                                params : {
                                    onText : "Oui",
                                    offText : "Non",
                                    onLabel : "Oui",
                                    offLabel : "Non",
                                    labelText : "estimate Prices"
                                },
                                checked : sectionDyf.<?php echo $kunik ?>ParamsData.estimate
                            },
                            prefilledDepense : {
                                inputType : "array",
                                label : "Dépenses pré-remplies",
                                values :  sectionDyf.<?php echo $kunik ?>ParamsData.prefilledDepense
                            }
                        },
                        onLoads:{
                            onload : function(){
                                alignInput2(["prefilledDepensearray"], "dep", 6, 6, null, null,"Dépenses pré-remplies", "#3f4e58", "");
                                alignInput2(["activeGroupcheckboxSimple","grouparray"], "group", 6, 6, null, null,"Groupe", "#3f4e58", "");
                                alignInput2(["activeNaturecheckboxSimple","naturearray"], "nature", 6, 6, null, null,"Nature", "#3f4e58", "");
                            }

                        },
                        save : function () {
                            tplCtx.value = {};
                            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                                if(k!= "fielLabel"){
                                    if(val.inputType == "properties")
                                        tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                                    else if(val.inputType == "array")
                                        tplCtx.value[k] = getArray('.'+k+val.inputType);
                                    else
                                        tplCtx.value[k] = $("#"+k).val();
                                }
                                mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                            });
                            mylog.log("save tplCtx",tplCtx);

                            if(typeof tplCtx.value == "undefined")
                                toastr.error('value cannot be empty!');
                            else {
                                dataHelper.path2Value( tplCtx, function(params) {
                                    tplCtx.value = $("#fielLabel").val();
                                    tplCtx.id = "<?= @$input["docinputsid"] ?>";
                                    tplCtx.collection = "<?= Form::INPUTS_COLLECTION ?>";
                                    tplCtx.path = "inputs.<?= $key ?>.label";
                                    dataHelper.path2Value(tplCtx, function(params) {
                                        dyFObj.closeForm();
                                        urlCtrl.loadByHash(location.hash );
                                    })
                                } );
                            }

                        }
                    }
                };

                sectionDyf.<?php echo $kunik ?>Comment = {
                    jsonSchema : {
                        title : "",
                        description : "Liste de question possible",
                        icon : "fa-cog",
                        properties : {
                            fielLabel : {
                                inputType : "text",
                                label : "Label du champ",
                                values :  sectionDyf.<?php echo $kunik ?>ParamsData.fielLabel
                            },
                            group : {
                                inputType : "array",
                                label : "Liste des groups",
                                values :  sectionDyf.<?php echo $kunik ?>ParamsData.group
                            },
                            activeGroup : {
                                inputType : "checkboxSimple",
                                label : "Activer le groupe",
                                params : {
                                    onText : "Oui",offText : "Non",onLabel : "Oui",offLabel : "Non",labelText : "Activer les groupes"
                                },
                                checked : sectionDyf.<?php echo $kunik ?>ParamsData.activeGroup
                            },
                            nature : {
                                inputType : "array",
                                labelKey : "Clef",
                                label : "Liste des natures possibles",
                                values :  sectionDyf.<?php echo $kunik ?>ParamsData.nature
                            },
                            activeNature : {
                                inputType : "checkboxSimple",
                                label : "Activer la nature",
                                params : {
                                    onText : "Oui",offText : "Non",onLabel : "Oui",offLabel : "Non",labelText : "Activer les groupes"
                                },
                                checked : sectionDyf.<?php echo $kunik ?>ParamsData.activeNature
                            },
                            amounts : {
                                inputType : "properties",
                                info : "veuillez garder la clef du coût à << price >> pour etre pris en compte dans les futurs calcul",
                                labelKey : "Clef",
                                labelValue : "Label affiché",
                                label : "Liste des prix(ex:par année)",
                                values :  sectionDyf.<?php echo $kunik ?>ParamsData.amounts
                            },
                            estimate : {
                                inputType : "checkboxSimple",
                                label : "estimate Prices",
                                params : {
                                    onText : "Oui",
                                    offText : "Non",
                                    onLabel : "Oui",
                                    offLabel : "Non",
                                    labelText : "estimate Prices"
                                },
                                checked : sectionDyf.<?php echo $kunik ?>ParamsData.estimate
                            },
                            prefilledDepense : {
                                inputType : "array",
                                label : "Dépenses pré-remplies",
                                values :  sectionDyf.<?php echo $kunik ?>ParamsData.prefilledDepense
                            }
                        },
                        onLoads:{
                            onload : function(){
                                alignInput2(["prefilledDepensearray"], "dep", 6, 6, null, null,"Dépenses pré-remplies", "#3f4e58", "");
                                alignInput2(["activeGroupcheckboxSimple","grouparray"], "group", 6, 6, null, null,"Groupe", "#3f4e58", "");
                                alignInput2(["activeNaturecheckboxSimple","naturearray"], "nature", 6, 6, null, null,"Nature", "#3f4e58", "");
                            }

                        },
                        save : function () {
                            tplCtx.value = {};
                            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                                if(k!= "fielLabel"){
                                    if(val.inputType == "properties")
                                        tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                                    else if(val.inputType == "array")
                                        tplCtx.value[k] = getArray('.'+k+val.inputType);
                                    else
                                        tplCtx.value[k] = $("#"+k).val();
                                }
                                mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                            });
                            mylog.log("save tplCtx",tplCtx);

                            if(typeof tplCtx.value == "undefined")
                                toastr.error('value cannot be empty!');
                            else {
                                dataHelper.path2Value( tplCtx, function(params) {
                                    tplCtx.value = $("#fielLabel").val();
                                    tplCtx.id = "<?= @$input["docinputsid"] ?>";
                                    tplCtx.collection = "<?= Form::INPUTS_COLLECTION ?>";
                                    tplCtx.path = "inputs.<?= $key ?>.label";
                                    dataHelper.path2Value(tplCtx, function(params) {
                                        dyFObj.closeForm();
                                        urlCtrl.loadByHash(location.hash );
                                    })
                                } );
                            }

                        }
                    }
                };

                $(".editdepenseParams").off().on("click",function() {
                    tplCtx.id = $(this).data("id");
                    tplCtx.collection = $(this).data("collection");
                    tplCtx.path = $(this).data("path");
                    dyFObj.openForm( sectionDyf.budgetdepenseParams,null, sectionDyf.depenseParamsData , null , null , {
                        type : "bootbox",
                        notCloseOpenModal : true,
                    });
                });

                $(".configurePaymentCandidat").off().on("click",function() {
                    var thisbtn = $(this);

                    function showAdditionForm(type) {
                        if (type === 'iban') {
                            paymentDetails.innerHTML = `
                                      <div class="form-group">
                                        <label for="iban">IBAN :</label>
                                        <input type="text" id="iban" name="iban" placeholder="Ex : FR76XXXXXXXXXXX" required>
                                      </div>
                                      <div class="form-group">
                                        <label for="bic">BIC (optionnel) :</label>
                                        <input type="text" id="bic" name="bic" placeholder="Ex : BANKXXXXX">
                                      </div>
                                    `;
                        } else if (type === 'api_key_stripe') {
                            paymentDetails.innerHTML = `
                                  <div class="form-group">
                                    <label for="apikey">Clé API stripe:</label>
                                    <input type="text" id="apikey" name="apikey" placeholder="Entrez votre clé API" required>
                                  </div>
                                  <div class="form-group">
                                    <label for="bic">webhook</label>
                                    <input type="text" id="webhook" name="webhook" placeholder="Entrez votre webhook">
                                  </div>
                                `;
                        }
                    }

                    // Afficher le formulaire pour modifier une méthode existante
                    function showModificationForm(type, method) {
                        if (type === 'iban') {
                            paymentDetails.innerHTML = `
                                  <div class="form-group">
                                    <label for="iban">IBAN (complet) :</label>
                                    <input type="text" id="iban" name="iban" placeholder="Ex : FR76XXXXXXXXXXX" required>
                                  </div>
                                  <div class="form-group">
                                    <label for="bic">BIC (optionnel) :</label>
                                    <input type="text" id="bic" name="bic" placeholder="Ex : BANKXXXXX">
                                  </div>
                                `;
                        } else if (type === 'api_key_stripe') {
                            paymentDetails.innerHTML = `
                                  <div class="form-group">
                                    <label for="apikey">Clé API (complet) :</label>
                                    <input type="text" id="apikey" name="apikey" placeholder="Entrez votre clé API" required>
                                  </div>
                                `;
                        }
                    }

                    // Mettre à jour la liste des méthodes de paiement
                    function updatePaymentList(data) {
                        $('#methodList').html('');
                        $.each(data.payement_list , function (index,method){
                            const li = document.createElement('li');
                            var favhtml = data.favorite == method.type ? '<span><i class="fa fa-star" style="color: var(--coremu-primary-orange-color)"></i>préféré</span>' : `<button class="favoritepayconfigbtn" data-type="${method.type}"><i class="fa fa-star-o"></i>Changer préférence</button>`;
                            var html = '';
                            var typeObj = {
                                "api_key_stripe" : "clé API"
                            }

                            html += `
                                 <div class="methodtitle-cont"> <h3 class="methodtitle"> ${method.type.toUpperCase()} </h3> <label class="methodactiondate pull-right"> <i class="fa fa-calendar"> </i> ${new Date(parseInt(method.addedAt) * 1000).toLocaleDateString("fr-FR")} </label> </div>
                                 <div class="methoddetail-cont">
                                    <div class="methoddetail">`;

                                        $.each(method , function (typekey,type){
                                            if(typekey != "addedAt" && typekey != "type")
                                            html += `<label> ${ typeof typeObj[typekey] != "undefined" ? typeObj[typekey] : typekey } : ${type} </label>`;
                                        });
                            html += `
                                    </div>
                                    <div class="methodaction">
                                        <span class="methodactionbtn margin-left-10 pull-right"> <button class="deletepaymethod" data-type="${method.type}">Supprimer</button> </span>
                                        <span class="methodactionbtn pull-right"> ${favhtml} </span>
                                    </div>
                                 </div>`;

                            li.innerHTML = html;
                            $('#methodList').append(li);
                        });

                        $('.deletepaymethod').off().on("click", function(e){
                            removePaymentMethod($(this).data('type'));
                        });

                        $('.favoritepayconfigbtn').off().on("click", function(e){
                            setFav($(this).data('type'));
                        });
                    }

                    function onUpdateEvent(paymentMethods) {
                        $('.btnconfpay').off().on("click", function(e) {
                            e.preventDefault();

                            var type = $('#paymentType').val();

                            //aaaaaa
                            if (type === "") return toastr.error("Veuillez sélectionner un type de paiement.");

                            // Récupérer les données saisies
                            var formpcData = {};
                            if(type == "iban"){
                                if ($('#iban').val() == '' || $('#bic').val() == '') return toastr.error("Veuillez remplir le champ");
                                formpcData = {
                                    "type" : type,
                                    "iban" : $('#iban').val(),
                                    "bic" : $('#bic').val(),
                                    "action" : typeof paymentMethods[type] != "undefined" ? "edit" : "save"
                                }
                            }else if(type == "api_key_stripe"){
                                if ($('#apikey').val() == '' || $('#webhook').val() == '') return toastr.error("Veuillez remplir le champ");
                                formpcData = {
                                    "type" : type,
                                    "api_key_stripe" : $('#apikey').val(),
                                    "webhook" : $('#webhook').val(),
                                    "action" : typeof paymentMethods[type] != "undefined" ? "edit" : "save"
                                }
                            }
                            coInterface.showCostumLoader("#methodList");
                            ajaxPost("", baseUrl + '/co2/aap/config_payment/',
                                formpcData,
                                function (data) {
                                    toastr.success(`Méthode de paiement sauvegardé avec succès !`);
                                    //paymentDetails.innerHTML = '';
                                    //$('.btnpc').html("Ajouter une méthode de paiement");
                                    updatePaymentList(data);
                                }
                            );
                        });
                    }

                    function removePaymentMethod(type) {
                        var prioModal = bootbox.dialog({
                            title: trad.confirmdelete,
                            show: false,
                            message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                            buttons: [{
                                label: "Ok",
                                className: "btn btn-primary pull-left",
                                callback: function() {
                                    coInterface.showCostumLoader("#methodList");
                                    ajaxPost("", baseUrl + '/co2/aap/config_payment/',
                                        {"action" : "delete" , "type" : type},
                                        function (data) {
                                            updatePaymentList(data);
                                        }
                                    );
                                }
                            },
                                {
                                    label: "Annuler",
                                    className: "btn btn-default pull-left",
                                    callback: function() {}
                                }
                            ]
                        });
                        prioModal.modal("show");
                    }

                    function setFav(type) {
                        coInterface.showCostumLoader("#methodList");
                        ajaxPost("", baseUrl + '/co2/aap/config_payment/',
                            {"action" : "favorite" , "type" : type},
                            function (data) {
                                updatePaymentList(data);
                            }
                        );
                    }

                    ajaxPost("", baseUrl + '/co2/aap/config_payment/',
                        {
                            action : 'get'
                        },
                        function (data) {
                            var payementconfig_container_dom = $('<div>', {
                                class: 'co-popup-payementconfig-container',
                                html: '' +
                                    '<div class="co-popup-cofinance">' +
                                    ` <button class="pull-right cart-stepper-close btn btn-default co-popup-payementconfig-container-close">
                                        <i class="fa fa-times"></i> <span class="hidden-xs">Fermer</span>
                                  </button>
                                <h2>Ajouter un méthode de paiement</h2>

                                <div id="paymentForm">
                                    <!-- Sélection du type de paiement -->
                                    <div class="form-group">
                                        <label for="paymentType">Type de paiement :</label>
                                        <select id="paymentType" name="paymentType" required>
                                            <option value="">-- Sélectionnez un type --</option>
                                            <option value="iban">IBAN</option>
                                            <option value="api_key_stripe">Clé API Stripe</option>
                                        </select>
                                    </div>

                                    <!-- Champs dynamiques -->
                                    <div id="paymentDetails"></div>

                                    <!-- Bouton de soumission -->
                                    <button class="btn btnpc btnconfpay">Ajouter / Modifier</button>
                                </div>

                                <div id="paymentList">
                                    <h2>Vos méthodes de paiement</h2>
                                    <ul id="methodList">
                                        <!-- Les méthodes de paiement ajoutées seront affichées ici -->
                                    </ul>
                                </div>
                            </div>`+
                                    '</div>'
                            });
                            //$('.co-popup-cofinance-container').remove();
                            $('body').append(payementconfig_container_dom).ready(function() {
                                $(".co-popup-payementconfig-container-close").off('click').on("click", function() {
                                    payementconfig_container_dom.remove();
                                });
                                coInterface.showCostumLoader("#methodList");
                                const paymentForm = document.getElementById('paymentForm');
                                const paymentType = document.getElementById('paymentType');
                                const paymentDetails = document.getElementById('paymentDetails');
                                const methodList = document.getElementById('methodList');

                                const paymentMethods = data.payement_list;
                                updatePaymentList(data);
                                // Mettre à jour les champs dynamiques en fonction du type de paiement
                                $('#paymentType').on('change', function() {
                                    var selectedType = $(this).val();
                                    $('#paymentDetails').html('');

                                    if (selectedType == "") {
                                        $('.btnconfpay').html("Ajouter une méthode de paiement");
                                        return;
                                    }

                                    var existingMethod = false;

                                    $.each(paymentMethods , function (i,v){
                                        if(v.type == selectedType){
                                            existingMethod = true;
                                        }
                                    });

                                    // Vérifier si une méthode existe déjà pour ce type
                                    if (existingMethod) {
                                        $('.btnconfpay').html("Modifier cette méthode de paiement");
                                        showAdditionForm(selectedType, existingMethod);
                                        onUpdateEvent(paymentMethods)
                                    } else {
                                        $('.btnconfpay').html("Ajouter cette méthode de paiement");
                                        showAdditionForm(selectedType);
                                        onUpdateEvent(paymentMethods)
                                    }
                                });

                            });
                        });

                    });

                $(".switchbudgetview").off().click( function(e) {
                    e.stopPropagation();
                    var thisbtn = $(this);
                    if(thisbtn.attr("data-action") == "line"){
                        thisbtn.find('i').removeClass("fa-table").addClass("fa-menu");
                        thisbtn.find('b').html(" En mode Compact ");
                        thisbtn.attr("data-action" , "table");

                        $(".newpropositiontable").fadeOut("slow");
                        $("#coremudashboard").fadeIn("slow");


                    }else if(thisbtn.attr("data-action") == "table"){
                        thisbtn.find('i').removeClass("fa-menu").addClass("fa-table");
                        thisbtn.find('b').html(" En mode Detaillé ");
                        thisbtn.attr("data-action" , "line");

                        $(".newpropositiontable").fadeIn("slow");
                        $("#coremudashboard").fadeOut("slow");

                        ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});

                        setTimeout(function(){
                            //$('.tablesorter-header[data-column="2"] .tablesorter-header-inner').trigger('click');
                        }, 2000);
                    }
                });

                $(".hhh").off().click(function(e) {
                    alert('gffgfg');
                });

                $('#addfromcsv').off().click(function() {

                    prioModal = bootbox.dialog({
                        message: $(".addfromcsvdiv").html(),
                        show: false,
                        size: "large",
                        className: 'csvdialog',
                        buttons: {
                            success: {
                                label: trad.save,
                                className: "btn-primary",
                                callback: function () {
                                    var ext = $('.csvdialog .inputcsvdata').val().split(".").pop();

                                    if($.inArray(ext, ["csv"]) == -1) {
                                        toastr.success(tradDynForm.youMustUseACSVFormat);
                                        return false;
                                    }

                                    if ($('.csvdialog .inputcsvdata').prop('files')[0] != undefined) {

                                        var reader = new FileReader();

                                        reader.onload = function(e) {
                                            var csvval = e.target.result.split("\n");

                                            if(FiData == null){
                                                FiData = [];
                                            }
                                            var lastinput = FiData.length;
                                            var importgroup = [];
                                            var importnature = [];
                                            $.each(csvval, function(ind, val){
                                                tplCtx.id = answerId;
                                                tplCtx.collection = "answers";
                                                tplCtx.path = "answers.aapStep1.depense."+lastinput;
                                                tplCtx.setType = [
                                                    {
                                                        "path": "price",
                                                        "type": "int"
                                                    }
                                                ];
                                                var today = new Date();
                                                tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };

                                                var thh = val.split(";");
                                                var prpr = 0;
                                                $.each(thh , function(thhid, thhhval){
                                                    if (typeof Object.keys(sectionDyf.<?php echo $kunik ?>.jsonSchema.properties)[prpr] != "undefined") {
                                                        tplCtx.value[Object.keys(sectionDyf.<?php echo $kunik ?>.jsonSchema.properties)[prpr]] = thhhval;
                                                    }
                                                    prpr++;
                                                });

                                                if (typeof tplCtx.value.group != "undefined") {
                                                    tplCtxgroup = {
                                                        id: parentId,
                                                        collection: "forms",
                                                        path: "params.<?php echo $kunik ?>.group"
                                                    }

                                                    tplCtxgroup.value = sectionDyf.<?php echo $kunik ?>ParamsData.group;

                                                    $.each(tplCtx.value.group.split(",") , function (ind , val) {
                                                        if(jQuery.inArray(val, tplCtxgroup.value) == -1 && jQuery.inArray(val, importgroup) == -1) {

                                                            tplCtxgroup.value.push(val);
                                                            importgroup.push(val);
                                                        }
                                                    });

                                                    dataHelper.path2Value(tplCtxgroup, function (params) {
                                                    });
                                                }

                                                if (typeof tplCtx.value.nature != "undefined") {
                                                    tplCtxnature = {
                                                        id: parentId,
                                                        collection: "forms",
                                                        path: "params.<?php echo $kunik ?>.nature"
                                                    }

                                                    tplCtxnature.value = sectionDyf.<?php echo $kunik ?>ParamsData.nature;

                                                    $.each(tplCtx.value.nature.split(",") , function (ind , val) {
                                                        if(jQuery.inArray(val, tplCtxnature.value) == -1 && jQuery.inArray(val, importnature) == -1) {

                                                            tplCtxnature.value.push(val);
                                                            importnature.push(val);
                                                        }
                                                    });

                                                    dataHelper.path2Value(tplCtxnature, function (params) {
                                                    });
                                                }

                                                var connectedData = ["financer","todo","payed","progress","worker","validFinal","votes"];
                                                $.each( connectedData , function(k,attr) {
                                                    if(notNull("answerObj."+tplCtx.path+"."+attr))
                                                        tplCtx.value[attr] = jsonHelper.getValueByPath(answerObj,tplCtx.path+"."+attr);
                                                });

                                                dataHelper.path2Value( tplCtx, function(params) {
                                                    var tplCtxproject = {};

                                                    if (projectId != null && projectId != "null") {

                                                        if (notNull(tplCtx.actionid)) {
                                                            tplCtxproject.id = tplCtx.actionid;
                                                            tplCtxproject.collection = "actions";
                                                            tplCtxproject.path = "name"
                                                            tplCtxproject.value = tplCtx.value.poste;

                                                            dataHelper.path2Value(tplCtxproject, function () {
                                                            });
                                                        }

                                                        var answerId = "<?php echo (string)$answer["_id"]; ?>";
                                                        var cntxtId = "<?php echo $ctxtid; ?>";
                                                        var cntxtType = "<?php echo $ctxttype; ?>";

                                                        ajaxPost("", baseUrl + '/survey/form/generateproject/answerId/' + answerId + '/parentId/' + cntxtId + '/parentType/' + cntxtType,
                                                            null,
                                                            function (data) {
                                                            });
                                                    }
                                                });

                                                delete tplCtx.setType;

                                                if (statusproposition && $.inArray("newaction", statusproposition) == -1)
                                                    tplCtx.value = statusproposition.push("newaction");

                                                tplCtx.path = "status";

                                                dataHelper.path2Value( tplCtx, function(params){});

                                                lastinput++;
                                            });
                                            prioModal.modal('hide');
                                            ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});

                                        };


                                        reader.readAsText($('.csvdialog .inputcsvdata').prop('files')[0]);

                                    }else{
                                        toastr.error(tradDynForm.weWereUnableToReadYourFile);
                                    }
                                    return false;
                                }
                            },
                            cancel: {
                                label: trad.cancel,
                                className: "btn-secondary",
                                callback: function(){
                                    prioModal.modal('hide');
                                }
                            }
                        },
                        onEscape: function(){
                            prioModal.modal('hide');
                        }
                    });

                    prioModal.on('shown.bs.modal', function (e) {
                        $(".inputcsvdata").change(function(e) {

                            $(".addfromcsvdivrs").html('<span class="homestead"><i class="fa fa-spin fa-circle-o-noch"></i> '+trad.erroroccurred+'...</span>')

                            var ext = $(this).val().split(".").pop();

                            if($.inArray(ext, ["csv"]) == -1) {
                                toastr.error(tradDynForm.youMustUseACSVFormat);
                                return false;
                            }

                            if (e.target.files != undefined) {

                                var reader = new FileReader();

                                reader.onload = function(e) {
                                    var csvval = e.target.result.split("\n");

                                    var str = '<table class="table table-bordered "><tbody>';
                                    $.each(csvval, function(ind, val){
                                        str += '<tr>';
                                        var thh = val.split(";");
                                        $.each(thh , function(thhid, thhhval){
                                            str += '<th>'+thhhval+'</th>';
                                        });
                                        str += '</tr>';
                                    });

                                    str += '</tbody></table>';

                                    $(".addfromcsvdivrs").html(str);

                                };
                                reader.readAsText(e.target.files.item(0));

                            }else{
                                toastr.error(tradDynForm.weWereUnableToReadYourFile);
                            }
                            return false;
                        });
                    });

                    prioModal.modal("show");
                });

                $('#reloadinput').off().click(function() {
                    if (typeof inputsList != "undefined"){
                        coInterface.showCostumLoader(".funding_body.funding_body_depense");
                        $.each(inputsList , function (inn, vall) {
                            $.each(vall , function (inn2, vall2) {
                                if (typeof vall2.type != "undefined"
                                    && vall2.type.split('.')[vall2.type.split('.').length-1] != "undefined"
                                    && ( vall2.type.split('.')[vall2.type.split('.').length-1] == "budget" ||
                                        vall2.type.split('.')[vall2.type.split('.').length-1] == "financement" ||
                                        vall2.type.split('.')[vall2.type.split('.').length-1] == "suivi"
                                    )
                                ){
                                    setTimeout(() => {
                                        reloadInput(inn2, inn);
                                        $('html, body').animate({scrollTop: $("#question<?= $key ?>").offset().top-$("#mainNav").height()},'slow');
                                    }, 500);
                                }
                            });
                        });
                    };
                });

                <?php //if ($paramsData["estimate"])
                //{ ?>

                $('.lockall').off().click(function() {
                    $('.locklinestatehide').each(function() {
                        $(this).trigger('click');
                    });

                    ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});

                });

                $('.unlockall').off().click(function() {
                    $('.unlocklinestatehide').each(function() {
                        $(this).trigger('click');
                    });

                    ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});

                });

                /*$('.getcoremutable').on('click', function(e) {
                    e.stopPropagation();
                    var depenseid = $(this).data('depenseid');
                    smallMenu.openAjaxHTML( baseUrl+'/survey/form/coremudashboard/tableonly/true/answer/'+depenseid );
                });*/

                ajaxPost("#coremudashboard", baseUrl+'/survey/form/coremudashboard/tableonly/true/answer/'+answerId,
                    null,
                    function(){
                        $("#coremudashboard .aapdashboard").removeClass("col-md-offset-1")
                            .removeClass("col-md-10")
                            .addClass("col-md-12")
                            .css("margin-top" , "0px");
                    },"html");


            <?php
                //} ?>


            });

        </script>
        <?php
    }
    else
    {
        //echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";

    }

}

?>
