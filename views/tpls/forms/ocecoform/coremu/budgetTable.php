<?php
$contextIdType = $context;

if ($canEdit){
    $keyTpl = (isset($kunik)) ? $kunik : $keyTpl;
}
?>

<div class="single_funding ">
    <div class="funding_header padding-15">
        <h4> <?php echo !empty($label) ? $label : "Ligne de dépense" ?>
            <span>
                    <i class="fa fa-euro"></i>
                </span>

            <button class="btn switchbudgetview" data-action="line" type="button" data-depenseid="<?php echo (string)$answer["_id"] ?>" >
                <i class="budgetviewicon fa fa-table"></i>
                <b> En mode Detaillé </b>
            </button>

            <?php echo $editBtnL . $editParamsBtn ?>

            <?php if ($mode == "w") { ?>
                <button type="button" class="btn" id="addfromcsv">
                    <i class="fa fa-file"></i>
                    Charger csv
                </button>
                <button type="button" class="btn configurePaymentCandidat" id="">
                    <i class="fa fa-file"></i>
                    Configurer paiement
                </button>
            <?php } ?>

                <button type="button" class="btn" id="reloadinput">
                    <i class="fa fa-refresh"></i>
                    Recharger
                </button>

                <?php if ($canAdminAnswer){ ?>
                    <button type="button" class="btn lockall" id="">
                        <i class="fa fa-lock"></i>
                    </button>
                    <button type="button" class="btn unlockall" id="">
                        <i class="fa fa-unlock"></i>
                    </button>
                <?php } ?>

            <?php
                $totalcandidattovalid = 0;
                $totalpricetovalid = 0;
                $totalneedcandidat = 0;
                if(!empty($answer["answers"]["aapStep1"][$key])) {
                    //$totalcandidattovalid = $depenseAction["totalcandidattovalid"];
                    //$totalpricetovalid = $depenseAction["totalpricetovalid"];
                    //$totalneedcandidat = $depenseAction["totalneedcandidat"];
                }

                $totalansweraction = $totalcandidattovalid + $totalpricetovalid + $totalneedcandidat;
            ?>

            <div class="dropdown actiondropdown">
                <button class="btn dropdown-toggle tooltips pull-right <?php echo ($totalansweraction > 0 ? "btn-warning" : "") ?>" data-placement="top"  data-original-title="Action en attente de d'interaction" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-exclamation"></i>(<label class="totalansweraction">  <?= $totalansweraction ?>  </label>)
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <div class="dropdown-menu-item">
                        <label class="">Appel à candidature total</label> <label class=" actionnumber pull-right"> <b class="totalneedcandidat"> <?= $totalneedcandidat ?> </b></label>
                    </div>
                    <div class="dropdown-menu-item">
                        <label class="">Budget à valider total</label> <label class=" actionnumber pull-right"> <b class="totalpricetovalid"> <?= $totalpricetovalid ?></b> </label>
                    </div>
                    <div class="dropdown-menu-item">
                        <label class="">Candidat à valider total</label> <label class=" actionnumber pull-right"> <b class="totalcandidattovalid"> <?= $totalcandidattovalid ?></b> </label>
                    </div>
                </div>
            </div>
        </h4>
    </div>
    <div class="funding_body funding_body_depense">
        <div class="panel-body no-padding">

            <div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding ">

                <div class="btn-group-vertical popup-btn-wrapper dismiss-popup-btn">
                    <button type="button" class="btn btn-xs toogle-minimal"><i class="fa fa-window-restore"></i></button>
                    <button type="button" class="btn btn-xs toogle-action hide"><i class="fa fa-cog"></i> </button>
                    <button type="button" class="btn btn-xs toogle-minimal-action hide"><i class="fa fa-window-maximize"></i></button>
                </div>

                <table class="table table-bordered funding_body_table newpropositiontable" id="editableTable">

                    <tbody id="coremutbody" style="color: #3f4e58;">


                    </tbody>
                </table>

                <div id="bootBcontainer">

                </div>

                <div id="coremudashboard" style="display: none">

                </div>

            </div>

        </div>
    </div>
</div>

<script type="text/javascript">

</script>
