<?php
HtmlHelper::registerCssAndScriptsFiles(['/plugins/d3/d3-v5/d3.min.js'], Yii::app()->request->baseUrl);
HtmlHelper::registerCssAndScriptsFiles(['/plugins/d3/sankey/d3-sankey.min.js'], Yii::app()->request->baseUrl);
HtmlHelper::registerCssAndScriptsFiles(["/js/coremu.js"], Yii::app()->getModule('survey')->assetsUrl);

?>
<style type="text/css">
    .financementerror {
        color: #e74c3c;
        font-size: 20px;
        background-color: #ecf0f1;
        text-align: center;
        padding: 20px;
    }

    .enveloppelabel {
        color: #b3a0a0;
    }

    .enveloppelabelnum {
        color: #a5c842;
    }

    .enveloppewarning {
        color: #f9690e;
    }

    .ui-widget.ui-widget-content{
        z-index: 2000000;
        max-height: 300px;
        overflow-y: auto;
        overflow-x: hidden;
    }

    .switchfinancementview {
        background-color: #dfe5c6 !important;
    }

    .switchfinancementview.active {
        background-color: #a5c842 !important;;
    }

    #coremufinancementbody .locklinestate{
        display: none;
    }

    #coremufinancementbody .budgetbadgebtn {
        display: flex !important;
        visibility: visible !important;
        opacity: 1 !important;
      }

</style>
<?php if ($answer)
{
    $isAapProject = $isAap;
    $contextId = $ctxtid = $context["_id"];
    $contextType = $ctxttype = $parentForm["parent"][(string)$context["_id"]]["type"];
    $editBtnLDepense = ($canEdit == true && ($mode == "w" || $mode == "fa")) ? " <a href='javascript:;' data-id='" . $answer["_id"] . "' data-collection='" . Form::ANSWER_COLLECTION . "' data-path='answers.aapStep1.depense.' class='addbudgetdepense btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";
    $debug = false;

    $copy = "aapStep1.depense";

    if (isset($parentForm["params"][$kunik]["budgetCopy"])) $copy = $parentForm["params"][$kunik]["budgetCopy"];
    else if (count(Yii::app()->session["budgetInputList"]) == 1) $copy = array_keys(Yii::app()->session["budgetInputList"]) [0];

    $copyT = explode(".", $copy);
    $copyF = $copyT[0];
    $budgetKey = $copyT[1];

    $paramsDataDepense = ["group" => ["Feature", "Costum", "Chef de Projet", "Data", "Mantenance"], "nature" => ["investissement", "fonctionnement"], "amounts" => ["price" => "Montant"], "estimate" => true];

    if (isset($parentForm["params"]["budgetdepense"]["group"])) $paramsDataDepense["group"] = $parentForm["params"]["budgetdepense"]["group"];
    if (isset($parentForm["params"]["budgetdepense"]["nature"])) $paramsDataDepense["nature"] = $parentForm["params"]["budgetdepense"]["nature"];
    if (isset($parentForm["params"]["budgetdepense"]["amounts"])) $paramsDataDepense["amounts"] = $parentForm["params"]["budgetdepense"]["amounts"];
    if (isset($parentForm["params"]["budgetdepense"]["estimate"])) $paramsDataDepense["estimate"] = Answer::is_true($parentForm["params"]["budgetdepense"]["estimate"]);

    $propertiesdepense =  ["group" => ["placeholder" => "Groupé", "inputType" => "tags", "tagsList" => "budgetgroupe", "rules" => ["required" => true], "maximumSelectionLength" => 3], "nature" => ["placeholder" => "Nature de l’action", "inputType" => "tags", "tagsList" => "budgetnature", "rules" => ["required" => true], "maximumSelectionLength" => 3], "poste" => ["inputType" => "text", "label" => "Poste de dépense", "placeholder" => "Poste de dépense", "rules" => ["required" => true]] , "tags" => ["placeholder" => "tags", "inputType" => "tags",  "maximumSelectionLength" => 3], "description" => ["placeholder" => "description", "inputType" => "textarea"]];

    unset($propertiesdepense["group"]);
    unset($propertiesdepense["nature"]);

    foreach ($paramsDataDepense["amounts"] as $k => $l)
    {
        $propertiesdepense[$k] = ["inputType" => "number", "label" => $l, "propType" => "amount", "placeholder" => $l,
            //"rules" => [ "required" => true, "number" => true ]
        ];
    }

    $financement = [];


    if ($answer)
    {
        if (isset($parentForm["mapping"]["depense"]))
        {
            $mapping = $parentForm["mapping"]["depense"];
            $mappingExplode = explode(".", $mapping);

            if (isset($answer[$mappingExplode[0]][$mappingExplode[1]][$mappingExplode[2]]))
            {
                $answers = $answer[$mappingExplode[0]][$mappingExplode[1]][$mappingExplode[2]];
            }
        }
        elseif (isset($parentForm["type"]) && ($parentForm["type"] == "aapConfig" || $parentForm["type"] == "aap"))
        {
            if ($parentForm["type"] == "aap")
            {
                $configEl = $configForm;
            }
            elseif ($parentForm["type"] == "aapConfig")
            {
                $configEl = $parentForm;
            }

            $mapping = $configEl["mapping"]["depense"];
            $mappingExplode = explode(".", $mapping);

            if (isset($answer[$mappingExplode[0]][$mappingExplode[1]][$mappingExplode[2]]))
            {
                $answers = $answer[$mappingExplode[0]][$mappingExplode[1]][$mappingExplode[2]];
            }
        }
    }

    if ($wizard)
    {
        if ($budgetKey)
        {
            if (isset($answer["answers"][$copyF][$budgetKey]) && count($answer["answers"][$copyF][$budgetKey]) > 0) $answers = $answer["answers"][$copyF][$budgetKey];
        }
        else if (isset($answer["answers"][$copyF][$budgetKey]) && count($answer["answers"][$copyF][$budgetKey]) > 0) $answers = $answer["answers"][$copyF][$kunik];
    }
    else
    {
        if (!empty($budgetKey) && !empty($answer["answers"][$budgetKey])) $answers = $answer["answers"][$budgetKey];
        else if (isset($answers)) $answers = $answers;
    }

    $editBtnL = "";

    $editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='" . $parentForm["_id"] . "' data-collection='" . Form::COLLECTION . "' data-path='params' class='previewTpl edit" . $kunik . "Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

    $paramsData = ["financerTypeList" => Ctenat::$financerTypeList, "limitRoles" => ["Financeur"], "openFinancing" => true];

    $envelopeData = [];

    if (isset($parentForm["params"][$kunik]["tpl"])) $paramsData["tpl"] = $parentForm["params"][$kunik]["tpl"];
    if (isset($parentForm["params"][$kunik]["budgetCopy"])) $paramsData["budgetCopy"] = $parentForm["params"][$kunik]["budgetCopy"];
    if (isset($parentForm["params"][$kunik]["financerTypeList"])) $paramsData["financerTypeList"] = $parentForm["params"][$kunik]["financerTypeList"];
    if (isset($parentForm["params"]["financeurLimitRoles"])) $paramsData["limitRoles"] = $parentForm["params"]["financeurLimitRoles"];
    if (isset($parentForm["params"]["envelope"])) $paramsData["envelope"] = $parentForm["params"]["envelope"];
    if (isset($parentForm["params"][$kunik]["openFinancing"])) $paramsData["openFinancing"] = $parentForm["params"][$kunik]["openFinancing"];


    $properties = [
        // "financerType" => [
        //   "placeholder" => "Type de Financement",
        //       "inputType" => "select",
        //       "list" => "financerTypeList",
        //       "rules" => [
        //           "required" => true
        //       ]
        //   ],
        "poste" => ["inputType" => "text", "label" => "Contexte", "placeholder" => "Contexte", "size" => 4, "rules" => ["required" => true]], "financer" => ["placeholder" => "Financeur", "inputType" => "select", "list" => "financersList", "subLabel" => "Si financeur public, l’inviter dans la liste ci-dessous (au cas où il n’apparait pas demandez à votre référent territoire de le déclarer comme partenaire financeur", "size" => 6, ]];

if (isset($rendermodal) && $rendermodal)
{
    if (!empty($parentForm["config"]))
    {
        $nameconfig = $configForm["name"];
    }
    else
    {
        $nameconfig = "";
    }
    ?>
    <div id="sticky-anchor"></div>
    <div id="sticky">
        <div class="text-left col-md-12 col-sm-12 col-xs-12">
            <ul class="breadcrumb">
                <li ><a><?php echo $nameconfig ?></a></li>
                <li class="active"><a><?php echo $parentForm["name"] ?></a></li>
                <li class="active"><a>financement du proposition : <?php echo @$answer["answers"]["aapStep1"]["titre"] ?> </a></li>
            </ul>
        </div>

    </div>
    <div class="col-xs-12 col-lg-10 col-lg-offset-1 project-detail">
        <?php
        }
        ?>

        <?php
        if (!empty($answers)) echo $this->renderPartial("survey.views.tpls.forms.ocecoform.coremu.financementTable", ["form" => $form, "wizard" => true, "answers" => $answers, "answer" => $answer, "mode" => $mode, "kunik" => $kunik, "answerPath" => $answerPath, "key" => $key, "titleColor" => $titleColor, "properties" => $properties, "copy" => $copy, "copyF" => $copyF, "budgetKey" => $budgetKey, "parentForm" => $parentForm,

            "editBtnLDepense" => $editBtnLDepense, "label" => $label, "editQuestionBtn" => $editQuestionBtn, "editParamsBtn" => $editParamsBtn, "editBtnL" => $editBtnL, "info" => $info,
            //"showForm" => $showForm,
            "paramsData" => $paramsData, "canEdit" => $canEdit, "canAdminAnswer" => $canAdminAnswer, "mappingForm" => $mappingExplode[1], "mappingInput" => $mappingExplode[2]
            //"el" => $el
        ], true);
        else
        {
            if ($mode == "r" || $mode == "pdf")
            { ?>
                <label ><h4 style="color:<?php echo (!empty($titleColor) ? $titleColor : "black"); ?>"><?php echo $label . $editBtnLDepense; ?></h4></label>
                <?php echo $info ?>
                <?php
            }
            else
            {
                ?>
                <label ><h4 style="color:<?php echo (!empty($titleColor) ? $titleColor : "black"); ?>"><?php echo $label . $editQuestionBtn . $editParamsBtn . $editBtnL . $editBtnLDepense; ?></h4></label>
                <?php echo $info;
                if (!isset($budgetKey)) echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> THIS FIELD HAS TO BE CONFIGURED FIRST " . $editParamsBtn . "</span>"; ?>
                <?php
            }

            echo "<div class='financementerror'> il n'y a pas encore de ligne depense </div>";
            //echo $editBtnLDepense ;

        }

        if (isset($rendermodal) && $rendermodal)
        {
        ?>
    </div>
    <?php
}
    ?>

    <?php
    if (isset($parentForm["params"]["financement"]["tpl"]))
    {
        //if( $parentForm["params"]["financement"]["tpl"] == "tpls.forms.equibudget" )
        $this->renderPartial("costum.views." . $parentForm["params"]["financement"]["tpl"], ["totalFin" => $total, "totalBudg" => Yii::app()->session["totalBudget"]["totalBudget"]]);
        // else
        // 	$this->renderPartial( "costum.views.".$parentForm["params"]["financement"]["tpl"]);

    }
    ?>
    <?php if ($mode != "pdf")
{ ?>
    <script>
        if(!notNull(answerId)){
            var answerId = answerId = "<?php echo (string)$answer["_id"]; ?>";
        }
        var formId = <?= json_encode((string)$parentForm["_id"]) ?>;
        function getFonds<?= $kunik ?>(key = null){
            var results = {};
            var params<?= $kunik ?> = {
                //"type" : "video",
                searchType : ["lists"],
                fields : ["name"],
                filters :{
                    form : formId,
                    name : key,
                    type:"fonds"
                },
                notSourceKey : true
            };
            if(key == null){
                delete params<?= $kunik ?>.filters.name;
            }
            ajaxPost(
                null,
                baseUrl+"/" + moduleId + "/search/globalautocomplete",
                params<?= $kunik ?>,
                function(data){
                    results = data.results;
                    if(key != null && key != "" && Object.keys(results).length == 0){
                        $(document).find('.save-finance').on('click',function(){
                            insertFond<?= $kunik ?>(key,formId)
                        });

                    }

                },null,null,{async:false}
            )
            return results;
        };
        function insertFond<?= $kunik ?>(name,formId){
            var tplCtx = {
                collection : "lists",
                path  : "allToRoot",
                value : {
                    name : name,
                    form : formId,
                    type: "fonds"
                }
            }
            dataHelper.path2Value( tplCtx, function(prms) {
            });
        }
        function prepareFond<?= $kunik ?>(){
            var fonds = getFonds<?= $kunik ?>();
            var allFonds = [];
            $.each(fonds,function(k,v){
                allFonds.push(v.name);
            })
            allFonds = allFonds.filter(function( element ) {
                return element !== undefined;
            });
            return allFonds;
        }
        mylog.log(prepareFond<?= $kunik ?>(),"prepareFond<?= $kunik ?>")

        var options<?= $kunik ?> = {
            source: prepareFond<?= $kunik ?>(),
            select: function (event, ui) {
                mylog.log(ui,event,"ioio");
            },
            change : function(event, ui){
                //mylog.log(event.target.value,"ioio");
                getFonds<?= $kunik ?>(event.target.value);
            }
        };

        var rendermodal = <?php echo (!empty($rendermodal) ? $rendermodal : "false") ?> ;

        var financementobj = <?php echo json_encode((isset($financement)) ? $financement : null); ?>;

        var orgsfitotal = <?php echo json_encode((isset($orgsfitotal)) ? $orgsfitotal : null); ?>;

        var financementobjinit = <?php echo json_encode((isset($financement)) ? $financement : null); ?>;

        var statusproposition = <?php echo json_encode((isset($answers["status"])) ? $answers["status"] : []); ?>;

        if(typeof dyFObj.elementObjParams == "undefined")
            dyFObj.elementObjParams = {};

        dyFObj.elementObjParams.budgetInputList = <?php echo json_encode(Yii::app()->session["budgetInputList"]); ?>;
        dyFObj.elementObjParams.financerTypeList = <?php echo json_encode(Ctenat::$financerTypeList); ?>;

        var <?php echo $kunik ?>Data = <?php echo json_encode((isset($answer["answers"][$budgetKey])) ? $answer["answers"][$budgetKey] : null); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsDataDepense = <?php echo json_encode($paramsDataDepense); ?>;
        sectionDyf.<?php echo $kunik ?>envelopeData = <?php echo json_encode($envelopeData); ?>;
        sectionDyf.budgetdepense = {
            jsonSchema : {
                title : "Budget prévisionnel",
                icon : "fa-money",
                text : "Décrire ici les principaux postes de dépenses : à quoi correspondent les coûts ? <br/>Préciser ce qui relève des dépenses de fonctionnement et des dépenses d’investissement.<br/>Les coûts doivent être en <b>hors taxe</b>.",
                properties : <?php echo json_encode($propertiesdepense); ?>,
                save : function () {
                    var today = new Date();
                    tplCtx.setType = [
                        {
                            "path": "price",
                            "type": "int"
                        },
                        {
                            "path": "date",
                            "type": "isoDate"
                        }
                    ];

                    if(notNull(tplCtx.key) && notNull(coremuObj.answerObjdepense.depense[tplCtx.key])){
                        tplCtx.value = coremuObj.answerObjdepense.depense[tplCtx.key];
                    } else {
                        tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
                    }

                    $.each( sectionDyf.budgetdepense.jsonSchema.properties , function(k,val) {
                        tplCtx.value[k] = $("#ajaxFormModal #"+k).val();
                    });

                    tplCtx.value["user"] = userId;

                    var estimateId = Math.floor(Math.random() * 100000);

                    if ( typeof tplCtx.value.price != "undefined" && tplCtx.value.price != 0){
                        if(!notNull(coremuObj.answerObjdepense.depense[tplCtx.key])) {
                            tplCtx.value["estimates"] = {};

                            tplCtx.value["estimates"][estimateId] = {
                                "price": tplCtx.value["price"],
                                "name": " ",
                                "date": today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear(),
                                "addedBy": userId,
                                "pricecomment": "Original",
                                "ispriceselected": true
                            }

                            tplCtx.setType.push({
                                "path": "estimates." + estimateId + ".price",
                                "type": "int"
                            });
                            tplCtx.setType.push({
                                "path": "estimates." + estimateId + ".date",
                                "type": "isoDate"
                            });
                            tplCtx.setType.push({
                                "path": "estimates." + estimateId + ".ispriceselected",
                                "type": "boolean"
                            });
                        }
                    }

                    if (typeof tplCtx.value.group != "undefined") {
                        tplCtxgroup = {
                            id: parentId,
                            collection: "forms",
                            path: "params.<?php echo $kunik ?>.group"
                        }
                        tplCtxgroup.value = sectionDyf.budgetdepenseParamsData.group;
                        $.each(tplCtx.value.group.split(",") , function (ind , val) {
                            if(jQuery.inArray(val, tplCtxgroup.value) == -1) {
                                tplCtxgroup.value.push(val);
                            }
                        });

                        //no need to delete setType
                        dataHelper.path2Value(tplCtxgroup, function (params) {});
                    }
                    if (typeof tplCtx.value.nature != "undefined") {
                        tplCtxnature = {
                            id: parentId,
                            collection: "forms",
                            path: "params.budgetdepense.nature"
                        }
                        tplCtxnature.value = sectionDyf.budgetdepenseParamsData.nature;
                        $.each(tplCtx.value.nature.split(",") , function (ind , val) {
                            if(jQuery.inArray(val, tplCtxnature.value) == -1) {
                                tplCtxnature.value.push(val);
                            }
                        });
                        //no need to delete setType
                        dataHelper.path2Value(tplCtxnature, function (params) {});
                    }

                    var connectedData = ["financer","todo","payed","progress","worker","validFinal","votes"];
                    $.each( connectedData , function(k,attr) {
                        if(notNull("answerObj."+tplCtx.path+"."+attr))
                            tplCtx.value[attr] = jsonHelper.getValueByPath(answerObj,tplCtx.path+"."+attr);
                    });

                    mylog.log("save tplCtx",tplCtx);
                    if(typeof tplCtx.value == "undefined"){}
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.closeForm();
                            var answerId = "<?php echo (string)$answer["_id"]; ?>";
                            ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/newdepense/answerid/' + answerId,
                                {
                                    pos : coremuObj.cache.nextLinePath,
                                    url : window.location.href
                                },
                                function (data) {

                                }, "html");
                            ajaxPost("", baseUrl + "/co2/aap/commonaap/action/notifyAddDepenseLine", {
                                answerId:answerId,
                                depense: tplCtx.value.poste +": "+  tplCtx.value.price + " Euro"
                            }, function(data){}, function(error){}, "json")

                            var tplCtxproject = {};

                            if (projectId != null && projectId != "null") {

                                if (notNull(tplCtx.actionid)) {
                                    tplCtxproject.id = tplCtx.actionid;
                                    tplCtxproject.collection = "actions";
                                    tplCtxproject.path = "name"
                                    tplCtxproject.value = tplCtx.value.poste;
                                    if(exists(tplCtxproject.value))
                                        dataHelper.path2Value(tplCtxproject, function () {});
                                }

                                var answerId = "<?php echo (string)$answer["_id"]; ?>";
                                var cntxtId = "<?php echo $ctxtid; ?>";
                                var cntxtType = "<?php echo $ctxttype; ?>";

                                ajaxPost("", baseUrl + '/survey/form/generateproject/answerId/' + answerId + '/parentId/' + cntxtId + '/parentType/' + cntxtType,
                                    null,
                                    function (data) {
                                    });
                            }
                        });

                        delete tplCtx.setType;

                        // if ($.inArray("newaction", statusproposition) == -1){
                        //     statusproposition.push("newaction");
                        //     tplCtx.value = statusproposition;
                        // }else{
                        //     tplCtx.value = statusproposition;
                        // }

                        // tplCtx.path = "status";

                        ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});

                    }
                },
                beforeBuild : function(data){
                    if(typeof costum != "undefined" && costum != null){
                        costum["budgetgroupe"] = budgetgroupe;
                        costum["budgetnature"] = budgetnature;
                    }
                },
            }
        };
        var FiData = <?php echo json_encode((isset($answer["answers"]["aapStep1"]["depense"])) ? $answer["answers"]["aapStep1"]["depense"] : null); ?>;

        if (FiData != null) {
            if ($.isPlainObject(FiData)){
                FiData = Object.values(FiData);
            }
            var financerList = FiData.reduce(function(accumulator, item){
                if (item != null && typeof item["financer"] != "undefined") {
                    accumulator.push(item["financer"]);
                    return accumulator;
                } else {
                    accumulator.push([]);
                    return accumulator;
                }
            },[]);
        } else {
            var financerList = {};
        }

        var standedAl = <?php echo (!empty($standAlone) ? "true" : "false") ?>;

        var cntxtId = "<?php echo $contextId; ?>";
        var cntxtType = "<?php echo $contextType; ?>";

        var supelementparents = {
            afterSave : function(data) {
                var sendDataM = {
                    parentId : cntxtId,
                    parentType : cntxtType,
                    listInvite : {
                        organizations : {}
                    }
                };
                sendDataM.listInvite.organizations[data.id] = {};
                if(typeof data.name != "undefined"){
                    sendDataM.listInvite.organizations[data.id]["name"] = data.name;
                } else {
                    sendDataM.listInvite.organizations[data.id]["name"] = data.map.name;
                }
                sendDataM.listInvite.organizations[data.id]["roles"] = ["Financeur"];

                ajaxPost("",
                    baseUrl+"/co2/link/multiconnect",
                    sendDataM,
                    function(data) {
                        dyFObj.closeForm();
                        //reloadInput("<?php //echo $key

                        ?>//", "<?php //echo @$form["id"]

                        ?>//");
                        if (rendermodal){
                            reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                        }else{
                            if (typeof inputsList != "undefined"){
                                $.each(inputsList , function (inn, vall) {
                                    $.each(vall , function (inn2, vall2) {
                                        if (typeof vall2.type != "undefined"
                                            && vall2.type.split('.')[vall2.type.split('.').length-1] != "undefined"
                                            && ( vall2.type.split('.')[vall2.type.split('.').length-1] == "budget" ||
                                                vall2.type.split('.')[vall2.type.split('.').length-1] == "financement" ||
                                                vall2.type.split('.')[vall2.type.split('.').length-1] == "suivi"
                                            )
                                        ){
                                            reloadInput(inn2, inn);
                                        }
                                    });
                                });
                            }
                        }
                    },
                    null,
                    "json"
                );
            }
        };

        if (typeof costum != "undefined" && costum != null) {
            costum.searchExist = function (type, id, name, slug, email) {
                mylog.log("costum searchExist : " + type + ", " + id + ", " + name + ", " + slug + ", " + email);
                var data = {
                    type: type,
                    id: id,
                    name: name,
                    map: {slug: slug}
                }
                supelementparents.afterSave(data);
            };
        }

        var <?php echo $kunik ?>DataDepense = <?php echo json_encode((isset($answers)) ? $answers : null); ?>;

        var projectIdDepense = "<?php echo (!empty($answer["project"]["id"]) ? $answer["project"]["id"] : "null"); ?>";

        var budgetgroupeDepenseFi = <?php echo (!empty($paramsDataDepense["group"]) ? json_encode($paramsDataDepense["group"]) : "[]"); ?>;
        var budgetnatureDepenseFi = <?php echo (!empty($paramsDataDepense["nature"]) ? json_encode($paramsDataDepense["nature"]) : "[]"); ?>;

        var parentId = "<?php echo (string)$answer["form"]; ?>";


        if(!notNull(answerObj)){
            var answerObj = <?php echo (!empty($answer) ? json_encode($answer) : "{}"); ?>;
        }

        if(typeof costum != "undefined" && costum != null){
            costum["budgetgroupeDepenseFi"] = budgetgroupeDepenseFi;
            costum["budgetnatureDepenseFi"] = budgetnatureDepenseFi;
        }

        $(document).ready(function() {

            if(!coremuObj.isInit) {
                coremuObj.init(coremuObj, answerId);
            }

            if(notNull(answerId)) {
                ajaxPost("#coremufinancement", baseUrl + '/survey/form/coremufinancement/tableonly/true/depenseid/' + answerId,
                    null,
                    function () {
                        $("#coremufinancement .aapdashboard").removeClass("col-md-offset-1")
                            .removeClass("col-md-10")
                            .addClass("col-md-12")
                            .css("margin-top", "0px");

                        $(".switchfinancementview").on("click", function (e) {
                            e.stopPropagation();
                            var thisbtn = $(this);
                            $(".switchfinancementview").removeClass("active");
                            thisbtn.addClass("active");
                            if (thisbtn.attr("data-action") == "line") {

                                $(".tablefin").fadeOut("slow");
                                $(".sankeyfin").fadeOut("slow");
                                $(".linefin").fadeIn("slow");

                            } else if (thisbtn.attr("data-action") == "table") {

                                $(".tablefin").fadeIn("slow");
                                $(".sankeyfin").fadeOut("slow");
                                $(".linefin").fadeOut("slow");

                            } else if (thisbtn.attr("data-action") == "chart") {
                                $(".tablefin").fadeOut("slow");
                                $(".sankeyfin").fadeIn("slow");
                                $(".linefin").fadeOut("slow");

                            }
                        });
                    }, "html");
            }

            var <?php echo $kunik ?>Data = <?php echo json_encode((isset($answers)) ? $answers : null); ?>;

            $(".edit<?php echo $kunik ?>Depense").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                tplCtx.actionid = $(this).data("actionid");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Depense,null, <?php echo $kunik ?>Data[$(this).data("key")] , null , null , {
                    type : "bootbox",
                    notCloseOpenModal : true,
                });
            });

            sectionDyf.<?php echo $kunik ?> = {
                "jsonSchema" : {
                    "title" : "Plan de Financement",
                    "icon" : "fa-money",
                    "text" : "Décrire ici les financements mobilisés ou à mobiliser. Les coûts doivent être en <b>hors taxe</b>.",
                    "properties" : <?php echo json_encode($properties); ?>,
                    save : function () {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                        });
                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, closePrioModalRel );
                        }

                    }
                }
            };

            sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema" : {
                    "title" : "<?php echo $kunik ?> config",
                    "icon" : "fa-cog",
                    "properties" : {
                        limitRoles : {
                            inputType : "array",
                            label : "Liste des roles financeurs",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsData.limitRoles
                        },
                        envelope : {
                            inputType : "checkboxSimple",
                            label : "Enveloppe",
                            subLabel : "",
                            params : {
                                onText : "Oui",
                                offText : "Non",
                                onLabel : "Oui",
                                offLabel : "Non",
                                labelText : "Enveloppe"
                            },
                            checked : sectionDyf.<?php echo $kunik ?>ParamsData.envelope
                        },
                        envelopeyear : {
                            inputType : "array",
                            label : "Liste des années pris en compte par l'enveloppe <small style='color: red'>(si enveloppe activé)</small>",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsData.envelopeyear
                        },

                    },
                    save : function () {

                        var ttpath = tplCtx.path;
                        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                            tplCtx.value = {};

                            if (k == "limitRoles") {
                                tplCtx.value = getArray('.' + k + val.inputType);
                                tplCtx.path = ttpath + ".financerLimitRoles"
                                if (typeof tplCtx.value == "undefined")
                                    toastr.error('value cannot be empty!');
                                else {
                                    dataHelper.path2Value(tplCtx, function (params) {

                                    });
                                }

                            }else if(k == "envelope"){
                                tplCtx.value = $("#" + k).val();
                                tplCtx.path = ttpath + ".envelope"
                                if (typeof tplCtx.value == "undefined")
                                    toastr.error('value cannot be empty!');
                                else {
                                    dataHelper.path2Value(tplCtx, function (params) {});
                                }

                            }

                        });

                        dyFObj.closeForm();

                        if (rendermodal) {
                            reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                        } else {
                            if (typeof inputsList != "undefined") {
                                $.each(inputsList, function (inn, vall) {
                                    $.each(vall, function (inn2, vall2) {
                                        if (typeof vall2.type != "undefined"
                                            && vall2.type.split('.')[vall2.type.split('.').length - 1] != "undefined"
                                            && (vall2.type.split('.')[vall2.type.split('.').length - 1] == "budget" ||
                                                vall2.type.split('.')[vall2.type.split('.').length - 1] == "financement" ||
                                                vall2.type.split('.')[vall2.type.split('.').length - 1] == "suivi"
                                            )
                                        ) {
                                            reloadInput(inn2, inn);
                                        }
                                    });
                                });
                            }
                        }

                        mylog.log("save tplCtx",tplCtx);


                    }
                }
            };

            sectionDyf.<?php echo $kunik ?>envelopeParams = {
                "jsonSchema" : {
                    "title" : "Enveloppe",
                    "icon" : "fa-envelope",
                    "properties" : {

                    },
                    afterBuild : function(data){
                        $('.envelopelistproperties .removePropLineBtn').hide();
                        $('.envelopelistpropertiesBtn').hide();
                        $('.envelopelistproperties .inputs.properties .col-xs-12 .col-sm-1').removeClass('col-sm-1');
                        $('.envelopelistproperties .inputs.properties .col-xs-12 .col-sm-3').removeClass('col-sm-3').addClass('col-sm-5');
                        $('.envelopelistproperties .inputs.properties .col-xs-12 .col-sm-7').removeClass('col-sm-7').addClass('col-sm-5');
                        $('.envelopelistproperties .inputs.properties .col-xs-12 .addmultifield0').attr('disabled','disabled');
                        $('.envelopelistproperties .inputs.properties .col-xs-12 .addmultifield1').attr('type','number');
                    },
                    save : function () {

                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>envelopeParams.jsonSchema.properties , function(k,val) {
                            //if (k == "envelopelist"){
                                tplCtx.value = getPairsObj('.' + k + val.inputType);
                                dataHelper.path2Value(tplCtx, function (params) {
                                    dyFObj.closeForm();
                                    reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                });
                                mylog.log("save tplCtx",tplCtx);
                            //}
                        });

                    }
                }
            };

            sectionDyf.<?php echo $kunik ?>envelopeParams.jsonSchema.properties['envelopelist' + new Date().getFullYear()] = {
                inputType: "properties",
                labelKey: "Financeur",
                labelValue: "Enveloppe",
                label: "Enveloppe par financeur pour " + new Date().getFullYear(),
                values: sectionDyf.<?php echo $kunik ?>envelopeData[new Date().getFullYear()]

            }

            mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData,null,null,{
                    type : "bootbox",
                    notCloseOpenModal : true,
                });
            });

            $(".envelopeparams").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>envelopeParams,null, sectionDyf.<?php echo $kunik ?>ParamsData , null , null , {
                    type : "bootbox",
                    notCloseOpenModal : true,
                });
            });

            $('.btnValidFinance').off().click(function() {
                tplCtx.pos = $(this).data("pos");

                tplCtx.budgetpath = $(this).data("budgetpath");
                tplCtx.collection = "answers";
                tplCtx.id = $(this).data("id");
                tplCtx.form = $(this).data("form");
                prioModal = bootbox.dialog({
                    message: $(".form-validate-work").html(),
                    title: "État du Financement",
                    show: false,
                    buttons: {
                        success: {
                            label: trad.save,
                            className: "btn-primary",
                            callback: function () {

                                var formInputsHere = formInputs;
                                tplCtx.path = "answers";
                                if( notNull(formInputs [tplCtx.form]) )
                                    tplCtx.path = "answers";

                                tplCtx.path = tplCtx.path+".aapStep1."+tplCtx.pos+".validFinal";

                                var today = new Date();
                                today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();
                                tplCtx.value = {
                                    valid : $(".bootbox #validWork").val(),
                                    user : userId,
                                    date : today
                                };
                                delete tplCtx.pos;
                                delete tplCtx.budgetpath;
                                mylog.log("btnValidateWork save",tplCtx);
                                dataHelper.path2Value( tplCtx, function(){
                                    //saveLinks(answerObj._id.$id,"financeValidated",userId,closePrioModalRel);
                                    closePrioModalRel();
                                } );
                            }
                        },
                        cancel: {
                            label: trad.cancel,
                            className: "btn-secondary",
                            callback: closePrioModal
                        }
                    },
                    onEscape: closePrioModal
                });
                prioModal.modal("show");
            });

        });

        function closePrioModal(){
            prioModal.modal('hide');
        }

        var flistObj = {
            data : null,

            buildList : function (pos) {

                if (typeof financerList[pos] !== "undefined") {
                    $('.financerdialog #financer-list').html("");
                    $.each(financerList[pos], function(fid, fval){

                        var fline = {};

                        if ( typeof fval.line != "undefined" )
                            fline.line = fval.line;

                        if ( typeof fval.amount != "undefined" ){
                            fline.amount = fval.amount;
                        } else {
                            fline.amount = "(pas précisé)";
                        }

                        if ( typeof fval.name != "undefined" ){
                            fline.name = fval.name;
                        } else {
                            fline.name = "(pas précisé)";
                        }

                        if ( typeof fval.email != "undefined" ){
                            fline.email = fval.email;
                        } else {
                            fline.email = "(pas précisé)";
                        }


                        var str = '<li class="" data-pos='+pos+'>'+
                            '<div class="todo-content">'+
                            '<div class="td-title">'+
                            '<h5 class="mb-0">'+fline.name+'</h5>'+
                            '<div class="td-datetime">'+

                            '<div><i class="fa fa-money"></i><span class="td-datetimelabel">'+fline.amount+'</span></div>'+
                            '<div class="td-datetimelabel2"><i class="fa fa-calendar-o"></i><span class="td-datetimelabel">'+fline.line+'</span></div>'+
                            '<div class="td-datetimelabel2"><i class="fa fa-user"></i><span class="td-datetimelabel">'+fline.email+'</span></div>'+
                            '</div>'+
                            '</div>'+
                            '</div>'+
                            '<div class="todo-btn">'+
                            '<button class="td-datetimelabel2 edittodoItem actionbtntodoedit" data-idpos="'+pos+' " ><i class="fa fa-pencil"></i></button>'+
                            '<button class="td-datetimelabel2 deletetodoItem actionbtntododelete" data-idpos="'+pos+' " ><i class="fa fa-trash"></i></button>'+
                            '</div>'+
                            '</li>';

                        $('.financerdialog #financer-list').prepend(str);
                    })
                }

                $('.addtodoItem').off().click(function() {
                    listObj.addItem($(this).data("name"));
                });

                $('.edittodoItem').off().click(function() {
                    listObj.editItem($(this).data("name"), parseInt($(this).data("idpos")));
                });

                $('.deletetodoItem').off().click(function() {
                    listObj.deleteItem($(this).data("name"), parseInt($(this).data("idpos")));
                });

                $('.tododialog #modif-button').on('click', function(e){
                    e.stopImmediatePropagation();
                    var idpos = $(this).data('idpos');
                    var name = $(this).data('name');
                    var worker = [];

                    if ( $('.tododialog .taskname').val() )
                        tasks[name][idpos].task = $('.tododialog .taskname').val();

                    if ( $('.tododialog .credit').val() )
                        tasks[name][idpos].credits = $('.tododialog .credit').val();

                    if ( $('.tododialog .duedate').val() ){
                        var formattedDatetodo =  $('.tododialog .duedate').val().split("-");
                        var ytodo = formattedDatetodo[0];
                        var mtodo  =  formattedDatetodo[1];
                        var dtodo  = formattedDatetodo[2];

                        tasks[name][idpos].endDate = dtodo +'/'+mtodo +'/'+ytodo;
                    }

                    if ( $('.tododialog #new-worker-value').val() ){

                        tasks[name][idpos].contributors = {};
                        if ($('.tododialog #new-worker-value').val() ){

                            worker = $('.tododialog #new-worker-value').val();
                            $.each(worker, function(workerid, workerusername){
                                $.each(member, function(memberid, membervalue){
                                    if (membervalue["username"] == workerusername) {
                                        tasks[name][idpos].contributors[memberid] = {
                                            "type" : membervalue.collection,
                                        }
                                    }
                                })
                            })
                        }
                    }

                    //post edit
                    $('.tododialog .taskname').val("");
                    $('.tododialog .credit').val("");
                    $('.tododialog .duedate').val("");
                    $('.selectMultiple > div a').each(function(){
                        $(this).trigger('click');
                    });

                    $('.tododialog #modif-button').hide();
                    $('.tododialog #add-button').show();

                    listObj.buildList(name);
                });

                $('.tododialog .checkicon').on('click', function(e){
                    listObj.tooglecheckItem($(this).data("name"), parseInt($(this).data("idpos")));

                    $(this).toggleClass("fa-check-circle");
                    $(this).toggleClass("fa-circle-o");
                });
            },

            addItem : function (name) {

                if ( $('.tododialog .taskname').val() &&  $('.tododialog .taskname').val() != "" && $('.tododialog .taskname').val() != undefined) {
                    var today = new Date();
                    today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();

                    var worker = [];

                    var todo = {
                        createdAt : today,
                        userId : userId,
                        checked : "false",
                    };

                    if ( $('.tododialog .taskname').val() )
                        todo.task = $('.tododialog .taskname').val();

                    if ( $('.tododialog .credit').val() )
                        todo.credits = $('.tododialog .credit').val();

                    if ( $('.tododialog .duedate').val() ){
                        todo.endDate = $('.tododialog .duedate').val();
                    }

                    if ( $('.tododialog .duedate').val() ){
                        var formattedDatetodo =  $('.tododialog .duedate').val().split("-");
                        var ytodo = formattedDatetodo[0];
                        var mtodo  =  formattedDatetodo[1];
                        var dtodo  = formattedDatetodo[2];
                        todo.endDate = dtodo +'/'+mtodo +'/'+ytodo;
                    }

                    if ( $('.tododialog #new-worker-value').val() ){

                        todo.contributors = {};
                        if ($('.tododialog #new-worker-value').val() ){

                            worker = $('.tododialog #new-worker-value').val();
                            $.each(worker, function(workerid, workerusername){
                                $.each(member, function(memberid, membervalue){
                                    if (membervalue["username"] == workerusername) {
                                        todo.contributors[memberid] = {
                                            "type" : membervalue.collection
                                        }
                                    }
                                })
                            })
                        }
                    }

                    mylog.log("addItem",todo, tplCtx.editTaskpos);

                    if(todo.task =='') {
                        alert("Please write what you need to do!");
                    } else {
                        var str = '<li class="" data-pos="'+tasks[name].length+'">'+
                            '<div class="todo-content"><i class="fa fa-circle-o checkicon"></i>'+
                            '<div class="td-title">'+
                            '<h5 class="mb-0">'+todo.task+'</h5>'+
                            '<div class="td-datetime">'+

                            '<div><i class="fa fa-money"></i><span class="td-datetimelabel">'+todo.credits+'</span></div>'+
                            '<div class="td-datetimelabel2"><i class="fa fa-calendar-o"></i><span class="td-datetimelabel">'+todo.endDate+'</span></div>'+
                            '<div class="td-datetimelabel2"><i class="fa fa-user"></i><span class="td-datetimelabel">'+worker+'</span></div>'+
                            '</div>'+
                            '</div>'+
                            '</div>'+
                            '<div class="todo-btn">'+
                            '<button class="td-datetimelabel2 edittodoItem actionbtntodoedit" data-name="'+name+'" data-idpos="'+tasks[name].length+'" ><i class="fa fa-pencil"></i></button>'+
                            '<button class="td-datetimelabel2 deletetodoItem actionbtntododelete" data-name="'+name+'" data-idpos="'+tasks[name].length+'" ><i class="fa fa-trash"></i></button>'+
                            '</div>'+
                            '</li>';

                        $('.tododialog #todos-list').prepend(str);
                    }

                    // listObj.buildList(todo, tplCtx.editTaskpos);
                    // listObj.btnInit();

                    // if( notNull(tplCtx.form) && notNull(formInputs [tplCtx.form]) ){
                    if( typeof tasks[name] == "undefined")
                        tasks[name] = [];
                    tasks[name].push(todo);
                    // }
                    // else if( notNull(tplCtx.budgetpath) ) {
                    // 	if( typeof answerObj.answers[tplCtx.budgetpath][tplCtx.pos].todo == "undefined")
                    // 		answerObj.answers[tplCtx.form][tplCtx.budgetpath][tplCtx.pos].todo = [];
                    //   	answerObj.answers[tplCtx.budgetpath][tplCtx.pos].todo.push(todo);
                    // }

                    $('.tododialog .taskname', '.tododialog .credit', '.tododialog .duedate').val("");

                    // $(".bootbox #new-todo-item, .bootbox #new-todo-item-date, .bootbox #new-todo-item-price").val("");
                }
                $('.selectMultiple todoinput').removeClass("open");
            },

            deleteItem : function (name, pos) {
                bootbox.dialog({
                    title: trad.confirmdelete,
                    message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                    buttons: [
                        {
                            label: "Ok",
                            className: "btn btn-primary pull-left",
                            callback: function() {
                                $('li[data-pos="'+pos+'"]').fadeOut('slow', function() {
                                    tasks[name].splice(pos, 1);
                                    listObj.buildList(name);
                                });
                            }
                        },
                        {
                            label: "Annuler",
                            className: "btn btn-default pull-left",
                            callback: function() {}
                        }
                    ]
                });
            },

            editItem : function (name, idpos) {

                if ( typeof tasks[name][idpos].task !== "undefined" )
                    $('.tododialog .taskname').val(tasks[name][idpos].task);

                if ( typeof tasks[name][idpos].credits !== "undefined" ){
                    $('.tododialog .credit').val(tasks[name][idpos].credits);
                }else {
                    $('.tododialog .credit').val("");
                }

                if ( typeof tasks[name][idpos].endDate !== "undefined" ){
                    var formattedDatetodo =  tasks[name][idpos].endDate.split("/");
                    var dtodo = formattedDatetodo[0];
                    var mtodo  =  formattedDatetodo[1];
                    var ytodo  = formattedDatetodo[2];
                    $('.tododialog .duedate').val(ytodo +'-'+mtodo +'-'+dtodo);
                }else{
                    $('.tododialog .duedate').val("");
                }


                $('.selectMultiple > div a').each(function(){
                    $(this).trigger('click');
                });

                if ( typeof tasks[name][idpos].contributors !== "undefined" ){
                    var worker = [];

                    $.each(tasks[name][idpos].contributors, function(cbId, cbValue){
                        if (typeof member[cbId] !== "undefined") {
                            $('.selectMultiple ul li:contains('+member[cbId].name+')').trigger("click");
                        }
                    })

                    // $('.tododialog #new-worker-value').val(worker);
                }else{
                    $('.tododialog #new-worker-value').val([]);
                }

                $('.tododialog #modif-button').data("idpos", idpos);
                $('.tododialog #modif-button').data("name", name);

                $('.tododialog #modif-button').show();
                $('.tododialog #add-button').hide();

            },

            tooglecheckItem : function (name, idpos) {

                if ( typeof tasks[name][idpos].checked !== "undefined" && tasks[name][idpos].checked == "true" ){
                    tasks[name][idpos].checked = "false";

                } else {
                    tasks[name][idpos].checked = "true";
                    tasks[name][idpos].checkedUserId = userId;
                }

            },

            // editItem : function (e, item) {
            //   e.preventDefault();
            // 	tplCtx.editTaskpos = $(item).parent().parent().data("pos");
            //   $(".bootbox #new-todo-item").val( $(item).parent().parent().find(".liText").text() );
            //   $(".bootbox #new-todo-item-date").val( $(item).parent().parent().data("when") );
            //   $(".bootbox #new-todo-item-price").val( $(item).parent().parent().data("price") );
            //   $(".bootbox #new-todo-item-who option").prop("selected",false)
            //   if($(item).parent().parent().data("who").indexOf(",")){
            //   	whosT = $(item).parent().parent().data("who").split(",");
            //   	$.each(whosT,function(i,u){
            //   		$(".bootbox #new-todo-item-who option[value='"+u+"']").prop("selected",true)
            //   	})
            //   }else
            // 	  $(".bootbox #new-todo-item-who").val( $(item).parent().parent().data("who") );
            // },

            btnInit : function() {
                // $(".bootbox #todo-list").on('click', '.todo-item-delete', function(e){
                // 	var item = this;
                // 	listObj.deleteItem(e, item)
                // })
                // $(".bootbox #todo-list").on('click', '.todo-item-edit', function(e){
                // 	var item = this;
                // 	listObj.editItem(e, item)
                // })
                // $(".bootbox .todo-item-done").off().on('click', listObj.completeItem);
                // // $(".bootbox .liTodo").on('click', function(e){
                // // 	var item = this;
                // // 	editItem(e, item)
                // // })
            }
        }

    </script>
    <?php
} ?>
    <?php
}
else
{
    //echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";

} ?>

