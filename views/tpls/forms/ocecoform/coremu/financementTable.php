<?php
HtmlHelper::registerCssAndScriptsFiles(array(
    '/css/graphbuilder.css',
    '/css/aap/aapGlobalDashboard.css',
), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );
?>

<style type="text/css">
    .btn-group.depenseoceco {
        display: flex;
    }

    .progress-bar-nf {
        background-color: #c3ccba;
        color: #607D8B;
    }

    .depenseoceco .btn {
        flex: 1
    }

    .pr_div {
        display: flex;
    }

    .pr_div table {
        flex: 1
    }


    .mr-3, .mx-3 {
        margin-right: 1rem !important;
    }

    .media-body {
        -webkit-box-flex: 1;
        -ms-flex: 1;
        flex: 1;
    }

    .mb-0, .my-0 {
        margin-bottom: 0 !important;
    }

    .align-items-center {
        -webkit-box-align: center !important;
        -ms-flex-align: center !important;
        align-items: center !important;
    }
    .selected-price {
        background-color:#e5ffe5;
    }

    .oceco-styled-table {
        border-collapse: collapse;
        margin: 25px 0;
        font-size: 0.9em;
        font-family: sans-serif;
        min-width: 400px;
        box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
    }

    .oceco-styled-table thead tr {
        background-color: #009879;
        color: #ffffff;
        text-align: left;
    }

    .oceco-styled-table th,
    .oceco-styled-table td {
        padding: 12px 15px;
    }

    .oceco-styled-table tbody tr {
        border-bottom: 1px solid #dddddd;
    }

    .oceco-styled-table tbody tr:nth-of-type(even) {
        background-color: #f3f3f3;
    }

    .oceco-styled-table tbody tr:nth-of-type(odd) {
        background-color: #e5ffe5;
    }

    .oceco-styled-table tbody tr:last-of-type {
        border-bottom: 2px solid #009879;
    }

    .oceco-styled-table tbody tr.active-row {
        font-weight: bold;
        color: #009879;
    }

    .oceco-styled-table tbody.oceco-blanc-table tr{
        background-color: #fff;
    }

    .btn-group.special {
        display: flex;
    }

    .special .btn {
        flex: 1
    }

    .financementbadgebtn {
        /*display: inline-block;*/
        border-color: #6e899e; color:#6c7a89; font-size :14px
    }

    .legend {
        font-size: 14px;
    }
    rect {
        cursor: pointer;
        stroke-width: 2;
    }
    rect.disabled {
        fill: transparent !important;
    }

</style>

<div class="single_funding <?= (empty($answer["answers"]["aapStep1"]["depense"]) && $mode != "fa") ? "hidden" : "" ?>">
    <div class="funding_header padding-15">
        <h4>Financement <?php if ($mode == "w") echo $editBtnLDepense; ?> <?php if ($mode == "fa") echo $editQuestionBtn.$editParamsBtn; ?> <button type="button" class="btn FinanceAllLine" data-id="<?= $answer["_id"] ?>" data-budgetpath="depense" data-form="aapStep1" > Financer l'ensemble</button>
            <?php
            if (isset($paramsData["envelope"]) && $paramsData["envelope"] == 'true' && (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) || Form::canAdmin((string)$parentForm["_id"]))){
                ?>
                <button type="button" class="btn envelopeparams" data-id='<?= $parentForm["_id"] ?>' data-collection='<?= Form::COLLECTION ?>' data-path='envelope'><i class="fa fa-envelope"></i></button>
                <?php
            }
            ?>

            <button class="btn switchfinancementview active" data-action="line" type="button" data-depenseid="<?php echo (string)$answer["_id"] ?>" >
                <i class="budgetviewicon fa fa-menu"></i>
                <b> En mode Detaillé </b>
            </button>

            <button class="btn switchfinancementview" data-action="table" type="button" data-depenseid="<?php echo (string)$answer["_id"] ?>" >
                <i class="budgetviewicon fa fa-table"></i>
                <b> En mode Compact </b>
            </button>

            <button class="btn switchfinancementview" data-action="chart" type="button" data-depenseid="<?php echo (string)$answer["_id"] ?>" >
                <i class="budgetviewicon fa fa-chart"></i>
                <b> En mode Graph </b>
            </button>

            <span>
                    <i class="fa fa-euro"></i>
                </span>
        </h4>
    </div>
    <div class="funding_body">
        <div class="panel-body no-padding linefin " id="coremufinancementbody">

        </div>

        <div id="coremufinancement" class="panel-body no-padding">

        </div>

        <div id="" class="panel-body no-padding sankeyfin" >
            <div class="col-md-offset-1 col-md-10 col-xs-12 aapdashboard sankeyfin" style="display: none">

                <div class="col-md-12 col-sm-12 col-xs-12 aaptilescont">
                    <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
                        <span class="aaptilestitles3"> <h5> Sankey</h5> </span>

                        <div class="aapinnertiles3 col-md-12" id="sankeyfinancement" data-aapdashcallback="viewinfo" style="padding: 50px 0 50px 0;">

                        </div>

                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 aaptilescont">
                    <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
                        <span class="aaptilestitles3"> <h5> Distribution par financeur</h5> </span>

                        <div class="aapinnertiles3 col-md-12" id="financementDistrPie" data-aapdashcallback="viewinfo" style="padding: 50px 0 50px 0;">

                        </div>

                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 aaptilescont">
                    <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
                        <span class="aaptilestitles3"> <h5> Distribution par candidat</h5> </span>

                        <div class="aapinnertiles3 col-md-12" id="candidatePieContainer" data-aapdashcallback="viewinfo" style="padding: 50px 0 50px 0;">

                        </div>

                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 aaptilescont">
                    <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
                        <span class="aaptilestitles3"> <h5> Distribution par tags</h5> </span>

                        <div class="aapinnertiles3 col-md-12" id="financementTagsPie" data-aapdashcallback="viewinfo" style="padding: 50px 0 50px 0;">

                        </div>
                    </div>
                </div>

            </div>
        </div>


    </div>

</div>


<script type="text/javascript">
    $(document).ready(function() {


    });
</script>
