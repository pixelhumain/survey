<?php
    $me = Yii::app()->session["userId"];
    $myVote = "";
    $ct = [
        "pour"   => 0,
        "neutre" => 0,
        "contre" => 0,
        "total"  => !empty($answer["answers"]["aapStep2"]['pourContre']) ? count($answer["answers"]["aapStep2"]['pourContre']) : 0
    ];
    
    if (!empty($answer["answers"]["aapStep2"]['pourContre'])) {
        foreach ($answer["answers"]["aapStep2"]['pourContre'] as $i => $v) {
            if ($v == "1"){
                $myVote = $me == $i ? "Pour" : "";
                $ct["pour"]++;
            }
            else if ($v == "0"){
                $myVote = $me == $i ? "Neutre" : "";
                $ct["neutre"]++;
            }
            else if ($v == "-1"){
                $myVote = $me == $i ? "Contre" : "";
                $ct["contre"]++;
            }
        }
    }
    $ct["pour%"] = $ct["pour"] * $ct["total"] * 100;
    $ct["neutre%"] = $ct["neutre"] * $ct["total"] * 100;
    $ct["contre%"] = $ct["contre"] * $ct["total"] * 100;
    if ($ct["pour"] == 0 && $ct["contre"] == 0) {
        $ct["neutre%"] = 100;
    }
?>

<?php if (!empty($mode) && in_array($mode, ['w', 'fa'])): ?>
    <div class="">
        <?php if (!isset($answer["answers"]["aapStep2"]['pourContre'][$me])) { ?>
            <h3>Votez</h3>
        <?php  } ?>
        <?php if (isset($answer["answers"]["aapStep2"]['pourContre'][$me])) { ?>
            <div class="alert alert-success">
                <span class="h3"><?= Yii::t("common", "Thank you for voting") ?> 
                    <label class="badge" for=""><?= $myVote?></label>!
                </span>
            </div>
        <?php  } ?>

        <div class="" role="group" style="width: 100%; margin-left: 10%;margin-right: 10%">
            <a href="javascript:;" class="voteBtn btn" data-vote="1"
               style="width:30%;background-color:#edf7fd;font-size: 20px; color: #2c6335; border-color: #2c6335;">Pour</a>
            <a href="javascript:;" class="voteBtn btn btn-default" data-vote="0" style="width:20%;font-size: 20px;z-index: 2;">Neutre</a>
            <a style="font-size: 20px;width:30%;background-color:#f8efe7;color: #db4300; border-color:#db4300" href="javascript:;"
               class="voteBtn btn " data-vote="-1">Contre</a>
        </div>
    </div>
<?php endif; ?>

<div class="container-yes-or-not" style="margin-right: 10%; margin-left: 10%;">
    <h2>Les votes</h2>
    <div class="progress" style="margin-bottom: 10px;background-color: #d9534f;">
        <div class="progress-bar progress-bar-success" style=" width:<?php echo $ct["pour%"] ?>%; ">Pour</div>
        <div class="progress-bar progress-bar-success" style="width:<?php echo $ct["neutre%"] ?>%; ">Neutre</div>
        <div class="progress-bar progress-bar-danger" style=" width:<?php echo $ct["contre%"] ?>%; ">Contre</div>
    </div>

    <table class="table table-bordered table-hover" style="font-size:19px">
        <tbody class="">
        <tr>
            <td>Nombre de votant</td>
            <td><?php echo $ct["total"] ?></td>
        </tr>

        <tr>
            <td>Pour</td>
            <td><?php echo $ct["pour%"] ?>%</td>
        </tr>
        <tr>
            <td>Neutre</td>
            <td><?php echo $ct["neutre%"] ?>%</td>
        </tr>
        <tr>
            <td>Contre</td>
            <td><?php echo $ct["contre%"] ?>%</td>
        </tr>

        <tr>
            <td>Communautés</td>
            <td><?php echo count(isset($context["links"]["members"]) ? $context["links"]["members"] : []) ?></td>
        </tr>
        <tr>
            <td>Nombre de vote minimum</td>
            <td><a href="javascript:;"
                   class="btn btn-default minimumVotes"><?php echo (isset($amswer["inputConfig"]["pourContre"]["minimumVotes"])) ? $amswer["inputConfig"]["pourContre"]["minimumVotes"] : "50%" ?></a>
            </td>
        </tr>
        </tbody>
    </table>
</div>


<script type="text/javascript">

    jQuery(document).ready(function () {

        mylog.log("render", "survey/views/tpls/forms/ocecoform/multiDecide.php");

        $(".voteBtn").on("click", function (event) {
            saveVote($(this).data("vote"))
        })
        $(".minimumVotes").on("click", function (event) {
            bootbox.prompt({
                inputType : 'number',
                title : "Valeur ? <span style='color:red'>(uniquement des chiffres)</span>",
                callback : function (result) {
                    if (result === null) {
                        //alert("null");
                    } else {
                        data = {
                            collection : "answers",
                            id : answerObj._id.$id,
                            path : "inputConfig.pourContre.minimumVotes",
                            value : result
                        }
                        mylog.log(".minimumVotes", "data", data);

                        dataHelper.path2Value(data, function (params) {
                            mylog.log("saveVisited saved", params);
                            // urlCtrl.loadByHash( location.hash );
                            reloadWizard();
                        });
                    }
                }
            });
        })
    });

    function saveVote(vote) {
        var saveObj = {
            collection : "answers",
            id : answerObj._id.$id,
            path : "answers.aapStep2.pourContre." + userId,
            value : vote
        };

        mylog.log("saveVisited", saveObj);

        dataHelper.path2Value(saveObj, function (params) {
            mylog.log("saveVisited saved", params);
            toastr.success(trad.saved);
            reloadInput("<?php echo $key ?>", "<?php echo $formId ?>");
        });
    }
</script>

<!--<div class="col-lg-offset-2 col-lg-8 col-xs-12 center">
	<h2>TODO</h2>
	<ul>
		<li> prendre en compte le combre de votant de la communauté</li>
		<li> parametrable que le vote à de la valeur à partir de combien de votant </li>
		<li> pie chart distribution des votes</li>
		<li> droit vote : interne , externe, public, privé, sur invitation </li>
	</ul>
</div>-->
