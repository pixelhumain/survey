<?php
if ($canEditForm){
?>
<div class="col-lg-offset-2 col-lg-8 col-xs-12 margin-bottom-20 text-left">
	<select class="multiDecideSelect form-control" style="width: 100%; ">
		<option value="tpls.forms.ocecoform.pourContre">Type de Gouvernance et Décision</option> 
		<option value="tpls.forms.ocecoform.decideFromBudget" <?= isset($parentForm["inputConfig"]["multiDecide"]) && $parentForm["inputConfig"]["multiDecide"] == "tpls.forms.ocecoform.decideFromBudget"? "selected":"" ?> > Votez chaque ligne du projet</option>
		<option value="tpls.forms.ocecoform.pourContre" <?= isset($parentForm["inputConfig"]["multiDecide"]) && $parentForm["inputConfig"]["multiDecide"] == "tpls.forms.ocecoform.pourContre"? "selected":"" ?>> Vote simple pour ou contre(todo)</option>
		<option value="tpls.forms.ocecoform.evaluation" <?= isset($parentForm["inputConfig"]["multiDecide"]) && $parentForm["inputConfig"]["multiDecide"] == "tpls.forms.ocecoform.evaluation"? "selected":"" ?>> Evaluation Ariane</option>
		<option value="tpls.forms.aap.selection" <?= isset($parentForm["inputConfig"]["multiDecide"]) && $parentForm["inputConfig"]["multiDecide"] == "tpls.forms.aap.selection"? "selected":"" ?>> Evaluation à tableau 2D</option>
	</select>
</div>
<?php
}
?>


<script type="text/javascript">

jQuery(document).ready(function() {
    
    mylog.log("render","survey/views/tpls/forms/ocecoform/multiDecide.php");
    
    $( ".multiDecideSelect" ).on( "change", function( event ) {
		saveDecide($(this).val())
	})
});
function saveDecide(type){  
    var saveObj = {
        collection : "forms",
        id : answerObj.form,
        path : "inputConfig.multiDecide",
        value : type
    };

    mylog.log("saveVisited", saveObj );
      
    dataHelper.path2Value( saveObj , function(params) { 
        mylog.log("saveVisited saved",params );
        if(typeof newReloadStepValidationInputGlobal != "undefined") {
            newReloadStepValidationInputGlobal({
                inputKey : <?php echo json_encode($key) ?>,
                inputType : "tpls.forms.ocecoform.multiDecide" 
            })
        }
        reloadWizard();
    } );
}
</script>