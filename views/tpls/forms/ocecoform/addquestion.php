<style type="text/css">
	.op-addquestion {
		border-radius: 15px;
		margin: 0 auto;
	}
</style>
<div class="form-group" style="overflow-x: auto;">
    <div style="display: flex;">
<?php 
	$isAdmin = false;
	if (!empty($contextId) && !empty($contextType)) {
		$elCom = Element::getCommunityByTypeAndId($contextType, $contextId);
		if(isset($elCom[Yii::app()->session["userId"]]) && $elCom[Yii::app()->session["userId"]] != null && $elCom[Yii::app()->session["userId"]]["isAdmin"] == true){
			$isAdmin = true;
			$inputsEl = PHDB::findOne(Form::INPUTS_COLLECTION, array( "parent.".$contextId => array('$exists'=>1), "subForm" => $form["id"] ));
		}
	}
	
	if ($isAdmin) {
?>
	<button type="button" class="btn btn-primary op-addquestion text-center"><?php echo $label ?></button>

<?php
	}
?>
</div>
</div>
<script type="text/javascript">
	var input_col_id = "<?php echo (isset($inputsEl["_id"]) ? (string)$inputsEl["_id"] : ""); ?>";
	var formInputs = <?php echo (isset($form["inputs"]) ? json_encode($form["inputs"]) : "[]") ?>;

	$('.op-addquestion').off().click(function() { 
		// reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
		var activeForm = {
            "jsonSchema" : {
                "title" : tradForm["addQuestion"],
                "type" : "object",
                "properties" : {
                    label : { label : "Titre de la Question" 
                			},
                    placeholder : { label : "Texte dans la Question" },
                    info : { 
                        inputType : "textarea",
                        label : "Information complémentaire sur la Question",
                        markdown : true
                    },
                    type : { label : "Type de cette question",
                             inputType : "select",
                             options : <?php echo json_encode(Form::inputTypes()); ?>,
                             value : "text" 
                    }
                }
            }
        };                 
        
        activeForm.jsonSchema.save = function () {  
            var formInputsHere = formInputs;
            if( notNull(formInputs[tplCtx.form]) )
                formInputsHere = formInputs[ tplCtx.form ];

            // var inputCt = (formInputsHere == null) ? "1" : (Object.keys(formInputsHere).length+1);
            // if(notNull(formInputsHere[tplCtx.form+inputCt]))
            //     inputCt = inputCt+"x";

            var maxform<?php echo $form["id"] ?> = 0;
            var nextValuekeyform<?php echo $form["id"] ?> = 0;

            if(notNull(formInputsHere)){
                $.each(formInputsHere, function(indf, valf){
                    if(parseInt(indf.substring(indf.length - 2, indf.length)) > maxform<?php echo $form["id"] ?>){
                        maxform<?php echo $form["id"] ?> = parseInt(indf.substring(indf.length - 2, indf.length));
                }
            });

                nextValuekeyform<?php echo $form["id"] ?> = maxform<?php echo $form["id"] ?> + 1;

            }

            var inputCt = nextValuekeyform<?php echo $form["id"] ?>;
            if(notNull(formInputsHere[tplCtx.form+inputCt]))
                inputCt = inputCt+"x";
            //alert("inputs."+tplCtx.form+inputCt);i
            
            tplCtx.id = input_col_id;
        	tplCtx.collection = "<?php echo Form::INPUTS_COLLECTION ?>";   
            tplCtx.path = "inputs.<?php echo $form["id"] ?>"+inputCt;
            tplCtx.value = {
                label : $("#label").val(),
                type : $("#type").val()
            };
            if( $("#placeholder").val() != "" )
                tplCtx.value.placeholder = $("#placeholder").val();
            if( $("#info").val() != "" )
                tplCtx.value.info = $("#info").val();

            delete tplCtx.form;
            mylog.log("activeForm save tplCtx",tplCtx);
            if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
            else {
                dataHelper.path2Value( tplCtx, function(params) { 
                    if(tplCtx.value.type == "tpls.forms.cplx.openDynform" ){
                        delete tplCtx.path;
                        delete tplCtx.id;
                        tplCtx.value = {
                            id : tplCtx.inputId,
                            name : tplCtx.value.label,
                            type : "dynform"
                        };
                        mylog.log("create openDynform save tplCtx",tplCtx);
                        dataHelper.path2Value( tplCtx, function(params) { 
                            $("#ajax-modal").modal('hide');
                            urlCtrl.loadByHash(location.hash);
                        } );

                    } else {
                        $("#ajax-modal").modal('hide');
                        urlCtrl.loadByHash(location.hash);
                    }

                } );
            }

        }

        dyFObj.openForm( activeForm );
	});
</script>