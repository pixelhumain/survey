<?php

?>

<style type="text/css">

    hr.rounded {
      border-top: 5px solid #bbb;
      border-radius: 5px;
      margin-top: 5px;
      margin-top: 5px;
    }

    .btn-group.depenseoceco {
        display: flex;
    }

    .depenseoceco .btn {
        flex: 1
    }

    .pr_div {
        display: flex;
    }

    .pr_div table {
        flex: 1
    }

    .action-btn {
        background-color: transparent;
        font-size: 14px;
    }

    .action-btn[data-action='disconect']{
        border: solid 2px #7da53d;
        color: #7da53d;
    }

    .action-btn[data-action='participate']{
        border: solid 2px #9ea196;
        color: #9ea196;
    }

    .finish-btn {
        background-color: transparent;
        font-size: 14px;
    }

    .finish-btn[data-action='todo']{
        border: solid 2px #7da53d;
        color: #7da53d;
    }

    .finish-btn[data-action='done']{
        border: solid 2px #9ea196;
        color: #9ea196;
    }

    .mr-3, .mx-3 {
        margin-right: 1rem !important;
    }

    .media-body {
        -webkit-box-flex: 1;
        -ms-flex: 1;
        flex: 1;
    }

    .mb-0, .my-0 {
        margin-bottom: 0 !important;
    }

    .align-items-center {
        -webkit-box-align: center !important;
        -ms-flex-align: center !important;
        align-items: center !important;
    }
    .selected-price {
        background-color:#e5ffe5;
    }

    .oceco-styled-table {
        border-collapse: collapse;
        margin: 25px 0;
        font-size: 0.9em;
        font-family: sans-serif;
        min-width: 400px;
        box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
    }

    .oceco-styled-table thead tr {
        background-color: #009879;
        color: #ffffff;
        text-align: left;
    }

    .oceco-styled-table th,
    .oceco-styled-table td {
        padding: 12px 15px;
    }

    .oceco-styled-table tbody tr {
        border-bottom: 1px solid #dddddd;
    }

    .oceco-styled-table tbody tr:nth-of-type(even) {
        background-color: #f3f3f3;
    }

    .oceco-styled-table tbody tr:nth-of-type(odd) {
        background-color: #e5ffe5;
    }

    .oceco-styled-table tbody tr:last-of-type {
        border-bottom: 2px solid #009879;
    }

    .oceco-styled-table tbody tr.active-row {
        font-weight: bold;
        color: #009879;
    }

    .oceco-styled-table tbody.oceco-blanc-table tr{
        background-color: #fff;
    }

    .btn-group.special {
        display: flex;
    }

    .special .btn {
        flex: 1
    }
</style>

<div class="form-group">
    <div class="single_task activitypanel" >
        <div class="task_header padding-15">
            <h4> Activité <?php echo $editQuestionBtn . $editParamsBtn . $editBtnL ; ?>
                <button class="btn tooglesuivi" data-div=".activitypanel" data-div2=".paypanel"> Payement </button>
                <span>
                    <i class="fa fa-check-square-o "></i>
                </span>
            </h4>
        </div>
        <div class="task_body" style="background-color: #eeeeee">
            <?php
            //if (isset($answer["project"]["id"]))
                /*$actionIdArray = [];
                $answer["answers"]["aapStep1"]["depense"] = $answer["answers"]["aapStep1"]["depense"] ?? [];
                foreach ($answer["answers"]["aapStep1"]["depense"] as $depK => $depV){
                    if(isset($depV["actionid"]))
                        $actionIdArray[] = new MongoId($depV["actionid"]);
                }*/

                $actionNoLDD = $actions;

                if(empty($answer["answers"]["aapStep1"]["depense"])){
                    $answer["answers"]["aapStep1"]["depense"]=[];
                }

                foreach ($answer["answers"]["aapStep1"]["depense"] as $depK => $depV){
                    if(isset($depV["actionid"])){
                        unset($actionNoLDD[$depV["actionid"]]);
                    }
                }

                foreach ($actionNoLDD as $kNoLDD => $vNoLDD) {
                    $answer["answers"]["aapStep1"]["depense"][] = ["actionid"=>$kNoLDD,"poste"=> $vNoLDD["name"],"noLdd" => true];
                }
            if (!empty($answer["answers"]["aapStep1"]["depense"]))
            {
                $icont = 1;
                foreach ($answer["answers"]["aapStep1"]["depense"] as $depId => $dep)
                {

                    ?>
                    <div class="table-responsive <?php echo !empty($dep["noLdd"]) ? 'no-ldd-action' : '' ?>" style="margin-top: 20px; border: 0; " /*box-shadow: 0 4px 2px -2px #e0e0e0;*/">
                        <div class="col-md-offset-1 col-md-10" style="box-shadow: rgba(149, 157, 165, 0.2) 0px 8px 24px; background-color: #ffffff; margin-top: 20px;">
                            <div class="col-md-4 col-sm-6" style="padding-top: 20px; ">

                                    <?php
                                    if (isset($dep["validFinal"]["valid"]) && $dep["validFinal"]["valid"] == "validated" ){
                                        ?>
                                        <h3 class="widget-title" style="color: #7da53d; text-align: left; font-size: 18px">
                                            <i class="fa fa-check-square"></i>
                                            <?php echo $icont."- ". @$dep["poste"]; ?> <?php echo !empty($dep["noLdd"]) ? "<small>(sans LDD)</small>" : "" ?>
                                        </h3>
                                        <?php
                                    } else {
                                    ?>
                                        <h3 class="widget-title" style="text-align: left; font-size: 18px">
                                            <?php echo $icont."- ".@$dep["poste"]; ?> <?php echo !empty($dep["noLdd"]) ? "<small>(sans LDD)</small>" : "" ?>
                                        </h3>
                                    <?php
                                        }
                                    ?>

                            </div>
                            <div class="col-md-3 col-sm-6" style="padding-top: 20px">
                                <?php
                                    $totaltask = 0;
                                    $completedtask = 0;
                                    if (!empty($answer["answers"]))
                                    {
                                        if (isset($answer["project"]["id"]))
                                        {
                                            if (!empty($projectId) && MongoId::isValid($projectId)) {

                                                if (!isset($dep["actionid"])) {
                                                $actions = PHDB::find(Actions::COLLECTION, ["parentId" => $projectId, "parentType" => Project::COLLECTION, "name" => $dep["poste"]]);
                                                }else{
                                                    $actions = PHDB::findOneById(Actions::COLLECTION, $dep["actionid"]);
                                                    $actions = [$actions];
                                                }
                                            }
                                            else
                                            {
                                                $actions = [];
                                            }

                                            $allactid = "";
                                            foreach ($actions as $aid => $aval){
                                                if (!isset($dep["actionid"])) {
                                                    $allactid = $aid;
                                                }else{
                                                    $allactid = $dep["actionid"];
                                                }
                                            }

                                            $actions = array_reduce($actions, function ($carry, $item)
                                            {
                                                if (!empty($item["tasks"]))
                                                {
                                                    $carry = array_merge($carry, $item["tasks"]);
                                                }
                                                return $carry;
                                            }
                                                , []);


                                            foreach ($actions as $uid => $esti)
                                            {
                                                if (isset($esti["checked"]) && $esti["checked"] == "true")
                                                {
                                                    $completedtask++;
                                                }
                                                $totaltask++;
                                            }
                                        }
                                    }
                                    $progress = 0;

                                    if ($completedtask != 0 && $totaltask != 0)
                                    {
                                        $progress = intval(($completedtask / $totaltask) * 100);
                                    }
                                    ?>

                                <?php
                                if (intval($progress) > 0){
                                ?>
                                <div class="progress">

                                        <div class="progress-bar progress-bar-success2" role="progressbar" style="width:<?php echo $progress ?>%">
                                            <small><?php echo $progress ?>%</small>
                                        </div>


                                </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="col-md-5" style="padding-top: 20px;text-align: right;">
                                <?php
                                if (isset($dep["validFinal"]["valid"]) && $dep["validFinal"]["valid"] == "validated" ){
                                    ?>
                                    <button type="button" class="btn btn-sm finish-btn finishaction" data-pos="<?= $depId ?>" data-id="<?= (string)$answer["_id"] ?>" data-actionid="<?= $allactid ?>" data-ansaction="reserved" data-action="todo"> terminé </button>
                                <?php
                                    }else{
                                    ?>
                                    <button type="button" class="btn btn-sm finish-btn finishaction" data-pos="<?= $depId ?>" data-id="<?= (string)$answer["_id"] ?>" data-actionid="<?= $allactid ?>" data-ansaction="validated" data-action="done"> terminer </button>
                                <?php
                                }
                                ?>
                                <?php
                                if (!empty($allactid))
                                {
                                    $communityLinksAct = Element::getCommunityByTypeAndId(Actions::COLLECTION, $allactid);

                                }
                                if (!isset($communityLinksAct[Yii::app()->session["userId"]])){
                                    ?>
                                <button type="button" class="btn btn-sm action-btn participeaction" data-id="<?= $allactid ?>" data-action="participate"> Participer </button>
                                <?php
                                    }else{
                                    ?>
                                    <button type="button" class="btn btn-sm action-btn participeaction" data-id="<?= $allactid ?>" data-action="disconect"> participant </button>
                                <?php
                                }
                                ?>
                                <button type="button" class="btn btn-sm newbtnTodo " style="font-size: 14px" data-id="<?= $answer["_id"] ?>" data-actionname="<?= @$dep["poste"] ?>" data-budgetpath="<?= $key ?>" data-form="<?= $form["id"] ?>" data-pos="<?= $icont ?>" data-name="<?= @$dep["poste"] ?>"  data-actkey="<?= $allactid ?>"> <i class="fa fa-plus-square"></i> </button>
<!--                                <button type="button" class="btn btn-sm"> <i class="fa fa-bars"></i> </button>
-->                            </div>
                            <div class="table-responsive" style="padding-top: 20px; overflow: ">
                                <table class="table align-items-center table-flush" style="overflow: overlay" >
                                    <tbody>
                                    <?php

                                            $i = 1;
                                            foreach ($actions as $actionid => $action)
                                            {
                                                ?>

                                                <tr class="<?php echo $i % 2 === 0 ? "" : "line-dark"; ?>">
                                                    <th scope="row">
                                                        <div class="media align-items-center">
                                                            <div class="input_wrapper">
                                                                <input class="tablecheckbox" value="true" data-actionname="<?= $dep["poste"] ?>" data-taskname="<?= $action["task"] ?>"  data-id="<?= $allactid ?>" data-budgetpath="<?= $mappingInput ?>" data-form="<?= $mappingForm ?>" data-pos="<?= $actionid ?>" id="checkbox<?php echo $i.$icont ?>" type="checkbox" <?php echo ((isset($action["checked"]) && $action["checked"] == "true") ? "checked" : "") ?> >
                                                                <label for="checkbox<?php echo $i.$icont ?>">
                                                                </label>
                                                            </div>
                                                            <div class="media-body">
                                                                <span class="mb-0 text-simple"><?php echo @$action["task"] ?> </span>
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <td>
                                                        <span class="text-simple price"><?php echo @$action["credits"] ?> €</span>
                                                    </td>

                                                    <td>
                                                        <div class="media">

                                                            <?php
                                                            if (isset($action["contributors"]))
                                                            {
                                                                ?>
                                                                <span class="text-simple italic">Assigné à : &nbsp;</span>
                                                                <?php
                                                                foreach ($action["contributors"] as $ckey => $cvalue)
                                                                {
                                                                    $wrkr = PHDB::findOneById(Person::COLLECTION, $ckey);
                                                                    if (isset($wrkr["profilImageUrl"]))
                                                                    {
                                                                        $pdpp = Yii::app()->createUrl('/' . $wrkr["profilImageUrl"]);
                                                                    }
                                                                    else
                                                                    {
                                                                        $pdpp = Yii::app()->getModule("co2")->assetsUrl . '/images/thumb/default_organizations.png';
                                                                    }

                                                                    ?>

                                                                    <div class="avatar-group">
                                                                        <a href="#" class="avatar avatar-sm tooltips nicoss" data-toggle="tooltip" data-placement="bottom" data-original-title="<?php echo $wrkr["username"] ?>">
                                                                            <img alt="Image placeholder" src="<?php echo $pdpp ?>" class="rounded-circle tooltips" data-toggle="tooltip" data-placement="bottom" data-original-title="<?php echo $wrkr["username"] ?>">
                                                                        </a>
                                                                    </div>
                                                                    <!--<span class="text-simple bold"> &nbsp; <?php /*echo $wrkr["username"] */?></span>-->
                                                                    <?php $i++;
                                                                }
                                                            }
                                                            ?>
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <?php
                                                        $endDateaction = "";
                                                        if (!empty($action["endDate"])) {
                                                            if (gettype($action["endDate"]) == "string") {
                                                                $endDateaction = $action["endDate"];
                                                            } elseif (gettype($action["endDate"]) == "object") {
                                                                $endDateaction = date('d-m-Y', $action["endDate"]->sec);
                                                            }
                                                        }
                                                        ?>
                                                        <span class="text-simple"> <?php if (!empty($endDateaction)){ ?> date limite : <b><?php echo ($endDateaction) ?></b>  <?php } ?> </span>
                                                    </td>

                                                    <td class="py-3">
                                                        <div class="esti-btn">
                                                            <button style="font-size: 14px;background-color: transparent;" type="button"  class="td-datetimelabel2  editbtnTodo" data-id="<?= $answer['_id'] ?>" data-uid="<?= $actionid ?>" data-key="<?= $mappingInput ?>" data-form="<?= $mappingForm ?>" data-pos="<?= $i ?>" data-name="<?= $dep["poste"] ?>" data-actkey="<?= $allactid ?>" ><i class="fa fa-pencil"></i></button><span class = "dv"></span>
                                                            <button style="font-size: 14px;background-color: transparent;" type="button" class="td-datetimelabel2  deletebtnTodo" data-id="<?= $answer['_id'] ?>'" data-uid="<?= $actionid ?>"  data-key="<?= $mappingInput ?>" data-form="<?= $mappingForm ?>" data-pos="<?= $i ?>" data-name="<?= $dep["poste"] ?>" data-actkey="<?= $allactid ?>" ><i class="fa fa-trash"></i></button>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <?php
                                            }

                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <?php
                    $icont++;
                }
            }
            ?>


        </div>

    </div>

</div>


    <div style="display: none" class="paypanel single_funding <?=empty($answer["answers"]["aapStep1"]["depense"]) ? "hidden" : "" ?>">
    <div class="funding_header padding-15">
        <h4>Payement
            <button type="button" class="btn PayementAllLine" data-id="<?= $answer["_id"] ?>" data-budgetpath="depense" data-form="aapStep1" > Payer </button>
            <button class="btn tooglesuivi" data-div=".paypanel" data-div2=".activitypanel"> Activité </button>
            <span>
                    <i class="fa fa-euro"></i>
                </span>
        </h4>
    </div>
    <div class="funding_body">
        <div class="panel-body no-padding">
            <?php
            if (!empty($answer["answers"]["aapStep1"]["depense"]))
            {

                $icont = 1;
                foreach ($answer["answers"]["aapStep1"]["depense"] as $depId => $dep)
                {
                    ?>

                    <?php
                    if (isset($dep["price"]) && is_numeric($dep["price"]))
                    {
                        $totalfi = 0;
                        $totalde = 0;
                        if (!empty($dep["financer"]))
                        {
                            foreach ($dep["financer"] as $fiid => $fi)
                            {
                                if (isset($fi["amount"]))
                                {
                                    $totalfi += (int)$fi["amount"];
                                }
                            }

                        }

                        if (!empty($dep["payement"]))
                        {
                            foreach ($dep["payement"] as $deid => $de)
                            {
                                if (isset($de["amount"]))
                                {
                                    $totalde += (int)$de["amount"];
                                }
                            }
                        }

                        $percent = round(($totalde * 100) / (int)$dep["price"]);
                        $financed = round(($totalfi * 100) / (int)$dep["price"]);
                    }
                    else
                    {
                        $totalfi = 0;
                        $percent = 0;
                        $financed = 0;
                    }

                    ?>


                    <div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding  <?php echo $icont % 2 === 0 ? "line-light" : "line-dark"; ?>">
                        <div class="col-md-5 col-sm-6" style="padding-top: 20px">
                            <h3 class="widget-title" style="font-size: 17px"><?php echo @$dep["poste"]; ?></h3>
                            <div>
                                <?php
                                if (!empty(Yii::app()->session['userId']) && ($answer["user"] == Yii::app()->session['userId'] || Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) || Form::canAdmin((string)@$parentForm["_id"])) ){
                                    echo '<button style="border-color: #6e899e; color:#6c7a89; font-size :14px" type="button" id="btnPayer' .($icont - 1).'" data-id="'.$answer["_id"].'" data-budgetpath="depense" data-form="aapStep1" data-pos="'.($icont - 1).'" data-name="'.@$dep["poste"].'" class="btn btn-xs PayementLine">Payer cette ligne</button>';
                                }
                                ?>
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-6" style="padding-top: 20px;">
                            <h3 class="widget-title" style="font-size: 17px;"><?php if (!empty($dep["price"]) && is_numeric($totalde))
                                {
                                    echo rtrim(rtrim(number_format($totalde, 3, ".", " ") , '0') , '.');
                                } ?><i class="fa fa-euro"></i> /

                                <span style="color: ">
                                <?php if (!empty($dep["price"]) && is_numeric($dep["price"]))
                                {
                                    echo rtrim(rtrim(number_format($dep["price"], 3, ".", " ") , '0') , '.');
                                } ?>

                                <i class="fa fa-euro"></i>
                                 </span>

                                <span style="color: #6c7a89">
                                (<?php if (!empty($dep["price"]) && is_numeric($totalde))
                                    {
                                        echo rtrim(rtrim(number_format($totalfi, 3, ".", " ") , '0') , '.');
                                    } ?><i class="fa fa-euro"></i> financé)
                                </span>
                            </h3>

                            <div class="">
                                <div class="">

                                    <div>

                                    </div>
                                    <div style="clear: both;"> </div>
                                    <div class="" style="max-height: 72px;">

                                        <div class="progress" style="height: 23px">
                                            <?php
                                            if ($financed > $percent){
                                                ?>

                                                <div class="progress-bar progress-bar-success2" role="progressbar" style="width:<?php echo ($financed > 100 ? 100 : $financed) ?>%">
                                                    <span style="font-size: 13px">Financé &nbsp<?php echo $financed ?> %</span>
                                                </div>

                                                <div class="progress-bar progress-bar-success" role="progressbar" style="width:<?php echo (($percent - $financed) > 100 ? 100 : ($percent - $financed)) ?>%">
                                                    <span style="font-size: 13px">Payé &nbsp<?php echo $percent ?> %</span>
                                                </div>

                                                <div class="progress-bar progress-bar-nf" role="progressbar" style="width:<?php echo (($percent - $financed) > 100 ? 100 : ($percent - $financed)) ?>%">
                                                    <span style="font-size: 13px">Non payé &nbsp<?php echo 100 - $percent - $financed ?> %</span>
                                                </div>

                                                <?php
                                            } elseif($depensed = $percent) {
                                                ?>
                                                <div class="progress-bar progress-bar-success" role="progressbar" style="width:<?php echo ($percent > 100 ? 100 : $percent) ?>%">
                                                    <span style="font-size: 13px">Financé et payé &nbsp <?php echo $financed ?> %</span>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="progress-bar progress-bar-success2" role="progressbar" style="width:<?php echo ($percent > 100 ? 100 : $percent) ?>%">
                                                    <span style="font-size: 13px">Payé &nbsp<?php echo $percent ?> %</span>
                                                </div>
                                                <div class="progress-bar progress-bar-nf" role="progressbar" style="width:<?php echo (($depensed - $percent) > 100 ? 100 : $percent) ?>%">
                                                    <span style="font-size: 13px">Non Payé &nbsp <?php echo $financed - $percent ?> %</span>
                                                </div>

                                                <?php
                                            }
                                            ?>

                                            <div class="progress-bar progress-bar-nf" role="progressbar" style="width:<?php echo abs(100 - $percent) ?>%">
                                                <span style="font-size: 13px">Non payé &nbsp<?php echo 100 - ($percent) ?> %</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="widget-controls hidden">
                                        <a href="#" class="widget-control-right"><span class="fa fa-times"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12  padding-10">
                            <?php
                            if(isset($dep["payement"])) {
                                $tds = "<div style=''  class='table-responsive pr_div " . $answer["_id"] . "paytable-" . $depId . "'> <table class='table table-bordered table-hover  directoryTable oceco-styled-table'><tbody class='oceco-blanc-table'>";
                                foreach ($dep["payement"] as $ix => $fin) {
                                    $datafin = "";
                                    $tds .= "<tr>";

                                    $tds .= "<td class='col-xs-4'>";
                                    if (!empty($fin["beneficiary"]["id"])) {
                                        $tds .= "<h5>" . $fin["beneficiary"]["name"] . "</h5>";
                                        $datafin .= ' data-payid="' . $fin["beneficiary"]["id"] . '"';
                                    } else {
                                        foreach ($fin["beneficiary"] as $fp => $fv) {
                                            $tds .= "<h5>" . $fv . " </h5>";
                                        }
                                    }
                                    //$tds .= "unkknown";
                                    $tds .= "</td>";


                                    $tds .= "<td class='col-xs-2'>";
                                    if (!empty($fin["amount"])) {
                                        $tds .= $fin["amount"] . "€";
                                        //$totalAmountFunded += (int)$fin["amount"];
                                        $datafin .= 'data-finamount="' . $fin["amount"] . '"';

                                    }
                                    $tds .= "</td>";

                                    $tds .= "<td class='col-xs-2'>";
                                    if (!empty($fin["amount"]) && isset($dep["price"])) {

                                        $percent = $fin["amount"] * 100 / (int)$dep["price"];
                                        $tds .= " <span class='pull-right label label-primary'>" . floor($percent) . "%</span>";
                                    }
                                    $tds .= "</td>";

                                    $tds .= '<td class="py-3"><div class="esti-btn">
                                                <button type="button"  class="td-datetimelabel2  fibtnedit" data-id="' . $answer['_id'] . '" data-uid="' . $ix . '" ' . $datafin . ' data-key="' . $mappingInput . '" data-form="' . $mappingForm . '" data-pos="' . $depId . '" ><i class="fa fa-pencil"></i></button><span class = "dv"></span>
                                                <button type="button" class="td-datetimelabel2  fibtndelete" data-id="' . $answer['_id'] . '" data-uid="' . $ix . '"  data-key="' . $mappingInput . '" data-form="' . $mappingForm . '" data-pos="' . $depId . '" ><i class="fa fa-trash"></i></button>
                                            </div></td>';

                                    $tds .= "</tr>";
                                }

                                //$totalFunded += $totalAmountFunded;

                                $tds .= "</tbody></table></div>";

                                echo $tds;
                            }
                            ?>

                        </div>

                        <!--<div class="col-md-7 col-sm-6 col-xs-12 valueAbout padding-10 <?php /*echo $icont % 2 === 0 ? "line-dark" : "line-light"; */?>">
                            <div class="visible-xs col-xs-12 no-padding"><?php /*echo @$dep["poste"] */?></div>
                            <div class="col-xs-12 col-sm-12 col-md-5 text-center">
                                <div class="media justify-center">
                                    <?php
                        /*                                    if (!empty(Yii::app()->session['userId']) && ($answer["user"] == Yii::app()->session['userId'] || Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) || Form::canAdmin((string)$elform["_id"])) ){
                                                                echo '<button style="background-color: #847e7e;" type="button" id="btnFinancer'.($icont - 1).'" data-id="'.$answer["_id"].'" data-budgetpath="depense" data-form="aapStep1" data-pos="'.($icont - 1).'" data-name="'.@$dep["poste"].'" class="btn btn-secondary btn-xs rebtnFinancer">Ajouter</button>';

                                                            }
                                                            */?>

                                    <span class="text-simple bold">
                                    <?php
                        /*                                    $totalname = "";
                                                            if (!empty($dep["financer"]))
                                                            {
                                                                foreach ($dep["financer"] as $fiid => $fi)
                                                                {
                                                                    if (isset($fi["name"]))
                                                                    {
                                                                        $totalname .= " " . $fi["name"] . " -";
                                                                    }
                                                                }
                                                            }
                                                            echo $totalname;
                                                            */?>
                                </span>
                                </div>
                            </div>



                        </div>
-->
                    </div>


                    <?php $icont++;
                }
            }

            ?>

        </div>
    </div>

    <!--<div class="btn-component padding-15 justify-center" >
        <button class="btn btn-default btn-financing">
            Financer
        </button>
    </div>-->
</div>



</div>

<script type="text/javascript">

    $(document).ready(function() {
        $(".tooglesuivi").click(function(){
            $($(this).data('div')).toggle();

            $($(this).data('div2')).toggle(); return false

        });

        $( '.taskpull-me' ).each(function( index ) {
            if (typeof $('.'+$(this).data('pull'))[0] != "undefined") {
                $('.'+$(this).data('pull'))[0].style.display = localStorage[$(this).data('pull')];
            }

            if (typeof $('.'+$(this).data('pull'))[0] != "undefined") {
                if (($('.'+$(this).data('pull'))[0].style.display == "flex" || $('.'+$(this).data('pull'))[0].style.display == "") && parseInt($(this).data('len')) > 0) {
                    $(this).html("Reduire la liste("+$(this).data('len')+")");
                }else if ($('.'+$(this).data('pull'))[0].style.display == "none"  && parseInt($(this).data('len')) > 0) {
                    $(this).html("Voir la liste("+$(this).data('len')+")");
                } else {
                    $(this).html("aucun resultat");
                }
            }
        });

        $('.taskpull-me').click(function() {
            var  thisdatapull = $(this).data('pull');
            var thisbtnpull = $(this);
            $('.'+$(this).data('pull')).slideToggle("fast", function(){
                var trtr = $('.'+thisdatapull)[0].style.display;

                localStorage.setItem(thisdatapull, $('.'+thisdatapull)[0].style.display);

                if (($('.'+thisbtnpull.data('pull'))[0].style.display == "flex" || $('.'+thisbtnpull.data('pull'))[0].style.display == "") && parseInt(thisbtnpull.data('len')) > 0) {
                    thisbtnpull.html("Reduire la liste("+thisbtnpull.data('len')+")");
                } else if ($('.'+thisbtnpull.data('pull'))[0].style.display == "none" && parseInt(thisbtnpull.data('len')) > 0) {
                    thisbtnpull.html("Voir la liste("+thisbtnpull.data('len')+")");
                } else {
                    thisbtnpull.html("aucun resultat");
                }

            });
        });

        $( '.payfipull-me' ).each(function( index ) {
            if (typeof $('.'+$(this).data('pull'))[0] != "undefined") {
                $('.'+$(this).data('pull'))[0].style.display = localStorage[$(this).data('pull')];
            }

            if (typeof $('.'+$(this).data('pull'))[0] != "undefined") {
                if (($('.'+$(this).data('pull'))[0].style.display == "flex" || $('.'+$(this).data('pull'))[0].style.display == "") && parseInt($(this).data('len')) > 0) {
                    $(this).html("Reduire la liste("+$(this).data('len')+")");
                }else if ($('.'+$(this).data('pull'))[0].style.display == "none"  && parseInt($(this).data('len')) > 0) {
                    $(this).html("Voir la liste("+$(this).data('len')+")");
                } else {
                    $(this).html("aucun resultat");
                }
            }
        });

        $('.payfipull-me').click(function() {
            var  thisdatapull = $(this).data('pull');
            var thisbtnpull = $(this);
            $('.'+$(this).data('pull')).slideToggle("fast", function(){
                var trtr = $('.'+thisdatapull)[0].style.display;

                localStorage.setItem(thisdatapull, $('.'+thisdatapull)[0].style.display);

                if (($('.'+thisbtnpull.data('pull'))[0].style.display == "flex" || $('.'+thisbtnpull.data('pull'))[0].style.display == "") && parseInt(thisbtnpull.data('len')) > 0) {
                    thisbtnpull.html("Reduire la liste("+thisbtnpull.data('len')+")");
                } else if ($('.'+thisbtnpull.data('pull'))[0].style.display == "none" && parseInt(thisbtnpull.data('len')) > 0) {
                    thisbtnpull.html("Voir la liste("+thisbtnpull.data('len')+")");
                } else {
                    thisbtnpull.html("aucun resultat");
                }

            });
        });

    });


</script>
