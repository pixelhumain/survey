<style type="text/css">
    .oceco-styled-table {
        border-collapse: collapse;
        margin: 25px 0;
        font-size: 0.9em;
        font-family: sans-serif;
        min-width: 400px;
        box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
    }

    .oceco-styled-table thead tr {
        background-color: #009879;
        color: #ffffff;
        text-align: left;
    }

    .oceco-styled-table th,
    .oceco-styled-table td {
        padding: 12px 15px;
    }

    .oceco-styled-table tbody tr {
        border-bottom: 1px solid #dddddd;
    }

    .oceco-styled-table tbody tr:nth-of-type(even) {
        background-color: #f3f3f3;
    }

    .oceco-styled-table tbody tr:nth-of-type(odd) {
        background-color: #e5ffe5;
    }

    .oceco-styled-table tbody tr:last-of-type {
        border-bottom: 2px solid #009879;
    }

    .oceco-styled-table tbody tr.active-row {
        font-weight: bold;
        color: #009879;
    }

    .oceco-styled-table tbody.oceco-blanc-table tr{
        background-color: #fff;
    }

    .btn-group.special {
        display: flex;
    }

    .special .btn {
        flex: 1
    }

    .budgetdropdown {

    }

    .budgetdropdown .dropdown-menu .dropdown-item{
        border-bottom: 1px solid rgba(0, 0, 0, 0.1);
        padding: 15px 15px;
        font-size: 14px;
        color: #8a9299;
    }

    .budgetdropdown .dropdown-menu .dropdown-item i{
        margin-right: 15px;
        display: inline-block;
    }

    .budgetdropdown .dropdown-menu .dropdown-item:hover{
        border-bottom: 1px solid rgba(0, 0, 0, 0.1);
        padding: 15px 15px;
        font-size: 14px;
        color: #000;
    }
</style>

<?php
HtmlHelper::registerCssAndScriptsFiles(["/css/form/budget.css"], Yii::app()->getModule('survey')->assetsUrl);
if ($answer)
{
    $isAapProject = $isAap;
    $contextId = $ctxtid = $context["_id"];
    $contextType = $ctxttype = $parentForm["parent"][(string)$context["_id"]]["type"];

    $debug = false;
    $editBtnL = ($canEdit == true && ($mode == "w" || $mode == "fa")) ? " <a href='javascript:;' data-id='" . $answer["_id"] . "' data-collection='" . Form::ANSWER_COLLECTION . "' data-path='" . $answerPath . "' class='add" . $kunik . "  dropdown-item'><i class='fa fa-plus'></i> Ajouter une ligne </a> <a id='addfromcsv' data-id='" . $answer["_id"] . "' data-collection='" . Form::ANSWER_COLLECTION . "' data-path='" . $answerPath . "' class='  dropdown-item'><i class='fa fa-plus'></i> Ajouter ligne via csv </a>" : "";

    $editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='" . $parentForm["_id"] . "' data-collection='" . Form::COLLECTION . "' data-path='params." . $kunik . "' class='previewTpl edit" . $kunik . "Params  dropdown-item'><i class='fa fa-cog'></i> </a>".$editQuestionBtn : "";


    $paramsData = [
        "group" => ["Feature", "Costum", "Chef de Projet", "Data", "Mantenance"],
        "nature" => ["investissement", "fonctionnement"],
        "amounts" => ["price" => "Montant"],
        "estimate" => false,
        "activeGroup" => false,
        "activeNature" => false,
        "fielLabel" => "Ligne de dépense",
        "prefilledDepense" => array(),
    ];

    if (isset($parentForm["params"][$kunik]["group"])) $paramsData["group"] = $parentForm["params"][$kunik]["group"];
    if (isset($parentForm["params"][$kunik]["nature"])) $paramsData["nature"] = $parentForm["params"][$kunik]["nature"];
    if (isset($parentForm["params"][$kunik]["amounts"])) $paramsData["amounts"] = $parentForm["params"][$kunik]["amounts"];
    if (isset($parentForm["params"][$kunik]["estimate"])) $paramsData["estimate"] = Answer::is_true($parentForm["params"][$kunik]["estimate"]);
    if (isset($parentForm["params"][$kunik]["activeGroup"])) $paramsData["activeGroup"] = Answer::is_true($parentForm["params"][$kunik]["activeGroup"]);
    if (isset($parentForm["params"][$kunik]["activeNature"])) $paramsData["activeNature"] = Answer::is_true($parentForm["params"][$kunik]["activeNature"]);
    if (isset($input["label"])) $paramsData["fielLabel"] = $input["label"];
    if (isset($parentForm["params"][$kunik]["prefilledDepense"])) $paramsData["prefilledDepense"] = $parentForm["params"][$kunik]["prefilledDepense"];


    $properties = [
            "group" => [
                "placeholder" => "Groupé",
                "inputType" => "tags",
                "tagsList" => "budgetgroupe",
                "rules" => ["required" => true],
                "maximumSelectionLength" => 3
            ],
            "nature" => [
                    "placeholder" => "Nature de l’action",
                "inputType" => "tags",
                "tagsList" => "budgetnature",
                "rules" => ["required" => true],
                "maximumSelectionLength" => 3
            ],
            "poste" => [
                    "inputType" => "text",
                "label" => "Poste de dépense",
                "placeholder" => "Poste de dépense",
                "rules" => ["required" => true]
            ],
            "description" => [
                "inputType" => "textarea",
                "label" => "Description",
                "placeholder" => "Description",
                "rules" => ["required" => false]
            ],
            "image" => [
                "label" => "image",
                "placeholder" => "",
                "inputType" => "uploader",
                "docType" => "image",
                "filetypes" => ["jpeg", "jpg", "gif", "png"]
            ]
    ];
    if(!$paramsData["activeGroup"]) unset($properties["group"]);
    if(!$paramsData["activeNature"]) unset($properties["nature"]);
    foreach ($paramsData["amounts"] as $k => $l)
    {
        $properties[$k] = ["inputType" => "number", "label" => $l, "propType" => "amount", "placeholder" => $l,
            //"rules" => [ "required" => true, "number" => true ]
        ];
    }
    if ($debug) var_dump($answers);
    if ($debug) var_dump($paramsData);

    ?>

    <?php
    $ignore = array('_file_', '_params_', '_obInitialLevel_' ,'ignore');
    $params = array_diff_key(get_defined_vars() + array_flip($ignore), array_flip($ignore));

    echo $this->renderPartial("survey.views.tpls.forms.ocecoform.budgetTable", $params , true);
    ?>

    <div class="form-group col-xs-12 addfromcsvdiv" style="display: none" >
        <div>
            <label class="info text-center text-dark-blue"> <h4> Importer des lignes de depense depuis un fichier csv </h4></label>
        </div>
        <!--<label for="inputcsvdata" > <input type="file" id="inputcsvdata" class="inputcsvdata" name="inputcsvdata" accept=".csv"> </label>-->
        <div id="filed-drop-area" style="border: 2px dashed #ccc;border-radius: 20px;width: 100%;padding: 20px;text-align: center;margin: 10px auto;">
						<label for="inputcsvdata" style="cursor: pointer;color: #00b1ca;font-weight: bold;">
						<b>Déposer un fichiers </b> ou 
							<input type="file"  id="inputcsvdata" class="inputcsvdata btn btn-default" name="inputcsvdata" accept=".csv">
						</label>
					</div>
        <div>
            <div>
                <label class="info text-blue"> Le delimiteur utilisé est le point-virgule<code>;</code></label>
            </div>
            <div>
                <label class="info text-blue"> Le delimiteur de fin de ligne utilisé est le retour à la ligne <code>/n</code></label>
            </div>
            <div>
                <label class="info text-blue"> Exemple </label>
                <div>
                    <code>
                        Data,Feature;fonctionnement;value1;200
                        Data;fonctionnement,improuvement;value2;350
                    </code>
                </div>
            </div>
        </div>

        <div class="addfromcsvdivrs">  </div>
    </div>

    <?php if ($paramsData["estimate"])
{ ?>
    <div id="" class="newform-estimate" style="display: none">
        <header>
            <h1>Proposer</h1>
        </header>
        <main class="mainc">
            <div class="ocecowebview">
                <label class="label-esti">Proposition de prix</label>
                <label class="label-esti">Durée</label>
            </div>
            <div class="">
                <label class="label-mobi">Proposition de prix</label>
                <input id="" class="todoinput e-price" type="number" spellcheck="false" placeholder="Proposition de prix" onfocus="this.placeholder=''" onblur="this.placeholder='Fonds, enveloppe ou budget mobilisé'" />
                <label class="label-mobi">Durée</label>
                <input id="new-credit-value" class="e-days todoinput" type="number" spellcheck="false" placeholder="Durée" onfocus="this.placeholder=''" onblur="this.placeholder='Durée'" />
                <label class="label" style="font-size: initial;margin: 1em 1% 0em 3%;color:#000;padding-left:0">Assigné à</label>
                <input id="new-credit-value" class="e-who todoinput search-person" type="text" spellcheck="false" placeholder="Tapez le nom" onfocus="this.placeholder=''" onblur="this.placeholder='Durée'" />
                <div style="clear: both;"></div>
            </div>

            <div id="single-line"></div>

            <ul id="estimate-list">
            </ul>
        </main>
    </div>
    <?php
}

    if ($mode != "r" && $mode != "pdf")
    {
        ?>

        <script type="text/javascript">

            var <?php echo $kunik ?>Data = <?php echo json_encode((isset($answers)) ? $answers : null); ?>;
            sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;

            var FiData = <?php echo json_encode((isset($answer["answers"]["aapStep1"][$key])) ? $answer["answers"]["aapStep1"][$key] : []); ?>;

            var projectId = "<?php echo (!empty($answer["project"]["id"]) ? $answer["project"]["id"] : "null"); ?>";

            var parentId = "<?php echo (string)$answer["form"]; ?>";

            var statusproposition = <?php echo ((isset($answers["status"]) && is_array($answers["status"])) ? json_encode($answers["status"]) : "[]"); ?>;

            var answerId = "<?php echo (string)$answer["_id"]; ?>";
            var cntxtId = "<?php echo $contextId; ?>";
            var cntxtType = "<?php echo $contextType; ?>";
            var parentForm<?= $kunik ?> = <?= json_encode($parentForm) ?>;
            var budgetObj = {file:null};
            if (FiData != null) {
                if ($.isPlainObject(FiData)){
                    FiData = Object.values(FiData);
                }
                if (Array.isArray(FiData)){
                    var estimateList = FiData.reduce(function(accumulator, item){
                        if (item != null && typeof item["estimates"] != "undefined") {
                            accumulator.push(item["estimates"]);
                            return accumulator;
                        } else {
                            accumulator.push([]);
                            return accumulator;
                        }
                    },[]);
                }
            } else {
                FiData = [];
                var estimateList = {};
            }

            var budgetgroupe = <?php echo (!empty($paramsData["group"]) ? json_encode($paramsData["group"]) : "[]"); ?>;
            var budgetnature = <?php echo (!empty($paramsData["nature"]) ? json_encode($paramsData["nature"]) : "[]"); ?>;

            if(typeof costum != "undefined" && costum != null){
                costum["budgetgroupe"] = budgetgroupe;
                costum["budgetnature"] = budgetnature;
            }

            $(document).ready(function() {
                function getTotalDepense(){
                    var globalBudget = (notEmpty(parentForm<?= $kunik ?>?.params?.globalBudget?.max) ? parentForm<?= $kunik ?>?.params?.globalBudget?.max : null);
                    if(globalBudget != 0 && notNull(globalBudget)){
                        params = {
                            searchType : ["answers"],
                            filters:{
                                'form' : parentForm<?= $kunik ?>._id.$id ,
                                "answers.aapStep1.titre" : {'$exists' : true},
                                "answers.aapStep1.depense":{'$exists': true }
                            },
                            notSourceKey : true,
                            fields:["answers.aapStep1.depense"]
                        };
                        ajaxPost(
                            null,
                            baseUrl+"/" + moduleId + "/search/globalautocomplete",
                            params,
                            function(data){
                                var cumule = 0;
                                $.each(data.results,function(k,v){
                                    mylog.log(v,"datako")
                                    $.each(v.answers?.aapStep1?.depense,function(kk,vv){
                                        if(notEmpty(vv) && notEmpty(vv.price) && !isNaN(vv.price)){
                                            cumule += parseInt(vv.price);
                                        }
                                    })
                                })
                                mylog.log(cumule,"cumule",globalBudget)
                                if(globalBudget > cumule){
                                    $('.funding_body .alert-content-information').removeClass("hidden").addClass("alert-success").html(`
                                        Vous avez dépensé <strong>${cumule} € (${(cumule*100)/globalBudget} %)</strong> de votre budget globale <strong>(${globalBudget} €)</strong>,<br />
                                        Vous pouvez ajouter <strong>${globalBudget-cumule} € </strong>.
                                    `)
                                }else if(globalBudget == cumule){
                                    $('.funding_body .alert-content-information').removeClass("hidden").addClass("alert-warning").html(`
                                        Vous avez égalé le montant global du budget (<strong>${globalBudget} € </strong>).
                                    `)
                                }else{
                                    $('.funding_body .alert-content-information').removeClass("hidden").addClass("alert-danger").html(`
                                        Vous avez dépassé le montant global du budget!. <br />
                                        Montant global du budget : <strong>${globalBudget} € </strong>,<br />
                                        Montant dépensé : <strong>${cumule} € </strong>,<br />
                                        Différence : <strong>${cumule - globalBudget} € </strong>
                                    `)
                                }
                            },null,null,{/*async:false*/}
                        )
                    }
                }
                //getTotalDepense();

                $('.openAnswersComment').on('click', function () {
                    setTimeout(
                        function() {
                            $('.footer-comments').on('DOMSubtreeModified', function () {
                                reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>" );
                                if(typeof newReloadStepValidationInputGlobal != "undefined") {
                                    newReloadStepValidationInputGlobal({
                                        inputKey : <?php echo json_encode($key); ?>,
                                        inputType : "tpls.forms.ocecoform.budgetTwoColumn"
                                    })
                                }
                            })
                        }
                        , 2000
                    );
                });

                $('.deleteLineBudget').off().click( function(){
                    formId = $(this).data("id");
                    key = $(this).data("key");
                    pathLine = $(this).data("path");
                    collection = $(this).data("collection");
                    if (typeof $(this).data("parentid") != "undefined") {
                        var parentfId = $(this).data("parentid");
                    }
                    bootbox.dialog({
                        title: trad.confirmdelete,
                        message: "<span class='text-red bold'><i class='fa fa-warning'></i> "+trad.actionirreversible+"</span>",
                        buttons: [
                            {
                                label: "Ok",
                                className: "btn btn-primary pull-left",
                                callback: function() {
                                    var formQ = {
                                        value:null,
                                        collection : collection,
                                        id : formId,
                                        path : pathLine,
                                        pull : "<?=$answerPath?>".slice(0, -1)
                                    };

                                    if (typeof parentfId != "undefined") {
                                        formQ["formParentId"] = parentfId;
                                    }

                                    dataHelper.path2Value( formQ , function(params) {
                                        if (typeof inputsList != "undefined"){
                                            $.each(inputsList , function (inn, vall) {
                                                $.each(vall , function (inn2, vall2) {
                                                    if (typeof vall2.type != "undefined"
                                                        && vall2.type.split('.')[vall2.type.split('.').length-1] != "undefined"
                                                        && ( vall2.type.split('.')[vall2.type.split('.').length-1] == "budget" ||
                                                            vall2.type.split('.')[vall2.type.split('.').length-1] == "financementFromBudget" ||
                                                            vall2.type.split('.')[vall2.type.split('.').length-1] == "suiviFromBudget"
                                                        )
                                                    ){
                                                        reloadInput(inn2, inn , null , true);
                                                    }
                                                });
                                            });
                                        }
                                        if(typeof newReloadStepValidationInputGlobal != "undefined") {
                                            newReloadStepValidationInputGlobal({
                                                inputKey : key,
                                                inputType : "tpls.forms.ocecoform.budget"
                                            })
                                        }
                                        //location.reload();
                                    } );
                                }
                            },
                            {
                                label: "Annuler",
                                className: "btn btn-default pull-left",
                                callback: function() {}
                            }
                        ]
                    });
                });

                ajaxPost(
                    null,
                    baseUrl+'/survey/answer/countvisit/answerId/'+answerId+'/',
                    null,
                    function(data){
                        if (data && data.new == true){
                            dataHelper.path2Value( data.upvalue, function(){
                                //$('#countv').html(data.upvalue.value.count);
                                //propostionObj.refreshNewCounter(answerId);
                            });
                        }
                    },
                    "json"
                );

                sectionDyf.<?php echo $kunik ?> = {
                    jsonSchema : {
                        title : "Budget prévisionnel",
                        icon : "fa-money",
                        text : "Décrire ici les principaux postes de dépenses : à quoi correspondent les coûts ? <br/>Préciser ce qui relève des dépenses de fonctionnement et des dépenses d’investissement.<br/>Les coûts doivent être en <b>hors taxe</b>.",
                        properties : <?php echo json_encode($properties); ?>,
                        onLoads :{
                            onload : function(){

                            }
                        },
                        afterBuild : function () {
                            setTimeout(function () {
                                //uploadObj.set("answers",answerObj._id.$id);
                                $('.qq-upload-button-selector.btn.btn-primary').css({
                                    position : 'absolute',
                                    left : '50%',
                                    transform : 'translateX(-50%)'
                                });
                            }, 200);
                        },
                        save : function (data) {
                            dyFObj.commonAfterSave(data,function(){
                                var depensename = "";

                                var today = new Date();
                                tplCtx.setType = [
                                    {
                                        "path": "price",
                                        "type": "int"
                                    },
                                    {
                                        "path": "date",
                                        "type": "isoDate"
                                    }
                                ];

                                tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };

                                $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) {
                                    tplCtx.value[k] = $("#"+k).val();

                                    if(k == "poste")
                                        depensename = tplCtx.value[k];
                                    if(k == "description")
                                        tplCtx.value[k] = data.description;

                                });

                                if(notEmpty(tplCtx.keygen))
                                    tplCtx.value["image"] = tplCtx.keygen;

                                if (typeof tplCtx.value.group != "undefined") {
                                    tplCtxgroup = {
                                        id: parentId,
                                        collection: "forms",
                                        path: "params.<?php echo $kunik ?>.group"
                                    }
                                    tplCtxgroup.value = sectionDyf.<?php echo $kunik ?>ParamsData.group;
                                    $.each(tplCtx.value.group.split(",") , function (ind , val) {
                                        if(jQuery.inArray(val, tplCtxgroup.value) == -1) {
                                            tplCtxgroup.value.push(val);
                                        }
                                    });

                                    dataHelper.path2Value(tplCtxgroup, function (params) {});
                                }
                                if (typeof tplCtx.value.nature != "undefined") {
                                    tplCtxnature = {
                                        id: parentId,
                                        collection: "forms",
                                        path: "params.<?php echo $kunik ?>.nature"
                                    }
                                    tplCtxnature.value = sectionDyf.<?php echo $kunik ?>ParamsData.nature;
                                    $.each(tplCtx.value.nature.split(",") , function (ind , val) {
                                        if(jQuery.inArray(val, tplCtxnature.value) == -1) {
                                            tplCtxnature.value.push(val);
                                        }
                                    });
                                    dataHelper.path2Value(tplCtxnature, function (params) {});
                                }

                                var connectedData = ["financer","todo","payed","progress","worker","validFinal","votes"];
                                $.each( connectedData , function(k,attr) {
                                    if(notNull("answerObj."+tplCtx.path+"."+attr))
                                        tplCtx.value[attr] = jsonHelper.getValueByPath(answerObj,tplCtx.path+"."+attr);
                                });

                                mylog.log("save tplCtx",tplCtx);
                                if(typeof tplCtx.value == "undefined"){}
                                else {
                                    dataHelper.path2Value( tplCtx, function(params) {
                                        delete tplCtx.keygen;
                                        dyFObj.closeForm();
                                        if(typeof newReloadStepValidationInputGlobal != "undefined") {
                                            newReloadStepValidationInputGlobal({
                                                inputKey : "nature",
                                                inputType : "tpls.forms.ocecoform.budget"
                                            })
                                        }
                                        var answerId = "<?php echo (string)$answer["_id"]; ?>";
                                        ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/newdepense/answerid/' + answerId,
                                            {
                                                name : depensename,
                                                url : window.location.href
                                            },
                                            function (data) {

                                            }, "html");

                                        ajaxPost("", baseUrl + "/co2/aap/commonaap/action/notifyAddDepenseLine", {
                                                answerId:answerId,
                                                depense: tplCtx.value.poste +": "+  tplCtx.value.price + " Euro"
                                        }, function(data){}, function(error){}, "json")

                                        var tplCtxproject = {};

                                        if (projectId != null && projectId != "null") {

                                            if (notNull(tplCtx.actionid)) {
                                                tplCtxproject.id = tplCtx.actionid;
                                                tplCtxproject.collection = "actions";
                                                tplCtxproject.path = "name"
                                                tplCtxproject.value = tplCtx.value.poste;
                                                if(exists(tplCtxproject.value))
                                                    dataHelper.path2Value(tplCtxproject, function () {});
                                            }

                                            var answerId = "<?php echo (string)$answer["_id"]; ?>";
                                            var cntxtId = "<?php echo $ctxtid; ?>";
                                            var cntxtType = "<?php echo $ctxttype; ?>";

                                            ajaxPost("", baseUrl + '/survey/form/generateproject/answerId/' + answerId + '/parentId/' + cntxtId + '/parentType/' + cntxtType,
                                                null,
                                                function (data) {
                                                });
                                        }
                                    }, function(p) {

                                    });

                                    delete tplCtx.setType;

                                    delete tplCtx.setType;

                                    // if ($.inArray("newaction", statusproposition) == -1){
                                    //     statusproposition.push("newaction");
                                    //     tplCtx.value = statusproposition;
                                    // }else{
                                    //     tplCtx.value = statusproposition;
                                    // }

                                    // tplCtx.path = "status";

                                    if (typeof inputsList != "undefined"){
                                        $.each(inputsList , function (inn, vall) {
                                            $.each(vall , function (inn2, vall2) {
                                                if (typeof vall2.type != "undefined"
                                                    && vall2.type.split('.')[vall2.type.split('.').length-1] != "undefined"
                                                    && ( vall2.type.split('.')[vall2.type.split('.').length-1] == "budget" ||
                                                        vall2.type.split('.')[vall2.type.split('.').length-1] == "financementFromBudget" ||
                                                        vall2.type.split('.')[vall2.type.split('.').length-1] == "suiviFromBudget"
                                                    )
                                                ){
                                                    setTimeout(() => {
                                                        reloadInput(inn2, inn);
                                                        $('html, body').animate({scrollTop: $("#question<?= $key ?>").offset().top-$("#mainNav").height()},'slow');
                                                    }, 500);
                                                }
                                            });
                                        });
                                    };

                                }
                            })
                        },
                        beforeBuild : function(data){
                            if(typeof costum != "undefined" && costum != null){
                                costum["budgetgroupe"] = budgetgroupe;
                                costum["budgetnature"] = budgetnature;
                            }
                            uploadObj.set("answers",answerObj._id.$id);

                            const chars = 'azertyuiopqsdfghjklmwxcvbn123456789';
                            var rand = (min = 0 , max = 1000) => Math.floor(Math.random() * (max - min) + min);
                                randChar = (length = 6) => {
                                    var randchars = [];
                                    for(let i = 0; i < length; i++){
                                        randchars.push(chars[rand(0, chars.length)]);
                                    }

                                    return randchars.join('');
                                }
                            keygenerator =  (prefix = 'a-', sufix='') => `${prefix}${randChar()}${sufix}`;
                            var keygen = keygenerator();
                            uploadObj.path += "/subKey/answers.aapStep1.depense.image/imageKey/"+keygen;
                            tplCtx.keygen = keygen;
                        },
                    }
                };

                sectionDyf.<?php echo $kunik ?>Params = {
                    jsonSchema : {
                        title : "<?php echo $label ?> config",
                        description : "Liste de question possible",
                        icon : "cog",
                        properties : {
                            fielLabel : {
                                inputType : "text",
                                label : "Label du champ",
                                values :  sectionDyf.<?php echo $kunik ?>ParamsData.fielLabel
                            },
                            group : {
                                inputType : "array",
                                label : "Liste des groups",
                                values :  sectionDyf.<?php echo $kunik ?>ParamsData.group
                            },
                            activeGroup : {
                                inputType : "checkboxSimple",
                                label : "Activer le groupe",
                                params : {
                                    onText : "Oui",offText : "Non",onLabel : "Oui",offLabel : "Non",labelText : "Activer les groupes"
                                },
                                checked : sectionDyf.<?php echo $kunik ?>ParamsData.activeGroup
                            },
                            nature : {
                                inputType : "array",
                                labelKey : "Clef",
                                label : "Liste des natures possibles",
                                values :  sectionDyf.<?php echo $kunik ?>ParamsData.nature
                            },
                            activeNature : {
                                inputType : "checkboxSimple",
                                label : "Activer la nature",
                                params : {
                                    onText : "Oui",offText : "Non",onLabel : "Oui",offLabel : "Non",labelText : "Activer les groupes"
                                },
                                checked : sectionDyf.<?php echo $kunik ?>ParamsData.activeNature
                            },
                            amounts : {
                                inputType : "properties",
                                info : "veuillez garder la clef du coût à << price >> pour etre pris en compte dans les futurs calcul",
                                labelKey : "Clef",
                                labelValue : "Label affiché",
                                label : "Liste des prix(ex:par année)",
                                values :  sectionDyf.<?php echo $kunik ?>ParamsData.amounts
                            },
                            estimate : {
                                inputType : "checkboxSimple",
                                label : "estimate Prices",
                                params : {
                                    onText : "Oui",
                                    offText : "Non",
                                    onLabel : "Oui",
                                    offLabel : "Non",
                                    labelText : "estimate Prices"
                                },
                                checked : sectionDyf.<?php echo $kunik ?>ParamsData.estimate
                            },
                            prefilledDepense : {
                                inputType : "array",
                                label : "Dépenses pré-remplies",
                                values :  sectionDyf.<?php echo $kunik ?>ParamsData.prefilledDepense
                            }
                        },
                        onLoads:{
                            onload : function(){
                                alignInput2(["prefilledDepensearray"], "dep", 6, 6, null, null,"Dépenses pré-remplies", "#3f4e58", "");
                                alignInput2(["activeGroupcheckboxSimple","grouparray"], "group", 6, 6, null, null,"Groupe", "#3f4e58", "");
                                alignInput2(["activeNaturecheckboxSimple","naturearray"], "nature", 6, 6, null, null,"Nature", "#3f4e58", "");
                            }

                        },
                        save : function () {
                            tplCtx.value = {};
                            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                                if(k!= "fielLabel"){
                                    if(val.inputType == "properties")
                                        tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                                    else if(val.inputType == "array")
                                        tplCtx.value[k] = getArray('.'+k+val.inputType);
                                    else
                                        tplCtx.value[k] = $("#"+k).val();
                                }
                                mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                            });
                            mylog.log("save tplCtx",tplCtx);

                            if(typeof tplCtx.value == "undefined")
                                toastr.error('value cannot be empty!');
                            else {
                                dataHelper.path2Value( tplCtx, function(params) {
                                    tplCtx.value = $("#fielLabel").val();
                                    tplCtx.id = "<?= @$input["docinputsid"] ?>";
                                    tplCtx.collection = "<?= Form::INPUTS_COLLECTION ?>";
                                    tplCtx.path = "inputs.<?= $key ?>.label";
                                    dataHelper.path2Value(tplCtx, function(params) {
                                        dyFObj.closeForm();
                                        urlCtrl.loadByHash(location.hash );
                                        //reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                    })
                                } );
                            }

                        }
                    }
                };

                var max<?php echo $kunik ?> = 0;
                var nextValuekey<?php echo $kunik ?> = 0;

                if(notNull(<?php echo $kunik ?>Data)){
                    $.each(<?php echo $kunik ?>Data, function(ind, val){
                        if(parseInt(ind) > max<?php echo $kunik ?>){
                            max<?php echo $kunik ?> = parseInt(ind);
                        }
                    });

                    nextValuekey<?php echo $kunik ?> = max<?php echo $kunik ?> + 1;

                }

                mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

                //adds a line into answer
                $( ".add<?php echo $kunik ?>" ).off().on("click",function() {
                    tplCtx.id = $(this).data("id");
                    tplCtx.collection = $(this).data("collection");
                    /*tplCtx.path = $(this).data("path")+ nextValuekey<?php echo $kunik ?>;*/
                    tplCtx.path = $(this).data("path").slice(0, -1);
                    tplCtx.arrayForm = true;
                    dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null,null,null,null,{
                        type : "bootbox",
                        notCloseOpenModal : true,
                    } );
                });

                $(".edit<?php echo $kunik ?>").off().on("click",function() {
                    tplCtx.id = $(this).data("id");
                    tplCtx.collection = $(this).data("collection");
                    tplCtx.path = $(this).data("path");
                    tplCtx.actionid = $(this).data("actionid");
                    delete tplCtx.arrayForm;
                    dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")],null,null,{
                        type : "bootbox",
                        notCloseOpenModal : true,
                    } );
                });

                $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                    tplCtx.id = $(this).data("id");
                    tplCtx.collection = $(this).data("collection");
                    tplCtx.path = $(this).data("path");
                    dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
                });

                $('.newbtnestimate[data-kunik="<?= $kunik ?>"]').off().click(function() {
                    tplCtx.pos = $(this).data("pos");
                    tplCtx.collection = "answers";
                    tplCtx.id = $(this).data("id");
                    tplCtx.key = $(this).data("key");
                    tplCtx.form = $(this).data("form");
                    var currentValue = [];
                    tplCtx.value = {};
                    prioModal = bootbox.dialog({
                        message: $(".newform-estimate").html(),
                        show: false,
                        size: "large",
                        className: 'estimatedialog',
                        buttons: {
                            success: {
                                label: trad.save,
                                className: "btn-primary",
                                callback: function () {
                                    tplCtx.path = "answers";
                                    // if( notNull(formInputs [tplCtx.form]) )
                                    tplCtx.path = "answers."+tplCtx.form;

                                    //tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+currentUserId;
                                    tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".estimates";

                                    tplCtx.setType = [
                                        {
                                            "path": "price",
                                            "type": "int"
                                        },
                                        {
                                            "path": "date",
                                            "type": "isoDate"
                                        }
                                    ];
                                    tplCtx.updatePartial = true;

                                    var today = new Date();
                                    today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();

                                    mylog.log("btnEstimate save",tplCtx);
                                    dataHelper.path2Value( tplCtx, function(){
                                        //saveLinks(answerObj._id.$id,"estimated",userId);
                                        if(currentValue.length != 0){
                                            var params = {
                                                parentId : answerId,
                                                parentType : "answers",
                                                listInvite : {
                                                    citoyens :{},
                                                    organizations:{}
                                                }
                                            }
                                            $.each(currentValue,function(k,v){
                                                if(exists(v.id) && v.text){
                                                    params.listInvite.citoyens[v.id] = v.text
                                                }
                                            })
                                            ajaxPost("",baseUrl+'/'+moduleId+"/link/multiconnect",params,function(data){
                                                if(typeof newReloadStepValidationInputGlobal != "undefined") {
                                                    newReloadStepValidationInputGlobal({
                                                        inputKey : tplCtx.key,
                                                        inputType : "tpls.forms.ocecoform.budget"
                                                    })
                                                }
                                                ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/addperson/answerid/' + answerId,
                                                    {
                                                        pers : params.listInvite.citoyens,
                                                        url : window.location.href
                                                    },
                                                    function (data) {

                                                    }, "html");
                                            })
                                        }
                                        prioModal.modal('hide');
                                        //reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                        if (typeof inputsList != "undefined"){
                                            $.each(inputsList , function (inn, vall) {
                                                $.each(vall , function (inn2, vall2) {
                                                    if (typeof vall2.type != "undefined"
                                                        && vall2.type.split('.')[vall2.type.split('.').length-1] != "undefined"
                                                        && ( vall2.type.split('.')[vall2.type.split('.').length-1] == "budget" ||
                                                            vall2.type.split('.')[vall2.type.split('.').length-1] == "financementFromBudget" ||
                                                            vall2.type.split('.')[vall2.type.split('.').length-1] == "suiviFromBudget"
                                                        )
                                                    ){
                                                        reloadInput(inn2, inn);
                                                    }
                                                });
                                            });
                                        }
                                    } );
                                }
                            },
                            cancel: {
                                label: trad.cancel,
                                className: "btn-secondary",
                                callback: function(){
                                    prioModal.modal('hide');
                                }
                            }
                        },
                        onEscape: function(){
                            prioModal.modal('hide');
                        }
                    });

                    var selectedErow = $(this).data("pos");

                    prioModal.on('shown.bs.modal', function (e) {
                        $(".bootbox .e-days,.bootbox .e-price").on('blur',function(){
                            $(".search-person").trigger('change');
                        })
                        setTimeout(() => {
                            $(".search-person").trigger('change');
                        }, 700);
                        $('.estimatedialog #add-button').data("pos", selectedErow );
                        $(".search-person").select2({
                            placeholder: "Assigner à quelqu'un",
                            minimumInputLength: 1,
                            //maximumSelectionSize:1,
                            //tags: true,
                            multiple:true,
                            "tokenSeparators": [','],
                            createSearchChoice: function (term, data) {
                                if (!data.length)
                                    return {
                                        id: term,
                                        text: term
                                    };
                            },
                            /*initSelection : function (element, callback) {
                                var data = [];
                                $(element.val().split(",")).each(function () {
                                    //data.push({id: this, text: this});_inviteObj
                                    data.push({id: this, text: exists(_inviteObj.contributors[this].name) ? _inviteObj.contributors[this].name :""});
                                });
                                callback(data);
                            },*/
                            ajax: {
                                url: baseUrl+"/"+moduleId+"/search/globalautocomplete",
                                dataType: 'json',
                                type: "POST",
                                quietMillis: 50,
                                data: function (term) {
                                    return {
                                        name: term,
                                        searchType : ["citoyens"],
                                        filters : {
                                            '$or' : {
                                                "links.projects.<?= array_keys($parentForm["parent"])[0] ?>":{'$exists':true},
                                                "links.memberOf.<?= array_keys($parentForm["parent"])[0] ?>":{'$exists':true}
                                            }
                                        }
                                    };
                                },
                                results: function (data) {
                                    return {
                                        results: $.map(Object.values(data.results), function (item) {
                                            return {
                                                text: item.name,
                                                id: item._id.$id
                                            }
                                        })
                                    };
                                }
                            }
                        }).on('change',function(e){
                            if(exists(e.added)){
                                currentValue.push(e.added);
                            }
                            if(exists(e.removed)){
                                currentValue = currentValue.filter(data => data.id != e.removed.id);
                            }
                            if(exists(currentValue) && currentValue.length != 0){
                                var today = new Date();
                                today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();
                                $.each(currentValue,function(k,v){
                                    tplCtx.value[v.id] = {
                                        price : (parseInt($(".bootbox .e-price").val())/currentValue.length),
                                        days : $(".bootbox .e-days").val(),
                                        name :  v.text,
                                        date : today,
                                        addedBy:userId,
                                    }
                                })
                            }
                        });
                    });

                    prioModal.modal("show");
                });

                $('#addfromcsv').click(function() {

                    prioModal = bootbox.dialog({
                        message: $(".addfromcsvdiv").html(),
                        show: false,
                        size: "large",
                        className: 'csvdialog',
                        buttons: {
                            success: {
                                label: trad.save,
                                className: "btn-primary",
                                callback: function () {
                                    var ext = $('.csvdialog .inputcsvdata').val().split(".").pop();

                                    if($.inArray(ext, ["csv"]) == -1) {
                                        toastr.error(tradDynForm.youMustUseACSVFormat);
                                        return false;
                                    }

                                    if ($('.csvdialog .inputcsvdata').prop('files')[0] != undefined) {

                                        var reader = new FileReader();

                                        reader.onload = function(e) {
                                            var csvval = e.target.result.split("\n");

                                            if(FiData == null){
                                                FiData = [];
                                            }
                                            var lastinput = FiData.length;
                                            var importgroup = [];
                                            var importnature = [];
                                            $.each(csvval, function(ind, val){
                                                tplCtx.id = answerId;
                                                tplCtx.collection = "answers";
                                                tplCtx.path = "answers.aapStep1.depense."+lastinput;
                                                tplCtx.setType = [
                                                    {
                                                        "path": "price",
                                                        "type": "int"
                                                    }
                                                ];
                                                var today = new Date();
                                                tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };

                                                var thh = val.split(";");
                                                var prpr = 0;
                                                $.each(thh , function(thhid, thhhval){
                                                    if (typeof Object.keys(sectionDyf.<?php echo $kunik ?>.jsonSchema.properties)[prpr] != "undefined") {
                                                        tplCtx.value[Object.keys(sectionDyf.<?php echo $kunik ?>.jsonSchema.properties)[prpr]] = thhhval;
                                                    }
                                                    prpr++;
                                                });

                                                if (typeof tplCtx.value.group != "undefined") {
                                                    tplCtxgroup = {
                                                        id: parentId,
                                                        collection: "forms",
                                                        path: "params.<?php echo $kunik ?>.group"
                                                    }

                                                    tplCtxgroup.value = sectionDyf.<?php echo $kunik ?>ParamsData.group;

                                                    $.each(tplCtx.value.group.split(",") , function (ind , val) {
                                                        if(jQuery.inArray(val, tplCtxgroup.value) == -1 && jQuery.inArray(val, importgroup) == -1) {

                                                            tplCtxgroup.value.push(val);
                                                            importgroup.push(val);
                                                        }
                                                    });

                                                    dataHelper.path2Value(tplCtxgroup, function (params) {
                                                    });
                                                }

                                                if (typeof tplCtx.value.nature != "undefined") {
                                                    tplCtxnature = {
                                                        id: parentId,
                                                        collection: "forms",
                                                        path: "params.<?php echo $kunik ?>.nature"
                                                    }

                                                    tplCtxnature.value = sectionDyf.<?php echo $kunik ?>ParamsData.nature;

                                                    $.each(tplCtx.value.nature.split(",") , function (ind , val) {
                                                        if(jQuery.inArray(val, tplCtxnature.value) == -1 && jQuery.inArray(val, importnature) == -1) {

                                                            tplCtxnature.value.push(val);
                                                            importnature.push(val);
                                                        }
                                                    });

                                                    dataHelper.path2Value(tplCtxnature, function (params) {
                                                    });
                                                }

                                                var connectedData = ["financer","todo","payed","progress","worker","validFinal","votes"];
                                                $.each( connectedData , function(k,attr) {
                                                    if(notNull("answerObj."+tplCtx.path+"."+attr))
                                                        tplCtx.value[attr] = jsonHelper.getValueByPath(answerObj,tplCtx.path+"."+attr);
                                                });

                                                dataHelper.path2Value( tplCtx, function(params) {
                                                    var tplCtxproject = {};

                                                    if (projectId != null && projectId != "null") {

                                                        if (notNull(tplCtx.actionid)) {
                                                            tplCtxproject.id = tplCtx.actionid;
                                                            tplCtxproject.collection = "actions";
                                                            tplCtxproject.path = "name"
                                                            tplCtxproject.value = tplCtx.value.poste;

                                                            dataHelper.path2Value(tplCtxproject, function () {
                                                                if(typeof newReloadStepValidationInputGlobal != "undefined") {
                                                                    newReloadStepValidationInputGlobal({
                                                                        inputKey : lastinput,
                                                                        inputType : "tpls.forms.ocecoform.budget"
                                                                    })
                                                                }
                                                            });
                                                        }

                                                        var answerId = "<?php echo (string)$answer["_id"]; ?>";
                                                        var cntxtId = "<?php echo $ctxtid; ?>";
                                                        var cntxtType = "<?php echo $ctxttype; ?>";

                                                        ajaxPost("", baseUrl + '/survey/form/generateproject/answerId/' + answerId + '/parentId/' + cntxtId + '/parentType/' + cntxtType,
                                                            null,
                                                            function (data) {
                                                            });
                                                    }
                                                });

                                                delete tplCtx.setType;

                                                if (statusproposition && $.inArray("newaction", statusproposition) == -1)
                                                    tplCtx.value = statusproposition.push("newaction");

                                                tplCtx.path = "status";

                                                dataHelper.path2Value( tplCtx, function(params){});

                                                lastinput++;
                                            });
                                            prioModal.modal('hide');
                                            if (typeof inputsList != "undefined"){
                                                $.each(inputsList , function (inn, vall) {
                                                    $.each(vall , function (inn2, vall2) {
                                                        if (typeof vall2.type != "undefined"
                                                            && vall2.type.split('.')[vall2.type.split('.').length-1] != "undefined"
                                                            && ( vall2.type.split('.')[vall2.type.split('.').length-1] == "budget" ||
                                                                vall2.type.split('.')[vall2.type.split('.').length-1] == "financementFromBudget" ||
                                                                vall2.type.split('.')[vall2.type.split('.').length-1] == "suiviFromBudget"
                                                            )
                                                        ){
                                                            reloadInput(inn2, inn);
                                                        }
                                                    });
                                                });
                                            }
                                        };

                                        reader.readAsText($('.csvdialog .inputcsvdata').prop('files')[0]);

                                    }else{
                                        toastr.error(tradDynForm.weWereUnableToReadYourFile);
                                    }
                                    return false;
                                }
                            },
                            cancel: {
                                label: trad.cancel,
                                className: "btn-secondary",
                                callback: function(){
                                    prioModal.modal('hide');
                                }
                            }
                        },
                        onEscape: function(){
                            prioModal.modal('hide');
                        }
                    });

                    prioModal.on('shown.bs.modal', function (e) {
                        $(".inputcsvdata").change(function(e) {
                            var file = e.target.files;
						    budgetObj.file = file; 
                            theHandlefile(this, e);
                        });

                        

                    });
                    prioModal.modal("show");
                    var dropArea = document.querySelector('.bootbox-body #filed-drop-area');

                    dropArea.addEventListener('drop', function(e) {
                        e.preventDefault();
                        var files = e.dataTransfer.files;
                        budgetObj.file = files;
                        
                        theHandlefile(this, e);
				    });

                  
				

                });

                $('.estibtnedit').off().click(function() {
                    var btn = $(this);
                    tplCtx.pos = $(this).data("pos");
                    tplCtx.collection = "answers";
                    tplCtx.id = $(this).data("id");
                    tplCtx.key = $(this).data("key");
                    tplCtx.form = $(this).data("form");
                    tplCtx.price = $(this).data("price");
                    tplCtx.uid = $(this).data("uid");
                    tplCtx.uname = $(this).data("uname");

                    var days = $(this).data("days");
                    var price = $(this).data("price");
                    var currentValue = [{id:tplCtx.uid,text:tplCtx.uname}];
                    var toDelete = "";
                    tplCtx.value = {};
                    prioModal = bootbox.dialog({
                        message: $(".newform-estimate").html(),
                        show: false,
                        size: "large",
                        className: 'estimatedialog',
                        buttons: {
                            success: {
                                label: trad.save,
                                className: "btn-primary",
                                callback: function () {

                                    tplCtx.path = "answers";
                                    // if( notNull(formInputs [tplCtx.form]) )
                                    tplCtx.path = "answers."+tplCtx.form;

                                    tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".estimates";
                                    tplCtx.setType = [
                                        {
                                            "path": "price",
                                            "type": "int"
                                        },
                                        {
                                            "path": "date",
                                            "type": "isoDate"
                                        }
                                    ];
                                    tplCtx.updatePartial = true;
                                    var today = new Date();
                                    today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();

                                    mylog.log("btnEstimate save",tplCtx);
                                    dataHelper.path2Value( tplCtx, function(){
                                        if(currentValue.length != 0){
                                            var params = {
                                                parentId : answerId,
                                                parentType : "answers",
                                                listInvite : {
                                                    citoyens :{},
                                                    organizations:{}
                                                }
                                            }
                                            $.each(currentValue,function(k,v){
                                                if(exists(v.id) && v.text){
                                                    params.listInvite.citoyens[v.id] = v.text
                                                }
                                            })
                                            ajaxPost("",baseUrl+'/'+moduleId+"/link/multiconnect",params,function(data){
                                                if(typeof newReloadStepValidationInputGlobal != "undefined") {
                                                    newReloadStepValidationInputGlobal({
                                                        inputKey : tplCtx.key,
                                                        inputType : "tpls.forms.ocecoform.budget"
                                                    })
                                                }
                                                ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/addperson/answerid/' + answerId,
                                                    {
                                                        pers : params.listInvite.citoyens,
                                                        url : window.location.href
                                                    },
                                                    function (data) {

                                                    }, "html");
                                            })
                                        }
                                        if(toDelete != ""){
                                            tplCtx.path = "answers."+tplCtx.form+"."+tplCtx.key+"."+btn.data("pos")+".estimates."+toDelete;
                                            mylog.log(tplCtx.path,"toDelete")
                                            tplCtx.value = null;
                                            delete tplCtx.setType;
                                            delete tplCtx.updatePartial;
                                            dataHelper.path2Value( tplCtx, function(){
                                                if(typeof newReloadStepValidationInputGlobal != "undefined") {
                                                    newReloadStepValidationInputGlobal({
                                                        inputKey : tplCtx.key,
                                                        inputType : "tpls.forms.ocecoform.budget"
                                                    })
                                                }
                                                links.disconnect('answers',answerId,toDelete,'citoyens','contributors')
                                                var person = {};
                                                person[toDelete] = "";
                                                ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/rmperson/answerid/' + answerId,
                                                    {
                                                        pers : person,
                                                        url : window.location.href
                                                    },
                                                    function (data) {

                                                    }, "html");
                                            })
                                        }
                                        saveLinks(answerObj._id.$id,"estimated",userId);
                                        prioModal.modal('hide');
                                        reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                    } );
                                }
                            },
                            cancel: {
                                label: trad.cancel,
                                className: "btn-secondary",
                                callback: function(){
                                    prioModal.modal('hide');
                                }
                            }
                        },
                        onEscape: function(){
                            prioModal.modal('hide');
                        }
                    });

                    prioModal.on('shown.bs.modal', function (e) {
                        $(".bootbox .e-days,.bootbox .e-price").on('blur',function(){
                            $(".search-person").trigger('change');
                        })
                        setTimeout(() => {
                            $(".search-person").trigger('change');
                        }, 700);
                        $(".e-price").val(price);
                        $(".e-days").val(days);
                        $(".search-person").val(tplCtx.uid).select2({
                            minimumInputLength: 1,
                            //maximumSelectionSize:1,
                            //tags: true,
                            multiple:true,
                            "tokenSeparators": [','],
                            createSearchChoice: function (term, data) {
                                if (!data.length)
                                    return {
                                        id: term,
                                        text: term
                                    };
                            },
                            initSelection : function (element, callback) {
                                var temp = [];
                                mylog.log(element.val().split(","),'rakoto0')
                                $(element.val().split(",")).each(function () {
                                    //data.push({id: this, text: this});_inviteObj
                                    temp.push({id: this, text: tplCtx.uname});
                                });
                                callback(temp);
                            },
                            ajax: {
                                url: baseUrl+"/"+moduleId+"/search/globalautocomplete",
                                dataType: 'json',
                                type: "POST",
                                quietMillis: 50,
                                data: function (term) {
                                    return {
                                        name: term,
                                        searchType : ["citoyens"],
                                        filters : {
                                            '$or' : {
                                                "links.projects.<?= array_keys($parentForm["parent"])[0] ?>":{'$exists':true},
                                                "links.memberOf.<?= array_keys($parentForm["parent"])[0] ?>":{'$exists':true}
                                            }
                                        }
                                    };
                                },
                                results: function (data) {
                                    return {
                                        results: $.map(Object.values(data.results), function (item) {
                                            return {
                                                text: item.name,
                                                id: item._id.$id
                                            }
                                        })
                                    };
                                }
                            }
                        }).on('change',function(e){
                            if(exists(e.added)){
                                currentValue.push(e.added);
                            }
                            if(exists(e.removed)){
                                currentValue = currentValue.filter(data => data.id != e.removed.id);
                                if(tplCtx.uid == e.removed.id)
                                    toDelete = e.removed.id;
                            }
                            mylog.log(toDelete,"currentValue")
                            if(exists(currentValue) && currentValue.length != 0){
                                $.each(currentValue,function(k,v){
                                    tplCtx.value[v.id] = {
                                        price : (parseInt($(".bootbox .e-price").val())/currentValue.length),
                                        days : $(".bootbox .e-days").val(),
                                        name :  v.text,
                                        date : today,
                                        addedBy:userId,
                                    }
                                })
                            }
                        });
                    });

                    prioModal.modal("show");

                });

                $('.estibtndelete').off().click(function() {
                    tplCtx.pos = $(this).data("pos");
                    tplCtx.collection = "answers";
                    tplCtx.id = $(this).data("id");
                    tplCtx.key = $(this).data("key");
                    tplCtx.form = $(this).data("form");
                    tplCtx.uid = $(this).data("uid");

                    prioModal = bootbox.dialog({
                        title: trad.confirmdelete,
                        show: false,
                        message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                        buttons: [
                            {
                                label: "Ok",
                                className: "btn btn-primary pull-left",
                                callback: function() {
                                    /*var esti_pos = FiData[tplCtx.pos]["estimates"];
                                    delete esti_pos[tplCtx.uid];*/

                                    tplCtx.path = "answers";
                                    // if( notNull(formInputs [tplCtx.form]) )
                                    tplCtx.path = "answers."+tplCtx.form;

                                    tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+tplCtx.uid;

                                    tplCtx.value = null;

                                    mylog.log("btnEstimate save",tplCtx);
                                    dataHelper.path2Value( tplCtx, function(){
                                        if(typeof newReloadStepValidationInputGlobal != "undefined") {
                                            newReloadStepValidationInputGlobal({
                                                inputKey : tplCtx.key,
                                                inputType : "tpls.forms.ocecoform.budget"
                                            })
                                        }
                                        links.disconnect('answers',answerObj._id.$id,tplCtx.uid,'citoyens','contributors')
                                        var person = {};
                                        person[tplCtx.uid] = "";
                                        ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/rmperson/answerid/' + answerId,
                                            {
                                                pers : person,
                                                url : window.location.href
                                            },
                                            function (data) {

                                            }, "html");
                                        saveLinks(answerObj._id.$id,"estimated",userId);
                                        prioModal.modal('hide');
                                        reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");

                                    } );
                                }
                            },
                            {
                                label: "Annuler",
                                className: "btn btn-default pull-left",
                                callback: function() {}
                            }
                        ]
                    });

                    prioModal.modal("show");
                });

                <?php if ($paramsData["estimate"])
                { ?>

                $('.btnEstimateSelected').off().click(function() {
                    tplCtx.pos = $(this).data("pos");
                    tplCtx.collection = "answers";
                    tplCtx.id = $(this).data("id");
                    tplCtx.key = $(this).data("key");
                    tplCtx.form = $(this).data("form");
                    tplCtx.price = $(this).data("price");

                    $(this).removeClass('btn-default').addClass("btn-success");

                    //remove all selected
                    var esti_pos = FiData[tplCtx.pos]["estimates"];

                    $.each(esti_pos, function(eid, evl){
                        if (typeof evl["selected"] != "undefined" && evl["selected"] == true) {
                            esti_pos[eid]["selected"] = false;
                        }
                    })

                    tplCtx.path = "answers";
                    // if( notNull(formInputs [tplCtx.form]) )
                    tplCtx.path = "answers."+tplCtx.form;

                    tplCtx.pos = $(this).data("pos");
                    tplCtx.collection = "answers";
                    tplCtx.id = $(this).data("id");
                    tplCtx.key = $(this).data("key");
                    tplCtx.form = $(this).data("form");
                    tplCtx.price = $(this).data("price");

                    tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".estimates";

                    tplCtx.value = esti_pos;

                    mylog.log("btnEstimate save",tplCtx);
                    dataHelper.path2Value( tplCtx, function(){
                        saveLinks(answerObj._id.$id,"estimated",userId);

                    } );
                    //end remove all selected

                    tplCtx.pos = $(this).data("pos");
                    tplCtx.collection = "answers";
                    tplCtx.id = $(this).data("id");
                    tplCtx.key = $(this).data("key");
                    tplCtx.form = $(this).data("form");
                    tplCtx.price = $(this).data("price");

                    tplCtx.pathBase = "answers."+tplCtx.form;

                    tplCtx.path = tplCtx.pathBase+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+$(this).data("uid")+".selected";
                    tplCtx.value = true;
                    var thisbtn = $(this);
                    mylog.log("btnEstimateSelected save",tplCtx);
                    dataHelper.path2Value( tplCtx, function(){
                        tplCtx.pos = thisbtn.data("pos");
                        tplCtx.collection = "answers";
                        tplCtx.id = thisbtn.data("id");
                        tplCtx.key = thisbtn.data("key");
                        tplCtx.form = thisbtn.data("form");
                        tplCtx.price = thisbtn.data("price");

                        tplCtx.path = tplCtx.pathBase+"."+tplCtx.key+"."+tplCtx.pos+".price";
                        tplCtx.value = tplCtx.price;

                        mylog.log("btnEstimateSelected save",tplCtx);
                        dataHelper.path2Value( tplCtx, function(){
                            if(typeof newReloadStepValidationInputGlobal != "undefined") {
                                newReloadStepValidationInputGlobal({
                                    inputKey : tplCtx.key,
                                    inputType : "tpls.forms.ocecoform.budgetTwo"
                                })
                            }
                            $("#price"+tplCtx.pos).html( tplCtx.price+"€" );
                            saveLinks(answerObj._id.$id,"intentValidated",userId);
                            reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                        } );
                    } );
                });

                <?php
                } ?>


            });

            function theHandlefile(thisss, e)
            {
                $(".addfromcsvdivrs").html('<span class="homestead"><i class="fa fa-spin fa-circle-o-noch"></i> '+trad.erroroccurred+'...</span>')

                    var ext = $(thisss).val().split(".").pop();

                    if (budgetObj.file[0].name.split(".").pop() != "csv") {
                        toastr.error(tradDynForm.youMustUseACSVFormat);
                        return false;
                    }

                    if (budgetObj.file.length > 0) {

                        var reader = new FileReader();

                        reader.onload = function(e) {
                            var csvval = e.target.result.split("\n");

                            var str = '<table class="table table-bordered "><tbody>';
                            $.each(csvval, function(ind, val){
                                str += '<tr>';
                                var thh = val.split(";");
                                $.each(thh , function(thhid, thhhval){
                                    str += '<th>'+thhhval+'</th>';
                                });
                                str += '</tr>';
                            });

                            str += '</tbody></table>';

                            $(".addfromcsvdivrs").html(str);

                        };
                        reader.readAsText(budgetObj.file.item(0));

                    }else{
                        toastr.error(tradDynForm.weWereUnableToReadYourFile);
                    }
                    return false;
            }


        </script>
        <?php
    }
    else
    {
        //echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";

    }

}

?>

