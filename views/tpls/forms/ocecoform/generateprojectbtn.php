<style type="text/css">
	.op-generateproject {
		border-radius: 15px;
		margin: 0 auto;
	}
    .generateproject-<?= $kunik ?> .select2-container .select2-choice .select2-arrow {
        background: none repeat scroll 0 0 transparent;
        border-left: 0 none;
        padding-right: 24px !important;
    }

    .generateproject-<?= $kunik ?> .select2-container .select2-choice {
        background-image: none;
        border: 1px solid #e6e8e8;
        border-radius: 5px;
        height: 35px;
        padding: 3px 14px;
        transition: border 0.2s linear 0s;
        border: 1px solid #aaa !important;
    }
</style>

<div class="form-group generateproject-<?= $kunik ?>" id="projectdiv" style="background-color:#fff ;overflow-x: auto;">
	<div ><h4 class="titlecolor<?php echo $kunik ?> pdftittlecolor text-center" style="padding-top:20px;"><?php echo $label ; ?></h4>
	</div>
<?php

$isAapProject = $isAap;
$contextId = $ctxtid = $context["_id"];
$contextType = $ctxttype = $parentForm["parent"][(string)$context["_id"]]["type"];

$links=[];
if (!empty($answer["links"])) {
    foreach ($answer["links"] as $k => $value) {
        if ($k == "answered") {
            $links = array_unique(array_merge($links, $value), SORT_REGULAR);
        } elseif (is_array($value)) {
            $links = array_unique(array_merge($links, array_keys($value)), SORT_REGULAR);
        }
    }
}

if ($canAdminAnswer && empty($answer["project"]["id"])) {
?>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

        <div class="form-group text-center">
            <div class="form-inline form-group">
            <label class="">Génerer un nouveau projet </label>
	<button type="button" class="text-center btn btn-outline-primary op-generateproject" data-collection="<?php echo Form::ANSWER_COLLECTION ?>"><?php echo $info ?> &nbsp;<i class="fa fa-plus " ></i></button><br>

        <h5>ou</h5>
                    <label class=""> Associer à un projet existant:  </label>
                    <select class="" id="op-applyproject-select" >
                        <option value="--" selected>séléctionner un projet</option>
                        <?php foreach ($parentProjects as $projectid => $project){
                            ?>
                            <option value="<?php echo $projectid; ?>"><?php echo $project["name"]; ?></option>
                        <?php } ?>
                    </select>
                <button type="button" class="text-center btn btn-default btn-outline-primary op-applyproject-btn" data-collection="<?php echo Form::ANSWER_COLLECTION ?>">Associer le projet</button>

            </div>
            </div>

        <?php
}elseif(!$canAdminAnswer && empty($answer["project"]["id"])){
	    ?>
    <div class="text-center">
        Le lien du projet  apparaitra ici une fois générée par l'admin
    </div>
        <?php
}elseif(isset($answer["project"]["id"])){
		$el = $projects;
        if (isset($el["profilMediumImageUrl"])){
?>
			<div class="text-center">
				<img src="<?php echo Yii::app()->createUrl('/'.$el["profilImageUrl"]); ?>" class="text-center img-circle" width='60px' height='60px' />
			</div>
            <h5 class="text-center"><?= $el["name"] ?></h5>
	    <?php
	    } else {
	    ?>
		    <div class="text-center">
	            <img src="<?php echo Yii::app()->getModule('co2')->assetsUrl.'/images/thumb/default_organizations.png'; ?>" class="text-center img-circle" width='60px' height='60px' />
	        </div>

	    <?php
	    	echo "<div class='text-center'><a href='#page.type.".Project::COLLECTION.".id.".@$answer["project"]["id"]."' class='lbh-preview-element' >".@$el["name"]."</a></div>";
	    	if (isset($el["shortDescription"])) {
	    		echo "<div class='text-center text-success'>".$el["shortDescription"]."</div>";
	    	}
	    }
		?>
    <div class="text-center">
            <button type="button" class="text-center btn btn-outline-primary disspro" data-collection="<?php echo Form::ANSWER_COLLECTION ?>">dissocier</button>
    </div>
        <?php
}
?>
</div>
<script type="text/javascript">
    var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( (isset($paramsData)) ? $paramsData : null ); ?>;

    var statusproject = <?php echo (isset($answers["status"]) ? json_encode($answers["status"]) : "[]"); ?>;
    var answerId = "<?php echo (string)$answer["_id"]; ?>";
    var answerLinks = <?= json_encode($links) ?>;
	var cntxtId = "<?php echo $ctxtid; ?>";
	var cntxtType = "<?php echo $ctxttype; ?>";
	var murirStep = "<?php echo (isset($parentForm['mapping']['murir']) ? $parentForm['mapping']['murir'] : ""); ?>";

    function matchCustom(params, data) {
        // If there are no search terms, return all of the data
        if ($.trim(params.term) === '') {
            return data;
        }

        // Do not display the item if there is no 'text' property
        if (typeof data.text === 'undefined') {
            return null;
        }

        // `params.term` should be the term that is used for searching
        // `data.text` is the text that is displayed for the data object
        if (data.text.indexOf(params.term) > -1) {
            var modifiedData = $.extend({}, data, true);
            modifiedData.text += ' (matched)';

            // You can return modified objects from here
            // This includes matching the `children` how you want in nested data sets
            return modifiedData;
        }

        // Return `null` if the term should not be displayed
        return null;
    }

    $("#op-applyproject-select").select2({
        width : '300px'
    });

    $('.op-generateproject').off().click(function() {
        
        var today = new Date();
        var tplCtxCollection = $(this).data("collection");
        var name = <?= json_encode($answer["answers"]["aapStep1"]["titre"]?? "")  ?>;

        var <?php echo $kunik ?>generateProject = function(){
            coInterface.showLoader("#projectdiv");
            ajaxPost("", baseUrl+'/survey/form/generateproject/answerId/'+answerId+'/parentId/'+cntxtId+'/parentType/'+cntxtType,
            null,
            function(data){
                tplCtx.id = answerId;
                tplCtx.collection = tplCtxCollection;
                tplCtx.path = "project";
                tplCtx.value = {
                    "id" : data["projet"],
                    "startDate" : today.getDay()+'/'+today.getMonth()+"/"+today.getFullYear()
                };
                dataHelper.path2Value( tplCtx, function(params) {} );

                tplCtx.path = "step";
                tplCtx.value = murirStep;

                dataHelper.path2Value( tplCtx, function(params) {
                    tplCtx4 = {};
                    tplCtx4.id = answerId;
                    tplCtx4.collection = tplCtxCollection;
                    tplCtx4.value = statusproject;

                    tplCtx4.value.push("projectstate");
                    tplCtx4.path = "status";
                    dataHelper.path2Value( tplCtx4, function(params){
                        if(typeof reloadWizard != "undefined") {
                            reloadWizard(function () {
                                if (exists(params.elt) && exists(params.elt.project) && exists(params.elt.project.id)) {
                                    $('#aapStep1 .sectionStepTitle,#aapStep1stepper .stepDesc').text("Projet");
                                    $(".sectionStepTitle").css("background", "#9fbd38");
                                    $('label[for="titre"]').children('h4').text("Nom du projet");
                                    $('label[for="description"]').children('h4').text("Description du projet");
                                    $('label[for="uploaderimage"]').children('h4').text("Ajouter une image pour votre projet");
                                }
                            });
                            if(typeof reloadInput != "undefined") {
                                reloadInput("<?= $key ?>", "<?= (string)@$form["id"] ?>");
                            }
                        }else{
                            reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                        }
                        /*if(notNull(aapObject) && notNull(aapObject.sections) && notNull(aapObject.sections.listItem) && notNull(aapObject.sections.listItem.currentAnswer)) {
                            $('.close-modal').off().on('click', function () {
                                if (notEmpty(viewParams) && notEmpty(aapObject.sections.listItem.currentAnswer)) {
                                    aapObject.sections.listItem.reloadPanel(viewParams.view);
                                }
                            })
                        }*/
                    });
                } );
            },"html");
        }

        var <?php echo $kunik ?>checkExistsProject = function(){
            var params = {
                searchType : ["projects"],
                notSourceKey : true,
                name : name,
                fields : ['name',"collection"]
            };

            params.filters={};
            params.filters["parent."+cntxtId]={'$exists':true};
        
            ajaxPost(
                null,
                baseUrl+"/" + moduleId + "/search/globalautocomplete",
                params,
                function(data){
                    if(notEmpty(data.results)){
                        var html = `
                        <div class="alert alert-warning">
                            Un projet "<b> ${escapeHtml(name)} </b>" existe déja ! Voullez vous continuer ?
                        </div>
                        <h4 class="text-center">Projet(s) existant(s)</h4>`;
                        html += `<ul class="list-group">`;
                        $.each(data.results,function(k,v){
                            html += 
                            `<li class="list-group-item">
                                <a href="#page.type.${v.collection}.id.${k}" target="_blank">${v.name}</a>
                            </li>`;
                        })
                        html += `</ul>`;

                        var btbx = bootbox.confirm({
                            message: html,
                            buttons: {
                                confirm: {
                                    label: "Générer un projet",
                                    className: "btn-warning"
                                },
                                cancel: {
                                    label: "Selection un projet existant",
                                    className: "btn-primary"
                                }
                            },
                            callback: function(result) {
                                if(result)
                                    <?=$kunik ?>generateProject();
                                else{
                                    var element = document.querySelector('#s2id_op-applyproject-select .select2-choice');
                                    var event = new MouseEvent('mousedown', {
                                        bubbles: true,
                                        cancelable: true,
                                        view: window
                                    });

                                    if(notNull(element)){
                                        element.dispatchEvent(event);
                                        $(".select2-drop .select2-input").val(name);
                                    }
                                        
                                }
                                
                                    
                            }
                        })

                        btbx.on('shown.bs.modal', function(e){
                            coInterface.bindLBHLinks();
                        });
                    }else{
                        <?=$kunik ?>generateProject();
                    }
                },
                null,
                null,
                {async : false}
            )
        }
        <?php echo $kunik ?>checkExistsProject();
    });

    $('.op-applyproject-btn').off().click(function() {

        var applyproject = $('#op-applyproject-select').val();
        if (applyproject != null && applyproject != "" && applyproject != "--"){
            coInterface.showLoader("#projectdiv");
            var today = new Date();
            var tplCtxCollection = $(this).data("collection");

            if (applyproject != null && applyproject != "--") {
                tplCtx.id = answerId;
                tplCtx.collection = tplCtxCollection;
                tplCtx.path = "project";
                tplCtx.value = {
                    "id" : applyproject,
                    "startDate" : today.getDay()+'/'+today.getMonth()+"/"+today.getFullYear()
                };
                dataHelper.path2Value( tplCtx, function(params) {
                    ajaxPost("", baseUrl+'/survey/form/connectlink/childType/citoyens/parentId/'+applyproject+'/parentType/projects',
                        {links : answerLinks},
                        function(data){

                        }
                    )
                } );

                tplCtx.id = answerId;
                tplCtx.collection = tplCtxCollection;
                tplCtx.path = "step";
                tplCtx.value = murirStep;
                dataHelper.path2Value( tplCtx, function(params) {
                    //reloadWizard();
                    bootbox.dialog({
                        title: "Synchroniser les lignes de depenses ?",
                        message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                        buttons: [
                            {
                                label: "Oui",
                                className: "btn btn-primary pull-left",
                                callback: function() {
                                    ajaxPost("", baseUrl+'/survey/form/generatedepense/projectid/'+applyproject,
                                        null,
                                        function(data){
                                            if (data != null){
                                                $.each(data, function(index, value){
                                                    var tplCtx = {};
                                                    var today = new Date();
                                                    var countact = 1;
                                                    tplCtx.value = {group : "", nature : "", date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
                                                    //value
                                                    if (typeof value.name != "undefined"){

                                                        tplCtx.value["poste"] = value.name;
                                                        tplCtx.value["montant"] = value.credits;


                                                        var max<?php echo $kunik ?> = 0;
                                                        var nextValuekey<?php echo $kunik ?> = 0;

                                                        if(notNull(<?php echo $kunik ?>Data)){
                                                            $.each(<?php echo $kunik ?>Data, function(ind, val){
                                                                if(parseInt(ind) > max<?php echo $kunik ?>){
                                                                    max<?php echo $kunik ?> = parseInt(ind);
                                                                }
                                                            });

                                                            nextValuekey<?php echo $kunik ?> = max<?php echo $kunik ?> + 1;

                                                        }


                                                        tplCtx.path = "answers.aapStep1.depense."+nextValuekey<?php echo $kunik ?>;

                                                        tplCtx.id = answerId;
                                                        tplCtx.collection = "answers";
                                                        dataHelper.path2Value( tplCtx, function(){

                                                        });

                                                    }

                                                });

                                                tplCtx4 = {};
                                                tplCtx4.id = answerId;
                                                tplCtx4.collection = tplCtxCollection;
                                                tplCtx4.value = statusproject;

                                                tplCtx4.value.push("projectstate");
                                                tplCtx4.path = "status";
                                                dataHelper.path2Value( tplCtx4, function(params){
                                                    reloadWizard(function(){
                                                        if(exists(params.elt) && exists(params.elt.project) && exists(params.elt.project.id)){
                                                            $('#aapStep1 .sectionStepTitle,#aapStep1stepper .stepDesc').text("Projet");
                                                            $(".sectionStepTitle").css("background","#9fbd38");
                                                            $('label[for="titre"]').children('h4').text("Nom du projet");
                                                            $('label[for="description"]').children('h4').text("Description du projet");
                                                            $('label[for="uploaderimage"]').children('h4').text("Ajouter une image pour votre projet");
                                                        }
                                                    });
                                                });
                                            }
                                            reloadWizard();
                                        }
                                        ,"json"
                                    );
                                }
                            },
                            {
                                label: "Non",
                                className: "btn btn-default pull-left",
                                callback: function() {
                                    reloadWizard();
                                }
                            }
                        ]
                    });


                } );
            }
        } else {
            toastr.error('Veuillez sélectionner un projet');
        }
    });

    $('.disspro').off().click(function() {
        tplCtx.id = answerId;
        tplCtx.collection = "answers";
        tplCtx.path = "project";
        tplCtx.value = null;
        dataHelper.path2Value( tplCtx, function(params) {
            reloadWizard();
        } );
    });
</script>
