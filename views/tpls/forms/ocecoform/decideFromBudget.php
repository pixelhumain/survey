<?php

?>
<?php
if($answer){
	$copy = "aapStep1.depense";

	if( isset($parentForm["params"][$kunik]["budgetCopy"]) )
		$copy = $parentForm["params"][$kunik]["budgetCopy"];
	else if( count(Yii::app()->session["budgetInputList"]) == 1 )
		$copy = array_keys( Yii::app()->session["budgetInputList"])[0];

	//var_dump($copy);
	$copyT = explode(".", $copy);
	// $copyF = $copyT[0];
	$copyF = "aapStep1";
	// $budgetKey = $copyT[1];
	$budgetKey = "depense";
	$answers = null;	
	//var_dump($budgetKey);
	if($wizard){
		if( $budgetKey ){
			if( isset($answer["answers"][$copyF][$budgetKey]) && count($answer["answers"][$copyF][$budgetKey])>0)
				$answers = $answer["answers"][$copyF][$budgetKey];
		} else if( isset($answer["answers"][$copyF][$kunik]) && count($answer["answers"][$copyF][$budgetKey])>0 )
			$answers = $answer["answers"][$copyF][$kunik];
	} else {
		if($budgetKey)
			$answers = $answer["answers"][$budgetKey];
		else if(isset($answer["answers"][$kunik]))
			$answers = $answer["answers"][$kunik];
	}
	
$editBtnL =  "";

$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";


$paramsData = [ 
	"group" => ["Feature","Costum","Chef de Projet","Data","Mantenance"],
	"nature" => [
        "investissement" => "Investissement",
        "fonctionnement" => "Fonctionnement"
    ],
	"amounts" => [
    	"price" => "Price"
    ]
];

if( isset($parentForm["params"][$kunik]) ) {
	if( isset($parentForm["params"][$budgetKey]["group"]) ) 
		$paramsData["group"] =  $parentForm["params"][$budgetKey]["group"];
	if( isset($parentForm["params"][$budgetKey]["nature"]) ) 
		$paramsData["nature"] =  $parentForm["params"][$budgetKey]["nature"];
	if( isset($parentForm["params"][$budgetKey]["amounts"]) ) 
		$paramsData["amounts"] =  $parentForm["params"][$kunik]["amounts"];
}

$communityLinks = array();
if(!empty($form["parent"])){
	foreach ($form["parent"] as $key => $value) {
		$cl= Element::getCommunityByTypeAndId($key,$value["type"]);
		if(!empty($cl))
			$communityLinks = array_merge($communityLinks, $cl);
	}
}
$organizations = Link::groupFindByType( Organization::COLLECTION,$communityLinks,["name","links"] );

$orgs = [];

foreach ($organizations as $id => $or) {
	$roles = $or["links"]["memberOf"][$this->costum["contextId"]]["roles"];
	if( $paramsData["limitRoles"] && !empty($roles))
	{
		foreach ($roles as $i => $r) {
			if( in_array($r, $paramsData["limitRoles"]) )
				$orgs[$id] = $or["name"];
		}		
	}
}
$listLabels = array_merge($orgs);//Ctenat::$financerTypeList,

$properties = [
    
    "poste" => [
        "inputType" => "text",
        "label" => "Contexte",
        "placeholder" => "Contexte",
        "rules" => [ "required" => true ]
    ]
];
foreach ($paramsData["amounts"] as $k => $l) {
	$properties[$k] = [ "inputType" => "text",
			            "label" => $l,
			            "propType" =>"amount",
			            "placeholder" => $l,
			            "rules" => [ "required" => true, "number" => true ]
			        ];
}
$properties["votes"] = [
    "inputType" => "text",
    "label" => "Votes",
    "placeholder" => "Votes",
    "rules" => [ "required" => true,"number" => true  ]
];

	?>	
<div class="form-group">
	<label ><h4 style="color:<?php echo (!empty($titleColor) ? $titleColor : "black" ); ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL ; ?></h4></label>
	<table class="table table-bordered table-hover  directoryTable oceco-styled-table" id="<?php echo $kunik?>">

	<thead>	
		<?php if($answers){ ?>
		<tr>
			</th>
			<?php 
			
			foreach ($properties as $i => $inp) {
				echo "<th>".$inp["placeholder"]."</th>";
			} ?>
			<th></th>
		</tr>
		<?php } ?>
	</thead>
	<tbody class="directoryLines">	
		<?php 
		$ct = 0;
		$totalVotes = 0;
		$bigTotal = 0;
		$totalPayed = 0;
		$validDecision = 0;
		$invalidDecision = 0;
		$payedWork = 0;
		if($answers){
			foreach ($answers as $q => $a) {

				$trStyle = "";
				$tds = "";
				foreach ($properties as $i => $inp) 
				{
					$tds .= "<td style='vertical-align:middle'>";
					
					if( $i == "poste") 
						$tds .= $a["poste"];
					if( $i == "price" && !empty($a["price"]))
						$tds .= $a["price"]."€";
					else if( $i == "workType" && isset( $a["worker"]["workType"] ) ) 
						$tds .= $a["worker"]["workType"];
					else if( $i == "votes"){
						$votes = (!empty($a["votes"]) ) ? (int)$a["votes"] : 0;
						

						$thisVoteTotal = 0;
						$percol = "danger";
						$youVoted = "";
						if(!empty($a["votes"]) ){
							$youVoted = "<span class='label label-default'>".count($a["votes"])."voter(s)"."</span>";
							foreach ($a["votes"] as $uid => $v) {
								$thisVoteTotal += (int)$v["vote"];
							}
							$thisVoteTotal = $thisVoteTotal / count($a["votes"]);
							if( $thisVoteTotal > 50 ){
								$percol = "warning";
								$validDecision++;
							}
							if( $thisVoteTotal > 75 )
								$percol = "success";
							if( $thisVoteTotal < 50 )
								$invalidDecision++;


							if( isset( $a["votes"][ Yii::app()->session["userId"] ] ) ){
								$col = "danger";
								if( (int)$a["votes"][Yii::app()->session["userId"]]["vote"] > 50 )
									$col = "success";
								$youVoted .= "<br/><span class='label label-".$col."'> you : ".$a["votes"][Yii::app()->session["userId"]]["vote"]."%</span>";
							}
						}
						$totalVotes += $thisVoteTotal;
						
						$tds .= "<a href='javascript:;' data-id='".$answer["_id"]."' data-budgetpath='".$copy."' data-form='".$copyF."' data-pos='".$q."'  class=' btnVote margin-left-5 padding-10'>".$thisVoteTotal."%</a>";
						$tds .= '<div class="progress btnVote"  data-id="'.$answer["_id"].'" data-budgetpath="'.$copy.'" data-form="'.$copyF.'" data-pos="'.$q.'"  style="cursor:pointer;margin-bottom: 0px;">'.
						  '<div class="progress-bar progress-bar-'.$percol.'" style="width:'.$thisVoteTotal.'%">'.
							    '<span class="sr-only">'.$thisVoteTotal.'% Complete</span>'.
						  '</div>'.
						'</div>'.$youVoted;
					}
					else if( isset( $a[$i] ) ) 
						$tds .= $a[$i];
					
					$tds .= "</td>";
				}
					
			?>
			
			<?php 
				$ct++;
				echo "<tr id='".$kunik.$q."' class='".$kunik."Line text-center' style='".$trStyle."'>";
				echo $tds;?>
				<td>
					<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
				</td>
			</tr>
			<?php 
			}
		}
?>
		</tbody>
	</table>

<?php 
$percol = "danger";
$totalVotes = (!empty($answers)) ? $totalVotes / count($answers) : 0;
if( $totalVotes > 50  ){
	$percol = "success";
}
echo "<h4 style='color:".(($titleColor) ? $titleColor : "black")."'>Décision Globale</h4>".
'<div class="progress " style="cursor:pointer" >'.
  '<div class="progress-bar progress-bar-'.$percol.'" style="width:'.$totalVotes.'%">'.
	    '<span class="sr-only">'.$totalVotes.'% Complete</span>'.
  '</div>'.
'</div>'; ?>

<table class="table table-bordered table-hover  ">
	<tbody class="">
		<tr>
			<td>Pour</td>
			<td><?php echo floor($totalVotes) ?>%</td>
		</tr>
		<tr>
			<td>Contre</td>
			<td><?php echo floor(100-$totalVotes)+1 ?>%</td>
		</tr>
		<tr>
			<td>Nombres de Décisions Validés </td>
			<td><?php echo $validDecision."/".count($answers) ?></td>
		</tr>
		<tr>
			<td>Nombres de Décisions Refusés </td>
			<td><?php echo $validDecision."/".count($answers) ?></td>
		</tr>
		
	</tbody>
</table>


</div>

<div class="form-votes" style="display:none;">
	<select id="votes" style="width:100%;">
		<option> VOTEZ pour cette partie </option>
		<?php foreach ([0,10,20,30,40,50,60,70,80,90,100] as $v => $f) {
			if($f == 0)
				$lbl = "Je ne suis pas pour!";
			else 
				$lbl = "Je suis pour à ".$f."%";
			echo "<option value='".$f."'>".$lbl."</option>";
		} ?>
	</select>
  
</div>





<?php 
if( isset($parentForm["params"]["financement"]["tpl"])){
	//if( $parentForm["params"]["financement"]["tpl"] == "tpls.forms.equibudget" )
		echo $this->renderPartial( "costum.views.".$parentForm["params"]["financement"]["tpl"] , 
		[ "totalFin"   => $total,
		  "totalBudg" => Yii::app()->session["totalBudget"]["totalBudget"] ] );
	// else 
	// 	$this->renderPartial( "costum.views.".$parentForm["params"]["financement"]["tpl"]);
}
 ?>

<script type="text/javascript">
if(typeof dyFObj.elementObjParams == "undefined")
	dyFObj.elementObjParams = {};

dyFObj.elementObjParams.budgetInputList = <?php echo json_encode( Yii::app()->session["budgetInputList"] ); ?>;
//dyFObj.elementObjParams.financerTypeList = <?php //echo json_encode(Ctenat::$financerTypeList); ?>;
dyFObj.elementObjParams.workerList = <?php echo json_encode($orgs); ?>;


var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answer["answers"][$copy])) ? $answer["answers"][$copy] : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

$(document).ready(function() { 
	
	sectionDyf.<?php echo $kunik ?> = {
		"jsonSchema" : {	
	        "title" : "Plan de Financement",
            "icon" : "fa-money",
            "text" : "Décrire ici les financements mobilisés ou à mobiliser. Les coûts doivent être en <b>hors taxe</b>.",
	        "properties" : <?php echo json_encode( $properties ); ?>,
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    prioModal.modal('hide');
						//reloadInput("<?php //echo $key ?>//", "<?php //echo @$form["id"] ?>//");
	                    reloadWizard();
	                } );
	            }

	    	}
	    }
	};

	sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "<?php echo $kunik ?> config",
	        "icon" : "fa-cog",
	        "properties" : {
	            limitRoles : {
	                inputType : "array",
	                label : "Liste des roles financeurs",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.limitRoles
	            },
	            tpl : {
	                label : "Sub Template",
	                value :  sectionDyf.<?php echo $kunik ?>ParamsData.tpl
	            },
	            budgetCopy : {
	            	label : "Input Bugdet",
	            	inputType : "select",
	            	options :  dyFObj.elementObjParams.budgetInputList
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	        		if(val.inputType == "array")
	        		 	tplCtx.value[k] = getArray('.'+k+val.inputType);
	        		else if(val.inputType == "properties")
	        		 	tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
	        		else
	        		 	tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    prioModal.modal('hide');
						//reloadInput("<?php //echo $key ?>//", "<?php //echo @$form["id"] ?>//");
                        reloadWizard();
	                } );
	            }

	    	}
	    }
	};


    mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

    //adds a line into answer
    $(".add<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
    });

    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
    });

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    
    $('.decidebtnValidateWork').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.budgetpath = $(this).data("budgetpath");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.form = $(this).data("form");
		prioModal = bootbox.dialog({
	        message: $(".form-validate-work").html(),
	        title: "Validation des travaux",
	        show: false,
	        buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {
			            
			        	var formInputsHere = formInputs;
			        	tplCtx.path = "answers";
			        	if( notNull(formInputs [tplCtx.form]) )
			        		tplCtx.path = "answers."+tplCtx.form;    

			        	tplCtx.path = tplCtx.path+"."+tplCtx.budgetpath+"."+tplCtx.pos+".validFinal";	   
			        	
			        	var today = new Date();
						today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();	
			            tplCtx.value = { 
		        			valid : $(".bootbox #validDecision").val(),
		        			user : userId,
		        			date : today
		        		};
		        		delete tplCtx.pos;
		        		delete tplCtx.budgetpath;
				    	mylog.log("btnValidateWork save",tplCtx);
				  	 	dataHelper.path2Value( tplCtx, function(){
				  	 		saveLinks(answerObj._id.$id,"validated",userId, function (){
				  	 			prioModal.modal('hide');
								//reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
				  	 		    reloadWizard();
				  	 		});
				  	 	} );
				  	 }		
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: closePrioModal
                }
              },
		        onEscape: closePrioModal
	    });
	    prioModal.modal("show");
	});

	$('.btnVote').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.budgetpath = $(this).data("budgetpath");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.form = $(this).data("form");
		prioModal = bootbox.dialog({
	        message: $(".form-votes").html(),
	        title: "Voter pour partie",
	        show: false,
	        buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {
			        	tplCtx.path = "answers."+tplCtx.budgetpath+"."+tplCtx.pos+".votes."+userId; 

			        	var today = new Date();
						today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();	
			            tplCtx.value = {
			            	vote : $(".bootbox #votes").val(),
			            	date : today
			            };

		        		delete tplCtx.pos;
		        		delete tplCtx.budgetpath;
				    	mylog.log("btnProgress save",tplCtx);
				  	 	dataHelper.path2Value( tplCtx, function(){
				  	 		saveLinks(answerObj._id.$id,"voted",userId);
				  	 		prioModal.modal('hide');
							/**/reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
				  	 	    reloadWizard();
				  	 	} );
			        }		
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: closePrioModal
                }
              },
		        onEscape: closePrioModal
	    });
	    prioModal.modal("show");
	});

});
function closePrioModal(){
	prioModal.modal('hide');
}

</script>
<?php } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>