<style type="text/css">
    .financementerror {
        color: #e74c3c;
        font-size: 20px;
        background-color: #ecf0f1;
        text-align: center;
        padding: 20px;
    }

    .enveloppelabel {
        color: #b3a0a0;
    }

    .enveloppelabelnum {
        color: #a5c842;
    }

    .enveloppewarning {
        color: #f9690e;
    }

    .ui-widget.ui-widget-content{
        z-index: 2000000;
        max-height: 300px;
        overflow-y: auto;
        overflow-x: hidden;
    }
</style>
<?php
$editBtnLDepense = ($canEdit == true && ($mode == "w" || $mode == "fa")) ? " <a href='javascript:;' data-id='" . $answer["_id"] . "' data-collection='" . Form::ANSWER_COLLECTION . "' data-path='answers.aapStep1.depense.' class='dropdown-item add" . $kunik . "Depense'><i class='fa fa-plus'></i> Ajouter une ligne de depense</a>" : "";
$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='" . $parentForm["_id"] . "' data-collection='" . Form::COLLECTION . "' data-path='params' class='previewTpl edit" . $kunik . "Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

if ($answer)
{
    $isAapProject = $isAap;
    $contextId = $context["_id"];
    $contextType = $parentForm["parent"][(string)$context["_id"]]["type"];

    $copy = "aapStep1.depense";

    if (isset($parentForm["params"][$kunik]["budgetCopy"])) $copy = $parentForm["params"][$kunik]["budgetCopy"];
    else if (count(Yii::app()->session["budgetInputList"]) == 1) $copy = array_keys(Yii::app()->session["budgetInputList"]) [0];

    $copyT = explode(".", $copy);
    $copyF = $copyT[0];
    $budgetKey = $copyT[1];
    $answers = null;

    $contextIdType = $parentForm["parent"];
    $communityLinks = $parentCommunity;
    $organizations = $communityOrgaGroup;
    $citoyens = $communityOrgaGroup;

    $propertiesdepense = [
        "group" => [
            "placeholder" => "Groupé",
            "inputType" => "tags",
            "tagsList" => "budgetgroupeDepenseFi",
            "rules" => ["required" => true],
            "maximumSelectionLength" => 3
        ],
        "nature" => [
            "placeholder" => "Nature de l’action",
            "inputType" => "tags",
            "tagsList" => "budgetnatureDepenseFi",
            "rules" => ["required" => true],
            "maximumSelectionLength" => 3
        ],
        "poste" => [
            "inputType" => "text",
            "label" => "Poste de dépense",
            "placeholder" => "Poste de dépense",
            "rules" => ["required" => true]
        ],
        "description" => [
            "inputType" => "textarea",
            "label" => "Description",
            "placeholder" => "Description",
            "rules" => ["required" => false]
        ],
        "image" => [
            "label" => "Photo",
            "placeholder" => "",
            "inputType" => "uploader",
            "type" => "uploader",
            "docType" => "file"
         ]
    ];

    $paramsDataDepense = ["activeGroup" => false,"activeNature" => false,"group" => ["Feature", "Costum", "Chef de Projet", "Data", "Mantenance"], "nature" => ["investissement", "fonctionnement"], "amounts" => ["price" => "Montant"], "estimate" => true];

    if (isset($parentForm["params"]["budgetdepense"]["group"])) $paramsDataDepense["group"] = $parentForm["params"]["budgetdepense"]["group"];
    if (isset($parentForm["params"]["budgetdepense"]["nature"])) $paramsDataDepense["nature"] = $parentForm["params"]["budgetdepense"]["nature"];
    if (isset($parentForm["params"]["budgetdepense"]["amounts"])) $paramsDataDepense["amounts"] = $parentForm["params"]["budgetdepense"]["amounts"];
    if (isset($parentForm["params"]["budgetdepense"]["estimate"])) $paramsDataDepense["estimate"] = Answer::is_true($parentForm["params"]["budgetdepense"]["estimate"]);
    if (isset($parentForm["params"]["budgetdepense"]["activeGroup"])) $paramsDataDepense["activeGroup"] = Answer::is_true($parentForm["params"]["budgetdepense"]["activeGroup"]);
    if (isset($parentForm["params"]["budgetdepense"]["activeNature"])) $paramsDataDepense["activeNature"] = Answer::is_true($parentForm["params"]["budgetdepense"]["activeNature"]);

    foreach ($paramsDataDepense["amounts"] as $k => $l)
    {
        $propertiesdepense[$k] = ["inputType" => "number", "label" => $l, "propType" => "amount", "placeholder" => $l
        ];
    }

    if(!$paramsDataDepense["activeGroup"]) unset($propertiesdepense["group"]);
    if(!$paramsDataDepense["activeNature"]) unset($propertiesdepense["nature"]);

    $properties = ["poste" => ["inputType" => "text", "label" => "Contexte", "placeholder" => "Contexte", "size" => 4, "rules" => ["required" => true]], "financer" => ["placeholder" => "Financeur", "inputType" => "select", "list" => "financersList", "subLabel" => "Si financeur public, l’inviter dans la liste ci-dessous (au cas où il n’apparait pas demandez à votre référent territoire de le déclarer comme partenaire financeur", "size" => 6, ]];

    $paramsData = ["financerTypeList" => Ctenat::$financerTypeList, "limitRoles" => ["Financeur"], "limitTypes" => ["citoyens","organizations"], "openFinancing" => true];

    $paramsData["envelopelist"] = ["" => ""];

    if (isset($parentForm["params"][$kunik]["tpl"])) $paramsData["tpl"] = $parentForm["params"][$kunik]["tpl"];
    if (isset($parentForm["params"][$kunik]["budgetCopy"])) $paramsData["budgetCopy"] = $parentForm["params"][$kunik]["budgetCopy"];
    if (isset($parentForm["params"][$kunik]["financerTypeList"])) $paramsData["financerTypeList"] = $parentForm["params"][$kunik]["financerTypeList"];
    if (isset($parentForm["params"]["financeurLimitRoles"])) $paramsData["limitRoles"] = $parentForm["params"]["financeurLimitRoles"];
    if (isset($parentForm["params"]["limitTypes"])) $paramsData["limitTypes"] = $parentForm["params"]["limitTypes"];
    if (isset($parentForm["params"]["envelope"])) $paramsData["envelope"] = $parentForm["params"]["envelope"];
    if (isset($parentForm["params"]["envelopelist"][date("Y")])) $paramsData["envelopelist"] = $parentForm["params"]["envelopelist"][date("Y")];
    if (isset($parentForm["params"][$kunik]["openFinancing"])) $paramsData["openFinancing"] = $parentForm["params"][$kunik]["openFinancing"];

    $financement = [];
    if(isset($answer["answers"]["aapStep1"]["depense"]))
    foreach ($answer["answers"]["aapStep1"]["depense"] as $in => $item){
        $financement[$in]["price"] = $item["price"];
        if(isset($item["financer"])) {
            $financement[$in]["financer"] =  $item["financer"];
            $financement[$in]["amount"] = array_sum(array_column($item["financer"], "amount"));
            $financement[$in]["tofin"] = $financement[$in]["price"] - $financement[$in]["amount"];
        } else{
            $financement[$in]["financer"] =  [];
            $financement[$in]["amount"] = 0;
            $financement[$in]["tofin"] = $financement[$in]["price"] ;
        }
    }
    $lines = isset($answer["answers"]["aapStep1"]["depense"]) ? array_merge_recursive(...$financement) : 0;
    $totalFunded = isset($answer["answers"]["aapStep1"]["depense"]) ? array_sum(array_column($lines, "amount")) : 0;
    $total = isset($answer["answers"]["aapStep1"]["depense"]) ? array_sum(array_column($answer["answers"]["aapStep1"]["depense"], 'price')) : 0;

    $totalReste = $total - $totalFunded;

    if (isset($parentForm["mapping"]["depense"]))
    {
        $mapping = $parentForm["mapping"]["depense"];
        $mappingExplode = explode(".", $mapping);

        if (isset($answer[$mappingExplode[0]][$mappingExplode[1]][$mappingExplode[2]]))
        {
            $answers = $answer[$mappingExplode[0]][$mappingExplode[1]][$mappingExplode[2]];
        }
    }
    elseif ($isAap)
    {
        $configEl = $configForm;

        $mapping = $configEl["mapping"]["depense"];
        $mappingExplode = explode(".", $mapping);

        if (isset($answer[$mappingExplode[0]][$mappingExplode[1]][$mappingExplode[2]]))
        {
            $answers = $answer[$mappingExplode[0]][$mappingExplode[1]][$mappingExplode[2]];
        }
    }

    if ($wizard)
    {
        if ($budgetKey)
        {
            if (isset($answer["answers"][$copyF][$budgetKey]) && count($answer["answers"][$copyF][$budgetKey]) > 0) $answers = $answer["answers"][$copyF][$budgetKey];
        }
        else if (isset($answer["answers"][$copyF][$budgetKey]) && count($answer["answers"][$copyF][$budgetKey]) > 0) $answers = $answer["answers"][$copyF][$kunik];
    }
    else
    {
        if (!empty($budgetKey) && !empty($answer["answers"][$budgetKey])) $answers = $answer["answers"][$budgetKey];
        else if (isset($answers)) $answers = $answers;
    }


    $orgsfi = [];
    $orgsfitotal = [
            $parentForm["name"] => 0
    ];
    foreach ($organizations as $id => $or)
    {
        $roles = null;
        if (isset($communityLinks[$id]["roles"])) $roles = $communityLinks[$id]["roles"];

        if ($paramsData["limitRoles"] && !empty($roles))
        {
            foreach ($roles as $i => $r)
            {
                if (in_array($r, $paramsData["limitRoles"]))
                    $orgsfi[$id] = $or["name"];
            }
        }
    }


    $citoyens = $communityCitoyenGroup;

    $orgs = [];
    if(empty($paramsData["limitTypes"]) || sizeof($paramsData["limitTypes"]) == 0 ){
        $paramsData["limitTypes"] = ["organizations" , "citoyens"];
    }

    if (in_array("organizations" , $paramsData["limitTypes"])) {
        foreach ($organizations as $id => $or) {
            $roles = null;

            if (isset($communityLinks[$id]["roles"])) $roles = $communityLinks[$id]["roles"];

            if ($paramsData["limitRoles"] && !empty($roles)) {
                foreach ($roles as $i => $r) {
                    if (in_array($r, $paramsData["limitRoles"])) $orgs[$id] = $or["name"];
                }
            }
        }
    }

    if (in_array("citoyens" , $paramsData["limitTypes"])) {
        foreach ($citoyens as $id => $or) {
            $roles = null;

            if (isset($communityLinks[$id]["roles"])) $roles = $communityLinks[$id]["roles"];

            if ($paramsData["limitRoles"] && !empty($roles)) {
                foreach ($roles as $i => $r) {
                    if (in_array($r, $paramsData["limitRoles"])) $orgs[$id] = $or["name"];
                }
            }
        }
    }

    $listLabels = array_merge(Ctenat::$financerTypeList, $orgs);

    $amounts = (isset($parentForm["params"][$budgetKey]["amounts"])) ? $parentForm["params"][$budgetKey]["amounts"] : ["price" => "Montant Financé"];
    foreach ($amounts as $k => $l)
    {
        $properties[$k] = ["inputType" => "text", "label" => $l, "placeholder" => $l, "propType" => "amount"];
    }


if (isset($rendermodal) && $rendermodal)
{
    if (!empty($parentForm["config"])) {
        $nameconfig = $configForm["name"];
    }
    ?>
    <div id="sticky-anchor"></div>
    <div id="sticky">
        <div class="text-left col-md-12 col-sm-12 col-xs-12">
            <ul class="breadcrumb">
                <li ><a><?php echo $nameconfig ?></a></li>
                <li class="active"><a><?php echo $parentForm["name"] ?></a></li>
                <li class="active"><a>financement du proposition : <?php echo @$answer["answers"]["aapStep1"]["titre"] ?> </a></li>
            </ul>
        </div>

    </div>
    <div class="col-xs-12 col-lg-10 col-lg-offset-1 project-detail">
        <?php
        }
        ?>

        <?php
        $ignore = array('_file_', '_params_', '_obInitialLevel_' ,'ignore');
        $params = array_diff_key(get_defined_vars() + array_flip($ignore), array_flip($ignore));

        if (!empty($answers)) echo $this->renderPartial("survey.views.tpls.forms.ocecoform.financementFromBudgetTable", $params , true);
        else
        {
            if ($mode == "r" || $mode == "pdf")
            { ?>
                <label ><h4 style="color:<?php echo (!empty($titleColor) ? $titleColor : "black"); ?>"><?php echo $label . $editBtnLDepense; ?></h4></label>
                <?php echo $info ?>
                <?php
            }
            else
            {
                ?>
                <label ><h4 style="color:<?php echo (!empty($titleColor) ? $titleColor : "black"); ?>"><?php echo $label . $editQuestionBtn . $editParamsBtn . $editBtnLDepense; ?></h4></label>
                <?php echo $info;
                if (!isset($budgetKey)) echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> THIS FIELD HAS TO BE CONFIGURED FIRST " . $editParamsBtn . "</span>"; ?>
                <?php
            }

            echo "<div class='financementerror'> il n'y a pas encore de ligne depense </div>";
        }

        ?>

        <div id="container" class="new-form-financer" style="display: none">

            <main class="mainc">
                <div id="switchcom">
                    <div class="btn-group depenseoceco">
                        <button type="button" id="dcombtn" class="dcombtn switchcombtn btn active" data-switchactbtn="dcombtn" data-removebtn="icombtn" data-switchactdiv="dcomdiv" data-removediv="icomdiv" >Depuis la communauté </button>

                        <button type="button" id="icombtn" class="icombtn switchcombtn btn " data-switchactbtn="icombtn" data-removebtn="dcombtn" data-switchactdiv="icomdiv" data-removediv="dcomdiv">Inexistant dans la communauté</button>
                    </div>

                    <div style="clear: both;"></div>
                </div>

                <div class="dcomdiv">
                    <label class="label-esti">Financeur </label>
                </div>
                <div id="dcomdiv" class="dcomdiv switchcomdiv">
                    <div class="col-sm-12 col-md-9">
                        <input type="text" style="width: max-content"  id="financer" class="whatFinancers fi-financer todoinput">
                    </div>
                    <div class="col-sm-12 col-md-3">
                        <button class="btn btn-default sup-btn " style=" display: block;">Ajouter financeur</button>
                    </div>
                    <div style="clear: both;"></div>
                </div>

                <div class="icomdiv hide">
                    <!-- <label class=" spanfi-name">Nom</label><label class=" spanfi-mail">Email</label> -->
                    <label class="label-mobi">Nom</label>
                    <input id="" class="form-control todoinput fi-name" type="text" spellcheck="false" placeholder="Nom" onfocus="this.placeholder=''" onblur="this.placeholder='Nom'" />
                    <label class="label-mobi">Email</label>
                    <input id="new-credit-value" class="form-control fi-mail todoinput" type="text" spellcheck="false" placeholder="Email" onfocus="this.placeholder=''" onblur="this.placeholder='Email'" />
                    <div style="clear: both;"></div>

                    <!--  <label class=" spanfi-fond">Fonds, enveloppe ou budget mobilisé</label><label class=" spanfi-montant">Montant Financé</label><label class=" spanfi-btn"></label> -->

                </div>

                <div class="envdiv" >
                    <label class="label-esti enveloppelabel"><i class="fa fa-info-circle"></i> Enveloppe pour ce financeur : </label>
                    <label class="label-esti fi-Env enveloppelabelnum"> </label>
                </div>
                <div class="envdiv">
                    <label class="label-esti enveloppelabel"><i class="fa fa-info-circle"></i> Reste dans l'enveloppe pour ce financeur : </label>
                    <label class="label-esti fi-resteEnv enveloppelabelnum"> </label>
                </div>

                <div class="envdiverror" >
                    <label class=" enveloppewarning label-esti enveloppelabel"> <span class="restEnvWarning"> </span> </label>
                </div>

                <div class="">

                    <label class="label-mobi">Fonds, enveloppe ou budget mobilisé</label>
                    <input id="" class="form-control todoinput fi-fond" type="text" spellcheck="false" placeholder="Fonds, enveloppe ou budget mobilisé" onfocus="this.placeholder=''" onblur="this.placeholder='Fonds, enveloppe ou budget mobilisé'" />
                    <label class="label-mobi">Montant Financé</label>
                    <input max="<?php echo $totalReste ?>" id="new-credit-value" class="form-control fi-montant todoinput" type="number" spellcheck="false" placeholder="Montant Financé" onfocus="this.placeholder=''" onblur="this.placeholder='Montant Financé'" />

                    <div style="clear: both;"></div>

                </div>

                <div id="single-line"></div>

                <div>
                    <table class='table table-bordered table-hover  directoryTable oceco-styled-table'>
                        <tr>
                            <th class="thead-light">  </th>
                            <th class="thead-light"> Coût </th>
                            <th class="thead-light"> Reste à financer </th>
                            <th class="thead-light"> montant completé par la somme saisi </th>
                        </tr>
                        <?php foreach ($financement as $fffid => $fffdata)
                        { ?>
                            <tr data-id="<?=$fffid
                            ?>">
                                <td> <?=$fffid ?></td>
                                <td> <?= intval($fffdata["amount"]) ?></td>
                                <td> <?= intval($fffdata["tofin"]) ?></td>
                                <td> 0 </td>
                            </tr>
                            <?php
                        } ?>
                    </table>
                </div>

                <div id="single-line"></div>

                <ul id="financer-list">
                </ul>
            </main>
        </div>

        <div id="container" class="new-form-financeallline" style="display: none">

            <main class="mainc">
                <div id="switchcom">
                    <div class="btn-group depenseoceco flex-sm-wrap flex-xs-wrap">
                        <button type="button" id="dcombtn" class="dcombtn switchcombtn btn mb-2 active" data-switchactbtn="dcombtn" data-removebtn="icombtn" data-switchactdiv="dcomdiv" data-removediv="icomdiv" >Depuis la communauté </button>

                        <button type="button" id="icombtn" class="icombtn switchcombtn btn " data-switchactbtn="icombtn" data-removebtn="dcombtn" data-switchactdiv="icomdiv" data-removediv="dcomdiv">Inexistant dans la communauté</button>
                    </div>

                    <div style="clear: both;"></div>
                </div>

                <div class="dcomdiv">
                    <label class="label-esti">Financeur</label>
                </div>
                <div id="dcomdiv" class="dcomdiv switchcomdiv">
                    <div class="col-sm-12 col-md-9">
                        <input type="text" style="width: max-content"  id="financer" class="whatFinancers fi-financer todoinput">
                    </div>
                    <div class="col-sm-12 col-md-3">
                        <button class="btn btn-default sup-btn " style=" display: block;">Ajouter financeur</button>
                    </div>

                    <div style="clear: both;"></div>
                </div>

                <div class="icomdiv hide">
                    <!-- <label class=" spanfi-name">Nom</label><label class=" spanfi-mail">Email</label> -->
                    <label class="label-mobi">Nom</label>
                    <input id="" class="form-control todoinput fi-name" type="text" spellcheck="false" placeholder="Nom" onfocus="this.placeholder=''" onblur="this.placeholder='Nom'" />
                    <label class="label-mobi">Email</label>
                    <input id="new-credit-value" class="form-control fi-mail todoinput" type="text" spellcheck="false" placeholder="Email" onfocus="this.placeholder=''" onblur="this.placeholder='Email'" />
                    <div style="clear: both;"></div>

                    <!--  <label class=" spanfi-fond">Fonds, enveloppe ou budget mobilisé</label><label class=" spanfi-montant">Montant Financé</label><label class=" spanfi-btn"></label> -->

                </div>

                <div class="envdiv" >
                    <label class="label-esti enveloppelabel"><i class="fa fa-info-circle"></i> Enveloppe : </label>
                    <label class="label-esti fi-Env enveloppelabelnum"> </label>
                </div>
                <div class="envdiv">
                    <label class="label-esti enveloppelabel"><i class="fa fa-info-circle"></i> Reste dans l'enveloppe : </label>
                    <label class="label-esti fi-resteEnv enveloppelabelnum"> </label>
                </div>

                <div class="envdiverror" >
                    <label class=" enveloppewarning label-esti enveloppelabel"><span class="restEnvWarning">  </span> </label>
                </div>

                <div class="">

                    <label class="label-mobi">Fonds, enveloppe ou budget mobilisé</label>
                    <input id="" class="form-control todoinput fi-fond" type="text" spellcheck="false" placeholder="Fonds, enveloppe ou budget mobilisé" onfocus="this.placeholder=''" onblur="this.placeholder='Fonds, enveloppe ou budget mobilisé'" />
                    <label class="label-mobi">Montant Financé</label>
                    <input max="<?php //echo $totalReste ?>" id="new-credit-value" class="form-control fi-montant todoinput" type="number" spellcheck="false" placeholder="Montant Financé" onfocus="this.placeholder=''" onblur="this.placeholder='Montant Financé'" />

                    <div style="clear: both;"></div>

                </div>

                <div id="single-line"></div>

                <div class="table-responsive">
                    <label> <?php echo Yii::t("common", "Total remaining financing") ?> : <b> <?= $totalReste ?> </b></label>
                    <table class='table table-bordered table-hover  directoryTable oceco-styled-table'>
                        <tr>
                            <th class="thead-light">  </th>
                            <th class="thead-light"> Coût </th>
                            <th class="thead-light"> Reste à financer </th>
                            <th class="thead-light"> montant completé par la somme saisi </th>
                        </tr>
                        <?php foreach ($financement as $fffid => $fffdata)
                        { ?>
                            <tr data-id="<?=$fffid
                            ?>">
                                <td> <?=$fffid ?></td>
                                <td> <?=$fffdata["amount"] ?></td>
                                <td> <?=$fffdata["tofin"] ?></td>
                                <td> 0 </td>
                            </tr>
                            <?php
                        } ?>
                    </table>

                </div>

                <div id="single-line"></div>

                <ul id="financer-list">
                </ul>
            </main>
        </div>


        <?php
        if (isset($rendermodal) && $rendermodal)
        {
        ?>
    </div>
    <?php
}
    ?>

    <?php
    if (isset($parentForm["params"]["financement"]["tpl"]))
    {
        $this->renderPartial("costum.views." . $parentForm["params"]["financement"]["tpl"], ["totalFin" => $total, "totalBudg" => Yii::app()->session["totalBudget"]["totalBudget"]]);
    }
    ?>
    <?php if ($mode != "pdf")
{ ?>
<script>
    var formId = <?= json_encode((string)$parentForm["_id"]) ?>;
    function getFonds<?= $kunik ?>(key = null){
        var results = {};
        var params<?= $kunik ?> = {
            //"type" : "video",
            searchType : ["lists"],
            fields : ["name"],
            filters :{
                form : formId,
                name : key,
                type:"fonds"
            },
            notSourceKey : true
        };
        if(key == null){
            delete params<?= $kunik ?>.filters.name;
        }
        ajaxPost(
            null,
            baseUrl+"/" + moduleId + "/search/globalautocomplete",
            params<?= $kunik ?>,
            function(data){
                results = data.results;
                if(key != null && key != "" && Object.keys(results).length == 0){
                    $(document).find('.save-finance').on('click',function(){
                        insertFond<?= $kunik ?>(key,formId)
                    });

                }

            },null,null,{async:false}
        )
        return results;
    };
    function insertFond<?= $kunik ?>(name,formId){
        var tplCtx = {
            collection : "lists",
            path  : "allToRoot",
            value : {
                name : name,
                form : formId,
                type: "fonds"
            }
        }
        dataHelper.path2Value( tplCtx, function(prms) {
        });
    }
    function prepareFond<?= $kunik ?>(){
        var fonds = getFonds<?= $kunik ?>();
        var allFonds = [];
        $.each(fonds,function(k,v){
            allFonds.push(v.name);
        })
        allFonds = allFonds.filter(function( element ) {
            return element !== undefined;
        });
        return allFonds;
    }

    var options<?= $kunik ?> = {
        source: prepareFond<?= $kunik ?>(),
        select: function (event, ui) {
            mylog.log(ui,event,"ioio");
        },
        change : function(event, ui){
            //mylog.log(event.target.value,"ioio");
            getFonds<?= $kunik ?>(event.target.value);
        }
    };
    //$(".fi-fond").autocomplete(options<?= $kunik ?>);
</script>
    <script type="text/javascript">
        if(typeof coForm_FormWizardParams == "undefined") {
            var coForm_FormWizardParams = <?php echo json_encode($params); ?>;
        }

        var rendermodal = <?php echo (!empty($rendermodal) ? $rendermodal : "false") ?> ;

        var financementobj = <?php echo json_encode((isset($financement)) ? $financement : null); ?>;

        var orgsfitotal = {};

        var financementobjinit = <?php echo json_encode((isset($financement)) ? $financement : null); ?>;

        var statusproposition = <?php echo json_encode((isset($answers["status"])) ? $answers["status"] : []); ?>;

        var flistObj = {
            data : null,

            buildList : function (pos) {

                if (typeof financerList[pos] !== "undefined") {
                    $('.financerdialog #financer-list').html("");
                    $.each(financerList[pos], function(fid, fval){

                        var fline = {};

                        if ( typeof fval.line != "undefined" )
                            fline.line = fval.line;

                        if ( typeof fval.amount != "undefined" ){
                            fline.amount = fval.amount;
                        } else {
                            fline.amount = "(pas précisé)";
                        }

                        if ( typeof fval.name != "undefined" ){
                            fline.name = fval.name;
                        } else {
                            fline.name = "(pas précisé)";
                        }

                        if ( typeof fval.email != "undefined" ){
                            fline.email = fval.email;
                        } else {
                            fline.email = "(pas précisé)";
                        }


                        var str = '<li class="" data-pos='+pos+'>'+
                            '<div class="todo-content">'+
                            '<div class="td-title">'+
                            '<h5 class="mb-0">'+fline.name+'</h5>'+
                            '<div class="td-datetime">'+

                            '<div><i class="fa fa-money"></i><span class="td-datetimelabel">'+fline.amount+'</span></div>'+
                            '<div class="td-datetimelabel2"><i class="fa fa-calendar-o"></i><span class="td-datetimelabel">'+fline.line+'</span></div>'+
                            '<div class="td-datetimelabel2"><i class="fa fa-user"></i><span class="td-datetimelabel">'+fline.email+'</span></div>'+
                            '</div>'+
                            '</div>'+
                            '</div>'+
                            '<div class="todo-btn">'+
                            '<button class="td-datetimelabel2 edittodoItem actionbtntodoedit" data-idpos="'+pos+' " ><i class="fa fa-pencil"></i></button>'+
                            '<button class="td-datetimelabel2 deletetodoItem actionbtntododelete" data-idpos="'+pos+' " ><i class="fa fa-trash"></i></button>'+
                            '</div>'+
                            '</li>';

                        $('.financerdialog #financer-list').prepend(str);
                    })
                }

                $('.addtodoItem').off().click(function() {
                    listObj.addItem($(this).data("name"));
                });

                $('.edittodoItem').off().click(function() {
                    listObj.editItem($(this).data("name"), parseInt($(this).data("idpos")));
                });

                $('.deletetodoItem').off().click(function() {
                    listObj.deleteItem($(this).data("name"), parseInt($(this).data("idpos")));
                });

                $('.tododialog #modif-button').on('click', function(e){
                    e.stopImmediatePropagation();
                    var idpos = $(this).data('idpos');
                    var name = $(this).data('name');
                    var worker = [];

                    if ( $('.tododialog .taskname').val() )
                        tasks[name][idpos].task = $('.tododialog .taskname').val();

                    if ( $('.tododialog .credit').val() )
                        tasks[name][idpos].credits = $('.tododialog .credit').val();

                    if ( $('.tododialog .duedate').val() ){
                        var formattedDatetodo =  $('.tododialog .duedate').val().split("-");
                        var ytodo = formattedDatetodo[0];
                        var mtodo  =  formattedDatetodo[1];
                        var dtodo  = formattedDatetodo[2];

                        tasks[name][idpos].endDate = dtodo +'/'+mtodo +'/'+ytodo;
                    }

                    if ( $('.tododialog #new-worker-value').val() ){

                        tasks[name][idpos].contributors = {};
                        if ($('.tododialog #new-worker-value').val() ){

                            worker = $('.tododialog #new-worker-value').val();
                            $.each(worker, function(workerid, workerusername){
                                $.each(member, function(memberid, membervalue){
                                    if (membervalue["username"] == workerusername) {
                                        tasks[name][idpos].contributors[memberid] = {
                                            "type" : membervalue.collection,
                                        }
                                    }
                                })
                            })
                        }
                    }

                    //post edit
                    $('.tododialog .taskname').val("");
                    $('.tododialog .credit').val("");
                    $('.tododialog .duedate').val("");
                    $('.selectMultiple > div a').each(function(){
                        $(this).trigger('click');
                    });

                    $('.tododialog #modif-button').hide();
                    $('.tododialog #add-button').show();

                    listObj.buildList(name);
                });

                $('.tododialog .checkicon').on('click', function(e){
                    listObj.tooglecheckItem($(this).data("name"), parseInt($(this).data("idpos")));

                    $(this).toggleClass("fa-check-circle");
                    $(this).toggleClass("fa-circle-o");
                });
            },

            addItem : function (name) {

                if ( $('.tododialog .taskname').val() &&  $('.tododialog .taskname').val() != "" && $('.tododialog .taskname').val() != undefined) {
                    var today = new Date();
                    today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();

                    var worker = [];

                    var todo = {
                        createdAt : today,
                        userId : userId,
                        checked : "false",
                    };

                    if ( $('.tododialog .taskname').val() )
                        todo.task = $('.tododialog .taskname').val();

                    if ( $('.tododialog .credit').val() )
                        todo.credits = $('.tododialog .credit').val();

                    if ( $('.tododialog .duedate').val() ){
                        todo.endDate = $('.tododialog .duedate').val();
                    }

                    if ( $('.tododialog .duedate').val() ){
                        var formattedDatetodo =  $('.tododialog .duedate').val().split("-");
                        var ytodo = formattedDatetodo[0];
                        var mtodo  =  formattedDatetodo[1];
                        var dtodo  = formattedDatetodo[2];
                        todo.endDate = dtodo +'/'+mtodo +'/'+ytodo;
                    }

                    if ( $('.tododialog #new-worker-value').val() ){

                        todo.contributors = {};
                        if ($('.tododialog #new-worker-value').val() ){

                            worker = $('.tododialog #new-worker-value').val();
                            $.each(worker, function(workerid, workerusername){
                                $.each(member, function(memberid, membervalue){
                                    if (membervalue["username"] == workerusername) {
                                        todo.contributors[memberid] = {
                                            "type" : membervalue.collection
                                        }
                                    }
                                })
                            })
                        }
                    }

                    mylog.log("addItem",todo, tplCtx.editTaskpos);

                    if(todo.task =='') {
                        alert("Please write what you need to do!");
                    } else {
                        var str = '<li class="" data-pos="'+tasks[name].length+'">'+
                            '<div class="todo-content"><i class="fa fa-circle-o checkicon"></i>'+
                            '<div class="td-title">'+
                            '<h5 class="mb-0">'+todo.task+'</h5>'+
                            '<div class="td-datetime">'+

                            '<div><i class="fa fa-money"></i><span class="td-datetimelabel">'+todo.credits+'</span></div>'+
                            '<div class="td-datetimelabel2"><i class="fa fa-calendar-o"></i><span class="td-datetimelabel">'+todo.endDate+'</span></div>'+
                            '<div class="td-datetimelabel2"><i class="fa fa-user"></i><span class="td-datetimelabel">'+worker+'</span></div>'+
                            '</div>'+
                            '</div>'+
                            '</div>'+
                            '<div class="todo-btn">'+
                            '<button class="td-datetimelabel2 edittodoItem actionbtntodoedit" data-name="'+name+'" data-idpos="'+tasks[name].length+'" ><i class="fa fa-pencil"></i></button>'+
                            '<button class="td-datetimelabel2 deletetodoItem actionbtntododelete" data-name="'+name+'" data-idpos="'+tasks[name].length+'" ><i class="fa fa-trash"></i></button>'+
                            '</div>'+
                            '</li>';

                        $('.tododialog #todos-list').prepend(str);
                    }

                    // listObj.buildList(todo, tplCtx.editTaskpos);
                    // listObj.btnInit();

                    // if( notNull(tplCtx.form) && notNull(formInputs [tplCtx.form]) ){
                    if( typeof tasks[name] == "undefined")
                        tasks[name] = [];
                    tasks[name].push(todo);
                    // }
                    // else if( notNull(tplCtx.budgetpath) ) {
                    // 	if( typeof answerObj.answers[tplCtx.budgetpath][tplCtx.pos].todo == "undefined")
                    // 		answerObj.answers[tplCtx.form][tplCtx.budgetpath][tplCtx.pos].todo = [];
                    //   	answerObj.answers[tplCtx.budgetpath][tplCtx.pos].todo.push(todo);
                    // }

                    $('.tododialog .taskname', '.tododialog .credit', '.tododialog .duedate').val("");

                    // $(".bootbox #new-todo-item, .bootbox #new-todo-item-date, .bootbox #new-todo-item-price").val("");
                }
                $('.selectMultiple todoinput').removeClass("open");
            },

            deleteItem : function (name, pos) {
                bootbox.dialog({
                    title: trad.confirmdelete,
                    message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                    buttons: [
                        {
                            label: "Ok",
                            className: "btn btn-primary pull-left",
                            callback: function() {
                                $('li[data-pos="'+pos+'"]').fadeOut('slow', function() {
                                    tasks[name].splice(pos, 1);
                                    listObj.buildList(name);
                                });
                            }
                        },
                        {
                            label: "Annuler",
                            className: "btn btn-default pull-left",
                            callback: function() {}
                        }
                    ]
                });
            },

            editItem : function (name, idpos) {

                if ( typeof tasks[name][idpos].task !== "undefined" )
                    $('.tododialog .taskname').val(tasks[name][idpos].task);

                if ( typeof tasks[name][idpos].credits !== "undefined" ){
                    $('.tododialog .credit').val(tasks[name][idpos].credits);
                }else {
                    $('.tododialog .credit').val("");
                }

                if ( typeof tasks[name][idpos].endDate !== "undefined" ){
                    var formattedDatetodo =  tasks[name][idpos].endDate.split("/");
                    var dtodo = formattedDatetodo[0];
                    var mtodo  =  formattedDatetodo[1];
                    var ytodo  = formattedDatetodo[2];
                    $('.tododialog .duedate').val(ytodo +'-'+mtodo +'-'+dtodo);
                }else{
                    $('.tododialog .duedate').val("");
                }


                $('.selectMultiple > div a').each(function(){
                    $(this).trigger('click');
                });

                if ( typeof tasks[name][idpos].contributors !== "undefined" ){
                    var worker = [];

                    $.each(tasks[name][idpos].contributors, function(cbId, cbValue){
                        if (typeof member[cbId] !== "undefined") {
                            $('.selectMultiple ul li:contains('+member[cbId].name+')').trigger("click");
                        }
                    })

                    // $('.tododialog #new-worker-value').val(worker);
                }else{
                    $('.tododialog #new-worker-value').val([]);
                }

                $('.tododialog #modif-button').data("idpos", idpos);
                $('.tododialog #modif-button').data("name", name);

                $('.tododialog #modif-button').show();
                $('.tododialog #add-button').hide();

            },

            tooglecheckItem : function (name, idpos) {

                if ( typeof tasks[name][idpos].checked !== "undefined" && tasks[name][idpos].checked == "true" ){
                    tasks[name][idpos].checked = "false";

                } else {
                    tasks[name][idpos].checked = "true";
                    tasks[name][idpos].checkedUserId = userId;
                }

            },

            // editItem : function (e, item) {
            //   e.preventDefault();
            // 	tplCtx.editTaskpos = $(item).parent().parent().data("pos");
            //   $(".bootbox #new-todo-item").val( $(item).parent().parent().find(".liText").text() );
            //   $(".bootbox #new-todo-item-date").val( $(item).parent().parent().data("when") );
            //   $(".bootbox #new-todo-item-price").val( $(item).parent().parent().data("price") );
            //   $(".bootbox #new-todo-item-who option").prop("selected",false)
            //   if($(item).parent().parent().data("who").indexOf(",")){
            //   	whosT = $(item).parent().parent().data("who").split(",");
            //   	$.each(whosT,function(i,u){
            //   		$(".bootbox #new-todo-item-who option[value='"+u+"']").prop("selected",true)
            //   	})
            //   }else
            // 	  $(".bootbox #new-todo-item-who").val( $(item).parent().parent().data("who") );
            // },

            btnInit : function() {
                // $(".bootbox #todo-list").on('click', '.todo-item-delete', function(e){
                // 	var item = this;
                // 	listObj.deleteItem(e, item)
                // })
                // $(".bootbox #todo-list").on('click', '.todo-item-edit', function(e){
                // 	var item = this;
                // 	listObj.editItem(e, item)
                // })
                // $(".bootbox .todo-item-done").off().on('click', listObj.completeItem);
                // // $(".bootbox .liTodo").on('click', function(e){
                // // 	var item = this;
                // // 	editItem(e, item)
                // // })
            }
        }

        function formatState (state) {
            //if (!state.id) { return state.text; }
            var $state = $(
            '<span><img src="' + baseurl+ state.image + '" class="img-flag" />fff ' + state.text + '</span>'
                );
            return $state;
        };

        if(typeof dyFObj.elementObjParams == "undefined")
            dyFObj.elementObjParams = {};

        dyFObj.elementObjParams.budgetInputList = <?php echo json_encode(Yii::app()->session["budgetInputList"]); ?>;
        dyFObj.elementObjParams.financerTypeList = <?php echo json_encode(Ctenat::$financerTypeList); ?>;
        dyFObj.elementObjParams.financersList = <?php echo json_encode($orgs); ?>;


        var <?php echo $kunik ?>Data = <?php echo json_encode((isset($answer["answers"][$budgetKey])) ? $answer["answers"][$budgetKey] : null); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsDataDepense = <?php echo json_encode($paramsDataDepense); ?>;
        sectionDyf.<?php echo $kunik ?>envelopeData = <?php echo json_encode($paramsData["envelopelist"]); ?>;

        var FiData = <?php echo json_encode((isset($answer["answers"]["aapStep1"]["depense"])) ? $answer["answers"]["aapStep1"]["depense"] : null); ?>;

        if (FiData != null) {
            if ($.isPlainObject(FiData)){
                FiData = Object.values(FiData);
            }
            var financerList = FiData.reduce(function(accumulator, item){
                if (item != null && typeof item["financer"] != "undefined") {
                    accumulator.push(item["financer"]);
                    return accumulator;
                } else {
                    accumulator.push([]);
                    return accumulator;
                }
            },[]);
        } else {
            var financerList = {};
        }

        var standedAl = <?php echo (!empty($standAlone) ? "true" : "false") ?>;

        var cntxtId = "<?php echo $contextId; ?>";
        var cntxtType = "<?php echo $contextType; ?>";

        var supelementparents = {
            afterSave : function(data) {
                var sendDataM = {
                    parentId : cntxtId,
                    parentType : cntxtType,
                    listInvite : {
                        organizations : {}
                    }
                };
                sendDataM.listInvite.organizations[data.id] = {};
                if(typeof data.name != "undefined"){
                    sendDataM.listInvite.organizations[data.id]["name"] = data.name;
                } else {
                    sendDataM.listInvite.organizations[data.id]["name"] = data.map.name;
                }
                sendDataM.listInvite.organizations[data.id]["roles"] = ["Financeur"];

                ajaxPost("",
                    baseUrl+"/co2/link/multiconnect",
                    sendDataM,
                    function(data) {
                        dyFObj.closeForm();
                        //reloadInput("<?php //echo $key

                        ?>//", "<?php //echo @$form["id"]

                        ?>//");
                        // if(typeof newReloadStepValidationInputGlobal != "undefined") {
                        //     newReloadStepValidationInputGlobal({
                        //         inputKey : <?php echo json_encode($budgetKey) ?>,
                        //         inputType : "tpls.forms.ocecoform.financementFromBudget"
                        //     })
                        // }
                        if (rendermodal){
                            reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                        }else{
                            if (typeof inputsList != "undefined"){
                                $.each(inputsList , function (inn, vall) {
                                    $.each(vall , function (inn2, vall2) {
                                        if (typeof vall2.type != "undefined"
                                            && vall2.type.split('.')[vall2.type.split('.').length-1] != "undefined"
                                            && ( vall2.type.split('.')[vall2.type.split('.').length-1] == "budget" ||
                                                vall2.type.split('.')[vall2.type.split('.').length-1] == "financementFromBudget" ||
                                                vall2.type.split('.')[vall2.type.split('.').length-1] == "suiviFromBudget"
                                            )
                                        ){
                                            reloadInput(inn2, inn);
                                        }
                                    });
                                });
                            }
                        }
                    },
                    null,
                    "json"
                );
            }
        };

        if (typeof costum != "undefined" && costum != null) {
            costum.searchExist = function (type, id, name, slug, email) {
                mylog.log("costum searchExist : " + type + ", " + id + ", " + name + ", " + slug + ", " + email);
                var data = {
                    type: type,
                    id: id,
                    name: name,
                    map: {slug: slug}
                }
                supelementparents.afterSave(data);
            };
        }

        var <?php echo $kunik ?>DataDepense = <?php echo json_encode((isset($answers)) ? $answers : null); ?>;

        var projectIdDepense = "<?php echo (!empty($answer["project"]["id"]) ? $answer["project"]["id"] : "null"); ?>";

        var budgetgroupeDepenseFi = <?php echo (!empty($paramsDataDepense["group"]) ? json_encode($paramsDataDepense["group"]) : "[]"); ?>;
        var budgetnatureDepenseFi = <?php echo (!empty($paramsDataDepense["nature"]) ? json_encode($paramsDataDepense["nature"]) : "[]"); ?>;

        var parentId = "<?php echo (string)$answer["form"]; ?>";

        if(!notNull(answerObj)){
            var answerObj = <?php echo (!empty($answer) ? json_encode($answer) : "{}"); ?>;
        }

        if(typeof costum != "undefined" && costum != null){
            costum["budgetgroupeDepenseFi"] = budgetgroupeDepenseFi;
            costum["budgetnatureDepenseFi"] = budgetnatureDepenseFi;
        }

        if(typeof fincmntObj == "undefined") {
            var fincmntObj = {
                commonModalEvent : function (financors) {
                    var financorsData = [
                        {
                            id : coForm_FormWizardParams.context._id.$id,
                            text : coForm_FormWizardParams.context.name,
                            image : typeof coForm_FormWizardParams.context.profilThumbImageUrl != "undefined" ? coForm_FormWizardParams.context.profilThumbImageUrl : ""
                        },
                        ...Object.values(financors).map(function(f) {
                            return {
                                id : typeof f._id.$id != "undefined" ? f._id.$id : f.name,
                                text : f.name,
                                image : typeof f.profilThumbImageUrl != "undefined" ? f.profilThumbImageUrl : ""
                            };
                        })];

                    $('.financerdialog .whatFinancers').select2({
                        data : financorsData,
                        width: '100%',
                        templateResult: function (state) {
                            //if (!state.id) { return state.text; }
                            mylog.log("aaaaaaa", state);
                            var $state = $(
                            '<span><img src="' + baseurl+ state.image + '" class="img-flag" />fff ' + state.text + '</span>'
                                );
                            return $state;
                        },
                        templateSelection: function (state) {
                            //if (!state.id) { return state.text; }
                            var $state = $(
                            '<span><img src="' + baseurl+ state.image + '" class="img-flag" />fff ' + state.text + '</span>'
                                );
                            return $state;
                        }
                    }).trigger("change");

                    $('.financerdialog .whatFinancers').select2('data', { id: coForm_FormWizardParams.context._id.$id , text: coForm_FormWizardParams.context.name});

                    $(".fi-fond").autocomplete(options<?= $kunik ?>);
                    $(".envdiv").hide();

                    $(".sup-btn").off().on("click",function() {
                        prioModal.modal("hide");
                        dyFObj.openForm("organization",null,null,null,
                            {
                                afterSave : function(data) {
                                    var sendDataM = {
                                        parentId : cntxtId,
                                        parentType : cntxtType,
                                        listInvite : {
                                            organizations : {}
                                        }
                                    };
                                    sendDataM.listInvite.organizations[data.id] = {};
                                    sendDataM.listInvite.organizations[data.id]["name"] = data.map.name;
                                    sendDataM.listInvite.organizations[data.id]["roles"] = ["Financeur"];

                                    ajaxPost("",
                                    baseUrl+"/co2/link/multiconnect",
                                        sendDataM,
                                        function(data) {
                                            dyFObj.closeForm();
                                            //reloadInput("<?php //echo $key

                                            ?>//", "<?php //echo @$form["id"]

                                            ?>//");
                                            // if(typeof newReloadStepValidationInputGlobal != "undefined") {
                                            //     newReloadStepValidationInputGlobal({
                                            //         inputKey : <?php echo json_encode($key) ?>,
                                            //         inputType : "tpls.forms.ocecoform.financementFromBudget"
                                            //     })
                                            // }
                                            if (!standedAl){
                                                if (rendermodal){
                                                    reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                                }else{
                                                    if (typeof inputsList != "undefined"){
                                                        $.each(inputsList , function (inn, vall) {
                                                            $.each(vall , function (inn2, vall2) {
                                                                if (typeof vall2.type != "undefined"
                                                                    && vall2.type.split('.')[vall2.type.split('.').length-1] != "undefined"
                                                                    && ( vall2.type.split('.')[vall2.type.split('.').length-1] == "budget" ||
                                                                        vall2.type.split('.')[vall2.type.split('.').length-1] == "financementFromBudget" ||
                                                                        vall2.type.split('.')[vall2.type.split('.').length-1] == "suiviFromBudget"
                                                                    )
                                                                ){
                                                                    reloadInput(inn2, inn);
                                                                }
                                                            });
                                                        });
                                                    }
                                                }
                                            }else {

                                            }
                                        },
                                        null,
                                        "json"
                                        );
                                }
                            },{
                            type : "bootbox",
                            notCloseOpenModal : true,
                        }
                            );
                    });

                    $(".switchcombtn").on("click",function() {
                        $(".financerdialog").find("."+$(this).data("switchactbtn")).addClass("active");
                        $(".financerdialog").find("."+$(this).data("removebtn")).removeClass("active");
                        $(".financerdialog").find("."+$(this).data("switchactdiv")).removeClass("hide");
                        $(".financerdialog").find("."+$(this).data("removediv")).addClass("hide");
                    });

                    $(".financerdialog .fi-name.todoinput").keyup( function( event ) {
                        var thisbtn = $(this);
                        if (sectionDyf.<?php echo $kunik ?>ParamsData.envelope == true){

                            if((typeof orgsfitotal[thisbtn.val().trim()] != "undefined" &&
                                sectionDyf.<?php echo $kunik ?>envelopeData[slugify(thisbtn.val().trim())] != "undefined")){
                                if(notEmpty(sectionDyf.<?php echo $kunik ?>envelopeData[slugify(thisbtn.val().trim())]) && sectionDyf.<?php echo $kunik ?>envelopeData[slugify(thisbtn.val().trim())] != 0 && sectionDyf.<?php echo $kunik ?>envelopeData[slugify(thisbtn.val().trim())] != "") {
                                    $(".envdiv").show();
                                }
                                $(".financerdialog .fi-Env").html(sectionDyf.<?php echo $kunik ?>envelopeData[slugify(thisbtn.val())].toString() );
                                $(".financerdialog .fi-resteEnv").html((sectionDyf.<?php echo $kunik ?>envelopeData[slugify(thisbtn.val())] - (typeof orgsfitotal[thisbtn.val()] != "undefiend" ? orgsfitotal[thisbtn.val()] : 0)).toString());
                                $(".financerdialog .fi-resteEnv").data("value" , (sectionDyf.<?php echo $kunik ?>envelopeData[slugify(thisbtn.val())] - (typeof orgsfitotal[thisbtn.val()] != "undefiend" ? orgsfitotal[thisbtn.val()] : 0)) );


                            }else{
                                $(".envdiv").hide();
                            }

                        }
                    });

                    if (sectionDyf.<?php echo $kunik ?>ParamsData.envelope == true){

                        if((typeof orgsfitotal[coForm_FormWizardParams.context.name.trim()] != "undefined" &&
                            sectionDyf.<?php echo $kunik ?>envelopeData[slugify(coForm_FormWizardParams.context.name)] != "undefined")){
                            if(notEmpty(sectionDyf.<?php echo $kunik ?>envelopeData[slugify(coForm_FormWizardParams.context.name.trim())]) && sectionDyf.<?php echo $kunik ?>envelopeData[slugify(coForm_FormWizardParams.context.name.trim())] != 0 && sectionDyf.<?php echo $kunik ?>envelopeData[slugify(coForm_FormWizardParams.context.name.trim())] != "") {
                                $(".envdiv").show();
                            }
                            $(".financerdialog .fi-Env").html(sectionDyf.<?php echo $kunik ?>envelopeData[slugify(coForm_FormWizardParams.context.name)].toString() );
                            $(".financerdialog .fi-resteEnv").html((sectionDyf.<?php echo $kunik ?>envelopeData[slugify(coForm_FormWizardParams.context.name)] - (typeof orgsfitotal[coForm_FormWizardParams.context.name] != "undefiend" ? orgsfitotal[coForm_FormWizardParams.context.name] : 0)).toString());
                            $(".financerdialog .fi-resteEnv").data("value" , (sectionDyf.<?php echo $kunik ?>envelopeData[slugify(coForm_FormWizardParams.context.name)] - (typeof orgsfitotal[thisbtn.val()] != "undefiend" ? orgsfitotal[coForm_FormWizardParams.context.namecoForm_FormWizardParams.context.name] : 0)) );


                        }else{
                            $(".envdiv").hide();
                        }

                    }

                    $('.financerdialog .whatFinancers').on('change', function (e) {
                        var thisbtn = $(this);
                        if (sectionDyf.<?php echo $kunik ?>ParamsData.envelope == true){
                            if(notEmpty($('.financerdialog .whatFinancers').select2('data')) && notEmpty(sectionDyf.<?php echo $kunik ?>envelopeData[slugify($('.financerdialog .whatFinancers').select2('data').text.trim())]) && sectionDyf.<?php echo $kunik ?>envelopeData[slugify($('.financerdialog .whatFinancers').select2('data').text.trim())] != 0 && sectionDyf.<?php echo $kunik ?>envelopeData[slugify($('.financerdialog .whatFinancers').select2('data').text.trim())] != "") {
                                $(".envdiv").show();
                            }else{
                                $(".envdiv").hide();
                            }
                            $(".financerdialog .fi-Env").html(sectionDyf.<?php echo $kunik ?>envelopeData[slugify($('.financerdialog .whatFinancers').select2('data').text.trim())] );
                            $(".financerdialog .fi-resteEnv").html(
                                (
                                    sectionDyf.<?php echo $kunik ?>envelopeData[slugify($('.financerdialog .whatFinancers').select2('data').text.trim())] - ( typeof orgsfitotal[$('.financerdialog .whatFinancers').select2('data').text.trim()] != "undefined" ? orgsfitotal[$('.financerdialog .whatFinancers').select2('data').text.trim()] : 0)
                                ).toString()
                            );
                            $(".financerdialog .fi-resteEnv").data("value" , (sectionDyf.<?php echo $kunik ?>envelopeData[slugify($('.financerdialog .whatFinancers').select2('data').text.trim())] - ( typeof orgsfitotal[$('.financerdialog .whatFinancers').select2('data').text.trim()] != "undefined" ? orgsfitotal[$('.financerdialog .whatFinancers').select2('data').text.trim()] : 0)) );

                        }
                        $('.financerdialog .fi-name').val('');
                        $('.financerdialog .fi-mail').val('');
                    });

                    $('.financerdialog .fi-name').on('blur', function (e) {
                        $('.financerdialog .whatFinancers').select2('data' , null);
                        $(".envdiv").hide();
                    });
                    $('.financerdialog .fi-mail').on('blur', function (e) {
                        $('.financerdialog .whatFinancers').select2('data' , null);
                        $(".envdiv").hide();
                    });

                    $(".icombtn").on('click',function (e) {
                        $('.financerdialog .whatFinancers').select2('data' , null);
                        $(".envdiv").hide();
                    });

                    $(".dcombtn").on('click',function (e) {
                        $('.financerdialog .whatFinancers').select2('data', { id: coForm_FormWizardParams.context._id.$id , text: coForm_FormWizardParams.context.name});
                    });
                }
            }
        }

        fincmntObj.bindModalEvent = function(financors, isEdited = false, montantForEdit = 0) {

            var unlimitedFinancing = <?= isset($parentForm["params"]["unlimitedFinancing"]) ?  "true" : "false" ?>;
            $(".financerdialog .fi-montant.todoinput").on("keyup" , function( event ) {
                if($(".financerdialog .fi-montant.todoinput").val() > 0) {
                    $('.save-finance').removeAttr('disabled');

                    if(!unlimitedFinancing) {
                        if (parseInt($(".financerdialog .fi-montant.todoinput").val()) > (parseInt($(".financerdialog .fi-montant.todoinput").attr("max")))) {
                            $(".financerdialog .fi-montant.todoinput").val($(".financerdialog .fi-montant.todoinput").attr("max"));
                        }
                    }

                    if (parseInt($(".financerdialog .fi-montant.todoinput").val()) > parseInt($(".financerdialog .fi-resteEnv").data("value"))){
                        //$(".financerdialog .fi-montant.todoinput").val($(".financerdialog .fi-resteEnv").data("value"));
                        $(".restEnvWarning").html("<i class='fa fa-info-circle'></i> Le montant à financer depasse la valeur restant dans l'enveloppe du financeur")
                    }else{
                        $(".restEnvWarning").html("");
                    }

                    var reste = parseInt($(".financerdialog .fi-montant.todoinput").val());
                    var signlefipos = 0;
                    $.each(financementobj, function (index, val) {
                        if (val.tofin > 0 && signlefipos == tplCtx.pos){
                            if (val.tofin < reste){
                                financementobj[index]["val"] = val.tofin;
                                $(`.financerdialog tr[data-id="${ index }"] td:nth-child(4)`).html(val.tofin);
                                reste -= val.tofin;
                            } else {
                                financementobj[index]["val"] = reste;
                                $(`.financerdialog tr[data-id="${ index }"] td:nth-child(4)`).html(reste);
                                reste = 0;
                            }
                        }
                        signlefipos++;
                    });
                }
            });

            var signlefipos2 = 0;
            $.each(financementobj, function (index, val) {
                if (signlefipos2 != tplCtx.pos){
                    $(`.financerdialog tr[data-id="${ index }"]`).addClass("hide");
                }else{
                    $(".financerdialog .fi-montant.todoinput").attr("max", parseInt($(`.financerdialog tr[data-id="${ index }"] td:nth-child(3)`).html()) + montantForEdit);
                    if(isEdited) {
                        $(`.financerdialog tr[data-id="${ index }"] td:nth-child(4)`).html(montantForEdit);
                    }
                }
                signlefipos2++;
            });

            fincmntObj.commonModalEvent(financors);
        }

        $(document).ready(function() {
            ajaxPost("", baseUrl + '/survey/answer/gettotalfinancement/form/'+ (typeof coForm_FormWizardParams != "undefined" && coForm_FormWizardParams?.parentForm?._id?.$id ? coForm_FormWizardParams.parentForm._id.$id : formId),
                {

                },
                function (data) {
                    orgsfitotal = data;
                }, null,  "json" );

            sectionDyf.<?php echo $kunik ?>Depense = {
                jsonSchema : {
                    title : "Budget prévisionnel",
                    icon : "money",
                    text : "Décrire ici les principaux postes de dépenses : à quoi correspondent les coûts ? <br/>Préciser ce qui relève des dépenses de fonctionnement et des dépenses d’investissement.<br/>Les coûts doivent être en <b>hors taxe</b>.",
                    properties : <?php echo json_encode($propertiesdepense); ?>,
                    beforeBuild : function(){
                        //uploadObj.set("answers",answerObj._id.$id, "file", null, null, "/subKey/"+tplCtx.path);
                    },
                    save : function () {
                        var depensename = "";
                        var today = new Date();
                        tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };

                        tplCtx.setType = [
                            {
                                "path": "price",
                                "type": "int"
                            }
                        ];

                        $.each( sectionDyf.<?php echo $kunik ?>Depense.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                            if(k == "poste"){
                                depensename = tplCtx.value[k];
                            }
                        });

                        if (typeof isInterfaceAdmin != "undefined" && isInterfaceAdmin && typeof tplCtx.value.group != "undefined") {
                            tplCtx2 = {
                                id: parentId,
                                collection: "forms",
                                path: "params.<?php echo $kunik ?>.group"
                            }

                            tplCtx2.value = sectionDyf.<?php echo $kunik ?>ParamsDataDepense.group;

                            $.each(tplCtx.value.group.split(",") , function (ind , val) {
                                if(jQuery.inArray(val, tplCtx2.value) == -1) {

                                    tplCtx2.value.push(val);

                                }
                            });

                            dataHelper.path2Value(tplCtx2, function (params) {
                            });
                        }

                        if (typeof isInterfaceAdmin != "undefined" && isInterfaceAdmin && typeof tplCtx.value.nature != "undefined") {
                            tplCtx3 = {
                                id: parentId,
                                collection: "forms",
                                path: "params.<?php echo $kunik ?>.nature"
                            }

                            tplCtx3.value = sectionDyf.<?php echo $kunik ?>ParamsDataDepense.nature;

                            $.each(tplCtx.value.nature.split(",") , function (ind , val) {
                                if(jQuery.inArray(val, tplCtx3.value) == -1) {

                                    tplCtx3.value.push(val);

                                }
                            });

                            dataHelper.path2Value(tplCtx3, function (params) {
                            });
                        }

                        var connectedData = ["financer","todo","payed","progress","worker","validFinal","votes",];
                        $.each( connectedData , function(k,attr) {
                            if(notNull("answerObj."+tplCtx.path+"."+attr))
                                tplCtx.value[attr] = jsonHelper.getValueByPath(answerObj,tplCtx.path+"."+attr);
                        });

                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined"){}
                        //projectstate.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                delete tplCtx.arrayForm;
                                var answerId = "<?php echo (string)$answer["_id"]; ?>";

                                ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/newdepense/answerid/' + answerId,
                                    {
                                        name : depensename,
                                        url : window.location.href
                                    },
                                    function (data) {

                                    }, "html");

                                ajaxPost("", baseUrl + "/co2/aap/commonaap/action/notifyAddDepenseLine", {
                                        answerId:answerId,
                                        depense: tplCtx.value.poste +": "+  tplCtx.value.price + " Euro"
                                }, function(data){}, function(error){}, "json")

                                if (rendermodal){
                                    var ansid = "<?php echo (string)$answer["_id"] ?>";
                                    var ansform = "<?php echo (isset($ansform) ? $ansform : '') ?>";
                                    var ansformstep = "aapStep4";
                                    var ansinputid = "suivredepense";
                                    smallMenu.openAjaxHTML(baseUrl+'/survey/answer/getinput/id/'+ansid+'/form/'+ansform+'/mode/w/formstep/'+ansformstep+'/inputid/'+ansinputid);
                                }else {
                                    dyFObj.closeForm();
                                }

                                var tplCtx2 = {};

                                var tplCtxproject = {};

                                if (projectIdDepense != null && projectIdDepense != "null") {

                                    if (notNull(tplCtx.actionid)) {
                                        tplCtxproject.id = tplCtx.actionid;
                                        tplCtxproject.collection = "actions";
                                        tplCtxproject.path = "name"
                                        tplCtxproject.value = tplCtx.value.poste;

                                        dataHelper.path2Value(tplCtxproject, function () {
                                        });
                                    }

                                    var answerId = "<?php echo (string)$answer["_id"]; ?>";
                                    var cntxtId = "<?php echo $contextId; ?>";
                                    var cntxtType = "<?php echo $contextType; ?>";

                                    ajaxPost("", baseUrl + '/survey/form/generateproject/answerId/' + answerId + '/parentId/' + cntxtId + '/parentType/' + cntxtType,
                                        null,
                                        function (data) {
                                        });
                                }

                                if (rendermodal){
                                    reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                }else{
                                    if (typeof inputsList != "undefined"){
                                        $.each(inputsList , function (inn, vall) {
                                            $.each(vall , function (inn2, vall2) {
                                                if (typeof vall2.type != "undefined"
                                                    && vall2.type.split('.')[vall2.type.split('.').length-1] != "undefined"
                                                    && ( vall2.type.split('.')[vall2.type.split('.').length-1] == "budget" ||
                                                        vall2.type.split('.')[vall2.type.split('.').length-1] == "financementFromBudget" ||
                                                        vall2.type.split('.')[vall2.type.split('.').length-1] == "suiviFromBudget"
                                                    )
                                                ){
                                                    reloadInput(inn2, inn);
                                                }
                                            });
                                        });
                                    }
                                }
                                if(typeof newReloadStepValidationInputGlobal != "undefined") {
                                    newReloadStepValidationInputGlobal({
                                        inputKey : <?php echo json_encode($key) ?>,
                                        inputType : "tpls.forms.ocecoform.financementFromBudget"
                                    })
                                }
                            } );

                            delete tplCtx.setType;
                        }
                    },
                    beforeBuild : function(data){
                        if(typeof costum != "undefined" && costum != null){
                            costum["budgetgroupeDepenseFi"] = budgetgroupeDepenseFi;
                            costum["budgetnatureDepenseFi"] = budgetnatureDepenseFi;
                        }
                    },
                }
            };

            var max<?php echo $kunik ?>Depense = 0;
            var nextValuekey<?php echo $kunik ?>Depense = 0;

            if(notNull(<?php echo $kunik ?>DataDepense)){
                $.each(<?php echo $kunik ?>DataDepense, function(ind, val){
                    if(parseInt(ind) > max<?php echo $kunik ?>Depense){
                        max<?php echo $kunik ?>Depense = parseInt(ind);
                    }
                });

                nextValuekey<?php echo $kunik ?>Depense = max<?php echo $kunik ?>Depense + 1;

            }

            $( ".add<?php echo $kunik ?>Depense" ).off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = "answers.aapStep1.depense";
                tplCtx.arrayForm = true;
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Depense,null,null,null,null,{
                        type : "bootbox",
                        notCloseOpenModal : true,
                    } );
            });

            sectionDyf.<?php echo $kunik ?> = {
                "jsonSchema" : {
                    "title" : "Plan de Financement",
                    "icon" : "money",
                    "text" : "Décrire ici les financements mobilisés ou à mobiliser. Les coûts doivent être en <b>hors taxe</b>.",
                    "properties" : <?php echo json_encode($properties); ?>,
                    save : function () {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                        });
                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, closePrioModalRel );
                        }

                    }
                }
            };

            sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema" : {
                    "title" : "<?php echo $label ?> config",
                    "icon" : "cog",
                    "properties" : {
                        limitRoles : {
                            inputType : "array",
                            label : "Liste des roles financeurs",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsData.limitRoles
                        },
                        limitTypes : {
                            inputType : "array",
                            label : "Liste des elements financeurs",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsData.limitTypes
                        },
                        chart : {
                            inputType : "checkboxSimple",
                            label : "Graphique",
                            subLabel : "",
                            params : {
                                onText : "Oui",
                                offText : "Non",
                                onLabel : "Oui",
                                offLabel : "Non",
                                labelText : "Enveloppe"
                            },
                            checked : sectionDyf.<?php echo $kunik ?>ParamsData.chart
                        },
                        envelope : {
                            inputType : "checkboxSimple",
                            label : "Enveloppe",
                            subLabel : "",
                            params : {
                                onText : "Oui",
                                offText : "Non",
                                onLabel : "Oui",
                                offLabel : "Non",
                                labelText : "Enveloppe"
                            },
                            checked : sectionDyf.<?php echo $kunik ?>ParamsData.envelope
                        },
                        envelopeyear : {
                            inputType : "array",
                            label : "Liste des années pris en compte par l'enveloppe <small style='color: red'>(si enveloppe activé)</small>",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsData.envelopeyear
                        },

                    },
                    save : function () {

                        var ttpath = tplCtx.path;
                        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                            tplCtx.value = {};

                            if (k == "limitRoles") {
                                tplCtx.value = getArray('.' + k + val.inputType);
                                tplCtx.path = ttpath + ".financerLimitRoles"
                                if (typeof tplCtx.value == "undefined")
                                    toastr.error('value cannot be empty!');
                                else {
                                    dataHelper.path2Value(tplCtx, function (params) {

                                    });
                                }

                            }else if(k == "envelope"){
                                tplCtx.value = $("#" + k).val();
                                tplCtx.path = ttpath + ".envelope"
                                if (typeof tplCtx.value == "undefined")
                                    toastr.error('value cannot be empty!');
                                else {
                                    dataHelper.path2Value(tplCtx, function (params) {});
                                }

                            }else if(k == "limitTypes"){
                                tplCtx.value = getArray('.' + k + val.inputType);
                                tplCtx.path = ttpath + ".limitTypes"
                                if (typeof tplCtx.value == "undefined")
                                    toastr.error('value cannot be empty!');
                                else {
                                    dataHelper.path2Value(tplCtx, function (params) {

                                    });
                                }

                            }

                        });

                        dyFObj.closeForm();

                        if (rendermodal) {
                            reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                        } else {
                            if (typeof inputsList != "undefined") {
                                $.each(inputsList, function (inn, vall) {
                                    $.each(vall, function (inn2, vall2) {
                                        if (typeof vall2.type != "undefined"
                                            && vall2.type.split('.')[vall2.type.split('.').length - 1] != "undefined"
                                            && (vall2.type.split('.')[vall2.type.split('.').length - 1] == "budget" ||
                                                vall2.type.split('.')[vall2.type.split('.').length - 1] == "financementFromBudget" ||
                                                vall2.type.split('.')[vall2.type.split('.').length - 1] == "suiviFromBudget"
                                            )
                                        ) {
                                            reloadInput(inn2, inn);
                                        }
                                    });
                                });
                            }
                        }

                        mylog.log("save tplCtx",tplCtx);


                    }
                }
            };

            sectionDyf.<?php echo $kunik ?>envelopeParams = {
                "jsonSchema" : {
                    "title" : "Enveloppe",
                    "icon" : "envelope",
                    "properties" : {

                    },
                    afterBuild : function(data){
                        //$('.envelopelistproperties .removePropLineBtn').hide();
                        //$('.envelopelistpropertiesBtn').hide();
                        $('.envelopelistproperties .inputs.properties .col-xs-12 .col-sm-1').removeClass('col-sm-1');
                        $('.envelopelistproperties .inputs.properties .col-xs-12 .col-sm-3').removeClass('col-sm-3').addClass('col-sm-5');
                        $('.envelopelistproperties .inputs.properties .col-xs-12 .col-sm-7').removeClass('col-sm-7').addClass('col-sm-5');
                        //$('.envelopelistproperties .inputs.properties .col-xs-12 .addmultifield0').attr('disabled','disabled');
                        $('.envelopelistproperties .inputs.properties .col-xs-12 .addmultifield1').attr('type','number');
                    },
                    save : function () {

                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>envelopeParams.jsonSchema.properties , function(k,val) {
                            //if (k == "envelopelist"){
                                tplCtx.value = getPairsObj('.' + k + val.inputType);
                                dataHelper.path2Value(tplCtx, function (params) {
                                    dyFObj.closeForm();
                                    reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                });
                                mylog.log("save tplCtx",tplCtx);
                            //}
                        });

                    }
                }
            };

            sectionDyf.<?php echo $kunik ?>envelopeParams.jsonSchema.properties['envelopelist'] = {
                inputType: "properties",
                labelKey: "Financeur",
                labelValue: "Enveloppe",
                label: "Enveloppe par financeur pour " + new Date().getFullYear(),
                values: sectionDyf.<?php echo $kunik ?>envelopeData
            }

            mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData,null,null,{
                    type : "bootbox",
                    notCloseOpenModal : true,
                });
            });

            $(".envelopeparams").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                tplCtx.arrayForm =  false;
                //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>envelopeParams,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            });

            $('.btnValidFinance').off().click(function() {
                tplCtx.pos = $(this).data("pos");

                tplCtx.budgetpath = $(this).data("budgetpath");
                tplCtx.collection = "answers";
                tplCtx.id = $(this).data("id");
                tplCtx.form = $(this).data("form");
                prioModal = bootbox.dialog({
                    message: $(".form-validate-work").html(),
                    title: "État du Financement",
                    show: false,
                    buttons: {
                        success: {
                            label: trad.save,
                            className: "btn-primary",
                            callback: function () {

                                var formInputsHere = formInputs;
                                tplCtx.path = "answers";
                                if( notNull(formInputs [tplCtx.form]) )
                                    tplCtx.path = "answers";

                                tplCtx.path = tplCtx.path+".aapStep1."+tplCtx.pos+".validFinal";

                                var today = new Date();
                                today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();
                                tplCtx.value = {
                                    valid : $(".bootbox #validWork").val(),
                                    user : userId,
                                    date : today
                                };
                                delete tplCtx.pos;
                                delete tplCtx.budgetpath;
                                mylog.log("btnValidateWork save",tplCtx);
                                dataHelper.path2Value( tplCtx, function(){
                                    //saveLinks(answerObj._id.$id,"financeValidated",userId,closePrioModalRel);
                                    if(typeof newReloadStepValidationInputGlobal != "undefined") {
                                        newReloadStepValidationInputGlobal({
                                            inputKey : <?php echo json_encode($key) ?>,
                                            inputType : "tpls.forms.ocecoform.financementFromBudget"
                                        })
                                    }
                                    closePrioModalRel();
                                } );
                            }
                        },
                        cancel: {
                            label: trad.cancel,
                            className: "btn-secondary",
                            callback: closePrioModal
                        }
                    },
                    onEscape: closePrioModal
                });
                prioModal.modal("show");
            });

            $('.rebtnFinancer').off().click(function() {
                tplCtx.pos = $(this).data("pos");
                var rercpos = $(this).data("pos");
                tplCtx.budgetpath = $(this).data("budgetpath");
                tplCtx.collection = "answers";
                tplCtx.id = $(this).data("id");
                tplCtx.form = $(this).data("form");
                tplCtx.arrayForm =  true;
                prioModal = bootbox.dialog({
                    message: $(".new-form-financer").html(),
                    title: "Ajouter un financement sur une ligne",
                    className: 'financerdialog',
                    show: false,
                    size: "large",
                    buttons: {
                        success: {
                            label: trad.save,
                            className: "btn-primary save-finance",
                            callback: function () {

                                //var formInputsHere = formInputs;
                                // var financersCount = ( typeof eval("answerObj.answers."+tplCtx.budgetpath+"["+tplCtx.pos+"].financer") != "undefined" ) ? eval("answerObj.answers."+tplCtx.budgetpath+"["+tplCtx.pos+"].financer").length : 0;

                                tplCtx.setType = [
                                    {
                                        "path": "amount",
                                        "type": "int"
                                    },
                                    {
                                        "path": "date",
                                        "type": "isoDate"
                                    }
                                ];

                                tplCtx.path = "answers.aapStep1";
                                tplCtx.arrayForm = true;

                                //tplCtx.path = tplCtx.path+"."+tplCtx.budgetpath+"."+tplCtx.pos+".financer."+financerList[tplCtx.pos].length;
                                tplCtx.path = tplCtx.path+"."+tplCtx.budgetpath+"."+tplCtx.pos+".financer";
                                // if( notNull(formInputs [tplCtx.form]) )
                                // 	tplCtx.path = "answers."+tplCtx.budgetpath+"."+tplCtx.pos+".financer."+financersCount;

                                var today = new Date();
                                today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();
                                tplCtx.value = {
                                    line   : $(".financerdialog .fi-fond").val(),
                                    amount : $(".financerdialog .fi-montant").val(),
                                    user   : userId,
                                    date   : today
                                };

                                if($(".financerdialog input.fi-financer").val() != 0 ){
                                    tplCtx.value.id = $(".financerdialog input.fi-financer").val();
                                    tplCtx.value.name = $('.financerdialog .whatFinancers').select2('data').text.trim();
                                }else if($(".financerdialog .fi-name").val() != "" ){
                                    tplCtx.value.name = $(".financerdialog .fi-name").val();
                                    tplCtx.value.email = $(".financerdialog .fi-mail").val();
                                }

                                delete tplCtx.pos;
                                delete tplCtx.budgetpath;
                                mylog.log("btnFinancer save",tplCtx);
                                dataHelper.path2Value( tplCtx, function(params) {
                                    delete tplCtx.arrayForm;
                                    var answerId = "<?php echo (string)$answer["_id"]; ?>";

                                    ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/newfinancement/answerid/' + answerId,
                                        {
                                            pos : rercpos,
                                            url : window.location.href
                                        },
                                        function (data) {

                                        }, "html");

                                    ajaxPost("", baseUrl + "/co2/aap/commonaap/action/notifyAddFinance", {
                                        answerId:tplCtx.id,
                                        status : tplCtx.value.line+' : '+ tplCtx.value.amount+' Euro'
                                    }, function(data){}, function(error){
                                        mylog.log("ajaxPost error", error)
                                    }, "json")

                                    prioModal.modal('hide');
                                    //saveLinks(answerObj._id.$id,"financerAdded",userId,function(){
                                    prioModal.modal('hide');
                                    //reloadInput("<?php //echo $key

                                    ?>//", "<?php //echo @$form["id"]

                                    ?>//");
                                    // });
                                    if(typeof newReloadStepValidationInputGlobal != "undefined") {
                                        newReloadStepValidationInputGlobal({
                                            inputKey : <?php echo json_encode($key) ?>,
                                            inputType : "tpls.forms.ocecoform.financementFromBudget"
                                        })
                                    }
                                    if (rendermodal){
                                        reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                    }else{
                                        if (typeof inputsList != "undefined"){
                                            $.each(inputsList , function (inn, vall) {
                                                $.each(vall , function (inn2, vall2) {
                                                    if (typeof vall2.type != "undefined"
                                                        && vall2.type.split('.')[vall2.type.split('.').length-1] != "undefined"
                                                        && ( vall2.type.split('.')[vall2.type.split('.').length-1] == "budget" ||
                                                            vall2.type.split('.')[vall2.type.split('.').length-1] == "financementFromBudget" ||
                                                            vall2.type.split('.')[vall2.type.split('.').length-1] == "suiviFromBudget"
                                                        )
                                                    ){
                                                        reloadInput(inn2, inn);
                                                    }
                                                });
                                            });
                                        }
                                    }
                                } );

                                delete tplCtx.setType;
                            }
                        },
                        cancel: {
                            label: trad.cancel,
                            className: "btn-secondary",
                            callback: function() {
                            }
                        }
                    },
                    onEscape: function() {
                        prioModal.modal("hide");
                    }
                });
                prioModal.modal("show");

                prioModal.on('shown.bs.modal', function (e) {
                    mylog.log("financementobj modal")
                    $('.save-finance').attr('disabled','disabled');
                    var financors = {};
                    ajaxPost('',baseUrl+'/co2/aap/getfinancor/id/'+formId+'/getAllItems/true',
                        null,
                        function(data){
                            financors = data;
                        },
                        null,
                        null,
                        {async : false})
                    if(typeof fincmntObj != "undefined" && fincmntObj.bindModalEvent) {
                        fincmntObj.bindModalEvent(financors)
                    }

                });

            });

            $('.FinanceAllLine').off().click(function() {
                var bfibudgetpath = $(this).data("budgetpath"),
                    bficollection = "answers" ,
                    bfiid = $(this).data("id") ,
                    bfiform = $(this).data("form");

                prioModal = bootbox.dialog({
                    message: $(".new-form-financeallline").html(),
                    title: "Ajouter un Financeur sur plusieurs lignes",
                    className: 'financerdialog',
                    show: false,
                    size: "large",
                    buttons: {
                        success: {
                            label: trad.save,
                            className: "btn-primary save-finance",
                            callback: function () {
                                var fpos = 0;
                                var answerObj = <?php echo json_encode($answer) ?>;
                                var fposarray = Object.keys(answerObj.answers.aapStep1.depense) ;
                                $.each(financementobj, function (ind, val) {
                                    if (parseInt(val.val) > 0) {

                                        tplCtx.setType = [
                                            {
                                                "path": "amount",
                                                "type": "int"
                                            },
                                            {
                                                "path": "date",
                                                "type": "isoDate"
                                            }
                                        ];

                                        tplCtx.budgetpath = bfibudgetpath;
                                        tplCtx.collection = bficollection;
                                        tplCtx.id = bfiid;
                                        tplCtx.form = bfiform;
                                        tplCtx.arrayForm = true;
                                        tplCtx.path = "answers.aapStep1";

                                        if (!Array.isArray(financerList[fpos]) && typeof financerList[fpos] != "undefined"){
                                            financerList[fpos] = Object.values(financerList[fpos]);
                                        }
                                        //tplCtx.path = tplCtx.path + "." + tplCtx.budgetpath + "." + fposarray[fpos] + ".financer." + financerList[fpos].length;
                                        tplCtx.path = tplCtx.path + "." + tplCtx.budgetpath + "." + fposarray[fpos] + ".financer";

                                        var today = new Date();
                                        today = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
                                        tplCtx.value = {
                                            line: $(".financerdialog .fi-fond").val(),
                                            amount: val.val,
                                            user: userId,
                                            date: today
                                        };

                                        if ($(".financerdialog input.fi-financer").val() != 0) {
                                            tplCtx.value.id = $(".financerdialog input.fi-financer").val();
                                            tplCtx.value.name = $('.financerdialog .whatFinancers').select2('data').text.trim();
                                        } else if ($(".financerdialog .fi-name").val() != "") {
                                            tplCtx.value.name = $(".financerdialog .fi-name").val();
                                            tplCtx.value.email = $(".financerdialog .fi-mail").val();
                                        }

                                        delete tplCtx.budgetpath;
                                        mylog.log("btnFinancer save", tplCtx);
                                        $.ajax({
                                            type : 'POST',
                                            data : {pos : fpos,
                                                url : window.location.href},
                                            url : baseUrl + '/survey/answer/rcnotification/action/newfinancement/answerid/' + answerId,
                                            dataType : "json",
                                            async : false,

                                            success : function(data){}
                                        });
                                        dataHelper.path2Value(tplCtx, function (params) {
                                            delete tplCtx.arrayForm;
                                            var answerId = "<?php echo (string)$answer["_id"]; ?>";

                                            prioModal.modal('hide');
                                            //saveLinks(answerObj._id.$id,"financerAdded",userId,function(){
                                            prioModal.modal('hide');
                                            if(typeof newReloadStepValidationInputGlobal != "undefined") {
                                                newReloadStepValidationInputGlobal({
                                                    inputKey : <?php echo json_encode($key) ?>,
                                                    inputType : "tpls.forms.ocecoform.financementFromBudget"
                                                })
                                            }
                                            //reloadInput("<?php //echo $key

                                            ?>//", "<?php //echo @$form["id"]

                                            ?>//");
                                            // });

                                        });

                                        delete tplCtx.setType;
                                    }
                                    fpos++;
                                });

                                if (rendermodal) {
                                    reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                } else {
                                    if (typeof inputsList != "undefined"){
                                        $.each(inputsList , function (inn, vall) {
                                            $.each(vall , function (inn2, vall2) {
                                                if (typeof vall2.type != "undefined"
                                                    && vall2.type.split('.')[vall2.type.split('.').length-1] != "undefined"
                                                    && ( vall2.type.split('.')[vall2.type.split('.').length-1] == "budget" ||
                                                        vall2.type.split('.')[vall2.type.split('.').length-1] == "financementFromBudget" ||
                                                        vall2.type.split('.')[vall2.type.split('.').length-1] == "suiviFromBudget"
                                                    )
                                                ){
                                                    reloadInput(inn2, inn);
                                                }
                                            });
                                        });
                                    }
                                }
                            }
                        },
                        cancel: {
                            label: trad.cancel,
                            className: "btn-secondary",
                            callback: function() {
                            }
                        }
                    },
                    onEscape: function() {
                        prioModal.modal("hide");
                    }
                });
                prioModal.modal("show");

                var thisbtn = $(this);

                prioModal.on('shown.bs.modal', function (e) {

                    $('.save-finance').attr('disabled','disabled');
                    $('.fi-montant').on('blur', function(){
                        if($(this).val() != ""){
                            $('.save-finance').removeAttr('disabled');
                        }
                    });
                    let restToFinance = 0;
                    $.each(financementobj, function (index, val) {
                        if (typeof val.tofin != "undefined" && val.tofin >= 0)
                                restToFinance += val.tofin;
                        $(".fi-montant.todoinput").val(restToFinance)
                        $(".fi-montant.todoinput").trigger("blur")
                    });
                    var setInputMontant = setInterval(() => {
                        if($(".fi-montant.todoinput").is(":visible")) {
                            $(".fi-montant.todoinput").trigger("keyup")
                            clearInterval(setInputMontant);
                            setInputMontant = null
                        }
                    }, 350);
                    var financors = {};
                    ajaxPost('',baseUrl+'/co2/aap/getfinancor/id/'+formId+'/getAllItems/true',
                        null,
                        function(data){
                            financors = data;

                        },
                        null,
                        null,
                        {async : false});

                    var unlimitedFinancing = <?= isset($parentForm["params"]["unlimitedFinancing"]) ?  "true" : "false" ?>;
                    fincmntObj.commonModalEvent(financors);
                    $(".financerdialog .fi-montant.todoinput").keyup( function( event ) {
                        if($(".financerdialog .fi-montant.todoinput").val() > 0) {

                            if (parseInt($(".financerdialog .fi-montant.todoinput").val()) > parseInt($(".financerdialog .fi-montant.todoinput").attr("max")) ) {
                                $(".financerdialog .fi-montant.todoinput").val($(".financerdialog .fi-montant.todoinput").attr("max") );
                            }

                            if (parseInt($(".financerdialog .fi-montant.todoinput").val()) > parseInt($(".financerdialog .fi-resteEnv").data("value"))){
                                //$(".financerdialog .fi-montant.todoinput").val($(".financerdialog .fi-resteEnv").data("value"));
                                $(".restEnvWarning").html("<i class='fa fa-info-circle'></i> Le montant à financer depasse la valeur restant dans l'enveloppe du financeur")
                            }else{
                                $(".restEnvWarning").html("");
                            }

                            var reste = parseInt($(".financerdialog .fi-montant.todoinput").val());
                            $.each(financementobj, function (index, val) {
                                if (typeof val.tofin)
                                    if (val.tofin > 0){
                                        if (val.tofin < reste){
                                            financementobj[index]["val"] = val.tofin;
                                            $(`.financerdialog tr[data-id="${ index }"] td:nth-child(4)`).html(val.tofin);
                                            reste -= val.tofin;
                                        } else {
                                            financementobj[index]["val"] = reste;
                                            $(`.financerdialog tr[data-id="${ index }"] td:nth-child(4)`).html(reste);
                                            reste = 0;
                                        }
                                    }
                            });
                        }
                    });

                });

            });

            $('.fibtnedit').off().click(function() {
                tplCtx.pos = $(this).data("pos");
                tplCtx.key = $(this).data("key");
                tplCtx.collection = "answers";
                tplCtx.id = $(this).data("id");
                tplCtx.arrayForm = false;
                tplCtx.form = $(this).data("form");
                tplCtx.uid = $(this).data("uid");
                tplCtx.setType = [
                    {
                        "path": "amount",
                        "type": "int"
                    },
                    {
                        "path": "date",
                        "type": "isoDate"
                    }
                ];

                if (typeof $(this).data("finid") != "undefined") {
                    var finid = $(this).data("finid");
                }else {
                    var finname = $(this).data("finname");
                    var finmail = $(this).data("finmail");
                }

                var finfond = $(this).data("finfond");
                var finmontant = $(this).data("finamount");

                prioModal = bootbox.dialog({
                    message: $(".new-form-financer").html(),
                    title: "Modifier un financement",
                    className: 'financerdialog',
                    show: false,
                    size: "large",
                    buttons: {
                        success: {
                            label: trad.save,
                            className: "btn-primary",
                            callback: function () {
                                tplCtx.path = "answers.aapStep1";

                                tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".financer."+tplCtx.uid;

                                var today = new Date();
                                today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();
                                tplCtx.value = {
                                    line   : $(".financerdialog .fi-fond").val(),
                                    amount : $(".financerdialog .fi-montant").val(),
                                    user   : userId,
                                    date   : today
                                };

                                if($(".financerdialog input.fi-financer").val() != 0 ){
                                    tplCtx.value.id = $(".financerdialog input.fi-financer").val();
                                    tplCtx.value.name = $('.financerdialog .whatFinancers').select2('data').text.trim();
                                }else if($(".financerdialog .fi-name").val() != "" ){
                                    tplCtx.value.name = $(".financerdialog .fi-name").val();
                                    tplCtx.value.email = $(".financerdialog .fi-mail").val();
                                }

                                if(tplCtx.value.name == "" )

                                    delete tplCtx.pos;
                                delete tplCtx.budgetpath;
                                mylog.log("btnFinancer save",tplCtx);
                                dataHelper.path2Value( tplCtx, function(params) {
                                    prioModal.modal('hide');
                                    //saveLinks(answerObj._id.$id,"financerAdded",userId,function(){
                                    prioModal.modal('hide');
                                    //reloadInput("<?php //echo $key

                                    ?>//", "<?php //echo @$form["id"]

                                    ?>//");
                                    //});
                                    if(typeof newReloadStepValidationInputGlobal != "undefined") {
                                        newReloadStepValidationInputGlobal({
                                            inputKey : <?php echo json_encode($key) ?>,
                                            inputType : "tpls.forms.ocecoform.financementFromBudget"
                                        })
                                    }
                                    if (rendermodal){
                                        reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                    }else{
                                        if (typeof inputsList != "undefined"){
                                            $.each(inputsList , function (inn, vall) {
                                                $.each(vall , function (inn2, vall2) {
                                                    if (typeof vall2.type != "undefined"
                                                        && vall2.type.split('.')[vall2.type.split('.').length-1] != "undefined"
                                                        && ( vall2.type.split('.')[vall2.type.split('.').length-1] == "budget" ||
                                                            vall2.type.split('.')[vall2.type.split('.').length-1] == "financementFromBudget" ||
                                                            vall2.type.split('.')[vall2.type.split('.').length-1] == "suiviFromBudget"
                                                        )
                                                    ){
                                                        reloadInput(inn2, inn);
                                                    }
                                                });
                                            });
                                        }
                                    }
                                } );
                            }
                        },
                        cancel: {
                            label: trad.cancel,
                            className: "btn-secondary",
                            callback: function() {
                            }
                        }
                    },
                    onEscape: function() {
                        prioModal.modal("hide");
                    }
                });
                prioModal.modal("show");

                prioModal.on('shown.bs.modal', function (e) {

                    var financors = {};
                    ajaxPost('',baseUrl+'/co2/aap/getfinancor/id/'+formId+'/getAllItems/true',
                        null,
                        function(data){
                            financors = data;

                        },
                        null,
                        null,
                        {async : false})
                    if(typeof fincmntObj != "undefined" && fincmntObj.bindModalEvent) {
                        fincmntObj.bindModalEvent(financors, true, finmontant)
                    }
                    $('.save-finance').attr('disabled','disabled');
                    $('.fi-montant').on('blur', function(){
                        if($(this).val() != ""){
                            $('.save-finance').removeAttr('disabled');
                        }
                    });
                    if (typeof finid != "undefined") {
                        if(notEmpty(coForm_FormWizardParams.context._id.$id) && coForm_FormWizardParams.context._id.$id == finid) {
                            $('.financerdialog .whatFinancers').select2('data', { id: coForm_FormWizardParams.context._id.$id , text: coForm_FormWizardParams.context.name});
                        }else{
                            $('.financerdialog .whatFinancers').select2('data', { id: finid , text: financors[finid].name});
                        }

                        $(".financerdialog").find(".dcombtn").addClass("active");
                        $(".financerdialog").find(".icombtn").removeClass("active");
                        $(".financerdialog").find(".dcomdiv").removeClass("hide");
                        $(".financerdialog").find(".icomdiv").addClass("hide");
                    }else {
                        $(".fi-name").val(finname);
                        $(".fi-mail").val(finmail);

                        $(".financerdialog").find(".icombtn").addClass("active");
                        $(".financerdialog").find(".dcombtn").removeClass("active");
                        $(".financerdialog").find(".icomdiv").removeClass("hide");
                        $(".financerdialog").find(".dcomdiv").addClass("hide");
                    }
                    $(".fi-fond").val(finfond);
                    $(".fi-montant").val(finmontant);
                    $(".fi-montant").data('montant' , finmontant);
                });

                $(".switchcombtn").on("click",function() {
                    $(".financerdialog").find("."+$(this).data("switchactbtn")).addClass("active");
                    $(".financerdialog").find("."+$(this).data("removebtn")).removeClass("active");
                    $(".financerdialog").find("."+$(this).data("switchactdiv")).removeClass("hide");
                    $(".financerdialog").find("."+$(this).data("removediv")).addClass("hide");
                });

            });

            $('.fibtndelete').off().click(function() {
                tplCtx.pos = $(this).data("pos"); pos-financer
                tplCtx.posFinacer = $(this).data("pos-financer");
                tplCtx.collection = "answers";
                tplCtx.id = $(this).data("id");
                tplCtx.key = $(this).data("key");
                tplCtx.form = $(this).data("form");
                tplCtx.uid = $(this).data("uid");
                tplCtx.value = null;
                prioModal = bootbox.dialog({
                    title: trad.confirmdelete,
                    show: false,
                    message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                    buttons: [
                        {
                            label: "Ok",
                            className: "btn btn-primary pull-left",
                            callback: function() {
                                tplCtx.path = "answers";
                                tplCtx.path = "answers.aapStep1";
                                tplCtx.pull = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".financer";

                                tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".financer."+tplCtx.posFinacer;
                                //coForm_FormWizardParams.answer.answers.aapStep1.depense[parseInt(tplCtx.pos)]["financer"].splice(parseInt(tplCtx.uid), 1);
                                //tplCtx.value = coForm_FormWizardParams.answer.answers.aapStep1.depense[parseInt(tplCtx.pos)]["financer"];
                                mylog.log("btnEstimate save",tplCtx);
                                dataHelper.path2Value( tplCtx, function(params){
                                    delete tplCtx.pull;
                                    if(tplCtx.result) toastr.success(trad.deleted)
                                } );

                                prioModal.modal('hide');
                                if(typeof newReloadStepValidationInputGlobal != "undefined") {
                                    newReloadStepValidationInputGlobal({
                                        inputKey : <?php echo json_encode($key) ?>,
                                        inputType : "tpls.forms.ocecoform.financementFromBudget"
                                    })
                                }
                                if (rendermodal){
                                    reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                }else{
                                    if (typeof inputsList != "undefined"){
                                        $.each(inputsList , function (inn, vall) {
                                            $.each(vall , function (inn2, vall2) {
                                                if (typeof vall2.type != "undefined"
                                                    && vall2.type.split('.')[vall2.type.split('.').length-1] != "undefined"
                                                    && ( vall2.type.split('.')[vall2.type.split('.').length-1] == "budget" ||
                                                        vall2.type.split('.')[vall2.type.split('.').length-1] == "financementFromBudget" ||
                                                        vall2.type.split('.')[vall2.type.split('.').length-1] == "suiviFromBudget"
                                                    )
                                                ){
                                                    reloadInput(inn2, inn);
                                                }
                                            });
                                        });
                                    }

                                }
                            }
                        },
                        {
                            label: "Annuler",
                            className: "btn btn-default pull-left",
                            callback: function() {}
                        }
                    ]
                });

                prioModal.modal("show");
            });

        });

        function closePrioModal(){
            prioModal.modal('hide');
        }

    </script>
    <?php
} ?>
    <?php
}else{
    echo "";
}
 ?>

