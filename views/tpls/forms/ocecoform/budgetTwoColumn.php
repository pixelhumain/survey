<style>
    .table-container-<?= $key ?> {
        /* display: flex; */
        /*m
        argin-bottom:50px;*/
    }
    .table-container-<?= $key ?> tfoot tr.tr-subvention{
        height: 94px;
    }

    .table-container-<?= $key ?> tbody>tr { 
        position: relative;
        /* height: 100px; */
    }
    .table-container-<?= $key ?> tbody>tr:hover .delete-line-<?=$kunik?>{
        display: initial;
    }
    .table-container-<?= $key ?> tbody>tr:hover{
        
    }
    .delete-line-<?=$kunik?>{
        display: none;
    }
    span.editable-line-<?=$kunik?>{
        display: inline-block;
        position: relative;
        height: 100%;
    }
</style>
<?php
$tableOptions = [
    "budget"  => array(
        "columnLabel" => "Budget"
    ),
    "depense" => array(
        "columnLabel" => "Plan de Financement"
    ),
];

$properties = [
    "group" =>[
        "placeholder" => "Groupé",
        "inputType" => "tags",
        "tagsList" => "budgetgroupe",
        "rules" => ["required" => true],
        "maximumSelectionLength" => 3
    ],
    "nature" => [
        "placeholder" =>
        "Nature de l’action",
        "inputType" => "tags",
        "tagsList" => "budgetnature",
        "rules" => ["required" => true],
        "maximumSelectionLength" => 3
    ],
    "poste" => [
        "inputType" => "text",
        "label" => "Poste de dépense",
        "placeholder" => "Poste de dépense",
        "rules" => ["required" => true]
    ]
];
$answerPath = str_replace('.'.$key.'.', '', $answerPath);

$paramsData = [
    "group" => ["Feature", "Costum", "Chef de Projet", "Data", "Mantenance"],
    "nature" => ["investissement", "fonctionnement"],
    "amounts" => ["price" => "Montant"],
    "estimate" => true,
    "activeGroup" => false,
    "activeNature" => false,
    "fielLabel" => "Ligne de dépense",
    "prefilledDepense" => array(),
];
    if (isset($parentForm["params"][$kunik]["group"])) $paramsData["group"] = $parentForm["params"][$kunik]["group"];
    if (isset($parentForm["params"][$kunik]["nature"])) $paramsData["nature"] = $parentForm["params"][$kunik]["nature"];
    if (isset($parentForm["params"][$kunik]["amounts"])) $paramsData["amounts"] = $parentForm["params"][$kunik]["amounts"];
    if (isset($parentForm["params"][$kunik]["estimate"])) $paramsData["estimate"] = Answer::is_true($parentForm["params"][$kunik]["estimate"]);
    if (isset($parentForm["params"][$kunik]["activeGroup"])) $paramsData["activeGroup"] = Answer::is_true($parentForm["params"][$kunik]["activeGroup"]);
    if (isset($parentForm["params"][$kunik]["activeNature"])) $paramsData["activeNature"] = Answer::is_true($parentForm["params"][$kunik]["activeNature"]);
    if (isset($input["label"])) $paramsData["fielLabel"] = $input["label"];
    if (isset($parentForm["params"][$kunik]["prefilledDepense"])) $paramsData["prefilledDepense"] = $parentForm["params"][$kunik]["prefilledDepense"];

    if(!$paramsData["activeGroup"]) unset($properties["group"]);
    if(!$paramsData["activeNature"]) unset($properties["nature"]);
    foreach ($paramsData["amounts"] as $k => $l)
    {
        $properties[$k] = ["inputType" => "number", "label" => $l, "propType" => "amount", "placeholder" => $l,
            "rules" => [ "required" => true, "number" => true ]
        ];
    }

    $ctxtid = "";
    $ctxttype = "";
    if (!empty($parentForm["parent"]))
    {
        foreach ($parentForm["parent"] as $idd => $valuee)
        {
            $ctxtid = $idd;
            $ctxttype = $valuee["type"];
            break;
        }
    }

if ($mode == "r" || $mode == "pdf") { ?>
    <div class="col-xs-12 no-padding" id="<?= $kunik ?>">
        <label for="<?= $kunik ?>">
            <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
                <?php echo $label ?>
            </h4>
        </label><br />
        <?php echo $answers; ?>
    </div>
<?php
} else {
?>
    <div class="col-md-12 no-padding">
        <div class="form-group">
            <label for="<?php echo $key ?>">
                <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>" class="<?= $canEditForm && $mode = "fa" ? "" : $type ?>">
                    <?php echo $label . $editQuestionBtn ?>
                    <?php if ($canEditForm) { ?>
                        <a href="javascript:;" class="btn btn-xs btn-danger config<?= $kunik ?>"><i class="fa fa-cog"></i></a>
                    <?php } ?>
                </h4>
            </label>
            <?php if (!empty($info)) { ?>
                <br />
                <small id="<?php echo $key ?>Help" class="form-text text-muted">
                    <?php echo $info ?>
                </small>
            <?php } ?>
            <br />
        </div>
        <div class="row table-container-<?= $key ?>">
            <?php foreach ($tableOptions as $ktable => $vtable) {
                $idtable = $key . "-" . $ktable ?>
                <div class="col-xs-12 col-md-6 no-padding margin-bottom-35" id="container-<?= $idtable ?>">
                    <table class="table table-bordered table-hover" id="table-<?= $idtable ?>">
                        <thead>
                            <tr class="th-0">
                                <th><?= $vtable["columnLabel"] ?>
                                    <button type="button" class="btn btn-primary pull-right tooltips add<?= $kunik ?>" data-id="<?= (string) $answer["_id"] ?>" data-collection="<?= Form::ANSWER_COLLECTION ?>" data-path="<?= $answerPath.'.'.$ktable ?>" data-toggle="tooltips" data-placement="bottom" data-original-title="<?= Yii::t("common", "Add line") ?>">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </th>
                                <th>Montant</th>
                            </tr>
                        </thead>
                        <tbody id="tbody-<?= $idtable ?>">
                            <!-- <tr class="tr-0" style="position:absolute;bottom:-23px;background:#eee">
                                <td><b>TOTAL</b></td>
                                <td id="total-<?= $idtable ?>"><b>20000</b></td>
                            </tr> -->
                        </tbody>
                        <tfoot>
                            <tr>
                                <td><b>TOTAL</b></td>
                                <td id="total-<?= $idtable ?>"><b>20000</b></td>
                            </tr>
                            <tr class="tr-subvention">
                                <td colspan="2" id="td-<?= $idtable ?>">
                                    &nbsp
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>

<script type="text/javascript">
    jQuery(document).ready(function() {
        var projectId = "<?php echo (!empty($answer["project"]["id"]) ? $answer["project"]["id"] : "null"); ?>";
        var <?= $key ?>Obj = {
            key: <?= json_encode($key) ?>,
            tableOptions: <?= json_encode($tableOptions) ?>,
            answer: <?= json_encode($answer) ?>,
            answerId: <?= json_encode((string)$answer["_id"]) ?>,
            form: <?= json_encode($parentForm) ?>,
            answerPath: <?= json_encode($answerPath) ?>,
            init: function(obj) {
                obj.generateTable(obj);
                setTimeout(() => {
                    obj.lineToSameHeight(obj);
                }, 1000);
                obj.events(obj);
            },
            generateTable: function(obj) {
                if (exists(jsonHelper.getValueByPath(obj.answer, "answers.aapStep1"))) {
                    $.each(obj.answer.answers.aapStep1, function(k, v) {
                        if (Object.keys(obj.tableOptions).includes(k)) {
                            obj.createTableContent(obj, k, v);
                            obj.getTotal(obj, k, v);
                        }
                    })
                }
            },
            getTotal: function(obj, key, value) {
                var total = 0;
                $.each(value, function(k, v) {
                    if (notEmpty(v?.price)) {
                        total += parseInt(v.price);
                    }
                })
                $('#total-' + obj.key + '-' + key + " b").html(total);
            },
            createTableContent: function(obj, key, value) {
                
                    var html = "";
                    var i = 1;
                    var showSubvention = {
                        active : false,
                        total : 0
                    }
                    $.each(value, function(k, v) {
                        if(v != null){
                            if(notEmpty(v?.poste) && v?.poste.toLowerCase().indexOf("subvention aap politique de la ville") != -1){
                                mylog.log("subvention",v?.poste,v?.price)
                                showSubvention.active = true;
                                showSubvention.total += parseFloat(exists(v.price) ? v.price : 0);
                                mylog.log("showSubvention",showSubvention.total);
                            }
                            html +=
                                `<tr class="tr-${i}">
                                    <td>
                                        <a href="javascript:;" class="delete-line-<?=$kunik?> btn btn-danger btn-xs tooltips margin-right-15" data-toggle="tooltips" data-pull="${obj.answerPath+'.'+key}" data-path="${obj.answerPath+'.'+key+'.'+k}" data-placement="bottom" data-original-title="<?= Yii::t("common", "Delete line") ?>">
                                        <i class="fa fa-times"></i>
                                        </a>
                                        <span class="editable-line-<?=$kunik?>" data-type="" data-action="${exists(v?.actionid) ? v?.actionid : ''}" data-path="${obj.answerPath+'.'+key+'.'+k+'.poste'}" contenteditable="true">
                                            ${notEmpty(v?.poste) ? v?.poste :''}
                                        </span>
                                    </td>
                                    <td class="editable-line-<?=$kunik?>" data-type="int" data-action="${exists(v?.actionid) ? v?.actionid : ''}" data-path="${obj.answerPath+'.'+key+'.'+k+'.price'}" contenteditable="true">
                                        ${notEmpty(v?.price) ? v?.price :''}
                                    </td>
                                </tr>`;
                            i++;
                        }
                        
                    })
                    if(showSubvention.total !=0){
                        $('#td-' + obj.key + '-' + key).html(`
                            <i class="pull-left alert alert-success" style="width: 100%;">
                            L'association sollicite une subvention totale de <span class="text-bold">${showSubvention.total} € </span></i>
                        `);
                    }else{
                        //$('#td-' + obj.key + '-' + key).parent().remove();
                    }

                    $('#tbody-' + obj.key + '-' + key).html(html);
            },
            lineToSameHeight: function(obj) {
                var opts = {};
                var tableMaxLine = {};
                $('.table-container-<?= $key ?> table tbody').each(function() {
                    var tableId = $(this).parent().attr("id");
                    tableMaxLine[tableId] = $(this).children().length;
                })
                tableMaxLine = Math.max(...Object.values(tableMaxLine));
                $('.table-container-<?= $key ?> table tbody').each(function() {
                    var tbody = $(this);
                    mylog.log("diff", tbody.children().length, tableMaxLine)
                    if (tbody.children().length < tableMaxLine) {
                        var diff = tableMaxLine - tbody.children().length;
                        mylog.log("diff", diff)
                        for (let i = 1; i <= diff; i++) {
                            tbody.append(`<tr class="${'tr-'+tbody.children().length}">
                                <td>&nbsp;</td><td>&nbsp;</td>
                            </tr>`)
                        }
                    }
                })

                $('.table-container-<?= $key ?> table tbody tr').each(function() {
                    var classe = $(this).attr('class');
                    var tableId = $(this).parent().parent().attr("id");
                    var height = $(this).height();
                    if (exists(opts[classe]) && height > opts[classe]) {
                        opts[classe] = height;
                        $('.' + classe).height(height)
                    } else {
                        opts[classe] = height;
                        $('.' + classe).height(height)
                    }

                    $("#" + tableId + " .tr-0 td").first().width($("#" + tableId + " tr th").first().width());
                    $("#" + tableId + " .tr-0 td").last().width($("#" + tableId + " tr th").last().width());
                })
            },
            events: function(obj) {
                $(".add<?= $kunik ?>").off().on("click", function() {
                    tplCtx.id = $(this).data("id");
                    tplCtx.collection = $(this).data("collection");
                    tplCtx.path = $(this).data("path");
                    dyFObj.openForm(obj.forms.add(obj), null, null, null, null, {
                        type: "bootbox",
                        notCloseOpenModal: true,
                    });
                });
                $(window).resize(function(){
                    obj.lineToSameHeight(obj);
                    mylog.log("resized")
                });
                obj.forms.edit(obj);
                obj.forms.delete(obj);
                
            },
            forms: {
                add : function(obj) {
                    return {
                        jsonSchema: {
                            title: "Budget prévisionnel",
                            icon: "fa-money",
                            text: "Décrire ici les principaux postes de dépenses : à quoi correspondent les coûts ? <br/>Préciser ce qui relève des dépenses de fonctionnement et des dépenses d’investissement.<br/>Les coûts doivent être en <b>hors taxe</b>.",
                            properties: <?php echo json_encode($properties); ?>,
                            onLoads: {
                                onload: function() {

                                }
                            },
                            afterBuild: function() {

                            },
                            save: function() {
                                var today = new Date();
                                tplCtx.setType = [{
                                        "path": "price",
                                        "type": "int"
                                    },
                                    {
                                        "path": "date",
                                        "type": "isoDate"
                                    }
                                ];

                                tplCtx.value = {
                                    date: today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear()
                                };

                                $.each(obj.forms.add(obj).jsonSchema.properties, function(k, val) {
                                    tplCtx.value[k] = $("#" + k).val();
                                });

                                /*if (typeof tplCtx.value.group != "undefined") {
                                    var tplCtxgroup = {
                                        id: parentId,
                                        collection: "forms",
                                        path: "params.<?php echo $kunik ?>.group"
                                    }
                                    tplCtxgroup.value = sectionDyf.<?php echo $kunik ?>ParamsData.group;
                                    $.each(tplCtx.value.group.split(","), function(ind, val) {
                                        if (jQuery.inArray(val, tplCtxgroup.value) == -1) {
                                            tplCtxgroup.value.push(val);
                                        }
                                    });

                                    dataHelper.path2Value(tplCtxgroup, function(params) {});
                                }*/
                                /*if (typeof tplCtx.value.nature != "undefined") {
                                    var tplCtxnature = {
                                        id: parentId,
                                        collection: "forms",
                                        path: "params.<?php echo $kunik ?>.nature"
                                    }
                                    tplCtxnature.value = sectionDyf.<?php echo $kunik ?>ParamsData.nature;
                                    $.each(tplCtx.value.nature.split(","), function(ind, val) {
                                        if (jQuery.inArray(val, tplCtxnature.value) == -1) {
                                            tplCtxnature.value.push(val);
                                        }
                                    });
                                    dataHelper.path2Value(tplCtxnature, function(params) {});
                                }*/

                                var connectedData = ["financer", "todo", "payed", "progress", "worker", "validFinal", "votes"];
                                $.each(connectedData, function(k, attr) {
                                    if (notNull("answerObj." + tplCtx.path + "." + attr))
                                        tplCtx.value[attr] = jsonHelper.getValueByPath(answerObj, tplCtx.path + "." + attr);
                                });

                                mylog.log("save tplCtx", tplCtx);
                                if (typeof tplCtx.value == "undefined") {} else {
                                    tplCtx.arrayForm = true;
                                    mylog.log(tplCtx.value,"ganagana");
                                    dataHelper.path2Value(tplCtx, function(params) {
                                        delete(tplCtx.arrayForm);
                                        delete tplCtx.setType;
                                        dyFObj.closeForm();
                                        if(typeof newReloadStepValidationInputGlobal != "undefined") {
                                            newReloadStepValidationInputGlobal({
                                                inputKey : obj.key,
                                                inputType : "tpls.forms.ocecoform.budgetTwoColumn" 
                                            })
                                        }
                                        ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/newdepense/answerid/' + obj.answerId, {
                                                name : tplCtx.value.poste,
                                                url: window.location.href
                                            },
                                            function(data) {
                                            }, "html");

                                            ajaxPost("", baseUrl + "/co2/aap/commonaap/action/notifyAddDepenseLine", {
                                                answerId:tplCtx.id,
                                                depense: tplCtx.value.poste +": "+  tplCtx.value.price + " Euro"
                                            }, function(data){}, function(error){}, "json")
                                        reloadInput(obj.key, "<?= $form["id"] ?>");
                                    });
                                }
                            },
                            beforeBuild: function(data) {
                                var budgetgroupe = <?php echo (!empty($paramsData["group"]) ? json_encode($paramsData["group"]) : "[]"); ?>;
                                var budgetnature = <?php echo (!empty($paramsData["nature"]) ? json_encode($paramsData["nature"]) : "[]"); ?>;
                                if (typeof costum != "undefined" && costum != null) {
                                    costum["budgetgroupe"] = budgetgroupe;
                                    costum["budgetnature"] = budgetnature;
                                }
                            },
                        }
                    }
                },
                edit : function(obj){
                    $('.editable-line-<?=$kunik?>').off().on('blur',function(){
                        var element = $(this);
                        var val = element.html().trim();
                        if(isNaN(parseInt(element.html().trim())) && element.data("type") == "int"){
                            element.html("");
                            return toastr.error("Nombre seulement");
                        }
                        if(element.data("type") == "int")
                            val = Math.abs(parseInt(val));
                        
                        var tplCtx = {
                            id : obj.answerId,
                            collection : "answers",
                            path : element.data("path"),
                            value: val,
                            setType : element.data("type")
                        }
                        dataHelper.path2Value(tplCtx,function(prms){
                            reloadInput(obj.key, "<?= $form["id"] ?>");
                            if(typeof newReloadStepValidationInputGlobal != "undefined") {
                                newReloadStepValidationInputGlobal({
                                    inputKey : obj.key,
                                    inputType : "tpls.forms.ocecoform.budgetTwoColumn" 
                                })
                            }
                            if(prms.result)
                                toastr.success(trad.saved)
                        })
                        var tplCtxproject = {};
                        if (projectId != null && projectId != "null") {
                            if (notNull(tplCtx.actionid)) {
                                tplCtxproject.id = tplCtx.actionid;
                                tplCtxproject.collection = "actions";
                                tplCtxproject.path = "name"
                                tplCtxproject.value = tplCtx.value.poste;
                                if(exists(tplCtxproject.value))
                                    dataHelper.path2Value(tplCtxproject, function () {});
                            }

                            var answerId = "<?php echo (string)$answer["_id"]; ?>";
                            var cntxtId = "<?php echo $ctxtid; ?>";
                            var cntxtType = "<?php echo $ctxttype; ?>";

                            ajaxPost("", baseUrl + '/survey/form/generateproject/answerId/' + answerId + '/parentId/' + cntxtId + '/parentType/' + cntxtType,
                                null,
                                function (data) {
                                });
                        }
                    })
                },
                delete : function(obj){
                    $('.delete-line-<?=$kunik?>').off().on('click',function(){
                        var element = $(this);
                        bootbox.confirm(trad.confirm+"<br/><span class='text-red bold'><i class='fa fa-warning'></i>"+trad.actionirreversible+"</span>", function(result){
                            if(result){
                                var tplCtx = {
                                    id : obj.answerId,
                                    collection : "answers",
                                    path : element.data("path"),
                                    value: null,
                                    pull: element.data("pull"),
                                }
                                dataHelper.path2Value(tplCtx,function(prms){
                                    reloadInput(obj.key, "<?= $form["id"] ?>");
                                    if(typeof newReloadStepValidationInputGlobal != "undefined") {
                                        newReloadStepValidationInputGlobal({
                                            inputKey : obj.key,
                                            inputType : "tpls.forms.ocecoform.budgetTwoColumn" 
                                        })
                                    }
                                    if(prms.result)
                                        toastr.success(trad.deleted)
                                })
                            }
                        })
                    })
                }
            }
        }
        <?= $key ?>Obj.init(<?= $key ?>Obj);
        mylog.log(<?= $key ?>Obj, "fcbarcelona")
    });
</script>