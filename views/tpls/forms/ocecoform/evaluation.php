<style>
    /** Field set */
    fieldset.quadrant {
        border: 2px solid #eeeeee;
        display: block;
        margin-left: 2em;
        margin-top: 1em;
        padding-top: 0.35em;
        padding-bottom: 0.625em;
        padding-left: 0.75em;
        padding-right: 0.75em;
        height: 100%;
    }

    fieldset.quadrant legend {
        background-color: gray;
        color:white;
        width:auto;
        font-size: 12pt;
        font-weight: bolder !important;
        padding: 5px 10px;
    }

    label.toggle{
        position: relative;
        cursor: pointer;
        color: #666;
    }

    label.toggle span{
        font-size: 18px;
    }

    input[type="checkbox"], input[type="checkbox"]{
        position: absolute;
        right: 9000px;
    }

    /*checkbox Toggle*/
    .toggle input[type="checkbox"] + .label-text:before{
        content: "\f204";
        font-size: 25px;
        font-family: "FontAwesome";
        speak: none;
        font-style: normal;
        font-weight: normal;
        font-variant: normal;
        text-transform: none;
        line-height: 1;
        -webkit-font-smoothing:antialiased;
        width: 1em;
        display: inline-block;
        margin-right: 10px;

    }

    .toggle input[type="checkbox"]:checked + .label-text:before{
        content: "\f205";
        color: #16a085;
        animation: effect 250ms ease-in;
    }

    .toggle input[type="checkbox"]:disabled + .label-text{
        color: #aaa;
    }

    .toggle input[type="checkbox"]:disabled + .label-text:before{
        content: "\f204";
        color: #ccc;
    }

    @keyframes effect{
        0%{transform: scale(0);}
        25%{transform: scale(1.3);}
        75%{transform: scale(1.4);}
        100%{transform: scale(1);}
    }
</style>

<?php
if (isset($rendermodal) && $rendermodal){
    if (!empty($parentForm["config"])) {
        $nameconfig = Element::getByTypeAndId(Form::COLLECTION, $parentForm["config"])["name"];
    } else {
        $nameconfig = "";
    }
    ?>
        <div id="sticky-anchor"></div>
        <div id="sticky">
            <div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
                <ul class="breadcrumb">
                    <li ><a><?php echo $nameconfig ?></a></li>
                    <li class="active"><a><?php echo $parentForm["name"] ?></a></li>
                    <li class="active"><a>financement du proposition : <?php echo @$answer["answers"]["aapStep1"]["titre"] ?> </a></li>
                </ul>
            </div>

        </div>
        <div class="col-xs-12 col-lg-10 col-lg-offset-1 project-detail">
    <?php
    }
    ?>


<?php if($answer){ ?>

    <div class="form-group">
        <?php
            # Initialize parameters data
            $paramsData = [
                "objet" => @$answer["aapStep1"]["titre"],
                "objectif" => "Évaluer la proposition", 
                "quadrants" => [
                    "quadrant0" => ["label" => "Ce qui est obsolète", "inputNb"=>2],
                    "quadrant1" => ["label" => "Ce qui est valide", "inputNb"=>2],
                    "quadrant2" => ["label" => "Ce qui est anticipé", "inputNb"=>2],
                    "quadrant3" => ["label" => "Ce qui est innovant", "inputNb"=>2]
                ]
            ];

            # Set parameters data
            if( isset($form["params"][$kunik]["objet"]) )
                $paramsData["objet"] = $form["params"][$kunik]["objet"];

            if( isset($form["params"][$kunik]["objectif"]) )
                $paramsData["objectif"] = $form["params"][$kunik]["objectif"];

            if( isset($form["params"][$kunik]["quadrants"]) )
                $paramsData["quadrants"] = $form["params"][$kunik]["quadrants"];

            # Get answers if exist
            # !!!  Warning for update !!! 
            # The answers here are the quadrants, If you want add other input (no quadrant) you should edit the code below

            // remove the to test directily (dev) "draft" => ["$exists" => false ]]
            $theAnswers = PHDB::find("answers", array("user"=>Yii::app()->session["userId"], "form"=>$parentForm["_id"]->{'$id'}, "draft" => ['$exists' => false ]));
            
            $myAnswers = reset($theAnswers);
            //$myAnswers = PHDB::find("answers", array("context.".$this->costum['contextId'].".name"=>$this->costum['name'], "user" => Yii::app()->session["userId"]));
            if(!empty($myAnswers["answers"][$form["id"]][$kunik])){
                $myEvaluation = $myAnswers["answers"][$form["id"]][$kunik];
            }else{
                $myEvaluation = [];
            }

            $editParamsBtn = "";

            if(@$form["creator"]==Yii::app()->session["userId"]){
                $editParamsBtn = ($canEdit) ? " <a href='javascript:;' 
                                                    data-id='".$form["_id"]."' 
                                                    data-collection='".Form::COLLECTION."' 
                                                    data-path='params.".$kunik."' 
                                                    class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'>
                                                    <i class='fa fa-cog'></i> 
                                                </a>" : "";
            }
        ?>

        <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
            <?php echo $label.$editQuestionBtn.$editParamsBtn?>
        </h4>

        <?php echo $info ?>

        <br>
        <div class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-sm-2" for="objectif<?= $kunik ?>">OBJECTIF :</label>
                <div class="col-sm-10">
                    <label><?php echo $paramsData["objectif"]?></label>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="objet<?= $kunik ?>">OBJET :</label>
                <div class="col-sm-10">
                    <label><?php echo $paramsData["objet"] ?></label>
                </div>
            </div>
            <div class="row">
            <?php foreach ($paramsData["quadrants"] as $quadrant => $value) {  ?>
                <div class="form-group col-md-6">
                    <fieldset class="quadrant">
                        <legend><?= $value["label"] ?> :</legend>
                        <div class="col-sm-12">
                            <div id="<?= $quadrant ?>Inputs<?= $kunik ?>">
                            </div>
                            <a role="button" class="btn btn-default addEvaluation<?= $kunik ?> btn<?=$kunik?><?=$quadrant?>" data-quadrant="<?= $quadrant ?>"><i class="fa fa-plus"></i> ajouter une case</a>
                        </div>
                    </fieldset>
                </div>
            <?php } ?>
            </div>
        </div>    
    </div>

    <?php
if (isset($rendermodal) && $rendermodal){
    ?>
        </div>

<?php
}
    ?>

    <script type="text/javascript">
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
        
        $(document).ready(function(){
            let kunik = "<?= $kunik ?>";
            let inputNb = {};
            let evaluations = <?php echo json_encode( $myEvaluation ); ?>;
            let evaluationQuadrants = <?php echo json_encode( $paramsData["quadrants"] ); ?>;
            let options = [];
            
            for (let [key, value] of Object.entries(evaluationQuadrants)) {
                let hasAnswered = false;
                let defaultLength = value.inputNb;
                // Test if options or input number
                if(isNaN(Number(value.inputNb))){
                    options = value.inputNb.split(";").map(item=>item.trim());
                    defaultLength = options.length;
                    $(".btn"+kunik+key).remove();
                }else{
                    if(evaluations[key]){
                        defaultLength = (Object.keys(Object.keys(evaluations[key])).length==0)?parseInt(value.inputNb):Object.keys(Object.keys(evaluations[key])).length;
                        hasAnswered = (Object.keys(evaluations[key]).length!=0);
                    }
                }
                
                for (let index = 0; index < defaultLength; index++) {
                    let checkboxId = "evaluation"+index;

                    let input = (options.length==0)?`<div class="form-group" id="${key}Input${index}">
                                    <div class="col-md-10 col-sm-11 col-xs-10">
                                        <input type="text" 
                                            value="${(hasAnswered)?evaluations[key][Object.keys(evaluations[key])[index]]:""}" 
                                            data-form='<?php echo $form["id"] ?>' 
                                            id="${kunik}.${key}.${(hasAnswered)?Object.keys(evaluations[key])[index]:"evaluation"+index}" 
                                            class="form-control saveOneByOne" 
                                            placeholder="${value.label}">
                                    </div>
                                    <div class="col-md-2 col-sm-1 col-xs-2 text-right">
                                        <a role="button" 
                                            class="btn btn-danger ${kunik}${key}${(hasAnswered)?Object.keys(evaluations[key])[index]:"evaluation"+index} remove${kunik}"
                                            data-code="${index}" 
                                            data-target="${kunik}${key}${(hasAnswered)?Object.keys(evaluations[key])[index]:"evaluation"+index}" 
                                            data-quadrant="${key}">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </div>
                                </div>`:`<div class="col-md-12 col-sm-12 col-xs-12 form-check">
                                        <label class="toggle">
                                            <input type="checkbox" 
                                                data-form='<?php echo $form["id"] ?>' 
                                                name="${kunik}"
                                                data-key="${(hasAnswered)?Object.keys(evaluations[key])[index]:"evaluation"+index}"
                                                id="${kunik}.${key}.evaluation${index}" 
                                                class="form-check-input evaluation${index} saveOneByOne">
                                                <span class="label-text">${options[index]}</span>
                                        </label>
                                    </div>`;
                                    
                    $("#"+key+"Inputs<?= $kunik ?>").append(input);
                    
                    // Get max index to avoid conflict
                    if(hasAnswered){
                        inputNb[key] = parseInt(Object.keys(evaluations[key])[index].substring(10));
                    }else{
                        inputNb[key] = value.inputNb;
                    }

                    // For Checkbox
                    if(options.length!=0 && evaluations[key]!=undefined ){
                        for (let k = 0; k < Object.keys(evaluations[key]).length; k++) {
                            $("."+Object.keys(evaluations[key])[k]).attr('checked', true);
                        }
                    }
                }

                options = [];
            }

            $(document).on("click", ".remove"+kunik, function(){
                $("."+$(this).data("target")).val("").blur();
                $("#"+$(this).data("quadrant")+"Input"+$(this).data("code")).remove();
            });
            
            $(".addEvaluation<?= $kunik ?>").click(function(){

                inputNb[$(this).data("quadrant")]++;
                
                let input = `<div class="form-group" id="${$(this).data("quadrant")}Input${inputNb[$(this).data("quadrant")]}">
                                <div class="col-md-10 col-sm-11 col-xs-10">
                                    <input type="text" 
                                        data-form='<?php echo $form["id"] ?>' 
                                        id="${kunik}.${$(this).data("quadrant")}.evaluation${inputNb[$(this).data("quadrant")]}" 
                                        class="form-control saveOneByOne" 
                                        placeholder="${evaluationQuadrants[$(this).data("quadrant")]["label"]}">
                                </div>
                                <div class="col-md-2 col-sm-1 col-xs-2 text-right">
                                    <a role="button" 
                                        class="btn btn-danger remove${kunik}" 
                                        data-code="${inputNb[$(this).data("quadrant")]}" 
                                        data-target="${kunik}.${$(this).data("quadrant")}.evaluation${inputNb[$(this).data("quadrant")]}" 
                                        data-quadrant="${$(this).data("quadrant")}">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </div>`;
                $("#"+$(this).data("quadrant")+"Inputs<?= $kunik ?>").append(input);
            });


            sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema" : {
                    "title" : "Paramétrage du <?php echo $kunik ?> (Evaluation)",
                    "description" : "Indication des informations nécessaire pour l'évaluation",
                    "icon" : "fa-cog",
                    "properties" : {
                        objet : {
                            inputType : "text",
                            label : "Objet de l'évaluation",
                            value :  sectionDyf.<?php echo $kunik ?>ParamsData.objet
                        },
                        objectif : {
                            inputType : "text",
                            label : "Objectif de l'évaluation",
                            value : sectionDyf.<?php echo $kunik ?>ParamsData.objectif

                        },
                        quadrants : {
                            inputType : "lists",
                            label : "Les quadrants de l'évaluation",
                            entries: {
                                label: {
                                    type:"text",
                                    label:"Quadrant"
                                },
                                inputNb: {
                                    type:"text",
                                    label:"Minimum / Options(;)",
                                    placeholder: "Ex. 2 / option 1; option 2, ..."
                                }
                            },
                            
                        }
                    },
                    save : function (data) {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                            if(val.inputType == "properties")
                                tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                            else if(val.inputType == "array")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else
                                tplCtx.value[k] = $("#"+k).val();
                            
                            if(k=="quadrants"){
                                let quadrants = {};
                                $.each(data.quadrants, function(index, va){
                                    let quadrant = {label: va.label, inputNb: va.inputNb};
                                    quadrants["quadrant"+index] = quadrant;
                                });
                                tplCtx.value[k] = quadrants;
                            }

                            mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                        });
                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == undefined)
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                                location.reload();
                            } );
                        }

                    }
                }
            };

            //adds a line into answer
            $(".add<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
            });

            $(".edit<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
            });

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                //if no params config on the element.costum.form.params.<?php echo $kunik ?>
                //then we load default values available in forms.inputs.<?php echo $kunik ?>xxx.params
                //mylog.log(".editParams",sectionDyf.<?php echo $kunik ?>Params,calData);
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            });
        });
    </script>
    <?php } else {
        //echo "<h4 class='text-red'>evaluation works with existing answers</h4>";
    } ?>