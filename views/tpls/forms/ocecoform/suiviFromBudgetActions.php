<?php 
/*
pour chaque ligne de dépense , on peut 
[X] associer un maitre d'oeuvre et ces type de travaux
[X] le maitre d'oeuvre peut déclaré le degré d'avancement
[X] l'administrateur poeut déclarer le total versé
[X] system de recettage ou validation globale par ligne de depense
[X] lister des sous tache ou todos
[X] connecté les todos à la progression de la ligne concerné

[/] edit les todos
[/] connecté les todos à la progression globale
[/] assigné une date de fin à une todo
[/] assigné les todos à d'autres

[ ] system de validation des todos
[ ] associer une url d'un outil de suivi externe
[ ] no reload sur la colonne total 
[ ] no reload sur la validation 
[ ] contraindre les actions à des roles  
*/

if($answer){ 
	$copy = "aapStep1.depense";
	if( !empty($parentForm["params"][$kunik]["budgetCopy"]) ) 
		$copy = $parentForm["params"][$kunik]["budgetCopy"];
	else if( count(Yii::app()->session["budgetInputList"]) == 1 )
		$copy = array_keys( Yii::app()->session["budgetInputList"])[0];
	//var_dump($copy);
	$copyT = explode(".", $copy);
	$copyF = $copyT[0];
	$copy = $copyT[1];

	$budgetKey = $copyT[1];
	$answers = null;	
	//var_dump($budgetKey);
	if($wizard){
		if( $budgetKey ){
			if( isset($answer["answers"][$copyF][$budgetKey]) && count($answer["answers"][$copyF][$budgetKey])>0)
			$answers = $answer["answers"][$copyF][$budgetKey];
		} else if( isset($answer["answers"][$copyF][$budgetKey]) && count($answer["answers"][$copyF][$budgetKey])>0 )
			$answers = $answer["answers"][$copyF][$kunik];
	} else {
		if(!empty($budgetKey) && !empty($answer["answers"][$budgetKey]))
			$answers = $answer["answers"][$budgetKey];
		else if(isset($answer["answers"][$kunik]))
			$answers = $answer["answers"][$kunik];
	}

$actions = PHDB::find(Actions::COLLECTION, [
						"parentId" => (string)$answer['_id'], 
						"parentType" => Form::ANSWER_COLLECTION] );

//clear any existing todos 
if(isset($answers)){
	foreach ($answers as $ix => $a) {
		$answers[$ix]["todo"] = [];
	}
}
//reset todo with actions
foreach ($actions as $id => $a) {
	if( isset($a["group"]) ) {
		$todo = [
			"id" => (string)$a["_id"],
			"what" => $a["name"],
			"uid" => $a["idUserAuthor"],
			"created" => $a["created"]
		];
		if(isset($a["credits"]))
			$todo["price"] = $a["credits"];
		if($a["status"] == "done")
			$todo["done"] = 1;
		if(!empty($a["endDate"]))
			$todo["doneDate"] = $a["endDate"];
		$answers[$a["group"]]["todo"][] = $todo;
	} 
}
//overload existing todo and add to answerObj
if(isset($answers)){
	foreach ($answers as $ix => $a) {
		if(empty($answers[$ix]["todo"]))
			unset($answers[$ix]["todo"]);
		else 
			echo "<script>answerObj.answers.".$copyF.".".$budgetKey."[".$ix."].todo = ".json_encode($answers[$ix]["todo"]).";</script>";
	}
}

$editBtnL =  "";

$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

$paramsData = [ 
	"limitRoles" =>["worker","maitreoeuvre"]
];


if( !empty($parentForm["params"][$kunik]["tpl"]) ) 
	$paramsData["tpl"] =  $parentForm["params"][$kunik]["tpl"];
if( !empty($parentForm["params"][$kunik]["budgetCopy"]) ) 
	$paramsData["budgetCopy"] =  $parentForm["params"][$kunik]["budgetCopy"];
if( isset($parentForm["params"][$kunik]["limitRoles"]) ) 
	$paramsData["limitRoles"] =  $parentForm["params"][$kunik]["limitRoles"];
if( isset($parentForm["params"][$kunik]["useActions"]) ) 
	$paramsData["useActions"] =  $parentForm["params"][$kunik]["useActions"];

$communityLinks = Element::getCommunityByParentTypeAndId( $parentForm["parent"] );
$organizations = Link::groupFindByType( Organization::COLLECTION,$communityLinks,["name","links"] );

$orgs = [];
foreach ($organizations as $id => $or) {
	$roles = null;
	if( isset( $communityLinks[$id]["roles"] ) )
		$roles = $communityLinks[$id]["roles"];
	if( $paramsData["limitRoles"] && !empty($roles) )
	{
		foreach ($roles as $i => $r) 
		{
			if( in_array($r, $paramsData["limitRoles"]) ) 
				$orgs[$id] = $or["name"];
		}		
	}
}
//var_dump($orgs);exit;
$listLabels = array_merge($orgs);//Ctenat::$financerTypeList,

$properties = [
    "worker" => [
        "placeholder" => "Maitre d'oeuvre",
        "inputType" => "select",
        "list" => "workerList",
        "subLabel" => "Si financeur public, l’inviter dans la liste ci-dessous (au cas où il n’apparait pas demandez à votre référent territoire de le déclarer comme partenaire financeur"
    ],
    "workType" => [
        "inputType" => "text",
        "label" => "Type de travaux",
        "placeholder" => "Type de travaux",
        "rules" => [ "required" => true ]
    ],
    "poste" => [
        "inputType" => "text",
        "label" => "Contexte",
        "placeholder" => "Contexte",
        "rules" => [ "required" => true ]
    ],
    "todo" => [
        "inputType" => "text",
        "label" => "Todo Liste",
        "placeholder" => "Todo Liste",
        "rules" => [ "required" => true,"number" => true  ]
    ],
    "progress" => [
        "inputType" => "text",
        "label" => "Avancement Travaux",
        "placeholder" => "Avancement Travaux",
        "rules" => [ "required" => true,"number" => true  ]
    ],
    "total" => [
        "inputType" => "text",
        "label" => "Total",
        "placeholder" => "Total",
        "rules" => [ "required" => true,"number" => true ]
    ],
    "validation" => [
        "inputType" => "text",
        "label" => "Validation",
        "placeholder" => "Validation",
        "rules" => [ "number" => true  ]
    ]
];


	?>	
<?php
echo $this->renderPartial("survey.views.tpls.forms.cplx.suiviFromBudgetTable",
	                      [ 
	                        "form" => $form,
	                        "wizard" => true, 
	                        "answers"=>$answers,
	                        "answer"=>$answer,
                            "mode" => $mode,
                            "kunik" => $kunik,
                            "answerPath"=>$answerPath,
                            "key" => $key,
                            "titleColor" => $titleColor,
                            "properties" => $properties,
                            "copy" => $copy,
                            "copyF" => $copyF,
                            "budgetKey" => $budgetKey,

                            "label" => $label,
                            "editQuestionBtn" => $editQuestionBtn,
                            "editParamsBtn" => $editParamsBtn,
                            "editBtnL" => $editBtnL,
                            "info" => $info,
	                        //"showForm" => $showForm,
	                        "paramsData" => $paramsData,
	                        "canEdit" => $canEdit,
	                        "canAdminAnswer"  => $canAdminAnswer,
	                        //"el" => $el 
	                    ] ,true );

?>

<div class="form-worker" style="display:none;">

	<select id="worker" style="width:100%;">
		<option>Choisir unnn maitre d'oeuvre</option>
		<?php 

		foreach ($orgs as $v => $f) {
			echo "<option value='".$v."'>".$f."</option>";
		} ?>
	</select>
	<br>
	<span class="bold">Type de travaux effectués :</span> <br/>
	<input type="text" id="workType" name="workType" style="width:100%;">
	<br><br>
	<span class="bold">Organisme qui effectue les travaux , s'il n'existe pas, créez le et ajoutez le à la communauté ici 
		<a class="btn btn-primary">Ajouter un maitre d'oeuvre</a>
	</span>
	<br>
</div>

<div class="form-progress" style="display:none;">
	<select id="progress" style="width:100%;">
		<option> DEGRÉ D'AVANCEMENT DE CE CHANTIER </option>
		<?php foreach ([25,50,75,100] as $v => $f) {
			echo "<option value='".$f."'>".$f."%</option>";
		} ?>
	</select>
</div>

<div class="form-pay" style="display:none;">
  	<select id="status" style="width:100%;">
		<option> État du paiement </option>
		<?php foreach (["accompte" => "Accompte avancé",
						"partly" => "Facture payées",
						"total" => "Soldé" ] as $v => $f) {
			echo "<option value='".$v."'>".$f."</option>";
		} ?>
		</select>
		<br/><br/>

	<span class="bold">Total des Montants payés :</span> <br/>
	<input type="text" id="amount" name="amount" style="width:100%;">
</div>

<div class="form-validate-work" style="display:none;">
  	<select id="validWork" style="width:100%;">
		<option> Valider ces travaux </option>
		<?php foreach (["validated" => "Validé sans réserve",
						"reserved" => "Validé avec réserves",
						"refused" => "Non validé" ] as $v => $f) {
			echo "<option value='".$v."'>".$f."</option>";
		} ?>
		</select>
</div>

<style type="text/css">
	.strike {text-decoration: line-through;}
	input[type=checkbox]
	{
	  /* Double-sized Checkboxes */
	  -ms-transform: scale(0.6); /* IE */
	  -moz-transform: scale(0.6); /* FF */
	  -webkit-transform: scale(0.6); /* Safari and Chrome */
	  -o-transform: scale(0.6); /* Opera */
	  transform: scale(0.6);
	  padding: 10px;
	  vertical-align: middle;
	}
	#todo-list{ list-style: none}

	#todo-list li{ border-bottom: 1px solid #ccc;padding: 5px;}
	#todo-list li.todo{ background-color: lightyellow}
	#todo-list li.done{ background-color: lightcyan}
</style>
<div class="form-todo" style="display:none;">
	<!-- Url externe : <br/>
	<input type="text" id="todoUrl" name="todoUrl" style="width:100%;">
	<br/> -->
	<form id="form-add-todo" class="form-add-todo">
		<!-- <label for="todo">To do:</label> -->
		<?php 
		$uids = [];
		if(isset($el["links"]["members"])){
			foreach (array_keys($el["links"]["members"]) as $ix => $id) {
				$uids[] = new MongoId($id);
			}
		}
		$members = PHDB::find ( Person::COLLECTION, [ "_id" => [ '$in' => $uids ] ] , ["name","username"] );

		?>
		<!-- <textarea id="new-todo-item" class="new-todo-item col-xs-5" name="todo" placeholder="Quoi"></textarea> -->
		<div class="col-xs-12">
			<div class="col-xs-8">
				<label for="new-todo-item">Choses à faire</label>
				<input type="text" id="new-todo-item" class="new-todo-item  col-xs-12" name="todo" placeholder="Quoi"/>
			</div>
			<div class="col-xs-4">
				<label for="new-todo-item-price"> <i class='fa fa-money'></i> Cout de cette tache</label>
				<input type="text" id="new-todo-item-price" class="new-todo-item-price col-xs-12" name="price"  placeholder="Pour combien"/>
				<!-- <select id="votes" style="width:100%;">
					<option> VOTEZ pour cette partie </option>
					<?php foreach ([0,10,20,30,40,50,60,70,80,90,100] as $v => $f) {
						if($f == 0)
							$lbl = "Je ne suis pas pour!";
						else 
							$lbl = "Je suis pour à ".$f."%";
						echo "<option value='".$f."'>".$lbl."</option>";
					} ?>
				</select> -->
			</div>
		</div>
		<div class="col-xs-12 margin-top-10" >
			<div class="col-xs-6">
				<div class="col-xs-12">
					<label for="new-todo-item-date"> <i class='fa fa-calendar'></i> Fin estimé :</label>
					<input type="date" id="new-todo-item-date" class="new-todo-item-date col-xs-12" name="when" />
				</div>
				<div class="col-xs-12">
					<label for="new-todo-item-date"> <i class='fa fa-calendar'></i> Début estimé :</label>
					<input type="date" id="new-todo-item-date-end" class="new-todo-item-date-end col-xs-12" name="end" />
				</div>
			</div>
			<div class="col-xs-6">
				<label for="new-todo-item-who"> <i class='fa fa-user'></i> Personne(s) qui réalisera cette tache :</label>
				<select id="new-todo-item-who" class="new-todo-item-who col-xs-12" multiple>
					<?php 
					foreach ( $members as $id => $p ) {
						$selected = ( $id == Yii::app()->session["userId"] ) ? "selected" : "";
						echo "<option value='".$p["username"]."' data-id='".$id."' ".$selected." >".$p["name"]."</option>";
					} 
					?> 	
				</select>
			</div>
		</div>
		<div class="col-xs-12 margin-top-10 text-center" >
			<a id="add-todo-item" class="btn btn-primary" href="javascript:;"><i class='fa fa-plus'></i> Ajouter</a>
		</div>
		<div class="col-xs-12 margin-top-10 text-center" >
			<a data-filter="task" class="filter-todo btn btn-xs btn-default" href="javascript:;"><i class='fa fa-plus'></i> ALL </a>
			<a data-filter="todo" class="filter-todo btn btn-xs btn-default" href="javascript:;"><i class='fa fa-square-o'></i> TODO <span id="filterTodoCt">(0)</span></a>
			<a data-filter="done" class="filter-todo btn btn-xs btn-default" href="javascript:;"><i class='fa fa-check-square-o'></i> DONE <span id="filterDoneCt">(0)</span></a>
		</div>
	</form>

	<form id="form-todo-list">
		<ul id="todo-list" class="margin-top-10 col-xs-12 todo-list" style="border: 1px solid black"></ul>
	</form>
</div>


<?php 
if( isset($parentForm["params"]["financement"]["tpl"])){
	//if( $parentForm["params"]["financement"]["tpl"] == "tpls.forms.equibudget" )
		echo $this->renderPartial( "costum.views.".$parentForm["params"]["financement"]["tpl"] , 
		[ "totalFin"   => $total,
		  "totalBudg" => Yii::app()->session["totalBudget"]["totalBudget"] ] );
	// else 
	// 	$this->renderPartial( "costum.views.".$parentForm["params"]["financement"]["tpl"]);
}
 ?>

<script type="text/javascript">

if(typeof dyFObj.elementObjParams == "undefined")
	dyFObj.elementObjParams = {};

dyFObj.elementObjParams.budgetInputList = <?php echo json_encode( Yii::app()->session["budgetInputList"] ); ?>;
dyFObj.elementObjParams.workerList = <?php echo json_encode($orgs); ?>;


var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answer["answers"][$copy])) ? $answer["answers"][$copy] : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

$(document).ready(function() { 
	
	sectionDyf.<?php echo $kunik ?> = {
		"jsonSchema" : {	
	        "title" : "Plan de Financement",
            "icon" : "fa-money",
            "text" : "Décrire ici les financements mobilisés ou à mobiliser. Les coûts doivent être en <b>hors taxe</b>.",
	        "properties" : <?php echo json_encode( $properties ); ?>,
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    urlCtrl.loadByHash(location.hash);
	                } );
	            }

	    	}
	    }
	};

	sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "<?php echo $kunik ?> config",
	        "icon" : "fa-cog",
	        "properties" : {
	            // financerTypeList : {
	            //     inputType : "properties",
	            //     labelKey : "Clef",
	            //     labelValue : "Label affiché",
	            //     label : "Liste des type de Fiannceurs",
	            //     values :  sectionDyf.<?php echo $kunik ?>ParamsData.financerTypeList
	            // } , 
	            limitRoles : {
	                inputType : "array",
	                label : "Liste des roles financeurs",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.limitRoles
	            },
	            tpl : {
	                label : "Sub Template",
	                value :  sectionDyf.<?php echo $kunik ?>ParamsData.tpl
	            },
	            budgetCopy : {
	            	label : "Input Bugdet",
	            	inputType : "select",
	            	options :  dyFObj.elementObjParams.budgetInputList
	            },
	            useActions : {
	                inputType : "checkboxSimple",
                    label : "Generer des actions de communecter ",
                    subLabel : "Permet d'utiliser le DDA et OCECO pour suivre et comptabiliser l'avancement",
                    params : {
                        onText : "Oui",
                        offText : "Non",
                        onLabel : "Oui",
                        offLabel : "Non",
                        labelText : "Generer des actions de communecter"
                    },
                    checked : sectionDyf.<?php echo $kunik ?>ParamsData.useActions
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	        		if(val.inputType == "array")
	        		 	tplCtx.value[k] = getArray('.'+k+val.inputType);
	        		else if(val.inputType == "properties")
	        		 	tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
	        		else
	        		 	tplCtx.value[k] = $("#"+k).val();
	        	});
	            mylog.log("save tplCtx",tplCtx);
	            
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    urlCtrl.loadByHash(location.hash);
	                } );
	            }

	    	}
	    }
	};


    mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

    //adds a line into answer
    $(".add<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
    });

    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
    });

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    
    $('.btnValidateWork').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.budgetpath = $(this).data("budgetpath");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.form = $(this).data("form");
		prioModal = bootbox.dialog({
	        message: $(".form-validate-work").html(),
	        title: "Validation des travaux",
	        show: false,
	        buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {
			            
			        	var formInputsHere = formInputs;
			        	tplCtx.path = "answers";
			        	if( notNull(formInputs [tplCtx.form]) )
			        		tplCtx.path = "answers."+tplCtx.form;    

			        	tplCtx.path = tplCtx.path+"."+tplCtx.budgetpath+"."+tplCtx.pos+".validFinal";	   
			        	
			        	var today = new Date();
						today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();	
			            tplCtx.value = { 
		        			valid : $(".bootbox #validWork").val(),
		        			user : userId,
		        			date : today
		        		};
		        		delete tplCtx.pos;
		        		delete tplCtx.budgetpath;
				    	mylog.log("btnValidateWork save",tplCtx);
				  	 	dataHelper.path2Value( tplCtx, function(){
				  	 		saveLinks(answerObj._id.$id,"workValidated",userId,closePrioModalRel);
				  	 	} );
				  	 }		
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: closePrioModal
                }
              },
		        onEscape: closePrioModal
	    });
	    prioModal.modal("show");
	});

    $('.btnWorker').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.budgetpath = $(this).data("budgetpath");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.form = $(this).data("form");
		prioModal = bootbox.dialog({
	        message: $(".form-worker").html(),
	        title: "Choix du maitre d'oeuvre",
	        show: false,
	        buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {
			            
			        	var formInputsHere = formInputs;
			        	tplCtx.path = "answers";
			        	if( notNull(formInputs [tplCtx.form]) )
			        		tplCtx.path = "answers."+tplCtx.form;    

			        	tplCtx.path = tplCtx.path+"."+tplCtx.budgetpath+"."+tplCtx.pos+".worker";   

			        	var today = new Date();
						today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();	
			            tplCtx.value = { 
		        			id : $(".bootbox #worker").val(),
		        			name : $(".bootbox #worker option:selected").text(),
		        			workType : $(".bootbox #workType").val(),
		        			user : userId,
		        			date : today
		        		};
		        		delete tplCtx.pos;
		        		delete tplCtx.budgetpath;
				    	
				    	mylog.log("btnFinancer save",tplCtx);

				  	 	dataHelper.path2Value( tplCtx, function() {
				  	 		saveLinks(answerObj._id.$id,"worker", userId,closePrioModal );
				  	 	} );
			        }		
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: closePrioModal
                }
              },
		        onEscape: closePrioModal
	    });
	    prioModal.modal("show");
	});
	$('.btnProgress').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.budgetpath = $(this).data("budgetpath");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.form = $(this).data("form");
		prioModal = bootbox.dialog({
	        message: $(".form-progress").html(),
	        title: "Degrés d'avancement",
	        show: false,
	        buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {
			            
			        	var formInputsHere = formInputs;
			        	tplCtx.path = "answers";
			        	if( notNull(formInputs [tplCtx.form]) )
			        		tplCtx.path = "answers."+tplCtx.form;    

			        	tplCtx.path = tplCtx.path+"."+tplCtx.budgetpath+"."+tplCtx.pos+".progress";   

			        	var today = new Date();
						today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();	
			            tplCtx.value = $(".bootbox #progress").val();
		        		delete tplCtx.pos;
		        		delete tplCtx.budgetpath;
				    	mylog.log("btnProgress save",tplCtx);
				  	 	dataHelper.path2Value( tplCtx, function(){
				  	 		saveLinks(answerObj._id.$id,"workProgress",userId,closePrioModalRel);
				  	 	} );
			        }		
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: closePrioModal
                }
              },
		        onEscape: closePrioModal
	    });
	    prioModal.modal("show");
	});

	$('.btnPay').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.budgetpath = $(this).data("budgetpath");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.form = $(this).data("form");
		mylog.log("btnPay open",tplCtx);
		prioModal = bootbox.dialog({
	        message: $(".form-pay").html(),
	        title: "Statuts des réglements",
	        show: false,
	        buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {
			            
			        	var formInputsHere = formInputs;
			        	tplCtx.path = "answers";
			        	if( notNull(formInputs [tplCtx.form]) )
			        		tplCtx.path = "answers."+tplCtx.form;    

			        	tplCtx.path = tplCtx.path+"."+tplCtx.budgetpath+"."+tplCtx.pos+".payed";   
						
			        	var today = new Date();
						today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();	
			            tplCtx.value = { 
		        			status : $(".bootbox #status").val(),
		        			amount : $(".bootbox #amount").val(),
		        			user : userId,
		        			date : today
		        		};
		        		delete tplCtx.pos;
		        		delete tplCtx.budgetpath;
				    	mylog.log("btnPay save",tplCtx);
				  	 	dataHelper.path2Value( tplCtx, function(){
				  	 		saveLinks(answerObj._id.$id,"workPayed",userId,closePrioModalRel);
				  	 	} );
			        }		
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: closePrioModal
                }
              },
		        onEscape: closePrioModal
	    });
	    prioModal.modal("show");
	});

	$('.btnTodo').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.budgetpath = $(this).data("budgetpath");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.form = $(this).data("form");
		mylog.log("btnTodo open",tplCtx);
		tplCtx.editTaskpos = null;

		prioModal = bootbox.dialog({
	        message: $(".form-todo").html(),
	        title: "Todo Liste",
	        size: "large",
	        show: false,
	        buttons: {
                close: {
                  label: "Fermer",
                  className: "btn-danger",
                  callback : closePrioModal
                }
              },
		        onEscape: closePrioModal
	    });
	    prioModal.on('shown.bs.modal', function (e) {
	    	prioModal.css({'background-color': '#000'} );
			$(".bootbox #add-todo-item").off().on('click', function(e){
				e.preventDefault();
				listObj.addItem()
			});
			$(".filter-todo").off().on('click', function(e){
				$("li.task").slideUp();
				$("li."+$(this).data('filter')).slideDown();
			});
			
			$(".bootbox #new-todo-item, .bootbox #new-todo-item-when,.bootbox #new-todo-item-who,.bootbox #new-todo-item-price ").off().on('keypress', function(e){
				var keycode;
				if (window.event) {keycode = window.event.keyCode;e=event;}
				else if (e){ keycode = e.which;}
				if( keycode=="13" )
					listObj.addItem();
			});


			todoPath = "answerObj.answers."; 
			if( notNull(formInputs [tplCtx.form]) )
				todoPath = "answerObj.answers."+tplCtx.form+"."; 
			todoPath += tplCtx.budgetpath+"["+tplCtx.pos+"].todo";
			mylog.log("build todos path ",todoPath);
			if( notNull( todoPath ) )
			{
				listObj.data = eval( todoPath );
				mylog.log("build actions",listObj.data);
				$.each(listObj.data, function(ix,todo) { 
					listObj.buildList(todo, ix );
				})
			}

			listObj.btnInit();
	    })
	    prioModal.modal("show");
	});

	
});
function closePrioModal(){
	prioModal.modal('hide');
	listObj.updateTodoProgress();
}
function closePrioModalRel	(){
	closePrioModal();
	urlCtrl.loadByHash(location.hash);
}
var listObj = {
	actions : <?php echo json_encode( $actions ); ?>,
	data : null,
	roomId : null,
	newRoom : function(line,callback) { 
        var params = { 
           	collection : "rooms",	
            parentId : answerObj._id.$id,
		    parentType : "answers",
		    status : "open",
		    name: "Todo List Ligne "+line		    
        };
        mylog.log("create action > create room line : ", line,params);
        ajaxPost(null, baseUrl+"/"+moduleId+'/element/save', params, 
        	function(data){
	            if(data.result){
	              	//toastr.success( "Room créé avec succès !");
	              	mylog.dir(data);
	              	listObj.baseObj.todo = [];
					listObj.roomId = data.id ;
					if(typeof callback == 'function') 
						callback();
	            }
	            else {
	              toastr.error(data.msg);
	            }
	            $.unblockUI();
	        },
	        function(){toastr.error( data.msg);},
	        "json");
    },
    newAction : function(line,todoObj) { 
    	mylog.log("create action : ", todoObj);
        var params = { 
           	collection : "actions",	
			idParentRoom : listObj.roomId,
			group : line,
            parentId : answerObj._id.$id,
		    parentType : "answers", 
		    status : "todo", 
		    name: todoObj.what,
		    email : userConnected.email,
    		idUserAuthor : userId
        };

        if(todoObj.when)
			params.startDate = todoObj.when;
		if(todoObj.end)
			params.endDate = todoObj.end;
		listObj.baseObj.todo.push(todoObj);
		mylog.log("create action > room line : ", line,params);
		ajaxPost(null, baseUrl+"/"+moduleId+'/element/save', params, 
			function(data){
	            if(data.result){
	              	//toastr.success( "Action créé avec succès !");
	              	mylog.dir(data);
	              	listObj.updateTodoProgress();
	            }
	            else {
	              toastr.error(data.msg);
	            }
	            $.unblockUI();
          	},
          	function(){toastr.error( data.msg);},
          	"json"
        );
    },
	addItem : function () { 
	
		var today = new Date();
		today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();	

		listObj.todo = { 
			what : $(".bootbox #new-todo-item").val(),
			uid : userId,
			who : userConnected.username,
			created : today
		};

		if ( $(".bootbox #new-todo-item-date").val() ) 
			listObj.todo.when = $(".bootbox #new-todo-item-date").val();
		if ( $(".bootbox #new-todo-item-date-end").val() ) 
			listObj.todo.end = $(".bootbox #new-todo-item-date-end").val();
		if( $(".bootbox #new-todo-item-price").val() )
			listObj.todo.price = $(".bootbox #new-todo-item-price").val();
		if($(".bootbox #new-todo-item-who").val())
			listObj.todo.who = $(".bootbox #new-todo-item-who").val() ; 

		mylog.log("addItem",listObj.todo, tplCtx.editTaskpos);
		
	  	listObj.buildList(listObj.todo, tplCtx.editTaskpos);
	  	listObj.btnInit();

	  	listObj.baseObj = ( notNull(tplCtx.form) && notNull(formInputs [tplCtx.form]) )  
	  				? answerObj.answers[tplCtx.form][tplCtx.budgetpath][tplCtx.pos] 
	  				: answerObj.answers[tplCtx.budgetpath][tplCtx.pos];
	  	
    	if( typeof listObj.baseObj.todo == "undefined"){
			//create room and 1st action
			listObj.newRoom( tplCtx.pos, function(){ listObj.newAction(tplCtx.pos, listObj.todo); } );
    	} else {
		    listObj.newAction( tplCtx.pos, listObj.todo )	
    	}
    	
		
	 	$(".bootbox #new-todo-item, .bootbox #new-todo-item-date, .bootbox #new-todo-item-date-end, .bootbox #new-todo-item-price").val("");
	},

	//todoArray au lieu de géré avec des li
	//avoir un array avec des objets + render
	buildList : function (todoObj, pos) {
		mylog.log("buildList ",todoObj, pos);
		var uid = (notNull( todoObj.uid)) ? todoObj.uid : userId;
		var who = "";
		if(!notNull( todoObj.who)) who = userConnected.name;
		else if(typeof todoObj.who == "object"){
			$.each(todoObj.who,function(i,w) { 
				if( who!= "") who +=", ";
			 who += w; })
		} else 
			who = todoObj.who;

		var whoids = "";
		var price = (notNull(todoObj.price)) ? todoObj.price : "";
		var when = (typeof todoObj.when == "undefined" || todoObj.when == "" ) ? "" : todoObj.when;

		if(!notNull( todoObj.created)) {
			var today = new Date();
			var created = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();	
		}
		var doneDate = (notNull( todoObj.doneDate)) ? " <br/><i class='fa fa-calendar-check-o'></i> "+todoObj.doneDate : "";
		var done = (notNull( todoObj.done ) && todoObj.done ) ? "task done " : "task todo";

		if(pos === null)
			pos = $(".liTodo").length;

		var checked = ( ( parseInt(todoObj.done) ) ? "checked" : "" );

		str = "<div class='col-xs-1'>"+
				"<a class='btn btn-xs btn-danger todo-item-delete' href='javascript:;'><i class='fa fa-times-rectangle'></i></a>"+
				" <a class='btn btn-xs btn-primary todo-item-edit' href='javascript:;'><i class='fa fa-pencil'></i></a>"+
			"</div>"+
			"<div class='col-xs-1'><input type='checkbox' name='todo-item-done' class='todo-item-done' "+checked+" /></div> " + 
		     "<div class='col-xs-10 liText' >"+todoObj.what +"</div>"+
		     "<div class='col-xs-12' >"+
		     	"<div class='col-xs-2'></div>"+
		     	"<div class='col-xs-2' style='font-size:0.8em'> <i class='fa fa-money'></i> "+price+"</div>"+
		     	"<div class='col-xs-3' style='font-size:0.8em'> <i class='fa fa-calendar-o'></i> "+when+doneDate+"</div>"+
		     	"<div class='col-xs-5' style='font-size:0.8em' > <i class='fa fa-user'></i> "+who+"</div>"+
		     "</div>";


		if(tplCtx.editTaskpos != null){
			$(".liText").eq(pos).parent().html(str);
			delete tplCtx.editTaskpos;
		}
		else
			$(".bootbox #todo-list").append("<li class='liTodo col-xs-12 "+done+"' data-pos='"+pos+"' data-uid='"+uid+"' data-created='"+created+"' data-who='"+who+"' data-whoids='"+whoids+"'  data-when='"+when+"' data-price='"+price+"' >"+str+"</li>");
	},

	deleteItem : function (e, item) {
	    e.preventDefault();
	    var taskPos = $(item).parent().parent().data("pos");
	    
	    listObj.baseObj = ( notNull(tplCtx.form) && notNull(formInputs [tplCtx.form]) )  
						  				? answerObj.answers[tplCtx.form][tplCtx.budgetpath][tplCtx.pos] 
						  				: answerObj.answers[tplCtx.budgetpath][tplCtx.pos];
		bootbox.confirm("Delete", 
		    function(result) {
		      if (result) {
		        $.ajax({
		              type: "POST",
		              url: baseUrl+"/dda/co/deleteaction/id/"+listObj.baseObj.todo[taskPos].id,
		          dataType: "json",
		              success: function(data){
		                if (data.result) {               
		                  	//toastr.success(data.msg);
		                  	$(item).parent().parent().fadeOut('slow', function() { 
						  		listObj.baseObj.todo.splice(taskPos,1);						 
							});
		                	$(item).parent().parent().remove();
		                	
		                	

		                } else {
		                  toastr.error(data.msg);
		                }
		            }
		        });
		      }
		});
	  
	},
	updateTodoProgress : function(){

		listObj.baseObj = ( notNull(tplCtx.form) && notNull(formInputs [tplCtx.form]) )  
			  				? answerObj.answers[tplCtx.form][tplCtx.budgetpath][tplCtx.pos] 
			  				: answerObj.answers[tplCtx.budgetpath][tplCtx.pos];
		var todoDone = 0;
			
		$.each(listObj.baseObj.todo, function(ix,todo) { 
			if(parseInt(todo.done) == 1)
				todoDone++;
		})
		mylog.log("listObj updateTodoProgress",todoDone,(listObj.baseObj.todo.length-todoDone),listObj.baseObj.todo.length,Math.floor(todoDone*100/listObj.baseObj.todo.length));

		$(".bootbox #filterTodoCt").html( "("+(listObj.baseObj.todo.length-todoDone)+")" );
		$(".bootbox #filterDoneCt").html( "("+todoDone+")" );
		
		$("#btnTodo"+tplCtx.pos).html( todoDone+"/"+listObj.baseObj.todo.length );
		$("#todoProgress"+tplCtx.pos).css( "width",Math.floor(todoDone*100/listObj.baseObj.todo.length)); 
		$("#todoPerc"+tplCtx.pos).html(Math.floor(todoDone*100/listObj.baseObj.todo.length)+"%");
	},
	editItem : function (e, item) {
	  e.preventDefault();
		tplCtx.editTaskpos = $(item).parent().parent().data("pos");
	  $(".bootbox #new-todo-item").val( $(item).parent().parent().find(".liText").text() );
	  $(".bootbox #new-todo-item-date").val( $(item).parent().parent().data("when") );
	  $(".bootbox #new-todo-item-price").val( $(item).parent().parent().data("price") );
	  $(".bootbox #new-todo-item-who option").prop("selected",false)
	  if($(item).parent().parent().data("who").indexOf(",")){
	  	whosT = $(item).parent().parent().data("who").split(",");
	  	$.each(whosT,function(i,u){
	  		$(".bootbox #new-todo-item-who option[value='"+u+"']").prop("selected",true)
	  	})
	  }else 
		  $(".bootbox #new-todo-item-who").val( $(item).parent().parent().data("who") );
	},

                           
	completeItem : function () { 
		tplCtx.editTaskpos = $(this).parent().parent().data("pos");
		var today = new Date();
		var now = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear()+ ' ' + today.getHours()+ ':' + today.getMinutes();	
		var taskPos = ""+$(this).parent().parent().data("pos");
		listObj.baseObj = ( notNull(tplCtx.form) && notNull(formInputs [tplCtx.form]) )  
	  				? answerObj.answers[tplCtx.form][tplCtx.budgetpath][tplCtx.pos] 
	  				: answerObj.answers[tplCtx.budgetpath][tplCtx.pos];
	  	var status = "open";
		if($(this).is(":checked") ){
			listObj.baseObj.todo[taskPos].done = 1;
			status = "done";
			listObj.baseObj.todo[taskPos].doneDate = now;
			$(this).parent().parent().removeClass("todo").addClass('done');
		} else {
			listObj.baseObj.todo[taskPos].done = 0;
			delete listObj.baseObj.todo[taskPos].doneDate;
			$(this).parent().parent().removeClass("done").addClass('todo');
		}

		//mettra jour lactions avec le bon status done
		var params = {
			collection : "actions",
			id : listObj.baseObj.todo[taskPos].id,
			path : "status",
			value : status
		}
		mylog.log("completeItem status",params);
  	 	dataHelper.path2Value( params,function(){} );

		listObj.buildList(listObj.baseObj.todo[taskPos], tplCtx.editTaskpos);
		listObj.btnInit();

		//alert(taskPos+"done"+todoDone+"/"+todos.length);
	},
	
	btnInit : function() { 
		$(".todo-item-delete").off().on('click', function(e){
			var item = this; 
			listObj.deleteItem(e, item)
		})
		$(".todo-item-edit").off().on('click', function(e){
			var item = this; 
			listObj.editItem(e, item)
		})
		$(".bootbox .todo-item-done").off().on('click', listObj.completeItem);
		listObj.updateTodoProgress();
		// $(".bootbox .liTodo").on('click', function(e){
		// 	var item = this; 
		// 	editItem(e, item)
		// })
	}
}




</script>
<?php } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>