<style type="text/css">
    .btn-group.depenseoceco {
        display: flex;
    }

    .progress-bar-nf {
        background-color: #c3ccba;
        color: #607D8B;
    }

    .depenseoceco .btn {
        flex: 1
    }

    .pr_div {
        display: flex;
    }

    .pr_div table {
        flex: 1
    }


    .mr-3, .mx-3 {
        margin-right: 1rem !important;
    }

    .media-body {
        -webkit-box-flex: 1;
        -ms-flex: 1;
        flex: 1;
    }

    .mb-0, .my-0 {
        margin-bottom: 0 !important;
    }

    .align-items-center {
        -webkit-box-align: center !important;
        -ms-flex-align: center !important;
        align-items: center !important;
    }
    .selected-price {
        background-color:#e5ffe5;
    }

    .oceco-styled-table {
        border-collapse: collapse;
        margin: 25px 0;
        font-size: 0.9em;
        font-family: sans-serif;
        min-width: 400px;
        box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
    }

    .oceco-styled-table thead tr {
        background-color: #009879;
        color: #ffffff;
        text-align: left;
    }

    .oceco-styled-table th,
    .oceco-styled-table td {
        padding: 12px 15px;
    }

    .oceco-styled-table tbody tr {
        border-bottom: 1px solid #dddddd;
    }

    .oceco-styled-table tbody tr:nth-of-type(even) {
        background-color: #f3f3f3;
    }

    .oceco-styled-table tbody tr:nth-of-type(odd) {
        background-color: #e5ffe5;
    }

    .oceco-styled-table tbody tr:last-of-type {
        border-bottom: 2px solid #009879;
    }

    .oceco-styled-table tbody tr.active-row {
        font-weight: bold;
        color: #009879;
    }

    .oceco-styled-table tbody.oceco-blanc-table tr{
        background-color: #fff;
    }

    .btn-group.special {
        display: flex;
    }

    .special .btn {
        flex: 1
    }

    .financer-image {
        display: inline-block;
        overflow: hidden;
        width: 35px;
        height: 35px;
        border-radius: 25%;
        vertical-align: middle;
    }

    .budgetdropdown {

    }

    .budgetdropdown .dropdown-menu .dropdown-item{
        border-bottom: 1px solid rgba(0, 0, 0, 0.1);
        padding: 15px 15px;
        font-size: 14px;
        color: #8a9299;
    }

    .budgetdropdown .dropdown-menu .dropdown-item i{
        margin-right: 15px;
        display: inline-block;
    }

    .budgetdropdown .dropdown-menu .dropdown-item:hover{
        border-bottom: 1px solid rgba(0, 0, 0, 0.1);
        padding: 15px 15px;
        font-size: 14px;
        color: #000;
    }
</style>

<?php
$sumDepense = array_reduce($answer["answers"]["aapStep1"]["depense"], function($carry, $item)
{
    if(isset($item["price"])){
        return $carry + (int)$item["price"];
    }
});

$sumFinancement = array_reduce(array_column($answer["answers"]["aapStep1"]["depense"], "financer"), function($carry, $item)
{
    return $carry + array_sum(array_column($item , "amount"));
});

$unlimitedFinancing = false;
if(isset($parentForm["params"]["unlimitedFinancing"]) && filter_var($parentForm["params"]["unlimitedFinancing"] , FILTER_VALIDATE_BOOLEAN )){
    $unlimitedFinancing = true;
}

$financeAllLabel = "<label class='mr-3 ml-3' style='font-size: 15px !important;'>Aucun depense</label>";
if($sumDepense != 0 && $sumDepense == $sumFinancement && !$unlimitedFinancing){
    $financeAllLabel = "<label class='mr-3 ml-3' style='font-size: 15px !important;'>Financement completé </label>";
} else if ($sumDepense != 0 ){
    $financeAllLabel = '<a type="button" class="FinanceAllLine dropdown-item" data-id="'.$answer["_id"].'" data-budgetpath="depense" data-form="aapStep1" > <i class="fa fa-money"></i> Financer l\'ensemble</a>';
}

 ?>
<div class="single_funding <?= (empty($answer["answers"]["aapStep1"]["depense"]) && $mode != "fa") ? "hidden" : "" ?>">
    <div class="funding_header padding-15">
        <h4>Financement
            <?php
            if (!empty($parentForm["crowdfunding"]) && $parentForm["crowdfunding"] && empty(Yii::app()->session['userId'])){
                ?>
                <button class="btn btn-sm copyfinancementstandalone pull-right" data-link='' ><i class="fa fa-link"></i> participer au cofinancement </button>
                <?php
            }
            if($mode == "w"){ ?>
            <span class="dropdown margin-left-30 budgetdropdown">


                <button class="btn btn-secondary dropdown-toggle margin-bottom-10" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-ellipsis-v "></i>
                </button>
                <div class="dropdown-menu pull-right" aria-labelledby="dropdownMenuButton">
                    <?php if ($mode == "w") echo $editBtnLDepense; ?> <?php if ($mode == "fa") echo $editQuestionBtn.$editParamsBtn; if( $mode == "w"){ echo $financeAllLabel ;} ?>

                    <?php
                    if ($isSuperAdmin || $canAdminAnswer){
                            if (isset($paramsData["envelope"]) && $paramsData["envelope"] == 'true'){
                        ?>
                                <a class="envelopeparams dropdown-item" data-id='<?= $parentForm["_id"] ?>' data-collection='<?= Form::COLLECTION ?>' data-path='params.envelopelist.<?= (string)date("Y") ?>'><i class="fa fa-envelope"></i> Configurer l'enveloppe</a>
                        <?php
                            }
                        ?>
                        <a class="copyfinancementstandalone dropdown-item" data-link='' ><i class="fa fa-link"></i> Copier lien standalone</a>
                        <?php
                    }
                    ?>
                </div>
            </span>
            <?php } ?>


        </h4>
    </div>
    <div class="funding_body">
        <div class="panel-body no-padding">
            <?php
            if (!empty($answer["answers"]["aapStep1"]["depense"]))
            {

                $icont = 1;
                foreach ($answer["answers"]["aapStep1"]["depense"] as $depId => $dep)
                {
                    $totalde = 0;
                    $totalfi = 0;
                    if(!empty($dep)){
                    ?>

                    <?php
                    if (!isset($dep["price"]) || $dep["price"] == "" || $dep["price"] == null)
                    {
                        $dep["price"] = 0;
                    }
                    if (isset($dep["price"]) && is_numeric($dep["price"]))
                    {
                        $totalfi = isset($dep["financer"]) ? array_sum(array_column($dep["financer"] , "amount")) : 0;
                        $totalde = isset($dep["payement"]) ? array_sum(array_column($dep["payement"] , "amount")) : 0;
                        if((int)$dep["price"] != 0){
                            $percent = round(($totalfi * 100) / (int)$dep["price"]);
                            $depensed = round(($totalde * 100) / (int)$dep["price"]);
                        } else {
                            $percent = 0;
                            $depensed = 0;
                        }
                    }
                    else
                    {
                        $totalfi = 0;
                        $percent = 0;
                        $depensed = 0;
                    }

                    ?>


                    <div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding  <?php echo $icont % 2 === 0 ? "line-light" : "line-dark"; ?>">
                        <div class="col-md-5 col-sm-6" style="padding-top: 20px">
                            <h3 class="widget-title" style="font-size: 17px"><?php echo @$dep["poste"]; ?></h3>
                            <div>
                                <?php
                                if($unlimitedFinancing || (int)$dep["price"] != $totalfi ) {
                                    if (!empty(Yii::app()->session['userId']) && ($mode == 'w')) {
                                        echo '<button style="border-color: #6e899e; color:#6c7a89; font-size :14px" type="button" id="btnFinancer' . ($icont - 1) . '" data-id="' . $answer["_id"] . '" data-budgetpath="depense" data-form="aapStep1" data-pos="' . ($icont - 1) . '" data-name="' . @$dep["poste"] . '" class="btn btn-xs rebtnFinancer">Financer cette ligne</button>';
                                    }
                                }
                                 ?>
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-6" style="padding-top: 20px;">
                            <h3 class="widget-title" style="font-size: 17px;"><?php
                                if (empty($dep["price"])){
                                    $dep["price"] = 0;
                                }
                                if (!empty($dep["price"] || $dep["price"] == 0) && is_numeric($totalfi))
                                {
                                    echo rtrim(rtrim(number_format($totalfi, 3, ".", " ") , '0') , '.');
                                } ?><i class="fa fa-euro"></i> /

                                <span style="color: ">
                                <?php
                                if (empty($dep["price"] )){
                                    $dep["price"] = 0;
                                }
                                if (!empty($dep["price"] || $dep["price"] == 0) && is_numeric($dep["price"]))
                                {
                                    echo rtrim(rtrim(number_format($dep["price"], 3, ".", " ") , '0') , '.');
                                } ?>

                                <i class="fa fa-euro"></i>
                                 </span>

                                <span style="color: #6c7a89">
                                (<?php
                                    if (empty($dep["price"])){
                                        $dep["price"] = 0;
                                    }
                                    if (!empty($dep["price"] || $dep["price"] == 0) && is_numeric($totalde))
                                {
                                    echo rtrim(rtrim(number_format($totalde, 3, ".", " ") , '0') , '.');
                                } ?><i class="fa fa-euro"></i> dépensé)
                                </span>
                               </h3>

                            <div class="">
                                <div class="">

                                    <div>

                                    </div>
                                    <div style="clear: both;"> </div>
                                    <div class="" style="max-height: 72px;">

                                        <div class="progress" style="height: 23px">
                                            <?php
                                            if ($depensed < $percent){
                                                ?>
                                                <div class="progress-bar progress-bar-success2" role="progressbar" style="width:<?php echo (is_nan($depensed) ? 0 : ($depensed > 100 ? 100 : $depensed)) ?>%">
                                                    <?php if(!is_nan($depensed)){ ?>
                                                        <span style="font-size: 13px">Dépensé<?php echo is_nan($depensed) ? 0 : (!is_infinite($depensed) ? $depensed : 100) ?> %</span>
                                                    <?php } ?>
                                               </div>
                                                <div class="progress-bar progress-bar-success" role="progressbar" style="width:<?php echo (is_nan($percent - $depensed) ? 0 : (($percent - $depensed) > 100 ? 100 : ($percent - $depensed))) ?>%">
                                                    <?php if(!is_nan($percent - $depensed)){ ?>
                                                        <span style="font-size: 13px">Financé<?php echo is_nan($percent - $depensed) ? 0 : (!is_infinite($percent - $depensed) ? $percent - $depensed : 100) ?> %</span>
                                                    <?php } ?>
                                                </div>

                                                <?php
                                            } elseif($depensed = $percent && !is_infinite($percent) && !is_nan($percent)) {
                                                ?>
                                                <div class="progress-bar progress-bar-success" role="progressbar" style="width:<?php echo ($percent > 100 ? 100 : $percent) ?>%">
                                                    <?php if(!is_nan($depensed)){ ?>
                                                        <span style="font-size: 13px">Depensé<?php echo !is_infinite($percent) ? $percent : 100  ?> %</span>
                                                    <?php } ?>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="progress-bar progress-bar-success2" role="progressbar" style="width:<?php echo ($percent > 100 ? 100 : $percent) ?>%">
                                                    <?php if(!is_nan($depensed)){ ?>
                                                        <span style="font-size: 13px">Financé<?php echo is_nan($percent) ? 0 : (!is_infinite($percent) ? $percent : 100) ?> %</span>
                                                    <?php } ?>
                                                </div>
                                                <div class="progress-bar progress-bar-nf" role="progressbar" style="width:<?php echo (is_nan($depensed - $percent) ? 100 : (($depensed - $percent) > 100 ? 100 : $percent)) ?>%">
                                                    <span style="font-size: 13px">Non financé <?php echo is_nan($depensed - $percent) ? 100 : (!is_infinite($depensed - $percent) ? $depensed - $percent : 100) ?> %</span>
                                                </div>

                                                <?php
                                            }
                                            ?>

                                            <div class="progress-bar progress-bar-nf" role="progressbar" style="width:<?php echo is_nan($percent) ? 100 : abs(100 - $percent) ?>%">
                                                <span style="font-size: 13px">Non financé <?php echo is_nan($percent) ? 100 : (100 - ($percent)) ?> %</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="widget-controls hidden">
                                        <a href="#" class="widget-control-right"><span class="fa fa-times"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12  padding-10">
                            <?php
                            if(isset($dep["financer"])) {
                                $tds = "<div style=''  class='table-responsive pr_div " . $answer["_id"] . "fitable-" . $depId . "'> <table class='table table-bordered table-hover  directoryTable oceco-styled-table'><tbody class='oceco-blanc-table'>";
                                foreach ($dep["financer"] as $ix => $fin) {
                                    $datafin = "";
                                    $datafinhref = "";
                                    $datafinname = "";
                                    $datafinsrc = Yii::app()->baseUrl.Yii::app()->getModule("graph")->getAssetsUrl() . "/images/thumbnail-organizations.png";
                                    $datafints = "";

                                    if (!empty($fin["id"]) && !empty($organizations[$fin["id"]])) {
                                        $datafinhref = "#page.type.".$organizations[$fin["id"]]["collection"].".id.".$fin["id"];
                                        $datafinname = @$organizations[$fin["id"]]["name"];
                                        if(isset($organizations[$fin["id"]]["profilImageUrl"])){
                                            $datafinsrc = $organizations[$fin["id"]]["profilImageUrl"];
                                        }
                                        $datafin .= ' data-finid="' . $fin["id"] . '"';
                                    } else if (isset($fin["name"]) && !empty($fin["email"])) {
                                        $datafinname = $fin["name"];
                                        $datafints = $fin["email"];
                                        $datafin .= 'data-finname="' . $fin["name"] . '" data-finmail="' . $fin["email"] . '"';
                                    } else {
                                        $datafinhref = "#page.type.".$context["collection"].".id.".$context["_id"];
                                        $datafinname = $context["name"];
                                        if(isset($context["profilImageUrl"])){
                                            $datafinsrc = $context["profilImageUrl"];
                                        }
                                        $datafints = "(Auto-financement)";
                                        $datafin .= ' data-finid="' . $context["_id"] . '"'.'data-name="' . $context["name"].'"';
                                    }

                                    $tds .= "<tr>";
                                    $tds .= "    <td class='col-xs-4'>";
                                    $tds .= "        <h5> <a class='lbh-preview-element' href='" . $datafinhref . "' > <img src='" . $datafinsrc . " ' class='financer-image mr-2' alt='" . $datafinname . "'> " . $datafinname . " </h5></a> <span class='text-secondary'>" . $datafints . " </span>";
                                    $tds .= "    </td>";
                                    $tds .= "<td  class='col-xs-5'>";
                                    if (!empty($fin["line"])) {
                                        $tds .= $fin["line"];
                                        $datafin .= 'data-finfond="' . $fin["line"] . '"';
                                    }
                                    $tds .= "</td>";

                                    $tds .= "<td class='col-xs-2'>";
                                    if (!empty($fin["amount"])) {
                                        $tds .= $fin["amount"] . "€";
                                        //$totalAmountFunded += (int)$fin["amount"];
                                        $datafin .= 'data-finamount="' . $fin["amount"] . '"';

                                    }
                                    $tds .= "</td>";

                                    $tds .= "<td class='col-xs-2'>";
                                    if (!empty($fin["amount"]) && isset($dep["price"])) {

                                        $percent = $fin["amount"] * 100 / (int)$dep["price"];
                                        $tds .= " <span class='pull-right label label-primary'>" . (is_nan($percent) ? 0 : floor($percent)) . "%</span>";
                                    }
                                    $tds .= "</td>";
                                    if( $mode == "w"){
                                    $tds .= '<td class="py-3"><div class="esti-btn">
                                                <button type="button"  class="td-datetimelabel2  fibtnedit" data-id="' . $answer['_id'] . '" data-uid="' . $ix . '" ' . $datafin . ' data-key="depense" data-form="' . $step["step"] . '" data-pos="' . $depId . '" data-pos-financer="' . $ix . '" ><i class="fa fa-pencil"></i></button><span class = "dv"></span>
                                                <button type="button" class="td-datetimelabel2  fibtndelete" data-id="' . $answer['_id'] . '" data-uid="' . $ix . '"  data-key="depense" data-form="' . $step["step"] . '" data-pos="' . $depId . '" data-pos-financer="' . $ix . '"><i class="fa fa-trash"></i></button>
                                            </div></td>';
                                    }else{
                                        $tds .= '<td class="py-3"><div class="esti-btn">
                                            </div></td>';
                                    }

                                    $tds .= "</tr>";
                                }

                                //$totalFunded += $totalAmountFunded;

                                $tds .= "</tbody></table></div>";

                                echo $tds;
                            }
                            ?>

                        </div>

                    </div>
                    <?php $icont++;
                    }
                }
            }

            ?>
        </div>
    </div>
</div>





<?php
$percol = "warning";
$financedPercentage = (!empty($total)) ? $totalFunded * 100 / $total: 0;
if( $financedPercentage >= 100 ){
	$percol = "success";
}
?>
<script type="text/javascript">
	$(document).ready(function() {
        coInterface.bindLBHLinks();
	    $( '.fipull-me' ).each(function( index ) {
		if (typeof $('.'+$(this).data('pull'))[0] != "undefined") {
	  		$('.'+$(this).data('pull'))[0].style.display = localStorage[$(this).data('pull')];
	  	}

	  	if (typeof $('.'+$(this).data('pull'))[0] != "undefined") {
		  	if (($('.'+$(this).data('pull'))[0].style.display == "flex" || $('.'+$(this).data('pull'))[0].style.display == "") && parseInt($(this).data('len')) > 0) {
		  		$(this).html("Reduire la liste("+$(this).data('len')+")");
		  	}else if ($('.'+$(this).data('pull'))[0].style.display == "none"  && parseInt($(this).data('len')) > 0) {
		  			$(this).html("Voir la liste("+$(this).data('len')+")");
		  	} else {
		  			$(this).html("aucun resultat");
		  	}
		}
	});

	    $('.fipull-me').click(function() {
	    var  thisdatapull = $(this).data('pull');
	    var thisbtnpull = $(this);
	    $('.'+$(this).data('pull')).slideToggle("fast", function(){
	    	var trtr = $('.'+thisdatapull)[0].style.display;

	    	localStorage.setItem(thisdatapull, $('.'+thisdatapull)[0].style.display);

	    	if (($('.'+thisbtnpull.data('pull'))[0].style.display == "flex" || $('.'+thisbtnpull.data('pull'))[0].style.display == "") && parseInt(thisbtnpull.data('len')) > 0) {
	  			thisbtnpull.html("Reduire la liste("+thisbtnpull.data('len')+")");
	  		} else if ($('.'+thisbtnpull.data('pull'))[0].style.display == "none" && parseInt(thisbtnpull.data('len')) > 0) {
	  			thisbtnpull.html("Voir la liste("+thisbtnpull.data('len')+")");
	  		} else {
	  			thisbtnpull.html("aucun resultat");
	  		}

	    });
	  });

        $(".copyfinancementstandalone").off().on("click", function () {
            var sampleTextarea = "";
            if(window.location.href.includes('/costum/')) {
                sampleTextarea = baseUrl + "/costum/co/index/slug/" + costum.slug + "#answer.answer.id." + coForm_FormWizardParams.answer["_id"]["$id"] + ".form." + aapObj.form["_id"]["$id"] + ".step.aapStep3.input.financer.view.custom"; //save main text in it
            } else {
                sampleTextarea = baseUrl + "/#answer.answer.id." + coForm_FormWizardParams.answer["_id"]["$id"] + ".form." + aapObj.form["_id"]["$id"] + ".step.aapStep3.input.financer.view.custom"; //save main text in it
            }
            window.open(sampleTextarea, "_blank");
        });
	});
</script>
