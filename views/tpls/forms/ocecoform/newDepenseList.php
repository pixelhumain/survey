<?php
    HtmlHelper::registerCssAndScriptsFiles(array(
        //'/plugins/jsontotable/JSON-to-Table.min.1.0.0.js',
        '/plugins/jqueryTablesorter/jquery.tablesorter.min.js',
        '/plugins/jqueryTablesorter/jquery.tablesorter.widgets.js',
        '/plugins/jqueryTablesorter/widget-output.min.js',
        //'/plugins/jqueryTablesorter/widget-grouping.js'
    ), Yii::app()->request->baseUrl);
    HtmlHelper::registerCssAndScriptsFiles(array(
    '/js/financementlog.js',
    '/css/financementlog.css',
    ), Yii::app()->getModule(Survey::MODULE)->getAssetsUrl());

?>
<style type="text/css">
    @font-face{
        font-family: "montserrat";
        src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/montserrat-regular.woff") format("woff"),
        url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/montserrat-regular.ttf") format("ttf")
    }.mst{font-family: 'montserrat'!important;}

    @font-face{
        font-family: "CoveredByYourGrace";
        src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/CoveredByYourGrace.ttf")
    }.cbyg{font-family: 'CoveredByYourGrace'!important;}

    :root {
        --pfpb-primary-green-color: #9fbd38 ;
        --pfpb-primary-grey-color: #808180;
        --pfpb-secondary-grey-color: #e6e1e1;
        --pfpb-tertiary-grey-color: #e4e4e4;
        --pfpb-primary-blue-color: #2271dd;
        --pfpb-secondary-blue-color: #0096D1;
        --pfpb-tertiary-blue-color: #3EBDC6;
        --pfpb-primary-red-color: rgb(238,122,122);
        --pfpb-secondary-red-color: #f1e2e2;
        --pfpb-primary-purple-color: <?= isset($configForm["campagne"][array_key_first($configForm["campagne"])]["campColor"]) ? $configForm["campagne"][array_key_first($configForm["campagne"])]["campColor"] : "#4623c9" ?>;
    }

    .ftlcase {
        color: var(--pfpb-primary-purple-color);
    }

    .contentInformation {
        background-color: rgba(223,234,247,0.5);
        display: flex;
        flex-wrap: wrap;
        align-items: flex-start;
        padding: 20px;
    }

    .newDepenseListCard {
        background-color: rgb(255,255,255);
        position: relative;
        font-family: "montserrat";
        padding: 10px 10px;
        margin: 10px;
        border-radius: 12px;
        width: 450px;
        box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 3px, rgba(0, 0, 0, 0.24) 0px 1px 2px;
        z-index: 99;
    }

    .newDepenseListCard:hover {
        position: absolute;
        font-family: "montserrat";
        padding: 10px 10px;
        margin: 10px;
        border-radius: 12px;
        width: 450px;
        z-index: 9999;
    }

    .newDL-entete {
        display: inline-flex;
        width: 100%;
    }

    .newDL-entete-image  {
        height: 100px;
        width: 120px;
    }

    .newDL-entete-image img {
        height: 100px;
        max-width: 120px;
        border-radius: 20px;
    }

    .newDL-entete-depense {
        position: relative;
        display: inline-flex;
        width: -webkit-fill-available;

    }

    .newDL-entete-date {
        display: inline;
        flex-direction: horizontal;
        width: 80px;
        text-align: center;
        min-height: 50px;
    }

    .newDL-entete-percentage {
        display: flex;
        flex-direction: vertical;
        flex-wrap: nowrap;
        width: 70px;
        text-align: left;
        height: 50px;
        position: relative;
        letter-spacing: -5px;
    }

    .newDL-entete-percentage .newDL-entete-perc {
        height: 50px;
        padding: 0;
        color : var(--pfpb-primary-blue-color);
        font-size: 30px;
        position: absolute;
        width: 50px;
        text-align: right;
        top: 15px;
        right: 20px;
    }

    .newDL-entete-percentage .newDL-entete-perc-sign {
        font-size: 15px !important;
        padding: 0;
        color : var(--pfpb-primary-green-color);
        width: 50px;
        text-align: right;
        position: absolute;
        height: 50px;
        top: 30px;
        right: 10px;
        font-weight: bold;
        stroke: 10px;
        z-index: 20;
    }

    .newDL-entete-date .newDL-entete-day {
        height: 30px;
        padding: 0;
        color : var(--pfpb-primary-green-color);
        font-size: 30px;
    }

    .newDL-entete-date .newDL-entete-month {
        color : var(--pfpb-secondary-blue-color);
        font-size: 15px;
    }

    .newDL-entete-date .newDL-entete-year {
        color : var(--pfpb-primary-grey-color);
        font-size: 10px;
    }

    .newDL-entete-date-sp:after {
        content: '';
        background: transparent;;
        border: 2px solid var(--pfpb-tertiary-grey-color);
        border-radius: 20px;
        height: 50px;
        position: absolute;
    }

    .newDL-entete-date-sp-financ {
        content: '';
        background: transparent;;
        border: 2px solid var(--pfpb-secondary-blue-color);
        border-radius: 20px;
        position: absolute;
        z-index: 8;
        bottom: 0;
    }

    .newDL-entete-date-sp-financ-container {
        height: 50px;
        position: relative;
    }

    .newDL-entete-info {
        min-height: 50px;
        width: 100%;
        padding-left: 12px;
    }

    .newDL-entete-info .newDL-entete-titre {
        color : var(--pfpb-primary-green-color);
        font-size: 16px;
        height: 30px;
        overflow: hidden;
        display: -webkit-box;
        -webkit-line-clamp: 1;
        line-clamp: 1;
        -webkit-box-orient: vertical;
        padding: 3px 0px;
        font-weight: bold;
    }

    .newDepenseListCard:hover .newDL-entete-info .newDL-entete-titre {
        color : var(--pfpb-primary-green-color);
        font-size: 16px;
        height : auto;
        max-height: 140px;
        overflow: hidden;
        display: -webkit-box;
        -webkit-line-clamp: 4;
        line-clamp: 4;
        -webkit-box-orient: vertical;
        padding: 3px 0px;
        font-weight: bold;
    }

    .newDL-entete-price {
        width: -webkit-fill-available;
        color: var(--pfpb-secondary-blue-color);
    }

    .newDL-entete-price .entete-financement {
        font-weight: bold;
        width: -webkit-fill-available;
        color: var(--pfpb-primary-blue-color);
    } {
        width: -webkit-fill-available;
        color: var(--pfpb-secondary-blue-color);
    }

    .newDepenseListCard[data-subtype="depense"] .entete-financement{
        display: none;
    }

    .entete-part , .entete-ftlpart{
        display: none;
    }

    .entete-part.active , .entete-ftlpart.active{
        display: block;
    }

    .newDL-entete-price {
        width: -webkit-fill-available;
        color: var(--pfpb-secondary-blue-color);
    }

    .newDL-body {
        min-height: 0px;
        max-height: 0px;
        overflow: hidden;
        margin:0;
        padding: 0;
        -webkit-transition: all .5s ease;
        -o-transition: all .5s ease;
        -ms-transition: all .5s ease;
        transition: all .5s ease;
    }

    .newDL-body.active {
        overflow: hidden;
        -webkit-transition: all .5s ease;
        -o-transition: all .5s ease;
        -ms-transition: all .5s ease;
        transition: all .5s ease;
        max-height: 1000px;
        min-height: 100px;
    }

    .newDL-body.active .newDL-body-tabs-container {
        max-height: 1000px;
    }

    .newDL-body.active .newDL-body-tab:not(.active){
        display: none;
    }

    .newDL-body-tab{
        display: none;
    }

    .newDL-body-tab.active{
        display: block;
    }

    .newDL-entete-action button {
        margin: 10px 5px;
        background-color: rgba(241,241,241,0.87);
        width: 30px;
        height: 30px;
        padding: 5px;
        border-radius: 5px;
        color: rgba(183,183,183,0.87);
    }

    .newDL-entete-action button:hover ,
    .menutabs.active
    {
        background-color: rgba(220,231,245,0.87);
        width: 30px;
        height: 30px;
        padding: 5px;
        border-radius: 5px;
        color: var(--pfpb-primary-blue-color);
    }

    .newDL-entete-action button.depmodal[data-content="delete"] {
        margin: 10px;
        background-color: var(--pfpb-secondary-red-color);
        width: 30px;
        height: 30px;
        padding: 5px;
        border-radius: 5px;
        color: rgba(183,183,183,0.87);
    }

    .newDL-entete-action button.depmodal[data-content="delete"]:hover {
        width: 30px;
        height: 30px;
        padding: 5px;
        border-radius: 5px;
        color: var(--pfpb-primary-red-color);
    }

    .newDL-body-tabs-container {
        padding: 10px;
    }

    .newDL-body-description-tab{
        color: var(--pfpb-primary-grey-color);
    }

    .span-user{
        color: var(--pfpb-primary-blue-color);
    }

    .span-description{
        color: var(--pfpb-primary-blue-color);
    }

    .dropdown .dropdown-menu-orgaaction  {
        display: none;
        background: #e6e4e4;
        padding: 10px;
        border-radius: 20px;
        border: 2px solid var(--pfpb-secondary-grey-color);
    }

    .dropdown .dropdown-menu-orgaaction h3  {
        color : var(--pfpb-primary-grey-color);
        font-size: 15px;
    }

    .dropdown .dropdown-menu-orgaaction li  {
        color : var(--pfpb-primary-grey-color);
        font-size: 12px;
        list-style: none;
    }

    .dropdown.open .dropdown-menu-orgaaction  {
        display: block;
    }

    .dropdown-menu-orgaaction {
        right: 0;
        text-align: left;
        padding: 30px;
    }

    .dropdown-menu-orgaaction h3,
    .dropdown-menu-orgaaction .span-name
    {
        width: 100%;
        white-space: initial;
    }

    .dropdown-menu-orgaaction li .span-part {
        right: 20px;
        position: absolute;
        text-align: left;
        color: var(--pfpb-primary-blue-color);
        font-weight: bold;
    }

    .dropdown-menu-orgaaction .span-info {
        white-space: initial;
        font-size: 12px;
    }

    .contentInformation[data-subtype="finance"]  .contentFinanceAction {
        display: inline-flex;
    }

    div.mm-dropdown {
        border: 1px solid #ddd;
        width : auto;
        max-width: 250px;
        border-radius: 25px;
        margin: 5px;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        cursor: pointer;
        height: 30px;
        padding: 0px 0px;
    }

    div.mm-dropdown ul {
        list-style: none;
        position: absolute;
        z-index: 999999;
        background-color: var(--pfpb-tertiary-grey-color);
        border-radius: 15px;
        cursor: pointer;
        padding: 0;
        max-height: 250px;
        overflow-y: auto;
    }

    div.mm-dropdown ul li {
        padding: 5px;
        color: var(--pfpb-primary-grey-color);

    }

    div.mm-dropdown ul li:hover {
        background-color: rgba(241,241,241,0.87);
        color: var(--pfpb-primary-blue-color);
    }

    div.mm-dropdown div.textfirst {
        height: 30px;
        color: var(--pfpb-primary-blue-color);
        font-weight: bold;
        width : auto;
        max-width: 250px;
        margin-right: 10px;
        /*margin-left: 10px;*/
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        font-size: 14px;
    }

    div.mm-dropdown ul li {
        display: none;
        padding-left: 5px;
    }

    div.mm-dropdown ul li.main {
        display: block;
    }

    div.mm-dropdown img {
        width: 25px;
        height: 25px;
        margin: 0px 5px;
        border-radius: 25px;
    }

    div.mm-dropdown .textfirst img {
        margin: 2px;
        margin-left: px;
        margin-right 10px;
        border: 2px solid var(--pfpb-primary-green-color);
    }

    .newDepenseListCard .depmodal span{
        display: none
    }

    .newDepenseListCard .menutabs span{
        display: none
    }

    /*.newDepenseListCard:hover .depmodal[data-content="finance"]{
        width: 90px;
    }*/

    /*.newDepenseListCard:hover .depmodal[data-content="finance"] span{
        display: inline-block;
    }*/


    .statbutton-cofinance {
        display: none;
        text-align: center;
        justify-content: center;
        width: 200px !important;
        margin: 5px;
        background-color: var(--pfpb-primary-blue-color);
        border: 0;
        border-radius: 20px;
        padding: 5px 10px;
        color: white;
        font-weight: bold;
        width: 220px;
        padding: auto;
        height: 30px;
    }

    .statbutton-cofinance i , .statbutton-sellectall i {
        margin: 3px;
    }

    .statbutton-cofinance:disabled {
        background-color: var(--pfpb-tertiary-grey-color);
        color: var(--pfpb-primary-grey-color);
    }

    .statbutton-cofinance {
        z-index: 99999;
        display: inline-flex;
    }

    .newdepenselist .contentFinanceAction{
        display: none;
        flex-direction: horizontal;
        width: 0%;
        white-space: nowrap;
        -webkit-transition: all .5s ease;
        -o-transition: all .5s ease;
        -ms-transition: all .5s ease;
        transition: all .5s ease;
    }

    .newdepenselist.active .contentFinanceAction{
        display: inline-flex;
        width: 100%;
        -webkit-transition: all .5s ease;
        -o-transition: all .5s ease;
        -ms-transition: all .5s ease;
        transition: all .5s ease;
        justify-content: space-between;
        padding-right: 10px !important;
    }

    .orgbtn {
        margin: 5px 10px 5px -21px !important;
        background-color: var(--pfpb-primary-grey-color);
        border: 0;
        border-radius: 0 20px 20px 0 !important;
        padding: 5px 10px;
        color: white;
        font-weight: bold;
        width: 80px !important;
        height: 30px;
    }

    .dropdown-menu-orgaaction {
        position: absolute;
        z-index: 9999999 !important;
        opacity: 1;
    }

    .dropdown .dropdown-menu-orgaaction  {
        display: none;
        background: #f8f4f4;
        padding: 10px;
        border-radius: 20px;
        border: 2px solid var(--pfpb-secondary-grey-color);
    }

    .dropdown .dropdown-menu-orgaaction h3  {
        color : var(--pfpb-primary-grey-color);
        font-size: 15px;
    }

    .dropdown .dropdown-menu-orgaaction li  {
        color : var(--pfpb-primary-grey-color);
        font-size: 12px;
        list-style: none;
    }

    .dropdown.open .dropdown-menu-orgaaction  {
        display: block;
    }

    .dropdown-menu-orgaaction {
        right: 0;
        text-align: left;
        padding: 30px;
    }

    .dropdown-menu-orgaaction h3,
    .dropdown-menu-orgaaction .span-name
    {
        width: 100%;
        white-space: initial;
    }

    .dropdown-menu-orgaaction li .span-part {
        right: 20px;
        position: absolute;
        text-align: left;
        color: var(--pfpb-primary-blue-color);
        font-weight: bold;
    }

    .dropdown-menu-orgaaction .span-info {
        white-space: initial;
        font-size: 12px;
    }

    .orgaaction-btngroup {
        position: relative;
        display: flex;
        flex-direction: row;
        border-collapse: separate;
        flex-wrap: nowrap;
    }

    .newDepenseListCard.active .entete-part , .newDepenseListCard.active .entete-ftlpart{
        display: inline-block;
        font-size: 12px;
    }

    .newDepenseListCard.active .entete-part {
        color: var(--pfpb-primary-grey-color);
    }

    .newDepenseListCard.active .entete-ftlpart{
        color: var(--pfpb-primary-blue-color);
    }

    .newDepenseListCard.depinclude {
        opacity: 0.5;
    }

    .newDepenseListCard.depinclude:hover {
        opacity: 1;
    }

    .newDepenseListCard.depinclude .newDL-entete-titre ,
    .newDepenseListCard.depinclude .newDL-entete-percentage .newDL-entete-perc ,
    .newDepenseListCard.depinclude .newDL-entete-percentage .newDL-entete-perc-sign ,
    .newDepenseListCard.depinclude .newDL-entete-date-sp,
    .newDepenseListCard.depinclude .newDL-entete-price
    {
        color : grey !important;
    }

    .newDepenseListCard.depinclude .newDL-entete-date-sp-financ {
        border: 2px solid grey;
    }

    .input-group-addon {
        padding: 5px;
        height: 30px;
        padding: 5px;
        border-top-right-radius: 20px;
        border-bottom-right-radius: 20px;
        background: var(--pfpb-primary-green-color);
    }

    .input-group {
        padding: 5px;
        height: 40px;
    }

    input {
        padding: 5px;
        height: 30px;
        border-top-left-radius: 20px;
        border-bottom-left-radius: 20px;
        border: 2px solid var(--pfpb-primary-green-color);
    }

    .newDepenseListCard.active .depmodal[data-content="finance"] span {
        display: inline-block;
        color: var(--pfpb-primary-red-color);
    }

    .newDepenseListCard.active .depmodal[data-content="finance"] {
        width: 80px;
        color: var(--pfpb-primary-red-color);
    }

    .newDepenseListCard.active {
        border:1px solid var(--pfpb-primary-green-color);
    }

    .depenseselectfinancer {
        padding: 15px !important;
    }

    .depenseresume td, .depenseresume th {
        padding: 10px;
        border : 1px solid var(--pfpb-primary-grey-color);
    }

    .depenseresume th {
        color: var(--pfpb-primary-green-color);
        max-width: 300px;
        overflow: hidden;
        overflow-wrap: normal;
        text-overflow: ellipsis;
    }

    .multiselect-group-button-2{
        display: inline-flex;
        flex-direction: horizontal;
        white-space: nowrap;
        margin-left: 10px!important;
        padding: 5px;
    }

    .multiselect-group-button-2 .btn-group .btn:first-child{
        border-bottom-left-radius: 20px;
        border-top-left-radius: 20px;
    }

    .multiselect-group-button-2 .btn-group .btn:last-child{
        border-bottom-right-radius: 20px;
        border-top-right-radius: 20px;
    }

    .statbutton-sellectall{
        color : var(--pfpb-primary-blue-color);
        background: var(--pfpb-secondary-grey-color);
        text-align: center;
        justify-content: center;
        width: 200px !important;
        margin: 5px;
        border: 0;
        border-radius: 20px;
        padding: 5px 10px;
        font-weight: bold;
        width: 220px;
        padding: auto;
        height: 30px;
    }

    .finListCard {
        background-color: rgb(228,225,225);
        position: relative;
        font-family: "montserrat";
        padding: 10px 10px;
        margin: 10px;
        border-radius: 12px;
        box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 3px, rgba(0, 0, 0, 0.24) 0px 1px 2px;
    }

    .flist-entete-action  {
        display: flex;
        float: right;
    }


    .fl-entete{
        display: inline-flex;
        width: 100%;
        border: 1px solid #d7d6d6;
        margin-bottom: 10px;
        padding: 5px;
    }

    .fl-entete button {
        height: 30px;
        display: absolute;
        right: 0;
    }

    .newDL-entete-date .newDL-entete-day {
        color: #d7d6d6
    }

    .newDL-entete-price .entete-financement {
        color: var(--pfpb-primary-grey-color);
    }

    .ftlcase-info {
        font-size: 12px;
    }

    .flist-entete-action  {
        display: inline-flex;
        flex-direction: horizontal;
        flex-wrap: nowrap;
    }

    .flist-entete-act {
        display: inline-flex;
        flex-direction: horizontal;
        flex-wrap: nowrap;
    }

    .flist-entete-action button {
        margin-right: 5px;
        color: var(--pfpb-primary-grey-color);
    }

    .btn.depbtn-tab.menutabs .fa-exclamation {
        color: var(--pfpb-primary-red-color);
    }

    .finance .numintervglobal {
        float: right;
        margin-left: 5px;
        margin-top: 5px;
        color : var(--pfpb-primary-red-color);
        font-weight: bold;
    }

    .finance .numintervglobal:after {
        content: '!';
        font-weight: bold;
    }

    .dropdownMenuButton {
        position: absolute;
        top : 10px;
    }

    .flist-entete-act {
        height: min-content;
    }

    .dropdown-menu {
        position: absolute;
        top : 10px;
    }

    .line {
        fill: none;
        stroke: blue;
        stroke-width: 2px;
    }

    .axis text, .nav-axis text {
        font: 10px sans-serif;
    }

    .axis path, .axis line, .nav-axis path, .nav-axis line {
        fill: none;
        stroke: #000;
        shape-rendering: crispEdges;
    }

    .y.axis path , .nav-axis.x path {
        display: none;
    }

    .navigator .data {
        fill: #d1ddef;
        stroke-width: 0px;
    }

    .nav-viewport {
        stroke: #b2b1b6;
        stroke-width: 0.5px;
        fill: #5daee0;
        fill-opacity: 0.3;
    }


    .btn-group {
        margin-top: 5px;
    }

    .entete-financement-edit,
    .entete-financementftl-edit {
        font-weight: bold;
    }

    .entete-financement-edit input ,
    .entete-financementftl-edit input {
        border-radius: 15px;
        width: 140px;
        text-align: center;
    }

    .entete-financement-edit button,
    .entete-financementftl-edit button {
        border-radius: 15px;
        width: 90px;
        text-align: center;
        height: 30px;
        padding: 0px;
        margin: 0px;
        margin-right: 10px;
    }

    .entete-financement-edit button.cofinance-edit,
    .entete-financementftl-edit button.cofinanceftl-edit{
        color: var(--pfpb-secondary-blue-color);
        background-color: var(--pfpb-secondary-grey-color);
    }

    .entete-financement-edit button.cofinance-edit:disabled,
    .entete-financementftl-edit button.cofinanceftl-edit:disabled{
        color: var(--pfpb-primary-grey-color);
        background-color: var(--pfpb-secondary-grey-color);
    }

    .entete-financement-edit button.cofinance-editcancel,
    .entete-financementftl-edit button.cofinanceftl-editcancel{
        width: 40px;
        color: var(--pfpb-primary-red-color);
        background-color: var(--pfpb-secondary-red-color);
    }

    @media (max-width: 920px) {

    }

    @media (max-width: 720px) {
        .newDepenseListCard {
            width: 95% !important;
        }
        .newDL-entete-action button {
            margin: 8px;
        }

        .newDL-entete {
            display: inline-flex;
            width: 100%;
            flex-direction: column;
            flex-wrap: nowrap;
            align-content: stretch;
            align-items: center;
        }
    }
    #modal-preview-comment.comment-modal-aac-financement {
        width: 500px;
        right: 0;
        left: auto;
    }

    .validcofinpay {
        position: absolute;
        height: 22px !important;
        padding: 2px 10px;
        margin: 2px;
    }

    .fl-entete .newDL-entete-depense {
        width: 400px;
    }

    .changefinancertype span {
        width: 0;
    }

    .changefinancertype:hover span {
        width: fit-content;
    }

    .changefinancertype {
        border: .5px solid;
    }

    .changefinancertype.active span {
        color: var(--pfpb-primary-blue-color);
    }

    .changefinancertype.active i {
        color: var(--pfpb-primary-blue-color);
    }

    .changefinancertype span {
        width: 0;
        display: inline-flex;
        overflow: hidden;
        color: var(--pfpb-primary-grey-color);
    }

    .changefinancertype:hover span , .changefinancertype.active span {
        width: 100%;
    }

    .changefinancertype i {
        padding-right: 5px;
        background-color: var(--pfpb-tertiary-grey-color);
        color: var(--pfpb-primary-grey-color);
    }

    .changefinancertype {
        background-color: var(--pfpb-tertiary-grey-color);
        color: var(--pfpb-primary-grey-color);
    }

    .newDepenseListCard.active .btn.depbtn-tab.depmodal[data-content="delete"]{
        display: none;
    }

    .tlcart{
        padding-left: 30px;
        margin-right: 10px;
    }

    .activatedoublecont {
        display: inline-flex;
    }

    .activatedoublecont label , .activatedoublecont input {
        height: min-content;
        padding-left: 5px;
    }

    .textfirst .tlcart {
        margin-top: 4px;
    }

    .itldoubleamount {
        margin: 3px;
    }

    .input-option-search span {
        display: inline-table;
    }

    .cofinanceftl-editcancel {
        width: 50px !important;
    }

</style>

<?php
    $initAnswerFiles = @$answerfiles;

    $depenseFinancerOrga = @$get_orga;
    $depenseFinancer = @$get_tls;

    $depimg = [];
    foreach ($initAnswerFiles as $key4 => $d4) {
        if(isset($d4["imageKey"])) {
            $depimg[$d4["imageKey"]] = $d4;
        }
    }

    $totalfinancement = Form::getFormTotalFinancement($answer["form"]);

?>
<div class="single_funding newdepenselist <?php echo !empty($input['subType']) ? $input['subType'] : "" ?>">
    <div class="funding_header padding-15">
        <h4> <?php echo !empty($label) ? $label : "Ligne de dépense" ?>
            <?php
                if(!empty($input["subType"]) && $input["subType"] == "finance") {
            ?>
            <button class="btn numintervglobal hide"></button>
            <?php
                }
            ?>
            <span class="dropdown margin-left-10 budgetdropdown">
                <button class="adddepensedropdown btn btn-secondary dropdown-toggle margin-bottom-10" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-plus-circle "></i> Ajouter
                </button>
                <div class="dropdown-menu pull-right" aria-labelledby="dropdownMenuButton">
                    <a href="javascript:;"  class="btn addDepense dropdown-item"><i class='fa fa-plus'></i> Ajouter une ligne </a>
                    <a href="javascript:;" id="addfromcsv" class="btn addfromcsv dropdown-item"><i class='fa fa-plus'></i> Charger via csv </a>
                </div>
            </span>
            <?php
                if(!empty($input["subType"]) && $input["subType"] == "finance") {
            ?>
            <div class="btn-group pull-right" role="group" aria-label="Basic example">
                <button type="button" data-act="contentInformation" class="btn tglv active depensetableview"><i class="fa fa-trello"></i></button>
                <button type="button" data-act="contentInformationchart" class="btn tglv depensechartview"><i class="fa fa-line-chart"></i></button>
                <button type="button" data-act="contentInformationCoremu" class="btn tglv depensecoremuview"><i class="fa fa-table"></i></button>
            </div>
            <?php
                }
            ?>

        </h4>
    </div>
    <div class="funding_body funding_body_depense">
        <div class="panel-body no-padding">
            <?php
                 if(!empty($input["subType"]) && $input["subType"] == "finance") {
            ?>
             <div class="col-md-12 col-sm-12 col-xs-12 contentFinanceAction no-padding ">
                 <div class="multiselect-group-button-2">
                     <div class="btn-group">
                         <button class="btn changefinancertype" data-fintype="person"><i class="fa fa-user"></i><span>Je suis un citoyen</span></button>
                         <button class="btn changefinancertype" data-fintype="organization"><i class="fa fa-users"></i><span>Je représente une organisation</span></button>
                         <button class="btn changefinancertype active" data-fintype="tl"><i class="fa fa-map-marker"></i><span>Je représente un tiers-lieu</span></button>
                     </div>
                 </div>
                 <div class="multiselect-group-button-2">
                     <button class="statbutton-sellectall" > <span>  Tout sélectionner </span> <i class="fa fa-check-square pull-right"></i> </button>
                 </div>
             </div>
             <div class="col-md-12 col-sm-12 col-xs-12 contentFinanceAction no-padding ">
                <div data-fintype="organization" class="margin-left-10 financementtype " style="display: none">
                    <div class="mm-dropdown">
                        <div class="textfirst" data-placeholder="Choisir une ogranisation"><span class='depenseselectfinancer'>Choisir une ogranisation</span> <i class='fa fa-caret-down'></i></div>
                        <ul>
                            <?php
                                if(!empty($depenseFinancerOrga)) {
                           ?>
                                    <li placeholder="Filtrer" class="input-option input-option-search" >
                                        <input type="text" class="searchorga" data-type="orga">
                                        <span class="input-group-addon"> <i class="fa fa-search"></i> </span>
                                    </li>
                            <?php
                                    foreach ($depenseFinancerOrga as $k => $financer) {
                                        $fimg = "";
                                            if(!empty($financer["image"])){
                                                $fimg = $financer["image"];
                                            } else {
                                                $fimg = Yii::app()->getModule(Yii::app()->params["module"]["parent"])->getAssetsUrl() . "/images/thumbnail-default.jpg";
                                            }
                            ?>
                                    <li class="input-option" data-name="<?= $financer["name"] ?>"  data-value="<?= $k ?>" data-type="orga">
                                        <img src="<?= $fimg ?>" alt="" width="20" height="20" /><?= $financer["name"] ?>
                                    </li>
                            <?php
                                    }
                                }else{
                            ?>
                                    <li class="input-option" data-value="" data-type="orga">
                                        <img height="140" src="<?php echo Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl()."/images/thumbnail-default.jpg"; ?>" class="answer-depense-img"> Vous n'êtes pas encore admin d'un tiers lieux, cliquer ici pour créer un
                                    </li>
                            <?php
                                }
                            ?>
                        </ul>
                        <input type="hidden" class="option" name="namesubmit" value="" />
                    </div>
                </div>
                <div data-fintype="person" class="margin-left-10 financementtype "  style="display: none">
                    <div class="input-group">
                        <input class="input-name-multiselect" type="text" placeholder="Nom">
                        <span class="input-group-addon"> <i class="fa fa-user-o"></i> </span>
                    </div>
                    <div class="input-group">
                        <input class="input-email-multiselect" type="text" placeholder="Email">
                        <span class="input-group-addon"> <i class="fa fa-envelope-o"></i> </span>
                    </div>
                </div>
                <div data-fintype="tl" class="margin-left-10 financementtype financerdropdown active">
                   <div class="mm-dropdown">
                       <div class="textfirst" data-placeholder="Choisir un tiers-lieux"><span class='depenseselectfinancer'>Choisir un tiers-lieux</span> <i class='fa fa-caret-down'></i></div>
                       <ul>
                           <?php
                           if(!empty($depenseFinancer)) {
                           ?>
                               <li placeholder="Filtrer" class="input-option input-option-search" >
                                   <input type="text" class="searchorga" data-type="tl">
                                   <span class="input-group-addon"> <i class="fa fa-search"></i> </span>
                               </li>
                           <?php
                               foreach ($depenseFinancer as $k => $financer) {
                                   $fimg = "";
                                   if(!empty($financer["image"])){
                                       $fimg = $financer["image"];
                                   } else {
                                       $fimg = Yii::app()->getModule(Yii::app()->params["module"]["parent"])->getAssetsUrl() . "/images/thumbnail-default.jpg";
                                   }
                                   ?>
                                   <li class="input-option" data-name="<?= $financer["name"] ?>"  data-value="<?= $k ?>" data-type="tl">
                                       <img src="<?= $fimg ?>" alt="" width="20" height="20" /><?= $financer["name"] ?> <span data-finid="<?= $k ?>" class="tlcart pull-right"></span>
                                   </li>
                                   <?php
                               }
                           }else{
                               ?>
                               <li class="input-option" data-value="" data-type="tl">
                                   <img height="140" src="<?php echo Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl()."/images/thumbnail-default.jpg"; ?>" class="answer-depense-img"> Vous n'êtes pas encore admin d'un tiers lieux, cliquer ici pour créer un
                               </li>
                               <?php
                           }
                           ?>
                       </ul>
                       <input type="hidden" class="option" name="namesubmit" value="" />
                   </div>
               </div>
                <div class="input-group">
                    <input class="input-amount-multiselect-tree" type="number" placeholder="Montant" >
                    <span class="input-group-addon"> </span>
                </div>
                <div class="orgaaction-btngroup">
                    <button class="statbutton-cofinance" disabled > financer (<span class="action-select-count">0</span>) action(s) <i class="fa fa-arrow-right pull-right"></i>  </button>
                    <div class="dropdown">
                        <button class="dropdown-toggle orgbtn" type="button" data-toggle="dropdown" id="dropdown-orgaaction" aria-haspopup="true" aria-expanded="false">
                            Liste <span class="caret"></span>
                        </button>
                        <div class="dropdown-menu-orgaaction" aria-labelledby="dropdown-orgaaction">
                            <table class="depenseresume">
                                <tr>
                                    <th>Label</th>
                                    <th>Montant</th>
                                    <th>Reste</th>
                                    <th>Votre contribution</th>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 contentFinanceAction no-padding ">
                 <div class="multiselect-group-button-2">
                     <div class="activatedoublecont" >
                         <input type="checkbox" class="input-group activatedouble" name="activatedouble" value="" />
                         <label class="activatedoublelabel" for="activatedouble">doublonner</label>
                     </div>
                 </div>
                 <div class="multiselect-group-button-2">
                     <span class="tldoubleamount" style="display: none"> 0 </span> <i class="fa fa-euro itldoubleamount"  style="display: none"></i>
                 </div>
                 <div class="multiselect-group-button-2">
                 </div>
            </div>
            <?php
                 }
            ?>

            <div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding tgledv">

            </div>
            <?php
            if(!empty($input["subType"]) && $input["subType"] == "finance") {
            ?>
            <div id="contentInformationchart" class="tgledv col-md-12 col-sm-12 col-xs-12 contentInformationchart no-padding " style="display: none">
                <!--<div style="margin: 0 20px;">
                    <button class="btn zoom-out">Dézoom </button>
                    <button class="btn zoom-month">Zoom sur 30j</button>
                    <button class="btn zoom-week">Zoom sur 7j</button>
                </div>-->
            </div>
            <div id="financementTable" class="col-md-12 col-sm-12 col-xs-12 tgledv contentInformationCoremu no-padding" style="display: none">

            </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>

<script>
        var initAnswerFiles = <?php echo (!empty($initAnswerFiles) ? json_encode($initAnswerFiles) : json_encode([])); ?>;
        var answerObj = <?php echo (!empty($answer) ? json_encode($answer) : json_encode([])); ?>;
        var inputs = <?php echo (!empty($input) ? json_encode($input) : json_encode([])); ?>;
        var newdepenseData = <?php echo (!empty($answer["answers"]["aapStep1"]["depense"]) ? json_encode($answer["answers"]["aapStep1"]["depense"]) : json_encode([])); ?>;
        var newdepenseDataImg = <?php echo (!empty($depimg) ? json_encode($depimg) : json_encode([])); ?>;
        var newdepmod = "<?php echo (!empty($mode) ? $mode : "r") ?>";
        var depenseSubType = "<?php echo (!empty($input["subType"]) ? $input["subType"] : ""); ?>";
        var depenseFinancer = <?php echo (!empty($depenseFinancer) ? json_encode($depenseFinancer) : json_encode([])); ?>;
        var depenseFinancerOrga = <?php echo (!empty($depenseFinancerOrga) ? json_encode($depenseFinancerOrga) : json_encode([])); ?>;
        var totalfinancement = <?php echo (!empty($totalfinancement) ? json_encode($totalfinancement) : json_encode([])); ?>;
        var tiersLieux = <?php echo (!empty($tiersLieux) ? json_encode($tiersLieux) : json_encode([])); ?>;
        const thisPhp = {
            step: JSON.parse(JSON.stringify(<?= json_encode( isset($step) ? $step : null ) ?>)),
            key: "<?= $key ?>",
            inputDepenseKey: "aapStep1-depense"
        }
        var depenselistobj = {
            init : function(depenselistobj){
                depenselistobj.action.getDepenseData(depenselistobj,depenselistobj.action.getDepenseExtraData(depenselistobj , newdepenseData));
                $(depenselistobj.data.container).append(depenselistobj.view.getAllDepenseView(depenselistobj));
                depenselistobj.event.menutabs();
                depenselistobj.event.barchart(depenselistobj);
                if(depenselistobj.view.getInterv(depenselistobj , 0 , newdepenseData) > 0){
                    $('.numintervglobal').html(depenselistobj.view.getInterv(depenselistobj , 0 , newdepenseData));
                    //$('.numintervglobal').removeClass('hide');
                }
                if(depenseSubType == 'finance') {
                    depenselistobj.view.generateLineChart(depenselistobj, newdepenseData);
                }

                if(typeof depenselistobj.data.cart == "undefined" || notEmpty(depenselistobj.data.cart)){
                    var post = {
                        form: answerObj.form
                    };
                    url = baseUrl + '/co2/aap/funding/request/cart';
                    ajaxPost(null, url, post, function (resp) {
                        depenselistobj.data.cart = resp.content;
                        $.each(depenselistobj.data.cart.campagne, function(index,value){
                            $('span.tlcart[data-finid="'+index+'"]').html(value+'<i class="fa fa-euro"></i>');
                        });
                    });
                }else{
                    $.each(depenselistobj.data.cart.campagne, function(index,value){
                        $('span.tlcart[data-finid="'+index+'"]').html(value);
                    });
                }

            },
            data : {
                container : '.newdepenselist .contentInformation',
                depenses : {},
                selectedDepenses : {},
                actionlist : [],
                depenseSubType : "",
                nr : `<span style="color : grey">(non renseignée)</span>`,
                personlinks : {},
                financertype : 'tl',
                iscofinancementftl : false,
                currentOrga : {
                    name : '',
                    id : ''
                },
                actionData : {
                    "description" : {
                        "label" : "Description",
                        "icon" : "fa fa-info",
                        "class" : "menutabs",
                        "content" : "description",
                        restricted : false
                    },
                    "edit" : {
                        "label" : "Modifier",
                        "icon" : "fa fa-pencil",
                        "class" : "depmodal",
                        "content" : "edit",
                        restricted : true
                    },
                    "comment" : {
                        "label" : "Commenter",
                        "icon" : "fa fa-comment",
                        "class" : "depmodal",
                        "content" : "comment",
                        restricted : false
                    },
                    "delete" : {
                        "label" : "Delete",
                        "icon" : "fa fa-trash",
                        "class" : "depmodal",
                        "content" : "delete",
                        restricted : true
                    },
                    "financeList" : {
                        "label" : "Liste des financements pour cette action",
                        "icon" : "fa fa-bars",
                        "class" : "menutabs",
                        "content" : "financeList",
                        restricted : false
                    },
                    "financeBarChart" : {
                        "label" : "Graph des financements pour cette action",
                        "icon" : "fa fa-bar-chart",
                        "class" : "menutabs",
                        "content" : "financeBarChart",
                        restricted : false
                    },
                    "finance" : {
                        "label" : "Financer",
                        "icon" : "fa fa-money",
                        "class" : "depmodal",
                        "content" : "finance",
                        restricted : false
                    },
                    "include" : {
                        "label" : "Afficher",
                        "icon" : "fa fa-eye",
                        "class" : "depmodal",
                        "content" : "include",
                        restricted : false
                    },
                    "exclude" : {
                        "label" : "Cacher",
                        "icon" : "fa fa-eye-slash",
                        "class" : "depmodal",
                        "content" : "exclude",
                        restricted : false
                    },
                    "config" : {
                        "label" : "Configurer",
                        "icon" : "fa fa-cog",
                        "class" : "depmodal",
                        "content" : "config",
                        "role" : "admin",
                        restricted : false
                    }
                }
            },
            view : {
                getDepensePanel : function(depenselistobj,index,depense){
                    var html = '';
                    if(notEmpty(depense)){
                        html += `
                            <div class="newDepenseListCard ${depense.include ? '' : 'depinclude'}" data-subtype="${depenselistobj.data.depenseSubType}" data-uid="${index}" >
                                <div class="newDL-entete" >
                                    <div class="newDL-entete-image" >
                                          <img class="" src="${ (notEmpty(depense.imageKey) && notEmpty(newdepenseDataImg[depense.imageKey])) ? newdepenseDataImg[depense.imageKey].imagePath : modules.co2.url +'/images/thumbnail-default.jpg'}" >
                                    </div>
                                    <div class="newDL-entete-data" >
                                        <div class="newDL-entete-depense" >
                                            <div class="newDL-entete-date ${depenselistobj.data.depenseSubType === 'finance' ? 'hide' : ''}" >
                                                <div class="newDL-entete-day" >
                                                    ${ depense?.date?.sec ? new Date(depense.date.sec* 1000 ).toLocaleDateString().slice(0,2) : ""}
                                                </div>
                                                <div class="newDL-entete-month" >
                                                    ${depense?.date?.sec ? new Date(depense.date.sec* 1000).toLocaleString('default', { month: 'short' }) : ""}
                                                </div>
                                                <!-- <div class="newDL-entete-year" >
                                                    ${depense?.date?.sec ? new Date(depense.date.sec* 1000).getFullYear() : ""}
                                                </div> -->
                                            </div>
                                            <div class="newDL-entete-percentage ${depenselistobj.data.depenseSubType === 'depense' ? 'hide' : ''}" >
                                                <div class="newDL-entete-perc" >
                                                    ${depense.percentage}
                                                </div>
                                                <div class="newDL-entete-perc-sign" >
                                                    %
                                                </div>
                                            </div>
                                            <div class="newDL-entete-date-sp" data-subtype="${depenselistobj.data.depenseSubType}">
                                            </div>
                                            <div class="newDL-entete-date-sp-financ-container ${depenselistobj.data.depenseSubType === 'depense' ? 'hide' : ''}" data-subtype="${depenselistobj.data.depenseSubType}">
                                                <div style="height: ${depense.percentage >= 100 ? 100 : depense.percentage }%" class="newDL-entete-date-sp-financ ${depenselistobj.data.depenseSubType === 'depense' ? 'hide' : ''}" data-subtype="${depenselistobj.data.depenseSubType}">
                                                </div>
                                            </div>
                                            <div class="newDL-entete-info" >
                                                <div class="newDL-entete-titre"  >
                                                    ${depense.poste}
                                                </div>
                                                <div class="newDL-entete-price" >
                                                    <span class="entete-financement"><i class="fa fa-money"></i> ${depenselistobj.action.addSpace(depense.financement)}  / </span> ${depenselistobj.action.addSpace(depense.price)}
                                                </div>
                                                <div class="newDL-entete-price" >
                                                    <span class="entete-part">votre contrib. : ${depenselistobj.action.addSpace(depense.part)} </span> <span class="entete-ftlpart" style="display: none">contrib. FTL : ${depenselistobj.action.addSpace(depense.ftlpart)} </span>
                                                </div>
                                             </div>
                                        </div>
                                        <div class="newDL-entete-action" >
                                            ${depenselistobj.view.getActionDepense(depenselistobj,index,depense)}
                                        </div>
                                    </div>
                                </div>
                                <div class="newDL-body" data-uid="${index}" >
                                    <div class="newDL-body-tabs-container" >
                                        ${depenselistobj.view.getActionTabs(depenselistobj,index,depense)}
                                    </div>
                                </div>
                            </div>
                        `;
                    }
                    return html;
                },
                getAllDepenseView : function(depenselistobj){
                    var html = '';
                    if(notEmpty(depenselistobj.data.depenses)){
                        $.each(depenselistobj.data.depenses, function (index, depense){
                            html += depenselistobj.view.getDepensePanel(depenselistobj , index, depense);
                        });
                    }
                    return html;
                },
                getInterv(depenselistobj , index , depenses){
                    numftl = 0;
                    numnoftl = 0;
                    $.each(depenses, function (i,depense) {
                        if (notEmpty(depense.financer)) {
                            numftl += depense.financer.reduce((a, d) => d.name == "FTL" ? a+1 : a, 0);
                            numnoftl += depense.financer.reduce((a, d) => d.name != "FTL" ? a+1 : a, 0);
                        }
                    });
                    return numnoftl - numftl;
                },
                getActionDepense : function(depenselistobj , index , depense){
                    var html = '';
                    var html2 = '';
                    if(depenselistobj.view.getInterv(depenselistobj , index , [depense]) > 0 ){
                        html2 = `<i class="fa fa-exclamation"></i>`;
                    }
                    $.each(depenselistobj.data.actionlist, function (i,value){
                        if(!depenselistobj.data.actionData[value].restricted || (typeof answerObj.user != undefined && userId === answerObj.user) || costum.isCostumAdmin) {
                            html += `<button aria-label="${depenselistobj.data.actionData[value].label ? depenselistobj.data.actionData[value].label : "" }" role="tooltip" data-microtip-position="bottom"  data-uid="${index}" class="btn depbtn-tab ${depenselistobj.data.actionData[value].class}" data-content="${depenselistobj.data.actionData[value].content}">
                                            <i class="fa ${depenselistobj.data.actionData[value].icon}"></i>${depenselistobj.data.actionData[value].content == 'financeList' ? html2 : ''}<span> ${depenselistobj.data.actionData[value].label} </span>
                                     </button>`;
                        }
                    });
                    if((typeof depense.include != "undefined" || depense.include == false) && ((typeof answerObj.user != undefined && userId === answerObj.user) || costum.isCostumAdmin)){
                        html += `<button aria-label="${depenselistobj.data.actionData["include"].label}" role="tooltip" data-microtip-position="bottom"  data-uid="${index}" class="btn depbtn-tab ${depenselistobj.data.actionData["include"].class}" data-content="${depenselistobj.data.actionData["include"].content}">
                                            <i class="fa ${depenselistobj.data.actionData["include"].icon}"></i><span> ${depenselistobj.data.actionData["include"].label} </span>
                                     </button>`;
                    }else if((typeof answerObj.user != undefined && userId === answerObj.user) || costum.isCostumAdmin){
                        html += `<button  aria-label="${depenselistobj.data.actionData["exclude"].label}" role="tooltip" data-microtip-position="bottom"  data-uid="${index}" class="btn depbtn-tab ${depenselistobj.data.actionData["exclude"].class}" data-content="${depenselistobj.data.actionData["exclude"].content}">
                                            <i class="fa ${depenselistobj.data.actionData["exclude"].icon}"></i><span> ${depenselistobj.data.actionData["exclude"].label} </span>
                                     </button>`;
                    }
                    return html;
                },
                getActionTabs : function (depenselistobj , index, depense) {
                    var html = '';
                    $.each(depenselistobj.data.actionlist, function (i,value){
                        if(depenselistobj.data.actionData[value].class == "menutabs") {
                            html += `<div data-uid="${index}" class="newDL-body-tab" data-content="${value}">
                                       ${depenselistobj.panel[value](depenselistobj,index,depense)}
                                    </div>`;
                        }
                    });
                    return html;
                },
                generateBarChart : function (depenselistobj , index, depense) {
                    var svgWidth = 450;
                    var svgHeight = 150;

                    var svg = d3.selectAll(`#financebarchart${index}`)
                        .attr("width", svgWidth)
                        .attr("height", svgHeight)

                    svg.append('rect')
                        .attr("width", svgWidth)
                        .attr("height", svgHeight)
                        .attr("fill", '#dce7f5de')
                        .attr("rx", 20);

                    var dataset = [];

                    if(notEmpty(depense)) {
                        dataset = depense.financer.map(function (line) {
                            return line.amount
                        });
                    }

                    var barPadding = 10;
                    var barWidth = 50;

                    var cumul = 0;
                    var datasetcumul = [];
                    $.each(dataset, function(i,v){
                        datasetcumul[i] = cumul ;
                        cumul = cumul + dataset[i];
                    });

                    svg.selectAll("rect.no1")
                        .data(dataset)
                        .enter()
                        .append("rect")
                        .attr("y" , function (d){
                            return svgHeight - (d / depense.price)*svgHeight
                        })
                        .attr("height" , function (d){
                            return (d / depense.price)*svgHeight
                        })
                        .attr("width" , function (d){
                            return barWidth - barPadding
                        })
                        .attr("transform" , function (d,i){
                            var translate = [(barWidth * i)+20 , -((datasetcumul[i]/depense.price)*svgHeight) ];
                            return "translate(" + translate + ")";
                        })
                        .attr('fill' , '#0096d1')
                        .attr("rx", 2)

                    svg.selectAll("rect.no2")
                        .data(datasetcumul)
                        .enter()
                        .append("rect")
                        .attr("y" , function (d){
                            return svgHeight - (d / depense.price)*svgHeight
                        })
                        .attr("height" , function (d){
                            return (d / depense.price)*svgHeight
                        })
                        .attr("width" , function (d){
                            return barWidth - barPadding
                        })
                        .attr("transform" , function (d,i){
                            var translate = [(barWidth * i)+20 , 0 ];
                            return "translate(" + translate + ")";
                        })
                        .attr('fill' , '#8dcee8')
                        .attr("rx", 2)

                },
                generateLineChart : function (depenselistobj, depenses) {

                    var TRANSITION_SPEED = 300;
                    var DATES_RANGE = 90;
                    var date = moment().subtract(DATES_RANGE, 'days');
					var data = [];

                    date.set('hour', 0);
                    date.set('minute', 0);
                    date.set('second', 0);

                    if (depenselistobj.data && depenselistobj.data.depenses && Array.isArray(depenselistobj.data.depenses))
					data = depenselistobj.data.depenses;
					data = data.map(function (params) {
                        return params.financer;
                    }).flat(1).map(function (params) {
                        if(!isNaN(parseInt(params.date.sec)) && !isNaN(parseInt(params.amount))){
                            return { date : params.date.sec * 1000 , montant : params.amount};
                        }
                    }).sort((a,b) => (a.date > b.date) ? 1 : ((b.date > a.date) ? -1 : 0));
                    $.each(data, function(i,v){
                        if(typeof v.montant != "undefined") {
                            data[i].montant = parseInt(v.montant) + parseInt(i > 0 ? data[i - 1].montant : 0);
                        }else{
                            data[i].montant = parseInt(i > 0 ? data[i - 1].montant : 0);
                        }
                    });

                    function getMaxValue(data, field) {
                        return Math.max.apply(Math, _.pluck(data, field));
                    }

                    function getDateRange(from, to) {
                        var fromIdx = _.findIndex(data, function(item) {
                                return item.date >= from;
                            }),
                            toIdx = _.findIndex(data, function(item) {
                                return item.date >= to;
                            });

                        return _.pluck(data.slice(fromIdx, toIdx), "date");
                    }

                    if(notEmpty(data) && data.length > 0) {
                        var minDate = data[0].date,
                            maxDate = data[data.length - 1].date;

                        var margin = {top: 20, right: 20, bottom: 30, left: 40},
                            navigatorMarginTop = 30,
                            navigatorHeight = 60,
                            width = 960 - margin.left - margin.right,
                            height = 500 - margin.top - margin.bottom - navigatorHeight - navigatorMarginTop;

                        var xScale = d3.scaleTime([0, width])
                            .domain([minDate, maxDate]);

                        var fakeOrdinalXScale = d3.scaleOrdinal([0, width])
                            .domain(_.pluck(data, 'date'));

                        var yScale = d3.scaleLinear([height, 0])
                            .domain([0, getMaxValue(data, "montant")]);

                        var xAxis = d3.axisBottom(xScale)
                            .tickFormat(function (d) {
                                return moment(d).format("MMM Y");
                            });

                        var yAxis = d3.axisLeft(yScale)
                            .tickSize(-width);

                        var zoom = d3.zoom().on("zoom", function () {
                            svg.attr("transform", d3.event.transform);
                        });

                        var svg = d3.select("#contentInformationchart").append("svg")
                            .attr("width", width + margin.left + margin.right)
                            .attr("height", height + margin.top + margin.bottom + navigatorHeight + navigatorMarginTop);

                        var mainGroup = svg.append("g")
                            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

// Clip-path
                        var chartsContainer = mainGroup.append('g')
                            .attr('clip-path', 'url(#plotAreaClip)')
                            .call(zoom);

                        chartsContainer.append('clipPath')
                            .attr('id', 'plotAreaClip')
                            .append('rect')
                            .attr("width", width)
                            .attr("height", height);

// Line chart
                        var line = d3.line()
                            .curve(d3.curveBasis)
                            .x(function (d) {
                                return xScale(d.date);
                            })
                            .y(function (d) {
                                return yScale(d.montant);
                            });

                        var lineGroup = chartsContainer
                            .append("path")
                            .attr("class", "line")
                            .data(data)
                            .attr("d", line(data));

// Axis
                        mainGroup.append("g")
                            .attr("class", "x axis")
                            .attr("transform", "translate(0," + height + ")")
                            .call(xAxis)
                            .call(g => g.select(".domain").remove);

                        mainGroup.append("g")
                            .attr("class", "y axis")
                            .call(yAxis)
                            .call(g => g.select(".domain").remove)
                            .call(g => g.selectAll(".tick:not(:first-of-type) line")
                                .attr("stroke-opacity", 0.5)
                                .attr("stroke-dasharray", "2,2"))
                            .call(g => g.selectAll(".tick text")
                                .attr("x", -4)
                                .attr("dx", -4))

// Navigator
                        /*var navXScale = d3.scaleTime([0, width])
                            .domain([minDate, maxDate]);

                        var navYScale = d3.scaleLinear([navigatorHeight, 0])
                            .domain([0, getMaxValue(data, "montant")]);


                        var navXAxis = d3.axisBottom(navXScale)
                            .tickFormat(function (d) {
                                return moment(d).format("MMM");
                            });

                        var navData = d3.area()
                            .x(function (d) {
                                return navXScale(d.date);
                            })
                            .y1(function (d) {
                                return navYScale(d.montant);
                            })
                            .y0(navYScale(0))
                            .curve(d3.curveBasis);

                        var navigatorGroup = svg.append("g")
                            .attr("class", "navigator")
                            .attr("width", width + margin.left + margin.right)
                            .attr("height", navigatorHeight + margin.top + margin.bottom)
                            .attr("transform", "translate(" + margin.left + "," + (margin.top + height + navigatorMarginTop) + ")");
*/
                        /*navigatorGroup.append("path")
                            .attr("class", "data")
                            .data(data)
                            .attr("d", navData(data));*/

                        /*svg.append("g")
                            .attr("class", "x nav-axis")
                            .attr("transform", "translate(" + margin.left + "," + (margin.top + height + navigatorHeight + navigatorMarginTop) + ")")
                            .call(navXAxis);*/

// Navigator viewport
                        /*var navViewport = d3.brushX(navXScale)
                            .extent([0, 0], [navigatorHeight, width])
                            .on("brush", display);

                        navigatorGroup.append("g")
                            .attr("class", "nav-viewport")
                            .call(navViewport)
                            .selectAll("rect")
                            .attr("height", navigatorHeight);*/

                        function display() {
                            var viewportExtent = navViewport.empty() ? navXScale.domain() : navViewport.extent();

                            zoomToPeriod(viewportExtent[0], viewportExtent[1]);
                        }

                        d3.select("button.zoom-out").on("click", function () {
                            zoomToPeriod(minDate, maxDate);
                        });

                        d3.select("button.zoom-month").on("click", function () {
                            var to = maxDate,
                                from = moment(to).subtract(30, 'days').valueOf();

                            zoomToPeriod(from, to);
                        });

                        d3.select("button.zoom-week").on("click", function () {
                            var to = maxDate,
                                from = moment(to).subtract(7, 'days').valueOf();

                            zoomToPeriod(from, to);
                        });

                        function zoomToPeriod(from, to) {
                            chartsContainer.call(zoom(xScale.domain([from, to])));
                            fakeOrdinalXScale.domain(getDateRange(from, to));
                            updateAxis();
                            updateCharts();
                        }

                        function updateCharts() {
                            lineGroup
                                .data([data])
                                .attr("d", line);

                            lineGroup
                                .exit().remove();

                        }

                        function updateAxis() {
                            mainGroup.select(".x.axis").call(xAxis);
                            mainGroup.select(".y.axis").call(yAxis);

                            navViewport.extent(xScale.domain());
                            navigatorGroup.select('.nav-viewport').call(navViewport);
                        }
                    }
                },
                getFinanceView : function (depenselistobj,uid,index,finance){
                    var html = '';
                    var iscofinancementftl = typeof finance.iscofinancementftl != "undefined" ? finance.iscofinancementftl : true;
                    var financetlstatus = typeof finance.status != "undefined" && finance.status == "paid" ? true : false;
                    var billstatus = typeof finance.payment_method != "undefined" && finance.payment_method == "bank" ? true : false;
                    if(notEmpty(finance) && finance.id !== Object.keys(answerObj.context)[0]){
                        html += `
                            <div class="$('input-amount-multiselect-tree').val('');" data-fid="${index}" >
                                <div class="fl-entete" >
                                    <div class="newDL-entete-data" >
                                        <div class="newDL-entete-depense" >
                                            <div class="newDL-entete-date" >
                                                <div class="newDL-entete-day" >
                                                    ${new Date(finance.date.sec* 1000).toLocaleDateString().slice(0,2)}
                                                </div>
                                                <div class="newDL-entete-month" >
                                                    ${new Date(finance.date.sec* 1000).toLocaleString('default', { month: 'short' })}
                                                </div>
                                                <!-- <div class="newDL-entete-year" >
                                                    ${new Date(finance.date.sec* 1000).getFullYear()}
                                                </div> -->
                                            </div>
                                            <div class="newDL-entete-date-sp">
                                            </div>
                                            <div class="newDL-entete-info" >
                                                <div class="newDL-entete-titre" data-uid="${uid}" data-fid="${index}" >
                                                    ${finance.name}
                                                </div>
                                                <div class="newDL-entete-titre-select" data-uid="${uid}" data-fid="${index}" >

                                                </div>
                                                <div class="newDL-entete-price" >
                                                    <span class="entete-financement"  data-uid="${uid}" data-fid="${index}"> Financement ${!iscofinancementftl || financetlstatus ? "du tiers lieux" : ""  } : ${depenselistobj.action.addSpace(finance.amount)} ${depenselistobj.view.financestatus(index, uid, finance , iscofinancementftl , financetlstatus , billstatus , false )}  </span>
                                                    <span style="display: none" class="entete-financement-edit"  data-uid="${uid}" data-fid="${index}"> Financement du tier lieu : <input class="entete-financement-input" data-uid="${uid}" data-fid="${index}" data-val="${finance.amount}"> <button data-uid="${uid}" data-fid="${index}" data-finkey="${finance.finkey}" data-tlname="${finance.name}" class="btn cofinance-edit">Modifier</i></button><button data-uid="${uid}" data-fid="${index}" class="btn cofinance-editcancel"><i class="fa fa-times"></i></button> </span>
                                                    ${depenselistobj.view.ftlfin(depenselistobj,uid,index,finance, iscofinancementftl , financetlstatus)}
                                                </div>
                                             </div>
                                            <div class="flist-entete-action" >
                                                <div class="flist-entete-act" >
                                                    ${depenselistobj.view.validatebtn(depenselistobj,uid,finance)}
                                                    <span class="dropdown budgetdropdown">`;
                                                    if(!financetlstatus && (userId == finance.user || notEmpty(depenseFinancer[finance.id]) || costum.isCostumAdmin)) {
                                                        html += `<button class=" btn btn-secondary dropdown-toggle margin-bottom-10" data-uid="${uid}" data-fid="${index}" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="fa fa-ellipsis-v "></i>
                                                                </button>`
                                                    }
                                                            html += `<div class="dropdown-menu pull-right" aria-labelledby="dropdownMenuButton">`;
                                                            if(!financetlstatus && (userId == finance.user || notEmpty(depenseFinancer[finance.id]) || (typeof answerObj != "undefined" && typeof answerObj.links != "undefined" && typeof answerObj.links.canEdit != "undefined" && typeof answerObj.links.canEdit[userId] != "undefined"))) {
                                                                html += `<a href="javascript:;"  class="btn dropdown-item editfinance" data-uid="${uid}" data-fid="${index}"  data-vid="${finance.id}"><i class='fa fa-pencil-square'></i> Modifier </a>`;
                                                            }
                                                            html += depenselistobj.view.editvalidatebtn(depenselistobj,uid,index,finance);
                                                            if(!financetlstatus && (userId == finance.user || notEmpty(depenseFinancer[finance.id]) || (typeof answerObj != "undefined" && typeof answerObj.links != "undefined" && typeof answerObj.links.canEdit != "undefined" && typeof answerObj.links.canEdit[userId] != "undefined"))) {
                                                                html += `<a href="javascript:;" id="addfromcsv" class="btn dropdown-item deletefinance" ${depenselistobj.view.ftlid(depenselistobj,uid,index,finance)} data-uid="${uid}" data-fid="${index}"  data-vid="${finance.id}" ><i class='fa fa-trash'></i> Supprimer </a>`;
                                                            }
                                                            html += depenselistobj.view.deletevalidatebtn(depenselistobj,uid,index,finance);

                                                html += `</div>
                                                    </span>
                                                </div>
                                                <div class="flist-entete-act" >

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                    }
                    return html;
                },
                validatebtn : function (depenselistobj , index, finance){
                    var html = '';
                    isvalidfinance = false;

                    if(notEmpty(depenselistobj.data.depenses[index])) {
                        $.each(depenselistobj.data.depenses[index].financer, function (i, value) {
                            if (value.id == Object.keys(answerObj.context)[0] && finance.finkey == value.finkey) {
                                isvalidfinance = true;
                                //html += `<button><i class="fa fa-check-circle-o"></i></button>`;
                            }
                        });
                    }
                    if(!isvalidfinance){
                        /*html += `<button class="btn btn-secondary doublebtn" data-uid="${index}" data-finkey="${finance.finkey}" data-tlname="${finance.name}" data-amount="${finance.amount}">
                                    Doubler
                                </button>`;*/
                    }
                    return html;
                },
                editvalidatebtn : function (depenselistobj , uid, index, finance ){
                    var html = ``;
                    if(notEmpty(depenselistobj.data.depenses[uid].financer) && costum.isCostumAdmin) {
                        $.each(depenselistobj.data.depenses[uid].financer, function (i, value) {
                            //html = `${index}${uid}`;
                            if (notEmpty(value.tlid) && finance.finkey == value.finkey && value.amount != 0) {
                                html += `<a href="javascript:;"  class="btn dropdown-item editfinanceftl" data-uid="${uid}" data-fid="${i}" data-fidtl="${index}" data-vid="${finance.id}"><i class='fa fa-pencil-square'></i> Modifier le financement FTL </a>`;
                            }
                        });
                    }
                    return html;
                },
                deletevalidatebtn : function (depenselistobj , uid, index, finance ){
                    var html = '';
                    if(notEmpty(depenselistobj.data.depenses[index]) && costum.isCostumAdmin) {
                        $.each(depenselistobj.data.depenses[index].financer, function (i, value) {
                            if (value.id == Object.keys(answerObj.context)[0] && finance.finkey == value.finkey) {
                                html += `<a href="javascript:;"  class="btn dropdown-item deletefinanceftl" data-uid="${uid}" data-fid="${i}" data-fidtl="${index}" data-vid="${finance.id}"><i class='fa fa-pencil-square'></i> Supprimer le financement FTL </a>`;
                            }
                        });
                    }
                    return html;
                },
                ftlid : function (depenselistobj , uid, index, finance ){
                    var html = '';
                    if(notEmpty(depenselistobj.data.depenses[index]) && costum.isCostumAdmin) {
                        $.each(depenselistobj.data.depenses[index].financer, function (i, value) {
                            if (value.id == Object.keys(answerObj.context)[0] && finance.finkey == value.finkey) {
                                html += `data-ftlid="${i}"`;
                            }
                        });
                    }
                    return html;
                },
                ftlfin : function (depenselistobj ,uid, index, finance , iscofinancementftl , financetlstatus) {
                    var html = '';
                    if (notEmpty(depenselistobj.data.depenses[uid]) && iscofinancementftl) {
                        $.each(depenselistobj.data.depenses[uid].financer, function (i, value) {
                            if (notEmpty(value.tlid) && finance.finkey == value.finkey && value.amount != 0) {
                                html += `<div class="ftlcase" data-uid="${uid}" data-fidtl="${index}" data-fid="${i}"> Financement FTL : ${depenselistobj.action.addSpace(value.amount)} ${depenselistobj.view.financestatus(i, uid, value , false , typeof value.status != "undefined" && (value.status == "paid" || value.status == "distributed") ? true : false , false , true )} </div>
                                         <div class="ftlcase" data-uid="${uid}" data-fidtl="${index}" data-fid="${i}"> du ${new Date(value.date.sec * 1000).toLocaleDateString()}  </div>
                                         <div style="display: none" class="entete-financementftl-edit"  data-uid="${uid}" data-fidtl="${index}" data-fid="${i}"> Financement FTL : <div> <input class="entete-financementftl-input"   data-uid="${uid}" data-fidtl="${index}" data-fid="${i}" data-val="${value.amount}"> <button   data-uid="${uid}" data-fidtl="${index}" data-fid="${i}" data-finkey="${i.finkey}" data-tlname="${i.name}"  class="btn cofinanceftl-edit">Modifier</i></button><button data-uid="${uid}" data-fidtl="${index}" data-fid="${i}" class="btn cofinanceftl-editcancel"><i class="fa fa-times"></i></button> </div> </div>

                                        `;
                            }
                        });
                    }
                    /*if (!isvalidfinance) {
                        html += `<div class="ftlcase-info"> <i class="fa fa-info"></i> Total contribution FTL pour ce tier lieu : (${notEmpty(totalfinancement["FTL"]) && notEmpty(totalfinancement["FTL"][finance.name]) ? depenselistobj.action.addSpace(totalfinancement["FTL"][finance.name]) : '0€'} </div>`;
                    }*/
                    return html;
                },
                financestatus : function (fid,uid,finance, iscofinancementftl , financetlstatus , billstatus , isftlfinance = false ){
                    //var validepayhtml = financetlstatus ? `<i class="fa fa-check" ></i>` :  `<button data-uid="${uid}" data-fid="${fid}" class="btn btn-sm validcofinpay"> <i class="fa fa-times hide" ></i> <span> valider <span> </button>`;
                    var isadmin = costum.isCostumAdmin;
                    var validepayhtml = "";
                    if(isftlfinance){
                        validepayhtml = !financetlstatus ? ( isadmin ? `<button data-uid="${uid}" data-fid="${fid}" class="btn btn-sm validcofinpay margin-top-10"> <i class="fa fa-times hide" ></i> <span> valider <span> </button>` : `<button disabled data-uid="${uid}" data-fid="${fid}" class="btn btn-sm validcofinpay"> <i class="fa fa-times hide" ></i><i class="fa fa-circle" ></i> En attente </button>`) : `<button data-uid="${uid}" data-fid="${fid}" disabled class="btn btn-sm validcofinpay"> <i class="fa fa-times hide" ></i>  validée </button>`
                    }else{
                        validepayhtml = billstatus && !financetlstatus ?
                        (isadmin ?
                            `<button data-uid="${uid}" data-fid="${fid}" class="btn btn-sm validcofinpay"> <i class="fa fa-times hide" ></i> <span> Valider virement <span> </button>`
                            : `<button disabled data-uid="${uid}" data-fid="${fid}" class="btn btn-sm validcofinpay"> <i class="fa fa-times hide" ></i><i class="fa fa-circle" ></i> Virement à valider </button>`
                        )
                        : (
                            financetlstatus ?
                                `<button disabled data-uid="${uid}" data-fid="${fid}" class="btn btn-sm validcofinpay paid"> <i class="fa fa-times hide" ></i><i class="fa fa-check" ></i> Payée </button>`
                                : `<button disabled data-uid="${uid}" data-fid="${fid}" class="btn btn-sm validcofinpay"> <i class="fa fa-times hide" ></i><i class="fa fa-circle" ></i> En attente </button>`
                        );
                    }
                    return validepayhtml;
                },
                createPageFin : function (tableDataFin) {
                    totalDepense = 0;
                    totalRecette = 0;
                    $.each(tableDataFin, function(index,value){
                        totalDepense += parseInt(value.depense);
                        totalRecette += parseInt(value.recette);
                    });

                    /*tableDataFin.push({
                        "poste de depense" : "TOTAL" ,
                        "depense" : totalDepense ,
                        "libelé financeur" : " ",
                        "libelé financement" : " ",
                        "recette" : totalRecette
                    });*/

                    $('#financementTable').createTable(tableDataFin , {
                        borderWidth:'1px',
                        borderStyle:'solid',
                        borderColor:'#DDDDDD',
                        fontFamily:'Verdana, Helvetica, Arial, FreeSans, sans-serif',

                        thBg:'#F3F3F3',
                        thColor:'#0E0E0E',
                        thHeight:'30px',
                        thFontFamily:'Verdana, Helvetica, Arial, FreeSans, sans-serif',
                        thFontSize:'14px',
                        thTextTransform:'capitalize',

                        trBg:'#FFFFFF',
                        trColor:'#0E0E0E',
                        trHeight:'25px',
                        trFontFamily:'Verdana, Helvetica, Arial, FreeSans, sans-serif',
                        trFontSize:'13px',

                        tdPaddingLeft:'10px',
                        tdPaddingRight:'10px'
                    });

                    depenselistobj.action.setRowSpanFin('#financementTable table', 1);
                    depenselistobj.action.setRowSpanFin('#financementTable table', 2);

                    $("#financementTable table tr:nth-child(1) th:nth-child(2)").addClass('filter-select');
                    $("#financementTable table tr:nth-child(1) th:nth-child(5)").addClass('filter-select');
                    $("input.tablesorter-filter.form-control[data-column='0']").remove();
                    $( "#financementTable table tr:nth-child(1) th:nth-child(2)" ).addClass('group-word');

                    $.tablesorter.themes.bootstrap = {
                        // these classes are added to the table. To see other table classes available,
                        // look here: http://getbootstrap.com/css/#tables
                        table        : 'table table-bordered table-striped',
                        caption      : 'caption',
                        // header class names
                        header       : 'bootstrap-header', // give the header a gradient background (theme.bootstrap_2.css)
                        sortNone     : '',
                        sortAsc      : '',
                        sortDesc     : '',
                        active       : '', // applied when column is sorted
                        hover        : '', // custom css required - a defined bootstrap style may not override other classes
                        // icon class names
                        icons        : '', // add "bootstrap-icon-white" to make them white; this icon class is added to the <i> in the header
                        iconSortNone : 'bootstrap-icon-unsorted', // class name added to icon when column is not sorted
                        iconSortAsc  : 'glyphicon glyphicon-chevron-up', // class name added to icon when column has ascending sort
                        iconSortDesc : 'glyphicon glyphicon-chevron-down', // class name added to icon when column has descending sort
                        filterRow    : '', // filter row class; use widgetOptions.filter_cssFilter for the input/select element
                        footerRow    : '',
                        footerCells  : '',
                        even         : '', // even row zebra striping
                        odd          : ''  // odd row zebra striping
                    };

                    $("#financementTable table").tablesorter({
                        // this will apply the bootstrap theme if "uitheme" widget is included
                        // the widgetOptions.uitheme is no longer required to be set
                        theme : "bootstrap",

                        widthFixed: true,

                        headerTemplate : '{content} {icon}', // new in v2.7. Needed to add the bootstrap icon!

                        // widget code contained in the jquery.tablesorter.widgets.js file
                        // use the zebra stripe widget if you plan on hiding any rows (filter widget)
                        widgets : [ "uitheme", "filter", "columns", "zebra" , "print" , "group"],

                        widgetOptions : {
                            columns: [ "primary", "secondary", "tertiary" ],
                            print_now        : true,
                            filter_reset : ".reset",

                            // extra css class name (string or array) added to the filter element (input or select)
                            filter_cssFilter: "form-control",

                            group_collapsible : true,
                            group_collapsed   : true,
                            group_count       : true,
                            filter_childRows  : true,
                            filter_columnFilters   : true,
                            headerTitle_useAria    : true,

                            group_count       : " ({num})",

                            group_forceColumn : [],   // only the first value is used; set as an array for future expansion
                            group_enforceSort : true,

                            output_separator: ',',         // Définir le séparateur pour le CSV
                            output_ignoreColumns: [],      // Ignorer certaines colonnes si besoin
                            output_includeFooter: false,   // Inclure le pied de page
                            output_dataAttrib: 'data-name', // Optionnel, pour nommer les colonnes de données
                            output_delivery: 'download',   // Préparer pour un téléchargement
                            output_saveFileName: 'financement.csv', // Nom du fichier CSV

                        },

                        headers: {
                            3 : { sorter: 'priceparser' },
                            7 : { filter_cssFilter : 'a'}
                        },

                        group_callback    : function($cell, $rows, column, table) {
                            // callback allowing modification of the group header labels
                            // $cell = current table cell (containing group header cells ".group-name" & ".group-count"
                            // $rows = all of the table rows for the current group; table = current table (DOM)
                            // column = current column being sorted/grouped
                            if (column === 3) {
                                var subtotal = 0;
                                $rows.each(function() {
                                    subtotal += parseFloat( $(this).find("td").eq(column).text() );
                                });
                                $cell.find(".group-count").append("; subtotal: " + subtotal );
                            }
                        },
                        // event triggered on the table when the grouping widget has finished work
                        group_complete    : "groupingComplete",

                        on_filters_loaded: function(o){
                            o.SetFilterValue(paramCol, paramCriteria);
                            o.Filter();
                        }

                    });
                }
            },
            action : {
                getDepenseData : function (depenselistobj, data){
                    depenselistobj.data.depenses = data;
                },
                addSpace(number){
                    if(notEmpty(number)) {
                        number = number.toString();
                        var pattern = /(-?\d+)(\d{3})/;
                        while (pattern.test(number))
                            number = number.replace(pattern, "$1 $2");
                    }else {
                        number = 0;
                    }
                    return number+'€';
                },
                getDepenseExtraData : function (depenselistobj, depenses) {
                    var returndep = {};
                    if (notEmpty(depenses) && depenses != null) {
                        returndep = depenses.map(function (depense,index){
                            if (notEmpty(depense) && depense != null) {
                                var lvl1 = {
                                    uid: index,
                                    poste: notEmpty(depense.poste) ? depense.poste : "",
                                    imageKey: depense.imageKey ? depense.imageKey : "",
                                    price: depense.price,
                                    date: depense.date,
                                    description: depense.description,
                                    user: depense.user,
                                    financer: depense.financer ? depense.financer : [],
                                    financementCount: depense.financer ? depense.financer.length : 0,
                                    financement: depense.financer ? depense.financer.reduce((n, {amount}) => n + parseInt(amount), 0) : 0,
                                    reste: depense.price - (depense.financer ? depense.financer.reduce((n, {amount}) => n + parseInt(amount), 0) : 0),
                                    part: 0,
                                    include : typeof depense.include != "undefined" ? depense.include : true
                                }

                                lvl1.financers = {};
                                if (notEmpty(depense.financer)) {
                                    $.each(depense.financer, function (id, fin) {
                                        if (lvl1.financers[fin.name]) {
                                            lvl1.financers[fin.name] += parseInt(fin.amount)
                                        } else {
                                            lvl1.financers[fin.name] = parseInt(fin.amount)
                                        }
                                    });
                                }

                                lvl1.percentage = 0;
                                if (lvl1.price != 0 && lvl1.financement != 0) {
                                    lvl1.percentage = Math.round((lvl1.financement / lvl1.price) * 100);
                                }
                                return lvl1;
                            }
                        });
                    }
                    return returndep;
                },
                updateselect : function (){
                    if($('.newdepenselist.finance .newDepenseListCard.active').length > 0){
                        $('.newdepenselist.finance').addClass('active');
                    }else{
                        $('input-amount-multiselect-tree').val('');
                        $('.newdepenselist.finance').removeClass('active');
                    }
                    $('.action-select-count').html($('.newdepenselist.finance .newDepenseListCard.active').length);
                    depenselistobj.action.disturbAmount(depenselistobj);
                    if($('.newdepenselist.finance .newDepenseListCard.active').length == $('.newdepenselist.finance .newDepenseListCard').length){
                        $('.statbutton-sellectall').html('<span>Tout désélectionner</span> <i class="fa fa-times-square"></i>');
                    }else{
                        $('.statbutton-sellectall').html('<span>Tout sélectionner</span> <i class="fa fa-check-square"></i>');
                    }
                },
                /*disturbAmount : function (depenselistobj){
                    var totalReste = 0;
                    var userInput = parseInt($('.input-amount-multiselect-tree').val());

                    $('.newdepenselist.finance .newDepenseListCard.active').each(function (){
                        var thisid = $(this);
                        totalReste += depenselistobj.data.depenses[thisid.data("uid")].reste;
                    });

                    if(userInput > totalReste){
                        userInput = totalReste;
                    }

                    $('.newdepenselist.finance .newDepenseListCard.active').each(function (){
                        var thisid = $(this);
                        var reste = depenselistobj.data.depenses[thisid.data("uid")].reste;
                        /!*if($('.input-amount-multiselect-tree').val() > reste){
                            depenselistobj.data.depenses[thisid.data("uid")].part = reste;
                        }else{*!/
                            if (!isNaN(Math.round(((reste / totalReste) * userInput)))) {
                                depenselistobj.data.depenses[thisid.data("uid")].part = Math.round(((reste / totalReste) * userInput));
                            } else {
                                depenselistobj.data.depenses[thisid.data("uid")].part = 0;
                            }
                        /!*}*!/
                    });

                    depenselistobj.action.updateAmountView(depenselistobj)
                },*/
                disturbAmount : function (depenselistobj){
                    var totalReste = 0;
                    var tlCart = 0;
                    if(depenselistobj.data.iscofinancementftl && typeof depenselistobj.data.cart.campagne[depenselistobj.data.currentOrga.id] != "undefined") {
                        tlCart = depenselistobj.data.cart.campagne[depenselistobj.data.currentOrga.id];
                        $('.newDepenseListCard.active .entete-ftlpart').show();
                    }
                    var userInput = parseInt($('.input-amount-multiselect-tree').val());
                    var finaldbln = 0;

                    $('.newdepenselist.finance .newDepenseListCard.active').each(function () {
                        var thisid = $(this);
                        totalReste += depenselistobj.data.depenses[thisid.data("uid")].reste;
                    });

                    if(userInput > totalReste){
                        userInput = totalReste;
                    }

                    if(depenselistobj.data.iscofinancementftl && tlCart >= userInput/2 ){
                        finaldbln = userInput/2;
                    }else if(depenselistobj.data.iscofinancementftl){
                        finaldbln = tlCart;
                    }

                    $('.tldoubleamount').html(finaldbln);

                    $('.newdepenselist.finance .newDepenseListCard.active').each(function () {
                        var thisid = $(this);
                        var reste = depenselistobj.data.depenses[thisid.data("uid")].reste;
                        /*if($('.input-amount-multiselect-tree').val() > reste){
                            depenselistobj.data.depenses[thisid.data("uid")].part = reste;
                        }else{*/
                        if (!isNaN(Math.round(((reste / totalReste) * userInput)))) {
                            depenselistobj.data.depenses[thisid.data("uid")].part = Math.round(((reste / totalReste) * userInput)) - Math.round(((reste / totalReste) * finaldbln));
                            depenselistobj.data.depenses[thisid.data("uid")].ftlpart = Math.round(((reste / totalReste) * finaldbln));
                        } else {
                            depenselistobj.data.depenses[thisid.data("uid")].part = 0;
                            depenselistobj.data.depenses[thisid.data("uid")].ftlpart = 0;
                        }
                        /*}*/
                    });

                    depenselistobj.action.updateAmountView(depenselistobj);

                },
                updateAmountView : function (depenselistobj){
                    $('.dropdown-menu-orgaaction table').html('');
                    var html = `<tr>
                                    <th>Label</th>
                                    <th>Montant</th>
                                    <th>Reste</th>
                                    <th>Votre contribution</th>
                                    ${ depenselistobj.data.iscofinancementftl ? '<th>contribution FTL</th>' : '' }
                                </tr>`;
                    var totalprice = 0;
                    var totalreste = 0;
                    var totalpart = 0;
                    var totalftlpart = 0;
                    var userInput = $('.input-amount-multiselect-tree').val();
                    $.each(depenselistobj.data.depenses , function (index, value){
                        var thisid = $(this);
                        $('.newdepenselist.finance .newDepenseListCard.active[data-uid="'+value.uid+'"] .entete-part').html(`votre contribution : ${value.part}€ `);
                        $('.newdepenselist.finance .newDepenseListCard.active[data-uid="'+value.uid+'"] .entete-ftlpart').html(`contribution FTL ${value.ftlpart}€ `);
                    });
                    $('.newdepenselist.finance .newDepenseListCard.active').each(function (){
                        var value = depenselistobj.data.depenses[$(this).data('uid')];
                        html += `<tr>
                                        <th>${value.poste}</th>
                                        <td>${value.price}</td>
                                        <td>${value.reste}</td>
                                        <td>${value.part}</td>
                                        ${ depenselistobj.data.iscofinancementftl ? '<td>'+value.ftlpart+'</td>' : '' }
                                </tr>`;
                        totalprice += value.price;
                        totalreste += value.reste;
                        totalpart += value.part;
                        totalftlpart += value.ftlpart;
                    });
                    html += `<tr>
                                    <th>Total:</th>
                                    <th>${totalprice}</th>
                                    <th>${totalreste}</th>
                                    <th>${totalpart}</th>
                                    ${ depenselistobj.data.iscofinancementftl ? '<td>'+totalpart+'</td>' : '' }
                             </tr>`;

                    $('.dropdown-menu-orgaaction table').append(html);

                    if(
                        userInput != "" &&
                        userInput > 0 &&
                        depenselistobj.data.currentOrga.name != '' &&
                        depenselistobj.data.currentOrga.id != ''
                    ){
                        $('.statbutton-cofinance').prop('disabled', false);
                    }else{
                        $('.statbutton-cofinance').prop('disabled', 'disabled');
                    }
                },
                setRowSpanFin : function (depenselistobj, tableId, col){
                    var rowSpanMapff1 = {};
                    var rowSpanMapffB = {};
                    var rowSpanMapf = {};

                    $(tableId).find('tr').each(function(){

                        var valueOfTheSpannableCell1 = $($(this).children('td')[1]).text();
                        $($(this).children('td')[1]).attr('data-original-value1', valueOfTheSpannableCell1);
                        rowSpanMapff1[valueOfTheSpannableCell1] = true;

                        if(col != 1){
                            $($(this).children('td')[col]).attr('data-original-value1', valueOfTheSpannableCell1);

                            var valueOfTheSpannableCellBefore = $($(this).children('td')[col - 1]).text();
                            $($(this).children('td')[col]).attr('data-original-valueB', valueOfTheSpannableCellBefore);
                            rowSpanMapffB[valueOfTheSpannableCellBefore] = true;

                            var valueOfTheSpannableCell = $($(this).children('td')[col]).text();
                            $($(this).children('td')[col]).attr('data-original-value'+col+'', valueOfTheSpannableCell);
                            rowSpanMapf[valueOfTheSpannableCell] = true;
                        }

                    });


                    if(col == 1){
                        $.each(rowSpanMapff1 , function(row, rowGroup) {
                            var $cellsToSpan1 = $(tableId+' td[data-original-value1="' + row + '"]');
                            var numberOfRowsToSpan1 = $cellsToSpan1.length;
                            $cellsToSpan1.each(function (index) {
                                if (index == 0) {
                                    $(this).attr('rowspan', numberOfRowsToSpan1);
                                } else {
                                    $(this).hide();
                                }
                            });
                        });
                    }else{
                        $.each(rowSpanMapff1 , function(row1, rowGroup1) {
                            $.each(rowSpanMapffB , function(rowB, rowGroupB) {
                                $.each(rowSpanMapf , function(row, rowGroup) {
                                    var $cellsToSpan = $(tableId+' td[data-original-value1="'+row1+'"][data-original-valueB="'+rowB+'"][data-original-value'+col+'="'+row+'"]');
                                    var numberOfRowsToSpan = $cellsToSpan.length;
                                    $cellsToSpan.each(function(index){
                                        if(index==0){
                                            $(this).attr('rowspan', numberOfRowsToSpan);
                                        }else{
                                            $(this).hide();
                                        }
                                    });
                                });
                            });
                        });
                    }

                },
                loadData : function (depenselistobj){
                    ajaxPost("", baseUrl + '/survey/form/financerlogs/answer/' + answerObj._id.$id, {}, function(data) {
                        depenselistobj.view.createPageFin(data["tableAnswers"]);
                    }, null, "json", {
                        async: true
                    });
                }
            },
            panel : {
                description: function(depenselistobj, index , depense){
                    var html = '';
                    html += `<div data-uid="${index}" class="newDL-body-description-tab">
                                  <div data-uid="${index}" class="newDL-body-description-tab">
                                    ajoutée par : <span class="span-user"> ${ notEmpty(depense.user) && notEmpty(depenselistobj.data.personlinks[depense.user]) ? depenselistobj.data.personlinks[depense.user].name : depenselistobj.data.nr } </span>
                                  </div>
                                  <div data-uid="${index}" class="newDL-body-description-tab">
                                        <div class="span-desc"> Description: </div>
                                        <div class="span-description"> ${notEmpty(depense.description) ? depense.description : depenselistobj.data.nr } </div>
                                  </div>
                             </div>`;
                    return html;
                },
                financeList : function(depenselistobj, index , depense){
                    var html = '';
                    if(notEmpty(depense.financer)){
                        $.each(depense.financer, function (i, fin){
                            if(!notEmpty(depense.status) || depense.status != 'financementFTL') {
                                html += depenselistobj.view.getFinanceView(depenselistobj,index, i, fin);
                            }
                        });
                    }else{
                        html += `<div class="center">Pas de financeur</div>`;
                    }
                    return html;
                },
                financeBarChart : function(depenselistobj, index , depense){
                    return `<svg id="financebarchart${depenselistobj.data.depenseSubType == "finance" ? index : ''}" ></svg>`;
                },
                finance : function(depenselistobj, index , depense){
                    return '';
                }
            },
            event : {
                menutabs : function (){
                    $('.menutabs').off().on('click' , function (){
                        var thisbtn = $(this);
                        var isBodyActive = $('.newDL-body[data-uid="'+thisbtn.data('uid')+'"]').hasClass('active');
                        var isTabsActive = $('.newDL-body-tab[data-uid="'+thisbtn.data('uid')+'"][data-content="'+thisbtn.data('content')+'"]').hasClass('active');
                        if(isBodyActive && isTabsActive) {
                            $('.menutabs[data-uid="'+thisbtn.data('uid')+'"]').removeClass('active');
                            $('.newDL-body-tab[data-uid="'+thisbtn.data('uid')+'"]').removeClass('active');
                            $('.newDL-body[data-uid="'+thisbtn.data('uid')+'"]').removeClass('active');
                        }else if(isBodyActive && !isTabsActive) {
                            $('.menutabs').removeClass('active');
                            $('.menutabs[data-uid="'+thisbtn.data('uid')+'"]').addClass('active');
                            $('.newDL-body-tab').removeClass('active');
                            $('.newDL-body-tab[data-uid="'+thisbtn.data('uid')+'"]').addClass('active');
                        }else{
                            $('.menutabs[data-uid="'+thisbtn.data('uid')+'"][data-content="'+thisbtn.data('content')+'"]').addClass('active');
                            $('.newDL-body[data-uid="'+thisbtn.data('uid')+'"]').addClass('active');
                            $('.newDL-body-tab[data-uid="'+thisbtn.data('uid')+'"][data-content="'+thisbtn.data('content')+'"]').addClass('active');
                        }
                        setTimeout(function (){
                            $('.contentInformation').masonry({
                                itemSelector: '.newDepenseListCard',
                                columnWidth: '.newDepenseListCard',
                                transitionDuration : '0.5s'
                            }).masonry('reload');
                        }, 500);
                    });

                    $('.depmodal[data-content="finance"]').off().on('click' , function (){
                        var thisbtn = $(this);
                        if(thisbtn.hasClass('active')){
                            $('i', this).removeClass('fa-times');
                            $('i', this).addClass('fa-money');
                            $('span', this).html('financer');
                        }else{
                            $('i', this).addClass('fa-times');
                            $('i', this).removeClass('fa-money');
                            $('span', this).html('enlever');
                        }
                        thisbtn.toggleClass('active');
                        $('.newDepenseListCard[data-uid="'+thisbtn.attr("data-uid")+'"').toggleClass('active');
                        depenselistobj.action.updateselect();
                        setTimeout(function (){
                            $('.contentInformation').masonry();
                        }, 500);
                    });

                    $('.depmodal[data-content="exclude"]').off().on('click' , function (){
                        var depenseIndex = $(this).data('uid');
                        var params = {
                            id: answerObj._id.$id,
                            collection: 'answers',
                            path: 'answers.aapStep1.depense.' + depenseIndex + '.include',
                            value: 'false',
                            setType : 'bool'
                        }
                        $.confirm({
                            title: "Inclure ce ligne de depense et ses financements",
                            content: `Cette action va inclure ce ligne de depense et ses financements dans les données pris en compte par la plateforme`,
                            buttons: {
                                yes: {
                                    text: 'Inclure',
                                    btnClass: 'btn btn-danger',
                                    action: function() {
                                        mylog.log(params, '_response')
                                        dataHelper.path2Value(params, function(__response) {
                                            if (__response.result && __response.result === true) {
                                                mylog.log(__response.result, '_response')
                                                showLoader('#question_financer');
                                                showLoader('#question_depense');
                                                reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>" );
                                            }
                                        });
                                    }
                                },
                                no: {
                                    text: coTranslate('cancel'),
                                    btnClass: 'btn btn-default'
                                }
                            }
                        });
                    });

                    $('.depmodal[data-content="include"]').off().on('click' , function (){

                        var depenseIndex = $(this).data('uid');
                        var params = {
                            id: answerObj._id.$id,
                            collection: 'answers',
                            path: 'answers.aapStep1.depense.' + depenseIndex + '.include',
                            value: 'true',
                            setType : 'bool'
                        }
                        $.confirm({
                            title: "Cacher momentanément ce ligne de depense et ses financements",
                            content: `Cette action va cacher momentanément ce ligne de depense et ses financements dans les données pris en compte par la plateforme."`,
                            buttons: {
                                yes: {
                                    text: 'Cacher',
                                    btnClass: 'btn btn-danger',
                                    action: function() {
                                        mylog.log(params, '_response')
                                        dataHelper.path2Value(params, function(__response) {
                                            if (__response.result && __response.result === true) {
                                                mylog.log(__response.result, '_response')
                                                showLoader('#question_financer');
                                                showLoader('#question_depense');
                                                reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>" );
                                            }
                                        });
                                    }
                                },
                                no: {
                                    text: coTranslate('cancel'),
                                    btnClass: 'btn btn-default'
                                }
                            }
                        });
                    });

                    $('.depmodal[data-content="comment"]').off().on('click' , function () {
                        var thisbtn = $(this);
                        $('#modal-preview-comment').addClass('comment-modal-aac-financement')

                        commentObj.openPreview('forms', answerObj.form, `${answerObj?.form ? answerObj.form : "formId"}.${thisPhp.step?._id ? thisPhp.step._id.$id : "stepId"}.${answerObj?._id?.$id ? answerObj._id.$id : "answerId"}.${thisPhp.key}.question_${thisPhp.key}.comment.financement.depensekey.${ thisPhp.inputDepenseKey }.depenseindex.${thisbtn.data('uid')}`, 'Commentaire')
                        commentObj.closePreview = function(){
                            $('#modal-preview-comment').removeClass('comment-modal-aac-financement')
                            $(".main-container").off();
                            $("#modal-preview-comment").css("display", "none");
                        }
                        // commentObj.openPreview('answers',answerObj._id.$id,thisbtn.data('uid'), 'aapStep1','null','null',{notCloseOpenModal : true})
                    });

                    $('.depmodal[data-content="delete"]').off().on("click",function() {
                        var thisbtn = $(this);
                        bootbox.dialog({
                            title: trad.confirmdelete,
                            message: "<span class='text-red bold'><i class='fa fa-warning'></i> "+trad.actionirreversible+"</span>",
                            buttons: [
                                {
                                    label: "Ok",
                                    className: "btn btn-primary pull-left",
                                    callback: function() {
                                        var formQ = {
                                            value:null,
                                            formParentId : answerObj.form,
                                            collection : "answers",
                                            id : answerObj._id.$id,
                                            pull : "answers.aapStep1.depense",
                                            path : "answers.aapStep1.depense."+thisbtn.data('uid')
                                        };

                                        if (typeof parentfId != "undefined") {
                                            formQ["formParentId"] = parentfId;
                                        }

                                        dataHelper.path2Value( formQ , function(params) {
                                            showLoader('#question_financer');
                                            showLoader('#question_depense');
                                            reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>" );
                                        } );
                                    }
                                },
                                {
                                    label: "Annuler",
                                    className: "btn btn-default pull-left",
                                    callback: function() {}
                                }
                            ]
                        });
                    });

                    $('.depmodal[data-content="edit"]').off().on("click",function() {
                        var thisbtn = $(this);
                        tplCtx = {
                            id : answerObj._id.$id ,
                            collection : "answers",
                            form : "aapStep1",
                            updatePartial: true,
                            setType : [
                                {
                                    "path": "price",
                                    "type": "int"
                                },
                                {
                                    "path": "date",
                                    "type": "isoDate"
                                }
                            ]
                        }

                        delete tplCtx.arrayForm;
                        tplCtx.path = "answers.aapStep1.depense." + thisbtn.data('uid');
                        if (answerObj?.answers?.aapStep1?.depense?.[thisbtn.data('uid')]?.imageKey && newdepenseDataImg[answerObj?.answers?.aapStep1?.depense?.[thisbtn.data('uid')]?.imageKey]) {
                            var currentdepenseImg = JSON.parse(JSON.stringify(newdepenseDataImg[answerObj?.answers?.aapStep1?.depense?.[thisbtn.data('uid')]?.imageKey]))
                            currentdepenseImg.title ? delete currentdepenseImg.title : "";
                            currentdepenseImg.subKey ? delete currentdepenseImg.subKey : "";
                            currentdepenseImg.metaData ? delete currentdepenseImg.metaData : "";

                            sectionDyf.depenseDyf.jsonSchema.properties.image.initList = [currentdepenseImg];
                            tplCtx.lastKeygen = currentdepenseImg.imageKey;
                        } else if (sectionDyf.depenseDyf.jsonSchema.properties.image.initList) {
                            delete sectionDyf.depenseDyf.jsonSchema.properties.image.initList
                        }
                        dyFObj.openForm( sectionDyf.depenseDyf,null, answerObj.answers.aapStep1.depense[thisbtn.data('uid')],null,null,{
                            type : "bootbox",
                            notCloseOpenModal : true,
                        });
                    });

                    $('.editfinance').off().on("click",function(){
                        var thisbtn = $(this);
                        $(`.newDL-entete-titre[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).hide();
                        $(`.newDL-entete-titre-select[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).append($(".financerdropdown").html());

                        var actv = depenselistobj.data.depenses[thisbtn.data('uid')].financer[thisbtn.data('fid')].amount;
                        $(`.entete-financement-input[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).val(actv);

                        $(`.newDL-entete-titre-select[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"] div.mm-dropdown .textfirst`).html($(`.input-option[data-value="${thisbtn.data('vid')}"]`).html());
                        $(`.newDL-entete-titre-select[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"] div.mm-dropdown .option`).val(thisbtn.data('vid'));

                        $(`.entete-financement[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).hide();
                        $(`.editfinanceftl[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).hide();
                        $(`.entete-financement-edit[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).show();

                        $(`.dropdown-toggle[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).hide();

                        $(`.newDL-entete-titre-select[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"] div.mm-dropdown .textfirst`).off().on('click', function() {
                            $('.input-option-search input').each(function( index ) {
                                $( this ).val("");
                            });
                            $(this).html("<span class='depenseselectfinancer'>"+$(this).data('placeholder')+"</span> <i class='fa fa-caret-down'></i>");
                            $(`.newDL-entete-titre-select[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"] div.mm-dropdown > ul > li.input-option`).toggle('fast');
                            $(`.newDL-entete-titre-select[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"] div.mm-dropdown .option`).val("");
                            if(
                                $(`.entete-financement-input[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).val() != "" &&
                                $(`.entete-financement-input[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).val() > 0 &&
                                ( $(`.entete-financement-input[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).val() != "" || $('.input-email-multiselect-tree').val() != "" ) &&
                                $(`.newDL-entete-titre-select[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"] .mm-dropdown .option`).val() != ""
                            ){
                                $(`.cofinance-edit[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).prop('disabled', false);
                            }else{
                                $(`.cofinance-edit[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).prop('disabled', 'disabled');
                            }

                        });
                        $(`.newDL-entete-titre-select[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"] div.mm-dropdown > ul > li.input-option:not(.input-option-search)`).off().on('click', function() {
                            $('.input-option-search input').each(function( index ) {
                                $( this ).val("");
                            });
                            var livalue = $(this).data('value');
                            var lihtml = $(this).html();
                            if(typeof depenseFinancer[livalue] != "undefined"){
                                depenselistobj.data.currentOrga.name = depenseFinancer[livalue].name;
                            }else if (typeof depenseFinancerOrga[livalue] != "undefined") {
                                depenselistobj.data.currentOrga.name = depenseFinancerOrga[livalue].name;
                            }
                            depenselistobj.data.currentOrga.id = livalue;
                            $(`.newDL-entete-titre-select[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"] div.mm-dropdown > ul > li.input-option`).hide('fast');
                            $(`.newDL-entete-titre-select[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"] div.mm-dropdown .textfirst`).html(lihtml);
                            $(`.newDL-entete-titre-select[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"] div.mm-dropdown .option`).val(livalue);
                            if(
                                $(`.entete-financement-input[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).val() != "" &&
                                $(`.entete-financement-input[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).val() > 0 &&
                                ( $(`.entete-financement-input[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).val() != "" || $('.input-email-multiselect-tree').val() != "" ) &&
                                $(`.newDL-entete-titre-select[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"] div.mm-dropdown .option`).val() != ""
                            ){
                                $(`.cofinance-edit[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).prop('disabled', false);
                            }else{
                                $(`.cofinance-edit[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).prop('disabled', 'disabled');
                            }
                        });
                        $(`.newdepenselist.finance .entete-financement-input[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).off().on('keyup', function (){
                            if(
                                $(`.newdepenselist.finance .entete-financement-input[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).val() != "" &&
                                $(`.newdepenselist.finance .entete-financement-input[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).val() > 0 &&
                                ( $(`.newdepenselist.finance .entete-financement-input[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).val() != "" || $('.input-email-multiselect-tree').val() != "" ) &&
                                $(`.newDL-entete-titre-select[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"] div.mm-dropdown .option`).val() != ""
                            ){
                                $(`.cofinance-edit[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).prop('disabled', false);
                            }else{
                                $(`.cofinance-edit[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).prop('disabled', 'disabled');
                            }
                        });
                    });

                    $('.validcofinpay').off().on("click",function(){
                        var thisbtn = $(this);
                        tplCtx = {
                            id : answerObj._id.$id ,
                            collection : "answers",
                            form : "aapStep1",
                            path : "answers.aapStep1.depense." + thisbtn.data('uid') +".financer."+ thisbtn.data('fid')+".status",
                            value : "paid"
                        }

                        delete tplCtx.arrayForm;
                        dataHelper.path2Value(tplCtx, function (params) {
                            thisbtn.html(`<i class="fa fa-check"></i> Payée`).prop('disabled','disabled');
                        });

                    });

                    $('.editfinanceftl').off().on("click",function(){
                        var thisbtn = $(this);

                        var actv = depenselistobj.data.depenses[thisbtn.data('uid')].financer[thisbtn.data('fid')].amount;
                        $(`.entete-financementftl-input[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).val(actv);

                        $(`.ftlcase[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).hide();
                        $(`.entete-financementftl-edit[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).show();

                        $(`.dropdown-toggle[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fidtl')}"]`).hide();

                        $(`.newdepenselist.finance .entete-financementftl-input[data-uid="${thisbtn.data('uid')}"][data-fidtl="${thisbtn.data('fidtl')}"]`).off().on('keyup', function (){
                            if(
                                $(`.newdepenselist.finance .entete-financementftl-input[data-uid="${thisbtn.data('uid')}"][data-fidtl="${thisbtn.data('fidtl')}"]`).val() != "" &&
                                $(`.newdepenselist.finance .entete-financementftl-input[data-uid="${thisbtn.data('uid')}"][data-fidtl="${thisbtn.data('fidtl')}"]`).val() > 0
                            ){
                                $(`.cofinanceftl-edit[data-uid="${thisbtn.data('uid')}"][data-fidtl="${thisbtn.data('fidtl')}"]`).prop('disabled', false);
                            }else{
                                $(`.cofinanceftl-edit[data-uid="${thisbtn.data('uid')}"][data-fidtl="${thisbtn.data('fidtl')}"]`).prop('disabled', 'disabled');
                            }
                        });
                    });

                    $('.cofinance-editcancel').off().on("click",function(){
                        var thisbtn = $(this);
                        $(`.newDL-entete-titre[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).show();
                        $(`.newDL-entete-titre-select[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).html("");

                        $(`.entete-financement[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).show();

                        $(`.entete-financement-edit[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).hide();
                        $(`.dropdown-toggle[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).show();
                    });

                    $('.cofinanceftl-editcancel').off().on("click",function(){
                        var thisbtn = $(this);
                        $(`.ftlcase[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).show();
                        $(`.entete-financementftl-edit[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).hide();

                        $(`.dropdown-toggle[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fidtl')}"]`).show();

                    });

                    $('.cofinance-edit').off().on("click",function(){
                        var thisbtn = $(this);

                        var tplCtx = {
                            id : answerObj._id.$id ,
                            collection : "answers",
                            form : "aapStep1",
                            setType : "int"
                        }

                        delete tplCtx.arrayForm;

                        tplCtx.path = "answers.aapStep1.depense."+thisbtn.data("uid")+".financer."+thisbtn.data("fid")+".amount";

                        tplCtx.value = $(`.newdepenselist.finance .entete-financement-input[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).val();

                        if($(`.newdepenselist.finance .entete-financement-input[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).val() > 0) {
                            dataHelper.path2Value(tplCtx, function (params) {
                                showLoader('#question_financer');
                                reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>" );
                            });
                        }
                    });

                    $('.cofinanceftl-edit').off().on("click",function(){
                        var thisbtn = $(this);
                        var tplCtx = {
                            id : answerObj._id.$id ,
                            collection : "answers",
                            form : "aapStep1",
                            setType : "int"
                        }

                        delete tplCtx.arrayForm;

                        tplCtx.path = "answers.aapStep1.depense."+thisbtn.data("uid")+".financer."+thisbtn.data("fid")+".amount";

                        tplCtx.value = $(`.newdepenselist.finance .entete-financementftl-input[data-uid="${thisbtn.data('uid')}"][data-fid="${thisbtn.data('fid')}"]`).val();

                        if($(`.newdepenselist.finance .entete-financementftl-input[data-uid="${thisbtn.data('uid')}"][data-fidtl="${thisbtn.data('fidtl')}"]`).val() > 0) {
                           dataHelper.path2Value(tplCtx, function (params) {
                                showLoader('#question_financer');
                                reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>" );
                            });
                        }
                    });

                    $('.deletefinance').off().on("click",function(){
                        var thisbtn = $(this);
                        bootbox.dialog({
                            title: trad.confirmdelete,
                            message: "<span class='text-red bold'><i class='fa fa-warning'></i> "+trad.actionirreversible+"</span>",
                            buttons: [
                                {
                                    label: "Ok",
                                    className: "btn btn-primary pull-left",
                                    callback: function() {
                                        var formQ = {
                                            value:null,
                                            formParentId : answerObj.form,
                                            collection : "answers",
                                            id : answerObj._id.$id,
                                            pull : "answers.aapStep1.depense."+thisbtn.data('uid')+"financer",
                                            path : "answers.aapStep1.depense."+thisbtn.data('uid')+"financer."+thisbtn.data('fid')
                                        };

                                        if (typeof parentfId != "undefined") {
                                            formQ["formParentId"] = parentfId;
                                        }

                                        dataHelper.path2Value( formQ , function(params) {
                                            if(typeof thisbtn.data("ftlid") != "undefined"){
                                                var formQ = {
                                                    value:null,
                                                    formParentId : answerObj.form,
                                                    collection : "answers",
                                                    id : answerObj._id.$id,
                                                    pull : "answers.aapStep1.depense."+thisbtn.data('uid')+"financer",
                                                    path : "answers.aapStep1.depense."+thisbtn.data('uid')+"financer."+thisbtn.data('ftlid')
                                                };

                                                if (typeof parentfId != "undefined") {
                                                    formQ["formParentId"] = parentfId;
                                                }
                                                dataHelper.path2Value( formQ , function(params) {
                                                    showLoader('#question_financer');
                                                    showLoader('#question_depense');
                                                    reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>" );
                                                } );
                                            }else{
                                                showLoader('#question_financer');
                                                showLoader('#question_depense');
                                                reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>" );
                                            }

                                        } );
                                    }
                                },
                                {
                                    label: "Annuler",
                                    className: "btn btn-default pull-left",
                                    callback: function() {}
                                }
                            ]
                        });
                    });

                    $('.deletefinanceftl').off().on("click",function(){
                        var thisbtn = $(this);
                        bootbox.dialog({
                            title: trad.confirmdelete,
                            message: "<span class='text-red bold'><i class='fa fa-warning'></i> "+trad.actionirreversible+"</span>",
                            buttons: [
                                {
                                    label: "Ok",
                                    className: "btn btn-primary pull-left",
                                    callback: function() {
                                        var formQ = {
                                            value:null,
                                            formParentId : answerObj.form,
                                            collection : "answers",
                                            id : answerObj._id.$id,
                                            pull : "answers.aapStep1.depense."+thisbtn.data('uid')+"financer",
                                            path : "answers.aapStep1.depense."+thisbtn.data('uid')+"financer."+thisbtn.data('fid')
                                        };

                                        if (typeof parentfId != "undefined") {
                                            formQ["formParentId"] = parentfId;
                                        }

                                        dataHelper.path2Value( formQ , function(params) {
                                            showLoader('#question_financer');
                                            showLoader('#question_depense');
                                            reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>" );
                                        } );
                                    }
                                },
                                {
                                    label: "Annuler",
                                    className: "btn btn-default pull-left",
                                    callback: function() {}
                                }
                            ]
                        });
                    });
                },
                personlinks : function(depenselistobj){
                    if (typeof answerObj != "undefined" && answerObj.links) {
                        ajaxPost(
                            null,
                            baseUrl + "/" + moduleId + "/element/get/type/citoyens", {
                                id: answerObj.links.answered
                            },
                            function(data) {
                                if (data.result) {
                                    depenselistobj.data.personlinks = data.map;
                                }
                            },
                            null,
                            "json", {
                                async: false
                            }
                        );
                    }
                },
                barchart : function (depenselistobj){
                    if(depenselistobj.data.depenseSubType == "finance") {
                        $.each(depenselistobj.data.depenses, function (index, value) {
                            depenselistobj.view.generateBarChart(depenselistobj, index, value)
                        });
                    }
                }
            }
        }

        $(document).ready(function() {
            $(".input-option-search input").off().on('input', function() {
                var thisbtn = $(this);
                var val = this.value.toLowerCase();
                var dom = $(`div.mm-dropdown > ul > li.input-option:not(.input-option-search)[data-type="${thisbtn.data("type")}"]`);
                dom.hide();
                if (!val.length)
                    dom.show();
                else {
                    dom.filter(function() {
                        var match = $(this).data('name') ? $(this).data('name') : '';
                        if (typeof match !== 'string')
                            return (false);
                        match = match.toLowerCase().match(new RegExp(val.toLowerCase(), 'g'));
                        return (match !== null);
                    }).show();
                }
            });
            if (newdepmod == "w") {
                depenselistobj.data.actionlist = ["description", "edit", "comment", "delete"];
            } else {
                depenselistobj.data.actionlist = ["description"];
            }
            depenselistobj.data.depenseSubType = depenseSubType;
            if(depenseSubType == "finance"){
                depenselistobj.data.actionlist.push("financeList","finance" );
            }
            depenselistobj.event.personlinks(depenselistobj);
            depenselistobj.init(depenselistobj);

            var fLog = financementlogObj.action.init();
            fLog.action.loadData(financementlogObj , answerObj._id.$id);
            setTimeout(function (){
                $('.contentInformation').masonry({
                    itemSelector: '.newDepenseListCard',
                    columnWidth: '.newDepenseListCard',
                    transitionDuration : '0.5s'
                });
                financementlogObj.action.actualisefinancerlog(financementlogObj);
            }, 500);
            if(typeof userConnected != "undefined"){
                $('.input-name-multiselect').val(userConnected.name);
                $('.input-email-multiselect').val(userConnected.email)
            }
            sectionDyf.depenseDyf = {
                jsonSchema : {
                    title : "Budget prévisionnel",
                    icon : "fa-money",
                    text : "Décrire ici les principaux postes de dépenses : à quoi correspondent les coûts ? <br/>Préciser ce qui relève des dépenses de fonctionnement et des dépenses d’investissement.<br/>Les coûts doivent être en <b>hors taxe</b>.",
                    properties : {
                        "poste" : {
                            "inputType" : "text",
                            "label" : "Poste de dépense",
                            "placeholder" : "Poste de dépense",
                            "rules" : {"required" : true}
                        },
                        "description" : {
                                "inputType" : "textarea",
                                "label" : "Description",
                                "placeholder" : "Description",
                                "rules" : {"required" : false}
                        },
                        "image" : {
                            "label" : "image",
                            "placeholder" : "",
                            "inputType" : "uploader",
                            "docType" : "image",
                            "itemLimit" : 1,
                            "filetypes" : ["jpeg", "jpg", "gif", "png"]
                        },
                        "price" : {
                            "inputType" : "text",
                            "label" : "Montant",
                            "placeholder" : "Montant",
                            "rules" : {"required" : true}
                        }
                    },
                    afterBuild : function () {
                        setTimeout(function () {
                            //uploadObj.set("answers",answerObj._id.$id);
                            $('.qq-upload-button-selector.btn.btn-primary').css({
                                position : 'absolute',
                                left : '50%',
                                transform : 'translateX(-50%)'
                            });
                        }, 200);
                        var bootboxClose = setInterval(() => {
                            if ($("#modal-form-bootbox .bootbox-close-button").length > 0) {
                                $("#modal-form-bootbox .bootbox-close-button")[0].addEventListener("click", function() {
                                    dyFObj.closeForm();
                                })
                                clearInterval(bootboxClose);
                            }
                        }, 700);

                        $('#price').attr('type','number');
                    },
                    save : function (data) {
                        dyFObj.commonAfterSave(data,function(){

                            var depensename = "";

                            tplCtx.value = { date : "now" };

                            $.each( sectionDyf.depenseDyf.jsonSchema.properties , function(k,val) {
                                tplCtx.value[k] = $("#"+k).val();

                                if(k == "poste")
                                    depensename = tplCtx.value[k];
                                if(k == "description")
                                    tplCtx.value[k] = data.description;

                            });
                            

                            if(notEmpty(tplCtx.keygen))
                                tplCtx.value["imageKey"] = tplCtx.keygen;
                            tplCtx.lastKeygen ? delete tplCtx.lastKeygen : "";

                            if(typeof tplCtx.value != "undefined"){
                                dataHelper.path2Value( tplCtx, function(params) {
                                    delete tplCtx.keygen;
                                    dyFObj.closeForm();
                                    ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/newdepense/answerid/' + answerObj._id.$id,
                                        {
                                            name : depensename,
                                            url : window.location.href
                                        },
                                        function (data) {

                                        }, "html");

                                    ajaxPost("", baseUrl + "/co2/aap/commonaap/action/notifyAddDepenseLine", {
                                        answerId:answerId,
                                        depense: tplCtx.value.poste +": "+  tplCtx.value.price + " Euro"
                                    }, function(data){}, function(error){}, "json")

                                    var tplCtxproject = {};
                                    showLoader('#question_financer');
                                    showLoader('#question_depense');
                                    reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>" );

                                }, function(p) {

                                });
                            }
                        });

                        // nesorina fa mampisy bug ajout ligne de dépense avec image
                        // delete tplCtx.setType;
                    },
                    beforeBuild : function(data){
                        uploadObj.set("answers",answerObj._id.$id);

                        const chars = 'azertyuiopqsdfghjklmwxcvbn123456789';
                        var rand = (min = 0 , max = 1000) => Math.floor(Math.random() * (max - min) + min);
                        randChar = (length = 6) => {
                            var randchars = [];
                            for(let i = 0; i < length; i++){
                                randchars.push(chars[rand(0, chars.length)]);
                            }

                            return randchars.join('');
                        }
                        keygenerator =  (prefix = 'a-', sufix='') => `${prefix}${randChar()}${sufix}`;
                        var keygen = keygenerator();
                        uploadObj.path += "/subKey/answers.aapStep1.depense.image/imageKey/"+ (tplCtx.lastKeygen ? tplCtx.lastKeygen : keygen);
                        tplCtx.keygen = tplCtx.lastKeygen ? tplCtx.lastKeygen : keygen;
                    }
                }
            };

            var fLog = financementlogObj.action.init();
            fLog.action.loadData(financementlogObj,answerObj._id.$id,null);

            $('.contentFinanceAction div.mm-dropdown .textfirst').off().on('click', function() {
                $(this).html("<span class='depenseselectfinancer'>"+$(this).data('placeholder')+"</span> <i class='fa fa-caret-down'></i>");
                $('div.mm-dropdown > ul > li.input-option').toggle('fast');
                $("div.mm-dropdown .option").val("");
                if(
                    $('.input-amount-multiselect-tree').val() != "" &&
                    $('.input-amount-multiselect-tree').val() > 0 &&
                    ( $('.input-name-multiselect-tree').val() != "" || $('.input-email-multiselect-tree').val() != "" ) &&
                    $('.mm-dropdown .option').val() != ""
                ){
                    $('.statbutton-cofinance').prop('disabled', false);
                }else{
                    $('.statbutton-cofinance').prop('disabled', 'disabled');
                }
                depenselistobj.data.currentOrga.name = '';
                depenselistobj.data.currentOrga.id = '';
            });
            $('.contentFinanceAction div.mm-dropdown > ul > li.input-option:not(.input-option-search').off().on('click', function() {
                $('.input-option-search input').each(function( index ) {
                    $( this ).val("");
                });
                var livalue = $(this).data('value');
                var lihtml = $(this).html();
                if(typeof depenseFinancer[livalue] != "undefined"){
                    depenselistobj.data.currentOrga.name = depenseFinancer[livalue].name;
                }else if (typeof depenseFinancerOrga[livalue] != "undefined") {
                    depenselistobj.data.currentOrga.name = depenseFinancerOrga[livalue].name;
                }
                depenselistobj.data.currentOrga.id = livalue;
                $('.contentFinanceAction div.mm-dropdown > ul > li.input-option').hide('fast');
                $('.contentFinanceAction div.mm-dropdown .textfirst').html(lihtml);
                $(".contentFinanceAction div.mm-dropdown .option").val(livalue);
                if(
                    $('.input-amount-multiselect-tree').val() != "" &&
                    $('.input-amount-multiselect-tree').val() > 0 &&
                    ( $('.input-name-multiselect-tree').val() != "" || $('.input-email-multiselect-tree').val() != "" ) &&
                    $('.contentFinanceAction .mm-dropdown .option').val() != ""
                ){
                    $('.statbutton-cofinance').prop('disabled', false);
                }else{
                    $('.statbutton-cofinance').prop('disabled', 'disabled');
                }
                depenselistobj.action.disturbAmount(depenselistobj);
            });
            $('.input-amount-multiselect-tree').off().on('keyup', function (){
                depenselistobj.action.disturbAmount(depenselistobj);
            });
            $('.statbutton-cofinance').off().on("click", function () {
                var lineToPay = [];
                $('.newdepenselist.finance .newDepenseListCard.active').each(function (){
                    var thisid = $(this).data('uid');
                    lineToPay.push({
                        id : thisid,
                        amount : depenselistobj.data.depenses[thisid].part,
                        ftl : depenselistobj.data.depenses[thisid].ftlpart
                    });
                });

                var tplCtx = {
                    id : answerObj._id.$id ,
                    collection : "answers",
                    arrayForm : true,
                    form : "aapStep1",
                    setType : [
                        {
                            "path": "amount",
                            "type": "int"

                        },
                        {
                            "path": "date",
                            "type": "isoDate"
                        }
                    ]
                }

                $.each(lineToPay , function (index , value){
                    const chars = 'azertyuiopqsdfghjklmwxcvbn123456789';
                    var rand = (min = 0 , max = 1000) => Math.floor(Math.random() * (max - min) + min);
                    randChar = (length = 6) => {
                        var randchars = [];
                        for(let i = 0; i < length; i++){
                            randchars.push(chars[rand(0, chars.length)]);
                        }

                        return randchars.join('');
                    }
                    keygenerator =  (prefix = 'a-', sufix='') => `${prefix}${randChar()}${sufix}`;
                    var finkey = keygenerator();

                    tplCtx.path = "answers.aapStep1.depense."+value.id+".financer";

                    tplCtx.value = {
                        line: '',
                        amount: value.amount,
                        user: userId,
                        date: "now",
                        name: depenselistobj.data.currentOrga.name,
                        id: depenselistobj.data.currentOrga.id,
                        status: "pending",
                        finkey: finkey,
                        type: depenselistobj.data.financertype,
                        iscofinancementftl: depenselistobj.data.iscofinancementftl
                    }
                    if(value.amount > 0) {
                        dataHelper.path2Value(tplCtx, function (params) {
                            if(index + 1 === lineToPay.length ){
                                if(value.ftl <= 0) {
                                    showLoader('#question_financer');
                                    reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>" );
                                }

                                var post = {
                                    form: answerObj.form
                                };
                                url = baseUrl + '/co2/aap/funding/request/cart';
                                ajaxPost(null, url, post, function (resp) {
                                    depenselistobj.data.cart = resp.content;
                                    $.each(depenselistobj.data.cart.campagne, function(index,value){
                                        $('span.tlcart[data-finid="'+index+'"]').html(value+'<i class="fa fa-euro"></i>');
                                    });
                                });
                            }
                        });
                    }
                    tplCtx.value = {
                        line: '',
                        amount: value.ftl,
                        user: userId,
                        date: "now",
                        name: Object.values(answerObj.context)[0].name,
                        id: costum.contextId,
                        status: "pending",
                        finkey: finkey,
                        tlname: depenselistobj.data.currentOrga.name,
                        tlid: depenselistobj.data.currentOrga.id
                    }
                    if(value.ftl > 0) {
                        dataHelper.path2Value(tplCtx, function (params) {
                            if(index + 1 === lineToPay.length ){
                                showLoader('#question_financer');
                                reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>" );
                                var post = {
                                    form: answerObj.form
                                };
                                url = baseUrl + '/co2/aap/funding/request/cart';
                                ajaxPost(null, url, post, function (resp) {
                                    depenselistobj.data.cart = resp.content;
                                    $.each(depenselistobj.data.cart.campagne, function(index,value){
                                        $('span.tlcart[data-finid="'+index+'"]').html(value+'<i class="fa fa-euro"></i>');
                                    });
                                });
                            }
                        });
                    }
                    /*tplCtx.value = {
                        line: '',
                        amount: value.ftl,
                        user: userId,
                        date: "now",
                        name: Object.values(answerObj.context)[0].name,
                        id: costum.contextId,
                        status: "pending",
                        finkey: finkey,
                    }
                    if(value.ftl > 0) {
                        dataHelper.path2Value(tplCtx, function (params) {
                            if(index + 1 === lineToPay.length ){
                                showLoader('#question_financer');
                                reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>" );
                                var post = {
                                    form: answerObj.form
                                };
                                url = baseUrl + '/co2/aap/funding/request/cart';
                                ajaxPost(null, url, post, function (resp) {
                                    depenselistobj.data.cart = resp.content;
                                    $.each(depenselistobj.data.cart.campagne, function(index,value){
                                        $('span.tlcart[data-finid="'+index+'"]').html(value+'<i class="fa fa-euro"></i>');
                                    });
                                });
                            }
                        });
                    }*/
                });
                //urlCtrl.loadByHash(location.hash);

            });
            $('.newdepenselist.finance .statbutton-sellectall').off().on("click", function () {
                var thisbtn = $(this);
                if($('.newdepenselist.finance .newDepenseListCard.active').length == $('.newdepenselist.finance .newDepenseListCard').length){
                    $('.newdepenselist.finance .statbutton-sellectall').html('<span>Tout sélectionner</span> <i class="fa fa-check-square"></i>');
                    $('.newdepenselist.finance .newDepenseListCard.active').removeClass('active');
                    $('.newdepenselist.finance .depmodal').removeClass('active');
                    $('.newdepenselist.finance .depmodal i').removeClass('fa-times');
                    $('.newdepenselist.finance .newDepenseListCard .depmodal[data-content="finance"] i').addClass('fa-money');
                    $('.newdepenselist.finance .newDepenseListCard .depmodal[data-content="finance"] span').html('financer');
                    $('input-amount-multiselect-tree').val('');
                }else{
                    $('.newdepenselist.finance .statbutton-sellectall').html('<span>Tout désélectionner</span> <i class="fa fa-square"></i>');
                    $('.newdepenselist.finance .newDepenseListCard').addClass('active');
                    $('.newdepenselist.finance .depmodal').addClass('active');
                    $('.newdepenselist.finance .depmodal i').addClass('fa-times');
                    $('.newdepenselist.finance .newDepenseListCard .depmodal[data-content="finance"] i').removeClass('fa-money');
                    $('.newdepenselist.finance .newDepenseListCard .depmodal[data-content="finance"] span').html('enlever');
                }
                depenselistobj.action.updateselect();
            });
            $('.addDepense').off().on("click",function() {
                tplCtx = {
                    id : answerObj._id.$id ,
                    collection : "answers",
                    arrayForm : true,
                    form : "aapStep1",
                    setType : [
                        {
                            "path": "price",
                            "type": "int"
                        },
                        {
                            "path": "date",
                            "type": "isoDate"
                        }
                    ],

                }

                tplCtx.path = "answers.aapStep1.depense";
                dyFObj.openForm( sectionDyf.depenseDyf,null,null,null,null,{
                    type : "bootbox",
                    notCloseOpenModal : true,
                });
            });
            $('.doublebtn').off().on("click",function() {
                var thisbtn = $(this);
                var tplCtx = {
                    id : answerObj._id.$id ,
                    collection : "answers",
                    arrayForm : true,
                    form : "aapStep1",
                    setType : [
                        {
                            "path": "amount",
                            "type": "int"
                        },
                        {
                            "path": "date",
                            "type": "isoDate"
                        }
                    ],
                    path : "answers.aapStep1.depense."+thisbtn.data("uid")+".financer"
                }

                tplCtx.value = {
                    line   : '',
                    amount : thisbtn.data('amount'),
                    user   : userId,
                    date   : "now",
                    id : Object.keys(answerObj.context)[0],
                    name : "FTL",
                    finkey : thisbtn.data("finkey"),
                    tlname : thisbtn.data("tlname")
                }

                dataHelper.path2Value(tplCtx, function (params) {
                    showLoader('#question_financer');
                    reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>" );
                });
            });
            $('.supfinline').off().on("click",function() {

            });
            $('.tglv').off().on('click' , function (){
                var thisbtn = $(this);
                var isTabsActive = thisbtn.hasClass('active');

                if(!isTabsActive) {
                    $('.tglv').removeClass('active');
                    $('.tgledv').hide();
                    thisbtn.addClass('active');
                    $('.'+thisbtn.data('act')).show();
                }

                if(thisbtn.data('act') != 'contentInformation'){
                    $('.depmodal[data-content="finance"] i').removeClass('fa-times');
                    $('.depmodal[data-content="finance"]  i').addClass('fa-money');
                    $('.depmodal[data-content="finance"] span').html('financer');
                    $('.newDepenseListCard').removeClass('active');
                    $('.depmodal[data-content="finance"]').removeClass('active');
                    depenselistobj.action.updateselect();
                }

                if (typeof financementlogObj.action.actualisefinancerlog != "undefined"){
                    financementlogObj.action.actualisefinancerlog(financementlogObj);
                }
            });
            $('.changefinancertype').off().on('click' , function (){
                var thisbtn = $(this);
                var isTabsActive = thisbtn.hasClass('active');

                if(!isTabsActive) {
                    $('.activatedouble').hide();
                    $('.activatedoublelabel').hide();
                    $('.tldoubleamount').hide();
                    $('.itldoubleamount').hide();
                    $('.changefinancertype').removeClass('active');
                    $('.financementtype').hide();
                    $('.entete-ftlpart').hide();
                    thisbtn.addClass('active');
                    $('.financementtype[data-fintype="'+thisbtn.data('fintype')+'"]').show();
                    depenselistobj.data.financertype = thisbtn.data('fintype');
                    $('.contentFinanceAction div.mm-dropdown .textfirst').trigger('click');
                    if(thisbtn.data('fintype') == 'person' ){
                        depenselistobj.data.currentOrga.name = $('.input-name-multiselect').val();
                        depenselistobj.data.currentOrga.id = userId;
                        if($('.input-amount-multiselect-tree').val() != '' && $('.input-amount-multiselect-tree').val() != 0){
                            $('.statbutton-cofinance').prop('disabled', false);
                        }else{
                            $('.statbutton-cofinance').prop('disabled', 'disabled');
                        }
                    }else if((thisbtn.data('fintype') == 'organization' || thisbtn.data('fintype') == 'tl')
                        && depenselistobj.data.currentOrga.name != ''
                        && $('.input-amount-multiselect-tree').val() != ''
                        && $('.input-amount-multiselect-tree').val() != 0
                    ){
                        depenselistobj.data.currentOrga.name = userConnected.name;
                        depenselistobj.data.currentOrga.id = userId;
                    }else{
                        $('.statbutton-cofinance').prop('disabled', 'disabled');
                    }
                    if(thisbtn.data('fintype') == 'tl'){
                        $('.activatedouble').show();
                        $('.activatedoublelabel').show();
                        if($('.activatedouble:checked').length > 0){
                            $('.tldoubleamount').show();
                            $('.itldoubleamount').show();
                        }
                    }
                }
                depenselistobj.action.disturbAmount(depenselistobj);
            });

            $('.activatedouble').off().on('change', function () {
                if(this.checked){
                    $('.tldoubleamount').show();
                    $('.itldoubleamount').show();
                    depenselistobj.data.iscofinancementftl = true;
                    if($('.input-amount-multiselect-tree').val() != ""){
                        depenselistobj.action.disturbAmount(depenselistobj);
                    }
                    $('.newDepenseListCard.active .entete-ftlpart').show();
                }else{
                    $('.tldoubleamount').hide();
                    $('.itldoubleamount').hide();
                    $('.newDepenseListCard.active .entete-ftlpart').hide();
                    depenselistobj.data.iscofinancementftl = false;
                }
                depenselistobj.action.disturbAmount(depenselistobj);
            });
        });
</script>