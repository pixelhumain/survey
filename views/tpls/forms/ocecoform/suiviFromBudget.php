<style type="text/css">
    .suivierror {
        color: #e74c3c;
        font-size: 20px;
        background-color: #ecf0f1;
        text-align: center;
        padding: 20px;
    }
</style>
<?php
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;

$ignore = array('_file_', '_params_', '_obInitialLevel_' ,'ignore');
$params = array_diff_key(get_defined_vars() + array_flip($ignore), array_flip($ignore));

if (isset($parentForm["usecoremudepense"]) && $parentForm["config"]["usecoremudepense"] == true){
    if(isset($input["hide"]) && $input["hide"] == true){
        $conditionalHide = "hide";
    } else {
        $conditionalHide = "";
    }
    $tpl = "";
    if(in_array($input["type"], ["textarea","markdown","wysiwyg"])) {
        $tpl = "tpls.forms.textarea";
    } else if(empty($input["type"]) || in_array($input["type"], ["text","button","color","date","datetime-local","email","image","month","number","radio","range","tel","time","url","week","tags","hidden"])) {
        $tpl = "tpls.forms.text";
    } else {
        $tpl = $input["type"];
    }

    if( stripos( $input["type"] , "tpls.forms.cplx" ) !== false ) {
        $params["saveOneByOne"] = false;
    }else{
        $params["saveOneByOne"] = true;
    }
    $kunikT = explode( ".", @$input["type"]);
    $params["keyTpl"] = ( count($kunikT)>1 ) ? $kunikT[ count($kunikT)-1 ] : @$input["type"];
    $params["kunik"] = $params["keyTpl"]."depense";
    $params["formId"] = $step["step"];
    $params["form"] = $step;
    $params["key"] = "depense";
    $params["type"] = $input["type"];
    $params["label"] = @$input["label"];
    $params["info"] = @$input["info"];
    $params["inpClass"] = " ";
    $params["placeholder"] = @$input["placeholder"];
    $params["titleColor"] = (isset($this->costum["colors"]["pink"])) ? $this->costum["colors"]["pink"] : "#000";
    $params["answerPath"] = "answers.".$params["formId"].".".$params["key"].".";
    if(!empty($answer["answers"][$params["formId"]][$params["key"]])) {
        $params["answers"] = $answer["answers"][$params["formId"]][$params["key"]];
    }else{
        $params["answers"] = null;
    }
    $params["editQuestionBtn"] = "";
    $tpl = $input["type"];

    echo $this->renderPartial( "survey.views.tpls.forms.ocecoform.coremu.budget" , $params , true );
}else if (empty($answer["project"]["id"]) && $mode != "fa"){
    $depensestepmapping = "aapStep1";
    if(!empty($parentForm["params"]["mapping"]["depensestep"])){
        $depensestepmapping = $parentForm["params"]["mapping"]["depensestep"];
    }
    $depenseinputmapping = $inputId = "depense";
    if(!empty($parentForm["params"]["mapping"]["depenseinput"])){
        $depenseinputmapping = $parentForm["params"]["mapping"]["depenseinput"];
    }
    foreach ($steps as $stepvalue){
        if($stepvalue["id"] == $depensestepmapping){
            $step = $stepvalue;
            $input = $stepvalue['inputs'][$depenseinputmapping];
            break;
        }
    }
    if(isset($input["hide"]) && $input["hide"] == true){
        $conditionalHide = "hide";
    } else {
        $conditionalHide = "";
    }
    $tpl = "";
    if(in_array($input["type"], ["textarea","markdown","wysiwyg"])) {
        $tpl = "tpls.forms.textarea";
    } else if(empty($input["type"]) || in_array($input["type"], ["text","button","color","date","datetime-local","email","image","month","number","radio","range","tel","time","url","week","tags","hidden"])) {
        $tpl = "tpls.forms.text";
    } else {
        $tpl = $input["type"];
    }

    if( stripos( $input["type"] , "tpls.forms.cplx" ) !== false ) {
        $params["saveOneByOne"] = false;
    }else{
        $params["saveOneByOne"] = true;
    }
    $kunikT = explode( ".", @$input["type"]);
    $params["keyTpl"] = ( count($kunikT)>1 ) ? $kunikT[ count($kunikT)-1 ] : @$input["type"];
    $params["kunik"] = $params["keyTpl"].$inputId;
    $params["formId"] = $step["step"];
    $params["form"] = $step;
    $params["key"] = $inputId;
    $params["type"] = $input["type"];
    $params["label"] = @$input["label"];
    $params["info"] = @$input["info"];
    $params["inpClass"] = " ";
    $params["placeholder"] = @$input["placeholder"];
    $params["titleColor"] = (isset($this->costum["colors"]["pink"])) ? $this->costum["colors"]["pink"] : "#000";
    $params["answerPath"] = "answers.".$params["formId"].".".$params["key"].".";
    if(!empty($answer["answers"][$params["formId"]][$params["key"]])) {
        $params["answers"] = $answer["answers"][$params["formId"]][$params["key"]];
    }else{
        $params["answers"] = null;
    }
    $params["editQuestionBtn"] = "";
    $tpl = $input["type"];

    echo $this->renderPartial( "survey.views.".$tpl , $params , true );
}else{

$isAapProject = $isAap;
$contextId = $ctxtid = $context["_id"];
$contextType = $ctxttype = $parentForm["parent"][(string)$context["_id"]]["type"];

if ($answer)
{
    if (isset($parentForm["mapping"]["depense"]))
    {
        $mapping = $parentForm["mapping"]["depense"];
        $mappingExplode = explode(".", $mapping);

        if (isset($answer[$mappingExplode[0]][$mappingExplode[1]][$mappingExplode[2]]))
        {
            $answers = $answer[$mappingExplode[0]][$mappingExplode[1]][$mappingExplode[2]];
        }
    }
    elseif (isset($parentForm["type"]) && ($parentForm["type"] == "aapConfig" || $parentForm["type"] == "aap"))
    {
        $configEl = $configForm;

        if (isset($configEl["mapping"]["depense"]))
        {
            $mapping = @$configEl["mapping"]["depense"];
            $mappingExplode = explode(".", $mapping);

            if (isset($answer[$mappingExplode[0]][$mappingExplode[1]][$mappingExplode[2]]))
            {
                $answers = $answer[$mappingExplode[0]][$mappingExplode[1]][$mappingExplode[2]];
            }
        }
    }

    $editBtnL = "";

    $editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='" . $parentForm["_id"] . "' data-collection='" . Form::COLLECTION . "' data-path='params' class='previewTpl edit" . $kunik . "Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

    $paramsData = ["limitRoles" => [], "flimitRoles" => ["Financeur"]];

    $members = $communityCitoyenGroup;

    if (!empty($parentForm["params"][$kunik]["tpl"])) $paramsData["tpl"] = $parentForm["params"][$kunik]["tpl"];
    if (!empty($parentForm["params"][$kunik]["budgetCopy"])) $paramsData["budgetCopy"] = $parentForm["params"][$kunik]["budgetCopy"];
    if (isset($parentForm["params"]["MoLimitRoles"])) $paramsData["limitRoles"] = $parentForm["params"]["MoLimitRoles"];
    if (isset($parentForm["params"]["financeurLimitRoles"])) $paramsData["flimitRoles"] = $parentForm["params"]["financeurLimitRoles"];
    if (isset($parentForm["params"][$kunik]["useActions"])) $paramsData["useActions"] = $parentForm["params"][$kunik]["useActions"];

    $contextIdType = $parentForm["parent"];
    $communityLinks = $parentCommunity;
    $organizations = $communityOrgaGroup;
    $citoyens = $communityOrgaGroup;

    $orgs = [];
    foreach ($organizations as $id => $or)
    {
        $roles = null;
        if (isset($communityLinks[$id]["roles"])) $roles = $communityLinks[$id]["roles"];

        if (!empty($roles))
        {
            foreach ($roles as $i => $r)
            {
                if (sizeof($paramsData["limitRoles"]) != 0){
                    if (in_array($r, $paramsData["limitRoles"]))
                        $orgs[$id] = $or["name"];
                }else{
                    $orgs[$id] = $or["name"];
                }

            }
        }
    }

    foreach ($citoyens as $id => $or)
    {
        $roles = null;
        if (isset($communityLinks[$id]["roles"])) $roles = $communityLinks[$id]["roles"];

        if (!empty($roles))
        {
            foreach ($roles as $i => $r)
            {
                if (sizeof($paramsData["limitRoles"]) != 0){
                    if (in_array($r, $paramsData["limitRoles"]))
                        $orgs[$id] = $or["name"];
                }else{
                    $orgs[$id] = $or["name"];
                }
            }
        }
    }

    $orgsfi = [];
    foreach ($organizations as $id => $or)
    {
        $roles = null;
        if (isset($communityLinks[$id]["roles"])) $roles = $communityLinks[$id]["roles"];

        if ($paramsData["flimitRoles"] && !empty($roles))
        {
            foreach ($roles as $i => $r)
            {
                if (in_array($r, $paramsData["flimitRoles"]))
                    $orgsfi[$id] = $or["name"];
            }
        }
    }

    foreach ($answers as $keynf => $valuenf)
    {
        if (isset($valuenf["financer"]))
        {
            foreach ($valuenf["financer"] as $keyvv => $valuevv)
            {
                if (isset($valuevv["name"]) && !in_array($valuevv["name"], $orgsfi))
                {
                    $orgsfi[] = $valuevv["name"];
                }
            }
        }
    }

    $listLabels = array_merge($orgs);

    $tasks = [];

    foreach ($actions as $keyact => $valueact)
    {
        if (!empty($valueact["tasks"]))
        {
            $tasks[$keyact] = $valueact["tasks"];
        }
        else
        {
            $tasks[$keyact] = [];
        }
    }

    $payement = [];
    $contributorspay = [];
    $totalPrice = 0;

    if (!empty($answer["answers"]["aapStep1"]["depense"]))
    {
        foreach ($answer["answers"]["aapStep1"]["depense"] as $fId => $fData)
        {

            if (!empty($fData["poste"]) && !empty($fData["price"]))
            {
                $totalPrice += intval($fData["price"]);
                $payement[$fData["poste"]] = [];

                $payement[$fData["poste"]]["amount"] = $fData["price"];
                if (!empty($fData["payement"]))
                {
                    $sumFinline = 0;
                    foreach ($fData["payement"] as $fDid => $fDval)
                    {
                        if (!empty($fDval["amount"]) && is_numeric($fDval["amount"]))
                        {
                            $sumFinline += intval($fDval["amount"]);
                        }
                    }
                    $payement[$fData["poste"]]["payement"] = $sumFinline;
                    $payement[$fData["poste"]]["topay"] = intval($fData["price"]) - $sumFinline;
                }
                else
                {
                    $payement[$fData["poste"]]["payement"] = 0;
                    $payement[$fData["poste"]]["topay"] = intval($fData["price"]);
                }


                if (isset($fData["actionid"]) && isset($actions[$fData["actionid"]])){
                    if (isset($actions[$fData["actionid"]]["tasks"])){
                        foreach ($actions[$fData["actionid"]]["tasks"] as $atid => $atdata){
                            if (isset($atdata["contributors"])){
                                foreach ($atdata["contributors"] as $cid => $cdata){
                                    if (!isset($contributorspay[$cid]) && !isset($parentCommunity[$contributorspay[$cid]])){
                                        $contributorspay[$cid] = $parentCommunity[$contributorspay[$cid]];
                                    }
                                    if (!isset($payement[$fData["poste"]]["ctopay"][$cid])){
                                        $payement[$fData["poste"]]["ctopay"][$cid] = 0;
                                    }
                                    $payement[$fData["poste"]]["ctopay"][$cid] += (intval($atdata["credits"]) / count((array)$atdata["contributors"]));
                                }
                            }
                        }
                    } else {

                        if (isset($actions[$fData["actionid"]]["links"]["contributors"])) {
                            foreach ($actions[$fData["actionid"]]["links"]["contributors"] as $flcid => $flcdata){
                                if (isset($contributorspay[$flcid]) && isset($parentCommunity[$contributorspay[$flcid]])){
                                    $contributorspay[$flcid] = $parentCommunity[$contributorspay[$flcid]];
                                }
                                if (!isset($payement[$fData["poste"]]["ctopay"][$flcid])){
                                        $payement[$fData["poste"]]["ctopay"][$flcid] = 0;
                                    }
                                $payement[$fData["poste"]]["ctopay"][$flcid] += (intval($actions[$fData["actionid"]]["credits"]) / count((array)$actions[$fData["actionid"]]["links"]["contributors"]));
                            }
                        }

                    }

                }
            }
        }
    }


    $totalReste = 0;

    foreach ($payement as $tid => $tval)
    {
        if (!empty($tval["topay"]))
        {
            $totalReste += intval($tval["topay"]);
        }
    }

    $properties = ["poste" => ["inputType" => "text", "label" => "Contexte", "placeholder" => "Contexte", "rules" => ["required" => true]],
        //"worker" => ["placeholder" => "Maitre d'oeuvre", "inputType" => "select", "list" => "workerList", "subLabel" => "Si financeur public, l’inviter dans la liste ci-dessous (au cas où il n’apparait pas demandez à votre référent territoire de le déclarer comme partenaire financeur"],
        // "workType" => [
        //     "inputType" => "text",
        //     "label" => "Type de travaux",
        //     "placeholder" => "Type de travaux",
        //     "rules" => [ "required" => true ]
        // ],
        "todo" => ["inputType" => "text", "label" => "Tâches", "placeholder" => "Tâches", "rules" => ["required" => true, "number" => true]],
        //"total" => ["inputType" => "text", "label" => "Payement", "placeholder" => "Payement", "rules" => ["required" => true, "number" => true]],
    "validation" => ["inputType" => "text", "label" => "Validation", "placeholder" => "Validation", "rules" => ["number" => true]]];

    if (isset($rendermodal) && $rendermodal){
        if (!empty($parentForm["config"])) {
            $nameconfig = $parentConfig;
        } else {
            $nameconfig = "";
        }
    ?>
        <div id="sticky-anchor"></div>
            <div class="text-left col-md-12 col-sm-12 col-xs-12">
                <ul class="breadcrumb">
                    <li ><a><?php echo $nameconfig ?></a></li>
                    <li class="active"><a><?php echo $parentForm["name"] ?></a></li>
                    <li class="active"><a>Suivi du proposition : <?php echo @$answer["answers"]["aapStep1"]["titre"] ?> </a></li>
                </ul>
            </div>

        </div>
        <div class="col-xs-12 col-lg-10 col-lg-offset-1 project-detail">
        <?php
    }

    $ignore = array('_file_', '_params_', '_obInitialLevel_' ,'ignore');
    $params = array_diff_key(get_defined_vars() + array_flip($ignore), array_flip($ignore));

    echo $this->renderPartial("survey.views.tpls.forms.ocecoform.suiviFromBudgetTable", $params , true);

    ?>

    <div class="form-worker" style="display:none;">
        <select id="worker" style="width:100%;">
            <option>Choisir un maitre d'oeuvre</option>
            <?php
            foreach ($orgs as $v => $f)
            {
                echo "<option value='" . $v . "'>" . $f . "</option>";
            } ?>
        </select>
        <br>
        <span class="bold">Type de travaux effectués :</span> <br/>
        <input type="text" id="workType" name="workType" style="width:100%;">
        <br><br>
        <span class="bold">Organisme qui effectue les travaux , s'il n'existe pas, créez le et ajoutez le à la communauté ici
		<a class="btn btn-primary">Ajouter un maitre d'oeuvre</a>
	</span>
        <br>
    </div>

    <div class="" style="display:none;">
        <header>
            <h1>Choix du maitre d'oeuvre</h1>
        </header>
        <main>
            <div>
                <select id="worker" class="todoinput su-worker">
                    <option>Choisir un maitre d'oeuvre</option>
                    <?php
                    foreach ($orgs as $v => $f)
                    {
                        echo "<option value='" . $v . "'>" . $f . "</option>";
                    } ?>
                </select>

                <a class="btn btn-primary">Ajouter un maitre d'oeuvre</a>

                <input type="text" id="workType" placeholder="Type de travaux effectués :" name="workType" class="todoinput">
            </div>
        </main>

    </div>

    <div id="container" class="new-form-worker" style="display: none">
        <header>
            <h1>Choix du maitre d'oeuvre</h1>
        </header>
        <main class="mainc">

            <div class="">
                <!-- <label class=" spanfi-name">Nom</label><label class=" spanfi-mail">Email</label> -->
                <select id="worker" class="todoinput su-worker">
                    <option>Choisir un maitre d'oeuvre</option>
                    <?php
                    foreach ($orgs as $v => $f)
                    {
                        echo "<option value='" . $v . "'>" . $f . "</option>";
                    } ?>
                </select>
                <button class="btn btn-default su-btn">Ajouter</button>
                <div style="clear: both;"></div>

                <!--  <label class=" spanfi-fond">Fonds, enveloppe ou budget mobilisé</label><label class=" spanfi-montant">Montant Financé</label><label class=" spanfi-btn"></label> -->

            </div>

            <div class="">

                <input type="text" id="workType" placeholder="Type de travaux effectués :" name="workType" class="todoinput su-wtype">

                <div style="clear: both;"></div>
            </div>

            <div id="single-line"></div>

        </main>
    </div>

    <!-- <div class="form-progress" style="display:none;">
	<select id="progress" style="width:100%;">
		<option> DEGRÉ D'AVANCEMENT DE CE CHANTIER </option>
		<?php foreach ([25, 50, 75, 100] as $v => $f)
    {
        echo "<option value='" . $f . "'>" . $f . "%</option>";
    } ?>
	</select>
</div> -->

    <div class="new-form-pay" style="display:none;">
        <header>
            <h1> Statuts des réglements </h1>
        </header>
        <main>
            <div class="mainc">
                <select id="status" class="todoinput">
                    <option> État du paiement </option>
                    <?php foreach (["accompte" => "Accompte avancé", "partly" => "Facture payées", "total" => "Soldé"] as $v => $f)
                    {
                        echo "<option value='" . $v . "'>" . $f . "</option>";
                    } ?>
                </select>

                <input type="text" placeholder="Total des Montants payés" id="amount" name="amount" class="todoinput">
            </div>
        </main>

    </div>

    <div id="containerv" class="new-form-validate-work" style="display:none;">
        <header>
            <h1> Validation </h1>
        </header>
        <main>
            <div class="mainc">
                <select id="validWork" class="todoinput">
                    <option> Valider ces travaux </option>
                    <?php foreach (["validated" => "Validé sans réserve", "reserved" => "Validé avec réserves", "refused" => "Non validé"] as $v => $f)
                    {
                        echo "<option value='" . $v . "'>" . $f . "</option>";
                    } ?>
                </select>
            </div>
        </main>
    </div>

    <div id="container" class="containert newform-todo" style="display: none">
        <header>
            <h1>Sous-tâche</h1>
        </header>
        <main>
            <div id="add-todo">
                <input id="new-todo-value" class="todoinput taskname form-control" type="text" spellcheck="false" placeholder="Choses à faire" onfocus="this.placeholder=''" onblur="this.placeholder='Choses à faire'" />
                <input id="new-credit-value " class="credit todoinput form-control" type="number" spellcheck="false" placeholder="Cout de cette tâche" onfocus="this.placeholder=''" onblur="this.placeholder='Cout de cette tâche'" />
                <div style="clear: both;"></div>
            </div>
            <div id="add-tododesc">
                <input id="new-duedate-value " class="duedate todoinput form-control" type="date" spellcheck="false" placeholder="Date de fin estimé" onfocus="this.placeholder=''" onblur="this.placeholder='Date de fin estimé'" />
                <select id="new-worker-value" class="member todoinput form-control" multiple data-placeholder="Personne(s) qui réalisera cette tache">
                    <?php
                    foreach ($members as $id => $p)
                    {
                        $selected = ($id == Yii::app()->session["userId"]) ? "selected" : "";
                        echo "<option value='" . $p["username"] . "' data-id='" . $id . "' " . $selected . " >" . $p["name"] . "</option>";
                    }
                    ?>
                </select>

                <div style="clear: both;"></div>
            </div>
            <div id="single-line"></div>

            <ul id="todos-list">
            </ul>
        </main>
    </div>

    <div id="container" class="new-form-payfi" style="display: none">
        <header>
            <h1>Payement</h1>
        </header>
        <main>
            <div id="">
                <label class="label-esti" >Financeur</label>
            </div>
            <div id="add-tododesc">

                <select id="new-worker-value" class="payfi-financor  todoinput" data-placeholder="Financeur">
                    <?php
                    foreach ($orgsfi as $id => $p)
                    {

                        echo "<option value='" . $p . "' >" . $p . "</option>";
                    }
                    ?>
                </select>
                <div style="clear: both;"></div>
            </div>
            <div>
                <div id="">
                    <label class="label-esti" >Bénéficiaire</label>
                </div>
                <select id="new-worker-value" class="payfi-benef todoinput" data-placeholder="Personne(s) qui réalisera cette tache">
                    <?php
                    foreach ($members as $id => $p)
                    {
                        $selected = ($id == Yii::app()->session["userId"]) ? "selected" : "";
                        echo "<option value='" . $p["name"] . "' data-id='" . $id . "' " . $selected . " >" . $p["name"] . "</option>";
                    }
                    ?>
                </select>

                <div style="clear: both;"></div>
            </div>
            <div id="">
                <label class="label-esti" >Montant</label>
            </div>
            <div id="add-todo">
                <input id="new-todo-value" class="todoinput payfi-amount" type="text" spellcheck="false" placeholder="Montant" onfocus="this.placeholder=''" onblur="this.placeholder='Choses à faire'" />
                <div style="clear: both;"></div>
            </div>
            <div id="single-line"></div>

            <div class="table-responsive">
                <table class='table table-bordered table-hover  directoryTable oceco-styled-table'>
                    <tr>
                        <th class="thead-light">  </th>
                        <th class="thead-light"> Coût </th>
                        <th class="thead-light"> Reste à financer </th>
                        <th class="thead-light"> montant completé par la somme saisi </th>
                        <?php foreach ($payement as $fffid => $fffdata)
                            {
                        ?>
                            <th class="thead-light">

                            </th>
                        <?php
                            }
                         ?>
                    </tr>
                    <?php foreach ($payement as $fffid => $fffdata)
                    { ?>
                        <tr data-id="<?=$fffid
                        ?>">
                            <td> <?=$fffid ?></td>
                            <td> <?=$fffdata["amount"] ?></td>
                            <td> <?=$fffdata["topay"] ?></td>
                            <td> 0 </td>

                        </tr>
                        <?php
                    } ?>
                </table>
            </div>

            <div id="single-line"></div>


        </main>
    </div>

    <div id="container" class="new-form-payfiall" style="display: none">
        <header>
            <h1>Payement</h1>
        </header>
        <main>
            <div id="">
                <label class="label-esti" >Montant</label>
            </div>
            <div id="add-todo">
                <input id="new-todo-value" max="<?= $totalReste ?>" class="todoinput payfi-amount" type="number" spellcheck="false" placeholder="Montant" onfocus="this.placeholder=''" onblur="this.placeholder='Montant'" />
                <div style="clear: both;"></div>
            </div>
            <div id="single-line"></div>

            <div class="table-responsive">
                <label> Reste à payer total : <b> <?= $totalReste ?> </b></label>
                <table class='table table-bordered table-hover  directoryTable oceco-styled-table'>
                    <tr>
                        <th class="thead-light">  </th>
                        <th class="thead-light"> Coût </th>
                        <th class="thead-light"> Reste à payer </th>
                        <th class="thead-light"> montant completé par la somme saisi </th>
                        <?php foreach ($contributorspay as $fffid => $fffdata)
                            {
                        ?>
                            <th class="thead-light">
                                <?php if (isset($fffdata["name"])){
                                    echo "due à ".$fffdata["name"];
                                } ?>
                            </th>
                                <th class="thead-light">
                                    <?php if (isset($fffdata["name"])){
                                        echo "sera payé à ".$fffdata["name"];
                                    } ?>
                                </th>
                        <?php
                            }
                            ?>
                    </tr>
                    <?php foreach ($payement as $fffid => $fffdata)
                    { ?>
                        <tr data-id="<?=$fffid
                        ?>">
                            <td> <?=$fffid ?></td>
                            <td> <?=$fffdata["amount"] ?></td>
                            <td> <?=$fffdata["topay"] ?></td>
                            <td> 0 </td>
                            <?php foreach ($contributorspay as $fffid1 => $fffdata1)
                            {
                                ?>
                                <td class="thead-light">
                                    <?php if (isset($fffdata["ctopay"][$fffid1])){
                                        echo $fffdata["ctopay"][$fffid1];
                                    } else {
                                        echo 0;
                                    }

                                    ?>
                                </td>
                                <td class="thead-light">
                                    0
                                </td>
                                <?php
                            }
                            ?>
                        </tr>
                        <?php
                    } ?>
                   <!-- <tr>
                        <th>Total : </th>
                        <th><?/*= $totalPrice */?></th>
                        <th><?/*= $totalReste */?></th>
                        <th></th>
                        <?php /*foreach ($contributorspay as $fffid1 => $fffdata1)
                        {
                            */?>
                            <th class="thead-light">

                            </th>
                            <th class="thead-light">

                            </th>
                            <?php
/*                        }
                        */?>
                    </tr>-->
                </table>

            </div>

            <div id="single-line"></div>


        </main>
    </div>

            <?php
            if (isset($rendermodal) && $rendermodal){
            ?>

    <div id="" class="newform-estimate" style="display: none">
                <header>
                    <h1>Proposer</h1>
                </header>
                <main class="mainc">
                    <div class="ocecowebview">
                        <label class="label-esti">Proposition de prix</label>
                        <label class="label-esti">Durée</label>
                    </div>
                    <div class="">
                        <label class="label-mobi">Proposition de prix</label>
                        <input id="" class="todoinput form-control e-price" type="number" spellcheck="false" placeholder="Proposition de prix" onfocus="this.placeholder=''" onblur="this.placeholder='Fonds, enveloppe ou budget mobilisé'" />
                        <label class="label-mobi">Durée</label>
                        <input id="new-credit-value" class="e-days todoinput form-control" type="number" spellcheck="false" placeholder="Durée" onfocus="this.placeholder=''" onblur="this.placeholder='Durée'" />
                        <div style="clear: both;"></div>
                    </div>

                    <div id="single-line"></div>

                    <ul id="estimate-list">
                    </ul>
                </main>
            </div>


        </div>
    <?php
}
    ?>


    <?php
    if (isset($parentForm["params"]["financement"]["tpl"]))
    {
        // //if( $parentForm["params"]["financement"]["tpl"] == "tpls.forms.equibudget" )
        // 	$this->renderPartial( "costum.views.".$parentForm["params"]["financement"]["tpl"] ,
        // 	[ "totalFin"   => $total,
        // 	  "totalBudg" => Yii::app()->session["totalBudget"]["totalBudget"] ] );
        // else
        // 	$this->renderPartial( "costum.views.".$parentForm["params"]["financement"]["tpl"]);

    }

    ?>

    <script type="text/javascript">
        var today = new Date().toGMTString();

        var rendermodal = <?php echo (!empty($rendermodal) ? $rendermodal : "false") ?> ;

        var projectaction = <?php echo (!empty($project) ? json_encode($project) : "[]") ?> ;

        var commpay = <?php echo (!empty($contributorspay) ? json_encode($contributorspay) : "[]") ?> ;

        if (notEmpty(projectaction) && typeof projectaction.slug != "undefind"){
            var projectChanel = projectaction.slug;
        }

        if(typeof dyFObj.elementObjParams == "undefined")
            dyFObj.elementObjParams = {};

        dyFObj.elementObjParams.budgetInputList = <?php echo json_encode(Yii::app()->session["budgetInputList"]); ?>;
        dyFObj.elementObjParams.workerList = <?php echo json_encode($orgs); ?>;


        var <?php echo $kunik ?>Data = <?php echo json_encode((isset($answer["answers"][$mappingExplode[1]])) ? $answer["answers"][$mappingExplode[1]] : null); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;

        var member = <?php echo json_encode(isset($members) ? $members : []); ?>;

        var tasks =  <?php echo json_encode(isset($tasks) ? $tasks : []); ?>;

        var FiData = <?php echo json_encode((isset($answer["answers"]["aapStep1"]["depense"])) ? $answer["answers"]["aapStep1"]["depense"] : null); ?>;

        if (FiData != null) {
            if ($.isPlainObject(FiData)){
                FiData = Object.values(FiData);
            }
            var payfiList = FiData.reduce(function(accumulator, item){
                if ( item != null && typeof item["payement"] != "undefined") {
                    accumulator.push(item["payement"]);
                    return accumulator;
                } else {
                    accumulator.push([]);
                    return accumulator;
                }
            },[]);
        } else {
            var payfiList = {};
        }

        var cntxtId = "<?php echo $contextId; ?>";
        var cntxtType = "<?php echo $contextType; ?>";

        var costumInputs = {};

        costumInputs.elementparents = {
            afterSave : function(data, searchExists) {

                var sendDataM = {
                    parentId : cntxtId,
                    parentType : cntxtType,
                    listInvite : {
                        organizations : {}
                    }
                };
                sendDataM.listInvite.organizations[data.id] = {};
                sendDataM.listInvite.organizations[data.id]["name"] = data.map.name;
                sendDataM.listInvite.organizations[data.id]["roles"] = ["maitreOuvrage"];

                ajaxPost("",
                    baseUrl+"/co2/link/multiconnect",
                    sendDataM,
                    function(data) {
                        dyFObj.closeForm();
                        reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                    },
                    null,
                    "json"
                );
            }
        };

        var projectIdDepense = "<?php echo (!empty($answer["project"]["id"]) ? $answer["project"]["id"] : "null"); ?>";

        var payementobj = <?php echo json_encode((isset($payement)) ? $payement : null); ?>;

        payementobj = Object.keys(payementobj);

        function closePrioModal(){
            prioModal.modal('hide');
        }

        var parentId = "<?php echo (string)$answer["form"]; ?>";

        if(!notNull(answerObj)){
            var answerObj = <?php echo (!empty($answer) ? json_encode($answer) : "{}" ); ?>;
        }

        $(document).ready(function() {

            <?php
            if (isset($rendermodal) && $rendermodal){
            ?>

            $('.newbtnestimate').click(function() {
                tplCtx.pos = $(this).data("pos");
                tplCtx.collection = "answers";
                tplCtx.id = $(this).data("id");
                tplCtx.key = $(this).data("key");
                tplCtx.form = $(this).data("form");
                prioModal = bootbox.dialog({
                    message: $(".newform-estimate").html(),
                    show: false,
                    size: "large",
                    className: 'estimatedialog',
                    buttons: {
                        success: {
                            label: trad.save,
                            className: "btn-primary",
                            callback: function () {

                                tplCtx.path = "answers";
                                // if( notNull(formInputs [tplCtx.form]) )
                                tplCtx.path = "answers."+tplCtx.form;

                                tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+userId;

                                tplCtx.value = {
                                    price : $(".bootbox .e-price").val(),
                                    days : $(".bootbox .e-days").val(),
                                    name :  userConnected.name,
                                    date : today
                                };

                                tplCtx.setType = [{
                                    "path": "date",
                                    "type": "isoDate"
                                }];

                                mylog.log("btnEstimate save",tplCtx);
                                dataHelper.path2Value( tplCtx, function(){
                                    saveLinks(answerObj._id.$id,"estimated",userId);
                                    prioModal.modal('hide');
                                    reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");

                                } );
                            }
                        },
                        cancel: {
                            label: trad.cancel,
                            className: "btn-secondary",
                            callback: function(){
                                prioModal.modal('hide');
                            }
                        }
                    },
                    onEscape: function(){
                        prioModal.modal('hide');
                    }
                });

                var selectedErow = $(this).data("pos");

                prioModal.on('shown.bs.modal', function (e) {

                    $('.estimatedialog #add-button').data("pos", selectedErow );

                });

                prioModal.modal("show");
            });

            <?php } ?>

            $("#rthis").off().on("click",function() {
                reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
            });

            sectionDyf.<?php echo $kunik ?> = {
                "jsonSchema" : {
                    "title" : "Plan de Financement",
                    "icon" : "fa-money",
                    "text" : "Décrire ici les financements mobilisés ou à mobiliser. Les coûts doivent être en <b>hors taxe</b>.",
                    "properties" : <?php echo json_encode($properties); ?>,
                    save : function () {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                        });
                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                prioModal.modal('hide');
                                reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                            } );
                        }

                    }
                }
            };

            sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema" : {
                    "title" : "<?php echo $kunik ?> config",
                    "icon" : "fa-cog",
                    "properties" : {
                        // financerTypeList : {
                        //     inputType : "properties",
                        //     labelKey : "Clef",
                        //     labelValue : "Label affiché",
                        //     label : "Liste des type de Fiannceurs",
                        //     values :  sectionDyf.<?php echo $kunik ?>ParamsData.financerTypeList
                        // } ,
                        limitRoles : {
                            inputType : "array",
                            label : "Liste des roles maitre d'oeuvre",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsData.limitRoles
                        }
                        /*tpl : {
                            label : "Sub Template",
                            value :  sectionDyf.<?php echo $kunik ?>ParamsData.tpl
                        },
                        budgetCopy : {
                            label : "Input Bugdet",
                            inputType : "select",
                            options :  dyFObj.elementObjParams.budgetInputList
                        },
                        useActions : {
                            inputType : "checkboxSimple",
                            label : "Generer des actions de communecter ",
                            subLabel : "Permet d'utiliser le DDA et OCECO pour suivre et comptabiliser l'avancement",
                            params : {
                                onText : "Oui",
                                offText : "Non",
                                onLabel : "Oui",
                                offLabel : "Non",
                                labelText : "Generer des actions de communecter"
                            },
                            checked : sectionDyf.<?php echo $kunik ?>ParamsData.useActions
                        }*/
                    },
                    save : function () {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                            if(k == "limitRoles")
                                tplCtx.value["MoLimitRoles"] = getArray('.'+k+val.inputType);
                            else if(val.inputType == "array")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else if(val.inputType == "properties")
                                tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                            else
                                tplCtx.value[k] = $("#"+k).val();
                        });
                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                //prioModal.modal('hide');
                                dyFObj.closeForm();
                                reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                            } );
                        }

                    }
                }
            };

            mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

            $('.btnValidateWork').off().click(function() {
                tplCtx.pos = $(this).data("pos");
                tplCtx.budgetpath = $(this).data("budgetpath");
                tplCtx.collection = "answers";
                tplCtx.id = $(this).data("id");
                tplCtx.form = $(this).data("form");
                tplCtx.actionname = $(this).data("actionname");
                var myactionid = $(this).data("actionid");
                prioModal = bootbox.dialog({
                    message: $(".new-form-validate-work").html(),
                    show: false,
                    className: 'validatedialog',
                    buttons: {
                        success: {
                            label: trad.save,
                            className: "btn-primary",
                            callback: function () {

                                var formInputsHere = formInputs;
                                // if( notNull(formInputs [tplCtx.form]) )
                                // 	tplCtx.path = "answers."+tplCtx.form;

                                tplCtx.path = "answers."+tplCtx.form+"."+tplCtx.budgetpath+"."+tplCtx.pos+".validFinal";


                                tplCtx.value = {
                                    valid : $(".bootbox #validWork").val(),
                                    user : userId,
                                    date : today
                                };

                                tplCtx.setType = [{
                                    "path": "date",
                                    "type": "isoDate"
                                }];

                                delete tplCtx.pos;
                                delete tplCtx.budgetpath;
                                mylog.log("btnValidateWork save",tplCtx);
                                dataHelper.path2Value( tplCtx, function(){
                                    //saveLinks(answerObj._id.$id,"workValidated",userId,function(){});
                                    prioModal.modal('hide');

                                    var tplCtx2 = {};

                                    tplCtx2.id = myactionid;
                                    tplCtx2.collection = "actions";
                                    tplCtx2.path = "status"

                                    if (tplCtx.value.valid == "validated"){
                                        tplCtx2.value = "done";
                                        if (typeof rcObj != "undefined") {
                                            rcObj.postMsg({
                                                "channel": "#" + projectChanel,
                                                "text": userConnected.name + "a terminé l'action " + tplCtx.actionname
                                            }).then(function (data) {
                                            });
                                        }
                                    }else{
                                        tplCtx2.value = "todo";
                                    }


                                    dataHelper.path2Value( tplCtx2, function(){
                                        reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                    });

/*                                    rcObj.postMsg({ "channel": "#cobugs", "text": "This is a test!" }).then(function(data) {
                                        // console.log('Created msg', data);
                                        console.log(data.success);
                                    });*/

                                } );
                            }
                        },
                        cancel: {
                            label: trad.cancel,
                            className: "btn-secondary",
                            callback: closePrioModal
                        }
                    },
                    onEscape: closePrioModal
                });
                prioModal.modal("show");
            });

            $('.btnWorker').off().click(function() {
                tplCtx.pos = $(this).data("pos");
                tplCtx.budgetpath = $(this).data("budgetpath");
                tplCtx.collection = "answers";
                tplCtx.id = $(this).data("id");
                tplCtx.form = $(this).data("form");
                tplCtx.actionname = $(this).data("actionname");
                prioModal = bootbox.dialog({
                    message: $(".new-form-worker").html(),
                    className : "workerdialog",
                    show: false,
                    size: "large",
                    buttons: {
                        success: {
                            label: trad.save,
                            className: "btn-primary",
                            callback: function () {

                                var formInputsHere = formInputs;
                                tplCtx.path = "answers";
                                if( notNull(formInputs [tplCtx.form]) )
                                    tplCtx.path = "answers."+tplCtx.form;

                                tplCtx.path = "answers."+tplCtx.form+"."+tplCtx.budgetpath+"."+tplCtx.pos+".worker";



                                tplCtx.value = {
                                    id : $(".bootbox #worker").val(),
                                    name : $(".bootbox #worker option:selected").text(),
                                    workType : $(".bootbox #workType").val(),
                                    user : userId,
                                    date : today
                                };

                                tplCtx.setType = [{
                                    "path": "date",
                                    "type": "isoDate"
                                }];

                                delete tplCtx.pos;
                                delete tplCtx.budgetpath;

                                mylog.log("btnFinancer save",tplCtx);

                                dataHelper.path2Value( tplCtx, function() {
                                    //rcObj.postMsg({ "channel": "#"+projectChanel, "text": userConnected.name +"a ajouter le maitre d'oeuvre "+tplCtx.value.name+ "sur l'action "+ tplCtx.actionname }).then(function(data) {});
                                    //saveLinks(answerObj._id.$id,"worker", userId,null );
                                    prioModal.modal('hide');
                                    reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                } );
                            }
                        },
                        cancel: {
                            label: trad.cancel,
                            className: "btn-secondary",
                            callback: closePrioModal
                        }
                    },
                    onEscape: closePrioModal
                });
                prioModal.modal("show");

                prioModal.on('shown.bs.modal', function (e) {

                    $(".su-btn").off().on("click",function() {
                        prioModal.modal("hide");
                        dyFObj.openForm( "organization",null,null,null,costumInputs.elementparents );
                    });

                });
            });

            $('.btnPay').off().click(function() {
                tplCtx.pos = $(this).data("pos");
                tplCtx.budgetpath = $(this).data("budgetpath");
                tplCtx.collection = "answers";
                tplCtx.id = $(this).data("id");
                tplCtx.form = $(this).data("form");
                mylog.log("btnPay open",tplCtx);
                prioModal = bootbox.dialog({
                    message: $(".new-form-pay").html(),
                    className : "paydialog",
                    show: false,
                    buttons: {
                        success: {
                            label: trad.save,
                            className: "btn-primary",
                            callback: function () {

                                var formInputsHere = formInputs;
                                if( notNull(formInputs [tplCtx.form]) )
                                    tplCtx.path = "answers."+tplCtx.form;

                                tplCtx.path = "answers."+tplCtx.form+"."+tplCtx.budgetpath+"."+tplCtx.pos+".payed";



                                tplCtx.value = {
                                    status : $(".bootbox #status").val(),
                                    amount : $(".bootbox #amount").val(),
                                    user : userId,
                                    date : today
                                };

                                tplCtx.setType = [{
                                    "path": "date",
                                    "type": "isoDate"
                                }];

                                delete tplCtx.pos;
                                delete tplCtx.budgetpath;
                                mylog.log("btnPay save",tplCtx);
                                dataHelper.path2Value( tplCtx, function(){
                                    //saveLinks(answerObj._id.$id,"workPayed",userId,null);
                                    prioModal.modal('hide');
                                    reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                } );
                            }
                        },
                        cancel: {
                            label: trad.cancel,
                            className: "btn-secondary",
                            callback: closePrioModal
                        }
                    },
                    onEscape: closePrioModal
                });
                prioModal.modal("show");
            });

            $('.btnTodo').off().click(function() {
                var selectedAction = $(this).data("name");

                taplCtxtask = {};

                taplCtxtask.collection = "actions";
                taplCtxtask.id = $(this).data("actkey");
                taplCtxtask.path = "tasks"

                prioModal = bootbox.dialog({
                    message: $(".newform-todo").html(),
                    className: 'tododialog',
                    size: "large",
                    show: false,
                    buttons: {
                        success: {
                            label: trad.save,
                            className: "btn-primary",
                            callback: function () {

                                taplCtxtask.value = tasks[selectedAction];

                                dataHelper.path2Value( taplCtxtask, function(){
                                    //saveLinks(taplCtxtask.id ,"workTodo",userId,null);
                                    prioModal.modal('hide');
                                    reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                });

                            }
                        },
                        cancel: {
                            label: trad.cancel,
                            className: "btn-secondary",
                            callback: closePrioModal
                        }
                    },
                    onEscape: closePrioModal
                });

                prioModal.on('shown.bs.modal', function (e) {

                    $('.tododialog #add-button').data("name", selectedAction );

                    var select = $('.tododialog select[multiple]');
                    var options = select.find('option');

                    var div = $('<div />').addClass('selectMultiple todoinput member');
                    var active = $('<div />');
                    var list = $('<ul />');
                    var placeholder = select.data('placeholder');

                    var span = $('<span />').text(placeholder).appendTo(active);

                    options.each(function() {
                        var text = $(this).text();
                        if($(this).is(':selected')) {
                            active.append($('<a />').html('<em>' + text + '</em><i></i>'));
                            span.addClass('hide');
                        } else {
                            list.append($('<li />').html(text));
                        }
                    });

                    active.append($('<div />').addClass('arrow'));
                    div.append(active).append(list);

                    select.wrap(div);

                    $(document).on('click', '.selectMultiple ul li', function(e) {
                        var select = $(this).parent().parent();
                        var li = $(this);
                        if(!select.hasClass('clicked')) {
                            select.addClass('clicked');
                            li.prev().addClass('beforeRemove');
                            li.next().addClass('afterRemove');
                            li.addClass('remove');
                            var a = $('<a />').addClass('notShown').html('<em>' + li.text() + '</em><i></i>').hide().appendTo(select.children('div'));
                            a.slideDown(100, function() {
                                // setTimeout(function() {
                                a.addClass('shown');
                                select.children('div').children('span').addClass('hide');
                                select.find('option:contains(' + li.text() + ')').prop('selected', true);
                                // }, 150);
                            });
                            // setTimeout(function() {
                            if(li.prev().is(':last-child')) {
                                li.prev().removeClass('beforeRemove');
                            }
                            if(li.next().is(':first-child')) {
                                li.next().removeClass('afterRemove');
                            }
                            // setTimeout(function() {
                            li.prev().removeClass('beforeRemove');
                            li.next().removeClass('afterRemove');
                            // }, 100);

                            li.slideUp(400, function() {
                                li.remove();
                                select.removeClass('clicked');
                            });
                            // }, 150);
                        }
                    });

                    $(document).on('click', '.selectMultiple > div a', function(e) {
                        var select = $(this).parent().parent();
                        var self = $(this);
                        self.removeClass().addClass('remove');
                        select.addClass('open');
                        // setTimeout(function() {
                        self.addClass('disappear');
                        // setTimeout(function() {
                        var li = $('<li />').text(self.children('em').text()).addClass('notShown').appendTo(select.find('ul'));
                        li.addClass('show');

                        select.find('option:contains(' + self.children('em').text() + ')').prop('selected', false);
                        if(!select.find('option:selected').length) {
                            select.children('div').children('span').removeClass('hide');
                        }
                        li.removeClass();

                        li.slideDown("fast", function() {
                        });

                        self.remove();
                        self.animate({
                            width: 0,
                            height: 0,
                            padding: 0,
                            margin: 0
                        }, 300, function() {

                        })
                    });

                    $(document).on('click', '.selectMultiple > div .arrow, .selectMultiple > div span', function(e) {
                        e.stopImmediatePropagation();
                        $(this).parent().parent().toggleClass('open');
                    });

                });
                prioModal.modal("show");
            });

            $('.newbtnTodo').off().click(function(e) {
                e.stopPropagation();
                var selectedAction = $(this).data("actkey");
                var rcposs = $(this).data("pos") - 1;
                if(typeof tasks[selectedAction] != "undefined") {

                    taplCtxtask = {};

                    taplCtxtask.collection = "actions";
                    taplCtxtask.id = $(this).data("actkey");

                    taplCtxtask.path = "tasks";
                    tplCtx.actionname = $(this).data("actionname");

                    prioModal = bootbox.dialog({
                        message: $(".newform-todo").html(),
                        className: 'tododialog',
                        size: "large",
                        show: false,
                        buttons: {
                            success: {
                                label: trad.save,
                                className: "btn-primary",
                                callback: function () {

                                    var todo = {};

                                    if ($('.tododialog .taskname').val() && $('.tododialog .taskname').val() != "" && $('.tododialog .taskname').val() != undefined) {

                                        var worker = [];

                                        todo = {
                                            taskId: Math.random().toString(36).slice(2),
                                            createdAt: today,
                                            userId: userId,
                                            checked: false,
                                        };

                                        if ($('.tododialog .taskname').val())
                                            todo.task = $('.tododialog .taskname').val();

                                        if ($('.tododialog .credit').val())
                                            todo.credits = $('.tododialog .credit').val();

                                        if ($('.tododialog .duedate').val() && $('.tododialog .duedate').val() != "") {
                                            todo.endDate = $('.tododialog .duedate').val();
                                        }


                                        if ($('.tododialog #new-worker-value').val()) {

                                            todo.contributors = {};
                                            if ($('.tododialog #new-worker-value').val()) {

                                                worker = $('.tododialog #new-worker-value').val();

                                                $.each(worker, function (workerid, workerusername) {
                                                    $.each(member, function (memberid, membervalue) {
                                                        if (membervalue["username"] == workerusername) {
                                                            todo.contributors[memberid] = {
                                                                "type": membervalue.collection
                                                            }
                                                        }
                                                    })
                                                })
                                            }
                                        }
                                    }

                                    taplCtxtask.value = todo;

                                    taplCtxtask.path = taplCtxtask.path;
                                    taplCtxtask.arrayForm = true,
                                    taplCtxtask.edit = false,

                                    taplCtxtask.setType = [
                                        {
                                            "path": "createdAt",
                                            "type": "isoDate"
                                        },
                                        {
                                            "path": "endDate",
                                            "type": "isoDate"
                                        },
                                        {
                                            "path": "checked",
                                            "type": "boolean"
                                        },
                                    ];

                                    var rctask = $('.tododialog .taskname').val();

                                    dataHelper.path2Value(taplCtxtask, function () {
                                        ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/newtask/answerid/' + answerId,
                                            {
                                                task : rctask ,
                                                pos : rcposs,
                                                url : window.location.href
                                            },
                                            function (data) {

                                            }, "html");
                                        if (typeof rcObj != "undefined") {
                                            rcObj.postMsg({
                                                "channel": "#" + projectChanel,
                                                "text": userConnected.name + "a ajouté la sous-tâche : " + $('.tododialog .taskname') + "sur l'action " + tplCtx.actionname
                                            }).then(function (data) {
                                            });
                                        }

                                        //saveLinks(taplCtxtask.id ,"workTodo",userId,null);
                                        prioModal.modal('hide');
                                        reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                    });

                                }
                            },
                            cancel: {
                                label: trad.cancel,
                                className: "btn-secondary",
                                callback: closePrioModal
                            }
                        },
                        onEscape: closePrioModal
                    });

                    prioModal.modal("show");

                    prioModal.on('shown.bs.modal', function (e) {
                        var select = $('.tododialog select[multiple]');
                        var options = select.find('option');

                        var div = $('<div />').addClass('selectMultiple todoinput member');
                        var active = $('<div />');
                        var list = $('<ul />');
                        var placeholder = select.data('placeholder');

                        var span = $('<span />').text(placeholder).appendTo(active);

                        options.each(function () {
                            var text = $(this).text();
                            if ($(this).is(':selected')) {
                                active.append($('<a />').html('<em>' + text + '</em><i></i>'));
                                span.addClass('hide');
                            } else {
                                list.append($('<li />').html(text));
                            }
                        });

                        active.append($('<div />').addClass('arrow'));
                        div.append(active).append(list);

                        select.wrap(div);

                        $(document).on('click', '.selectMultiple ul li', function (e) {
                            var select = $(this).parent().parent();
                            var li = $(this);
                            if (!select.hasClass('clicked')) {
                                select.addClass('clicked');
                                li.prev().addClass('beforeRemove');
                                li.next().addClass('afterRemove');
                                li.addClass('remove');
                                var a = $('<a />').addClass('notShown').html('<em>' + li.text() + '</em><i></i>').hide().appendTo(select.children('div'));
                                a.slideDown(100, function () {
                                    // setTimeout(function() {
                                    a.addClass('shown');
                                    select.children('div').children('span').addClass('hide');
                                    select.find('option:contains(' + li.text() + ')').prop('selected', true);
                                    // }, 150);
                                });
                                // setTimeout(function() {
                                if (li.prev().is(':last-child')) {
                                    li.prev().removeClass('beforeRemove');
                                }
                                if (li.next().is(':first-child')) {
                                    li.next().removeClass('afterRemove');
                                }
                                // setTimeout(function() {
                                li.prev().removeClass('beforeRemove');
                                li.next().removeClass('afterRemove');
                                // }, 100);

                                li.slideUp(400, function () {
                                    li.remove();
                                    select.removeClass('clicked');
                                });
                                // }, 150);
                            }
                        });

                        $(document).on('click', '.selectMultiple > div a', function (e) {
                            var select = $(this).parent().parent();
                            var self = $(this);
                            self.removeClass().addClass('remove');
                            select.addClass('open');
                            // setTimeout(function() {
                            self.addClass('disappear');
                            // setTimeout(function() {
                            var li = $('<li />').text(self.children('em').text()).addClass('notShown').appendTo(select.find('ul'));
                            li.addClass('show');

                            select.find('option:contains(' + self.children('em').text() + ')').prop('selected', false);
                            if (!select.find('option:selected').length) {
                                select.children('div').children('span').removeClass('hide');
                            }
                            li.removeClass();

                            li.slideDown("fast", function () {
                            });

                            self.remove();
                            self.animate({
                                width: 0,
                                height: 0,
                                padding: 0,
                                margin: 0
                            }, 300, function () {

                            })
                        });

                        $(document).on('click', '.selectMultiple > div .arrow, .selectMultiple > div span', function (e) {
                            e.stopImmediatePropagation();
                            $(this).parent().parent().toggleClass('open');
                        });
                    });

                } else {
                    toastr.error('l\'action associé à cette ligne de dépense à été supprimé ou introuvable');
                }

            });

            $('.editbtnTodo').off().click(function() {
                var selectedAction = $(this).data("actkey");

                taplCtxtask = {};

                taplCtxtask.collection = "actions";
                taplCtxtask.id = $(this).data("actkey");
                taplCtxtask.path = "tasks";
                taplCtxtask.uid = $(this).data("uid");

                var sutaskname = tasks[selectedAction][taplCtxtask.uid].task;
                var sucredit = tasks[selectedAction][taplCtxtask.uid].credits;
                var suchecked = tasks[selectedAction][taplCtxtask.uid].checked;
                var suenddate = tasks[selectedAction][taplCtxtask.uid].endDate;
                var sucont = tasks[selectedAction][taplCtxtask.uid].contributors;


                prioModal = bootbox.dialog({
                    message: $(".newform-todo").html(),
                    className: 'tododialog',
                    size: "large",
                    show: false,
                    buttons: {
                        success: {
                            label: trad.save,
                            className: "btn-primary",
                            callback: function () {

                                var todo = {};

                                if ( $('.tododialog .taskname').val() &&  $('.tododialog .taskname').val() != "" && $('.tododialog .taskname').val() != undefined) {


                                    var worker = [];

                                    todo = tasks[selectedAction][taplCtxtask.uid];

                                    if ( $('.tododialog .taskname').val() )
                                        todo.task = $('.tododialog .taskname').val();

                                    if ( $('.tododialog .credit').val() )
                                        todo.credits = $('.tododialog .credit').val();

                                    if ( $('.tododialog .duedate').val() ){
                                        todo.endDate = $('.tododialog .duedate').val();
                                    }

                                    if ( $('.tododialog .duedate').val() ){
                                        var formattedDatetodo =  $('.tododialog .duedate').val().split("-");
                                        var ytodo = formattedDatetodo[0];
                                        var mtodo  =  formattedDatetodo[1];
                                        var dtodo  = formattedDatetodo[2];
                                        todo.endDate = dtodo +'/'+mtodo +'/'+ytodo;
                                    }

                                    if ( $('.tododialog #new-worker-value').val() ){

                                        todo.contributors = {};
                                        if ($('.tododialog #new-worker-value').val() ){

                                            worker = $('.tododialog #new-worker-value').val();
                                            $.each(worker, function(workerid, workerusername){
                                                $.each(member, function(memberid, membervalue){
                                                    if (membervalue["username"] == workerusername) {
                                                        todo.contributors[memberid] = {
                                                            "type" : membervalue.collection
                                                        }
                                                    }
                                                })
                                            })
                                        }
                                    }
                                }

                                tasks[selectedAction][taplCtxtask.uid] = todo;

                                taplCtxtask.value = tasks[selectedAction];

                                dataHelper.path2Value( taplCtxtask, function(){
                                    //saveLinks(taplCtxtask.id ,"workTodo",userId,null);
                                    prioModal.modal('hide');
                                    reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                });

                            }
                        },
                        cancel: {
                            label: trad.cancel,
                            className: "btn-secondary",
                            callback: closePrioModal
                        }
                    },
                    onEscape: closePrioModal
                });

                prioModal.modal("show");


                prioModal.on('shown.bs.modal', function (e) {

                    var select = $('.tododialog select[multiple]');
                    var options = select.find('option');

                    var div = $('<div />').addClass('selectMultiple todoinput member');
                    var active = $('<div />');
                    var list = $('<ul />');
                    var placeholder = select.data('placeholder');

                    var span = $('<span />').text(placeholder).appendTo(active);

                    options.each(function() {
                        var text = $(this).text();
                        if($(this).is(':selected')) {
                            active.append($('<a />').html('<em>' + text + '</em><i></i>'));
                            span.addClass('hide');
                        } else {
                            list.append($('<li />').html(text));
                        }
                    });

                    active.append($('<div />').addClass('arrow'));
                    div.append(active).append(list);

                    select.wrap(div);

                    $(document).on('click', '.selectMultiple ul li', function(e) {
                        var select = $(this).parent().parent();
                        var li = $(this);
                        if(!select.hasClass('clicked')) {
                            select.addClass('clicked');
                            li.prev().addClass('beforeRemove');
                            li.next().addClass('afterRemove');
                            li.addClass('remove');
                            var a = $('<a />').addClass('notShown').html('<em>' + li.text() + '</em><i></i>').hide().appendTo(select.children('div'));
                            a.slideDown(100, function() {
                                // setTimeout(function() {
                                a.addClass('shown');
                                select.children('div').children('span').addClass('hide');
                                select.find('option:contains(' + li.text() + ')').prop('selected', true);
                                // }, 150);
                            });
                            // setTimeout(function() {
                            if(li.prev().is(':last-child')) {
                                li.prev().removeClass('beforeRemove');
                            }
                            if(li.next().is(':first-child')) {
                                li.next().removeClass('afterRemove');
                            }
                            // setTimeout(function() {
                            li.prev().removeClass('beforeRemove');
                            li.next().removeClass('afterRemove');
                            // }, 100);

                            li.slideUp(400, function() {
                                li.remove();
                                select.removeClass('clicked');
                            });
                            // }, 150);
                        }
                    });

                    $(document).on('click', '.selectMultiple > div a', function(e) {
                        var select = $(this).parent().parent();
                        var self = $(this);
                        self.removeClass().addClass('remove');
                        select.addClass('open');
                        // setTimeout(function() {
                        self.addClass('disappear');
                        // setTimeout(function() {
                        var li = $('<li />').text(self.children('em').text()).addClass('notShown').appendTo(select.find('ul'));
                        li.addClass('show');

                        select.find('option:contains(' + self.children('em').text() + ')').prop('selected', false);
                        if(!select.find('option:selected').length) {
                            select.children('div').children('span').removeClass('hide');
                        }
                        li.removeClass();

                        li.slideDown("fast", function() {
                        });

                        self.remove();
                        self.animate({
                            width: 0,
                            height: 0,
                            padding: 0,
                            margin: 0
                        }, 300, function() {

                        })
                    });

                    $(document).on('click', '.selectMultiple > div .arrow, .selectMultiple > div span', function(e) {
                        e.stopImmediatePropagation();
                        $(this).parent().parent().toggleClass('open');
                    });

                    if ( typeof sutaskname !== "undefined" )
                        $('.tododialog .taskname').val(sutaskname);

                    if ( typeof sucredit !== "undefined" ){
                        $('.tododialog .credit').val(sucredit);
                    }else {
                        $('.tododialog .credit').val("");
                    }

                    if ( typeof suenddate !== "undefined" ){
                        var formattedDatetodo =  suenddate.split("/");
                        var dtodo = formattedDatetodo[0];
                        var mtodo  =  formattedDatetodo[1];
                        var ytodo  = formattedDatetodo[2];
                        $('.tododialog .duedate').val(ytodo +'-'+mtodo +'-'+dtodo);
                    }else{
                        $('.tododialog .duedate').val("");
                    }


                    $('.selectMultiple > div a').each(function(){
                        $(this).trigger('click');
                    });

                    if ( typeof sucont !== "undefined" ){
                        var worker = [];

                        $.each(sucont, function(cbId, cbValue){
                            if (typeof member[cbId] !== "undefined") {
                                $('.selectMultiple ul li:contains('+member[cbId].name+')').trigger("click");
                            }
                        })

                        // $('.tododialog #new-worker-value').val(worker);
                    }else{
                        $('.tododialog #new-worker-value').val([]);
                    }


                });

            });

            $('.deletebtnTodo').off().click(function() {
                var selectedAction = $(this).data("actkey");

                tplCtx = {};

                tplCtx.collection = "actions";
                tplCtx.id = $(this).data("actkey");
                tplCtx.path = "tasks";
                tplCtx.uid = $(this).data("uid");

                prioModal = bootbox.dialog({
                    title: trad.confirmdelete,
                    show: false,
                    message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                    buttons: [
                        {
                            label: "Ok",
                            className: "btn btn-primary pull-left",
                            callback: function() {
                                var task_pos = tasks[selectedAction];

                                // delete fi_pos[tplCtx.uid];
                                task_pos.splice(parseInt(tplCtx.uid), 1);

                                tplCtx.path = "tasks";

                                tplCtx.value = task_pos;

                                mylog.log("btnEstimate save",tplCtx);
                                dataHelper.path2Value( tplCtx, function(){
                                    //saveLinks(answerObj._id.$id,"estimated",userId);
                                    prioModal.modal('hide');
                                    reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");

                                } );
                            }
                        },
                        {
                            label: "Annuler",
                            className: "btn btn-default pull-left",
                            callback: function() {}
                        }
                    ]
                });

                prioModal.modal("show");
            });

            $('.tablecheckbox').on('change', function(e){
                var tablecheckiconpos = $(this).data("pos");
                var tablecheckiconcollection = "actions";
                var tablecheckiconid = $(this).data("id");

                tplCtx.pos = $(this).data("pos");
                tplCtx.collection = "actions";
                tplCtx.id = $(this).data("id");

                tplCtx.path = "tasks."+tplCtx.pos+".checked";
                tplCtx.value = $(this).is(":checked");
                tplCtx.actionname = $(this).data("actionname");
                tplCtx.taskname = $(this).data("taskname");
                tplCtx.setType = "boolean";

                var rctask = tplCtx.taskname;
                var rcpos = tplCtx.actionname;
                if (tplCtx.value == true) {
                    dataHelper.path2Value( tplCtx, function() {
                        var answerId = "<?php echo (string)$answer["_id"]; ?>";

                        ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/checktask/answerid/' + answerId,
                            {
                                actname :rcpos,
                                task : rctask,
                                url : window.location.href
                            },
                            function (data) {

                            }, "html");
                    } );

                    tplCtx.pos = tablecheckiconpos;
                    tplCtx.collection = tablecheckiconcollection;
                    tplCtx.id = tablecheckiconid;

                    tplCtx.path = "tasks."+tplCtx.pos+".checkedUserId";
                    tplCtx.value = userId;

                    dataHelper.path2Value( tplCtx, function() {

                    } );

                    tplCtx.pos = tablecheckiconpos;
                    tplCtx.collection = tablecheckiconcollection;
                    tplCtx.id = tablecheckiconid;


                    tplCtx.path = "tasks."+tplCtx.pos+".checkedAt";
                    tplCtx.value = today;
                    tplCtx.setType = "isoDate";

                    dataHelper.path2Value( tplCtx, function() {

                        reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                    } );
                } else {
                    dataHelper.path2Value( tplCtx, function() {

                        reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                    } );
                }

            });

            /*$('.PayementLine').off().click(function() {

                tplCtx.pos = $(this).data("pos");

                var rcpos =$(this).data("pos");

                var bfibudgetpath = $(this).data("budgetpath"),
                    bficollection = "answers" ,
                    bfiid = $(this).data("id") ,
                    bfiform = $(this).data("form");

                prioModal = bootbox.dialog({
                    message: $(".new-form-payfiall").html(),
                    className: 'payfidialog',
                    show: false,
                    size: "large",
                    buttons: {
                        success: {
                            label: trad.save,
                            className: "btn-primary",
                            callback: function () {

                                var fpos = 0;
                                $.each(payementobj, function (ind, val) {
                                    if (typeof val.valctpay != "undefined") {
                                        var pfos = payfiList[fpos].length;

                                        $.each( val.valctpay, function (ind2, val2) {
                                            if (parseInt(val2) > 0) {

                                                tplCtx.setType = [
                                                    {
                                                        "path": "amount",
                                                        "type": "int"
                                                    },
                                                    {
                                                        "path": "date",
                                                        "type": "isoDate"
                                                    }
                                                ];

                                                tplCtx.budgetpath = bfibudgetpath;
                                                tplCtx.collection = bficollection;
                                                tplCtx.id = bfiid;
                                                tplCtx.form = bfiform;

                                                tplCtx.path = "answers.aapStep1";

                                                tplCtx.path = tplCtx.path + "." + tplCtx.budgetpath + "." + fpos + ".payement." + pfos;



                                                tplCtx.value = {
                                                    beneficiary: {
                                                        id : ind2,
                                                        type : "citoyens"
                                                    },
                                                    amount: val2,
                                                    user: userId,
                                                    date: today
                                                };

                                                tplCtx.setType = [
                                                    {
                                                        "path": "date",
                                                        "type": "isoString"
                                                    }
                                                ];

                                                if (typeof commpay[ind2] != "undefined" && typeof commpay[ind2].name != "undefined"){
                                                    tplCtx.value.beneficiary.name = commpay[ind2].name;
                                                }

                                                delete tplCtx.budgetpath;
                                                mylog.log("btnFinancer save", tplCtx);

                                                $.ajax({
                                                    type : 'POST',
                                                    data : {pos : pfos,
                                                        url : window.location.href},
                                                    url : baseUrl + '/survey/answer/rcnotification/action/newpayement/answerid/' + answerId,
                                                    dataType : "json",
                                                    async : false,

                                                    success : function(data){
                                                    }
                                                });

                                                dataHelper.path2Value(tplCtx, function (params) {
                                                    var answerId = "<?php echo (string)$answer["_id"]; ?>";

                                                    ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/newpayement/answerid/' + answerId,
                                                        {
                                                            pos : rcpos,
                                                            url : window.location.href
                                                        },
                                                        function (data) {

                                                        }, "html");

                                                    prioModal.modal('hide');
                                                    //saveLinks(answerObj._id.$id,"financerAdded",userId,function(){
                                                    prioModal.modal('hide');
                                                    //reloadInput("<?php //echo $key

                                                    ?>//", "<?php //echo @$form["id"]

                                                    ?>//");
                                                    // });

                                                });

                                                delete tplCtx.setType;
                                            }
                                            pfos++;
                                        });
                                    }

                                    fpos++;
                                });

                                if (rendermodal) {
                                    reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                } else {
                                    if (typeof inputsList != "undefined"){
                                                $.each(inputsList , function (inn, vall) {
                                                    $.each(vall , function (inn2, vall2) {
                                                        if (typeof vall2.type != "undefined"
                                                            && vall2.type.split('.')[vall2.type.split('.').length-1] != "undefined"
                                                            && ( vall2.type.split('.')[vall2.type.split('.').length-1] == "budget" ||
                                                                vall2.type.split('.')[vall2.type.split('.').length-1] == "financementFromBudget" ||
                                                                vall2.type.split('.')[vall2.type.split('.').length-1] == "suiviFromBudget"
                                                            )
                                                        ){
                                                            reloadInput(inn2, inn);
                                                        }
                                                    });
                                                });
                                        }
                                }

                            }
                        },
                        cancel: {
                            label: trad.cancel,
                            className: "btn-secondary",
                            callback: function() {
                            }
                        }
                    },
                    onEscape: function() {
                        prioModal.modal("hide");
                    }
                });
                prioModal.modal("show");

                prioModal.on('shown.bs.modal', function (e) {


                    $(".payfidialog .payfi-amount").unbind().keyup( function( event ) {
                        if(parseInt($(".payfidialog .payfi-amount").val()) > 0) {

                            if (parseInt($(".payfidialog .payfi-amount").val()) > parseInt($(".payfidialog .payfi-amount").attr("max"))){
                                $(".payfidialog .payfi-amount").val($(".payfidialog .payfi-amount").attr("max"));
                            }

                            var reste = parseInt($(".payfidialog .payfi-amount").val());
                            var signlefipos = 0;
                            $.each(payementobj, function (index, val) {
                                if (val.topay > 0 && signlefipos == tplCtx.pos){
                                    if (val.topay < reste){

                                        var totalpay = 0;
                                        payementobj[index]["val"] = val.topay;
                                        if (typeof payementobj[index]["ctopay"] != "undefined"){
                                            var countc = 6;
                                            payementobj[index]["valctpay"] = {};
                                            $.each(payementobj[index]["ctopay"], function (ctid, ctval) {
                                                if (ctval < reste){
                                                    payementobj[index]["valctpay"][ctid] = ctval;
                                                    $(".payfidialog tr[data-id='" + index + "'] td:nth-child(" + countc + ")").html(ctval);
                                                    reste -= ctval;
                                                    totalpay += ctval;
                                                } else {
                                                    $(".payfidialog tr[data-id='" + index + "'] td:nth-child(" + countc + ")").html(reste);
                                                    payementobj[index]["valctpay"][ctid] = reste;
                                                    totalpay += reste;
                                                    reste = 0;
                                                }
                                                countc = countc +2;
                                            });
                                        }
                                        $(".payfidialog tr[data-id='" + index + "'] td:nth-child(4)").html(totalpay);
                                        //reste -= val.topay;

                                    } else {

                                        payementobj[index]["val"] = reste;
                                        var totalpay = 0;
                                        if (typeof payementobj[index]["ctopay"] != "undefined"){
                                            var countc = 6;
                                            payementobj[index]["valctpay"] = {};
                                            $.each(payementobj[index]["ctopay"], function (ctid, ctval) {
                                                if (ctval < reste){
                                                    payementobj[index]["valctpay"][ctid] = ctval;
                                                    $(".payfidialog tr[data-id='" + index + "'] td:nth-child(" + countc + ")").html(ctval);
                                                    reste -= ctval;
                                                    totalpay += ctval;
                                                } else {
                                                    $(".payfidialog tr[data-id='" + index + "'] td:nth-child(" + countc + ")").html(reste);
                                                    payementobj[index]["valctpay"][ctid] = reste;
                                                    totalpay += reste;
                                                    reste = 0;

                                                }
                                                countc = countc +2;
                                            });
                                        }
                                        $(".payfidialog tr[data-id='" + index + "'] td:nth-child(4)").html(totalpay);
                                        //reste = 0;
                                    }
                                }
                                signlefipos++;
                            });
                        }
                    });

                    var signlefipos2 = 0;
                    $.each(payementobj, function (index, val) {
                        if (signlefipos2 != tplCtx.pos){
                            $(".payfidialog tr[data-id='" + index + "']").addClass("hide");
                        }else{
                            $(".payfidialog .payfi-amount").attr("max", parseInt($(".payfidialog tr[data-id='" + index + "'] td:nth-child(3)").html()));
                        }
                        signlefipos2++;
                    });


                });

            });

            $('.PayementAllLine').off().click(function() {
                var bfibudgetpath = $(this).data("budgetpath"),
                    bficollection = "answers" ,
                    bfiid = $(this).data("id") ,
                    bfiform = $(this).data("form");

                prioModal = bootbox.dialog({
                    message: $(".new-form-payfiall").html(),
                    className: 'payfidialog',
                    show: false,
                    size: "large",
                    buttons: {
                        success: {
                            label: trad.save,
                            className: "btn-primary",
                            callback: function () {

                                var fpos = 0;
                                $.each(payementobj, function (ind, val) {
                                    if (typeof val.valctpay != "undefined") {
                                        var pfos = payfiList[fpos].length;

                                        $.each( val.valctpay, function (ind2, val2) {
                                            if (parseInt(val2) > 0) {

                                                tplCtx.setType = [
                                                    {
                                                        "path": "amount",
                                                        "type": "int"
                                                    },
                                                    {
                                                        "path": "date",
                                                        "type": "isoDate"
                                                    }
                                                ];

                                                tplCtx.budgetpath = bfibudgetpath;
                                                tplCtx.collection = bficollection;
                                                tplCtx.id = bfiid;
                                                tplCtx.form = bfiform;

                                                tplCtx.path = "answers.aapStep1";

                                                tplCtx.path = tplCtx.path + "." + tplCtx.budgetpath + "." + fpos + ".payement." + pfos;


                                                tplCtx.value = {
                                                    beneficiary: {
                                                        id : ind2,
                                                        type : "citoyens"
                                                    },
                                                    amount: val2,
                                                    user: userId,
                                                    date: today
                                                };

                                                tplCtx.setType = [{
                                                    "path": "date",
                                                    "type": "isoDate"
                                                }];

                                                if (typeof commpay[ind2] != "undefined" && typeof commpay[ind2].name != "undefined"){
                                                    tplCtx.value.beneficiary.name = commpay[ind2].name;
                                                }

                                                delete tplCtx.budgetpath;
                                                mylog.log("btnFinancer save", tplCtx);
                                                dataHelper.path2Value(tplCtx, function (params) {
                                                    var answerId = "<?php echo (string)$answer["_id"]; ?>";

                                                    ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/newpayement/answerid/' + answerId,
                                                        {url : window.location.href},
                                                        function (data) {

                                                        }, "html");

                                                    prioModal.modal('hide');
                                                    //saveLinks(answerObj._id.$id,"financerAdded",userId,function(){
                                                    prioModal.modal('hide');
                                                    //reloadInput("<?php //echo $key

                                                    ?>//", "<?php //echo @$form["id"]

                                                    ?>//");
                                                    // });

                                                });

                                                delete tplCtx.setType;
                                            }
                                            pfos++;
                                        });
                                    }

                                    fpos++;
                                });

                                if (rendermodal) {
                                    reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                } else {
                                    if (typeof inputsList != "undefined"){
                                                $.each(inputsList , function (inn, vall) {
                                                    $.each(vall , function (inn2, vall2) {
                                                        if (typeof vall2.type != "undefined"
                                                            && vall2.type.split('.')[vall2.type.split('.').length-1] != "undefined"
                                                            && ( vall2.type.split('.')[vall2.type.split('.').length-1] == "budget" ||
                                                                vall2.type.split('.')[vall2.type.split('.').length-1] == "financementFromBudget" ||
                                                                vall2.type.split('.')[vall2.type.split('.').length-1] == "suiviFromBudget"
                                                            )
                                                        ){
                                                            reloadInput(inn2, inn);
                                                        }
                                                    });
                                                });
                                        }
                                }

                            }
                        },
                        cancel: {
                            label: trad.cancel,
                            className: "btn-secondary",
                            callback: function() {
                            }
                        }
                    },
                    onEscape: function() {
                        prioModal.modal("hide");
                    }
                });
                prioModal.modal("show");

                prioModal.on('shown.bs.modal', function (e) {


                    $(".payfidialog .payfi-amount").unbind().keyup( function( event ) {
                        if(parseInt($(".payfidialog .payfi-amount").val()) > 0) {


                            if (parseInt($(".payfidialog .payfi-amount").val()) > parseInt($("payfidialog .payfi-amount").attr("max"))){
                                $("payfidialog .payfi-amount").val($("payfidialog .payfi-amount").attr("max"));
                            }

                            var reste = parseInt($(".payfidialog .payfi-amount").val());
                            $.each(payementobj, function (index, val) {
                                if (val.topay > 0){
                                    if (val.topay < reste){

                                        var totalpay = 0;
                                        payementobj[index]["val"] = val.topay;
                                        if (typeof payementobj[index]["ctopay"] != "undefined"){
                                            var countc = 6;
                                            payementobj[index]["valctpay"] = {};
                                            $.each(payementobj[index]["ctopay"], function (ctid, ctval) {
                                                if (ctval < reste){
                                                    payementobj[index]["valctpay"][ctid] = ctval;
                                                    $(".payfidialog tr[data-id='" + index + "'] td:nth-child(" + countc + ")").html(ctval);
                                                    reste -= ctval;
                                                    totalpay += ctval;
                                                } else {
                                                    $(".payfidialog tr[data-id='" + index + "'] td:nth-child(" + countc + ")").html(reste);
                                                    payementobj[index]["valctpay"][ctid] = reste;
                                                    totalpay += reste;
                                                    reste = 0;
                                                }
                                                countc = countc +2;
                                            });
                                        }
                                        $(".payfidialog tr[data-id='" + index + "'] td:nth-child(4)").html(totalpay);
                                        //reste -= val.topay;

                                    } else {

                                        payementobj[index]["val"] = reste;
                                        var totalpay = 0;
                                        if (typeof payementobj[index]["ctopay"] != "undefined"){
                                            var countc = 6;
                                            payementobj[index]["valctpay"] = {};
                                            $.each(payementobj[index]["ctopay"], function (ctid, ctval) {
                                                if (ctval < reste){
                                                    payementobj[index]["valctpay"][ctid] = ctval;
                                                    $(".payfidialog tr[data-id='" + index + "'] td:nth-child(" + countc + ")").html(ctval);
                                                    reste -= ctval;
                                                    totalpay += ctval;
                                                } else {
                                                    $(".payfidialog tr[data-id='" + index + "'] td:nth-child(" + countc + ")").html(reste);
                                                    payementobj[index]["valctpay"][ctid] = reste;
                                                    totalpay += reste;
                                                    reste = 0;

                                                }
                                                countc = countc +2;
                                            });
                                        }
                                        $(".payfidialog tr[data-id='" + index + "'] td:nth-child(4)").html(totalpay);
                                        //reste = 0;
                                    }
                                }
                            });
                        }
                    });


                });

            });*/

            $('.PayementLine').off().click(function() {
                tplCtx.pos = $(this).data("pos");
                var rercpos = $(this).data("pos");
                tplCtx.budgetpath = $(this).data("budgetpath");
                tplCtx.collection = "answers";
                tplCtx.id = $(this).data("id");
                tplCtx.form = $(this).data("form");
                tplCtx.arrayForm =  true;
                prioModal = bootbox.dialog({
                    message: $(".new-form-payfi").html(),
                    title: "Ajouter un payement sur une ligne",
                    className: 'payementdialog',
                    show: false,
                    size: "large",
                    buttons: {
                        success: {
                            label: trad.save,
                            className: "btn-primary save-payement",
                            callback: function () {

                                //var formInputsHere = formInputs;
                                // var financersCount = ( typeof eval("answerObj.answers."+tplCtx.budgetpath+"["+tplCtx.pos+"].financer") != "undefined" ) ? eval("answerObj.answers."+tplCtx.budgetpath+"["+tplCtx.pos+"].financer").length : 0;

                                tplCtx.setType = [
                                    {
                                        "path": "amount",
                                        "type": "int"
                                    },
                                    {
                                        "path": "date",
                                        "type": "isoDate"
                                    }
                                ];

                                tplCtx.path = "answers.aapStep1";
                                tplCtx.arrayForm = true;

                                //tplCtx.path = tplCtx.path+"."+tplCtx.budgetpath+"."+tplCtx.pos+".financer."+financerList[tplCtx.pos].length;
                                tplCtx.path = tplCtx.path+"."+tplCtx.budgetpath+"."+tplCtx.pos+".financer";
                                // if( notNull(formInputs [tplCtx.form]) )
                                // 	tplCtx.path = "answers."+tplCtx.budgetpath+"."+tplCtx.pos+".financer."+financersCount;

                                var today = new Date();
                                today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();
                                tplCtx.value = {
                                    line   : $(".financerdialog .fi-fond").val(),
                                    amount : $(".financerdialog .fi-montant").val(),
                                    user   : userId,
                                    date   : today
                                };

                                delete tplCtx.pos;
                                delete tplCtx.budgetpath;
                                mylog.log("btnpayement save",tplCtx);
                                dataHelper.path2Value( tplCtx, function(params) {
                                    delete tplCtx.arrayForm;
                                    var answerId = "<?php echo (string)$answer["_id"]; ?>";

                                    ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/newpayement/answerid/' + answerId,
                                        {
                                            pos : rercpos,
                                            url : window.location.href
                                        },
                                        function (data) {

                                        }, "html");

                                    prioModal.modal('hide');
                                    //saveLinks(answerObj._id.$id,"financerAdded",userId,function(){
                                    prioModal.modal('hide');
                                    //reloadInput("<?php //echo $key

                                    ?>//", "<?php //echo @$form["id"]

                                    ?>//");
                                    // });
                                    if(typeof newReloadStepValidationInputGlobal != "undefined") {
                                        newReloadStepValidationInputGlobal({
                                            inputKey : <?php echo json_encode($key) ?>,
                                            inputType : "tpls.forms.ocecoform.financementFromBudget"
                                        })
                                    }
                                    if (rendermodal){
                                        reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                    }else{
                                        if (typeof inputsList != "undefined"){
                                            $.each(inputsList , function (inn, vall) {
                                                $.each(vall , function (inn2, vall2) {
                                                    if (typeof vall2.type != "undefined"
                                                        && vall2.type.split('.')[vall2.type.split('.').length-1] != "undefined"
                                                        && ( vall2.type.split('.')[vall2.type.split('.').length-1] == "budget" ||
                                                            vall2.type.split('.')[vall2.type.split('.').length-1] == "financementFromBudget" ||
                                                            vall2.type.split('.')[vall2.type.split('.').length-1] == "suiviFromBudget"
                                                        )
                                                    ){
                                                        reloadInput(inn2, inn);
                                                    }
                                                });
                                            });
                                        }
                                    }
                                } );

                                delete tplCtx.setType;
                            }
                        },
                        cancel: {
                            label: trad.cancel,
                            className: "btn-secondary",
                            callback: function() {
                            }
                        }
                    },
                    onEscape: function() {
                        prioModal.modal("hide");
                    }
                });
                prioModal.modal("show");

                prioModal.on('shown.bs.modal', function (e) {
                    mylog.log("financementobj modal")
                    $('.save-payement').attr('disabled','disabled');
                    var financors = {};
                });

            });

            $('.PayementAllLine').off().click(function() {
                var bfibudgetpath = $(this).data("budgetpath"),
                    bficollection = "answers" ,
                    bfiid = $(this).data("id") ,
                    bfiform = $(this).data("form");

                prioModal = bootbox.dialog({
                    message: $(".new-form-financeallline").html(),
                    title: "Ajouter un Financeur sur plusieurs lignes",
                    className: 'financerdialog',
                    show: false,
                    size: "large",
                    buttons: {
                        success: {
                            label: trad.save,
                            className: "btn-primary save-finance",
                            callback: function () {
                                var fpos = 0;
                                var answerObj = <?php echo json_encode($answer) ?>;
                                var fposarray = Object.keys(answerObj.answers.aapStep1.depense) ;
                                $.each(financementobj, function (ind, val) {
                                    if (parseInt(val.val) > 0) {

                                        tplCtx.setType = [
                                            {
                                                "path": "amount",
                                                "type": "int"
                                            },
                                            {
                                                "path": "date",
                                                "type": "isoDate"
                                            }
                                        ];

                                        tplCtx.budgetpath = bfibudgetpath;
                                        tplCtx.collection = bficollection;
                                        tplCtx.id = bfiid;
                                        tplCtx.form = bfiform;
                                        tplCtx.arrayForm = true;
                                        tplCtx.path = "answers.aapStep1";

                                        if (!Array.isArray(financerList[fpos]) && typeof financerList[fpos] != "undefined"){
                                            financerList[fpos] = Object.values(financerList[fpos]);
                                        }
                                        //tplCtx.path = tplCtx.path + "." + tplCtx.budgetpath + "." + fposarray[fpos] + ".financer." + financerList[fpos].length;
                                        tplCtx.path = tplCtx.path + "." + tplCtx.budgetpath + "." + fposarray[fpos] + ".financer";

                                        var today = new Date();
                                        today = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
                                        tplCtx.value = {
                                            line: $(".financerdialog .fi-fond").val(),
                                            amount: val.val,
                                            user: userId,
                                            date: today
                                        };

                                        if ($(".financerdialog input.fi-financer").val() != 0) {
                                            tplCtx.value.id = $(".financerdialog input.fi-financer").val();
                                            tplCtx.value.name = $('.financerdialog .whatFinancers').select2('data').text.trim();
                                        } else if ($(".financerdialog .fi-name").val() != "") {
                                            tplCtx.value.name = $(".financerdialog .fi-name").val();
                                            tplCtx.value.email = $(".financerdialog .fi-mail").val();
                                        }

                                        delete tplCtx.budgetpath;
                                        mylog.log("btnFinancer save", tplCtx);
                                        $.ajax({
                                            type : 'POST',
                                            data : {pos : fpos,
                                                url : window.location.href},
                                            url : baseUrl + '/survey/answer/rcnotification/action/newfinancement/answerid/' + answerId,
                                            dataType : "json",
                                            async : false,

                                            success : function(data){}
                                        });
                                        dataHelper.path2Value(tplCtx, function (params) {
                                            delete tplCtx.arrayForm;
                                            var answerId = "<?php echo (string)$answer["_id"]; ?>";

                                            prioModal.modal('hide');
                                            //saveLinks(answerObj._id.$id,"financerAdded",userId,function(){
                                            prioModal.modal('hide');
                                            if(typeof newReloadStepValidationInputGlobal != "undefined") {
                                                newReloadStepValidationInputGlobal({
                                                    inputKey : <?php echo json_encode($key) ?>,
                                                    inputType : "tpls.forms.ocecoform.financementFromBudget"
                                                })
                                            }
                                            //reloadInput("<?php //echo $key

                                            ?>//", "<?php //echo @$form["id"]

                                            ?>//");
                                            // });

                                        });

                                        delete tplCtx.setType;
                                    }
                                    fpos++;
                                });

                                if (rendermodal) {
                                    reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                } else {
                                    if (typeof inputsList != "undefined"){
                                        $.each(inputsList , function (inn, vall) {
                                            $.each(vall , function (inn2, vall2) {
                                                if (typeof vall2.type != "undefined"
                                                    && vall2.type.split('.')[vall2.type.split('.').length-1] != "undefined"
                                                    && ( vall2.type.split('.')[vall2.type.split('.').length-1] == "budget" ||
                                                        vall2.type.split('.')[vall2.type.split('.').length-1] == "financementFromBudget" ||
                                                        vall2.type.split('.')[vall2.type.split('.').length-1] == "suiviFromBudget"
                                                    )
                                                ){
                                                    reloadInput(inn2, inn);
                                                }
                                            });
                                        });
                                    }
                                }
                            }
                        },
                        cancel: {
                            label: trad.cancel,
                            className: "btn-secondary",
                            callback: function() {
                            }
                        }
                    },
                    onEscape: function() {
                        prioModal.modal("hide");
                    }
                });
                prioModal.modal("show");

                var thisbtn = $(this);

                prioModal.on('shown.bs.modal', function (e) {

                    $('.save-finance').attr('disabled','disabled');
                    $('.fi-montant').on('blur', function(){
                        if($(this).val() != ""){
                            $('.save-finance').removeAttr('disabled');
                        }
                    });
                    let restToFinance = 0;
                    $.each(financementobj, function (index, val) {
                        if (typeof val.tofin != "undefined" && val.tofin >= 0)
                            restToFinance += val.tofin;
                        $(".fi-montant.todoinput").val(restToFinance)
                        $(".fi-montant.todoinput").trigger("blur")
                    });
                    var setInputMontant = setInterval(() => {
                        if($(".fi-montant.todoinput").is(":visible")) {
                            $(".fi-montant.todoinput").trigger("keyup")
                            clearInterval(setInputMontant);
                            setInputMontant = null
                        }
                    }, 350);
                    var financors = {};
                    ajaxPost('',baseUrl+'/co2/aap/getfinancor/id/'+formId+'/getAllItems/true',
                        null,
                        function(data){
                            financors = data;

                        },
                        null,
                        null,
                        {async : false});

                    var unlimitedFinancing = <?= isset($parentForm["params"]["unlimitedFinancing"]) ?  "true" : "false" ?>;
                    fincmntObj.commonModalEvent(financors);
                    $(".financerdialog .fi-montant.todoinput").keyup( function( event ) {
                        if($(".financerdialog .fi-montant.todoinput").val() > 0) {

                            if (parseInt($(".financerdialog .fi-montant.todoinput").val()) > parseInt($(".financerdialog .fi-montant.todoinput").attr("max")) ) {
                                $(".financerdialog .fi-montant.todoinput").val($(".financerdialog .fi-montant.todoinput").attr("max") );
                            }

                            if (parseInt($(".financerdialog .fi-montant.todoinput").val()) > parseInt($(".financerdialog .fi-resteEnv").data("value"))){
                                //$(".financerdialog .fi-montant.todoinput").val($(".financerdialog .fi-resteEnv").data("value"));
                                $(".restEnvWarning").html("<i class='fa fa-info-circle'></i> Le montant à financer depasse la valeur restant dans l'enveloppe du financeur")
                            }else{
                                $(".restEnvWarning").html("");
                            }

                            var reste = parseInt($(".financerdialog .fi-montant.todoinput").val());
                            $.each(financementobj, function (index, val) {
                                if (typeof val.tofin)
                                    if (val.tofin > 0){
                                        if (val.tofin < reste){
                                            financementobj[index]["val"] = val.tofin;
                                            $(`.financerdialog tr[data-id="${ index }"] td:nth-child(4)`).html(val.tofin);
                                            reste -= val.tofin;
                                        } else {
                                            financementobj[index]["val"] = reste;
                                            $(`.financerdialog tr[data-id="${ index }"] td:nth-child(4)`).html(reste);
                                            reste = 0;
                                        }
                                    }
                            });
                        }
                    });

                });

            });

            $('.editbtnpayfi').off().click(function() {
                tplCtx.pos = $(this).data("pos");
                tplCtx.budgetpath = $(this).data("key");
                tplCtx.collection = "answers";
                tplCtx.id = $(this).data("id");
                tplCtx.form = $(this).data("form");
                tplCtx.uid = $(this).data("uid");

                var payfifinanceur = $(this).data("payfifinanceur");
                var payfibeneficiary =$(this).data("payfibeneficiary");
                var payfiamount = $(this).data("payfiamount");

                prioModal = bootbox.dialog({
                    message: $(".new-form-payfi").html(),
                    className: 'payfidialog',
                    show: false,
                    size: "large",
                    buttons: {
                        success: {
                            label: trad.save,
                            className: "btn-primary",
                            callback: function () {

                                tplCtx.path = "answers."+tplCtx.form;

                                tplCtx.path = tplCtx.path+"."+tplCtx.budgetpath+"."+tplCtx.pos+".payement."+tplCtx.uid;




                                tplCtx.value = {
                                    financeur : $(".payfidialog .payfi-financor").val(),
                                    beneficiary : $(".payfidialog .payfi-benef").val(),
                                    amount : $(".payfidialog .payfi-amount").val(),
                                    user   : userId,
                                    date   : today
                                };

                                tplCtx.setType = [{
                                    "path": "date",
                                    "type": "isoDate"
                                }];

                                delete tplCtx.pos;
                                delete tplCtx.budgetpath;

                                dataHelper.path2Value( tplCtx, function(params) {
                                    prioModal.modal('hide');
                                    //saveLinks(answerObj._id.$id,"payementAdded",userId,function(){prioModal.modal('hide');
                                    reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                    //});

                                } );
                            }
                        },
                        cancel: {
                            label: trad.cancel,
                            className: "btn-secondary",
                            callback: function() {
                            }
                        }
                    },
                    onEscape: function() {
                        prioModal.modal("hide");
                    }
                });
                prioModal.modal("show");

                prioModal.on('shown.bs.modal', function (e) {


                    var select = $('.payfidialog .payfi-financor');
                    var options = select.find('option');

                    var div = $('<div />').addClass('selectMultiple todoinput pfinancor');
                    var active = $('<div />');
                    var list = $('<ul />');
                    var placeholder = select.data('placeholder');

                    var span = $('<span />').text(placeholder).appendTo(active);

                    options.each(function() {
                        var text = $(this).text();
                        if($(this).is(':selected')) {
                            active.append($('<a />').html('<em>' + text + '</em><i></i>'));
                            span.addClass('hide');
                        } else {
                            list.append($('<li />').html(text));
                        }
                    });

                    active.append($('<div />').addClass('arrow'));
                    div.append(active).append(list);

                    select.wrap(div);

                    var select2 = $('.payfidialog .payfi-benef');
                    var options2 = select2.find('option');

                    var div2 = $('<div />').addClass('selectMultiple todoinput pbenef');
                    var active2 = $('<div />');
                    var list2 = $('<ul />');
                    var placeholder2 = select2.data('placeholder');

                    var span2 = $('<span />').text(placeholder2).appendTo(active2);

                    options2.each(function() {
                        var text2 = $(this).text();
                        if($(this).is(':selected')) {
                            active2.append($('<a />').html('<em>' + text2 + '</em><i></i>'));
                            span2.addClass('hide');
                        } else {
                            list2.append($('<li />').html(text2));
                        }
                    });

                    active2.append($('<div />').addClass('arrow'));
                    div2.append(active2).append(list2);

                    select2.wrap(div2);

                    $(document).on('click', '.selectMultiple ul li', function(e) {
                        var select = $(this).parent().parent();
                        var li = $(this);
                        if(!select.hasClass('clicked')) {
                            select.addClass('clicked');
                            li.prev().addClass('beforeRemove');
                            li.next().addClass('afterRemove');
                            li.addClass('remove');
                            var a = $('<a />').addClass('notShown').html('<em>' + li.text() + '</em><i></i>').hide().appendTo(select.children('div'));
                            a.slideDown(100, function() {
                                // setTimeout(function() {
                                a.addClass('shown');
                                select.children('div').children('span').addClass('hide');
                                select.find('option:contains(' + li.text() + ')').prop('selected', true);
                                // }, 150);
                            });
                            // setTimeout(function() {
                            if(li.prev().is(':last-child')) {
                                li.prev().removeClass('beforeRemove');
                            }
                            if(li.next().is(':first-child')) {
                                li.next().removeClass('afterRemove');
                            }
                            // setTimeout(function() {
                            li.prev().removeClass('beforeRemove');
                            li.next().removeClass('afterRemove');
                            // }, 100);

                            li.slideUp(400, function() {
                                li.remove();
                                select.removeClass('clicked');
                            });
                            // }, 150);
                        }
                    });

                    $(document).on('click', '.selectMultiple > div a', function(e) {
                        var select = $(this).parent().parent();
                        var self = $(this);
                        self.removeClass().addClass('remove');
                        select.addClass('open');
                        // setTimeout(function() {
                        self.addClass('disappear');
                        // setTimeout(function() {
                        var li = $('<li />').text(self.children('em').text()).addClass('notShown').appendTo(select.find('ul'));
                        li.addClass('show');

                        select.find('option:contains(' + self.children('em').text() + ')').prop('selected', false);
                        if(!select.find('option:selected').length) {
                            select.children('div').children('span').removeClass('hide');
                        }
                        li.removeClass();

                        li.slideDown("fast", function() {
                        });

                        self.remove();
                        self.animate({
                            width: 0,
                            height: 0,
                            padding: 0,
                            margin: 0
                        }, 300, function() {

                        })
                    });

                    $(document).on('click', '.selectMultiple > div .arrow, .selectMultiple > div span', function(e) {
                        e.stopImmediatePropagation();
                        $(this).parent().parent().toggleClass('open');
                    });

                    $('.selectMultiple > div a').each(function(){
                        $(this).trigger('click');
                    });

                    if ( typeof payfibeneficiary !== "undefined" ){
                        var payfibeneficiaryarr = [];

                        $.each(payfibeneficiary, function(cbId, cbValue){
                            $('.selectMultiple.pbenef ul li:contains('+cbValue+')').trigger("click");
                        })

                        // $('.tododialog #new-worker-value').val(worker);
                    }

                    if ( typeof payfifinanceur !== "undefined" ){
                        var payfifinanceurarr = [];

                        $.each(payfifinanceur, function(cbId, cbValue){
                            $('.selectMultiple.pfinancor ul li:contains('+cbValue+')').trigger("click");

                        })

                        // $('.tododialog #new-worker-value').val(worker);
                    }

                    if ( typeof payfiamount !== "undefined" )
                        $('.payfidialog .payfi-amount').val(payfiamount);

                });

            });

            $('.deletebtnpayfi').off().click(function() {
                tplCtx.pos = $(this).data("pos");
                tplCtx.collection = "answers";
                tplCtx.id = $(this).data("id");
                tplCtx.key = $(this).data("key");
                tplCtx.form = $(this).data("form");
                tplCtx.uid = $(this).data("uid");

                prioModal = bootbox.dialog({
                    title: trad.confirmdelete,
                    show: false,
                    message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                    buttons: [
                        {
                            label: "Ok",
                            className: "btn btn-primary pull-left",
                            callback: function() {
                                var payfi_pos = FiData[tplCtx.pos]["payement"];

                                // delete fi_pos[tplCtx.uid];
                                payfi_pos.splice(parseInt(tplCtx.uid), 1);

                                tplCtx.path = "answers";
                                // if( notNull(formInputs [tplCtx.form]) )
                                tplCtx.path = "answers."+tplCtx.form;

                                tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".payement";

                                tplCtx.value = payfi_pos;

                                mylog.log("btnEstimate save",tplCtx);
                                dataHelper.path2Value( tplCtx, function(){
                                    //saveLinks(answerObj._id.$id,"estimated",userId);
                                    prioModal.modal('hide');
                                    reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");

                                } );
                            }
                        },
                        {
                            label: "Annuler",
                            className: "btn btn-default pull-left",
                            callback: function() {}
                        }
                    ]
                });

                prioModal.modal("show");
            });

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            });

            $('.participeaction').off().click(function() {

                var actionidf = $(this).data("id");
                var thisbtn = $(this);
                /*if ($(this).data("action") == "participate"){
                    tplCtx.value = null;
                } else {
                    tplCtx.value = {
                        type: "citoyens"
                    }
                }*!/*/

                if (thisbtn.data("action") == "participate"){
                    var sendDataM = {
                        parent : {},
                        childId : userId,
                        childType : "citoyens",
                        childName : userConnected.name,
                        childEmail : userConnected.email,
                        connectType : "contributors"
                    };
                    sendDataM.parent[actionidf] = {
                        type : "actions"
                    }

                    ajaxPost("",
                        baseUrl+"/co2/link/linkchildparent",
                        sendDataM,
                        function(data) {
                            reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                        }
                    );
                } else {
                    var sendDataM = {
                        parentType : "actions",
                        childId : userId,
                        childType : "citoyens",
                        childName : userConnected.name,
                        childEmail : userConnected.email,
                        connectType : "contributors"
                    }

                    sendDataM.parentId = actionidf;

                    ajaxPost("",
                        baseUrl+"/co2/link/disconnect",
                        sendDataM,
                        function(data) {
                            reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                        }
                    );
                }

                //dataHelper.path2Value( tplCtx, function(params) {
                //saveLinks(answerObj._id.$id,"payementAdded",userId,function(){prioModal.modal('hide');
                //reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                //});
                //} );

            });

            $('.finishaction').off().click(function() {

                var thisbtn = $(this);

                tplCtx.collection = "answers";
                tplCtx.id = thisbtn.data("id");
                tplCtx.path = "answers.aapStep1.depense."+thisbtn.data("pos")+".validFinal";

                tplCtx.value = {
                    valid : thisbtn.data("ansaction"),
                    user : userId,
                    date : today
                };
                tplCtx.setType = [{
                    "path": "date",
                    "type": "isoDate"
                }];
                dataHelper.path2Value( tplCtx, function(){
                    //reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                });

                tplCtx.collection = "actions";
                tplCtx.id = thisbtn.data("actionid");
                tplCtx.path = "status";

                tplCtx.value = thisbtn.data("action");

                dataHelper.path2Value( tplCtx, function(){
                    reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                });


            });
 });

    </script>
    <?php
}
} ?>
