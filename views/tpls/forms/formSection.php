
<style type="text/css">
.questionList{list-style-type: none;}
.ui-state-highlight { height: 1.5em; line-height: 1.2em; }
</style>
<?php
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;
$timezone = !empty(Yii::app()->session["timezone"]) ? Yii::app()->session["timezone"] : "UTC";
date_default_timezone_set($timezone);
$startDate = !empty($parentForm["startDate"]) ? $parentForm["startDate"]->toDateTime()->format('c') : null;
$endDate = !empty($parentForm["endDate"]) ? $parentForm["endDate"]->toDateTime()->format('c') : null;
$now = date('d-m-Y H:i', time());
$checkDate = Api::checkDateStatus($now,$startDate,$endDate);

if(
     (
        ( !isset($parentForm["session"]) || $parentForm["session"] == false ) &&
        ($checkDate == "include" || $canAdminAnswer )
     ) ||
     (
        (isset($parentForm["session"]) && $parentForm["session"] == true && isset($parentForm["sessionMapping"]) && isset($parentForm["actualSession"])) &&
        (Api::path_get_value($answer, $parentForm["sessionMapping"] , '.') == null || Api::path_get_value($answer, $parentForm["sessionMapping"] , '.') == $parentForm["actualSession"] || $canAdminAnswer) &&
        ($checkDate == "include" || $canAdminAnswer )
     )
)
{
	if( isset(Yii::app()->session["userId"]))
	{
	        //[X] ouvrir sur une answer /answer/xxx
	        //[X] revenir sur un answer pour modification
	        //[X] ouvrir la premiere fois et demander à participer
	        //[X] ouvrir directement sur le formulaire ouvert et ca génére un answer vide
	        //[ ] choisir une answer dans la liste pour lire
			if(!empty($form["inputs"]))
				foreach ($form["inputs"] as $kform => $vform) {
					$form["inputs"][$kform]["docinputsid"] = (string) $form["_id"];
				}
	        if($showForm)
	        {
	        	$params =[
	        			"contextId" => @$contextId,
                        "contextType" => @$contextType,
	          			"parentForm"=>$parentForm,
	          			"formId" => $formId,
	           			"form" => $form,
	           			"subFs" => isset($subFs) ? $subFs : "",
	           			"answer" => $answer,
	           			"mode" => $mode,
	           			"canEdit" => $canEdit,
	           			"canEditForm" => $canEditForm,
	           			"canAdminAnswer" => $canAdminAnswer,
	           			"usersAnswered" => @$usersAnswered,
	           		  	"el" => $el ];
	          	if( isset($wizard) ){
	          		$params["saveOneByOne"] = true;
	          		$params["wizard"] = true;
	          	}

	           	echo $this->renderPartial("survey.views.tpls.forms.formbuilder",$params ,true );
	        } else if( count($myAnswers) == 0 ) {
	            echo "<h4>Bienvenue vous n'avez pas encore participer.</h4>";

	            $countTotalAnswers = PHDB::count( Answer::COLLECTION,[ "form" => (String)$form['_id'] ] );
	            $countdistinctUsers = count(PHDB::distinct( Answer::COLLECTION,"user",[ "form" => (String)$form['_id'] ] ));
	            if($countTotalAnswers == 0)
	              echo "<br/>Soyez le premier à répondre!!!";
	            else
	              echo "<br/>".$countTotalAnswers." réponses déposées par ".$countdistinctUsers." personnes";

	            echo "<a class='btn btn-primary' href='/costum/co/index/slug/".$el["slug"]."/new/true'>Participer</a>";
	   		} else {

	   			echo "<h4>Vous avez deja participer, vous pouvez retoucher vos ".
	   					PHDB::count( Form::ANSWER_COLLECTION,
	   								[ 	"form" => (String)$form['_id'],
                                    	"user" => Yii::app()->session["userId"] ] ).
	   					" réponses dans la liste.</h4>";
	            echo "<a class='btn btn-primary' href='/costum/co/index/slug/".$el["slug"]."/new/true'>Ajouter une réponse</a>";
	        }

	}

} else {
  if($checkDate == "late")
    echo "<h4 class='text-center text-red'><i class='fa fa-warning'></i> ". Yii::t("common", "You can start responding from")." ".$parentForm["startDate"]."</h4>";
  else if ($checkDate == "past")
    echo "<h4 class='text-center text-red'><i class='fa fa-warning'></i> ".Yii::t("common", "Response time expired on")." ".$parentForm["endDate"]."</h4>";
}

?>


<script type="text/javascript">

jQuery(document).ready(function() {
	mylog.log("render","/modules/survey/views/tpls/forms/formSection.php");
});

</script>
