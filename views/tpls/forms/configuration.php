<?php
    if ($canAdminAnswer) {
        HtmlHelper::registerCssAndScriptsFiles(array(
            '/plugins/jQuery-Knob/js/jquery.knob.js',
            // SHOWDOWN
            '/plugins/showdown/showdown.min.js',
            // MARKDOWN
            '/plugins/to-markdown/to-markdown.js',
            //Float button
            '/plugins/float-button-st-panel/css/st.action-panel.css',
            '/plugins/float-button-st-panel/js/st.action-panel.js',
            //SmartWizard
            '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.min.js',
            '/plugins/jQuery-Smart-Wizard/css/smart_wizard_all.min.css',
            '/plugins/moment/min/moment.min.js',
            '/plugins/bootstrap-datetimepicker/xdsoft/jquery.datetimepicker.full.min.js',
            '/plugins/bootstrap-datetimepicker/xdsoft/jquery.datetimepicker.min.css'
        ), Yii::app()->request->baseUrl);

        $cssAnsScriptFilesModule = array(
            '/js/admin/panel.js',
            '/js/admin/admin_directory.js',
        );
        HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());

        HtmlHelper::registerCssAndScriptsFiles(array(
            '/js/answer.js',
            '/css/newFormWizard.css',
        ), Yii::app()->getModule(Survey::MODULE)->getAssetsUrl());
        $ignore = array('_file_', '_params_', '_obInitialLevel_', 'ignore');
        $params = array_diff_key(get_defined_vars() + array_flip($ignore), array_flip($ignore));
        
        ?>
        <style type="text/css">
            .coform-navbar #configcontent .tab-pane{
                font-family: "montserrat" !important;
            }

            .coform-navbar #configcontent .tab-pane label {
                font-weight: bold;
                font-size: 15px;
            }

            .coform-navbar #configcontent .panel-body h5 {
                position: absolute;
                top: -23px;
                left: 30px;
                z-index: 100;
                color: var(--aap-primary-color);
                background: #fff;
                padding: 5px 10px;
            }

            .coform-navbar .nav.nav-tabs {
                margin-top: 10px;
            }

            .coform-navbar .panel-default {
                border-color: #a5a5a5;
            }

            .coform-navbar .panel-default h5 {
                color: var(--aap-primary-color);;
            }

            .checkbox.checbox-switch {
                padding-left: 0;
            }

            .checkbox.checbox-switch label,
            .checkbox-inline.checbox-switch {
                display: inline-block;
                position: relative;
                padding-left: 0;
            }

            .checkbox.checbox-switch label input,
            .checkbox-inline.checbox-switch input {
                display: none;
            }

            .checkbox.checbox-switch label span,
            .checkbox-inline.checbox-switch span {
                width: 45px;
                border-radius: 20px;
                height: 25px;
                border: 1px solid #dbdbdb;
                background-color: rgb(255, 255, 255);
                border-color: rgb(223, 223, 223);
                box-shadow: rgb(223, 223, 223) 0px 0px 0px 0px inset;
                transition: border 0.4s ease 0s, box-shadow 0.4s ease 0s;
                display: inline-block;
                vertical-align: middle;
                margin-right: 5px;
            }

            .checkbox.checbox-switch label span:before,
            .checkbox-inline.checbox-switch span:before {
                display: inline-block;
                width: 22px;
                height: 22px;
                border-radius: 50%;
                background: rgb(255, 255, 255);
                content: " ";
                top: 0;
                position: relative;
                left: 0;
                transition: all 0.3s ease;
                box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
            }

            .checkbox.checbox-switch label > input:checked + span:before,
            .checkbox-inline.checbox-switch > input:checked + span:before {
                left: 25px;
            }

            /* Switch Success */
            .checkbox.checbox-switch.switch-success label > input:checked + span,
            .checkbox-inline.checbox-switch.switch-success > input:checked + span {
                background-color: var(--aap-primary-color);
                border-color: var(--aap-primary-color);
                box-shadow: var(--aap-primary-color) 0px 0px 0px 8px inset;
                transition: border 0.4s ease 0s, box-shadow 0.4s ease 0s, background-color 1.2s ease 0s;
            }

        </style>

        <div class="padding-left-10 coform-navbar">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" id="confignav" role="tablist">
                <?php
                    if ($canAdminAnswer) {
                        ?>
                        <li class="<?= $aapview == "parentform" ? 'active' : '' ?>">
                            <a href="#parentform" role="tab" data-toggle="tab">Detail du formulaire</a>
                        </li>
                        <li class="<?= $aapview == "formaccess" ? 'active' : '' ?>">
                            <a href="#formaccess" role="tab" data-toggle="tab">Accessibilité</a>
                        </li>
                        <li class="<?= $aapview == "wizard" ? 'active' : '' ?>">
                            <a href="#wizard" role="tab" data-toggle="tab">Les étapes</a>
                        </li>
                        <li class="<?= $aapview == "inputlist" ? 'active' : '' ?>">
                            <a href="#inputlist" role="tab" data-toggle="tab">Les questions</a>
                        </li>
                        <!--<li><a href="#standalone" role="tab" data-toggle="tab">Reponse rapide (standalone)</a></li>
                        <li><a href="#devs" role="tab" data-toggle="tab">Developer space </a></li>-->
                        <?php
                    }
                    if ($isAap && $canAdminConfig) {
                        ?>
                        <li class="<?= $aapview == "configform" ? 'active' : '' ?>">
                            <a href="#configform" role="tab" data-toggle="tab">Template appel à projet</a>
                        </li>
                        <?php
                    }
                ?>
                <li class="<?= $aapview == "community" ? 'active' : '' ?>">
                    <a href="#community" role="tab" data-toggle="tab"><?= Yii::t("common", "Community") ?></a>
                </li>
                <li class="<?= $aapview == "organization" ? 'active' : '' ?>">
                    <a href="#organization" role="tab" data-toggle="tab"><?= ucfirst(Yii::t("common","organization")) ?></a>
                </li>
                <li class="<?= $aapview == "rcChat" ? 'active' : '' ?>">
                    <a href="#rcChat" role="tab" data-toggle="tab">Notification</a>
                </li>
                <?php
                if ($isAap && $canAdminConfig) {
                ?>
                    <li class="<?= $aapview == "aacConfig" ? 'active' : '' ?>">
                        <a href="#aacConfig" role="tab" data-toggle="tab">Campagne</a>
                    </li>
                <?php
                }
                ?>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content" id="configcontent">
                <?php
                    if ($canAdminAnswer) {
                        ?>
                        <div class="tab-pane container <?= $aapview == "parentform" ? 'active' : '' ?>" id="parentform">
                            
                        </div>
                        <div class="tab-pane container <?= $aapview == "formaccess" ? 'active' : '' ?>" id="formaccess">
                            
                        </div>
                        <div class="tab-pane container <?= $aapview == "wizard" ? 'active' : '' ?>" id="wizard">
                            
                        </div>
                        <div class="tab-pane container <?= $aapview == "inputlist" ? 'active' : '' ?>" id="inputlist">
                            
                        </div>

                        <div class="tab-pane container <?= $aapview == "community" ? 'active' : '' ?>" id="community">
                            <div id="adminDirectory" class="col-xs-12 no-padding"></div>
                        </div>

                        <div class="tab-pane container <?= $aapview == "organization" ? 'active' : '' ?>" id="organization">
                            
                        </div>
                        <div class="tab-pane container <?= $aapview == "rcChat" ? 'active' : '' ?>" id="rcChat">
                            
                        </div>
                        <?php
                    }
                    if ($isAap && $canAdminConfig) {
                        ?>
                        <div class="tab-pane <?= $aapview == "configform" ? 'active' : '' ?>" id="configform">
                            
                        </div>
                        <div class="tab-pane <?= $aapview == "aacConfig" ? 'active' : '' ?>" id="aacConfig">
                            
                        </div>
                        <?php
                    }
                ?>
            </div>
        </div>

        <script type="text/javascript">
            if (typeof loadPartialConfigView == "undefined") {
                function loadPartialConfigView(formId, configView, container = "#configView", successCallback = () => {}) {
                    // coInterface.showLoader(container);
                    coInterface.showCostumLoader(container)
                    var url = baseUrl + `/survey/answer/answer/form/${formId}/mode/fa`;
                    var post = {
                        configView: configView
                    };
                    var callback = function (data) {
                        $(container).empty();
                        $(container).append(data);
                        successCallback();
                    };
                    ajaxPost(null, url, post, callback, "html");
                }
            }
            const allParams = <?= json_encode($params) ?>;
            var contextElt = <?php echo json_encode(@$context)  ?>;
            var panelAdmin = {
                "title": "Communauté",
                "context": {
                    "id": aapObj?.context?._id?.$id,
                    "collection": aapObj?.context?.type
                },
                "invite": {
                    "contextId": aapObj?.context?._id?.$id,
                    "contextType": aapObj?.context?.type
                },
                "community": {
                    "contextId": aapObj?.context?._id?.$id,
                    "contextType": aapObj?.context?.type,
                    "contextSlug": aapObj?.context?.slug
                },
                "table": {
                    "name": {
                        "name": "Membres"
                    },
                    "tobeactivated": {
                        "name": "Validation de compte",
                        "class": "col-xs-2 text-center"
                    },
                    "isInviting": {
                        "name": "Validation pour être membres",
                        "class": "col-xs-2 text-center"
                    },
                    "roles": {
                        "name": "Roles",
                        "class": "col-xs-1 text-center"
                    },
                    "admin": {
                        "name": "Admin",
                        "class": "col-xs-1 text-center"
                    }
                },
                "paramsFilter": {
                    "container": "#filterContainer",
                    "defaults": {
                        "types": [
                            "citoyens"
                        ],
                        "fields": [
                            "name",
                            "email",
                            "links",
                            "collection"
                        ],
                        "notSourceKey": "true",
                        "indexStep": "50",
                        "filters": {
                            ["links.memberOf."+aapObj?.context?._id?.$id]: {
                                "$exists": "1"
                            }
                        }
                    },
                    "filters": {
                        "text": "true"
                    }
                },
                "actions": {
                    "admin": "true",
                    "roles": "true",
                    "disconnect": "true"
                },
                "costumSlug": aapObj?.context?.slug,
                "costumEditMode": "false"
            };
            var filterAdmin = {};
            var paramsFilterAdmin= {};
            if (typeof initializeAdminPanel == "undefined") {
                function initializeAdminPanel() {
                    var paramsFilterAdmin= {
                        container : "#filters-nav-admin",
                        loadEvent: {
                            default : "admin",
                            options : {
                                results : {},
                                initType : panelAdmin.types,
                                panelAdmin : panelAdmin
                            }
                        },
                        results : {
                            multiCols : false
                        },
                        header : {
                            dom : ".headerSearchContainerAdmin",
                            options : {
                                left : {
                                    classes : 'col-xs-8 elipsis no-padding',
                                    group:{
                                        count : true
                                    }
                                }
                            }
                        },
                        urlData : baseUrl+"/co2/search/globalautocompleteadmin"
                    };
                    if(notNull(contextElt) && notEmpty(contextElt)){
                        paramsFilterAdmin.urlData+="/type/"+contextElt.type+"/id/"+contextElt.id;
                    }
                    if(typeof panelAdmin != "undefined" && typeof panelAdmin.paramsFilter != "undefined"){
                        if(typeof panelAdmin.paramsFilter.filters != "undefined")
                            paramsFilterAdmin.filters = panelAdmin.paramsFilter.filters;
                        if(typeof panelAdmin.paramsFilter.defaults != "undefined")
                            paramsFilterAdmin.defaults = panelAdmin.paramsFilter.defaults;
                            if(typeof panelAdmin.paramsFilter.header != "undefined")
                            paramsFilterAdmin.header = panelAdmin.paramsFilter.header;
                    }

                    if(typeof panelAdmin != "undefined" && typeof panelAdmin.context != "undefined"){
                        
                        paramsFilterAdmin.loadEvent.options["context"] = panelAdmin.context;
                    }
                    if(costum!=null && typeof costum[costum.slug] != "undefined" && typeof costum[costum.slug].aDirectory!="undefined" && typeof costum[costum.slug].aDirectory.actions != "undefined"){
                        $.each(costum[costum.slug].aDirectory.actions, function(key, value){
                            panelAdmin.actions[key] = value;
                        });
                    }
                    // Admin structure simple d'admin pour qu'elle s'adapte au header du searchObj	
                    if( typeof panelAdmin.csv != "undefined" || 
                        typeof panelAdmin.pdf != "undefined"|| 
                        typeof panelAdmin.invite != "undefined"){
                        paramsFilterAdmin.header.options.right = {
                            classes : 'col-xs-4 text-right no-padding',
                            group : { }
                        }

                        if(typeof panelAdmin.csv != "undefined")
                            paramsFilterAdmin.header.options.right.group.csv = panelAdmin.csv;
                        if(typeof panelAdmin.pdf != "undefined")
                            paramsFilterAdmin.header.options.right.group.pdf = panelAdmin.pdf;
                        if(typeof panelAdmin.invite != "undefined")
                            paramsFilterAdmin.header.options.right.group.invite = panelAdmin.invite;
                        /*if(typeof panelAdmin.community != "undefined")
                            paramsFilterAdmin.header.options.right.group.community = panelAdmin.community;*/
                    }
                    // Configure searchObj for admin and construct central table 
                    filterAdmin = searchObj.init(paramsFilterAdmin);

                    // Search results for admin initialisation
                    var myResult = filterAdmin.search.init(filterAdmin);
                }
            }
            if ($('#confignav')) {
                var navmenu = $('#confignav');
                var navcontent = $('#configcontent');
            }
            function scroll() {
                if ($(window).scrollTop() >= $('#mainNav').height() + $('drop-down-sub-menu').height()) {
                    if (navmenu) {
                        navmenu.addClass('navbar-fixed-top');
                        navmenu.css('padding-top', ($('#mainNav').height() + $('drop-down-sub-menu').height() + 8) + 'px');
                    }
                } else {
                    if (navmenu) {
                        navmenu.removeClass('navbar-fixed-top');
                        navmenu.css('padding-top', '10px');
                    }
                }
            }

            function toogleConfig(id,isTrue){
                switch (id) {
                    case "useBackgroundImg":
                        isTrue ? $('.add-background-container').show(200) : $('.add-background-container').hide(200);
                        break;
                    case "useBannerImg":
                    case "useBannerText":
                        ($("#useBannerImg").is(':checked') || $("#useBannerText").is(':checked')) ? $('#go-to-banner-config').parent().show(200) : $('#go-to-banner-config').parent().hide(200);
                        break;
                    case "useBackgroundColor":
                        isTrue ? $('.add-backgroundColor-container').show(200) : $('.add-backgroundColor-container').hide(200);
                        break;
                    /*case "hideNameOrganizationBesideLogo" : 
                        isTrue ? $('.aap-organism-chooser').hide(200) : $('.aap-organism-chooser').show(200)*/
                    default:
                        break;
                }
            }
            if (typeof configurationEvent == "undefined") {
                function configurationEvent() {
                    $(".btn-toggle-coformbuilder-sidepanel").off("click").on("click", function() {
                        $(this).toggleClass("arrow-inverse")
                        $(`.coformbuilder-${$(this).data("target")}-content`).toggleClass("active")
                        $("#coformbuilderCenterContent").css("margin-right", "auto")
                        var changeWidth = setInterval(() => {
                            const sidePanelWidth = $(".coformbuilder-side-content").width();
                            if(sidePanelWidth > 0) {
                                if(parseInt($("#coformbuilderCenterContent").css("margin-right")) < (sidePanelWidth + 13) && $(this).hasClass("arrow-inverse")) {
                                    $("#coformbuilderCenterContent").css("margin-right", sidePanelWidth + 40)
                                }
                                clearInterval(changeWidth);
                                changeWidth = null; 
                            }
                        }, 250);
                        // $("#coformbuilderCenterContent").toggleClass("ml-5")
                    });
                    $('.aapchecboxconfig').off().on('change', function () {
                        let thisbtn = $(this);
                        thisbtn.is(':checked') ? toogleConfig(thisbtn.attr("id"),true) : toogleConfig(thisbtn.attr("id"),false)

                        if ($(this).data().hasOwnProperty("toogle")) {
                            if ($(this).is(':checked')) {
                                $.each($(this).attr("data-toogle").split(','), function (index, item) {
                                    $('#' + item).show();
                                });
                            } else {
                                toogleConfig($(this).attr("id"),false)
                                $.each($(this).attr("data-toogle").split(','), function (index, item) {
                                    $('#' + item).hide();
                                });
                            }
                        }
                        var tplCtx = {
                            collection : thisbtn.data("collection"),
                            id : thisbtn.data("id"),
                        };

                        if (thisbtn.data("updatepartial") && !thisbtn.hasClass("notswitch")) {
                            tplCtx.path = "allToRoot";
                            tplCtx.updatePartial = true;
                            tplCtx.value = {};
                            tplCtx.value[thisbtn.data("path")] = thisbtn.is(':checked');
                        } else if (thisbtn.hasClass("notswitch")) {

                        } else {
                            tplCtx.path = thisbtn.data("path");
                            tplCtx.value = thisbtn.is(':checked');
                        }

                        dataHelper.path2Value(tplCtx, function (params) {
                            toastr.success('Configuration mis à jour');
                            if (thisbtn.data("callback") && typeof window[thisbtn.data("callback")] === "function") {
                                window[thisbtn.data("callback")].call(thisbtn, params);
                            }
                            if(thisbtn.data("path") == "DisconnectedView"){
                                var tplCtx2 = {
                                    collection : aapObj.context.type,
                                    id : aapObj.context._id.$id
                                };
                                var pathArray = [
                                    "proposalDisconnectAap",
                                    "projectDisconnectAap"
                                ];
                                var valueArray = [
                                    "proposal",
                                    "project"
                                ];
                                $.each(pathArray , function (index , value){
                                    if(thisbtn.is(':checked')){
                                        tplCtx2.value = true;
                                    } else {
                                        tplCtx2.value = false;
                                    }
                                    tplCtx2.path = `costum.htmlConstruct.header.menuTop.left.buttonList.app.buttonList.#${value}`;
                                    dataHelper.path2Value(tplCtx2, function(){});
                                    tplCtx2.path = `costum.htmlConstruct.header.menuTop.left.buttonList.xsMenu.buttonList.app.buttonList.#${value}`;
                                    dataHelper.path2Value(tplCtx2, function(){});
                                    tplCtx2.path = `costum.app.#${value}`;
                                    tplCtx2.value = {
                                            "icon": "",
                                            "hash": "#app.aap",
                                            "urlExtra": `/page/${valueArray[index]}`,
                                            "name": {
                                                "fr": "Propositions"
                                            },
                                            "restricted": {
                                                "disconnected": true
                                            }
                                    };
                                    dataHelper.path2Value(tplCtx2, function(){});
                                });
                            }
                        });
                    });

                    $('.aapchecboxconfig').each(function (index, thisbtn) {
                        if ($(this).data().hasOwnProperty("toogle")) {
                            if ($(this).is(':checked')) {
                                $.each($(this).attr("data-toogle").split(','), function (index, item) {
                                    $('#' + item).show();
                                });
                            } else {
                                $.each($(this).attr("data-toogle").split(','), function (index, item) {
                                    $('#' + item).hide();
                                });
                            }
                        }
                    });

                    $(".aapinputconfig").on("blur", function (event) {
                        var tplCtx = {};
                        var thisbtn = $(this);
                        tplCtx.collection = thisbtn.data("collection");
                        tplCtx.id = thisbtn.data("id");
                        tplCtx.path = thisbtn.data("path");
                        tplCtx.value = thisbtn.val();

                        var valueAsMoment = moment(tplCtx.value);
                        if (valueAsMoment.isValid()) {
                            tplCtx.value = valueAsMoment.format();
                        }
                        if (thisbtn.data("settype")) {
                            tplCtx.setType = thisbtn.data("settype");
                        }
                        dataHelper.path2Value(tplCtx, function (params) {
                            toastr.success('Configuration mis à jour');
                            var newProposalButtonDom = $('.proposalDropdown>.dropdown-menu a:nth-child(3)');
                            var form = params.elt;
                            if(form.active) {
                                newProposalButtonDom.hide();
                                var url = baseUrl + '/co2/aap/proposition/request/user_have_answer';
                                var post = {
                                    form: form._id.$id
                                };
                                ajaxPost(null, url, post, function(results) {
                                    if(notEmpty(form.oneAnswerPerPers) && form.oneAnswerPerPers) {
                                        if(!results.hasAnswer && typeof results?.session == "string" && results.session == "include") newProposalButtonDom.show();
                                    } else if(typeof results?.session == "string" && results.session == "include") {
                                        newProposalButtonDom.show();
                                    }
                                });
                            } else {
                                newProposalButtonDom.hide();
                            }
                        });
                    });
                }
            }
            if (typeof loadSpecView == "undefined") {
                function loadSpecView(view) {
                    if (typeof loadPartialConfigView == "function") {
                        if (allParams?.parentForm?._id?.$id) {
                            var configView = "parentformConfig";
                            switch (view) {
                                case "rcChat":
                                    configView = "rcNotificationConfig";
                                    break;
                                case "aacConfig":
                                    configView = "aacConfigView";
                                    break;
                                default:
                                    configView = view + "Config";
                                    break;
                            }
                            if (view != "community") {
                                loadPartialConfigView(allParams.parentForm._id.$id, configView, `.tab-pane.container#${ view },.tab-pane#${ view }`, configurationEvent);
                            } else {
                                initializeAdminPanel();
                            }
                        }
                    }
                }
            }
            jQuery(document).ready(function () {
                loadSpecView(allParams.aapview);
                $("#confignav li a[role=tab][data-toggle=tab]").on("shown.bs.tab", function (e) {
                    if (typeof aapObj != 'undefined' && aapObj.common.buildUrlQuery) {
                        history.pushState(null, null, aapObj.common.buildUrlQuery(document.location.href, {aapview : $(this).attr("href").replace("#", "")}, aapObj.common.getQueryStringObject()));
                        loadSpecView($(this).attr("href").replace("#", ""));
                    }
                })
                window.addEventListener('scroll', scroll);
            });
        </script>
        <?php
    } else {
        echo $this->renderPartial("co2.views.default.unTpl", ["msg"  => Yii::t("survey", "You are not allowed to access. You don't have the right"),
                                                                          "icon" => "fa-lock"
        ], true);
    }
?>
