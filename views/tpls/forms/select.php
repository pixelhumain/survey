<?php 
$inpClass = "";
if($saveOneByOne)
    $inpClass = " saveOneByOne"; 

$paramsData = [ "options" => [ ] ];

if( isset($parentForm["params"][$key]) ) 
    $paramsData =  $parentForm["params"][$key];


if(!isset($options) && isset($parentForm["params"][$key]['options']))
    $options = $parentForm["params"][$key]['options'];

if($mode == "r" || $mode == "pdf"){ ?>
    <div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
        <label for="<?php echo $kunik ?>">
            <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label ?></h4>
        </label><br/>
        <?php echo (!empty($options) && !empty($options[$answers])) ? $options[$answers] : "" ; ?>
    </div>
<?php 
}else{

$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$key."' class='previewTpl edit".$key."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";
?>

<div class="form-group col-xs-12 col-md-6 no-padding">
	<label for="<?php echo $key ?>">
        <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
            <?php echo $label.$editQuestionBtn.$editParamsBtn ?>
        </h4>
    </label>

    <?php if( !isset($options) ) 
        echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i>".Yii::t("survey","THIS FIELD HAS TO BE CONFIGURED FIRST").$editParamsBtn."</span>"; 
        else { 
            
            ?>
            <select class="form-control select2Input <?php echo $inpClass ?>" id="<?php echo $key ?>" data-form='<?php echo $form["id"] ?>' required>
            	<option value="">Selectionner</option>
            	<?php 
                $isAssociativeArray = (array_keys($options) !== range(0, count($options) - 1));
                foreach ($options as $k => $v) {
            		echo '<option value="'.($isAssociativeArray ? $k : $v).'" '.((!empty($answers) && (string)$k == $answers ) ? "selected" : "").' >'.$v.'</option>';
            	} ?>
            </select>
    
	<?php } if(!empty($info)){ ?>
    	<small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
    <?php } ?>
</div>
<?php } ?>
<?php if($mode != "pdf"){ ?>
<script type="text/javascript">
sectionDyf.<?php echo $key ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
jQuery(document).ready(function() {
    mylog.log("render form input","/modules/costum/views/tpls/forms/select.php");

    sectionDyf.<?php echo $key ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo $label ?> config",
            "description" : tradForm.possibleQuestionList,
            "icon" : "cog",
            "properties" : {
                options : {
                    inputType : "array",
                    label : tradForm.titleList,
                    values :  sectionDyf.<?php echo $key ?>ParamsData.options,
                    init : function() {
                        $(`<input type="text" class="form-control copyConfig" placeholder="vous pouvez copier le liste ici, séparé par des virgules; ou utiliser le button ajout ci-dessous"/>`).insertBefore('.optionsarray .inputs.array');
                        $(".copyConfig").off().on("blur", function() {
                            let textVal = $(this).val().length > 0 ? $(this).val().split(",") : [];
                            textVal.forEach((el, index) => {
                                dyFObj.init.addfield('.optionsarray', el, 'list')
                            });
                            $(this).val('')
                        })
                    }
                }
            },
            save : function () {  
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $key ?>Params.jsonSchema.properties , function(k,val) { 
                    if(val.inputType == "array")
                        tplCtx.value[k] = getArray('.'+k+val.inputType);
                    else
                        tplCtx.value[k] = $("#"+k).val();
                 });
                mylog.log("save tplCtx",tplCtx);
                
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
                        location.reload();
                    } );
                }

            }
        }
    };

    $(".edit<?php echo $key ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $key ?>Params,null, sectionDyf.<?php echo $key ?>ParamsData);
    });
});
</script>
<?php } ?>