<style type="text/css">
.questionList{list-style-type: none;}
.ui-state-highlight { height: 1.5em; line-height: 1.2em; }
</style>
<?php
if( !isset($parentForm["startDate"]) || 
    ( time() > strtotime(str_replace("/","-",$parentForm["startDate"])) && 
      ( !isset($parentForm["endDate"]) || time() <= strtotime(str_replace("/","-",$parentForm["endDate"])))) ||
    (isset($parentForm["endDate"]) && $parentForm["endDate"] == "")
  )
	{
	    if( isset(Yii::app()->session["userId"]))
	    {
	        //[X] ouvrir sur une answer /answer/xxx
	        //[X] revenir sur un answer pour modification
	        //[X] ouvrir la premiere fois et demander à participer 
	        //[X] ouvrir directement sur le formulaire ouvert et ca génére un answer vide 
	        //[ ] choisir une answer dans la liste pour lire
			if(!empty($form["inputs"]))
				foreach ($form["inputs"] as $kform => $vform) {
					$form["inputs"][$kform]["docinputsid"] = (string) $form["_id"];
				}
	        if($showForm)
	        {	
	        	$params =[ 
	          			"parentForm"=>$parentForm,
	          			"formId" => $formId,
	           			"form" => $form,
	           			"answer" => $answer,
	           			"mode" => $mode,
	           			"canEdit" => $canEdit,
	           			"canEditForm" => $canEditForm,
	           			"canAdminAnswer" => $canAdminAnswer,
	           		  	"el" => $el ];
	          	if( isset($wizard) ){
	          		$params["saveOneByOne"] = true;
	          		$params["wizard"] = true;
	          	}
	          	
	           	echo $this->renderPartial("survey.views.tpls.forms.formbuilder",$params ,true ); 
	        } else if( count($myAnswers) == 0 ) {
	            echo "<h4>Bienvenue vous n'avez pas encore participer.</h4>";
	            
	            $countTotalAnswers = PHDB::count( Answer::COLLECTION,[ "form" => (String)$form['_id'] ] );
	            $countdistinctUsers = count(PHDB::distinct( Answer::COLLECTION,"user",[ "form" => (String)$form['_id'] ] ));
	            if($countTotalAnswers == 0)
	              echo "<br/>Soyez le premier à répondre!!!";
	            else
	              echo "<br/>".$countTotalAnswers." réponses déposées par ".$countdistinctUsers." personnes";

	            echo "<a class='btn btn-primary' href='/costum/co/index/slug/".$el["slug"]."/new/true'>Participer</a>";
	   		} else {
	            
	   			echo "<h4>Vous avez deja participer, vous pouvez retoucher vos ".
	   					PHDB::count( Form::ANSWER_COLLECTION,
	   								[ 	"form" => (String)$form['_id'],
                                    	"user" => Yii::app()->session["userId"] ] ).
	   					" réponses dans la liste.</h4>";
	            echo "<a class='btn btn-primary' href='/costum/co/index/slug/".$el["slug"]."/new/true'>Ajouter une réponse</a>";
	        }
	    
	}
    
} else {
  if(time() < strtotime(str_replace("/","-",$parentForm["startDate"])) ) 
    echo "<h4 class='text-center text-red'><i class='fa fa-warning'></i> Les réponses commenceront le ".$parentForm["startDate"]."</h4>";
  else if ( time() > strtotime(str_replace("/","-",$parentForm["endDate"])))
    echo "<h4 class='text-center text-red'><i class='fa fa-warning'></i> Le temps de réponses est écoulé depuis le ".$parentForm["endDate"]."</h4>";
}
?>


<script type="text/javascript">

jQuery(document).ready(function() {
	mylog.log("render","/modules/survey/views/tpls/forms/formDescription.php");
});

</script>