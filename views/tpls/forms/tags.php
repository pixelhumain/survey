<style>
	.select2-container{
		width: 100%;
	}
</style>
<?php 
$value = (!empty($answers) && is_array($answers)) ? @$answers : [];

$inpClass = "";

if($mode == "r" || $mode == "pdf"){ ?>
    <div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
        <label for="<?php echo $kunik ?>">
        	<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
        		<?php echo $label ?>
        	</h4>
        </label><br/>
        <?php echo implode(" , ",$value); ?>
    </div>
<?php 
}else{
?>
	<div class="col-md-12 no-padding">
			<div class="form-group">
	    <label for="<?php echo $key ?>">
	    	<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
	    		<?php echo $label.$editQuestionBtn ?>
	    	</h4>
	    </label>
	    <br/>
	    <input type="<?php echo (!empty($type)) ? $type : 'text' ?>" 
	    		class="<?php echo $inpClass ?> select2TagsInput" 
	    		id="<?php echo $key ?>" 
	    		aria-describedby="<?php echo $key ?>Help" 
	    		placeholder="<?php echo (isset($placeholder)) ? $placeholder : '' ?>" 
	    		data-form='<?php echo $form["id"] ?>' 
				value= "<?= join(',',$value) ?>"
	    />
		<!-- <select class="<?php echo $inpClass ?>" id="<?php echo $key ?>" 
				aria-describedby="<?php echo $key ?>Help" 
	    		placeholder="<?php echo (isset($placeholder)) ? $placeholder : '' ?>" 
	    		data-form='<?php echo $form["id"] ?>'  <?php // echo @$value ?> style="width:100%" multiple>
			<option value="3620194" selected="selected">select2/select2</option>
		</select> -->
	    <?php if(!empty($info)){ ?>
	    	<small id="<?php echo $key ?>Help" class="form-text text-muted">
	    		<?php echo $info ?>
	    	</small>
	    <?php } ?>
	</div>

	</div>

<?php } ?>

<?php if($mode != "pdf"){ ?>
<script type="text/javascript">
	jQuery(document).ready(function() {
		var parentFornType = "<?= isset($parentForm["type"]) ? $parentForm["type"]:"" ?>";
		var formId = "<?php echo (string) $parentForm["_id"] ?>";
		var key = "<?php echo $key ?>";
		var url = baseUrl+'/api/tags/search';
		if(parentFornType =="aap")
			url = baseUrl+'/co2/aap/searchtags/';
		$("#<?php echo $key ?>").select2({
			// minimumInputLength: 3,
			tags: true,
			multiple: true,
			"tokenSeparators": [','],
			createSearchChoice: function (term, data) {
				if (!data.length)
					return {
						id: term,
						text: term
					};
			},
			ajax: {
				url: url,
				dataType: 'json',
				type: "GET",
				quietMillis: 50,
				data: function (term) {
					return {
						q: term,
						formId : formId,
						key:"params.tags.list"
					};
				},
				results: function (data) {
					return {
						results: $.map(data, function (item) {
							return {
								text: item.tag,
								id: item.tag
							}
						})
					};
				}
			}
		})
		.on('change',()=>{
			var tplCtx = {
				id : "<?php echo (string) $answer["_id"] ?>",
				path : "answers"+"."+$("#<?php echo $key ?>").data('form')+"."+$("#<?php echo $key ?>").attr('id'),
				value : $("#<?php echo $key ?>").val().split(","),
				collection : "<?= Form::ANSWER_COLLECTION ?>"
			}
			if(tplCtx.value[0] && tplCtx.value?.[0] != "") {
				dataHelper.path2Value(tplCtx, function (params) {
						var tag = $("#<?php echo $key ?>").val().split(',');
						var length = $("#<?php echo $key ?>").val().split(',').length
						tag = tag[length-1];
						if(typeof newReloadStepValidationInputGlobal != "undefined") {
							newReloadStepValidationInputGlobal({
								inputKey : <?php echo json_encode($key) ?>,
								inputType : "tpls.forms.tags"
							})
						}
						toastr.success(trad.saved);
						ajaxPost('',baseUrl+'/co2/aap/pushtags/id/'+formId+'/tag/'+tag+'/key/'+key, 
						null,
						function(data){},null);
				})
			} else {
				tplCtx.value = null;
				dataHelper.path2Value(tplCtx, function (params) {
					toastr.success(trad.saved);
					if(typeof newReloadStepValidationInputGlobal != "undefined") {
						newReloadStepValidationInputGlobal({
							inputKey : <?php echo json_encode($key) ?>,
							inputType : "tpls.forms.tags"
						})
					}
				})
			}
		});
		if ($("#<?php echo $key ?>").attr("value")) {
			$("#<?php echo $key ?>").select2('data', $.map($("#<?php echo $key ?>").val().split(','), function (item) {
				return {
					text: item,
					id: item
				}
			}));
		}
	   mylog.log("render form input","/modules/costum/views/tpls/forms/text.php");
	});
</script>
<?php } ?>

