<?php
HtmlHelper::registerCssAndScriptsFiles(['/plugins/html5sortable/html5sortable.js'], Yii::app()->request->baseUrl);
HtmlHelper::registerCssAndScriptsFiles(['/plugins/clipboard/clipboard.min.js'], Yii::app()->request->baseUrl);
?>

<style>
    .aapDetail .formImg{
        width: auto;
        height: 100px;
    }
    .aapDetail .configBtn{
        /* position: absolute;
        right: 5px;
        top: 0; */
        background-color:#9fbd38;
        border:1px solid #9fbd38;
    }
    .aapDetail .configBtn:hover{
        background-color:#fff;
        color:#9fbd38;
    }

    .dropplaceholder{
        padding: 0px;
        background: transparent;
        border: 2px dashed rgb(100, 150, 150);
    }

    .md-editor.md-fullscreen-mode .md-header {
        z-index: 1
    }


</style>

<?php
$subForms = ["Pas de bloquage"];
$completSubForms = array();
$formsInputs = array();

if (!empty($form["subForms"]))
{
    foreach ($form["subForms"] as $sbid => $sb)
    {
        $sbf = PHDB::findOne(Form::COLLECTION, ["id" => $sb]);

        if (!empty($sbf["name"]))
        {
            array_push($subForms, $sbf["name"]);
            $completSubForms[(String)$sbf["_id"]] = $sbf["name"];
            $sbfInputs = PHDB::findOneById(Form::COLLECTION, (string)$sbf['_id'], ['inputs']);
            if(isset($sbfInputs['inputs'])) $formsInputs[(String) $sbf["_id"]] = $sbfInputs['inputs'];
        }
    }
}

$formType = isset($form["type"]) ? $form["type"] : "";
if ($canEditForm)
{
    $subFormIds = [];
    if (isset($form["subForms"])) $subFormIds = array_merge($subFormIds, $form["subForms"]);
    if ($formType != "aap")
    {
        ?>

        <div class="container padding-10 text-center form-config-btn-container" style="background-color: #eee">
            <h4>Admin Form : <?php echo @$form["name"] ?></h4>
            <a href='javascript:;' data-id='<?php echo (String)$form["_id"]; ?>' class="configBtn btn btn-danger bold"><i class='fa fa-cogs'></i> <?php echo Yii::t("survey", "Set up") ?> </a>
            <?php if (!isset($parentForm["type"]) || (isset($parentForm["type"]) && $parentForm["type"] != "aapConfig" && $parentForm["type"] != "aap"))
            { ?>
                <a href='javascript:;' data-id='<?php echo (String)$form["_id"]; ?>' class='addStepBtn btn btn-danger bold'> <i class='fa fa-plus'></i> <?php echo Yii::t("survey","Add a step") ?> </a>
                <a href='javascript:;' data-id='<?php echo (String)$form["_id"]; ?>' class='createFrom btn btn-danger bold'> <i class='fa fa-plus'></i> <?php echo Yii::t("survey","Add indented text") ?> </a>
                <?php
            } ?>
            <a href='javascript:;' data-formparentid='<?php echo (String)$parentForm["_id"]; ?>' data-id='<?php echo (String)$form["_id"]; ?>' data-active="false" class="sortBtn btn btn-danger bold"><i class='fa fa fa-arrows-v'></i> <?php echo Yii::t("survey","Move question") ?> </a>
            <a href='javascript:;'  data-id='<?php echo (String)$form["_id"]; ?>' class='copystandalonelink btn btn-danger bold' data-clipboard-action="copy"> <i class='fa fa-plus'></i> <?php echo Yii::t("common","Link to Share this form") ?></a>
        </div>
        <?php
    } else {
        ?>
        <div class="container padding-10 text-center form-config-btn-container" style="background-color: #eee">
            <h4>Admin Form : <?php echo @$form["name"] ?></h4>
            <a href='javascript:;' data-formtype="aap" data-formparentid='<?php echo (String)$parentForm["_id"]; ?>' data-id='<?php echo (String)$form["_id"]; ?>' data-active="false" class="sortBtn btn btn-danger bold"><i class='fa fa fa-arrows-v'></i> <?php echo Yii::t("survey","Move question") ?> </a>
            <a href='javascript:;' data-id='<?php echo (String)$form["_id"]; ?>' class="configBtn btn btn-danger bold"><i class='fa fa-cogs'></i> <?php echo Yii::t("survey","Configure") ?></a>
            <?php if (isset($parentForm["type"]) && ($parentForm["type"] == "aapConfig" || $parentForm["type"] == "aap") )
            { ?>
                <a href='javascript:;'  data-id='<?php echo (String)$form["_id"]; ?>' class='copystandalonelink btn btn-danger bold' data-clipboard-action="copy"> <i class='fa fa-plus'></i> <?php echo Yii::t("common","Link to Share this form") ?> </a>
                <?php
            } ?>
        </div>

<?php

}
} ?>

<?php
$initImage = Document::getListDocumentsWhere(array(
    "id" => (string)$form["_id"],
    "type" => 'forms',
    "subKey" => "formImg"
) , "image");
$initDoc = Document::getListDocumentsWhere(array(
    "id" => (string)$form["_id"],
    "type" => 'forms',
    "subKey" => "formDoc"
) , "file");
?>

<?php 
$lastSubformId = (count($subFormIds) > 0 ? PHDB::findOne(Form::COLLECTION, ["id" => $subFormIds[count($subFormIds) - 1]], array('_id')) : 'null');
 ?>

<script type="text/javascript">
    var tplCtx = {};
    var subForms = <?php echo json_encode($subFormIds) ?>;
    var subFselect = <?php echo json_encode($subForms) ?>;
    var parentFormIdForNewStep = <?php echo isset($form['_id']) ? json_encode((string) $form['_id']) : 'null' ?>;
    var lastSubformId = <?php echo  json_encode($lastSubformId) ?>;
    var completSubForms = <?php echo json_encode($completSubForms) ?>;
    var formsInputs = <?php echo json_encode($formsInputs); ?>;
    var isCoform = <?php echo isset($parentForm['subType']) ? json_encode("false") : json_encode("true"); ?>;
    var parentFormStep = <?php echo json_encode($parentForm); ?>;

    jQuery(document).ready(function() {

        $('.copystandalonelink').off().on().click(function(){
            var sampleTextarea = document.createElement("textarea");
            document.body.appendChild(sampleTextarea);
            if(costum != null)
                sampleTextarea.value = baseUrl+"/costum/co/index/slug/"+slug+"#answer.index.id.new.form."+answerObj.form+".mode.w.standalone.true";
            else
                sampleTextarea.value = baseUrl+"#answer.index.id.new.form."+answerObj.form+".mode.w.standalone.true";
            sampleTextarea.select(); //select textarea contenrs
            document.execCommand("copy");
            document.body.removeChild(sampleTextarea);
            toastr.success('lien standalone copié');

        });

        sortable(".questionList", {
            forcePlaceholderSize: true,
            //placeholderClass: "questionBlockplaceholder",
            hoverClass: "questionBlockhover",
            disabled : true,
            placeholder: '<li class="dropplaceholder col-xs-12 no-padding questionBlock"></li>'
            //copy:true
        });

        sortable('.questionList', 'disable');

        $('.sortBtn').on("click",function() {
            $('.navigationarrow').toggle();

            var thisbtn = $(this);

            if (typeof thisbtn.data('formtype') != "undefined" && thisbtn.data('formtype') == 'aap' ){
                if (thisbtn.data('active') == true) {
                    sortable('.questionList', 'disable');
                    thisbtn.data('active', false);
                    thisbtn.html('<i class="fa fa-arrows-v"></i> <?php echo Yii::t("survey","Move question") ?>');

                    $.each(sortable('.questionList', 'serialize'), function (ind, val) {
                        if (typeof val.container != "undefined" &&
                            typeof val.container.node != "undefined" &&
                            typeof val.items != "undefined"
                        ) {
                            var formId = $(val.container.node).data('id');
                            var formIds = $(val.container.node).data('ids');
                            var fpid = $(val.container.node).data('fpid');

                            $.each(val.items, function (itmid, itmval) {
                                if (typeof itmval.index != "undefined" &&
                                    typeof itmval.node != "undefined" &&
                                    typeof $(itmval.node).data('docinputsid') != "undefined"
                                )

                                    var position
                                        = parseInt(itmval.index) + 1;
                                var inputid = $(itmval.node).data('key');
                                var inputids = $(itmval.node).data('docinputsid');

                                tplCtx.id = inputids;
                                tplCtx.collection = "inputs";
                                tplCtx.path = 'inputs.' + inputid + '.positions.'+fpid;
                                tplCtx.value = position;
                                tplCtx.formParentId = thisbtn.data('formparentid');

                                dataHelper.path2Value(tplCtx, function () {
                                });

                            });

                        }
                    });
                    toastr.success('Sauvegardé');

                } else if (thisbtn.data('active') == false) {
                    sortable('.questionList', 'enable');
                    thisbtn.data('active', true);
                    thisbtn.html('<i class="fa fa-save"></i> Enregistrer');
                }
            } else {
                if (thisbtn.data('active') == true) {
                    sortable('.questionList', 'disable');
                    thisbtn.data('active', false);
                    thisbtn.html('<i class="fa fa-arrows-v"></i> <?php echo Yii::t("survey","Move question") ?>');

                    $.each(sortable('.questionList', 'serialize'), function (ind, val) {
                        if (typeof val.container != "undefined" &&
                            typeof val.container.node != "undefined" &&
                            typeof val.items != "undefined"
                        ) {
                            var formId = $(val.container.node).data('id');
                            var formIds = $(val.container.node).data('ids');

                            $.each(val.items, function (itmid, itmval) {
                                if (typeof itmval.index != "undefined" &&
                                    typeof itmval.node != "undefined"
                                )
                                    var position = parseInt(itmval.index) + 1;
                                var inputid = $(itmval.node).data('key');

                                tplCtx.id = formIds;
                                tplCtx.collection = "forms";
                                tplCtx.path = 'inputs.' + inputid + '.position';
                                tplCtx.value = position;
                                tplCtx.formParentId = thisbtn.data('formparentid');

                                dataHelper.path2Value(tplCtx, function () {
                                });

                            });

                        }
                    });

                    toastr.success('Sauvegardé');

                } else if (thisbtn.data('active') == false) {
                    sortable('.questionList', 'enable');
                    thisbtn.data('active', true);
                    thisbtn.html('<i class="fa fa-save"></i> Enregistrer');
                }
            }
        });

        $('.navigationarrowbtn').click(function(e){
            var thisbtn = $(this),
                dir = $(this).data('dir'),
                jItems = $('li.questionBlock'),
                jItem = $(this).parents('li.questionBlock'),
                index = jItems.index(jItem);

            switch (dir) {
                case 'up':
                    if (index != 0) {
                        var item = thisbtn.parents('li.questionBlock').detach().insertBefore(jItems[index - 1]);
                    }
                    break;
                case 'down':
                    if (index != jItems.length - 1) {
                        var item = thisbtn.parents('li.questionBlock').detach().insertAfter(jItems[index + 1]);
                    }
                    break;
            }
            //var sortOrder = $('ul').sortable('toArray', {attribute: 'data-z'});

        });

        mylog.log("render","/modules/costum/views/tpls/forms/cplx/answers.php");
        mylog.log("govalosy",form);
        $('.configBtn').off().on("click",function() {
            tplCtx.id = $(this).data("id");
            mylog.log( "configBtn", tplCtx.id );
            //dyFObj.openForm( oformParams, null, form )
            var  copyFormObj =formObj.init();
            copyFormObj.dynForm.addDescription(copyFormObj,form,true)
        });
        $('.addStepBtn').off().on("click",function() {
            tplCtx.id = $(this).data("id");
            mylog.log( "addStepBtn", tplCtx.id );
            dyFObj.openForm( addStepParams, null, form )
        });
        /* $.each($(".markdown"), function(k,v){
             descHtml = dataHelper.markdownToHtml($(v).html());
             $(v).html(descHtml);
         });*/
        $('.description').moreAndLess({
            showChar : 200,
            ellipsestext : "<span class='letter-green'>. . .</span>",
            moretext : '<button class="btn btn-xs btn-success letter-white bg-green">'+trad.seemore+'</button>',
            lesstext : '<button class="btn btn-xs btn-success letter-white bg-green">'+trad.seeless+'</button>',
        });

        $('.createFrom').off().on('click', function() {
            dyFObj.openForm( createFromParams, null, form )
        })
    });
    var is_aap = <?=$formType == "aap" ? "true" : "false" ?>;
    var oformParams = {
        jsonSchema : {
            title : trad.configurequestionnaire,
            description : trad.readcarefullyoptions,
            icon : "fa-question",
            properties : {
                name : {
                    label : trad.formname,
                    rules: {
                        required:true
                    },
                    order:1
                },
                description : {
                    label : trad.questionnairedescription,
                    markdown : true,
                    inputType:"textarea",
                    order:2
                },
                what : { label : trad.nameananswer, placeholder:trad.proposal +", "+ trad.folders +", "+ trad.Projects +" ...",order:3},
                image :{
                    inputType : "uploader",
                    label : "Image",
                    docType : "image",
                    contentKey : "slider",
                    itemLimit : 1,
                    domElement:"image",
                    endPoint :"/subKey/formImg",
                    filetypes: ["jpeg", "jpg", "gif", "png"],
                    initList : <?php echo json_encode($initImage) ?>,
                    order:6
                },
                active : {
                    inputType : "checkboxSimple",
                    label : trad.activateopenresponse,
                    subLabel : trad.activatetoshareandopenresponsetrad,
                    params : { onText : trad.yes, offText : trad.no, onLabel : trad.yes, offLabel : trad.no,
                        labelText : "Activé"},
                    checked : false,
                    order:8
                },
                private : {
                    inputType : "checkboxSimple",
                    label : trad.private ,
                    subLabel : trad.onlycommunitycanedit,
                    params : { onText : trad.yes, offText : trad.no, onLabel : trad.yes, offLabel : trad.no,
                        labelText : trad.private},
                    checked : false,
                    order:9
                },
                canReadOtherAnswers: {
                    inputType : "checkboxSimple",
                    label : trad.openreadinganswers,
                    subLabel : trad.areanswersopentopublic,
                    params : { onText : trad.yes,offText : trad.no,onLabel : trad.yes,offLabel : trad.no,
                        labelText : trad.openanswers},
                    checked : true,
                    order:10
                },
                copyable : {
                    inputType : "checkboxSimple",
                    label : trad.otherelementscancopyform,
                    subLabel : trad.otherelementscancopyform,
                    params : { onText : trad.yes,offText : trad.no,onLabel : trad.yes,offLabel : trad.no,
                        labelText : "Copiable"},
                    checked : true,
                    order:11
                },
                startDate: {
                    inputType : "date",
                    label : tradDynForm.startDate,
                    order:12
                },
                endDate: {
                    inputType : "date",
                    label : tradDynForm.endDate ,
                    order:13
                },
                anyOnewithLinkCanAnswer : {
                    inputType : "checkboxSimple",
                    label : trad.sharebylink,
                    subLabel : trad.havelinkbeableanswer,
                    params : { onText : trad.yes,offText : trad.no,onLabel : trad.yes,offLabel : trad.no,
                        labelText : trad.uniqueresponse},
                    checked : true ,
                    order:14
                },
                oneAnswerPerPers: {
                    inputType : "checkboxSimple",
                    label : trad.blocktooneresponse,
                    subLabel : trad.apersoncanansweronly,
                    params : { onText : trad.yes,offText : trad.no,onLabel : trad.yes,offLabel : trad.no,
                        labelText : trad.uniqueresponse},
                    checked : true,
                    order:15
                },
                canModify : {
                    inputType : "checkboxSimple",
                    label : trad.modifiableresponse,
                    subLabel : trad.oncesubmittedreponsemodifiable,
                    params : { onText : trad.yes,offText : trad.no,onLabel : trad.yes,offLabel : trad.no,
                        labelText : trad.modifiableresponse},
                    checked : true,
                    order:16
                },
                showAnswers: {
                    inputType : "checkboxSimple",
                    label : trad.displayresponse,
                    subLabel : trad.arepostedpublicly,
                    params : { onText : trad.yes,offText : trad.no,onLabel : trad.yes,offLabel : trad.no,
                        labelText : trad.displayresponse},
                    checked : true ,
                    order:17
                },
                /*validateBtn: {
                    inputType : "checkboxSimple",
                    label : "Bouton Valider à la fin du formulaire",
                    subLabel : "Bouton Valider à la fin du formulaire",
                    params : { onText : trad.yes,offText : trad.no,onLabel : trad.yes,offLabel : trad.no,
                        labelText : "Bouton Valider"},
                    checked : true ,
                    order:18
                },*/
                temporarymembercanreply: {
                    inputType : "checkboxSimple",
                    label : trad.availableoffline,
                    subLabel : trad.availableoffline,
                    params : { onText : trad.yes,offText : trad.no,onLabel : trad.yes,offLabel : trad.no,
                        labelText : ""},
                    checked : false,
                    order:4
                },
                withconfirmation: {
                    inputType : "checkboxSimple",
                    label : trad.ifavailableofflinesendmail,
                    subLabel : trad.availableoffline,
                    params : { onText : trad.yes,offText : trad.no,onLabel : trad.yes,offLabel : trad.no,
                        labelText : ""},
                    checked : true,
                    order:4
                },
                startDateNoconfirmation: {
                    inputType : "date",
                    label : trad.startdatetoanswerwithoummail,
                    order:12
                },
                endDateNoconfirmation: {
                    inputType : "date",
                    label : trad.enddatetoanswerwithoummail,
                    order:13
                },
                hasStepValidations: {
                    inputType : "select",
                    label : trad.lockfromstep,
                    options : subFselect,
                    order:13
                },
                /*feedbackBtn : {
                    inputType : "checkboxSimple",
                    label : "Bouton Valider à la fin du formulaire",
                    subLabel : "Bouton Valider à la fin du formulaire",
                    params : { onText : trad.yes,offText : trad.no,onLabel : trad.yes,offLabel : trad.no,
                        labelText : "Bouton Valider"},
                    checked : true },*/
            },
            onLoads : {
                onload: function(){
                    <?php if ($formType == "aap")
                    { ?>
                    alignInput2(["activecheckboxSimple"], "activation", 6, 6, null, null, "Activation", "#3f4e58", "");
                    alignInput2(["imageuploader","documentuploader"], "upload", 6, 6, null, null, "Image et fichier PDF", "#3f4e58", "");
                    alignInput2(["startDatedate","endDatedate"], "date", 6, 6, null, null, "Dates", "#3f4e58", "");
                    alignInput2(["privatecheckboxSimple","canReadOtherAnswerscheckboxSimple","anyOnewithLinkCanAnswercheckboxSimple","oneAnswerPerPerscheckboxSimple","canModifycheckboxSimple","showAnswerscheckboxSimple","validateBtncheckboxSimple","feedbackBtncheckboxSimple","copyablecheckboxSimple"], "test", 6, 12, null, null, "Plus", "#3f4e58", "");
                    $('.mainDynFormCloseBtn').addClass('btn-block btn-lg').css('margin-right',"0");
                    $('#btn-submit-form').addClass("btn-block btn-lg bg-green-k letter-white").removeClass("letter-green");
                    <?php
                    } ?>
                }
            },
            beforeBuild : function(){
                uploadObj.set("forms","<?=(string)$form["_id"] ?>");
            },
            beforeSave : function(){
                if ($("#ajaxFormModal #hasStepValidations").val() == "" || $("#ajaxFormModal #hasStepValidations").val() == "0"){
                    $("#ajaxFormModal #hasStepValidations").val("");
                }else{
                    $("#ajaxFormModal #hasStepValidations").val(parseInt($("#ajaxFormModal #hasStepValidations").val()) - 1);
                }
            },
            save : function (formData) {
                mylog.log('oformParams save tplCtx formData', formData)
                tplCtx.collection = "forms";
                tplCtx.path = "allToRoot";
                tplCtx.value = {};
                $.each(oformParams.jsonSchema.properties, function (k, v) {
                    var kk = k.split("-").join("][");

                    if (k != "hasStepValidations") {
                        if (v.inputType == "array")
                            tplCtx.value[kk] = getArray('.' + k + v.inputType);
                        else
                            tplCtx.value[kk] = $("#" + k).val();
                    } else {
                        tplCtx.value[kk] = parseInt($("#" + k).val());
                    }


                    // if(k == "objectives")
                    //     tplCtx.value[kk] = formData["objectives"];
                    // if(k == "actionDomains")
                    //     tplCtx.value[kk] = formData["actionDomains"];
                });
                mylog.log("oformParams save tplCtx", tplCtx);

            }
        }
    }


    //             if(typeof tplCtx.value == "undefined")
    //             	toastr.error('value cannot be empty!');
    //             else {
    //                 dataHelper.path2Value( tplCtx, function(params) {
    //                     dyFObj.commonAfterSave(params,function(){
    //                         reloadWizard();
    //                         dyFObj.closeForm();
    //                     })
    //                 } );
    //             }

    //     	}
    //     }
    // };
    // //if(is_aap){
    //     oformParams.jsonSchema.properties.actionDomains = {
    //         label : "Domaine d'actions",
    //         inputType : "array",
    //         order:4
    //     };
    //     oformParams.jsonSchema.properties.objectives = {
    //         label : "Objectifs",
    //         inputType : "array",
    //         order:5
    //     };
    //     oformParams.jsonSchema.properties.document = {
    //         inputType : "uploader",
    //         label : "Document",
    //         docType : "file",
    //         contentKey : "file",
    //         itemLimit : 3,
    //         domElement:"document",
    //         endPoint :"/subKey/formDoc",
    //         filetypes: ["pdf"],
    //         initList : <?php echo json_encode($initDoc) ?>,
    //         order:7
    //     };
    // //}


    var addStepParams = {
        jsonSchema : {
            title : trad.addstep,
            description : trad.aformcancontainoneormorestep,
            icon : "fa-wpforms",
            properties : {
                name : {
                    label : trad.namethisstep,
                    value : ""
                }
            },
            save : function (formData) {
                mylog.log('addStepParams  save formData', formData)

                tplCtx.collection = "forms";
                tplCtx.path = "subForms";
                tplCtx.value = subForms;
                if( formData.name.indexOf("form:") >= 0 ){
                    var parts = formData.name.split(":");
                    mylog.log( "addStepParams added existing form", parts[1] );

                    if( typeof tplCtx.value == "undefined" )
                        tplCtx.value = [ parts[1] ];
                    else
                        tplCtx.value.push( parts[1] );

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        mylog.log("addStepParams save tplCtx",tplCtx);
                        dataHelper.path2Value( tplCtx, function(params) {
                            reloadWizard();
                            dyFObj.closeForm();
                        } );
                    }
                } else {

                    var now = new Date();
                    var cx = ""+now.getDate() + (now.getMonth()+1) + now.getFullYear()+"_"+now.getHours()+now.getMinutes();

                    var newFormId = "<?php echo $el["slug"] ?>"+cx+"_"+tplCtx.value.length;
                    var stepInputCt = Date.now().toString(36) + Math.random().toString(36).substring(2);
                    tplCtx.value.push(newFormId);

                    var newForm = {
                        collection : "forms",
                        value : {
                            id : newFormId,
                            name : formData.name,
                            type : "openForm",
                            inputs : isCoform ? {
                                [newFormId+stepInputCt] : {
                                    label : "default step validation",
                                    type : "tpls.forms.cplx.validateStep"
                                }
                            } : {}
                        }
                    };
                    mylog.log("addStepParams new sub Form",newForm);
                    dataHelper.path2Value( newForm, function() {
                        mylog.log("addStepParams created new sub Form",newFormId);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            mylog.log("addStepParams save tplCtx",tplCtx);
                            dataHelper.path2Value( tplCtx, function(params) { 
                                if(parentFormStep.hasStepValidations) {
                                    if(isCoform && parentFormStep.hasStepValidations == "") {
                                        const configForm = {
                                            id : tplCtx.id,
                                            collection : tplCtx.collection,
                                            path : "hasStepValidations",
                                            value : 0
                                        }
                                        dataHelper.path2Value(configForm, function(){})
                                    }
                                }
                                if(tplCtx.value.length > 1 && isCoform) {
                                    var newParams = {};
                                    var newParentForm = {...parentFormStep};
                                    if(params.elt) {
                                        if(params.elt['params'])
                                         newParentForm['params'] = {...params.elt['params']};
                                    }
                                    if(newParentForm.params) {
                                        for([paramsKey, paramsValue] of Object.entries(newParentForm.params)) {
                                            if(newParentForm['params'][paramsKey]['step']) {
                                                tplCtx.value.forEach((subValue, subIndex) => {
                                                    if(paramsKey.includes('validateStep'+subValue)) {
                                                        if(tplCtx.value[subIndex+1]) {
                                                            paramsValue.step = tplCtx.value[subIndex+1]
                                                        } else {
                                                            paramsValue.step = subValue
                                                        }
                                                        newParams[paramsKey] = paramsValue
                                                    } else {
                                                        newParams[paramsKey] = paramsValue
                                                    }
                                                })
                                            } else {
                                                newParams[paramsKey] = paramsValue
                                            }
                                        }
                                    }
                                    newParams['validateStep'+newFormId+stepInputCt] = {
                                        step : newFormId
                                    }
                                    const configFormParams = {
                                        id : tplCtx.id,
                                        collection : tplCtx.collection,
                                        path : "params",
                                        value : newParams
                                    }
                                    dataHelper.path2Value(configFormParams, function(){})
                                } else if(tplCtx.value.length == 1 && isCoform) {
                                    const configFormParams = {
                                        id : tplCtx.id,
                                        collection : tplCtx.collection,
                                        path : "params.validateStep"+newFormId+stepInputCt+".step",
                                        value : newFormId
                                    }
                                    dataHelper.path2Value(configFormParams, function(){})
                                }
                                urlCtrl.loadByHash(location.hash);
                                reloadWizard();
                                dyFObj.closeForm();
                            } );
                        }
                    } );
                }
            }
        }
    };

    trad['paste text here'] = "Coller les textes ici";
    trad['create form from text'] = "Créer un formulaire à partir des textes indentés";
    var inputsType = <?php echo json_encode(Form::inputTypes()); ?>;
    let inputsTypeAsArray;
    inputsType ? inputsTypeAsArray = Object.entries(inputsType) : '';
    var createFromParams = {
        jsonSchema : {
            title : trad['create form from text'],
            description : trad['paste text here'],
            icon : "wpforms",
            properties : {
                textvalue : {
                    label : trad['paste text here'],
                    inputType : 'textarea',
                    markdown : true,
                    init: function() {
                        var markdownInterval = setInterval(function() {
                            if($('.md-header.btn-toolbar .btn-group').length > 0) {
                                $('.md-header.btn-toolbar .btn-group').hide();
                                $('.md-header.btn-toolbar .dropdown').show().find('button.dropdown-toggle').removeClass("hide");
                                clearInterval(markdownInterval);
                            }
                        }, 1000)
                        $("#textvalue").off().on('blur', function() {
                            if($(this).val().indexOf('[step]') > -1) {
                                $('.toNewStepradio').hide().animate({opacity: 0}, 600);
                                $('.newSubFormTitletext').hide().animate({opacity: 0}, 600)
                            } else {
                                let val = $('.toNewStepradio').find('input[type="radio"]:checked').val();
                                if(val == 'non') {
                                    $('.toNewStepradio').show().animate({opacity: 1}, 600)
                                    // $('.newSubFormTitletext').show().animate({opacity: 1}, 600)
                                    if(Object.keys(completSubForms).length > 0) $('.existStepselect').show();
                                    $('.newSubFormTitletext').hide();
                                } else {
                                    $('.existStepselect').hide();
                                    $('.newSubFormTitletext').show();
                                }
                            }
                        })
                    }
                },
                // dyFInputs.radio( "Display Images ?", { "true" : { icon:"check-circle-o", lbl:trad.yes },
				// 							 			"false" : { icon:"circle-o", lbl:trad.no} } )
                toNewStep : {
                    label : 'Souhaitez vous créer une nouvelle étape',
                    inputType: 'radio',
                    options: {
                        oui: {
                            lbl: 'Oui',
                            icon: 'check',
                            class: 'active class-sty',

                        },
                        non: {
                            lbl: 'Non',
                            icon: 'times',
                            class: 'class-sty',
                        } 
                    },
                    value: 'oui',
                    init : function(){
                        $("label.class-sty").off().on("click",function()
                        {
                            let val = $(this).find('input[type="radio"]').val();
                            if(val == 'non') {
                                if(Object.keys(completSubForms).length > 0) $('.existStepselect').show();
                                $('.newSubFormTitletext').hide();
                            } else {
                                $('.existStepselect').hide();
                                $('.newSubFormTitletext').show();
                            }
                        });
                        //manage update bulding here 
                    }
                },
                newSubFormTitle : {
                    label : 'Titre de la nouvelle étape',
                    inputType : 'text',
                    value : 'Form name'
                },
                existStep : {
                    label : 'Séléctionner une étape où ajouter les questions',
                    inputType: 'select',
                    select2 : true,
                    placeholder : 'Sélectionner une étape',
                    options : completSubForms,
                    value : completSubForms ? Object.keys(completSubForms)[0] : '',
                    init : function() {
                        $('.existStepselect').hide()
                    }
                }
            },
            save : function (formData) {

                tplCtx.collection = "forms";
                tplCtx.path = "subForms";
                tplCtx.value = subForms;

                if(formData.textvalue != undefined) {
                    
                    if(formData.textvalue.indexOf('-') > -1) {
                        var now = new Date();
                        var cx = ""+now.getDate() + (now.getMonth()+1) + now.getFullYear()+"_"+now.getHours()+now.getMinutes();

                        var newFormId = "<?php echo $el["slug"] ?>"+cx+"_"+tplCtx.value.length;
                        tplCtx.value.push(newFormId);

                        var newForm = {
                            collection : "forms",
                            value : {
                                id : newFormId,
                                name : 'subfromname',
                                textvalue : formData.textvalue,
                                type : "openForm",
                                inputs : {}
                            }
                        };

                        var params = {
                            id: parentFormIdForNewStep,
                            collection: "forms",
                            path: "params",
                            value: {
                            },
                            formParentId: parentFormIdForNewStep,
                            key: "",
                            // inputId: "iHadADream772022_012_0l5aasgy47t2commqtd7"
                        };
                        textArray = newForm.value.textvalue.split("\n");
                        var lastKUnik = "";
                        let inputType = '';
                        let formNames = [];
                        let forms = {};
                        textArray.forEach((element, iterIndex) => {
                            if(iterIndex == 0) {
                                if(element.match(/\[.+]/g) != '[step]') {
                                    formNames.push('New subForm');
                                    forms[formNames[formNames.length -1]+'_'+formNames.length] = {}
                                }
                            }
                            if(element.match(/\[.+]/g)) {
                                if(element.match(/\[.+]/g) == '[step]') {
                                    formNames.push(element.replace(/\[.+\]|-\[.+\]|-\s\[.+\]|-\s|\[|\]|\]\s|:|\r/g, ''));
                                    forms[formNames[formNames.length -1]+'_'+formNames.length] = {};
                                    if(iterIndex > 0) {
                                        newFormId = "<?php echo $el["slug"] ?>"+cx+"_"+tplCtx.value.length;
                                        tplCtx.value.push(newFormId);
                                        newForm = {
                                            collection : "forms",
                                            value : {
                                                id : newFormId,
                                                name : 'subfromname',
                                                textvalue : formData.textvalue,
                                                type : "openForm",
                                                inputs : {}
                                            }
                                        };
                                    }
                                }
                            }
                            let inputCt = Date.now().toString(36) + Math.random().toString(36).substring(2);
                            path = newFormId+inputCt
                            if(element.match(/^(\s{2,})|((?<=\n(\s)+))(\s{2,})/g)) {
                                switch (inputType) {
                                    case 'tpls.forms.cplx.evaluation': {
                                        const quadrantsLength = Object.keys(params.value[lastKUnik].quadrants).length;
                                        params.value[lastKUnik].quadrants['quadrant'+quadrantsLength] = {
                                            label : "ce qui est a modifier",
                                            description : element.replace(/-\s{1,}|-\s{2,}|\s{2,}|\r/g, '').trim(),
                                            inputNb : 2
                                        };
                                        params.value[lastKUnik].transformationQuadrants['quadrantTransform'+quadrantsLength] = {
                                            label : "CRÉER",
                                            description : "À Créer",
                                            inputNb : 1
                                        }
                                    } break;
                                    case 'tpls.forms.select': {
                                        params.value[lastKUnik].options.push(element.replace(/-\s{1,}|-\s{2,}|\s{2,}|\r/g, '').trim())
                                    } break;
                                    case 'tpls.forms.cplx.multiCheckboxPlus': {
                                        var currElem = element.replace(/-\s{1,}|-\s{2,}|\s{2,}|\r/g, '');
                                        params.value[lastKUnik].global.list.push(currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim());
                                        if(currElem.toLowerCase().includes("autres")) {
                                            params.value[lastKUnik].tofill[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim()] = "cplx";
                                            if(currElem.match(/\s+`.+`|`.+`/g)) {
                                                params.value[lastKUnik].placeholdersckb[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim()] = currElem.match(/\s+`.+`|`.+`/g)[0].replace(/\s*``.+``|``.+``|\s*`|`/g, '')
                                            }
                                            if(currElem.match(/\s+``.+``|``.+``/g)) {
                                                params.value[lastKUnik].optinfo[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim()] = currElem.match(/\s+``.+``|``.+``/g)[0].replace(/\s+`|`/g, '')
                                            }
                                        } else {
                                            if(currElem.toLowerCase().match(/\[cplx\]/g)) {
                                                params.value[lastKUnik].tofill[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim()] = "cplx"
                                                if(currElem.match(/\s+`.+`|`.+`/g)) {
                                                    params.value[lastKUnik].placeholdersckb[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim()] = currElem.match(/\s+`.+`|`.+`/g)[0].replace(/\s*``.+``|``.+``|\s*`|`/g, '')
                                                }
                                                if(currElem.match(/\s+``.+``|``.+``/g)) {
                                                    params.value[lastKUnik].optinfo[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim()] = currElem.match(/\s+``.+``|``.+``/g)[0].replace(/\s+`|`/g, '')
                                                }
                                            } else {
                                                params.value[lastKUnik].tofill[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]/g, '').trim()] = "simple"
                                            }
                                        }
                                    } break;
                                    case 'tpls.forms.cplx.multiRadio' : {
                                        var currElem = element.replace(/-\s{1,}|-\s{2,}|\s{2,}|\r/g, '');
                                        params.value[lastKUnik].global.list.push(currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim());
                                        if(currElem.toLowerCase() == "autres") {
                                            params.value[lastKUnik].tofill[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim()] = "cplx";
                                            if(currElem.match(/\s`.+`|`.+`/g)) {
                                                params.value[lastKUnik].placeholdersradio[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim()] = currElem.match(/\s+`.+`|`.+`/g)[0].replace(/\s+`|`|`\s+``.+``|``.+``|\s+``.+``|``.+``/g, '')
                                            }
                                            if(currElem.match(/\s``.+``|``.+``/g)) {
                                                params.value[lastKUnik].optinfo[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim()] = currElem.match(/\s``.+``|``.+``/g)[0].replace(/\s+`|`/g, '')
                                            }
                                        } else {
                                            if(currElem.toLowerCase().match(/\[cplx\]/g)) {
                                                params.value[lastKUnik].tofill[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim()] = "cplx";
                                                if(currElem.match(/\s`.+`|`.+`/g)) {
                                                    params.value[lastKUnik].placeholdersradio[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim()] = currElem.match(/\s+`.+`|`.+`/g)[0].replace(/\s+`|`|`\s+``.+``|``.+``|\s+``.+``|``.+``/g, '')
                                                }
                                                if(currElem.match(/\s``.+``|``.+``/g)) {
                                                    params.value[lastKUnik].optinfo[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim()] = currElem.match(/\s``.+``|``.+``/g)[0].replace(/\s+`|`/g, '')
                                                }
                                            } else {
                                                params.value[lastKUnik].tofill[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]/g, '').trim()] = "simple"
                                            }
                                        }
                                    } break;
                                    case 'tpls.forms.cplx.simpleTable' : {
                                        var currElem = element.replace(/-\s{1,}|-\s{2,}|\s{2,}|\r/g, '');
                                        if(currElem.match(/\[.+\]/g)) {
                                            var currentMatch = currElem.match(/\[.+\]/g)[0].replace(/\[|\]/g, '').toLowerCase();
                                            if(currentMatch == "columns") {
                                                if(currElem.toLowerCase().match(/\(nombre\)/g)) {
                                                    params.value[lastKUnik].columns.push({
                                                        label : currElem.replace(/\[columns\]|-\[columns\]|-\s\[columns\]|\(nombre\)|\(Nombre\)|\s\(nombre\)|\s\(Nombre\)/g, '').trim(),
                                                        type : "Nombre"
                                                    })
                                                } else if(currElem.toLowerCase().match(/\(text\)/g)) {
                                                    params.value[lastKUnik].columns.push({
                                                        label : currElem.replace(/\[columns\]|-\[columns\]|\(text\)|\(Text\)|\s\(text\)|\s\(Text\)/g, '').trim(),
                                                        type : "Text"
                                                    })
                                                }else {
                                                    params.value[lastKUnik].columns.push({
                                                        label : currElem.replace(/\[columns\]|-\[columns\]|\(text\)|\(Text\)|\s\(text\)|\s\(Text\)/g, '').trim(),
                                                        type : "Text"
                                                    })
                                                }
                                            } else if(currentMatch == "rows") {
                                                params.value[lastKUnik].rows.push({
                                                    label : currElem.replace(/\[rows\]|-\[rows\]|-\s\[rows\]/g, '').trim()
                                                })
                                            }
                                        }
                                    } break;
                                    default: {
                                        params.value[lastKUnik].list.push(element.replace(/-\s{1,}|-\s{2,}|\s{2,}|\r/g, '').trim());
                                    } break;
                                }
                            } else {
                                let inputTypeSplit;
                                if(element.match(/\[.+\]/g)) {
                                    if(element.match(/\[.+\]/g) != '[step]'){
                                        let filtered = inputsTypeAsArray.filter(([key, values]) => {
                                            if(count < 1 && key.toLowerCase().includes(element.match(/\[.+\]/g)[0].toLowerCase().replace(/\[|\]/g, ''))){
                                                count++; return true} else {return false}
                                            }, count = 0);
                                        newForm.value.inputs[path] = {
                                            label : element.replace(/\[.+\]|-\[.+\]|-\s\[.+\]|-\s|\[|\]|\]\s|:|\r|\s*`.+`|`.+`/g, '').trim(),
                                            placeholder : element.match(/\s*`.+`|`.+`/g) ? element.match(/\s*`.+`|`.+`/g)[0].replace(/\s*``.+``|``.+``|\s*`|`/g, '') : '',
                                            info: element.match(/\s*``.+``|``.+``/g) ? element.match(/\s*``.+``|``.+``/g)[0].replace(/\s*`|`/g, '') : '',
                                            type : filtered.length > 0 ? filtered[0][0] : 'text'
                                        };
                                        inputType = newForm.value.inputs[path].type;
                                        inputTypeSplit = newForm.value.inputs[path].type.split('.');
                                        lastKUnik = inputTypeSplit[inputTypeSplit.length - 1]+path;
                                    }
                                } else {
                                    lastKUnik = 'text'+path;
                                    newForm.value.inputs[path] = {
                                        label : element.replace(/\[.+\]|-\[.+\]|-\s\[.+\]|-\s|\[|\]|\]\s|:|\r/g, '').trim(),
                                        placeholder : element.match(/\s*`.+`|`.+`/g) ? element.match(/\s*`.+`|`.+`/g)[0].replace(/\s*``.+``|``.+``|\s*`|`/g, '') : '',
                                        info: element.match(/\s*``.+``|``.+``/g) ? element.match(/\s*``.+``|``.+``/g)[0].replace(/\s*`|`/g, '') : '',
                                        type : 'text'
                                    }
                                }
                                if(textArray[iterIndex + 1]) {
                                    if(textArray[iterIndex + 1].match(/^(\s{2,})|((?<=\n(\s)+))(\s{2,})/g)) {
                                        switch (inputType) {
                                            case 'tpls.forms.checkbox': {
                                                params.value[lastKUnik] = {
                                                    list : []
                                                }
                                            } break;
                                            case 'tpls.forms.select': {
                                                lastKUnik = path;
                                                params.value[lastKUnik] = {
                                                    options : []
                                                }
                                            } break;
                                            case 'tpls.forms.cplx.evaluation': {
                                                params.value[lastKUnik] = {
                                                    objet : "A modifier dans la paramètrage",
                                                    objectif : "A modifier dans la paramètrage",
                                                    quadrants : {},
                                                    deleteOthersProposition : false,
                                                    isCollectif : false,
                                                    enableSteps : true,
                                                    transformationQuadrants : {}
                                                }
                                            } break;
                                            case 'tpls.forms.cplx.multiCheckboxPlus': {
                                                params.value[lastKUnik] = {
                                                    global : {
                                                        list : [],
                                                        dependOn : "",
                                                        width : ""
                                                    },
                                                    tofill : {},
                                                    optinfo : {},
                                                    optimage : {},
                                                    placeholdersckb : {}
                                                }
                                            } break;
                                            case 'tpls.forms.cplx.multiRadio': {
                                                params.value[lastKUnik] = {
                                                    global : {
                                                        list : [],
                                                        dependOn : "",
                                                        width : ""
                                                    },
                                                    tofill : {},
                                                    optinfo : {},
                                                    placeholdersradio : {}
                                                }
                                            } break;
                                            case 'tpls.forms.cplx.simpleTable' : {
                                                params.value[lastKUnik] = {
                                                    tableName : 'Titre',
                                                    columns : [],
                                                    rows : [],
                                                    activeNewLine : false,
                                                    singleAnswerByLine : true
                                                }
                                            } break;
                                            default: {
                                                params.value[lastKUnik] = {
                                                    list : []
                                                }
                                            } break;
                                        }
                                    }
                                }
                                forms[formNames[formNames.length -1]+'_'+formNames.length] = newForm;
                            }
                        });
                        let allParamsLast;
                        if(parentFormIdForNewStep) {
                            allParamsLast = <?php echo isset($form['_id']) ? json_encode(PHDB::findOneById(Form::COLLECTION, (string)$form['_id'], ['params'])) : 'null' ?>
                        }
                        if(formData.textvalue.indexOf('[step]') > -1){
                            formNames.forEach((formItem, formIndex) => {
                                if(forms[formItem+'_'+(formIndex+1)]) {
                                    if(formItem != '') forms[formItem+'_'+(formIndex+1)].value.name = formItem;
                                    dataHelper.path2Value(forms[formItem+'_'+(formIndex+1)], function() {
                                        if(dyFObj.path2Value.result) {
                                            if(dyFObj.path2Value.result.saved) {
                                                let tplCtxToSend = {
                                                    collection : 'forms',
                                                    id : parentFormIdForNewStep,
                                                    path : 'subForms',
                                                    value : tplCtx.value
                                                }
                                                dataHelper.path2Value(tplCtxToSend, function() {
                                                    
                                                })
                                            }
                                            
                                        }
                                    });
                                } else {
                                    toastr.error(trad.somethingwrong)
                                }
                                if(formNames.length - 1 == formIndex) {
                                    if(Object.keys(params.value).length > 0) {
                                        allParamsLast.params ? params.value = {...allParamsLast.params, ...params.value} : '';
                                        dataHelper.path2Value(params, function() {
                                            urlCtrl.loadByHash(location.hash);
                                            dyFObj.closeForm()
                                        })
                                    } else {
                                        urlCtrl.loadByHash(location.hash);
                                        dyFObj.closeForm();
                                    }
                                }
                            })
                        } else {
                            if(formData.toNewStep == "oui") {
                                /* let allParamsLast;
                                if(parentFormIdForNewStep) {
                                    allParamsLast = <?php echo isset($form['_id']) ? json_encode(PHDB::findOneById(Form::COLLECTION, (string)$form['_id'], ['params'])) : 'null' ?>
                                } */
                                if(formData.newSubFormTitle != "undefined" && formData.newSubFormTitle != '') newForm.value.name =formData.newSubFormTitle;
                                dataHelper.path2Value(newForm, function() {
                                        if(dyFObj.path2Value.result) {
                                            if(dyFObj.path2Value.result.saved) {
                                                let tplCtxToSend = {
                                                    collection : 'forms',
                                                    id : parentFormIdForNewStep,
                                                    path : 'subForms',
                                                    value : tplCtx.value
                                                }
                                                dataHelper.path2Value(tplCtxToSend, function() {
                                                    if(Object.keys(params.value).length > 0) {
                                                        allParamsLast.params ? params.value = {...allParamsLast.params, ...params.value} : '';
                                                        dataHelper.path2Value(params, function() {
                                                            // reloadWizard();
                                                            urlCtrl.loadByHash(location.hash);
                                                            dyFObj.closeForm()
                                                        })
                                                    } else {
                                                        urlCtrl.loadByHash(location.hash);
                                                        dyFObj.closeForm();
                                                    }
                                                })
                                            }
                                            
                                        }
                                    })
                            } else {
                                if(lastSubformId == 'null') {
                                    dataHelper.path2Value(newForm, function() {
                                        if(dyFObj.path2Value.result) {
                                            if(dyFObj.path2Value.result.saved) {
                                                let tplCtxToSend = {
                                                    collection : 'forms',
                                                    id : parentFormIdForNewStep,
                                                    path : 'subForms',
                                                    value : tplCtx.value
                                                }
                                                dataHelper.path2Value(tplCtxToSend, function() {
                                                    if(Object.keys(params.value).length > 0) {
                                                        dataHelper.path2Value(params, function() {
                                                            // reloadWizard();
                                                            urlCtrl.loadByHash(location.hash);
                                                            dyFObj.closeForm()
                                                        })
                                                    }  else {
                                                        urlCtrl.loadByHash(location.hash);
                                                        dyFObj.closeForm();
                                                    }
                                                })
                                            }
                                            
                                        }
                                    })
                                } else {
                                    let allInputsLast;
                                    // let allParamsLast;
                                    let existSubFormId;
                                    /* if(parentFormIdForNewStep) {
                                        allParamsLast = <?php echo isset($form['_id']) ? json_encode(PHDB::findOneById(Form::COLLECTION, (string)$form['_id'], ['params'])) : 'null' ?>
                                    } */
                                    if(formData.existStep != 'undefined' && formData.existStep != '') { 
                                        allInputsLast = formsInputs[formData.existStep] ? formsInputs[formData.existStep] : null;
                                        existSubFormId = formData.existStep
                                    } else {
                                        allInputsLast = formsInputs[lastSubformId["_id"]["$id"]] ? formsInputs[lastSubformId["_id"]["$id"]] : null;
                                    }
                                    let tplCtxToSend = {
                                        collection: "forms",
                                        formParentId: parentFormIdForNewStep,
                                        id: existSubFormId ? existSubFormId : lastSubformId['_id']['$id'],
                                        key: "",
                                        path: "inputs",
                                        value: allInputsLast ? {...allInputsLast,...newForm.value.inputs} :newForm.value.inputs
                                    }
                                    dataHelper.path2Value(tplCtxToSend, function() {
                                        if(Object.keys(params.value).length > 0) {
                                            allParamsLast.params ? params.value = {...allParamsLast.params, ...params.value} : '';
                                            dataHelper.path2Value(params, function() {
                                                urlCtrl.loadByHash(location.hash);
                                                dyFObj.closeForm()
                                            })
                                        } else {
                                            urlCtrl.loadByHash(location.hash);
                                            dyFObj.closeForm();
                                        }
                                    })
                                }
                            }
                        }
                    } else {
                        toastr.info('<h6 style="text-transform: none !important">Ajouter au moins une ligne dans le textarea</h6>');
                        $("#btn-submit-form").removeAttr("disabled");
                        $("#btn-submit-form").html(trad.Validate+`<i class="fa fa-arrow-circle-right"></i>`)
                    }
                } else {
                    toastr.warning('<h6 style="text-transform: none !important">Le textarea ne doit pas être vide</h6>');
                    $("#btn-submit-form").removeAttr("disabled");
                    $("#btn-submit-form").html(trad.Validate+`<i class="fa fa-arrow-circle-right"></i>`)
                }
            }
        }
    };

</script>
