
<div class="col-md-12 col-sm-12 col-xs-12 labelTitleDir margin-bottom-15">
	<i class="fa fa-file-text-o fa-2x margin-right-10"></i> <i class="fa fa-angle-down"></i> <?php echo count($forms) ?> <b>Open Forms</b> créé par <span class="Montserrat" id="name-lbl-title"><?php echo $el["name"] ?></span>
	<a href='javascript:dyFObj.openForm(oform);' class="btn btn-sm btn-link bg-green-k pull-right "><i class="fa fa-plus"></i> Nouveau Formulaire</a>
</div>

<?php 
$ct = 0;
$subFormIds = [];
foreach ($forms as $fix => $f) {
	if(isset($f["subForms"])){
		foreach ($f["subForms"] as $ix => $sfid) {
			if(!in_array($sfid , $subFormIds))
				$subFormIds[] = $sfid;
		}
		
	}
	if($ct == 0)
		echo '<div class="row ">';
?>
<div id="formline<?php echo $fix ?>" class="liForm col-md-4 col-sm-12 col-xs-12 " style="padding:5px;">
	<div class="col-xs-12 " style="padding:10px;border:1px solid grey">
		<h4 class="letter-turq ">
			<span class="col-xs-10"><?php echo (isset($f['name'])) ? $f['name'] : "Formulaire ".$el["slug"]  ?></span>
			<span class="col-xs-2"> 
				<a href="javascript:;" class="editFormBtn" data-id="<?php echo $fix ?>"><i class="fa fa-pencil text-dark"></i></a> 
				<a href="javascript:;" class="deleteFormBtn" data-id="<?php echo $fix ?>"><i class="fa fa-trash text-red"></i></a> </span></h4>


		<?php
		$activStr = "<span class='label label-danger'><i class='fa fa-thumbs-o-down'></i> Pas activé</span>";
		if( isset($f['active']) && ($f['active'] === "true" || $f['active'] === true) )  
			$activStr = "<span class='label label-success'><i class='fa fa-thumbs-o-up'></i> Activé</span>"; ?>
				
		active : <?php echo $activStr.$f['active'] ?><br/>
		steps : <br/>

		<?php 
		if(isset($f['subForms'])){
			foreach ( $f["subForms"] as $ix => $i ) {
				$countQ = ( isset($f[$i]["inputs"]) ) ? count($f[$i]["inputs"]) : 0;
				echo '<i class="fa fa-file-text-o"></i> '.$f[$i]['name']." (".$countQ." inputs)<br/>"; 	
			}
		}?>

		<a href='javascript:;' data-id='<?php echo $fix ?>' class="showAnswersBtn btn btn-xs btn-default col-md-4 col-sm-12 bold">
			<i class='fa fa-comments'></i>(<?php echo $f["answers"]; ?>) <?php echo (isset($f['what'])) ? $f['what'] : "RÉPONSES" ?>
		</a>

		<!-- <a href='javascript:;' data-id='<?php //echo $fix ?>' class="configBtn btn btn-xs btn-danger col-md-4 col-sm-12 bold">
			<i class='fa fa-cogs'></i> CONFIG
		</a> -->

		<?php if( !empty( $f['active'] ) && $f['active'] === true ) { ?>
			<a target='_blank' href='costum/co/index/slug/<?php echo $el["slug"] ?>#answer.index.id.new.form.<?php echo $fix ?>' class="btn btn-xs btn-primary col-md-4 col-sm-12 bold">
				<i class='fa fa-file-text-o'></i> RÉPONDRE
			</a> 
		<?php } else { ?>
			<a target='_blank' href='costum/co/index/slug/<?php echo $el["slug"] ?>/form/<?php echo $fix ?>#form.edit.id.<?php echo $fix ?>' class="btn btn-xs btn-primary col-md-4 col-sm-12 bold">
				<i class='fa fa-pencil'></i> CONSTRUIRE
			</a> 
		<?php } ?>
	</div>
</div>

<?php
	$ct++;
	if($ct == 3){
		echo "</div>";
		$ct = 0;
	}
 } 



 ?>


<script type="text/javascript">
var tplCtx = {};
var formsData = <?php echo (!empty($forms)) ? json_encode( $forms ) : "null"; ?>;
var subForms = <?php echo json_encode($subFormIds)?>;
jQuery(document).ready(function() {

	mylog.log("render","/modules/costum/views/tpls/forms/cplx/answers.php");
	$('.showAnswersBtn').off().on("click",function() { 
		getAjax("#central-container",baseUrl+"/survey/answer/get/form/"+$(this).data("id"),function(){
		        	
		        },"html");
	});
	// $('.configBtn').off().on("click",function() { 
	// 	tplCtx.id = $(this).data("id");
	// 	mylog.log( "configBtn", tplCtx.id );
	// 	dyFObj.openForm( oformParams, null, formsData[$(this).data("id")] )
	// });
	$('.editFormBtn').off().click( function(){
	      tplCtx.id = $(this).data("id");
	      dyFObj.openForm( oform , null, formsData[$(this).data("id")] )
	  });
	$('.deleteFormBtn').off().click( function(){
      tplCtx.id = $(this).data("id");
      bootbox.dialog({
          title: trad.confirmdelete,
          message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
          buttons: [
            {
              label: "Ok",
              className: "btn btn-primary pull-left",
              callback: function() {
                getAjax("",baseUrl+"/survey/form/delete/id/"+tplCtx.id,function(res){
                	if(res.result)
	                	toastr.success("Le form été supprimée avec succès");
	                else 
	                	toastr.error(res.msg);
	                urlCtrl.loadByHash(location.hash);
                },"html");
              }
            },
            {
              label: "Annuler",
              className: "btn btn-default pull-left",
              callback: function() {}
            }
          ]
      });
    });
	
});
var oform = {
	jsonSchema : {	
        title : "Créer un Questionnaire",
        description : "Tout est possible, faut juste poser les bonnes questions",
        icon : "fa-question",
        properties : {
        	name : { label : "Nom du formulaire"},
        	description : { label : "Description du questionnaire", inputType:"textarea"},
        	what : { label : "Nom d'une Réponse, à quoi correspondent les réponses ?", placeholder:"Proposition, Dossiers, Projets..."},
            copyable : {
                inputType : "checkboxSimple",
                label : "D'autre éléménts peuvent copier cette formulaire",
                subLabel : "D'autre éléménts peuvent copier cette formulaire",
                params : { onText : "Oui",offText : "Non",onLabel : "Oui",offLabel : "Non",
                    labelText : "Copiable"},
                checked : true },
            image : dyFInputs.image(),
            formid : { inputType : "hidden",value:"<?php echo $el["slug"].count($forms) ?>"},
            parent : {
	            inputType : "finder",
	            label : "Qui porte le questionnaire ?",
	           	multiple : true,
	           	rules : { required : true, lengthMin:[1, "parent"]}, 
    			initType: ["organizations", "projects","events","citoyens"],
    			openSearch :true
	        },
	        image : dyFInputs.image(),

                checked : true },
        	tpl : { inputType : "hidden",value:"survey.views.tpls.forms.formWizard"},
        	answersTpl : { inputType : "hidden",value:"survey.views.tpls.forms.answers"}

        },
        beforeBuild : function(){
	    	dyFObj.setMongoId('forms',function(){
	    		uploadObj.gotoUrl = '#page.type.forms.id.'+uploadObj.id;
	    	});
	    },
	    afterSave : function(){
			dyFObj.commonAfterSave();
		},
        save : function (formData) {
        	mylog.log('save tplCtx formData', formData)

        	delete formData.collection ;
            tplCtx = {
            	collection : "forms",
            	value : formData
            };
            delete tplCtx.value.formid;
            tplCtx.value.id = tplCtx.value.formid;
            mylog.log("save tplCtx",tplCtx);
            
            if(typeof tplCtx.value == "undefined")
            	toastr.error('value cannot be empty!');
            else {
                dataHelper.path2Value( tplCtx, function(params) { 
                   urlCtrl.loadByHash(location.hash);
                } );
            }

    	}
    }
};


</script>
