<?php
$paramsData = [
	"defaultValue" => "",
	"type" => $type,
	"rules" => [],
	"maxLength" => ""
];
$maxLength=(isset($input["maxLength"])) ? $input["maxLength"] : $paramsData["maxLength"];
$oninput=(!empty($maxLength) && $type!="text") ? 'oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"' : '';
if(!empty($input["defaultValue"]))
	$paramsData["defaultValue"] = $input["defaultValue"];
if(isset($input['rules']))
	$paramsData['rules'] = $input["rules"];
$value = (!empty($answers) && is_string($answers)) ? (" value='".(str_replace("'", "&#39;", $answers))."' ") : ' value="'.@$paramsData["defaultValue"].'" ';
$inpClass = "form-control";

if($type == "tags")
	$inpClass = "select2TagsInput";

if($saveOneByOne)
	$inpClass .= " saveOneByOne";

$year = date("Y");
if(!empty($parentForm["type"]) && $parentForm["type"] == "aap" && $key == "year"){
	if(!empty(Yii::app()->session["aapSession-".(string)$parentForm["_id"]])){
		$year = Yii::app()->session["aapSession-".(string)$parentForm["_id"]];
	}elseif(!empty($parentForm["actualSession"])){
		$year = $parentForm["actualSession"];
	}
}

if($mode == "r" || $mode == "pdf"){ ?>
    <div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
        <label for="<?php echo $kunik ?>">
        	<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
        		<?php echo $label ?>
        	</h4>
        </label><br/>
        <?php
			if($type=="url"){
				echo '<a href="'.$answers.'" target="_blank">'.$answers.'</a>';
			}else{
				echo $answers;
			}
		 ?>
    </div>
<?php
}else{
?>
	<div class="col-md-12 no-padding">
			<div class="form-group">
	    <label for="<?php echo $key ?>">
	    	<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>" class="<?= $canEditForm && $mode="fa" ? "" : $type ?>">
	    		<?php echo $label.$editQuestionBtn ?>
				<?php if($canEditForm){ ?>
                        <a href="javascript:;" class="btn btn-xs btn-danger config<?= $kunik ?>"><i class="fa fa-cog"></i></a>
                    <?php } ?>
	    	</h4>
	    </label>
	    <?php if(!empty($info)){ ?>
	    	<br/>
	    	<small id="<?php echo $key ?>Help" class="form-text text-muted">
	    		<?php echo $info ?>
	    	</small>
	    <?php } ?>
	    <br/>
	    <input type="<?php echo (!empty($type)) ? $type : 'text' ?>"
	    		class="<?php echo $inpClass ?>"
	    		maxlength="<?php echo $maxLength ?>"
	    		<?php echo $oninput ?>
	    		id="<?php echo $key ?>"
	    		aria-describedby="<?php echo $key ?>Help"
	    		placeholder="<?php echo (isset($placeholder)) ? $placeholder : '' ?>"
	    		data-form='<?php echo $form["id"] ?>'  <?php echo @$value ?>
	    />
	    <div class="rules-container<?php echo $key ?>" data-rules="<?php echo str_replace('"', "'", json_encode($paramsData['rules'])) ?>">

		</div>
	</div>

	</div>

<?php } ?>


<?php if($mode != "pdf"){ ?>
<script type="text/javascript">

	jQuery(document).ready(function() {
        /*var wsCO = null

        var socketConfigurationEnabled = coWsConfig.enable && coWsConfig.serverUrl && coWsConfig.pingRefreshCoformAnswerUrl;
	    if(socketConfigurationEnabled){
                wsCO = io(coWsConfig.serverUrl, {
                    query:{
                        answerId: answerId
                    }
                });

                wsCO.on('refresh-coform-answer', function(){
                    var newData = {};
                    ajaxPost(
                        '',
                        baseUrl + "/co2/element/get/type/answers/id/" + answerId,
                        null,
                        function (data) {
                            if (
                                typeof data.map != "undefined" &&
                                typeof data.map.answers != "undefined"
                            ){
                                newData =  data.map.answers;
                            }

                        },
                        null,
                        null,
                        {"async" : false}
                    );
                });
        }*/

	   mylog.log("render form input","/modules/costum/views/tpls/forms/text.php");
	   $('.config<?= $kunik ?>').off().on('click',function(){
			var config<?=$kunik ?>ParamsData = <?= json_encode($paramsData) ?>;
			var config<?=$kunik ?>Params = {
				"jsonSchema" : {
					"title" : "<?php echo $label != '' ? $label : $key ?> config",
					"icon" : "cog",
					"properties" : {
						defaultValue : {
							inputType : "text",
							label : tradSettings.defaultValue,
						},
						type : {
							inputType : "select",
							label : "Type",
							options : {
								"text":"text",
								"hidden" : "hidden",
								"number" : "number",
								"date" : "date",
							},
							init : function() {
								$('div.typeselect select#type').on('change', function() {
									if($(this).val() != 'date' && $(this).val() != 'hidden') {
										$('div.forcustom,div.minWidthcustom,div.maxWidthcustom,div.regexpcustom').show()
									} else {
										$('div.forcustom,div.minWidthcustom,div.maxWidthcustom,div.regexpcustom').hide()
									}
								})
							}
						},
						"for":{
			            	inputType : "custom",
			            	html : "<h4> Règle de syntaxe </h4>",
							init : function() {
								if($('div.typeselect select#type').val() == 'date' || $('div.typeselect select#type').val() == 'hidden') {
									$('div.forcustom').hide()
								}
							}
			          	},
						minWidth : {
							inputType : "custom",
							html : `
									<label class="col-xs-12 text-left control-label no-padding" for="minWidth">
										<i class="fa fa-chevron-down"></i> ${trad.minCharacters ? trad.minCharacters.charAt(0).toUpperCase() + trad.minCharacters.substring(1) : 'Caractères minimum'}
									</label>
									<input type="number" class="form-control " name="minWidth" id="minWidth" value="" placeholder="">`,
							init : function() {
								$(`div.minWidthcustom input#minWidth`).val('<?php echo isset($paramsData['rules']['width']['min']) ? $paramsData['rules']['width']['min'] : 0 ?>')
								if($('div.typeselect select#type').val() == 'date' || $('div.typeselect select#type').val() == 'hidden') {
									$('div.minWidthcustom').hide()
								}
							}
						},
						maxWidth : {
							inputType : "custom",
							html : `
									<label class="col-xs-12 text-left control-label no-padding" for="maxWidth">
										<i class="fa fa-chevron-down"></i> ${trad.maxCharacters ? trad.maxCharacters.charAt(0).toUpperCase() + trad.maxCharacters.substring(1) : 'Caractères maximum'}
									</label>
									<input type="number" class="form-control " name="maxWidth" id="maxWidth" value="" placeholder="">`,
							init : function() {
								$(`div.maxWidthcustom input#maxWidth`).val('<?php echo isset($paramsData['rules']['width']['max']) ? $paramsData['rules']['width']['max'] : 0 ?>')
								if($('div.typeselect select#type').val() == 'date' || $('div.typeselect select#type').val() == 'hidden') {
									$('div.maxWidthcustom').hide()
								}
							}
						},
						regexp : {
							inputType : "custom",
							html : `
								<label class="col-xs-12 text-left control-label no-padding" for="regexp">
									<i class="fa fa-chevron-down"></i> ${trad.CharacterType}</label>
									<select class="form-control valid" placeholder="undefined" name="regexp" id="regexp" style="width: 100%;height:auto;" data-placeholder="">
										<option></option>
										<option value="chiffre" data-value="" data-regex="^[0-9]+$">${trad.Numbers}</option>
										<option value="chiffrewithcomma" data-value="" data-regex="^-?[0-9]+(,[0-9]+)?$">${trad["Numbers with or without comma"]}</option>

									</select>
							`,
							//<option value="special" data-value="">Complexe</option> more option
							init : function(){
								var selectedOption = $(`#regexp option[data-regex='<?php echo isset($paramsData['rules']['syntaxe']['regexp']) ? $paramsData['rules']['syntaxe']['regexp'] : json_encode([])?>']`);
								selectedOption.attr('selected', true);
								$("div.regexpcustom select#regexp").attr('data-regex', '<?php echo isset($paramsData['rules']['syntaxe']['regexp']) ? $paramsData['rules']['syntaxe']['regexp'] : json_encode([])?>');
								$("div.regexpcustom select#regexp").val(selectedOption.val())
								$("div.regexpcustom select#regexp").on("change", function() {
									$(this).attr('data-regex', $(`#regexp option[value='${$(this).val()}']`).attr('data-regex'));
									if($(this).val() == 'special') {
										$('div.regexpsyntaxetext,div.errormsgtext').show()
									} else {
										$('div.regexpsyntaxetext,div.errormsgtext').hide()
									}
								});
								if($('div.typeselect select#type').val() == 'date' || $('div.typeselect select#type').val() == 'hidden') {
									$('div.regexpcustom').hide()
								}
							}
						},
						regexpsyntaxe : {
							inputType : "text",
							label : 'Regular expression (RegExp)',
							init : function() {
								$('div.regexpsyntaxetext').hide()
							}
						},
						errormsg : {
							inputType : "text",
							label : trad.ErrorMessage,
							init : function() {
								$('div.errormsgtext').hide()
							}
						},
					},
					save : function () {
						var collec = <?php echo json_encode((isset($parentForm) && isset($parentForm["type"]) && $parentForm["type"]=="aap") ? Form::INPUTS_COLLECTION : Form::COLLECTION )?>;
						var tplCtx = {
							id : "<?= @$input["docinputsid"] ?>",
							path : "inputs.<?= $key ?>",
							collection : collec,
							value : {},
							formParentId : <?= json_encode(isset($parentForm) && isset($parentForm['_id']) ? (String) $parentForm['_id'] : 'null') ?>,
							updatePartial : true
						};
						var errormsg = 'NumbersOnly';

						$.each( config<?=$kunik ?>Params.jsonSchema.properties , function(k,val) {
							if(val.inputType == "properties")
								tplCtx.value = getPairsObj('.'+k+val.inputType);
							else if(val.inputType == "array")
								tplCtx.value[k] = getArray('.'+k+val.inputType);
							else if(val.inputType == "formLocality")
								tplCtx.value[k] = getArray('.'+k+val.inputType);
							else if(k == 'regexp' && $("#"+k).val() != '' && $("#"+k).val() != 'special') {
								switch ($("#"+k).val()) {
									case 'email':
										errormsg = 'syntaxe invalid';
										break;
									case 'chiffre':
										errormsg = 'NumbersOnly';
										break;
									case 'chiffrewithcomma':
										errormsg = 'NumbersWoWoutCommaOnly';
										break;
									default:
										errormsg = 'syntaxe invalid';
										break;
								}
								tplCtx.value[k] = $("#"+k).attr('data-regex')
							} else
								tplCtx.value[k] = $("#"+k).val();
						});
						tplCtx.value.rules = {};
						if(tplCtx.value && typeof tplCtx.value.minWidth != 'undefined' && typeof tplCtx.value.maxWidth != 'undefined') {
							tplCtx.value.rules['width'] = {
								min : tplCtx.value.minWidth*1,
								max : tplCtx.value.maxWidth*1
							}
						}
						if(tplCtx.value && typeof tplCtx.value.regexp != 'undefined' && tplCtx.value.regexp != 'special') {
							tplCtx.value.rules['syntaxe'] = {
								regexp : tplCtx.value.regexp,
								errormsg : errormsg
							}
						}
						if(tplCtx.value && typeof tplCtx.value.regexp != 'undefined' && tplCtx.value.regexp == 'special') {
							tplCtx.value.rules['syntaxe'] = {
								regexp : tplCtx.value.regexpsyntaxe,
								errormsg : tplCtx.value.errormsg
							}
						}
						typeof tplCtx.value.minWidth != 'undefined' ? delete tplCtx.value.minWidth : '';
						typeof tplCtx.value.maxWidth != 'undefined' ? delete tplCtx.value.maxWidth : '';
						typeof tplCtx.value.regexp != 'undefined' ? delete tplCtx.value.regexp : '';
						typeof tplCtx.value.regexpsyntaxe != 'undefined' ? delete tplCtx.value.regexpsyntaxe : '';
						typeof tplCtx.value.errormsg != 'undefined' ? delete tplCtx.value.errormsg : '';
						mylog.log("save tplCtx",tplCtx);

						if(typeof tplCtx.value == "undefined")
							toastr.error('value cannot be empty!');
						else {
							dataHelper.path2Value( tplCtx, function(params) {
								dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
								reloadInput("<?= $key ?>", "<?= (string)$form["id"] ?>");
							} );
						}

					}
				}
			};
			dyFObj.openForm(config<?=$kunik ?>Params,null,config<?=$kunik ?>ParamsData);
		});


		if("<?= $type?>" == "hidden"){
			if("<?= $key?>" == "year"){
				var answerPath<?= $key?> = <?= json_encode(rtrim($answerPath,'.'))?>;
				var answers<?= $key?> = <?= json_encode($answer) ?>;
				var currentYear<?= $key?> =  jsonHelper.getValueByPath(answers<?= $key?>,answerPath<?= $key?>);
				if(typeof currentYear<?= $key?> == "undefined"){
					setTimeout(() => {
						$("#<?= $key?>").val("<?= $year; ?>").focus().blur();
					}, 900);
				}
			}else{
				setTimeout(() => {
					$("#<?= $key?>").val("<?= $paramsData["defaultValue"] ?>").focus().blur();
				}, 900);
			}
		}
	});
</script>
<?php } ?>

