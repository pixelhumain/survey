<?php if($answer){
    ?>
    <div class="form-group">
        
            <?php

$re = "";
$cr = "";
$lb = "";

if(isset($answer["answers"]["sommomForm2"])){
 foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
        if (isset($v["plageHoraireReg"])){
            $re = $v["plageHoraireReg"];
        }
        if (isset($v["plageHoraireCrt"])){
         $cr = $v["plageHoraireCrt"];
        }
        if (isset($v["plageHoraireLbl"])){
         $lb = $v["plageHoraireLbl"];
        }
     }
}
}

$editBtnReg = "";
$editBtnCrt = "";
$editBtnLbl = "";
            if ($re == ""){
                $editBtnReg = ($canEdit) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik."Reg btn btn-default'><i class='fa fa-plus'></i> Ajouter Réglementation </a>" : "";
            }
            if ($cr == ""){
            $editBtnCrt = ($canEdit) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik."Crt btn btn-default'><i class='fa fa-plus'></i> Ajouter Charte </a>" : "";
            }
            if ($lb == ""){
                $editBtnLbl = ($canEdit) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik."Lbl btn btn-default'><i class='fa fa-plus'></i> Ajouter Label </a>" : "";
            }
            $editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

            $paramsData = [
                "paramsP" => [
                        "onText" => "oui",
                        "offText" => "Non",
                        "onLabel" => "Oui",
                        "offLabel" => "Non",
                        "labelText" => "Concerne les cétacés"
                ],
                "moteur" => [
                    "Allumé" => "Allumé",
                    "Eteint" => "Eteint",
                ]
            ];
            if( isset($parentForm["params"][$kunik]["statut"]) )
                $paramsData["statut"] =  $parentForm["params"][$kunik]["statut"];

            $propertiesReglementation = [
                "plageHoraireReg" => [
                    "label" => "Plages horaires de pratique",
                    "placeholder" => "Plages horaires de pratique",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "navireObsReg" => [
                    "label" => "Nombre de navires en observation",
                    "placeholder" => "Nombre de navires en observation",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "distanceReg" => [
                    "label" => "Distance d’approche + espèce",
                    "placeholder" => "Distance d’approche + espèce",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "stadeDevReg" => [
                    "label" => "Spécificité par stade de développement (juvénile, …)",
                    "placeholder" => "Spécificité par stade de développement (juvénile, …)",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "specifReg" => [
                    "label" => "Spécificité par type de groupe (mère/baleineau, groupe actif…)",
                    "placeholder" => "Spécificité par type de groupe (mère/baleineau, groupe actif…)",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "specifZoneReg" => [
                    "label" => "Spécificité par zone (sanctuaires…)",
                    "placeholder" => "Spécificité par zone (sanctuaires…)",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "limiteTempsReg" => [
                    "label" => "Limite temps d’observation",
                    "placeholder" => "Limite temps d’observation",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "navireReg" => [
                    "label" => "Regroupement des navires (oui/non)",
                    "placeholder" => "Regroupement des navires (oui/non)",
                    "inputType" => "checkboxSimple",
                    "params" => $paramsData["paramsP"],
                    "rules" => [ "required" => true ]
                ],
                "moteurReg" => [
                    "label" => "Moteur (allumé ou éteint)",
                    "placeholder" => "Moteur (allumé ou éteint)",
                    "inputType" => "select",
                    "noOrder" => true,
                    "options" => $paramsData["moteur"],
                    "rules" => [ "required" => true ]
                ],
                "equipReg" => [
                    "label" => "Contrainte Équipements électroniques sous-marins",
                    "placeholder" => "Contrainte Équipements électroniques sous-marins",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "protocolSupReg" => [
                    "label" => "Protocole supplémentaire",
                    "placeholder" => "Protocole supplémentaire",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "comReg" => [
                    "label" => "Protocole de communication entre observateurs",
                    "placeholder" => "Protocole de communication entre observateurs",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ]

            ];

            $propertiesCharte = [
                "plageHoraireCrt" => [
                    "label" => "Plages horaires de pratique",
                    "placeholder" => "Plages horaires de pratique",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "navireObsCrt" => [
                    "label" => "Nombre de navires en observation",
                    "placeholder" => "Nombre de navires en observation",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "distanceCrt" => [
                    "label" => "Distance d’approche + espèce",
                    "placeholder" => "Distance d’approche + espèce",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "stadeDevCrt" => [
                    "label" => "Spécificité par stade de développement (juvénile, …)",
                    "placeholder" => "Spécificité par stade de développement (juvénile, …)",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "specifCrt" => [
                    "label" => "Spécificité par type de groupe (mère/baleineau, groupe actif…)",
                    "placeholder" => "Spécificité par type de groupe (mère/baleineau, groupe actif…)",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "specifZoneCrt" => [
                    "label" => "Spécificité par zone (sanctuaires…)",
                    "placeholder" => "Spécificité par zone (sanctuaires…)",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "limiteTempsCrt" => [
                    "label" => "Limite temps d’observation",
                    "placeholder" => "Limite temps d’observation",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "navireCrt" => [
                    "label" => "Regroupement des navires (oui/non)",
                    "placeholder" => "Regroupement des navires (oui/non)",
                    "inputType" => "checkboxSimple",
                    "params" => $paramsData["paramsP"],
                    "rules" => [ "required" => true ]
                ],
                "moteurCrt" => [
                    "label" => "Moteur (allumé ou éteint)",
                    "placeholder" => "Moteur (allumé ou éteint)",
                    "inputType" => "select",
                    "noOrder" => true,
                    "options" => $paramsData["moteur"],
                    "rules" => [ "required" => true ]
                ],
                "equipCrt" => [
                    "label" => "Contrainte Équipements électroniques sous-marins",
                    "placeholder" => "Contrainte Équipements électroniques sous-marins",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "protocolSupCrt" => [
                    "label" => "Protocole supplémentaire",
                    "placeholder" => "Protocole supplémentaire",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "comCrt" => [
                    "label" => "Protocole de communication entre observateurs",
                    "placeholder" => "Protocole de communication entre observateurs",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ]
            ];

            $propertiesLabel = [
                "plageHoraireLbl" => [
                    "label" => "Plages horaires de pratique",
                    "placeholder" => "Plages horaires de pratique",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "navireObsLbl" => [
                    "label" => "Nombre de navires en observation",
                    "placeholder" => "Nombre de navires en observation",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "distanceLbl" => [
                    "label" => "Distance d’approche + espèce",
                    "placeholder" => "Distance d’approche + espèce",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "stadeDevLbl" => [
                    "label" => "Spécificité par stade de développement (juvénile, …)",
                    "placeholder" => "Spécificité par stade de développement (juvénile, …)",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "specifLbl" => [
                    "label" => "Spécificité par type de groupe (mère/baleineau, groupe actif…)",
                    "placeholder" => "Spécificité par type de groupe (mère/baleineau, groupe actif…)",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "specifZoneLbl" => [
                    "label" => "Spécificité par zone (sanctuaires…)",
                    "placeholder" => "Spécificité par zone (sanctuaires…)",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "limiteTempsLbl" => [
                    "label" => "Limite temps d’observation",
                    "placeholder" => "Limite temps d’observation",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "navireLbl" => [
                    "label" => "Regroupement des navires (oui/non)",
                    "placeholder" => "Regroupement des navires (oui/non)",
                    "inputType" => "checkboxSimple",
                    "params" => $paramsData["paramsP"],
                    "rules" => [ "required" => true ]
                ],
                "moteurLbl" => [
                    "label" => "Moteur (allumé ou éteint)",
                    "placeholder" => "Moteur (allumé ou éteint)",
                    "inputType" => "select",
                    "noOrder" => true,
                    "options" => $paramsData["moteur"],
                    "rules" => [ "required" => true ]
                ],
                "equipLbl" => [
                    "label" => "Contrainte Équipements électroniques sous-marins",
                    "placeholder" => "Contrainte Équipements électroniques sous-marins",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "protocolSupLbl" => [
                    "label" => "Protocole supplémentaire",
                    "placeholder" => "Protocole supplémentaire",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "comLbl" => [
                    "label" => "Protocole de communication entre observateurs",
                    "placeholder" => "Protocole de communication entre observateurs",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ]
            ];

            $propertiesParams = [
                "labels"=>[],
                "placeholders"=>[],
            ];

            foreach ($propertiesLabel as $k => $v) {
                if( isset($v["label"])){
                    if( isset($parentForm["params"][$kunik]["labels"][$k]) )
                        $properties[$k]["label"] = $parentForm["params"][$kunik]["labels"][$k];
                    $propertiesParams["labels"][$k] = $propertiesLabel[$k]["label"];
                }

                if( isset($v["placeholder"])){
                    if(isset($parentForm["params"][$kunik]["placeholders"][$k]) )
                       $properties[$k]["placeholder"] = $parentForm["params"][$kunik]["placeholders"][$k];
                    $propertiesParams["placeholders"][$k] = $propertiesLabel[$k]["placeholder"];
                }
            }

            foreach ($propertiesCharte as $k => $v) {
                if( isset($v["label"])){
                    if( isset($parentForm["params"][$kunik]["labels"][$k]) )
                        $properties[$k]["label"] = $parentForm["params"][$kunik]["labels"][$k];
                    $propertiesParams["labels"][$k] = $propertiesCharte[$k]["label"];
                }

                if( isset($v["placeholder"])){
                    if(isset($parentForm["params"][$kunik]["placeholders"][$k]) )
                       $properties[$k]["placeholder"] = $parentForm["params"][$kunik]["placeholders"][$k];
                    $propertiesParams["placeholders"][$k] = $propertiesCharte[$k]["placeholder"];
                }
            }

            foreach ($propertiesReglementation as $k => $v) {
                if( isset($v["label"])){
                    if( isset($parentForm["params"][$kunik]["labels"][$k]) )
                        $properties[$k]["label"] = $parentForm["params"][$kunik]["labels"][$k];
                    $propertiesParams["labels"][$k] = $propertiesReglementation[$k]["label"];
                }

                if( isset($v["placeholder"])){
                    if(isset($parentForm["params"][$kunik]["placeholders"][$k]) )
                       $properties[$k]["placeholder"] = $parentForm["params"][$kunik]["placeholders"][$k];
                    $propertiesParams["placeholders"][$k] = $propertiesReglementation[$k]["placeholder"];
                }
            }
            ?>

            <?php if(  $mode != "pdf" ){ ?>

            <table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>" style="overflow-x: auto">
            <thead>
            <tr>
                <td colspan='<?php echo count( $propertiesReglementation)+2?>' ><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnReg.$editBtnCrt.$editBtnLbl?></h4>
                    <?php echo $info ?>
                </td>
            </tr>
           
            </thead>
           <?php if(isset($answer["answers"]["sommomForm2"]["recommendationsommomForm26"])) { ?>
                <tbody class="directoryLines">
            <tr>
                    <th>Recommandations</th>
                    <th>Réglementation</th>
                    <th>Charte</th>
                    <th>Label</th>
            </tr>
            <tr>
                    <th>Plages horaires de pratique</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                         foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                                if (isset($v["plageHoraireReg"])){
                                    echo $v["plageHoraireReg"];
                                }
                             }
                        }
                     
                    ?>
                    </td>
                    <td>
                    <?php 
                      
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                            if (isset($v["plageHoraireCrt"])){
                                echo $v["plageHoraireCrt"];
                            }
                         }
                    }
                     
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                         foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                                if (isset($v["plageHoraireLbl"])){
                                    echo $v["plageHoraireLbl"];
                                }
                             }
                        }
                        
                    ?>
                    </td>
            </tr>
            <tr>
                    <th>Nombre de navires en observation</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                            if (isset($v["navireObsReg"])){
                                echo $v["navireObsReg"];
                            }
                         }
                     }
                       
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                        
                             foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                                    if (isset($v["navireObsCrt"])){
                                        echo $v["navireObsCrt"];
                                    }
                            }
                        }
                        
                     
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                         foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["navireObsLbl"])){
                            echo $v["navireObsLbl"];
                        }
                     }
                    }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th>Distance d’approche + espèce</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["distanceReg"])){
                            echo $v["distanceReg"];
                        }}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["distanceCrt"])){
                            echo $v["distanceCrt"];
                        }}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["distanceLbl"])){
                            echo $v["distanceLbl"];
                        }}
                     }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th>Spécificité par stade de développement (juvénile, …)</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["stadeDevReg"])){
                            echo $v["stadeDevReg"];
                        }}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["stadeDevCrt"])){
                            echo $v["stadeDevCrt"];
                        }}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["stadeDevLbl"])){
                            echo $v["stadeDevLbl"];
                        }}
                     }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th>Spécificité par type de groupe (mère/baleineau, groupe actif…)</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["specifReg"])){
                            echo $v["specifReg"];
                        }}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["specifCrt"])){
                            echo $v["specifCrt"];
                        }}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["specifLbl"])){
                            echo $v["specifLbl"];
                        }}
                     }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th>Spécificité par zone (sanctuaires…)</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["specifZoneReg"])){
                            echo $v["specifZoneReg"];
                        }}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["specifZoneCrt"])){
                            echo $v["specifZoneCrt"];
                        }}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["specifZoneLbl"])){
                            echo $v["specifZoneLbl"];
                        }}
                     }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th>Limite temps d’observation</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["limiteTempsReg"])){
                            echo $v["limiteTempsReg"];
                        }}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["limiteTempsCrt"])){
                            echo $v["limiteTempsCrt"];
                        }}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["limiteTempsLbl"])){
                            echo $v["limiteTempsLbl"];
                        }}
                     }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th>Regroupement des navires (oui/non)</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["navireReg"])){
                            echo $v["navireReg"];
                        }}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["navireCrt"])){
                            echo $v["navireCrt"];
                        }}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["navireLbl"])){
                            echo $v["navireLbl"];
                        }}
                     }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th>Moteur (allumé ou éteint)</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["moteurReg"])){
                            echo $v["moteurReg"];
                        }}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["moteurCrt"])){
                            echo $v["moteurCrt"];
                        }}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["moteurLbl"])){
                            echo $v["moteurLbl"];
                        }}
                     }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th>Contrainte Équipements électroniques sous-marins</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["equipReg"])){
                            echo $v["equipReg"];
                        }}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["equipCrt"])){
                            echo $v["equipCrt"];
                        }}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["equipLbl"])){
                            echo $v["equipLbl"];
                        }}
                     }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th>Protocole supplémentaire</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["protocolSupReg"])){
                            echo $v["protocolSupReg"];
                        }}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["protocolSupCrt"])){
                            echo $v["protocolSupCrt"];
                        }}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["protocolSupLbl"])){
                            echo $v["protocolSupLbl"];
                        }}
                     }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th>Protocole de communication entre observateurs</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["comReg"])){
                            echo $v["comReg"];
                        }}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["comCrt"])){
                            echo $v["comCrt"];
                        }}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationsommomForm26"]);
                      foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["comLbl"])){
                            echo $v["comLbl"];
                        }}
                     }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th></th>
                    <td style="white-space: normal;">
                    <?php
                        if(isset($answers)){
                            foreach ($answers as $q => $a) {
                                if(isset($a["comReg"])){
                    ?>

                                    <?php
                                    echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
                                        "canEdit"=>($canEdit || $canEdit),
                                        "id" => $answer["_id"],
                                        "collection" => Form::ANSWER_COLLECTION,
                                        "q" => $q,
                                        "path" => $answerPath.$q,
                                        "keyTpl"=>$kunik,
                                        "type" => "reg"
                                    ] ); ?>

                                    <a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
                        

                        <?php
                            }
                        }
                    }
                        ?>
                    </td>
                    <td style="white-space: normal;">
                    <?php
                        if(isset($answers)){
                            foreach ($answers as $q => $a) {
                                if(isset($a["comCrt"])){
                    ?>
                                
                                    <?php
                                    echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
                                        "canEdit"=>($canEdit || $canEdit),
                                        "id" => $answer["_id"],
                                        "collection" => Form::ANSWER_COLLECTION,
                                        "q" => $q,
                                        "path" => $answerPath.$q,
                                        "keyTpl"=>$kunik,
                                        "type" => "crt"
                                    ] ); ?>
            
                                    <a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
                               

                                <?php
                            }
                        }
                    }
                        ?>
                    </td>  <td style="white-space: normal;">
                    <?php
                        if(isset($answers)){
                            foreach ($answers as $q => $a) {
                                if(isset($a["comLbl"])){
                    ?>
                                
                                    <?php
                                    echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
                                        "canEdit"=>($canEdit || $canEdit),
                                        "id" => $answer["_id"],
                                        "collection" => Form::ANSWER_COLLECTION,
                                        "q" => $q,
                                        "path" => $answerPath.$q,
                                        "keyTpl"=>$kunik,
                                        "type" => "lbl"
                                    ] ); ?>
            
                                    <a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
                               

                                <?php
                            }
                        }
                    }
                        ?>
                    </td>

            </tr>
           
            </tbody>
         <?php   } ?>
            
        </table>

        <?php
            } else {
            ?>
            <div>
                <label>
                    <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
                                <?php echo $info ?>
                </label>
            </div>


<?php } ?>
            
    </div>
    <?php  if( $mode != "pdf"){?>
    <script type="text/javascript">

        var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsProperty = {
            labels : <?php echo json_encode( $propertiesParams["labels"] ); ?>,
            placeholders : <?php echo json_encode( $propertiesParams["placeholders"] ); ?>
        };


        var max<?php echo $kunik ?> = 0;
        var nextValuekey<?php echo $kunik ?> = 0;

        if(notNull(<?php echo $kunik ?>Data)){
            $.each(<?php echo $kunik ?>Data, function(ind, val){
                if(parseInt(ind) > max<?php echo $kunik ?>){
                    max<?php echo $kunik ?> = parseInt(ind);
                }
            });

            nextValuekey<?php echo $kunik ?> = max<?php echo $kunik ?> + 1;

        }


        $(document).ready(function() {

            sectionDyf.<?php echo $kunik ?>Reg = {
                "jsonSchema" : {
                    "title" : "Réglementation",
                    "icon" : "fa-globe",
                    "text" : "",
                    "properties" : <?php echo json_encode( $propertiesReglementation ); ?>,
                    save : function () {
                        var today = new Date();
                        tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
                        $.each( sectionDyf.<?php echo $kunik ?>Reg.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                        });

                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");//$("#ajax-modal").modal('hide');
                                location.reload();
                            } );
                        }

                    }
                }
            };

            sectionDyf.<?php echo $kunik ?>Crt = {
                "jsonSchema" : {
                    "title" : "Charte",
                    "icon" : "fa-globe",
                    "text" : "",
                    "properties" : <?php echo json_encode( $propertiesCharte ); ?>,
                    save : function () {
                        var today = new Date();
                        tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
                        $.each( sectionDyf.<?php echo $kunik ?>Crt.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                        });

                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");//$("#ajax-modal").modal('hide');
                                location.reload();
                            } );
                        }

                    }
                }
            };

            sectionDyf.<?php echo $kunik ?>Lbl = {
                "jsonSchema" : {
                    "title" : "Label",
                    "icon" : "fa-globe",
                    "text" : "",
                    "properties" : <?php echo json_encode( $propertiesLabel ); ?>,
                    save : function () {
                        var today = new Date();
                        tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
                        $.each( sectionDyf.<?php echo $kunik ?>Lbl.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                        });

                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");//$("#ajax-modal").modal('hide');
                                location.reload();
                            } );
                        }

                    }
                }
            };

            sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema" : {
                    "title" : "<?php echo $kunik ?> config",
                    "description" : "Liste de question possible",
                    "icon" : "fa-cog",
                    "properties" : {
                        labels : {
                            inputType : "properties",
                            labelKey : "Clef",
                            labelValue : "Libellé du champs",
                            label : "Modifier les libélés des Questions",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsProperty.labels
                        },
                        placeholders : {
                            inputType : "properties",
                            labelKey : "Clef",
                            labelValue : "Texte dans le champs",
                            label : "Modifier les textes à l'interieur du champs de saisie",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsProperty.placeholders
                        }
                        
                    },
                    save : function () {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                            if(val.inputType == "properties")
                                tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                            else if(val.inputType == "array")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else
                                tplCtx.value[k] = $("#"+k).val();
                            mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                        });
                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
 

$("#ajax-modal").modal('hide');
		urlCtrl.loadByHash( location.hash );

                            } );
                        }

                    }
                }
            };


            mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

            //adds a line into answer
            $(".add<?php echo $kunik ?>Reg").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+nextValuekey<?php echo $kunik ?>;
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Reg );
            });

            $(".add<?php echo $kunik ?>Crt").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+nextValuekey<?php echo $kunik ?>;
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Crt );
            });

            $(".add<?php echo $kunik ?>Lbl").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+nextValuekey<?php echo $kunik ?>;
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Lbl );
            });
            
            $(".edit<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                if($(this).data("type") == "reg"){
                    dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Reg,null, <?php echo $kunik ?>Data[$(this).data("key")]);
                } else if ($(this).data("type")== "crt") {
                    dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Crt,null, <?php echo $kunik ?>Data[$(this).data("key")]);
                } else if ($(this).data("type") == "lbl") {
                    dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Lbl,null, <?php echo $kunik ?>Data[$(this).data("key")]);
                }
            });

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                //if no params config on the element.costum.form.params.<?php echo $kunik ?>
                //then we load default values available in forms.inputs.<?php echo $kunik ?>xxx.params
                //mylog.log(".editParams",sectionDyf.<?php echo $kunik ?>Params,calData);
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            });


        });
    </script>
<?php } 
} ?>