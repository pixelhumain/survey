<?php if($answer){
    ?>
    <div class="form-group">
        

            <?php
$re = "";
$cr = "";
$lb = "";

if(isset($answer["answers"]["sommomForm2"])){
    foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
        if (isset($v["aprochReg"])){
            $re = $v["aprochReg"];
        }
        if (isset($v["aprochCrt"])){
         $cr = $v["aprochCrt"];
        }
        if (isset($v["aprochLbl"])){
         $lb = $v["aprochLbl"];
        }
     }
 }
}

$editBtnReg = "";
$editBtnCrt = "";
$editBtnLbl = "";
            if ($re == ""){
                $editBtnReg = ($canEdit) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik."Reg btn btn-default'><i class='fa fa-plus'></i> Ajouter Réglementation </a>" : "";
            }
            if ($cr == ""){
            $editBtnCrt = ($canEdit) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik."Crt btn btn-default'><i class='fa fa-plus'></i> Ajouter Charte </a>" : "";
            }
            if ($lb == ""){
                $editBtnLbl = ($canEdit) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik."Lbl btn btn-default'><i class='fa fa-plus'></i> Ajouter Label </a>" : "";
            }
            $editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

            $paramsData = [
                "paramsP" => [
                        "onText" => "oui",
                        "offText" => "Non",
                        "onLabel" => "Oui",
                        "offLabel" => "Non",
                        "labelText" => "Concerne les cétacés"
                ],
                "moteur" => [
                    "Allumé" => "Allumé",
                    "Eteint" => "Eteint",
                ]
            ];
            if( isset($parentForm["params"][$kunik]["statut"]) )
                $paramsData["statut"] =  $parentForm["params"][$kunik]["statut"];

            $propertiesReglementation = [
                "aprochReg" => [
                    "label" => "Approche (interdite, tolérée, autorisée)",
                    "placeholder" => "Approche (interdite, tolérée, autorisée)",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "equipReg" => [
                    "label" => "Équipement",
                    "placeholder" => "Équipement",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "nageurReg" => [
                    "label" => "Nombre de nageurs maximum",
                    "placeholder" => "Nombre de nageurs maximum",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "regNagReg" => [
                    "label" => "Regroupement des nageurs",
                    "placeholder" => "Regroupement des nageurs",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "specEspReg" => [
                    "label" => "Spécificités par espèce",
                    "placeholder" => "Spécificités par espèce",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "devSpecReg" => [
                    "label" => "Stade de développement spécifique",
                    "placeholder" => "Stade de développement spécifique",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "passiviteReg" => [
                    "label" => "Passivité des nageurs",
                    "placeholder" => "Passivité des nageurs",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]

                ],
                "apneeReg" => [
                    "label" => "Apnées",
                    "placeholder" => "Apnées",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "disApReg" => [
                    "label" => "Distance d’approche minimale, Zone d’exclusion",
                    "placeholder" => "Distance d’approche minimale, Zone d’exclusion",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "vigZoneReg" => [
                    "label" => "Zone de vigilance",
                    "placeholder" => "Zone de vigilance",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "tmpsObReg" => [
                    "label" => "Temps d’observation",
                    "placeholder" => "Temps d’observation",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "zoneSpecReg" => [
                    "label" => "Zones spécifiques",
                    "placeholder" => "Zones spécifiques",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "nbrBatmaxReg" => [
                    "label" => "Nombre de bateaux maximum pour la pratique",
                    "placeholder" => "Nombre de bateaux maximum pour la pratique",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "contactReg" => [
                    "label" => "Contact avec les animaux",
                    "placeholder" => "Contact avec les animaux",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "protocoleReg" => [
                    "label" => "Protocole spécifique existant",
                    "placeholder" => "Protocole spécifique existant",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "evalRisqReg" => [
                    "label" => "Évaluation des risques liés à l’activité",
                    "placeholder" => "Évaluation des risques liés à l’activité",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ]

            ];

            $propertiesCharte = [
                "aprochCrt" => [
                    "label" => "Approche (interdite, tolérée, autorisée)",
                    "placeholder" => "Approche (interdite, tolérée, autorisée)",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "equipCrt" => [
                    "label" => "Équipement",
                    "placeholder" => "Équipement",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "nageurCrt" => [
                    "label" => "Nombre de nageurs maximum",
                    "placeholder" => "Nombre de nageurs maximum",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "regNagCrt" => [
                    "label" => "Regroupement des nageurs",
                    "placeholder" => "Regroupement des nageurs",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "specEspCrt" => [
                    "label" => "Spécificités par espèce",
                    "placeholder" => "Spécificités par espèce",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "devSpecCrt" => [
                    "label" => "Stade de développement spécifique",
                    "placeholder" => "Stade de développement spécifique",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "passiviteCrt" => [
                    "label" => "Passivité des nageurs",
                    "placeholder" => "Passivité des nageurs",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "apneeCrt" => [
                    "label" => "Apnées",
                    "placeholder" => "Apnées",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "disApCrt" => [
                    "label" => "Distance d’approche minimale, Zone d’exclusion",
                    "placeholder" => "Distance d’approche minimale, Zone d’exclusion",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "vigZoneCrt" => [
                    "label" => "Zone de vigilance",
                    "placeholder" => "Zone de vigilance",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "tmpsObCrt" => [
                    "label" => "Temps d’observation",
                    "placeholder" => "Temps d’observation",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "zoneSpecCrt" => [
                    "label" => "Zones spécifiques",
                    "placeholder" => "Zones spécifiques",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "nbrBatmaxCrt" => [
                    "label" => "Nombre de bateaux maximum pour la pratique",
                    "placeholder" => "Nombre de bateaux maximum pour la pratique",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "contactCrt" => [
                    "label" => "Contact avec les animaux",
                    "placeholder" => "Contact avec les animaux",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "protocoleCrt" => [
                    "label" => "Protocole spécifique existant",
                    "placeholder" => "Protocole spécifique existant",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "evalRisqCtr" => [
                    "label" => "Évaluation des risques liés à l’activité",
                    "placeholder" => "Évaluation des risques liés à l’activité",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ]
            ];

            $propertiesLabel = [
                "aprochLbl" => [
                    "label" => "Approche (interdite, tolérée, autorisée)",
                    "placeholder" => "Approche (interdite, tolérée, autorisée)",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "equipLbl" => [
                    "label" => "Équipement",
                    "placeholder" => "Équipement",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "nageurLbl" => [
                    "label" => "Nombre de nageurs maximum",
                    "placeholder" => "Nombre de nageurs maximum",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "regNagLbl" => [
                    "label" => "Regroupement des nageurs",
                    "placeholder" => "Regroupement des nageurs",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "specEspLbl" => [
                    "label" => "Spécificités par espèce",
                    "placeholder" => "Spécificités par espèce",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "devSpecLbl" => [
                    "label" => "Stade de développement spécifique",
                    "placeholder" => "Stade de développement spécifique",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "passiviteLbl" => [
                    "label" => "Passivité des nageurs",
                    "placeholder" => "Passivité des nageurs",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]

                ],
                "apneeLbl" => [
                    "label" => "Apnées",
                    "placeholder" => "Apnées",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "disApLbl" => [
                    "label" => "Distance d’approche minimale, Zone d’exclusion",
                    "placeholder" => "Distance d’approche minimale, Zone d’exclusion",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "vigZoneLbl" => [
                    "label" => "Zone de vigilance",
                    "placeholder" => "Zone de vigilance",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "tmpsObLbl" => [
                    "label" => "Temps d’observation",
                    "placeholder" => "Temps d’observation",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "zoneSpecLbl" => [
                    "label" => "Zones spécifiques",
                    "placeholder" => "Zones spécifiques",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "nbrBatmaxLbl" => [
                    "label" => "Nombre de bateaux maximum pour la pratique",
                    "placeholder" => "Nombre de bateaux maximum pour la pratique",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "contactLbl" => [
                    "label" => "Contact avec les animaux",
                    "placeholder" => "Contact avec les animaux",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "protocoleLbl" => [
                    "label" => "Protocole spécifique existant",
                    "placeholder" => "Protocole spécifique existant",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "evalRisqLbl" => [
                    "label" => "Évaluation des risques liés à l’activité",
                    "placeholder" => "Évaluation des risques liés à l’activité",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ]
            ];

            $propertiesParams = [
                "labels"=>[],
                "placeholders"=>[],
            ];

                        foreach ($propertiesLabel as $k => $v) {
                if( isset($v["label"])){
                    if( isset($parentForm["params"][$kunik]["labels"][$k]) )
                        $properties[$k]["label"] = $parentForm["params"][$kunik]["labels"][$k];
                    $propertiesParams["labels"][$k] = $propertiesLabel[$k]["label"];
                }

                if( isset($v["placeholder"])){
                    if(isset($parentForm["params"][$kunik]["placeholders"][$k]) )
                       $properties[$k]["placeholder"] = $parentForm["params"][$kunik]["placeholders"][$k];
                    $propertiesParams["placeholders"][$k] = $propertiesLabel[$k]["placeholder"];
                }
            }

            foreach ($propertiesCharte as $k => $v) {
                if( isset($v["label"])){
                    if( isset($parentForm["params"][$kunik]["labels"][$k]) )
                        $properties[$k]["label"] = $parentForm["params"][$kunik]["labels"][$k];
                    $propertiesParams["labels"][$k] = $propertiesCharte[$k]["label"];
                }

                if( isset($v["placeholder"])){
                    if(isset($parentForm["params"][$kunik]["placeholders"][$k]) )
                       $properties[$k]["placeholder"] = $parentForm["params"][$kunik]["placeholders"][$k];
                    $propertiesParams["placeholders"][$k] = $propertiesCharte[$k]["placeholder"];
                }
            }

            foreach ($propertiesReglementation as $k => $v) {
                if( isset($v["label"])){
                    if( isset($parentForm["params"][$kunik]["labels"][$k]) )
                        $properties[$k]["label"] = $parentForm["params"][$kunik]["labels"][$k];
                    $propertiesParams["labels"][$k] = $propertiesReglementation[$k]["label"];
                }

                if( isset($v["placeholder"])){
                    if(isset($parentForm["params"][$kunik]["placeholders"][$k]) )
                       $properties[$k]["placeholder"] = $parentForm["params"][$kunik]["placeholders"][$k];
                    $propertiesParams["placeholders"][$k] = $propertiesReglementation[$k]["placeholder"];
                }
            }
$actualNbr1 = 0;
            ?>

            <?php if(  $mode != "pdf" ){ ?>

            <table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>" style="overflow-x: auto">
            <thead>
            <tr>
                <td colspan='<?php echo count( $propertiesReglementation)+2?>' ><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnReg.$editBtnCrt.$editBtnLbl?></h4>
                    <?php echo $info ?>
                </td>
            </tr>
           
            </thead>
            <tbody class="directoryLines">

            <?php if(isset($answer) && isset($answer["answers"]) && isset($answer["answers"]["sommomForm2"])){ ?>

            <tr>
                    <th>Recommandations</th>
                    <th>Réglementation</th>
                    <th>Charte</th>
                    <th>Label</th>
            </tr>
            <tr>
                    <th>Approche (interdite, tolérée, autorisée)</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                   
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {

                        if (isset($v["aprochReg"])){
                            echo $v["aprochReg"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["aprochCrt"])){
                            echo $v["aprochCrt"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["aprochLbl"])){
                            echo $v["aprochLbl"];
                        }
}
                     }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th>Équipement</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["equipReg"])){
                            echo $v["equipReg"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["equipCrt"])){
                            echo $v["equipCrt"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["equipLbl"])){
                            echo $v["equipLbl"];
                        }
}
                     }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th>Nombre de nageurs maximum</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["nageurReg"])){
                            echo $v["nageurReg"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["nageurCrt"])){
                            echo $v["nageurCrt"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["nageurLbl"])){
                            echo $v["nageurLbl"];
                        }
}
                     }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th>Regroupement des nageurs</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["regNagReg"])){
                            echo $v["regNagReg"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["regNagCrt"])){
                            echo $v["regNagCrt"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["regNagLbl"])){
                            echo $v["regNagLbl"];
                        }
}
                     }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th>Spécificités par espèce</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["specEspReg"])){
                            echo $v["specEspReg"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["specEspCrt"])){
                            echo $v["specEspCrt"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["specEspLbl"])){
                            echo $v["specEspLbl"];
                        }
}
                     }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th>Stade de développement spécifique</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["devSpecReg"])){
                            echo $v["devSpecReg"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["devSpecCrt"])){
                            echo $v["devSpecCrt"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["devSpecLbl"])){
                            echo $v["devSpecLbl"];
                        }
}
                     }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th>Passivité des nageurs</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["passiviteReg"])){
                            echo $v["passiviteReg"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["passiviteCrt"])){
                            echo $v["passiviteCrt"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["passiviteLbl"])){
                            echo $v["passiviteLbl"];
                        }
}
                     }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th>Apnées</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["apneeReg"])){
                            echo $v["apneeReg"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["apneeCrt"])){
                            echo $v["apneeCrt"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["apneeLbl"])){
                            echo $v["apneeLbl"];
                        }
}
                     }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th>Distance d’approche minimale, Zone d’exclusion)</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["disApReg"])){
                            echo $v["disApReg"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["disApCrt"])){
                            echo $v["disApCrt"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["disApLbl"])){
                            echo $v["disApLbl"];
                        }
}
                     }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th> Zone de vigilance </th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["vigZoneReg"])){
                            echo $v["vigZoneReg"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["vigZoneCrt"])){
                            echo $v["vigZoneCrt"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["vigZoneLbl"])){
                            echo $v["vigZoneLbl"];
                        }
}
                     }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th>Temps d’observation</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["tmpsObReg"])){
                            echo $v["tmpsObReg"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["tmpsObCrt"])){
                            echo $v["tmpsObCrt"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["tmpsObLbl"])){
                            echo $v["tmpsObLbl"];
                        }
}
                     }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th>Zones spécifiques</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["zoneSpecReg"])){
                            echo $v["zoneSpecReg"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["zoneSpecCtr"])){
                            echo $v["zoneSpecCtr"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["zoneSpecLbl"])){
                            echo $v["zoneSpecLbl"];
                        }
}
                     }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th>Nombre de bateaux maximum pour la pratique</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["nbrBatMaxReg"])){
                            echo $v["nbrBatMaxReg"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["nbrBatMaxCtr"])){
                            echo $v["nbrBatMaxCtr"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["nbrBatMaxLbl"])){
                            echo $v["nbrBatMaxLbl"];
                        }
}
                     }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th>Contact avec les animaux</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["contactReg"])){
                            echo $v["contactReg"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["contactCtr"])){
                            echo $v["contactCtr"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["contactLbl"])){
                            echo $v["contactLbl"];
                        }
}
                     }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th>Protocole spécifique existant</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["protocoleReg"])){
                            echo $v["protocoleReg"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["protocoleCtr"])){
                            echo $v["protocoleCtr"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["protocoleLbl"])){
                            echo $v["protocoleLbl"];
                        }
}
                     }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th>Évaluation des risques liés à l’activité</th>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["evalRisqReg"])){
                            echo $v["evalRisqReg"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["evalRisqCtr"])){
                            echo $v["evalRisqCtr"];
                        }
}
                     }
                    ?>
                    </td>
                    <td>
                    <?php 
                        //var_dump($answer["answers"]["sommomForm2"]["recommendationnagesommomForm27"]);
                     foreach ($answer["answers"]["sommomForm2"] as $kk => $vv) {
                        foreach ($vv as $k => $v) {
                        if (isset($v["evalRisqLbl"])){
                            echo $v["evalRisqLbl"];
                        }
}
                     }
                    ?>
                    </td>
            </tr>
            <tr>
                    <th></th>
                    <td style="white-space: normal;">
                    <?php
                        if(isset($answers)){
                            foreach ($answers as $q => $a) {
                                if(isset($a["evalRisqReg"])){
                    ?>

                                    <?php
                                    echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
                                        "canEdit"=>($canEdit || $canEdit),
                                        "id" => $answer["_id"],
                                        "collection" => Form::ANSWER_COLLECTION,
                                        "q" => $q,
                                        "path" => $answerPath.$q,
                                        "keyTpl"=>$kunik,
                                        "type" => "reg"
                                    ] ); ?>

                                    <a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
                        

                        <?php
                            }
                        }
                    }
                        ?>
                    </td>
                    <td style="white-space: normal;">
                    <?php
                        if(isset($answers)){
                            foreach ($answers as $q => $a) {
                                if(isset($a["evalRisqCtr"])){
                    ?>
                                
                                    <?php
                                    echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
                                        "canEdit"=>($canEdit || $canEdit),
                                        "id" => $answer["_id"],
                                        "collection" => Form::ANSWER_COLLECTION,
                                        "q" => $q,
                                        "path" => $answerPath.$q,
                                        "keyTpl"=>$kunik,
                                        "type" => "crt"
                                    ] ); ?>
            
                                    <a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
                               

                                <?php
                            }
                        }
                    }
                        ?>
                    </td>  <td style="white-space: normal;">
                    <?php
                        if(isset($answers)){
                            foreach ($answers as $q => $a) {
                                if(isset($a["evalRisqLbl"])){
                    ?>
                                
                                    <?php
                                    echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
                                        "canEdit"=>($canEdit || $canEdit),
                                        "id" => $answer["_id"],
                                        "collection" => Form::ANSWER_COLLECTION,
                                        "q" => $q,
                                        "path" => $answerPath.$q,
                                        "keyTpl"=>$kunik,
                                        "type" => "lbl"
                                    ] ); ?>
            
                                    <a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
                               

                                <?php
                            }
                        }
                    }
                        ?>
                    </td>

            </tr>
                <?php } ?>
            </tbody>
        </table>

        <?php
            } else {
            ?>
            <div>
                <label>
                    <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
                                <?php echo $info ?>
                </label>
            </div>


            <?php } ?>

            
    </div>
    <?php  if( $mode != "pdf"){?>
    <script type="text/javascript">

        var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsProperty = {
            labels : <?php echo json_encode( $propertiesParams["labels"] ); ?>,
            placeholders : <?php echo json_encode( $propertiesParams["placeholders"] ); ?>
        };


        var max<?php echo $kunik ?> = 0;
        var nextValuekey<?php echo $kunik ?> = 0;

        if(notNull(<?php echo $kunik ?>Data)){
            $.each(<?php echo $kunik ?>Data, function(ind, val){
                if(parseInt(ind) > max<?php echo $kunik ?>){
                    max<?php echo $kunik ?> = parseInt(ind);
                }
            });

            nextValuekey<?php echo $kunik ?> = max<?php echo $kunik ?> + 1;

        }


        $(document).ready(function() {

            sectionDyf.<?php echo $kunik ?>Reg = {
                "jsonSchema" : {
                    "title" : "Réglementation",
                    "icon" : "fa-globe",
                    "text" : "",
                    "properties" : <?php echo json_encode( $propertiesReglementation ); ?>,
                    save : function () {
                        var today = new Date();
                        tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
                        $.each( sectionDyf.<?php echo $kunik ?>Reg.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                        });

                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");//$("#ajax-modal").modal('hide');
                                location.reload();
                            } );
                        }

                    }
                }
            };

            sectionDyf.<?php echo $kunik ?>Crt = {
                "jsonSchema" : {
                    "title" : "Charte",
                    "icon" : "fa-globe",
                    "text" : "",
                    "properties" : <?php echo json_encode( $propertiesCharte ); ?>,
                    save : function () {
                        var today = new Date();
                        tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
                        $.each( sectionDyf.<?php echo $kunik ?>Crt.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                        });

                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");//$("#ajax-modal").modal('hide');
                                location.reload();
                            } );
                        }

                    }
                }
            };

            sectionDyf.<?php echo $kunik ?>Lbl = {
                "jsonSchema" : {
                    "title" : "Label",
                    "icon" : "fa-globe",
                    "text" : "",
                    "properties" : <?php echo json_encode( $propertiesLabel ); ?>,
                    save : function () {
                        var today = new Date();
                        tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
                        $.each( sectionDyf.<?php echo $kunik ?>Lbl.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                        });

                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");//$("#ajax-modal").modal('hide');
                                location.reload();
                            } );
                        }

                    }
                }
            };

            sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema" : {
                    "title" : "<?php echo $kunik ?> config",
                    "description" : "Liste de question possible",
                    "icon" : "fa-cog",
                    "properties" : {
                        labels : {
                            inputType : "properties",
                            labelKey : "Clef",
                            labelValue : "Libellé du champs",
                            label : "Modifier les libélés des Questions",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsProperty.labels
                        },
                        placeholders : {
                            inputType : "properties",
                            labelKey : "Clef",
                            labelValue : "Texte dans le champs",
                            label : "Modifier les textes à l'interieur du champs de saisie",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsProperty.placeholders
                        }
                        
                    },
                    save : function () {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                            if(val.inputType == "properties")
                                tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                            else if(val.inputType == "array")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else
                                tplCtx.value[k] = $("#"+k).val();
                            mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                        });
                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
 

$("#ajax-modal").modal('hide');
		urlCtrl.loadByHash( location.hash );

                            } );
                        }

                    }
                }
            };


            mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

            //adds a line into answer
            $(".add<?php echo $kunik ?>Reg").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+nextValuekey<?php echo $kunik ?>;
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Reg );
            });

            $(".add<?php echo $kunik ?>Crt").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+nextValuekey<?php echo $kunik ?>;
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Crt );
            });

            $(".add<?php echo $kunik ?>Lbl").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+nextValuekey<?php echo $kunik ?>;
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Lbl );
            });
            
            $(".edit<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                if($(this).data("type") == "reg"){
                    dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Reg,null, <?php echo $kunik ?>Data[$(this).data("key")]);
                } else if ($(this).data("type")== "crt") {
                    dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Crt,null, <?php echo $kunik ?>Data[$(this).data("key")]);
                } else if ($(this).data("type") == "lbl") {
                    dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Lbl,null, <?php echo $kunik ?>Data[$(this).data("key")]);
                }
            });

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                //if no params config on the element.costum.form.params.<?php echo $kunik ?>
                //then we load default values available in forms.inputs.<?php echo $kunik ?>xxx.params
                //mylog.log(".editParams",sectionDyf.<?php echo $kunik ?>Params,calData);
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            });


        });
    </script>
<?php } 
} ?>