<?php if($answer){
    ?>
    <div class="form-group">
        

            <?php
            $editBtnL = ($canEdit and $mode != "r" and $mode != "pdf") ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";
        $editParamsBtn = ($canEditForm and $mode != "r" || $mode != "pdf") ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

             $paramsData = [
                "month" => [
                    "Janvier" => "Janvier",
                    "Février" => "Février",
                    "Mars" => "Mars",
                    "Avril" => "Avril",
                    "Mai" => "Mai",
                    "Juin" => "Juin",
                    "Juillet" => "Juillet",
                    "Aout" => "Août",
                    "Septembre" => "Septembre",
                    "Octobre" => "Octobre",
                    "Novembre" => "Novembre",
                    "Décembre" => "Décembre",
                ]
             ];
            
            $properties = [
                "localisation" => [
                    "label" => "Localisation de l’activité",
                    "placeholder" => "Nom du lieu : île ou ville",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "portName" => [
                    "label" => "Port de départ",
                    "placeholder" => "Port de départ",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "pointGPS" => [
                    "label" => "Point GPS",
                    "placeholder" => "Point GPS",
                    "inputType" => "formLocality",
                    "rules" => [ "required" => true ]
                ],
                 "location" => [
                    "inputType" => "location",
                    "rules" => [ "required" => true ]
                ],
                "nbrPlace" => [
                    "label" => "Nombre de places au port",
                    "placeholder" => "Nombre de places au port",
                    "inputType" => "text",
                    "rules" => [ "required" => false ]
                ],
                "startDate" => [
                    "label" => "Mois de début d’activité",
                    "placeholder" => "Mois de début d’activité",
                    "inputType" => "select",
                    "options" => $paramsData["month"],
                    "noOrder" => true,
                    "rules" => [ "required" => false ]
                ],
                "endDate" => [
                    "label" => "Mois de fin d’activité",
                    "placeholder" => "Mois de fin d’activité",
                    "inputType" => "select",
                    "noOrder" => true,
                    "options" => $paramsData["month"],
                    "rules" => [ "required" => false ]
                ]
            ];

            $propertiesParams = [
                "labels"=>[],
                "placeholders"=>[],
            ];
            foreach ($properties as $k => $v) {
                if( isset($v["label"])){
                    if( isset($parentForm["params"][$kunik]["labels"][$k]) )
                        $properties[$k]["label"] = $parentForm["params"][$kunik]["labels"][$k];
                    $propertiesParams["labels"][$k] = $properties[$k]["label"];
                }

                if( isset($v["placeholder"])){
                    if(isset($parentForm["params"][$kunik]["placeholders"][$k]) )
                       $properties[$k]["placeholder"] = $parentForm["params"][$kunik]["placeholders"][$k];
                    $propertiesParams["placeholders"][$k] = $properties[$k]["placeholder"];
                }
            }

            ?>

            <?php if(  $mode != "pdf" ){ ?>

            <table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>" style="overflow-x: auto">
            <thead>

            <tr>
                <td colspan="<?php  if( $mode != "r"){ echo count( $properties)+2 ; } else { echo count( $properties) ;} ?>" ><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
                    <?php echo $info ?>
                </td>
            </tr>
            <?php if(isset($answers)){ ?>
            <tr>
                <th rowspan="2"><?php $mn = 0; foreach ($properties as $k => $v) { if( $mn < 1) { echo $v["label"]; $mn++ ;} }  ?></th>
                <th colspan="3">Port de départ</th>
                <th colspan="2">Période de pratique</th>
                <?php if(  $mode != "r" ){ ?>
                           <th rowspan="2"></th>
                        <?php } ?>
                
            </tr>
            <tr>
                <th><?php 
                $mn = 1; 
                foreach ($properties as $k => $v) { 
                    if( $mn ==2) { 
                        echo $v["label"];
                    } 
                    $mn++ ;
                }  ?></th>
                <th><?php 
                $mn = 1; 
                foreach ($properties as $k => $v) { 
                    if( $mn ==3) { 
                        echo $v["label"];
                    } 
                    $mn++ ;
                }  ?></th>
                <th><?php 
                $mn = 1; 
                foreach ($properties as $k => $v) { 
                    if( $mn ==5) { 
                        echo $v["label"];
                    } 
                    $mn++ ;
                }  ?></th>
                <th><?php 
                $mn = 1; 
                foreach ($properties as $k => $v) { 
                    if( $mn ==6) { 
                        echo $v["label"];
                    } 
                    $mn++ ;
                }  ?></th>
                <th><?php 
                $mn = 1; 
                foreach ($properties as $k => $v) { 
                    if( $mn ==7) { 
                        echo $v["label"];
                    } 
                    $mn++ ;
                }  ?></th>
                
            </tr>
            </thead>
            <tbody class="directoryLines">
            <?php
            $ct = 0;

            
                foreach ($answers as $q => $a) {

                    echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
                    foreach ($properties as $i => $inp) {
                        if($i == "location"){
                        } else {
                        echo "<td>";
                        if(isset($a[$i])) {
                            if($i == "pointGPS"){
                                $nameM = $a["localisation"];
                                $lat = $a["pointGPS"]["latitude"];
                                $lon = $a["pointGPS"]["longitude"];
                                echo $a["pointGPS"]["latitude"]." / ".$a["pointGPS"]["longitude"];
                                //echo implode(",", $a["pointGPS"]);
                                echo '<span class="pull-right" style= "margin-right : 5px;"><a id="showActeurLocation" href="javascript:;" onclick="" class="shmap"><i class="fa fa-map-o" aria-hidden="true"></i></a></span>';
                                
                            }
                            else{
                                echo $a[$i];
                            }
                        }
                        echo "</td>";
                        }
                       
                    }
                    ?>
                     <?php  if( $mode != "r"){?>
                            <td style="white-space: normal;">
                                <?php
                                echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
                                    "canEdit"=>($canEdit),
                                    "id" => $answer["_id"],
                                    "collection" => Form::ANSWER_COLLECTION,
                                    "q" => $q,
                                    "path" => $answerPath.$q,
                                    "keyTpl"=>$kunik
                                ] ); ?>

                                <a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
                            </td>
                            <?php } ?>
                    <?php
                    $ct++;
                    echo "</tr>";
                }
            }

            ?>
            </tbody>
        </table>

        <?php
            } else {
            ?>
            <div>
                <label>
                    <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
                                <?php echo $info ?>
                </label>
            </div>

            <table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>" style="overflow-x: auto">
            <thead>
            <?php if(isset($answers)){ ?>
            <tr>
                <th rowspan="2"><?php $mn = 0; foreach ($properties as $k => $v) { if( $mn < 1) { echo $v["label"]; $mn++ ;} }  ?></th>
                <th colspan="3">Port de départ</th>
                <th colspan="2">Période de pratique</th>
            </tr>
            <tr>
                <th><?php 
                $mn = 1; 
                foreach ($properties as $k => $v) { 
                    if( $mn ==2) { 
                        echo $v["label"];
                    } 
                    $mn++ ;
                }  ?></th>
                <th><?php 
                $mn = 1; 
                foreach ($properties as $k => $v) { 
                    if( $mn ==3) { 
                        echo $v["label"];
                    } 
                    $mn++ ;
                }  ?></th>
                <th><?php 
                $mn = 1; 
                foreach ($properties as $k => $v) { 
                    if( $mn ==5) { 
                        echo $v["label"];
                    } 
                    $mn++ ;
                }  ?></th>
                <th><?php 
                $mn = 1; 
                foreach ($properties as $k => $v) { 
                    if( $mn ==6) { 
                        echo $v["label"];
                    } 
                    $mn++ ;
                }  ?></th>
                <th><?php 
                $mn = 1; 
                foreach ($properties as $k => $v) { 
                    if( $mn ==7) { 
                        echo $v["label"];
                    } 
                    $mn++ ;
                }  ?></th>
                
            </tr>
            </thead>
            <tbody class="directoryLines">
            <?php
            $ct = 0;

            
                foreach ($answers as $q => $a) {

                    echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
                    foreach ($properties as $i => $inp) {
                        if($i == "location"){
                        } else {
                        echo "<td>";
                        if(isset($a[$i])) {
                            if($i == "pointGPS"){
                                $nameM = $a["localisation"];
                                $lat = $a["pointGPS"]["latitude"];
                                $lon = $a["pointGPS"]["longitude"];
                                echo $a["pointGPS"]["latitude"]." / ".$a["pointGPS"]["longitude"];
                                //echo implode(",", $a["pointGPS"]);
                                echo '<span class="pull-right" style= "margin-right : 5px;"><a id="showActeurLocation" href="javascript:;" onclick="" class="shmap"><i class="fa fa-map-o" aria-hidden="true"></i></a></span>';
                                
                            }
                            else{
                                echo $a[$i];
                            }
                        }
                        echo "</td>";
                        }
                       
                    }
                    echo "</tr>";
                }
            }

            ?>
            </tbody>
        </table>



          

            

    <?php } ?>



    </div>
    <?php  if( $mode != "pdf"){?>
    <script type="text/javascript">

        var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsProperty = {
            labels : <?php echo json_encode( $propertiesParams["labels"] ); ?>,
            placeholders : <?php echo json_encode( $propertiesParams["placeholders"] ); ?>
        };


        var max<?php echo $kunik ?> = 0;
        var nextValuekey<?php echo $kunik ?> = 0;

        if(notNull(<?php echo $kunik ?>Data)){
            $.each(<?php echo $kunik ?>Data, function(ind, val){
                if(parseInt(ind) > max<?php echo $kunik ?>){
                    max<?php echo $kunik ?> = parseInt(ind);
                }
            });

            nextValuekey<?php echo $kunik ?> = max<?php echo $kunik ?> + 1;

        }


        $(document).ready(function() {

            sectionDyf.<?php echo $kunik ?> = {
                "jsonSchema" : {
                    "title" : "Zones d’observation/d’activité",
                    "icon" : "fa-flag",
                    "text" : "Décrire ici les principaux postes de dépenses : à quoi correspondent les coûts ? <br/>Préciser ce qui relève des dépenses de fonctionnement et des dépenses d’investissement.<br/>Les coûts doivent être en <b>hors taxe</b>.",
                    "properties" : <?php echo json_encode( $properties ); ?>,
                    save : function (formData) {
                        mylog.log("save tplCtx formData",formData);
                        var today = new Date();
                        tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
                        $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                        });

                        if(typeof formData != "undefined" && typeof formData.geo != "undefined"){
                            tplCtx.value["pointGPS"] = formData.geo;
                            tplCtx.value["address"] = formData.address;
                            tplCtx.value["geo"] = formData.geo;
                            tplCtx.value["geoPosition"] = formData.geoPosition;

                            if(typeof formData.addresses != "undefined")
                                tplCtx.value["addresses"] = formData.addresses;
                        }

                        mylog.log("save tplCtx", tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");//$("#ajax-modal").modal('hide');
                                location.reload();
                            } );
                        }

                    }
                }
            };



            sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema" : {
                    "title" : "<?php echo $kunik ?> config",
                    "description" : "Liste de question possible",
                    "icon" : "fa-cog",
                    "properties" : {
                        labels : {
                            inputType : "properties",
                            labelKey : "Clef",
                            labelValue : "Libellé du champs",
                            label : "Modifier les libélés des Questions",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsProperty.labels
                        },
                        placeholders : {
                            inputType : "properties",
                            labelKey : "Clef",
                            labelValue : "Texte dans le champs",
                            label : "Modifier les textes à l'interieur du champs de saisie",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsProperty.placeholders
                        }
                    },
                    save : function () {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                           if(val.inputType == "properties")
                                tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                            else if(val.inputType == "array")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else if(val.inputType == "formLocality")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else
                                tplCtx.value[k] = $("#"+k).val();
                            mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                        });
                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
 

$("#ajax-modal").modal('hide');
		urlCtrl.loadByHash( location.hash );

                            } );
                        }

                    }
                }
            };


            mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

            //adds a line into answer
            $(".add<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+nextValuekey<?php echo $kunik ?>;
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
            });

            $(".edit<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?> ,null, <?php echo $kunik ?>Data[$(this).data("key")]);
            });

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                //if no params config on the element.costum.form.params.<?php echo $kunik ?>
                //then we load default values available in forms.inputs.<?php echo $kunik ?>xxx.params
                //mylog.log(".editParams",sectionDyf.<?php echo $kunik ?>Params,calData);
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            });

    

        });

          $(window).load(function(){
             <?php if(isset($nameM)) { ?>


            mapCO.getPopupSimple = function (data) {
                if(data["typeObs"] !== undefined){
                    if(data["typeObs"] == "zone"){
                        var mapAct = "";
                        var mapEsp = "";
                        for (let i = 0; i < data["mapDataAct"].length + 1; i++) {
                            if(data["mapDataAct"][i] != undefined){
                                mapAct += '<li class="s19li">'+ data["mapDataAct"][i] + '</li>';
                            }
                        }

                        for (let i = 0; i < data["mapDataEsp"].length + 1; i++) {
                            if(data["mapDataEsp"][i] != undefined){
                                mapEsp += '<li class="s19li">'+ data["mapDataEsp"][i] + '</li>';
                            }
                        }

                        if(data["mapDataAct"].length == 0){
                            mapAct = '<li class="s19li">non renseigné</li>'
                        }

                        if(data["mapDataEsp"].length == 0){
                            mapEsp = '<li class="s19li">non renseigné</li>'
                        }

                        return '<div class="container surcharge-container" style="padding-left: 0px;">'+
                                '<div class="surcharge-container" style="width: 300px;">'+
                                    '<div class="panel-group wrap" id="bs-collapse">'+

                                        '<div class="panel">'+
                                            '<div class="panel-heading">'+
                                                '<h4 class="panel-title">'+
                                                    '<a data-toggle="collapse" data-parent="#" href="#one">'+
                                                    'Port'+
                                                    '</a>'+
                                                '</h4>'+
                                            '</div>'+
                                            '<div id="one" class="panel-collapse collapse">'+
                                                '<div class="panel-body">'+
                                                ''+
                                                '<ol class="s19"> <li class="s19li">'+
                                                       data["mapDataPort"]+
                                                ' </li></ol>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+

                                        '<div class="panel">'+
                                            '<div class="panel-heading">'+
                                                '<h4 class="panel-title">'+
                                                    '<a data-toggle="collapse" data-parent="#" href="#three">'+
                                                        'Acteur'+
                                                    '</a>'+
                                                '</h4>'+
                                            '</div>'+
                                            '<div id="three" class="panel-collapse collapse">'+
                                                '<div class="panel-body">'+
                                                    '<span class="pull-right" style= "margin-right : 5px;"><a id="showActeurLocation" href="javascript:;" onclick="addActinMap('+data["num"]+');return false;"><i class="fa fa-map-o" aria-hidden="true"></i></a></span>' +
                                                     '<ol class="s19">'+
                                                        mapAct+
                                                    '</ol>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+

                                        '<div class="panel">'+
                                            '<div class="panel-heading">'+
                                                '<h4 class="panel-title">'+
                                                    '<a data-toggle="collapse" data-parent="#" href="#two">'+
                                                        'Especes Obsérvé'+
                                                    '</a>'+
                                                '</h4>'+
                                            '</div>'+
                                            '<div id="two" class="panel-collapse collapse">'+
                                              '<div class="panel-body">'+
                                              '<ol class="s19">'+
                                                mapEsp+
                                                '</ol>'+
                                              '</div>'+
                                            '</div>'+
                                        '</div>'+

                                        
                                    '</div>'+
                                '</div>'+
                            '</div>';
                    }
                    if(data["typeObs"] == 'acteur'){

                        var formdedie= "";
                        var pgmsci = "";
                        var pgmrech = "";
                        for (let i = 0; i < data["mapformdedie"].length + 1; i++) {
                            if(data["mapformdedie"][i] != undefined){
                                formdedie += '<li class="s19li">'+ data["mapformdedie"][i] + '</li>';
                            }
                        }

                        for (let i = 0; i < data["mappgmsci"].length + 1; i++) {
                            if(data["mappgmsci"][i] != undefined){
                                pgmsci += '<li class="s19li">'+ data["mappgmsci"][i] + '</li>';
                            }
                        }

                        for (let i = 0; i < data["mappgmrech"].length + 1; i++) {
                            if(data["mappgmrech"][i] != undefined){
                                pgmrech += '<li class="s19li">'+ data["mappgmrech"][i] + '</li>';
                            }
                        }

                        if(data["mapformdedie"].length == 0){
                            formdedie = '<li class="s19li">non renseigné</li>'
                        }

                        if(data["mappgmsci"].length == 0){
                            pgmsci = '<li class="s19li">non renseigné</li>'
                        }

                        if(data["mappgmrech"].length == 0){
                            pgmrech = '<li class="s19li">non renseigné</li>'
                        }

                        return '<div class="container surcharge-container" style="padding-left: 0px;">'+
                                '<div class="surcharge-container" style="width: 300px;">'+
                                    '<div class="panel-group wrap" id="bs-collapse">'+

                                        '<div class="panel">'+
                                            '<div class="panel-heading acteurtitle">'+
                                                '<h4 class="panel-title">'+
                                                    '<a data-toggle="collapse" data-parent="#" href="#one">'+
                                                    'Formations'+
                                                    '</a>'+
                                                '</h4>'+
                                            '</div>'+
                                            '<div id="one" class="panel-collapse collapse">'+
                                                '<div class="panel-body">'+
                                                ''+
                                                '<ol class="s19"> '+
                                                       formdedie+
                                                '</ol>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+

                                        '<div class="panel">'+
                                            '<div class="panel-heading acteurtitle">'+
                                                '<h4 class="panel-title">'+
                                                    '<a data-toggle="collapse" data-parent="#" href="#three">'+
                                                        'Programmes de sciences'+
                                                    '</a>'+
                                                '</h4>'+
                                            '</div>'+
                                            '<div id="three" class="panel-collapse collapse">'+
                                                '<div class="panel-body">'+
                                                     '<ol class="s19">'+
                                                        pgmsci+
                                                    '</ol>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+


                                        '<div class="panel">'+
                                            '<div class="panel-heading acteurtitle">'+
                                                '<h4 class="panel-title">'+
                                                    '<a data-toggle="collapse" data-parent="#" href="#two">'+
                                                        'Programmes de recherche'+
                                                    '</a>'+
                                                '</h4>'+
                                            '</div>'+
                                            '<div id="two" class="panel-collapse collapse">'+
                                              '<div class="panel-body">'+
                                              '<ol class="s19">'+
                                                pgmrech+
                                                '</ol>'+
                                              '</div>'+
                                            '</div>'+
                                        '</div>'+

                                        
                                    '</div>'+
                                '</div>'+
                            '</div>';
                    }

                     if(data["typeObs"] == 'acteurform'){
                        return '<span>'+data["nameM"] +' </span>';
                     }

                
                } else {
                    return mapCustom.popup.default(data, this);
                }
            }
             var contextDatAc = [];
             contextDatAc[0] = {
                name : '<?php // echo $nameM;?>',
                type: "citoyens",
                geo: { "type": 'GeoCoordinates', "latitude" : '<?php echo $lat; ?>', "longitude" : '<?php // echo $lon; ?>'},
                typeObs : 'acteurform',
                nameM : '<?php // echo $nameM;?>'
            };


            mapCO.clearMap();
            mapCO.addElts(contextDatAc);
            mapCO.map.invalidateSize();
            $('.shmap').off().click(function(){
                if(typeof mapCO != 'undefined')
                    showMap();
            });  

       
            <?php } ?>

 });
    </script>
<?php } } ?>