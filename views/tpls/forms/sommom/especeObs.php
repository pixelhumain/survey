<?php if($answer){
    ?>
    <div class="form-group">
        

            <?php
            $editBtnL = ($canEdit and $mode != "r" and $mode != "pdf") ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";
        $editParamsBtn = ($canEditForm and $mode != "r" || $mode != "pdf") ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

             
             
            $a = [];

            if(isset($answer["answers"])){
                foreach ($answer["answers"] as $k => $v) {
                    if(is_array($v)){
                        foreach ($v as $kk => $vv) {
                            if(is_array($vv)){  
                               foreach ($vv as $kkk => $vvv) {
                                    if(isset($vvv["localisation"])){
                                        if(isset($vvv["pointGPS"])){
                                            $a += array($vvv["localisation"] => $vvv["localisation"]);
                                        }
                                    }
                                } 
                            }
                        }
                    }
                }
            }
            

            $l = PHDB::findOne(Lists::COLLECTION, array('name' => 'cetaces'));
            $cetacesArray = [];
            foreach ($l['list'] as $i => $inp) {
                $cetacesArray += array($inp["nomScientifique"] => $inp["nomScientifique"]);
            }

    $paramsData = [
                "frequenceOb" => [
                    "frequent" => "Fréquent",
                    "occasionnel" => "Occasionnel",
                    "rare" => "Rare"
                ],
                "statut" => [
                    "Espèce disparue (EX)" => "Espèce disparue (EX)",
                    "Espèce disparue, survivant uniquement en élevage (EW)" => "Espèce disparue, survivant uniquement en élevage (EW)",
                    "Espèce en danger critique d'extinction (CR)" => "Espèce en danger critique d'extinction (CR)",
                    "Espèce en danger (EN)" => "Espèce en danger (EN)",
                    "Espèce vulnérable (VU)" => "Espèce vulnérable (VU)",
                    "Espèce quasi menacée (NT)" => "Espèce quasi menacée (NT)",
                    "Préoccupation mineure (LC)" => "Préoccupation mineure (LC)",

                    "Données insuffisantes (DD)" => "Données insuffisantes (DD)",
                    "Non évalué (NE)" => "Non évalué (NE)"
                ],
                "paramsP" => [
                    "onText" => "oui",
                    "offText" => "Non",
                    "onLabel" => "Oui",
                    "offLabel" => "Non",
                    "labelText" => "Concerne les cétacés"
                ],
                "month" => [
                    "Annuelle " => "Annuelle ",
                    "Janvier" => "Janvier",
                    "Fevrier" => "Février",
                    "Mars" => "Mars",
                    "Avril" => "Avril",
                    "Mai" => "Mai",
                    "Juin" => "Juin",
                    "Juillet" => "Juillet",
                    "Aout" => "Août",
                    "Septembre" => "Septembre",
                    "Octobre" => "Octobre",
                    "Novembre" => "Novembre",
                    "Decembre" => "Décembre",
                ]
            ];
            if( isset($parentForm["params"][$kunik]["frequenceOb"]) )
                $paramsData["frequenceOb"] =  $parentForm["params"][$kunik]["frequenceOb"];
            if( isset($parentForm["params"][$kunik]["statut"]) )
                $paramsData["statut"] =  $parentForm["params"][$kunik]["statut"];

            $properties = [
                "especeRec" => [
                    "label" => "Espèce recensée sur le territoire",
                    "placeholder" => "Espèce recensée sur le territoire",
                    "inputType" => "select",
                    "noOrder" => false,
                    "options" => $cetacesArray,
                    "rules" => [ "required" => true ]
                ],
                "residence" => [
                    "label" => "Résidence",
                    "placeholder" => "Résidence",
                    "inputType" => "checkboxSimple",
                    "params" => $paramsData["paramsP"],
                    "rules" => [ "required" => false ]
                ],
                "migratrice" => [
                    "label" => "migratrice",
                    "placeholder" => "migratrice",
                    "inputType" => "checkboxSimple",
                    "params" => $paramsData["paramsP"],
                    "rules" => [ "required" => false ]
                ],
                "dpPresence" => [
                    "label" => "début du  Période de présence",
                    "placeholder" => "début du Période de présence",
                    "inputType" => "select",
                    "noOrder" => true,
                    "options" => $paramsData["month"],
                    "rules" => [ "required" => false ]
                ],
                "pPresence" => [
                    "label" => "fin du Période de présence",
                    "placeholder" => "fin du Période de présence",
                    "inputType" => "select",
                    "noOrder" => true,
                    "options" => $paramsData["month"],
                    "rules" => [ "required" => false ]
                ],
                // "dpObservation" => [
                //     "label" => " début du Période d’observation",
                //     "placeholder" => "début du Période d’observation",
                //     "inputType" => "select",
                //     "noOrder" => true,
                //     "options" => $paramsData["month"],
                //     "rules" => [ "required" => false ]
                // ],
                // "pObservation" => [
                //     "label" => "fin du Période d’observation",
                //     "placeholder" => "fin du  Période d’observation",
                //     "inputType" => "select",
                //     "noOrder" => true,
                //     "options" => $paramsData["month"],
                //     "rules" => [ "required" => false ]
                // ],
                "frequenceOb" => [
                    "label" => "Fréquence d’observation lorsqu’elle est présente",
                    "placeholder" => "Fréquence d’observation lorsqu’elle est présente",
                    "inputType" => "select",
                    "noOrder" => true,
                    "options" => $paramsData["frequenceOb"],
                    "rules" => [ "required" => false ]
                ],
                "statut" => [
                    "label" => "Statut Liste Rouge locale",
                    "placeholder" => "Statut Liste Rouge locale ",
                    "inputType" => "select",
                    "noOrder" => true,
                    "options" => $paramsData["statut"],
                    "rules" => ["required" => false ]
                ],
                "zoneObs" => [
                    "label" => "Zone d'observation",
                    "placeholder" => "Zone d'observation",
                    "inputType" => "selectMultiple",
                    "options" => $a,
                    "rules" => ["required" => false ]
                ]
            ];

            $propertiesParams = [
                "labels"=>[],
                "placeholders"=>[],
            ];

            foreach ($properties as $k => $v) {
                if( isset($v["label"])){
                    if( isset($parentForm["params"][$kunik]["labels"][$k]) )
                        $properties[$k]["label"] = $parentForm["params"][$kunik]["labels"][$k];
                    $propertiesParams["labels"][$k] = $properties[$k]["label"];
                }

                if( isset($v["placeholder"])){
                    if(isset($parentForm["params"][$kunik]["placeholders"][$k]) )
                       $properties[$k]["placeholder"] = $parentForm["params"][$kunik]["placeholders"][$k];
                    $propertiesParams["placeholders"][$k] = $properties[$k]["placeholder"];
                }
            }

            ?>

            <?php if(  $mode != "pdf" ){ ?>

            <table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>" style="overflow-x: auto">
             <thead>
            <tr>
                <td colspan="<?php  if( $mode != "r"){ echo count( $properties)+2 ; } else { echo count( $properties) ;} ?>" ><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
                    <?php echo $info ?>
                </td>
            </tr>
            <?php

            if( count($answers)>0 ){ ?>
                <tr>
                    </th>
                    <?php

                    foreach ($properties as $i => $inp) {
                        if(isset($inp["placeholder"])){
                        echo "<th>".$inp["placeholder"]."</th>";
                        }
                    
                    } ?>
                    <?php if(  $mode != "r" ){ ?>
                            <th></th>
                        <?php } ?>
                </tr>
                <tr></tr>
            <?php } ?>
            </thead>
            <tbody class="directoryLines">
            <?php
            $ct = 0;

            if(isset($answers)){
                foreach ($answers as $q => $a) {

                    echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
                    $contr = 0;
                        foreach ($properties as $i => $inp) {
                         if($i == "location"){
                        } else {
                        echo "<td>";
                        if(isset($a[$i])) {
                            if(is_array($a[$i])) {
                                
                                     echo implode(",", $a[$i]);
                                  
                                
                            } 
                             else {
                                    if (ctype_digit($a[$i]) && $contr == 0) {
                                        foreach ($l['list'] as $k => $inp) {
                                        if($k == $a[$i]){
                                            echo $inp["nomScientifique"];
                                        }
                                    }
                                    } else
                                    if($a[$i] == "true"){
                                        echo "oui";
                                    }else if($a[$i] == "false"){
                                        echo "non";
                                    }else if($i == "frequenceOb" && isset($paramsData[$i][$a[$i]])){

                                        echo $paramsData[$i][$a[$i]];
                                    }
                                    else if($i == "dpPresence" && isset($paramsData["month"][$a[$i]])){
                                        echo($paramsData["month"][$a[$i]]);
                                    }
                                    else if($i == "pPresence" && isset($paramsData["month"][$a[$i]])){
                                        echo($paramsData["month"][$a[$i]]);
                                    }
                                    else {
                                        echo $a[$i];
                                    }
                            }  
                            
                        }
                        $contr++;
                        echo "</td>";
                    }
                }
                    ?>
                      <?php  if( $mode != "r"){?>
                            <td style="white-space: normal;">
                                <?php
                                echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
                                    "canEdit"=>($canEdit),
                                    "id" => $answer["_id"],
                                    "collection" => Form::ANSWER_COLLECTION,
                                    "q" => $q,
                                    "path" => $answerPath.$q,
                                    "keyTpl"=>$kunik
                                ] ); ?>

                                <a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
                            </td>
                            <?php } ?>
                    <?php
                    $ct++;
                    echo "</tr>";
                }
            }

            ?>
            </tbody>
        </table>

        <?php
            } else {
            ?>
            <div>
                <label>
                    <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
                                <?php echo $info ?>
                </label>
            </div>


             <table class="table table-bordered table-hover  directoryTable" style="font-size: 14px !important;" id="<?php echo $kunik?>">
             <thead>
            <?php

            if( count($answers)>0 ){ ?>
                <tr>
                    <?php

                    foreach ($properties as $i => $inp) {
                        if(isset($inp["placeholder"])){
                        echo "<th>".$inp["placeholder"]."</th>";
                        }
                    
                    } ?>
                </tr>
            <?php } ?>
            </thead>
            <tbody class="directoryLines">
            <?php
            $ct = 0;

            if(isset($answers)){
                foreach ($answers as $q => $a) {

                    echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
                    $contr = 0;
                        foreach ($properties as $i => $inp) {
                         if($i == "location"){
                        } else {
                        echo "<td>";
                        if(isset($a[$i])) {
                            if(is_array($a[$i])) {
                                
                                     echo implode(",", $a[$i]);
                                  
                                
                            } 
                             else {
                                    if (ctype_digit($a[$i]) && $contr == 0) {
                                        foreach ($l['list'] as $k => $inp) {
                                        if($k == $a[$i]){
                                            echo $inp["nomScientifique"];
                                        }
                                    }
                                    } else
                                    if($a[$i] == "true"){
                                        echo "oui";
                                    }else if($a[$i] == "false"){
                                        echo "non";
                                    }else if($i == "frequenceOb" && isset($paramsData[$i][$a[$i]])){

                                        echo $paramsData[$i][$a[$i]];
                                    }
                                    else if($i == "dpPresence" && isset($paramsData["month"][$a[$i]])){
                                        echo($paramsData["month"][$a[$i]]);
                                    }
                                    else if($i == "pPresence" && isset($paramsData["month"][$a[$i]])){
                                        echo($paramsData["month"][$a[$i]]);
                                    }
                                    else {
                                        echo $a[$i];
                                    }
                            }  
                            
                        }
                        $contr++;
                        echo "</td>";
                    }
                }
                    echo "</tr>";
                }
            }

            ?>
            </tbody>
        </table>


          
           

    <?php } ?>


    </div>
    <?php  if( $mode != "pdf"){?>
    <script type="text/javascript">

        var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsProperty = {
            labels : <?php echo json_encode( $propertiesParams["labels"] ); ?>,
            placeholders : <?php echo json_encode( $propertiesParams["placeholders"] ); ?>
        };

        var max<?php echo $kunik ?> = 0;
        var nextValuekey<?php echo $kunik ?> = 0;

        if(notNull(<?php echo $kunik ?>Data)){
            $.each(<?php echo $kunik ?>Data, function(ind, val){
                if(parseInt(ind) > max<?php echo $kunik ?>){
                    max<?php echo $kunik ?> = parseInt(ind);
                }
            });

            nextValuekey<?php echo $kunik ?> = max<?php echo $kunik ?> + 1;

        }


        $(document).ready(function() {

            sectionDyf.<?php echo $kunik ?> = {
                "jsonSchema" : {
                    "title" : "Espèce observée",
                    "icon" : "fa-money",
                    "text" : "",
                    "properties" : <?php echo json_encode( $properties ); ?>,
                    save : function () {
                        var today = new Date();
                        tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
                        $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                        });

                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");//$("#ajax-modal").modal('hide');
                                location.reload();
                            } );
                        }

                    }
                }
            };

            sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema" : {
                    "title" : "<?php echo $kunik ?> config",
                    "description" : "Liste de question possible",
                    "icon" : "fa-cog",
                    "properties" : {
                        labels : {
                            inputType : "properties",
                            labelKey : "Clef",
                            labelValue : "Libellé du champs",
                            label : "Modifier les libélés des Questions",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsProperty.labels
                        },
                        placeholders : {
                            inputType : "properties",
                            labelKey : "Clef",
                            labelValue : "Texte dans le champs",
                            label : "Modifier les textes à l'interieur du champs de saisie",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsProperty.placeholders
                        },
                        labels : {
                            inputType : "properties",
                            labelKey : "Clef",
                            labelValue : "Libellé du champs",
                            label : "Modifier les libélés des Questions",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsProperty.labels
                        },
                        placeholders : {
                            inputType : "properties",
                            labelKey : "Clef",
                            labelValue : "Texte dans le champs",
                            label : "Modifier les textes à l'interieur du champs de saisie",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsProperty.placeholders
                        }
                    },
                    save : function () {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                            if(val.inputType == "properties")
                                tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                            else if(val.inputType == "array")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else
                                tplCtx.value[k] = $("#"+k).val();
                            mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                        });
                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
 

$("#ajax-modal").modal('hide');
		urlCtrl.loadByHash( location.hash );

                            } );
                        }

                    }
                }
            };


            mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

            //adds a line into answer
            $(".add<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+nextValuekey<?php echo $kunik ?>;
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
            });

            $(".edit<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
            });

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                //if no params config on the element.costum.form.params.<?php echo $kunik ?>
                //then we load default values available in forms.inputs.<?php echo $kunik ?>xxx.params
                //mylog.log(".editParams",sectionDyf.<?php echo $kunik ?>Params,calData);
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            });


        });
    </script>
<?php } }?>

