<?php if($answer){
    ?>
    <div class="form-group">
        <?php
        $editBtnL = ($canEdit and $mode != "r" and $mode != "pdf") ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";
        $editParamsBtn = ($canEditForm and $mode != "r" || $mode != "pdf") ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

        $a = [];
        if(isset($answer["answers"]["sommomForm1"])){
            if(is_array($answer["answers"]["sommomForm1"])){
                foreach ($answer["answers"]["sommomForm1"] as $i => $inp) {
                    if(is_array($inp)){
                        foreach ($inp as $line) {
                            if(isset($line["portName"])){
                                $a += array($line["portName"] => $line["portName"]);
                            }   
                        }
                    }
                }
            }
        }

        if (isset($el["_id"])) {
            $communityLinks = Element::getCommunityByTypeAndId( Project::COLLECTION, (string)$el["_id"]);
 
                $partenaire = false;
                $isAdm = false;

                foreach ($communityLinks as $memberId => $memberValue) {
                    if(isset(Yii::app()->session["userId"])){
                        if($memberId != ""){
                            if($memberId == (string)Yii::app()->session["userId"]){
                                if (isset($memberValue["isAdmin"]) && $memberValue["isAdmin"] == true) {
                                    $isAdm = true;
                                }else{
                                    if(isset($memberValue["roles"])){
                                        if(in_array("Partenaire", $memberValue["roles"])){
                                            if (in_array($answer["answers"]["sommomForm1"]["sommomForm120"], $memberValue["roles"])) {
                                                $partenaire = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


        }
            
        $paramsData = [
            "role" => [
                  "Organisme impliqué dans le suivi, l’encadrement de l’activité d’observation" => "Organisme impliqué dans le suivi, l’encadrement de l’activité d’observation",
                  "Organisme impliqué dans la pratique de l’activité d’observation : Opérateur en mer, opérateur aérien" => "Organisme impliqué dans la pratique de l’activité d’observation : Opérateur en mer, opérateur aérien",
                  "Organisme impliqué dans la science et la recherche sur l’activité d’observation" => "Organisme impliqué dans la science et la recherche sur l’activité d’observation",
                  "Organisme impliqué dans l’éducation et la sensibilisation sur l’activité d’observation" => "Organisme impliqué dans l’éducation et la sensibilisation sur l’activité d’observation",
                  "Organisme impliqué dans la protection de l’environnement et la conservation des espèces" => "Organisme impliqué dans la protection de l’environnement et la conservation des espèces"
            ],

            "statut" => [
                  "Service représentant de l’Etat" => "Service représentant de l’Etat",
                  "Collectivité" => "Collectivité",
                  "Syndicat, fédération, représentant d’acteurs de la mer" => "Syndicat, fédération, représentant d’acteurs de la mer",
                  "Structure privée commerciale" => "Structure privée commerciale",
                  "Association de sport et loisir" => "Association de sport et loisir",
                  "Association environnementale" => "Association environnementale",
                  "Institut de recherche" => "Institut de recherche",
                  "Autre" => "Autre"
            ]
        ];

        if( isset($parentForm["params"][$kunik]["statut"]) )
            $paramsData["statut"] =  $parentForm["params"][$kunik]["statut"];

            
            $properties = [
                "intitule" => [
                    "label" => "Nom",
                    "placeholder" => "Intitulé",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "role" => [
                    "label" => "Rôles",
                    "placeholder" => "Choisir parmi les rôles de la liste",
                    "inputType" => "selectMultiple",
                    "noOrder" => true,
                    "options" => $paramsData["role"],
                    "rules" => [ "required" => true ]
                ],
                "statut" => [
                    "label" => "Statut",
                    "placeholder" => "Statut",
                    "inputType" => "select",
                    "noOrder" => true,
                    "options" => $paramsData["statut"],
                    "rules" => [ "required" => true ]
                ],
                 "contact" => [
                    "label" => "contact",
                    "placeholder" => "Adresse mail",
                    "inputType" => "text",
                    "rules" => [ "required" => false ]
                ],
                "file" => [
                    "label" => "logo",
                    "placeholder" => "logo",
                    "inputType" => "uploader",
                    "rules" => [ "required" => false ],
                    "docType" => "file"
                ]
            ];

            $propertiesParams = [
                "labels"=>[],
                "placeholders"=>[],
            ];

            foreach ($properties as $k => $v) {
                if( isset($v["label"])){
                    if( isset($parentForm["params"][$kunik]["labels"][$k]) )
                        $properties[$k]["label"] = $parentForm["params"][$kunik]["labels"][$k];

                    $propertiesParams["labels"][$k] = $properties[$k]["label"];
                }

                if( isset($v["placeholder"])){
                    if(isset($parentForm["params"][$kunik]["placeholders"][$k]) )
                       $properties[$k]["placeholder"] = $parentForm["params"][$kunik]["placeholders"][$k];
                    $propertiesParams["placeholders"][$k] = $properties[$k]["placeholder"];
                }
            }

            ?>

            <?php if(  $mode != "pdf" ){ ?>

            <table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>" style="overflow-x: auto" >
                <thead>
                    <tr>
                        <td colspan="<?php  if( $mode != "r"){ echo count( $properties)+2 ; } else { echo count( $properties) ;} ?>" ><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
                            <?php echo $info ?>
                        </td>
                    </tr>
                </thead>
        
                <tbody class="directoryLines">
                <?php
                    if( count($answers)>0 ){ ?>
                        <tr>
                            <?php

                            foreach ($properties as $i => $inp) {
                                if (isset($inp["label"])){
                                    if ($inp["label"] == "contact") {
                                        if($isAdm or $partenaire){
                                            echo "<th>".$inp["label"]."</th>";
                                        }
                                    } else {
                                        echo "<th>".$inp["label"]."</th>";
                                    }
                                }
                            } 
                            ?>
                            <?php if(  $mode != "r" ){ ?>
                            <th></th>
                        <?php } ?>
                        </tr>
                    <?php
                    }
            
                    if(isset($answers)){
                        foreach ($answers as $q => $a) {
                            echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
                            foreach ($properties as $i => $inp) {
                                if($i == "contact"){
                                    if($isAdm || $partenaire){
                                        echo "<th>".$a[$i]."</th>";
                                    }
                                }
                                elseif($i != "location" && $i != "file"){
                                    echo "<td>";
                                        if(isset($a[$i])) {
                                            if(is_array($a[$i])){
                                                echo implode(",", $a[$i]);
                                            } else {
                                                if($i == "site") {
                                                    echo "<a href='".$a[$i]."'>".$a[$i]."</a>";
                                                }
                                                else {
                                                    echo $a[$i];
                                                }
                                            }
                                        }
                                        echo "</td>";
                                }
                            }

                            echo "<td>";

                            $initAnswerFiles=Document::getListDocumentsWhere(array(
                            "id"=>(string)$answer["_id"], 
                            "type"=>'answers',
                            "subKey"=>$answerPath.$q), "file");

                    
                            if(!empty($initAnswerFiles)){
                                $answers[$q]["files"] = $initAnswerFiles;
                                foreach ($initAnswerFiles as $key => $d) {
                                    ?>

                                        <div style="width: 100%;" class=' padding-5 margin-top-5 margin-bottom-5' id='<?php echo $key ?>'>
                                        <img src="<?php echo $d["docPath"] ?>" class="img-responsive">
                                        <!-- <span class="col-xs-12 elipsis"><?php //echo $d["name"] ?></span> -->
                                        <?php if($canEdit) { ?>
                                            <!-- <a href='javascript:;' class='pull-right text-red btn-remove-document' data-id='<?php //echo $key ?>'><i class='fa fa-trash'></i> <?php //echo Yii::t("common","Delete") ?></a> -->
                                        <?php } ?>
                                    </div>
                                    <?php
                                     }
                            }else {
                            ?>
                            
                            <div style="width: 100%;" class='col-xs-12 col-sm-4 padding-5 shadow2 margin-top-5 margin-bottom-5' >
                                <img src="<?php echo Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl()."/images/thumbnail-default.jpg"; ?>" class="img-responsive">
                            </div>
                            <?php
                            }
                        
                            echo "</td>";
                            ?>

                            <?php  if( $mode != "r"){?>
                            <td style="white-space: normal;">
                                <?php
                                echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
                                    "canEdit"=>($canEdit),
                                    "id" => $answer["_id"],
                                    "collection" => Form::ANSWER_COLLECTION,
                                    "q" => $q,
                                    "path" => $answerPath.$q,
                                    "keyTpl"=>$kunik
                                ] ); ?>

                                <a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
                            </td>
                            <?php } ?>
                    
                            <?php
                                echo "</tr>";
                        }
                    }

                    ?>
                </tbody>
            </table>
            <?php
            } else {
            ?>
            <div>
                <label>
                    <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
                                <?php echo $info ?>
                </label>
            </div>

            <table style="font-size: 14px !important;">
                <thead>
                   
                    <?php
                    if( count($answers)>0 ){ ?>
                        
                           <tr>
                                <?php

                                foreach ($properties as $i => $inp) {
                                       if (isset($inp["label"])){
                                            if ($inp["label"] == "contact") {
                                                if($isAdm or $partenaire){
                                                    echo "<th>".$inp["label"]."</th>";
                                                }
                                            } else {
                                                echo "<th>".$inp["label"]."</th>";
                                            }
                                        }
                                    } 
                                 ?>
                            </tr>
                        
                    <?php
                    } 
                    ?>
                </thead>
        
                <tbody class="directoryLines">
                <?php
                    
                    if(isset($answers)){
                        foreach ($answers as $q => $a) {
                            echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
                            foreach ($properties as $i => $inp) {
                                if($i == "contact"){
                                    if($isAdm or $partenaire){
                                        echo "<td>".$a[$i]."</td>";
                                    }
                                }
                                elseif($i != "location" and $i != "file"){
                                    echo "<td>";
                                        if(isset($a[$i])) {
                                            if(is_array($a[$i])){
                                                echo implode(",", $a[$i]);
                                            } else {
                                                if($i == "site") {
                                                    echo "<a href='".$a[$i]."'>".$a[$i]."</a>";
                                                }
                                                else {
                                                    echo $a[$i];
                                                }
                                            }
                                        }
                                        echo "</td>";
                                }
                            }

                            echo "<td>";

                            $initAnswerFiles=Document::getListDocumentsWhere(array(
                            "id"=>(string)$answer["_id"], 
                            "type"=>'answers',
                            "subKey"=>$answerPath.$q), "file");

                    
                            if(!empty($initAnswerFiles)){
                                $answers[$q]["files"] = $initAnswerFiles;
                                foreach ($initAnswerFiles as $key => $d) {
                                    ?>

                                        <div style="width: 100%;" class=' padding-5 margin-top-5 margin-bottom-5' id='<?php echo $key ?>'>
                                        <img src="<?php echo $d["docPath"] ?>" class="img-responsive">
                                        <!-- <span class="col-xs-12 elipsis"><?php //echo $d["name"] ?></span> -->
                                        <?php if($canEdit) { ?>
                                            <!-- <a href='javascript:;' class='pull-right text-red btn-remove-document' data-id='<?php //echo $key ?>'><i class='fa fa-trash'></i> <?php //echo Yii::t("common","Delete") ?></a> -->
                                        <?php } ?>
                                    </div>
                                    <?php
                                     }
                            }else {
                            ?>
                            
                            <div style="width: 100%;" class='col-xs-12 col-sm-4 padding-5 shadow2 margin-top-5 margin-bottom-5' >
                                <img src="<?php echo Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl()."/images/thumbnail-default.jpg"; ?>" class="img-responsive">
                            </div>
                            <?php
                            }
                        
                            echo "</td>";
                            ?>

                            <?php
                                echo "</tr>";
                        }
                    }

                    ?>
                </tbody>
            </table>


            <?php
            }
            ?>
    </div>

    <?php  if( $mode != "pdf"){?>
    <script type="text/javascript">

        var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsProperty = {
            labels : <?php echo json_encode( $propertiesParams["labels"] ); ?>,
            placeholders : <?php echo json_encode( $propertiesParams["placeholders"] ); ?>
        };

        var max<?php echo $kunik ?> = 0;
        var nextValuekey<?php echo $kunik ?> = 0;

        if(notNull(<?php echo $kunik ?>Data)){
            $.each(<?php echo $kunik ?>Data, function(ind, val){
                if(parseInt(ind) > max<?php echo $kunik ?>){
                    max<?php echo $kunik ?> = parseInt(ind);
                }
            });

            nextValuekey<?php echo $kunik ?> = max<?php echo $kunik ?> + 1;

        }


        $(document).ready(function() {

            sectionDyf.<?php echo $kunik ?> = {
                "jsonSchema" : {
                    "title" : "Les organismes impliqués dans l’activité",
                    "icon" : "fa-globe",
                    "text" : "Décrire ici les principaux postes de dépenses : à quoi correspondent les coûts ? <br/>Préciser ce qui relève des dépenses de fonctionnement et des dépenses d’investissement.<br/>Les coûts doivent être en <b>hors taxe</b>.",
                    "properties" : <?php echo json_encode( $properties ); ?>,
                    beforeBuild : function(){
                       uploadObj.set("answers",answerObj._id.$id, "file", null, null, "/subKey/"+tplCtx.path);
                    },
                    save : function () {
                        var today = new Date();
                        tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
                        $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) {
                            if(val.inputType == "properties")
                                tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                            else if(val.inputType == "array")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else if(val.inputType == "formLocality")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else
                                tplCtx.value[k] = $("#"+k).val();
                        });



                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                // $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");//$("#ajax-modal").modal('hide');
                                dyFObj.commonAfterSave(null, function(){
                                   
                                    // tplCtx.path = tplCtx.path + ".img";
                                    // tplCtx.value = "answers/"+answerObj._id.$id+ ;
                                    // dataHelper.path2Value( tplCtx, function(params) {
                                    if(dyFObj.closeForm()){
                                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                                    location.reload();
                                    }else{
                                        location.reload();
                                    }
                                    // } );
                                });
                            } );
                        }

                    }
                }
            };

            sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema" : {
                    "title" : "<?php echo $kunik ?> config",
                    "description" : "Liste de question possible",
                    "icon" : "fa-cog",
                    "properties" : {
                        labels : {
                            inputType : "properties",
                            labelKey : "Clef",
                            labelValue : "Libellé du champs",
                            label : "Modifier les libélés des Questions",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsProperty.labels
                        },
                        placeholders : {
                            inputType : "properties",
                            labelKey : "Clef",
                            labelValue : "Texte dans le champs",
                            label : "Modifier les textes à l'interieur du champs de saisie",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsProperty.placeholders
                        },
                        statut : {
                            inputType : "array",
                            label : "Liste des statuts",
                            value :  sectionDyf.<?php echo $kunik ?>ParamsData.statut
                        },
                        statut : {
                            inputType : "array",
                            label : "Liste des roles",
                            value :  sectionDyf.<?php echo $kunik ?>ParamsData.role
                        },
                    },
                    save : function (formData) {
                        mylog.log("save tplCtx formData",formData);
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                            if(val.inputType == "properties")
                                tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                            else if(val.inputType == "array")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else
                                tplCtx.value[k] = $("#"+k).val();
                            mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                        });

                        if(typeof formData != "undefined" && typeof formData.geo != "undefined"){
                            tplCtx.value["pointGPS"] = formData.geo;
                            tplCtx.value["address"] = formData.address;
                            tplCtx.value["geo"] = formData.geo;
                            tplCtx.value["geoPosition"] = formData.geoPosition;

                            if(typeof formData.addresses != "undefined")
                                tplCtx.value["addresses"] = formData.addresses;
                        }
                        
                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
 

$("#ajax-modal").modal('hide');
		urlCtrl.loadByHash( location.hash );

                            } );
                        }

                    }
                }
            };


            mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

            //adds a line into answer
            $(".add<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+nextValuekey<?php echo $kunik ?>;
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
            });

            $(".edit<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
            });

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                //if no params config on the element.costum.form.params.<?php echo $kunik ?>
                //then we load default values available in forms.inputs.<?php echo $kunik ?>xxx.params
                //mylog.log(".editParams",sectionDyf.<?php echo $kunik ?>Params,calData);
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            });

            $('.deleteLine').off().click( function(){
              formId = $(this).data("id");
              key = $(this).data("key");
              pathLine = $(this).data("path");
              collection = $(this).data("collection");
              bootbox.dialog({
                  title: trad.confirmdelete,
                  message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                  buttons: [
                    {
                      label: "Ok",
                      className: "btn btn-primary pull-left",
                      callback: function() {
                        var formQ = {
                            value:null,
                            collection : collection,
                            id : formId,
                            path : pathLine
                          };
                          
                          dataHelper.path2Value( formQ , function(params) { 
                                $("#"+key).remove();
                                location.reload();
                            } );
                      }
                    },
                    {
                      label: "Annuler",
                      className: "btn btn-default pull-left",
                      callback: function() {}
                    }
                  ]
              });

            });


        });
    </script>
<?php  } }?>