<?php if($answer){
    ?>
    <div class="form-group">
        

            <?php

            $editBtnL = ($canEdit and $mode != "r" and $mode != "pdf") ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";
        $editParamsBtn = ($canEditForm and $mode != "r" || $mode != "pdf") ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

            $paramsData = [
                "type" => [
                    "Transport de passagers" => "Transport de passagers",
                    "Club de plongée" => "Club de plongée",
                    "Location" => "Location de navires",
                    "Autre" => "Autre"
                ],
                "statut" => [
                    "Collectivité (commune, département, région…)" => "Collectivité (commune, département, région…)",
                    "Service représentant de l’Etat (DEAL, affaires maritimes, préfectures…)" => "Service représentant de l’Etat (DEAL, affaires maritimes, préfectures…)",
                    "Association environnementale" => "Association environnementale",
                    "Association de sport et loisir " => "Association de sport et loisir ",
                    "Syndicats, fédérations, représentants d’acteurs de la mer (FFESSM…)" => "Syndicats, fédérations, représentants d’acteurs de la mer (FFESSM…)",
                    "Structure privée commerciale" => "Structure privée commerciale",
                    "Institut de recherche (IRD, Ifremer,...)" => "Institut de recherche (IRD, Ifremer,...)",
                    "Autre" => "Autre"               
                ],
                "pratique" => [
                    "Embarquée" => "Embarquée",
                    "Nage avec" => "Nage avec",
                    "deux" => "Les deux"
                ],
                "paramsP" => [
                    "onText" => "oui",
                    "offText" => "Non",
                    "onLabel" => "Oui",
                    "offLabel" => "Non",
                    "labelText" => "Concerne les cétacés"
                ]
            ];

            $b = [];
             if(isset($answer["answers"]["sommomForm1"])){
                
                if(is_array($answer["answers"]["sommomForm1"])){
                    foreach ($answer["answers"]["sommomForm1"] as $i => $inp) {
                    if(is_array($inp)){
                        foreach ($inp as $line) {
                            
                                if(isset($line["portName"])){
                                    $b += array($line["portName"] => $line["portName"]);
                                }
                            
                        }
                    }
                }
                   
                }
            }

            $a=[];

            if(isset($answer["answers"]["sommomForm1"])){
                foreach ($answer["answers"]["sommomForm1"] as $k => $v) {
                    if(is_array($v)){
                        foreach ($v as $key => $value) {
                            if(isset($value["intitule"])) {
                                $a += array($value["intitule"] => $value["intitule"]);
                            }
                        }
                    }
                }
            }
            
            if( isset($parentForm["params"][$kunik]["ocean"]) )
                $paramsData["ocean"] =  $parentForm["params"][$kunik]["ocean"];
            $properties = [
               "organisme" => [
                    "label" => "Nom de l’organisme",
                    "placeholder" => "Sélectionner l’organisme",
                    "inputType" => "selectMultiple",
                    "options" => $a,
                    "rules" => [ "required" => false ]
                ],
                "typeStruct" => [
                    "label" => "Type de structure",
                    "placeholder" => "Type de structure",
                    "inputType" => "select",
                    "noOrder" => true,
                    "options" => $paramsData["type"],
                    "rules" => [ "required" => false ]
                ],
                // "statut" => [
                //     "label" => "Statut",
                //     "placeholder" => "Statut",
                //     "inputType" => "select",
                //     "noOrder" => true,
                //     "options" => $paramsData["statut"],
                //     "rules" => [ "required" => false ]
                // ],
                "nbrNav" => [
                    "label" => "Nombre de navires ou appareils",
                    "placeholder" => "Nombre de navires ou appareils",
                    "inputType" => "text",
                    "rules" => [ "required" => false ]
                ],
                "portExp" => [
                    "label" => "Port d’exploitation de l’activité",
                    "placeholder" => "Port d’exploitation de l’activité",
                    "inputType" => "selectMultiple",
                    "options" => $b,
                    "rules" => [ "required" => false ]
                ],
                "pratique" => [
                    "label" => "Type de pratique",
                    "placeholder" => "Type de pratique",
                    "inputType" => "select",
                    "noOrder" => true,
                    "options" => $paramsData["pratique"],
                    "rules" => [ "required" => false ]
                ],
                "intervention" => [
                    "label" => "Intervention de guides formés",
                    "placeholder" => "Intervention de guides formés",
                    "inputType" => "checkboxSimple",
                    "params" => $paramsData["paramsP"],
                    "rules" => [ "required" => false ]
                ],
                "labelisee" => [
                    "label" => "Labellisé",
                    "placeholder" => "Labellisé",
                    "inputType" => "checkboxSimple",
                    "params" => $paramsData["paramsP"],
                    "rules" => [ "required" => false ]
                ]
            ];

            $propertiesParams = [
                "labels"=>[],
                "placeholders"=>[],
            ];

            foreach ($properties as $k => $v) {
                if( isset($v["label"])){
                    if( isset($parentForm["params"][$kunik]["labels"][$k]) )
                        $properties[$k]["label"] = $parentForm["params"][$kunik]["labels"][$k];

                    $propertiesParams["labels"][$k] = $properties[$k]["label"];
                }

                if( isset($v["placeholder"])){
                    if(isset($parentForm["params"][$kunik]["placeholders"][$k]) )
                       $properties[$k]["placeholder"] = $parentForm["params"][$kunik]["placeholders"][$k];
                    $propertiesParams["placeholders"][$k] = $properties[$k]["placeholder"];
                }
            }

            ?>

            <?php if(  $mode != "pdf" ){ ?>

            <table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>" style="overflow-x: auto">
            <thead>
            <tr>
                <td colspan="<?php  if( $mode != "r"){ echo count( $properties)+2 ; } else { echo count( $properties) ;} ?>" ><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
                    <?php echo $info ?>
                </td>
            </tr>
            <?php

            if( count($answers)>0 ){ ?>
                <tr>
                    </th>
                    <?php

                    foreach ($properties as $i => $inp) {
                        echo "<th>".$inp["label"]."</th>";
                    } ?>
                    <?php if(  $mode != "r" ){ ?>
                            <th></th>
                        <?php } ?>
                </tr>
                <tr></tr>
            <?php } ?>
            </thead>
            <tbody class="directoryLines">
            <?php
            $ct = 0;

            if(isset($answers)){
                foreach ($answers as $q => $a) {

                    echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
                    foreach ($properties as $i => $inp) {
                        echo "<td>";
                        if(isset($a[$i])) {
                            if(is_array($a[$i])){
                                echo implode(",", $a[$i]);
                            }
                            else {
                                    if($a[$i] == "true"){
                                        echo "oui";
                                    }else if($a[$i] == "false"){
                                        echo "non";
                                    }else if($i == "pratique" && isset($paramsData[$i][$a[$i]])){
                                        echo $paramsData[$i][$a[$i]];
                                    }
                                    else if($i == "typeStruct" && isset($paramsData["type"][$a[$i]])){
                                        echo $paramsData["type"][$a[$i]];
                                    }
                                    else {
                                        echo $a[$i];
                                    }
                                }  
                        }
                        echo "</td>";
                    }
                    ?>
                    <?php  if( $mode != "r"){?>
                            <td style="white-space: normal;">
                                <?php
                                echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
                                    "canEdit"=>($canEdit),
                                    "id" => $answer["_id"],
                                    "collection" => Form::ANSWER_COLLECTION,
                                    "q" => $q,
                                    "path" => $answerPath.$q,
                                    "keyTpl"=>$kunik
                                ] ); ?>

                                <a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
                            </td>
                            <?php } ?>
                    <?php
                    $ct++;
                    echo "</tr>";
                }
            }

            ?>
            </tbody>
        </table>

        <?php
            } else {
            ?>
            <div>
                <label>
                    <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
                                <?php echo $info ?>
                </label>
            </div>

             <table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>" style="overflow-x: auto">
            <thead>
            <?php

            if( count($answers)>0 ){ ?>
                <tr>
                    <?php

                    foreach ($properties as $i => $inp) {
                        echo "<th>".$inp["label"]."</th>";
                    } ?>
                </tr>
            <?php } ?>
            </thead>
            <tbody class="directoryLines">
            <?php
            $ct = 0;

            if(isset($answers)){
                foreach ($answers as $q => $a) {

                    echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
                    foreach ($properties as $i => $inp) {
                        echo "<td>";
                        if(isset($a[$i])) {
                            if(is_array($a[$i])){
                                echo implode(",", $a[$i]);
                            }
                            else {
                                    if($a[$i] == "true"){
                                        echo "oui";
                                    }else if($a[$i] == "false"){
                                        echo "non";
                                    }else if($i == "pratique" && isset($paramsData[$i][$a[$i]])){
                                        echo $paramsData[$i][$a[$i]];
                                    }
                                    else if($i == "typeStruct" && isset($paramsData["type"][$a[$i]])){
                                        echo $paramsData["type"][$a[$i]];
                                    }
                                    else {
                                        echo $a[$i];
                                    }
                                }  
                        }
                        echo "</td>";
                    }
                    echo "</tr>";
                }
            }

            ?>
            </tbody>
        </table>



            <?php } ?>

            
    </div>
    <?php  if( $mode != "pdf"){?>
    <script type="text/javascript">

        var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsProperty = {
            labels : <?php echo json_encode( $propertiesParams["labels"] ); ?>,
            placeholders : <?php echo json_encode( $propertiesParams["placeholders"] ); ?>
        };


        var max<?php echo $kunik ?> = 0;
        var nextValuekey<?php echo $kunik ?> = 0;

        if(notNull(<?php echo $kunik ?>Data)){
            $.each(<?php echo $kunik ?>Data, function(ind, val){
                if(parseInt(ind) > max<?php echo $kunik ?>){
                    max<?php echo $kunik ?> = parseInt(ind);
                }
            });

            nextValuekey<?php echo $kunik ?> = max<?php echo $kunik ?> + 1;

        }


        $(document).ready(function() {

            sectionDyf.<?php echo $kunik ?> = {
                "jsonSchema" : {
                    "title" : "LES STRUCTURES DE L’ACTIVITÉ - OPÉRATEUR MARITIME",
                    "icon" : "fa-globe",
                    "text" : "Décrire ici les principaux postes de dépenses : à quoi correspondent les coûts ? <br/>Préciser ce qui relève des dépenses de fonctionnement et des dépenses d’investissement.<br/>Les coûts doivent être en <b>hors taxe</b>.",
                    "properties" : <?php echo json_encode( $properties ); ?>,
                    save : function () {
                        var today = new Date();
                        tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
                        $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                        });

                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");//$("#ajax-modal").modal('hide');
                                location.reload();
                            } );
                        }

                    }
                }
            };

            sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema" : {
                    "title" : "<?php echo $kunik ?> config",
                    "description" : "Liste de question possible",
                    "icon" : "fa-cog",
                    "properties" : {
                        labels : {
                            inputType : "properties",
                            labelKey : "Clef",
                            labelValue : "Libellé du champs",
                            label : "Modifier les libélés des Questions",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsProperty.labels
                        },
                        placeholders : {
                            inputType : "properties",
                            labelKey : "Clef",
                            labelValue : "Texte dans le champs",
                            label : "Modifier les textes à l'interieur du champs de saisie",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsProperty.placeholders
                        },
                        type : {
                            inputType : "array",
                            label : "Liste des type",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsData.type
                        },
                        statut : {
                            inputType : "array",
                            label : "Liste des statuts",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsData.statut
                        },
                        pratique : {
                            inputType : "array",
                            label : "Liste des pratiques",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsData.pratique
                        }
                    },
                    save : function () {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                            if(val.inputType == "properties")
                                tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                            else if(val.inputType == "array")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else
                                tplCtx.value[k] = $("#"+k).val();
                            mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                        });
                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
 

$("#ajax-modal").modal('hide');
		urlCtrl.loadByHash( location.hash );

                            } );
                        }

                    }
                }
            };


            mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

            //adds a line into answer
            $(".add<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+nextValuekey<?php echo $kunik ?>;
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
            });

            $(".edit<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
            });

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                //if no params config on the element.costum.form.params.<?php echo $kunik ?>
                //then we load default values available in forms.inputs.<?php echo $kunik ?>xxx.params
                //mylog.log(".editParams",sectionDyf.<?php echo $kunik ?>Params,calData);
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            });

            $('.deleteLine').off().click( function(){
              formId = $(this).data("id");
              key = $(this).data("key");
              pathLine = $(this).data("path");
              collection = $(this).data("collection");
              bootbox.dialog({
                  title: trad.confirmdelete,
                  message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                  buttons: [
                    {
                      label: "Ok",
                      className: "btn btn-primary pull-left",
                      callback: function() {
                        var formQ = {
                            value:null,
                            collection : collection,
                            id : formId,
                            path : pathLine
                          };
                          
                          dataHelper.path2Value( formQ , function(params) { 
                                $("#"+key).remove();
                                location.reload();
                            } );
                      }
                    },
                    {
                      label: "Annuler",
                      className: "btn btn-default pull-left",
                      callback: function() {}
                    }
                  ]
              });

            });


        });
    </script>
<?php } 
} ?>