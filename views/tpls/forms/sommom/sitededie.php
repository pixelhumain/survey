<?php if($answer){
    ?>
    <div class="form-group">
        

            <?php
            $editBtnL = ($canEdit and $mode != "r" and $mode != "pdf") ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";
        $editParamsBtn = ($canEditForm and $mode != "r" || $mode != "pdf") ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

            $paramsData = [ ];

            $a = [];
            if(isset($answer["answers"]["sommomForm2"])){
                foreach ($answer["answers"]["sommomForm2"] as $k => $v) {
                    foreach ($v as $key => $value) {
                        if(isset($value["intituleact"])) {
                            $a += array($value["intituleact"] => $value["intituleact"]);
                        }
                    }
                }
            }

            if( isset($parentForm["params"][$kunik]["ocean"]) )
                $paramsData["ocean"] =  $parentForm["params"][$kunik]["ocean"];

            $properties = [
                "intitule" => [
                    "label" => "Intitulé",
                    "placeholder" => "Intitulé",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "lien" => [
                    "label" => "lien de référence",
                    "placeholder" => "lien de référence",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
            ];

            $propertiesParams = [
                "labels"=>[],
                "placeholders"=>[],
            ];

            foreach ($properties as $k => $v) {
                if( isset($v["label"])){
                    if( isset($parentForm["params"][$kunik]["labels"][$k]) )
                        $properties[$k]["label"] = $parentForm["params"][$kunik]["labels"][$k];
                    $propertiesParams["labels"][$k] = $properties[$k]["label"];
                }

                if( isset($v["placeholder"])){
                    if(isset($parentForm["params"][$kunik]["placeholders"][$k]) )
                       $properties[$k]["placeholder"] = $parentForm["params"][$kunik]["placeholders"][$k];
                    $propertiesParams["placeholders"][$k] = $properties[$k]["placeholder"];
                }
            }

            ?>

            <?php if(  $mode != "pdf" ){ ?>

            <table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>" style="overflow-x: auto">
            <thead>
            <tr>
                <td colspan="<?php  if( $mode != "r"){ echo count( $properties)+2 ; } else { echo count( $properties) ;} ?>" ><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
                    <?php echo $info ?>
                </td>
            </tr>
            <?php

            if( count($answers)>0 ){ ?>
                <tr>
                    </th>
                    <?php

                    foreach ($properties as $i => $inp) {
                        echo "<th>".$inp["placeholder"]."</th>";
                    } ?>
                    <?php if(  $mode != "r" ){ ?>
                            <th></th>
                        <?php } ?>
                </tr>
                <tr></tr>
            <?php } ?>
            </thead>
            <tbody class="directoryLines">
            <?php
            $ct = 0;

            if(isset($answers)){
                foreach ($answers as $q => $a) {

                    echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
                    foreach ($properties as $i => $inp) {
                        echo "<td>";
                        if(isset($a[$i])) {
                            if(is_array($a[$i])){
                                echo implode(",", $a[$i]);
                            }else {
                                if($i == "lien"){
                                    echo "<a href='".$a[$i]."'>".$a[$i]."</a>";
                                }else{
                                    echo $a[$i];
                                }
                            }
                        }
                        echo "</td>";
                    }
                    ?>
                      <?php  if( $mode != "r"){?>
                            <td style="white-space: normal;">
                                <?php
                                echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
                                    "canEdit"=>($canEdit),
                                    "id" => $answer["_id"],
                                    "collection" => Form::ANSWER_COLLECTION,
                                    "q" => $q,
                                    "path" => $answerPath.$q,
                                    "keyTpl"=>$kunik
                                ] ); ?>

                                <a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
                            </td>
                            <?php } ?>
                    <?php
                    $ct++;
                    echo "</tr>";
                }
            }

            ?>
            </tbody>
        </table>

        <?php
            } else {
            ?>
            <div>
                <label>
                    <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
                                <?php echo $info ?>
                </label>
            </div>


            <table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>" style="overflow-x: auto">
            <thead>
            <?php

            if( count($answers)>0 ){ ?>
                <tr>
                    <?php

                    foreach ($properties as $i => $inp) {
                        echo "<th>".$inp["placeholder"]."</th>";
                    } ?>
                </tr>
            <?php } ?>
            </thead>
            <tbody class="directoryLines">
            <?php
            $ct = 0;

            if(isset($answers)){
                foreach ($answers as $q => $a) {

                    echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
                    foreach ($properties as $i => $inp) {
                        echo "<td>";
                        if(isset($a[$i])) {
                            if(is_array($a[$i])){
                                echo implode(",", $a[$i]);
                            }else {
                                if($i == "lien"){
                                    echo "<a href='".$a[$i]."'>".$a[$i]."</a>";
                                }else{
                                    echo $a[$i];
                                }
                            }
                        }
                        echo "</td>";
                    }
                     echo "</tr>";
                }
            }

            ?>
            </tbody>
        </table>



            <?php } ?>


    </div>
    <?php  if( $mode != "pdf"){?>
    <script type="text/javascript">

        var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsProperty = {
            labels : <?php echo json_encode( $propertiesParams["labels"] ); ?>,
            placeholders : <?php echo json_encode( $propertiesParams["placeholders"] ); ?>
        };


        var max<?php echo $kunik ?> = 0;
        var nextValuekey<?php echo $kunik ?> = 0;

        if(notNull(<?php echo $kunik ?>Data)){
            $.each(<?php echo $kunik ?>Data, function(ind, val){
                if(parseInt(ind) > max<?php echo $kunik ?>){
                    max<?php echo $kunik ?> = parseInt(ind);
                }
            });

            nextValuekey<?php echo $kunik ?> = max<?php echo $kunik ?> + 1;

        }


        $(document).ready(function() {

            sectionDyf.<?php echo $kunik ?> = {
                "jsonSchema" : {
                    "title" : "Site dédié à l’information et la sensibilisation du grand public",
                    "icon" : "fa-globe",
                    "text" : "Décrire ici les principaux postes de dépenses : à quoi correspondent les coûts ? <br/>Préciser ce qui relève des dépenses de fonctionnement et des dépenses d’investissement.<br/>Les coûts doivent être en <b>hors taxe</b>.",
                    "properties" : <?php echo json_encode( $properties ); ?>,
                    save : function () {
                        var today = new Date();
                        tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
                        $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                        });

                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");//$("#ajax-modal").modal('hide');
                                location.reload();
                            } );
                        }

                    }
                }
            };

            sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema" : {
                    "title" : "<?php echo $kunik ?> config",
                    "description" : "Liste de question possible",
                    "icon" : "fa-cog",
                    "properties" : {
                        labels : {
                            inputType : "properties",
                            labelKey : "Clef",
                            labelValue : "Libellé du champs",
                            label : "Modifier les libélés des Questions",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsProperty.labels
                        },
                        placeholders : {
                            inputType : "properties",
                            labelKey : "Clef",
                            labelValue : "Texte dans le champs",
                            label : "Modifier les textes à l'interieur du champs de saisie",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsProperty.placeholders
                        }
                    },
                    save : function () {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                            if(val.inputType == "properties")
                                tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                            else if(val.inputType == "array")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else
                                tplCtx.value[k] = $("#"+k).val();
                            mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                        });
                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
 

$("#ajax-modal").modal('hide');
		urlCtrl.loadByHash( location.hash );

                            } );
                        }

                    }
                }
            };


            mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

            //adds a line into answer
            $(".add<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+nextValuekey<?php echo $kunik ?>;
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
            });

            $(".edit<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
            });

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                //if no params config on the element.costum.form.params.<?php echo $kunik ?>
                //then we load default values available in forms.inputs.<?php echo $kunik ?>xxx.params
                //mylog.log(".editParams",sectionDyf.<?php echo $kunik ?>Params,calData);
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            });


        });
    </script>
<?php } 
} ?>