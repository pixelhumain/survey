<?php 
$cssAnsScriptFilesTheme = array(
    '/plugins/DataTables/media/css/DT_bootstrap.css',
    '/plugins/DataTables/media/js/jquery.dataTables.min.1.10.4.js',
    '/plugins/DataTables/media/js/DT_bootstrap.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme,Yii::app()->request->baseUrl);
    if(isset(Yii::app()->session['userId']) && Yii::app()->session['userId']){
        $answerId = isset($answer) ? (string) $answer['_id'] : null;
        HtmlHelper::registerCssAndScriptsFiles(['/plugins/rater/rater-js.js'], Yii::app()->request->baseUrl);
        $criteriasFromForms = !empty($parentForm['params']["criterias".$key]) ? $parentForm['params']["criterias".$key] : [];
        foreach ($criteriasFromForms as $kcf => $vcf) {
            $criteriasFromForms[$kcf]["countComment"] = PHDB::count(Comment::COLLECTION, array("contextId"=> (string)$parentForm["_id"],"contextType"=>"forms", "path"=> $kcf));
            $criteriasFromForms[$kcf]["fromForm"] = true;
        }

        $criteriasFromAnswers = [];
        $allSelfCriterias = PHDB::find(Form::ANSWER_COLLECTION,array(
                "form" => (string)$parentForm["_id"],
                "answers.criterias".$key => ['$exists'=> true],
            ),
            array("answers.criterias".$key)
        );
        foreach ($allSelfCriterias as $kcrit => $vcrit) {
            if(!empty($vcrit['answers']["criterias".$key])){
                foreach($vcrit['answers']["criterias".$key] as $k => $v){
                    $criteriasFromAnswers[$k] = $v;
                    $criteriasFromAnswers[$k]["countComment"] = PHDB::count(Comment::COLLECTION, array("contextId"=> $v["fromAnswerId"],"contextType"=>"answers", "path"=> $k));
                }
            }
        }

        $myAnswer = !empty($answer["answers"]["yesOrNo".$key]) ? $answer["answers"]["yesOrNo".$key] :[];
        $showColumn = !empty($parentForm['params']["config".$key]) ? $parentForm['params']["config".$key] : [];
        $columnLabel = !empty($parentForm['params']["columnLabel".$key]) ? $parentForm['params']["columnLabel".$key] : [];

        $criteriasMerge = array_merge($criteriasFromForms,$criteriasFromAnswers);
        $admin = Form::canAdmin((string)$parentForm["_id"]);
        $editableLabelClass = !empty($canEditForm) && $canEditForm ? 'class="text-center bg-grey editable-label-'.$key.'" contenteditable="true"' : 'class="text-center bg-grey"';

        if(!isset($showColumn["yesNoColumn"])) $showColumn["yesNoColumn"] = true;
        if(!isset($showColumn["usageColumn"])) $showColumn["usageColumn"] = true;
        if(!isset($showColumn["criteriaColumn"])) $showColumn["criteriaColumn"] = true;

        if(empty($columnLabel["usageColumn"]) ) $columnLabel["usageColumn"] = "Usage(s)";
        if(empty($columnLabel["criteriaColumn"]) ) $columnLabel["criteriaColumn"] = "Critères";
        if(empty($columnLabel["yesNoColumn"]) ) $columnLabel["yesNoColumn"] = "Oui ou Nom";
        if(empty($columnLabel["starColumn"]) ) $columnLabel["starColumn"] = "Note";
        if(empty($columnLabel["humourColumn"]) ) $columnLabel["humourColumn"] = "Humeur"; 
?>
<script>
    function emojiButtonList_Initialize(n,e){return new emojiButtonList(n,e)}function emojiButtonList(n,e){var t,o,i={},l={},r={},s=null,a=null,f=null,c=null,u=null;function g(){d("none")}function d(n){u.style.display!==n&&(u.style.display=n)}function p(n){var e=null,t=null==n?"div":n.toLowerCase();return r.hasOwnProperty(t)||(r[t]="text"===t?s.createTextNode(""):s.createElement(t)),e=r[t].cloneNode(!1)}function h(n){var e=null;return y(n)&&(l.hasOwnProperty(n)&&null!==l[n]||(l[n]=s.getElementById(n)),e=l[n]),e}function v(n){var e;return null!==i&&y(i[n])&&(e=i[n],"function"==typeof e)}function y(n){return null!=n&&""!==n}this.setOptions=function(n){y((i=null!==n&&"object"==typeof n?n:{}).emojiRangesToShow)||(i.emojiRangesToShow=[[128513,128591],[9986,10160],[128640,128704]]),y(i.dropDownXAlign)||(i.dropDownXAlign="left"),y(i.dropDownYAlign)||(i.dropDownYAlign="bottom"),y(i.textBoxID)||(i.textBoxID=null),y(i.xAlignMargin)||(i.xAlignMargin=0),y(i.yAlignMargin)||(i.yAlignMargin=0)},t=document,o=window,e=y(e)?e:{},s=t,a=o,this.setOptions(e),f=h(n),c=h(i.textBoxID),(u=document.createElement("div")).className="emoji-drop-down custom-scroll-bars",u.style.display="none",s.body.appendChild(u),i.emojiRangesToShow.length,i.emojiRangesToShow.forEach(n=>{var e,t,o,l;e=n,t="&#"+e+";",o=p("div"),o.innerHTML=t,l=p("div"),l.className="emoji",l.innerHTML=t,u.appendChild(l),l.onclick=function(){var n,e="onEmojiClick";null===c||v("onEmojiClick")?(n=o.innerHTML,v(e)&&i[e](n)):function n(e){if(s.selection)c.focus(),s.selection.createRange().text=e,c.focus();else if(c.selectionStart||0===c.selectionStart){var t=c.selectionStart,o=c.selectionEnd,i=c.scrollTop;c.value=c.value.substring(0,t)+e+c.value.substring(o,c.value.length),c.focus(),c.selectionStart=t+e.length,c.selectionEnd=t+e.length,c.scrollTop=i}else c.value+=e,c.focus()}(o.innerHTML)}}),s.body.addEventListener("click",g),a.addEventListener("resize",g),f.addEventListener("click",function n(e){var t;if((t=e).preventDefault(),t.cancelBubble=!0,"block"===u.style.display)d("none");else{d("block");var o,l,r,s,c=function n(e){for(var t=0,o=0;e&&!isNaN(e.offsetLeft)&&!isNaN(e.offsetTop);)t+=e.offsetLeft-e.scrollLeft,o+=e.offsetTop-e.scrollTop,e=e.offsetParent;return{left:t,top:o}}(f),g=(o=c.left,l=o+i.xAlignMargin,"center"===i.dropDownXAlign&&(l=o-(u.offsetWidth/2-f.offsetWidth/2)),(l+u.offsetWidth>a.innerWidth||"right"===i.dropDownXAlign)&&(l=o-(u.offsetWidth-f.offsetWidth)-i.xAlignMargin),l<i.xAlignMargin&&(l=i.xAlignMargin),l),p=(r=c.top,s=r+f.offsetHeight+i.yAlignMargin,(s+u.offsetHeight>a.innerHeight||"top"===i.dropDownYAlign)&&(s=r-(u.offsetHeight+i.yAlignMargin)),s<i.yAlignMargin&&(s=i.yAlignMargin),s);u.style.top=p+"px",u.style.left=g+"px"}})}
</script>
<style>
    .evaluation<?=$kunik ?> .dropdown:hover .dropdown-menu {
        display: block;
        border-radius: 36px;
    }
    .evaluation<?=$kunik ?> .dropdown .dropdown-menu {
        padding: 0 !important;
        margin: 0 !important;
    }
    .evaluation<?=$kunik ?> .dropdown-menu>li>a:hover, .dropdown-menu>li>a:focus {
        border-radius: 100% !important;
    }

    .evaluation<?=$kunik ?> .btn-group-action{
        display: none;
        position: absolute;
        top: 0;
        left: 0;
    }
    .evaluation<?=$kunik ?> td.label-criteria:hover .btn-group-action,
    .evaluation<?=$kunik ?> td.usage-criteria:hover .btn-group-action {
        display:inline-block;
    }
    .comments-<?= $kunik ?>:empty:before {
        content: attr(placeholder);
        color: #555; 
    }
    .note-happiness-dropdown-<?= $kunik ?> ul li,.note-happiness-dropdown-<?= $kunik ?> ul li a{
        display: inline;
        font-size: 32px;
    }
    .note-happiness-dropdown-<?= $kunik ?> .dropdown-menu>li>a{
        padding: 3px 5px;
    }
    .evaluation<?=$kunik ?> input[type="checkbox"].animate-check {
        display: none;
    }
    .evaluation<?=$kunik ?> input[type="checkbox"].animate-check:checked + .check-box, .check-box.checked {
        top: 9px;
        border-color: #92d12f;
    }
    .evaluation<?=$kunik ?> .check-box {
        top: 9px;
        height: 30px;
        width: 30px;
        background-color: transparent;
        border: 3px solid #3e3e3e;
        border-radius: 5px;
        position: relative;
        display: inline-block;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        -moz-transition: border-color ease 0.2s;
        -o-transition: border-color ease 0.2s;
        -webkit-transition: border-color ease 0.2s;
        transition: border-color ease 0.2s;
        cursor: pointer;
    }
    .evaluation<?=$kunik ?> input[type="checkbox"].animate-check:checked + .check-box::before,.evaluation<?=$kunik ?> .check-box.checked::before {
        visibility: visible;
        width: 6px;
        height: 30px;
        -moz-animation: dothatopcheck 0.4s ease 0s forwards;
        -o-animation: dothatopcheck 0.4s ease 0s forwards;
        -webkit-animation: dothatopcheck 0.4s ease 0s forwards;
        animation: dothatopcheck 0.4s ease 0s forwards;
    }

    .evaluation<?=$kunik ?> .check-box::before {
        top: 21.6px;
        left: 12.3px;
        box-shadow: 0 0 0 1.5px #9bad80;
        -moz-transform: rotate(-135deg);
        -ms-transform: rotate(-135deg);
        -o-transform: rotate(-135deg);
        -webkit-transform: rotate(-135deg);
        transform: rotate(-135deg);
    }
    .evaluation<?=$kunik ?> .check-box::before,.evaluation<?=$kunik ?> .check-box::after {
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        position: absolute;
        height: 0;
        width: 0;
        visibility: hidden;
        background-color: #92d12f;
        display: inline-block;
        -moz-transform-origin: left top;
        -ms-transform-origin: left top;
        -o-transform-origin: left top;
        -webkit-transform-origin: left top;
        transform-origin: left top;
        border-radius: 5px;
        content: " ";
        -webkit-transition: opacity ease 0.5;
        -moz-transition: opacity ease 0.5;
        transition: opacity ease 0.5;
    }
    /**afte */
    .evaluation<?=$kunik ?> input[type="checkbox"].animate-check:checked + .check-box::after,.evaluation<?=$kunik ?> .check-box.checked::after {
        visibility: visible;
        height: 15px;
        width: 7px;
        -moz-animation: dothabottomcheck 0.2s ease 0s forwards;
        -o-animation: dothabottomcheck 0.2s ease 0s forwards;
        -webkit-animation: dothabottomcheck 0.2s ease 0s forwards;
        animation: dothabottomcheck 0.2s ease 0s forwards;
    }
    .evaluation<?=$kunik ?> .check-box::after {
        top: 11.1px;
        left: 1.5px;
        -moz-transform: rotate(-45deg);
        -ms-transform: rotate(-45deg);
        -o-transform: rotate(-45deg);
        -webkit-transform: rotate(-45deg);
        transform: rotate(-45deg);
    }
    .evaluation<?=$kunik ?> .check-box::before,.evaluation<?=$kunik ?> .check-box::after {
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        position: absolute;
        height: 0;
        width: 0;
        visibility: hidden;
        background-color: #92d12f;
        display: inline-block;
        -moz-transform-origin: left top;
        -ms-transform-origin: left top;
        -o-transform-origin: left top;
        -webkit-transform-origin: left top;
        transform-origin: left top;
        border-radius: 5px;
        content: " ";
        -webkit-transition: opacity ease 0.5;
        -moz-transition: opacity ease 0.5;
        transition: opacity ease 0.5;
    }
    .evaluation<?=$kunik ?> .bg-grey{
        background-color: #eee;
    }
    .ui-widget-content{
        z-index: 1000000 !important;
    }
    .ui-menu .ui-menu-item{
        font-size: 15px;
    }
</style>

<div class="container-fluid evaluation<?=$kunik ?>">   
    <div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
        <?php if(isset($resultMode)) {
        } else {
        ?>
            <label for="<?php echo $key ?>">
                <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>" class="<?= $canEditForm && $mode="fa" ? "" : $type ?>">
                    <?php echo $label.$editQuestionBtn ?>
                    <?php if(!empty($canEditForm) && $canEditForm){ ?>
                            <a href="javascript:;" class="btn btn-xs btn-danger config<?= $kunik ?>"><i class="fa fa-cog"></i></a>
                        <?php } ?>
                </h4>
            </label>
        <?php
        }
        ?>
    </div>
    <?php if(!empty($info)){ ?>
        <small id="<?= $key ?>Help" class="form-text text-muted">
            <?php echo $info ?>
        </small>
    <?php } ?>
    <div class="row">
        <div class="col-xs-12 col-md-12 padding-bottom-25">
            <h6 id="entity-<?= $key ?>" style="cursor:pointer">

            </h6>
            <h6 id="tools-users-counter-<?= $key ?>" class="all-tools-users" style="cursor:pointer"></h6>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 no-padding">
            <div class="table-responsive">
                <table class="table table-bordered table-stripped text-left" id="table-<?= $key ?>" style="font-size: 15px;">
                    <thead>
                        <tr>
                            <?php if(!empty($showColumn["usageColumn"]) && $showColumn["usageColumn"]){ ?>
                                <th  <?= $editableLabelClass ?> data-id="usageColumn">
                                    <?= $columnLabel["usageColumn"] ?>
                                </th>
                            <?php } ?>

                            <?php if(!empty($showColumn["criteriaColumn"]) && $showColumn["criteriaColumn"]){ ?>
                                <th <?= $editableLabelClass ?> data-id="criteriaColumn">
                                    <?= $columnLabel["criteriaColumn"] ?>
                                </th>
                            <?php } ?>

                            <?php if(!empty($showColumn["yesNoColumn"]) && $showColumn["yesNoColumn"] && !isset($resultMode)){ 
                            ?>
                                            <th  class="<?= $editableLabelClass ?>" data-id="yesNoColumn">
                                                <?= $columnLabel["yesNoColumn"] ?>
                                            </th>
                            <?php
                                } 
                            ?>
                            <?php if(!empty($showColumn["starColumn"]) && $showColumn["starColumn"] && !isset($resultMode)){ ?>
                            <th  <?= $editableLabelClass ?> data-id="starColumn">
                                <?= $columnLabel["starColumn"] ?>
                            </th>
                            <?php } ?>
                            <?php if(!empty($showColumn["humourColumn"]) && $showColumn["humourColumn"] && !isset($resultMode)){ ?>
                            <th  <?= $editableLabelClass ?> data-id="humourColumn">
                                <?= $columnLabel["humourColumn"] ?> 
                            </th>
                            <?php } ?>

                            <?php if(!empty($showColumn["commentColumn"]) && $showColumn["commentColumn"]){ ?>
                            <th  style="width: 25%;">Commentaires</th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody id="tbody"></tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-5 add-line-<?= $kunik ?>-form">
                
        </div>
    </div>
</div>

<script>
    $(function(){
        var yesOrNoObj = {
            answerId : <?= json_encode($answerId) ?>,
            formId : <?= json_encode((string)$parentForm["_id"]) ?>,
            el : <?= json_encode($el) ?>,
            criteriasFromForms : <?= json_encode($criteriasFromForms) ?>,
            criteriasFromAnswers : <?= json_encode($criteriasFromAnswers) ?>,
            criteriasMerge : <?= json_encode($criteriasMerge) ?>,
            myAnswer : <?= json_encode($myAnswer) ?>,
            allAnswers : [],
            canEditForm : <?= !empty($canEditForm) ? json_encode($canEditForm) : json_encode("") ?>,
            mode : <?= json_encode($mode) ?>,
            key : <?= json_encode($key) ?>,
            configuration : <?= json_encode($showColumn) ?>,
            columnLabel : <?= json_encode($columnLabel) ?>,
            resultMode : <?= isset($resultMode) ? json_encode($resultMode) : json_encode(false) ?>,
            smiley : {
                love : {dec : "&#128525" ,label : "Love", noteWithCoeff:0, ansCount:0},
                happySmile : {dec : "&#128512" ,label : "Smile", noteWithCoeff:0, ansCount:0},
                neutral : {dec : "&#128528" ,label : "Neutre", noteWithCoeff:0, ansCount:0},
                sad : {dec : "&#128542" ,label : trad.sad, noteWithCoeff:0, ansCount:0},
                cry : {dec : "&#128557" ,label : "Pleure", noteWithCoeff:0, ansCount:0}
            },
            allToolsUsers : [],
            addedUsage : [],
            editableLabelClass : <?= json_encode($editableLabelClass) ?>,
            init : function(hcObj){
                hcObj.getAllAnswers(hcObj,function(){
                    hcObj.toolsUsers(hcObj);
                    hcObj.sortByLabel(hcObj);
                    hcObj.createTable(hcObj);
                    hcObj.events(hcObj);
                });
            },
            getAllAnswers : function(hcObj,callback){
                var params = [...Object.keys(hcObj.allAnswers),hcObj.formId];
                ajaxPost('',baseUrl+"/"+moduleId+"/search/globalautocomplete", 
                {  
                    searchType : ["answers"],
                    filters:{
                        'form' : hcObj.formId,
                    },
                    notSourceKey : true,
                    fields:["answers","user","form"]
                },
                function(data){
                    hcObj.allAnswers = data.results;
                    callback();
                },null,null,{async:false});
            },
            sortByLabel : function(hcObj){
                let criteriasLabelArr = [];
                let newSortedObj = {};
                $.each(hcObj.criteriasMerge,function(k,v){
                    criteriasLabelArr.push(v.label);
                })
                criteriasLabelArr = criteriasLabelArr.sort();
                $.each(criteriasLabelArr,function(k,v){
                    $.each(hcObj.criteriasMerge,function(kk,vv){
                        if(vv.label === v)
                            newSortedObj[kk] = vv;
                    })
                })
                hcObj.criteriasMerge = newSortedObj;
            },
            events : function(hcObj){
                hcObj.rate(hcObj);
                hcObj.addHappiness(hcObj);
                //hcObj.addUsage(hcObj);
                hcObj.addYesNo(hcObj);
                hcObj.config(hcObj);
                $('.add-line-<?= $kunik ?>').on('click',function(){
                    hcObj.addEditDeleteLine(hcObj,$(this),"add");
                })
                $('.edit-line<?= $kunik ?>').on('click',function(){
                    hcObj.addEditDeleteLine(hcObj,$(this),"edit");
                })
                $('.delete-line<?= $kunik ?>').on('click',function(){
                    hcObj.addEditDeleteLine(hcObj,$(this),"delete");
                })
                $('.critera-counter-'+hcObj.key+',#tools-users-counter-'+hcObj.key).off().on('click',function(){
                    var btn = $(this);
                    var usersIds = [];
                    if(btn.hasClass('all-tools-users')){
                        usersIds = hcObj.allToolsUsers;
                    }
                    if(notEmpty(hcObj.criteriasMerge[btn.data('key')]) && notEmpty(hcObj.criteriasMerge[btn.data('key')]["usersYes"])){
                        usersIds = hcObj.criteriasMerge[btn.data('key')]["usersYes"];
                    } 
                    ajaxPost('', baseUrl + "/co2/element/get/type/citoyens",
                    { id: usersIds, fields: ["name", "profilMediumImageUrl"] },
                    function (users) {
                        var html = "";
                        $.each(users.map, function (kv, vv) {
                            var roles = (exists(vv.links) && exists(vv.links.memberOf) && exists(vv.links.memberOf.roles)) ? vv.links.memberOf.roles :  [];
                            if(hcObj.el.collection == "projects"){
                                roles = (exists(vv.links) && exists(vv.links.projects) && exists(vv.links.projects.roles)) ? vv.links.projects.roles :  [];
                            }
                            var url = `${baseUrl}#page.type.citoyens.id.${kv}`

                            html +=
                                `<a href='${url}' target="_blank" class="">
                                    <div class='row' id='row-${kv}'>
                                        <div class="col-xs-2">
                                            <img src="${exists(vv.profilMediumImageUrl) ? vv.profilMediumImageUrl : defaultImage}" alt=""  width="45" height="45" style="border-radius: 100%;object-fit:cover"/>
                                        </div>
                                        <div class="col-xs-8">
                                            <span><small>${vv.name}</small></span><br/>
                                            <span class="text-green"><small>${roles.join(',')}</small></span>
                                        </div>
                                    </div>
                                </a>`;
                        })
                        var dialog = bootbox.dialog({
                            title: trad.attendees,
                            message: html
                        });
                        dialog.init(function(){
	                        coInterface.bindLBHLinks();
                        })
                    })
                })
                //hcObj.mean(hcObj);
                $('[data-toggle="tooltip"]').tooltip();
                $('#entity-'+hcObj.key).html(`<a href=""></a>`)
                hcObj.editColumnLabel(hcObj);
            },
            createTable : function(hcObj){
                var html = "";
                if(Object.keys(hcObj.criteriasMerge).length == 0 && Object.keys(hcObj.criteriasFromAnswers).length == 0){
                    html+= `
                        <tr style="position:relative">
                            <td colspan="4"><span class="text-danger"><?= $columnLabel["criteriaColumn"] ?> vide</span></td>
                        </tr>
                    `;
                }else{
                    $.each(hcObj.criteriasMerge,function(k,v){
                        if(notEmpty(v.usage) && !hcObj.addedUsage.includes(v.usage)){
                            hcObj.addedUsage.push(v.usage);
                        }
                        var note = exists(hcObj.myAnswer[k]) && exists(hcObj.myAnswer[k]["note"]) ? hcObj.myAnswer[k]["note"] : 0
                        var happiness = exists(hcObj.myAnswer[k]) && exists(hcObj.myAnswer[k]["happiness"]) ? hcObj.myAnswer[k]["happiness"] : "";
                        //var usage = exists(hcObj.myAnswer[k]) && exists(hcObj.myAnswer[k]["usage"]) ? hcObj.myAnswer[k]["usage"] : " ";
                        var yesno = exists(hcObj.myAnswer[k]) && exists(hcObj.myAnswer[k]["yesOrNo"]) && hcObj.myAnswer[k]["yesOrNo"] ? hcObj.myAnswer[k]["yesOrNo"] : false;
                        var btnComment = `commentObj.openPreview('answers','${v.fromAnswerId}','${k}', 'Commentaire','${hcObj.key}','<?= $form['id'] ?>')`;
                        if(notEmpty(v.fromForm)){
                            btnComment = `commentObj.openPreview('forms','${hcObj.formId}','${k}', 'Commentaire','${hcObj.key}','<?= $form['id'] ?>')`;
                        }
                        html+= `
                            <tr style="position:relative">`;
                            if(notEmpty(hcObj.configuration.usageColumn) && hcObj.configuration.usageColumn){
                                html+= `<td class="text-center usage-criteria bg-grey">
                                        ${notEmpty(v.usage) ? ucfirst(v.usage) : ''}    
                                        <div class="btn-group btn-group-action pull-right margin-right-15">`;
                                            if((v.userId === userId) || hcObj.canEditForm ){
                                                if(hcObj.resultMode == false) {
                                                    html+= `<button data-key="${k}" data-user-id=${v.userId} data-answer-id=${v.fromAnswerId} data-value="${'me'}" data-label="${v.label}" data-usage="${notEmpty(v.usage) ? v.usage : ''}" data-coeff="${v.coeff}" class="btn btn-success btn-xs edit-line<?= $kunik ?>" type="button"><i class="fa fa-edit"></i></button>
                                                    <button data-key="${k}" data-user-id=${v.userId} data-answer-id=${v.fromAnswerId} data-value="${'me'}" data-label="${v.label}" data-usage="${notEmpty(v.usage) ? v.usage : ''}" data-coeff="${v.coeff}" class="btn btn-danger btn-xs delete-line<?= $kunik ?>" type="button"><i class="fa fa-times"></i></button>`;
                                                }
                                            }
                                html+= `</div>
                                    </td>`;
                            }
                            if(notEmpty(hcObj.configuration.criteriaColumn) && hcObj.configuration.criteriaColumn){
                                html+= `<td class="text-left label-criteria bg-grey">${v.label + " " + (v.userId == userId ? "<b>(perso)</b>" : "")} 
                                        <span class="label label-success pull-right hidden">${v.coeff}</span>
                                        <span id="criteria-${k}-counter" data-key="${k}" class="label label-success pull-right critera-counter-${hcObj.key}" style="cursor:pointer"><i class="fa fa-user"></i> ${v.usersYes.length}</span>
                                        <div class="btn-group btn-group-action pull-right margin-right-15">`;
                                        if((v.userId === userId) || hcObj.canEditForm ){
                                            if(hcObj.resultMode == false) {
                                                html+= `<button data-key="${k}" data-user-id=${v.userId} data-answer-id=${v.fromAnswerId} data-value="${'me'}" data-label="${v.label}" data-usage="${notEmpty(v.usage) ? v.usage : ''}" data-coeff="${v.coeff}" class="btn btn-success btn-xs edit-line<?= $kunik ?>" type="button"><i class="fa fa-edit"></i></button>
                                                <button data-key="${k}" data-user-id=${v.userId} data-answer-id=${v.fromAnswerId} data-value="${'me'}" data-label="${v.label}" data-usage="${notEmpty(v.usage) ? v.usage : ''}" data-coeff="${v.coeff}" class="btn btn-danger btn-xs delete-line<?= $kunik ?>" type="button"><i class="fa fa-times"></i></button>`;
                                            }
                                        }
                                html+= `</div>
                                    </td>`;
                            }

                                if(notEmpty(hcObj.configuration.yesNoColumn) && hcObj.configuration.yesNoColumn && hcObj.resultMode == false){
                                    html+= `<td class="text-center">
                                                <input type="checkbox" id="check-${k}" data-key="${k}" name="value${hcObj.key}" class="animate-check yes-no-${hcObj.key}" ${yesno ? "checked" : ""}>
                                                <label for="check-${k}" class="check-box yes-no-label-${hcObj.key}"></label> 
                                            </td>`;
                                }

                                if(notEmpty(hcObj.configuration.starColumn) && hcObj.configuration.starColumn && hcObj.resultMode == false){
                        html+= `<td class="text-center">
                                    <div class="note-value" data-rating='${note}' data-key='${k}'</div>
                                </td>`;
                                }
                                if(notEmpty(hcObj.configuration.humourColumn) && hcObj.configuration.humourColumn && hcObj.resultMode == false){
                        html+= `<td class="text-center">
                                    <div class="dropdown note-happiness-dropdown-<?= $kunik ?>">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="font-size:30px;text-decoration:none">${(notEmpty(hcObj.smiley[happiness]) && notEmpty(hcObj.smiley[happiness].dec)) ? hcObj.smiley[happiness].dec : "&#128528;"}</a>
                                        <ul class="dropdown-menu">`;
                                        $.each(hcObj.smiley,function(kk,vv){
                                            html+= `<li class="tooltips" data-toggle="tooltip" data-placement="top" data-original-title="${vv.label}" >
                                                <a href="javascript:;" class="note-happiness-<?= $kunik ?>" ${happiness == kk ? "selected" : ""} data-key='${k}' data-value="${kk}">${vv.dec}</a>
                                            </li>`;   
                                        })
                                html+= `</ul>
                                    </div>
                                </td>`;
                                }

                                if(notEmpty(hcObj.configuration.commentColumn) && hcObj.configuration.commentColumn){
                                html+=` <td class="text-center comments-<?= $kunik ?>" placeholder="Ajouter un commentaire" data-key="${k}">
                                    <a href="javascript:;" class=" btn-lg margin-right-5 openAnswersComment tooltips btn-custom" onclick="${btnComment}" data-toggle="tooltip" data-placement="bottom" data-original-title="Ajouter un commentaire" style="border:0">
                                        ${v.countComment} <i class='fa fa-commenting'></i>
                                    </a>
                                </td>`;
                                }
                    html+=`</tr>
                        `;
                    })
                }
                /*html+= ` <tr style="border-top: 2px solid;border-bottom: 2px solid;">
                        <td class="text-right bold" style="vertical-align:middle">Moyenne</td>
                        <td class="text-left text-center bold mean-yesno-<?= $kunik ?> ${(notEmpty(hcObj.configuration.yesNoColumn) && hcObj.configuration.yesNoColumn) ? "":"hidden" }" style="font-size:30px"></td>
                        <td class="text-left text-center bold mean-star-<?= $kunik ?>" ${(notEmpty(hcObj.configuration.starColumn) && hcObj.configuration.starColumn) ? "":"hidden"} style="font-size:30px"></td>
                        <td class="text-left text-center bold mean-smiley-<?= $kunik ?>" ${(notEmpty(hcObj.configuration.humourColumn) && hcObj.configuration.humourColumn) ? "":"hidden"} style="font-size:30px"></td>
                    </tr>`;*/

                
                $('#tbody').html(html);
                $('#table-<?= $key ?>').dataTable(
                    {
                        language: {
                            lengthMenu: trad['Display _MENU_ records per page'],
                            zeroRecords: trad.noresult,
                            info: trad['Showing page _PAGE_ of _PAGES_'],
                            infoEmpty: trad.noresult,
                            infoFiltered: '(filtered from _MAX_ total records)',
                            search: trad.search+": "
                        }
                    }
                );
                if(hcObj.resultMode == false){
                    $('#table-<?= $key ?>').after(` <button type="button" data-value="me" class="btn btn-success add-line-<?= $kunik ?>">Ajouter <?= $columnLabel["criteriaColumn"] ?></button>&nbsp;`)
                }
            },
            rate : function(hcObj){
                $('.note-value').each(function(i, obj) {
                    var opts = {
                        rating : $(obj).data("rating"),
                        starSize:20,
                        step:0.5,
                        element:obj,
                        rateCallback : function rateCallback(rating, done) {
                            var $this = this;
                            this.setRating(rating); 
                            var tplCtx ={
                                id : hcObj.answerId,
                                collection : "answers",
                                path : "answers.yesOrNo"+hcObj.key+"."+$(obj).data("key")+".note",
                                value : rating,
                                setType : "float"
                            }
                            dataHelper.path2Value(tplCtx, function (params) {
                                if(params.result){
                                    toastr.success("Ok");
                                    hcObj.myAnswer = params.elt.answers["yesOrNo"+hcObj.key];
                                    //hcObj.mean(hcObj);
                                }
                            });
                            done(); 
                        }
                    };
                    var starRating = raterJs(opts); 
                });
            },
            addHappiness : function(hcObj){
                $(".note-happiness-<?= $kunik ?>").off().on('click',function(){
                    var btn = $(this);
                    var tplCtx ={
                        id : hcObj.answerId,
                        collection : "answers",
                        path : "answers.yesOrNo"+hcObj.key+"."+btn.data("key")+".happiness",
                        value : btn.data("value"),
                    }
                    dataHelper.path2Value(tplCtx, function (params) {
                        if(params.result){
                            btn.parent().parent().parent().find(".dropdown-toggle").html(btn.html());
                            hcObj.myAnswer = params.elt.answers["yesOrNo"+hcObj.key];
                            toastr.success("Ok");
                            //hcObj.mean(hcObj);
                        }
                    });
                })
            },
            addYesNo :function(hcObj){
                $(".yes-no-<?= $key ?>").off().on('change',function(){
                    var btn = $(this);
                    var tplCtx ={
                        id : hcObj.answerId,
                        collection : "answers",
                        path : "answers.yesOrNo"+hcObj.key+"."+btn.data("key")+".yesOrNo",
                        value : "false",
                        format : true,
                        setType : "boolean"
                    }
                    if(this.checked){
                        tplCtx.value = true;
                    }else{
                        tplCtx.value = "false";
                    }

                    dataHelper.path2Value(tplCtx, function (params) {
                        if(params.result){
                            hcObj.myAnswer = params.elt.answers["yesOrNo"+hcObj.key];
                            hcObj.allAnswers[params.id] = params.elt.answers["yesOrNo"+hcObj.key];
                            if(tplCtx.value == "false" && exists(hcObj.criteriasMerge[btn.data("key")] && hcObj.criteriasMerge[btn.data("key")]["usersYes"])){
                                var index =  hcObj.criteriasMerge[btn.data("key")]["usersYes"].indexOf(userId);
                                if (index > -1) {
                                    hcObj.criteriasMerge[btn.data("key")]["usersYes"].splice(index, 1);
                                }
                            }else{
                                if(hcObj.criteriasMerge[btn.data("key")]["usersYes"].indexOf(userId) == -1){
                                    hcObj.criteriasMerge[btn.data("key")]["usersYes"].push(userId);
                                }
                            }
                            $('#criteria-'+btn.data("key")+'-counter').html("<i class='fa fa-user'></i> "+ hcObj.criteriasMerge[btn.data("key")]["usersYes"].length)
                            toastr.success("Ok");
                        }
                    });
                })
            },
            /*addUsage : function(hcObj){
                $('.usage-<?= $kunik ?>').on('blur',function(){
                    var field = $(this);
                    var tplCtx ={
                        id : hcObj.answerId,
                        collection : "answers",
                        path : "answers.yesOrNo"+hcObj.key+"."+field.data("key")+".usage",
                        value : field.text(),
                    }
                    dataHelper.path2Value(tplCtx, function (params) {
                        if(params.result){
                            toastr.success(trad.saved);
                        }
                    });
                })
            },*/
            addEditDeleteLine : function(hcObj,button=null,action="add"){
                var buttonLabel = "Ajouter";
                var label = "";
                var usage = "";
                var coeff= 1;
                var key="";
                var path = "";

                var id = hcObj.formId;
                var collection = "forms";
                var timestamps = <?= json_encode(time()) ?>;
                if(action=="add"){
                    if(button.data("value") == "all")
                        path = "params.criterias"+hcObj.key+".criteria"+timestamps;
                    else if(button.data("value") == "me" ){
                        path = "answers.criterias"+hcObj.key+".criteria"+timestamps;
                        id = hcObj.answerId;
                        collection = "answers";
                        cuserId = userId;
                        fromAnswerId = hcObj.answerId;
                    }
                }else if(action=="edit"){
                    buttonLabel = "Modifier";
                    label = button.data('label');
                    usage = button.data('usage');
                    coeff= button.data('coeff');
                    rang = button.data('key');
                    if(button.data("value") == "all")
                        path = "params.criterias"+hcObj.key+"."+rang
                    else if(button.data("value") == "me" ){
                        path = "answers.criterias"+hcObj.key+"."+rang;
                        id = button.data('answer-id');
                        collection = "answers";
                        cuserId = button.data("user-id");
                        fromAnswerId = button.data("answer-id");
                    }
                }else if(action=="delete"){
                    buttonLabel = "Supprimer";
                    rang = button.data('key');
                    if(button.data("value") == "all")
                        path = "params.criterias"+hcObj.key+"."+rang
                    else if(button.data("value") == "me" ){
                        path = "answers.criterias"+hcObj.key+"."+rang;
                        id = button.data('answer-id');
                        collection = "answers";
                    }
                }
                
                var dialog = bootbox.dialog({
                    title: `<h6 class="text-center"> ${buttonLabel} une ligne</h6>`,
                    message:   action=="delete" ? `<h6>${trad.confirmdelete}</h6>` : 
                                `<div class="form-group">
                                    <label for="name-criteria">${hcObj.columnLabel['criteriaColumn']}</label>
                                    <input type="text" class="form-control" id="name-criteria" value="${label}">
                                </div>
                                <div class="form-group ${(notEmpty(hcObj.configuration.usageColumn) && hcObj.configuration.usageColumn) ? '' : 'hidden' }" >
                                    <label for="usage-criteria">${hcObj.columnLabel['usageColumn']}</label>
                                    <input type="text" class="form-control" id="usage-criteria" value="${usage}">
                                </div>
                                <div class="form-group hidden">
                                    <label for="coeff-criteria">Coefficient:</label>
                                    <input type="number" class="form-control" id="coeff-criteria" value="${coeff}">
                                </div>`,
                    size: 'small',
                    centerVertical: true,
                    buttons: {
                        cancel: {
                            label: "Annuler",
                            className: 'btn-danger',
                            callback: function(){
                                console.log('Custom cancel clicked');
                            }
                        },
                        ok: {
                            label: buttonLabel,
                            className: 'btn-info',
                            callback: function(){
                                var isExistsCriteria = hcObj.checkExistsCriteria(hcObj,$('#name-criteria').val());
                                if(isExistsCriteria && action == "add"){
                                    toastr.error(trad["This criterion already exists"]);
                                    $('.add-line-<?= $kunik ?>').trigger('click');
                                    var val = $('#name-criteria').val();
                                    setTimeout(() => {
                                        $('#name-criteria').val(val).focus();
                                    }, 600);
                                   
                                }else{
                                    var accept = false;
                                    if(action == "delete") accept = true;
                                    if((action == "edit" || action == "add")  && $('#name-criteria').val() != "" ) accept = true;
                                    if(accept){
                                        var parameters = {
                                            id:id,
                                            collection:collection,
                                            path: path,
                                            value: action=="delete" ? null : {
                                                label : $('#name-criteria').val(),
                                                coeff: notEmpty($('#coeff-criteria').val()) ? $('#coeff-criteria').val() : 1,
                                                usage: $('#usage-criteria').val(),
                                                me : button.data("value") == "me" ? true : false,
                                                userId : cuserId,
                                                fromAnswerId : fromAnswerId
                                                
                                            },
                                            setType : [
                                                {
                                                    "path": "coeff",
                                                    "type": "int"
                                                },
                                                {
                                                    "path": "me",
                                                    "type": "boolean"
                                                }
                                            ]
                                        }
                                        dataHelper.path2Value(
                                            parameters,function(prms){
                                                if(action == "add"){
                                                    var tplCtx = {
                                                        id : hcObj.answerId,
                                                        collection : "answers",
                                                        path : "answers.yesOrNo"+hcObj.key+".criteria"+timestamps+".yesOrNo",
                                                        value : true,
                                                        format : true,
                                                        setType : "boolean"
                                                    }
                                                    dataHelper.path2Value(tplCtx, function (params) {})
                                                }
                                                reloadInput(hcObj.key,"<?= $form['id'] ?>"); 
                                            }
                                        )
                                    }else 
                                        toastr.error("Le champs critère est vide")
                                }
                            }
                        }
                    }
                });
                dialog.on('shown.bs.modal', function(e){
                    hcObj.autocompletionUsage(hcObj);
                });
            },
            sumCoeff : function(hcObj){
                var sumCoeff = 0;
                $.each(hcObj.criteriasMerge,function(k,v){
                    if(exists(v.coeff)){
                        sumCoeff += parseInt(v.coeff);
                    }
                })
                return sumCoeff;
            },
            // mean : function(hcObj){
            //     var meanStar = 0;
            //     var totalStar = 0;

            //     var yn = {
            //         countyes : 0, countno : 0,
            //         noteYesWithCoeff:0, noteNoWithCoeff:0
            //     }

            //     $.each(hcObj.smiley,function(k,v){
            //         hcObj.smiley[k].ansCount = 0;
            //         hcObj.smiley[k].noteWithCoeff = 0;
            //     })

            //     $.each(hcObj.myAnswer,function(k,v){ //get count
            //         if(exists(v.note) && exists(hcObj.criteriasMerge[k]) && exists(hcObj.criteriasMerge[k]["coeff"])){
            //             totalStar += parseInt(v.note)*hcObj.criteriasMerge[k]["coeff"];
            //         }

            //         if(notEmpty(v.happiness)){
            //             if(exists(hcObj.smiley[v.happiness]) && exists(hcObj.criteriasMerge[k]) && exists(hcObj.criteriasMerge[k]["coeff"])){
            //                 hcObj.smiley[v.happiness].ansCount++;
            //             }
            //         }else{
            //             hcObj.smiley["neutral"].ansCount++;
            //         }

            //         if(notEmpty(v.yesOrNo) && v.yesOrNo){
            //             yn.countyes++;
            //         }else{
            //             yn.countno++;
            //         }
            //     })

            //     $.each(hcObj.myAnswer,function(k,v){ // get note with coeff
            //         if(notEmpty(v.happiness)){
            //             if(exists(hcObj.smiley[v.happiness]) && exists(hcObj.criteriasMerge[k]) && exists(hcObj.criteriasMerge[k]["coeff"])){
            //                 hcObj.smiley[v.happiness].noteWithCoeff += hcObj.smiley[v.happiness].ansCount * hcObj.criteriasMerge[k]["coeff"];
            //             }
            //         }else{
            //             if(exists(hcObj.smiley[v.happiness]) && exists(hcObj.criteriasMerge[k]) && exists(hcObj.criteriasMerge[k]["coeff"])){
            //                 hcObj.smiley["neutral"].noteWithCoeff += hcObj.smiley["neutral"].ansCount * hcObj.criteriasMerge[k]["coeff"];
            //             }
            //         }

            //         if(notEmpty(v.yesOrNo) && v.yesOrNo){
            //             if(exists(hcObj.smiley[v.happiness]) && exists(hcObj.criteriasMerge[k]) && exists(hcObj.criteriasMerge[k]["coeff"])){
            //                 yn.noteYesWithCoeff += yn.countyes * hcObj.criteriasMerge[k]["coeff"];
            //             }
            //         }else{
            //             if(exists(hcObj.smiley[v.happiness]) && exists(hcObj.criteriasMerge[k]) && exists(hcObj.criteriasMerge[k]["coeff"])){
            //                 yn.noteNoWithCoeff += yn.countno * hcObj.criteriasMerge[k]["coeff"];
            //             }
            //         }
            //     })

            //     /**stat****************************/
            //     if(hcObj.sumCoeff(hcObj)==0 )
            //         meanStar = 0;
            //     else
            //         meanStar = totalStar/hcObj.sumCoeff(hcObj);

            //     meanStar = Math.round(meanStar * 100) / 100;
            //     $('.mean-star-<?= $kunik ?>').html(meanStar +'/ 5');

            //     /**smiley ********************** */
            //     var max = hcObj.smiley["neutral"];
            //     $.each(hcObj.smiley,function(k,v){
            //         if(v.noteWithCoeff > max.noteWithCoeff){
            //             max = hcObj.smiley[k];
            //         }
            //     })
            //     mylog.log(hcObj.smiley,"hcObj.smiley",max);
            //     $('.mean-smiley-<?= $kunik ?>').html(max.dec);

            //     /*yes no*************************/
            //     var maxyn = trad.no;
            //     if(yn.noteYesWithCoeff > yn.noteNoWithCoeff){
            //         maxyn = trad.yes;
            //     }
            //     $('.mean-yesno-<?= $kunik ?>').html(maxyn);

            // },
            config : function(hcObj){
                $('.config<?= $kunik ?>').on('click',()=>{
                    if(hcObj.canEditForm){
                        var config = {
                            jsonSchema :{
                                title : "Configuration",
                                description : "",
                                properties : {
                                    "criteriaColumn":{
                                        inputType:"checkboxSimple",
                                        label : "Activer colone ("+ hcObj.columnLabel['criteriaColumn']+")",
                                        params : {
                                            "onText" : trad.yes,
                                            "offText" : trad.no,
                                            "onLabel" : trad.yes,
                                            "offLabel" : trad.no
                                        },
                                        checked:false
                                    },
                                    "usageColumn":{
                                        inputType:"checkboxSimple",
                                        label : "Activer colone ( "+hcObj.columnLabel['usageColumn']+")",
                                        params : {
                                            "onText" : trad.yes,
                                            "offText" : trad.no,
                                            "onLabel" : trad.yes,
                                            "offLabel" : trad.no
                                        },
                                        checked:false
                                    },
                                    "humourColumn":{
                                        inputType:"checkboxSimple",
                                        label : "Activer colone ( "+hcObj.columnLabel['humourColumn']+")",
                                        params : {
                                            "onText" : trad.yes,
                                            "offText" : trad.no,
                                            "onLabel" : trad.yes,
                                            "offLabel" : trad.no
                                        },
                                        checked:false
                                    },
                                    "starColumn":{
                                        inputType:"checkboxSimple",
                                        label : "Activer colone ( "+hcObj.columnLabel['starColumn']+")",
                                        params : {
                                            "onText" : trad.yes,
                                            "offText" : trad.no,
                                            "onLabel" : trad.yes,
                                            "offLabel" : trad.no
                                        },
                                        checked:false
                                    },
                                    "yesNoColumn":{
                                        inputType:"checkboxSimple",
                                        label : "Activer colone ( "+hcObj.columnLabel['yesNoColumn']+")",
                                        params : {
                                            "onText" : trad.yes,
                                            "offText" : trad.no,
                                            "onLabel" : trad.yes,
                                            "offLabel" : trad.no
                                        },
                                        checked:false
                                    },
                                    "commentColumn":{
                                        inputType:"checkboxSimple",
                                        label : "Activer colone commentaire",
                                        params : {
                                            "onText" : trad.yes,
                                            "offText" : trad.no,
                                            "onLabel" : trad.yes,
                                            "offLabel" : trad.no
                                        },
                                        checked:false
                                    },
                                    /*"usage":{
                                        inputType:"checkboxSimple",
                                        label : "Usage",
                                        params : {
                                            "onText" : trad.yes,
                                            "offText" : trad.no,
                                            "onLabel" : trad.yes,
                                            "offLabel" : trad.no
                                        },
                                        checked:false
                                    }*/
                                },
                                onLoads:{
                                    onload : function(){}
                                },
                                save : function(formData){
                                    delete formData.collection;
                                    var tplCtx = {
                                        id : hcObj.formId,
                                        collection: "forms",
                                        value: formData,
                                        path: "params.config"+hcObj.key,
                                        setType:[
                                            {
                                                "path": "usageColumn",
                                                "type": "boolean"
                                            },
                                            {
                                                "path": "criteriaColumn",
                                                "type": "boolean"
                                            },
                                            {
                                                "path": "humourColumn",
                                                "type": "boolean"
                                            },
                                            {
                                                "path": "starColumn",
                                                "type": "boolean"
                                            },
                                            {
                                                "path": "yesNoColumn",
                                                "type": "boolean"
                                            },
                                            {
                                                "path": "commentColumn",
                                                "type": "boolean"
                                            }
                                        ]
                                    };
                                    if (typeof tplCtx.value == "undefined")
                                        toastr.error('value cannot be empty!');
                                    else {
                                        dataHelper.path2Value(tplCtx, function (params) {
                                            dyFObj.closeForm();
                                            reloadInput(hcObj.key,"<?= $form['id'] ?>");
                                        });
                                    }
                                }
                            }
                        }
                        dyFObj.openForm(config,null,hcObj.configuration);
                    }
                })
            },
            editColumnLabel : function(hcObj){
                if(hcObj.canEditForm){
                    $(".editable-label-"+hcObj.key).on('blur',function(){
                        var path =  $(this).data("id");
                        var label = $(this).text();
                        var tplCtx = {
                            id : hcObj.formId,
                            collection: "forms",
                            value: label,
                            path: "params.columnLabel"+hcObj.key+"."+path,
                        };
                        if (typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value(tplCtx, function (params) {
                                //reloadInput(hcObj.key,"<?= $form['id'] ?>");
                            });
                        }
                    })
                }

            },
            checkExistsCriteria : function(hcObj,scope){
                var isExistsCriteria = false;
                hcObj.getAllAnswers(hcObj,function(){
                    $.each(hcObj.allAnswers,function(k,v){
                        if(notEmpty(v.answers) && notEmpty(v.answers["criterias"+hcObj.key])){
                            $.each(v.answers["criterias"+hcObj.key],function(kcrit,vcrit){
                                //if(vcrit.label.toLowerCase().localeCompare(scope.toLowerCase(), 'en', { sensitivity: 'base' }) === 0){
                                if(vcrit.label.toLowerCase() == scope.toLowerCase()){
                                    isExistsCriteria = true;
                                    mylog.log(scope+" - "+vcrit.label,"comparisons",vcrit.label.toLowerCase().localeCompare(scope.toLowerCase(), 'en', { sensitivity: 'base' }));
                                    return false;
                                }
                            })
                        }
                    })
                })
                return isExistsCriteria;
            },
            toolsUsers : function(hcObj){
                $.each(hcObj.allAnswers,function(k,v){
                    if(!hcObj.allToolsUsers.includes(v.user)){
                        hcObj.allToolsUsers.push(v.user);// get user
                    }

                    $.each(hcObj.criteriasMerge,function(kcrit,vcrit){
                        if(!exists(hcObj.criteriasMerge[kcrit]["usersYes"])){
                            hcObj.criteriasMerge[kcrit]["usersYes"] = [];
                        }

                        if(notEmpty(v.answers) && notEmpty(v.answers["yesOrNo"+hcObj.key][kcrit]) && notEmpty(v.answers["yesOrNo"+hcObj.key][kcrit]["yesOrNo"]) && v.answers["yesOrNo"+hcObj.key][kcrit]["yesOrNo"]){
                            if(exists(hcObj.criteriasMerge[kcrit]["usersYes"])){
                                hcObj.criteriasMerge[kcrit]["usersYes"].push(v.user);
                            }
                        }
                    })
                })

                $('#tools-users-counter-<?= $key ?>').html(`<p class="text-green-k">${hcObj.allToolsUsers.length} utilisateurs utilisent cet outil</p>`)
            },
            autocompletionUsage : function(hcObj){
            hcObj.addedUsage = hcObj.addedUsage.sort();
            $("#usage-criteria").autocomplete({
                source: hcObj.addedUsage,
            });
        },
        }
        yesOrNoObj.init(yesOrNoObj);
    })
</script>
<?php 
}
?>