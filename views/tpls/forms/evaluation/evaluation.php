
<style>
    #tbody<?=$key ?> tr.principal-<?=$key ?> td{
        vertical-align: middle;
        cursor: pointer;
    }
    #tbody<?=$key ?> tr td select{
        border:0;
        border-radius: 0;
        cursor: pointer;
    }
    select::-ms-expand {
        display: none;
    }
    td select {
    /* for Firefox */
    -moz-appearance: none;
    /* for Chrome */
    -webkit-appearance: none;
    }

        /* Style the form */
    #regForm {
    background-color: #ffffff;
    margin: 100px auto;
    padding: 40px;
    width: 70%;
    min-width: 300px;
    }

    /* Style the input fields */
    /*input {
    padding: 10px;
    width: 100%;
    font-size: 17px;
    font-family: Raleway;
    border: 1px solid #aaaaaa;
    }*/

    /* Mark input boxes that gets an error on validation: */
    .to-validate.invalid {
    background-color: #ffdddd;
    }

    /* Hide all steps by default: */
    .tab {
    display: none;
    }

    /* Make circles that indicate the steps of the form: */
    .step {
    height: 15px;
    width: 15px;
    margin: 0 2px;
    background-color: #bbbbbb;
    border: none;
    border-radius: 50%;
    display: inline-block;
    opacity: 0.5;
    }

    /* Mark the active step: */
    .step.active {
    opacity: 1;
    }

    /* Mark the steps that are finished and valid: */
    .step.finish {
    background-color: #04AA6D;
    }
    .btn-group-criteria,.edit-category-number-<?=$key ?><?=$key ?>,.edit-label-criteria<?=$key ?>,.delete-category-level<?=$key ?>,.edit-category-level<?=$key ?>{
        display: none;
    }
    th:hover .btn-group-criteria{
        display: flex;
    }
    th:hover .edit-category-number-<?=$key ?><?=$key ?>,th:hover .edit-label-criteria<?=$key ?>,td:hover .delete-category-level<?=$key ?>,td:hover .edit-category-level<?=$key ?>{
        display: inline;
    }
    #table1<?=$key ?> td, #table1<?=$key ?> th{
        text-align: center;
    }
    @media screen and (max-width: 500px){
        .table-responsive{
            margin-bottom: 15px;
            overflow-y: hidden;
            overflow-x: scroll;
            width: 99%;
            margin-left:2px
        }
        .coFormbody,#wizardcontainer,.bg-contain,#wizardcontainer>.col-xs-12{
            padding-left:0;
            padding-right: 0;
        }
    }
    .evaluation<?=$kunik ?> .select2-container{
        padding: 0 !important;
        border: 0 !important;
    }
    /*.evaluation<?=$kunik ?> .select2-container-multi .select2-choices{
        height: 100% !important;
    }*/
    /*.evaluation<?=$kunik ?> .select2-search-field{
        display: none !important;
    }*/
    .evaluation<?=$kunik ?> .select2-container-multi:not(.aapstatus-container) .select2-choices .select2-search-choice{
        margin: 0 !important;
        padding: 6px 20px !important;
    }
    .select2-drop-multi{
        width: 206px !important;
    }
    .select2-results li{
        font-size: 14px !important;
    }
    .evaluation<?=$kunik ?> .form-group input[type="checkbox"] {
        display: none;
    }

    .evaluation<?=$kunik ?> .form-group input[type="checkbox"] + .btn-group > label span {
        width: 20px;
    }

    .evaluation<?=$kunik ?> .form-group input[type="checkbox"] + .btn-group > label span:first-child {
        display: none;
    }
    .evaluation<?=$kunik ?> .form-group input[type="checkbox"] + .btn-group > label span:last-child {
        display: inline-block;   
    }

    .evaluation<?=$kunik ?> .form-group input[type="checkbox"]:checked + .btn-group > label span:first-child {
        display: inline-block;
    }
    .evaluation<?=$kunik ?> .form-group input[type="checkbox"]:checked + .btn-group > label span:last-child {
        display: none;   
    }
</style>
<?php 
if(isset(Yii::app()->session['userId']) && Yii::app()->session['userId']){
    //$answer = PHDB::findOne(Form::ANSWER_COLLECTION,array("form"=>(string)$parentForm["_id"],"user" =>Yii::app()->session['userId']));
    HtmlHelper::registerCssAndScriptsFiles(["/js/appelAProjet/row-merge-bundle.min.js"], Yii::app()->getModule('costum')->assetsUrl);
    //HtmlHelper::registerCssAndScriptsFiles(["/js/appelAProjet/jquery.rowspanizer.js"], Yii::app()->getModule('costum')->assetsUrl);
    $data = !empty($parentForm["params"]["categories".$key]) ? $parentForm["params"]["categories".$key] : [];
    $isConfidential = !empty($parentForm["params"]["isConfidential".$key]) ? $parentForm["params"]["isConfidential".$key] : false;
    $criteria = !empty($parentForm["params"]["criterias".$key]) ? $parentForm["params"]["criterias".$key] : [];
    $answers = [];
    if($isConfidential){
        $answer = PHDB::findOne(Form::ANSWER_COLLECTION,array("form"=>(string)$parentForm["_id"],"user" =>Yii::app()->session['userId']));
        $answers = !empty($answer["answers"]["evaluation".$key]) ? $answer["answers"]["evaluation".$key] : [];
    }else
        $answers = !empty($answer["answers"]["evaluation".$key]) ? $answer["answers"]["evaluation".$key] : [];

    $voteType = "colour";
    $colours = ["OK" => "#9fbd38","NotOK" => "#D7193B"];
    $criteriaLabel = !empty($parentForm["params"]["criteriaLabel".$key]) ? $parentForm["params"]["criteriaLabel".$key] : "Changer le nom";
    $categoryNumber = !empty($parentForm["params"]["categoryNumber".$key]) ? (int)$parentForm["params"]["categoryNumber".$key] : 1;
    $categoryTitle = !empty($parentForm["params"]["categoryTitle".$key]) ? $parentForm["params"]["categoryTitle".$key] : "Changer le label";
    $multiVotePerLine = !empty($parentForm["params"]["multiVotePerLine".$key]) ? $parentForm["params"]["multiVotePerLine".$key] : false;
    $admin = Form::canAdmin((string)$parentForm["_id"]);
    $modeEdit = ($mode == "fa") ? true : false;
    $modeResponse = ($mode == "w") ? true :false; 
    $modePdf = ($mode == "r" || $mode == "pdf") ? true :false; 
?>
<div class="no-padding container-fluid evaluation<?=$kunik ?>">
    <!-- <div class="row text-center">
        <h6>EVALUATION DES DÉGATS - DIAGNOSTIC TEMPS - Pour Soi</h6>
        <p>Comment dépenses tu ton temps ?</p>
    </div> -->

    <?php if($admin && $modeEdit){ ?>
        <label for="<?php echo $key ?>">
            <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>" class="<?= $canEditForm && $mode="fa" ? "" : $type ?>">
                <?php echo $label.$editQuestionBtn ?>
                <?php if($canEditForm && $mode="fa"){ ?>
                        <a href="javascript:;" class="btn btn-xs btn-danger config<?= $kunik ?>"><i class="fa fa-cog"></i></a>
                    <?php } ?>
            </h4>
        </label>
        <div class="form-group pull-right" id="confidential-response-<?= $key ?>">
            <input type="checkbox" <?= $isConfidential ? "checked" :"" ?>  name="confidential-checkbox-response-<?= $key ?>" id="confidential-checkbox-response-<?= $key ?>" autocomplete="off" />
            <div class="btn-group">
                <label for="confidential-checkbox-response-<?= $key ?>" class="btn btn-default">
                    <span class="glyphicon glyphicon-ok"></span>
                    <span> </span>
                </label>
                <label for="confidential-checkbox-response-<?= $key ?>" class="btn btn-default active">
                <?php echo Yii::t("common","Confidential response") ?>
                </label>
            </div>
        </div>
    <?php }else{ ?>
                <label for="<?php echo $kunik ?>">
                <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
                    <?php echo $label ?>
                </h4>
            </label><br/>
    <?php } ?>
    <?php if(!empty($info)){ ?>
        <small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
    <?php } ?>
    <div class="row">
        <?php if($admin && $modeEdit){ ?>
            <!-- <div class="col-xs-12 col-md-4 padding-bottom-25 margin-top-20">
                <div class="col-xs-12">
                    <button type="button" class="btn btn-success config-evaluation<?=$key ?> hidden"><i class="fa fa-cogs"></i></button>
                    <a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        Config
                    </a>
                    
                    <div class="collapse" id="collapseExample">
                        <div class="col-xs-12">
                            <div class="checkbox">
                                <label>
                                <input type="checkbox" class="multiple-evaluation-per-line<?=$key ?>" <?= $multiVotePerLine ? "checked":"" ?> > Multiple evaluation par ligne
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-5">
                    <label for="">Type d'évaluation</label>
                    <select class="form-control" name="" id="">
                        <option value="couleur">Couleur</option>
                        <option value="emoji">Emoji</option>
                        <option value="note">Note</option>
                        <option value="star">Etoile</option>
                    </select>
                </div>
            </div> -->
        <?php } ?>
    </div>
    <div class="row">
        <div class="col-xs-12 no-padding margin-top-20">
            <div class="table-responsivee">
                <table class="table table-bordered table-stripped" id="table1<?=$key ?>" style="font-size: 15px;">
                    <thead id="thead<?=$key ?>"></thead>
                    <tbody id="tbody<?=$key ?>">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-5 add-line-form">
                
        </div>
    </div>
</div>
<script>
$(function () {
    var descHtml<?php echo $key ?> = dataHelper.markdownToHtml($("#<?php echo $key ?>Help").html(),{
        parseImgDimensions:true,
        simplifiedAutoLink:true,
        strikethrough:true,
        tables:true,
        openLinksInNewWindow:true
    });
    $("#<?php echo $key ?>Help").html(descHtml<?php echo $key ?>);
    function flattenObject (ob){
        var toReturn = {};
        for (var i in ob) {
            if (!ob.hasOwnProperty(i)) continue;

            if ((typeof ob[i]) == 'object' && ob[i] !== null) {
                var flatObject = flattenObject(ob[i]);
                for (var x in flatObject) {
                    if (!flatObject.hasOwnProperty(x)) continue;

                    toReturn[i + '.' + x] = flatObject[x];
                }
            } else {
                toReturn[i] = ob[i];
            }
        }
        mylog.log(toReturn,"datakoo");
        return toReturn;
    };

    var evaluationObj<?=$key?> = {
        data : <?= json_encode($data) ?>,
        flattenData : null,
        flattenRealPath:[],
        criteria : <?= json_encode($criteria) ?>,
        criteriaLabel : <?= json_encode($criteriaLabel) ?>,
        answers : <?= json_encode($answers) ?>,
        voteType : <?= json_encode($voteType) ?>,
        colours : <?= json_encode($colours) ?>,
        answerId : <?= json_encode((string)$answer["_id"]) ?>,
        formId : <?= json_encode((string)$parentForm["_id"]) ?>,
        categoryNumber : <?= json_encode($categoryNumber) ?>,
        categoryTitle : <?= json_encode($categoryTitle) ?>,
        newCategoriesPath : "",
        admin : <?= json_encode($admin) ?>,
        modeEdit : <?= json_encode($modeEdit) ?>,
        modeResponse : <?= json_encode($modeResponse) ?>,
        multiVotePerLine: <?= json_encode($multiVotePerLine) ?>,
        isConfidential: <?= json_encode($isConfidential) ?>,
        init: function(eObj){
            eObj.flattenData = flattenObject(eObj.data);
            eObj.createTable(eObj);
            mylog.log(eObj.flattenRealPath,"flattenData");
            eObj.events(eObj);
            eObj.addBackgroundByCategorie(eObj);
            eObj.autocompletion(eObj);
        },
        flattenObject : function(eObj,ob){
            var toReturn = {};
            for (var i in ob) {
                if (!ob.hasOwnProperty(i)) continue;

                if ((typeof ob[i]) == 'object' && ob[i] !== null) {
                    var flatObject = this.flattenObject(ob[i]);
                    for (var x in flatObject) {
                        if (!flatObject.hasOwnProperty(x)) continue;

                        toReturn[i + '.' + x] = flatObject[x];
                    }
                } else {
                    toReturn[i] = ob[i];
                }
            }

            mylog.log(toReturn,"datakoo");
            return toReturn;
        },
        parseDataFromCriteria : function(eObj){
            $.each(eObj.flattenData,function(k,v){
                k = k.split('.');
                var path = "";
                $.each(k,function(kk,vv){
                    path+="."+vv;
                    var value = jsonHelper.getValueByPath(eObj.data,path.substring(1));
                    
                    if(Array.isArray(value) && path.substring(1).split('.').length == eObj.categoryNumber){
                        var answers = jsonHelper.getValueByPath(eObj.answers,path.substring(1));
                        mylog.log(answers,"answersko");
                        var newPath = path+"."+Object.keys(eObj.criteria).join('.');
                        //var newPath = path+"."+Object.keys(answers).join('.');
                        newPath = newPath.substring(1);
                        eObj.flattenData[newPath] = "";
                        delete eObj.flattenData[k.join('.')];
                    }else{
                        delete eObj.flattenData[k.join('.')]
                    }
                })
            })
            return eObj.flattenData;
        },
        createTable : function(eObj){
            var table = "";
            var data = eObj.parseDataFromCriteria(eObj);
            mylog.log(data,"realPath 1");
            $.each(data,function(k,v){
                k = k.split('.');
                table += `<tr class="principal-<?=$key ?>">`;
                $.each(k,function(kk,vv){
                    var realPath = "";
                    var temp = Object.keys(eObj.criteria).join(".");
                    realPath = k.join('.').replace(temp,'').slice(0, -1);
                    mylog.log(realPath,"realPath");
                    
                    if(exists(eObj.criteria[vv])){
                        if(!eObj.flattenRealPath.includes(realPath)){
                            eObj.flattenRealPath.push(realPath); // init real path
                        }
                        mylog.log(eObj.answers,realPath+"."+vv,"handeha")
                        var current =  jsonHelper.getValueByPath(eObj.answers,realPath+"."+vv)
                        if(eObj.voteType == "colour"){
                            table += eObj.colour.td(eObj,vv,realPath,k.join('.'),current);
                        }
                    }else{
                        table += 
                        `<td data-value="${escapeHtml(vv)}"  data-path="${escapeHtml(realPath)}" data-long-path="${escapeHtml(k.join('.'))}">
                            <span>${vv}</span>`;
                            if(eObj.admin && eObj.modeEdit){
                    table +=    `<button class="btn btn-xs btn-danger delete-category-level<?=$key ?>" type="button" data-value="${escapeHtml(vv)}"  data-path="${escapeHtml(realPath)}" data-long-path="${escapeHtml(k.join('.'))}"><i class="fa fa-trash"></i></button>`;
                    table +=    `<button class="btn btn-xs btn-defauly edit-category-level<?=$key ?>" type="button" data-value="${escapeHtml(vv)}"  data-path="${escapeHtml(realPath)}" data-long-path="${escapeHtml(k.join('.'))}"><i class="fa fa-edit"></i></button>`;
                }
            table +=    `</td>`;
                    }  
                })
                table += `</tr>`;
            });
            if(eObj.admin && eObj.modeEdit){
                table += 
                    `<tr class="bg-dark" style="height:83px">`;
                    for (let index = 1; index <= eObj.categoryNumber; index++) {
        table +=       `<td>
                            <input type="text" class="form-control input-category<?= $key ?> category<?= $key ?>${index}" data-index=${index} />
                        </td>`;
                    }
    table +=       `<td colspan="${Object.keys(eObj.criteria).length}">
                            <button type="button" class="btn btn-success btn-block add-new-line<?= $key ?>" disabled>Ajouter une ligne</button>
                        </td>
                    </tr>`;
            }

            $("#tbody<?=$key ?>").append(table);

            //$("#table1").rowspanizer({vertical_align: 'middle'});
            var excludeColumns = [];
            var count = eObj.categoryNumber + Object.keys(eObj.criteria).length;
            for (let index = (eObj.categoryNumber+1); index <= count; index++) {
                excludeColumns.push(index);
            }
            var table = $('#table1<?=$key ?>').rowMerge({
                excludedColumns: excludeColumns,
            });
            table.merge();//table.unmerge();
            eObj.createHead(eObj);
            //$('[rowspan]').find("span").css({"transform": "rotate(297deg)","display":"block"});
            $('[rowspan]').css("width", "80px");
            
        },
        createHead : function(eObj){
            var thead = `<tr>
                <th colspan="${eObj.categoryNumber}">&nbsp;</th>
                <th colspan="${Object.keys(eObj.criteria).length}" class="text-center" style="position:relative">
                    ${eObj.criteriaLabel}`;
                    if(eObj.admin && eObj.modeEdit){
            thead +=    `<button class="btn btn-xs btn-default edit-label-criteria<?=$key ?>" type="button"><i class="fa fa-pencil"></i></button>`
                    }
        thead +=    `</th>
                </tr>`;

            thead += `<tr>`;
            //for (let index = 0; index < eObj.categoryNumber; index++) {
                //thead += `<th>Category niv ${index+1}</th>`;
                thead += `<th class="text-center" colspan="${eObj.categoryNumber}">
                        <h5 class="no-padding no-margin">
                    ${eObj.categoryTitle}`;
                    if(eObj.admin && eObj.modeEdit){
                        thead +=    `<button class="btn btn-xs btn-default edit-category-number-<?=$key ?><?=$key ?>" type="button"><i class="fa fa-pencil"></i></button>`
                    }
                thead += `</h5>
                        </th>`;
            //}
            if(Object.keys(eObj.criteria).length == 0){
                thead += `<th style="position: relative;">&nbsp;`;
                if(eObj.admin && eObj.modeEdit){
                    thead += `<button type="button" class="add-criteria<?=$key ?> btn btn-success" style="position: absolute;left: 100%;top: 0;bottom: 0;border-radius: 0px;" data-toggle="tooltip" data-placement="top" data-original-title="Ajouter un critère"><i class="fa fa-plus"></i></button>`;
                }          
                thead += `</th>`;
            }
            $.each(eObj.criteria,function(k,v){
                if(k == (Object.keys(eObj.criteria)[Object.keys(eObj.criteria).length - 1])){
                    thead += `<th style="position: relative;" data-criteria="${k}" data-coeff="${v}">${eObj.criteria[k]["name"]}`;
                    if(eObj.admin && eObj.modeEdit){
                        thead += `<button type="button" class="add-criteria<?=$key ?> btn btn-success" style="position: absolute;left: 100%;top: 0;bottom: 0;border-radius: 0px;" data-toggle="tooltip" data-placement="top" data-original-title="Ajouter un critère"><i class="fa fa-plus"></i></button>`;
                        thead += `<div class="btn-group btn-group-criteria" style="position:absolute;bottom: 100%;">
                            <button type="button" class="btn btn-default btn-xs edit-criteria<?=$key ?>" data-criteria="${k}" data-name="${eObj.criteria[k]["name"]}" type="button"  data-toggle="tooltip" data-placement="top" data-original-title="Editer"><i class="fa fa-edit"></i></button>
                            <button type="button" class="btn btn-danger btn-xs delete-criteria<?=$key ?>" data-criteria="${k}" data-name="${eObj.criteria[k]["name"]}" type="button"  data-toggle="tooltip" data-placement="top" data-original-title="Supprimer"><i class="fa fa-times"></i></button>
                        </div>`;
                    }          
                    thead += `</th>`;
                }else{
                    thead += `<th  style="position: relative" data-criteria="${k}" data-coeff="${v}">${eObj.criteria[k]["name"]}`;
                    if(eObj.admin && eObj.modeEdit){
                        thead += `<div class="btn-group btn-group-criteria" style="position:absolute;bottom: 100%;">
                            <button type="button" class="btn btn-default btn-xs edit-criteria<?=$key ?>" data-criteria="${k}" data-name="${eObj.criteria[k]["name"]}" type="button"  data-toggle="tooltip" data-placement="top" data-original-title="Editer"><i class="fa fa-edit"></i></button>
                            <button type="button" class="btn btn-danger btn-xs delete-criteria<?=$key ?>" data-criteria="${k}" data-name="${eObj.criteria[k]["name"]}" type="button"  data-toggle="tooltip" data-placement="top" data-original-title="Supprimer"><i class="fa fa-times"></i></button>
                        </div>`;
                    } 
                    thead += `</th>`;
                }
            })
            thead += `</tr>`;
            
            $("#thead<?=$key ?>").append(thead);
        },
        colour : {
            td : function(eObj,value,realPath,longPath,current){
                /*var td = "";
                td += `<td class="bold"  data-value="${value}" data-path="${realPath}"  data-long-path="${longPath}" style="background-color:${eObj.colours[current]}">
                        <select  class="form-control evaluation-field" name="" id="" data-value="${value}" data-path="${realPath}"  data-long-path="${longPath}" style="background-color:${eObj.colours[current]}">
                            <option value="">--</option>`;
                           $.each(eObj.colours,function(k,v){
                                td +=  `<option ${current == k ? 'selected':"" } style="background-color:${v} "value="${k}">${k}</option>`;
                           });
                td +=   `</select>
                    </td>`;
                    return td;*/
                    var td = "";
                td += `<td class="bold change-color change-color<?=$key ?> ${typeof current !="undefined" ? "selected":""}" data-color="${typeof eObj.colours[current] != 'undefined' ? eObj.colours[current] : '#fff'}"  data-criteriaId="${value}" data-path="${escapeHtml(realPath)}"  data-long-path="${escapeHtml(longPath)}" style="background-color:${eObj.colours[current]}">
                        ${typeof current !="undefined" ? "O":""}
                    </td>`;
                    return td;
            },
            /*persistance : function(eObj){
                if(eObj.multiVotePerLine){
                    $('.evaluation-field').off().on("change",function(){
                        var select = $(this);
                        var params = {
                            id : eObj.answerId,
                            collection:"answers",
                            path: "answers.evaluation<?= $key ?>."+select.data("path")+"."+select.data("value"),
                            value : select.val(),
                        }
                        dataHelper.path2Value( params, function(data) {
                            reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                        } );
                    })
                }else{
                    $('.evaluation-field').off().on("change",function(){
                        var select = $(this);
                        var params = {
                            id : eObj.answerId,
                            collection:"answers",
                            path: "answers.evaluation<?= $key ?>."+select.data("path"),
                            value : {},
                        }
                        $.each(eObj.criteria,function(k,v){
                            if(k == select.data("value")){
                                params.value[k] = select.val();
                            }else{
                                params.value[k] = "";
                            }
                        })
                        dataHelper.path2Value( params, function(data) {
                            reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                        } );
                    })
                }

            },*/
            addVote : function(eObj){
                if(eObj.modeResponse || eObj.modeEdit){
                    $('.change-color<?=$key ?>').on('click',function(){
                        var btn = $(this);
                        var params = {
                                id : eObj.answerId,
                                collection:"answers",
                                path: "answers.evaluation<?= $key ?>."+btn.data("path"),
                                value : {},
                            }
                        //if(btn.data('color') == "undefined"){
                            btn.css('background-color',eObj.colours["OK"])
                            $.each(eObj.criteria,function(k,v){
                                if(k == btn.data("criteriaid")){
                                    params.value[k+""] = Object.keys(eObj.colours)[0];
                                }
                            })
                            dataHelper.path2Value( params, function(data) {
                                var currentBg =  btn.attr("data-bg");
                                btn.parent().find('.change-color').text("").css('backgroundColor',currentBg);
                                btn.text('O').css('background-color','#9fbd38');
                                //reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                            } );
                        //}
                        /*else if(btn.data('color') == eObj.colours["OK"]){
                            btn.css('background-color',"white");
                            btn.text("")
                            $.each(eObj.criteria,function(k,v){
                                if(k == btn.data("criteriaid")){
                                    params.value[k+""] = "";
                                }
                            })
                            dataHelper.path2Value( params, function(data) {
                                //reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                            } );
                        }*/
                        /*else if(btn.data('color') == eObj.colours["NotOK"]){
                            $.each(eObj.criteria,function(k,v){
                                if(k == btn.data("criteriaid")){
                                    params.value[k+""] = Object.keys(eObj.colours)[0];
                                }
                            })
                            dataHelper.path2Value( params, function(data) {
                                reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                            } );
                        }*/
                    })
                }
            }
        },
        events : function(eObj){
            //$('#wizardcontainer').removeClass('container').addClass('container');
            $('[data-toggle="tooltip"]').tooltip();
            $('.add-new-line<?= $key ?>').on('click',function(){
                eObj.addNewLine(eObj);
            })
            $('.input-category<?= $key ?>').on('keyup',function(){
                setTimeout(() => {
                    if(eObj.activeButtonAddLine(eObj)){
                        $('.add-new-line<?= $key ?>').removeAttr('disabled');
                    }else{
                        $('.add-new-line<?= $key ?>').prop("disabled",true);
                    }         
                }, 500);
            })
            $('.add-criteria<?=$key ?>').on('click',function(){
                eObj.addNewCriteria(eObj,$(this));
            })
            $('.edit-criteria<?=$key ?>').on('click',function(){
                eObj.editDeleteCriteria(eObj,$(this),"edit");
            })
            $('.delete-criteria<?=$key ?>').on('click',function(){
                eObj.editDeleteCriteria(eObj,$(this),"delete");
            })
            $('.edit-label-criteria<?=$key ?>').on('click',function(){
                eObj.changeLabelCriteria(eObj);
            })
            $('.config-evaluation<?=$key ?>').on('click',function(){
                eObj.configuration(eObj);
            })
            $('.edit-category-number-<?=$key ?><?=$key ?>').on('click',function(){
                eObj.changeCategoryNumber(eObj);
            })
            $('.delete-category-level<?=$key ?>').on('click',function(){
                eObj.deleteCategoryLevel(eObj,$(this));
            })
            $('.edit-category-level<?=$key ?>').on('click',function(){
                eObj.editCategoryLevel(eObj,$(this));
            })
            $('.multiple-evaluation-per-line<?=$key ?>').on('click',function(){
                eObj.multipleVotePerLine(eObj,this);
            })
            eObj.colour.addVote(eObj);
            eObj.settingConfidentialResponse(eObj);
        },
        addNewLine : function(eObj){
            if(eObj.admin && eObj.modeEdit){
                var path = [];
                var allowAdd =  true;
                for (let index = 1; index <= eObj.categoryNumber; index++) {
                    mylog.log($('.category<?= $key ?>'+index).val());
                    path.push($('.category<?= $key ?>'+index).val());
                    if($('.category<?= $key ?>'+index).val().indexOf('.') != -1){
                        allowAdd = false;
                    }
                }
                if(!allowAdd)
                    return bootbox.alert("<p class='h4 text-danger'>Le nom de la categorie ne peut pas contenir un point (.)</p>");
                mylog.log(path,"pathko");
                dataHelper.path2Value(
                    {
                    id:eObj.formId,
                    collection:"forms",
                    path:"params.categories<?= $key ?>."+path.join('.'),
                    value:[""]
                    },function(prms){
                        reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                })
            }
        },
        addNewCriteria : function(eObj,button){
            if(eObj.admin && eObj.modeEdit){
                var dialog = bootbox.dialog({
                    title: '<h6 class="text-center">Ajouter un critère</h6>',
                    message:   `<div class="form-group">
                                    <label for="nameCriteria-<?=$key ?>">Nom du critère:</label>
                                    <input type="text" class="form-control" id="nameCriteria-<?=$key ?>">
                                </div>`,
                    size: 'small',
                    centerVertical: true,
                    buttons: {
                        cancel: {
                            label: "Annuler",
                            className: 'btn-danger',
                            callback: function(){
                                console.log('Custom cancel clicked');
                            }
                        },
                        ok: {
                            label: "Ajouter",
                            className: 'btn-info',
                            callback: function(){
                                if($('#nameCriteria-<?=$key ?>').val().indexOf('.') != -1){
                                    return bootbox.alert("<p class='h4 text-danger'>Le nom du critère ne peut pas contenir un point (.)</p>");
                                }
                                dataHelper.path2Value(
                                    {
                                        id:eObj.formId,
                                        collection:"forms",
                                        path:"params.criterias<?= $key ?>."+ (Object.keys(eObj.criteria).length != 0 ? (parseInt(Object.keys(eObj.criteria)[Object.keys(eObj.criteria).length-1])+1) : 1),
                                        value: {
                                            name : $('#nameCriteria-<?=$key ?>').val(),
                                            coeff: 1
                                        },
                                    },function(prms){
                                        reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                    }
                                )
                            }
                        }
                    }
                });
            }
        },
        editDeleteCriteria : function(eObj,button,action="edit"){
            if(eObj.admin && eObj.modeEdit){
                var labelButton = "Modifier";
                if(action == "delete"){
                    labelButton = "Supprimer";
                }
                var dialog = bootbox.dialog({
                    title: '<h6 class="text-center">'+labelButton+' un critère</h6>',
                    message:  action == "delete" ? `<h6>${trad.confirmdelete}</h6>` : `<div class="form-group">
                                    <label for="nameEditCriteria<?=$key ?>">Nom du critère:</label>
                                    <input type="text" class="form-control" id="nameEditCriteria<?=$key ?>" value="${button.data("name")}">
                                    <label class="small text-danger" id="nameEditCriteria<?=$key ?>Rules" style="display:none">Le nom du critère ne peut pas contenir du point(.)</label>
                                    <input type="text" class="form-control hidden" id="keyEditCriteria" value="${button.data("criteria")}">
                                </div>`,
                    size: 'small',
                    centerVertical: true,
                    buttons: {
                        cancel: {
                            label: "Annuler",
                            className: 'btn-danger',
                            callback: function(){
                                console.log('Custom cancel clicked');
                            }
                        },
                        ok: {
                            label: labelButton,
                            className: 'btn-info',
                            callback: function(){
                                if(action == "edit"){
                                    if($('#nameEditCriteria<?=$key ?>').val().indexOf('.') != -1){
                                        return bootbox.alert("<p class='h4 text-danger'>Le nom du critère ne peut pas contenir un point (.)</p>");
                                    }
                                }
                                dataHelper.path2Value(
                                    {
                                        id:eObj.formId,
                                        collection:"forms",
                                        path:"params.criterias<?= $key ?>."+button.data("criteria"),
                                        value: action == "delete" ? null : {
                                            name : $('#nameEditCriteria<?=$key ?>').val(),
                                            coeff: 1
                                        },
                                    },function(prms){
                                        reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                    }
                                )
                            }
                        }
                    }
                });
                dialog.on('shown.bs.modal', function(e){
                    $('#nameEditCriteria<?=$key ?>').on('keyup',function(){

                    })
                });
            }
        },
        changeLabelCriteria : function(eObj){
            if(eObj.admin && eObj.modeEdit){
                var dialog = bootbox.dialog({
                    title: '<h6 class="text-center">Modifier le label</h6>',
                    message:   `<div class="form-group">
                                    <label for="label-criteria">Nom du critère:</label>
                                    <input type="text" class="form-control" id="label-criteria" value="${eObj.criteriaLabel}">
                                </div>`,
                    size: 'small',
                    centerVertical: true,
                    buttons: {
                        cancel: {
                            label: "Annuler",
                            className: 'btn-danger',
                            callback: function(){
                                console.log('Custom cancel clicked');
                            }
                        },
                        ok: {
                            label: "Modifier",
                            className: 'btn-info',
                            callback: function(){
                                dataHelper.path2Value(
                                    {
                                        id:eObj.formId,
                                        collection:"forms",
                                        path:"params.criteriaLabel<?= $key ?>",
                                        value: $('#label-criteria').val(),
                                    },function(prms){
                                        reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                    }
                                )
                            }
                        }
                    }
                });
            }
        },
        changeCategoryNumber : function(eObj){
            if(eObj.admin && eObj.modeEdit){
                var dialog = bootbox.dialog({
                    title: '<h6 class="text-center">Modifier le label</h6>',
                    message:   `<div class="form-group">
                                    <label for="label-categorie">Changer cette titre:</label>
                                    <input type="text" class="form-control" id="category-title-<?=$key ?>" value="${eObj.categoryTitle}">
                                    <label for="category-number-<?=$key ?>">Nombre de niveau de catégorie:</label>
                                    <input type="text" class="form-control" id="category-number-<?=$key ?>" value="${eObj.categoryNumber}">
                                </div>`,
                    size: 'small',
                    centerVertical: true,
                    buttons: {
                        cancel: {
                            label: "Annuler",
                            className: 'btn-danger',
                            callback: function(){
                                console.log('Custom cancel clicked');
                            }
                        },
                        ok: {
                            label: "Modifier",
                            className: 'btn-info',
                            callback: function(){
                                var height = $('#category-number-<?=$key ?>').val();
                                var label = $('#category-title-<?=$key ?>').val();
                                
                                dataHelper.path2Value(
                                    {
                                        id:eObj.formId,
                                        collection:"forms",
                                        path:"params.categoryNumber<?= $key ?>",
                                        value: height,
                                        setType : "int"
                                    },function(prms){
                                        
                                    }
                                )
                                dataHelper.path2Value({
                                    id:eObj.formId,
                                    collection:"forms",
                                    path:"params.categoryTitle<?= $key ?>",
                                    value: label,
                                    },function(prms){ 
                                        reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                    }
                                )
                            }
                        }
                    }
                });
            }
        },
        deleteCategoryLevel : function(eObj,button){
            if(eObj.admin && eObj.modeEdit){
                var index = button.data('path').split('.').indexOf(button.data('value'));
                var path = button.data('path').split('.');
                path.splice(index+1);
                dataHelper.path2Value(
                    {
                    id:eObj.formId,
                    collection:"forms",
                    path:"params.categories<?= $key ?>."+path.join('.'),
                    value:null
                    },function(prms){
                        reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                })
            }
        },
        editCategoryLevel : function(eObj,button){
            if(eObj.admin && eObj.modeEdit){
                var index = button.data('path').split('.').indexOf(button.data('value'));
                var path = button.data('path').split('.');

                path.splice(index+1);
                path = path.join('.');
                current = path.split('.');
                var dialog = bootbox.dialog({
                    title: '<h6 class="text-center">Renomer</h6>',
                    message:   `<div class="form-group">
                                    <label for="label-criteria">Renomer</label>
                                    <input type="text" class="form-control" id="categorie-name-<?=$key ?>" value="${escapeHtml(current[current.length-1])}">
                                </div>`,
                    size: 'small',
                    centerVertical: true,
                    buttons: {
                        cancel: {
                            label: "Annuler",
                            className: 'btn-danger',
                            callback: function(){
                                console.log('Custom cancel clicked');
                            }
                        },
                        ok: {
                            label: "Modifier",
                            className: 'btn-info',
                            callback: function(){
                                var newPath = path;
                                if(path.split('.').length > 1){
                                    var splitedPath = path.split('.');
                                    splitedPath[splitedPath.length-1] = $('#categorie-name-<?=$key ?>').val();
                                    newPath = splitedPath.join(".");
                                }else if(path.split('.').length == 1){
                                    newPath = $('#categorie-name-<?=$key ?>').val();
                                }
                                if(newPath != path){
                                    dataHelper.path2Value(
                                        {
                                        id:eObj.formId,
                                        collection:"forms",
                                        path:"params.categories<?= $key ?>."+path,
                                        renameKey:true,
                                        value: "params.categories<?= $key ?>."+newPath,
                                        },function(prms){
                                            /*var params = {
                                                id : eObj.formId,
                                                collection:"forms",
                                                path: "answers.evaluation."+path,
                                                value : "answers.evaluation."+newPath,
                                                updateMany : {
                                                    verb : "$rename",
                                                    collection : "answers"
                                                },
                                            }
                                            dataHelper.path2Value( params, function(data) {
                                                reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                            } );*/
                                            ajaxPost('', baseUrl + '/survey/answer/renamekey',
                                                {
                                                    formId : eObj.formId,
                                                    path: "answers.evaluation<?= $key ?>."+path,
                                                    value : "answers.evaluation<?= $key ?>."+newPath,
                                                },
                                                function(){
                                                    reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                                                });
                                        }
                                    )
                                }
                            }
                        }
                    }
                });
            }
        },
        multipleVotePerLine : function(eObj,checkbox){
            if(eObj.admin && eObj.modeEdit){
                if(checkbox.checked) {
                    dataHelper.path2Value(
                    {
                        id:eObj.formId,
                        collection:"forms",
                        path:"params.multiVotePerLine",
                        value:true,
                        setType : "boolean"
                        },function(prms){
                            reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                    })
                }else{
                    dataHelper.path2Value(
                {
                    id:eObj.formId,
                    collection:"forms",
                    path:"params.multiVotePerLine",
                    value:false,
                    setType : "boolean"
                    },function(prms){
                        reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                })
                }


            }
        },
        configuration : function(eObj){
            var dyfConfig = {
                jsonSchema : {	
			        title : "Config",
			        icon : "cog",
			        properties : {
			        	categoryNumber : { 
                            label : "Nombre de categorie",
                            rules :{
                                required :true,
                                number : true,
                                min:1
                            }
                        }
			        },
			        beforeBuild : function(){

				    },
			        save : function (formData) {
                        
					}
				}
            }
            dyFObj.openForm(dyfConfig);
        },
        autocompletion : function(eObj){
            var autocompletion= [];
            $.each(eObj.flattenData,function(k,v){
                k = k.split(".");
                $.each(k,function(kk,vv){
                    if(kk <= eObj.categoryNumber-1){
                        if(!autocompletion.includes(vv)){
                            autocompletion.push(vv);
                        }
                    }
                })
            })
            autocompletion = autocompletion.sort();
            $(".input-category<?= $key ?>").autocomplete({
                source: autocompletion,
            });
        },
        activeButtonAddLine :function(eObj){
            var active = true;
            for (let index = 1; index <= eObj.categoryNumber; index++) {
                if($('.category<?= $key ?>'+index).val() == ""){
                    active = false;break;
                }
            }
            return active;
        },
        addBackgroundByCategorie(eObj){
            var index = [];
            $("tbody tr").each(function(k,v){
                if(typeof $(this).find("td:first-child").data("path") != "undefined"){
                    var path = $(this).find("td:first-child").data("path").split('.')[0];
                    if(!index.includes(path))
                        index.push(path);
                }
            })
            mylog.log(index,"indexako")
            var i = 0;
            $.each(index,function(k,v){
                v = v.replace(/'/g, "\\'").replace(/"/g, '\\"');
                if(i % 2 != 0){
                    $('[data-path^= "'+v+'"]').css("background-color","rgb(128 128 128 / 16%)");
                    $('[data-path^= "'+v+'"]').attr("data-bg","rgb(128 128 128 / 16%)");
                }
                else
                    $('[data-path^= "'+v+'"]').attr("data-bg","#fff");
                i++;
            })
            $('.change-color.selected').css("background-color",eObj.colours["OK"])
        },
        settingConfidentialResponse(eObj){
            if(eObj.admin && eObj.modeEdit){
                $("#confidential-response-<?= $key ?>").off().on('click',function(){
                    var isConfidential = false;
                    if ($("#confidential-checkbox-response-<?= $key ?>:checked").length > 0){
                        isConfidential = true;
                    } else {
                        isConfidential = false;
                    }
                    setTimeout(() => {
                        dataHelper.path2Value(
                            {
                                id:eObj.formId,
                                collection:"forms",
                                path:"params.isConfidential",
                                value: isConfidential,
                                setType : "boolean"
                            },function(prms){
                                //reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                            }
                        )
                    }, 900);
                })
            }
        }
    }
    evaluationObj<?=$key?>.init(evaluationObj<?=$key?>);
});
</script>
<?php } ?>
