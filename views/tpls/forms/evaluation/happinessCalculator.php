<?php 
    if(isset(Yii::app()->session['userId']) && Yii::app()->session['userId']){
        $answer = PHDB::findOne(Form::ANSWER_COLLECTION,array("form"=>(string)$parentForm["_id"],"user" =>Yii::app()->session['userId']));
        HtmlHelper::registerCssAndScriptsFiles(['/plugins/rater/rater-js.js'], Yii::app()->request->baseUrl);
        $happinessCalculatorCriterias = !empty($parentForm['params']["happinessCalculatorCriterias".$key]) ? $parentForm['params']["happinessCalculatorCriterias".$key] : [];
        $myHappinessCalculatorCriterias = !empty($answer['answers']["happinessCalculatorCriterias".$key]) ? $answer['answers']["happinessCalculatorCriterias".$key] : [];
        $happinessCalculatorAnswers = !empty($answer["answers"]["happinessCalculator".$key]) ? $answer["answers"]["happinessCalculator".$key] :[];
        $answerId = (string) $answer['_id'];
        $admin = Form::canAdmin((string)$parentForm["_id"]);
?>
<script>
    function emojiButtonList_Initialize(n,e){return new emojiButtonList(n,e)}function emojiButtonList(n,e){var t,o,i={},l={},r={},s=null,a=null,f=null,c=null,u=null;function g(){d("none")}function d(n){u.style.display!==n&&(u.style.display=n)}function p(n){var e=null,t=null==n?"div":n.toLowerCase();return r.hasOwnProperty(t)||(r[t]="text"===t?s.createTextNode(""):s.createElement(t)),e=r[t].cloneNode(!1)}function h(n){var e=null;return y(n)&&(l.hasOwnProperty(n)&&null!==l[n]||(l[n]=s.getElementById(n)),e=l[n]),e}function v(n){var e;return null!==i&&y(i[n])&&(e=i[n],"function"==typeof e)}function y(n){return null!=n&&""!==n}this.setOptions=function(n){y((i=null!==n&&"object"==typeof n?n:{}).emojiRangesToShow)||(i.emojiRangesToShow=[[128513,128591],[9986,10160],[128640,128704]]),y(i.dropDownXAlign)||(i.dropDownXAlign="left"),y(i.dropDownYAlign)||(i.dropDownYAlign="bottom"),y(i.textBoxID)||(i.textBoxID=null),y(i.xAlignMargin)||(i.xAlignMargin=0),y(i.yAlignMargin)||(i.yAlignMargin=0)},t=document,o=window,e=y(e)?e:{},s=t,a=o,this.setOptions(e),f=h(n),c=h(i.textBoxID),(u=document.createElement("div")).className="emoji-drop-down custom-scroll-bars",u.style.display="none",s.body.appendChild(u),i.emojiRangesToShow.length,i.emojiRangesToShow.forEach(n=>{var e,t,o,l;e=n,t="&#"+e+";",o=p("div"),o.innerHTML=t,l=p("div"),l.className="emoji",l.innerHTML=t,u.appendChild(l),l.onclick=function(){var n,e="onEmojiClick";null===c||v("onEmojiClick")?(n=o.innerHTML,v(e)&&i[e](n)):function n(e){if(s.selection)c.focus(),s.selection.createRange().text=e,c.focus();else if(c.selectionStart||0===c.selectionStart){var t=c.selectionStart,o=c.selectionEnd,i=c.scrollTop;c.value=c.value.substring(0,t)+e+c.value.substring(o,c.value.length),c.focus(),c.selectionStart=t+e.length,c.selectionEnd=t+e.length,c.scrollTop=i}else c.value+=e,c.focus()}(o.innerHTML)}}),s.body.addEventListener("click",g),a.addEventListener("resize",g),f.addEventListener("click",function n(e){var t;if((t=e).preventDefault(),t.cancelBubble=!0,"block"===u.style.display)d("none");else{d("block");var o,l,r,s,c=function n(e){for(var t=0,o=0;e&&!isNaN(e.offsetLeft)&&!isNaN(e.offsetTop);)t+=e.offsetLeft-e.scrollLeft,o+=e.offsetTop-e.scrollTop,e=e.offsetParent;return{left:t,top:o}}(f),g=(o=c.left,l=o+i.xAlignMargin,"center"===i.dropDownXAlign&&(l=o-(u.offsetWidth/2-f.offsetWidth/2)),(l+u.offsetWidth>a.innerWidth||"right"===i.dropDownXAlign)&&(l=o-(u.offsetWidth-f.offsetWidth)-i.xAlignMargin),l<i.xAlignMargin&&(l=i.xAlignMargin),l),p=(r=c.top,s=r+f.offsetHeight+i.yAlignMargin,(s+u.offsetHeight>a.innerHeight||"top"===i.dropDownYAlign)&&(s=r-(u.offsetHeight+i.yAlignMargin)),s<i.yAlignMargin&&(s=i.yAlignMargin),s);u.style.top=p+"px",u.style.left=g+"px"}})}
</script>
<style>
    .evaluation<?=$kunik ?> .dropdown:hover .dropdown-menu {
        display: block;
        border-radius: 36px;
    }
    .evaluation<?=$kunik ?> .dropdown .dropdown-menu {
        padding: 0 !important;
        margin: 0 !important;
    }
    .evaluation<?=$kunik ?> .dropdown-menu>li>a:hover, .dropdown-menu>li>a:focus {
        border-radius: 100% !important;
    }

    .evaluation<?=$kunik ?> .btn-group-action{
        display: none;
        position: absolute;
        top: 0;
        left: 0;
    }
    .evaluation<?=$kunik ?> td.label-criteria:hover .btn-group-action {
        display:inline-block;
    }
    .comments-<?= $kunik ?>:empty:before {
        content: attr(placeholder);
        color: #555; 
    }
    .note-happiness-dropdown-<?= $kunik ?> ul li,.note-happiness-dropdown-<?= $kunik ?> ul li a{
        display: inline;
        font-size: 32px;
    }
    .note-happiness-dropdown-<?= $kunik ?> .dropdown-menu>li>a{
        padding: 3px 5px;
    }
</style>

<div class="container-fluid evaluation<?=$kunik ?>">   
    <div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
        <label for="<?php echo $key ?>">
	    	<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>" class="<?= $canEditForm && $mode="fa" ? "" : $type ?>">
	    		<?php echo $label.$editQuestionBtn ?>
				
	    	</h4>
	    </label>
    </div>

    <?php if(!empty($info)){ ?>
        <small id="<?= $key ?>Help" class="form-text text-muted">
            <?php echo $info ?>
        </small>
    <?php } ?>
    <div class="row">
        <div class="col-xs-12 col-md-4 padding-bottom-25">
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 no-padding">
            <div class="table-responsive">
                <table class="table table-bordered table-stripped text-left" id="table1" style="font-size: 15px;">
                    <thead>
                        <tr>
                            <th>Critères</th>
                            <th class="text-center">Note de valeurs</th>
                            <th class="text-center">Note de bonheur sur le sujet</th>
                            <th class="text-center" style="width: 25%;">Commentaires</th>
                        </tr>
                    </thead>
                    <tbody id="tbody">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-5 add-line-<?= $kunik ?>-form">
                
        </div>
    </div>
</div>
<script>
    $(function(){
        var happinessCalculatorObj = {
            answerId : <?= json_encode($answerId) ?>,
            formId : <?= json_encode((string)$parentForm["_id"]) ?>,
            happinessCalculatorCriterias : <?= json_encode($happinessCalculatorCriterias) ?>,
            myHappinessCalculatorCriterias : <?= json_encode($myHappinessCalculatorCriterias) ?>,
            happinessCalculatorAnswers : <?= json_encode($happinessCalculatorAnswers) ?>,
            admin : <?= json_encode($admin) ?>,
            mode : <?= json_encode($mode) ?>,
            key : <?= json_encode($key) ?>,
            smiley : {
                love : {dec : "&#128525" ,label : "Love", noteWithCoeff:0, ansCount:0},
                happySmile : {dec : "&#128512" ,label : "Smile", noteWithCoeff:0, ansCount:0},
                neutral : {dec : "&#128528" ,label : "Neutre", noteWithCoeff:0, ansCount:0},
                sad : {dec : "&#128542" ,label : trad.sad, noteWithCoeff:0, ansCount:0},
                cry : {dec : "&#128557" ,label : "Pleure", noteWithCoeff:0, ansCount:0}
            },
            init : function(hcObj){
                if(Object.keys(hcObj.myHappinessCalculatorCriterias).length != 0){
                    hcObj.happinessCalculatorCriterias = Object.assign(hcObj.happinessCalculatorCriterias,hcObj.myHappinessCalculatorCriterias);
                }
                hcObj.createTable(hcObj);
                hcObj.events(hcObj);
            },
            events : function(hcObj){
                hcObj.rate(hcObj);
                hcObj.addHappiness(hcObj);
                hcObj.addComments(hcObj);
                $('.add-line-<?= $kunik ?>').on('click',function(){
                    hcObj.addEditDeleteLine(hcObj,$(this),"add");
                })
                $('.edit-line<?= $kunik ?>').on('click',function(){
                    hcObj.addEditDeleteLine(hcObj,$(this),"edit");
                })
                $('.delete-line<?= $kunik ?>').on('click',function(){
                    hcObj.addEditDeleteLine(hcObj,$(this),"delete");
                })
                hcObj.mean(hcObj);
                $('[data-toggle="tooltip"]').tooltip();
                if(hcObj.happinessCalculatorCriterias.length == 0){
                    var prms = {
                        id : hcObj.formId,
                        path : "params.happinessCalculatorCriterias"+hcObj.key,
                        collection : "forms",
                        value : {
                            "criteria1" : {
                                "label" : "Argent",
                                "coeff" : 2,
                                "me" : false
                            },
                            "criteria2" : {
                                "label" : "Amour",
                                "coeff" : 3,
                                "me" : false
                            },
                            "criteria3" : {
                                "label" : "Santé",
                                "coeff" : 3,
                                "me" : false
                            },
                            "criteria4" : {
                                "label" : "Nourriture",
                                "coeff" : 2,
                                "me" : false
                            },
                            "criteria5" : {
                                "label" : "Loisir",
                                "coeff" : 2,
                                "me" : false
                            },
                            "criteria6" : {
                                "label" : "Métier",
                                "coeff" : 2,
                                "me" : false
                            }
                        }
                    }
                    dataHelper.path2Value(prms,function(){
                        reloadInput(hcObj.key, "<?php echo (string)$form["_id"] ?>");
                    })
                }
            },
            createTable : function(hcObj){
                var html = "";
                if(Object.keys(hcObj.happinessCalculatorCriterias).length == 0 && Object.keys(hcObj.myHappinessCalculatorCriterias).length == 0){
                    html+= `
                        <tr style="position:relative">
                            <td colspan="4"><span class="text-danger">Critère vide</span></td>
                        </tr>
                    `;
                }else{
                    $.each(hcObj.happinessCalculatorCriterias,function(k,v){
                        var note = exists(hcObj.happinessCalculatorAnswers[k]) && exists(hcObj.happinessCalculatorAnswers[k]["note"]) ? hcObj.happinessCalculatorAnswers[k]["note"] : 0
                        var happiness = exists(hcObj.happinessCalculatorAnswers[k]) && exists(hcObj.happinessCalculatorAnswers[k]["happiness"]) ? hcObj.happinessCalculatorAnswers[k]["happiness"] : "";
                        var comments = exists(hcObj.happinessCalculatorAnswers[k]) && exists(hcObj.happinessCalculatorAnswers[k]["comments"]) ? hcObj.happinessCalculatorAnswers[k]["comments"] : " "
                        html+= `
                            <tr style="position:relative">
                                <td class="text-left label-criteria">${v.label + " " + (k.includes("mycriteria") ? "<b>(perso)</b>" : "")} 
                                    <span class="label label-success pull-right">${v.coeff}</span>
                                    <div class="btn-group btn-group-action pull-right margin-right-15">`;
                                    if(hcObj.admin && !k.includes("mycriteria")){
                                        html+= `<button data-key="${k}" data-value="${'all'}" data-label="${v.label}" data-coeff="${v.coeff}" class="btn btn-success btn-xs edit-line<?= $kunik ?>" type="button"><i class="fa fa-edit"></i></button>
                                        <button data-key="${k}" data-value="${'all'}" data-label="${v.label}" data-coeff="${v.coeff}" class="btn btn-danger btn-xs delete-line<?= $kunik ?>" type="button"><i class="fa fa-times"></i></button>`;
                                    }else if(k.includes("mycriteria")){
                                        html+= `<button data-key="${k}" data-value="${'me'}" data-label="${v.label}" data-coeff="${v.coeff}" class="btn btn-success btn-xs edit-line<?= $kunik ?>" type="button"><i class="fa fa-edit"></i></button>
                                        <button data-key="${k}" data-value="${'me'}" data-label="${v.label}" data-coeff="${v.coeff}" class="btn btn-danger btn-xs delete-line<?= $kunik ?>" type="button"><i class="fa fa-times"></i></button>`;
                                    }
                            html+= `</div>
                                    </td>
                                <td>
                                    <div class="note-value" data-rating='${note}' data-key='${k}'</div>
                                </td>
                                <td>
                                    <div class="dropdown note-happiness-dropdown-<?= $kunik ?>">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="font-size:30px;text-decoration:none">${(notEmpty(hcObj.smiley[happiness]) && notEmpty(hcObj.smiley[happiness].dec)) ? hcObj.smiley[happiness].dec : "&#128528;"}</a>
                                        <ul class="dropdown-menu">`;
                                        $.each(hcObj.smiley,function(kk,vv){
                                            html+= `<li class="tooltips" data-toggle="tooltip" data-placement="top" data-original-title="${vv.label}" >
                                                <a href="javascript:;" class="note-happiness-<?= $kunik ?>" ${happiness == kk ? "selected" : ""} data-key='${k}' data-value="${kk}">${vv.dec}</a>
                                            </li>`;   
                                        })
                                html+= `</ul>
                                    </div>
                                </td>
                                <td class="text-left comments-<?= $kunik ?>" contenteditable="true" placeholder="Ajouter un commentaire" data-key="${k}">${comments}</td>
                            </tr>
                        `;
                    })
                }
                html+= ` <tr style="border-top: 2px solid;border-bottom: 2px solid;">
                        <td class="text-right bold" style="vertical-align:middle">Moyenne</td>
                        <td class="text-left text-center bold mean-star-<?= $kunik ?>" style="font-size:30px"></td>
                        <td class="text-left text-center bold mean-smiley-<?= $kunik ?>" style="font-size:30px"></td>
                    </tr>`;
                
                    html+= ` <tr>
                        <td colspan="4">`;
                            if(hcObj.admin){
                                html+= ` <button type="button" data-value="all" class="btn btn-success add-line-<?= $kunik ?>">Ajouter un critère pour tous le monde</button>&nbsp;`;
                            }
                            html+= `<button type="button" data-value="me" class="btn btn-success add-line-<?= $kunik ?>">Ajouter mon propre critère</button>
                        </td>
                    </tr>`;
                
                $('#tbody').html(html);
            },
            rate : function(hcObj){
                $('.note-value').each(function(i, obj) {
                    var opts = {
                        rating : $(obj).data("rating"),
                        starSize:20,
                        step:0.5,
                        element:obj,
                        rateCallback : function rateCallback(rating, done) {
                            var $this = this;
                            this.setRating(rating); 
                            var tplCtx ={
                                id : hcObj.answerId,
                                collection : "answers",
                                path : "answers.happinessCalculator"+hcObj.key+"."+$(obj).data("key")+".note",
                                value : rating,
                                setType : "float"
                            }
                            dataHelper.path2Value(tplCtx, function (params) {
                                if(params.result){
                                    toastr.success("Ok");
                                    hcObj.happinessCalculatorAnswers = params.elt.answers.happinessCalculator;
                                    hcObj.mean(hcObj);
                                }
                            });
                            done(); 
                        }
                    };
                    var starRating = raterJs(opts); 
                });
            },
            addHappiness : function(hcObj){
                $(".note-happiness-<?= $kunik ?>").off().on('click',function(){
                    var btn = $(this);
                    var tplCtx ={
                        id : hcObj.answerId,
                        collection : "answers",
                        path : "answers.happinessCalculator"+hcObj.key+"."+btn.data("key")+".happiness",
                        value : btn.data("value"),
                    }
                    dataHelper.path2Value(tplCtx, function (params) {
                        if(params.result){
                            btn.parent().parent().parent().find(".dropdown-toggle").html(btn.html());
                            hcObj.happinessCalculatorAnswers = params.elt.answers.happinessCalculator
                            toastr.success("Ok");
                            hcObj.mean(hcObj);
                        }
                    });
                })
            },
            addComments : function(hcObj){
                $('.comments-<?= $kunik ?>').on('blur',function(){
                    var field = $(this);
                    var tplCtx ={
                        id : hcObj.answerId,
                        collection : "answers",
                        path : "answers.happinessCalculator"+hcObj.key+"."+field.data("key")+".comments",
                        value : field.text(),
                    }
                    dataHelper.path2Value(tplCtx, function (params) {
                        if(params.result){
                            toastr.success(trad.saved);
                        }
                    });
                })
            },
            addEditDeleteLine : function(hcObj,button=null,action="add"){
                //if(hcObj.admin && hcObj.mode == "fa"){
                    if(!hcObj.admin && button.data("value") == "all"){
                        return toastr(trad["You are not authorized"]);
                    }
                    var buttonLabel = "Ajouter";
                    var label = "";
                    var coeff= 1;
                    var key="";
                    var path = "";

                    var id = hcObj.formId;
                    var collection = "forms";

                    var lastCriteria = Object.keys(hcObj.happinessCalculatorCriterias).length != 0 ? (Object.keys(hcObj.happinessCalculatorCriterias)[Object.keys(hcObj.happinessCalculatorCriterias).length-1]) : "criteria0";
                    var newRang = parseInt(lastCriteria.split('criteria')[1])+1;

                    if(button.data("value")=="me"){
                        lastCriteria = Object.keys(hcObj.myHappinessCalculatorCriterias).length != 0 ? (Object.keys(hcObj.myHappinessCalculatorCriterias)[Object.keys(hcObj.myHappinessCalculatorCriterias).length-1]) : "mycriteria0";
                        newRang = parseInt(lastCriteria.split('mycriteria')[1])+1;
                    }
                    
                    
                    if(action=="add"){
                        if(button.data("value") == "all")
                            path = "params.happinessCalculatorCriterias"+hcObj.key+".criteria"+newRang;
                        else if(button.data("value") == "me" ){
                            path = "answers.happinessCalculatorCriterias"+hcObj.key+".mycriteria"+newRang;
                            id = hcObj.answerId;
                            collection = "answers";
                        }
                    }else if(action=="edit"){
                        buttonLabel = "Modifier";
                        label = button.data('label');
                        coeff= button.data('coeff');
                        rang = button.data('key');
                        if(button.data("value") == "all")
                            path = "params.happinessCalculatorCriterias"+hcObj.key+"."+rang
                        else if(button.data("value") == "me" ){
                            path = "answers.happinessCalculatorCriterias"+hcObj.key+"."+rang;
                            id = hcObj.answerId;
                            collection = "answers";
                        }
                    }else if(action=="delete"){
                        buttonLabel = "Supprimer";
                        rang = button.data('key');
                        if(button.data("value") == "all")
                            path = "params.happinessCalculatorCriterias"+hcObj.key+"."+rang
                        else if(button.data("value") == "me" ){
                            path = "answers.happinessCalculatorCriterias"+hcObj.key+"."+rang;
                            id = hcObj.answerId;
                            collection = "answers";
                        }
                    }
                    //alert(button.data("value")+" - "+collection+" - "+action);
                    
                    var dialog = bootbox.dialog({
                        title: `<h6 class="text-center"> ${buttonLabel} une ligne</h6>`,
                        message:   action=="delete" ? `<h6>${trad.confirmdelete}</h6>` : `<div class="form-group">
                                        <label for="name-criteria">Nom du critère:</label>
                                        <input type="text" class="form-control" id="name-criteria" value="${label}">
                                    </div>
                                    <div class="form-group">
                                        <label for="name-criteria">Coefficient:</label>
                                        <input type="number" class="form-control" id="coeff-criteria" value="${coeff}">
                                    </div>`,
                        size: 'small',
                        centerVertical: true,
                        buttons: {
                            cancel: {
                                label: "Annuler",
                                className: 'btn-danger',
                                callback: function(){
                                    console.log('Custom cancel clicked');
                                }
                            },
                            ok: {
                                label: buttonLabel,
                                className: 'btn-info',
                                callback: function(){
                                    var accept = false;
                                    if(action == "delete") accept = true;
                                    if((action == "edit" || action == "add")  && $('#name-criteria').val() != "") accept = true;
                                    if(accept){
                                        dataHelper.path2Value(
                                            {
                                                id:id,
                                                collection:collection,
                                                path: path,
                                                value: action=="delete" ? null : {
                                                    label : $('#name-criteria').val(),
                                                    coeff: notEmpty($('#coeff-criteria').val()) ? Math.abs($('#coeff-criteria').val()) : 1,
                                                    me : button.data("value") == "me" ? true : false
                                                },
                                                setType : [
                                                    {
                                                        "path": "coeff",
                                                        "type": "int"
                                                    },
                                                    {
                                                        "path": "me",
                                                        "type": "boolean"
                                                    }
                                                ]
                                            },function(prms){
                                                reloadWizard();
                                            }
                                        )
                                    }else 
                                        toastr.error("Le champs critère est vide")
                                }
                            }
                        }
                    });
                    dialog.on('shown.bs.modal', function(e){    
                        $('#coeff-criteria').on('change keyup',function(){
                            var val = Math.abs($(this).val());
                            $(this).val(val);
                        })
                    });
                //}
            },
            sumCoeff : function(hcObj){
                var sumCoeff = 0;
                $.each(hcObj.happinessCalculatorCriterias,function(k,v){
                    if(exists(v.coeff)){
                        sumCoeff += parseInt(v.coeff);
                    }
                })
                return sumCoeff;
            },
            mean : function(hcObj){
                var meanStar = 0;
                var totalStar = 0;
                var meanSmiley = 0;
                var totalSmiley = 0;

                $.each(hcObj.smiley,function(k,v){
                    hcObj.smiley[k].ansCount = 0;
                    hcObj.smiley[k].noteWithCoeff = 0;
                })
                mylog.log(hcObj.smiley,"hcObj.smiley 0");

                $.each(hcObj.happinessCalculatorAnswers,function(k,v){
                    if(exists(v.note) && exists(hcObj.happinessCalculatorCriterias[k]) && exists(hcObj.happinessCalculatorCriterias[k]["coeff"])){
                        totalStar += parseInt(v.note)*hcObj.happinessCalculatorCriterias[k]["coeff"];
                    }
                    if(notEmpty(v.happiness)){
                        if(exists(hcObj.smiley[v.happiness]) && exists(hcObj.happinessCalculatorCriterias[k]) && exists(hcObj.happinessCalculatorCriterias[k]["coeff"])){
                            hcObj.smiley[v.happiness].ansCount++;
                        }
                    }else{
                        hcObj.smiley["neutral"].ansCount++;
                    }
                })

                $.each(hcObj.happinessCalculatorAnswers,function(k,v){
                    if(exists(v.happiness)){
                        if(exists(hcObj.smiley[v.happiness]) && exists(hcObj.happinessCalculatorCriterias[k]) && exists(hcObj.happinessCalculatorCriterias[k]["coeff"])){
                            hcObj.smiley[v.happiness].noteWithCoeff += hcObj.smiley[v.happiness].ansCount * hcObj.happinessCalculatorCriterias[k]["coeff"];
                        }
                    }
                })



                /**stat****************************/
                if(hcObj.sumCoeff(hcObj)==0 )
                    meanStar = 0;
                else
                    meanStar = totalStar/hcObj.sumCoeff(hcObj);

                meanStar = Math.round(meanStar * 100) / 100;
                $('.mean-star-<?= $kunik ?>').html(meanStar +'/ 5');

                /**smiley ********************** */
                var max = hcObj.smiley["neutral"];
                $.each(hcObj.smiley,function(k,v){
                    if(v.noteWithCoeff > max.noteWithCoeff){
                        max = hcObj.smiley[k];
                    }
                })
                mylog.log(hcObj.smiley,"hcObj.smiley",max);
                $('.mean-smiley-<?= $kunik ?>').html(max.dec);

            }
        }
        happinessCalculatorObj.init(happinessCalculatorObj);
    })
</script>
<?php 
}
?>