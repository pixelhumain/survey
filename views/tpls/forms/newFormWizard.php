<?php
//todo position
//todo coFormjs
//todo starDate and EndDate
//todo mode edit
//todo banner text && banner img
//todo subforms rules
//todo wizard intro && homeTpl

$ignore = array('_file_', '_params_', '_obInitialLevel_' ,'ignore');
$params = array_diff_key(get_defined_vars() + array_flip($ignore), array_flip($ignore));
if($mode == "fa" && isset($params["answer"]["answers"])){
    unset($params["answer"]["answers"]);
}
// $urlParams = Yii::$app->request->getQueryParams();

HtmlHelper::registerCssAndScriptsFiles(array(
    '/plugins/jQuery-Knob/js/jquery.knob.js',
    // SHOWDOWN
    '/plugins/showdown/showdown.min.js',
    // MARKDOWN
    '/plugins/to-markdown/to-markdown.js'
), Yii::app()->request->baseUrl);
HtmlHelper::registerCssAndScriptsFiles(array(
    '/js/answer.js'
), Yii::app()->getModule(Survey::MODULE)->getAssetsUrl());

if(!$isajax){
    HtmlHelper::registerCssAndScriptsFiles(array(
        //Float button
        '/plugins/float-button-st-panel/css/st.action-panel.css',
        //SmartWizard
        '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.min.js',
        '/plugins/jQuery-Smart-Wizard/css/smart_wizard_all.min.css',
        //Float button
        '/plugins/float-button-st-panel/js/st.action-panel.js'
    ), Yii::app()->request->baseUrl);
    HtmlHelper::registerCssAndScriptsFiles(array(
        '/css/newFormWizard.css',
    ), Yii::app()->getModule(Survey::MODULE)->getAssetsUrl());
?>
    <style>
        .error-msg::after {
            content: '<?php echo Yii::t("common","This field is required."); ?>';
            color: #a94442;
            font-size: 85%;
            padding: 1%;
        }
    </style>
<?php
    if(!$isajax || (isset($mode) && $mode !== 'w' && $mode !== 'r' && $mode !== 'pdf')){
        HtmlHelper::registerCssAndScriptsFiles(array(
            '/css/sidenav.css',
            //DnD
            '/js/inputDnDAap.js',
        ), Yii::app()->getModule(Survey::MODULE)->getAssetsUrl());
    }
?>

    <style type="text/css">
        @font-face{
            font-family: "montserrat";
            src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/montserrat-regular.woff") format("woff"),
            url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/montserrat-regular.ttf") format("ttf")
        }.mst{font-family: 'montserrat'!important;}

        @font-face{
            font-family: "CoveredByYourGrace";
            src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/CoveredByYourGrace.ttf")
        }.cbyg{font-family: 'CoveredByYourGrace'!important;}
        .questionBlockplaceholder {
            padding: 0px;
            background: transparent;
            border: 1px dashed rgb(100, 150, 150);
        }

        .questionBlockhover {
            border: 1px solid #0a91ff;
        }
        .buildBlockPlaceholder {
            background-color: #eeeeee;
            opacity: 0.6;
            float:left;
        }

        .oblsign{
             color : red ;
            margin-left: 5px;
        }

        .sw-toolbar-elm{
            z-index:99999 !important;
        }

        .sw.sw-loading.coFormbody::before {
            display: none !important;
        }
        .sw .sw-custom-loader {
            display: none;
            position: absolute;
            top: 0%;
            left: 45%;
            z-index: 1000;
            width: 10vw;
        }
        .sw .sw-custom-loader .call_loader {
            margin-top: 0 !important;
        }
        .sw.sw-loading .sw-custom-loader {
            display: block;
        }

    </style>

<?php
}
?>

<script type="text/javascript">
    if(typeof sectionDyf == "undefined")
        var sectionDyf = {};
</script>

<?php
if (!$isajax && $mode == 'fa' && isset($canAdminAnswer) && $canAdminAnswer) {
    ?>

    <div class="container mt-4 padding-10 text-center form-config-btn-container" style="background-color: #eee">
        <h4>Admin Form : <?php echo isset($parentForm['name']) ? $parentForm["name"] : "(Sans nom)" ?></h4>
        <a href='javascript:;' data-id='<?php echo (String)$parentForm["_id"]; ?>' class='addStepBtn btn btn-danger bold'> <i class='fa fa-plus'></i> <?php echo Yii::t("survey","Add a step") ?> </a>
        <a href='javascript:;' data-id='<?php echo (String)$parentForm["_id"]; ?>' class='createFrom btn btn-danger bold'> <i class='fa fa-plus'></i> <?php echo Yii::t("survey","Add indented text") ?> </a>
        <a href='javascript:;'  data-id='<?php echo (String)$parentForm["_id"]; ?>' class='copystandalonelink btn btn-danger bold' data-clipboard-action="copy"> <i class='fa fa-plus'></i> <?php echo Yii::t("common","Link to Share this form") ?></a>
    </div>
    <?php
}
    if(isset($isInsideForm) && filter_var($isInsideForm, FILTER_VALIDATE_BOOLEAN))
echo $this->renderPartial("survey.views.tpls.forms.formBanner" , array(
    "isajax"=>$isajax,
    "mode" => $mode,
    "isStandalone" => $isStandalone,
    "parentForm" =>$parentForm,
    "canAdminAnswer" => $canAdminAnswer
));

if(empty($input)){
?>
    <div class="col-xs-12 no-padding margin-top-20 coFormbody coform-stepbody sticky-to" id="smartwizard" style="--clr_var: #9bc129; --clr_dark_var: #79981C;--pfpb-primary-green-color: #9bc129;">
        <?php if(empty($step)) { ?>
            <div class="sw-custom-loader">
            </div>
        <?php
        }
        //Wizard Step header
        if(empty($step)){
            echo $this->renderPartial("survey.views.tpls.forms.newWizard" , $params);
        }

        //Wizard Step content
        echo $this->renderPartial("survey.views.tpls.forms.newWizardContent" , $params);
        ?>
    </div>
<?php
}else{
    if($params["isAap"] && !empty($params["configForm"]["subForms"][$step["step"]]["params"]["haveEditingRules"]) && $params["configForm"]["subForms"][$step["step"]]["params"]["haveEditingRules"]) {
        if (
            !Form::getAnswerAuthorisation($params["answer"], $params["parentForm"], $params["context"])["canAdminAnswer"] &&
            (
                !empty($params["configForm"]["subForms"][$step["id"]]["params"]["canEdit"]) ||
                empty($params["configForm"]["subForms"][$step["id"]]["params"]["canEdit"][0])
            )&&
            sizeof(array_intersect(Form::getAnswerAuthorisation($params["answer"], $params["parentForm"], $params["context"])["roles"], @$params["configForm"]["subForms"][$step["step"]]["params"]["canEdit"])) <= 0
        ) {
            $params["mode"] = "r";
        }
    }

    $access = Form::getParamsWizard($params["parentForm"], $params["answer"], $params["isStandalone"] , $params["configForm"] );
   if(!in_array($step["step"], $access["disabledStepsId"]) && !in_array($step["step"], $access["hiddenStepsId"])){
        if(isset($isInsideForm) && !filter_var($isInsideForm, FILTER_VALIDATE_BOOLEAN)) {
?>
            <div id="question_<?= $params["input"]["id"] ?>" class="coforminput questionBlock col-xs-12 ui-corner-all no-padding questionBlock standaloneinput" data-form="<?= (string)$params["parentForm"]["_id"] ?>" data-answer="<?= (string)$params["answer"]["_id"] ?>"  data-step="<?= $step["step"] ?>">
        <?php
        }
                echo $this->renderPartial( "survey.views.".$params["input"]["tpl"] , $params , true );
        if(isset($isInsideForm) && !filter_var($isInsideForm, FILTER_VALIDATE_BOOLEAN)) {
        ?>
            </div>
<?php
        }
   }
}
    if(!$isajax && $mode != 'r' && isset($canAdminAnswer) && $canAdminAnswer && $mode == 'fa') {
?>
        <div class="main-menu st-actionContainer right-bottom">
            <div class="settings st-panel">
                <div class="searchBar-filters" style="width: 100%;margin-top: 10px; margin-bottom: 8px">
                    <input type="text" class="form-control text-center main-search-bar search-bar searchInputCoform" data-field="text" placeholder="Que recherchez-vous ?">
                    <span class="text-white input-group-addon pull-left main-search-bar-addon searchBtnCoform" data-field="text">
                    <i class="fa fa-arrow-circle-right" style="font-size: 1em !important;"></i>
                    </span>
                </div>
                <div class="scrollbar" id="style-1">
                    <ul id="inputsCoformList" style="padding: 0">
                        <?php foreach (Form::inputTypes() as $key => $value) {
                        ?>
                            <li class="questionDraggable" data-type="<?php echo $key ?>">
                                <a class="clearfix questionPreview" href="javascript:;"  data-toggle='modal' data-target='#questionPreview' data-preview="<?php echo Yii::app()->getModule('survey')->assetsUrl . '/images/question-preview/'.$key.'.png'; ?>" data-title="<?php echo $value ?>" data-desc="<?php echo isset($coformDesc[$key]) ? $coformDesc[$key] : '#todo' ?>">
                                <div id="questionDraggablePreview" class="d-none inputPreview"><img src="<?php echo Yii::app()->getModule('survey')->assetsUrl . '/images/question-preview/'.$key.'.png'; ?>" alt="setting-ico"></div>
                                <i id="questionDraggableIcon" class="" style="vertical-align: middle;"><img style="width: 30px; height: 30px;" src="<?php if(isset($coformQuestionIcons[$key])) echo Yii::app()->getModule('survey')->assetsUrl . '/images/questions-icon/'.$coformQuestionIcons[$key]; else echo Yii::app()->getModule('survey')->assetsUrl . '/images/gear.png '; ?>" alt="setting-ico"></i>
                                <span class="nav-text"><?php echo $value ?></span>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="st-btn-container right-bottom">
                <div class="st-button-main"><i class="fa fa-gear fa-3x" aria-hidden="true"></i></div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal preview-modal fade" id="questionPreview" tabindex="-1" role="dialog" aria-labelledby="QuestionPreview">
            <div class="modal-dialog" role="document">
                <div class="modal-content">

                </div>
            </div>
        </div>
<?php
    }
?>

<script type="text/javascript">
    const documentReadyPromise = new Promise(function (resolve) {
        if (document.readyState === 'complete') {
            resolve();
        } else {
            function onReady() {
                resolve();
                document.removeEventListener('DOMContentLoaded', onReady, true);
                window.removeEventListener('load', onReady, true);
            }
            document.addEventListener('DOMContentLoaded', onReady, true);
            window.addEventListener('load', onReady, true);
        }
    });

    var coForm_FormWizardParams = <?php echo json_encode($params); ?>;

    if(typeof coForm_FormWizardParams != "undefined" && coForm_FormWizardParams.mode != "undefined" && coForm_FormWizardParams.mode == "fa"){
        coForm_FormWizardParams.answer.answers == null
    }

    var answerObj = <?php echo json_encode($params["answer"]); ?>;
    // var urlParams = <?php /* echo json_encode($urlParams); */ ?>;

    if(typeof formInputsfor == "undefined") {
        var formInputsfor = {}
    }

    if(typeof isAjaxRequest == "undefined") {
        var isAjaxRequest = <?php echo json_encode($isajax) ?>
    }
    isAjaxRequest = <?php echo json_encode($isajax) ?>

    if(typeof inputsList == "undefined") {
        var inputsList = {}
        $.each( coForm_FormWizardParams.steps , function (key, index) {
            if(typeof index.inputs != "undefined") {
                inputsList[index["step"]] = index.inputs;
            }
        });
    }

    if(typeof answerObj == "undefined") {
        var answerObj = <?php echo json_encode($params["answer"]); ?>;
    }

    if(typeof eventScrollAlreadyCalled == 'undefined') {
            var eventScrollAlreadyCalled = false;
    }
    eventScrollAlreadyCalled = false;

    if(typeof eventScrollCalledFrom == 'undefined') {
            var eventScrollCalledFrom = false;
    }

    var inputsType = <?php echo json_encode(Form::inputTypes()); ?>;
    let inputsTypeAsArray;
    inputsType ? inputsTypeAsArray = Object.entries(inputsType) : '';

    var swDisabledSteps;
    var swHiddenSteps;

    function reloadInput(key, formId , _reloadCallback, showLoader=false ){
        if(showLoader){
            coInterface.showLoader("#question_"+key);
        }

        ajaxPost(
            "#question_"+key,
            baseUrl+'/survey/answer/answer/id/'+coForm_FormWizardParams.answer["_id"]["$id"]+'/form/'+coForm_FormWizardParams.parentForm["_id"]["$id"]+'/step/'+formId+'/input/'+key+'/isajax/true',
            {
                editMode : coForm_FormWizardParams.mode
            },
            _reloadCallback
            ,"html");
    }

    function scrollintoDiv(div, t_out){
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#"+div).offset().top - (( $(window).height() - $("#"+div).outerHeight(true) ) / 2)
        }, 500);
        $("#"+div ).get(0).style.setProperty("--colortitle", "<?php echo (isset($this->costum["colors"]["pink"])) ? $this->costum["colors"]["pink"] : "#d9534f"?>");

        $( "#"+div ).addClass('div-pulse');
        setTimeout(function() {
            $( "#"+div ).removeClass('div-pulse');
        }, t_out);
    }

    function saveLinks(id,linkType,uid,callB){
        var today = new Date();
        today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear()+ ' ' + today.getHours()+ ':' + today.getMinutes();
        //alert(uid+":"+today);
        if( typeof answerObj.links != "undefined" &&
            typeof answerObj.links[linkType] != "undefined" &&
            typeof answerObj.links[linkType][uid] != "undefined" //||typeof answerObj.links[linkType][uid] != today
        )
        {
            var incVal = parseInt(answerObj.links[linkType][uid].inc)+1 ;
            var link = {
                collection : "answers",
                id : answerObj._id.$id,
                path : "links."+linkType+"."+uid,
                value : {
                    date : today,
                    name : userConnected.name,
                    inc : incVal
                }
            };

            mylog.log("saveLinks", link );

            dataHelper.path2Value( link , function(params) {
                mylog.log("saveLinks saved",params );
                if(typeof callB == "function" )
                    callB();
            } );
        } else if(typeof callB == "function" )
            callB();
    }

    if(typeof stepObj == "undefined") {
        var stepObj = {};
    }

    if(coForm_FormWizardParams && typeof coForm_FormWizardParams.input != "undefined" && typeof ownAnswer == "undefined") {
        var ownAnswer = {};
    }

    function reloadWizard(stepId = null) {
        if(stepId == null){
            $.each($(".coformstep") , function(index , value){
                if($(value).data('step') != undefined){
                    ajaxPost(
                        '#'+$(value).attr('id'),
                        baseUrl+'/survey/answer/answer/id/'+$(value).data('answer')+'/form/'+$(value).data('form')+'/step/'+$(value).data('step')+'/isajax/true',
                        null,
                        null,
                        null,
                        "html",
                    );
                }
            });

            $.each($(".coforminput.standaloneinput") , function(index , value){
                if($(value).data('step') != undefined && $(value).data('input') != undefined){
                    ajaxPost(
                        '#'+$(value).attr('id'),
                        baseUrl+'/survey/answer/answer/id/'+$(value).data('answer')+'/form/'+$(value).data('form')+'/step/'+$(value).data('step')+'/input/'+$(value).data('input')+'/isajax/true',
                        null,
                        null,
                        null,
                        "html",
                    );
                }
            });
        }else{

        }
    }

    function provideContent(idx, stepDirection, stepPosition, selStep, callback) {
        if(notNull(coForm_FormWizardParams?.answer?.["_id"]?.["$id"]) && ( !notNull(answerId) || answerId == "new" || !answerId.match(/^[0-9a-fA-F]{24}$/))){
            answerId = coForm_FormWizardParams?.answer?.["_id"]?.["$id"];
        } else if (notNull(coForm_FormWizardParams?.answer?.["_id"]?.["$id"]) && (coForm_FormWizardParams?.answer?.["_id"]?.["$id"] != answerId || answerId == "new" || !answerId.match(/^[0-9a-fA-F]{24}$/))) {
            answerId = coForm_FormWizardParams?.answer?.["_id"]?.["$id"];
        }
        ajaxPost(
            "",
            baseUrl+'/survey/answer/answer/id/'+answerId+'/form/'+coForm_FormWizardParams["parentForm"]["_id"]["$id"]+'/step/'+Object.values(coForm_FormWizardParams["steps"])[idx]["step"]+'/isajax/true',
            {
                url : window.location.href,
                editMode : coForm_FormWizardParams?.mode
            },
            function(data){
                $('#smartwizard').smartWizard("loader", "hide");
                callback(data);
            },
            function(data){
                callback("<h3 class='text-danger center'>Il y a une erreur sur le chargement de cette étape. veuillez contacter l'administrateur</h3>");
                $('#smartwizard').smartWizard("loader", "hide");
            },
            "html",
            {
                beforeSend : function(){
                    $('#smartwizard').smartWizard("loader", "show");
                }
            }
        );
        return false;
    }

    function updateWizardAccess(coForm_FormWizardParams , samrtWizardId = "#smartwizard" , disabledSteps , hiddenSteps , swcallback) {
        ajaxPost(null, baseUrl+'/survey/answer/getparamswizard/answer/'+coForm_FormWizardParams["answer"]["_id"]["$id"]+'/form/'+coForm_FormWizardParams["parentForm"]["_id"]["$id"]+'/standAlone/'+coForm_FormWizardParams["isStandalone"],
            { url : window.location.href },
            function(data) {
                if(typeof swDisabledSteps != "undefined" && typeof swHiddenSteps != "undefined"){
                    swDisabledSteps = JSON.parse(data).disabledSteps;
                    swHiddenSteps = JSON.parse(data).hiddenSteps;
                }else{
                    var swDisabledSteps = JSON.parse(data).disabledSteps;
                    var swHiddenSteps = JSON.parse(data).hiddenSteps;
                }
                var parsedata = JSON.parse(data);
                if (typeof parsedata.disabledSteps != 'undefined' && parsedata.disabledSteps !== disabledSteps) {
                    const enableDifference = disabledSteps.filter((element) => !parsedata.disabledSteps.includes(element));
                    const disableDifference = parsedata.disabledSteps.filter((element) => !disabledSteps.includes(element));
                    if(notNull(enableDifference)) {
                        $(samrtWizardId).smartWizard("unsetState", enableDifference, "disable");
                    }
                    if(notNull(disableDifference)) {
                        $(samrtWizardId).smartWizard("setState", disableDifference, "disable");
                    }
                }
                if (typeof parsedata.hiddenSteps != 'undefined' && parsedata.hiddenSteps !== hiddenSteps) {
                    const showDifference = hiddenSteps.filter((element) => !parsedata.hiddenSteps.includes(element));
                    const hideDifference = parsedata.hiddenSteps.filter((element) => !hiddenSteps.includes(element));
                    if(notNull(showDifference)) {
                        $(samrtWizardId).smartWizard("unsetState", enableDifference, "hidden");
                    }
                    if(notNull(hideDifference)) {
                        $(samrtWizardId).smartWizard("setState", disableDifference, "hidden");
                    }
                }
                swcallback;
            }
        );
    }

    function updateWizardBtn(coForm_FormWizardParams , actualStep , stepPosition , stepshow = false) {
        let coForm_actualStep = Object.values(coForm_FormWizardParams["steps"])[0]["step"];
        if( !notNull(actualStep)) {
            stepPosition = "first";
            if (notNull(localStorage.getItem("stepCoForm_id_" + coForm_FormWizardParams.answer?.["_id"]?.["$id"]))) {
                coForm_actualStep = localStorage.getItem("stepCoForm_id_" + coForm_FormWizardParams["answer"]["_id"]["$id"]);
            }
            actualStep = Object.values(coForm_FormWizardParams["steps"]).map(function (o) {
                return o.step;
            }).indexOf(coForm_actualStep);
            if (notNull(localStorage.getItem("stateCoForm_" + coForm_FormWizardParams.answer?.["_id"]?.["$id"]))) {
                stepPosition = localStorage.getItem("stateCoForm_" + coForm_FormWizardParams["answer"]["_id"]["$id"]);
            }
        }

        $.each($('button.aap-sw-btn'), function(index,item){
            if ($(item).hasClass('disabled')) {
                $(item).hide();
            } else {
                $(item).show();
            }
            if ($(item).hasClass('sw-btn-next') || $(item).hasClass('sw-btn-finish')) {
                $(item).addClass('btn-aap-primary');
            }
            if ($(item).hasClass('sw-btn-next')) {
                const thisActualStep = $('#smartwizard').smartWizard("getStepInfo");
                const stepOptions = $('#smartwizard').smartWizard("getOptions");
                var availableSteps = coForm_FormWizardParams.steps ? Object.entries(coForm_FormWizardParams.steps).map(function (o, index) {
                    return index;
                }) : [];
                if (notEmpty(stepOptions.hiddenSteps)) {
                    availableSteps = availableSteps.filter(step => !stepOptions.hiddenSteps.includes(step));
                }
                hiddenNextStep = false;
                disabledNextStep = false;
                if (notEmpty(stepOptions.disabledSteps) && stepOptions.disabledSteps.includes(thisActualStep.currentStep + 1)) {
                    disabledNextStep = true;
                }
                if (notEmpty(stepOptions.hiddenSteps) && stepOptions.hiddenSteps.includes(thisActualStep.currentStep + 1) && availableSteps.length < 2) {
                    hiddenNextStep = true;
                }
                if (hiddenNextStep) {
                    $(item).hide();
                }
                if (disabledNextStep) {
                    $(item).prop('disabled', true);
                    $(item).show();
                    $(item).addClass("disabled");
                }
            }
            if ($(item).hasClass('sw-btn-prev') || $(item).hasClass('sw-btn-cancel') || $(item).hasClass('sw-btn-draft')) {
                $(item).addClass('btn-aap-secondary');
            }
            if (($(item).hasClass('sw-btn-cancel') || $(item).hasClass('sw-btn-draft')) && typeof coForm_FormWizardParams.isNew != "undefined" && coForm_FormWizardParams.isNew) {
                $(item).prop('disabled', false);
                $(item).removeClass('disabled');
                $(item).show();
            }
            /*if ($(item).hasClass('sw-btn-finish') && stepPosition == "last" && typeof coForm_FormWizardParams.hasFinishBtn != "undefined" && coForm_FormWizardParams.hasFinishBtn && !coForm_FormWizardParams.isFinish) {
                $(item).prop('disabled', false);
                $(item).show();
            }*/

            if (typeof stepOblList != "undefined" && typeof stepOblList[actualStep] != "undefined"){
                var html = "";
                var errorNum = 0;
                $.each(stepOblList[actualStep], function(indexObj,itemObj){
                    if(stepshow){
                        if(typeof coForm_FormWizardParams["answer"]["answers"] != "undefined" && typeof coForm_FormWizardParams["answer"]["answers"][coForm_actualStep] != "undefined" && notNull(coForm_FormWizardParams["answer"]["answers"][coForm_actualStep][indexObj])){
                            // html += '<li class="list-group-item"><a href="javascript:;" style="display: flex !important" class="inputScroll d-flex justify-content-between" data-target="#question_'+indexObj+'"><span class="text-ellipsis oblilist-item-text">'+itemObj.label+'</span><i class="fa fa-check-circle pull-right" style="color: green"></i></a></li>';
                        }else{
                            errorNum++;
                            html += '<li class="list-group-item"><a href="javascript:" style="display: flex !important" class="inputScroll d-flex justify-content-between" data-target="#question_'+indexObj+'"><span class="text-ellipsis oblilist-item-text"">'+itemObj.label+'</span> <i class="fa fa-times-circle pull-right" style="color: red"></i></a></li>';
                        }
                    }else{
                        if(typeof formKey != "undefined" && ownAnswer[formKey]) {
                            if(ownAnswer[formKey][indexObj]) {
                                if(!notEmpty(ownAnswer[formKey][indexObj])) {
                                    errorNum++;
                                    html += '<li class="list-group-item"><a href="javascript:" style="display: flex !important" class="inputScroll d-flex justify-content-between" data-target="#question_'+indexObj+'"><span class="text-ellipsis oblilist-item-text"">'+itemObj.label+'</span> <i class="fa fa-times-circle pull-right" style="color: red"></i></a></li>';
                                }
                            } else {
                                errorNum++;
                                html += '<li class="list-group-item"><a href="javascript:" style="display: flex !important" class="inputScroll d-flex justify-content-between" data-target="#question_'+indexObj+'"><span class="text-ellipsis oblilist-item-text"">'+itemObj.label+'</span> <i class="fa fa-times-circle pull-right" style="color: red"></i></a></li>';
                            }
                        }
                        if($("#question_"+indexObj).hasClass('error-msg')){
                            // errorNum++;
                            // html += '<li class="list-group-item"><a href="javascript:" style="display: flex !important" class="inputScroll d-flex justify-content-between" data-target="#question_'+indexObj+'"><span class="text-ellipsis oblilist-item-text"">'+itemObj.label+'</span> <i class="fa fa-times-circle pull-right" style="color: red"></i></a></li>';
                        }else{
                            // html += '<li class="list-group-item"><a href="javascript:;" style="display: flex !important" class="inputScroll d-flex justify-content-between" data-target="#question_'+indexObj+'"><span class="text-ellipsis oblilist-item-text"">'+itemObj.label+'</span><i class="fa fa-check-circle pull-right" style="color: green"></i></a></li>';
                        }
                    }
                });
                $('.obldroplist').html('');
                $('.obldroplist').html(html);

                $('.inputScroll').click(function(){
                    var tthis = $(this);
                    $('html, body, #dialogContent .modal-content').animate({
                        scrollTop: $(tthis.data('target')).offset().top - $(tthis.data('target')).height()
                    }, 500);
                    return false;
                });
                if(errorNum > 0) {
                    $('.sw-btn-obl').prop('disabled', false);
                    $('.sw-btn-obl').show();
                    $('.sw-btn-obl').removeClass("disabled");
                    $('.badgeobl').html(errorNum);
                    $('.sw-btn-next').prop('disabled', true);
                    if(!$('.oblsign').length) {
                        $('.sw-btn-next').append('<span class="oblsign"><i class="fa fa-ban"></i></span>');
                    }
                }else{
                    $('.sw-btn-obl').prop('disabled', true);
                    $('.sw-btn-obl').hide();
                    $('.sw-btn-obl').addClass("disabled");
                    $('.oblsign').remove();
                    $('.sw-btn-next').prop('disabled', false);
                }
            }else{
                $('.sw-btn-obl').prop('disabled', true);
                $('.sw-btn-obl').hide();
                $('.sw-btn-obl').addClass("disabled");
                $('.oblsign').remove();
                $('.sw-btn-next').prop('disabled', false)
            }
        });
    }

    function onCancelStep(){
        directory.deleteElement(
            "answers" ,
            coForm_FormWizardParams.answer._id.$id ,
            null ,
            function(){
                if(typeof aapObj !== "undefined"){
                    urlCtrl.loadByHash("#proposalAap.context."+aapObj.common.getQuery().context+".formid."+aapObj.common.getQuery().formid);
                }
            },
            "<span class='bold'><i class='fa fa-warning'></i>" + trad["Are you sure you want to delete this answer"] + " ?</span><br><span class='text-red bold'>" + trad.actionirreversible + "</span>",
            trad["Answer successfully deleted"]
        );
    }

    function onDraftStep(){
        var tplCtx = {
            path : "draft",
            collection : "answers",
            id : coForm_FormWizardParams.answer._id.$id,
            value : true,
            setType : "boolean"
        }
        dataHelper.path2Value( tplCtx, function(){
            if(typeof aapObj !== "undefined"){
                urlCtrl.loadByHash("#proposalAap.context."+aapObj.common.getQuery().context+".formid."+aapObj.common.getQuery().formid);
            }
        });
    }

    async function answerNotification(coForm_FormWizardParams){
        await documentReadyPromise;
        ajaxPost(null, baseUrl+'/survey/answer/answernotification',
            {
                id : coForm_FormWizardParams.answer._id.$id ,
                url : window.location.href ,
                parentForm : coForm_FormWizardParams.parentForm
            },
            function(data) {},"html"
        );
    }

    if(typeof checkVisible == "undefined") {
		function checkVisible(elm) {
			var rect = elm.getBoundingClientRect();
			// var rect = elm.offset();
			var viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
			return !(rect.top - viewHeight/2 >= 0);
		}
	}

    if(typeof addError == "undefined") {
		function addError(elem, cssValue = {}, className = "") {
			if(elem instanceof jQuery){
				if(Object.keys(cssValue).length > 0) {
					for([key, value] of Object.entries(cssValue)) {
						elem.css(key, value)
					}
				}
				if(className != "") {
					elem.addClass(className)
				}
			}
		}
	}

    if(typeof removeError == "undefined") {
		function removeError(elem, cssValue = {}, className = "") {
			if(elem instanceof jQuery){
				if(Object.keys(cssValue).length > 0) {
					for([key, value] of Object.entries(cssValue)) {
						elem.css(key, value)
					}
				}
				if(className != "") {
					elem.removeClass(className)
				}
			}
		}
	}

    if(typeof bindSectionCollapseEvent == "undefined") {
        function bindSectionCollapseEvent() {
            $(`[data-schu-toggle="collapse"]`).off("click").on("click", function() {
                const btnToggle = $(this);
                const collapseParent = btnToggle.parent().parent();
                const collapseBody = $(collapseParent.find(btnToggle.attr("data-target")));
                if(collapseBody.is(":visible")) {
                    collapseBody.hide(300);
                } else {
                    collapseBody.show(300);
                }
            })
        }
    }

    stepObj.bindEvent = {
        inputEvents : function (stepObj) {
            $(".editQuestionaap").off("click").on("click",function() {
                var editQuestionBtn = $(this);
                const selectedInputType = editQuestionBtn.data("type");
                const includeInputsType = ["tpls.forms.cplx.checkboxNew", "tpls.forms.cplx.radioNew"];
                var activeForm = {
                    "jsonSchema" : {
                        "title" : tradForm.modifyConfigQuestion,
                        "type" : "object",
                        "properties" : {
                            label : {
                                label : tradForm.titleOfQuestion,
                                value :  (typeof editQuestionBtn.data("label") != "undefined") ? editQuestionBtn.data("label") : ""
                            },
                            placeholder : {
                                label : tradForm.titleInQuestion,
                                value :  (typeof editQuestionBtn.data("placeholder") != "undefined") ? editQuestionBtn.data("placeholder") : ""
                            },
                            info : {
                                inputType : "textarea",
                                label : tradForm.additionalInfoQuestion,
                                value :  (typeof editQuestionBtn.data("info") != "undefined") ? editQuestionBtn.data("info") : ""
                            },
                            isRequired : {
                                "label": "Est-ce une question obligatoire ?",
                                "inputType": "checkboxSimple",
                                "params": {
                                    "onText": "Oui",
                                    "offText": "Non",
                                    "onLabel": "Oui",
                                    "offLabel": "Non",
                                    "labelText": "Est-ce une question obligatoire ?"
                                },
                                "checked": (typeof editQuestionBtn.data("isrequired") != "undefined") ? (/true/i).test(editQuestionBtn.data("isrequired")) : false,
                                "optionsValueAsKey": true
                            },
                            activeComments : {
                                "label": "Activer les commentairs",
                                "inputType": "checkboxSimple",
                                "params": {
                                    "onText": "Activé",
                                    "offText": "Désactivé",
                                    "onLabel": "Activer",
                                    "offLabel": "Désactiver",
                                    "labelText": ""
                                },
                                "checked": (typeof editQuestionBtn.data("activecomments") != "undefined") ? (/true/i).test(editQuestionBtn.data("activecomments")) : false,
                            }
                        },
                        onLoads: {
                            onload: function() {
                                $("#isRequired").val((typeof editQuestionBtn.data("isrequired") != "undefined") ? (/true/i).test(editQuestionBtn.data("isrequired")) : false)
                                $("#activeComments").val((typeof editQuestionBtn.data("activecomments") != "undefined") ? (/true/i).test(editQuestionBtn.data("activecomments")) : false)
                                if (selectedInputType && includeInputsType.includes(selectedInputType)) {
                                    $("#activeMultieval").val((typeof editQuestionBtn.data("activemultieval") != "undefined") ? (/true/i).test(editQuestionBtn.data("activemultieval")) : false)
                                    if ((/true/i).test(editQuestionBtn.data("activemultieval"))) {
                                        $("#evaluationKey").parent().show()
                                    } else {
                                        $("#evaluationKey").parent().hide()
                                    }
                                    $(".activeMultievalcheckboxSimple .btn-dyn-checkbox").on("click", function() {
                                        if (/true/.test($("#activeMultieval").val())) {
                                            $("#evaluationKey").parent().show(300)
                                        } else {
                                            $("#evaluationKey").parent().hide(300)
                                        }
                                    })
                                }
                            }
                        }
                    }
                };

                if (selectedInputType && includeInputsType.includes(selectedInputType)) {
                    activeForm.jsonSchema.properties.activeMultieval = {
                        "label": "Activer l'évaluation multiple",
                        "inputType": "checkboxSimple",
                        "params": {
                            "onText": "Activé",
                            "offText": "Désactivé",
                            "onLabel": "Activer",
                            "offLabel": "Désactiver",
                            "labelText": ""
                        },
                        "checked": (typeof editQuestionBtn.data("activemultieval") != "undefined") ? (/true/i).test(editQuestionBtn.data("activemultieval")) : false,
                    },
                    activeForm.jsonSchema.properties.evaluationKey = {
                        "label": "Clé d'évaluation",
                        "inputType": "text",
                        "value":  (typeof editQuestionBtn.data("evaluationkey") != "undefined") ? editQuestionBtn.data("evaluationkey") : ""
                    }
                }
                tplCtx.id = editQuestionBtn.data("id");
                tplCtx.collection = editQuestionBtn.data("collection");
                tplCtx.formParentId = editQuestionBtn.data("parentid");
                tplCtx.path = editQuestionBtn.data("path");
                tplCtx.updatePartial = true;
                activeForm.jsonSchema.save = function () {
                    tplCtx.value = {};
                    $.each( activeForm.jsonSchema.properties , function(k,val) {
                        tplCtx.value[k] = $("#"+k).val();
                        if(k == "type") {
                            tplCtx.value[k] = editQuestionBtn.data("type")
                        }
                    })

                    // tplCtx.value["label"] = $("#label").val();
                    // tplCtx.value["placeholder"] = $("#placeholder").val();
                    // tplCtx.value["info"] = $("#info").val();
                    // tplCtx.value["type"] = editQuestionBtn.data("type");

                    // if(editQuestionBtn.data("position")){
                    //     tplCtx.value["position"] = editQuestionBtn.data("position");
                    // }

                    // if(editQuestionBtn.data("positions")){
                    //     tplCtx.value["positions"] = editQuestionBtn.data("positions");
                    // }

                    mylog.log("save tplCtx",tplCtx);
                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            $("#ajax-modal").modal('hide');
                            if(typeof reloadInput != "undefined") {
                                reloadInput(editQuestionBtn.data("key"), editQuestionBtn.data("formid"), () => stepObj.bindEvent.inputEvents(stepObj))
                            }
                        } );
                    }
                }
                dyFObj.openForm( activeForm );
            });
        }
    }

    if(typeof newReloadStepValidationInputGlobal == 'undefined') {
        function newReloadStepValidationInputGlobal(unikParams = {}) {
            let ownAnswerBefore = {}
            if(typeof ownAnswer != "undefined")
                ownAnswerBefore = JSON.parse(JSON.stringify(ownAnswer))
            ajaxPost(null, baseUrl + '/co2/aap/refreshanswer',
                {
                    id : coForm_FormWizardParams.answer?.["_id"]?.["$id"] ? coForm_FormWizardParams.answer?.["_id"]?.["$id"] : ""
                },
                function (res) {
                    if(typeof aapObj != "undefined" && aapObj.directory && aapObj.directory.reloadAapMiniItem && res?.["_id"]?.["$id"] && $(`#propItem${ res?.["_id"]?.["$id"] }`).length > -1) {
                        aapObj.directory.reloadAapMiniItem(aapObj, res?.["_id"]?.["$id"])
                    }
                    if (notEmpty(res) && res.answers && typeof ownAnswer != "undefined") {
                        ownAnswer = res.answers;
                        updateWizardBtn(coForm_FormWizardParams , null , null);
                        ajaxPost(null, typeof coWsConfig != "undefined" && coWsConfig.pingRefreshViewUrl ? coWsConfig.pingRefreshViewUrl : null, {
							action : "proposalEditedByForm",
							answerId : res?.["_id"]?.["$id"],
                            userSocketId : typeof aapObj != "undefined" && aapObj && aapObj?.userSocketId ? aapObj.userSocketId : null,
                            responses : {...res, ...{nameUser : userConnected?.name ? userConnected?.name : '(No name)', editedInput : unikParams?.inputKey ? unikParams.inputKey : null}},
						}, null, null, {contentType : 'application/json'});

                        if(typeof formKey != "undefined" && ownAnswer[formKey] && Object.keys(ownAnswer[formKey]).length > 0) {
                            for([ansKey, ansVal] of Object.entries(ownAnswer[formKey])) {
                                if(ownAnswer[formKey][ansKey]) {
                                    elem = $("#question_"+ansKey)
                                    if(notEmpty(ownAnswer[formKey][ansKey]) && typeof removeError != "undefined") {
                                        removeError(
                                            elem,
                                            {
                                                'border' : 'none',
                                            },
                                            'error-msg'
                                        )
                                    }
                                }
                            }
                            for([ansKey, ansVal] of Object.entries(ownAnswerBefore[formKey])) {
                                if(ownAnswerBefore[formKey][ansKey]) {
                                    elem = $("#question_"+ansKey)
                                    if(notEmpty(ownAnswer[formKey][ansKey]) && typeof removeError != "undefined") {

                                    } else if(typeof addError != "undefined" && coForm_FormWizardParams?.step?.inputs?.[ansKey]?.isRequired == true) {
                                        addError(
                                            elem,
                                            {
                                                'border' : '1px solid #a94442'
                                            },
                                            'error-msg'
                                        )
                                    }
                                }
                            }
                        }
                    }
                    if(typeof previewFormRefresh != "undefined" && typeof previewFormRefresh == "function") {
                        previewFormRefresh();
                    }
                }, null
            );
        }
    }

    if(typeof coformBuilder == 'undefined') {
        var coformBuilder = {
            initTabs : function(args) {
                var htmlContent = `<div class="col-xs-12 p-2">`;
                var headerTitle = trad.aboutElement;
                var headerIcon = "cog";
                if(!args.kunik)
                    args.kunik = "rundomkey";
                var footerContent = `
                    <div class="d-flex px-3">
                        <button class="btn btn-sm btn-danger save-coform mr-2 disabled" data-kunik="${args.kunik}">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i>${ trad.save }
                        </button>
                        <button class="btn btn-sm btn-primary cancel-coform" data-kunik="${args.kunik}">
                            <i class="fa fa-times" aria-hidden="true"></i>${ trad.cancel }
                        </button>
                    </div>
                `;
                
                const btnSelector = `.save-coform[data-kunik="${args.kunik}"]`;

                args?.headerTitle ? headerTitle = args.headerTitle : "";
                args?.headerIcon ? headerIcon = args.headerIcon : "";
                args?.footerContent ? footerContent = args.footerContent : "";

                htmlContent += `
                    <div class="col-xs-12 p-2 mb-3 side-tabs-header">
                        <span class="side-tabs-title">
                            <span class="pr-2">
                                <i class="fa fa-${ headerIcon }"></i>
                            </span> 
                            <span>${ headerTitle }</span>
                        </span>
                    </div>
                    <div class="col-xs-12 p-0 side-tabs-body">
                    </div>
                    <div class="col-xs-12 p-0 side-tabs-footer">
                        ${ footerContent }
                    </div>
                </div>
                `;
                args?.container ? $(args?.container).append(htmlContent) : "";
                $(btnSelector).off("click").on("click", function() {
                    if(args.onSave)
                        args.onSave();
                })
                $(`.cancel-coform[data-kunik="${ args.kunik }"]`).off("click").on("click", function() {
                    $("#coformRightPanel").empty();
                    $(".btn-toggle-coformbuilder-sidepanel").trigger("click")
                })
            }
        }
    }

    if(typeof syncSection == 'undefined') {
        var TOP_MARGIN = 0.1,
            BOTTOM_MARGIN = 0.2,
            lastItemVisible = null;
            lastVisible = null;
        function syncSection() {
            var windowHeight = window.innerHeight;

            var visibleItems = 0;

            if(typeof tocItems == "undefined") {
                var tocItems = [].slice.call( $('.coform-nav li' ) );

                // Cache element references and measurements
                tocItems = tocItems.map( function( item ) {
                    var anchor = $(item).find( 'a' );
                    var target = document.getElementById(anchor.attr('data-target').slice(1));

                    return {
                        listItem: item,
                        anchor: anchor,
                        target: target
                    };
                } );

                // Remove missing targets
                tocItems = tocItems.filter( function( item ) {
                    return !!item.target;
                } );
            }
            tocItems.forEach((item, key) => {

                var targetBounds = item.target.getBoundingClientRect();

                if(lastVisible != null && $(item.target)[0] == $(lastVisible.target)[0] && lastVisible.target.getBoundingClientRect().top > windowHeight * ( 1 - BOTTOM_MARGIN )) {
                    if(key != 0)
                        $(lastVisible.listItem).removeClass('active');
                    if(tocItems[key-1]) {
                        $(tocItems[key-1].listItem).addClass("active");
                        lastVisible = tocItems[key-1];
                        lastItemVisible = $(tocItems[key-1].listItem)[0];
                    }
                }
                if( targetBounds.bottom > windowHeight * TOP_MARGIN && targetBounds.top < windowHeight * ( 1 - BOTTOM_MARGIN ) ) {

                    visibleItems += 1;

                    lastVisible = item;
                    $(item.listItem).addClass( 'active' );
                    if(lastItemVisible != null && $(item.listItem)[0] != lastItemVisible) {
                        $(lastItemVisible).removeClass('active')
                    }
                    lastItemVisible = $(item.listItem)[0]
                }else if(lastItemVisible == null && $(item.listItem)[0] != lastItemVisible){
                    $(item.listItem).removeClass( 'active' );
                }

            } );
        }
    }

    $(document).ready(function() {
        if($('.schu-sticky')) {
            var navmenuform = $('.schu-sticky');
        }

        function scrollform() {
            if ($(window).scrollTop() >= $('#mainNav').height() + $('drop-down-sub-menu').height() + $('ul.nav').height() + $('.schu-btn-sticky').height()) {
                const subMenuHeight = $(".dropdown-menu.drop-down-sub-menu").is(":visible") ? 50 : 0;
                const coformBodyTop = $(".coFormbody.coform-stepbody.sticky-to").offset()?.top - ($("#proposition-summary").is(":visible") && $(window).width() > 766 ? 100 : 0);
                let wizardBannerHeight = ($(".wizard-banner")?.offset()?.top + $(".wizard-banner")?.height() - 100) > $(window).scrollTop() ? $(".wizard-banner").height() : 0;
                if(navmenuform && !$("#dialogContent.modal-custom").is(":visible")) {
                    navmenuform.addClass('coform-navbar-fixed-top');
                    const configNavHeight = $("#confignav").is(":visible") ? 30 : 0;
                    if(coformBodyTop > $(window).scrollTop())
                        navmenuform.css("margin-top", coformBodyTop - ($(window).scrollTop()-($(window).scrollTop()/2)) + configNavHeight)
                    else
                        navmenuform.css('margin-top', ($('#mainNav').height() + subMenuHeight + $('ul.nav').height() + wizardBannerHeight + 10 + configNavHeight)+'px');
                    // navmenuform.css('margin-top', ($('#mainNav').height() + subMenuHeight + $('ul.nav').height() + wizardBannerHeight + 10)+'px');
                }
                if($('.schu-btn-sticky').height() > 0 && !$("#dialogContent.modal-custom").is(":visible")){
                    // navmenuform.css('margin-top', ($('#mainNav').height()+ subMenuHeight + $('.schu-btn-sticky').height()+ wizardBannerHeight - 10 + 5)+'px');
                    $('.schu-btn-sticky').addClass('coform-navbar-fixed-top');
                    if(coformBodyTop > $(window).scrollTop())
                        $('.schu-btn-sticky').css("margin-top", coformBodyTop - ($(window).scrollTop()-($(window).scrollTop()/4)))
                    else
                        $('.schu-btn-sticky').css('margin-top', ($('#mainNav').height() + subMenuHeight + wizardBannerHeight + 10)+'px');
                }else{

                }
            } else {
                if(navmenuform && !$("#dialogContent.modal-custom").is(":visible")) {
                    navmenuform.removeClass('coform-navbar-fixed-top');
                    navmenuform.css('margin-top', '10px');
                }
                if($('.schu-btn-sticky').height() > 0 && !$("#dialogContent.modal-custom").is(":visible")){
                    $('.schu-btn-sticky').removeClass('coform-navbar-fixed-top');
                    $('.schu-btn-sticky').css('margin-top', '0px');
                }
            }
        }

        window.addEventListener('scroll', scrollform);
        window.addEventListener('resize', scrollform);

        if (typeof costum != "undefined" && costum.slug && costum[costum.slug] && costum[costum.slug].costumColors && costum[costum.slug].costumColors.primaryColor) {
            var currentStyle = $(".coFormbody#smartwizard").attr("style")
            var modifiedStyle = ""
            if (currentStyle && currentStyle.includes("--clr_var")) {
                const keysToReplace = ["--clr_var", "--clr_dark_var", "--pfpb-primary-green-color"]
                const costumColorsMap = {
                    "--clr_var": costum[costum.slug].costumColors.primaryColor,
                    "--clr_dark_var": costum[costum.slug].costumColors.primaryDark ? costum[costum.slug].costumColors.primaryDark : "#3b9387",
                    "--pfpb-primary-green-color": costum[costum.slug].costumColors.primaryColor,
                }
                $.each(keysToReplace, function(index, keyToReplace) {
                    var newColor = costumColorsMap[keyToReplace];
                    var regex = new RegExp(`(${keyToReplace}\\s*:\\s*)(#[a-fA-F0-9]{6}|#[a-fA-F0-9]{3})(;)`);
                    modifiedStyle = currentStyle.replace(regex, `$1${newColor}$3`);
                    currentStyle = modifiedStyle;
                });
                $(".coFormbody#smartwizard").attr("style", modifiedStyle)
            } 
        }

        coInterface.showCostumLoader(".coFormbody#smartwizard .sw-custom-loader")

        if(typeof coForm_FormWizardParams.msg != 'undefined' && coForm_FormWizardParams.msg != ""){
            toastr.success(coForm_FormWizardParams.msg);
        }
        stepObj.bindEvent.inputEvents(stepObj);
        if(isAjaxRequest == false || isAjaxRequest == 'false') {
            $('.st-actionContainer').launchBtn({defaultState: false, documentClick: true});

            $(".questionPreview").on("click", function() {
                var that = this;
                $("#questionPreview .modal-content").empty();
                $("#questionPreview .modal-content").append(`
                    <div class="col-xs-12 content p-0">
                    <div class="modal-header bg-dark text-center">
                        <button type="button" class="close color-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">${$(that).attr("data-title")}</h4>
                    </div>
                    <div class="modal-body">
                        <img class="preview-img" src="${$(that).attr("data-preview")}"></img>
                        <div class="col-xs-12">
                        <h5>Description</h5>
                        <p>${$(that).attr("data-desc")}</p>
                        </div>
                    </div>
                    </div>
                `);

                var contentHeight = setInterval(function() {
                    const contentHeight = $(".modal.preview-modal .content").height();
                    $("#questionPreview .modal-content").css({height: contentHeight});
                    clearInterval(contentHeight);
                }, 200)
                $("#questionPreview").modal('show')
            });

            $('.coform-navbar a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // if($(e.target).attr("href") == "#inputlist") {
                //     stPanelInterval = setInterval(function() {
                //     // if($(".settings.st-panel").is(":visible")) {
                //         $(".st-panel").show().animate({opacity: 1}, 300)
                //         clearInterval(stPanelInterval)
                //     // }
                //     }, 700);
                // }
            });
            $('.addStepBtn').off("click").on("click",function() {
                tplCtx.id = $(this).data("id");
                let form = JSON.parse(JSON.stringify(coForm_FormWizardParams?.parentForm));
                let subForms = coForm_FormWizardParams?.parentForm?.subForms
                var addStepParams = {
                    jsonSchema : {
                        title : trad.addstep,
                        description : trad.aformcancontainoneormorestep,
                        icon : "wpforms",
                        properties : {
                            name : {
                                label : trad.namethisstep,
                                value : "",
                                rules : {
                                    required : true
                                }
                            }
                        },
                        save : function (formData) {
                            mylog.log('addStepParams  save formData', formData)

                            tplCtx.collection = "forms";
                            tplCtx.path = "subForms";
                            tplCtx.value = subForms;
                            tplCtx.updatePartial = true;
                            if( formData.name.indexOf("form:") >= 0 ){
                                var parts = formData.name.split(":");
                                mylog.log( "addStepParams added existing form", parts[1] );

                                if( typeof tplCtx.value == "undefined" )
                                    tplCtx.value = [ parts[1] ];
                                else
                                    tplCtx.value.push( parts[1] );

                                if(typeof tplCtx.value == "undefined")
                                    toastr.error('value cannot be empty!');
                                else {
                                    mylog.log("addStepParams save tplCtx",tplCtx);
                                    dataHelper.path2Value( tplCtx, function(params) {
                                        reloadWizard();
                                        dyFObj.closeForm();
                                    } );
                                }
                            } else {

                                var now = new Date();
                                var cx = ""+now.getDate() + (now.getMonth()+1) + now.getFullYear()+"_"+now.getHours()+now.getMinutes();

                                var newFormId = coForm_FormWizardParams.isAap ? "aapStep"+ (coForm_FormWizardParams?.parentForm?.subForms?.length + 1) : aapObj?.context?.slug+cx+"_"+tplCtx.value.length;
                                var stepInputCt = Date.now().toString(36) + Math.random().toString(36).substring(2);
                                tplCtx.value.push(newFormId);

                                var newForm = {
                                    collection : coForm_FormWizardParams.isAap ? "inputs" : "forms",
                                    value : !coForm_FormWizardParams.isAap ? {
                                        id : newFormId,
                                        name : formData.name,
                                        type : "openForm",
                                        // inputs : isCoform ? {
                                        //     [newFormId+stepInputCt] : {
                                        //         label : "default step validation",
                                        //         type : "tpls.forms.cplx.validateStep"
                                        //     }
                                        // } : {}
                                    } : {
                                        isSpecific : true,
                                        formParent : coForm_FormWizardParams?.parentForm?.['_id']?.["$id"],
                                        step : newFormId,
                                        type : "openForm",
                                        inputs : {}
                                    }
                                };
                                mylog.log("addStepParams new sub Form",newForm);
                                dataHelper.path2Value( newForm, function(inputInserted) {
                                    mylog.log("addStepParams created new sub Form",inputInserted);
                                    if(typeof tplCtx.value == "undefined")
                                        toastr.error('value cannot be empty!');
                                    else {
                                        let formKey = tplCtx.value.length -1 > 0 ? tplCtx.value[tplCtx.value.length -1] : "aapStep"+(tplCtx.value.length +1);
                                        if(coForm_FormWizardParams.isAap) {
                                            let paramsToSend = {
                                                id : tplCtx.id,
                                                collection : tplCtx.collection,
                                                path : "params."+formKey,
                                                value : {
                                                    haveEditingRules : false,
                                                    haveReadingRules : false,
                                                    canEdit : "",
                                                    canRead : ""
                                                },
                                                updatePartial : true
                                            }
                                            dataHelper.path2Value( paramsToSend, function() {})
                                        }
                                        mylog.log("addStepParams save tplCtx",tplCtx);
                                        dataHelper.path2Value( tplCtx, function(params) {
                                            if(coForm_FormWizardParams.isAap) {
                                                let configFormParams = {
                                                    id : coForm_FormWizardParams?.configForm?.["_id"]?.["$id"],
                                                    collection : tplCtx.collection,
                                                    path : tplCtx.path+"."+formKey,
                                                    value : {
                                                        inputs : inputInserted.result ? inputInserted?.saved?.["_id"]?.["$id"] : "null",
                                                        name : formData.name,
                                                        params : {
                                                            haveEditingRules : false,
                                                            haveReadingRules : false,
                                                            canEdit : [
                                                                ""
                                                            ],
                                                            canRead : [
                                                                ""
                                                            ]
                                                        }
                                                    },
                                                    updatePartial : true
                                                }
                                                dataHelper.path2Value( configFormParams, function() {})
                                            }
                                            // if(parentFormStep.hasStepValidations) {
                                            //     if(isCoform && parentFormStep.hasStepValidations == "") {
                                            //         const configForm = {
                                            //             id : tplCtx.id,
                                            //             collection : tplCtx.collection,
                                            //             path : "hasStepValidations",
                                            //             value : 0
                                            //         }
                                            //         dataHelper.path2Value(configForm, function(){})
                                            //     }
                                            // }
                                            // if(tplCtx.value.length > 1 && isCoform) {
                                            //     var newParams = {};
                                            //     var newParentForm = {...parentFormStep};
                                            //     if(params.elt) {
                                            //         if(params.elt['params'])
                                            //         newParentForm['params'] = {...params.elt['params']};
                                            //     }
                                            //     if(newParentForm.params) {
                                            //         for([paramsKey, paramsValue] of Object.entries(newParentForm.params)) {
                                            //             if(newParentForm['params'][paramsKey]['step']) {
                                            //                 tplCtx.value.forEach((subValue, subIndex) => {
                                            //                     if(paramsKey.includes('validateStep'+subValue)) {
                                            //                         if(tplCtx.value[subIndex+1]) {
                                            //                             paramsValue.step = tplCtx.value[subIndex+1]
                                            //                         } else {
                                            //                             paramsValue.step = subValue
                                            //                         }
                                            //                         newParams[paramsKey] = paramsValue
                                            //                     } else {
                                            //                         newParams[paramsKey] = paramsValue
                                            //                     }
                                            //                 })
                                            //             } else {
                                            //                 newParams[paramsKey] = paramsValue
                                            //             }
                                            //         }
                                            //     }
                                            //     newParams['validateStep'+newFormId+stepInputCt] = {
                                            //         step : newFormId
                                            //     }
                                            //     const configFormParams = {
                                            //         id : tplCtx.id,
                                            //         collection : tplCtx.collection,
                                            //         path : "params",
                                            //         value : newParams
                                            //     }
                                            //     dataHelper.path2Value(configFormParams, function(){})
                                            // } else if(tplCtx.value.length == 1 && isCoform) {
                                            //     const configFormParams = {
                                            //         id : tplCtx.id,
                                            //         collection : tplCtx.collection,
                                            //         path : "params.validateStep"+newFormId+stepInputCt+".step",
                                            //         value : newFormId
                                            //     }
                                            //     dataHelper.path2Value(configFormParams, function(){})
                                            // }
                                            urlCtrl.loadByHash(location.hash);
                                            reloadWizard();
                                            dyFObj.closeForm();
                                        } );
                                    }
                                } );
                            }
                        }
                    }
                };
                dyFObj.openForm(addStepParams, null, form )
            });

            var completSubForms = {};
            let subForms = [];
            let lastSubformId = "null";
            let formsInputs = {};
            if(coForm_FormWizardParams.steps) {
                const stepLength = Object.keys(coForm_FormWizardParams.steps).length;
                let stepsIteration = 0;
                for([stepKey, stepVal] of Object.entries(coForm_FormWizardParams.steps)) {
                    if(stepVal.name) {
                        completSubForms[stepKey] = stepVal.name;
                        subForms.push(stepVal.id)
                    }
                    formsInputs[stepKey] = stepVal.inputs ? JSON.parse(JSON.stringify(stepVal.inputs)) : {};
                    if(stepsIteration == stepLength-1) {
                        lastSubformId = {};
                        lastSubformId["_id"] = {
                            "$id" : stepKey
                        }
                        mylog.log("last step", lastSubformId);
                    }
                    stepsIteration++;
                }
            }
            $('.createFrom').off("click").on('click', function() {
                var createFromParams = {
                    jsonSchema : {
                        title : trad['Create form from text'],
                        description : trad['paste text here'],
                        icon : "wpforms",
                        properties : {
                            textvalue : {
                                label : trad['paste text here'],
                                inputType : 'textarea',
                                markdown : true,
                                init: function() {
                                    var markdownInterval = setInterval(function() {
                                        if($('.md-header.btn-toolbar .btn-group').length > 0) {
                                            $('.md-header.btn-toolbar .btn-group').hide();
                                            $('.md-header.btn-toolbar .dropdown').show().find('button.dropdown-toggle').removeClass("hide");
                                            clearInterval(markdownInterval);
                                            markdownInterval = null;
                                        }
                                    }, 300)
                                    $("#textvalue").off().on('blur', function() {
                                        if($(this).val().indexOf('[step]') > -1) {
                                            $('.toNewStepradio').hide().animate({opacity: 0}, 600);
                                            $('.newSubFormTitletext').hide().animate({opacity: 0}, 600)
                                        } else {
                                            let val = $('.toNewStepradio').find('input[type="radio"]:checked').val();
                                            if(val == 'non') {
                                                $('.toNewStepradio').show().animate({opacity: 1}, 600)
                                                // $('.newSubFormTitletext').show().animate({opacity: 1}, 600)
                                                if(Object.keys(completSubForms).length > 0) $('.existStepselect').show();
                                                $('.newSubFormTitletext').hide();
                                            } else {
                                                $('.existStepselect').hide();
                                                $('.newSubFormTitletext').show();
                                            }
                                        }
                                    })
                                }
                            },
                            // dyFInputs.radio( "Display Images ?", { "true" : { icon:"check-circle-o", lbl:trad.yes },
                            // 							 			"false" : { icon:"circle-o", lbl:trad.no} } )
                            toNewStep : {
                                label : 'Souhaitez vous créer une nouvelle étape',
                                inputType: 'radio',
                                options: {
                                    oui: {
                                        lbl: 'Oui',
                                        icon: 'check',
                                        class: 'active class-sty',

                                    },
                                    non: {
                                        lbl: 'Non',
                                        icon: 'times',
                                        class: 'class-sty',
                                    }
                                },
                                value: 'oui',
                                init : function(){
                                    $("label.class-sty").off().on("click",function()
                                    {
                                        let val = $(this).find('input[type="radio"]').val();
                                        if(val == 'non') {
                                            if(Object.keys(completSubForms).length > 0) $('.existStepselect').show();
                                            $('.newSubFormTitletext').hide();
                                        } else {
                                            $('.existStepselect').hide();
                                            $('.newSubFormTitletext').show();
                                        }
                                    });
                                    //manage update bulding here
                                }
                            },
                            newSubFormTitle : {
                                label : 'Titre de la nouvelle étape',
                                inputType : 'text',
                                value : 'Form name'
                            },
                            existStep : {
                                label : 'Séléctionner une étape où ajouter les questions',
                                inputType: 'select',
                                select2 : true,
                                placeholder : 'Sélectionner une étape',
                                options : completSubForms,
                                value : completSubForms ? Object.keys(completSubForms)[0] : '',
                                init : function() {
                                    $('.existStepselect').hide()
                                }
                            }
                        },
                        save : function (formData) {
                            tplCtx.collection = "forms";
                            tplCtx.path = "subForms";
                            tplCtx.value = subForms;
                            let parentFormIdForNewStep = coForm_FormWizardParams.parentForm?.["_id"]?.["$id"];

                            if(formData.textvalue != undefined) {

                                if(formData.textvalue.indexOf('-') > -1) {
                                    var now = new Date();
                                    var cx = ""+now.getDate() + (now.getMonth()+1) + now.getFullYear()+"_"+now.getHours()+now.getMinutes();

                                    var newFormId = coForm_FormWizardParams.isAap ? "aapStep"+ (coForm_FormWizardParams?.parentForm?.subForms?.length + 1) : aapObj?.context?.slug+cx+"_"+tplCtx.value.length;
                                    tplCtx.value.push(newFormId);

                                    var newForm = {
                                        collection : coForm_FormWizardParams.isAap ? "inputs" : "forms",
                                        value : !coForm_FormWizardParams.isAap ? {
                                            id : newFormId,
                                            name : 'subfromname',
                                            textvalue : formData.textvalue,
                                            type : "openForm",
                                            inputs : {}
                                        } : {
                                            isSpecific : true,
                                            formParent : coForm_FormWizardParams?.parentForm?.['_id']?.["$id"],
                                            step : newFormId,
                                            type : "openForm",
                                            inputs : {}
                                        }
                                    };

                                    var params = {
                                        id: parentFormIdForNewStep,
                                        collection: "forms",
                                        path: "params",
                                        value: {
                                        },
                                        formParentId: parentFormIdForNewStep,
                                        key: "",
                                        updatePartial: true,
                                    };
                                    if(coForm_FormWizardParams.isAap) {
                                        params.value[newFormId] = {
                                            haveEditingRules : false,
                                            haveReadingRules : false,
                                            canEdit : "",
                                            canRead : ""
                                        };
                                    }
                                    var configFormParams = {
                                        id : coForm_FormWizardParams?.configForm?.["_id"]?.["$id"],
                                        collection : "forms",
                                        path : "subForms",
                                        value : {
                                            [newFormId] : {
                                                inputs : "",
                                                name : 'subformname',
                                                params : {
                                                    haveEditingRules : false,
                                                    haveReadingRules : false,
                                                    canEdit : [
                                                        ""
                                                    ],
                                                    canRead : [
                                                        ""
                                                    ]
                                                }
                                            }
                                        },
                                        updatePartial : true,
                                    }
                                    // textArray = newForm.value.textvalue.split("\n");
                                    textArray = formData.textvalue.match(/[^\r\n|\r|\n]+/g).filter(elem => elem.trim());
                                    var lastKUnik = "";
                                    let inputType = '';
                                    let formNames = [];
                                    let forms = {};
                                    textArray.forEach((element, iterIndex) => {
                                        if(notEmpty(element.trim())) {
                                            if(iterIndex == 0) {
                                                if(element.match(/\[.+]/g) != '[step]') {
                                                    formNames.push('New subForm');
                                                    forms[formNames[formNames.length -1]+'_'+formNames.length] = {}
                                                }
                                            }
                                            if(element.match(/\[.+]/g)) {
                                                if(element.match(/\[.+]/g) == '[step]') {
                                                    formNames.push(element.replace(/\[.+\]|-\[.+\]|-\s\[.+\]|-\s|\[|\]|\]\s|:|\r/g, ''));
                                                    forms[formNames[formNames.length -1]+'_'+formNames.length] = {};
                                                    if(iterIndex > 0) {
                                                        newFormId = coForm_FormWizardParams.isAap ? "aapStep"+ (tplCtx.value.length + 1) : aapObj?.context?.slug+cx+"_"+tplCtx.value.length;
                                                        tplCtx.value.push(newFormId);
                                                        newForm = {
                                                            collection : coForm_FormWizardParams.isAap ? "inputs" : "forms",
                                                            value : !coForm_FormWizardParams.isAap ? {
                                                                id : newFormId,
                                                                name : 'subfromname',
                                                                textvalue : formData.textvalue,
                                                                type : "openForm",
                                                                inputs : {}
                                                            } : {
                                                                isSpecific : true,
                                                                formParent : coForm_FormWizardParams?.parentForm?.['_id']?.["$id"],
                                                                step : newFormId,
                                                                type : "openForm",
                                                                inputs : {}
                                                            }
                                                        };
                                                        if(coForm_FormWizardParams.isAap) {
                                                            params.value[newFormId] = {
                                                                haveEditingRules : false,
                                                                haveReadingRules : false,
                                                                canEdit : "",
                                                                canRead : ""
                                                            };
                                                            configFormParams.value[newFormId] = {
                                                                inputs : "",
                                                                name : 'subformname',
                                                                params : {
                                                                    haveEditingRules : false,
                                                                    haveReadingRules : false,
                                                                    canEdit : [
                                                                        ""
                                                                    ],
                                                                    canRead : [
                                                                        ""
                                                                    ]
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            let inputCt = Date.now().toString(36) + Math.random().toString(36).substring(2);
                                            path = newFormId+inputCt
                                            if(element.match(/^(\s{2,})|((?<=\n(\s)+))(\s{2,})/g)) {
                                                switch (inputType) {
                                                    case 'tpls.forms.cplx.evaluation': {
                                                        const quadrantsLength = Object.keys(params.value[lastKUnik].quadrants).length;
                                                        params.value[lastKUnik].quadrants['quadrant'+quadrantsLength] = {
                                                            label : "ce qui est a modifier",
                                                            description : element.replace(/-\s{1,}|-\s{2,}|\s{2,}|\r/g, '').trim(),
                                                            inputNb : 2
                                                        };
                                                        params.value[lastKUnik].transformationQuadrants['quadrantTransform'+quadrantsLength] = {
                                                            label : "CRÉER",
                                                            description : "À Créer",
                                                            inputNb : 1
                                                        }
                                                    } break;
                                                    case 'tpls.forms.select': {
                                                        params.value[lastKUnik].options.push(element.replace(/-\s{1,}|-\s{2,}|\s{2,}|\r/g, '').trim())
                                                    } break;
                                                    case 'tpls.forms.cplx.multiCheckboxPlus': {
                                                        var currElem = element.replace(/-\s{1,}|-\s{2,}|\s{2,}|\r/g, '');
                                                        params.value[lastKUnik].global.list.push(currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim());
                                                        if(currElem.toLowerCase().includes("autres")) {
                                                            params.value[lastKUnik].tofill[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim()] = "cplx";
                                                            if(currElem.match(/\s+`.+`|`.+`/g)) {
                                                                params.value[lastKUnik].placeholdersckb[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim()] = currElem.match(/\s+`.+`|`.+`/g)[0].replace(/\s*``.+``|``.+``|\s*`|`/g, '')
                                                            }
                                                            if(currElem.match(/\s+``.+``|``.+``/g)) {
                                                                params.value[lastKUnik].optinfo[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim()] = currElem.match(/\s+``.+``|``.+``/g)[0].replace(/\s+`|`/g, '')
                                                            }
                                                        } else {
                                                            if(currElem.toLowerCase().match(/\[cplx\]/g)) {
                                                                params.value[lastKUnik].tofill[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim()] = "cplx"
                                                                if(currElem.match(/\s+`.+`|`.+`/g)) {
                                                                    params.value[lastKUnik].placeholdersckb[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim()] = currElem.match(/\s+`.+`|`.+`/g)[0].replace(/\s*``.+``|``.+``|\s*`|`/g, '')
                                                                }
                                                                if(currElem.match(/\s+``.+``|``.+``/g)) {
                                                                    params.value[lastKUnik].optinfo[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim()] = currElem.match(/\s+``.+``|``.+``/g)[0].replace(/\s+`|`/g, '')
                                                                }
                                                            } else {
                                                                params.value[lastKUnik].tofill[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]/g, '').trim()] = "simple"
                                                            }
                                                        }
                                                    } break;
                                                    case 'tpls.forms.cplx.multiRadio' : {
                                                        var currElem = element.replace(/-\s{1,}|-\s{2,}|\s{2,}|\r/g, '');
                                                        params.value[lastKUnik].global.list.push(currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim());
                                                        if(currElem.toLowerCase() == "autres") {
                                                            params.value[lastKUnik].tofill[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim()] = "cplx";
                                                            if(currElem.match(/\s`.+`|`.+`/g)) {
                                                                params.value[lastKUnik].placeholdersradio[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim()] = currElem.match(/\s+`.+`|`.+`/g)[0].replace(/\s+`|`|`\s+``.+``|``.+``|\s+``.+``|``.+``/g, '')
                                                            }
                                                            if(currElem.match(/\s``.+``|``.+``/g)) {
                                                                params.value[lastKUnik].optinfo[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim()] = currElem.match(/\s``.+``|``.+``/g)[0].replace(/\s+`|`/g, '')
                                                            }
                                                        } else {
                                                            if(currElem.toLowerCase().match(/\[cplx\]/g)) {
                                                                params.value[lastKUnik].tofill[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim()] = "cplx";
                                                                if(currElem.match(/\s`.+`|`.+`/g)) {
                                                                    params.value[lastKUnik].placeholdersradio[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim()] = currElem.match(/\s+`.+`|`.+`/g)[0].replace(/\s+`|`|`\s+``.+``|``.+``|\s+``.+``|``.+``/g, '')
                                                                }
                                                                if(currElem.match(/\s``.+``|``.+``/g)) {
                                                                    params.value[lastKUnik].optinfo[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]|\s`.+`|`.+`|\s``.+``|``.+``/g, '').trim()] = currElem.match(/\s``.+``|``.+``/g)[0].replace(/\s+`|`/g, '')
                                                                }
                                                            } else {
                                                                params.value[lastKUnik].tofill[currElem.replace(/\s\[cplx\]|\[cplx\]\s|\[cplx\]/g, '').trim()] = "simple"
                                                            }
                                                        }
                                                    } break;
                                                    case 'tpls.forms.cplx.simpleTable' : {
                                                        var currElem = element.replace(/-\s{1,}|-\s{2,}|\s{2,}|\r/g, '');
                                                        if(currElem.match(/\[.+\]/g)) {
                                                            var currentMatch = currElem.match(/\[.+\]/g)[0].replace(/\[|\]/g, '').toLowerCase();
                                                            if(currentMatch == "columns") {
                                                                if(currElem.toLowerCase().match(/\(nombre\)/g)) {
                                                                    params.value[lastKUnik].columns.push({
                                                                        label : currElem.replace(/\[columns\]|-\[columns\]|-\s\[columns\]|\(nombre\)|\(Nombre\)|\s\(nombre\)|\s\(Nombre\)/g, '').trim(),
                                                                        type : "Nombre"
                                                                    })
                                                                } else if(currElem.toLowerCase().match(/\(text\)/g)) {
                                                                    params.value[lastKUnik].columns.push({
                                                                        label : currElem.replace(/\[columns\]|-\[columns\]|\(text\)|\(Text\)|\s\(text\)|\s\(Text\)/g, '').trim(),
                                                                        type : "Text"
                                                                    })
                                                                }else {
                                                                    params.value[lastKUnik].columns.push({
                                                                        label : currElem.replace(/\[columns\]|-\[columns\]|\(text\)|\(Text\)|\s\(text\)|\s\(Text\)/g, '').trim(),
                                                                        type : "Text"
                                                                    })
                                                                }
                                                            } else if(currentMatch == "rows") {
                                                                params.value[lastKUnik].rows.push({
                                                                    label : currElem.replace(/\[rows\]|-\[rows\]|-\s\[rows\]/g, '').trim()
                                                                })
                                                            }
                                                        }
                                                    } break;
                                                    default: {
                                                        params.value[lastKUnik].list.push(element.replace(/-\s{1,}|-\s{2,}|\s{2,}|\r/g, '').trim());
                                                    } break;
                                                }
                                            } else {
                                                let inputTypeSplit;
                                                if(element.match(/\[.+\]/g)) {
                                                    if(element.match(/\[.+\]/g) != '[step]'){
                                                        let filtered = inputsTypeAsArray.filter(([key, values]) => {
                                                            if(count < 1 && key.toLowerCase().includes(element.match(/\[.+\]/g)[0].toLowerCase().replace(/\[|\]/g, ''))){
                                                                count++; return true} else {return false}
                                                            }, count = 0);
                                                        newForm.value.inputs[path] = {
                                                            label : element.replace(/\[.+\]|-\[.+\]|-\s\[.+\]|-\s|\[|\]|\]\s|:|\r|\s*`.+`|`.+`/g, '').trim(),
                                                            placeholder : element.match(/\s*`.+`|`.+`/g) ? element.match(/\s*`.+`|`.+`/g)[0].replace(/\s*``.+``|``.+``|\s*`|`/g, '') : '',
                                                            info: element.match(/\s*``.+``|``.+``/g) ? element.match(/\s*``.+``|``.+``/g)[0].replace(/\s*`|`/g, '') : '',
                                                            type : filtered.length > 0 ? filtered[0][0] : 'text'
                                                        };
                                                        inputType = newForm.value.inputs[path].type;
                                                        inputTypeSplit = newForm.value.inputs[path].type.split('.');
                                                        lastKUnik = inputTypeSplit[inputTypeSplit.length - 1]+path;
                                                    }
                                                } else {
                                                    lastKUnik = 'text'+path;
                                                    newForm.value.inputs[path] = {
                                                        label : element.replace(/\[.+\]|-\[.+\]|-\s\[.+\]|-\s|\[|\]|\]\s|:|\r/g, '').trim(),
                                                        placeholder : element.match(/\s*`.+`|`.+`/g) ? element.match(/\s*`.+`|`.+`/g)[0].replace(/\s*``.+``|``.+``|\s*`|`/g, '') : '',
                                                        info: element.match(/\s*``.+``|``.+``/g) ? element.match(/\s*``.+``|``.+``/g)[0].replace(/\s*`|`/g, '') : '',
                                                        type : 'text'
                                                    }
                                                }
                                                if(textArray[iterIndex + 1]) {
                                                    if(textArray[iterIndex + 1].match(/^(\s{2,})|((?<=\n(\s)+))(\s{2,})/g)) {
                                                        switch (inputType) {
                                                            case 'tpls.forms.checkbox': {
                                                                params.value[lastKUnik] = {
                                                                    list : []
                                                                }
                                                            } break;
                                                            case 'tpls.forms.select': {
                                                                lastKUnik = path;
                                                                params.value[lastKUnik] = {
                                                                    options : []
                                                                }
                                                            } break;
                                                            case 'tpls.forms.cplx.evaluation': {
                                                                params.value[lastKUnik] = {
                                                                    objet : "A modifier dans la paramètrage",
                                                                    objectif : "A modifier dans la paramètrage",
                                                                    quadrants : {},
                                                                    deleteOthersProposition : false,
                                                                    isCollectif : false,
                                                                    enableSteps : true,
                                                                    transformationQuadrants : {}
                                                                }
                                                            } break;
                                                            case 'tpls.forms.cplx.multiCheckboxPlus': {
                                                                params.value[lastKUnik] = {
                                                                    global : {
                                                                        list : [],
                                                                        dependOn : "",
                                                                        width : ""
                                                                    },
                                                                    tofill : {},
                                                                    optinfo : {},
                                                                    optimage : {},
                                                                    placeholdersckb : {}
                                                                }
                                                            } break;
                                                            case 'tpls.forms.cplx.multiRadio': {
                                                                params.value[lastKUnik] = {
                                                                    global : {
                                                                        list : [],
                                                                        dependOn : "",
                                                                        width : ""
                                                                    },
                                                                    tofill : {},
                                                                    optinfo : {},
                                                                    placeholdersradio : {}
                                                                }
                                                            } break;
                                                            case 'tpls.forms.cplx.simpleTable' : {
                                                                params.value[lastKUnik] = {
                                                                    tableName : 'Titre',
                                                                    columns : [],
                                                                    rows : [],
                                                                    activeNewLine : false,
                                                                    singleAnswerByLine : true
                                                                }
                                                            } break;
                                                            default: {
                                                                params.value[lastKUnik] = {
                                                                    list : []
                                                                }
                                                            } break;
                                                        }
                                                    }
                                                }
                                                forms[formNames[formNames.length -1]+'_'+formNames.length] = newForm;
                                            }
                                        }
                                    });
                                    let allParamsLast;
                                    if(parentFormIdForNewStep) {
                                        allParamsLast = coForm_FormWizardParams.parentForm?.params ? JSON.parse(JSON.stringify(coForm_FormWizardParams.parentForm?.params)) : {}
                                    }
                                    if(formData.textvalue.indexOf('[step]') > -1){
                                        formNames.forEach((formItem, formIndex) => {
                                            if(forms[formItem+'_'+(formIndex+1)]) {
                                                if(formItem != '' && typeof newForm.value.name != "undefined") forms[formItem+'_'+(formIndex+1)].value.name = formItem.trim();
                                                let currentNewFormId;
                                                if(coForm_FormWizardParams.isAap && formData.newSubFormTitle != "undefined" && formData.newSubFormTitle != '') {
                                                    currentNewFormId = forms[formItem+'_'+(formIndex+1)]?.value?.step;
                                                    configFormParams.value?.[currentNewFormId]?.name ? configFormParams.value[currentNewFormId].name = formItem.trim() : ""
                                                }
                                                dataHelper.path2Value(forms[formItem+'_'+(formIndex+1)], function(subFormInserted) {
                                                    if(dyFObj.path2Value.result) {
                                                        if(dyFObj.path2Value.result.saved) {
                                                            let tplCtxToSend = {
                                                                collection : 'forms',
                                                                id : parentFormIdForNewStep,
                                                                path : 'subForms',
                                                                value : tplCtx.value
                                                            }
                                                            dataHelper.path2Value(tplCtxToSend, function() {

                                                            })
                                                            if(coForm_FormWizardParams.isAap) {
                                                                configFormParams.value?.[currentNewFormId] ? configFormParams.value[currentNewFormId].inputs = subFormInserted?.saved?.["_id"]?.["$id"] : ""
                                                            }
                                                        }

                                                    }
                                                });
                                            } else {
                                                toastr.error(trad.somethingwrong)
                                            }
                                            if(formNames.length - 1 == formIndex) {
                                                dataHelper.path2Value(configFormParams, function() {})
                                                if(Object.keys(params.value).length > 0) {
                                                    allParamsLast.params ? params.value = {...allParamsLast.params, ...params.value} : '';
                                                    dataHelper.path2Value(params, function() {
                                                        urlCtrl.loadByHash(location.hash);
                                                        dyFObj.closeForm()
                                                    })
                                                } else {
                                                    urlCtrl.loadByHash(location.hash);
                                                    dyFObj.closeForm();
                                                }
                                            }
                                        })
                                    } else {
                                        if(formData.toNewStep == "oui") {
                                            if(formData.newSubFormTitle != "undefined" && formData.newSubFormTitle != '' && typeof newForm.value.name != "undefined") newForm.value.name =formData.newSubFormTitle;
                                            if(coForm_FormWizardParams.isAap && formData.newSubFormTitle != "undefined" && formData.newSubFormTitle != '') {
                                                configFormParams.value?.[newFormId]?.name ? configFormParams.value[newFormId].name = formData.newSubFormTitle : ""
                                            }
                                            dataHelper.path2Value(newForm, function(inputInserted) {
                                                if(coForm_FormWizardParams.isAap && inputInserted.result) {
                                                    mylog.log("configformparams", configFormParams.value?.[newFormId], inputInserted?.saved?.["_id"]?.["$id"])
                                                    configFormParams.value?.[newFormId] ? configFormParams.value[newFormId].inputs = inputInserted?.saved?.["_id"]?.["$id"] : ""
                                                }
                                                    if(dyFObj.path2Value.result) {
                                                        if(dyFObj.path2Value.result.saved) {
                                                            let tplCtxToSend = {
                                                                collection : 'forms',
                                                                id : parentFormIdForNewStep,
                                                                path : 'subForms',
                                                                value : tplCtx.value
                                                            }
                                                            dataHelper.path2Value(tplCtxToSend, function() {
                                                                dataHelper.path2Value(configFormParams, function() {})
                                                                if(Object.keys(params.value).length > 0) {
                                                                    allParamsLast.params ? params.value = {...allParamsLast.params, ...params.value} : '';
                                                                    dataHelper.path2Value(params, function() {
                                                                        // reloadWizard();
                                                                        urlCtrl.loadByHash(location.hash);
                                                                        dyFObj.closeForm()
                                                                    })
                                                                } else {
                                                                    urlCtrl.loadByHash(location.hash);
                                                                    dyFObj.closeForm();
                                                                }
                                                            })
                                                        }

                                                    }
                                                })
                                        } else {
                                            if(lastSubformId == 'null') {
                                                dataHelper.path2Value(newForm, function(inputInserted) {
                                                    if(coForm_FormWizardParams.isAap && inputInserted.result) {
                                                        configFormParams.value?.[newFormId] ? configFormParams.value[newFormId].inputs = inputInserted?.saved?.["_id"]?.["$id"] : ""
                                                    }
                                                    if(dyFObj.path2Value.result) {
                                                        if(dyFObj.path2Value.result.saved) {
                                                            let tplCtxToSend = {
                                                                collection : 'forms',
                                                                id : parentFormIdForNewStep,
                                                                path : 'subForms',
                                                                value : tplCtx.value
                                                            }
                                                            dataHelper.path2Value(configFormParams, function() {})
                                                            dataHelper.path2Value(tplCtxToSend, function() {
                                                                if(Object.keys(params.value).length > 0) {
                                                                    dataHelper.path2Value(params, function() {
                                                                        // reloadWizard();
                                                                        urlCtrl.loadByHash(location.hash);
                                                                        dyFObj.closeForm()
                                                                    })
                                                                }  else {
                                                                    urlCtrl.loadByHash(location.hash);
                                                                    dyFObj.closeForm();
                                                                }
                                                            })
                                                        }

                                                    }
                                                })
                                            } else {
                                                let allInputsLast;
                                                let existSubFormId;
                                                if(formData.existStep != 'undefined' && formData.existStep != '') {
                                                    allInputsLast = formsInputs[formData.existStep] ? formsInputs[formData.existStep] : null;
                                                    existSubFormId = formData.existStep
                                                } else {
                                                    allInputsLast = formsInputs[lastSubformId["_id"]["$id"]] ? formsInputs[lastSubformId["_id"]["$id"]] : null;
                                                }
                                                let tplCtxToSend = {
                                                    collection: coForm_FormWizardParams.isAap ? "inputs" : "forms",
                                                    formParentId: parentFormIdForNewStep,
                                                    id: existSubFormId ? existSubFormId : lastSubformId['_id']['$id'],
                                                    key: "",
                                                    path: "inputs",
                                                    updatePartial: true,
                                                    value: newForm.value.inputs
                                                }
                                                dataHelper.path2Value(tplCtxToSend, function() {
                                                    if(newForm.value?.step && params.value[newForm.value?.step]) {
                                                        delete params.value[newForm.value?.step]
                                                    }
                                                    if(Object.keys(params.value).length > 0) {
                                                        allParamsLast.params ? params.value = {...allParamsLast.params, ...params.value} : '';
                                                        dataHelper.path2Value(params, function() {
                                                            urlCtrl.loadByHash(location.hash);
                                                            dyFObj.closeForm()
                                                        })
                                                    } else {
                                                        urlCtrl.loadByHash(location.hash);
                                                        dyFObj.closeForm();
                                                    }
                                                })
                                            }
                                        }
                                    }
                                } else {
                                    toastr.info('<h6 style="text-transform: none !important">Ajouter au moins une ligne dans le textarea</h6>');
                                    $("#btn-submit-form").removeAttr("disabled");
                                    $("#btn-submit-form").html(trad.Validate+`<i class="fa fa-arrow-circle-right"></i>`)
                                }
                            } else {
                                toastr.warning('<h6 style="text-transform: none !important">Le textarea ne doit pas être vide</h6>');
                                $("#btn-submit-form").removeAttr("disabled");
                                $("#btn-submit-form").html(trad.Validate+`<i class="fa fa-arrow-circle-right"></i>`)
                            }
                        }
                    }
                };
                dyFObj.openForm( createFromParams, null, null )
            });

            $('.copystandalonelink').off().on().click(function(){
                var sampleTextarea = document.createElement("textarea");
                document.body.appendChild(sampleTextarea);
                if(typeof costum != "undefined" && costum != null)
                    sampleTextarea.value = baseUrl+ "/costum/co/index/slug/" + costum.slug + "#answer.answer.id.new.form." + aapObj.form["_id"]["$id"] + ".mode.w.standalone.true.ask.true";
                else
                    sampleTextarea.value = baseUrl+"#answer.index.id.new.form."+answerObj.form+".mode.w.standalone.true";
                sampleTextarea.select(); //select textarea contenrs
                document.execCommand("copy");
                document.body.removeChild(sampleTextarea);
                toastr.success('lien standalone copié');

            });

            $(window).on('scroll', function(e) {
                if(typeof syncSection != "undefined") syncSection();
                var $sticky = $('.drschu-sticky'),
                $stickyTo = $('.sticky-to'),
                stickyToTop = $stickyTo && typeof $stickyTo.offset() != 'undefined' ? $stickyTo.offset().top : 0,
                stickyToBottom = stickyToTop && $stickyTo ? stickyToTop + $stickyTo.outerHeight() : 0;
                var scroll = $(window).scrollTop(),
                    stickyTop = $sticky && typeof $sticky.offset() != "undefined" ? $sticky.offset().top : 0,
                    stickyBottom = $sticky && typeof $sticky.offset() != "undefined" ? $sticky.offset().top + $sticky.outerHeight() : 0;

                if (stickyBottom >= stickyToBottom) {
                    if (scroll < stickyTop) {
                    $sticky.addClass('pos-fixed').removeClass('pos-abs');
                    } else {
                    $sticky.addClass('pos-abs');
                    }
                } else if (scroll > stickyToTop) {
                    $sticky.addClass('pos-fixed');
                } else if (scroll < stickyToTop) {
                    $sticky.removeClass('pos-abs').removeClass('pos-fixed');
                }
            });
        }

        if(typeof syncSection != "undefined") {
            window.addEventListener('scroll', syncSection);
            window.addEventListener('resize', syncSection);
        }

        $(".saveOneByOne").unbind().blur( function( event ) {
            var tthis = $(this);
            event.preventDefault();
            const myKey = $(this).attr('id');
            var parentElem = $('div#question_'+myKey);
            var errorElem = parentElem.find(`div.rules-container${myKey}`);
            const rulesAttr = parentElem.find(`div.rules-container${myKey}`).attr('data-rules');
            var rules = {};
            var strError = '';
            var hasError = false;
            if(typeof rulesAttr != 'undefined') {
                rules = JSON.parse(rulesAttr.replace(/'/g, '"'));
            }
            if(notEmpty(rules) && rules.width) {
                if(rules.width.min && rules.width.min > 0 && tthis.val().trim().length < rules.width.min) {
                    strError = `<small class="text-danger">${rules.width.min} ${trad.minCharacters}</small> </br>`;
                    hasError = true;
                }
                if(rules.width.min && rules.width.max && rules.width.max > 0 && tthis.val().trim().length > rules.width.max) {
                    strError = `<small class="text-danger">${rules.width.max} ${trad.maxCharacters}</small> </br>`;
                    hasError = true;
                }
            }
            if(notEmpty(rules) && rules.syntaxe) {
                if(rules.syntaxe && rules.syntaxe.errormsg && rules.syntaxe.errormsg != "" && rules.syntaxe.regexp && rules.syntaxe.regexp != "") {
                    matchRegex = new RegExp(rules.syntaxe.regexp);
                    if(tthis.val().trim().length > 0 && !tthis.val().trim().match(matchRegex)) {
                        strError += `<small class="text-danger">${ trad[rules.syntaxe.errormsg] ? trad[rules.syntaxe.errormsg] : rules.syntaxe.errormsg}</small>`;
                        hasError = true;
                    }
                }
            }
            if(hasError) {
                tthis.css('border-color', '#a94442')
            } else {
                tthis.css('border-color', '#ddd')
            }
            errorElem.empty();
            errorElem.append(strError);

            var answer = {
                collection : "answers",
                id : coForm_FormWizardParams.answer._id.$id,
                path : "answers."+$(this).attr("form")+"."+$(this).attr("id")
            };

            if(answerObj.form)
                answer.path = "answers."+$(this).data("form")+"."+$(this).attr("id");

            if($(this).attr("type") == "checkbox")
                answer.value = $(this).is(":checked");
            else
                answer.value = $(this).val().trim();

            mylog.log("saveOneByOne",$(this).attr("id"), answer );

            if(!hasError) {
                dataHelper.path2Value( answer , function(params) {
                    if(tthis.prop("tagName") == "TEXTAREA" && tthis.css("display") == "none"){
                        tthis.parent().find(".editor-statusbar").prepend('&nbsp;<b class="text-success alert-saved"><i class="fa fa-check"></i><span class="cursor">'+trad.saved+'</span></b>');
                        setTimeout(function() { tthis.parent().find(".alert-saved").remove(); }, 1500);
                    }else{
                        //toastr.success('Mise à jour enregistrée');
                    }
                    saveLinks(answerObj._id.$id,"updated",userId);
                    newReloadStepValidationInputGlobal({
                        inputKey : tthis.attr("id")
                    })
                } );
            }
        });
        $('.deleteLine').off().click( function(){
            formId = $(this).data("id");
            key = $(this).data("key");
            pathLine = $(this).data("path");
            collection = $(this).data("collection");
            const inputKey = $(this).data("inputkey");
            if (typeof $(this).data("parentid") != "undefined") {
                var parentfId = $(this).data("parentid");
            }
            bootbox.dialog({
                title: trad.confirmdelete,
                message: "<span class='text-red bold'><i class='fa fa-warning'></i> "+trad.actionirreversible+"</span>",
                buttons: [
                    {
                        label: "Ok",
                        className: "btn btn-primary pull-left",
                        callback: function() {
                            var formQ = {
                                value:null,
                                collection : collection,
                                id : formId,
                                path : pathLine
                            };

                            if (typeof parentfId != "undefined") {
                                formQ["formParentId"] = parentfId;
                            }

                            dataHelper.path2Value( formQ , function(params) {
                                $("#"+key).remove();
                                //location.reload();
                                if(coForm_FormWizardParams.steps?.[formId]?.inputs?.[inputKey] != undefined) {
                                    delete coForm_FormWizardParams.steps[formId].inputs[inputKey]
                                }
                                if(typeof provideContent != 'undefined') {
                                    // provideContent(formId)
                                }
                            } );
                        }
                    },
                    {
                        label: "Annuler",
                        className: "btn btn-default pull-left",
                        callback: function() {}
                    }
                ]
            });
        });
        $('.tooltips').tooltip();
        if(!coForm_FormWizardParams.isStandalone && location.hash.indexOf("answerId.new")>=0){
            answerNotification(coForm_FormWizardParams);
            //history.replaceState(location.hash, "", "answerId."+answerId);
            history.replaceState(location.hash, "",location.hash.replace("answerId.new", "answerId."+answerId));
        }

        if(typeof inputObliList != "undefined" && Object.keys(inputObliList).length > 0){
            if(coForm_FormWizardParams?.step?.step) {
                $("#"+coForm_FormWizardParams.step.step+" :input:not(.exclude-input),#"+coForm_FormWizardParams.step.step+" select,#"+coForm_FormWizardParams.step.step+" button").change(function(e) {
                    e.stopImmediatePropagation();
                    updateWizardBtn(coForm_FormWizardParams , null , null);
                    if(typeof newReloadStepValidationInputGlobal != "undefined") {
                        newReloadStepValidationInputGlobal(null)
                    }
                });
            }

            $('a[data-dom-uploader]').each(function(){
                $("#"+$(this).data('dom-uploader')).on("allComplete", function(event, succeeded, failed) {
                    updateWizardBtn(coForm_FormWizardParams , null , null);
                    event.stopImmediatePropagation();
                });
            });

            $('div[data-inputtype="address"]').each(function(){

                if (typeof window[$(this).data("mapobj")] !== "undefined" && typeof window[$(this).data("mapobj")].map !== "undefined" && typeof window[$(this).data("mapobj")].map.map !== "undefined") {
                    window[$(this).data("mapobj")].map.map.on("viewreset" ,  function (e) {

                        $('div[data-inputtype="address"]').each(function(){

                            $(this).mouseleave(function(event){
                                updateWizardBtn(coForm_FormWizardParams , null , null);
                                $(this).off('mouseleave');
                                event.stopImmediatePropagation();
                            })

                        });

                    });
                }

            });
        }

        $('.sw-btn-next').off().click( function(e){
            e.stopImmediatePropagation();
            var actualStep = $('#smartwizard').smartWizard("getStepInfo");
            var stepOptions = $('#smartwizard').smartWizard("getOptions");
            if(!stepOptions.disabledSteps.includes(actualStep.currentStep + 1)) {
                $('#smartwizard').smartWizard("next");
            }else{
                ajaxPost(null, baseUrl+'/survey/answer/updatewizard/answer/'+coForm_FormWizardParams["answer"]["_id"]["$id"]+'/form/'+coForm_FormWizardParams["parentForm"]["_id"]["$id"]+'/step/'+actualStep.currentStep,
                    null,
                    function(data) {
                        if(data.value == true) {
                            updateWizardAccess(coForm_FormWizardParams , "#smartwizard" , stepOptions.disabledSteps , stepOptions.hiddenSteps , function () {
                                $('#smartwizard').smartWizard("next");
                            })
                        }
                    }
                );
            }
        });
    });
</script>
<?php
// if(!$isajax || (isset($mode) && $mode !== 'w' && $mode !== 'r' && $mode !== 'pdf')){
//     HtmlHelper::registerCssAndScriptsFiles(array(
//         //DnD
//         '/js/inputDnDAap.js',
//     ), Yii::app()->getModule(Survey::MODULE)->getAssetsUrl());
// }
?>
