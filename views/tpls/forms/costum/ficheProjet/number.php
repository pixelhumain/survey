<style >
	input:focus {
  border-bottom: 2px solid black;
}
</style>
<?php 
$value = (!empty($answers)) ? " value='".$answers."' " : "";

$inpClass = "form-control";


if($saveOneByOne)
	$inpClass .= " saveOneByOne";

if($mode == "r" || $mode == "pdf"){ ?>
    <div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
        <label for="<?php echo $kunik ?>">
        	<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
        		<?php echo $label ?>        			
        		</h4>
        </label>
        <br/>
        <?php echo $answers; ?>
    </div>
<?php 
}else{
?>
	<div class="form-group">
	    <label for="<?php echo $key ?>">
	    	<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
	    		<?php echo $label.$editQuestionBtn ?>	    		
	    	</h4>
	    </label>
	    <br/>
	    <input 
	    style="border: none; border-bottom: 2px solid #ddd"
		    type="<?php echo "number" ?>" 
		    class="<?php echo $inpClass ?>" id="<?php echo $key ?>" 
		    aria-describedby="<?php echo $key ?>Help" 
		    placeholder="<?php echo (isset($placeholder)) ? $placeholder : '' ?>" 
		    data-form='<?php echo $form["id"] ?>'  
		    <?php echo $value ?> 
	    >
	    <?php if(!empty($info)){ ?>
	    	<small id="<?php echo $key ?>Help" class="form-text text-muted">
	    		<?php echo $info ?>	    			
	    	</small>
	    <?php } ?>
	</div>
<?php } ?>


<?php if($mode != "pdf"){ ?>
<script type="text/javascript">
	jQuery(document).ready(function() {
	    mylog.log("render form input","/modules/costum/views/tpls/forms/text.php");
	});
</script>
<?php } ?>