<?php if($answer){
	//var_dump($mode); exit;
$debug = false;
$editBtnL = ( $canEdit === true && ( $mode == "w" || $mode == "fa") ) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";

$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

$paramsData = [ 
	"group" => [ 
		"Feature"=>"Feature", 
		"Costum"=>"Costum", 
		"Chef de Projet"=>"Chef de Projet", 
		"Data"=>"Data", 
		"Maintenance"=>"Maintenance" 
	],
	"nature" => [
		"investissement" => "Investissement",
		"fonctionnement" => "Fonctionnement"
	],
	"amounts" => [
		"price" => "Montant"
	],
	"estimate" => false 
];

function is_true($val, $return_null=false){
    $boolval = ( is_string($val) ? filter_var($val, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) : (bool) $val );
    return ( $boolval===null && !$return_null ? false : $boolval );
}

if( isset($parentForm["params"][$kunik]["group"]) ) 
	$paramsData["group"] =  $parentForm["params"][$kunik]["group"];
if( isset($parentForm["params"][$kunik]["nature"]) ) 
	$paramsData["nature"] =  $parentForm["params"][$kunik]["nature"];
if( isset($parentForm["params"][$kunik]["amounts"]) ) 
	$paramsData["amounts"] =  $parentForm["params"][$kunik]["amounts"];
if( isset($parentForm["params"][$kunik]["estimate"]) ) 
	$paramsData["estimate"] =  is_true($parentForm["params"][$kunik]["estimate"]);

// if(isset($answers)){
// 	foreach ($answers as $q => $a) {
// 		if(isset($a["group"]))
// 			$paramsData["group"][] = $a["group"];
// 	}
// }


$properties = [
		"group" => [
            "placeholder" => "Groupé",
            "inputType" => "select",
            "options" => $paramsData["group"],
            "rules" => [ "required" => true ]
        ],
        "nature" => [
            "placeholder" => "Nature de l’action",
            "inputType" => "select",
            "options" => $paramsData["nature"],
            "rules" => [ "required" => true ]
        ],
        "poste" => [
            "inputType" => "text",
            "label" => "Poste de dépense",
            "placeholder" => "Poste de dépense",
            "rules" => [ "required" => true  ]
        ]
    ];
    foreach ($paramsData["amounts"] as $k => $l) {
    	$properties[$k] = [ "inputType" => "text",
				            "label" => $l,
				            "propType" =>"amount",
				            "placeholder" => $l,
				            //"rules" => [ "required" => true, "number" => true ]
				        ];
    }
    if($debug)var_dump($answers);
    if($debug)var_dump($paramsData);
?>	

<?php

echo $this->renderPartial("survey.views.tpls.forms.costum.ctenat.budgetTable",
	                      [ 
	                        "form" => $form,
	                        "wizard" => true, 
	                        "answers"=>$answers,
	                        "answer"=>$answer,
                            "mode" => $mode,
                            "kunik" => $kunik,
                            "answerPath"=>$answerPath,
                            "key" => $key,
                            "titleColor" => $titleColor,
                            "properties" => $properties,
                            "canAdminAnswer"  => $canAdminAnswer,
                            "label" => $label,
                            "editQuestionBtn" => $editQuestionBtn,
                            "editParamsBtn" => $editParamsBtn,
                            "editBtnL" => $editBtnL,
                            "info" => $info,
	                        //"showForm" => $showForm,
	                        "paramsData" => $paramsData,
	                        "canEdit" => $canEdit,  
	                        //"el" => $el 
	                    ] ,true );

?>

<?php if( $paramsData["estimate"] ) {  ?>
<div class="form-estimate" style="display:none;">
  	Proposition de prix : <br/>
	<input type="text" id="priceEstimate" name="priceEstimate" style="width:100%;">
	Durée : <br/>
	<input type="text" id="daysEstimate" name="daysEstimate" style="width:100%;">
</div>
<?php } 


if($mode != "r" && $mode != "pdf"){
?>

<script type="text/javascript">

var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

$(document).ready(function() { 
	
	sectionDyf.<?php echo $kunik ?> = {
		jsonSchema : {	
	        title : "Budget prévisionnel",
            icon : "fa-money",
            text : "Décrire ici les principaux postes de dépenses : à quoi correspondent les coûts ? <br/>Préciser ce qui relève des dépenses de fonctionnement et des dépenses d’investissement.<br/>Les coûts doivent être en <b>hors taxe</b>.",
	        properties : <?php echo json_encode( $properties ); ?>,
	        save : function () {  
	        	var today = new Date();
	            tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
	            $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });
	            
	            var connectedData = ["financer","todo","payed","progress","worker","validFinal","votes",];
	            $.each( connectedData , function(k,attr) { 
	        		if(notNull("answerObj."+tplCtx.path+"."+attr))
	            		tplCtx.value[attr] = jsonHelper.getValueByPath(answerObj,tplCtx.path+"."+attr);
	        	 });

	            mylog.log("save tplCtx",tplCtx);
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    urlCtrl.loadByHash(location.hash);
	                } );
	            }
	    	}
	    }
	};

	sectionDyf.<?php echo $kunik ?>Params = {
		jsonSchema : {	
	        title : "<?php echo $kunik ?> config",
	        description : "Liste de question possible",
	        icon : "fa-cog",
	        properties : {
	            group : {
	                inputType : "array",
	                label : "Liste des groups",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.group
	            },
	            nature : {
	                inputType : "properties",
	                labelKey : "Clef",
	                labelValue : "Label affiché",
	                label : "Liste des natures possibles",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.nature
	            },
	            amounts : {
	                inputType : "properties",
	                labelKey : "Clef",
	                labelValue : "Label affiché",
	                label : "Liste des prix(ex:par année)",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.amounts
	            },
	            estimate : {
	                inputType : "checkboxSimple",
                    label : "estimate Prices",
                    params : {
                        onText : "Oui",
                        offText : "Non",
                        onLabel : "Oui",
                        offLabel : "Non",
                        labelText : "estimate Prices"
                    },
                    checked : sectionDyf.<?php echo $kunik ?>ParamsData.estimate
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	        		if(val.inputType == "properties")
	        		 	tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
	        		else if(val.inputType == "array")
	        		 	tplCtx.value[k] = getArray('.'+k+val.inputType);
	        		else
	        		 	tplCtx.value[k] = $("#"+k).val();
	        		 mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                   urlCtrl.loadByHash(location.hash);
	                } );
	            }

	    	}
	    }
	};


    mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

    //adds a line into answer
    $( ".add<?php echo $kunik ?>" ).off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
    });

    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
    });

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });


<?php if( $paramsData["estimate"] ) {  ?>
	
	$('.btnEstimateSelected').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.key = $(this).data("key");
		tplCtx.form = $(this).data("form");
		tplCtx.price = $(this).data("price");
		$(this).removeClass('btn-default').addClass("btn-success");

		tplCtx.pathBase = "answers";
    	if( notNull(formInputs [tplCtx.form]) )
    		tplCtx.pathBase = "answers."+tplCtx.form;    

    	tplCtx.path = tplCtx.pathBase+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+$(this).data("uid")+".selected"; 	
        tplCtx.value = true;

		mylog.log("btnEstimateSelected save",tplCtx);
  	 	dataHelper.path2Value( tplCtx, function(){
  	 		tplCtx.path = tplCtx.pathBase+"."+tplCtx.key+"."+tplCtx.pos+".price"; 	
	        tplCtx.value = tplCtx.price;

			mylog.log("btnEstimateSelected save",tplCtx);
	  	 	dataHelper.path2Value( tplCtx, function(){
	  	 		$("#price"+tplCtx.pos).html( tplCtx.price+"€" );
	  	 		saveLinks(answerObj._id.$id,"intentValidated",userId);
	  	 	 } );
  	 	} );

  	 	

		
	});

    $('.btnEstimate').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.key = $(this).data("key");
		tplCtx.form = $(this).data("form");
		prioModal = bootbox.dialog({
	        message: $(".form-estimate").html(),
	        title: "Voter pour partie",
	        show: false,
	        buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {

                    	tplCtx.path = "answers";
			        	if( notNull(formInputs [tplCtx.form]) )
			        		tplCtx.path = "answers."+tplCtx.form;    

			        	tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+userId; 

			        	var today = new Date();
						today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();	
			            tplCtx.value = {
			            	price : $(".bootbox #priceEstimate").val(),
			            	days : $(".bootbox #daysEstimate").val(),
			            	name :  userConnected.name,
			            	date : today
			            };

				    	mylog.log("btnEstimate save",tplCtx);
				  	 	dataHelper.path2Value( tplCtx, function(){
				  	 		saveLinks(answerObj._id.$id,"estimated",userId);
				  	 		closePrioModalRel();
				  	 	} );
			        }		
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: closePrioModal
                }
              },
		        onEscape: closePrioModal
	    });
	    prioModal.modal("show");
	});
<?php } ?>


});
function closePrioModal(){
	prioModal.modal('hide');
}
function closePrioModalRel	(){
	closePrioModal();
	urlCtrl.loadByHash(location.hash);
}
</script>
<?php } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} 


}

?>
