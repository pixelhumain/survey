<?php
$cssJS = array(
	'/plugins/gantt/js/jquery.fn.gantt.js',
	'/plugins/gantt/js/moment.min.js',
	'/plugins/gantt/css/style.css',
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);
?>
<style type="text/css">

	@import url(http://taitems.github.com/UX-Lab/core/js/prettify.js);
	@import url(http://taitems.github.com/UX-Lab/core/css/prettify.css);
	@import url(http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css);
	body {
		font-family: Helvetica, Arial, sans-serif;
		font-size: 13px;
		padding: 0 0 50px 0;
	}
	.contain {
		width: 800px;
		margin: 0 auto;
	}
	h1 {
		margin: 40px 0 20px 0;
	}
	h2 {
		font-size: 1.5em;
		padding-bottom: 3px;
		border-bottom: 1px solid #DDD;
		margin-top: 50px;
		margin-bottom: 25px;
	}
	table th:first-child {
		width: 150px;
	}
</style>
<?php 
if($mode == "r" || $mode == "pdf"){ ?>
	<h4>
		<?php echo $label ?>        			
	</h4>
	<div class="gantt">
	</div>
	<?php 
}else{ ?>

	<h4>
		<?php echo $label.$editQuestionBtn ?>        			
	</h4>
	<div class="gantt">
	</div>
<?php }?>
	<script>

		$(function() {

			"use strict";
			var today = moment();
			var andTwoHours = moment().add("hours",2);
			var today_friendly = "/Date(" + today.valueOf() + ")/";
			var next_friendly = "/Date(" + andTwoHours.valueOf() + ")/";

			$(".gantt").gantt({
				source: [{
					name: "Testing",
					desc: " ",
					values: [{
						from: "02-03-2020",
						to: "02-02-2020",
						label: "Test", 
						customClass: "ganttRed"
					}]
				},
				{
					name: "Testing",
					desc: " ",
					values: [{
						from: today,
						to: today,
						label: "Test", 
						customClass: "ganttRed"
					}]
				},
				],
				scale: "weeks",
				navigate: "scroll"
			});

			$(".gantt").popover({
				selector: ".bar",
				title: "I'm a popover",
				content: "And I'm the content of said popover.",
				trigger: "hover"
			});

		});

	</script>
	</html>