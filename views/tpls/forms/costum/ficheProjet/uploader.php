<?php //var_dump($form["inputs"][$key]);var_dump($key);var_dump($form["id"]);var_dump(@$answer);var_dump(@$form); 
$domUploader=$form["id"]."_".$key."_uploader";
$inputsObj=$form["inputs"][$key];
$uploaderObj=$form["inputs"][$key]["uploader"];
$uploaderObj["contextId"]=(string)$answer["_id"];
$uploaderObj["dom"]=$domUploader;
$uploaderObj["contextType"]=Form::ANSWER_COLLECTION;
$uploaderObj["formats"]=(isset($uploaderObj["formats"])) ? $uploaderObj["formats"] : ['jpeg', "pdf", 'jpg', 'gif', 'png'];
$uploaderObj["params"]=array("subKey"=>$form["id"].".".$key);
if(isset($uploaderObj["cryptage"]))
	$uploaderObj["params"]["cryptage"]=true;
if(isset($uploaderObj["restricted"]))
	$uploaderObj["params"]["restricted"]=true;
$initAnswerFiles=Document::getListDocumentsWhere(array(
		  			"id"=>$uploaderObj["contextId"], 
		  			"type"=>$uploaderObj["contextType"],
		  			"subKey"=>$uploaderObj["params"]["subKey"]), "file");
?>

<style type="text/css">
	.uploader_container{
		white-space: normal;
	}
</style>
<div class="uploader_container col-xs-12 no-padding margin-bottom-20">
	<br/>
	<label for="<?php echo $kunik ?>">
		<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn ?>
		</h4>
	</label><!--<h4><?php echo @$inputsObj["label"] ?></h4>-->
	<?php if(isset($inputsObj["info"])){ ?> 
		<span class="italic">
			<?php echo $inputsObj["info"] ?>
		</span>
	<?php } 
	if($mode=="r" || $mode=="pdf"){ 
        echo $this->renderPartial("co2.views.pod.docsList",array("edit"=>false, "documents"=>$initAnswerFiles,"docType"=>@$uploaderObj["docType"]) ); 
	} else{ 
		if(!isset($uploaderObj["paste"]) || $uploaderObj["paste"]){ ?> 
			<input type="text" name="upload" id="<?php echo $domUploader ?>_paste" placeholder="coller une image de votre navigateur ici" class="form-control text-center qq-paste-input"/>
		<?php } ?>
		<div class="col-xs-12 fine-uploader-manual-trigger no-padding"  id="<?php echo $domUploader; ?>" data-type="" data-id=""></div>
	    	<?php if(isset($uploaderObj["docType"]) && $uploaderObj["docType"]=="image"){ ?>
				<script type="text/template" id="qq-template-gallery<?php echo $domUploader ?>">
			<?php }else{ ?>
				<script type="text/template" id="qq-template-manual-trigger<?php echo $domUploader ?>">
			<?php  }  ?>

			<div class="qq-uploader-selector qq-uploader qq-upload-drop-area-active<?php if(isset($fieldObj["docType"]) && $fieldObj["docType"]=="image"){ echo " qq-gallery"; } ?>" qq-drop-area-text="drop files" style="<?php if(!isset($uploaderObj["paste"]) || $uploaderObj["paste"]){ ?>border-radius:0px 0px 10px 10px;<?php } ?>">
				<div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
					<div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
				</div>
				<!--<div class="qq-paste-element-triger"><input type="text" value="" placeholder="paste a link"/></div>qq-hide-dropzone-->
				<div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
					<span class="qq-upload-drop-area-text-selector"></span>
				</div>
				<div class="col-xs-12 margin-bottom-10 text-center">
					<div class="qq-upload-button-selector btn btn-primary">
						<div>Selectionner un document</div>
					</div>	
				</div>
				<button type="button" id="trigger-upload" class="btn btn-danger hide">
        	        <i class="icon-upload icon-white"></i> <?php echo Yii::t("common", "Save") ?>
                </button>
				<span class="qq-drop-processing-selector qq-drop-processing">
					<span>En cours de progression...</span>
					<span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
				</span>
				<?php if(isset($uploaderObj["docType"]) && $uploaderObj["docType"]=="image"){ ?>
					<ul class="qq-upload-list-selector qq-upload-list" role="region" aria-live="polite" aria-relevant="additions removals">
						<li>
							<span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
							<div class="qq-progress-bar-container-selector qq-progress-bar-container">
								<div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
							</div>
							<span class="qq-upload-spinner-selector qq-upload-spinner"></span>
							<div class="qq-thumbnail-wrapper">
								<img class="qq-thumbnail-selector" qq-max-size="120" qq-server-scale>
							</div>
							<button type="button" class="qq-upload-cancel-selector qq-upload-cancel">X</button>
							<button type="button" class="qq-upload-retry-selector qq-upload-retry">
								<span class="qq-btn qq-retry-icon" aria-label="Retry"></span>
								Retry
							</button>
							<div class="qq-file-info">
								<div class="qq-file-name">
								<span class="qq-upload-file-selector qq-upload-file"></span>
								</div>
								<input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
								<span class="qq-upload-size-selector qq-upload-size"></span>
								<button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">
									<?php echo Yii::t("common", "Delete") ?>
								</button>
								<button type="button" class="qq-btn qq-upload-pause-selector qq-upload-pause">
								<span class="qq-btn qq-pause-icon" aria-label="Pause"></span>
								</button>
								<button type="button" class="qq-btn qq-upload-continue-selector qq-upload-continue">
								<span class="qq-btn qq-continue-icon" aria-label="Continue"></span>
								</button>
							</div>
							<button type="button" class="view-btn hide btn">View</button>
						</li>
					</ul>
				<?php }else{ ?>
					<ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
				        <li>
		                    <div class="qq-progress-bar-container-selector">
		                        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
		                    </div>
				             <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
		                    <img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale>
		                    <a href="" target="_blank" class="qq-upload-link">
		                    	<span class="qq-upload-file-selector qq-upload-file"></span>
			                    <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
			                    <span class="qq-upload-size-selector qq-upload-size"></span>
		                    </a>
				            <button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel"><?php echo Yii::t("common", "Cancel") ?></button>
		                    <button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry"><?php echo Yii::t("common", "Retry") ?></button>
		                    <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete"><?php echo Yii::t("common", "Delete") ?></button>
							<button type="button" class="view-btn hide btn">View</button>
				            <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
		                </li>
		            </ul>
				<?php } ?>
				<dialog class="qq-alert-dialog-selector">
					<div class="qq-dialog-message-selector"></div>
						<div class="qq-dialog-buttons">
						<button type="button" class="qq-cancel-button-selector">Close</button>
					</div>
				</dialog>
				<dialog class="qq-confirm-dialog-selector">
					<div class="qq-dialog-message-selector"></div>
					<div class="qq-dialog-buttons">
						<button type="button" class="qq-cancel-button-selector">No</button>
						<button type="button" class="qq-ok-button-selector">Yes</button>
					</div>
				</dialog>
				<dialog class="qq-prompt-dialog-selector">
					<div class="qq-dialog-message-selector"></div>
					<input type="text">
					<div class="qq-dialog-buttons">
						<button type="button" class="qq-cancel-button-selector">Cancel</button>
						<button type="button" class="qq-ok-button-selector">Ok</button>
					</div>
				</dialog>
				<div class="col-xs-12 text-center fineUploaderOpenFormSubmit" style="display: none;">
					<a href="javascript:;" class="btn btn-success margin-top-10 col-sm-6 col-sm-offset-3 fineUploader_submit" data-dom-uploader="<?php echo $domUploader ?>" id="<?php echo $domUploader ?>_submit"><?php echo Yii::t("common","Save") ?></a>
				</div>
			</div>
		</script>
	<!---->
	
<?php } ?>
</div>
<?php if($mode != "pdf"){ ?>
<script type="text/javascript">
	//var domUploader="<?php echo $domUploader ?>";
	var uploadAnswerObj=<?php echo json_encode($uploaderObj); ?>;
	var initListUploader=<?php echo json_encode(@$initAnswerFiles); ?>;
	uploadAnswerObj["path"]=getPath(uploadAnswerObj.contextType,uploadAnswerObj.contextId, uploadAnswerObj.docType, null, null, uploadAnswerObj.extraPath);
	jQuery(document).ready(function() {


		$("#"+uploadAnswerObj.dom).off();
		$("#"+uploadAnswerObj.dom).fineUploader({
            template: ((uploadAnswerObj.docType=="image") ? 'qq-template-gallery'+uploadAnswerObj.dom : 'qq-template-manual-trigger'+uploadAnswerObj.dom),
            paste: {
		        defaultName: 'pasted_image',
		       //promptForName:true,
		        targetElement: $("#"+uploadAnswerObj.dom+"_paste")
		    },
		    classes : {
		    	dropActive : true
		    },
            request: {
                endpoint: uploadAnswerObj.path,
                params : uploadAnswerObj.params
            },
            validation: {
                allowedExtensions: uploadAnswerObj.formats,
                sizeLimit: ((typeof uploadAnswerObj.sizeLimit != "undefined") ? uploadAnswerObj.sizeLimit : 5000000),
                itemLimit: ((typeof uploadAnswerObj.itemLimit != "undefined") ? uploadAnswerObj.itemLimit : 3)
            },
            messages: {
		        sizeError : '{file} '+tradDynForm.istooheavy+'! '+tradDynForm.limitmax+' : {sizeLimit}.',
		        typeError : '{file} '+tradDynForm.invalidextension+'. '+tradDynForm.extensionacceptable+': {extensions}.'
		    },
		    session:{
		    	endpoint:null
		    },
		    deleteFile: {
		        enabled: true
		    },
            callbacks: {
            	//when a img is selected
			   onPasteReceived: function(blob) {
    			},
    			onSubmitDelete: function(id) {},
			    //when every img finish upload process whatever the status
			    onComplete: function(id, fileName,responseJSON,xhr) {
			    	mylog.log("onCompleate", id, fileName, responseJSON);
			    	setFiles={};
			    	setFiles[responseJSON.id.$id]=responseJSON;
		            var serverPathToFile = responseJSON.docPath,
			       	fileItem = this.getItemByFileId(id);
			        var viewTnh = qq(fileItem).getByClass("qq-upload-link")[0];
			        if(typeof viewTnh != "undefined")
                		viewTnh.setAttribute("href", serverPathToFile);
			      	this.setDeleteFileEndpoint(baseUrl+"/"+moduleId+"/document/deletedocumentbyid/id/"+responseJSON.id.$id, id);
				   
			    	if(!responseJSON.result){
			    		toastr.error(trad.somethingwentwrong+" : "+responseJSON.msg );		
			    		mylog.error(trad.somethingwentwrong , responseJSON.msg)
			    	}
			    },
			    onStatusChange : function(id, oldStatus, newStatus) {	
				},
			    onSessionRequestComplete:function(response, success, xhrOrXdr){
			    },
			    onError: function(id) {
			      toastr.info(trad.somethingwentwrong);
			    }
			},
            thumbnails: {
                placeholders: {
                    waitingPath: baseUrl+'/plugins/fine-uploader/jquery.fine-uploader/processing.gif',
                    notAvailablePath: baseUrl+'/plugins/fine-uploader/jquery.fine-uploader/retry.gif'
                }
            },
            autoUpload: false
        }).on("submit", function(event,id, fileName) {
	    	listObject=$(this).fineUploader('getUploads');
			showBtnUpload=false;
			if(listObject.length > 0){
	    		$.each(listObject, function(e,v){
	    			if(v.status == "submitting")
	    				showBtnUpload=true;
	    		});
	    	}
			if( showBtnUpload ){
	    		$(this).find(".fineUploaderOpenFormSubmit").show();//.fineUploader('uploadStoredFiles');
	    	}
		    
	    }).on("allComplete", function(event, succeeded, failed) {
	    	mylog.log("ooooooooooooo",succeeded,failed);
	    	$(this).find(".fineUploaderOpenFormSubmit").fadeOut();
	    });
		if(typeof initListUploader != "undefined" && Object.keys(initListUploader).length > 0){
			//objectListFiles=uploadObj.prepareInit(initFiles);
        	//console.log(objectListFiles,"bloublou");
			$("#"+uploadAnswerObj.dom).fineUploader("addInitialFiles",prepFileForFormsUploader(initListUploader));
			$i=0;
			$.each(initListUploader, function(e,v){
				$("#"+uploadAnswerObj.dom).find(".qq-file-id-"+$i+" .qq-upload-link").attr("href", v.docPath);
				$i++;
			});
		}
					
		$(".fineUploader_submit").off().on("click",function(){
			idUploader=$(this).data("dom-uploader");
			listObject=$("#"+idUploader).fineUploader('getUploads');
			goToUpload=false;
			if(listObject.length > 0){
	    		$.each(listObject, function(e,v){
	    			if(v.status == "submitted")
	    				goToUpload=true;
	    		});
	    	}
			if( goToUpload ){
				insideCallBMulti=true;
	    		$("#"+idUploader).fineUploader('uploadStoredFiles');
		  	}
		});
});

function getPath (type,id, docT, contentK, foldKey, extraUrl){
		//mylog.log("uploadObj.get", type,id, docT, contentK, foldKey, extraUrl);
		docT=(docT !== null && docT) ? docT : "image";
		typeForUpload = type; 
		path = baseUrl+"/"+moduleId+"/document/upload-save/dir/communecter/folder/"+typeForUpload+"/ownerId/"+id+"/input/qqfile/docType/"+docT;
		if(contentK !== null && contentK != "")
			path += "/contentKey/"+contentK;
		else if(docT == "image")
			path += "/contentKey/profil";
		if(foldKey !== null && foldKey != "")
			path += "/folderId/"+foldKey;
		if(extraUrl !== null && extraUrl != "" && extraUrl !== true)
			path += extraUrl;
		//mylog.log("uploadObj.get return", path, uploadObj.path);
		return path;
	}
function prepFileForFormsUploader(data){
		arrayList=[];
		$i=0;
		$.each(data, function(e, v){
			item=new Object;
			item.size=v.size,
			item.uuid=e,
			item.name=v.name,
			item.deleteFileEndpoint=baseUrl+"/"+moduleId+"/document/deletedocumentbyid/id";
			item.deleteFileParams={ids:e};
			if(typeof v.imageThumbPath != "undefined"){
				item.thumbnailUrl=v.imageThumbPath;
				item.pathUrl=v.docPath;
			}if(typeof v.docPath != "undefined"){
				item.thumbnailUrl=v.docPath;
				item.pathUrl=v.docPath;
			}
			//initListUploader[e].uuid=$i;
			$i++;
			arrayList.push(item);
		} );
		return arrayList;
	}
</script>
<?php } ?>