<?php 
$inpClass = "";
if($saveOneByOne)
	$inpClass = " saveOneByOne";

if($mode == "r" || $mode == "pdf"){ ?>
    <div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
        <label for="<?php echo $kunik ?>"><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label ?></h4></label><br/>
        <?php echo $answers; ?>
    </div>
<?php 
} else {
?>
	<div class="form-group">
	    <label for="<?php echo $key ?>"><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn ?></h4></label>
	    <br/><textarea class="form-control <?php echo $inpClass ?>" id="<?php echo $key ?>" aria-describedby="<?php echo $key ?>Help" placeholder="<?php echo (isset($placeholder)) ? $placeholder : '' ?>" data-form='<?php echo $form["id"] ?>' > <?php echo (!empty($answers)) ? $answers : "" ?></textarea>
	    <?php if(!empty($info)){ ?>
	    	<small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
	    <?php } ?>
	</div>

	
<?php } ?>



<?php if($mode != "pdf"){ ?>
<script type="text/javascript">
	jQuery(document).ready(function() {
	    mylog.log("render form input","/modules/costum/views/tpls/forms/textarea.php");
	});
</script>
<?php } ?>