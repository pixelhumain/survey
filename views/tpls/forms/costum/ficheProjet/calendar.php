<?php if($answer){ ?>
	<div class="form-group">
		<table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>">
			<?php 
			
			function afficheEnFonctionAnnee($annee){
				$pdata = [ 
					"sectionTitles" => [
						"Janv", 
						"Fév", 
						"Mars", 
						"Avr", 
						"Mai", 
						"Juin", 
						"Juil", 
						"Août", 
						"Sept", 
						"Oct",
						"Nov",
						"Dec"
					],
					"dateSections" => [ 
						"01/01/".$annee, 
						"01/02/".$annee, 
						"01/03/".$annee, 
						"01/04/".$annee, 
						"01/05/".$annee, 
						"01/06/".$annee, 
						"01/07/".$annee, 
						"01/08/".$annee, 
						"01/09/".$annee, 
						"01/10/".$annee, 
						"01/11/".$annee,
						"01/12/".$annee,
						"31/12/".$annee
					]
				];
				return $pdata;
			}
			$userId = null;
			if(isset(Yii::app()->session["userId"])){
				$userId = Yii::app()->session["userId"];
			}
			$valeur_annee=null;
			$anneeCourante=date('Y');
			$paramsData = afficheEnFonctionAnnee($anneeCourante);
			if (isset($parentForm["params"][$userId]["annee"])){
				$valeur_annee = $parentForm["params"][$userId]["annee"];
				$paramsData = afficheEnFonctionAnnee($valeur_annee);
			} 
			// var_dump($valeur_annee);
			// var_dump($paramsData);
 			$properties_annee = [
                "annee" => [
                    "label" => "Entrez l'année",
                    "inputType" => "text",
                    "rules" => [ "required" => true]
                ]
            ];

			$dateSections = (isset($parentForm["params"][$kunik]["dateSections"])) ? $parentForm["params"][$kunik]["dateSections"] : $paramsData["dateSections"] ;

			$editBtnL = (Yii::app()->session["userId"] == $answer["user"]) ? " 
			<a href='javascript:;' 
				data-id='".$answer["_id"]."' 
				data-collection='".Form::ANSWER_COLLECTION."' 
				data-path='".$answerPath."' 
				class='add".$kunik." btn btn-default'>
			<i class='fa fa-plus'>
			</i>
			Ajouter une action 
			</a>" : "";
			$choisir = "
			<a href='javascript:;' 
				data-id='".(string)$parentForm["_id"]."' 
				data-collection='".Form::COLLECTION."' 
				data-path='params.".Yii::app()->session["userId"]."'
				class='choisir".$kunik." btn btn-default'>
				<i class='fa fa-plus'>
				</i>
				Choisir année
			</a>";
			//var_dump($parentForm["_id"]->{'$id'});
			/*var_dump((string)$parentForm["_id"]);
			var_dump($answerPath);
			var_dump(Form::COLLECTION);*/
			//var_dump($parentForm);
			$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";
			
			
			?>	
			<thead>
				<tr>
					<td colspan='<?php echo count( $dateSections)+2?>' >
						<?php if($mode == "r" || $mode == "pdf"){ ?>
							<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label." Année ".$valeur_annee.$editQuestionBtn.$editParamsBtn.$choisir?>
							</h4>
							<?php echo $info ?>
						<?php }else{ ?>
							<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label." Année ".$valeur_annee.$editQuestionBtn.$editParamsBtn.$editBtnL.$choisir?>
							</h4>
						<?php }?>							

					</td>
				</tr>	
				<?php if(isset($answers) && count($answers)>0){ ?>
					<tr>
						<th>Actions<br/>
						</th>
						<?php 
						$sectionTitles = (isset($parentForm["params"][$kunik]["sectionTitles"])) ? $parentForm["params"][$kunik]["sectionTitles"] : $paramsData["sectionTitles"];
						foreach ($sectionTitles as $i => $lbl) {
							echo "<th>".$lbl."</th>";
						} ?>
						<th></th>
					</tr>
				<?php } ?>
			</thead>
			<tbody class="directoryLines">	
				<?php 
				$ct = 0;

				if(isset($answers)){
					
					foreach ($answers as $q => $a) {
						$startAnnee =date("Y",strtotime($a["startDate"]));
						$endAnnee=date("Y",strtotime($a["startDate"]));
						if($startAnnee == $valeur_annee && $endAnnee == $valeur_annee){
						echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>".
						"<td>".@$a["step"]."</td>";
						$bgColor = "white";
						$td = "";
						foreach ($dateSections as $sa => $sv) {
							if( $bgColor == "white"){
								if( isset( $dateSections[$sa+1] ) && strtotime(str_replace('/', '-',@$a["startDate"])) < strtotime(str_replace('/', '-',$dateSections[$sa+1]))  ) {
									$bgColor = "#9fbd38";
									$td = "";
								}
							} else if( $bgColor == "#9fbd38"){
								if( strtotime(str_replace('/', '-',@$a["endDate"])) < strtotime(str_replace('/', '-',$sv))  ) {
									$bgColor = "";
									$td = "";
								}
							}
							if( $sa != sizeof( $dateSections ) - 1 )
								echo "<td class='text-center' style='border: 1px solid #ddd;background-color:".$bgColor."'>".$td."</td>";
						}
						?>
						<td>
							<?php 
							echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
								"canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
								"id" => $answer["_id"],
								"collection" => Form::ANSWER_COLLECTION,
								"q" => $q,
								"path" => $answerPath.$q ,
								"keyTpl"=>$kunik
							] );
							?>
							<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')">
								<?php 
								echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q)); ?> 
								<i class='fa fa-commenting'></i></a>
							</td>
							<?php 
							$ct++;
							echo "</tr>";
						}
					}
					}
					?>
				</tbody>
			</table>
		</div>
		<script type="text/javascript">
			var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;

			$(document).ready(function() { 

				sectionDyf.<?php echo $kunik ?> = {
					"jsonSchema" : {	
						"title" : "Planning  et temporalité",
						"icon" : "fa-calendar",
						"properties" : {
							"step" : {
								"inputType" : "text",
								"label" : "Actions",
								"placeholder" : "Actions",
								"rules" : { "required" : true }
							},
							"startDate" : {
								"inputType" : "date",
								"label" : "Date début",
								"placeholder" : "Début de l'action",
								"rules" : { "required" : true }
							},
							"endDate" : {
								"inputType" : "date",
								"label" : "Date fin",
								"placeholder" : "Fin de l'action",
								"rules" : { "required" : true }
							}
						},
						save : function () {  
							tplCtx.value = {};
							$.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) { 
								tplCtx.value[k] = $("#"+k).val();
							});
							mylog.log("save tplCtx",tplCtx);
							if(typeof tplCtx.value == "undefined")
								toastr.error('value cannot be empty!');
							else {
								dataHelper.path2Value( tplCtx, function(params) { 
									urlCtrl.loadByHash(location.hash);
								} );
							}

						}
					}
				};

				sectionDyf.<?php echo $kunik ?>choix = {
					"jsonSchema" : {	
						"title" : "Choisir une année",
						"icon" : "fa-calendar",
						"properties" : <?php echo json_encode($properties_annee); ?>,
						save : function () {  
							tplCtx.value = {};
							$.each( sectionDyf.<?php echo $kunik ?>choix.jsonSchema.properties , function(k,val) { 
								tplCtx.value[k] = $("#"+k).val();
							});
							mylog.log("save tplCtx",tplCtx);
							if(typeof tplCtx.value == "undefined")
								toastr.error('value cannot be empty!');
							else {
								dataHelper.path2Value( tplCtx, function(params) { 
									urlCtrl.loadByHash(location.hash);
								} );
							}

						}
					}
				};

				sectionDyf.<?php echo $kunik ?>Params = {
					"jsonSchema" : {	
						"title" : "<?php echo $kunik ?> config",
						"description" : "Labels et dates de la section",
						"icon" : "fa-cog",
						"properties" : {
							sectionTitles : {
								"inputType" : "array",
								"label" : "Label de chaque période"
							},
							dateSections : {
								"inputType" : "array",
								"label" : "Date de chaque période"
							}
						},
						save : function () {  
							tplCtx.value = {};
							$.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
								if(val.inputType == "array")
									tplCtx.value[k] = getArray('.'+k+val.inputType);
								else
									tplCtx.value[k] = $("#"+k).val();
							});
							mylog.log("save tplCtx",tplCtx);

							if(typeof tplCtx.value == "undefined")
								toastr.error('value cannot be empty!');
							else {
								dataHelper.path2Value( tplCtx, function(params) { 
									urlCtrl.loadByHash(location.hash);
								} );
							}

						}
					}
				};

				sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( ( isset($parentForm["params"][$kunik]) ) ? $parentForm["params"][$kunik] : $paramsData); ?>;

				mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

    //adds a line into answer
    $(".add<?php echo $kunik ?>").off().on("click",function() {  
    	tplCtx.id = $(this).data("id");
    	tplCtx.collection = $(this).data("collection");       
    	tplCtx.path = $(this).data("path")+(notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length :"0");
    	dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
    });

    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
    	tplCtx.id = $(this).data("id");
    	tplCtx.collection = $(this).data("collection"); 
    	tplCtx.path = $(this).data("path");
    	dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
    });

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
    	tplCtx.id = $(this).data("id");
    	tplCtx.collection = $(this).data("collection");
    	tplCtx.path = $(this).data("path");
    	dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    $(".choisir<?php echo $kunik ?>").off().on("click",function() {
	 tplCtx.id = $(this).data("id");
	 tplCtx.collection = $(this).data("collection");
	 tplCtx.path = $(this).data("path");
	 dyFObj.openForm( sectionDyf.<?php echo $kunik ?>choix );
	});

     
});
</script>
<?php } else {
} ?>