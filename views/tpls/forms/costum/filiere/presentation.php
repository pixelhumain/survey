    <?php if($answer){
    	//var_dump($answers);
    	//var_dump(Yii::app()->session["user"]);
    	//var_dump($answerPath);
    	/*if (isset($answer["answers"][$form["id"]][$kunik])) {
    		var_dump($answer["answers"][$form["id"]][$kunik]);
    	}*/

        //var_dump(Form::ANSWER_COLLECTION);

        ?>
        <div class="form-group">
            <?php

                $paramsData = [
                    "nbmax" => "2", "num_row" => ["limité","illimité"]
                ];

                if( isset($this->costum["form"]["params"][$kunik]["nbmax"]) )
                    $paramsData["nbmax"] =  $this->costum["form"]["params"][$kunik]["nbmax"];
                if( isset($this->costum["form"]["params"][$kunik]["limited"]) )
                    $paramsData["num_row"] =  $this->costum["form"]["params"][$kunik]["num_row"];


                $independant_props = [
                    "entity" => [
                        "label" => "",
                        "placeholder" => "",
                        "inputType" => "hidden",
                        "value" => "Indépendant"
                    ],
                    "speciality" => [
                        "label" => "Votre spécialité",
                        "class" => "form-control",
                        "inputType" => "select",
                        "placeholder" => "Vous vous êtes caractérisé en quoi",
                        "options" => [
                            "Programmation et Développements" => "Programmation et Développements",
                            "Intélligence artificielle et de la données" => "Intélligence artificielle et de la données",
                            "Infrastructures, clouds, réseaux et data centers" => "Infrastructures, clouds, réseaux et data centers",
                            "Maintenance, assistance et support pour l'exploitation" => "Maintenance, assistance et support pour l'exploitation",
                            "Interfaces utilisateurs et créations numériques" => "Interfaces utilisateurs et créations numériques",
                            "Direction, management et stratégie" => "Direction, management et stratégie",
                            "Communication et marketing" => "Communication et marketing",
                            "Commerce" => "Commerce",
                            "Expertise et conseil" => "Expertise et conseil"
                        ],
                        "rules" => ["required" => true]
                    ],
                    "situation" => [
                        "label" => "Situation professionnel",
                        "placeholder" => "",
                        "inputType" => "textarea",
                        "class" => "col-md-11 col-sm-12",
                        "rules" => [ "required" => true ]
                    ],
                    "competences" => [
                        "label" => "Compétences",
                        "placeholder" => "",
                        "inputType" => "lists",
                        "entries" => [
                            "competence" => [
                                "label" => "ajouter une compétence",
                                "placeholder" => "",
                                "type" => "text",
                                "class" => "col-md-11 col-sm-12",
                            ]
                        ]
                    ],
                    "passions" => [
                        "label" => "Passion personnelle",
                        "placeholder" => "",
                        "inputType" => "lists",
                        "entries" => [
                            "passion" => [
                                "label" => "ajouter une passion",
                                "placeholder" => "",
                                "type" => "text",
                                "class" => "col-md-11 col-sm-12",
                            ]
                        ]
                    ],
                    "address" => [
                        "label" => "Adresse",
                        "placeholder" => "",
                        "inputType" => "formLocality"
                    ]
                ];

                $entreprise_props = [
                    "entity" => [
                        "label" => "",
                        "placeholder" => "",
                        "inputType" => "hidden",
                        "value" => "Entreprise"
                    ],
                    "organizaitonName" => [
                        "label" => "Nom de votre entreprise :",
                        "placeholder" => "",
                        "inputType" => "text",
                        "rules" => [ "required" => true ]
                    ],
                    "organizationPropos" => [
                        "label" => "à propos de votre entreprise :",
                        "placeholder" => "",
                        "inputType" => "textarea",
                        "rules" => [ "required" => true ]
                    ],
                    "services" => [
                        "label" => "Services",
                        "placeholder" => "",
                        "inputType" => "lists",
                        "entries" => [
                            "passion" => [
                                "label" => "ajouter un service",
                                "placeholder" => "",
                                "type" => "text",
                                "class" => "col-md-11 col-sm-12"
                            ]
                        ]
                    ],
                    "address" => [
                        "label" => "Adresse",
                        "placeholder" => "",
                        "inputType" => "formLocality"
                    ]
                ];
            ?>

            <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
                <?php echo $label.$editQuestionBtn?>
            </h4>

            <?php echo $info ?>

            <style>
                .s19{list-style: none;}
                .s19 li:before{
                    content: '\f0a9';
                    margin-right: 15px;
                    font-family: FontAwesome;
                    color: #d9534f;
                }
            </style>

            <br>

            <?php
            
            // indépendant and entreprise buttons
            
            echo "<a href='javascript:;' 
                    data-id='".$answer["_id"]."' 
                    data-collection='".Form::ANSWER_COLLECTION."' 
                    data-path='".$answerPath."' 
                    class='add".$kunik."1 btn btn-default btn-lg'>
                    <i class='fa fa-user'></i> 
                    Indépendant
                </a>

                &nbsp; &nbsp;

                <a href='javascript:;' 
                    data-id='".$answer["_id"]."' 
                    data-collection='".Form::ANSWER_COLLECTION."' 
                    data-path='".$answerPath."' 
                    class='add".$kunik."2 btn btn-success btn-lg'>
                    <i class='fa fa-users'></i> 
                    Entreprise
                </a>";

            ?>
            
            <ol class="s19">
                <?php
                # dispay user's info
                if(isset($answers)){
                    foreach ($answers as $q => $a) {
                ?>
                    <!--li>
                        <?php echo $a["entity"] ?>
                        <span class="pull-right">
                            <?php
                            echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
                                "canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
                                "id" => $answer["_id"],
                                "collection" => Form::ANSWER_COLLECTION,
                                "q" => $q,
                                "path" => "answers.".$kunik.".".$q,
                                "keyTpl"=>$kunik
                            ] ); ?>
                            <a 
                                href="javascript:;" 
                                class="btn btn-xs btn-primary openAnswersComment" 
                                onclick="commentObj.openPreview(
                                    'answers',
                                    '<?php echo $answer["_id"]?>',
                                    '<?php echo $answer["_id"].$key.$q ?>', 
                                    '<?php echo @$a['step'] ?>')"
                            >
                                    <?php echo PHDB::count(Comment::COLLECTION, 
                                    array(
                                    "contextId"=>$answer["_id"],
                                    "contextType"=>"answers", 
                                    "path"=>$answer["_id"].$key.$q
                                    )
                                )
                                ?> 
                                <i class='fa fa-commenting'></i>
                            </a>
                        </span>
                    </li-->
                <?php
                    }
                } ?>
            </ol>
        </div>

        <script type="text/javascript">
            var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
            sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

            $(document).ready(function() {
                // Indépendant dynForm
                sectionDyf.<?php echo $kunik ?>modalIndependant = {
                    "jsonSchema" : {
                        "title" : "Présentez vous",
                        "icon" : "fa fa-user",
                        "text" : "De projet que vous avez réalisé ou aimerez faire",
                        "properties" : <?php echo json_encode( $independant_props ); ?>,
                        save : function () {
                            var today = new Date();
                            tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
                            
                            $.each(sectionDyf.<?php echo $kunik ?>modalIndependant.jsonSchema.properties , function(k,val) {
                                if(val.inputType == "formLocality"){
                                    tplCtx.value[k] = getArray('.'+k+val.inputType);
                                    console.log("Coordonnées", tplCtx.value[k]);
                                }else{
                                        tplCtx.value[k] = $("#"+k).val();
                                        
                                    }
                                });
                                /**
                                else if(val.inputType=="lists"){
                                    $.each(data.pages,function(inc,va){
                                        nameKeyPage=(typeof va.key != "undefined" && notEmpty(va.key)) ? va.key : "#"+va.name.replace(/[^\w]/gi, '').toLowerCase();
                                        tplCtx.value[nameKeyPage] = {
                                            hash : "#app.view",
                                            icon : va.icon,
                                            urlExtra : "/page/"+nameKeyPage.replace("#","")+"/url/costum.views.tpls.staticPage",
                                            useHeader : true,
                                            isTemplate : true,
                                            useFooter : true,
                                            useFilter : false,
                                            subdomainName : va.name,
                                            staticPage : true
                                        }
                                    });
                                }
                                 */

                            mylog.log("save tplCtx",tplCtx);

                            

                            if(typeof tplCtx.value == "undefined")
                                toastr.error('value cannot be empty!');
                            else {
                                dataHelper.path2Value(tplCtx, function(params) {
                                   dyFObj.commonAfterSave(null, function(){
                                        if(dyFObj.closeForm()){
                                        $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                                            location.reload();
                                        }else{
                                            location.reload();
                                        }
                                    });
                                });
                            }
                        }
                    }
                };

                // Entreprise dynForm
                sectionDyf.<?php echo $kunik ?>modalEntreprise = {
                    "jsonSchema" : {
                        "title" : "Présentez votre entreprise",
                        "icon" : "fa-users",
                        "text" : "De projet que vous avez réalisé ou aimerez faire",
                        "properties" : <?php echo json_encode( $entreprise_props ); ?>,
                        save : function () {
                            var today = new Date();
                            tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
                            $.each( sectionDyf.<?php echo $kunik ?>modalEntreprise.jsonSchema.properties , function(k,val) {

                                if(val.inputType == "formLocality"){
                                    tplCtx.value[k] = getArray('.'+k+val.inputType);
                                    console.log("Coordonnées", tplCtx.value[k]);
                                }else{
                                    tplCtx.value[k] = $("#"+k).val();
                                }
                            });

                            mylog.log("save tplCtx",tplCtx);


                            if(typeof tplCtx.value == "undefined")
                                toastr.error('value cannot be empty!');
                            else {
                                dataHelper.path2Value( tplCtx, function(params) {
                                   dyFObj.commonAfterSave(null, function(){
                                        if(dyFObj.closeForm()){
                                        $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                                            location.reload();
                                        }else{
                                            location.reload();
                                        }
                                    });
                                });
                            }
                        }
                    }
                };

                mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

                //add independant infos
                $(".add<?php echo $kunik ?>1").off().on("click",function() {
                    tplCtx.id = $(this).data("id");
                    tplCtx.collection = $(this).data("collection");
                    tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
                    dyFObj.openForm( sectionDyf.<?php echo $kunik ?>modalIndependant);
                });

                //add enterprise infos
                $(".add<?php echo $kunik ?>2").off().on("click",function() {
                    tplCtx.id = $(this).data("id");
                    tplCtx.collection = $(this).data("collection");
                    tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
                    dyFObj.openForm( sectionDyf.<?php echo $kunik ?>modalEntreprise);
                });

                // edit independant infos
                $(".edit<?php echo $kunik ?>1").off().on("click",function() {
                    tplCtx.id = $(this).data("id");
                    tplCtx.collection = $(this).data("collection");
                    tplCtx.path = $(this).data("path");
                    dyFObj.openForm( sectionDyf.<?php echo $kunik ?>modalIndependant,null, <?php echo $kunik ?>Data[$(this).data("key")]);
                });

                // edit entreprise infos
                $(".edit<?php echo $kunik ?>2").off().on("click",function() {
                    tplCtx.id = $(this).data("id");
                    tplCtx.collection = $(this).data("collection");
                    tplCtx.path = $(this).data("path");
                    dyFObj.openForm( sectionDyf.<?php echo $kunik ?>modalEntreprise,null, <?php echo $kunik ?>Data[$(this).data("key")]);
                });

                $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                    tplCtx.id = $(this).data("id");
                    tplCtx.collection = $(this).data("collection");
                    tplCtx.path = $(this).data("path");
                    //if no params config on the element.costum.form.params.<?php echo $kunik ?>
                    //then we load default values available in forms.inputs.<?php echo $kunik ?>xxx.params
                    //mylog.log(".editParams",sectionDyf.<?php echo $kunik ?>Params,calData);
                    dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
                });
            });
        </script>
    <?php } else {
        //echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
    } ?>