<?php
$ids = explode(".",  $answerPath);

if(isset(Yii::app()->session["userId"])){
    $data = PHDB::findByIds("citoyens", [$answer["user"]] ,["links.follows", "links.memberOf"]);
    
    $follows_ids = array();
    $memberOf_ids = array();
    
    $links1 = [];
    
    if($mode=="w"){
        if(isset($data[Yii::app()->session["userId"]]["links"]["follows"])){
            $links1 = $data[Yii::app()->session["userId"]]["links"]["follows"];
        }
    }else{
        if(isset($answer[$ids[0]][$ids[1]]["friends"])){
            $links1 = $answer[$ids[0]][$ids[1]]["friends"];
        }
    }
    
    foreach ($links1 as $key => $value) {
        array_push($follows_ids, $key);
    }
        
    $friends = PHDB::findByIds("citoyens", $follows_ids ,["name", "profilImageUrl"]);

    $links2 = [];
    
    if($mode=="w"){
        if(isset($data[Yii::app()->session["userId"]]["links"]["memberOf"])){
            $links2 = $data[Yii::app()->session["userId"]]["links"]["memberOf"];
        }
    }else{
        if(isset($answer[$ids[0]][$ids[1]]["entreprises"])){
            $links2 = $answer[$ids[0]][$ids[1]]["entreprises"];
        }
    }
    
    foreach ($links2 as $key => $value) {
        array_push($memberOf_ids, $key);
    }

    $organizations = PHDB::findByIds("organizations", $memberOf_ids ,["name", "profilImageUrl"]);

}
?>

<?php $inpClass =  "saveOneByOne"; ?>

<label class="form-check-label" for="<?php echo $kunik ?>"><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn ?></h4></label>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <h5>Citoyens :</h5>
            <?php if(isset($friends)) { 
                foreach ($friends as $keyf => $friend) {
                    $valueId =  "";
                    if(isset($answer[$ids[0]][$ids[1]]["friends"])){
                        if(array_key_exists("$keyf", $answer[$ids[0]][$ids[1]]["friends"])){
                            $valueId = "checked";
                        }
                    }
                ?>

                <div class="form-check list-group-item">
                    &nbsp; &nbsp;
                    <?php if($mode=="w"){ ?>
                        <input type="checkbox" class="form-check-input <?php echo $inpClass ?>"  id="<?= $keyf ?>" data-form='<?php echo $form["id"] ?>.friends' name="friends" <?php echo $valueId ?>  > &nbsp; &nbsp;
                    <?php } ?>
                    
                    <img src="<?= (isset($friend["profilImageUrl"]))?$friend["profilImageUrl"]:Yii::app()->getModule("co2")->assetsUrl.'/images/thumb/default_citoyens.png' ?>" height="40" class="img-rounded" alt="">
                    &nbsp; &nbsp; <?= $friend["name"]; ?>
                    <br/>
                </div>
            <?php }
            }
            ?>
        </div>
        <div class="col-md-6 col-sm-12">
            <h5>Organisations :</h5>
            <?php if(isset($organizations)) { 
                foreach ($organizations as $keyo => $orga) {
                    $valueOrgId = "";
                    if(isset($answer[$ids[0]][$ids[1]]["entreprises"])){
                        if(array_key_exists("$keyo", $answer[$ids[0]][$ids[1]]["entreprises"])){
                            $valueOrgId = "checked";
                        }
                    }
            ?>
                <div class="form-check list-group-item">
                    &nbsp; &nbsp;
                    <?php if($mode=="w"){ ?>
                        <input type="checkbox" class="form-check-input <?php echo $inpClass ?>"  id="<?= $keyo ?>" data-form='<?php echo $form["id"] ?>.entreprises' name="entreprises" <?php echo $valueOrgId ?>  > &nbsp; &nbsp;
                    <?php } ?>
                    
                    <img src="<?= (isset($orga["profilImageUrl"]))?$orga["profilImageUrl"]:Yii::app()->getModule("co2")->assetsUrl.'/images/thumbnail-default.jpg' ?>" height="40" class="img-rounded" alt="">
                    &nbsp; &nbsp; <?= $orga["name"]; ?>
                    <br/>
                </div>
            <?php }
            }
            ?>
        </div>
    </div>
</div>
