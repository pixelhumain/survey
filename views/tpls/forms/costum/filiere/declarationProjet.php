
<?php 
$ids = explode(".",  $answerPath);
$inpClass =  "saveOneByOne";
if($answer){
    $myProjects = PHDB::find("projects", array("creator"=>Yii::app()->session["userId"]));
    $prj_ids = [];
    if($mode=="w"){
        $projs = $myProjects;
    }else{
        if(isset($answer[$ids[0]][$ids[1]]["projets"])){
            foreach ($answer[$ids[0]][$ids[1]]["projets"] as $kp => $vpbool) {
                array_push($prj_ids, $kp);
            }
        }
        $projs = PHDB::findByIds("projects", $prj_ids);
    }
    
    
?>
<div class="form-group">
    <?php

    $editBtnL="";

    if($answer["user"]==Yii::app()->session["userId"]){
        $editBtnL = " <a href='javascript:;' 
            data-id='".$answer["_id"]."' 
            data-collection='".Form::ANSWER_COLLECTION."' 
            data-path='".$answerPath."' 
            class='add btn btn-default' onclick='dyFObj.openForm(`project`);'>
            <i class='fa fa-plus'></i> 
            Ajouter un projet
        </a>";
    }
    ?>
    <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
      	<?php echo $label.$editQuestionBtn.$editBtnL?>
    </h4>
    <?php echo $info ?>
        <!--h5>prjnisations :</h5-->
        <?php if(isset($projs)){
            foreach ($projs as $keyp => $prj) { 
                echo '<div class="col-md-6 col-sm-12">';
                $value = "";
                if(isset($answer[$ids[0]][$ids[1]]["projets"])){
                    if(array_key_exists("$keyp", $answer[$ids[0]][$ids[1]]["projets"])){
                        $value = "checked";
                    }
                }

        ?>
            <div class="form-check list-group-item">
                &nbsp; &nbsp;
                <?php if($mode=="w"){ ?>
                    <input type="checkbox" class="form-check-input <?php echo $inpClass ?>"  id="<?= $keyp ?>" data-form='<?php echo $form["id"] ?>.projets' name="projets" <?php echo $value ?>  > &nbsp; &nbsp;
                <?php } ?>
                
                <img src="<?= (isset($prj["profilImageUrl"]))?$prj["profilImageUrl"]:Yii::app()->getModule("co2")->assetsUrl.'/images/thumbnail-default.jpg' ?>" height="40" class="img-rounded" alt="">
                &nbsp; &nbsp; <?= $prj["name"]; ?>
                <br/>
            </div>
        <?php 

        echo "</div>";
        
        }
    }
    ?>
    </div>

    <script></script>
<?php } else {
    //echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>