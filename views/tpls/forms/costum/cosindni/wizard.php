<style>
    .sectionStepTitle {
        background: #fff;
        display: block;
        font-weight: 600;
        line-height: 40px;
        margin: 0;
        padding: 0 30px;
        position: relative;
        text-transform: uppercase;
        border: 3px solid <?php echo $color2; ?>;
        border-radius: 25px;
    }

    .title-header:before {
        border-top: 3px solid <?php echo $color2; ?>;
        content: "";
        left: 0;
        position: absolute;
        right: 0;
        top: 50%;
    }

</style>
<?php
$defaultColor = "#354C57";
$structField = "structags";

$keyTpl = "wizard";

$paramsData = ["title" => "", "color" => "", "background" => "", "nbList" => 2, "defaultcolor" => "#354C57", "tags" => "structags"];

if (isset($this->costum["tpls"][$keyTpl]))
{
    foreach ($paramsData as $i => $v)
    {
        if (isset($this->costum["tpls"][$keyTpl][$i])) $paramsData[$i] = $this->costum["tpls"][$keyTpl][$i];
    }
}

if (!empty($parentForm['hasStepValidations']) && is_numeric($parentForm['hasStepValidations']) && !(is_int($parentForm['hasStepValidations'])))
    $parentForm['hasStepValidations'] = intval($parentForm['hasStepValidations']);

//var_dump($parentForm);exit;
//var_dump($canEdit);
if (isset($parentForm["wizardIntroTpl"])) echo $this->renderPartial($parentForm["wizardIntroTpl"]);

if (!empty($parentForm["type"]) && ($parentForm["type"] == "aap" || $parentForm["type"] == "aapConfig"))
{

    if ($parentForm["type"] == "aap")
    {
        $configEl = PHDB::findOneById(Form::COLLECTION, $parentForm["config"]);
    }
    elseif ($parentForm["type"] == "aapConfig")
    {
        $configEl = $parentForm;
    }

    foreach ($configEl["subForms"] as $idConfigEl => $valueConfigEl)
    {
        $forms[$idConfigEl] = $valueConfigEl;
        if (isset($parentForm["params"][$idConfigEl])){
            $forms[$idConfigEl]["params"] = $parentForm["params"][$idConfigEl] ;
        }

    }



    $form["subForms"] = array_keys($configEl["subForms"]);
}

if (!empty($parentForm["type"]) && $parentForm["type"] == "aapConfig")
{
    $formsubForms = [];
    foreach ($configEl["subForms"] as $ksubF => $vsubF)
    {
        if (isset($vsubF["active"]) && $vsubF["active"] == true) // check if aapCOnfig subForms (step) is active and push to subforms array
            $formsubForms[] = $ksubF;
    }
}
else
{
    if (!empty($form["subForms"]))
    {
        $formsubForms = $form["subForms"];
    }
}

if (!empty($parentForm["projectGeneration"]) && $parentForm["projectGeneration"] == "onStart"){

    if (isset($parentForm["mapping"])) {
        if (isset($parentForm["mapping"]["proposition"])) {
            $namemap = explode(".", $parentForm["mapping"]["proposition"]);
        }
        if (isset($parentForm["mapping"]["description"])) {
            $descrmap = explode(".", $parentForm["mapping"]["description"]);
        }
        if (isset($parentForm["mapping"]["depense"])) {
            $depensemap = explode(".", $parentForm["mapping"]["depense"]);
        }
    }

    if (!empty($namemap) && isset($answer[$namemap[0]][$namemap[1]][$namemap[2]])) {
        $namepro = $answer[$namemap[0]][$namemap[1]][$namemap[2]];
    }
    if (!empty($descrmap) && isset($answer[$descrmap[0]][$descrmap[1]][$descrmap[2]])) {
        $descrpro = $answer[$descrmap[0]][$descrmap[1]][$descrmap[2]];
    }
    if (!empty($depensemap) && isset($answer[$depensemap[0]][$depensemap[1]][$depensemap[2]])) {
        foreach ($answer[$depensemap[0]][$depensemap[1]][$depensemap[2]] as $keyf => $valuef) {
            if (isset($valuef["financer"])) {
                foreach ($valuef["financer"] as $keyi => $valuei) {
                    if (isset($valuei["id"])) {
                        array_push($financer, $valuei["id"]);
                    }
                }
            }
        }
        if (isset($answer[$descrmap[0]][$descrmap[1]][$descrmap[2]])) {
            $descr = $answer[$descrmap[0]][$descrmap[1]][$descrmap[2]];
        }
    }


}


if ((isset($parentForm["subForms"]) && count($parentForm["subForms"]) > 1) || (isset($configEl["subForms"]) && count($configEl["subForms"]) > 1))
{
    ?>

    <style type="text/css">
        .swMain ul li  a.done .stepNumber {
            border-color: <?php echo (@$color2) ? $color2 : $defaultColor ?>;
            background-color: <?php echo (@$color1) ? $color1 : $defaultColor ?>;
        }

        .swMain ul li a.selected .stepDesc, .swMain ul li a.done .stepDesc {
            color: <?php echo (@$color2) ? $color2 : $defaultColor ?>;
            font-weight: bolder;
        }

        .swMain ul li a.selected::before, .swMain ul li a.done::before{
            border-color: <?php echo (@$color2) ? $color2 : $defaultColor ?>;
        }
        .wizard-banner #col-banner,#form_banner_uploader-contentBanner,#form_banner_uploader-contentBanner img{
            min-height: 300px;
        }
        .wizard-banner #col-banner a.fileupload-new{
            background-color: #7da53d;
            /* display: none; */
        }
        .wizard-banner #col-banner .fileupload-new:hover,#col-banner .fileupload-new-text:hover{
            color: #fff;
        }
        .wizard-banner #col-banner .fileupload-new-text{
            position: absolute;
            top: 43px;
            left: 10px;
            z-index: 1000;
            background-color: #7da53d;
            display: none;
        }
        .wizard-banner #col-banner:hover .fileupload-new-text{
            display: initial;
        }
        .wizard-banner .form-banner-text{
            /*position: absolute;
            bottom: 0;
            z-index: 1;
            width: 100%;
            background-color: #00000047;
            color: #fff;
            display: flex;
            justify-content: center;
            align-items: center;
            text-align: justify;
            padding: 0 40px;
            line-height: 1.5 !important;*/
        }
        .form-banner-text a{
            color:#7da53d;
        }
        .wizard-banner #form_banner_uploader-contentBanner .form-banner img{
            object-fit: cover;
        }
    </style>

    <div class="col-md-12 col-xs-12 margin-top-20 no-padding wizard-banner">
    <?php if(((isset($parentForm["useBannerImg"]) && $parentForm["useBannerImg"]== true) || (isset($parentForm["useBannerText"]) && $parentForm["useBannerText"]== true) )){  ?>
        <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 text-left no-padding" id="col-banner">
            <?php  if(isset($parentForm["useBannerImg"]) && $parentForm["useBannerImg"] == true){
                if($canEditForm){
                    $parentForm["collection"] = Form::COLLECTION;
                    echo $this->renderPartial("co2.views.element.modalBanner", array(
                            "id" => "form_banner_uploader",
                            "edit" => $canEditForm,
                            "openEdition" => $canEditForm,
                            "profilBannerUrl"=> @$parentForm["profilBannerUrl"],
                            "element"=> array(
                                "_id" => $parentForm["_id"],
                                "name" => $parentForm["name"],
                                "collection" => $parentForm["collection"],
                            ),
                        )
                    );       
                }
                ?>
                <div id="form_banner_uploader-contentBanner" class="col-md-12 col-sm-12 col-xs-12 no-padding">
                    <div class="form-banner">
                        <?php if (@$parentForm["profilBannerUrl"] && !empty($parentForm["profilBannerUrl"])){
                            $imgHtml='<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" alt="'.Yii::t("common","Banner").'"
                                src="'.Yii::app()->createUrl('/'.$parentForm["profilBannerUrl"]).'">';
                            if (@$parentForm["profilRealBannerUrl"] && !empty($parentForm["profilRealBannerUrl"])){
                                $imgHtml='<a href="'.Yii::app()->createUrl('/'.$parentForm["profilRealBannerUrl"]).'"
                                            class="thumb-info"  
                                            data-title="'.Yii::t("common","Cover image of")." ".$parentForm["name"].'"
                                            data-lightbox="all">'.
                                            $imgHtml.
                                        '</a>';
                            }
                            echo $imgHtml;
                            }else{
                            if(isset($pageConfig["banner"]["img"]) && !empty($pageConfig["banner"]["img"]))
                                $url=Yii::app()->getModule( "costum" )->assetsUrl.$pageConfig["banner"]["img"];
                            else if(in_array($parentForm["collection"], [Event::COLLECTION, Project::COLLECTION, Person::COLLECTION, Organization::COLLECTION]))
                                $url=Yii::app()->theme->baseUrl.'/assets/img/background-onepage/'.$parentForm["collection"].'.png';
                            else
                                $url=Yii::app()->theme->baseUrl.'/assets/img/background-onepage/connexion-lines.jpg';

                            echo '<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" alt="'.Yii::t("common","Banner").'"
                                src="'.$url.'">';
                        } ?>
                    </div>
                </div>
            <?php } ?>

            <?php  if(isset($parentForm["useBannerText"]) && $parentForm["useBannerText"] == true){  ?>
                <div class="form-banner-text" ><?= @$parentForm["bannerText"] ?></div>
                <?php if(!isset($parentForm["useBannerImg"]) || (isset($parentForm["useBannerImg"]) && $parentForm["useBannerImg"]==false )){ ?>
                    <style>
                        /*.wizard-banner .form-banner-text{
                            top:0 !important;
                            background-color: transparent;
                            color: #2C3E50;
                        }*/
                    </style>
                <?php } ?>
            <?php } ?>
        </div>
            <script>
                $(function(){
                    var originalText = $(".form-banner-text").text();
                    var parsedText = curlyBrace(originalText);
                    $(".form-banner-text").text(parsedText);
                    var descHtml = dataHelper.markdownToHtml($(".form-banner-text").html(),{
                        parseImgDimensions:true,
                        simplifiedAutoLink:true,
                        strikethrough:true,
                        tables:true,
                        openLinksInNewWindow:true
                    });
                    $(".form-banner-text").html(descHtml);
                })
            </script>
    <?php }  ?>

        <div id="<?php echo $wizid ?>" class="swMain">
            <?php
            $activeStep = 0;
            $nextStepValid = true;
            $lockedStep = null;
            $showStepper = true;
            //parentForm ne contient que les subForms après la contrainte des "Rules"
            $havRules = false;
            $missingStep = [];
            if (!empty($parentForm["type"]) && ($parentForm["type"] == "aap" || $parentForm["type"] == "aapConfig"))
            {
                if (count($form["subForms"]) != count($configEl["subForms"]))
                {
                    $havRules = true;
                    $missingStep = array_diff($form["subForms"], $parentForm["subForms"]);
                }
                if (count($configEl["subForms"]) == 5)
                {
                    $indexOfEvaluation = "aapStep3";
                    $indexOfFinancement = "aapStep4";
                    $indexOfFollowUp = "aapStep5";

                }
                elseif (count($configEl["subForms"]) == 4)
                {
                    $indexOfEvaluation = "aapStep2";
                    $indexOfFinancement = "aapStep3";
                    $indexOfFollowUp = "aapStep4";
                }
                $allowedEvaluator = isset($configEl["subForms"][$indexOfEvaluation]["params"]["canEdit"]) ? $configEl["subForms"][$indexOfEvaluation]["params"]["canEdit"] : array();
                $allowedFinancor = isset($configEl["subForms"][$indexOfFinancement]["params"]["canEdit"]) ? $configEl["subForms"][$indexOfFinancement]["params"]["canEdit"] : array();
                $allowedFollowUp = isset($configEl["subForms"][$indexOfFollowUp]["params"]["canEdit"]) ? $configEl["subForms"][$indexOfFollowUp]["params"]["canEdit"] : array();

                $parentElement = array_keys($form["parent"]) [0];
                $me = isset(Yii::app()->session['userId']) ? Person::getById(Yii::app()
                    ->session['userId']) : null;
                $roles = isset($me["links"]["memberOf"][$parentElement]["roles"]) ? $me["links"]["memberOf"][$parentElement]["roles"] : null;
                $isAdmin = isset($me["links"]["memberOf"][$parentElement]["isAdmin"]) ? $me["links"]["memberOf"][$parentElement]["isAdmin"] : false;

                $canShowStepList = [];

                if (count($form["subForms"]) != count($configEl["subForms"]))
                {
                    $havRules = true;
                    $missingStep = array_diff($form["subForms"], $parentForm["subForms"]);
                }
                //for aap only
                $allowedEvaluator = isset($configEl["subForms"]["aapStep3"]["params"]["canEdit"]) ? $configEl["subForms"]["aapStep3"]["params"]["canEdit"] : array();
                $allowedFinancor = isset($configEl["subForms"]["aapStep4"]["params"]["canEdit"]) ? $configEl["subForms"]["aapStep4"]["params"]["canEdit"] : array();
                $allowedFollowUp = isset($configEl["subForms"]["aapStep5"]["params"]["canEdit"]) ? $configEl["subForms"]["aapStep5"]["params"]["canEdit"] : array();

                $intersectEvaluator = array_intersect($allowedEvaluator, $roles);
                $intersectFinancor = array_intersect($allowedFinancor, $roles);
                $intersectFollowUp = array_intersect($allowedFollowUp, $roles);

                if (count($intersectEvaluator) != 0 || $isAdmin) $canEvaluate = true;
                if (count($intersectFinancor) != 0 || $isAdmin) $canFinance = true;
                if (count($intersectFollowUp) != 0 || $isAdmin) $canFollowUp = true;

                foreach ($formsubForms as $k => $v)
                {
                    /*if (isset($indexOfEvaluation) && $v == $indexOfEvaluation && @$canEvaluate == false) unset($formsubForms[$k]);
                    elseif (isset($indexOfFinancement) && $v == $indexOfFinancement && @$canFinance == false) unset($formsubForms[$k]);
                    elseif (isset($indexOfFollowUp) && $v == $indexOfFollowUp && @$canFollowUp == false) unset($formsubForms[$k]);*/
                }
                if (count($formsubForms) <= 1) $showStepper = false;
            }
            else
            {
                if (count($form["subForms"]) != count($parentForm["subForms"]))
                {
                    $havRules = true;
                    $missingStep = array_diff($form["subForms"], $parentForm["subForms"]);
                }
            }

            if (!empty($showWizard))
            { ?>
                <ul id="wizardLinks" data-archi="liste des étapes" class="<?=$showStepper ? '' : 'hidden' ?>">
                    <?php
                    foreach ($formsubForms as $k => $v)
                    {
                        if (in_array($v, $missingStep) !== true)
                        {
                            $n = "todo";
                            $p = null;
                            $p = null;

                            if (!empty($forms[$v])) $n = $forms[$v]["name"];

                            //echo "<li>";
                            $lbl = "?";
                            $d = '';
                            // if(( isset($parentForm["hasStepValidations"]) && $parentForm["hasStepValidations"] !=="" && is_numeric($parentForm["hasStepValidations"]) ) )
                            //  $lbl = "";
                            $forms[$v]['open'] = false;
//                    var_dump(!empty($parentForm["type"]) && ($parentForm["type"] == "aap" || $parentForm["type"] == "aapConfig") && isset($configEl["hasStepValidations"]) && $k <= $configEl["hasStepValidations"]);

                            if( $mode == "fa" ||
                                (!isset($answer["step"]) && !( isset($parentForm["hasStepValidations"]) && $parentForm["hasStepValidations"] !=="" && is_numeric($parentForm["hasStepValidations"]) )) ||
                                (!isset($answer["step"]) && !empty($configEl)  && !isset($configEl["hasStepValidations"])) ||
                                $k == 0 ||
                                (isset($answer["step"]) && $answer["step"] == "all" ) ||
                                (( isset($parentForm["hasStepValidations"]) && $parentForm["hasStepValidations"] !=="" && is_numeric($parentForm["hasStepValidations"]) ) && isset($answer["step"]) && array_search($answer["step"], $form["subForms"]) !== false && $k <= array_search($answer["step"], $form["subForms"]) ) ||
                                ( ( isset($parentForm["hasStepValidations"]) && $parentForm["hasStepValidations"] !=="" && is_numeric($parentForm["hasStepValidations"]) ) && !isset($answer["step"]) && $k <= $parentForm["hasStepValidations"]   ) ||
                                (isset($configEl["hasStepValidations"]) && isset($answer["step"]) && array_search($answer["step"], $form["subForms"]) !== false && $k <= array_search($answer["step"], $form["subForms"]) ) ||
                                ( isset($configEl["hasStepValidations"]) && !isset($answer["step"]) && $k <= $configEl["hasStepValidations"]   )
                            )
                            {

                                //chec step rules
                                if( isset( $forms[$v]["rules"] ) ){
                                    //var_dump($forms[$v]["rules"]);
                                    if (isset($forms[$v]["rules"]["required"])) {
                                        foreach ($forms[$v]["rules"]["required"] as $i => $path) {
                                            $arr = $answer;
                                            $keys = explode('.', $path);
                                            foreach ($keys as $key) {
                                                if (isset($arr[$key])) $arr = &$arr[$key];
                                                else $lockedStep = true;
                                            }
                                        }
                                    }
                                    if (isset($forms[$v]["rules"]["checkValue"])) {
                                        foreach ($forms[$v]["rules"]["checkValue"] as $path => $value) {
                                            $arr = $answer;
                                            $keys = explode('.', $path);
                                            foreach ($keys as $key) {
                                                if (isset($arr[$key])) {
                                                    $arr = &$arr[$key];
                                                    if (is_array($value)) $lockedStep = (!in_array($arr, $value)) ? true : null;
                                                    else $lockedStep = ($arr != $value) ? true : null;
                                                } else $lockedStep = true;
                                            }
                                        }
                                    }
                                }

                                if (!empty($parentForm["type"]) && ($parentForm["type"] == "aap" || $parentForm["type"] == "aapConfig")) {
                                    //var_dump($forms[$v]["params"]["haveReadingRules"]);
                                    if (isset($forms[$v]["params"]["haveReadingRules"]) && $forms[$v]["params"]["haveReadingRules"] == "true") {
                                        $com_aap = Element::getCommunityByParentTypeAndId($parentForm["parent"]);
                                        foreach ($com_aap as $comid => $comd){
                                            if ($comd["type"] == "organizations" || $comd["type"] == "projects"){
                                                $subcom_aap = Element::getCommunityByTypeAndId($comd["type"], $comid);

                                                foreach ($subcom_aap as $subcomid => $subcomd){
                                                    $subroles = [];
                                                    if(!empty($subcomd["roles"])){
                                                        $subroles = $subcomd["roles"];
                                                    }
                                                    if(!empty($comd["roles"])){
                                                        $subroles = array_merge($subroles, $comd["roles"]);
                                                    }
                                                    $com_aap[$subcomid]["roles"] = $subroles;
                                                }
                                            }
                                        }
                                        /*var_dump(isset($com_aap[Yii::app()->session['userId']]));
                                        var_dump($com_aap);*/
                                        if (!empty($forms[$v]["params"]["canRead"]) && !is_array($forms[$v]["params"]["canRead"])){
                                            $forms[$v]["params"]["canRead"] = explode("," , $forms[$v]["params"]["canRead"]);
                                        }

                                        if (!empty($forms[$v]["params"]["canRead"]) && $forms[$v]["params"]["canRead"] != [""] && $forms[$v]["params"]["canRead"] != "") {
                                            if ( $answer["user"] != Yii::app()->session['userId']
                                                && (empty($parentForm["params"]["adminabsoluteaccess"] ) || $parentForm["params"]["adminabsoluteaccess"] != true || empty($com_aap[Yii::app()->session['userId']]["isAdmin"]) || $com_aap[Yii::app()->session['userId']]["isAdmin"] != "true")
                                                && (empty($com_aap[Yii::app()->session['userId']]["roles"]) || count(array_intersect($forms[$v]["params"]["canRead"], $com_aap[Yii::app()->session['userId']]["roles"])) <= 0 )
                                            ) {
                                                $lockedStep = true;
                                            } else {
                                                $lockedStep = false;
                                            }

                                        } else {
                                            if (   $answer["user"] != Yii::app()->session['userId']
                                                && (empty($com_aap[Yii::app()->session['userId']]["isAdmin"]) || $com_aap[Yii::app()->session['userId']]["isAdmin"] != "true")
                                            ) {
                                                $lockedStep = true;
                                            } else {
                                                $lockedStep = false;
                                            }
                                            /*if (empty($com_aap[Yii::app()->session['userId']]["isAdmin"]) || $com_aap[Yii::app()->session['userId']]["isAdmin"] != "true") {
                                                $lockedStep = true;
                                            }else {
                                                $lockedStep = false;
                                            }*/
                                        }
                                    }else{
                                        $lockedStep = false;
                                    }
                                }

                                if (!empty($parentForm["type"]) && ($parentForm["type"] == "aap" || $parentForm["type"] == "aapConfig")) {
                                    if (!$lockedStep) {
                                        $d = 'class="done"';
                                        $lbl = $k;
                                        $l = 'showStepForm(\'#' . $v . '\')';
                                        $activeStep = $k;
                                        $forms[$v]['open'] = true;
                                        $canShowStepList[] = "#" . $form["subForms"][$k];
                                    } else {
                                        $d = '';
                                        $lbl = "<i class='fa fa-lock'></i>";
                                        $l = '';
                                        $activeStep = $k;
                                        $forms[$v]['open'] = true;
                                        $canShowStepList[] = "#" . $form["subForms"][$k];
                                    }
                                }else{
                                    if (!$lockedStep) {
                                        $d = 'class="done"';
                                        $lbl = $k;
                                        $l = 'showStepForm(\'#' . $v . '\')';
                                        $activeStep = $k;
                                        $forms[$v]['open'] = true;
                                        $canShowStepList[] = "#" . $form["subForms"][$k];
                                    } else {
                                        $d = '';
                                        $lbl = "?";
                                        $l = '';
                                        $activeStep = $k;
                                        //$forms[$v]['open'] = true;

                                    }
                                }

                            }
                            //echo '<a onclick="' . $l . '" href="javascript:;" ' . $d . ' >';
                            //echo '<div class="stepNumber">' . $lbl . '</div>';
                            //echo '<span class="stepDesc">' . $n . '</span></a>';
                            if ($canEditForm === true && isset($forms[$v]["_id"]))
                            {
                                //echo '<div><a href="javascript:;" class="editFormBtn" data-id="' . $forms[$v]["_id"] . '"  data-formid="' . $v . '" ><i class="fa fa-pencil text-dark"></i></a> ';
                                //echo '<a href="javascript:;" class="deleteFormBtn" data-id="' . $forms[$v]["_id"] . '"  data-formid="' . $v . '" ><i class="fa fa-trash text-red"></i></a></div> ';
                            }
                            else
                            {
                                //var_dump($forms);

                            }
                            //echo "</li>";
                        }
                    }
                    ?>
                </ul>
                <?php
            } ?>

            <?php
            if (isset($parentForm["homeTpl"]) && !isset($answer["answers"])) echo $this->renderPartial($parentForm["homeTpl"]);
            ?>

            <?php
            foreach ($formsubForms as $k => $v)
            {

            if (!empty($parentForm["type"]) && ($parentForm["type"] == "aap" || $parentForm["type"] == "aapConfig")) {
                //var_dump($forms[$v]["params"]["haveReadingRules"]);
                if (isset($forms[$v]["params"]["haveReadingRules"]) && $forms[$v]["params"]["haveReadingRules"] == "true") {
                    $com_aap = Element::getCommunityByParentTypeAndId($parentForm["parent"]);
                    foreach ($com_aap as $comid => $comd){
                        if ($comd["type"] == "organizations" || $comd["type"] == "projects"){
                            $subcom_aap = Element::getCommunityByTypeAndId($comd["type"], $comid);

                            foreach ($subcom_aap as $subcomid => $subcomd){
                                $subroles = [];
                                if(!empty($subcomd["roles"])){
                                    $subroles = $subcomd["roles"];
                                }
                                if(!empty($comd["roles"])){
                                    $subroles = array_merge($subroles, $comd["roles"]);
                                }
                                $com_aap[$subcomid]["roles"] = $subroles;
                            }
                        }
                    }
                    /*var_dump(isset($com_aap[Yii::app()->session['userId']]));
                    var_dump($com_aap);*/
                    if (!empty($forms[$v]["params"]["canEdit"]) && !is_array($forms[$v]["params"]["canEdit"])){
                        $forms[$v]["params"]["canEdit"] = explode("," , $forms[$v]["params"]["canEdit"]);
                    }
                    if (!empty($forms[$v]["params"]["canRead"]) && $forms[$v]["params"]["canEdit"] != [""]) {
                        if (   $answer["user"] != Yii::app()->session['userId']
                            && (empty($forms[$v]["params"]["adminabsoluteaccess"] ) || $forms[$v]["params"]["adminabsoluteaccess"] != true || empty($com_aap[Yii::app()->session['userId']]["isAdmin"]) || $com_aap[Yii::app()->session['userId']]["isAdmin"] != "true")
                            && (empty($com_aap[Yii::app()->session['userId']]["roles"]) || count(array_intersect($forms[$v]["params"]["canRead"], $com_aap[Yii::app()->session['userId']]["roles"])) <= 0 )
                        ) {
                            $lockedStep = true;
                        } else {
                            $lockedStep = false;
                        }

                    } else {
                        if (   $answer["user"] != Yii::app()->session['userId']
                            && (empty($com_aap[Yii::app()->session['userId']]["isAdmin"]) || $com_aap[Yii::app()->session['userId']]["isAdmin"] != "true")
                        ) {
                            $lockedStep = true;
                        } else {
                            $lockedStep = false;
                        }
                        /*if (empty($com_aap[Yii::app()->session['userId']]["isAdmin"]) || $com_aap[Yii::app()->session['userId']]["isAdmin"] != "true") {
                            $lockedStep = true;
                        }else {
                            $lockedStep = false;
                        }*/
                    }
                }else{
                    $lockedStep = false;
                }
            }

            $hide = ($k == @$activeStep) ? "" : "hide";
            $lkdstep = "";
            if (!isset($parentForm["homeTpl"])) {
                $lkdstep = $lockedStep ? "lkdstep" : "";
            }
            $questionCol = ""; //col-sm-offset-1 col-sm-10
            ?>
            <div id='<?php echo $v; ?>' class='<?php $questionCol ?> sectionStep <?php echo $hide; ?> <?php echo $lkdstep; ?>' style="padding-bottom:40px" data-archi="block contenant les formulaires de chaques étapes">

                <script type="text/javascript">
                    formInputs["<?php echo $v ?>"] = <?php echo (!empty($forms[$v]['inputs']) ? json_encode($forms[$v]['inputs']) : json_encode([])) ?>;
                </script>

                <?php
                if (!empty($forms[$v]) && isset($forms[$v]['open']))
                {
                /*   echo '<div class="col-xs-12 title-header text-center"><h2 class="text-center sectionStepTitle"  style="margin-top:20px;margin-bottom:20px; color:' . $color1 . '" >' . @$forms[$v]["name"] . '</h2></div>';*/

                if( !empty($forms[$v]) && isset($forms[$v]['open'])     )
                {
                    echo '<div class="col-xs-12 title-header text-center"><h2 class="text-center sectionStepTitle"  style="margin-top:20px;margin-bottom:20px; color:'.$color1.'" >'.@$forms[$v]["name"].'</h2></div>';
                    echo '<div class="markdown" style="color:'.$color1.'" >'.@$forms[$parentForm["subForms"][0]]["info"].'</div>';

                    if (!empty($parentForm["type"]) && ($parentForm["type"] == "aap" || $parentForm["type"] == "aapConfig")) {
                        //var_dump($forms[$v]["params"]["haveReadingRules"]);
                        if (isset($forms[$v]["params"]["haveEditingRules"]) && $forms[$v]["params"]["haveEditingRules"] == "true") {
                            $com_aap = Element::getCommunityByParentTypeAndId($parentForm["parent"]);
                            foreach ($com_aap as $comid => $comd){
                                if (isset($comd["type"])) {
                                    if ($comd["type"] == "organizations" || $comd["type"] == "projects") {
                                        $subcom_aap = Element::getCommunityByTypeAndId($comd["type"], $comid);

                                        foreach ($subcom_aap as $subcomid => $subcomd) {
                                            $subroles = [];
                                            if (!empty($subcomd["roles"])) {
                                                $subroles = $subcomd["roles"];
                                            }
                                            if (!empty($comd["roles"])) {
                                                $subroles = array_merge($subroles, $comd["roles"]);
                                            }
                                            $com_aap[$subcomid]["roles"] = $subroles;
                                        }
                                    }
                                }

                            }
                            /*var_dump(isset($com_aap[Yii::app()->session['userId']]));
                            var_dump($com_aap);*/
                            if (!empty($forms[$v]["params"]["canEdit"]) && !is_array($forms[$v]["params"]["canEdit"])){
                                $forms[$v]["params"]["canEdit"] = explode("," , $forms[$v]["params"]["canEdit"]);
                            }
                            if (!empty($forms[$v]["params"]["canEdit"]) && $forms[$v]["params"]["canEdit"] != [""]) {
                                if (
                                    $answer["user"] != Yii::app()->session['userId']
                                    && (empty($forms[$v]["params"]["adminabsoluteaccess"] ) || $forms[$v]["params"]["adminabsoluteaccess"] != true || empty($com_aap[Yii::app()->session['userId']]["isAdmin"]) || $com_aap[Yii::app()->session['userId']]["isAdmin"] != "true")
                                    && (empty($com_aap[Yii::app()->session['userId']]["roles"]) || count(array_intersect($forms[$v]["params"]["canEdit"], $com_aap[Yii::app()->session['userId']]["roles"])) <= 0 )

                                ){
                                    $mode = "r";
                                }

                            } else {
                                if (
                                    $answer["user"] != Yii::app()->session['userId']
                                    && (empty($com_aap[Yii::app()->session['userId']]["isAdmin"]) || $com_aap[Yii::app()->session['userId']]["isAdmin"] != "true")

                                ){
                                    $mode = "r";
                                }
                                /*if (empty($com_aap[Yii::app()->session['userId']]["isAdmin"]) || $com_aap[Yii::app()->session['userId']]["isAdmin"] != "true") {
                                    $lockedStep = true;
                                }else {
                                    $lockedStep = false;
                                }*/
                            }
                        }
                    }

                    //echo "<div class='markdown'>";
                    echo "<div class=''>";
                    echo $this->renderPartial("survey.views.tpls.forms.formSection",
                        [
                            "contextId" => @$contextId,
                            "contextType" => @$contextType,
                            "parentForm"=>$parentForm,
                            "formId" => $v,
                            "form" => $forms[$v],
                            "subFs" => $forms,
                            "wizard" => true,
                            "answer"=>$answer,
                            "mode" => @$mode,
                            "showForm" => $showForm,
                            "canEdit" => $canEdit,
                            "canEditForm" => @$canEditForm,
                            "canAdminAnswer" => @$canAdminAnswer,
                            "el" => $el ] ,true );

                    $showNextButton = true;

                    if (isset($parentForm["showNextButton"]) && $parentForm["showNextButton"] == false)
                    {
                        $showNextButton = false;
                    }

                    if (empty($parentForm["type"]) || ($parentForm["type"] != "aap" && $parentForm["type"] != "aapConfig"))
                    {

                        if ($k < count($parentForm["subForms"]) - 1 && $showNextButton)
                        {
                            $nextFormId = $parentForm["subForms"][$k + 1];
                            if ($forms[$nextFormId]['open'])
                            {
                                /*                           echo '<a href="javascript:showStepForm(\'#' . $nextFormId . '\')" class="btn btn-primary col-xs-12" style="background-color:<?php echo ( $color2 ) ? $color2 : $defaultColor ?>"> ÉTAPE SUIVANTE </a>';*/
                            }
                        }
                    }
                    echo "</div>";
                }
                else
                {
                    echo "";
                }
                ?>
            </div>
        <?php
        } ?>
        </div>
        <?php  } ?>
    </div>
<?php }  else if( isset($parentForm["subForms"]) && count($parentForm["subForms"]) == 1 ) {
    //var_dump("coucou");exit;
    echo "<div class='col-xs-12'>";
    echo '<h2 class="text-center sectionStepTitle" style="color:'.$color1.'" >'.@$forms[$parentForm["subForms"][0]]["name"].'</h2><br>';
    echo '<div class="markdown" style="color:'.$color1.'" >'.@$forms[$parentForm["subForms"][0]]["info"].'</div>';
    if(!isset($preview) && $canEditForm === true){
        echo "<div class='text-center col-xs-12'>";
        echo '<a href="javascript:;" class="editFormBtn" data-id="' . $forms[$parentForm["subForms"][0]]["_id"] . '" data-formid="' . $parentForm["subForms"][0] . '"><i class="fa fa-pencil text-dark"></i></a> ';
        echo '<a href="javascript:;" class="deleteFormBtn" data-id="' . $forms[$parentForm["subForms"][0]]["_id"] . '" data-formid="' . $parentForm["subForms"][0] . '"><i class="fa fa-trash text-red"></i></a> ';
        echo "</div>";
    }

    //echo "<div class='markdown'>";
    echo "<div class=''>";
    echo $this->renderPartial("survey.views.tpls.forms.formSection", ["contextId" => @$contextId, "contextType" => @$contextType, "parentForm" => $parentForm, "formId" => $parentForm["subForms"][0], "form" => $forms[$parentForm["subForms"][0]], "wizard" => true, "answer" => $answer, "mode" => @$mode, "showForm" => $showForm, "canEdit" => $canEdit, "canEditForm" => @$canEditForm, "canAdminAnswer" => @$canAdminAnswer, "el" => $el], true);
    echo "</div>";
    echo "</div>";
}
else
{
    if ($canEditForm && (!isset($parentForm["type"]) || (isset($parentForm["type"]) && $parentForm["type"] != "aapConfig" && $parentForm["type"] != "aap")))
    {
        echo "<div class='col-xs-12 text-center'>";
        echo '<br/><h4 class="text-red text-center">ce formulaire est encore vide</h4>';
        echo "<br/><br/><a href='javascript:;' data-id='" . (String)$parentForm["_id"] . "' class='addStepBtn btn btn-danger bold'> <i class='fa fa-cogs'></i> Ajouter un formulaire </a>";
        echo "<br/><br/><i class='fa fa-5x fa-wpforms'></i>";
        echo "</div>";
    }
    else
    {
        echo "Ce formulaire est encore vide";
    }
}
?>
</div>

<script type="text/javascript">
    var formsData = <?php echo (!empty($forms)) ? json_encode($forms) : "null"; ?>;

    var formInputsHere = formInputs;

    var isNew = <?php echo (!empty($isNew) ? $isNew : "false") ?>;

    function showStep(id){
        $(".stepperContent").addClass("hide");
        $(id).removeClass("hide");
        //alert(location.hash.split(".")[0]+"."+location.hash.split(".")[1]+"."+location.hash.split(".")[2]+".subview."+$(".stepperContent:not(.hide").attr("id"));
        history.pushState(null, null, location.hash.split(".")[0]+"."+location.hash.split(".")[1]+"."+location.hash.split(".")[2]+".subview."+$(".stepperContent:not(.hide").attr("id"));
    }

    function showStepForm(id){
        if (!($(id).hasClass("lkdstep"))) {
            $(".stepDesc, .editFormBtn, .deleteFormBtn").addClass("hidden-xs");
            $(id + "-stepDesc, " + id + "-editFormBtn, " + id + "-deleteFormBtn").removeClass("hidden-xs");

            $("#explain").addClass("hide");
            $(".sectionStep").addClass("hide");
            $(id).removeClass("hide");
            localStorage.setItem("wizardStep_<?php echo (string)@$answer["_id"] ?>", id);
            var topScrollForm = ($("#customHeader").length > 0) ? $("#customHeader").offset().top : 0;
            $('html, body').animate({scrollTop: topScrollForm}, 500);
        }
    }

    var canShowStepList = <?php echo json_encode(@$canShowStepList) ?>;

    var haveProject = <?php echo ((!empty($parentForm["projectGeneration"]) && $parentForm["projectGeneration"] == "onStart") ? "true" : "false") ?>;

    var namepro = "<?php echo ((!empty($parentForm["projectGeneration"]) && $parentForm["projectGeneration"] == "onStart" && !empty($namemap[2])) ? $namemap[2] : "") ?>";

    var descrpro = "<?php echo ((!empty($parentForm["projectGeneration"]) && $parentForm["projectGeneration"] == "onStart" && !empty($descrmap[2])) ? $descrmap[2] : "") ?>";

    if(!notNull(answerObj)){
        var answerObj = <?php echo (!empty($answer) ? json_encode($answer) : "{}" ); ?>;
    }

    var idproans =  "<?php echo (!empty($answer["project"]["id"]) ? $answer["project"]["id"] : "null"); ?>";

    jQuery(document).ready(function() {

        $('#modeSwitch').hide();
        if (notNull(namepro) && namepro != "" && notNull(idproans) && idproans != ""){
            $("#"+namepro).unbind().blur( function( event ) {
                var tthis = $(this);
                event.preventDefault();
                //toastr.info('saving...'+$(this).attr("id"));
                //$('#openForm118').parent().children("label").children("h4").css('color',"green")
                if (notNull(answerObj)) {

                    var projectgn = {
                        collection: "projects",
                        id: idproans,
                        path: "name",
                        value :  $("#"+namepro).val()
                    };

                    dataHelper.path2Value( projectgn , function(params) {
                        toastr.success('Nom du projet associé modifié');
                    } );
                }
            });

            $("#"+descrpro).unbind().blur( function( event ) {
                var tthis = $(this);
                event.preventDefault();
                //toastr.info('saving...'+$(this).attr("id"));
                //$('#openForm118').parent().children("label").children("h4").css('color',"green")
                if (notNull(answerObj)) {

                    var projectgn = {
                        collection: "projects",
                        id: idproans,
                        path: "shortDescription",
                        value :  $("#"+descrpro).val()
                    };

                    dataHelper.path2Value( projectgn , function(params) {
                        toastr.success('Description courte du projet associé modifié');
                    } );
                }
            });
        }

        mylog.log("render","survey.views.tpls.forms.wizard");
        var urlPreview      = location.hash;
        mylog.log("urlPreview",urlPreview.indexOf("preview"));
        $.each($(".markdown"), function(k,v){
            descHtml = dataHelper.markdownToHtml($(v).html());
            $(v).html(descHtml);
        });

        showStepForm('#aapStep1');

        if ($("#explain").length && !($("#explain").hasClass("hide")) ){
            $(".sectionStep").addClass("hide");
        }


        $('.editFormBtn').off().click( function(){
            tplCtx.id = $(this).data("id");
            tplCtx.collection = "forms";
            dyFObj.openForm( subformDF , null, formsData[$(this).data("formid")] )
        });
        $('.deleteFormBtn').off().click( function(){
            tplCtx.id = $(this).data("id");
            bootbox.dialog({
                title: trad.confirmdelete,
                message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                buttons: [
                    {
                        label: "Ok",
                        className: "btn btn-primary pull-left",
                        callback: function() {
                            getAjax("",baseUrl+"/survey/form/delete/id/"+tplCtx.id,function(res){
                                if(res.result)
                                    toastr.success("Le form été supprimée avec succès");
                                else
                                    toastr.error(res.msg);
                                urlCtrl.loadByHash(location.hash);
                            },"html");
                        }
                    },
                    {
                        label: "Annuler",
                        className: "btn btn-default pull-left",
                        callback: function() {}
                    }
                ]
            });
        });
    });

    var subformDF = {
        jsonSchema : {
            title : "Configurer cette section",
            description : "Tout est possible, faut juste poser les bonnes questions",
            icon : "fa-question",
            properties : {
                name : { label : "Nom du formulaire"},
                info : { label : "Description",
                    inputType: "textarea",
                    markdown: true
                }
            },
            save : function (formData) {
                mylog.log('save tplCtx formData', formData)

                delete formData.collection ;
                tplCtx.path = "allToRoot";
                tplCtx.value = {};
                tplCtx.formParentId = idParentForm;
                tplCtx.value.name = formData.name;
                tplCtx.value.info = formData.info;

                mylog.log("save tplCtx",tplCtx);

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) {
                        urlCtrl.loadByHash(location.hash);
                    } );
                }

            }
        }
    };
</script>
