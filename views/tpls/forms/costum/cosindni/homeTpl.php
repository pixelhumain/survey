<?php
$paramsData = [
    "homeTplAap" => " Vous êtes salarié, bénévole, prestataire sur une fonction de gestion de tiers-lieu, ce rapide sondage est fait pour vous !<br><br>


                🧾  Cette enquête vise à établir un état des lieux de l’emploi et des contributions en tiers-lieu en 2022 pour donner à voir l’évolution du secteur, adapter les modalités d’accompagnement des structures employeuses et les besoins en formation des salariés / bénévoles<br><br>


                📨  La Coopérative Tiers-Lieux, La Compagnie des Tiers-Lieux, Cap Tiers-Lieux Pays de la Loire, le consortium Île de France Tiers-Lieux, Illusion et Macadam, Bretagne Tiers-Lieux et le Réseau Sud Tiers-Lieux, auront accès aux réponses de ce questionnaire. Les données que vous nous transmettez seront traitées de manière anonyme.<br><br>


                ⌛️ Nous vous remercions de prendre le temps de répondre  à ce sondage. Attention, prévoyez bien 15 minutes pour le remplir et ne laisser pas la page inactive plus de 5 minutes sans quoi vos réponses s'auto-détruiront instantanément.<br><br>
"
];
?>

<style type="text/css">
    .elformdescription.markdown{
        font-size: initial;
        font-weight: initial;
        padding: 0 11px;
    }
    .markdown {
        white-space: initial;
    }
</style>

<div id='explain' class='col-sm-offset-1 col-sm-10 stepperContent ' style='margin-top:30px; padding-bottom:40px'>
    <div class='col-xs-12 text-center'>
        <a href="javascript:showStepForm('#aapStep1')" class="btn btn-primary">C'est parti</a>
    </div>
    <div class="text-body elformdescription markdown">
        <?php
            echo $paramsData["homeTplAap"];
        ?>
    </div>
</div>