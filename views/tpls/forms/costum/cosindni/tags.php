<?php
$value = (!empty($answers)) ? ' value="'.@$answers.'" ' : '';

$inpClass = "form-control";

$allAssoc = PHDB::distinct(Form::ANSWER_COLLECTION , "answers.aapStep1.association" , array("form" => (string)$parentForm["_id"],"answers.aapStep1.titre"=>['$exists'=>true]));
$allAssoc = array_map(function($val){
    return mb_strtoupper(trim($val),'UTF-8');

},$allAssoc);

$allAssoc = array_values(array_unique($allAssoc));
$paramsData = ["list" => []];

$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='" . $parentForm["_id"] . "' data-collection='" . Form::COLLECTION . "' data-path='params." . $kunik . "' class='previewTpl edit" . $kunik . "Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

if (isset($parentForm["params"][$kunik]["list"])) $paramsData["list"] = $parentForm["params"][$kunik]["list"];

$properties = ["list" => ["placeholder" => "Groupé", "inputType" => "array" , "rules" => ["required" => true]] ];

/*$allAnswerDoc = Document::getListDocumentsWhere(array(
    "id"=>$uploaderObj["contextId"],
    "type"=>$uploaderObj["contextType"],
    "subKey"=>$uploaderObj["params"]["subKey"]), "file");*/
$uploaderObj = [
        "docType" => "file",
          "paste" => true,
          "itemLimit" => 10
    ];

$uploaderObj["contextId"]=(string)$answer["_id"];
$uploaderObj["contextType"]=Form::ANSWER_COLLECTION;

if($mode == "r" || $mode == "pdf"){ ?>
    <div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
        <label for="<?php echo $kunik ?>">
            <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
                <?php echo $label ?>
            </h4>
        </label><br/>
        <?php echo $answers; ?>
    </div>
    <?php
}else{
    ?>
    <div class="col-md-12 no-padding">
        <div class="form-group">
            <label for="<?php echo $key ?>">
                <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
                    <?php echo $label.$editQuestionBtn.$editParamsBtn ?>
                </h4>
            </label>
            <br/>
            <input type="<?php echo (!empty($type)) ? $type : 'text' ?>"
                   class="<?php echo $inpClass ?>"
                   id="<?php echo $key ?>"
                   aria-describedby="<?php echo $key ?>Help"
                   placeholder="<?php echo (isset($placeholder)) ? $placeholder : '' ?>"
                   data-form='<?php echo $form["id"] ?>'  <?php echo @$value ?>
            />
            <?php if(!empty($info)){ ?>
                <small id="<?php echo $key ?>Help" class="form-text text-muted">
                    <?php echo $info ?>
                </small>
            <?php } ?>
        </div>

    </div>

<?php } ?>

<?php if($mode != "pdf"){ ?>
    <script type="text/javascript">
        var allAssoc = <?php echo json_encode($allAssoc) ?>;
        var <?php echo $kunik ?>Data = <?php echo json_encode((isset($answers)) ? $answers : null); ?>;
        var <?= $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;

        var forminputs = <?php echo json_encode($form["inputs"]) ?>;
        var parentForm<?=$key?> = <?php echo json_encode($parentForm) ?>;
        function aapAutofill(name){
            ajaxPost(
                null,
                baseUrl + '/co2/aap/searchanswer',
                {
                   "key" : name,
                   "path" : "answers.aapStep1.association",
                   "collection" : "answers"
                },
                function(data){
                    if(data.result){
                        bootbox.confirm({
                            message: "Completer le reste des champs ?",
                            buttons: {
                                confirm: {
                                    label: trad.yes,
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: trad.no,
                                    className: 'btn-danger'
                                }
                            },
                            callback: function (result) {
                                if (!result) {
                                    return;
                                } else {
                                    ajaxPost(
                                        null,
                                        baseUrl + '/co2/aap/copyfieldsfromfield',
                                        {
                                            "answersSource" : data.data._id.$id,
                                            "answersTarget" : "<?= (string) $answer["_id"] ?>",
                                            "form" : parentForm<?=$key?>._id.$id
                                        },
                                        function(data){
                                            if(data.result){
                                                toastr.success(trad.saved);
                                                $.each( <?= $kunik ?>ParamsData.list , function (index, value) {
                                                    setTimeout(() => {
                                                        //reloadInput(value, "<?= @$form["id"] ?>");
                                                        reloadInput(value, "<?= @$form["id"] ?>", () => stepObj.bindEvent.inputEvents(stepObj))
                                                    }, 900);
                                                })
                                            }
                                        }
                                    )
                                }
                            }
                        });
                    }
                }
            );

        }

        jQuery(document).ready(function() {
            $( "#<?php echo $key ?>" ).autocomplete({
                source: allAssoc ,
                select: function (event, ui) {
                    aapAutofill(ui.item.value);
                }
            });

            $("#<?php echo $key ?>").blur( function( event ) {
                var tthis = $(this);
                event.preventDefault();
                //toastr.info('saving...'+$(this).attr("id"));
                //$('#openForm118').parent().children("label").children("h4").css('color',"green")
                if( notNull(answerObj)){

                    var answer = {
                        collection : "answers",
                        id : answerObj._id.$id,
                        path : "answers."+$(this).attr("id")
                    };

                    if(answerObj.form)
                        answer.path = "answers."+$(this).data("form")+"."+$(this).attr("id");

                    answer.value = $(this).val().trim();

                    mylog.log("saveOneByOne",$(this).attr("id"), answer );

                    setTimeout(
                    dataHelper.path2Value( answer , function(params) {
                        toastr.success('Mise à jour enregistrée');
                        saveLinks(answerObj._id.$id,"updated",userId);
                        if(typeof newReloadStepValidationInputGlobal != "undefined") {
                            newReloadStepValidationInputGlobal({
                                inputKey : tthis.attr("id"),
                                inputType : "tpls.forms.costum.cosindni.tags"
                            })
                        }
                        if (typeof stepValidationReload<?php echo @$formId?> !== "undefined") {
                            stepValidationReload<?php echo @$formId?>();
                        }
                    } ) , 2000);


                } else {
                    toastr.error('answer cannot be empty, on saveOneByOne!');
                }
            });

            sectionDyf.<?php echo $kunik ?>Params = {
                jsonSchema : {
                    title : "<?php echo $kunik ?> config",
                    description : "",
                    icon : "fa-cog",
                    properties : {
                        list : {
                            inputType : "array",
                            label : "Liste des champs",
                            values :  <?= $kunik ?>ParamsData.list.list
                        },
                    },
                    save : function () {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                            if(val.inputType == "properties")
                                tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                            else if(val.inputType == "array")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else
                                tplCtx.value[k] = $("#"+k).val();
                        });
                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                dyFObj.closeForm();
                                reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
                            } );
                        }

                    }
                }
            };

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, <?= $kunik ?>ParamsData.list);
            });

        });
    </script>
<?php } ?>

