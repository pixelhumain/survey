<?php if($isNew){
    $baseurl = (!empty($el["costum"])) ? Yii::app()->getBaseUrl(true) . "/costum/co/index/slug/" . $el["slug"] : Yii::app()->getBaseUrl(true);
    ?>
    <script>
        $(function(){
            let userAnswers = <?= json_encode($userAnswers)?>;
            let oneresponse = <?= isset($parentForm["oneAnswerPerPers"]) && $parentForm["oneAnswerPerPers"] ? $parentForm["oneAnswerPerPers"] : "false" ?>;
            let addResponseBtn = "";
            if (!oneresponse){
                addResponseBtn = `<button class="btn btn-success btn-block" id="new-anwers-popup"><i class="fa fa-plus"></i>${ucfirst(trad["Generate a new answer"])}</button>`;
            }
            let dialogNewAns = bootbox.dialog({
                message: `<div class="">
                            <p>${tradForm['Please check on the list below if you have already added an answer. If not, click on button below']}</p>
                            <table class="table">
                                <tbody id="user-exists-answers" style="font-size:15px">

                                </tbody>
                            </table>
                            ${addResponseBtn}
                        </div>`,
                closeButton: false
            });
            dialogNewAns.on('shown.bs.modal', function(e){
                dialogNewAns.attr("id", "new-anwers-dialog");
                $("#new-anwers-dialog").css("background-color","rgb(0 0 0 / 54%)");
                var html = '';
                var i = 1;
                $.each(userAnswers,function(k,v){
                    v.customTitle = "";
                    if(exists(v.answers) && exists(v.answers.aapStep1) && exists(v.answers.aapStep1.titre))
                        v.customTitle = "<b>"+ucfirst(v.answers.aapStep1.titre)+"</b> /";
                    html+= `<tr id="ans-${k}">
                                <td>
                                    ${i+"-  "}
                                </td>
                                <td>
                                    <a href="javascript:;" onclick="window.open('${location.href.split("#")[0]}/#answer.index.id.${k}.mode.w.standalone.true')" >${v.customTitle} ${ucfirst(moment.unix(v.created).format('dddd,Do MMMM YYYY h:mm:ss A'))} </a>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-xs btn-danger delete-answer-popup" data-id="${k}"><i class="fa fa-times"></i></button>
                                </td>
                            </tr>`;
                    i++;
                })
                html += '';
                $('#user-exists-answers').html(html);
                $("#new-anwers-popup").off().on("click",function(){
                    window.open(location.href+'.ask.false',"_self")
                    location.reload();
                });
                $(".delete-answer-popup").off().on('click',function(){
                    var idans = $(this).data("id");
                    bootbox.dialog({
                        title: trad.confirm,
                        message: "<span class='text-red bold'><i class='fa fa-warning'></i>"+trad.actionirreversible+"</span>",
                        buttons: [{
                            label: "Ok",
                            className: "btn btn-primary pull-left",
                            callback: function () {
                                getAjax("", baseUrl + "/survey/co/delete/id/" + idans, function (data) {
                                    if (data.result) {
                                        toastr.success(trad.deleted);
                                        $("#ans-"+idans).hide(500)
                                    }
                                }, "html");


                            }
                        },
                        {
                            label: "Annuler",
                            className: "btn btn-default pull-left",
                            callback: function () { }
                        }
                        ]
                    });
                })
            });
        })
    </script>
<?php }else{ ?>
    <style>
        .bg-contain {
            /*background-color: #4ecdc4;*/
            background-image: url("<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/coSindniSmarterre/saint_denis.jpg");
            background-size: cover;
            background-attachment:fixed;
            min-height: 100vh;
        }
        .contain-offset {
            border: 1px solid #CCCCCC;
            padding: 0;
            min-height: 100px;
            margin-top: 10px;
            padding-bottom: 5px;
            background-color: #fff;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
            -webkit-box-shadow: 5px 5px 10px #000;
            -moz-box-shadow: 5px 5px 10px #000;
            box-shadow: 5px 5px 10px #000;
        }
        .banner-section {
            padding: 20px;
        }
        .body-section, .footer-section {
            padding: 30px 15px;
        }
        .text-body {
            font-size: 20px;
        }
        .header-title {
            text-transform: none;
            word-wrap: break-word;
            font-size: 48px;
        }
        .brands {
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
        }

        .brands__item {
            flex: 0 0 50%;
            list-style: none;
            text-align: center;
        }
        .brands__item img {
            width: 130px;
            height: 75px;
            object-fit: contain;
            mix-blend-mode: multiply;
        }

        @media (min-width: 700px) {
            .brands__item {
                flex: 0 0 33.33%;
            }
        }

        @media (min-width: 1100px) {
            .brands__item {
                flex: 0 0 25%;
            }
        }
        @media (min-width: 768px) and (max-width: 991px) {
            .header-title {
                font-size: 17px;
            }
        }
        @media (max-width: 767px) {
            .header-title {
                font-size: 17px;
            }
            .text-body {
                font-size: 14px;
            }
            .body-section, .footer-section {
                padding: 10px 5px;
            }
        }

        /* formWizard */
        @font-face{
            font-family: "montserrat";
            src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/montserrat-regular.woff") format("woff"),
            url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/montserrat-regular.ttf") format("ttf")
        }.mst{font-family: 'montserrat'!important;}

        @font-face{
            font-family: "CoveredByYourGrace";
            src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/CoveredByYourGrace.ttf")
        }.cbyg{font-family: 'CoveredByYourGrace'!important;}


        .switchTopButton{
            position: fixed;
            /*padding: 5px;*/
            right:-3px;
            font-size: 18px;

            border-radius: 20%;
            z-index:1;
        }

        .switchTopButton a, .switchTopButton span{
            font-size: 20px
        }


        #customHeader{
            margin-top: 0px;
        }
        #costumBanner{
            /* max-height: 375px; */
        }
        #costumBanner h1{
            position: absolute;
            color: white;
            background-color: rgba(0,0,0,0.4);
            font-size: 29px;
            bottom: 0px;
            padding: 20px;
        }
        #costumBanner h1 span{
            color: #eeeeee;
            font-style: italic;
        }
        #costumBanner img{
            min-width: 100%;
        }
        .btn-main-menu{
            background: <?php echo @$this->costum["colors"]["pink"]; ?>;
            border-radius: 10px;
            padding: 10px !important;
            color: white;
            cursor: pointer;
            border:3px solid transparent;
            font-size: 1.5em
            /*min-height:100px;*/
        }
        .btn-main-menuW{
            background: white;
            color: <?php echo @$this->costum["colors"]["pink"]; ?>;
            border:none;
            cursor:text ;
        }
        .btn-main-menu:hover{
            border:2px solid <?php echo @$this->costum["colors"]["pink"]; ?>;
            background-color: white;
            color: <?php echo @$this->costum["colors"]["pink"]; ?>;
        }
        .btn-main-menuW:hover{
            border:none;
        }
        @media screen and (min-width: 450px) and (max-width: 1024px) {
            .logoDescription{
                width: 60%;
                margin:auto;
            }
        }

        @media (max-width: 1024px){
            #customHeader{
                margin-top: -1px;
            }
        }
        #customHeader #newsstream .loader{
            display: none;
        }

        .mr-4{
            margin-right: 1em !important;
        }
        /* by nicoss */
        #modeSwitch .btn-primary{
            position: relative;
            vertical-align: center;
            margin: 0px;
            height: 100x;
            padding: 10px 14px;
            font-size: 14px;
            color: white;
            text-align: center;
            text-shadow: 0 3px 2px rgba(0, 0, 0, 0.3);
            background: #62b1d0;
            border: 0;
            border-bottom: 3px solid #9FE8EF;
            cursor: pointer;
            -webkit-box-shadow: inset 0 -3px #9FE8EF;
            box-shadow: inset 0 -3px #9FE8EF;
            margin-left: 5px;
        }

        #modeSwitch .btn-primary:active {
            top: 2px;
            outline: none;
            -webkit-box-shadow: none;
            box-shadow: none;
            color: #354C57;
            border-bottom: 3px solid #354C57;
        }
        #modeSwitch .btn-primary:hover {
            background: #45E1E8;
            color: #354C57;
            border-bottom: 3px solid #354C57;
        }

        .monTitle{
            border-top: 1px dashed <?php echo @$this->costum["colors"]["pink"]; ?>;
            border-bottom: 1px dashed <?php echo @$this->costum["colors"]["pink"]; ?>;
            /*margin-top: -20px;*/
        }

    </style>
    <script type="text/javascript">
        var sectionDyf = {};
    </script>

    <?php
    $bannerTitleStandalone = @$form["name"];
    $bannerFooterStandalone = "<p style='font-size:23px'>".ucfirst(Yii::t("common", "the holder"))." : <a href='#page.type.".$el["collection"].".id.".(string)$el["_id"]."' class=' lbh-preview-element'>".ucfirst($el["name"])."</a></p>";
    if (isset($blockCms["bannerTitleStandalone"])){
        $bannerTitleStandalone = $blockCms["bannerTitleStandalone"];
    }

    $cssJS = array(
        '/plugins/jQuery-Knob/js/jquery.knob.js',
        '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
        //'/plugins/jQuery-Smart-Wizard/styles/smart_wizard.css',
        // SHOWDOWN
        '/plugins/showdown/showdown.min.js',
        // MARKDOWN
        '/plugins/to-markdown/to-markdown.js'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);
    HtmlHelper::registerCssAndScriptsFiles(array(
        '/js/answer.js',
    ), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );

    $poiList = array();

    if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
        $poiList = PHDB::find(Poi::COLLECTION,
            array( "parent.".$this->costum["contextId"] => array('$exists'=>1),
                "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"],
                "type"=>"cms") );
    }
    ?>

    <div class="col-xs-12 bg-contain">
        <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-1 contain-offset no-padding">
            <div class="banner-section">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                        <h1 class="header-title"><?= $bannerTitleStandalone  ?></h1>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12" >
                        <img class="img-responsive" src="<?php echo Yii::app()->getModule("survey")->assetsUrl ?>/images/aap_banner2.jpg" alt="Book Icon">
                    </div>
                </div>
            </div>
            <hr style = "margin: 0">

            <div class="body-section">

                <div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">

                    <?php
                    $color1 = "#E63458";
                    if(isset($this->costum["cms"]["color1"]))
                        $color1 = $this->costum["cms"]["color1"];
                    // if($canEdit)
                    //   echo "<a class='btn btn-xs btn-danger editBtn' href='javascript:;' data-key='color1' data-type='color'  data-path='costum.cms.color1' data-label='Couleur Principale '><i class='fa fa-pencil'></i></a>";
                    ?>

                    <?php $formSmallSize =  12; ?>
                    <div class="col-md-12 col-lg-<?php echo $formSmallSize?> no-padding "><br/>

                        <script type="text/javascript">
                            var formInputs = {};
                            var answerObj = <?php echo (!empty($answer)) ? json_encode( $answer ) : "null"; ?>;
                            var form = <?php echo (!empty($form)) ? json_encode( $form ) : "null"; ?>;
                        </script>

                        <div class="col-xs-12 margin-top-20 coFormbody">
                            <?php
                            $wizardUid = (String) $form["_id"];
                            if($canEdit === true && $mode != "fa" && empty($answer["validated"]) ){
                                $nameMode = "mode read";
                                if($mode == "w")
                                    $nameMode = "mode write";

                                echo '<div class="col-xs-12 margin-bottom-15" id="modeSwitch" ></div>';
                                echo '<div id="bottomModeSwitch" class="switchTopButton" style="bottom:50%"></div>';

                                echo '<div id="arrowTop" class="switchTopButton" style="bottom:5%"></div>';

                            }

                            if($mode != "fa" && !empty($parentForm["answersTpl"])){
                                $params = [
                                    "parentForm"=>$parentForm,
                                    "el" => $el,
                                    "color1" => $color1,
                                    "canEdit" => $canEdit,
                                    "answer"=>$answer,
                                    "forms"=>$forms,
                                    "allAnswers"=>@$allAnswers,
                                    "what" => "dossiers",
                                    "wizid"=> $wizardUid
                                ];
                                echo $this->renderPartial($parentForm["answersTpl"],$params);
                            }

                            if( $mode == "fa" && $canEditForm === true ){
                                $params = [
                                    "parentForm"=>$parentForm,
                                    "canEditForm"=>$canEditForm,
                                    "mode" => $mode,
                                    "form" => $form,
                                    "el" => $el
                                ];
                                //echo $this->renderPartial("survey.views.tpls.forms.config",$params);
                            }
                            ?>
                            <div id="wizardcontainer">
                                <?php
                                if( isset($answer) && !empty($showForm) && $showForm === true  ) {
                                    //var_dump($canEditForm);exit;
                                    $params = [
                                        "parentForm"=>$parentForm,
                                        "form" => $form, //identicall to parentForm kept on refactor
                                        "forms"=>$forms,
                                        "el" => $el,
                                        "active" => "all",
                                        "color1" => @$this->costum["colors"]["dark"],
                                        "color2" => @$this->costum["colors"]["pink"],
                                        "canEdit" => $canEdit,
                                        "canEditForm" => $canEditForm,
                                        "canAdminAnswer" => $canAdminAnswer,
                                        "answer"=>$answer,
                                        "showForm" => $showForm,
                                        "mode" => $mode,
                                        "showWizard"=>true,
                                        "wizid"=> $wizardUid,
                                        "isNew" => @$isNew,
                                        "contextId" => @$contextId,
                                        "contextType" => @$contextType
                                    ];
                                    $tplstepWizard = "";

                                    if (isset($parentForm["tplstepWizard"])){
                                        $tplstepWizard = $parentForm["tplstepWizard"];
                                    }else{
                                        $tplstepWizard = "survey.views.tpls.forms.wizard";
                                    }
                                    echo $this->renderPartial($tplstepWizard , $params);
                                }
                                ?>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="col-xs-12">
                    <?= $bannerFooterStandalone  ?>
                </div>
                <!--<hr style = "margin: 0">

                <div class="footer-section">
                    <ul class="brands ">
                        <li class="brands__item">
                            <img src="<?php /*echo Yii::app()->getModule("costum")->assetsUrl */?>/images/franceTierslieux/logo-02.png" alt="" />
                        </li>
                        <li class="brands__item">
                            <img src="<?php /*echo Yii::app()->getModule("costum")->assetsUrl */?>/images/franceTierslieux/bannerFTL.png" alt="" />
                        </li>
                        <li class="brands__item">
                            <img src="<?php /*echo Yii::app()->getModule("costum")->assetsUrl */?>/images/franceTierslieux/Modèle de gestion.png" alt="" />
                        </li>
                        <li class="brands__item">
                            <img src="<?php /*echo Yii::app()->getModule("costum")->assetsUrl */?>/images/franceTierslieux/baner.png" alt="" />
                        </li>
                        <li class="brands__item">
                            <img src="<?php /*echo Yii::app()->getModule("costum")->assetsUrl */?>/images/franceTierslieux/cartographie.png" alt="" />
                        </li>
                        <li class="brands__item">
                            <img src="<?php /*echo Yii::app()->getModule("costum")->assetsUrl */?>/images/franceTierslieux/Services.png" alt="" />
                        </li>
                        <li class="brands__item">
                            <img src="<?php /*echo Yii::app()->getModule("costum")->assetsUrl */?>/images/franceTierslieux/Réseau associé.png" alt="" />
                        </li>
                    </ul>
                </div>-->
            </div>




            <hr>

        </div>
    </div>

    <script type="text/javascript">
        //var sectionDyf = {};
        //to edit costum page pieces
        var configDynForm = <?php echo json_encode(@$this->costum['dynForm']); ?>;
        var answerId = <?php echo json_encode((String)$answer['_id']); ?>;
        var mode = <?php echo json_encode($mode); ?>;
        var canAdminAnswer = <?php echo json_encode($canAdminAnswer); ?>;
        var elTest = <?php echo json_encode($el); ?>;
        //information and structure of the form in this page
        var tplCtx = {};
        if (typeof costum!="undefined" && notNull(costum)!=false && typeof costum.app !="undefined" && typeof costum.app[location.hash]!="undefined" && typeof costum.app[location.hash].hash!="undefined" && typeof costum.app[location.hash].urlExtra!="undefined"){
            var fullUrl=  costum.app[location.hash].hash + costum.app[location.hash].urlExtra;
            fullUrl=fullUrl.replace(/[/]/g,".");

            if(fullUrl.indexOf("#answer.index.id.new")>=0){
                history.replaceState(location.hash, "", "#answer.index.id."+answerId+".mode."+mode+".standalone.true");
            }
        }

        if(location.hash.indexOf("#answer.index.id.new")>=0){
            history.replaceState("#answer.index.id.new", "", "#answer.index.id."+answerId+".mode."+mode+".standalone.true");
        }
        var strListMode = "";
        var strLowListMode = "";
        var strArrowTop = "";

        function reloadWizard(callback = null){
            coInterface.showLoader("#wizardcontainer");
            var reloadWizardData = {
                "answerId" : "<?php echo (string)$answer["_id"] ?>",
                "formId" : "<?php echo (string)$form["_id"] ?>",
                "forms": <?php echo json_encode($forms) ?>,
                "active" : <?php echo json_encode("all") ?>,
                "color1" : <?php echo json_encode(@$this->costum["colors"]["dark"]) ?>,
                "color2" : <?php echo json_encode(@$this->costum["colors"]["pink"]) ?>,
                "canEdit" : <?php echo json_encode($canEdit) ?>,
                "canEditForm" : <?php echo json_encode($canEditForm) ?>,
                "canAdminAnswer" : <?php echo json_encode($canAdminAnswer) ?>,
                "showForm" : <?php echo json_encode($showForm) ?>,
                "mode" : <?php echo json_encode($mode) ?>,
                "showWizard": true,
                "wizid": <?php echo json_encode($wizardUid) ?>,
                "contextId" : <?php echo json_encode(@$contextId) ?>,
                "contextType" : <?php echo json_encode(@$contextType) ?>,
                "standalone" : true
            }

            if (reloadWizardData.contextType == "") {
                delete reloadWizardData.contextType;
            }
            if (reloadWizardData.contextId == "") {
                delete reloadWizardData.contextId;
            }

            ajaxPost(
                "#wizardcontainer",
                baseUrl+"/survey/answer/reloadwizard",
                reloadWizardData,
                function(){
                    if(typeof callback == "function")
                        callback();
                }
            ,"html");
        }

        jQuery(document).ready(function() {
            mylog.log("render","modules/survey/views/tpls/forms/formWizard.php");


            if(typeof pageProfil != "undefined" && typeof pageProfil.form != "undefined" && pageProfil.form != null){

                if(mode == "w")
                    strListMode = '<a href="javascript:;" data-id="'+answerId+'" data-mode="r" data-form="'+form._id.$id+'" class="btnAnswer btn btn-primary pull-right"><i class="fa fa-eye"></i> Lecture Seule</a>';
                if(mode == "r")
                    strListMode = '<a href="javascript:;" data-id="'+answerId+'" data-mode="w" data-form="'+form._id.$id+'" class="btnAnswer btn btn-primary pull-right">Modifier</a>';
                if(canAdminAnswer){
                    strListMode += '<a href="javascript:;" data-form="'+form._id.$id+'" data-id="'+form._id.$id+'" class="btnForm btn btn-primary pull-right"><i class="fa fa-cog"></i> Configurer</a>';
                }
            } else {
                strArrowTop= '<a class="topButton btn btn-primary tooltips"><i class="fa fa-chevron-up" data-toggle="tooltip" data-placement="left" data-original-title="Début du formulaire"></i></a>';
                if(mode == "w"){
                    strListMode = '<a href="#answer.index.id.'+answerId+'.mode.r" class="lbh btn btn-primary pull-right">Lecture Seule</a>';
                    strLowListMode = '<a href="#answer.index.id.'+answerId+'.mode.r" class="lbh btn btn-primary tooltips" data-toggle="tooltip" data-placement="left" data-original-title="Lecture seule"><i class="fa fa-eye"></i></a>';
                }
                if(mode == "r"){
                    strListMode = '<a href="#answer.index.id.'+answerId+'.mode.w" class="lbh btn btn-primary pull-right"><i class="fa fa-pencil"></i> Modifier</a>';
                    strLowListMode = '<a href="#answer.index.id.'+answerId+'.mode.w" class="lbh btn btn-primary tooltips" data-toggle="tooltip" data-placement="left" data-original-title="Modifier"><i class="fa fa-pencil"></i></a>';
                }
            }

            <?php // if (empty($parentForm["type"]) || ($parentForm["type"] == "aap" && $parentForm == "aapConfig")){ ?>
            $("#modeSwitch").html(strListMode);

            if(strLowListMode!="" && strArrowTop!=""){
                $(document).on('scroll',function(){
                    var scrollTop = $(window).scrollTop();
                    if (scrollTop > 100) {
                        $("#bottomModeSwitch").html(strLowListMode);
                        coInterface.bindLBHLinks();
                    }
                    else{
                        $("#bottomModeSwitch").empty();
                    }

                    if(scrollTop > 500){
                        $("#arrowTop").html(strArrowTop);
                        coInterface.bindLBHLinks();
                    }else{
                        $("#arrowTop").empty();
                    }

                });
            }
            <?php // } ?>

            $("#arrowTop").off().on("click",function(){
                scrollintoDiv("wizardLinks",2000);
            });


            if(typeof pageProfil != "undefined" && typeof pageProfil.form != "undefined" && pageProfil.form != null){
                pageProfil.form.events.answers(pageProfil.form);
            }
            if(location.href.indexOf(".ask.false")){
                lhref = location.href.split(".ask.false")[0];
                history.pushState(null,null,lhref);
            }
        });
    </script>
<?php } ?>
