<style type="text/css">

.rdo-grp label {
  cursor: pointer;
  -webkit-tap-highlight-color: transparent;
  padding: 6px 8px;
  border-radius: 20px;
  float: left;
  transition: all 0.2s ease;
}


.input-grp label span:first-child {
  position: relative;
  display: inline-block;
  vertical-align: middle;
  width: 20px;
  height: 20px;
  background: #e8eaed;
  /*background: rgba(125,100,247,0.06);*/
  border-radius: 50%;
  transition: all 0.2s ease;
  margin-right: 8px;
}

.multiinput :hover {
  background: rgba(125,100,247,0.06);
}

.effect-2{border: 0; padding: 7px 0; border-bottom: 2px solid #ccc;}

.effect-2 ~ .focus-border{position: absolute; bottom: 0; left: 0; width: 0; height: 2px; background-color: #3399FF; transition: 0.4s;}
.effect-2:focus ~ .focus-border{width: 100%; transition: 0.4s;}

.paramsonebtn , .paramsonebtnP {
	font-size: 17px;
	display: none;
	padding: 5px;
}

.paramsonebtn:hover {
	color: red;
}

.paramsonebtnP:hover {
	color: blue;
}

.multiinput {
	display: flex;
    border-radius: 15px;
}

.multiinput:hover .paramsonebtn, .multiinput:hover .paramsonebtnP {
	display: inline-block;
}

.effect-2:focus {
	outline: none !important;
}

/*azzaz*/

.validate-input {
  position: relative;
}

.alert-validate::before {
	z-index: 10;
  content: attr(data-validate);
  position: absolute;
  max-width: 70%;
  background-color: #fff;
  border: 1px solid #c80000;
  border-radius: 2px;
  padding: 4px 30px 4px 10px;
  bottom: calc((100% - 100px) / 2);
  -webkit-transform: translateY(50%);
  -moz-transform: translateY(50%);
  -ms-transform: translateY(50%);
  -o-transform: translateY(50%);
  transform: translateY(50%);
  right: 32px;
  pointer-events: none;

  font-family: Poppins-Medium;
  color: #c80000;
  font-size: 14px;
  line-height: 1.4;
  text-align: left;

  visibility: hidden;
  opacity: 0;

  -webkit-transition: opacity 0.4s;
  -o-transition: opacity 0.4s;
  -moz-transition: opacity 0.4s;
  transition: opacity 0.4s;
}

.alert-validate::after {
	z-index: 10;
  content: "\f06a";
  font-family: FontAwesome;
  display: block;
  position: absolute;
  color: #c80000;
  font-size: 18px;
  bottom: calc((100% - 100px) / 2);
  -webkit-transform: translateY(50%);
  -moz-transform: translateY(50%);
  -ms-transform: translateY(50%);
  -o-transform: translateY(50%);
  transform: translateY(50%);
  right: 38px;
}

.alert-validate:hover:before {
  visibility: visible;
  opacity: 1;
}

@media (max-width: 992px) {
  .alert-validate::before {
    visibility: visible;
    opacity: 1;
  }
}

.true-validate::after {
	z-index: 10;
  content: "\f26b";
  font-family: Material-Design-Iconic-Font;
  font-size: 22px;
  color: #00ad5f;
  display: -webkit-box;
  display: -webkit-flex;
  display: -moz-box;
  display: -ms-flexbox;
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  bottom: calc((100% - 100px) / 2);
  -webkit-transform: translateY(50%);
  -moz-transform: translateY(50%);
  -ms-transform: translateY(50%);
  -o-transform: translateY(50%);
  transform: translateY(50%);
  right: 5px;
}

.responsemultitext:before{
  content: '\f0a9';
  margin-right: 15px;
  font-family: FontAwesome;
  color: #d9534f;
}

</style>

<?php 
  $me = isset(Yii::app()->session['userId']) ? Person::getById(Yii::app()->session['userId']) : null;

  if (!empty($parentForm["params"][$kunik]["validation"])) {

      if ($parentForm["params"][$kunik]["validation"]=="autocomplete") {
        $value = (!empty($parentForm["params"][$kunik]["autocomplete"])) ? 

            "value='".$me[$parentForm["params"][$kunik]["autocomplete"]]."'" 
            : 
            "";

      }else{
        $value = (!empty($answer["answers"][$form["id"]][$kunik])) ? 
         " value='".$answer["answers"][$form["id"]][$kunik]."' " 
         : 
         "";
      }
        
  }else{
      $value = (!empty($answer["answers"][$form["id"]][$kunik])) ? " value='".$answer["answers"][$form["id"]][$kunik]."' " : "";
  }

     


/*echo '$answer'; var_dump($answer["answers"][$form["id"]]);
echo '$saveOneByOne'; var_dump($saveOneByOne);
echo '$label'; var_dump($label);
echo '$placeholder'; var_dump($placeholder);
echo '$editQuestionBtn'; var_dump($editQuestionBtn);
echo '$key'; var_dump($key);
echo '$kunik'; var_dump($kunik);
echo '$form["id"]'; var_dump($form["id"]);
echo '$parentForm["_id"]'; var_dump($parentForm["_id"]);
echo 'Form::COLLECTION'; var_dump(Form::COLLECTION);*/

//var_dump(Yii::app()->session["user"]["name"]);
//$me = isset(Yii::app()->session['userId']) ? Person::getById(Yii::app()->session['userId']) : null;

//var_dump($me);

//var_dump($canEditForm
/*if (!empty($parentForm["params"][$kunik]["validation"])) {
     var_dump($parentForm["params"][$kunik]["validation"]);
}*/
/*ar_dump($answerPath);
if (isset($answer["answers"][$form["id"]][$kunik])) {
    var_dump($answer["answers"][$form["id"]][$kunik]);
  }*/
//var_dump($answer);

$inpClass = "form-control";

$paramsData = [ 
	"validation" => [
		"text" => "texte",
		"number" => "Nombre",
    "date" => "Date",
		"email" => "adresse mail",
    "autocomplete" => "Autocompletion"
	]
];

$paramsDataAutocompletion = [ 
  "autocomplete" => [
    "name" => "name",
    "username" => "username",
    "email"=>"email"
  ]
];


if($saveOneByOne)
	$inpClass .= " saveOneByOne";

if($mode == "r"){ ?>
    <div class="input-grp " style="position: relative; padding-top: 50px; text-transform: unset;">
      <label for="<?php echo $key ?>">
        <h4 style="text-transform: unset; color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
          <?php echo $label.$editQuestionBtn ?>
        </h4>
      </label>
      <div>
          <?php if(!empty($answer["answers"][$form["id"]][$kunik])) {?>
            <label class="responsemultitext">
            <?php 
              echo ((!empty($answer["answers"][$form["id"]][$kunik])) ? $answer["answers"][$form["id"]][$kunik] : "") ; 
            ?>
            </label>
          <?php } ?>
        </div>

  </div>
<?php 
}else{
?>
	<div class="input-grp" style="position: relative; padding-top: 50px; text-transform: unset;">
	    <label for="<?php echo $key ?>">
        <h4 style="text-transform: unset; color <?php echo ($titleColor) ? $titleColor : "black"; ?>">
          <?php echo $label.$editQuestionBtn ?>
        </h4>
      </label>
	    <br/>
	    
	    <div class="multiinput" data-validate="Valid email is required: ex@abc.xyz">
    	    <input 
             <?php echo $value ?> 
             data-id="<?php echo $kunik ?>" 
             data-form='<?php echo $form["id"] ?>' 
             class="effect-2 validate-input" 
             placeholder="<?php echo (isset($placeholder)) ? $placeholder : '' ?>" 
             type="<?php 
            	    if( isset($parentForm["params"][$kunik]["validation"]) ) {
            	    	echo $parentForm["params"][$kunik]["validation"];
              	 	}else{
              	 		echo "text";
              	 	}

            	    ?>" 
             style="position: relative; width: 100%;" 
          />
          <span class="focus-border"></span>
    	    <?php 
    	  			if($canEditForm){
    	  					echo "<a href='javascript:;' 
                            data-id='".$parentForm["_id"]."' 
                            data-collection='".Form::COLLECTION."' 
                            data-path='params.".$kunik.".validation' 
                            class='previewTpl paramsonebtn edit".$kunik."ParamsMt '>
                            <i class='fa fa-cog' style='padding: 4px;'></i> 
                          </a>";	

    	  			}
    	  	?>

          <?php  if (!empty($parentForm["params"][$kunik]["validation"])) {
              if ($parentForm["params"][$kunik]["validation"] == "autocomplete") {
                  echo "<a href='javascript:;' 
                              data-id='".$parentForm["_id"]."' 
                              data-collection='".Form::COLLECTION."' 
                              data-path='params.".$kunik.".autocomplete' 
                              class='previewTpl paramsonebtn edit".$kunik."Autocompletion '>
                              <i class='fa fa-edit' style='padding: 4px;'></i> 
                          </a>";
              }
          }
     
          ?>
        </div>
	</div>


	<script type="text/javascript">
	sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  sectionDyf.<?php echo $kunik ?>paramsDataAutocompletion = <?php echo json_encode( $paramsDataAutocompletion ); ?>;
   //console.log(sectionDyf.<?php echo $kunik ?>paramsDataAutocompletion);


	jQuery(document).ready(function() {
	    mylog.log("render form input","/modules/costum/views/tpls/forms/text.php");
	//modal for input type	
		sectionDyf.<?php echo $kunik ?>Params = {
		    "jsonSchema" : {	
    	        "title" : "Type de l'input",
    	        "icon" : "fa-cog",
    	        "properties" : {
    	            validation : {
    	                inputType : "select",
    	                label : "type de l'input",
    	                options :  sectionDyf.<?php echo $kunik ?>ParamsData.validation,
    	                values :  ["<?php echo (isset($parentForm["params"][$kunik]["validation"])) ? 
                                    $parentForm["params"][$kunik]["validation"] : ''; ?>"]
    	            }
    	        },
    	        save : function () {  
    	            tplCtx.value = {};
    	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
    	        	if(val.inputType == "properties")
                    tplCtx.value = getPairsObj('.'+k+val.inputType);
                else if(val.inputType == "array")
                    tplCtx.value = getArray('.'+k+val.inputType);
                else if(val.inputType == "formLocality")
                    tplCtx.value[k] = getArray('.'+k+val.inputType);
                else
                    tplCtx.value = $("#"+k).val();
                    //alert($("#"+k).val());
    	        	});
    	            mylog.log("save tplCtx",tplCtx);

    	            if(typeof tplCtx.value == "undefined")
    	            	toastr.error('value cannot be empty!');
    	            else {
    	                dataHelper.path2Value( tplCtx, function(params) { 
    	                    $("#ajax-modal").modal('hide');
    	                    location.reload();
    	                } );
    	            }

    	    	}
    	    }
    	};

        //modal for auto completion 
    sectionDyf.<?php echo $kunik ?>paramsAutocompletion = {
        "jsonSchema" : {  
              "title" : "Information à récuperé",
              "icon" : "fa-cog",
              "properties" : {
                  autocomplete : {
                      inputType : "select",
                      label : "information de l'utilisateur",
                      options :  sectionDyf.<?php echo $kunik ?>paramsDataAutocompletion.autocomplete,
                      values :  "<?php echo (isset($parentForm["params"][$kunik]["autocomplete"])) ? 
                                    $parentForm["params"][$kunik]["autocomplete"] : ''; ?>"

                      
                  }
              },
              save : function () {  
                  tplCtx.value = {};
                  $.each( sectionDyf.<?php echo $kunik ?>paramsAutocompletion.jsonSchema.properties , function(k,val) { 
                if(val.inputType == "properties")
                    tplCtx.value = getPairsObj('.'+k+val.inputType);
                else if(val.inputType == "array")
                    tplCtx.value = getArray('.'+k+val.inputType);
                else if(val.inputType == "formLocality")
                    tplCtx.value[k] = getArray('.'+k+val.inputType);
                else
                    tplCtx.value = $("#"+k).val();
                    //alert($("#"+k).val());
                });
                  mylog.log("save tplCtx",tplCtx);

                  if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                  else {
                      dataHelper.path2Value( tplCtx, function(params) { 
                          $("#ajax-modal").modal('hide');
                          location.reload();
                      } );
                  }
                  
            }
          }
      };

     //Open modal for input type
    	$(".edit<?php echo $kunik ?>ParamsMt").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = $(this).data("path");
            //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
      //Open modal for input autocompletion
      $(".edit<?php echo $kunik ?>Autocompletion").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = $(this).data("path");
            //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>paramsAutocompletion,null, sectionDyf.<?php echo $kunik ?>paramsDataAutocompletion);
        });

        $('#question<?php echo $key ?> .effect-2').each(function(){
            $(this).on('blur', function(){
                if($(this).val().trim() != "") {
                    $(this).addClass('has-val');
                }
                else {
                    $(this).removeClass('has-val');
                }
            })    
        }) 

        $('#question<?php echo $key ?> .effect-2').each(function(){
            $(this).on('blur', function(){
                if(validate(this) == false){
                    showValidate(this);
                } else {
                    if($(this).attr('type') == 'email' ) {
                        $(this).parent().addClass('true-validate');
                    }

                    if($(this).attr('type') == 'unique' ) {
                        ajaxPost(null, baseUrl+"/co2/search/globalautocomplete", 
                          {"searchType" : ["organizations"], "filters": { "siret" : [$(this).val()] }},
                          function (data){
                              if (Object.entries(data.results).length === 0){
                                  answer.path = "answers."+$(this).data("form")+"."+$(this).data("id");
                                  answer.collection = "answers" ;
                                  answer.id = "<?php echo $answer["_id"]; ?>";
                                  answer.value = $(this).val();
                                  dataHelper.path2Value(answer , function(params) { 
                                      //toastr.success('saved');
                                  });
                              } else {
                                  toastr.error('cette organisation existe déjà');
                              }
                          }
                        ); 
                    } else {

                        answer.path = "answers."+$(this).data("form")+"."+$(this).data("id");
                        answer.collection = "answers" ;
                        answer.id = "<?php echo $answer["_id"]; ?>";
                        answer.value = $(this).val();
                        dataHelper.path2Value(answer , function(params) { 
                            toastr.success('saved');
                        });
                    }
                }
            })    
        });

        $('#question<?php echo $key ?> .effect-2').each(function(){
            $(this).focus(function(){
               hideValidate(this);
               $(this).parent().removeClass('true-validate');
            });
        });
	});


    
    function validate (input) {
        if($(input).attr('type') == 'email' ) {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
    }


    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
	

	</script>
<?php } ?>

