<style>
.timeline {
  list-style: none;
  padding: 20px 0 20px;
  position: relative;
}
.timeline:before {
  top: 0;
  bottom: 0;
  position: absolute;
  content: " ";
  width: 3px;
  background-color: #eeeeee;
  left: 5%;
  margin-left: -1.5px;
}
:before, :after {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.timeline > li {
  margin-bottom: 20px;
  position: relative;
}
.timeline > li:before,
.timeline > li:after {
  content: " ";
  display: table;
}
.timeline > li:after {
  clear: both;
}
.timeline > li:before,
.timeline > li:after {
  content: " ";
  display: table;
}
.timeline > li:after {
  clear: both;
}
.timeline > li > .timeline-panel {
  width: 100%;
  float: left;
  border: 1px solid #d4d4d4;
  border-radius: 2px;
  padding: 20px;
  position: relative;
  -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
  box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
}
.timeline > li.timeline-inverted + li:not(.timeline-inverted),
.timeline > li:not(.timeline-inverted) + li.timeline-inverted {
margin-top: -60px;
}

.timeline > li:not(.timeline-inverted) {
padding-right:90px;
}

.timeline > li.timeline-inverted {
padding-left:90px;
}
.timeline > li.timeline-inverted{
  float: right;
  clear: right;
  margin-top: 0px;
  margin-bottom: 30px;
  width: 100%;
}
.timeline > li > .timeline-panel:before {
  position: absolute;
  top: 26px;
  right: -15px;
  display: inline-block;
  border-top: 15px solid transparent;
  border-left: 15px solid #ccc;
  border-right: 0 solid #ccc;
  border-bottom: 15px solid transparent;
  content: " ";
}
.timeline > li > .timeline-panel:after {
  position: absolute;
  top: 27px;
  right: -14px;
  display: inline-block;
  border-top: 14px solid transparent;
  border-left: 14px solid #fff;
  border-right: 0 solid #fff;
  border-bottom: 14px solid transparent;
  content: " ";
}
.timeline > li > .timeline-badge {
  color: #fff;
  width: 50px;
  height: 50px;
  line-height: 50px;
  font-size: 1.4em;
  text-align: center;
  position: absolute;
  top: 16px;
  left: 5%;
  margin-left: -25px;
  background-color: #999999;
  z-index: 100;
  border-top-right-radius: 50%;
  border-top-left-radius: 50%;
  border-bottom-right-radius: 50%;
  border-bottom-left-radius: 50%;
}
/*.timeline > li.timeline-inverted > .timeline-panel {
  float: right;
}*/
.timeline > li.timeline-inverted > .timeline-badge{
  left: 5%;
}
.timeline > li.timeline-inverted > .timeline-panel:before {
  border-left-width: 0;
  border-right-width: 15px;
  left: -15px;
  right: auto;
}
.timeline > li.timeline-inverted > .timeline-panel:after {
  border-left-width: 0;
  border-right-width: 14px;
  left: -14px;
  right: auto;
}
.timeline-badge.primary {
  background-color: #2e6da4 !important;
}
.timeline-badge.success {
  background-color: #3f903f !important;
}
.timeline-badge.warning {
  background-color: #f0ad4e !important;
}
.timeline-badge.danger {
  background-color: #d9534f !important;
}
.timeline-badge.info {
  background-color: #5bc0de !important;
}
.timeline-title {
  margin-top: 0;
  color: inherit;
}
.timeline-body > p,
.timeline-body > ul {
  margin-bottom: 0;
}
.timeline-body > p + p {
  margin-top: 5px;
}
</style>

<?php if($answer){
	//var_dump($answers);
	//var_dump(Yii::app()->session["user"]);
	//var_dump($answerPath);
	if (isset($answer["answers"][$form["id"]][$kunik])) {
		var_dump($answer["answers"][$form["id"]][$kunik]);
	}
    ?>
    <div class="form-group">
            <?php
            $i = 0;
            $show = true;
            if (isset($answers)) {
                foreach ($answers as $q => $a) {
                    if (isset($answers)) {
                        $i++;
                    }
                }
            }
            if(isset($this->costum["form"]["params"][$kunik]['num_row']) and $this->costum["form"]["params"][$kunik]['num_row'] == "0" and isset($this->costum["form"]["params"][$kunik]["nbmax"]) and ((int)$this->costum["form"]["params"][$kunik]["nbmax"]) <= $i){
                $show = false;
            }
            $editBtnL="";
            if($show){
                $editBtnL = (Yii::app()->session["userId"] == $answer["user"]) ? 
                " <a href='javascript:;' 
                  data-id='".$answer["_id"]."' 
                  data-collection='".Form::ANSWER_COLLECTION."' 
                  data-path='".$answerPath."' 
                  class='add".$kunik." btn btn-default'>
                  <i class='fa fa-plus'></i> 
                  Ajouter une ligne 
                 </a>"
                 :
                  "";
            }

            $editParamsBtn = ($canEdit) ? 
	            "<a href='javascript:;' 
	                 data-id='".$el["_id"]."' 
	                 data-collection='".$this->costum["contextType"]."' 
	                 data-path='costum.form.params.".$kunik."' 
	                 class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'>
	                 <i class='fa fa-cog'></i> 
	            </a>" 
            : 
            	"";

            $paramsData = [
                "nbmax" => "2", 
                "num_row" => ["limité","illimité"]
            ];

            if( isset($this->costum["form"]["params"][$kunik]["nbmax"]) )
                $paramsData["nbmax"] =  $this->costum["form"]["params"][$kunik]["nbmax"];
            if( isset($this->costum["form"]["params"][$kunik]["limited"]) )
                $paramsData["num_row"] =  $this->costum["form"]["params"][$kunik]["num_row"];


            $properties = [
                "liste_row" => [
                    "label" => "mot ou phrase",
                    "placeholder" => "mot ou phrase",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "liste_row1" => [
                    "label" => "Année début",
                    "placeholder" => "ex : 2017",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "liste_row2" => [
                    "label" => "Année fin",
                    "placeholder" => "ex : 2019",
                    "inputType" => "text",
                    "rules" => [ "required" => false ]
                ]
            ];

            ?>
                <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
                	<?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?>
                </h4>
                    <?php echo $info ?>

                    <style>
                        .s19{list-style: none;}
                        .s19 li:before{
                            content: '\f0a9';
                            margin-right: 15px;
                            font-family: FontAwesome;
                            color: #d9534f;
                        }
                    </style>

                    <ol class="timeline" style="white-space: normal;
">
                        <?php
                        $ct = 0;

                        if(isset($answers)){
                            foreach ($answers as $q => $a) {
                            	//var_dump($q);var_dump($a);
                                ?>
                               <li class="timeline-inverted">
						          <div class="timeline-badge"><i class="glyphicon glyphicon-check"></i></div>
						          <div class="timeline-panel">
						            <div class="timeline-heading">
						              <h4 class="timeline-title">
						              	<?php echo $a["liste_row1"] ?>
						              	 <?php echo !empty($a["liste_row2"]) ? " - ".$a["liste_row2"] : ""  ?>
						              	<div class="pull-right"><?php
	                                       echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
	                                           "canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
	                                           "id" => $answer["_id"],
	                                           "collection" => Form::ANSWER_COLLECTION,
	                                           "q" => $q,
	                                           "path" => "answers.".$kunik.".".$q,
	                                           "keyTpl"=>$kunik
                                             //"class" =>"edit".$kunik."formation"
	                                       ] ); ?><a 
	                                       href="javascript:;" 
	                                       class="btn btn-xs btn-primary openAnswersComment" 
	                                       onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
	                           </div>
						              </h4>
						            </div>
						            <div class="timeline-body">
						              <p>
						              	<?php echo $a["liste_row"] ?>
						              </p>
						            </div>
						          </div>
						        </li>


                               </li><br>
                        <?php
                            }
                        }

                        ?>

                    </ol>

    </div>
    <script type="text/javascript">

        var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

        $(document).ready(function() {

            sectionDyf.<?php echo $kunik ?> = {
                "jsonSchema" : {
                    "title" : "Formation et Diplome",
                    "icon" : "fa-money",
                    "text" : "",
                    "properties" : <?php echo json_encode( $properties ); ?>,
                    save : function () {
                        var today = new Date();
                        tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
                        $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                        });

                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");//$("#ajax-modal").modal('hide');
                                location.reload();
                            } );
                        }
                    }
                }
            };


            mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

            //adds a line into answer
            $(".add<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
            });

            $(".edit<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
            });

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                //if no params config on the element.costum.form.params.<?php echo $kunik ?>
                //then we load default values available in forms.inputs.<?php echo $kunik ?>xxx.params
                //mylog.log(".editParams",sectionDyf.<?php echo $kunik ?>Params,calData);
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            });


        });
    </script>
<?php } else {
    //echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>