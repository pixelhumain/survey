<?php if($answer){ ?>
<div class="form-group">
	<table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>">
		
	<?php 
		$mapping =[
			 "Tiers-lieu agricole" => "Tiers-lieu agricole / Nourricier / Espace de production agricole / Jardin potager",
			 "Tiers-lieu culturel" => "Scène / Lieu de résidence-création"
		];
		$paramsData = [ "type" => [
					    	Organization::COLLECTION => "Organization",
					    	Person::COLLECTION 		 => "Person",
					    	Event::COLLECTION 		 => "Event",
					    	Project::COLLECTION 	 => "Project",
							News::COLLECTION 		 => "News",
					    	//Need::COLLECTION 		 => "Need",
					    	City::COLLECTION 		 => "City",
					    	Thing::COLLECTION 		 => "Thing",
					    	Poi::COLLECTION 		 => "Poi",
					    	Classified::COLLECTION   => "Classified",
					    	Product::COLLECTION 	 => "Product",
					    	Service::COLLECTION   	 => "Service",
					    	Survey::COLLECTION   	 => "Survey",
					    	Bookmark::COLLECTION   	 => "Bookmark",
					    	Proposal::COLLECTION   	 => "Proposal",
					    	Room::COLLECTION   	 	 => "Room",
					    	Action::COLLECTION   	 => "Action",
					    	Network::COLLECTION   	 => "Network",
					    	Url::COLLECTION   	 	 => "Url",
					    	Risk::COLLECTION   => "Risk",
					    	Badge::COLLECTION   => "Badge",
					    ],
					    "limit" => 0 ];
		
		if( isset($parentForm["params"][$kunik]) ) {
			if( isset($parentForm["params"][$kunik]["limit"]) ) 
				$paramsData["limit"] =  $parentForm["params"][$kunik]["limit"];
		}

		$properties = [
                "qui" => [
                    "label" => "Nom de votre structure",
                    "placeholder" => "Qui...",
                ],
                "type" => [
                    "label" => "Type",
                    "placeholder" => "...type...",
                ]
	        ];

		$editBtnL = ($canEdit === true
					&& isset($parentForm["params"][$kunik])
					&& ( $paramsData["limit"] == 0 || 
						!isset($answers) || (isset($answers) && !isset($answers[0])) ||
						( isset($answers) && $paramsData["limit"] > count($answers) ))) 
			? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default '><i class='fa fa-plus'></i> Ajouter un élément </a>" 
			: "";
		
		$editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";
	?>	
	<thead>
		<tr>
			<td colspan='<?php echo count( $properties)+2?>' ><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
				<?php echo $info;
				if( !isset($parentForm["params"][$kunik]['type']) ) 
					echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> THIS FIELD HAS TO BE CONFIGURED FIRST ".$editParamsBtn."</span>";
				 ?>

			</td>
		</tr>	
		<?php if(isset($answers) && count($answers)>0){ ?>
		<tr>
			</th>
			<?php 
			
			foreach ($properties as $i => $inp) {
				echo "<th>".$inp["placeholder"]."</th>";
			} ?>
			<th></th>
		</tr>
		<?php } ?>
	</thead>
	<tbody class="directoryLines">	
		<?php 
		$ct = 0;

		//var_dump($answers);
		
		if(isset($answers))
		{
			foreach ($answers as $q => $a) 
			{
				if( $paramsData["limit"] == 0 || $paramsData["limit"] > $q )
				{
					if(isset($a["slug"]))
						$el = Slug::getElementBySlug($a["slug"],["name","type"]);
					echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
					foreach ($properties as $i => $inp) 
					{
						if( $i == "qui" && isset($a["slug"]) && !empty($el)) {
							echo "<td><a href='#page.type.".$el["type"].".id.".$el["id"]."' class='lbh-preview-element' >".$el["el"]["name"]."</a></td>";
						} 
						else if($i == "type" && isset($a["slug"]) && !empty($el)){
							$elType=(isset($el["el"]["type"])) ? $el["el"]["type"] : $el["type"];
							echo "<td>".Yii::t("common",$elType)."</a></td>";
						}
						// else 
						// 	echo "<td>".$a[$i]."</td>";
					}
				?>
				<td>
					<?php
					// echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
					// 					"canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
					// 					"id" => $answer["_id"],
					// 					"collection" => Form::ANSWER_COLLECTION,
					// 					"q" => $q,
					// 					"path" => substr($answerPath, 0, -1),
					// 					"kunik"=>$kunik, 
					// 				    "parentid"=>(string)$form["_id"]] );
					?>

					<a href='javascript:;' data-actionid='' data-id='<?php echo $answer["_id"] ?>' data-collection='<?php echo Form::ANSWER_COLLECTION ?>' data-key='<?php echo $q?>' data-path='<?php echo substr($answerPath, 0, -1) ?>' class='edit<?php echo $kunik ?> previewTpl btn btn-xs btn-default'><i class='fa fa-pencil'></i></a>

					<a href='javascript:;' data-id='<?php echo $answer["_id"] ?>' data-collection='<?php echo Form::ANSWER_COLLECTION ?>' data-key='<?php echo $kunik.$q?>' data-path='<?php echo substr($answerPath, 0, -1) ?>' class='deleteLineTl previewTpl btn btn-xs btn-danger'><i class='fa fa-times'></i></a>
					<!-- <a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')">
								<?php 
									echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q)); ?> 
									<i class='fa fa-commenting'></i></a> -->
				</td>
				<?php 
					$ct++;
					echo "</tr>";
				}
			}
		}
		 ?>
		</tbody>
	</table>
</div>
<?php
$form3=Form::getById($parentForm["subForms"][3]);
	$form3=(string)$form3["_id"];
$form1=Form::getById($parentForm["subForms"][1]);
	$form1=(string)$form1["_id"];	
?>
<script type="text/javascript">


var answerLinks=<?php echo json_encode( (isset($answer["links"])) ? $answer["links"] : null ); ?>;

var elementType<?php echo $kunik ?> = <?php echo json_encode(isset($parentForm["params"][$kunik]['type']) ? $parentForm["params"][$kunik]['type'] : null) ?>;

var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

var form3_id = <?php echo json_encode(isset($form3)? $form3 : null) ;?>;

var form3Id = <?php echo json_encode((isset($parentForm["subForms"][3]))) ? json_encode($parentForm["subForms"][3]) : ""; ?>;

var form1_id = <?php echo json_encode(isset($form1)? $form1 : null) ;?>;

var form1Id = <?php echo json_encode((isset($parentForm["subForms"][1]))) ? json_encode($parentForm["subForms"][1]) : ""; ?>;

var mapping = <?php echo json_encode( (isset($mapping)) ? $mapping : null) ;?>;


$(document).ready(function() { 
	mylog.log("render","/modules/costum/views/tpls/forms/element.php");

	// TODO - reload project input if new answers

	if(costum && "<?php echo $parentForm["params"][$kunik]['type'] ?>" == "<?php echo Organization::COLLECTION ?>"){
		var answerThirdPlace = <?php echo json_encode(isset($answers)) ? json_encode($answers) : null ?>;

		costum.thirdPlaceRef= (notNull(answerThirdPlace)) ? Object.values(answerThirdPlace)[0] : null ;
	}
	// add classified - modules.classifieds
	if(<?php echo $kunik ?>Data!=null && <?php echo $kunik ?>Data.length < 0){
		$("#question<?php echo $key ?>").removeClass("hide");
	}
	//can be hacked to apply further costumization
	//is used like a dynFormCostumIn in openForm
	if(typeof costum=="undefined" || !costum){
		costum={};
	}

	

	
	costum.<?php echo $kunik ?> = {
		onload : {"actions" : { "setTitle" : "<?php echo $input["label"] ?>"}},
		prepData : function(data){
			data=data.map;
			mylog.log("prepData costum",data);
			$.each(costum.lists, function(e, v){
				constructDataForEdit=[];
				$.each(v, function(i, tag){
					if($.inArray(tag, data.tags) >=0){
						constructDataForEdit.push(tag);
						data.tags.splice(data.tags.indexOf(tag),1);
					}
				});
				data[e]=constructDataForEdit;
			});
			if(typeof data.tags != "undefined" && notNull(data.tags))
			data.tags.splice(data.tags.indexOf("TiersLieux"),1);
			data.map=data;
			return data;
		},
		afterSave : function(data) { 
			mylog.log("element afterSave",data);
			costum.<?php echo $kunik ?>.connectToAnswer(data);
			//var dataElement=data;
			// if(notNull(answerLinks) && typeof answerLinks.elementType!="undefined" && answerLinks.elementType[data._id.$id]){
			
			
				
			
			
		},
		connectToAnswer : function ( data ) { 
			var typeElement = (data.type) ? data.type : "<?php echo (isset($parentForm["params"][$kunik]['type'])) ? $parentForm["params"][$kunik]['type'] : ''; ?>";
			mylog.log("costum.<?php echo $kunik ?>.connectToAnswer",data);
			//alert(typeElement);
			tplCtx.value = {
				type : typeElement,
				id : data.id,
				slug : data.map.slug,
				name : data.map.name
			};   

		    mylog.log("save tplCtx",tplCtx);
		    
		    if(typeof tplCtx.value == "undefined")
		    	toastr.error('value cannot be empty!');
		    //alert(typeElement);
		    else {
		    	var idOrga=data.id;
		        dataHelper.path2Value ( tplCtx, function(params) { 
		            $("#ajax-modal").modal('hide');
		            
		            reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
                });



                if("<?php echo $key ?>"=="lesTiersLieuxCollaboratifsdelaReunion2892021_950_a1"){

                	// save step1 orga on answer
                	    //             tplCtx.value={};
                	    //             tplCtx.value[data.id] = {
                	    //             	type : typeElement,
                					// slug : data.map.slug
                	    //             };

                	tplCtx.path = "step1";

                	dataHelper.path2Value ( tplCtx, function(params) { 
                		toastr.success("formulaire relié à "+data.map.name);
                	});                	


                	costum.thirdPlaceRef=tplCtx.value;
		        	var form=$('#question<?php echo $key ?>').data("form");

                                      // Champ 2 : nom du tiers-lieu		        	

                                      answer.path = "answers."+form1Id+".lesTiersLieuxCollaboratifsdelaReunion2892021_950_02";
                                       answer.collection = "answers" ;
                                      answer.id = "<?php echo $answer["_id"]; ?>";
                                      answer.value = data.map.name;
                                      dataHelper.path2Value(answer , function(params) { 
                                          //toastr.success('saved');
                                          reloadInput("lesTiersLieuxCollaboratifsdelaReunion2892021_950_02", form1_id);
                                      });


                                      // description courte

                                      answer.path = "answers."+form1Id+".lesTiersLieuxCollaboratifsdelaReunion2892021_950_06";
                                       answer.collection = "answers" ;
                                      answer.id = "<?php echo $answer["_id"]; ?>";
                                      answer.value = data.map.shortDescription;
                                      dataHelper.path2Value(answer , function(params) { 
                                          //toastr.success('saved');
                                          reloadInput("lesTiersLieuxCollaboratifsdelaReunion2892021_950_06", form1_id);
                                      });


                                      // numéro téléphone

                                      answer.path = "answers."+form1Id+".lesTiersLieuxCollaboratifsdelaReunion2892021_950_017";
                                      
                                      answer.value = data.map.mobile;
                                      dataHelper.path2Value(answer , function(params) { 
                                          //toastr.success('saved');
                                          reloadInput("lesTiersLieuxCollaboratifsdelaReunion2892021_950_017", form1_id);
                                      });

                                      // adresse

                                      answer.path = "answers."+form1Id+".lesTiersLieuxCollaboratifsdelaReunion2892021_950_08";
                                     
                                      var address = {};
                                      var name = data.map.address.streetAddress+", "+data.map.address.postalCode+", "+data.map.address.addressLocality+", "+data.map.address.level1Name;
                                      data.map.address.name=name;
                                      address = {
                                      	"address" :  data.map.address,
                                      	"geo" : data.map.geo,
                                      	"geoPosition" : data.map.geoPosition
                                      };
                                      answer.value = address;
                                      dataHelper.path2Value(answer , function(params) { 
                                          //toastr.success('saved');
                                          reloadInput("lesTiersLieuxCollaboratifsdelaReunion2892021_950_08", form1_id);
                                      });
                                      

                                      // statut juridique
                                      answer.path = "answers."+form1Id+".multiRadiolesTiersLieuxCollaboratifsdelaReunion2892021_950_04";
                                      var status={};
                                      $.each(costum.lists.manageModel,function(k,value){
                                      	if($.inArray(value, data.map.tags)>-1){
                                      		status={
                                      			"value" : value,
                                      			"type" : "simple"
                                      		};
                                      	}
                                      });
                                      answer.value = status;

                                      dataHelper.path2Value(answer , function(params) { 
                                          //toastr.success('saved');
                                          reloadInput("lesTiersLieuxCollaboratifsdelaReunion2892021_950_04", form1_id);
                                      });
                                      

                                      // type de lieux
                                      mylog.log("mapping typePlace",mapping);
                                      answer.path = "answers."+form1Id+".multiCheckboxPluslesTiersLieuxCollaboratifsdelaReunion2892021_950_05";
                                      var typo =[];
                                      var i=0;
                                      $.each(data.map.tags,function(k,value){
                                      	mapppedVal=(typeof mapping[value]!="undefined") ? mapping[value] : value;
                                      	//alert(value);
	                                      	if($.inArray(value,costum.lists.typePlace)>-1){
	                                      		typo[i]={};
	                                      		typo[i][mapppedVal]={};
	                                      		typo[i][mapppedVal]={
	                                      			"type" : "simple"
	                                      		};
	                                      		i++;
	                                      	}
                                      });
                                      answer.value = typo;

                                      dataHelper.path2Value(answer , function(params) { 
                                          //toastr.success('saved');
                                          reloadInput("lesTiersLieuxCollaboratifsdelaReunion2892021_950_05", form1_id);
                                      });


                                      // labels

                                      answer.path = "answers."+form1Id+".multiCheckboxPluslesTiersLieuxCollaboratifsdelaReunion2892021_950_011";
                                      var certification =[];
                                      var i=0;
                                      $.each(costum.lists.certification,function(k,value){
                                      	if($.inArray(value, data.map.tags)>-1){
                                      		certification[i]={};
                                      		certification[i][value]={};
                                      		certification[i][value]={
                                      			"type" : "simple"
                                      		};
                                      		i++;
                                      	}
                                      });
                                      answer.value = certification;

                                      dataHelper.path2Value(answer , function(params) { 
                                          //toastr.success('saved');
                                          reloadInput("lesTiersLieuxCollaboratifsdelaReunion2892021_950_011", form1_id);
                                      });


                                      // Autres services

                                      answer.path = "answers."+form3Id+".multiCheckboxPluslesTiersLieuxCollaboratifsdelaReunion2992021_87_21";
                                      var services =[];
                                      var i=0;
                                      $.each(data.map.tags,function(k,value){
                                      	if($.inArray(value, costum.lists.services)>-1){
                                      		services[i]={};
                                      		services[i][value]={};
                                      		services[i][value]={
                                      			"type" : "simple"
                                      		};
                                      		i++;
                                      	}
                                      });
                                      answer.value = services;

                                      dataHelper.path2Value(answer , function(params) { 
                                          //toastr.success('saved');
                                          reloadInput("lesTiersLieuxCollaboratifsdelaReunion2992021_87_21", form3_id);
                                      });



                                     // projects 
                                     answer.path = "answers."+form3Id+".lesTiersLieuxCollaboratifsdelaReunion2992021_87_24";

                                     var parentId="parent."+idOrga;
                                     var params = {
						                 	searchType : ["projects"],
						                 	notSourceKey : true
						                 };
						             params.filters={};
						             params.filters[parentId]={};
						             params.filters[parentId]={'$exists':true};
						            
                                     ajaxPost(
                                     	null,
						                 baseUrl+"/" + moduleId + "/search/globalautocomplete/type/organizations/id/"+idOrga,
						                 params,
						                 function(data){
						                 	mylog.log("data projects",data);
						                 	data = data.results;
						                 	mylog.log("data projects",data);
						                 	if(Object.keys(data)!="undefined" && Object.keys(data).length>0){
						                 		//alert("resultat !");
							                 	 var i="0item";
							                 	 var projects={};
							                 	 projects[i]={};
							                 	 var j=0;
	                                      
							                 	$.each(data, function(k,v){
							                 		//projects[i]= {};
							                 		projects[i] = {
							                 			type : v.collection,
							                 			id : k,
							                 			slug : v.slug
							                 		};
							                 		 //var j=parseInt(i);
							                 		 j++;
							                 		i=j+"item";
							                 		//mylog.log("iiiii",i, typeof i);
							                 	});

							                 	// projects=JSON.stringify(projects);

							                 	mylog.log("projects value", projects);
							                 	answer.collection = "answers" ;
                                      			answer.id = "<?php echo $answer["_id"]; ?>";
							                 	answer.value=(notNull(projects)) ? projects : "";
							                 	
								                 	dataHelper.path2Value(answer , function(params) { 
		                                          	//toastr.success('saved');
		                                          	reloadInput("lesTiersLieuxCollaboratifsdelaReunion2992021_87_24", form3_id);
		                                      		 });   
	                						 }
	                					 } 

                                     );


                                     //validatestep reload

                                    reloadInput("lesTiersLieuxCollaboratifsdelaReunion2892021_950_a3","<?php echo (string)$form["_id"] ?>");
                                    // reloadInput("lesTiersLieuxCollaboratifsdelaReunion2892021_950_037","615463fd88feb00501fe84ca");
                                    
                }
 
                                      	        

		    }
	    }
	  // onload : {
	  // 	"actions" : {
	  //    	"hide": {
	  //           		"parentfinder" : 1
	  //           	}
	  //           }
	  //       }
	};


	sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "Element config",
	        "icon" : "fa-cog",
	        "properties" : {
	            type : {
	                inputType : "select",
	                label : "Définir un type d'élément",
	                options :  sectionDyf.<?php echo $kunik ?>ParamsData.type,
	                value : "<?php echo (isset($parentForm["params"][$kunik]['type'])) ? $parentForm["params"][$kunik]['type'] : ''; ?>"
	            },
	            limit : {
	                label : "Combien d'éléments peuvent être ajoutés (0 si pas de limite)",
	                value : "<?php echo (isset($parentForm["params"][$kunik]['limit'])) ? $parentForm["params"][$kunik]['limit'] : ''; ?>"
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);

	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    $("#ajax-modal").modal('hide');
	                    urlCtrl.loadByHash(location.hash);
	                } );
	            }

	    	}
	    }
	};


    
	var dynformCostumAnswer = (typeof costum)!="undefined" ? costum.<?php echo $kunik ?> : null;

	if("<?php echo $parentForm["params"][$kunik]['type'] ?>" == "<?php echo Organization::COLLECTION ?>" && typeof costum!="undefined"){
		
		

		costum.searchExist = function (type,id,name,slug,email) { 
			mylog.log("costum searchExist : "+type+", "+id+", "+name+", "+slug+", "+email); 
			var data = {
				type : type,
				id : id,
				map : { slug : slug }
			};

			// costum.openFormSearchedOrg<?php echo $kunik ?>=function(typeO,idO){
			// 	var dynFormOrga=costum.<?php echo $kunik ?>;
			// 	dyFObj.editElement( typeO,idO, null, dynFormOrga);
			// };

			var searchedOrgaId = id;
			var searchedOrgaType = type;
			//alert("here");
			$("#similarLink").hide();
			$("#ajaxFormModal #name").val("");
			ajaxPost(
                null,
				baseUrl+"/co2/aap/searchanswer",
				{collection:"answers",
				path:"step1.id",
				key:id
			},
				function(data){
					
					mylog.log("callback searchexist",searchedOrgaId, typeof searchedOrgaId);
					var dataLinkedAnsw=data;
					if(typeof userConnected!="undefined" && typeof userConnected.links!="undefined" && typeof userConnected.links[links.linksExternalContext.organizations.citoyens]!="undefined" && typeof userConnected.links[links.linksExternalContext.organizations.citoyens][searchedOrgaId]!="undefined" && notNull(dataLinkedAnsw.data)){
						mylog.log("existing data");
						
						//$("#ajax-modal").modal('hide');
						urlCtrl.modalExplain({
							icon : "check-square-o",
							name : "Une réponse liée à "+name+" existe",
							text : "Nous vous invitons à mettre à jour le formulaire de réponse de "+name+", dont vous êtes membre. Cliquez sur le bouton ci-dessous pour y accéder.<br> Si vous souhaitez apporter une nouvelle réponse, cliquez sur fermer (<i class='fa fa-warning'></i> Attention aux doublons ! ).",
							link : {
								redirectAnswer : {
								    label : "Accéder au formulaire de réponses existant",
								    icon : "folder-open",
								    link : "#answer.index.id."+dataLinkedAnsw.data._id.$id+".mode.w",
								    linkClass : " lbh"
								}
								// stayNewAnswer : {
								//     label : "Je remplis un nouveau formulaire pour mon tiers-lieux",
								//     icon : "save",
								//     link : "javascript:;",
								//     onclick : ""
								// }
								// stayNewAnswer : {
								//     label : "Je continue sur ce nouveau formulaire",
								//     icon : "save",
								//     link : "javascript:;",
								//     onclick : "costum.openFormSearchedOrg<?php echo $kunik ?>(`"+searchedOrgaType+"`,`"+searchedOrgaId+"`)"
								// }
						    }

						});
						setTimeout($(".explain-modal.modal").css("z-index",100000),500);
						dyFObj.editElement( type,id, null, dynformCostumAnswer);

					}else if(typeof userConnected.links=="undefined" || typeof userConnected.links[links.linksExternalContext.organizations.citoyens]=="undefined" || typeof userConnected.links[links.linksExternalContext.organizations.citoyens][searchedOrgaId]=="undefined"){
						mylog.log("not a member");
						urlCtrl.modalExplain({
							icon : "times-circle",
							name : "Vous n'êtes pas membre de "+name,
							text : "Vous ne pouvez pas sélectionner cette structure car vous n'en êtes pas membre. <a href='javascript:links.connect(`"+type+"`,`"+id+"`,`"+userConnected._id.$id+"`,`citoyens`,`member`)'>Cliquez ici<a/> si vous voulez en être membre. Sinon, veuillez rechercher et sélectionner une autre structure."
						});	
						setTimeout($(".explain-modal.modal").css("z-index",100000),500);
					}
					else{
						dyFObj.editElement( type,id, null, dynformCostumAnswer);
					}

				});


			// TODO - set a condition ONLY if can edit
			
			
			
	    
		};

		$(".deleteLineTl").off().on('click',function(e){
			//alert("here");
			// e.preventDefault();
		  formId = $(this).data("id");
		  key = $(this).data("key");
		  pathLine = $(this).data("path");
		  collection = $(this).data("collection");
		  // if (typeof $(this).data("parentid") != "undefined") {
		  //     var parentfId = $(this).data("parentid");
		  // }
		  bootbox.dialog({
		      title: trad.confirmdelete,
		      message: "<span class='text-red bold'><i class='fa fa-warning'></i> "+trad.actionirreversible+"</span>",
		      buttons: [
		        {
		          label: "Ok",
		          className: "btn btn-primary pull-left",
		          callback: function() {
		            var formQ = {
		                    value:null,
		                    collection : collection,
		                    id : formId,
		                    path : pathLine
		                  };

		        // if (typeof parentfId != "undefined") {
		        //     formQ["formParentId"] = parentfId;
		        // }
		              
		            dataHelper.path2Value( formQ , function(params) { 
		                    reloadInput("<?php echo $key ?>","<?php echo (string)$form["_id"]?>");

		                    formQ.path="step1";
		                    
		                    dataHelper.path2Value( formQ , function(params) { 
		                    reloadInput("lesTiersLieuxCollaboratifsdelaReunion2892021_950_a3","<?php echo (string)$form["_id"]?>");
		                    });
		            });
		          }
		        },
		        {
		          label: "Annuler",
		          className: "btn btn-default pull-left",
		          callback: function() {}
		        }
		      ]
		  });
		});
	}	

    //adds a line into answer

   <?php if(isset($parentForm["params"][$kunik]["type"])) { ?>
	   		if(costum.typeObj && costum.typeObj.<?php echo $parentForm["params"][$kunik]["type"]; ?> && costum.typeObj.<?php echo $parentForm["params"][$kunik]["type"]; ?>.dynFormCostum!="undefined"){
	   		 dynformCostumAnswer = $. extend({}, costum.typeObj.<?php echo $parentForm["params"][$kunik]["type"] ?>.dynFormCostum, dynformCostumAnswer);
	   		}
 <?php  } ?>




    <?php if( isset($parentForm["params"][$kunik]['type']) ) { ?>
    $(".add<?php echo $kunik ?>").off().on("click",function() { 
    	mylog.log("dynformCostumAnswer",dynformCostumAnswer); 
        tplCtx.id = $(this).data("id");
        form= $(this).data("form");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));

        var value = null;
        if("<?php echo $parentForm["params"][$kunik]['type']; ?>"=="<?php echo Project::COLLECTION ?>" && typeof costum.thirdPlaceRef!="undefined" && notNull(costum.thirdPlaceRef)){
        	value={};
        	value.parent={};
        	var nameTl = typeof(costum.thirdPlaceRef.name) ? costum.thirdPlaceRef.name : costum.thirdPlaceRef.slug;
        	value.parent[costum.thirdPlaceRef.id] =  {
        		type : costum.thirdPlaceRef.type,
        		name : nameTl
        	};
        	//alert(costum.thirdPlaceRef.name);
         	mylog.log("third place parentvalue",value);

        // 	value={}
        }

        dyFObj.openForm( "<?php echo Element::getControlerByCollection($parentForm["params"][$kunik]['type']); ?>",null,value,null,dynformCostumAnswer);
    });

    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.editElement( <?php echo $kunik ?>Data[$(this).data("key")].type,<?php echo $kunik ?>Data[$(this).data("key")].id, null, dynformCostumAnswer);
    });
    <?php } ?>

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    
});
</script>
<?php } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>