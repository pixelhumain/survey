<?php
HtmlHelper::registerCssAndScriptsFiles(
    [
        '/plugins/d3/d3.v3.min.js'
    ],
    Yii::app()->request->baseUrl);
    $ignore = array('_file_', '_params_', '_obInitialLevel_' ,'ignore');
    $params = array_diff_key(get_defined_vars() + array_flip($ignore), array_flip($ignore));
?>

<button type="button" class="btn" id="reloadinput">
    <i class="fa fa-refresh"></i>
    Recharger
</button>
<div class="ccv_container" id="ccv_container">

</div>

<div id="graphique"></div>

<script type="application/javascript">
    var cvvphp = <?php echo json_encode((isset($params)) ? $params : null); ?>;

    var userResponseData = {
        surfaceToit : 0,
        pluviocsv : [],
        datapluviocsv : [],
        pluieLocale : [],
        coeffRuiss : 0,
        coeffSysteme : 0.9,
        volumesCuves : [0.5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        consommationJournaliere : [],
        pluieRecuperable : [],
        resultatsSimulation : [],
        totalBesoins : [],
        ratios : [],
        recommandations : [],
        stations : ""
    }

    async function fetchCSV(url) {
        try {
            const response = await fetch(url); // Charger le fichier CSV
            const text = await response.text(); // Lire le contenu comme texte brut
            const rows = text.split("\n").map(row => row.split(",")); // Découper en lignes et colonnes
            return rows; // Retourner les données CSV sous forme de tableau
        } catch (error) {
            console.error("Erreur lors du chargement du fichier CSV :", error);
            return null; // En cas d'erreur
        }
    }

    (async () => {
        const csvUrl = baseUrl + '/doc/csv/donneepluviometrereunion.csv';
        const csvData = await fetchCSV(csvUrl);

        if (csvData) {
            userResponseData.pluviocsv = csvData;
        }
    })();

    (async () => {
        const csvUrl = baseUrl + '/doc/csv/data_pluvios_2012-2022.csv';
        const csvData = await fetchCSV(csvUrl);

        if (csvData) {
            userResponseData.datapluviocsv = csvData;
        }
    })();

    function fillData(answerstep1) {
        var success = 1;
        var error = [];

        //surfaceToit
        if(typeof answerstep1.roofSurface != "undefined"
            && !isNaN(parseFloat(answerstep1.roofSurface))
            && parseFloat(answerstep1.roofSurface)
        ){
            userResponseData.surfaceToit = answerstep1.roofSurface;
        }else{
            success = 0;
            error.push('Veuillez entrer des valeurs numériques correctes pour la surface du toit');
        }

        //pluieLocale
        //Geolocalisation
        if(typeof answerstep1.address != "undefined"
            && typeof answerstep1.address.geo != "undefined"
            && typeof answerstep1.address.geo != "undefined"
            && typeof answerstep1.address.address != "undefined"
            && typeof answerstep1.address.address != "undefined"
            && answerstep1.address.address.addressCountry == "RE"
        ){
            var lon = answerstep1.address.geo.longitude[0];
            var lat = answerstep1.address.geo.latitude[1];

            var city = answerstep1.address.geo.latitude[1];

            userResponseData.stations = trouverCheckpointProche([answerstep1.address.geo.latitude,answerstep1.address.geo.longitude], userResponseData.pluviocsv);

        }else{
            success = 0;
            error.push('Veuillez entrer une adresse qui se trouve à La Réunion');
        }

        if(userResponseData.datapluviocsv.length > 0
            && userResponseData.stations != ""
            && userResponseData.datapluviocsv[0][0].split(';').includes(userResponseData.stations)
        ){
            userResponseData.datapluviocsv.shift();
            userResponseData.pluieLocale = calculerPluieLocale(userResponseData.datapluviocsv[0][0].split(';').indexOf(userResponseData.stations) , userResponseData.datapluviocsv);
        }else{
            success = 0;
            error.push(`La station la plus proche n'existe pas dans les données pluviométriques`);
        }

        const wctypetrans = {
            "1_double-commande" : 10,
            "1_simple-commande" : 5,
        }

        var useWc = answerstep1.anatolerakotoson22112024_177_0m3svakq2gy6veh06w5o == "Oui" ? true : false ;
        var wcType = "";
        var wcJour = 0;
        if(useWc
        ){
            wcType = answerstep1.anatolerakotoson22112024_177_0m3svy2fho8hwb1za1x;
            wcJour = parseInt(answerstep1.anatolerakotoson22112024_177_0m3sx34bzaf0h8wr24ek);
        }else{
            success = 0;
            error.push(`Donnée sur les wc incorect ou manquant`);
        }

        var personne = "";
        if(
            typeof answerstep1.nbPerson != "undefined"
            && answerstep1.nbPerson != 0
        ){
            personne = answerstep1.nbPerson;
        }else{
            success = 0;
            error.push(`Donnée sur les wcs incorecte ou manquant`);
        }

        const arrtypetrans = {
            "1_double-commande" : 10,
            "1_simple-commande" : 5,
        }

        var useArr = answerstep1.anatolerakotoson22112024_177_0m3sw7kx6elbgeqgi9rl == "Oui" ? true : false;
        var arrType = answerstep1.anatolerakotoson22112024_177_0m3swil1s33oe4jzbna7;
        var arrJour = answerstep1.anatolerakotoson22112024_177_0m3sx34bzaf0h8wr24ek;
        var supJardin = answerstep1.anatolerakotoson22112024_177_0m3swgypyyiq8lxe715;

        //true 10 1 6 true undefined 1 80
        console.log('aaaaa',useWc , wcType , wcJour, personne , useArr , arrType , arrJour , supJardin )

        userResponseData.consommationJournaliere = calculerConsommation(useWc , wcType , wcJour, personne , useArr , arrType , arrJour , supJardin)
        userResponseData.pluieRecuperable = calculEauRecuperable(userResponseData.pluieLocale, userResponseData.surfaceToit, userResponseData.coeffRuiss, userResponseData.coeffSysteme);
        userResponseData.resultatsSimulation = simulationCuve(userResponseData.pluieRecuperable, userResponseData.consommationJournaliere, userResponseData.volumesCuves);
        userResponseData.totalBesoins = Array(365).fill(userResponseData.consommationJournaliere).reduce((a, b) => a + b, 0);
        userResponseData.ratios = calculRatios(userResponseData.resultatsSimulation, userResponseData.totalBesoins);
        userResponseData.recommandations = determinerCuvesRecommandees(userResponseData.ratios);


        return {
            success : success,
            error : error
        }

        //

    }

    function trouverCheckpointProche(coordActuelle, checkpoints) {
        // Décomposer les coordonnées actuelles
        const [x1, y1] = coordActuelle;

        // Initialiser le checkpoint le plus proche
        let checkpointLePlusProche = null;
        let distanceMin = Infinity;

        // Parcourir tous les checkpoints pour calculer les distances
        checkpoints.forEach((checkpoint) => {
            const [x2, y2] = [checkpoint[0].split(';')[2],checkpoint[0].split(';')[2]];

            // Calculer la distance euclidienne
            const distance = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));

            // Mettre à jour si une distance plus petite est trouvée
            if (distance < distanceMin) {
                distanceMin = distance;
                checkpointLePlusProche = checkpoint[0].split(';')[1];
            }
        });

        return checkpointLePlusProche; // Retourne le checkpoint (coordonnées [x, y])
    }

    // Fonction pour calculer le volume d'eau récupérable
    function calculEauRecuperable(pluieLocale, surfaceToit, coeffRuiss, coeffSysteme) {
        return pluieLocale.map(pluie => (pluie / 1000) * surfaceToit * coeffRuiss * coeffSysteme);
    }

    // Fonction pour calculer la consommation quotidienne
    function calculConsommation(jour, wcUsage, arrosageUsage, consoWC, consoArrosage) {
        let consommation = 0;
        if (wcUsage) consommation += consoWC[jour];
        if (arrosageUsage) consommation += consoArrosage[jour];
        return consommation;
    }

    // Simulation du remplissage et vidage de cuve
    function simulationCuve(pluieRecuperable, consommationJournaliere, volumesCuves) {
        const epsilon = 1e-10; // Tolérance pour éliminer les erreurs d'arrondi
        const resultats = [];

        volumesCuves.forEach(volume => {
            let volumeStocke = 0; // Volume dans la cuve au début
            let pluieConsomme = 0; // Pluie réellement utilisée
            let eauReseau = 0; // Eau potable utilisée
            let tropPlein = 0; // Eau non stockée (perdue)

            pluieRecuperable.forEach((pluieJour, index) => {
                const besoinsJour = consommationJournaliere[index];

                // Calcul du volume stocké (avant consommation)
                if (volumeStocke + pluieJour > volume) {
                    tropPlein += volumeStocke + pluieJour - volume;
                    volumeStocke = volume;
                } else {
                    volumeStocke += pluieJour;
                }

                // Calcul de la consommation
                if (volumeStocke >= besoinsJour) {
                    pluieConsomme += besoinsJour;
                    volumeStocke -= besoinsJour;
                } else {
                    pluieConsomme += volumeStocke;
                    eauReseau += besoinsJour - volumeStocke;
                    volumeStocke = 0;
                }
            });

            // Élimination des erreurs d'arrondi
            eauReseau = Math.abs(eauReseau) < epsilon ? 0 : eauReseau;

            // Stocker les résultats pour ce volume de cuve
            resultats.push({ volume, pluieConsomme, eauReseau, tropPlein });
        });

        return resultats;
    }

    // Calcul des ratios pour chaque volume
    function calculRatios(resultatsSimulation, totalBesoins) {
        return resultatsSimulation.map(res => ({
            volume: res.volume,
            couverture: (res.pluieConsomme / totalBesoins) * 100,
            economie: res.pluieConsomme,
            reseau: res.eauReseau
        }));
    }

    // Fonction pour déterminer les volumes recommandés
    function determinerCuvesRecommandees(ratios) {
        const sortedRatios = [...ratios].sort((a, b) => a.volume - b.volume);
        const variations = [];
        //const seuilEconomique = 1.5; // Seuil pour le volume économique
        //const seuilEcologique = 0.1; // Seuil pour le volume écologique
        const seuilEconomique = 0.5; // Seuil plus bas pour détecter des petites variations
        const seuilEcologique = 0.05;

        for (let i = 1; i < sortedRatios.length; i++) {
            const deltaCouverture = sortedRatios[i].couverture - sortedRatios[i - 1].couverture;
            const deltaVolume = sortedRatios[i].volume - sortedRatios[i - 1].volume;
            variations.push(deltaCouverture / deltaVolume);
        }

        let volumeEconomique = null;
        let volumeOptimal = null;
        let volumeEcologique = 10;

        // Identifier les volumes recommandés
        for (let i = 0; i < variations.length; i++) {
            if (!volumeEconomique && variations[i] >= seuilEconomique) {
                volumeEconomique = sortedRatios[i].volume;
            }
            if (!volumeOptimal && variations[i] <= seuilEcologique) {
                volumeOptimal = sortedRatios[i].volume;
            }
        }

        // Fallback si aucun volume détecté
        if (!volumeEconomique) volumeEconomique = sortedRatios[0].volume;
        if (!volumeOptimal) volumeOptimal = sortedRatios[1].volume;

        return { economique: volumeEconomique, optimal: volumeOptimal, ecologique: volumeEcologique };
    }

    // Fonction pour recupérer les datas pluie pendant une annnée
    function calculerPluieLocale(indiceRegion , data) {
        const pluieJournaliere = [];

        // Parcourir les lignes pour ajouter chaque précipitation au tableau
        for (let i = 1; i < data.length; i++) {
            const pluie = parseFloat(data[i][indiceRegion]); // Lecture de la colonne région
            if (!isNaN(pluie)) {
                pluieJournaliere.push(pluie);
            }
        }

        return pluieJournaliere;
    }

    // Fonction pour créer un graphique avec D3.js v3
    function creerGraphiqueD3v3(volumes, donnees, titre, yLabel) {
        const width = 400;
        const height = 400;
        const margin = { top: 20, right: 30, bottom: 50, left: 50 };

        const svg = d3.select("#graphique")
            .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", `translate(${margin.left},${margin.top})`);

        const x = d3.scale.linear()
            .domain([d3.min(volumes), d3.max(volumes)])
            .range([0, width]);

        const y = d3.scale.linear()
            .domain([0, d3.max(donnees)])
            .range([height, 0]);

        const xAxis = d3.svg.axis().scale(x).orient("bottom").ticks(10);
        const yAxis = d3.svg.axis().scale(y).orient("left").ticks(10);

        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", `translate(0,${height})`)
            .call(xAxis);

        svg.append("g")
            .attr("class", "y axis")
            .call(yAxis);

        svg.append("text")
            .attr("text-anchor", "middle")
            .attr("transform", `translate(${width / 2},${height + margin.bottom - 10})`)
            .text("Volume de la cuve (m³)");

        svg.append("text")
            .attr("text-anchor", "middle")
            .attr("transform", `translate(${-margin.left + 20},${height / 2})rotate(-90)`)
            .text(yLabel);

        const line = d3.svg.line()
            .x((d, i) => x(volumes[i]))
            .y(d => y(d));

        svg.append("path")
            .datum(donnees)
            .attr("class", "line")
            .attr("fill", "none")
            .attr("stroke", "blue")
            .attr("stroke-width", 2)
            .attr("d", line);

        svg.append("text")
            .attr("text-anchor", "middle")
            .attr("x", width / 2)
            .attr("y", -10)
            .style("font-size", "16px")
            .style("font-weight", "bold")
            .text(titre);
    }

    function calculerConsommation(useWc , wcType , wcJour, personnes , useArsg , arrosageType , foisSem , surfaceJardin) {

        let consoWC = 0;

        if(useWc){
            consoWC = (wcType * wcJour * personnes) / 1000; // Conversion en m³/jour
        }

        // Calcul pour l'arrosage
        let consoArrosage = 0;
        const volumeBase = 20;

        if (useArsg) {
            const arrosageOptions = {
                "Econome": 0.5,
                "Raisonné": 1,
                "Abondant" : 1.5,
            };

            const coeffVolume = arrosageOptions[arrosageType];

            consoArrosage = (volumeBase * coeffVolume * foisSem) / 7; // Consommation journalière en L/m²
            consoArrosage = (consoArrosage / 1000) * surfaceJardin; // Conversion en m³/jour

            return consoWC + consoArrosage;
        }
    }

    $(document).ready(function() {
        $('#reloadinput').off().click(function () {
            reloadInput(cvvphp.key, cvvphp.formId);
        });

        var res = {
            success: 0,
            error: "Pas de données"
        };

        if (
            typeof cvvphp != "undefined"
            && typeof cvvphp.answer != "undefined"
            && typeof cvvphp.answer.answers != "undefined"
            && typeof cvvphp.answer.answers != "undefined"
            && typeof cvvphp.answer.answers.anatolerakotoson22112024_177_0 != "undefined"
        ) {
            res = fillData(cvvphp.answer.answers.anatolerakotoson22112024_177_0);
        }

        if (res.success == 0) {
            $('.ccv_container_error').append(res.error)
        } else {
            $('.ccv_container_error').html('')
        }

        /*const surfaceToit = 300;
        const pluieLocale = Array(365).fill(10); // 4 mm/jour sur une année
        const coeffRuiss = 0.8;
        const coeffSysteme = 0.9;
        const volumesCuves = [0.5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        const consommationJournaliere = Array(365).fill(4); // 0.1 m³/jour

        const pluieRecuperable = calculEauRecuperable(pluieLocale, surfaceToit, coeffRuiss, coeffSysteme);
        console.log("pluieRecuperable:", pluieRecuperable);
        const resultatsSimulation = simulationCuve(pluieRecuperable, consommationJournaliere, volumesCuves);
        console.log("resultatsSimulation :", resultatsSimulation);

        const totalBesoins = consommationJournaliere.reduce((a, b) => a + b, 0);
        console.log("resultatsSimulation :", totalBesoins);
        const ratios = calculRatios(resultatsSimulation, totalBesoins);
        console.log("resultatsSimulation :", ratios);
*/
        if(notEmpty(userResponseData.ratios)) {
            const recommandations = determinerCuvesRecommandees(userResponseData.ratios);

            creerGraphiqueD3v3(userResponseData.volumesCuves, userResponseData.ratios.map(r => r.couverture), "Taux de Couverture des Besoins", "Couverture (%)");

            // Afficher les recommandations
            document.getElementById('ccv_container').innerHTML = `
        <p>Volume économique : ${recommandations.economique} m³</p>
        <p>Volume optimal : ${recommandations.optimal} m³</p>
        <p>Volume écologique : ${recommandations.ecologique} m³</p>
        <br>
    `;
        }else{
            document.getElementById('ccv_container').innerHTML = `
        <p>Données incomplets</p>
        <br>
    `;
        }

    });
</script>
