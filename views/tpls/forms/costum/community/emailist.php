<?php if($answer){
    ?>
    <div class="form-group">
            <?php
            $i = 0;
            $show = true;
            if (isset($answers)) {
                foreach ($answers as $q => $a) {
                    if (isset($answers)) {
                        $i++;
                    }
                }
            }
            
            $editBtnL = ($canAdmin) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";
            
            $editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$el["_id"]."' data-collection='".$this->costum["contextType"]."' data-path='costum.form.params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

            $paramsData = [
                "mail_type" => ""
            ];

            if( isset($this->costum["form"]["params"][$kunik]["mail_type"]) )
                $paramsData["mail_type"] =  $this->costum["form"]["params"][$kunik]["mail_type"];


            $properties = [
                "liste_row" => [
                    "label" => "mot ou phrase",
                    "placeholder" => "mot ou phrase",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ]
            ];

            ?>
                <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
                    <?php echo $info ?>

                    <style>
                        .s19{list-style: none;}
                        .s19 li:before{
                            content: '\f0a9';
                            margin-right: 15px;
                            font-family: FontAwesome;
                            color: #d9534f;
                        }
                    </style>

                    <ol class="s19" style="
    white-space: normal;
">
                        <?php
                        $ct = 0;

                        if(isset($answers)){
                            foreach ($answers as $q => $a) {
                                ?>
                               <li> <?php echo $a['liste_row']; ?>
                                   <div class="pull-right"><?php
                                       echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
                                           "canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
                                           "id" => $answer["_id"],
                                           "collection" => Form::ANSWER_COLLECTION,
                                           "q" => $q,
                                           "path" => "answers.".$kunik.".".$q,
                                           "keyTpl"=>$kunik
                                       ] ); ?><a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a></div>
                               </li>
                        <?php
                            }
                        }

                        ?>
                    </ol>

    </div>
    <script type="text/javascript">

        var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

        $(document).ready(function() {

            sectionDyf.<?php echo $kunik ?> = {
                "jsonSchema" : {
                    "title" : "Mail",
                    "icon" : "fa-money",
                    "text" : "",
                    "properties" : <?php echo json_encode( $properties ); ?>,
                    save : function () {
                        var today = new Date();
                        tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
                        $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                        });

                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            ajaxPost(
                                    null,
                                    baseUrl+"/survey/answer/sendmail/id/<?php echo (string)$answer['_id'] ?>",
                                    {to:$("#liste_row").val(),msg:sectionDyf.<?php echo $kunik ?>ParamsData.mail_type},
                                    function(data){ 
                                        toastr.success("Un mail a été envoyé à toutes les parties prenantes afin de les notifier");
                                        //urlCtrl.loadByHash(location.hash);  
                                        dataHelper.path2Value( tplCtx, function(params) {
                                            $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");//$("#ajax-modal").modal('hide');
                                            //location.reload();
                                        } );    
                                    }
                                );
                        }
                    }
                }
            };

            sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema" : {
                    "title" : "<?php echo $kunik ?> config",
                    "description" : "Mail type",
                    "icon" : "fa-cog",
                    "properties" : {
                         subject : {
                            inputType : "text",
                            label : "Sujet",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsData.mail_type
                        },
                        mail_type : {
                            inputType : "textarea",
                            label : "Mail type",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsData.mail_type
                        }
                    },
                    save : function () {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                            if(val.inputType == "properties")
                                tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                            else if(val.inputType == "array")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else
                                tplCtx.value[k] = $("#"+k).val();
                            mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                        });
                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                                
                                location.reload();
                            } );
                        }

                    }
                }
            };


            mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

            //adds a line into answer
            $(".add<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
            });

            $(".edit<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
            });

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                //if no params config on the element.costum.form.params.<?php echo $kunik ?>
                //then we load default values available in forms.inputs.<?php echo $kunik ?>xxx.params
                //mylog.log(".editParams",sectionDyf.<?php echo $kunik ?>Params,calData);
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            });


        });
    </script>
<?php } else {
    //echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>