<?php 
$defaultColor = "#354C57"; 
$structField = "structags";

$keyTpl = "wizard";

$paramsData = [ "title" => "",
                "color" => "",
                "background" => "",
                "nbList" => 2,
                "defaultcolor" => "#354C57",
                "tags" => "structags"
                ];

if( isset($this->costum["tpls"][$keyTpl]) ) {
    foreach ($paramsData as $i => $v) {
        if( isset($this->costum["tpls"][$keyTpl][$i]) ) 
            $paramsData[$i] =  $this->costum["tpls"][$keyTpl][$i];      
    }
}

?>
<div class="col-xs-12 margin-top-20">
<div id="<?php echo $wizid ?>" class="swMain">

    <style type="text/css">
        .swMain ul li > a.done .stepNumber {
            border-color: <?php echo ( @$color1 ) ? $color1 : $defaultColor ?>;
            background-color: <?php echo ( @$color1 ) ? $color1 : $defaultColor ?>; 
        }

        swMain > ul li > a.selected .stepDesc, .swMain li > a.done .stepDesc {
         color: <?php echo ( @$color1 ) ? $color1 : $defaultColor ?>;  
         font-weight: bolder; 
        }

        .swMain > ul li > a.selected::before, .swMain li > a.done::before{
          border-color: <?php echo ( @$color1 ) ? $color1 : $defaultColor ?>;      
        }
    </style>


    <?php  
    foreach ($formList as $k => $v) {
        
    ?>
    <div id='<?php echo $v ?>xx' class='col-sm-offset-1 col-sm-10 sectionStep ' style="padding-bottom:40px">
    <?php 
        $form = PHDB::findOne( Form::COLLECTION,[ "id"=>$v ] ); 
        ?>

<script type="text/javascript">
formInputs["<?php echo $v ?>"] = <?php echo json_encode( $form['inputs'] ); ?>;
var formInputsHere = formInputs;
</script>

        <?php
        if( !empty($form) )
        {   
            echo '<h1 class="text-center" style="color:'.$color1.'" >'.@$form["name"].'</h1>';
            echo "<div class='text-center'>";
                if(isset($this->costum["cms"][$form["id"]."desc"]))
                  echo htmlentities($this->costum["cms"][$form["id"]."desc"]);
                else 
                    echo "<span style='color:#aaa'>* section description</span>";
                if($canEdit)
                  echo "<a class='btn btn-xs btn-danger editBtn' href='javascript:;' data-key='".@$form["id"]."desc' data-type='textarea' data-markdown='1'  data-path='costum.cms.".@$form["id"]."desc' data-label='Expliquez les objectifs de cette étape ? '> <i class='fa fa-pencil'></i></a>";
            echo "</div>";

            //echo "<div class='markdown'>";
            echo "<div class=''>";
                echo $this->renderPartial("survey.views.tpls.forms.formSection",
                          [ "formId" => $v,
                            "form" => $form,
                            "wizard" => false, 
                            "answer"=>$answer,
                            "mode" => "w",
                            "showForm" => $showForm,
                            "canEdit" => $canEdit,  
                            "el" => $el ] ,true );    
            echo "</div>";
        } 
        else 
        { 
            echo "";
        }
    ?>        
    </div>
    <?php  
    }  

    
 ?>

    <script type="text/javascript">
jQuery(document).ready(function() {
    mylog.log("render","survey.views.tpls.forms.wizard");
    $.each($(".markdown"), function(k,v){
        descHtml = dataHelper.markdownToHtml($(v).html()); 
        $(v).html(descHtml);
    });
    
});

    </script>


</div>

</div>

