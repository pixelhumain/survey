<style>
    /* The container */
    .container<?= $kunik ?> {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 22px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    }

    /* Hide the browser's default radio button */
    .container<?= $kunik ?> input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    }

    /* Create a custom radio button */
    .checkmark<?= $kunik ?> {
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: #eee;
    border-radius: 50%;
    }

    /* On mouse-over, add a grey background color */
    .container<?= $kunik ?>:hover input ~ .checkmark<?= $kunik ?> {
    background-color: #ccc;
    }

    /* When the radio button is checked, add a blue background */
    .container<?= $kunik ?> input:checked ~ .checkmark<?= $kunik ?> {
    background-color: #2196F3;
    }

    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark<?= $kunik ?>:after {
    content: "";
    position: absolute;
    display: none;
    }

    /* Show the indicator (dot/circle) when checked */
    .container<?= $kunik ?> input:checked ~ .checkmark<?= $kunik ?>:after {
    display: block;
    }

    /* Style the indicator (dot/circle) */
    .container<?= $kunik ?> .checkmark<?= $kunik ?>:after {
        top: 9px;
        left: 9px;
        width: 8px;
        height: 8px;
        border-radius: 50%;
        background: white;
    }
</style>
<?php

$contextId = array_keys($parentForm["parent"])[0];

$networkForms = PHDB::findAndSort(Form::COLLECTION,array("parent.".$contextId=>array('$exists'=>true),"what"=>"Réseau de tiers-lieux"),array("name"=>1));

$regionalAnswer=PHDB::findOne(Form::ANSWER_COLLECTION,array("previousAnswerId"=>(string)$answer["_id"]));

$disabled=(isset($regionalAnswer)) ? "disabled" : "";

$networkMapping = [];
foreach ($networkForms as $idForm => $netForm) {
    $networkMapping[$netForm["name"]]="#answer.index.id.new.form.".$idForm;
}
$inpClass = "";
// if($saveOneByOne)
//     $inpClass = " saveOneByOne";

 $paramsData = [ "options" => [ ] ];

// if( isset($parentForm["params"][$key]) )
//     $paramsData =  $parentForm["params"][$key];


// if(!isset($options) && isset($parentForm["params"][$key]['options']))
//     $options = $parentForm["params"][$key]['options'];

if($mode == "r" || $mode == "pdf"){ ?>
    <div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
        <label for="<?php echo $kunik ?>">
            <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label ?></h4>
        </label><br/>
        <?php  if(!empty($info)){ ?>
        <small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
    <?php } ?>
<!--         <?php echo (!empty($options) && !empty($options[$answers])) ? $options[$answers] : "" ; ?> -->
    </div>
<?php
}else{

// $editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$key."' class='previewTpl edit".$key."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";
?>

<div class="form-group col-xs-12 col-md-6 no-padding">
	<label for="<?php echo $key ?>">
        <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
            <?php echo $label ?>
        </h4>
    </label>
    <?php  if(!empty($info)){ ?>
        <small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
    <?php } ?>

            <!-- <select class="form-control " id="<?php echo $key ?>" data-form='<?php echo $form["id"] ?>' <?php echo $disabled ?> >
            	<option value="">Selectionner</option> -->


            	<?php
                $ix=0;
                foreach ($networkMapping as $name => $link) {
                ?>
                    <label class="container<?= $kunik ?>"><?= $name ?>
                        <input type="radio" name="<?= $kunik ?>" class="form-check-input <?php echo $inpClass ?>  <?= $kunik ?>"  id="<?php echo $kunik.$ix ?>" data-value="<?= $name ?>" data-form='<?php echo $form["id"] ?>' <?php echo (!empty($answers) && (string)$name== $answers ) ? "checked" : "" ?>  >
                        <span class="checkmark<?= $kunik ?>"></span>
                    </label>
            		<?php
                    $ix++;
            	} ?>
                <label class="container<?= $kunik ?>">Aucun
                    <input type="radio" name="<?= $kunik ?>" class="form-check-input <?php echo $inpClass ?>  <?= $kunik ?>"  id="<?php echo $kunik.$ix ?>" data-value="<?= @$name ?>" data-form='<?php echo $form["id"] ?>' <?php echo (!empty($answers) && $answers=='Aucun' ) ? "checked" : "" ?>  >
                    <span class="checkmark<?= $kunik ?>"></span>
                </label>
            <!-- </select> -->

</div>
<?php } ?>
<?php if($mode != "pdf"){ ?>
<script type="text/javascript">
var answers=<?php echo json_encode( $answers ); ?>;
sectionDyf.<?php echo $key ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
var networkMapping=<?php echo json_encode( $networkMapping ); ?>;
jQuery(document).ready(function() {
    if(typeof answers !="undefined" && answers!=""){
        if($("#question<?php echo $key ?>").siblings(":last").find(".step-vld button").length==1){
            var redirectHash=networkMapping[answers];
            mylog.log("redirectHash",redirectHash);
            $("#question<?php echo $key ?>").siblings(":last").find(".step-vld button").attr("data-hash",redirectHash).attr("data-name-form",answers);
        }
    }

    mylog.log("render form input","/modules/costum/views/tpls/forms/select.php");

    sectionDyf.<?php echo $key ?>Params = {
        "jsonSchema" : {
            "title" : "<?php echo $key ?> config",
            "description" : tradForm.possibleQuestionList,
            "icon" : "fa-cog",
            "properties" : {
                options : {
                    inputType : "array",
                    label : tradForm.titleList,
                    values :  sectionDyf.<?php echo $key ?>ParamsData.options,
                    init : function() {
                        $(`<input type="text" class="form-control copyConfig" placeholder="vous pouvez copier le liste ici, séparé par des virgules; ou utiliser le button ajout ci-dessous"/>`).insertBefore('.optionsarray .inputs.array');
                        $(".copyConfig").off().on("blur", function() {
                            let textVal = $(this).val().length > 0 ? $(this).val().split(",") : [];
                            textVal.forEach((el, index) => {
                                dyFObj.init.addfield('.optionsarray', el, 'list')
                            });
                            $(this).val('')
                        })
                    }
                }
            },
            save : function () {
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $key ?>Params.jsonSchema.properties , function(k,val) {
                    if(val.inputType == "array")
                        tplCtx.value[k] = getArray('.'+k+val.inputType);
                    else
                        tplCtx.value[k] = $("#"+k).val();
                 });
                mylog.log("save tplCtx",tplCtx);

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) {
                        $("#ajax-modal").modal('hide');
                        location.reload();
                    } );
                }

            }
        }
    };

    $(".edit<?php echo $key ?>Params").off().on("click",function() {
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $key ?>Params,null, sectionDyf.<?php echo $key ?>ParamsData);
    });

    $(".<?= $kunik ?>").off().on("change",function(e){
        if($(this).is(":checked")){
        var net=$(this).parent().text().trim();
        mylog.log("resultat",net);
        var redirectHash=networkMapping[net];
        mylog.log("redirectHash",redirectHash);
        const dataId = "<?= $key ?>"
		if(typeof ownAnswer != 'undefined' && ownAnswer[$(this).attr("data-form")]) {
			ownAnswer[$(this).attr("data-form")][dataId] = $(this).data("value")
		}
		if(typeof removeError != "undefined") {
			const elem = $(`li#question${dataId.replace('select', '')}`);
			removeError(
				elem,
				{
					'border' : 'none'
				},
				'error-msg'
			)
		}

        $(this).parent().parent().parent().parent().siblings(":last").find(".step-vld button").attr("data-hash",redirectHash).attr("data-name-form",net);


        var answer={
            path : "answers.<?= $form["id"] ?>.<?= $key ?>",
            collection : "answers",
            id : "<?php echo $answer["_id"]; ?>",
            value : net
        };
        dataHelper.path2Value(answer , function(params) {
            stepValidationReload<?php echo $form["id"] ?>();
            toastr.success(tradForm['modificationSave']);
        });
        e.stopImmediatePropagation();
    }




    });
});
</script>
<?php } ?>
