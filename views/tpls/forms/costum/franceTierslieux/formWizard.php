<?php
// var_dump($answer);count(array_keys($userAnswers))>1
$mode="w";

if($isNew){
    $baseurl = (!empty($el["costum"])) ? Yii::app()->getBaseUrl(true) . "/costum/co/index/slug/" . $el["slug"] : Yii::app()->getBaseUrl(true);
    ?>
    <script>

        $(function(){

            let userAnswers = <?= json_encode($userAnswers)?>;
            mylog.log("userAnswers",userAnswers);
            let dialogNewAns = bootbox.dialog({
                message: `<div class="">
                            <p>Vous avez déjà répondu au Recensement des tiers-lieux 2023. Sélectionnez l'une de vos réponses qui s'affichent ci-dessous ou cliquez sur le bouton \"Générer une nouvelle réponse\"</p>
                            <table class="table">
                                <tbody id="user-exists-answers" style="font-size:15px">

                                </tbody>
                            </table>
                            <button class="btn btn-success btn-block" id="new-anwers-popup"><i class="fa fa-plus"></i> ${ucfirst(trad["Generate a new answer"])}</button>
                        </div>`,
                closeButton: false
            });
            // if(Object.keys(userAnswers).length>1){
            dialogNewAns.on('shown.bs.modal', function(e){
                dialogNewAns.attr("id", "new-anwers-dialog");
                $("#new-anwers-dialog").css("background-color","rgb(0 0 0 / 54%)");
                var html = '';
                var i = 1;
                $.each(userAnswers,function(k,v){
                    v.customTitle = "";
                    if(exists(v.answers) && exists(v.answers.aapStep1) && exists(v.answers.aapStep1.titre))
                        v.customTitle = "<b>"+ucfirst(v.answers.aapStep1.titre)+"</b> /";
                    html+= `<tr id="ans-${k}">
                                <td>
                                    ${i+" -  "}
                                </td>
                                <td>
                                <a href="javascript:;" class="openanswerlink" data-link="${location.href.split("#")[0]}#answer.index.id.${k}.mode.w.standalone.true" >
                                    ${v.customTitle} ${ucfirst(moment.unix(v.created).format('dddd,Do MMMM YYYY h:mm:ss'))} </br>`;
                    if(typeof v["links"]!="undefined" && typeof v["links"]["organizations"]!="undefined" && v["links"]["organizations"]!=""){
                        html+=          `Réponse liée à <span href='#page.type.organizations.id.`+Object.keys(v["links"]["organizations"])[0]+`' class=' text-red'>`+v["links"]["organizations"][Object.keys(v["links"]["organizations"])[0]]["name"]+`</span>`;
                    }

                    html+=            `</a></td>
                                <td>
                                    <button type="button" class="btn btn-xs btn-danger delete-answer-popup" data-id="${k}"><i class="fa fa-times"></i></button>
                                </td>
                            </tr>`;
                    i++;
                })
                html += '';
                $('#user-exists-answers').html(html);
                // coInterface.bindLBHLinks();
                $("#new-anwers-popup").off().on("click",function(){
                    window.open(location.href+'.ask.false',"_self")
                    location.reload();
                });
                $(".delete-answer-popup").off().on('click',function(){
                    var idans = $(this).data("id");
                    bootbox.dialog({
                        title: trad.confirm,
                        message: "<span class='text-red bold'><i class='fa fa-warning'></i>"+trad.actionirreversible+"</span>",
                        buttons: [{
                            label: "Ok",
                            className: "btn btn-primary pull-left",
                            callback: function () {
                                getAjax("", baseUrl + "/survey/co/delete/id/" + idans, function (data) {
                                    if (data.result) {
                                        toastr.success(trad.deleted);
                                        $("#ans-"+idans).hide(500)
                                    }
                                }, "html");


                            }
                        },
                            {
                                label: "Annuler",
                                className: "btn btn-default pull-left",
                                callback: function () { }
                            }
                        ]
                    });
                })

                $('.openanswerlink').off().on('click',function(){
                    $(location).prop('href', $(this).data('link'));
                    window.location.reload();
                });
            });
            // }
        })
    </script>
<?php }else{ ?>
    <style>
        /*hiding interface button*/

        #mainNav .searchBarInMenu, #menuBottom, #menuRight, #menuTopRight .divider, #menuTopRight ul li:nth-child(3){
            display:none;
        }

        #wizardLinks .stepDesc{
            padding:5px;
        }
        .bg-contain {
            /*background-color: #4ecdc4;
            background-image: url("<?php echo Yii::app()->getModule("survey")->assetsUrl ?>/images/oceco_background.png");*/
            background-position: center;
            background-repeat: repeat-x;
            background-attachment: fixed;
            min-height: 100vh;
            background-color: #4623C9;
        }
        .contain-offset {
            border: 1px solid #CCCCCC;
            padding: 0;
            min-height: 100px;
            margin-top: 10px;
            padding-bottom: 5px;
            background-color: #fff;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
            -webkit-box-shadow: 0px 0px 5px #000;
            -moz-box-shadow: 0px 0px 5px #000;
            box-shadow: 0px 0px 5px #000;
        }
        .banner-section {
            padding: 20px;
        }
        .body-section, .footer-section {
            /*padding: 30px 15px;*/
        }
        .text-body {
            font-size: 20px;
        }
        .header-title {
            text-transform: none;
            word-wrap: break-word;
            font-size: 48px;
        }
        .brands {
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
        }

        .brands__item {
            flex: 0 0 50%;
            list-style: none;
            text-align: center;
        }
        .brands__item img {
            width: 130px;
            height: 75px;
            object-fit: contain;
            mix-blend-mode: multiply;
        }

        @media (min-width: 700px) {
            .brands__item {
                flex: 0 0 33.33%;
            }
        }

        @media (min-width: 1100px) {
            .brands__item {
                flex: 0 0 25%;
            }
        }
        @media (min-width: 768px) and (max-width: 991px) {
            .header-title {
                font-size: 17px;
            }
        }
        @media (max-width: 767px) {
            .header-title {
                font-size: 17px;
            }
            .text-body {
                font-size: 14px;
            }
            .body-section, .footer-section {
                padding: 10px 5px;
            }
        }

        /* formWizard */
        @font-face{
            font-family: "montserrat";
            src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/montserrat-regular.woff") format("woff"),
            url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/montserrat-regular.ttf") format("ttf")
        }.mst{font-family: 'montserrat'!important;}

        @font-face{
            font-family: "CoveredByYourGrace";
            src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/CoveredByYourGrace.ttf")
        }.cbyg{font-family: 'CoveredByYourGrace'!important;}


        .switchTopButton{
            position: fixed;
            /*padding: 5px;*/
            right:-3px;
            font-size: 18px;

            border-radius: 20%;
            z-index:1;
        }

        .switchTopButton a, .switchTopButton span{
            font-size: 20px
        }


        #customHeader{
            margin-top: 0px;
        }
        #costumBanner{
            /* max-height: 375px; */
        }
        #costumBanner h1{
            position: absolute;
            color: white;
            background-color: rgba(0,0,0,0.4);
            font-size: 29px;
            bottom: 0px;
            padding: 20px;
        }
        #costumBanner h1 span{
            color: #eeeeee;
            font-style: italic;
        }
        #costumBanner img{
            min-width: 100%;
        }
        .btn-main-menu{
            background: <?php echo @$this->costum["colors"]["pink"]; ?>;
            border-radius: 10px;
            padding: 10px !important;
            color: white;
            cursor: pointer;
            border:3px solid transparent;
            font-size: 1.5em
            /*min-height:100px;*/
        }
        .btn-main-menuW{
            background: white;
            color: <?php echo @$this->costum["colors"]["pink"]; ?>;
            border:none;
            cursor:text ;
        }
        .btn-main-menu:hover{
            border:2px solid <?php echo @$this->costum["colors"]["pink"]; ?>;
            background-color: white;
            color: <?php echo @$this->costum["colors"]["pink"]; ?>;
        }
        .btn-main-menuW:hover{
            border:none;
        }
        @media screen and (min-width: 450px) and (max-width: 1024px) {
            .logoDescription{
                width: 60%;
                margin:auto;
            }
        }

        @media (max-width: 1024px){
            #customHeader{
                margin-top: -1px;
            }
        }
        #customHeader #newsstream .loader{
            display: none;
        }

        .mr-4{
            margin-right: 1em !important;
        }
        .monTitle{
            border-top: 1px dashed <?php echo @$this->costum["colors"]["pink"]; ?>;
            border-bottom: 1px dashed <?php echo @$this->costum["colors"]["pink"]; ?>;
            /*margin-top: -20px;*/
        }

        #btn-copylink {
            height: 0px;
            width: 40px;
            position: fixed;
            right: 0;
            top: 20%;
            z-index: 1000;
            /*transform: rotate(-90deg);
            -webkit-transform: rotate(-90deg);
            -moz-transform: rotate(-90deg);
            -o-transform: rotate(-90deg);
            filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);*/
        }
        #btn-copylink a {
            display: block;
            background:#00495d;
            height: 40px;
            padding-top: 7px;
            /*width: 155px;*/
            text-align: center;
            color: #fff;
            font-family: Arial, sans-serif;
            font-size: 17px;
            font-weight: bold;
            text-decoration: none;
            border-top-left-radius: 5px;
            border-bottom-left-radius: 5px;
        }
        #btn-copylink a:hover {
            background:#00495d;
        }

        .resumemodalul{
            max-height: 250px;
            overflow-y: auto;
        }

    </style>
    <script type="text/javascript">
        var sectionDyf = {};
    </script>
    <?php
    // var_dump((count($form["subForms"])>1 && $form["what"]=="Réseau de tiers-lieux") ? "titre" : "image");
    $bannerTitleStandalone = (count($form["subForms"])>1 && $form["what"]=="Réseau de tiers-lieux") ? "<h1 style='text-align:center;font-size:35px'>".$form["name"]."</h1>" : "";
    $bannerFooterStandalone = '<img src="https://www.communecter.org/upload/communecter/organizations/5ec3b895690864db108b46b8/album/footer-recensement.png" style="width:100%;text-align:center;display:inline-block;"/>';
    if (isset($blockCms["bannerTitleStandalone"])){
        $bannerTitleStandalone = $blockCms["bannerTitleStandalone"];
    }

    $cssJS = array(
        '/plugins/jQuery-Knob/js/jquery.knob.js',
        '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
        //'/plugins/jQuery-Smart-Wizard/styles/smart_wizard.css',
        // SHOWDOWN
        '/plugins/showdown/showdown.min.js',
        // MARKDOWN
        '/plugins/to-markdown/to-markdown.js'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);
    HtmlHelper::registerCssAndScriptsFiles(array(
        '/js/answer.js',
    ), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );

    $poiList = array();

    if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
        $poiList = PHDB::find(Poi::COLLECTION,
            array( "parent.".$this->costum["contextId"] => array('$exists'=>1),
                "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"],
                "type"=>"cms") );
    }
    ?>

    <div class="col-xs-12 bg-contain">
        <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-1 contain-offset no-padding">
            <div class="banner-section">
                <div class="row">
                    <?php
                    if(isset($parentForm["params"]["standAlone"]["leftImg"]) && !filter_var(    $parentForm["params"]["standAlone"]["leftImg"], FILTER_VALIDATE_BOOLEAN)){
                        ?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h1 class="header-title"><?= $bannerTitleStandalone  ?></h1>
                        </div>
                        <?php
                    }else{
                        ?>
                        <?php if (isset($parentForm["type"]) && $parentForm["type"] == "aap" ){ ?>

                            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                                <h1 class="header-title"><?= $bannerTitleStandalone  ?></h1>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12" >
                                <img class="img-responsive" src="<?php echo Yii::app()->getModule("survey")->assetsUrl ?>/images/aap_banner2.jpg" alt="Book Icon">
                            </div>
                        <?php }else { ?>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <img src="https://www.communecter.org/upload/communecter/organizations/5ec3b895690864db108b46b8/album/Banniere-site-recensement.png" style="width:100%;text-align:center;display:inline-block;"/>
                                <!-- <h1 class="header-title">-->
                                <?= $bannerTitleStandalone  ?>

                                <!--</h1> -->
                            </div>
                        <?php } ?>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <hr style = "margin: 0">
            <div class="body-section">
                <div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">
                    <?php
                    $color1 = "#E63458";
                    if(isset($this->costum["cms"]["color1"]))
                        $color1 = $this->costum["cms"]["color1"];
                    // if($canEdit)
                    //   echo "<a class='btn btn-xs btn-danger editBtn' href='javascript:;' data-key='color1' data-type='color'  data-path='costum.cms.color1' data-label='Couleur Principale '><i class='fa fa-pencil'></i></a>";
                    ?>

                    <?php $formSmallSize =  12; ?>
                    <div class="col-md-12 col-lg-<?php echo $formSmallSize?> no-padding "><br/>

                        <script type="text/javascript">
                            var formInputs = {};
                            var answerObj = <?php echo (!empty($answer)) ? json_encode( $answer ) : "null"; ?>;
                            var form = <?php echo (!empty($form)) ? json_encode( $form ) : "null"; ?>;
                        </script>

                        <div class="col-xs-12 coFormbody">
                            <?php
                            $wizardUid = (String) $form["_id"];
                            if($canEdit === true && $mode != "fa" && empty($answer["validated"]) ){
                                $nameMode = "mode read";
                                if($mode == "w")
                                    $nameMode = "mode write";

                                echo '<div class="col-xs-12 margin-bottom-15" id="modeSwitch" ></div>';
                                echo '<div id="bottomModeSwitch" class="switchTopButton" style="bottom:50%"></div>';

                                echo '<div id="arrowTop" class="switchTopButton" style="bottom:5%"></div>';

                            }

                            if($mode != "fa" && !empty($parentForm["answersTpl"])){
                                $params = [
                                    "parentForm"=>$parentForm,
                                    "el" => $el,
                                    "color1" => $color1,
                                    "canEdit" => $canEdit,
                                    "answer"=>$answer,
                                    "forms"=>$forms,
                                    "allAnswers"=>@$allAnswers,
                                    "what" => "dossiers",
                                    "wizid"=> $wizardUid
                                ];
                                echo $this->renderPartial($parentForm["answersTpl"],$params);
                            }

                            if( $mode == "fa" && $canEditForm === true ){
                                $params = [
                                    "parentForm"=>$parentForm,
                                    "canEditForm"=>$canEditForm,
                                    "mode" => $mode,
                                    "form" => $form,
                                    "el" => $el
                                ];
                                //echo $this->renderPartial("survey.views.tpls.forms.config",$params);
                            }
                            ?>
                            <div id="wizardcontainer">
                                <?php
                                if( isset($answer) && !empty($showForm) && $showForm === true  ) {
                                    //var_dump($canEditForm);exit;
                                    $params = [
                                        "parentForm"=>$parentForm,
                                        "form" => $form, //identicall to parentForm kept on refactor
                                        "forms"=>$forms,
                                        "el" => $el,
                                        "active" => "all",
                                        "color1" => @$this->costum["colors"]["dark"],
                                        "color2" => @$this->costum["colors"]["pink"],
                                        "canEdit" => $canEdit,
                                        "canEditForm" => $canEditForm,
                                        "canAdminAnswer" => $canAdminAnswer,
                                        "answer"=>$answer,
                                        "showForm" => $showForm,
                                        "mode" => $mode,
                                        "showWizard"=>true,
                                        "wizid"=> $wizardUid,
                                        "isNew" => @$isNew,
                                        "contextId" => @$contextId,
                                        "contextType" => @$contextType
                                    ];
                                    $tplstepWizard = "";

                                    if (isset($parentForm["tplstepWizard"])){
                                        $tplstepWizard = $parentForm["tplstepWizard"];
                                    }else{
                                        $tplstepWizard = "survey.views.tpls.forms.wizard";
                                    }
                                    echo $this->renderPartial($tplstepWizard , $params);
                                }
                                ?>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="col-xs-12">
                    <?= $bannerFooterStandalone  ?>
                </div>
            </div>
        </div>
    </div>

    <div id="btn-copylink">
        <a href="javascript:;" class="copylink tooltips" data-toggle="tooltip" data-placement="left" title="" data-original-title="Copier lien du formulaire"><i class="fa fa-copy"></i> </a>
    </div>

    <div id ="resumemodal" style="display: none">
        <h5>Vous avez répondu plusieurs fois au grand recensement</h5>
        <h6>Cliquez sur la réponse que vous souhaitez compléter / consulter</h6>

        <?php
        $baseurl = (!empty($el["costum"])) ? Yii::app()->getBaseUrl(true) . "/costum/co/index/slug/" . $el["slug"] : Yii::app()->getBaseUrl(true);
        $useranswers = PHDB::find(Form::ANSWER_COLLECTION, array("form" => (string)$parentForm["_id"], "user" => Yii::app()->session["userId"]));
        ?>
        <ul class="resumemodalul">
            <?php
            foreach ($useranswers as $ans => $usranswer){
                ?>
                <li>
                    <a href="<?= $baseurl."#answer.index.id." .$ans. ".mode.w.standalone.true" ?>" class="lbh" >
                        <span>Réponse liée <?= (isset($usranswer["links"]) && isset($usranswer["links"]["organizations"]) && count(array_keys($usranswer["links"]["organizations"]))==1 && isset($usranswer["links"]["organizations"][array_keys($usranswer["links"]["organizations"])[0]]["name"])) ? "au tiers-lieu <span style='font-weight:bold'>".$usranswer["links"]["organizations"][array_keys($usranswer["links"]["organizations"])[0]]["name"]."</span>" : "à aucun tiers-lieux" ?></span>
                        <span>,créée le <?= date("d/m/Y" , $usranswer["created"]  ) ?></span>

                    </a>
                </li>
                <?php
            }
            ?>
        </ul>
    </div>

    <script type="text/javascript">
        var useranswers=<?php echo json_encode($useranswers); ?>;
        var configDynForm = <?php echo json_encode(@$this->costum['dynForm']); ?>;
        var answerId = <?php echo json_encode((string)$answer['_id']); ?>;
        var mode = <?php echo json_encode($mode); ?>;
        var canAdminAnswer = <?php echo json_encode($canAdminAnswer); ?>;
        var elTest = <?php echo json_encode($el); ?>;
        var formlink = "";
        var toastinfohtml = $('#resumemodal').html();
        //information and structure of the form in this page
        var tplCtx = {};
        if (costum!= null && typeof costum.host=="undefined"){
            formlink = baseUrl + "/costum/co/index/slug/" +costum.slug+ "#answer.index.id." +answerId+ ".mode."+mode+".standalone.true";
        }else {
            formlink = baseUrl + "/#answer.index.id." +answerId+ ".mode."+mode+".standalone.true";
        }

        var strListMode = "";
        var strLowListMode = "";
        var strArrowTop = "";

        function reloadWizard(callback = null){
            coInterface.showLoader("#wizardcontainer");
            var reloadWizardData = {
                "answerId" : "<?php echo (string)$answer["_id"] ?>",
                "formId" : "<?php echo (string)$form["_id"] ?>",
                "forms": <?php echo json_encode($forms) ?>,
                "active" : <?php echo json_encode("all") ?>,
                "color1" : <?php echo json_encode(@$this->costum["colors"]["dark"]) ?>,
                "color2" : <?php echo json_encode(@$this->costum["colors"]["pink"]) ?>,
                "canEdit" : <?php echo json_encode($canEdit) ?>,
                "canEditForm" : <?php echo json_encode($canEditForm) ?>,
                "canAdminAnswer" : <?php echo json_encode($canAdminAnswer) ?>,
                "showForm" : <?php echo json_encode($showForm) ?>,
                "mode" : <?php echo json_encode($mode) ?>,
                "showWizard": true,
                "wizid": <?php echo json_encode($wizardUid) ?>,
                "contextId" : <?php echo json_encode(@$contextId) ?>,
                "contextType" : <?php echo json_encode(@$contextType) ?>,
                "standalone" : true
            }

            if (reloadWizardData.contextType == "") {
                delete reloadWizardData.contextType;
            }
            if (reloadWizardData.contextId == "") {
                delete reloadWizardData.contextId;
            }

            ajaxPost(
                "#wizardcontainer",
                baseUrl+"/survey/answer/reloadwizard",
                reloadWizardData,
                function(){
                    if(typeof callback == "function")
                        callback();
                }
                ,"html");
        }

        function unlockStep(step){
            if(notNull(form['subForms']) && form['subForms'].indexOf(step) > 0){
                $.each(form['subForms'] , function(index , value){
                    if(index < form['subForms'].indexOf(step) + 1 ){
                        $('#'+value+'stepper a').addClass("done");
                        $('#'+value+'stepper a').attr("onclick" , "showStepForm('#"+value+"')");
                    }
                });
                showStepForm("#"+form['subForms'][form['subForms'].indexOf(step)]);
            }
        }

        jQuery(document).ready(function() {
            mylog.log("render","modules/survey/views/tpls/forms/formWizardRecensementLastStep.php");

            $('#popupRecensement .custom-modal-header .close').trigger("click");
            $("#menuTopLeft img").parent().attr("href",location.hash);



            if(useranswers!=null && Object.keys(useranswers)>1){
                bootbox.dialog({
                    message : toastinfohtml,
                    closeButton: false
                });
            }


            if(typeof pageProfil != "undefined" && typeof pageProfil.form != "undefined" && pageProfil.form != null){
                strListMode = '';
            } else {
                strArrowTop= '<a class="topButton btn btn-primary tooltips"><i class="fa fa-chevron-up" data-toggle="tooltip" data-placement="left" data-original-title="Début du formulaire"></i></a>';
                if(mode == "w"){
                    strListMode = '';
                    strLowListMode = '';
                }
                if(mode == "r"){
                    strListMode = '<a href="#answer.index.id.'+answerId+'.mode.w.standalone.true" class="lbh btn btn-primary pull-right"><i class="fa fa-pencil"></i> Modifier</a>';
                    strLowListMode = '<a href="#answer.index.id.'+answerId+'.mode.w.standalone.true" class="lbh btn btn-primary tooltips" data-toggle="tooltip" data-placement="left" data-original-title="Modifier"><i class="fa fa-pencil"></i></a>';
                }
            }

            $("#modeSwitch").html(strListMode);

            if(strLowListMode!="" && strArrowTop!=""){
                $(document).on('scroll',function(){
                    var scrollTop = $(window).scrollTop();
                    if (scrollTop > 100) {
                        $("#bottomModeSwitch").html(strLowListMode);
                        coInterface.bindLBHLinks();
                    }
                    else{
                        $("#bottomModeSwitch").empty();
                    }

                    if(scrollTop > 500){
                        $("#arrowTop").html(strArrowTop);
                        coInterface.bindLBHLinks();
                    }else{
                        $("#arrowTop").empty();
                    }

                });
            }
            <?php // } ?>

            remerciementFin=function(){
                var contentRem=
                "<h2>Remerciements</h2>"+
                "<p style='font-weight:bold;'>Merci pour votre participation à ce grand recensement 2023 ! Ces informations nous seront grandement utiles pour faire avancer et développer les tiers-lieux partout en France.</p>"+
                "<p style='font-weight:bold;'>Si vous avez un autre tiers-lieu à recenser merci de suivre ce lien : <a style='color:#2296f3;' href='#answer.index.id.new.form.63e0a8abeac0741b506fb4f7.mode.w.standalone.true' class='lbh'>Générer une nouvelle réponse</a></p>"+
                "<p style='font-weight:bold;'>Si vous souhaitez vous abonner à la newsletter de France Tiers-Lieux : <a style='color:#2296f3;' href='https://mailchi.mp/francetierslieux/inscription-newsletter' target='_blank'>https://mailchi.mp/francetierslieux/inscription-newsletter</a></p>"+
                "<p style='font-weight:bold;'>Découvrez les travaux de l'Association Nationale des Tiers-Lieux, cofondatrice de France Tiers-Lieux : <a style='color:#2296f3;' href='https://tiers-lieux.fr/' target='_blank'>https://tiers-lieux.fr/</a></p>"+
                "<p><i class='fa fa-info'></i> Toutes vos réponses ont été enregistrées, vous pouvez fermer la fenêtre. Un mail vous a été envoyé contenant un lien permettant d'accéder au récapitulatif de votre réponse.</p>";
                bootbox.confirm({
                   message : contentRem,
                   closeButton : false,
                   buttons : {
                        cancel : {className:"hidden"},
                        confirm : {className:"btn-primary text-white"}
                    },
                    callback : function(result){
                        urlCtrl.loadByHash("#welcome");

                    }
                });
                var answId = (typeof answerObj.previousAnswerId!="undefined") ? answerObj.previousAnswerId : answerObj._id.$id ;
                var paramsmailRemerciement = {
                    tpl : "basic",
                    tplObject : "[Recensement Tiers-Lieux 2023] - Accusé de réception et lien d'accès",
                    tplMail : userConnected.email,
                    html: "Nous accusons réception de votre réponse au recensement des tiers-lieux 2023. </br>Nous vous rappelons que vous pouvez accéder à vos réponses (tronc commun et questionnaire spécifique régional) jusqu’au 10 mai en cliquant sur le lien ci-dessous et sur votre Tableau de Bord depuis votre compte.",
                    btnRedirect : {
                        hash : "#answer.index.id." + answId + ".mode.w.standalone.true",
                        label : "Accéder au formulaire"
                    }
                };
                mylog.log('azee', paramsmailRemerciement);
                ajaxPost(
                    null,
                    baseUrl+"/co2/mailmanagement/createandsend",
                    paramsmailRemerciement,
                    function(data){
                        // toastr.success("");
                    }
                );

                coInterface.bindLBHLinks();

                ajaxPost('#modal-dashboard-account', baseUrl+'/'+moduleId+'/default/render?url=co2.views.person.dashboard',
                    {view : "costum.views.custom.franceTierslieux.accountModal"},
                    function(){});
            }

            $("#arrowTop").off().on("click",function(){
                scrollintoDiv("wizardLinks",2000);
            });

            $("#btn-copylink").off().on("click", function () {
                var sampleTextarea = document.createElement("textarea");
                document.body.appendChild(sampleTextarea);
                sampleTextarea.value = formlink;
                sampleTextarea.select();
                document.execCommand("copy");
                document.body.removeChild(sampleTextarea);
                toastr.success('lien du formulaire copié');
            });

            if(location.hash.indexOf("#answer.index.id.new")>=0) {
                //toastr["info"](toastinfohtml, "Reprendre");
            }

            if(typeof pageProfil != "undefined" && typeof pageProfil.form != "undefined" && pageProfil.form != null){
                pageProfil.form.events.answers(pageProfil.form);
            }
            if(location.href.indexOf(".ask.false")){
                lhref = location.href.split(".ask.false")[0];
                history.pushState(null,null,lhref);
            }
        });
    </script>
<?php } ?>
