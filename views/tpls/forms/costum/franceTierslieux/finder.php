<?php if($answer){ 
	
	$keyTpl = "finder";
		
		$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

		$elementType=[
					Organization::COLLECTION => "Organization",
					Person::COLLECTION 		 => "Person",
					Event::COLLECTION 		 => "Event",
					Project::COLLECTION 	 => "Project",
					News::COLLECTION 		 => "News",
					City::COLLECTION 		 => "City",
					Thing::COLLECTION 		 => "Thing",
					Poi::COLLECTION 		 => "Poi",
					Classified::COLLECTION   => "Classified",
					Product::COLLECTION 	 => "Product",
					Service::COLLECTION   	 => "Service",
					Survey::COLLECTION   	 => "Survey",
					Bookmark::COLLECTION   	 => "Bookmark",
					Proposal::COLLECTION   	 => "Proposal",
					Room::COLLECTION   	 	 => "Room",
					Action::COLLECTION   	 => "Action",
					Network::COLLECTION   	 => "Network",
					Url::COLLECTION   	 	 => "Url",
					Circuit::COLLECTION   	 => "Circuit",
					Risk::COLLECTION   => "Risk",
					Badge::COLLECTION   => "Badge"
			];
		
		$paramsData = [ 
			"type" => Organization::COLLECTION,
			"filter"   => [],
		    "notSourceKey" => true,
		    "myContacts"   => false,
		    "elementLabel" => "Elément",
		    "buttonLabel" => "Rechercher et ajouter",
		    "placeholderSearchField" => "Saisir le nom de l'élément que vous recherchez",
		    "field" => "element",
		    "multiple" => true,
		    "addNew" => false,
		    "invite" => false,
		    "linkToAnswer" => false,
		    "singleAnswerPerElement" => false,
		    "msgSingleAnswerPerElement" => "",
		    "redirectSingleAnswerPerElement" => "Accueil",
		    "editElement" =>false,
		    "open" =>true
		];

		if(isset($parentForm["params"][$kunik])){
			foreach ($paramsData as $e => $v) {
		        if ( isset($parentForm["params"][$kunik][$e]) ) {
		            $paramsData[$e] = $parentForm["params"][$kunik][$e];
		        }
		    }
		}
	?>	
<style type="text/css">
    .population-elt-finder .popup-address{
    	font-size: 14px;
    }
    .population-elt-finder .type-element{
    	font-size: 14px;
    }	

</style>

	<label>
		<h4><?php echo $label.$editQuestionBtn.$editParamsBtn ?></h4>
	</label>
	<?php
	if(!empty($info)){ 
        ?>
        <br/>
    	<small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
    	<?php
    	}
    	?> 
    <div class="finder-<?php echo $paramsData['field'] ?>">
        <div class='col-xs-12'><span  class='col-xs-12 hidden text-red errorForm'></span></div>
        	<input type="hidden" id="<?php echo $paramsData['field'] ?>" name="<?php echo $paramsData['field'] ?>"/>
        		<span class='error bg-warning' style='display:none'></span>
         <div class='form-list-finder'>
        </div>
    </div> 
	<div id="finderSelectHtml" style="max-height:480px;">
		<input class="form-group form-control" type="text" id="populateFinder" placeholder="<?php echo $paramsData["placeholderSearchField"] ?>"/>
		<div id="list-finder" style="display: none;">
		    <div id="list-finder-selected" style="max-height:200px; overflow-y:auto"></div>
		    <hr/><div id="list-finder-selection" style="height:200px; overflow-y:auto" class="shadow2"><p><i class="fa fa-spin fa-spinner"></i> <?php echo Yii::t("common","loading")?>...</p></div>
		</div>    
    </div>
	

<script type="text/javascript">

var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answer["answers"][$kunik])) ? $answer["answers"][$kunik] : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
var mode = "<?php echo $mode ?>";
var step=<?php echo json_encode($form["id"]) ?>;
var ansKey= <?php echo json_encode($kunik) ?> ;
const validateFormId = <?php echo json_encode($form["id"]); ?>;
const validateKunik = <?php echo json_encode($kunik); ?>;
var finderElementChecked = $(`li#question${validateKunik} div.form-list-finder div.element-finder`).length;
var parentFormId=<?php echo json_encode((string)$parentForm["_id"]); ?>;


var answers= <?php echo json_encode( (isset($answer["answers"][$form["id"]][$kunik])) ? $answer["answers"][$form["id"]][$kunik] :null ); ?>;


		saveElementFinder<?php echo $kunik ?>=function(id,name,type,img){	
			var valData = {
				"id" : id,
				"name" : name,
				"type" : type,
				"img" : img
			};

			var savedInfo={};
            savedInfo[id]=valData;

            var answer={};

			answer.path = (sectionDyf.<?php echo $kunik ?>ParamsData.multiple=="false") ? "answers.<?= $form["id"] ?>.<?= $kunik ?>" : "answers.<?= $form["id"] ?>.<?= $kunik ?>."+id;
			answer.collection = "answers" ;
	        answer.id = "<?php echo $answer["_id"]; ?>";
	        answer.value = (sectionDyf.<?php echo $kunik ?>ParamsData.multiple=="false") ? savedInfo : valData;

	        // save the element in the answer
	        dataHelper.path2Value(answer , function(params) { 
	           
				finderElementChecked = $(`li#question${validateKunik.replace('finder', '')} div.form-list-finder div.element-finder`).length;
	            reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
	            //alert("okok");
	            if(typeof stepValidationReload<?php echo $form["id"] ?> == "function"){
                        stepValidationReload<?php echo $form["id"] ?>();   
                }
                toastr.success(trad['saved']);

	        });

	        // link the element to the answer
	        if(sectionDyf.<?php echo $kunik ?>ParamsData.linkToAnswer=="true" || sectionDyf.<?php echo $kunik ?>ParamsData.linkToAnswer==true){
	            answer.path="links."+type+"."+id;
	            answer.value = {"name" : name,"type":type};
	            dataHelper.path2Value(answer , function(params) { 
					if(parentFormId=="63e0a8abeac0741b506fb4f7"){
     					ajaxPost(
         						null,
         					baseUrl+"/costum/francetierslieux/setidrecensement/id/"+id+"/type/"+type,
         					{},
         					function(resRecensement){
     							mylog.log("res id Recensement",resRecensement);
        						ajaxPost(
                 					null,
                 					baseUrl+"/costum/francetierslieux/addinseedensity",
                 					{"id":id},
                 					function(inseeDensity){
             							mylog.log("res inseeDensity",inseeDensity);
                 					},
                 					null,
                 					null,
                 					{
                 						async: false
                 					}
             					);
         					},
         					null,
         					null,
         					{
         						async: false
         					}
     					);
					}
	            });
	        }
        };

    finder.showPanel= function(keyForm, typeSearch, open, multiple,field = null){
		mylog.log("finder.showPanel", keyForm, typeSearch, open, multiple);
		finder.selectedItems[keyForm]={};
		titleForm="Sélectionner dans la liste";
		if(!notNull(multiple) && !multiple)
			titleForm+="(Un seulement)";
		
		
		    //setTimeout(function(){
		    finder.finderPopulation[keyForm]={};
			if(userId != "" && typeof myContacts != "undefined" && myContacts == null){
				ajaxPost(
					null,
					baseUrl+"/"+moduleId+"/person/getcontacts",
					{"id" : userId},
					function(data){
						myContacts = data;
					},
					null,
					"json",
					{
						async: false
					}
				);
			}
		    if(typeof myContacts != "undefined" && notNull(myContacts) && finder.initContacts[keyForm]){
		    	$.each(typeSearch, function(e, type){
		    		if(typeof myContacts[type] != "undefined"){
		    			$.each(myContacts[type], function(e, v){
		    				//mylog.log()
		    				if(	typeof finder.toBeValidated[keyForm] == "undefined" ||
		    					finder.toBeValidated[keyForm] == null ){
		    					finder.finderPopulation[keyForm][e]={
			    					"name": v.name,
			    					"type": type,
			    					"profilThumbImageUrl":v.profilThumbImageUrl
			    				};
		    				} else if( finder.toBeValidated[keyForm] == true &&
		    							( 	typeof v.source == "undefined" ||
			    							(	typeof v.source != "undefined" &&
				    							v.source != null &&
				    							( typeof v.source.toBeValidated == "undefined" ||
				    								v.source.toBeValidated == null ||
					    							( 	typeof costum != "undefined" &&
						    							costum != null &&
						    							typeof costum.slug != "undefined" &&
						    							costum.slug != null &&
						    							( 	typeof v.source.toBeValidated[costum.slug] == "undefined" ||
							    							v.source.toBeValidated[costum.slug] == null ||
							    							v.source.toBeValidated[costum.slug] != true ) ) ) ) ) ) {

		    					finder.finderPopulation[keyForm][e]={
			    					"name": v.name,
			    					"type": type,
			    					"profilThumbImageUrl":v.profilThumbImageUrl
			    				};
		    				}



		    				if(type=="events"){
		    					finder.finderPopulation[keyForm][e].startDate=v.startDate;
		    					finder.finderPopulation[keyForm][e].endDate=v.endDate;
		    				}
		    			});
		    		}
		    	});

		    }
		    finder.populateFinder(keyForm, finder.finderPopulation[keyForm], multiple, true);
		    if(typeof finder.initBySearch[keyForm] != "undefined" && finder.initBySearch[keyForm]){
		    	finder.searchAndPopulateFinder(keyForm,"", typeSearch, multiple,field);
		    }
		    $("#populateFinder").keyup(function(){
		    	mylog.log("finder.showPanel keyup");
		    	if($(this).val().length < 3){
		    		finder.filterPopulation($(this).val());
		    	}else{
		    		if(notNull(open) && open){
		    			finder.filterPopulation($(this).val());
		    			finder.searchAndPopulateFinder(keyForm,$(this).val(), typeSearch, multiple,field);
		    		}else{
		    			finder.filterPopulation($(this).val());
		    		}
		    	}
		    });
		

		if(typeof finder.invite[keyForm] != "undefined" && finder.invite[keyForm] != null && finder.invite[keyForm] === true){
			mylog.log("finder.#finderSelectHtml", $("#finderSelectHtml").length);
			dialog.on('shown.bs.modal', function(e){
				mylog.log("finder.#finderSelectHtml HERE", $("#finderSelectHtml").length);
				 var paramsInvite = {
				 	container : "#finderSelectHtml #form-invite"
				 };
			 	 inviteObj.init(paramsInvite);
				 inviteObj.formInvite(function(data){

				 	finder.finderPopulation[keyForm][data.id]={
    					"name": data.name,
    					"email": data.mail,
    					"type": "citoyens",
    					"profilThumbImageUrl": modules.co2.url + "/images/thumb/default_citoyens.png"
    				};
    				finder.selectedItems[keyForm][data.id]=finder.finderPopulation[keyForm][data.id];

				 	var str ="<div class='population-elt-finder population-elt-finder-"+data.id+" col-xs-12' data-value='"+data.id+"'>"+
								'<div class="checkbox-content pull-left">'+
									'<label>'+
					    				'<input type="checkbox" class="check-population-finder checkbox-info" data-value="'+data.id+'">'+
					    				'<span class="cr"><i class="cr-icon fa fa-check"></i></span>'+
									'</label>'+
								'</div>'+
								"<div class='element-finder element-finder-"+data.id+"'>"+
									'<img src="'+ modules.co2.url + "/images/thumb/default_citoyens.png" +'" class="thumb-send-to pull-left img-circle" height="40" width="40">'+
									'<span class="info-contact pull-left margin-left-20">' +
										'<span class="name-element text-dark text-bold" data-id="'+data.id+'">' + data.name + '</span>'+
										'<br/>'+
										'<span class="type-element text-light pull-left">' + trad[data.type]+ '</span>'+
									'</span>' +
								"</div>"+
							"</div>";

					$("#form-invite").addClass("hidden");
				 	$("#list-finder-selected").append(str);
				 	$(".check-population-finder[data-value='"+data.id+"'").trigger("click");
				 	$("#list-finder-selection").removeClass("hidden");

				 	if(finder.invite[keyForm]!=null && typeof finder.invite[keyForm].callback=="function"){
				 		finder.invite[keyForm].callback(data);
				 	}

				 	return true;
				 });

			});
		}


	};    

    finder.bindSelectItems = function(keyForm, multiple){
		//mylog.log("finder.bindSelectItems", multiple);
		//alert("yuo");
		$(".modal-footer .btn-success").addClass("disabled").css("color","white").html("Valider");
		$(".population-elt-finder").off().on("click", function(e){
			if(e.target.className!="cr-icon fa fa-check" && e.target.className!="check-population-finder checkbox-info")
				$(".check-population-finder[data-value='"+$(this).data("value")+"'").trigger("click");
		});
		$(".check-population-finder").off().on("click", function(){
			if($(this).is(":checked")){
				if(!multiple){
					finder.selectedItems[keyForm]={};
					$("#list-finder-selected").html("");
				}
					finder.selectedItems[keyForm][$(this).data("value")]=finder.finderPopulation[keyForm][$(this).data("value")];

				$(".population-elt-finder-"+$(this).data("value")).prependTo("#list-finder-selected");
				//Direct addInForm
				$("#list-finder-selected").hide();
			    finder.addSelectedToForm(keyForm,multiple);
			    // end
				$("#list-finder-selected").append('<a href="javascript:;" onclick="finder.addSelectedToForm(`'+keyForm+'`,`'+multiple+'`)" class="btn btn-primary">Valider ce tiers-lieu</a>');
				$('#finderSelectHtml .container-addNew').addClass("hidden");
				$(".modal-footer .btn-success").removeClass("disabled");
			}else{
				delete finder.selectedItems[keyForm][$(this).data("value")];
				$(".population-elt-finder-"+$(this).data("value")).prependTo("#list-finder-selection");
				$('#finderSelectHtml .container-addNew').removeClass("hidden");
				$(".modal-footer .btn-success").addClass("disabled");
				$("#list-finder-selected").html('');
			}
		});
	};        

	finder.addInForm<?php echo $kunik ?> = function(keyForm, id, type, name, img, data,firstLoad=false){
		mylog.log("finder.addInForm", keyForm, id, type, name, img, data);
		mylog.log("firstload finder addInForm", firstLoad);
		//alert("ok");
		imgUrl= (typeof img!="undefined") ? baseUrl + img : modules.co2.url + "/images/thumb/default_"+type+".png";

		// --------------------- Finir editElement -----------------------------
		var nameElem=(sectionDyf.<?php echo $kunik ?>ParamsData.editElement==true || sectionDyf.<?php echo $kunik ?>ParamsData.editElement=="true") ?
		name + ' <a href="javascript:;" style="font-weight:100;font-size:small;" class="text-red updateElementInfo<?php echo $kunik ?>" data-id="'+id+'" data-type="'+type+'"><i class="fa fa-pencil"></i>Mettre à jour</a>' : name;

		// (sectionDyf.<?php echo $kunik ?>ParamsData.editElement==true || sectionDyf.<?php echo $kunik ?>ParamsData.editElement=="true") ?
		// '<a href=`javascript:;` onclick=`dyFObj.editElement('+type+', '+id+','+null+','+JSON.stringify(costum.typeObj.organizations.dynFormCostum)+');return false;`>'+name+'</a>' : name;
		//img= (img != "") ? img : modules.co2.url + "/images/thumb/default_"+type+".png";
		var str="";
		str="<div class='col-xs-12 element-finder element-finder-"+id+" shadow2 padding-10'>"+
					'<img src="'+ imgUrl+'" class="img-circle pull-left margin-right-10" height="35" width="35">'+
					'<span class="info-contact pull-left margin-right-5">' +
						'<span class="name-contact text-dark text-bold">' + nameElem + '</span>'+
						'<br/>'+
						'<span class="cp-contact text-light pull-left">' + trad[((type == "citoyens" ) ? "citizens" : type)]+ '</span>'+
					'</span>';
		if(mode=="w"){			
		str +=			'<button class="bg-red text-white pull-right" style="border: none;font-size: 15px;border-radius: 6px;padding: 5px 10px !important;" onclick="finder.removeFromForm<?php echo $kunik ?>(\''+keyForm+'\', \''+id+'\',\''+type+'\')"><i class="fa fa-times"></i></button>';
		}			
		str+=	"</div>";
		$(".finder-"+keyForm+" .form-list-finder").append(str);

		$(".updateElementInfo<?php echo $kunik ?>").off().on("click",function(){
			// prevent open form rester finder Object
			var dynCostumUpdate=costum.typeObj.organizations.dynFormCostum;
			dynCostumUpdate.onload.actions.hide.similarLinkcustom=1;
			dynCostumUpdate.onload.actions.setTitle="Mettez à jour la fiche d'identité de votre tiers-lieu";
			// prevent open form rester finder Object
			
	        dyFObj.editElement($(this).data("type"),$(this).data("id"),null,dynCostumUpdate, {}, "afterLoad");
	        reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
	        
		});	

		finder.object[keyForm]={};
		finder.object[keyForm][id]={"type" : type, "name" : name, "img" : img};


		if(notNull(finder.finderPopulation[keyForm]) && notNull(finder.finderPopulation[keyForm][id]) && notNull(finder.finderPopulation[keyForm][id].email)){
			finder.object[keyForm][id].email = finder.finderPopulation[keyForm][id].email;
		}

		if(notNull(finder.roles) && notNull(finder.roles[keyForm])){
			finder.object[keyForm][id].roles = finder.roles[keyForm];
		}

		if(notNull(finder.search) && notNull(finder.search[keyForm]) && notNull(finder.search[keyForm].filterBy)){
			
			mylog.log("filterBy split", split, finder.selectedItems[keyForm][id][finder.search.filterBy], notNull( finder.selectedItems[keyForm][id][finder.search.filterBy]) );
			if(notNull( finder.selectedItems[keyForm][id][finder.search.filterBy] ) ) {
				var fBy = {} ;
				var split = id.split(".");
				fBy[finder.search[keyForm].filterBy] = finder.selectedItems[keyForm][id][finder.search.filterBy] ;
				finder.object[keyForm][split[0]]=Object.assign({}, finder.object[keyForm][id], fBy);
				delete finder.object[keyForm][id];
			}
		}

		var answers= <?php echo json_encode( (isset($answer["answers"][$form["id"]][$kunik])) ? $answer["answers"][$form["id"]][$kunik] :null ); ?>;

		mylog.log("answers",answers);

		// if(notNull(answers) && (typeof answers[id]=="undefined" || Object.keys(answers[id])==0)){
        if((sectionDyf.<?php echo $kunik ?>ParamsData.singleAnswerPerElement=="true" || sectionDyf.<?php echo $kunik ?>ParamsData.singleAnswerPerElement==true) && (sectionDyf.<?php echo $kunik ?>ParamsData.linkToAnswer=="true" || sectionDyf.<?php echo $kunik ?>ParamsData.linkToAnswer==true)){
        	        
        	        var answerId="<?php echo (string)$answer["_id"] ?>";
						ajaxPost(
			                null,
							baseUrl+"/costum/cressreunion/answerlink",
							{form:form._id.$id,
							currentAnswer:answerId,
							linkedElementType:type,
							linkedElementId : id
						    },
							function(data){
								var dataLinkedAnsw=data;
								if(Object.keys(dataLinkedAnsw.data).length>0){
                                var redirectVal = {
                                	"Accueil" : "#welcome",
                                	"Page précédente" : urlBackHistory,
                                	"Réponse existante" : "#answer.index.id."+Object.keys(dataLinkedAnsw.data)[0]+".mode.w"
                                } 


								var redirectAfterMsg = redirectVal[sectionDyf.<?php echo $kunik ?>ParamsData.redirectSingleAnswerPerElement];
								//alert(redirectAfterMsg);
								
								var specMsg = (typeof sectionDyf.<?php echo $kunik ?>ParamsData.msgSingleAnswerPerElement!="undefined" && sectionDyf.<?php echo $kunik ?>ParamsData.msgSingleAnswerPerElement!="") ? sectionDyf.<?php echo $kunik ?>ParamsData.msgSingleAnswerPerElement : "Un questionnaire a déjà été rempli et enregistré pour cette structure."
								
								// mylog.log("objectkeys",Object.keys(dataLinkedAnsw.data));
								
								bootbox.dialog({
								            closeButton: false,
								            title: "Une réponse liée à votre structure existe déjà",
								            message: specMsg,
								            buttons: {
								                success:{
								                    label: "Ok",
								                    callback: function(){
								                    	urlCtrl.loadByHash(redirectAfterMsg);

								                    }
								                }
								            }       
								        });
							    }else if(firstLoad==false){
                                    saveElementFinder<?php echo $kunik ?>(id,name,type,img);
							    }


							});		
        }						
		else if(firstLoad==false){	
            saveElementFinder<?php echo $kunik ?>(id,name,type,img);
		}
		
		
	};	

			

	finder.removeFromForm<?php echo $kunik ?> = function(keyForm, id, type){
		//mylog.log("finder.removeFromForm", keyForm, id);
        var nbAnswer=$(".finder-"+keyForm+" .form-list-finder .element-finder").length;

		$(".finder-"+keyForm+" .form-list-finder .element-finder-"+id).remove();
		delete finder.object[keyForm][id];
        

		var answer={};
		answer.path = nbAnswer>1 ? "answers.<?= $form["id"] ?>.<?= $kunik ?>."+id : "answers.<?= $form["id"] ?>.<?= $kunik ?>";
		answer.collection = "answers" ;
		answer.value="";
	    answer.id = "<?php echo $answer["_id"]; ?>";
	    dataHelper.path2Value(answer , function(params) { 
	            
	            if(typeof stepValidationReload<?php echo $form["id"] ?> == "function") {
                        stepValidationReload<?php echo $form["id"] ?>();
                }
                toastr.success(trad['deleted']);
	        });

	    if(sectionDyf.<?php echo $kunik ?>ParamsData.linkToAnswer=="true" || sectionDyf.<?php echo $kunik ?>ParamsData.linkToAnswer==true){
	            answer.path="links."+type+"."+id;
	            answer.value = "";
	            dataHelper.path2Value(answer , function(params) { 

	            });
	        }
	    // gérer le multiple : true    
	    $("#populateFinder").show();    
	};

	finder.addSelectedToForm=  function(keyForm, multiple){
		mylog.log("finder.addSelectedToForm", keyForm, multiple);
		// var kunik = $("."+keyForm+"finder").parent().data("key");
		// kunik="<?= $keyTpl ?>"+kunik ;
		if(Object.keys(finder.selectedItems[keyForm]).length > 0){
			if(!multiple){
				finder.object[keyForm]={};
				$(".finder-"+keyForm+" .form-list-finder").html("");
			}
			$.each(finder.selectedItems[keyForm], function(e, v){
				typeCol=(typeof v.collection != "undefined") ? v.collection : v.type; 
				window["finder"]["addInForm<?= $kunik ?>"](keyForm, e, typeCol, v.name, v.profilThumbImageUrl);
			});
			if(typeof finder.callback[keyForm] != "undefined") finder.callback[keyForm](finder.selectedItems[keyForm]);
		}
		if((sectionDyf.<?php echo $kunik ?>ParamsData.editElement==true || sectionDyf.<?php echo $kunik ?>ParamsData.editElement=="true") && (sectionDyf.<?php echo $kunik ?>ParamsData.multiple==false || sectionDyf.<?php echo $kunik ?>ParamsData.multiple=="false")){
			var idEdit=Object.keys(finder.selectedItems[keyForm])[0];
			var typeEdit=finder.selectedItems[keyForm][idEdit].collection;
			var dynCostumUpdate=costum.typeObj.organizations.dynFormCostum;
			dynCostumUpdate.onload.actions.hide.similarLinkcustom=1;
			dynCostumUpdate.onload.actions.setTitle="Mettez à jour la fiche d'identité de votre tiers-lieu";
			
	        dyFObj.editElement(typeEdit,idEdit,null,dynCostumUpdate,{},"afterLoad");
	        reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
		}
	};

	bindAddNew<?php echo $kunik ?> = function(){
		mylog.log("finder bindAddNews");
		$(".add-new-element").off().on("click", function(){
			var filters = $(this).data("filters");
			var field = $(this).data("field");
			mylog.log("filters",filters);
			//filters=JSON.parse(filters);
			//var category = $(this).data("category");
			var type = $(this).data("type");

			var customForm = {
					"beforeBuild" : {
			            "properties" : {
			                        
			            }    
					}

				};
			//mylog.log("category",category);
			
			if(typeof filters!="undefined" &&  typeof filters=="object" && Object.keys(filters).length>0){
				mylog.log("filters",filters);
				$.each(filters,function(k,v){
					customForm.beforeBuild.properties[k] = {
						inputType : "hidden",
						label : k,
						value : v,
						class :" form-control"
					};
					if(k=="tags"){
						customForm.beforeBuild.properties[k].inputType="tags";
					}
					else if(k=="source.key"){
						delete customForm.beforeBuild.properties[k];
					}
				});

			}

			
				customForm.afterSave = function(data){
					mylog.log("afterSave reference", data);
					$("#ajax-modal").modal('hide');
					if(field=="thirdPlace"){
						ajaxPost(
							null,
							baseUrl+"/"+moduleId+"/admin/setsource/action/add/set/reference",
							{id:data.id,type:data.map.collection,sourceKey:"franceTierslieux"},
							function(data){
								mylog.log("reférencé!",data);
								toastr.success("Tiers-lieu référencé sur Ftl");
							}
							
						);

						ajaxPost(
							null,
							baseUrl+"/"+moduleId+"/admin/setsource/action/add/set/reference",
							{id:data.id,type:data.map.collection,sourceKey:"tiersLieux"},
							function(data){
								mylog.log("reférencé!",data);
								toastr.success("Tiers-lieu référencé sur tiersLieux");
							}
							
						);
					}
					
					finder.addInForm<?php echo $kunik ?>(sectionDyf.<?php echo $kunik ?>ParamsData.field,data.id,data.map.collection,data.map.name,data.map.profilThumbImageUrl);	

					if(costum!=null){
						if(typeof costum[costum.slug].organizations !="undefined" && typeof costum[costum.slug].organizations.afterSave=="function"){
							costum[costum.slug].organizations.afterSave(data);
						}
					}
					
				};

				mylog.log("customForm",customForm);

				var extendedForm=customForm;

				if(typeof costum!="undefined" && costum!=null && costum.typeObj!="undefined" && costum.typeObj[type]!="undefined" && costum.typeObj[type].dynFormCostum!="undefined") 
					extendedForm=$.extend(true,costum.typeObj[type].dynFormCostum,customForm);
			
			var type = type.slice(0, -1);
		

			mylog.log("extendedForm",extendedForm);
			dyFObj.openForm(type,"afterLoad",null, null, extendedForm);
			reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
			$(".bootbox").modal('hide');
				
		});



	};

$(document).ready(function() { 
	mylog.log("answers ready",answers);
	mylog.log("paramsData<?php echo $kunik?>",sectionDyf.<?php echo $kunik ?>ParamsData);


        finder.populateFinder = function(keyForm, obj, multiple, first){
		mylog.log("finder.populateFinder", keyForm, obj, multiple, first);
		str="";
		if(first && typeof finder.object[keyForm][userId] == "undefined" && typeof finder.initMe[keyForm] != "undefined" && finder.initMe[keyForm]){
			img= (userConnected.profilThumbImageUrl != "") ? baseUrl + userConnected.profilThumbImageUrl : modules.co2.url + "/images/thumb/default_citoyens.png";
			if(typeof finder.finderPopulation[keyForm][userId]=="undefined"){
				finder.finderPopulation[keyForm][userId]={
					name:userConnected.name + ' ('+tradDynForm.me+')',
					type:"citoyens",
					profilThumbImageUrl:userConnected.profilThumbImageUrl
				};
			}
			str+="<div class='population-elt-finder population-elt-finder-"+userId+" col-xs-12' data-value='"+userId+"'>"+
					'<div class="checkbox-content pull-left">'+
						'<label>'+
		    				'<input type="checkbox" class="check-population-finder checkbox-info" data-value="'+userId+'">'+
		    				'<span class="cr"><i class="cr-icon fa fa-check"></i></span>'+
						'</label>'+
					'</div>'+
					"<div class='element-finder element-finder-"+userId+"'>"+
						'<img src="'+img+'" class="thumb-send-to pull-left img-circle" height="40" width="40">'+
						'<span class="info-contact pull-left margin-left-20">' +
							'<span class="name-element text-dark text-bold" data-id="'+userId+'">' + userConnected.name + ' ('+tradDynForm.me+')</span>'+
							'<br/>'+
							'<span class="type-element text-light pull-left">' + trad.citizens+ '</span>'+
						'</span>' +
					"</div>"+
				"</div>";
		}
		if(notNull(obj)){
			$.each(obj, function(e, v){
				if(v.address){
                    var addressStr="";
                    if(v.address.streetAddress)
                        addressStr += v.address.streetAddress;
                        if(v.address.postalCode)
                            addressStr += ((addressStr != "")?", ":"") + v.address.postalCode;
                        if(v.address.addressLocality)
                            addressStr += ((addressStr != "")?", ":"") + v.address.addressLocality;
                }
				if(typeof finder.object[keyForm][e] == "undefined" && e != userId){
					if(typeof finder.finderPopulation[keyForm][e]== "undefined")
						finder.finderPopulation[keyForm][e]=v;
					if($(".population-elt-finder-"+e).length <= 0){
						typeElt=(typeof v.collection != "undefined" && v.collection!="organizations") ? v.collection : v.type;
						img= (v.profilThumbImageUrl != "") ? baseUrl + v.profilThumbImageUrl : modules.co2.url + "/images/thumb/default_"+typeElt+".png";

							str+="<div class='population-elt-finder population-elt-finder-"+e+" col-xs-12' data-value='"+e+"'>"+
								'<div class="checkbox-content pull-left">'+
									'<label>'+
					    				'<input type="checkbox" class="check-population-finder checkbox-info" data-value="'+e+'">'+
					    				'<span class="cr"><i class="cr-icon fa fa-check"></i></span>'+
									'</label>'+
								'</div>'+
								"<div class='element-finder element-finder-"+e+"'>"+
									'<img src="'+ img+'" class="thumb-send-to pull-left img-circle" height="40" width="40">'+
									'<span class="info-contact pull-left margin-left-20">' +
										'<span class="name-element text-dark text-bold" data-id="'+e+'">' +  v.name + '</span>'+
										'<br/>';
						if(typeof addressStr!="undefined" && addressStr!=""){
							str += "<div class='popup-address text-dark'>";
							str +=    "<i class='fa fa-map-marker text-orange'></i> "+addressStr;
							str += "</div>";
						}

							str+=			'<span class="type-element text-light pull-left">' + trad[typeElt]+ '</span>'+
									'</span>' +
								"</div>"+
							"</div>";
					}else{
						$(".population-elt-finder-"+e).show();
					}
				}
			});
		}
		if(first)
			$("#list-finder-selection").html(str);
		else
			$("#list-finder-selection").append(str);
		finder.bindSelectItems(keyForm, multiple);
	};            

		finder.searchAndPopulateFinder = function(keyForm, text, typeSearch, multiple){
			mylog.log("finder.searchAndPopulateFinder", keyForm, text, typeSearch, multiple);
			//finder.isSearching=true;

			var dataSearch = {
	        	searchType : typeSearch, 
	        	name: text
	        };

	        if(notNull(finder.search) && notNull(finder.search[keyForm])){
	        	dataSearch = Object.assign({}, dataSearch, finder.search[keyForm]);
	        }
			if(finder.filters[keyForm]){
				dataSearch['filters'] = finder.filters[keyForm];
			}
	  		$.ajax({
				type: "POST",
		        url: baseUrl+"/"+moduleId+"/search/globalautocomplete",
		        data: dataSearch,
		        dataType: "json",
		        success: function(retdata){
		        	mylog.log("retdata",retdata);
		        	$("#list-finder").show();
		        	// mylog.log("finder.extraParams[keyForm].addNew",finder.extraParams[keyForm].addNew);
		        	mylog.log("etdata.results.length",retdata.results.length);
			        	mylog.log("finder.invite[keyForm]",finder.invite[keyForm]);
		        	if(!retdata){
		        		toastr.error(retdata.content);
		        	} else {
			        	if(Object.keys(retdata.results).length== 0 && (finder.invite[keyForm] == true || finder.invite[keyForm]=="true" || (notNull(finder.invite[keyForm]) && typeof finder.invite[keyForm]=="object"))){
			        		$("#form-invite").removeClass("hidden");
			        		$("#list-finder-selection").addClass("hidden");

			        		var search =  "#finderSelectHtml #populateFinder" ;
			        		var id = "#finderSelectHtml #form-invite" ; 
			        		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
							if(emailReg.test( $(search).val() )){
								$(id+' #inviteEmail').val( $(search).val());
								var nameEmail = $(search).val().split("@");
								$(id+" #inviteName").val(nameEmail[0]);
							}else{
								$(id+" #inviteName").val($(search).val());
								$(id+" #inviteEmail").val("");
							}

							var invitor = (typeof userConnected !="undefined" && userConnected!=null) ? userConnected.name : "un.e citoyen.ne";

							finder.invite[keyForm]={};
							finder.invite[keyForm].callback=function(data){
								mylog.log("invite callback", data)
			        			var listInvite={};
			        			listInvite.invites={};
			        			listInvite.invites[data.id] = { 
			        				name : data.name,
			        				profilThumbImageUrl : parentModuleUrl + "/images/thumb/default_"+data.type+".png",
			        				mail : data.mail,
			        				msg : data.msg
			        			} 
			        			var params = {
			        				parentId : costum.contextId,
			        				parentType : costum.contextType,
			        				listInvite : listInvite
			        			};
			        			
			        			 ajaxPost("",baseUrl+'/'+moduleId+"/link/multiconnect",params,function(data){
			        			 		var invited=(typeof trad.keyForm!="undefined") ? trad.keyForm : keyForm;
			        			 		toastr.success(invited +' bien invité.e');
			        			 	},
			        			 	function(data){
			        			 		toastr.error(trad.somethingwrong);
			        			 	}
			        			); 	
			        		};

		        		} else if ((finder.extraParams[keyForm].addNew=="true" || finder.extraParams[keyForm].addNew==true)){
		        				$("#form-invite").addClass("hidden");
		        				$('#finderSelectHtml .container-addNew').remove();
		        				//$("#list-finder-selection .population-elt-finder").hide();	
		        				var addNew='<div class="container-addNew"><span><i class="fa fa-more "></i>Il semblerait que votre '+finder.extraParams[keyForm].label+' ne figure pas encore sur la cartographie nationale.</span><br/><a href="javascript:;" data-type="'+finder.typeAuthorized[keyForm][0]+'" data-field="'+keyForm+'" data-filters='+JSON.stringify(finder.search[keyForm].filters)+' class=" add-new-element btn btn-primary">Ajouter un nouveau tiers-lieu</a></div>';
		        				if(Object.keys(retdata.results).length == 0) {
		        					$("#list-finder-selected").prepend(addNew);
		        					$("#list-finder-selection").hide();
		        					// var inputName=$("#"+keyForm).parent().parent().parent().data("key");
		        					//var inputName="<?php echo $keyTpl.$key ?>";
		        					window["bindAddNew<?php echo $kunik ?>"]();
		        				}else if(Object.keys(retdata.results).length > 0){
		        					$("#list-finder-selection").show();
		        					addNew='<div class="container-addNew margin-top-10"><span style="font-size: 15px;color: orangered;"><i class="fa fa-warning"></i> Dans un premier temps, parcourez la liste de résultats ci-dessous. Si vous y trouvez votre tiers-lieu, cliquez dessus.<br/>Si votre '+finder.extraParams[keyForm].label+' n\'y figure pas, <a href="javascript:;" data-type="'+finder.typeAuthorized[keyForm][0]+'" data-field="'+keyForm+'" data-filters='+JSON.stringify(finder.search[keyForm].filters)+' class=" add-new-element">Cliquez ici pour l\'ajouter.</a></span></div>';
		        					
	  			                        $("#list-finder-selected").prepend(addNew);
		        					    var inputName=$("#"+keyForm).parent().parent().parent().data("key");
		        					    inputName="<?php echo $keyTpl ?>"+inputName;
		        					    window["bindAddNew<?php echo $kunik ?>"]();
		        				    
		        					$("#form-invite").addClass("hidden");
		        			        //$('#finderSelectHtml .add-new-element').addClass("hidden")
		        			        $("#list-finder-selected").removeClass("hidden");
			        		        finder.populateFinder(keyForm, retdata.results, multiple);
		        				}else{
		        					$('#finderSelectHtml .container-addNew').removeClass("hidden");
		        				}	
		        				
		        		}
		        		
		        		else{
		        			$("#form-invite").addClass("hidden");
		        			$('#finderSelectHtml .container-addNew').addClass("hidden");
		        			$("#list-finder-selection").removeClass("hidden");
			        		finder.populateFinder(keyForm, retdata.results, multiple);
		        		}
		  			}
				}	
			});
		};

		var filters = {};
		if(typeof sectionDyf.<?php echo $kunik ?>ParamsData.filter!="undefined" && typeof sectionDyf.<?php echo $kunik ?>ParamsData.filter=="object"){
			$.each(sectionDyf.<?php echo $kunik ?>ParamsData.filter,function(k,v){
				filters[v.attributeName]=v.valueName;
			});
		}
        
            update=(typeof sectionDyf.<?php echo $kunik ?>ParamsData.update != "undefined") ? true : null;
            initValues=null;
            if(typeof answers != "undefined" && notNull(answers) && Object.keys(answers).length > 0)
            	initValues=answers;
            else if(typeof value != "undefined" && notNull(value) && Object.keys(value).length > 0)
            	initValues=value;
            var finderParams = {
	        			field : sectionDyf.<?php echo $kunik ?>ParamsData.field,
	        			id : sectionDyf.<?php echo $kunik ?>ParamsData.field,
	        			multiple : sectionDyf.<?php echo $kunik ?>ParamsData.multiple,
	        			initType : [sectionDyf.<?php echo $kunik ?>ParamsData.type],
	        			values : initValues,
	        			update : update,
	        			invite : sectionDyf.<?php echo $kunik ?>ParamsData.invite,
	        			// roles : fieldObj.roles,
	        			search : {
                        	"filters":filters,
                        	"notSourceKey":sectionDyf.<?php echo $kunik ?>ParamsData.notSourceKey
                        },
	        			// initElt : fieldObj.initElt,
	        			extraParams : {
                        	"addNew" : sectionDyf.<?php echo $kunik ?>ParamsData.addNew,
                        	"label" : sectionDyf.<?php echo $kunik ?>ParamsData.elementLabel
                        }
	        		};
	        		finderParams.initContacts=false;
	        		finderParams.initContext=false;
	        		finderParams.initMe =false;
	        		finderParams.initBySearch = false;

	        mylog.log("finder.init!", finderParams);
	        		finder.object[finderParams.id]={};
	        		finder.typeAuthorized[finderParams.id]=finderParams.initType;
	        		finder.field[finderParams.id]=finderParams.field;
	        		finder.initMe[finderParams.id]=(typeof finderParams.initMe != "undefined") ? finderParams.initMe : true;
	        		finder.initBySearch[finderParams.id]=(typeof finderParams.initBySearch != "undefined") ? finderParams.initBySearch : false;
	        		finder.initContacts[finderParams.id]=(typeof finderParams.initContacts != "undefined") ? finderParams.initContacts : true;
	        		finder.placeholder[finderParams.id]=(typeof finderParams.placeholder != "undefined") ? finderParams.placeholder : "Search an element";
	        		finder.filters[finderParams.id]=(typeof finderParams.filters != "undefined") ? finderParams.filters : null;
	        		finder.extraParams[finderParams.id] = (typeof finderParams.extraParams != "undefined") ? finderParams.extraParams : null;

	        		finder.toBeValidated[finderParams.id]=(typeof finderParams.toBeValidated != "undefined") ? finderParams.toBeValidated : null;
	        		
	        if(typeof finderParams.search != "undefined" && finderParams.search != null ){
	        			finder.search[finderParams.id] = finderParams.search;
	        		}

	                
	                	//if($(this).data("multiple") || $(this).parent().find(".form-list-finder > .element-finder").length == 0){
	            		
	            		finder.showPanel(finder.field[finderParams.id], finder.typeAuthorized[finderParams.id], true, false,finder.field[finderParams.id]);
	                	//}else{
	                	//	$(this).parent().find(".error").show(700).text("Vous ne pouvez ajouter qu'un élément");
	                	//}
	                	
	    //     		if(typeof fieldObj.toBeValidated != "undefined") finderParams.toBeValidated = fieldObj.toBeValidated;
	    //     		if(typeof fieldObj.placeholder != "undefined") finderParams.placeholder=fieldObj.placeholder;
	    //     		if(typeof fieldObj.filters != "undefined") finderParams.filters=fieldObj.filters;
					// if(typeof fieldObj.field != "undefined") finderParams.field = fieldObj.field;
					// if(typeof fieldObj.extraParams != "undefined") finderParams.extraParams = fieldObj.extraParams;

	        		//finder.init(finderParams);       

		//dyFObj.initFieldOnload[sectionDyf.<?php echo $kunik ?>ParamsData.field+"Finder"]();

        if(mode!=="w"){
        	setTimeout(
        		$("#question<?php echo $key ?> .selectParent").hide()
        		,1000);
        }
		// setTimeout($("#populateFinder").off().on("change",function(){
		//     	mylog.log("finder.showPanel keyup new");
		//     	if($(this).val().length < 3){
		//     		finder.filterPopulation($(this).val());
		//     	}else{
		//     		if(notNull(open) && open){
		//     			finder.filterPopulation($(this).val());
		//     			finder.searchAndPopulateFinder(keyForm,$(this).val(), typeSearch, multiple);
		//     		}else{
		//     			finder.filterPopulation($(this).val());
		//     		}
		//     	}
		//     }),5000);



		

		var answers= <?php echo json_encode( (isset($answer["answers"][$form["id"]][$kunik])) ? $answer["answers"][$form["id"]][$kunik] :null ); ?>;

    	if(typeof answers!="undefined" && notNull(answers) && Object.keys(answers).length>0){
           	$.each(answers,function(k,v){
           		mylog.log("answer elem",v);
           		if(typeof sectionDyf.<?php echo $kunik ?>ParamsData.field!="undefined" && notNull(sectionDyf.<?php echo $kunik ?>ParamsData.field)){
           			finder.addInForm<?php echo $kunik ?>(sectionDyf.<?php echo $kunik ?>ParamsData.field,v.id,v.type,v.name,v.img,null,true);
           		}	
            });
            $("#populateFinder").hide();
        }else{
           $("#populateFinder").show();
        }    

	$('div.form-list-finder').bind('DOMSubtreeModified', function(e) {
		e.stopImmediatePropagation();
		finderElementChecked = $(`li#question${validateKunik.replace('finder', '')} div.form-list-finder div.element-finder`).length;
		if(typeof ownAnswer != 'undefined') {
			if(ownAnswer[validateFormId] && finderElementChecked > 0) {
				ownAnswer[validateFormId][validateKunik] = finderElementChecked
			} else if (ownAnswer[validateFormId] && finderElementChecked == 0){
				ownAnswer[validateFormId][validateKunik] = ""
			}
		}
		mylog.log('reload input', finderElementChecked)
		if(typeof removeError != 'undefined') {
			if(finderElementChecked > 0) {
				removeError(
					$(`li#question${validateKunik}`),
					{
						'border' : 'none'
					},
					'error-msg'
				)
			}
		}
	});
	
	sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "<?php echo $kunik ?> config",
	        "icon" : "fa-cog",
	        "properties" : {
	            type : {
	                inputType : "select",
	                label : "Définir un type d'élément",
	                options :  <?php echo json_encode($elementType) ?>
	            },
	            filter : {
	                inputType : "lists",
                    label : "Filtres appliqués",
                    entries: {
                        attributeName: {
                            type:"select",
                            label:"Attribut",
                            options : ["category","type","tags","source.key"],
                            class:"col-lg-5"
                        },
                        valueName: {
                            type:"text",
                            label:"valeur",
                            class:"col-lg-5"
                        }
                    }
	            },
	            notSourceKey : dyFInputs.checkboxSimple(sectionDyf.<?php echo $kunik ?>ParamsData.notSourceKey, "notSourceKey", 
            										 {"onText" : trad.yes,
            										  "offText": trad.no,
            										  "onLabel" : "Recherche globale",
            										  "offLabel": "Recherche sourcée",
            										  "labelText" : "Recherche globale"}
				),
	            myContacts : dyFInputs.checkboxSimple(sectionDyf.<?php echo $kunik ?>ParamsData.myContacts, "myContacts", 
            										 {"onText" : trad.yes,
            										  "offText": trad.no,
            										  "onLabel" : "Recherches dans mes contacts",
            										  "offLabel": "Ne pas chercher dans mes contacts",
            										  "labelText" : "Rechercher parmi mes contacts"}
				),
               placeholderSearchField : {
                   inputType : "text",
	                label : "Texte d'explication (placeholder) dans le champ de recherche"
               },
	           elementLabel : {
	                inputType : "text",
	                label : "Label de l'élément recherché"
	            },
	            field : {
	                inputType : "text",
	                label : "Nom du champ"
	            },
	            multiple : dyFInputs.checkboxSimple(sectionDyf.<?php echo $kunik ?>ParamsData.multiple, "multiple", 
            										 {"onText" : trad.yes,
            										  "offText": trad.no,
            										  "onLabel" : "Ajouter plusieurs éléments",
            										  "offLabel": "N'ajouter qu'un seul élément",
            										  "labelText" : "Permettre l'ajout de plusieurs éléments"}
            	),
            	addNew : dyFInputs.checkboxSimple(sectionDyf.<?php echo $kunik ?>ParamsData.addNew, "addNew", 
            										 {"onText" : trad.yes,
            										  "offText": trad.no,
            										  "onLabel" : "Ajouter de nouveaux éléments",
            										  "offLabel": "Ne pas ajouter de nouveaux éléments",
            										  "labelText" : "Permettre l'ajout de nouveaux éléments"}
            	),
            	invite : dyFInputs.checkboxSimple(sectionDyf.<?php echo $kunik ?>ParamsData.invite,"invite",
            										 {"onText" : trad.yes,
            										  "offText": trad.no,
            										  "onLabel" : "Afficher les invitations",
            										  "offLabel": "Ne pas afficher les invitations",
            										  "labelText" : "Permettre l'affichage des invitations"}
            	),
            	linkToAnswer : dyFInputs.checkboxSimple(sectionDyf.<?php echo $kunik ?>ParamsData.linkToAnswer,"linkToAnswer",
            										 {"onText" : trad.yes,
            										  "offText": trad.no,
            										  "onLabel" : "Lier l'élément à la réponse",
            										  "offLabel": "Ne pas l'élément à la réponse",
            										  "labelText" : "Créer un lien entre l'élément et la réponse"}
            	),
            	singleAnswerPerElement : dyFInputs.checkboxSimple(sectionDyf.<?php echo $kunik ?>ParamsData.singleAnswerPerElement,"singleAnswerPerElement",
            										 {"onText" : trad.yes,
            										  "offText": trad.no,
            										  "onLabel" : "Réponse unique par élément (impossible d'aller plus loin de le formulaire",
            										  "offLabel": "Plusieurs réponses possibles par élément",
            										  "labelText" : "N'autoriser qu'une seule réponse associée à un même élément"}
            	),
            	msgSingleAnswerPerElement : {
            		inputType : "textarea",
                    label : "Contenu du message d'alerte signifiant l'existance d'une réponse relative à un élement"
            	},
            	redirectSingleAnswerPerElement : {
            		inputType : "select",
            		label : "Redirection après le message d'alerte signifiant l'existance d'une réponse relative à un élement",
            		options : {"Accueil":"Accueil","Page précédente":"Page précédente","Réponse existante":"Réponse existante"}
            	},
            	editElement : dyFInputs.checkboxSimple(sectionDyf.<?php echo $kunik ?>ParamsData.editElement,"editElement",
            										 {"onText" : trad.yes,
            										  "offText": trad.no,
            										  "onLabel" : "L'édition de l'élément est possible",
            										  "offLabel": "L'édition de l'élément n'est pas autorisée",
            										  "labelText" : "Edition de élément"}
            	)




	     //        "category"   => null,
		    // "notSourceKey" => true,
		    // "myContacts"   => false,
		    // "elementLabel" => "Elément"
		    // "field" => "element"
	        },
	        save : function (data) {  
	        	mylog.log("save sectionDyf",data);
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	            	if(k=="filter"){
	        		    var filters = {};

	        		    $.each(data.filter, function(index, va){
	        		        var filter = {attributeName: va.attributeName, valueName: va.valueName};
	        		        filters["filter"+index] = filter;
	        		    });

	        		    tplCtx.value[k] = filters;
	        		}
	        		else{
	        		tplCtx.value[k] = $("#"+k).val();
		        		if(tplCtx.value[k]=="true") {
		        			tplCtx.value[k]=true;
		        		}
		        		if(tplCtx.value[k]=="false") {
		        			tplCtx.value[k]=false;
		        		}
		        	}	
	        		
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    $("#ajax-modal").modal('hide');
	                    reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
	                    //urlCtrl.loadByHash(location.hash);
	                } );
	            }

	    	}
	    }
	};


    mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

    //adds a line into answer

    <?php if( isset($parentForm["params"][$kunik]['type']) ) { ?>


    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( "<?php echo $parentForm["params"][$kunik]['type']; ?>",null, <?php echo $kunik ?>Data[$(this).data("key")]);
    });
    <?php } ?>

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        //if no params config on the element.costum.form.params.<?php echo $keyTpl ?>
        //then we load default values available in forms.inputs.<?php echo $keyTpl ?>xxx.params
        //mylog.log(".editParams",sectionDyf.<?php echo $keyTpl ?>Params,calData);
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    

    
});
</script>
<?php } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>