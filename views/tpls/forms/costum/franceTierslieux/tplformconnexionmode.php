<?php
HtmlHelper::registerCssAndScriptsFiles(array(
// '/css/dashboard.css',
'/js/form.js'
), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );

$parentform = $form;
?>
<style type="text/css">

    #mainNav .searchBarInMenu, #menuBottom, #menuRight, #menuTopRight .divider, #menuTopRight ul li:nth-child(3), .btn-menu-connect{
        display:none;
      }


    #containerConnex{
        border:solid 1px #4623C9;
        margin-top:20px;
        margin-bottom:20px;
    }
    .formconexmode .containerConnex{
        text-align: center;
    }

    .formconexmode .haveaccountbtn, .formconexmode .givemailbtn {
        width: 250px;
        height: 50px;
        background: #FF286B;
        border: 0;
        display: inline-block;
        color: #fff;
        font-size: 18px;
        margin: 10px;
    }

    .formconexmode .haveaccountbtn:hover, .formconexmode .givemailbtn:hover{
        background: #4623C9;
        font-weight: bold;
    }

    .formconexmode h2 {
        font-weight: 100 !important;
        color: #4623C9;
        font-size: 48px;
    }

    .formconexmode h4 {
        font-weight: 100;
        color: #4623C9;
    }

    .formconexmode p {
        font-size: 15px !important;
        color:#4623C9;
    }

    .introExplain p{
        color:unset;
    }


</style>

<div class="row formconexmode">
    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
       <img src="https://www.communecter.org/upload/communecter/organizations/5ec3b895690864db108b46b8/album/Banniere-site-recensement.png" style="width:100%;text-align:center;display:inline-block;"/>
    </div>

    <div class="col-xs-12 col-sm-8 col-sm-offset-2 introExplain">
        <!-- <h3>Bienvuenue sur la page d'accueil du recensemement des tiers-lieux 2023</h3> -->
        <h4 class="margin-top-30 text-center">France Tiers-Lieux, en partenariat avec l'ensemble des réseaux de tiers-lieux,  lance le recensement 2023 des tiers-lieux !</h4>
        <div class="col-xs-12">
            <p><span style="margin-left:60px">En répondant à ce questionnaire rejoignez la cartographie nationale des tiers-lieux et aidez-nous à mettre en lumière les apports des tiers-lieux, les enjeux auxquels ils font face et leurs besoins. <strong>Mais surtout, gagnez du temps en évitant d'avoir à répondre à des dizaines de questionnaires éparpillés !</strong></span></p>
            <p><span style="margin-left:60px">Plus de 2 ans après le premier recensement national, qui a permis de dresser l'état des tiers-lieux en France et qui a donné lieu à la publication du rapport <em>“Nos territoires en action - Dans les tiers-lieux se fabrique notre avenir”</em> et au renforcement du programme national de soutien aux tiers-lieux, avec 130 millions d'euros supplémentaires engagés par l'État, nous poursuivons ainsi notre travail collectif de compréhension du phénomène des tiers-lieux.</span></p>
            <p><span style="margin-left:60px">En 2023, ce nouveau recensement est l'occasion d'aller plus loin, d'affiner notre connaissance des tiers-lieux, de donner à voir leur diversité, de démontrer encore l'importance et l'impact du mouvement des tiers-lieux en France. A la suite de cette campagne de recensement, France Tiers-Lieux lancera officiellement l'Observatoire des tiers-lieux, un espace de production et de diffusion de connaissances, pour les tiers-lieux et l'ensemble de leurs partenaires.</span></p>
            <p><span style="margin-left:60px">Aux côtés des réseaux régionaux, des réseaux nationaux (comme le RFFLabs, le réseau des Lieux intermédiaires et indépendants ou encore Tiers-Lieux Édu), de Commune Mesure, de l'ensemble des ministères qui soutiennent les tiers-lieux et de l'ANCT, nous sollicitons aujourd'hui les tiers-lieux pour construire le panorama 2023 des tiers-lieux et faire en sorte que la réalité des tiers-lieux soient mieux comprise.</span></p>
            <p><span style="margin-left:60px">Nous nous appuyons sur une conception inclusive des tiers-lieux, respectueuse de leur diversité : fablabs, jardins partagés, lieux intermédiaires, tiers-espaces, cafés associatifs, espaces de travail partagés, etc. Ce recensement s'adresse à tous ceux qui font tiers-lieux : mutualisant des espaces et des compétences, hybridant des activités, réunissant des collectifs citoyens engagés, favorisant la coopération pour répondre aux enjeux de leur territoire, etc.</span></p>
            <p><span style="margin-left:60px"><strong>Accessible depuis le site de la Cartographie France Tiers-Lieux, vous n'aurez pas à recommencer un questionnaire chaque année, mais plutôt à mettre à jour vos propres données, quand vous le souhaitez</strong>. Ce recensement étant construit sur le premier schéma de données commun sur les tiers-lieux, il rendra possible la circulation des données et l'échange d'informations entre outils et entre acteurs. L'ensemble des données seront partagées en open data (selon le principe de la Licence Ouverte Etalab), accessibles à tous et alimentant ainsi la recherche sur les tiers-lieux.</span></p>
            <p><span style="margin-left:60px">Enfin, tout ceci est rendu possible grâce à la solution Communecter, élaborée par l'association Open Atlas, qui a été choisie pour la réalisation de la cartographie et du recensement national des tiers-lieux. Il s'agit d'une ressource libre et ouverte qui permet à des collectifs d'animer la récolte de données et de déployer des services à partir de celle-ci (cartographie, cofinancement, gestion de projet, sites internet, etc).
            </span></p>
        </div>


    </div>
    <?php
    if (isset($parentform["startDateNoconfirmation"])
    && ( time() > strtotime(str_replace("/","-",$parentform["startDateNoconfirmation"]))
        && isset($parentform["endDateNoconfirmation"])
        && time() <= strtotime(str_replace("/","-",$parentform["endDateNoconfirmation"])))
    ){
    ?>

    <div id="containerConnex" class="containerConnex col-xs-12 col-md-8 col-md-offset-2">
        <h2>Identifiez-vous</h2>
        <h4>pour répondre au recensement 2023</h4>

        <div class="row">
            <div class="col-md-6 col-sm-12">
                <button class="haveaccountbtn " data-target="#modalLogin" data-toggle="modal" data-dismiss="modal" >J'ai déjà un compte</button>
                <p>Répondre en me connectant avec mon compte utilisé pour la cartographie France Tiers-Lieux (ou compte Communecter)</p>
            </div>
            <div class="col-md-6 col-sm-12">
                <button class="givemailbtn">Repondre sans avoir un compte</button>
                <p>Accéder en fournissant mon adresse e-mail. Je finirai mon inscription plus tard.</p>
            </div>
        </div>

    </div>
    <?php
    } else if (isset($parentform["startDateNoconfirmation"])) {
        if(time() < strtotime(str_replace("/","-",$parentform["startDateNoconfirmation"])) )
            echo "<div class='containerConnex'><h3 style='display: inline-block; margin: 70px 0px;'>Le formulaire sera ouvert le ".$parentform["startDateNoconfirmation"]."</h3></div>";
        else if ( time() > strtotime(str_replace("/","-",$parentform["endDateNoconfirmation"])))
            echo "<div class='containerConnex'><h3 style='display: inline-block; margin: 70px 0px;'>Le recensement 2023 est désormais clos.</h3></div>";
    }
    ?>
    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
       <img src="https://www.communecter.org/upload/communecter/organizations/5ec3b895690864db108b46b8/album/footer-recensement.png" style="width:100%;text-align:center;display:inline-block;"/>
    </div>
</div>

<?php
$form=(is_array($form) && isset($form["_id"])) ? (string)$form["_id"] : (string)$form;
$connexionmodeParams = [
    "id" => $id,
    "answer" => $answer,
    "mode" => $mode,
    "form" => $form
];

//var_dump($form);
?>

<script type="text/javascript">
    $(function(){
        $('#popupRecensement .custom-modal-header .close').trigger("click");
        $("#menuTopLeft img").parent().attr("href",location.hash);
        var  copyFormObj =formObj.init();
        $('.givemailbtn').off().on('click',function(){
            $this = $(this);
            var params = <?php echo json_encode($connexionmodeParams); ?>;
            mylog.log("params.form",params.form);
            bootbox.prompt({
                title: " votre adresse Email",
                /*message: '<p>Please select an option below:</p>',*/
                callback: function (result) {
                    if(result){
                        ajaxPost(
                            null,
                            baseUrl+`/survey/answer/answerwithemail/mode/w/standalone/true`,
                            {
                                id : params.id,
                                form : params.form,
                                email : result
                            }
                            , function(res){

                                if(res.status && notNull(res.location)){
                                $("#firstLoader .contentFirstLoading").append('<h2 style="color:#4623C9;text-align:center" class="col-xs-10 col-xs-offset-1">Vous allez être redirigé(e) vers le formulaire du recensement 2023</h2>');
                                $("#firstLoader").show();
                                    window.open(res.location+".standalone.true","_self");
                                    // window.location.reload();


                            }else{
                                toastr.error(res.msg);
                            }


                        },
                        null,
                        null,
                        {async:false});
                    }
                }
            });
            var emailLastVal = "";
            var lastStat = false;
            var bootboxModal = setInterval(function() {
                if($('.bootbox.modal.fade.bootbox-prompt').is(':visible')) {
                    const emailRegex = new RegExp(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g);
                    $('.bootbox-input.bootbox-input-text.form-control').on('keyup', delay(function(e) {
                        var self = this;
                        if($(this).val().match(emailRegex) && emailLastVal != $(this).val()) {
                            emailLastVal = $(this).val();
                            lastStat = true;
                            var resVal = false;
                            ajaxPost(
                                null,
                                baseUrl+`/survey/answer/checkemailuser/`,
                                {
                                    id : params.id,
                                    form : params.form,
                                    email : $(self).val()
                                },
                                function(res){
                                    if(res && res.duplicated) {
                                        lastStat = false;
                                        $('.btn.btn-primary.bootbox-accept').prop('disabled', true);
                                        var msgError = trad.AnActiveAccountExists;
                                        var btnHTML = `
                                            <a href="javascript:;" class="btn btn-default bg-white letter-blue bold margin-top-10" data-target="#modalLogin" data-toggle="modal" data-dismiss="modal">
                                                <i class="fa fa-sign-in"></i>
                                                ${tradCms.login}
                                            </a>`;
                                        if(res && res.tobeactivated) {
                                            lastStat = false;
                                            msgError = trad.ATemporaryAccountExists;
                                            var btnHTML = `
                                                <a href="javascript:;" class="btn btn-default bg-white letter-blue bold margin-top-10 openModalResendEmail" data-toggle="modal" data-target="#modalSendActivation" onclick="${$('#modalSendActivation #email2').val($(self).val())};">
                                                    <i class="fa fa-envelope"></i>
                                                    ${trad.ReceiveAnotherValidationEmail}
                                                </a>
                                            `;
                                        }
                                        $('.bootbox-form-error').remove();
                                        $('.bootbox-form').append(`
                                            <div class="bootbox-form-error text-center">
                                                <span class="bg-white alert-danger margin-top-10">${msgError}</span><br />
                                                ${btnHTML}
                                            </div>
                                        `).animate({ opacity:1 }, 300 );
                                        $('.openModalResendEmail').off().on('click', function() {
                                            $('.bootbox.modal.fade.bootbox-prompt').hide()
                                        });
                                        resVal = e.which !== 13;
                                    } else if(res && !res.duplicated) {
                                        $('.btn.btn-primary.bootbox-accept').prop('disabled', false);
                                        $('.bootbox-form-error').remove();
                                        resVal = true
                                    }
                                },
                                null,
                                null,
                                {
                                    async:false
                                }
                            );
                            return resVal;
                        } else {
                            if(lastStat) {
                                $('.btn.btn-primary.bootbox-accept').prop('disabled', false);
                                $('.bootbox-form-error').remove();
                                return lastStat
                            } else {
                                $('.btn.btn-primary.bootbox-accept').prop('disabled', true);
                                return e.which !== 13;
                            }
                        }
                    }, 350));
                    $('.bootbox-input.bootbox-input-text.form-control').on('blur', function() {
                        var self = this;
                        if($(self).val() == '') {
                            $('.btn.btn-primary.bootbox-accept').prop('disabled', true)
                        } else if($(self).val().match(emailRegex) && emailLastVal != $(self).val()) {
                            $('.btn.btn-primary.bootbox-accept').prop('disabled', true);
                            emailLastVal = $(self).val();
                            lastStat = true;
                            ajaxPost(
                                null,
                                baseUrl+`/survey/answer/checkemailuser/`,
                                {
                                    id : params.id,
                                    form : params.form,
                                    email : $(self).val()
                                }
                                , function(res){
                                    if(res && res.duplicated) {
                                        $('.btn.btn-primary.bootbox-accept').prop('disabled', true);
                                        lastStat = false;
                                        var msgError = trad.AnActiveAccountExists;
                                        var btnHTML = `
                                            <a href="javascript:;" class="btn btn-default bg-white letter-blue bold margin-top-10" data-target="#modalLogin" data-toggle="modal" data-dismiss="modal">
                                                <i class="fa fa-sign-in"></i>
                                                ${tradCms.login}
                                            </a>`;
                                        if(res && res.tobeactivated) {
                                            msgError = trad.ATemporaryAccountExists;
                                            lastStat = false;
                                            var btnHTML = `
                                                <a href="javascript:;" class="btn btn-default bg-white letter-blue bold margin-top-10 openModalResendEmail" data-toggle="modal" data-target="#modalSendActivation" onclick="${$('#modalSendActivation #email2').val($(self).val())};">
                                                    <i class="fa fa-envelope"></i>
                                                    ${trad.ReceiveAnotherValidationEmail}
                                                </a>
                                            `;
                                        }
                                        $('.bootbox-form-error').remove();
                                        $('.bootbox-form').append(`
                                            <div class="bootbox-form-error text-center">
                                                <span class="bg-white alert-danger margin-top-10">${msgError}</span><br />
                                                ${btnHTML}
                                            </div>
                                        `).animate({ opacity:1 }, 300 );
                                        $('.openModalResendEmail').off().on('click', function() {
                                            $('.bootbox.modal.fade.bootbox-prompt').hide()
                                        });
                                    } else if(res && !res.duplicated) {
                                        $('.btn.btn-primary.bootbox-accept').prop('disabled', false);
                                        $('.bootbox-form-error').remove();
                                    }
                                }
                            );
                        } else {
                            if(lastStat) {
                                $('.btn.btn-primary.bootbox-accept').prop('disabled', false);
                                $('.bootbox-form-error').remove();
                            } else
                                $('.btn.btn-primary.bootbox-accept').prop('disabled', true)
                        }
                    })
                    clearInterval(bootboxModal)
                }
            },200)
        });
    })
</script>
