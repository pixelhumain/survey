<style type="text/css">
    .checkedvalue{
        color: #00C851;
    }

    .timesvalue{
        color: #ff4444;
    }

    .tovalidatelist{
        list-style: none;
    }

    .gotoinput-btn{
        border: 2px solid #0099CC;
        border-radius: 5px;
        font-size: 15px;
        background-color: transparent;
        cursor: pointer;
        padding: 2px;
        margin: 2px;
    }

    .inputList{
        display: flow-root;
    }

    .switch input {
        display:none;
    }
    .switch {
        display:inline-block;
        width:60px;
        height:30px;
        margin:8px;
        transform:translateY(50%);
        position:relative;
    }
    /* Style Wired */
    .slider {
        position:absolute;
        top:0;
        bottom:0;
        left:0;
        right:0;
        border-radius:30px;
        box-shadow:0 0 0 2px #777, 0 0 4px #777;
        cursor:pointer;
        border:4px solid transparent;
        overflow:hidden;
        transition:.4s;
    }
    .slider:before {
        position:absolute;
        content:"";
        width:100%;
        height:100%;
        background:#777;
        border-radius:30px;
        transform:translateX(-30px);
        transition:.4s;
    }

    input:checked + .slider:before {
        transform:translateX(30px);
        background:limeGreen;
    }
    input:checked + .slider {
        box-shadow:0 0 0 2px limeGreen,0 0 2px limeGreen;
    }

    .error-msg::after {
        content: '<?php echo Yii::t("common","This field is required."); ?>';
        color: #a94442;
        font-size: 85%;
        padding: 1%;
    }

</style>

<?php

$isValidate = false;

$formList = [];
$stepList = [];
$lastForm = false;
$ischecked= [];
$isAlreadyPassed = false;


if(!empty($parentForm["type"]) && ($parentForm["type"] == "aap" || $parentForm["type"] == "aapConfig")){
    if (isset($subFs)) {
        foreach ($subFs as $keysF => $valuesF) {
            $stepList[$keysF] = $valuesF["name"];
            $lastSubForm = $valuesF;
        }
    } else {
        $lastForm = true;
    }
}else {
    if (isset($parentForm["subForms"])) {
        foreach ($parentForm["subForms"] as $keysF => $valuesF) {
            $stepList[$valuesF] = $valuesF;
            $lastSubForm = $valuesF;
        }
    } else {
        $lastForm = true;
    }
}

if (!$lastForm && $form["id"] == @$lastSubForm) {
    $lastForm = true;
}

if ($lastForm ){
    $lastSubForm = $form["id"];
}


if (isset($parentForm["params"][$kunik]["step"]) && $form["id"] == $parentForm["params"][$kunik]["step"]){
    $lastSubForm = $form["id"];
    $lastForm = true;
}

if (isset($form)) {
    foreach ($form["inputs"] as $fid => $fvalue) {
        if ($fid != $key) {
            $formList[$fid] = !empty($fvalue["label"]) ? $fvalue["label"] : (isset($fvalue["info"]) ? $fvalue["info"] : "") ;
        }
    }
}

if (isset($answers) && !is_null($answers) && !empty($answers)) {
    $isValidate = true;
}


$canValidate = false;
$alreadyChecked = 0;
$inputListOptions = [];
$comV = [];

//var_dump($canEditForm);

$editParamsBtn = ($canEditForm=="true" && ($mode != "r" || $mode != "pdf")) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

$contactType = [
    "Partenaire"=>"Partenaire",
    "Administrateur" => "Administrateur",
    "Financeur" => "Financeur",
    "Opérateur"=>"Opérateur",
    "Répondant"=>"Répondant",
    "OpérateurValidated"=>"Opérateur validé",
    "customInput" => "Réponse d'une question"
];

foreach ($form["inputs"] as $forminputskey => $forminputsvalue) {
    if (isset($forminputsvalue["label"])) {
        $inputListOptions[$forminputskey] = $forminputsvalue["label"];
    }
}

if (isset($answer["links"]["operators"])) {
    $operateurVid = $answer["links"]["operators"];
    foreach ($operateurVid as $vkey => $vvalue) {
        $comV = Element::getCommunityByTypeAndId("organizations", $vkey);
    }
}

$selectedContact = array("roles"=>array());

if( isset($parentForm["params"][$kunik]["contact"]) ){
    if (is_array($parentForm["params"][$kunik]["contact"]) &&  in_array("Financeur", $parentForm["params"][$kunik]["contact"]) )  {
        array_push($selectedContact["roles"], "Financeur");
    }
    if (is_array($parentForm["params"][$kunik]["contact"]) &&  in_array("Opérateur", $parentForm["params"][$kunik]["contact"])) {
        array_push($selectedContact["roles"], "Opérateur");
    }
}

$paramsData = [
    "showChecked" => true,
    "showMandatoriesInit"=>false,
    "inputList" => [],
    "step" => "",
    "contact" => [],
    "object" => "",
    "msg" => "",
    "msgbox" => "",
    "notif" => "",
    "urlRedirect" => "",
    "forbiddenChange" => "",
    "tplMail" =>"basic",
    "showMsg" => 'true',
];

if( isset($parentForm["params"][$kunik]["showChecked"]) && !empty($parentForm["params"][$kunik]["showChecked"])){
    $paramsData["showChecked"] =  $parentForm["params"][$kunik]["showChecked"];
}
if( isset($parentForm["params"][$kunik]["showMandatoriesInit"]) && !empty($parentForm["params"][$kunik]["showMandatoriesInit"])){
    $paramsData["showMandatoriesInit"] =  $parentForm["params"][$kunik]["showMandatoriesInit"];
}
if( isset($parentForm["params"][$kunik]["inputList"]) && !empty($parentForm["params"][$kunik]["inputList"])){
    $paramsData["inputList"] =  $parentForm["params"][$kunik]["inputList"];
}

if( isset($parentForm["params"][$kunik]["step"]) ){
    $paramsData["step"] =  $parentForm["params"][$kunik]["step"];
}

if( isset($parentForm["params"][$kunik]["object"]) ){
    $paramsData["object"] =  $parentForm["params"][$kunik]["object"];
}

if( isset($parentForm["params"][$kunik]["msg"]) ){
    $paramsData["msg"] =  $parentForm["params"][$kunik]["msg"];
}

if( isset($parentForm["params"][$kunik]["msgbox"]) ){
    $paramsData["msgbox"] =  $parentForm["params"][$kunik]["msgbox"];
}

if( isset($parentForm["params"][$kunik]["notif"]) ){
    $paramsData["notif"] =  $parentForm["params"][$kunik]["notif"];
}

if( isset($parentForm["params"][$kunik]["contact"]) ){
    $paramsData["contact"] =  $parentForm["params"][$kunik]["contact"];
}

if( isset($parentForm["params"][$kunik]["urlRedirect"]) ){
    $paramsData["urlRedirect"] =  $parentForm["params"][$kunik]["urlRedirect"];
}
if( isset($parentForm["params"][$kunik]["forbiddenChange"]) ){
    $paramsData["forbiddenChange"] =  $parentForm["params"][$kunik]["forbiddenChange"];
}
if( isset($parentForm["params"][$kunik]["tplMail"]) ){
    $paramsData["tplMail"] =  $parentForm["params"][$kunik]["tplMail"];
}
if( isset($parentForm["params"][$kunik]["showMsg"]) && !empty($parentForm["params"][$kunik]["showMsg"])){
    $paramsData["showMsg"] =  $parentForm["params"][$kunik]["showMsg"];
}

if (isset($answers) && !is_null($answers) && !empty($answers) && !$lastForm && isset($answer["step"]) && $answer["step"] == $form["id"]) {
    $isValidate = false;
    $isAlreadyPassed = true;
}

$totalToChecked = sizeof($paramsData["inputList"]);

if($totalToChecked > 0){

    foreach ($paramsData["inputList"] as $inid => $invalue) {
        $nbCar=strlen($invalue);
        if ($alreadyChecked < $totalToChecked && isset($answer["answers"][$form["id"]])) {

            foreach ($answer["answers"][$form["id"]] as $kAns => $vAns)
            {
                $strCut=substr($kAns,-$nbCar);
                if($strCut==$invalue){
                    $alreadyChecked++;
                    array_push($ischecked, $invalue);
                }
            }
        }
    }
}

$message = "Veuillez remplir les champs obligatoires pour passer à l'etape suivante";

$labelClass=($paramsData["showMandatoriesInit"]==true || $paramsData["showMandatoriesInit"]=="true") ? "Cacher la liste des champs obligatoires" : "Montrer la liste des champs obligatoires";
$iconMandatory=($paramsData["showMandatoriesInit"]==true || $paramsData["showMandatoriesInit"]=="true") ? "eye-slash" : "eye" ;
$displayMandatory=($paramsData["showMandatoriesInit"]==true || $paramsData["showMandatoriesInit"]=="true") ? "block" : "none";

$message .=
    '<div class="form-check form-check-inline">
          <button class="btn btn-primary btnshowvalidatelist" type="button"><i class="fa fa-'.$iconMandatory.'" ></i></button>
          <label class="form-check-label eyee lblshowvalidatelist">'.$labelClass.'</label>
        </div>';

$message .= "<div class='tovalidatelist' id='tovalidatelist".$kunik."' style='display:".$displayMandatory."'>";
//var_dump($paramsData["showChecked"]);
if (!empty($parentForm["params"][$kunik]["inputList"])) {
    //var_dump($parentForm["params"][$kunik]["inputList"]);
    //$tovalidateMsg='';
    foreach ($parentForm["params"][$kunik]["inputList"] as $keysF => $valuesF) {
        $inputListMsg ="<div class='inputList'>";

        if (in_array($valuesF, $ischecked)) {
            $inputListMsg .= "<span class='checkedvalue'>";
        } else {
            $inputListMsg .= "<span class='timesvalue'>";
        }
        $inputListMsg .= $formList[$valuesF];
        if (in_array($valuesF, $ischecked)) {
            $inputListMsg .= '</span><i class="fa fa-check-circle checkedvalue pull-left success" aria-hidden="true"></i>';
        } else {
            $inputListMsg .= '</span><i class="fa fa-times-circle timesvalue pull-left" aria-hidden="true"></i>';
        }

        $inputListMsg .='<a data-path="question'.$valuesF.'" class="gotoinput-btn pull-right">s\'y rendre</a></div>';
        if(in_array($valuesF, $ischecked) && ($paramsData["showChecked"]!=true || $paramsData["showChecked"]!="true")){
            $inputListMsg='';
        }
        $message.=$inputListMsg;
    }
}
//$message .= "</div>";

$message.="</div>";


if ($paramsData['step'] == "" && !$isAlreadyPassed) {
    $paramsData['step'] = $form['id'];
}

if (($totalToChecked == $alreadyChecked) || (!isset($parentForm["params"][$kunik]["inputList"]) or (isset($parentForm["params"][$kunik]["inputList"]) && sizeof($parentForm["params"][$kunik]["inputList"]) == 0)) || (isset($answers) && $answers == "canValidate")) {
    $canValidate = true;
}

if ($isValidate) {
    $canValidate = false;

    $message = (isset($parentForm["params"][$kunik]["msgbox"]) && $parentForm["params"][$kunik]["msgbox"] != "") ? $parentForm["params"][$kunik]["msgbox"] : "Cette étape a été validée le ".$answers;
}

$hasUserEmail = false;
$userEmail = "";

if (isset(Yii::app()->session["userEmail"])) {
    $hasUserEmail = true;
    $userEmail = Yii::app()->session["userEmail"];
}

?>

<?php
if(!$isValidate && !$canValidate){
    ?>
    <div class="col-xs-12 text-center padding-20">
        <a href="javascript:;" class="btn btn-primary text-white" onclick="stepValidationReload<?php echo $form['id']?>('btnValidate')">Valider les réponses</a>
    </div>
    <?php
}
?>

<?php if(!$lastForm){ ?>
    <div style="border: 2px solid darkgrey;padding: 20px;border-radius: 20px; overflow-x: auto; text-align: center" class="step-vld">
        <?php if(!$isValidate && $canValidate){
            ?>
            <button type="button" class="btn validate<?php echo $kunik ?>Params" >Passer à l'étape suivante</button>
        <?php }else if(!$isValidate){
            echo "<span style='color:red;'>".$message."</span>";
        }else{
            echo $message;
        }
        echo $editParamsBtn.$editQuestionBtn; ?>

    </div>
<?php } else if($lastForm){ ?>
    <div style="border: 2px solid darkgrey;padding: 20px;border-radius: 20px; overflow-x: auto; text-align: center" class="step-vld">
        <?php if(!$isValidate && $canValidate){ ?>
            <button type="button" class="btn validate<?php echo $kunik ?>Params" >Envoyer</button>
        <?php } elseif(!$isValidate) { echo "<span style='color:red;'>".$message."</span>";} else{
            echo $message;
        } echo $editParamsBtn.$editQuestionBtn; ?>

    </div>
<?php }

$mustSendMail = [];
$mailList =[];

if (isset($parentForm["params"][$kunik]["contact"]) && is_array($parentForm["params"][$kunik]["contact"])) {
    if (is_array($parentForm["params"][$kunik]["contact"]) && !empty($parentForm["params"][$kunik]["contact"])) {
        if(in_array("Répondant", $parentForm["params"][$kunik]["contact"])){
            array_push($mailList, $userEmail);
        }
    }

    if(in_array("OpérateurValidated", $parentForm["params"][$kunik]["contact"]) && isset($comV)){
        foreach ($comV as $keypers => $valuepers) {
            if ($valuepers["type"] == "citoyens") {
                array_push($mustSendMail, $keypers);
            }
        }
    }
    if(in_array("Opérateur", $parentForm["params"][$kunik]["contact"]) || in_array("Financeur", $parentForm["params"][$kunik]["contact"])){
        foreach ($this->costum["communityLinks"] as $comkey => $comvalue) {
            foreach ($comvalue as $ke => $valu) {
                if (isset($valu["roles"])) {
                    if (in_array("Opérateur", $parentForm["params"][$kunik]["contact"]) && in_array("Opérateur", $valu["roles"])) {
                        array_push($mustSendMail, $ke);
                    }
                    if (in_array("Financeur", $parentForm["params"][$kunik]["contact"]) && in_array("Financeur", $valu["roles"])) {
                        array_push($mustSendMail, $ke);
                    }
                }
            }
        }
    }

    if(in_array("customInput", $parentForm["params"][$kunik]["contact"]) && isset($parentForm["params"][$kunik]["inputscontact"])){
        foreach ($parentForm["params"][$kunik]["inputscontact"] as $inputscontactkey => $inputscontactvalue) {
            if (isset($answer["answers"][$form["id"]][$inputscontactvalue])) {

                array_push($mailList, $answer["answers"][$form["id"]][$inputscontactvalue]);
            }
        }


    }
    if(in_array("Administrateur", $parentForm["params"][$kunik]["contact"]) || in_array("Financeur", $parentForm["params"][$kunik]["contact"])){
        //var_dump($parentForm["params"][$kunik]["contact"]);
        //var_dump($this->costum["communityLinks"]);
        foreach ($this->costum["communityLinks"] as $comkey => $comvalue) {
            foreach ($comvalue as $ke => $valu) {
                if (isset($valu["isAdmin"])) {
                    array_push($mustSendMail, $ke);
                    //var_dump($valu);
                    /*$email = PHDB::findOne($valu["type"], [ "_id" => new MongoId($ke)],["email"]);
                    if ($email["email"] != NULL) {
                        var_dump($email["email"]);
                        array_push($mailList, $email["email"]);
                    }*/

                }

            }
        }
    }
}

foreach ($mustSendMail as $persk => $persid) {

    $cytns = Person::getEmailById($persid);
    if (!empty($cytns["email"]) && $cytns["email"] != "") {
        array_push($mailList,$cytns["email"]);
    }
}
$mailList = array_unique($mailList);

?>

<script>

    var stepVld = {
        newThis : function (answerId){
            var params = {
                "answerId" : answerId,
                "seen" : false,
                "action" : "newanswer"
            }
            $.ajax({
                type : 'POST',
                data : params,
                url : baseUrl+"/survey/answer/newanswer",
                dataType : "json",
                async : false,

                success : function(data){
                }
            });
        }
    }

    var parentForm=<?php echo json_encode($parentForm)?>;

    var contacttplMail<?php echo $kunik ?> = <?php echo json_encode( (isset($mailList)) ? $mailList : null ); ?>;

    var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    const myKunik = <?php echo json_encode($kunik); ?>;
    const myFormid = <?php echo json_encode($form['id']); ?>;
    var viewMode = <?php echo json_encode($mode); ?>;
    const showMsg = sectionDyf.<?php echo $kunik ?>ParamsData['showMsg'];

    if(typeof ownAnswer != 'undefined' && ownAnswer[myFormid]) {
		ownAnswer[myFormid] = <?php echo isset($answer['answers'][$form['id']]) ? json_encode($answer['answers'][$form['id']]) : json_encode(array()) ?>;
	}

    function stepValidationReload<?php echo $form["id"]?>(elemSource = ''){
        reloadInput("<?php echo $key ?>", "<?php echo $formId ?>");
        var alreadyScrolled = false;
        if(elemSource != '') {
            inputObliList.<?php echo $kunik ?>.forEach((inVal, inIndex) => {
                var inputKey = "";
                const inputTypeArr = formAllInputs['inputs'+myKunik][inVal]['type'].split('.');
                inputKey = inputTypeArr[inputTypeArr.length -1] ? inputTypeArr[inputTypeArr.length -1] : 'none';
                if(ownAnswer && ownAnswer[myFormid] && (ownAnswer[myFormid][inVal]) || ownAnswer[myFormid][inputKey+inVal]) {
                    if(ownAnswer[myFormid][inVal]) {
                        if(!notEmpty(ownAnswer[myFormid][inVal])) {
                            if(!alreadyScrolled) {
                                scrollintoDiv(`question${inVal}`, 2000);
                                alreadyScrolled = true;
                            }
                        }
                    }
                    if(ownAnswer[myFormid][inputKey+inVal]) {
                        if(!notEmpty(ownAnswer[myFormid][inputKey+inVal])) {
                            if(!alreadyScrolled) {
                                scrollintoDiv(`question${inVal}`, 2000);
                                alreadyScrolled = true;
                            }
                        }
                    }
                } else {
                    if(!alreadyScrolled) {
                        scrollintoDiv(`question${inVal}`, 2000);
                        alreadyScrolled = true;
                    }
                }
            });
        }
    }

    $(document).ready(function() {

        // if(true){
        // 	$("#question<?php echo $key ?> .btnshowvalidatelist i").removeClass().addClass("fa fa-eye-slash");
        // }

        // $("#question<?php echo $key ?> .btnshowvalidatelist").trigger("click");


        //$("#tovalidatelist<?php echo $kunik ?>").hide();

        $(".btnshowvalidatelist").click(function() {
            var thisbtn = $(this).children("i");
            var thislbl=$(this).next();
            if (thisbtn.hasClass("fa-eye-slash")){
                thisbtn.removeClass("fa-eye-slash");
                thisbtn.addClass("fa-eye");
                thislbl.html("Montrer la liste des champs obligatoires");
                $("#tovalidatelist<?php echo $kunik ?>").toggle(300);
            }else if(thisbtn.hasClass("fa-eye")){
                thisbtn.removeClass("fa-eye");
                thisbtn.addClass("fa-eye-slash");
                thislbl.html("Cacher la liste des champs obligatoires");
                $("#tovalidatelist<?php echo $kunik ?>").toggle(300);
            }

            // var thislbl=$(this).next();
            // if (thislbl.hasClass("eyeslash")){
            //     thislbl.html("Cacher la liste des champs obligatoires");
            //     thislbl.removeClass("eyeslash");
            //     thislbl.addClass("eyee");
            // }else{
            //    thislbl.html("Montrer la liste des champs obligatoires");
            //    thislbl.addClass("eyeslash");
            //    thislbl.removeClass("eyee");
            // }
        });

        // $("#showtovalidatelist").click(function() {

        // });

        // $("#tovalidatelist").hide();

        // $("#switch-valid").change(function () {
        //     if ($(this).is(":checked")) {
        //         $("#tovalidatelist").show(3000);
        //     } else {
        //         $("#tovalidatelist").hide(3000);
        //     }
        // });

        $(".gotoinput-btn").click( function() {
            scrollintoDiv($(this).data('path'), 2000);
        });
        var relid="#<?php echo $form["id"]?> :input,#<?php echo $form["id"]?> select,#<?php echo $form["id"]?> button";
        mylog.log("relid",relid);
        $("#<?php echo $form["id"]?> :input:not(.exclude-input),#<?php echo $form["id"]?> select,#<?php echo $form["id"]?> button").change(function(e) {
            e.stopImmediatePropagation();
            reloadInput("<?php echo $key ?>", "<?php echo $formId ?>");
        });


        $('a[data-dom-uploader]').each(function(){
            $("#"+$(this).data('dom-uploader')).on("allComplete", function(event, succeeded, failed) {
                reloadInput("<?php echo $key ?>", "<?php echo $formId ?>");
                event.stopImmediatePropagation();
            });
        });
        $('#<?php echo $form["id"]?> div.uploader_container').bind('DOMSubtreeModified', function(e) {
            e.stopImmediatePropagation();
            reloadInput("<?php echo $key ?>", "<?php echo $formId ?>");
        });
        /* $('#<?php echo $form["id"]?> li div.form-check.text-left').bind('DOMSubtreeModified', function(e) {
			e.stopImmediatePropagation();
			mylog.log('ownAnswer', ownAnswer);
			setTimeout(() => {
				reloadInput("<?php echo $key ?>", "<?php echo $formId ?>");
			}, 1000);
		}); */

        $('div[data-inputtype="address"]').each(function(){

            if (typeof window[$(this).data("mapobj")] !== "undefined" && typeof window[$(this).data("mapobj")].map !== "undefined" && typeof window[$(this).data("mapobj")].map.map !== "undefined") {
                window[$(this).data("mapobj")].map.map.on("viewreset" ,  function (e) {

                    $('div[data-inputtype="address"]').each(function(){

                        $(this).mouseleave(function(event){
                            reloadInput("<?php echo $key ?>", "<?php echo $formId ?>");
                            $(this).off('mouseleave');
                            event.stopImmediatePropagation();
                        })

                    });

                });
            }

        });

        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {
                "title" : "<?php echo $kunik ?> config",
                "description" : "Liste de questions possibles",
                "icon" : "fa-cog",
                "properties" : {
                    inputList : {
                        inputType : "selectMultiple",
                        label : "Question obligatoire",
                        options :  <?php echo json_encode($formList) ?>,
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.inputList
                    },
                    showMsg : {
                        inputType : "checkboxSimple",
                        label: "Afficher message des champs obligatoires pendant le remplissage",
                        params : {
                            "onText" : "Oui",
                            "offText" : "Non",
                            "onLabel" : "Afficher message",
                            "offLabel" : "Cacher message",
                            "labelInformation" : ""
                        },
                        value : sectionDyf.<?php echo $kunik ?>ParamsData['showMsg'] ? sectionDyf.<?php echo $kunik ?>ParamsData['showMsg'] : 'false',
                    },
                    step : {
                        inputType : "select",
                        label : "Etape à débloquer",
                        options :  <?php echo json_encode($stepList) ?>,
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.step
                    },
                    showChecked : {
                        inputType : "checkboxSimple",
                        label: "Montrer les champs obligatoires déjà validés",
                        params : {
                            "onText" : "Oui",
                            "offText" : "Non",
                            "onLabel" : "Montrer les champs déjà validés",
                            "offLabel" : "Cacher les champs déjà validés",
                            "labelInformation" : ""
                        }
                    },
                    showMandatoriesInit : {
                        inputType : "checkboxSimple",
                        label: "Montrer la liste des champs obligatoire au chargement",
                        params : {
                            "onText" : "Oui",
                            "offText" : "Non",
                            "onLabel" : "Montrer la liste des champs obligatoire au chargement",
                            "offLabel" : "Cacher la liste des champs obligatoire au chargement",
                            "labelInformation" : ""
                        }
                    },
                    "for":{
                        "inputType":"custom",
                        "html" : "<p> configuration mail </p>"
                    },
                    contact : {
                        inputType : "selectMultiple",
                        label : "Contact",
                        options :  <?php echo json_encode($contactType) ?>,
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.contact
                    },
                    inputscontact : {
                        inputType : "selectMultiple",
                        label : "Question avec une réponse en format email",
                        options :  <?php echo json_encode($inputListOptions) ?>,
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.inputscontact
                    },
                    object : {
                        inputType : "text",
                        label : "Objet",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.object
                    },
                    msg : {
                        inputType : "text",
                        label : "Message",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.msg
                    },
                    msgbox : {
                        inputType : "text",
                        label : "Text à afficher si validé",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.msgbox
                    },
                    notif : {
                        inputType : "text",
                        label : "Notification à afficher si validé",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.notif
                    }
                    <?php if($lastForm) {?>
                    ,
                    urlRedirect : {
                        inputType : "url",
                        label : "Lien de redirection (externe [commençant par http(s)://] ou interne [commençant par #])",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.urlRedirect
                    },
                    forbiddenChange : {
                        inputType : "checkboxSimple",
                        label: "Modification impossible après envoi",
                        params : {
                            "onText" : "Oui",
                            "offText" : "Non",
                            "onLabel" : "Modification impossible après envoi",
                            "offLabel" : "Modification encore possible après envoi",
                            "labelInformation" : ""
                        }
                    }
                    <?php } ?>

                },
                save : function () {
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                        if(val.inputType == "properties")
                            tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                        else if(val.inputType == "array")
                            tplCtx.value[k] = getArray('.'+k+val.inputType);
                        else
                            tplCtx.value[k] = $("#"+k).val();
                        mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                    });
                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined"){
                        toastr.error('value cannot be empty!');
                    }
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.closeForm();
                            urlCtrl.loadByHash(location.hash);
                            //reloadWizard();
                        } );
                    }

                }
            }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = $(this).data("path");
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });

        $(".validate<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtxR = {};
            tplCtxR.id = "<?php echo $answer["_id"] ?>";
            tplCtxR.collection = "<?php echo Form::ANSWER_COLLECTION ?>";
            tplCtxR.path = "<?php echo "answers.".$form["id"].".".$key ?>";
            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            today = dd + '/' + mm + '/' + yyyy;

            tplCtxR.value = today;
            dataHelper.path2Value( tplCtxR, function(params) {});

            // stepVld.newThis("<?php echo $answer["_id"] ?>");

            <?php if($paramsData['step'] != $form['id']) { ?>
            tplCtx = {};
            tplCtx.id = "<?php echo $answer["_id"] ?>";
            tplCtx.path = "step";
            tplCtx.collection = "<?php echo Form::ANSWER_COLLECTION ?>";
            var actualStep = "<?php echo $paramsData["step"] ?>";
            tplCtx.value = (typeof parentForm.hasStepValidations!="undefined" && parentForm.hasStepValidations!=="") ? actualStep : "";
            dataHelper.path2Value( tplCtx, function(params) {
                localStorage.setItem("wizardStep_<?php echo $answer["_id"] ?>","#<?php echo $paramsData["step"] ?>");
                //urlCtrl.loadByHash(location.hash);
                unlockStep(actualStep);
            } );
            <?php }else{?>
            $("#ajax-modal #ajax-modal-modal-body").append("<h1>Votre formulaire a bien été envoyé</h1>");
            $("#ajax-modal .modal-header").hide();
            $("#ajax-modal").modal('show');
            if(typeof sectionDyf.<?php echo $kunik ?>ParamsData.forbiddenChange!="undefined" && (sectionDyf.<?php echo $kunik ?>ParamsData.forbiddenChange=="true" || sectionDyf.<?php echo $kunik ?>ParamsData.forbiddenChange==true)){
                tplCtx = {};
                tplCtx.id = "<?php echo $answer["_id"] ?>";
                tplCtx.path = "validated";
                tplCtx.collection = "<?php echo Form::ANSWER_COLLECTION ?>";
                tplCtx.value = true;
                dataHelper.path2Value( tplCtx, function(params) {
                } );
            }
            setTimeout(
                function()
                {
                    <?php if(isset($paramsData["urlRedirect"]) && !empty($paramsData["urlRedirect"])){
                    //alert("yoyo");
                    if(strrpos($paramsData["urlRedirect"],"http")!=false){ ?>
                    document.location.href="<?php echo $paramsData["urlRedirect"];?>";
                    // if ( costum != null) {
                    // 	location.reload();
                    <?php }else if(strrpos($paramsData["urlRedirect"],"#")==0){ ?>
                    urlCtrl.loadByHash("<?php echo $paramsData["urlRedirect"];?>")

                    <?php } ?>

                    <?php }
                    else{ ?>
                    //urlCtrl.loadByHash(location.hash);
                    //reloadWizard();
                    <?php }?>
                }, 3000);
            <?php }?>
            var standaloneBtnRedir=(location.hash.includes("standalone.true")) ? ".standalone.true" : "";
            var answerId = <?php echo json_encode((String)$answer['_id']); ?>;
            var paramsmail<?php echo $kunik ?> = {
                tpl : sectionDyf.<?php echo $kunik ?>ParamsData.tplMail,
                tplObject : "<?php echo (isset($parentForm["params"][$kunik]["object"]) ? $parentForm["params"][$kunik]["object"] : "") ?>",
                tplMail : contacttplMail<?php echo $kunik ?>,
                html: "<?php echo (isset($parentForm["params"][$kunik]["msg"]) ? $parentForm["params"][$kunik]["msg"] : "") ?>",
                community : <?php echo (!empty($selectedContact["roles"]) ? json_encode($selectedContact) : "[]" ) ?>,
                btnRedirect : {
                    hash : "#answer.index.id." + answerId + ".mode.w"+standaloneBtnRedir,
                    label : "Accéder au formulaire"
                }
            };


            if(paramsmail<?php echo $kunik ?>.tplMail!=[] && paramsmail<?php echo $kunik ?>.html!="" && paramsmail<?php echo $kunik ?>.tplObject!=""){
                mylog.log('azee', paramsmail<?php echo $kunik ?>);
                ajaxPost(
                    null,
                    baseUrl+"/co2/mailmanagement/createandsend",
                    paramsmail<?php echo $kunik ?>,
                    function(data){
                        toastr.success("<?php echo (!empty($parentForm["params"][$kunik]["notif"]) ? $parentForm["params"][$kunik]["notif"] : "Mail de confirmation envoyée") ?>");
                    }
                );
            }


        });





    });
</script>
