<?php
	$permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	 
	function generate_string($input, $strength = 16) {
	    $input_length = strlen($input);
	    $random_string = '';
	    for($i = 0; $i < $strength; $i++) {
	        $random_character = $input[mt_rand(0, $input_length - 1)];
	        $random_string .= $random_character;
	    }
	 
	    return $random_string;
	}
	$random_id="";

$answer_collection = PHDB::find(Form::ANSWER_COLLECTION,$arrayName = array('form' =>(string)$parentForm["_id"]));

$id_form = $parentForm["subForms"][0];
$id_fields = $parentForm["subForms"][0]."1";
$me = isset(Yii::app()->session['userId']) ? Person::getById(Yii::app()->session['userId']) : null;
//var_dump((string)$me["_id"]);
//var_dump($answer["answers"][$id_form][$id_fields])
//AUTHORIZATION VARIABLE--------------------------------------------------------------
  $authorization = "";
  $public_login_not_required = "";
  $public_login_required = "";
  $private_to_a_community = "";
  $private_to_the_role_of_a_community = "";

  if (isset($parentForm["params"]["proposer".$id_fields]["configuration"]["droit_acces"])) {
    $authorization = $parentForm["params"]["proposer".$id_fields]["configuration"]["droit_acces"];
    $public_login_not_required = (!empty($authorization) && $authorization == "public_login_not_required")? true : false ;
    $public_login_required = (!empty($authorization) && $authorization == "public_login_required")? true : false ;
    $private_to_a_community = (!empty($authorization) && $authorization == "private_to_a_community")? true : false;
    $private_to_the_role_of_a_community = (!empty($authorization) && $authorization == "private_to_the_role_of_a_community")? true : false;
  }
//END AUTHORIZATION VARIABLE-----------------------------------------------------------

//PARAMETERS VARIABLE-------------------------------------------------------------------
  $config_param =null;
  $nom_element =""; 
  $type_choix_vote="";
  $type_proposition ="";
  $date_demarage_proposition ="";
  $duree_proposition ="";
  $duree_vote ="";
  $droit_acces ="";
  if (isset($parentForm["params"]["proposer".$id_fields]["configuration"])) {
    $config_param      = $parentForm["params"]["proposer".$id_fields]["configuration"];
    $type_choix_vote   = !empty($config_param["type_choix_vote"]) ? $config_param["type_choix_vote"] : "";
    $nom_element       = !empty($config_param["nom_element"]) ? $config_param["nom_element"] : "";
    $type_proposition  = !empty($config_param["type_proposition"]) ? $config_param["type_proposition"] : "";
    $date_demarage_proposition     = !empty($config_param["date_demarage"]) ? $config_param["date_demarage"] : "";
    $duree_proposition = !empty($config_param["duree_proposition"]) ? $config_param["duree_proposition"] : "";
    $duree_vote        = !empty($config_param["duree_vote"]) ? $config_param["duree_vote"] : "";
    $droit_acces       = !empty($config_param["droit_acces"]) ? $config_param["droit_acces"] : "";
  }
//END PARAMETERS VARIABLE----------------------------------------------------------------

//VERIFY_IF_MEMBER_OF_COMMUNITY---------------------------------------
 $is_member_of_community = false;
 $id_community = isset(array_keys($parentForm["parent"])["0"]) ? array_keys($parentForm["parent"])["0"] :null;
 
 if(isset($me["links"]["memberOf"]) ){
    foreach ($me["links"]["memberOf"] as $key => $value) {
      if($key == $id_community ){
          $is_member_of_community = true;
       }
    }  
 }
 //echo $is_member_of_community ? "je suis membre<br>" : "Je ne suis pas membre<br>";
//END VERIFY MEMBER OF COMMUNITY---------------------------------------

 //VERIFY_IF_ROLES_IN_COMMUNITY---------------------------------------
$is_have_roles_required = false;

$config_roles = isset($parentForm["params"]["proposer".$id_fields]["configuration"]["roles"]) ? $parentForm["params"]["proposer".$id_fields]["configuration"]["roles"] : null;
$roles= [];


if (isset($me["links"]["memberOf"][array_keys($parentForm["parent"])["0"]]["roles"]) && $config_roles!=null) {
  $roles =  $me["links"]["memberOf"][array_keys($parentForm["parent"])["0"]]["roles"];
  foreach ($roles as $key => $value) {
  	if (in_array($value, $config_roles)) {
            $is_have_roles_required = true;
     }
  }
}
// var_dump($roles);
// var_dump($id_fields);
// var_dump("proposer".$id_fields);

// echo $is_have_roles_required ? "j'ai un role" : "je n'ai pas un role";
// var_dump($roles);
// var_dump($is_have_roles_required);
//END VERIFY_IF_ROLES_IN_COMMUNITY---------------------------------------

//BEGIN PERCENTEGE PERSON VOTED-------------------------------------
$vote_percentage=null;
$vote_fraction="";


if ($public_login_required) {
	$count_citoyen = PHDB::count(Person::COLLECTION);
	$userIdArray = [];

    foreach ($answer_collection as $key => $value) {
    	if (isset($value["answers"][$id_form][$id_fields])) {
    		foreach ($value["answers"][$id_form][$id_fields] as $k => $v) {
    			if (isset($v["vote"])) {
    				foreach ($v["vote"] as $kk => $vv) {
						if (!in_array(@$vv["user"], $userIdArray)) {
							array_push($userIdArray, @$vv["user"]);
						}
    				}
    			}
    		}
    	}
    }
	if ($count_citoyen!=null && $count_citoyen!=0) {
		$vote_percentage = (count($userIdArray)*100)/$count_citoyen;
		$vote_fraction = "<sup>".count($userIdArray)."</sup>&frasl;<sub>".$count_citoyen."</sub>";
	}
}



if ($private_to_the_role_of_a_community) {
	function check($array,$config){
	$count=0;
		foreach ($array as $key => $value) {
			if (in_array($value, $config)) {
				$count++;
			}
		}
		return $count;
	}
	$citoyen = PHDB::find(Person::COLLECTION);
	$count_citoyen =0;
	$array_citoyen=[];
	foreach ($citoyen as $key => $value) {
		if (isset($value["links"]["memberOf"][array_keys($parentForm["parent"])["0"]]["roles"])) {
			$memberOf = $value["links"]["memberOf"][array_keys($parentForm["parent"])["0"]]["roles"];
			if (isset($config_roles) && $config_roles!=null && isset($memberOf) && $memberOf != null) {
				$check = check($memberOf,$config_roles);
			}
			
			if (isset($check) && $check != 0) {
				array_push($array_citoyen, $check);
			}
			
		}
	}
	$count_citoyen = count($array_citoyen);
	//var_dump($count_citoyen);

	$nb_participated=0;
	$array_participated=[];
	foreach ($answer_collection as $key => $value) {
		if (isset($value["answers"][$id_form][$id_fields])) {
			foreach ($value["answers"][$id_form][$id_fields] as $k => $v) {
				if (isset($v["roles"]) && isset($v["vote"]) ) {
					foreach ($v["vote"] as $kk => $vv) {
						if (!in_array($vv["user"], $array_participated)) {
							array_push($array_participated, $vv["user"]);
						}
					}
				}
				
			}
		}
	}
	$nb_participated= count($array_participated);
	
	if ($count_citoyen!=null && $count_citoyen!=0) {
		$vote_percentage = ($nb_participated*100)/$count_citoyen;
		$vote_fraction = "<sup>".$nb_participated."</sup>&frasl;<sub>".$count_citoyen."</sub>";
	}
};

if ($private_to_a_community) {
	$citoyen = PHDB::find(Person::COLLECTION);
	$count_citoyen =0;
	$array_citoyen=[];
	foreach ($citoyen as $key => $value) {
		if (isset($value["links"]["memberOf"][array_keys($parentForm["parent"])["0"]])) {
			array_push($array_citoyen, array_keys($parentForm["parent"])["0"]);
		}
	}
	$count_citoyen = count($array_citoyen);
	//var_dump($count_citoyen);

	$nb_participated=0;
	$array_participated=[];
	foreach ($answer_collection as $key => $value) {
	   if (isset($value["answers"][$id_form][$id_fields])) {
			foreach ($value["answers"][$id_form][$id_fields] as $k => $v) {
				if (isset($v["vote"]) && isset($v["communityId"]) && $v["communityId"] ==array_keys($parentForm["parent"])["0"] ) {
					foreach ($v["vote"] as $kk => $vv) {
						if (isset($vv["user"])) {
							if (!in_array($vv["user"], $array_participated)) {
								array_push($array_participated, $vv["user"]);
							}
						}
					}
				}
			}
	   }
	}

	$nb_participated = count($array_participated);
	if ($count_citoyen!=null && $count_citoyen!=0) {
		$vote_percentage = ($nb_participated*100)/$count_citoyen;
		$vote_fraction = "<sup>".$nb_participated."</sup>&frasl;<sub>".$count_citoyen."</sub>";
	}
}

//var_dump($vote_percentage);
//END PERCENTEGE PERSON VOTED---------------------------------------


//BEGIN FONCTION GET IMAGE PROPERTY ------------------------------------------------
function getImageProperty($myanswer,$myanswers,$myanswerPath,$key){
$initAnswerFiles=Document::getListDocumentsWhere(array(
    "id"=>(string)$myanswer["_id"], 
    "type"=>'answers',
    "subKey"=>$myanswerPath.$key), "file");
    if(!empty($initAnswerFiles)){
        $myanswers[$key]["files"] = $initAnswerFiles;
        foreach ($initAnswerFiles as $key => $d) {
                return $d;
             }
    }
} 
//END FONCTION GET IMAGE PROPERTY -----------------------------------------------

//BEGIN GET MAX POUR------------------------------------------
$max_user_pour="";
$max_proposition_pour="";
$max_image_pour="";
$max_vote_pour=0;
$min_vote_pour=0;

$max_user_contre="";
$max_proposition_contre="";
$max_image_contre="";
$max_vote_contre=0;
$min_vote_contre=0;

$array_max=[];
foreach ($answer_collection as $k => $v) {
	if (isset($v["answers"][$id_form][$id_fields])) {
		foreach ($v["answers"][$id_form][$id_fields] as $kk => $vv) {
				if (isset($vv["vote"])) {
					//var_dump($vv["user"]." AVEC  ".$vv["titre"]);
					$total_temp_pour=0;
					$total_temp_contre=0;
					foreach ($vv["vote"] as $kkk => $vvv) {
						if($vvv["choix"]=="pour") $total_temp_pour++;
						if($vvv["choix"]=="contre") $total_temp_contre++;
					}
					if ($max_vote_pour < $total_temp_pour) $max_vote_pour = $total_temp_pour;
					if ($max_vote_contre < $total_temp_contre) $max_vote_contre = $total_temp_contre;
					
				}
				
		}
    }
}
//var_dump($max_vote_contre);
foreach ($answer_collection as $k => $v) {
	if (isset($v["answers"][$id_form][$id_fields])) {
		foreach ($v["answers"][$id_form][$id_fields] as $kk => $vv) {
			if (isset($vv["vote"])) {
				$total_temp_pour=0;
				$total_temp_contre=0;
				foreach ($vv["vote"] as $kkk => $vvv) {
					if($vvv["choix"]=="pour") $total_temp_pour++;
					if($vvv["choix"]=="contre") $total_temp_contre++;
				}
				if ($total_temp_pour == $max_vote_pour) {
					array_push($array_max, 
						array("max_temporaire_pour"=> $total_temp_pour,
							  "max_temporaire_contre"=> $total_temp_contre,
							 "max_user" => isset($vv["user"]) ? $vv["user"] :null,
							 "max_proposition"  => isset($vv["titre"]) && $type_proposition=="text" ? $vv["titre"] :null,
							 "max_image" => getImageProperty(
												$v, 
												$v["answers"][$id_form][$id_fields],
												"answers.".$id_form.".".$id_fields.".",
												$kk
											)
							)

					);
				}
			}
		}
	}
}
//var_dump($array_max);
//END GET MAX POUR--------------------------------------------

//BEGIN COLOR DEGRADE ------------------------------------------------
function colorDegradePour($value,$max,$data="",$random_id){
 $tab= [$max/1,$max/2,$max/3,$max/4,$max/5,$max/6];
	if($value<=$tab[5]){
		echo "<span class='label pour btn' data-data='".$data."' style='border-color : grey; border-radius: 50%;background-color:#ccffdd;color:grey'>".$value."</span>
		<script>
			var x = document.getElementById('".$random_id."').parentElement;
			x.parentElement.style.backgroundColor = '#ccffdd';
			x.parentElement.style.color = 'black';
		</script>
		";
    }
	elseif($value>$max[5] && $value<=$tab[4]){
		echo "<span class='label pour btn' data-data='".$data."' style='border-color : white; border-radius: 50%;background-color:#80ffaa'>".$value."</span>";
	}
	elseif($value>$max[4] && $value<=$tab[3]){
		echo "<span class='label pour btn' data-data='".$data."' style='border-color : white; border-radius: 50%;background-color:#33ff77'>".$value."</span>
		<script>
			var x = document.getElementById('".$random_id."').parentElement;
			x.parentElement.style.backgroundColor = '#33ff77';
			x.parentElement.style.color = 'black';
		</script>
		";
	}
	elseif($value>$max[3] && $value<=$tab[2]){
		echo "<span class='label pour btn' data-data='".$data."' style='border-color : white; border-radius: 50%;background-color:#00ff55'>".$value."</span>
		<script>
			var x = document.getElementById('".$random_id."').parentElement;
			x.parentElement.style.backgroundColor = '#00ff55';
			x.parentElement.style.color = 'black';
		</script>";
	}
	elseif($value>$max[2] && $value<=$tab[1]){
		echo "<span class='label pour btn' data-data='".$data."' style='border-color : white; border-radius: 50%;background-color:#00cc44'>".$value."</span>
		<script>
				var x = document.getElementById('".$random_id."').parentElement;
				x.parentElement.style.backgroundColor = '#00cc44';
				x.parentElement.style.color = 'black';
		</script>";
	}
	elseif($value>$max[1] && $value<$tab[0]){
		echo "<span class='label pour btn' data-data='".$data."' style='border-color : white; border-radius: 50%;background-color:#009933'>".$value."</span>
			<script>
				var x = document.getElementById('".$random_id."').parentElement;
				x.parentElement.style.backgroundColor = '#009933';
				x.parentElement.style.color = 'black';
			</script>";
	}
	elseif($value==$tab[0]){
		echo "
		   <span class='label pour btn' data-data='".$data."' style='border-color : white; border-radius: 50%;background-color:#003311;color:white'>
		   	<i class='fa fa-star' aria-hidden='true'></i> ".$value."
		   </span>
		   <script>
				var x = document.getElementById('".$random_id."').parentElement;
				x.parentElement.style.backgroundColor = '#003311';
				x.parentElement.style.color = 'white';
			</script>";
	}


}

function contre($value,$data=""){
echo "<span class='label label-danger contre btn' data-data='".$data."' style='border-radius: 100%'>".$value."</span>";
}
//END COLOR DEGRADE---------------------------------------------------

//BEGIN FONCTION GET IMAGE ------------------------------------------------
function getImageChoisir($myanswer,$myanswers,$myanswerPath,$key){
$initAnswerFiles=Document::getListDocumentsWhere(array(
    "id"=>(string)$myanswer["_id"], 
    "type"=>'answers',
    "subKey"=>$myanswerPath.$key), "file");

/*var_dump($myanswer);
var_dump($myanswers);
var_dump($myanswerPath.$key);*/


    if(!empty($initAnswerFiles)){
        $myanswers[$key]["files"] = $initAnswerFiles;
        foreach ($initAnswerFiles as $key => $d) {          
                echo "<div style='width: 100%;' class='padding-5 margin-top-5 margin-bottom-5' id='".$key."'>
                   <img 
                   src='".@$d["docPath"]."' 
	                   data-docpath='".@$d["docPath"]."' 
	                   class='img-responsive' 
	                   style='width:60px;height:60px;cursor:pointer;border-radius:80%'
                   >
                </div>";
            
             }
    }else {
    echo "<div style='width: 100%;' class='col-xs-12 col-sm-4 padding-5 shadow2 margin-top-5 margin-bottom-5' >
        <img 
          style='width:60px;height:60px'
          src='".Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl()."/images/thumbnail-default.jpg' 
          data-docpath='".Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl()."/images/thumbnail-default.jpg'
          class='img-responsive'>
    </div>";
    }
} 
//END FONCTION GET IMAGE -----------------------------------------------



//BEGIN CHECK IF PARTICIPATED IN VOTE-----------------------------------
	$is_already_participated_general=false;

	foreach ($answer_collection as $key => $value) {
		if (isset($value["links"]["already_voted"][(string)$me["_id"]]) && $value["links"]["already_voted"][(string)$me["_id"]]=="1") {
				$is_already_participated_general=true;
			}
		}
//var_dump($is_already_participated_general);
//END CHECK IF PARTICIPATED IN VOTE-------------------------------------

//BEGIN VERIFY PROPOSITION EXPIRATION DATE--------------------------------
  $today = strtotime(date("d-m-Y"));

  $date_demarage_proposition2 =""; 
  $proposition_expire_date ="";
  $is_authorize_vote_date =false;

  $date_demarage_vote ="";
  $vote_expire_date="";
  $is_authorize_vote_date=false;
  $vote_date_place="";
  if (!empty($date_demarage_proposition) && !empty($duree_proposition)) {
      $date_demarage_proposition2 = date("d-m-Y", strtotime(str_replace('/', '-', $date_demarage_proposition)));
      $proposition_expire_date =  date('d-m-Y', strtotime($date_demarage_proposition2. ' + '.($duree_proposition-1).' days'));
      if ($today >= strtotime($date_demarage_proposition2) && $today <= strtotime($proposition_expire_date) )  {
        $is_authorize_vote_date = true;
      }
  }
  if (!empty($proposition_expire_date) && !empty($duree_vote)) {
  	$date_demarage_vote = date('d-m-Y', strtotime($proposition_expire_date. ' + 1 days'));
  	$vote_expire_date = date('d-m-Y', strtotime($date_demarage_vote. ' + '.($duree_vote-1).' days'));
  	if ($today >= strtotime($date_demarage_vote) && $today <= strtotime($vote_expire_date) )  {
        $is_authorize_vote_date = true;
        $vote_date_place="in";
      }elseif ($today < strtotime($date_demarage_vote)) {
        $is_authorize_vote_date = false;
        $vote_date_place="before";
      }elseif ($today > strtotime($vote_expire_date)) {
        $is_authorize_vote_date = false;
        $vote_date_place="after";
      }	
  }
// var_dump($is_already_participated_general);
// var_dump($is_authorize_vote_date);


  // echo "<br> Démarage proposition: ".$date_demarage_proposition2;
  // echo "<br> Durée de proposition: ".$duree_proposition;
  // echo "<br> Expiration proposition date: ".$proposition_expire_date;

  // echo("<br>-----------------------------------------------");
  // echo "<br> Démarage vote: ".$date_demarage_vote;
  // echo "<br> Durée de vote: ".$duree_vote;
  // echo "<br> Expiration vote date: ".$vote_expire_date;
  
  
  // echo "<br> Aujourd'hui: ".date("d-m-Y");
//END VERIFY PROPOSITION EXPIRATION DATE--------------------------------

//BEGIN GLOBAL AUTHORIZATION-----------------------------
  $allow=false;
  if($public_login_not_required && $is_authorize_vote_date){
    $allow =true;
  }elseif ($public_login_required && $is_authorize_vote_date) {
    $allow =true;
  }elseif ($private_to_a_community && $is_member_of_community && $is_authorize_vote_date) {
    $allow =true;
  }elseif($private_to_the_role_of_a_community && $is_member_of_community && $is_have_roles_required && $is_authorize_vote_date) {
   $allow =true;
  }else{
    $allow =false;
  }
  /*var_dump($private_to_the_role_of_a_community);
  var_dump($is_member_of_community);
  var_dump($is_have_roles_required);
  var_dump($is_authorize_vote_date);*/

//END GLOBAL AUTHORIZATION-------------------------------



$vote = [
	"choix" => [
        "label" => "Pour ou Contre",
        "placeholder" => "Pour ou Contre",
        "inputType" => "select",
        "options" => [
        	"pour" => "je suis pour",
        	"contre" => "je suis contre"
        ],
        "rules" => [ "required" => true ]
    ],
    "user" => [
        "inputType" => "hidden",
        "value" => (string)$me["_id"],
        "rules" => [ "required" => true ]
    ],
]

 ?>

 <!-- BEGIN SHOW PARAMETER-------------------------------- -->
<?php if ($mode == "r" || $mode == "pdf"){ ?>
	<div class="alert alert-danger">
	  <strong>
	  	<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
	  </strong> 
	  Pour l'instant, le vote est disponible en mode écriture
	</div>
<?php }else{ ?>

	
 <?php if (!empty($date_demarage_vote) && !empty($vote_expire_date) && !empty($duree_vote)): ?>
 	Le vote commence le <strong><?php echo $date_demarage_vote ?></strong> et se termine le <strong><?php echo $vote_expire_date ?> (<?php echo $duree_vote ?> jours)</strong><br>
 <?php endif ?>
 

 <?php echo $vote_date_place=="before" ? 
 "<span class='text-danger'>Le vote commence le : ".$date_demarage_vote."</span><br>" : null ?>
 <?php echo $vote_date_place=="after" ? 
 "<span class='text-danger'>Le vote est términé le : ".$vote_expire_date."</span><br>" : null ?>
 <?php echo $is_already_participated_general && $type_choix_vote=="one" ? 
 "<span class='text-danger'>Vous avez déja voté,vous ne pouvez plus particiter à ce vote!</span><br>" : null ?>
 <?php echo $vote_date_place=="in"  ? 
 "<span class='text-success'>Vote en cours <i class='fa fa-circle-o-notch fa-pulse' aria-hidden='true'></i> </span><br>" : null ?>
 <?php echo $public_login_not_required && $is_authorize_vote_date ? 
    "<span class='text-success'>Vote est publique</span><br>" : null ?>

<?php echo $public_login_required && $is_authorize_vote_date ? 
"<span class='text-success'>Vote reservé au utilisateur connécté</span><br>" : null ?>

<?php echo $private_to_a_community && $is_member_of_community  &&  $is_authorize_vote_date ? 
"<span class='text-success'>Vote reservé à une communauté</span><br>" : null ?>

<?php echo $private_to_the_role_of_a_community && $is_member_of_community && $is_have_roles_required && $is_authorize_vote_date ? 
"<span class='text-success'>Vote reservé à un ou des rôles d'une communauté</span><br>" : null ?>
<?php echo $allow ?  
"<span class='text-success'>Vous pouvez voter maintenant</span><br>" : 
"<span class='text-danger'>Vous ne pouvez pas voter</span><br>" ?>
<?php echo $type_choix_vote=="many" ? 
	"<span class='text-success'>Vous pouvez choisir plusieurs proposition</span><br>" : 
	"<span class='text-success'>Vous pouver choisir un seul proposition</span><br>" 
?>
<?php if (!empty($vote_percentage)) {
	echo round($vote_percentage,2) >= 50 ? 
	"<span class='text-success'><strong> Taux de participation au vote: ".$vote_fraction." soit ".round($vote_percentage,2)."%</strong></span><br>":
	"<span class='text-danger'><strong> Taux de participation au vote: ".$vote_fraction." soit ".round($vote_percentage,2)."%</strong></span><br>";
} ?>

<select  id="show_line" class="form-control input-sm" style="width: 30%">
	<option value="all_line" <?php echo @$answer["config"]["showLine"] == "all_line" ? "selected":null ?> >Tous les lignes</option>
	<option value="max_line" <?php echo @$answer["config"]["showLine"] == "max_line" ? "selected":null ?> >Maximum seulement</option>
	<option value="my_line" <?php echo @$answer["config"]["showLine"] == "my_line" ? "selected":null ?> >Mes propositions</option>
    <option value="no_vote" <?php echo @$answer["config"]["showLine"] == "no_vote" ? "selected":null ?>>Aucun vote</option>
</select>


 <!-- END SHOW PARAMETER---------------------------------- -->


<!--BEGIN  MAX ALERT------------------------------- -->
<!--  <div class="alert alert-success">
		<?php 
	      echo isset($parentForm["params"]["proposer".$id_fields]["configuration"]["nom_element"]) ? 
	            $parentForm["params"]["proposer".$id_fields]["configuration"]["nom_element"] :
	            "Proposition";
	     ?>
	 <strong>
	 	<img src="" alt="">
	     <?php echo $type_proposition =="text" ? $max_proposition_pour : null ?>
	     <?php echo $type_proposition =="image" ?  "<img style='width:60px;height:60px' src='".@$max_image_pour["docPath"]."' alt=''>" : null ?>

	 </strong> 
	     proposé par <strong><?php echo $max_user_pour ?></strong> a le maximum de vote : <strong><?php echo $max_vote_pour ?> votes</strong>
</div> -->

<!-- <?php if (isset($array_max) && count($array_max)!=0 ): ?>
	<?php foreach ($array_max as $key => $value) { ?>
<div class="alert alert-success">
		<?php 
	      echo isset($parentForm["params"]["proposer".$id_fields]["configuration"]["nom_element"]) ? 
	            $parentForm["params"]["proposer".$id_fields]["configuration"]["nom_element"] :
	            "Proposition";
	     ?>
	 <strong>
	 	<img src="" alt="">
	     <?php echo $type_proposition =="text" ? @$value["max_proposition"]  : null ?>
	     <?php echo $type_proposition =="image" ?  "<img data-docpath='".@$value["max_image"]["docPath"]."' class='img-responsive'  style='width:60px;height:60px;border-radius:80%' src='".@$value["max_image"]["docPath"]."' alt=''>" : null ?>
	     <br>

	 </strong> 
	     proposé par <strong><?php echo @$value["max_user"] ?></strong> a le maximum de vote <br> 
	     <strong><?php echo @$value["max_temporaire_pour"] ?> Pour |</strong>
	     <strong class="text-danger"><?php echo @$value["max_temporaire_contre"] ?> Contre</strong>
</div>

 <?php } ?>
<?php endif ?> -->
<!-- END MAX ALERT---------------------------------- -->


<!--BEGIN PANEL CHOICE ------------------------------->
     <table class="table table-borderded" style="border-top-color: #93C01F">
        <thead>
          <tr>
          	<td><strong>Proposer par</strong></td>
            <td>
            	<strong>
	            	<?php 
	                  echo isset($parentForm["params"]["proposer".$id_fields]["configuration"]["nom_element"]) ? 
	                        $parentForm["params"]["proposer".$id_fields]["configuration"]["nom_element"] :
	                        "Proposition";
	                 ?>
                 </strong>
             </td>
            <td><strong>Choix</strong></td>
            <!-- <td><strong>Choix</strong></td> -->
            <td><strong>Total</strong></td>
          </tr>
        </thead>
        <tbody>
          <?php 
			foreach($answer_collection as $q => $v){
				if (isset($v["answers"][$id_form][$id_fields])) {
					/*var_dump($v);
					var_dump($v["answers"][$id_form][$id_fields]);
					var_dump("answers.".$id_form.".".$id_fields);*/
					foreach ($v["answers"][$id_form][$id_fields] as $key => $value) {
			?>			
						     <tr style="height:100px;">
						     	<td style="vertical-align:center">
						     		<?php if (isset($value["userId"]) && $value["userId"]==(string)$me["_id"]) {
						     			echo "Moi";
						     		}else{
						     			echo isset($value["user"]) ? $value["user"] :null;	
						     		} ?>    		
						     	</td>
								<td>
									<?php 
				                      if ($type_proposition =="image"){
				                       getImageChoisir(
											$v, 
											$v["answers"][$id_form][$id_fields],
											"answers.".$id_form.".".$id_fields.".",
											$key
										);
				                      }
				                      if($type_proposition =="text" || $type_proposition =="date"){
				                      	echo isset($value["proposition"]) ? $value["proposition"] :null;
				                      }
				                      if($type_proposition == "adresse"){
				                      	echo @$value["address"]["streetAddress"]."/".@$value["address"]["addressLocality"]."/".@$value["address"]["level1Name"]."<br>lat/long: ".@$value["coord"]["latitude"]."/".$value["coord"]["longitude"];
				                      	;
				                      	;
				                      }
				                    ?>
								</td>
								<!-- <td>
									<?php if ($is_already_participated_general || $allow == false) { 
										echo "<i class='fa fa-remove text-danger'></i>";
									 }else{ 
							   			$data_length = isset($v["answers"][$id_form][$id_fields][$key]["vote"]) ? 
											count($v["answers"][$id_form][$id_fields][$key]["vote"]) :0;
										echo "<a 
											href='javascript:;' 
											data-id='".(string)$v["_id"]."'
											data-path='answers.".$id_form.".".$id_fields.".".$key.".vote'
											data-collection='".Form::ANSWER_COLLECTION."'
											data-length='".$data_length."'
											data-type='vote'
											class='addchoisir".$id_fields." btn btn-success btn-sm'
										>
										Choix
										</a>";
									 } ?>
								</td> -->
								<td valign="middle">
								<?php 
								if ($allow && $type_choix_vote =="many") {//***********
									if (isset($v["answers"][$id_form][$id_fields][$key]["vote"])) {//-------------
										$is_already_participated_partial=false;
										$mykey="";
										foreach ($v["answers"][$id_form][$id_fields][$key]["vote"] as $kc => $vc) {
											//var_dump($kc);
											// var_dump(@$vc["user"]);
											//var_dump((string)$me["_id"]);
											if (isset($vc["user"]) && $vc["user"]==(string)$me["_id"]) {
												$is_already_participated_partial=true;
												$mykey=$kc;
											}
										}
										//var_dump($is_already_participated_partial);
										if ($is_already_participated_partial) {
											$data_length = isset($v["answers"][$id_form][$id_fields][$key]["vote"]) ? 
											count($v["answers"][$id_form][$id_fields][$key]["vote"]) :0;
											echo "<a 
												href='javascript:;' 
												data-id='".(string)$v["_id"]."'
												data-path='answers.".$id_form.".".$id_fields.".".$key.".vote.".$mykey."'
												data-collection='".Form::ANSWER_COLLECTION."'
												data-length='".$data_length."'
												data-type='changer_vote'
												class='addchoisir".$id_fields." btn btn-default btn-sm'
												title='Clique pour changer de vote'
											>
											<i class='fa fa-repeat fa-2x text-success'></i>
											</a>";
										}else{
											$data_length = isset($v["answers"][$id_form][$id_fields][$key]["vote"]) ? 
												count($v["answers"][$id_form][$id_fields][$key]["vote"]) :0;
											echo "<a 
												href='javascript:;' 
												data-id='".(string)$v["_id"]."'
												data-path='answers.".$id_form.".".$id_fields.".".$key.".vote'
												data-collection='".Form::ANSWER_COLLECTION."'
												data-length='".$data_length."'
												data-type='vote'
												class='addchoisir".$id_fields." btn btn-default btn-sm'
												title='Clique pour voter'
											>
											<i class='fa fa-hand-o-up fa-2x text-success'></i>

											</a>";
										}
									}else{//---------------------------------------------------------------------------------
										$data_length = isset($v["answers"][$id_form][$id_fields][$key]["vote"]) ? 
												count($v["answers"][$id_form][$id_fields][$key]["vote"]) :0;
											echo "<a 
												href='javascript:;' 
												data-id='".(string)$v["_id"]."'
												data-path='answers.".$id_form.".".$id_fields.".".$key.".vote'
												data-collection='".Form::ANSWER_COLLECTION."'
												data-length='".$data_length."'
												data-type='vote'
												class='addchoisir".$id_fields." btn btn-default btn-sm'
												title='Clique pour voter'
											>
											<i class='fa fa-hand-o-up fa-2x text-success'></i>									
											</a>";

									} 
								}elseif ($allow && $type_choix_vote =="one") {
									if (isset($v["answers"][$id_form][$id_fields][$key]["vote"])) {//-------------
										$is_already_participated_partial=false;
										$mykey="";
										foreach ($v["answers"][$id_form][$id_fields][$key]["vote"] as $kc => $vc) {
											//var_dump($kc);
											// var_dump(@$vc["user"]);
											//var_dump((string)$me["_id"]);
											if (isset($vc["user"]) && $vc["user"]==(string)$me["_id"]) {
												$is_already_participated_partial=true;
												$mykey=$kc;
											}
										}
										//var_dump($is_already_participated_partial);
										if ($is_already_participated_partial) {
											$data_length = isset($v["answers"][$id_form][$id_fields][$key]["vote"]) ? 
											count($v["answers"][$id_form][$id_fields][$key]["vote"]) :0;
											echo "<a 
												href='javascript:;' 
												data-id='".(string)$v["_id"]."'
												data-path='answers.".$id_form.".".$id_fields.".".$key.".vote.".$mykey."'
												data-collection='".Form::ANSWER_COLLECTION."'
												data-length='".$data_length."'
												data-type='changer_vote'
												class='addchoisir".$id_fields." btn btn-default btn-sm'
												title='Clique pour changer de vote'
											>
											<i class='fa fa-repeat fa-2x text-success'></i>
											</a>";
										}elseif($is_already_participated_general == false){
											$data_length = isset($v["answers"][$id_form][$id_fields][$key]["vote"]) ? 
												count($v["answers"][$id_form][$id_fields][$key]["vote"]) :0;
											echo "<a 
												href='javascript:;' 
												data-id='".(string)$v["_id"]."'
												data-path='answers.".$id_form.".".$id_fields.".".$key.".vote'
												data-collection='".Form::ANSWER_COLLECTION."'
												data-length='".$data_length."'
												data-type='vote'
												class='addchoisir".$id_fields." btn btn-default btn-sm'
												title='Clique pour voter'
											>
											<i class='fa fa-hand-o-up fa-2x text-success'></i>

											</a>";
										}elseif($is_already_participated_general == true){
											echo "<a 
													
													title='Vote bloqué'
												>
												<i class='fa fa-lock fa-2x text-danger'></i>
											    </a>";
										}
									}elseif($is_already_participated_general == false){
										$data_length = isset($v["answers"][$id_form][$id_fields][$key]["vote"]) ? 
												count($v["answers"][$id_form][$id_fields][$key]["vote"]) :0;
											echo "<a 
												href='javascript:;' 
												data-id='".(string)$v["_id"]."'
												data-path='answers.".$id_form.".".$id_fields.".".$key.".vote'
												data-collection='".Form::ANSWER_COLLECTION."'
												data-length='".$data_length."'
												data-type='vote'
												class='addchoisir".$id_fields." btn btn-default btn-sm'
												title='Clique pour voter'
											>
											<i class='fa fa-hand-o-up fa-2x text-success'></i>									
											</a>";

									}elseif($is_already_participated_general == true){
											echo "<a 
													title='Vote bloqué'
												>
												<i class='fa fa-lock fa-2x text-danger'></i>
											    </a>";
									}
								}else{
									echo "<a 
												title='Vote bloqué'
											>
												<i class='fa fa-lock fa-2x text-danger'></i>
											</a>";
								
								}
								?>
								</td>
								<td style="vertical-align:center">
									<?php $random_id = generate_string($permitted_chars, 20) ?>
									<?php echo "<span id=".$random_id."></span>" ?>
									<?php if (isset($v["answers"][$id_form][$id_fields][$key]["vote"])){ 
										$nb_pour = 0;
										$nb_contre = 0;
										$arrayUserPour=[];
										$arrayUserContre=[];
										$no_vote="Aucun vote";
										foreach ($v["answers"][$id_form][$id_fields][$key]["vote"] as $i => $j) {
											if ($j["choix"]=="pour") {
												$nb_pour++;
												array_push($arrayUserPour,Person::getMinimalUserById($j["user"]));
											}elseif($j["choix"]=="contre"){
												$nb_contre++;
												array_push($arrayUserContre,Person::getMinimalUserById($j["user"]));
											}
										}
									?>
									
									
									<?php colorDegradePour($nb_pour,$max_vote_pour,json_encode($arrayUserPour),$random_id); ?>
									<?php contre($nb_contre,json_encode($arrayUserContre)) ?>

									
									<?php 
									if (isset($answer["config"]["showLine"]) && $answer["config"]["showLine"] == "all_line"  ) {
										
									}elseif (isset($answer["config"]["showLine"]) && $answer["config"]["showLine"] == "max_line" && ($nb_pour !== $max_vote_pour)) {
											echo "<script>
												var y = document.getElementById('".$random_id."').parentElement;
												y.parentElement.style.display = 'none';
											</script>";
										
									}elseif (isset($answer["config"]["showLine"]) && $answer["config"]["showLine"] == "my_line" && isset($v["answers"][$id_form][$id_fields][$key]["userId"]) && (string)$me["_id"] != $v["answers"][$id_form][$id_fields][$key]["userId"]) {
										echo "<script>
												var y = document.getElementById('".$random_id."').parentElement;
												y.parentElement.style.display = 'none';
											</script>";
									}elseif (isset($v["answers"][$id_form][$id_fields][$key]["vote"]) && isset($answer["config"]["showLine"]) && $answer["config"]["showLine"] == "no_vote" ) {
										 echo "<script>
												var y = document.getElementById('".$random_id."').parentElement;
												y.parentElement.style.display = 'none';
											</script>";
									} ?>
									<?php 

									}else{
										if (isset($answer["config"]["showLine"]) && $answer["config"]["showLine"] == "max_line" && !isset($v["answers"][$id_form][$id_fields][$key]["vote"])) {
											echo "<script>
												var y = document.getElementById('".$random_id."').parentElement;
												y.parentElement.style.display = 'none';
											</script>";
										
									    }elseif (isset($answer["config"]["showLine"]) && $answer["config"]["showLine"] == "my_line" && isset($v["answers"][$id_form][$id_fields][$key]["userId"]) && (string)$me["_id"] != $v["answers"][$id_form][$id_fields][$key]["userId"]) {
										echo "<script>
												var y = document.getElementById('".$random_id."').parentElement;
												y.parentElement.style.display = 'none';
											</script>";
										}
										$no_vote="Aucun vote";
										echo $no_vote;
									} ?>
									

									

								</td>
							</tr>
							
			<?php 
						
						
					}
				}
			};
           ?>
        </tbody>
      </table>
<?php } ?> 
<!--END PANEL CHOICE <------------------------------->
<script type="text/javascript">
$(document).ready(function(){
	var type_choix_vote = <?php echo json_encode($type_choix_vote)?>;
	
	//BEGIN VOTE------------------------------------------
	sectionDyf.<?php echo "choisir".$id_fields ?> = {
            "jsonSchema" : {
                "title" : "Veuller choisir",
                "icon" : "fa-money",
                "text" : "",
                "properties" : <?php echo json_encode( $vote ); ?>,
                save : function () {
			      bootbox.dialog({
			          title: "Confirmez le vote",
			          message: type_choix_vote=="one" ?
			          "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>" :
			          "<span class='text-success bold'><i class='fa fa-check'></i> Vous avez plusieurs choix</span>" ,
			          buttons: [
			            {
			              label: "Ok",
			              className: "btn btn-primary pull-left",
			              callback: function() {
			       				tplCtx.value = {};
			                    $.each( sectionDyf.<?php echo "choisir".$id_fields ?>.jsonSchema.properties , function(k,val) {
			                        tplCtx.value[k] = $("#"+k).val();
			                    });

			                    mylog.log("save tplCtx",tplCtx);
			                    if(typeof tplCtx.value == "undefined")
			                        toastr.error('value cannot be empty!');
			                    else {
			                        dataHelper.path2Value( tplCtx, function(params) {
			                            $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
			                            if (type_choix_vote== "many") {
			                            	location.reload();
			                            }else if(type_choix_vote == "one"){
			                            	saveLinks(answerObj._id.$id,"already_voted",userId,function(){
			                            	 	location.reload();
			                            	});
			                            }
			                           
			                        } );
			                    }
			              }
			            },
			            {
			              label: "Annuler",
			              className: "btn btn-default pull-left",
			              callback: function() {}
			            }
			          ]
			      });
                }
            }
        };


        mylog.log("render","/modules/costum/views/tpls/forms/<?php echo "choisir".$id_fields ?>.php");
        $(".add<?php echo "choisir".$id_fields ?>").off().on("click",function() {
        	var dataLength = $(this).data("length");
        	var type_vote = $(this).data("type");
        	var random = Math.floor(Math.random() * 100) + 1;
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path =null;

            if (type_vote=="changer_vote") {
            	tplCtx.path = $(this).data("path");
            }
            if (type_vote=="vote") {
            	tplCtx.path = $(this).data("path")+'.'+((notNull(dataLength) ? (dataLength+random).toString() : "0"+random));
            }
            
            dyFObj.openForm( sectionDyf.<?php echo "choisir".$id_fields ?> );
        });
	//END VOTE -------------------------------------------
	
	$('.pour').off().click( function(){
	var data = $(this).data("data");
	var message ="";
	data.forEach(function(item,i){
		message+=`<span class='text-dark'> <i class='fa fa-user'></i> ${item.name}</span><br>`;
	});
      bootbox.dialog({
          title: `<span class='text-success'>Pour: </span> ${data.length} Votant(s)`,
          message: message,
          buttons: [
            {
              label: "Fermer",
              className: "btn btn-default pull-right",
              callback: function() {}
            }
          ]
      });
    });

$('.contre').off().click( function(){
	var data = $(this).data("data");
	var message ="";
	data.forEach(function(item,i){
		message+=`<span class='text-dark'> <i class='fa fa-user'></i> ${item.name}</span><br>`;
	});
      bootbox.dialog({
          title: `<span class='text-danger'>Contre: </span> ${data.length} Votant(s)`,
          message: message,
          buttons: [
            {
              label: "Fermer",
              className: "btn btn-default pull-right",
              callback: function() {}
            }
          ]
      });
    });
$('.contre').off().click( function(){
	var data = $(this).data("data");
	var message ="";
	data.forEach(function(item,i){
		message+=`<span class='text-dark'> <i class='fa fa-user'></i> ${item.name}</span><br>`;
	});
      bootbox.dialog({
          title: `<span class='text-danger'>Contre: </span> ${data.length} Votant(s)`,
          message: message,
          buttons: [
            {
              label: "Fermer",
              className: "btn btn-default pull-right",
              callback: function() {}
            }
          ]
      });
    });

$('.img-responsive').off().click( function(){
	var data = $(this).data("docpath");
	console.log(data);
      bootbox.dialog({
          title: `Aperçu`,
          message: `<img 
                        src=${data}
          	            class='img-responsive' 
          	            style='width: 100%;'
                    >`,
          buttons: [
            {
              label: "Fermer",
              className: "btn btn-default pull-right",
              callback: function() {}
            }
          ]
      });
    });

$('#show_line').change(function(){
	if ($(this).val() != "") {
		var showLine = $(this).val();
		var tplCtx={};
		tplCtx.id = "<?php echo (string)$answer["_id"] ?>";
	   tplCtx.collection = "<?php echo FORM::ANSWER_COLLECTION ?>";
	    tplCtx.path ="config";
	    tplCtx.value = {
	    	showLine : showLine
	    };
	    console.log(tplCtx.value);
	     dataHelper.path2Value( tplCtx, function(params) {
	       location.reload();
	    });
	}
});

})

</script>