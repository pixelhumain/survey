
<?php if($answer){

  $me = isset(Yii::app()->session['userId']) ? Person::getById(Yii::app()->session['userId']) : null;


//VERIFY_IF_MEMBER_OF_COMMUNITY---------------------------------------
 $is_member_of_community = false;
 $id_community = isset(array_keys($parentForm["parent"])["0"]) ? array_keys($parentForm["parent"])["0"] :null;
 
 if(isset($me["links"]["memberOf"]) ){
    foreach ($me["links"]["memberOf"] as $key => $value) {
      if($key == $id_community ){
          $is_member_of_community = true;
       }
    }  
 }
 //echo $is_member_of_community ? "je suis membre" : "Je ne suis pas membre";
//END VERIFY MEMBER OF COMMUNITY---------------------------------------


//VERIFY_IF_HAVE_ROLES_IN_COMMUNITY---------------------------------------
$is_have_roles_required = false;

$config_roles = isset($parentForm["params"][$kunik]["configuration"]["roles"]) ? $parentForm["params"][$kunik]["configuration"]["roles"] : null;
$roles= [];


if (isset($me["links"]["memberOf"][array_keys($parentForm["parent"])["0"]]["roles"]) && $config_roles!=null) {
  $roles =  $me["links"]["memberOf"][array_keys($parentForm["parent"])["0"]]["roles"];
  foreach ($roles as $key => $value) {
    if (in_array($value, $config_roles)) {
            $is_have_roles_required = true;
     }
  }
}
//var_dump($roles);
// var_dump($roles);
 //var_dump($is_have_roles_required);
//END VERIFY_IF_HAVE_ROLES_IN_COMMUNITY---------------------------------------

//AUTHORIZATION VARIABLE--------------------------------------------------------------
  $authorization = "";
  $public_login_not_required = "";
  $public_login_required = "";
  $private_to_a_community = "";
  $private_to_the_role_of_a_community = "";

  if (isset($parentForm["params"][$kunik]["configuration"]["droit_acces"])) {
    $authorization = $parentForm["params"][$kunik]["configuration"]["droit_acces"];
    $public_login_not_required = (!empty($authorization) && $authorization == "public_login_not_required")? true : false ;
    $public_login_required = (!empty($authorization) && $authorization == "public_login_required")? true : false ;
    $private_to_a_community = (!empty($authorization) && $authorization == "private_to_a_community")? true : false;
    $private_to_the_role_of_a_community = (!empty($authorization) && $authorization == "private_to_the_role_of_a_community")? true : false;
  }
  // var_dump($private_to_the_role_of_a_community);
  // var_dump($is_have_roles_required);
//END AUTHORIZATION VARIABLE-----------------------------------------------------------

 

//PARAMETER VARIABLE-------------------------------------------------------------------
  $config_param =null;
  $type_choix_vote="";
  $nom_element =""; 
  $type_proposition ="";
  $date_demarage_proposition ="";
  $duree_proposition ="";
  $duree_vote ="";
  $droit_acces ="";
  if (isset($parentForm["params"][$kunik]["configuration"])) {
    $config_param      = $parentForm["params"][$kunik]["configuration"];
    $type_choix_vote       = !empty($config_param["type_choix_vote"]) ? $config_param["type_choix_vote"] : "";
    $nom_element       = !empty($config_param["nom_element"]) ? $config_param["nom_element"] : "";
    $type_proposition  = !empty($config_param["type_proposition"]) ? $config_param["type_proposition"] : "";
    $date_demarage_proposition     = !empty($config_param["date_demarage"]) ? $config_param["date_demarage"] : "";
    $duree_proposition = !empty($config_param["duree_proposition"]) ? $config_param["duree_proposition"] : "";
    $duree_vote        = !empty($config_param["duree_vote"]) ? $config_param["duree_vote"] : "";
    $droit_acces       = !empty($config_param["droit_acces"]) ? $config_param["droit_acces"] : "";
  }
//END PARAMETER VARIABLE----------------------------------------------------------------

//BEGIN VERIFY PROPOSITION BEGIN AND EXPIRATION DATE---------------------------------------
  $today = strtotime(date("d-m-Y"));
  $date_demarage_proposition2 =""; 
  $proposition_expire_date ="";
  $is_authorize_proposition_date =false;
  $proposition_date_place="";
  if (!empty($date_demarage_proposition) && !empty($duree_proposition)) {
      $date_demarage_proposition2 = date("d-m-Y", strtotime(str_replace('/', '-', $date_demarage_proposition)));
      $proposition_expire_date =  date('d-m-Y', strtotime($date_demarage_proposition2. ' + '.($duree_proposition-1).' days'));
      if ($today >= strtotime($date_demarage_proposition2) && $today <= strtotime($proposition_expire_date) )  {
        $is_authorize_proposition_date = true;
        $proposition_date_place="in";
      }elseif ($today < strtotime($date_demarage_proposition2)) {
        $is_authorize_proposition_date = false;
        $proposition_date_place="before";
      }elseif ($today > strtotime($proposition_expire_date)) {
        $is_authorize_proposition_date = false;
        $proposition_date_place="after";
      }
  }

  // echo "<br> Démarage: ".$date_demarage_proposition2;
  // echo "<br> Expiration date: ".$proposition_expire_date;
  // echo "<br> Aujourd'hui: ".date("d-m-Y");
  //var_dump($is_authorize_proposition_date);  
  //var_dump($is_member_of_community)    ;
  //var_dump($private_to_a_community);
//BEGIN VERIFY PROPOSITION BEGIN AND EXPIRATION DATE---------------------------------------

//BEGIN GLOBAL AUTHORIZATION-----------------------------
  $allow=false;
  if($public_login_not_required && $is_authorize_proposition_date){
    $allow =true;
  }elseif ($public_login_required && $is_authorize_proposition_date) {
    $allow =true;
  }elseif ($private_to_a_community && $is_member_of_community && $is_authorize_proposition_date) {
    $allow =true;
  }elseif($private_to_the_role_of_a_community && $is_member_of_community && $is_have_roles_required && $is_authorize_proposition_date) {
   $allow =true;
  }else{
    $allow =false;
  }

//END GLOBAL AUTHORIZATION-------------------------------


//PROPOSITION PROPERTIES------------------------------------------------------------
  $proposition=null;
  $is_basic_type = $type_proposition =="text" || $type_proposition =="date";
  $basic_type =null;
  switch ($type_proposition) {
    case 'text':
      $basic_type ="text";
      break;
    case 'date':
      $basic_type ="date";
      break;
    case 'location':
      $basic_type ="location";
      break;
    default:
      $basic_type ="text";
      break;
  }
//var_dump($basic_type);

  if ($public_login_not_required) {///-----------
    if ($is_basic_type) {
        $proposition = [
          "user" => [
              "label" => "Nom d'utilisateur",
              "placeholder" => "",
              "inputType" => $basic_type,
              "rules" => [ "required" => true ]
          ],
          "proposition" => [
              "label" => "Entrez votre proposition",
              "placeholder" => "",
              "inputType" => $basic_type,
              "rules" => [ "required" => true ]
          ]
        ];
    }elseif($type_proposition=="image"){
        $proposition = [
          "user" => [
              "label" => "Nom d'utilisateur",
              "placeholder" => "",
              "inputType" => $basic_type,
              "rules" => [ "required" => true ]
          ],
         "file" => [
          "label" => "logo",
          "placeholder" => "logo",
          "inputType" => "uploader",
          "rules" => [ "required" => false ],
          "docType" => "file"
      ]
        ];
    }elseif($type_proposition=="adresse"){
        $proposition = [
          "user" => [
              "label" => "Nom d'utilisateur",
              "placeholder" => "",
              "inputType" => $basic_type,
              "rules" => [ "required" => true ]
          ],
          "coord" => [                
              "label" => "Coordonnée",
              "placeholder" => "Coordonnée",
              "inputType" => "formLocality",
              "rules" => [ "required" => true ]
          ],
           "location" => [
              "inputType" => "location",
              "rules" => [ "required" => true ]
          ]
        ];
    }

  }elseif($public_login_required){//----------------
    if ($is_basic_type) {
        $proposition = [
        "user" => [
            "inputType" => "hidden",
            "value" => isset($me["name"]) ? $me["name"] :null,
            "rules" => [ "required" => true]
        ],
         "userId" => [
            "inputType" => "hidden",
            "value" => isset($me["_id"]->{'$id'}) ? $me["_id"]->{'$id'}:null,
            "rules" => [ "required" => true ]
        ],
          "userSlug" => [
            "inputType" => "hidden",
            "value" => isset($me["slug"]) ? $me["slug"] :null,
            "rules" => [ "required" => true]
        ],
        "proposition" => [
            "label" => "Entrez votre proposition",
            "placeholder" => "",
            "inputType" => $basic_type,
            "rules" => [ "required" => true ]
        ]
      ];
    }elseif($type_proposition=="image"){
            $proposition = [
              "user" => [
                  "inputType" => "hidden",
                  "value" => isset($me["name"]) ? $me["name"] :null,
                  "rules" => [ "required" => true]
              ],
               "userId" => [
                  "inputType" => "hidden",
                  "value" => isset($me["_id"]->{'$id'}) ? $me["_id"]->{'$id'}:null,
                  "rules" => [ "required" => true ]
              ],
                "userSlug" => [
                  "inputType" => "hidden",
                  "value" => isset($me["slug"]) ? $me["slug"] :null,
                  "rules" => [ "required" => true]
              ],
              "file" => [
                "label" => "logo",
                "placeholder" => "logo",
                "inputType" => "uploader",
                "rules" => [ "required" => false ],
                "docType" => "file"
            ]
          ];
    }elseif($type_proposition=="adresse"){
            $proposition = [
              "user" => [
                  "inputType" => "hidden",
                  "value" => isset($me["name"]) ? $me["name"] :null,
                  "rules" => [ "required" => true]
              ],
               "userId" => [
                  "inputType" => "hidden",
                  "value" => isset($me["_id"]->{'$id'}) ? $me["_id"]->{'$id'}:null,
                  "rules" => [ "required" => true ]
              ],
                "userSlug" => [
                  "inputType" => "hidden",
                  "value" => isset($me["slug"]) ? $me["slug"] :null,
                  "rules" => [ "required" => true]
              ],
              "coord" => [                
                "label" => "Coordonnée",
                "placeholder" => "Coordonnée",
                "inputType" => "formLocality",
                "rules" => [ "required" => true ]
              ],
               "location" => [
                  "inputType" => "location",
                  "rules" => [ "required" => true ]
              ]
          ];
    }
  }elseif($private_to_a_community && $is_member_of_community){//-----
    if ($is_basic_type) {
        $proposition = [
          "user" => [
              "inputType" => $basic_type,
              "value" => isset($me["name"]) ? $me["name"] :"",
              "rules" => [ "required" => true]
          ],
           "userId" => [
              "inputType" => $basic_type,
              "value" => isset($me["_id"]->{'$id'}) ? $me["_id"]->{'$id'}:"",
              "rules" => [ "required" => true ]
          ],
          "userSlug" => [
              "inputType" => "hidden",
              "value" => isset($me["slug"]) ? $me["slug"] :"",
              "rules" => [ "required" => true]
          ],
            "communityId" => [
            "inputType" => $basic_type,
            "value" =>  $id_community,
            "rules" => [ "required" => true ]
          ],
          "proposition" => [
              "label" => "Entrez votre proposition",
              "placeholder" => "",
              "inputType" => $basic_type,
              "rules" => [ "required" => true ]
          ]
      ];
    }elseif ($type_proposition=="image") {
              $proposition = [
                "user" => [
                    "inputType" => $basic_type,
                    "value" => isset($me["name"]) ? $me["name"] :"",
                    "rules" => [ "required" => true]
                ],
                 "userId" => [
                    "inputType" => $basic_type,
                    "value" => isset($me["_id"]->{'$id'}) ? $me["_id"]->{'$id'}:"",
                    "rules" => [ "required" => true ]
                ],
                "userSlug" => [
                    "inputType" => "hidden",
                    "value" => isset($me["slug"]) ? $me["slug"] :"",
                    "rules" => [ "required" => true]
                ],
                  "communityId" => [
                  "inputType" => $basic_type,
                  "value" => $id_community,
                  "rules" => [ "required" => true ]
                ],
                  "file" => [                       
                    "label" => "logo",
                    "placeholder" => "logo",
                    "inputType" => "uploader",
                    "rules" => [ "required" => false ],
                    "docType" => "file"
                ]
            ];
    }elseif($type_proposition=="adresse"){
              $proposition = [
                "user" => [
                    "inputType" => $basic_type,
                    "value" => isset($me["name"]) ? $me["name"] :"",
                    "rules" => [ "required" => true]
                ],
                 "userId" => [
                    "inputType" => $basic_type,
                    "value" => isset($me["_id"]->{'$id'}) ? $me["_id"]->{'$id'}:"",
                    "rules" => [ "required" => true ]
                ],
                "userSlug" => [
                    "inputType" => "hidden",
                    "value" => isset($me["slug"]) ? $me["slug"] :"",
                    "rules" => [ "required" => true]
                ],
                  "communityId" => [
                    "inputType" => $basic_type,
                    "value" => $id_community,
                    "rules" => [ "required" => true ]
                ],
                "coord" => [                
                  "label" => "Coordonnée",
                  "placeholder" => "Coordonnée",
                  "inputType" => "formLocality",
                  "rules" => [ "required" => true ]
              ],
               "location" => [
                  "inputType" => "location",
                  "rules" => [ "required" => true ]
              ]      
            ];
    }
  }elseif($private_to_the_role_of_a_community && $is_member_of_community && $is_have_roles_required){//-----
    if ($is_basic_type) {
        $proposition = [
          "user" => [
              "inputType" => $basic_type,
              "value" => isset($me["name"]) ? $me["name"] :"",
              "rules" => [ "required" => true]
          ],
           "userId" => [
              "inputType" => $basic_type,
              "value" => isset($me["_id"]->{'$id'}) ? $me["_id"]->{'$id'}:"",
              "rules" => [ "required" => true ]
          ],
          "userSlug" => [
              "inputType" => "hidden",
              "value" => isset($me["slug"]) ? $me["slug"] :"",
              "rules" => [ "required" => true]
          ],
            "communityId" => [
            "inputType" => $basic_type,
            "value" =>  $id_community,
            "rules" => [ "required" => true ]
          ],
          "roles" => [
            "inputType" => $basic_type,
            "value" =>  $roles,
            "rules" => [ "required" => true ]
          ],
          "proposition" => [
              "label" => "Entrez votre proposition",
              "placeholder" => "",
              "inputType" => $basic_type,
              "rules" => [ "required" => true ]
          ]
      ];
    }elseif ($type_proposition=="image") {
          $proposition = [
                "user" => [
                    "inputType" => $basic_type,
                    "value" => isset($me["name"]) ? $me["name"] :"",
                    "rules" => [ "required" => true]
                ],
                 "userId" => [
                    "inputType" => $basic_type,
                    "value" => isset($me["_id"]->{'$id'}) ? $me["_id"]->{'$id'}:"",
                    "rules" => [ "required" => true ]
                ],
                "userSlug" => [
                    "inputType" => "hidden",
                    "value" => isset($me["slug"]) ? $me["slug"] :"",
                    "rules" => [ "required" => true]
                ],
                  "communityId" => [
                  "inputType" => $basic_type,
                  "value" => $id_community,
                  "rules" => [ "required" => true ]
                ],
                "roles" => [
                    "inputType" => $basic_type,
                    "value" =>  $roles,
                    "rules" => [ "required" => true ]
                  ],
                  "file" => [
                    "label" => "logo",
                    "placeholder" => "logo",
                    "inputType" => "uploader",
                    "rules" => [ "required" => false ],
                    "docType" => "file"
                ]
            ];
    }elseif ($type_proposition=="adresse"){
          $proposition = [
                "user" => [
                    "inputType" => $basic_type,
                    "value" => isset($me["name"]) ? $me["name"] :"",
                    "rules" => [ "required" => true]
                ],
                 "userId" => [
                    "inputType" => $basic_type,
                    "value" => isset($me["_id"]->{'$id'}) ? $me["_id"]->{'$id'}:"",
                    "rules" => [ "required" => true ]
                ],
                "userSlug" => [
                    "inputType" => "hidden",
                    "value" => isset($me["slug"]) ? $me["slug"] :"",
                    "rules" => [ "required" => true]
                ],
                  "communityId" => [
                  "inputType" => $basic_type,
                  "value" => $id_community,
                  "rules" => [ "required" => true ]
                ],
                "roles" => [
                    "inputType" => $basic_type,
                    "value" =>  $roles,
                    "rules" => [ "required" => true ]
                ],
                  "coord" => [                
                    "label" => "Coordonnée",
                    "placeholder" => "Coordonnée",
                    "inputType" => "formLocality",
                    "rules" => [ "required" => true ]
                ],
                  "location" => [
                    "inputType" => "location",
                    "rules" => [ "required" => true ]
                ] 
            ];
    }
  }
//END PROPOSITION PROPERTIES------------------------------------------------------------         
    ?>

    <div class="form-group">
            <?php
            $editBtnL="";
            $addParameterBtn="";
            $btnAddProposition="";
  
              $btnAddProposition = (Yii::app()->session["userId"] == $answer["user"]) ? 
                " <a href='javascript:;' 
                    data-id='".$answer["_id"]."' 
                    data-collection='".Form::ANSWER_COLLECTION."' 
                    data-path='".$answerPath."' 
                    class='add".$kunik."proposition btn btn-default btn-sm'>
                    <i class='fa fa-plus'></i> 
                    Ma proposition 
                 </a>"
                 :
                  "";

              $addParameterBtn = (Yii::app()->session["userId"] == $answer["user"]) ? 
                " <a href='javascript:;' 
                  data-id='".$parentForm["_id"]."' 
                  data-collection='".Form::COLLECTION."' 
                  data-path='params.".$kunik.".configuration' 
                  class='add".$kunik."editParameterBtn btn btn-default btn-sm'>
                  <i class='fa fa-cog fa-spin'></i> 
                  Configuration 
                 </a>"
                 :
                  "";

}

            $editParameterBtn = (Yii::app()->session["userId"] == $parentForm["creator"] || $is_have_roles_required) ?
              "<a href='javascript:;' 
                   data-id='".$el["_id"]."' 
                   data-collection='".$this->costum["contextType"]."' 
                   data-path='costum.form.params.".$kunik."' 
                   class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'>
                   <i class='fa fa-cog'></i> 
              </a>" 
            : 
              null;

          
            $parameter = "";
            if ($canEdit || Yii::app()->session["userId"] == $parentForm["creator"]) {
                $parameter =[
                "type_choix_vote"=>[
                  "label"=>"un choix ou plusieurs choix pour chaque votant",
                  "placeholder"=> "un choix ou plusieurs choix",
                  "inputType"=>"select",
                  "options" => [
                      "one" => "un choix pour un votant",
                      "many" => "plusieurs choix pour un votant",
                  ],
                  "rules"=>["required" => true]
                ],   
                "nom_element"=>[
                  "label"=>"Nom de l'élément à proposé",
                  "placeholder"=> "nom de l’elément à proposer",
                  "inputType"=>"text",
                  "rules"=>["required" => true]
                ],
                "type_proposition"=>[
                  "label"=>"type de la proposition",
                  "placeholder"=> "type de la proposition",
                  "inputType"=>"select",
                  "options" => [
                      "text" => "text",
                      "image" => "image",
                      "date" => "date",
                      "adresse" => "adresse"
                  ],
                  "rules"=>["required" => true]
                ],
                "date_demarage"=>[
                  "label"=>"date de démarrage",
                  "placeholder"=> "dd/mm/yy",
                  "inputType"=>"date",
                  "rules"=>["required" => true]
                ],
                "duree_proposition"=>[
                  "label"=>"Durée de proposition",
                  "placeholder"=> "",
                  "inputType"=>"text",
                  "rules"=>["required" => true]
                ],
                "duree_vote"=>[
                  "label"=>"Durée de vote",
                  "placeholder"=> "durée de vote",
                  "inputType"=>"text",
                  "rules"=>["required" => true]
                ],
                "droit_acces"=>[
                  "label"=>"Droit d'accès",
                  "placeholder"=> "Choisir un droit d'accès",
                  "inputType"=>"select",
                  "options" => [
                      "public_login_required" => "public identification obligatoire",
                      "public_login_not_required" => "public identification non obligatoire",
                      "private_to_a_community" => "privé à une communauté",
                      "private_to_the_role_of_a_community" => "privé au role d’une communauté"

                  ],
                  "rules"=>["required" => true]
                ],
                "roles"=>[
                  "label"=>" Sélectionner un ou plusieurs rôle(s) si vous choississez 'privé au role d’une communauté' aux dessus",
                  "placeholder"=> "Sélectionner un ou plusieurs rôle(s)",
                  "inputType"=>"selectMultiple",
                  "options" => [
                      "Financeur" => "Financeur",
                      "Partenaire" => "Partenaire",
                      "Sponsor" => "Sponsor",
                      "Organisateur" => "Organisateur",
                      "Président" => "Président",
                      "Directeur" => "Directeur",
                      "Conférencier" => "Conférencier",
                      "Intervenant" => "Intervenant"
                  ],
                  "rules"=>["required" => false]
                ],
              ];
            }
            

            ?>
<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
  <?php echo $label.$editQuestionBtn.$editParameterBtn.$editBtnL?>
</h4>

</div>

<h3><?php echo $nom_element ?></h3>
<!--START PANEL CONFIGURATION ------------------------------->
<script>
  $(document).ready(function(){
    $('.edit<?php echo $kunik ?>editParameter').append(' Modifier les parametres');
    
  })
</script>
<?php 
if ($config_param && count($config_param) != 0 ) {
 ?>
    La proposition commence le <strong><?php echo $date_demarage_proposition ?></strong> et se termine le <strong><?php echo $proposition_expire_date ?> ( <?php echo $duree_proposition ?> jours ).</strong> <br>
    Le vote commence une fois la proposition terminée!<br>

    <?php echo $proposition_date_place=="before" ? 
    "<span class='text-danger'>La proposition commence le : ".$date_demarage_proposition2."</span><br>" : null ?>

    <?php echo $proposition_date_place=="after" ? 
    "<span class='text-danger'>La proposition est términé le : ".$proposition_expire_date."</span><br>" : null ?>

    <?php echo $proposition_date_place=="in" ? 
    "<span class='text-success'>La proposition est en cours . . .</span><br>" : null ?>

    <?php echo $public_login_not_required && $is_authorize_proposition_date ? 
    "<span class='text-success'>Cette proposition est publique</span><br>" : null ?>

    <?php echo $public_login_required && $is_authorize_proposition_date ? 
    "<span class='text-success'>Cette proposition est reservé au utilisateur connécté</span><br>" : null ?>

    <?php echo $private_to_a_community && $is_member_of_community  &&  $is_authorize_proposition_date ? 
    "<span class='text-success'>Cette proposition est reservé à un communauté</span><br>" : null ?>

    <?php echo $private_to_the_role_of_a_community && $is_member_of_community && $is_have_roles_required && $is_authorize_proposition_date ? 
    "<span class='text-success'>Cette proposition est reservé à un ou des rôles d'un communauté</span><br>" : null ?>
    <?php echo $allow ?  
    "<span class='text-success'>Vous pouvez proposer</span><br>" : "<span class='text-danger'>Vous ne pouvez pas proposer</span><br>" ?>

<?php }else{?>
  <h6>Aucun paramètre disponible <?php echo Yii::app()->session["userId"] == $parentForm["creator"]? $addParameterBtn : null?></h6>
<?php } ?>
<?php if ((Yii::app()->session["userId"] == $parentForm["creator"] || $is_have_roles_required) && isset($config_param) && count($config_param)!=0) {
  echo $this->renderPartial("survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
                         "canEdit"=>($canEdit||Yii::app()->session["userId"] == $parentForm["creator"]),
                         "id" => $parentForm["_id"],
                         "collection" => Form::COLLECTION,
                         "q" => "configuration",
                         "path" => "params.".$kunik.".configuration",
                         "keyTpl"=>$kunik."editParameter"
                        ] ); 
}
?>
<!--END PANEL CONFIGURATION ------------------------------->

<!-- FUNCTION AFFICHAGE IMAGE----------------------------- --> 
<?php 

/*var_dump($answer);
var_dump($answers);
var_dump($answerPath);*/

    function getImageProposer($myanswer,$myanswers,$myanswerPath,$key){
      $initAnswerFiles=Document::getListDocumentsWhere(array(
          "id"=>(string)$myanswer["_id"], 
          "type"=>'answers',
          "subKey"=>$myanswerPath.$key), "file");


          if(!empty($initAnswerFiles)){
              $myanswers[$key]["files"] = $initAnswerFiles;
              foreach ($initAnswerFiles as $key => $d) {
                  
                      echo "<div style='width: 100%;' class='padding-5 margin-top-5 margin-bottom-5' id='".$key."'>
                         <img 
                           src='".@$d["docPath"]."' 
                           data-docpath='".@$d["docPath"]."' 
                           class='img-responsive' 
                           style='width:60px;height:60px;border-radius:80%'
                         >
                      </div>";
                  
                   }
          }else {
          
          echo "<div style='width: 100%;' class='col-xs-12 col-sm-4 padding-5 shadow2 margin-top-5 margin-bottom-5' >
              <img 
                style='width:60px;height:60px'
                src='".Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl()."/images/thumbnail-default.jpg' 
                data-docpath='".Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl()."/images/thumbnail-default.jpg'
                class='img-responsive'>
                style='border-radius:80%'
          </div>";
          }
      }  


//getImageProposer($answer,$answers,$answerPath,"10");
?>
<!-- FUNCTION AFFICAHGE IMAGE----------------------------- -->



<!--START PANEL PROPOSITION ------------------------------->
 <?php if($allow) {?>
<div class="panel panel-success" style="border: 1px solid #93C01F;margin-top: 5px">
  <div class="panel-heading" style="background-color: #93C01F">
     <?php if ($is_authorize_proposition_date && count($config_param) != 0) {?>
    <h3 class="panel-title"><span style="color: white">Votre proposition </span>
      <span class="pull-right" style="margin-top: -7px">
        <?php 
          if ($allow) {
                echo $btnAddProposition;
          }
       ?>
      </span>
    </h3>
     <?php }else{?>
    <h3 class="panel-title">
      <span style="color: white">
        <?php echo $proposition_date_place=="before" ? "La proposition commence le : ".$date_demarage_proposition2 : null ?>
        <?php echo $proposition_date_place=="after" ? "La proposition est términé le : ".$proposition_expire_date : null ?>
      </span>
    </h3> 
     <?php } ?>

  </div>
 
  <div class="panel-body">
     <table class="table table-borderded" style="border-top-color: #93C01F">
        <thead>
          <tr>
            <td><strong>Utilisateur</strong></td>
            <td>
              <strong>
                <?php 
                  echo isset($parentForm["params"][$kunik]["configuration"]["nom_element"]) ? 
                        $parentForm["params"][$kunik]["configuration"]["nom_element"] :
                        "Proposition";
                 ?>
              </strong>
          </td>
            <td><strong>Action</strong></td>
          </tr>
        </thead>
        <tbody>
          <?php if(isset($answers) && count($config_param) != 0 ){ 
              foreach ($answers as $q => $a) {
          ?>
              <tr>
                <td><?php echo @$a["user"] ?></td>
                <td>
                    <?php 
                      if ($type_proposition =="image"){
                        getImageProposer($answer,$answers,$answerPath,$q);
                      }
                      if(isset($a["proposition"]) && !empty($a["proposition"]) && $is_basic_type){
                          echo $a["proposition"];
                      }
                      if($type_proposition == "adresse"){
                        echo @$a["address"]["streetAddress"]."/".@$a["address"]["addressLocality"]."/".@$a["address"]["level1Name"]."<br>lat/long: ".@$a["coord"]["latitude"]."/".$a["coord"]["longitude"];
                      }
                    ?>
                </td>
                <td>
                    <?php if ($allow) {
                      echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
                         "canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
                         "id" => $answer["_id"],
                         "collection" => Form::ANSWER_COLLECTION,
                         "q" => $q,
                         "path" => "answers.".$kunik.".".$q,
                         "keyTpl"=>$kunik
                     ] );
                    }
                     
                    ?>
                     <a 
                       href="javascript:;" 
                       class="btn btn-xs btn-primary openAnswersComment" 
                       onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>',
                       '<?php echo $answer["_id"].$key.$q ?>', 
                       '<?php echo @$a['step'] ?>')">
                       <?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> 
                       <i class='fa fa-commenting'></i>
                   </a>
                </td>
              </tr>
          <?php 
            }
          }
        ?>
         
        </tbody>
      </table>
  </div>
</div>
  <?php }?>
<!--END PANEL PROPOSITION ------------------------------->

<script type="text/javascript">
        var <?php echo $kunik ?>DataParameter = <?php echo json_encode( (isset($parentForm["params"][$kunik])) ? $parentForm["params"][$kunik] : null ); ?>;
        var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
        
        sectionDyf.<?php echo $kunik ?>ParameterData = <?php echo json_encode( $parameter ); ?>;

        $(document).ready(function() {
//BEGIN ADD PROPOSITION------------------------------------------------------------
            sectionDyf.<?php echo $kunik ?>proposition = {
                "jsonSchema" : {
                    "title" : "Proposition",
                    "icon" : "fa-money",
                    "text" : "",
                    "properties" : <?php echo json_encode( $proposition ); ?>,
                    beforeBuild : function(){
                       uploadObj.set("answers",answerObj._id.$id, "file", null, null, "/subKey/"+tplCtx.path);
                    },
                    save : function () {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>proposition.jsonSchema.properties , function(k,val) {
                            if(val.inputType == "properties"){
                              tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                              console.log(tplCtx.value[k]);
                            }
                            else if(val.inputType == "array"){
                              tplCtx.value[k] = getArray('.'+k+val.inputType);
                              console.log(tplCtx.value[k]);
                            }
                            else if(val.inputType == "formLocality"){
                              tplCtx.value[k] = getArray('.'+k+val.inputType);
                              console.log(tplCtx.value[k]);
                            }
                            else{
                              tplCtx.value[k] = $("#"+k).val();
                              console.log(tplCtx.value[k]);
                            }
                                
                        });
                        if(typeof formData != "undefined" && typeof formData.geo != "undefined"){
                            tplCtx.value["coord"] = formData.geo;
                            tplCtx.value["address"] = formData.address;
                            tplCtx.value["geo"] = formData.geo;
                            tplCtx.value["geoPosition"] = formData.geoPosition;

                            if(typeof formData.addresses != "undefined")
                                tplCtx.value["addresses"] = formData.addresses;
                        }
                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                          console.log(tplCtx);
                            dataHelper.path2Value( tplCtx, function(params) {


                               dyFObj.commonAfterSave(null, function(){
                                   
                                    if(dyFObj.closeForm()){
                                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                                        location.reload();
                                    }else{
                                        location.reload();
                                    }
                                });

                                /*$("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                                //saveLinks(answerObj._id.$id,"pour",userId);
                                location.reload();*/
                            } );
                        }
                    }
                }
            };


            mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");
            $(".add<?php echo $kunik ?>proposition").off().on("click",function() {
             tplCtx.id = $(this).data("id");
             tplCtx.collection = $(this).data("collection");
             tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
             dyFObj.openForm( sectionDyf.<?php echo $kunik ?>proposition );
            });
            $(".edit<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>proposition,null, <?php echo $kunik ?>Data[$(this).data("key")]);
            });
//END ADD PROPOSITION--------------------------------------------------------------


//BEGIN ADD PARAMETER------------------------------------------------------------------------------
            sectionDyf.<?php echo $kunik ?>parameter = {
                "jsonSchema" : {
                    "title" : "Configuration",
                    "icon" : "fa-money",
                    "text" : "",
                    "properties" : <?php echo json_encode( $parameter ); ?>,
                    save : function () {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>parameter.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                            console.log(tplCtx);
                        });

                       mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");

                                location.reload();
                            } );
                        }
                    }
                }
            };
              $(".add<?php echo $kunik ?>editParameterBtn").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>parameter );
            });

              $(".edit<?php echo $kunik ?>editParameter").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>parameter,null,<?php echo $kunik ?>DataParameter[$(this).data("key")]);
            });

//END PARAMETER------------------------------------------------------------------------------

//SHOW MODAL IMAGE----------------------------
$('.img-responsive').off().click( function(){
  var data = $(this).data("docpath");
  console.log(data);
      bootbox.dialog({
          title: `Aperçu`,
          message: `<img 
                        src=${data}
                        class='img-responsive' 
                        style='width: 100%;'
                    >`,
          buttons: [
            {
              label: "Fermer",
              className: "btn btn-default pull-right",
              callback: function() {}
            }
          ]
      });
    });
    });
    </script>
