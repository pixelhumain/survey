<?php 
$value = (!empty($answers)) ? " value='".$answers."' " : "";

$inpClass = "form-control";

if($type == "tags") 
	$inpClass = "select2TagsInput";

if($saveOneByOne)
	$inpClass .= " saveOneByOne";

 $formId =PHDB::find( Form::COLLECTION,  array( "parent.".$this->costum["contextId"] => array('$exists'=>1) ) );

reset($formId);
$first_key = key($formId);

$allAnswers = PHDB::find( Form::ANSWER_COLLECTION, [ "form" => $first_key, "answers" => array('$exists'=>1) ] );
$allId = [];
foreach ($allAnswers as $k => $v) {
	if (isset($v["answers"]["deal1"]["deal111"])) {
		array_push($allId, $v["answers"]["deal1"]["deal111"]);
	}
}

if($mode == "r" || $mode == "pdf"){ ?>
    <div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
        <label for="<?php echo $kunik ?>"><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label ?></h4></label><br/>
        <?php echo $answers; ?>
    </div>
<?php 
}else{
?>
	<div class="form-group">
	    <label for="<?php echo $key ?>"><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn ?></h4></label>
	    <br/><input type="<?php echo (!empty($type)) ? $type : 'text' ?>" class="<?php echo $inpClass ?>" id="<?php echo $key ?>" aria-describedby="<?php echo $key ?>Help" placeholder="<?php echo (isset($placeholder)) ? $placeholder : '' ?>" data-form='<?php echo $form["id"] ?>'  <?php echo $value ?> >
	    <?php if(!empty($info)){ ?>
	    	<small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
	    <?php } ?>
	</div>
<?php } ?>

<?php if($mode != "pdf"){ ?>
<script type="text/javascript">

	var users = [];


	jQuery(document).ready(function() {
	    mylog.log("render form input","/modules/costum/views/tpls/forms/text.php");

	    if($("#<?php echo $key ?>").val() == "" ){
	    if (typeof userConnected.telephone != 'undefined') {	
		    if (typeof userConnected.telephone.fixe != 'undefined') {
		    	

			    $("#<?php echo $key ?>").val(userConnected.telephone.fixe);

			    answer = new Object;
			    answer.path = "answers."+$("#<?php echo $key ?>").data("form")+"."+"<?php echo $key ?>";
		    	answer.collection = "answers" ;
		    	answer.id = "<?php echo $answer["_id"]; ?>";
		    	answer.value = $("#<?php echo $key ?>").val();
		    	dataHelper.path2Value(answer , function(params) { 
		            //toastr.success('saved');
		        });
		    } else if (typeof userConnected.telephone.fixe !== 'undefined'){

			    $("#<?php echo $key ?>").val(userConnected.telephone.mobile);

			    answer = new Object;
			    answer.path = "answers."+$("#<?php echo $key ?>").data("form")+"."+"<?php echo $key ?>";
		    	answer.collection = "answers" ;
		    	answer.id = "<?php echo $answer["_id"]; ?>";
		    	answer.value = $("#<?php echo $key ?>").val();
		    	dataHelper.path2Value(answer , function(params) { 
		            //toastr.success('saved');
		        });
		    }
		}
		}

	});




</script>
<?php } ?>