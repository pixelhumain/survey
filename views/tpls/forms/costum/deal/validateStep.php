<style type="text/css">
	.checkedvalue{
		color: #00C851;
	}

	.timesvalue{
		color: #ff4444;
	}

	.tovalidatelist{
		list-style: none;
	}

	.gotoinput-btn{
	    border: 2px solid #0099CC;
	    border-radius: 5px;
	    font-size: 15px;
	    background-color: transparent;
	    cursor: pointer;
	    padding: 2px;
	    margin: 2px;
	}

	.inputList{
		display: flow-root;
	}
</style>
<?php

	$isValidate = false;

	$formList = [];
	$stepList = [];
	$lastForm = false;
	$ischecked= [];

	if (isset($parentForm["subForms"])) {
		foreach ($parentForm["subForms"] as $keysF => $valuesF) {
			$stepList[$valuesF] = $valuesF;
			$lastSubForm = $valuesF;
		}
	}else{
		$lastForm = true;
	}

	if ($form["id"] == $lastSubForm) {
		$lastForm = true;
	}


	if (isset($form)) {
		foreach ($form["inputs"] as $fid => $fvalue) {
			if ($fid != $key) {
				$formList[$fid] = $fvalue["label"];
			}
		}
	}
	if (isset($answers) && !is_null($answers) && !empty($answers)) {
		$isValidate = true;
	}


	$canValidate = false;
	$alreadyChecked = 0;
	$inputListOptions = [];
	$comV = [];

	//var_dump($canEditForm);

	$editParamsBtn = ($canEditForm=="true" && ($mode != "r" || $mode != "pdf")) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

	$contactType = [
		"Partenaire"=>"Partenaire",
		"Financeur" => "Financeur",
	 	"Opérateur"=>"Opérateur",
	 	"Répondant"=>"Répondant",
	 	"OpérateurValidated"=>"Opérateur validé",
	 	"customInput" => "Réponse d'une question"
	 ];

	foreach ($form["inputs"] as $forminputskey => $forminputsvalue) {
		if (isset($forminputsvalue["label"])) {
			$inputListOptions[$forminputskey] = $forminputsvalue["label"];
		}
	}

	if (isset($answer["links"]["operators"])) {
	 	$operateurVid = $answer["links"]["operators"];
	 	foreach ($operateurVid as $vkey => $vvalue) {
		 	$comV = Element::getCommunityByTypeAndId("organizations", $vkey);
	 	}
	}


	$selectedContact = array("roles"=>array());



	 if( isset($parentForm["params"][$kunik]["contact"]) ){
           	if (is_array($parentForm["params"][$kunik]["contact"]) &&  in_array("Financeur", $parentForm["params"][$kunik]["contact"]) )  {
           		array_push($selectedContact["roles"], "Financeur");
           	}
           	if (is_array($parentForm["params"][$kunik]["contact"]) &&  in_array("Opérateur", $parentForm["params"][$kunik]["contact"])) {
           		array_push($selectedContact["roles"], "Opérateur");
           	}
     }

	$paramsData = [
        "inputList" => [],
        "step" => "",
        "contact" => [],
		"object" => "",
		"msg" => "",
		"msgbox" => "",
		"notif" => "",
		"urlRedirect" => ""
    ];

     if( isset($parentForm["params"][$kunik]["inputList"]) && !empty($parentForm["params"][$kunik]["inputList"])){
           $paramsData["inputList"] =  $parentForm["params"][$kunik]["inputList"];
     }

     if( isset($parentForm["params"][$kunik]["step"]) ){
           $paramsData["step"] =  $parentForm["params"][$kunik]["step"];
     }

     if( isset($parentForm["params"][$kunik]["object"]) ){
           $paramsData["object"] =  $parentForm["params"][$kunik]["object"];
     }

     if( isset($parentForm["params"][$kunik]["msg"]) ){
           $paramsData["msg"] =  $parentForm["params"][$kunik]["msg"];
     }

     if( isset($parentForm["params"][$kunik]["msgbox"]) ){
           $paramsData["msgbox"] =  $parentForm["params"][$kunik]["msgbox"];
     }

      if( isset($parentForm["params"][$kunik]["notif"]) ){
           $paramsData["notif"] =  $parentForm["params"][$kunik]["notif"];
     }

     if( isset($parentForm["params"][$kunik]["contact"]) ){
           $paramsData["contact"] =  $parentForm["params"][$kunik]["contact"];
     }

     if( isset($parentForm["params"][$kunik]["urlRedirect"]) ){
           $paramsData["urlRedirect"] =  $parentForm["params"][$kunik]["urlRedirect"];
     }

    $totalToChecked = sizeof($paramsData["inputList"]);
   // var_dump($totalToChecked);
   // var_dump($paramsData["inputList"]);exit;

    if($totalToChecked > 0){
	//	var_dump($paramsData["inputList"]);exit;
    	foreach ($paramsData["inputList"] as $inid => $invalue) {
			if ($alreadyChecked < $totalToChecked && isset($answer["answers"][$form["id"]])) {
				foreach ($answer["answers"][$form["id"]] as $kAns => $vAns) {
					if(strpos($kAns, $invalue) !== false){
						$alreadyChecked++;
						array_push($ischecked, $invalue);
					}
				}
			}
		}
    }

    $message = "Veuillez remplir les champs obligatoires pour passer à l'etape suivante";

	$message .= "<div class='tovalidatelist'>";
	if (!empty($parentForm["params"][$kunik]["inputList"])) {
		foreach ($parentForm["params"][$kunik]["inputList"] as $keysF => $valuesF) {
			$message .="<div class='inputList'>";

			if (in_array($valuesF, $ischecked)) {
				$message .= "<span class='checkedvalue'>";
			} else {
				$message .= "<span class='timesvalue'>";
			}
			$message .= $formList[$valuesF];
			if (in_array($valuesF, $ischecked)) {
				$message .= '</span><i class="fa fa-check-circle checkedvalue pull-left success" aria-hidden="true"></i>';
			} else {
				$message .= '</span><i class="fa fa-times-circle timesvalue pull-left" aria-hidden="true"></i>';
			}

			$message .='<a data-path="question'.$valuesF.'" class="gotoinput-btn pull-right">s\'y rendre</a></div>';
		}
	}
	$message .= "</div>";


	if ($paramsData['step'] == "") {
		$paramsData['step'] = $form['id'];
	}

    if (($totalToChecked == $alreadyChecked) || (!isset($parentForm["params"][$kunik]["inputList"]) or (isset($parentForm["params"][$kunik]["inputList"]) && sizeof($parentForm["params"][$kunik]["inputList"]) == 0)) || (isset($answers) && $answers == "canValidate")) {
    	$canValidate = true;
    }

    if ($isValidate) {
    	$canValidate = false;

    	$message = (isset($parentForm["params"][$kunik]["msgbox"]) && $parentForm["params"][$kunik]["msgbox"] != "") ? $parentForm["params"][$kunik]["msgbox"] : "Cette étape a été validée le ".$answers;
    }

	$hasUserEmail = false;
	$userEmail = "";

	$costum = CacheHelper::getCostum();

	if(in_array("Financeur",Yii::app()->session["costum"][$costum["slug"]]["hasRoles"]) || in_array("Opérateur",Yii::app()->session["costum"][$costum["slug"]]["hasRoles"])){ 
		$userEmail = (isset($answer["answers"]["deal1"]["deal125"])) ? $answer["answers"]["deal1"]["deal125"] : "" ;

	}else if(isset(Yii::app()->session["userEmail"])){
			$hasUserEmail = true;
			$userEmail = Yii::app()->session["userEmail"];
	}	





?>

<?php if(!$lastForm){?>
	<div style="border: 2px solid darkgrey;padding: 20px;border-radius: 20px; overflow-x: auto; text-align: center" class="step-vld">
		<?php if(!$isValidate && $canValidate){ 
			?>
			<button type="button" class="btn validate<?php echo $kunik ?>Params" >Passer à l'étape suivante</button>
		<?php } else { echo $message; } echo $editParamsBtn?>

	</div>
<?php } elseif ($lastForm) { ?>
	<div style="border: 2px solid darkgrey;padding: 20px;border-radius: 20px; overflow-x: auto; text-align: center" class="step-vld">
		<?php if(!$isValidate && $canValidate){ ?>
			<button type="button" class="btn validate<?php echo $kunik ?>Params" >Terminer</button>
		<?php } else { echo $message; } echo $editParamsBtn?>

	</div>
<?php }  

$mustSendMail = [];
$mailList =[];

if (isset($parentForm["params"][$kunik]["contact"]) && is_array($parentForm["params"][$kunik]["contact"])) {
	if (is_array($parentForm["params"][$kunik]["contact"]) && !empty($parentForm["params"][$kunik]["contact"])) {
		if(in_array("Répondant", $parentForm["params"][$kunik]["contact"])){
			array_push($mailList, $userEmail);
		}
	}

	if(in_array("OpérateurValidated", $parentForm["params"][$kunik]["contact"]) && isset($comV)){
		foreach ($comV as $keypers => $valuepers) {
			if ($valuepers["type"] == "citoyens") {
				array_push($mustSendMail, $keypers);
			}
		}
	}

	if(in_array("Opérateur", $parentForm["params"][$kunik]["contact"]) || in_array("Financeur", $parentForm["params"][$kunik]["contact"])){
		foreach ($this->costum["communityLinks"] as $comkey => $comvalue) {
			foreach ($comvalue as $ke => $valu) {
				if (isset($valu["roles"])) {					
					if (in_array("Opérateur", $parentForm["params"][$kunik]["contact"]) && in_array("Opérateur", $valu["roles"])) {
						array_push($mustSendMail, $ke);
					}
					if (in_array("Financeur", $parentForm["params"][$kunik]["contact"]) && in_array("Financeur", $valu["roles"])) {
						array_push($mustSendMail, $ke);
					}
				}
			}
		}
	}

	if(in_array("customInput", $parentForm["params"][$kunik]["contact"]) && isset($parentForm["params"][$kunik]["inputscontact"])){
			foreach ($parentForm["params"][$kunik]["inputscontact"] as $inputscontactkey => $inputscontactvalue) {
				if (isset($answer["answers"][$form["id"]][$inputscontactvalue])) {
					array_push($mailList, $answer["answers"][$form["id"]][$inputscontactvalue]);
				}
			}
			
	}
}

foreach ($mustSendMail as $persk => $persid) {
	$cytns = Person::getEmailById($persid);
	if (isset($cytns) && $cytns["email"] != "") {
		array_push($mailList,$cytns["email"]);
	}
}

$mailList = array_unique($mailList);

?>

<script>
	

	var contacttplMail = <?php echo json_encode( (isset($mailList)) ? $mailList : null ); ?>;

	var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

    function stepValidationReload<?php echo $form["id"]?>(){
    	reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
    }

	$(document).ready(function() {

		$(".gotoinput-btn").click( function() {
			scrollintoDiv($(this).data('path'), 2000);
		});
		var relid="#<?php echo $form["id"]?> :input,#<?php echo $form["id"]?> select,#<?php echo $form["id"]?> button";
		mylog.log("relid",relid);
		$("#<?php echo $form["id"]?> :input:not(.exclude-input),#<?php echo $form["id"]?> select,#<?php echo $form["id"]?> button").change(function(e) {
			e.stopImmediatePropagation();
			 reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
		});

		$('a[data-dom-uploader]').each(function(){
			$("#"+$(this).data('dom-uploader')).on("allComplete", function(event, succeeded, failed) {
	    		reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
	    		event.stopImmediatePropagation();
	    	});
		});

		$('div[data-inputtype="address"]').each(function(){
			
			if (typeof window[$(this).data("mapobj")] !== "undefined" && typeof window[$(this).data("mapobj")].map !== "undefined" && typeof window[$(this).data("mapobj")].map.map !== "undefined") {
			window[$(this).data("mapobj")].map.map.on("viewreset" ,  function (e) { 

				$('div[data-inputtype="address"]').each(function(){
					
					$(this).mouseleave(function(event){
						reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
						$(this).off('mouseleave');
			    		event.stopImmediatePropagation();
					})

				});

			});
			}
			
		});

		sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {
                "title" : "<?php echo $kunik ?> config",
                "description" : "Liste de questions possibles",
                "icon" : "fa-cog",
                "properties" : {
                	inputList : {
		                inputType : "selectMultiple",
		                label : "Question obligatoire",
		                options :  <?php echo json_encode($formList) ?>,
		                values :  sectionDyf.<?php echo $kunik ?>ParamsData.inputList
		            },
		            step : {
		                inputType : "select",
		                label : "Etape à débloquer",
		                options :  <?php echo json_encode($stepList) ?>,
		                values :  sectionDyf.<?php echo $kunik ?>ParamsData.step
		            },
		            "for":{
			              "inputType":"custom",
			              "html" : "<p> configuration mail </p>"
			          },
		            contact : {
		                inputType : "selectMultiple",
		                label : "Contact",
		                options :  <?php echo json_encode($contactType) ?>,
		                values :  sectionDyf.<?php echo $kunik ?>ParamsData.contact
		            },
		            inputscontact : {
		            	inputType : "selectMultiple",
		                label : "Question avec une réponse en format email",
		                options :  <?php echo json_encode($inputListOptions) ?>,
		                values :  sectionDyf.<?php echo $kunik ?>ParamsData.inputscontact
		            }, 
		            object : {
		                inputType : "text",
		                label : "Objet",
		                values :  sectionDyf.<?php echo $kunik ?>ParamsData.object
		            },
		            msg : {
		                inputType : "text",
		                label : "Message",
		                values :  sectionDyf.<?php echo $kunik ?>ParamsData.msg
		            },
		            msgbox : {
		                inputType : "text",
		                label : "Text à afficher si validé",
		                values :  sectionDyf.<?php echo $kunik ?>ParamsData.msgbox
		            },
		            notif : {
		                inputType : "text",
		                label : "Notification à afficher si validé",
		                values :  sectionDyf.<?php echo $kunik ?>ParamsData.notif
		            }
		            <?php if($lastForm) {?>
		    		,        
		            urlRedirect : {
		            	inputType : "url",
		                label : "Lien de redirection",
		                values :  sectionDyf.<?php echo $kunik ?>ParamsData.urlRedirect
		            }
		    <?php } ?>

                },
                save : function () {
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                        if(val.inputType == "properties")
                            tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                        else if(val.inputType == "array")
                            tplCtx.value[k] = getArray('.'+k+val.inputType);
                        else
                            tplCtx.value[k] = $("#"+k).val();
                        mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                    });
                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined"){
                        toastr.error('value cannot be empty!');
                    }
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.closeForm();
                            urlCtrl.loadByHash(location.hash);
                        } );
                    }

                }
            }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
		    tplCtx.id = $(this).data("id");
		    tplCtx.collection = $(this).data("collection");
		    tplCtx.path = $(this).data("path");
		    dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
		});

		$(".validate<?php echo $kunik ?>Params").off().on("click",function() {
			tplCtxR = {};
			tplCtxR.id = "<?php echo $answer["_id"] ?>";
            tplCtxR.collection = "<?php echo Form::ANSWER_COLLECTION ?>";
            tplCtxR.path = "<?php echo "answers.".$form["id"].".".$key ?>"; 
            var today = new Date();
			var dd = String(today.getDate()).padStart(2, '0');
			var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
			var yyyy = today.getFullYear();

			today = dd + '/' + mm + '/' + yyyy;

            tplCtxR.value = today;
            dataHelper.path2Value( tplCtxR, function(params) {
            });

            <?php if($paramsData['step'] != $form['id']) { ?>
            tplCtx = {};
              tplCtx.id = "<?php echo $answer["_id"] ?>",
              tplCtx.path = "step",
              tplCtx.collection = "<?php echo Form::ANSWER_COLLECTION ?>"; 
              tplCtx.value = "<?php echo $paramsData["step"] ?>";
              dataHelper.path2Value( tplCtx, function(params) {
              		localStorage.setItem("wizardStep","#<?php echo $paramsData["step"] ?>");
              		<?php if(isset($paramsData["urlRedirect"]) && !empty($paramsData["urlRedirect"])){ ?>  
                  		document.location.href="<?php echo $paramsData["urlRedirect"];?>";
                  <?php }
                  	else{ ?>
                  		urlCtrl.loadByHash(location.hash);
                  	 <?php }?>	
                  	
              } );
            <?php }else{?>
            	urlCtrl.loadByHash(location.hash);
            <?php }?>	
            var answerId = <?php echo json_encode((String)$answer['_id']); ?>;
            var paramsmail<?php echo $kunik ?> = {
            tpl : "basic",
            tplObject : "<?php echo (isset($parentForm["params"][$kunik]["object"]) ? $parentForm["params"][$kunik]["object"] : "") ?>",
            tplMail : contacttplMail,
            html: "<?php echo (isset($parentForm["params"][$kunik]["msg"]) ? $parentForm["params"][$kunik]["msg"] : "") ?>",
            community : <?php echo (!empty($selectedContact["roles"]) ? json_encode($selectedContact) : "[]" ) ?>,
            btnRedirect : {
            	hash : "#answer.index.id." + answerId,
            	label : "Accéder au dossier"
            }
          };

          
         if(paramsmail<?php echo $kunik ?>.tplMail!=[] && paramsmail<?php echo $kunik ?>.html!="" && paramsmail<?php echo $kunik ?>.tplObject!=""){
         	mylog.log('azee', paramsmail<?php echo $kunik ?>);
	          ajaxPost(
	                null,
	                baseUrl+"/co2/mailmanagement/createandsend",
	                paramsmail<?php echo $kunik ?>,
	                function(data){ 
	                    toastr.success("<?php echo (isset($parentForm["params"][$kunik]["notif"]) ? $parentForm["params"][$kunik]["notif"] : "Mail de confirmation envoyée") ?>");
	                }
	          ); 
      	}


        });

          //  var urlRedirect = <?php isset($paramsData["urlRedirect"]) ? $paramsData["urlRedirect"] : location.hash ;?>

            
         
           
	});
</script>
