<?php 

?>

<style type="text/css">
 .progress {
height: 20px;
margin-bottom: 20px;
overflow: hidden;
background-color: #f5f5f5;
border-radius: 4px;
-webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
}
.progress {
background-image: -webkit-gradient(linear,left 0,left 100%,from(#ebebeb),to(#f5f5f5));
background-image: -webkit-linear-gradient(top,#ebebeb 0,#f5f5f5 100%);
background-image: -moz-linear-gradient(top,#ebebeb 0,#f5f5f5 100%);
background-image: linear-gradient(to bottom,#ebebeb 0,#f5f5f5 100%);
background-repeat: repeat-x;
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffebebeb',endColorstr='#fff5f5f5',GradientType=0);
}
.progress {
height: 27px;
background-color: #ebeef1;
background-image: none;
box-shadow: none;
}
.progress-bar {
float: left;
width: 0;
height: 100%;
font-size: 12px;
line-height: 20px;
color: #fff;
text-align: center;
background-color: #428bca;
-webkit-box-shadow: inset 0 -1px 0 rgba(0,0,0,0.15);
box-shadow: inset 0 -1px 0 rgba(0,0,0,0.15);
-webkit-transition: width .6s ease;
transition: width .6s ease;
}
.progress-bar {
background-image: -webkit-gradient(linear,left 0,left 100%,from(#428bca),to(#3071a9));
background-image: -webkit-linear-gradient(top,#428bca 0,#3071a9 100%);
background-image: -moz-linear-gradient(top,#428bca 0,#3071a9 100%);
background-image: linear-gradient(to bottom,#428bca 0,#3071a9 100%);
background-repeat: repeat-x;
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff428bca',endColorstr='#ff3071a9',GradientType=0);
}
.progress-bar {
box-shadow: none;
border-radius: 3px;
background-color: #0090D9;
background-image: none;
-webkit-transition: all 1000ms cubic-bezier(0.785, 0.135, 0.150, 0.860);
-moz-transition: all 1000ms cubic-bezier(0.785, 0.135, 0.150, 0.860);
-ms-transition: all 1000ms cubic-bezier(0.785, 0.135, 0.150, 0.860);
-o-transition: all 1000ms cubic-bezier(0.785, 0.135, 0.150, 0.860);
transition: all 1000ms cubic-bezier(0.785, 0.135, 0.150, 0.860);
-webkit-transition-timing-function: cubic-bezier(0.785, 0.135, 0.150, 0.860);
-moz-transition-timing-function: cubic-bezier(0.785, 0.135, 0.150, 0.860);
-ms-transition-timing-function: cubic-bezier(0.785, 0.135, 0.150, 0.860);
-o-transition-timing-function: cubic-bezier(0.785, 0.135, 0.150, 0.860);
transition-timing-function: cubic-bezier(0.785, 0.135, 0.150, 0.860);
}
.progress-bar-success {
background-image: -webkit-gradient(linear,left 0,left 100%,from(#5cb85c),to(#449d44));
background-image: -webkit-linear-gradient(top,#5cb85c 0,#449d44 100%);
background-image: -moz-linear-gradient(top,#5cb85c 0,#449d44 100%);
background-image: linear-gradient(to bottom,#5cb85c 0,#449d44 100%);
background-repeat: repeat-x;
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff5cb85c',endColorstr='#ff449d44',GradientType=0);
}
.progress-bar-success {
background-color: #0AA699;
background-image: none;
}

</style>

<?php
$label = "0";
$paramsData = [
    "label" => [
        "0" => "20" , "80" => "80", "100" => "100"
    ],
    "0" => [

    ]
];
if (isset($answers["0"])){
    $label = $answers["0"];
} 
$isFinanceur=false;
if(Authorisation::isInterfaceAdmin())
  $isFinanceur=true;
if(isset($this->costum["financorOf"])){
  $isFinanceur=true;
}
$isOperator = false;
if(isset($this->costum["operatorOf"])){
  $isOperator= true; 
}


?>

<div class="form-group">
      <div class="col-md-<?php echo ($isOperator || $isFinanceur)? "10" : "12" ?> col-sm-12">
          <div class="progress">
              <div data-percentage="0%" style="width: <?php echo $paramsData["label"][$label] ?>%;" class="progress-bar progress-bar-success" role="progressbar" aria-valuemin="20" aria-valuemax="100">
                  <div class="skill-name"><strong><?php echo $label ?> %</strong></div>
              </div>
          </div>
      </div>
      <?php
          // if ($isFinanceur || $isOperator) {
      ?>
      <div class="col-md-2 col-sm-12">
            <select class="form-control changeprogression col-md-4 col-sm-6" data-path="">
                <?php foreach ($paramsData["label"] as $lid => $lvalue) { ?>
                    <option <?php echo ($lid == $label)? "selected": ""; ?> value="<?php echo $lid ?>"> <?php echo $lid ?> %</option>
                <?php } ?>
            </select>
      </div>
      <?php
          // }
      ?>
      
</div>


<script type="text/javascript">
$(document).ready(function() {   

      $('.changeprogression').on('change', function() {
          tplCtx.id = "<?php echo $answer["_id"] ?>",
          tplCtx.path = "<?php echo $answerPath."0" ?>",
          tplCtx.collection = "<?php echo Form::ANSWER_COLLECTION ?>"; 
          tplCtx.value = this.value
          dataHelper.path2Value( tplCtx, function(params) {
              urlCtrl.loadByHash(location.hash);
          } );

      });

});    
</script>

<?php
    // if ($isFinanceur || $isOperator) {
?>

<script type="text/javascript">
$(document).ready(function() {   

<?php
    if ($label == "0" || $label == "80" || $label == "100") {
?>
    $("#questionsection0").removeClass("hide");
    $("#questiondeal501").removeClass("hide");
    $("#questiondeal502").removeClass("hide");
    $("#questiondeal503").removeClass("hide");
    $("#questiondeal504").removeClass("hide");
    $("#questiondeal505").removeClass("hide");
    $("#questiondeal506").removeClass("hide");
    $("#questiondeal507").removeClass("hide");
<?php
    }
    if ($label == "80" || $label == "100") {
?>
    $("#questionsection1").removeClass("hide");
    $("#questiondeal511").removeClass("hide");
    $("#questiondeal512").removeClass("hide");
    $("#questiondeal513").removeClass("hide");
    $("#questiondeal514").removeClass("hide");
<?php
    }
    if ($label == "100") {
?>
    $("#questionsection3").removeClass("hide");
    $("#questiondeal521").removeClass("hide");
    $("#questiondeal522").removeClass("hide");
    $("#questiondeal523").removeClass("hide");
    $("#questiondeal524").removeClass("hide");
    $("#questiondeal525").removeClass("hide");
    $("#questiondeal526").removeClass("hide");
    $("#questiondeal527").removeClass("hide");
    $("#questiondeal528").removeClass("hide");
    $("#questiondeal529").removeClass("hide");
<?php
    }
?>


});    
</script>

<?php
    // }
?>
