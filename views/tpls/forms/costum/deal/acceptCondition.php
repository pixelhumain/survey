<style type="text/css">
	.ckd-grp {
  /*position: absolute;
  top: calc(50% - 10px);*/
}
.ckd-grp label {
  cursor: pointer;
  -webkit-tap-highlight-color: transparent;
  padding: 6px 8px;
  border-radius: 20px;
  float: left;
  transition: all 0.2s ease;
}
.ckd-grp label:hover {
  background: rgba(125,100,247,0.06);
}
.ckd-grp label:not(:last-child) {
  margin-right: 16px;
}
.ckd-grp label span {
  vertical-align: middle;
}
.ckd-grp label span:first-child {
  position: relative;
  display: inline-block;
  vertical-align: middle;
  width: 27px;
  height: 27px;
  background: #e8eaed;
  border-radius: 10px;
  transition: all 0.2s ease;
  margin-right: 8px; 
   padding: 3px;
}
.ckd-grp label span:first-child:after {
  content: '';
  position: absolute;
  width: 16px;
  height: 16px;
  margin: 2px;
  background: #fff;
  border-radius: 6px;
  transition: all 0.2s ease;
}
.ckd-grp label:hover span:first-child {
  /*background*/: #7d64f7;
}

.ckb-grp label:hover span:first-child:after {
  /*background: #7d64f7;*/
  /*background: #7d64f7;*/
  padding: 0px;
  background: white;

}

.ckd-grp input {
  display: none;
}
.ckd-grp input:checked + label span:first-child {
  /*background: #7d64f7;*/
  background: #e8eaed;
}
.ckd-grp input:checked + label span:first-child:after {
  /*transform: scale(0.5);*/
  background: #7d64f7;
}

.effect-2{border: 0; padding: 7px 0; border-bottom: 1px solid #ccc;}

.effect-2 ~ .focus-border{position: absolute; bottom: 0; left: 0; width: 0; height: 2px; background-color: #3399FF; transition: 0.4s;}
.effect-2:focus ~ .focus-border{width: 100%; transition: 0.4s;}

.paramsonebtn , .paramsonebtnP {
	font-size: 17px;
	display: none;
	padding: 5px;
}

.paramsonebtn:hover {
	color: red;
}

.paramsonebtnP:hover {
	color: blue;
}

.thckd:hover .paramsonebtn, .thckd:hover .paramsonebtnP {
	display: inline-block;
}

.effect-2:focus {
	outline: none !important;
}

.responsemulticheckboxplus:before{
	content: '\f0a9';
	margin-right: 15px;
	font-family: FontAwesome;
	color: #d9534f;
}

</style>



<?php 

$value = (!empty($answers)) ? " value='".$answers."' " : "";
$inpClass = " saveOneByOne"; 

$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";



$subkey = $key;


$paramsData = [ 
	"list" => [	"Je comprends"],
	"tofill" => [ ],
	"placeholdersckb" => [ ],
	"type" => [
		"simple" => "Sans champ de saisie",
		"cplx" => "Avec champ de saisie"
	],
	'checked' => [],
	"width" => [
		"12" => "1",
		"6" => "2",
		"4" => "3"
	]
 ];




if( isset($answer["answers"][$form["id"]][$kunik]) ) {
	$paramsData["checked"] =  $answer["answers"][$form["id"]][$kunik];
}


if($mode == "r")
{ 
?>
   <div class="" style="padding-top: 50px; text-transform: unset;">
    	<label class="form-check-label" for="<?php echo $key ?>">
    		<h4 style="text-transform: unset; color: <?php echo ($titleColor) ? $titleColor : "black"; ?>;"><?php echo $label ?>
    		</h4>
    	</label>	
    

    	<?php 
   
    	?>
		
			<div class="">
    			<table style="width: 100%">
  				<tr class=""><th >
  				<div class="col-md-12">
	    			<?php 
		    		foreach ($paramsData["list"] as $ix => $lbl) 
					{ 
						$ckd = "";
						$place = "";
						foreach ($paramsData["checked"] as $key => $value) {
							foreach ($value as $ke => $va) {
								if($ke == $lbl){
									$ckd = "checked";
								}
							}
						}
						if ($ckd == "checked"){
					?>

		    			<!-- <tr class="thckd"><theih > -->
		    			<div class="col-md-12 col-xs-12 col-sm-12" style="min-height: 50px;">
		    			<div class="">
			    			
	  						<label for="<?php echo $kunik.$ix ?>" class="responsemulticheckboxplus">
	  							<span>
	  								
	  							</span>
	  							<span><?php echo $lbl ?>
		  							
								</span>
							</label> 
	  					</div>
	  					</div>
		    		<?php 
		    		} }
		    		?>
		    	</div>
		    	</th></tr>
				</table>
			</div>
		
    
		<?php 
		if(!empty($info))
		{ 
		?>
	    	<small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
	    <?php
	    }
    	?>
	</div>

<?php 
}else
{
?>


	<div class="" style="padding-top: 50px; text-transform: unset;">
    	<div class="col-md-12" style="padding: 30px; border : 3px solid <?php echo ($titleColor) ? $titleColor : "black"; ?>">
    		<h3>Engagement</h3>
    		<h4>Si votre dossier est retenu ... </h4><span>... Vous vous engagerez auprès des financeurs à des obligations relatives à votre logement (durée d'occupation, usage, ...). Celles-ci vous seront précisées dans les arrêtés préfectoraux d'attribution des subventions publiques.</span>
    		<!-- <ul style="padding: 10px">
    			<li>Occuper votre logement en tant que résidence principale pendant 15 ans</li>
    			<li>Ne pas mettre à la location ou transformer votre logement en local commercial</li>
    			<li>Prévenir le préfet en cas d'intention de vente ou d'obligation de vente ou de location</li>
    		</ul> -->
    		<br>
    		<h4> Pièces justificatives à fournir (à titre informatif) </h4>
    		<ol style="padding: 10px">
    			<li>Copie de la carte nationale d'identité ou du passeport</li>
    			<li>Copie du justificatif des ressources annuelles de l'année N-2 (ou N-1 si baisse de revenus) : avis
d'imposition ou, à défaut, déclaration sur l'honneur et une attestation de la
CAF, le justificatif de droit pour les personnes aux minima sociaux*</li>
    			<li>Copie du livret de famille</li>
    			<li>Copie acte de propriété ou attestation notariée du titre de propriété</li>
    		</ol>
    	</div>	
    
			<div class="ckd-grp">
    			<table style="width: 100%">
  				<tr class=""><th >
  				<div class="col-md-12">
	    			<?php 
		    		foreach ($paramsData["list"] as $ix => $lbl) 
					{ 
						$ckd = "";
						$place = "";
						foreach ($paramsData["checked"] as $key => $value) {
							foreach ($value as $ke => $va) {
								if($ke == $lbl){
									$ckd = "checked";
								}
							}
						}
					?>

		    			<!-- <tr class="thckd"><theih > -->
		    			<div class="col-md-<?php echo (isset($parentForm["params"][$kunik]['global']['width'])) ? $parentForm["params"][$kunik]['global']['width'] : '12'; ?> col-xs-12 col-sm-12" style="min-height: 50px;">
		    			<div class="thckd">
			    			<input data-id="<?php echo $kunik ?>" class=" ckbCo "  id="<?php echo $kunik.$ix ?>" data-form='<?php echo $form["id"] ?>' <?php echo $ckd?> type="checkbox" name="<?php echo $kunik ?>" data-type="simple" value="<?php echo $lbl ?>" />

	  						<label for="<?php echo $kunik.$ix ?>">
	  							<span>
	  								
	  							</span>
	  							<span><?php echo $lbl ?>
								</span>
							</label> 
	  					</div>
	  					</div>
		    		<?php 
		    		} 
		    		?>
		    	</div>
		    	</th></tr>
				</table>
			</div>
	
		<?php 
		if(!empty($info))
		{ 
		?>
	    	<small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
	    <?php
	    }
    	?>
	</div>

<script type="text/javascript">
var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answer["answers"][$kunik])) ? $answer["answers"][$kunik] : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;



sectionDyf.<?php echo $kunik ?>ParamsPlace = "";


jQuery(document).ready(function() {
    mylog.log("render form input","/modules/costum/views/tpls/forms/cplx/multiCheckbox.php");

    	answer = {};
    	$('.ckbCo').change(function(){
    		var allckd = [];
    		// alert('<?php echo $kunik ?>');
    		$("input:checkbox[name='"+$(this).attr("name")+"']:checked").each(function(){
    			mylog.log("ckd", $(this).val());
    			var key = $(this).val();
    			var b = { [key] : { 'type' : $(this).data("type"), 'textsup' : $('input[data-imp="' + $(this).val() + '"]').val() }};
    			allckd.push(b);
			});

			mylog.log("hehe",allckd);

			// alert(allckd);

				answer.path = "answers."+$(this).data("form")+"."+$(this).data("id");
	    		answer.collection = "answers" ;
	    		answer.id = "<?php echo $answer["_id"]; ?>";
	    		answer.value = allckd;
	    		dataHelper.path2Value(answer , function(params) { 
				});
		});

    	$('.inputckbCo').keyup(function(){ 
    		if($('input[value="' + $(this).data("imp") + '"]').is(':checked')) {

    			var allckd = [];

    			$("input:checkbox[name='"+$(this).data("impname")+"']:checked").each(function(){
	    			mylog.log("ckd", $(this).val());
	    			var key = $(this).val();
	    			var b = { [key] : { 'value' : $(this).val(), 'type' : $(this).data("type"), 'textsup' : $('input[data-imp="' + $(this).val() + '"]').val() }};
	    			allckd.push(b);
				});

			// alert(allckd);

				answer.path = "answers."+$(this).data("form")+"."+$(this).data("id");
	    		answer.collection = "answers" ;
	    		answer.id = "<?php echo $answer["_id"]; ?>";
	    		answer.value = allckd;
	    		dataHelper.path2Value(answer , function(params) { 
				});

	    	}
    	})


    sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "<?php echo $kunik ?> config",
	        "icon" : "fa-cog",
	        "properties" : {
	            list : {
	                inputType : "array",
	                label : "Liste de bouton check",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.list
	            },
	            width : {
                    inputType : "select",
                    label : "Nombre d'element par ligne",
                    options :  sectionDyf.<?php echo $kunik ?>ParamsData.width,
                    value : "<?php echo (isset($parentForm["params"][$kunik]['global']['width']) and $parentForm["params"][$kunik]['global']['width'] != "") ? $paramsData["width"][strval($parentForm["params"][$kunik]['global']['width'])] : ''; ?>"
                }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	        		 if(val.inputType == "properties")
                                tplCtx.value = getPairsObj('.'+k+val.inputType);
                            else if(val.inputType == "array")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else if(val.inputType == "formLocality")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else
                                tplCtx.value[k] = $("#"+k).val();
	        	});
	            mylog.log("save tplCtx",tplCtx);

	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    $("#ajax-modal").modal('hide');
	                    location.reload();
	                } );
	            }

	    	}
	    }
	};


   	<?php if(count($paramsData["list"]) != 0){
	  	foreach ($paramsData["list"] as $key => $value) {
	  		// if($lbl == $key){
	  		// 	if($value == "cplx"){
	?>
	sectionDyf.<?php echo $kunik ?>oneParams<?php echo $key ?> = {
		"jsonSchema" : {	
	        "title" : "<?php echo $kunik ?> config",
	        "icon" : "fa-cog",
	        "properties" : {
	            tofill : {
	                inputType : "select",
	                label : "Bouton check avec champ de saisie",
	                options :  sectionDyf.<?php echo $kunik ?>ParamsData.type,
                    values : "<?php echo (isset($paramsData["tofill"][$value])) ? $paramsData["tofill"][$value] : ''; ?>"
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>oneParams<?php echo $key ?>.jsonSchema.properties , function(ko,valo) { 

                            $.each(sectionDyf.<?php echo $kunik ?>ParamsData.tofill, function(ke, va) {
                            	
                            	if(ke == "<?php echo $value; ?>"){
                            		
                            		var azerazer = ke.toString();
                            		sectionDyf.<?php echo $kunik ?>ParamsData.tofill[azerazer] = $("#"+ko).val();
                            	}
                            })

                            tplCtx.value = sectionDyf.<?php echo $kunik ?>ParamsData.tofill;


	        	});
	            mylog.log("save tplCtx",tplCtx);

	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    $("#ajax-modal").modal('hide');
	                    location.reload();
	                } );
	            }

	    	}
	    }
	};
	
	$(".editone<?php echo $kunik ?>Params<?php echo $key ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path")+'.tofill';
        //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>oneParams<?php echo $key ?>,null, sectionDyf.<?php echo $kunik ?>oneParams<?php echo $key ?>);
    });
	<?php
	  			}
	  		}
	//   	}
	// } 
	?>



	$(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path")+'.global';
        //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    
});
</script>
<?php
}
?> <!-- else 164 -->