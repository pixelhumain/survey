<?php 
$value = (!empty($answers)) ? " value='".$answers."' " : "";

$inpClass = "form-control";

if($type == "tags") 
	$inpClass = "select2TagsInput";

if($saveOneByOne)
	$inpClass .= " saveOneByOne";


$isFinanceur=false;
$isOperator=false;
if(Authorisation::isInterfaceAdmin())
	$isFinanceur=true;
if(isset($this->costum["financorOf"])){
	$isFinanceur=true;
}


if(isset($this->costum["hasRoles"]) && in_array("Opérateur", $this->costum["hasRoles"])){
	$isOperator=true;
}
// if ($isFinanceur || $isOperator) {

if($mode == "r" || $mode == "pdf"){ ?>
    <div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
        <label for="<?php echo $kunik ?>"><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label ?></h4></label><br/>
        <?php echo $answers; ?>
    </div>
<?php 
}else{
?>
	<div class="form-group">
	    <label for="<?php echo $key ?>"><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn ?></h4></label>
	    <br/><input type="<?php echo (!empty($type)) ? $type : 'text' ?>" class="<?php echo $inpClass ?>" id="<?php echo $key ?>" aria-describedby="<?php echo $key ?>Help" placeholder="<?php echo (isset($placeholder)) ? $placeholder : '' ?>" data-form='<?php echo $form["id"] ?>'  <?php echo $value ?> >
	    <?php if(!empty($info)){ ?>
	    	<small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
	    <?php } ?>
	</div>
<?php } ?>

<?php if($mode != "pdf"){ ?>
<script type="text/javascript">

	var users = [];


	jQuery(document).ready(function() {
	    mylog.log("render form input","/modules/costum/views/tpls/forms/text.php");

	    if($("#<?php echo $key ?>").val() == "" ){
		    if (typeof userConnected.email !== 'undefined') {
		    	

			    $("#<?php echo $key ?>").val(userConnected.email);

			    answer = new Object;
			    answer.path = "answers."+$("#<?php echo $key ?>").data("form")+"."+"<?php echo $key ?>";
		    	answer.collection = "answers" ;
		    	answer.id = "<?php echo $answer["_id"]; ?>";
		    	answer.value = $("#<?php echo $key ?>").val();
		    	dataHelper.path2Value(answer , function(params) { 
		            //toastr.success('saved');
		        });
		    }
		}

	});




</script>
<?php } 
// }
 ?>