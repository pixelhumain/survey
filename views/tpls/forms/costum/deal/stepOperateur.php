<?php 
//check answer of "Ampleur des travaux"
$petitTravaux = false;
$disabled = "";
$splar = "";

if (!isset($parentForm["communityLinks"])) {
	$communityLinks = array();
	if(!empty($form["parent"])){
		foreach ($form["parent"] as $key => $value) {
			$cl= Element::getCommunityByTypeAndId($value["type"],$key);
			if(!empty($cl))
				$communityLinks = array_merge($communityLinks, $cl);
		}
	}
	$parentForm["communityLinks"] = $communityLinks;
}

//var_dump("<pre>",$parentForm["communityLinks"],"</pre>");

// La splarSocietePubliqueLocaleAvenirReunion est seul financeur des petits travaux 
$splar= PHDB::findOne(Organization::COLLECTION, array("slug" => "splarSocietePubliqueLocaleAvenirReunion"));
$splar=(!empty($splar) && isset($splar["_id"])) ? (string)$splar["_id"] : "";

if (isset($answer["answers"]["deal1"]["multiRadiodeal149"]) && sizeof($answer["answers"]["deal1"]["multiRadiodeal149"]) > 0) {
        if($answer["answers"]["deal1"]["multiRadiodeal149"]["value"] == "Petits travaux"){
        	$petitTravaux = true;
        	$disabled = "disabled";
        }
}

//var_dump("<pre>",$this->costum["financorOf"],"</pre>");

//check show btn use cases 
// role is financer or operater
if( $mode != "pdf" ){

	if(Form::canAccess(['roles'=>["Financeur","Opérateur"] ])){ 
		$isFinanceur=false;
		if(Authorisation::isInterfaceAdmin())
			$isFinanceur=true;
		if(isset($this->costum["financorOf"])){
			$isFinanceur=true;
			$financorOf=$this->costum["financorOf"];
			$financorOf["thumbImg"]=(isset($financorOf["profilThumbImageUrl"])) ? Yii::app()->createUrl($financorOf["profilThumbImageUrl"]) :  Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl()."/images/thumb/default_organizations.jpg"; 
		} 
		$isOperator = false;
		if(isset($this->costum["operatorOf"])){
			$isOperator= true;
			$operatorOf=$this->costum["operatorOf"];
			$operatorOf["thumbImg"]=(isset($operatorOf["profilThumbImageUrl"])) ? Yii::app()->createUrl($operatorOf["profilThumbImageUrl"]) :  Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl()."/images/thumb/default_organizations.jpg"; 
				
		}
		$selectedOperator = false;
		$alreadyCandidate=false; ?>
		<table class="table table-bordered table-hover  directoryTable" >
			<tbody class="directoryLines">	
				<tr>

					<?php $label=(@$answer["step"]=="deal12") ? "Opérateur sélectionné par le ménage" : "Intention opérateur"; ?>
					<td colspan='2' ><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label ?></h4></td>
				</tr>
				<?php 
				$str = ""; 
				if(isset($answer["links"]["operators"]))
				{
					foreach ($answer["links"]["operators"] as $oid => $state) {
						$elt=Element::getElementById($oid, Organization::COLLECTION, null, array("name","slug", "profilThumbImageUrl"));
						$thumb=$elt["thumbImg"]=(isset($elt["profilThumbImageUrl"])) ? Yii::app()->createUrl($elt["profilThumbImageUrl"]) :  Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl()."/images/thumb/default_organizations.jpg"; 
						if($isOperator && $oid==(string)$operatorOf["_id"])
							$alreadyCandidate=true;
						// Un opérateur a déja été selectionné !
						if($state != "0")
						{
							$selectedOperator = true;
							$str .= '<tr>'.
								'<td class="padding-top-15">Opérateur</td>'.
								'<td><a href="#page.type.organizations.id.'.$oid.'" class="lbh-preview-element"><img src="'.$thumb.'" width=50 height=50/> '.$elt["name"].'</a></td>'.
								'<td class="padding-top-15"><span class="label label-success">Validé</span></td>'.
							'</tr>'; 
						} 
						else if( !$selectedOperator )
						{
							//if role financeurs
							$str .= '<tr>'.
								'<td class="padding-top-15">Opérateur candidat</td>'.
								'<td>'.
									'<a href="#page.type.organizations.id.'.$oid.'" class="lbh-preview-element">'.
										'<img src="'.$thumb.'" width=40 height=40/> '.$elt["name"].
									'</a>'.
								'</td>'.
								'<td class="padding-top-15">';
								if($isFinanceur)
									$str.='<span class="label label-warning"><a href="javascript:;" class="validateOperator text-white" data-id="'.$oid.'"> Valider l\'opérateur</a></span>';
								else
									$str.='<span class="label italic text-dark">En attente de traitement</span>';
							$str.='</td>'.
							'</tr>'; 
						}
					}
				} else{
					$str="<tr>".
							"<td colspan='2'>Aucun opérateur n'est positionné sur ce dossier</td>".
						"</tr>"; 
				}
				echo $str;
				
				
				// Si l'utilisateur a un role opérator 
				if(!$selectedOperator){
					if($isOperator && !$isFinanceur){
						if( !$alreadyCandidate && @$answer["step"]!="deal12"){
						?>
							<tr>
								<td colspan='2' class="text-center" >
									<a href="javascript:;" data-oid="<?php echo (string)$operatorOf["_id"] ?>" class="intentOperator btn btn-primary">Demande de prise en charge en tant qu'opérateur <?php echo $operatorOf["name"] ?></a>		
								</td>
							</tr>
						<?php 
						}else{ ?>
							<tr>
								<td colspan='3' class="text-center" >
									Vous avez déjà inscrit <?php echo @$operatorOf["name"] ?> sur la liste des opérateurs
								</td>
							</tr>
							
						<?php }
					}else if($isFinanceur){ ?>
						<tr>
						 	<td colspan="3">
							<select id="operators-selections" path="links.operators" style="width:100%;" data-financeur="true" <?php echo $disabled; ?>>
								<option class=''  selected>Ajouter un opérateur à ce dossier</option>
								<?php 

								foreach ($parentForm["communityLinks"] as $v => $f) {
									if($f["type"]==Organization::COLLECTION 
										&& isset($f["roles"]) && in_array("Opérateur", $f["roles"])){
										$elt=Element::getElementById($v, $f["type"], null, array("name", "profilThumbImageUrl"));
										echo "<option class='optoper' value='".$v."' ".((isset($selectedOps) && $selectedOps==$v) ? "selected" : "").">".$elt["name"]."</option>";
									}
								} ?>
							</select>
							</td>
						</tr>
					<?php }
				}  ?>
			</tbody>
		</table>	
		<?php if($isFinanceur && !$selectedOperator){ 
			if(!@$answer["step"] || in_array($answer["step"], ["deal1","deal12"])){ ?> 
				<!-- <div class="col-xs-12 text-center margin-top-50 well">
					<span style="font-size: 22px;font-weight: 800;">Ce dossier n'a pas encore d'opérateur rattaché. En tant que financeurs ou administrateur de la DEAL, veuillez valider un opérateur afin de passer ce dossier à l'étape suivant d'instruction.<br/>
					Vous pouvez aussi sélectionner un opérateur de votre choix qui sera mieux placé pour répondre au dossier du demandeur.</span>
				</div> -->
				<div class="col-xs-12 text-center well">
						<span class="text-center" style="font-size: 22px;font-weight: 800;"> 
							<!-- Une fois votre dossier estimé rempli, vous pouvez le valider afin qu'il soit mis sous étude des opérateurs et des financeurs<br/><br/>  -->
							<a href="javascript:;" class="validateForSubmit btn btn-success" data-step="<?php echo (isset($selectedOps)) ? "deal12" : "deal1" ?>">
								<!-- Soumettre votre dossier -->
								Confirmer l'opérateur sélectionné
							</a>
						</span>
				</div>
			<?php }  
		}
		// ELSE On rentre dans le cas où c'est un propriétaire qui a soumis le dossier 
		// Il voit donc plusieurs états : possibilité de choisir son opérateur (deal12) ou de laisser libre.(deal1)
		// Il a aussi un bouton de soumission afin d'instruire le dossier et de le rendre visible pour les financeurs opérateurs et admin
	} else 
	{ ?>
			<?php 
				if(@$answer["step"]){
					// deal 1 => cas où le demandeur a soumis son dossier mais qu'il n'a pas choisi d'opé
					if($answer["step"]=="deal1"){ ?>
						<<div class="form-operator col-xs-12 no-padding">
						<label for="<?php echo $kunik ?>">
							<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
								Avez-vous une préférences sur le choix du maître d'ouvrage ?</h4>
						</label>
						<select id="operators-selections" path="links.operators" style="width:100%;" <?php echo $disabled; ?>>
							<?php if(isset($answer["links"]) && isset($answer["links"]["operators"])){
								foreach($answer["links"]["operators"] as $k => $v){
									$operator=Element::getElementById($k, Organization::COLLECTION, null, array("name", "profilThumbImageUrl"));
									echo "<option class='optoper' value='".$k."'>".$operator["name"]."</option>";
									$selectedOps=$k;
								}


							} ?> 
							<option value="<?php echo @$selectedOps ?>" data-action="unset" class='optoperunset' <?php echo (!isset($selectedOps)) ? "selected" : "" ?>>Pas de préférences sur le maitre d'oeuvre</option>
							<?php 

							foreach ($parentForm["communityLinks"] as $v => $f) {
								if($f["type"]==Organization::COLLECTION 
									&& isset($f["roles"]) && in_array("Opérateur", $f["roles"])){
									$elt=Element::getElementById($v, $f["type"], null, array("name", "profilThumbImageUrl"));

									//$thumb=$elt["thumbImg"]=(isset($elt["profilThumbImageUrl"])) ? Yii::app()->createUrl($elt["profilThumbImageUrl"]) :  Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl()."/images/thumb/default_organizations.jpg"; 
									echo "<option class='optoper' value='".$v."' ".((isset($selectedOps) && $selectedOps==$v) ? "selected" : "").">".$elt["name"]."</option>";
								}
							} ?>
						</select>
					</div>
					<div class="col-xs-12 text-center margin-top-50 well">
						<span class="text-center" style="font-size: 22px;font-weight: 800;"> 
							Une fois votre dossier estimé rempli, vous pouvez le valider afin qu'il soit mis sous étude des opérateurs et des financeurs<br/><br/> 
							<a href="javascript:;" class="validateForSubmit btn btn-success" data-step="<?php echo (isset($selectedOps)) ? "deal12" : "deal1" ?>">
								Soumettre votre dossier
							</a>
						</span>
					</div>
			<?php	}else{
					// Cas ou l'opérateur a été choisi par le demandeur et où le dossier avance dans les steps 
						foreach ($answer["links"]["operators"] as $oid => $state) {
							$opId=$oid;
							$operator=Element::getElementById($oid, Organization::COLLECTION, null, array("name","slug", "profilThumbImageUrl"));
							$thumbOp=(isset($operator["profilThumbImageUrl"])) ? Yii::app()->createUrl($operator["profilThumbImageUrl"]) :  Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl()."/images/thumb/default_organizations.png"; 
						} ?>
						<div class="col-xs-12 well margin-top-50">	
						<?php if($answer["step"]=="deal12"){ ?>
							<span style="font-size: 22px;font-weight: 800;">L'opérateur que vous avez sélectionné pour la prise en charge du dossier est </span>
							<?php }else{ ?>
								<?php $labelStep=array(
									"deal2"=> "en cours de construction par l'opérateur", 
									"deal3"=> "en cours d'attribution des financements", 
									"deal4"=>"est en phase d'échelonnage du planning", 
									"deal5"=>"finalisé. Vous pouvez réaliser le suivi des travaux"); ?>
								<span style="font-size: 22px;font-weight: 800;"> Votre dossier est <?php echo $labelStep[$answer["step"]] ?>.<br/><br> L'opérateur en charge du dossier est  </span>
						
						<?php } ?>			
						<a href="#page.type.organizations.id.<?php echo $opId ?>" class="lbh-preview-element"><img src="<?php echo $thumbOp ?>" width=40 height=40 style="margin-right: 5px;border-radius: 20px"/><?php echo $operator["name"] ?></a>
						</div>
						
				<?php }
				// ELse dossier pas 
				}else{ ?>

					<div class="form-operator col-xs-12 no-padding">
						<label for="<?php echo $kunik ?>">
							<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
								Avez-vous une préférences sur le choix du maître d'ouvrage ?</h4>
						</label>
						<select id="operators-selections" path="links.operators" style="width:100%;" <?php echo $disabled; ?>>
							<?php if(isset($answer["links"]) && isset($answer["links"]["operators"])){
								foreach($answer["links"]["operators"] as $k => $v){
									$operator=Element::getElementById($k, Organization::COLLECTION, null, array("name", "profilThumbImageUrl"));
									echo "<option class='optoper' value='".$k."'>".$operator["name"]."</option>";
									$selectedOps=$k;
								}


							} ?> 
							<option value="<?php echo @$selectedOps ?>" data-action="unset" class='optoperunset' <?php echo (!isset($selectedOps)) ? "selected" : "" ?>>Pas de préférences sur le maitre d'oeuvre</option>
							<?php 

							foreach ($parentForm["communityLinks"] as $v => $f) {
								if($f["type"]==Organization::COLLECTION 
									&& isset($f["roles"]) && in_array("Opérateur", $f["roles"])){
									$elt=Element::getElementById($v, $f["type"], null, array("name", "profilThumbImageUrl"));

									//$thumb=$elt["thumbImg"]=(isset($elt["profilThumbImageUrl"])) ? Yii::app()->createUrl($elt["profilThumbImageUrl"]) :  Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl()."/images/thumb/default_organizations.jpg"; 
									echo "<option class='optoper' value='".$v."' ".((isset($selectedOps) && $selectedOps==$v) ? "selected" : "").">".$elt["name"]."</option>";
								}
							} ?>
						</select>
					</div>
					<div class="col-xs-12 text-center margin-top-50 well">
						<span class="text-center" style="font-size: 22px;font-weight: 800;"> 
							Une fois votre dossier estimé rempli, vous pouvez le valider afin qu'il soit mis sous étude des opérateurs et des financeurs<br/><br/> 
							<a href="javascript:;" class="validateForSubmit btn btn-success" data-step="<?php echo (isset($selectedOps)) ? "deal12" : "deal1" ?>">
								Soumettre votre dossier
							</a>
						</span>
					</div>
			<?php } ?>
	
<?php } 
?>

<script type="text/javascript">

	$(document).ready(function() { 

	    mylog.log("render","modules/survey/views/tpls/forms/costum/deal/stepOperateur.php");
	    var today = new Date();
		today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();

		$('input[type=radio][name=multiRadiodeal149]').change(function() {
			if($(this).val() == "Petit travaux"){

				$('#operators-selections option[value="<?php echo ($splar != "")? $splar : "null" ; ?>"]').prop('selected', true).change();
				$( "#operators-selections" ).prop( "disabled", true );

			}else {
				$( "#operators-selections" ).prop( "disabled", false );
			}
		});
		
	    $('.validateOperator').off().on("click", function() {
	    	var oid = $(this).data("id");
	    	ctxTpl = {
				id : "<?php echo $answer['_id'] ?>",
				collection : "answers",
				path : "links.operators."+oid,
				value : {
					date : today, 
					user : userId
				}
			};
			ctxTplR = {
				id : "<?php echo $answer['_id'] ?>",
				collection : "answers",
				path : "<?php echo $answerPath ?>",
				value : {
					date : today, 
					user : userId
				}
			};

			$(this).fadeOut();
			dataHelper.path2Value( ctxTplR, function(params) { 
  	 			mylog.log("aaazf",ctxTpl);
			});
			dataHelper.path2Value( ctxTpl, function(params) { 
  	 	
				mylog.log("save step save",ctxTpl);
				validateForStep2();
			});
			
				
	    	
		});
		$("#operators-selections").off().on("change", function(){
			var oid = $(this).val();
			// var actionFromFinanceur=(notNull($(this).data("financeur")) && $(this).data("financeur")) ? true : false;
			var actionFromFinanceur= false;
			
			if(notNull(oid)){
				var valOp= 0;
				submitVal="deal2";
				
				$(".optoperunset").val(oid);
			
				if(notNull($(this).find(":selected").data("action"))){
					valOp=null;
					submitVal="deal1";
				}

				//$(".validateForSubmit").attr("data-step",submitVal);
		    	
		    	ctxTpl = {
					id : "<?php echo $answer['_id'] ?>",
					collection : "answers",
					path : "links.operators."+oid,
					value : {
						date : today, 
						user : userId
					}
				};

				//	if(notNull($(this).data("action"))
				//		ctxTpl.action=$(this).data("action");
					dataHelper.path2Value( ctxTpl, function(params) {
						if(actionFromFinanceur)
							reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
						else	
							toastr.success("Votre préférence opérateur a bien été prise en compte");
						//urlCtrl.loadByHash(location.hash);	
					});
					}
			});

	    $('.validateForSubmit').off().on("click", function() {
	    	ctxTpl = {
				id : "<?php echo $answer['_id'] ?>",
				collection : "answers",
				path : "step",
				value : $(this).data("step")
			};
			ctxTplRR = {
				id : "<?php echo $answer['_id'] ?>",
				collection : "answers",
				path : "answers.<?php echo $form["id"].".".$key ?>",
				value : {
					date : today, 
					user : userId
				}
			};
			dataHelper.path2Value( ctxTplRR, function(params) {});

			dataHelper.path2Value( ctxTpl, function(params) {
				reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>"); reloadInput("deal1check", "<?php echo (string)$form["_id"] ?>");
			});
		} );
		$('.intentOperator').off().on("click", function() {
	    	var oid = $(this).data("oid");
	    	ctxTpl = {
				id : "<?php echo $answer['_id'] ?>",
				collection : "answers",
				path : "links.operators."+oid,
				value : {
					date : today, 
					user : userId
				}
			};
	    	dataHelper.path2Value( ctxTpl, function(params) { 
	  	 		reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>"); reloadInput("deal1check", "<?php echo (string)$form["_id"] ?>");
	  	 	} );
		});
		$('.selectOperator').off().on("click", function() {
	    	var oid = $(this).data("oid");
	    	ctxTpl = {
				id : "<?php echo $answer['_id'] ?>",
				collection : "answers",
				path : "links.operators."+oid,
				value : {
					date : today, 
					user : userId
				}
			};
	    	dataHelper.path2Value( ctxTpl, function(params) { 
	  	 		/*ctxTpl = {
					id : "<?php echo $answer['_id'] ?>",
					collection : "answers",
					path : "step",
					value : "deal12"
				};
				dataHelper.path2Value( ctxTpl, function(params) {
					urlCtrl.loadByHash(location.hash);	
				});*/
				reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>"); reloadInput("deal1check", "<?php echo (string)$form["_id"] ?>");
	  	 	} );
		});


		
	    
	});
	function validateForStep2(){
		ctxTpl = {
	    		id    : "<?php echo (string)$answer['_id'] ?>",
	    		collection : "answers",
	    		path  : "step",
	    		value : "deal2" //pourrait etre un parametre element.costum.form.stepOperateur : deal3
	    	};
	    	mylog.log("save step save",ctxTpl);
  	 		// dataHelper.path2Value( ctxTpl, function(params) { 
  	 			//Send mail Validation
  	 			toastr.success("La validation de l'opérateur a été prise en compte.<br/>Un mail de notification est en cours d'envoie");
  	 			ajaxPost(
			        null,
			        baseUrl+"/survey/answer/sendmail/id/<?php echo (string)$answer['_id'] ?>",
			        {step:"<?php echo @$answer["step"] ?>"},
			        function(data){ 
			        	toastr.success("Un mail a été envoyé à toutes les parties prenantes afin de les notifier");
			      		reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>"); reloadInput("deal1check", "<?php echo (string)$form["_id"] ?>");  
			      		return false;    
			        }
			    );
	  	 	// } );
  	 //	} );
	}
	</script>


<?php
}
?>