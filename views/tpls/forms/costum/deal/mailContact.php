
<?php
$cssJS = array(
    '/plugins/jQuery-Knob/js/jquery.knob.js',
    '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
    //'/plugins/jQuery-Smart-Wizard/styles/smart_wizard.css',
  // SHOWDOWN
  '/plugins/showdown/showdown.min.js',
  // MARKDOWN
  '/plugins/to-markdown/to-markdown.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);
HtmlHelper::registerCssAndScriptsFiles(array( 
  '/js/answer.js',
  ), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );
  
$isFinanceur=false;
if(Authorisation::isInterfaceAdmin())
	$isFinanceur=true;
if(isset($this->costum["financorOf"])){
	$isFinanceur=true;
}

if ($isFinanceur) {

if($mode != "pdf" && ( !isset($inputsObj["canAccess"]) || Form::canAccess($inputsObj["canAccess"]) ) ) { 

	$inputsObj=$form["inputs"][$key]; 
	?>
	<div class="contactFormEmail col-xs-12 no-padding margin-bottom-20">
		<br/>
		<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo @$inputsObj["label"] ?></h4>
	<?php if(isset($inputsObj["info"])){ ?> 
		<span class="italic">
			<?php echo $inputsObj["info"] ?>
		</span>
	<?php } ?>
		<button class="btn btn-success contactMail" data-key="<?php echo $key ?>">
			<?php echo $inputsObj["buttonLabel"] ?>
		</button>
	</div>
	<!-- check on select answer + add default obj + add default content-->
	<?php 
	if(isset($inputsObj["formAdmin"])){

	}else if (isset($inputsObj["selectContact"])){
		$inputsObj["selectOptByGroup"]=array();
		foreach($inputsObj["selectContact"] as $k => $v){
			$inputsObj["selectOptByGroup"][$k]=array();
			if(isset($answer["links"][$k])){
				$typeElt=($k=="answered") ? Person::COLLECTION : Organization::COLLECTION;
				foreach($answer["links"][$k] as $e => $st){
					$idCol=($k=="answered") ? $st : $e;
					$elt=Element::getElementById($idCol, $typeElt, null, array("name","slug", "profilThumbImageUrl"));
					$elt["collection"]=$typeElt;
					$inputsObj["selectOptByGroup"][$k][$idCol]=$elt;		
				}
			}
		}
		//$inputsObj["selectOptByGroup"]=$selectOptByGroup;
	} ?>
	<script type="text/javascript">
		//if(typeof answerMailConfig =="undefined") var answerMailConfig={};
		//var answerObject.mailConfig.inputs[<?php echo $key ?>]=new Object;
		answerObject.mailConfig.inputs["<?php echo $key; ?>"]=<?php echo json_encode($inputsObj); ?>;
		var answerValues=<?php echo json_encode($answer); ?>;
		jQuery(document).ready(function() {
			$(".contactMail").off().on("click", function(){
				inputsMailConfig=answerObject.mailConfig.inputs[$(this).data("key")];
				bootbox.dialog({
			        onEscape: function() {},
			        message: '<div id="send-mail-answer" class="row">  ' +
			            '<div class="col-xs-12"> ' +
			            	answerObject.mailConfig.setContactList(inputsMailConfig)+
			            	//'<input type="text" id="contact-email" class="col-xs-12" value="'+aObj.mailTo.initMail(elt)+'"/>'+
			            '</div>'+
			            '<div class="col-xs-12"> ' +
			            	'<span class="col-xs-12 bold no-padding">Object</span> ' +
			            	answerObject.mailConfig.setObject(inputsMailConfig)+
			            '</div>'+
			            '<div class="col-xs-12"> ' +
			            	'<span class="col-xs-12 bold no-padding">Message</span> ' +
			            	'<textarea id="message-email" class="col-xs-12 text-dark" style="min-height:250px;" placeholder="Ecrivez votre message">'+answerObject.mailConfig.defaultMessage(inputsMailConfig)+'</textarea>'+
			            '</div>'+
			        '</div>',
			        buttons: {
			            success: {
			                label: "Ok",
			                className: "btn-primary",
			                callback: function () {
			                	errorMsg={
			                		msg : "Veuillez écrire un message",
			                		object : "Veuillez renseigner un objet",
			                		contact : "Veuillez saisir un destinataire"
			                	}
			                	var msg="";

			                	if(notEmpty($("#send-mail-answer #message-email").val()))
				                	msg="<span style='white-space: pre-line;'>"+$("#send-mail-answer #message-email").val()+"<br/></span>";
									//var email 
								else {
									toastr.error(errorMsg.msg);return false;
								}
								
								if(!notEmpty($("#send-mail-answer #object-email").val())){
									toastr.error(errorMsg.object);return false;
								}
								
								var params={
										tpl : "default",
										tplObject : $("#send-mail-answer #object-email").val(),
										html: msg
									};
								if($("#send-mail-answer #contact-email").length > 0)
									params.tplMail=$("#send-mail-answer #contact-email").val();
								if($("#send-mail-answer .checkboxAnswerContact").length > 0){
									listContact={};
									$("#send-mail-answer .checkboxAnswerContact input[type='checkbox']:checked").each(function(){
										listContact[$(this).data("value")] = { type : $(this).data("collection")};
									});
									params.listContact=listContact;
								}
								if((typeof params.listContact != "undefined" && Object.keys(params.listContact).length > 0) || (typeof params.tplMail != "undefined" && notEmpty(params.tplMail))){
									ajaxPost(
								        null,
								        baseUrl+"/survey/answer/sendmail/tpl/default/id/"+answerObj._id.$id,
								        params,
								        function(data){ 
								          	toastr.success("Le mail a été envoyé avec succès");
										}
								  	);
								 }else{ toastr.error(errorMsg.contact);return false; }
			                	//aObj.mailTo.sendMail(aObj, elt, type, id);
			                }
			            },
			            cancel: {
			            	label: trad["cancel"],
			            	className: "btn-secondary",
			            	callback: function() {}
			            }
			        }
			    });
			});
		});
	</script>
<?php } } ?>