<div class="form-group">

    <?php if(!empty($label)){ ?>
            <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label ?></h4>
    <?php } ?>
    <?php if(!empty($info)){ ?>
            <small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
    <?php } ?>


    <?php
    $community=$this->costum["communityLinks"]["members"];
        foreach ($community as $id => $value){
            if(isset($value["roles"]) && isset($value["type"]) && $value["type"]=="organizations" && in_array("Financeur",$value["roles"])){
                $el=Element::getElementById($id,"organizations");
                //echo $el["name"];
                $value = (isset($answer["answers"][$form["id"]][$key][$id])) ? $answer["answers"][$form["id"]][$key][$id] : "";

                if($mode == "r" || $mode == "pdf"){ ?>
                    <div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
                        <label for="<?php echo $kunik ?>"><h5 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $el["name"] ?></h5></label><br/>
                        <?php echo $value; ?>
                    </div>
                <?php 
                }else{
                ?>
                    <div class="form-group">
                        <label for="<?php echo $key ?>"><h5 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $el["name"] ?></h5></label>
                        <br/><input type="text" class="form-control <?php echo $key ?>"
                        data-id="<?php echo $id ?>" data-path="<?php echo "answers.".$form["id"].".".$key.".".$id ?>" aria-describedby="<?php echo $key ?>Help" placeholder="<?php echo (isset($placeholder)) ? $placeholder : '' ?>" data-form='<?php echo $form["id"] ?>' value= "<?php echo $value ?>" >
                        
                    </div>
                <?php } 
            }

        }
   // var_dump("<pre>",$this->costum["communityLinks"]["members"],"</pre>");exit;

 ?>   


    
    
    
</div>

<script type="text/javascript">

    
    var answerId = <?php echo json_encode((String)$answer['_id']); ?>;

    var key = <?php echo json_encode($key) ?>;


        $( document ).ready(function() {
            
            $('.' + key ).change(function(){
                tplCtx={};
                tplCtx.id=answerId;
                tplCtx.collection="answers";
                tplCtx.path=$(this).data("path");
                tplCtx.value=$(this).val();
                dataHelper.path2Value( tplCtx, function(params) {
                    toastr.success("Demande de financement enregistrée");
                });
            });

        });

</script>