<?php if($answer){
    ?>
    <div class="form-group">
        

            <?php
            $editBtnL = ($canEdit and $mode != "r" and $mode != "pdf") ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";
        $editParamsBtn = ($canEditForm and $mode != "r" || $mode != "pdf") ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

             
             
            $ab = [];


    $paramsData = [
                "qualite" => [
                    "demandeur" => "Demandeur(se)",
                    "parent1" => "Parent 1",
                    "parent2" => "Parent 2",
                    "fils" => "Fils",
                    "fille" => "Fille",
                    "ascendant" => "Ascendant(e)",
                    "descendant" => "Descendant(e)",                    
                    "conjoint" => "Conjoint(e)"
                ],
                "nature" => [
                    "salaire" => "Salaire",
                    "chomage" => "Allocation chômage",
                    "rsa" => "RSA",
                    "aah" => "AAH",
                    "api" => "API",
                    "min" => "Minimum vieillesse",
                    "retraite" => "Retraite",
                    "sansrevenu" => "Sans revenu"
                ],
            ];
            if( isset($parentForm["params"][$kunik]["frequenceOb"]) )
                $paramsData["frequenceOb"] =  $parentForm["params"][$kunik]["frequenceOb"];
            if( isset($parentForm["params"][$kunik]["statut"]) )
                $paramsData["statut"] =  $parentForm["params"][$kunik]["statut"];

            $properties = [
                 "nom" => [
                    "label" => "Nom et prénom de l'ensemble des personnes devant occuper le logement",
                    "placeholder" => "Nom et prénom",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                 "qualite" => [
                    "label" => "Qualité",
                    "placeholder" => "Qualité",
                    "inputType" => "select",
                    "noOrder" => true,
                    "options" => $paramsData["qualite"],
                    "rules" => [ "required" => true ]
                ],
                "revenu" => [
                    "label" => "Revenus nets imposables n-2 (ou n-1 si baisse de revenus l'année dernière)",
                    "placeholder" => "Revenus nets imposables n-2 (ou n-1)",
                    "inputType" => "number",
                    "rules" => [ "required" => true ]
                ],
                "nature" => [
                    "label" => "Nature des revenus de toutes les personnes devant occuper le logement",
                    "placeholder" => "Nature des revenus ",
                    "inputType" => "select",
                     "noOrder" => true,
                    "options" => $paramsData["nature"],
                    "rules" => [ "required" => true ]
                ]
            ];

            $propertiesParams = [
                "labels"=>[],
                "placeholders"=>[],
            ];

            foreach ($properties as $k => $v) {
                if( isset($v["label"])){
                    if( isset($parentForm["params"][$kunik]["labels"][$k]) )
                        $properties[$k]["label"] = $parentForm["params"][$kunik]["labels"][$k];
                    $propertiesParams["labels"][$k] = $properties[$k]["label"];
                }

                if( isset($v["placeholder"])){
                    if(isset($parentForm["params"][$kunik]["placeholders"][$k]) )
                       $properties[$k]["placeholder"] = $parentForm["params"][$kunik]["placeholders"][$k];
                    $propertiesParams["placeholders"][$k] = $properties[$k]["placeholder"];
                }
            }

            ?>

            <?php if(  $mode != "pdf" ){ ?>

            <div>
                <label>
                    <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
                                <?php echo $info ?>
                </label>
            </div>


            <?php if(sizeof($answers) > 0 ){?>
           <div style="border: 2px solid darkgrey;padding-top: 20px;border-radius: 20px; overflow-x: auto;">

            <table class="table  table-hover  directoryTable" id="<?php echo $kunik?>" >
             <thead>
            <?php

            if( count($answers)>0 ){ ?>
                <tr>
                    </th>
                    <?php

                    foreach ($properties as $i => $inp) {
                        if(isset($inp["placeholder"])){
                        echo "<th>".$inp["placeholder"]."</th>";
                        }
                    
                    } ?>
                    <?php if(  $mode != "r" ){ ?>
                            <th></th>
                        <?php } ?>
                </tr>
                <tr></tr>
            <?php } ?>
            </thead>
            <tbody class="directoryLines">
            <?php
            $ct = 0;

            if(isset($answers)){
                foreach ($answers as $q => $a) {

                    echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
                    $contr = 0;
                        foreach ($properties as $i => $inp) {
                         if($i == "location"){
                        } else {
                        echo "<td>";
                        if(isset($a[$i])) {
                            if(is_array($a[$i])) {
                                
                                     echo implode(",", $a[$i]);
                                  
                                
                            } 
                             else {
                                    

                                    if($i == "revenu"){
                                        array_push($ab, $a["revenu"]);
                                    }
                                        echo $a[$i];
                                    
                            }  
                            
                        }
                        $contr++;
                        echo "</td>";
                    }
                }
                    ?>
                      <?php  if( $mode != "r"){?>
                            <td style="white-space: normal;">
                                <?php
                                echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
                                    "canEdit"=>($canEdit),
                                    "id" => $answer["_id"],
                                    "collection" => Form::ANSWER_COLLECTION,
                                    "q" => $q,
                                    "path" => $answerPath.$q,
                                    "keyTpl"=>$kunik
                                ] ); ?>

                                <a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
                            </td>
                            <?php } ?>
                    <?php
                    $ct++;
                    echo "</tr>";
                }
            }

            ?>
            </tbody>
        </table>
    </div>
        <?php }?>

        <?php 
        $totalrevenu = 0;

        foreach ($ab as $ke => $valu) {

        if (ctype_digit($valu)) { 
            $totalrevenu += (int)$valu ;
        }
        } 
        ?>

        <div>  <label> <span style="color:#79B51C" > Total des revenus :</span> <?php echo $totalrevenu; ?> €</label></div>

        <?php
            } else {
            ?>
            <div>
                <label>
                    <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
                                <?php echo $info ?>
                </label>
            </div>


             <table class="table table-bordered table-hover  directoryTable" style="font-size: 14px !important;" id="<?php echo $kunik?>">
             <thead>
            <?php

            if( count($answers)>0 ){ ?>
                <tr>
                    <?php

                    foreach ($properties as $i => $inp) {
                        if(isset($inp["placeholder"])){
                        echo "<th>".$inp["placeholder"]."</th>";
                        }
                    
                    } ?>
                </tr>
            <?php } ?>
            </thead>
            <tbody class="directoryLines">
            <?php
            $ct = 0;

            if(isset($answers)){
                foreach ($answers as $q => $a) {

                    echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
                    $contr = 0;
                        foreach ($properties as $i => $inp) {
                         if($i == "location"){
                        } else {
                        echo "<td>";
                        if(isset($a[$i])) {
                            if(is_array($a[$i])) {
                                
                                     echo implode(",", $a[$i]);
                                  
                                
                            } 
                             else {
                                    if($i == "nature" && isset($paramsData[$i][$a[$i]])){

                                        echo $paramsData[$i][$a[$i]];
                                    } else if($i == "qualite" && isset($paramsData[$i][$a[$i]])){

                                        echo $paramsData[$i][$a[$i]];
                                    } else {
                                    echo $a[$i];
                                    }

                                    if($i == "revenu"){
                                        array_push($ab, $a["revenu"]);
                                    }
                            }  
                            
                        }
                        $contr++;
                        echo "</td>";
                    }
                }
                    echo "</tr>";
                }
            }

            ?>
            </tbody>
        </table>

        <?php 
        $totalrevenu = 0;
        foreach ($ab as $ke => $valu) {
        if (ctype_digit($valu)) { 
            $totalrevenu =   $totalrevenu + (int)$valu ;
        }
        } 
        ?>

        <div>  <label> <span style="color:#79B51C" > Total des révenu :</span> <?php echo $totalrevenu; ?>£ </label></div>
          
           

    <?php } ?>


    </div>
    <?php  if( $mode != "pdf"){?>
    <script type="text/javascript">

        var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsProperty = {
            labels : <?php echo json_encode( $propertiesParams["labels"] ); ?>,
            placeholders : <?php echo json_encode( $propertiesParams["placeholders"] ); ?>
        };

        $(document).ready(function() {

            sectionDyf.<?php echo $kunik ?> = {
                "jsonSchema" : {
                    "title" : "Revenus du foyer",
                    "icon" : "fa-money",
                    "text" : "",
                    "properties" : <?php echo json_encode( $properties ); ?>,
                    save : function () {
                        var today = new Date();
                        tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
                        $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                        });

                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                dyFObj.closeForm();
                                reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
                            } );
                        }

                    }
                }
            };

            sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema" : {
                    "title" : "<?php echo $kunik ?> config",
                    "description" : "Liste de question possible",
                    "icon" : "fa-cog",
                    "properties" : {
                        labels : {
                            inputType : "properties",
                            labelKey : "Clef",
                            labelValue : "Libellé du champs",
                            label : "Modifier les libélés des Questions",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsProperty.labels
                        },
                        placeholders : {
                            inputType : "properties",
                            labelKey : "Clef",
                            labelValue : "Texte dans le champs",
                            label : "Modifier les textes à l'interieur du champs de saisie",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsProperty.placeholders
                        },
                        labels : {
                            inputType : "properties",
                            labelKey : "Clef",
                            labelValue : "Libellé du champs",
                            label : "Modifier les libélés des Questions",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsProperty.labels
                        },
                        placeholders : {
                            inputType : "properties",
                            labelKey : "Clef",
                            labelValue : "Texte dans le champs",
                            label : "Modifier les textes à l'interieur du champs de saisie",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsProperty.placeholders
                        }
                    },
                    save : function () {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                            if(val.inputType == "properties")
                                tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                            else if(val.inputType == "array")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else
                                tplCtx.value[k] = $("#"+k).val();
                            mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                        });
                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                dyFObj.closeForm();
                                reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
                            } );
                        }

                    }
                }
            };


            mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

            //adds a line into answer
            $(".add<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
            });

            $(".edit<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
            });

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                //if no params config on the element.costum.form.params.<?php echo $kunik ?>
                //then we load default values available in forms.inputs.<?php echo $kunik ?>xxx.params
                //mylog.log(".editParams",sectionDyf.<?php echo $kunik ?>Params,calData);
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            });


        });
    </script>
<?php } }?>

