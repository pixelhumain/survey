<?php if($answer){
    ?>
    <div class="form-group">
        
	<?php
        $editBtnL = ($canEdit and $mode != "r" and $mode != "pdf") ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";
        $editParamsBtn = ($canEditForm and $mode != "r" || $mode != "pdf") ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

        $paramsData = [
        	"nature" => [
				"chargefonciere" => "Charge foncière",
				"batiment" => "Bâtiment",
				"honoraires" => "Honoraires",
				"fraisoperationnels" => "Frais opérationnels",
				"fraisdedossier" => "Frais de dossier"
			],
			
			"chargefonciere" => [
				"coutdacquisition" => "Coût d’acquisition",
			],

			"batiment" => [
				"coutdacquisition" => "Coût d’acquisition",
				"deposecharpente" => "Dépose charpente",
				"demolitionbeton" => "Démolition béton",
				"exterieurs" => "Extérieurs",
				"fondations" => "Fondations",
				"structurebetonarme" => "Structure béton armé",
				"maconnerie" => "Maçonnerie",
				"enduits" => "Enduits",
				"escaliers" => "Escaliers",
				"divers" => "Divers",
				"charpenteacier" => "Charpente acier",
				"couverture" => "Couverture",
				"peinturesurbois" => "Peinture sur bois",
				"peintureexterieure" => "Peinture Extérieure",
				"peintureinterieure" => "Peinture Intérieure",
				"revetementmural" => "Revêtement mural",
				"carrelage" => "Carrelage",
				"depose" => "Dépose",
				"raccordementdistribution" => "Raccordement - distribution",
				"appareilssanitaires" => "Appareils sanitaires",
				"amenagementpmr" => "Aménagement PMR",

			],

			"honoraires" => [
				"maitrisedoeuvrebatiment" => "Maîtrise d’Oeuvre Bâtiment",
				"remunerationmaitredouvrage" => "Rémunération Maître d’Ouvrage",
				"assurancesdommagesouvrage" => "Assurances Dommages Ouvrage"
			],

			"fraisoperationnels" => [
				"fraisgeneraux" => "Frais généraux",
				"fraisfinanciers" => "Frais financiers",
				"participationlcrexterne" => "Participation LCR externe",
				"fraisdecommercialisation" => "Frais de commercialisation"

			],

			"fraisdedossier" => [
				"fraisdossierdepartement" => "Frais dossier Département",
				"fraisdossierregion" => "Frais dossier Région",
				"fraisdossieretat" => "Frais dossier Etat",
				"fraisdossiercgss" => "Frais dossier CGSS"
			],
        ];

        $tfrais = $paramsData["chargefonciere"] + $paramsData["batiment"] + $paramsData["honoraires"] + $paramsData["fraisoperationnels"] + $paramsData["fraisdedossier"];

        $properties = [
                "nature" => [
                    "label" => "Nature",
                    "inputType" => "select",
		            "options" => $paramsData["nature"],
		            "rules" => [ "required" => true ]
                ],
                "Tfrais" => [
                    "label" => "Types de frais",
                    "inputType" => "select",
		            "options" => $tfrais,
		            "noOrder" => false,
		            "rules" => [ "required" => true ]
                ],
                "poste" => [
		            "inputType" => "text",
		            "label" => "Poste de dépense",
		            "placeholder" => "Poste de dépense",
		            "rules" => [ "required" => true  ]
		        ],
		        "montant" => [
		            "inputType" => "text",
		            "label" => "Montant",
		            "placeholder" => "Montant",
		            "rules" => [ "required" => true, "number" => true  ]
		        ]
            ];

        $propertiesParams = [
                "labels"=>[],
                "placeholders"=>[],
        ];

        foreach ($properties as $k => $v) {
            if( isset($v["label"])){
                    if( isset($parentForm["params"][$kunik]["labels"][$k]) )
                        $properties[$k]["label"] = $parentForm["params"][$kunik]["labels"][$k];
                    $propertiesParams["labels"][$k] = $properties[$k]["label"];
            }

            if( isset($v["placeholder"])){
                    if(isset($parentForm["params"][$kunik]["placeholders"][$k]) )
                       $properties[$k]["placeholder"] = $parentForm["params"][$kunik]["placeholders"][$k];
                    $propertiesParams["placeholders"][$k] = $properties[$k]["placeholder"];
            }
        }

        $groupdepense = array();
        $groupmontant = array();
        $total = 0;

        if (isset($answers) && sizeof($answers) > 0) {
        	foreach ($answers as $idelement => $depenseelement) {
                $depenseelement["q"] = $idelement;
        		$groupdepense[$depenseelement["nature"]][] = $depenseelement;
        		if (!array_key_exists($depenseelement["nature"], $groupmontant)) {
        			$groupmontant[$depenseelement["nature"]] = (int)$depenseelement["montant"];
        		} else {
        			$groupmontant[$depenseelement["nature"]] = (int)$depenseelement["montant"] + $groupmontant[$depenseelement["nature"]];
        		}
        		$total = $total + (int)$depenseelement["montant"];
        	}
        }

        ?>

            <div><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
                    <?php echo $info ?></div>
            
            <div style="border: 2px solid darkgrey;padding-top: 20px;border-radius: 20px; overflow-x: auto;">

    <?php
        if( count($answers)>0 ){ 
    ?>
            <table class="table  table-hover  directoryTable" id="<?php echo $kunik?>" >
            <thead>
            <tr style="color: #8898aa;background-color: #f6f9fc;">
             
            </tr>

                <tr style="color: #8898aa;background-color: #f6f9fc;">
                    
                    <?php

                    foreach ($properties as $i => $inp) {
                    	// if ($i != "nature") {
                        	echo "<th>".$inp["label"]."</th>";
                    	// }
                    } ?>
                    <?php if(  $mode != "r" ){ ?>
                            <th></th>
                        <?php } ?>
                </tr>
            <?php } ?>
            </thead>
            <tbody class="directoryLines">
            <?php
            $ct = 0;

            if(isset($answers)){
                foreach ($groupdepense as $qg => $ag) {
                	echo "<tr ><th colspan='3'>".$paramsData["nature"][$qg]."</th><th>".$groupmontant[$qg]."</th></tr>";
                	
                	foreach ($ag as $q => $a) {

                    echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
                    
                    foreach ($properties as $i => $inp) {
                    	if ($i != "nature") {
                    		echo "<td>";
	                        if(isset($a[$i])) {
	                            if($i == "Tfrais"){
	                                echo $tfrais[$a[$i]];
	                        	}
	                        	else {
									echo $a[$i];
	                            }
	                        }
	                        echo "</td>";
	                    } else {
	                    	echo "<td></td>";
	                    }
                    }
                        
         			if( $mode != "r"){ ?>
                        <td style="white-space: normal;">
                        		<div class="dropdown" >
								<button class="btn btn-link dropdown-toggle" type="button" id="action<?php echo $kunik.$q ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-ellipsis-v"></i>
								</button>
								<ul class="dropdown-menu" aria-labelledby="action<?php echo $kunik.$q ?>" style="z-index: 20">
								<li style="width: fit-content;"><?php
                                echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
                                    "canEdit"=>($canEdit),
                                    "id" => $answer["_id"],
                                    "collection" => Form::ANSWER_COLLECTION,
                                    "q" => $a["q"],
                                    "path" => $answerPath.$a["q"],
                                    "keyTpl"=>$kunik
                                ] ); ?>

                                <a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a></li>
								</ul>
								</div>
                        </td>
                    <?php 
                	}
                    echo "</tr>";
                }
            	}
            	?>
                <tr style="color: #8898aa;background-color: #f6f9fc;">
                	<th colspan="3">
                		Total :
                	</th>

                	<th style="color: red">
                		<?php echo $total ?>
                	</th>

                </tr>
                <?php
            }

            ?>
            </tbody>
        </table>
    <?php

            } ?>
        <div>
            
    </div>
    <?php  if( $mode != "pdf"){?>
    <script type="text/javascript">

        var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsProperty = {
            labels : <?php echo json_encode( $propertiesParams["labels"] ); ?>,
            placeholders : <?php echo json_encode( $propertiesParams["placeholders"] ); ?>
        };


        $(document).ready(function() {

            sectionDyf.<?php echo $kunik ?> = {
                "jsonSchema" : {
                    "title" : "Ajouter dépense",
                    "icon" : "fa-globe",
                    "text" : "",
                    "properties" : <?php echo json_encode( $properties ); ?>,
                    onLoads : {
	                    onload : function () {
	                    	$('#nature').on('change', function() {
	                    		var valnature = $(this).val();
								$("#Tfrais > option").each(function() {
								   if (this.value in sectionDyf.<?php echo $kunik ?>ParamsData[valnature]) {
								   		if(this.value!=""){
								   			$("#Tfrais option[value=" + this.value + "]").show();
								   		}
								   } else {
								   		if(this.value!=""){
								   			$("#Tfrais option[value=" + this.value + "]").hide();
								   		}
								   }
								});
							});
	                    }
                	},
                    save : function () {
                        var today = new Date();
                        tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
                        $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                        });

                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                dyFObj.closeForm();
                                ajaxPost("", baseUrl + "/co2/aap/commonaap/action/notifyAddDepenseLine", {
                                    answerId:tplCtx.id,
                                    depense: tplCtx.value.poste +": "+  tplCtx.value.price + " Euro"
                                });
                                reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
                            } );
                        }

                    }
                }
            };

            sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema" : {
                    "title" : "<?php echo $kunik ?> config",
                    "description" : "Liste de question possible",
                    "icon" : "fa-cog",
                    "properties" : {
                    },
                    save : function () {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                            if(val.inputType == "properties")
                                tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                            else if(val.inputType == "array")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else
                                tplCtx.value[k] = $("#"+k).val();
                            mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                        });
                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                dyFObj.closeForm();
                                reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
                            } );
                        }

                    }
                }
            };


            mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

            //adds a line into answer
            $(".add<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
            });

            $(".edit<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
            });

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                //if no params config on the element.costum.form.params.<?php echo $kunik ?>
                //then we load default values available in forms.inputs.<?php echo $kunik ?>xxx.params
                //mylog.log(".editParams",sectionDyf.<?php echo $kunik ?>Params,calData);
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            });


        });
    </script>
<?php 
} ?>