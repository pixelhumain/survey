<?php if($answer){

    $validateKunik = "";
    foreach ($form["inputs"] as $fip => $fiv) {
        if ($fiv["type"] == "tpls.forms.hidden") {
           $validateKunik = $fip; 
        }
    }

    ?>
    <div class="form-group">

    <style type="text/css">
        .item__percentage {
            float: left;
            /*margin-left: 20px;*/
            transition: transform 0.3s;
            font-size: 11px;
            background-color: #FFDAD9;
            padding: 3px;
            border-radius: 3px;
            /*width: 32px;*/
            text-align: center;
            color: #FF5049;
            /*transform: translateX(-20px);*/
            box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.1);
        }


    </style>
        
	<?php
        $editBtnL = ($canEdit and $mode != "r" and $mode != "pdf") ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";
        $editParamsBtn = ($canEditForm and $mode != "r" || $mode != "pdf") ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

        $paramsData = [
        	"naturerecette" => [
				"subvention" => "Subvention",
				"apport" => "Apport"
			],
            "subvention" =>  [
                "etat" => "Etat",
                "region" => "Région",
                "departement" => "Département",
                "cgss" => "CGSS"
            ],
            "apport" => [
                "personnel" => "Personnel",
                "credit" => "Crédit"

            ]
        ];

        $properties = [
                "naturerecette" => [
                    "label" => "Nature des recettes",
                    "inputType" => "select",
		            "options" => $paramsData["naturerecette"],
		            "rules" => [ "required" => true ]
                ],
		        "etat" => [
		            "inputType" => "text",
		            "label" => "Etat",
		            "placeholder" => "Etat",
		            "rules" => [ "required" => false, "number" => true  ]
		        ],
                "region" => [
                    "inputType" => "text",
                    "label" => "Région",
                    "placeholder" => "Région",
                    "rules" => [ "required" => false, "number" => true  ]
                ],
                "departement" => [
                    "inputType" => "text",
                    "label" => "Département",
                    "placeholder" => "Département",
                    "rules" => [ "required" => false, "number" => true  ]
                ],
                "cgss" => [
                    "inputType" => "text",
                    "label" => "CGSS",
                    "placeholder" => "CGSS",
                    "rules" => [ "required" => false, "number" => true  ]
                ],
                "personnel" => [
                    "inputType" => "text",
                    "label" => "Personnel",
                    "placeholder" => "Personnel",
                    "rules" => [ "required" => false, "number" => true  ]
                ],
                "credit" => [
                    "inputType" => "text",
                    "label" => "Crédit",
                    "placeholder" => "Crédit",
                    "rules" => [ "required" => false, "number" => true  ]
                ]
                // "file" => [
                //     "label" => "arrête préfectoral d'attribution de subvention",
                //     "placeholder" => "arrête préfectoral d'attribution de subvention",
                //     "inputType" => "uploader",
                //     "rules" => [ "required" => false ],
                //     "docType" => "file"

                // ]
            ];

        $propertiesParams = [
                "labels"=>[],
                "placeholders"=>[],
        ];

        foreach ($properties as $k => $v) {
            if( isset($v["label"])){
                    if( isset($parentForm["params"][$kunik]["labels"][$k]) )
                        $properties[$k]["label"] = $parentForm["params"][$kunik]["labels"][$k];
                    $propertiesParams["labels"][$k] = $properties[$k]["label"];
            }

            if( isset($v["placeholder"])){
                    if(isset($parentForm["params"][$kunik]["placeholders"][$k]) )
                       $properties[$k]["placeholder"] = $parentForm["params"][$kunik]["placeholders"][$k];
                    $propertiesParams["placeholders"][$k] = $properties[$k]["placeholder"];
            }
        }

        $groupfinancement = array();
        $groupmontant = array();
        $total = 0;

        $totaldepense = 0;

        if (isset($answer["answers"]["deal3"]["deal32"]) && is_numeric($answer["answers"]["deal3"]["deal32"])) {
                $totaldepense = (int)$answer["answers"]["deal3"]["deal32"];
        }

        ?>

            <div><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
                    <?php echo $info ?></div>
            
            <div style="border: 2px solid darkgrey;padding-top: 20px;border-radius: 20px; overflow-x: auto;">

            <table class="table  table-hover  directoryTable" id="<?php echo $kunik?>" >
            <thead>

            <tr style="color: #8898aa;background-color: #f6f9fc;">
                <th colspan="2">Dépenses prévisionnelles</th> <th style='text-align: right;'> <?php echo $totaldepense; ?></th><th></th>
            </tr>

    <?php
        if( count($answers)>0 ){ 
    ?>

                <tr style="color: #8898aa;background-color: #f6f9fc;">
                     <th colspan="4">
                         Recettes prévisionnelles
                     </th>
                </tr>

            </thead>
            <tbody class="directoryLines">
            <?php
            $ct = 0;

            if(isset($answers)){
                foreach ($answers as $q => $a) {
                    ${"value".$q} = 0;
                    foreach ($a as $ka => $va) {
                        if ($va != '' and $ka != "naturerecette" and $ka != "date") {
                             ${"value".$q} =  ${"value".$q} + (int)$va;

                        }
                    }
                    $total = $total + ${"value".$q};

                    ${"per".$q} = ($totaldepense != 0) ? round((100 * ${"value".$q})/$totaldepense) : 0 ;

                    echo "<tr ><th colspan='2'>".$paramsData["naturerecette"][$a["naturerecette"]];

                    ?>
                                <div class="dropdown" >
                                <button class="btn btn-link dropdown-toggle" type="button" id="action<?php echo $kunik.$q ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-ellipsis-v"></i>
                                </button>

                                <button class="btn btn-link" type="button" id="action<?php echo $kunik.$q ?>" data-tgle="<?php echo $kunik.$q ?>">
                                    details
                                </button>

                                <ul class="dropdown-menu" aria-labelledby="action<?php echo $kunik.$q ?>" style="z-index: 20">
                                <li class=""><?php
                                echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
                                    "canEdit"=>($canEdit),
                                    "id" => $answer["_id"],
                                    "collection" => Form::ANSWER_COLLECTION,
                                    "q" => $q,
                                    "path" => $answerPath.$q,
                                    "keyTpl"=>$kunik
                                ] ); ?>

                                <a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a></li>
                                </ul>
                                </div>
                    <?php
                    echo "</th><th style='text-align: right; '>".${"value".$q}."</th><th><div class='item__percentage'>".${"per".$q}."%</div></th></tr>";

                    foreach ($a as $ka => $va) {
                        if ($va != '' and $ka != "naturerecette" and $ka != "date") {
                             echo "<tr class=".$kunik.$q.">";
                                echo "<td></td><td>".$paramsData[$a["naturerecette"]][$ka]."</td><td style='text-align: right;'>".$va."</td><td></td>";
                             echo "</tr>";
                        }
                    }
                    echo "<tr class=".$kunik.$q."><td>";
                    $initAnswerFiles=Document::getListDocumentsWhere(array(
                            "id"=>(string)$answer["_id"], 
                            "type"=>'answers',
                            "subKey"=>$answerPath.$q), "file");

                    // $initAnswerFiles = Document::getLastImageByKey((string)$answer["_id"],"answers","presentation");

                    
                            if(!empty($initAnswerFiles)){
                                $answers[$q]["files"] = $initAnswerFiles;
                                foreach ($initAnswerFiles as $flk => $flv) {
                                    ?>
                                    <a href="<?php echo $flv["docPath"] ?>" style ="color: red">arrête préfectoral d'attribution de subvention</a>
                            <?php          
                           }
                       }
                        
                   
                }
                 echo "</td> </tr>";
                $per = ($totaldepense != 0  && ($totaldepense > $total) ) ? round((100 * $total)/$totaldepense) : 0 ;

                ?>
                <tr style="color: #8898aa;background-color: #f6f9fc;">
                    <th colspan="2">
                        TOTAL des recettes prévisionnelles :
                    </th>

                    <th style="color: #79b521; text-align: right;">
                        <?php echo $total ?> 
                    </th>
                    <th><div class="item__percentage"> <?php echo $per ?> %</div></th>

                </tr>

                <?php
            }
}
            ?>
            <tr style="color: #8898aa;background-color: #f6f9fc;">
                     <th colspan="2">
                         Reste
                     </th>
                     <th style="color: red; text-align: right;">
                         <?php echo $totaldepense - $total ?>

                     </th>
                     <th>
                     </th>
            </tr>
            </tbody>
        </table>

        <div>

            
    </div>
    <?php
    if($validateKunik != "" && !isset($answer["answers"][$form["id"]][$validateKunik."hidden"]) && $total > 0 && $totaldepense - $total <= 0 && isset($answer["step"]) && $answer["step"] != "deal5"){
    ?>
    <script type="text/javascript">
        $(document).ready(function(){
            $.when(validateFinancement()).then(
              reloadInput("<?php echo $validateKunik ?>", "<?php echo (string)$form["_id"] ?>")
          );
        });   
    </script>
    <?php
    }

    ?>
    <?php  if( $mode != "pdf"){?>
    <script type="text/javascript">

        var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsProperty = {
            labels : <?php echo json_encode( $propertiesParams["labels"] ); ?>,
            placeholders : <?php echo json_encode( $propertiesParams["placeholders"] ); ?>
        };

        function validateFinancement() {   
            tplCtx = {};
              tplCtx.id = "<?php echo $answer["_id"] ?>",
              tplCtx.path = "answers.<?php echo $form["id"] ?>.<?php echo $validateKunik."hidden" ?>",
              tplCtx.collection = "<?php echo Form::ANSWER_COLLECTION ?>"; 
              tplCtx.value = "canValidate";
              dataHelper.path2Value( tplCtx, function(params) {
                  urlCtrl.loadByHash(location.hash);
              } );

        }


        $(document).ready(function() {

            $("#deal32").change(function() {
             reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
            });

            $('button[data-tgle]').each(function(){
                $('.'+$(this).data('tgle')).each(function(){
                    $(this).fadeToggle();
                });
            });

            $('button[data-tgle]').each(function(){
                $(this).on("click", function(){
                    $('.'+$(this).data('tgle')).each(function(){
                        $(this).fadeToggle();
                    });
                });
            });

            sectionDyf.<?php echo $kunik ?> = {
                "jsonSchema" : {
                    "title" : "Ajouter financement",
                    "icon" : "fa-globe",
                    "text" : "",
                    "properties" : <?php echo json_encode( $properties ); ?>,
                    onLoads : {
	                    onload : function () {
                            $("#ajaxFormModal > .form-group").each(function() {

                                var allClass = $(this).attr('class').split(' ');
                                var thisform = $(this);
                                if (allClass[2].includes("text")) {
                                    thisform.hide();
                                }
                            });

	                    	$('#naturerecette').on('change', function() {
	                    		var valnature = $(this).val();
							

                                $("#ajaxFormModal > .form-group").each(function() {

                                    var thisformeach = $(this);

                                    var allClass = $(this).attr('class').split(' ');

                                    if (allClass[2].includes("text")) {
                                        var fiformid =  allClass[2].replace('text','');

                                        if (fiformid in sectionDyf.<?php echo $kunik ?>ParamsData[valnature]) {
                                            thisformeach.show();
                                        } else {
                                            thisformeach.hide();
                                        }
                                    }
								});
							});

                            if ($('#naturerecette').val() != "") {
                                var valnature = $('#naturerecette').val();
                            

                                $("#ajaxFormModal > .form-group").each(function() {

                                    var thisformeach = $(this);

                                    var allClass = $(this).attr('class').split(' ');

                                    if (allClass[2].includes("text")) {
                                        var fiformid =  allClass[2].replace('text','');

                                        if (fiformid in sectionDyf.<?php echo $kunik ?>ParamsData[valnature]) {
                                            thisformeach.show();
                                        } else {
                                            thisformeach.hide();
                                        }
                                    }
                                });
                            }


	                    }
                	},
                    beforeBuild : function(){
                        uploadObj.set("answers",answerObj._id.$id, "image", null, null, "/subKey/"+tplCtx.path);
                   },
                    save : function () {
                        var today = new Date();
                        tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
                        $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                    });




                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                dyFObj.commonAfterSave(null, function(){
                                    dyFObj.closeForm();
                                    reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
                                } );
                            } );
                        }

                    }
                }
            };

            sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema" : {
                    "title" : "<?php echo $kunik ?> config",
                    "description" : "Liste de question possible",
                    "icon" : "fa-cog",
                    "properties" : {
                    },
                    save : function () {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                            if(val.inputType == "properties")
                                tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                            else if(val.inputType == "array")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else
                                tplCtx.value[k] = $("#"+k).val();
                            mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                        });
                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                dyFObj.closeForm();
                                reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
                            } );
                        }

                    }
                }
            };


            mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

            //adds a line into answer
            $(".add<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
            });

            $(".edit<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
            });

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                //if no params config on the element.costum.form.params.<?php echo $kunik ?>
                //then we load default values available in forms.inputs.<?php echo $kunik ?>xxx.params
                //mylog.log(".editParams",sectionDyf.<?php echo $kunik ?>Params,calData);
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            });


        });
    </script>
<?php 
} }?>