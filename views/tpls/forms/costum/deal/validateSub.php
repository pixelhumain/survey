<div class="form-group">
    
    <?php
    $validatedOp = (isset($answer["links"]["operators"])) ? array_keys($answer["links"]["operators"]) : "";
    $validatedOp = (!empty($answer["links"]["operators"])) ? $validatedOp[0] : ""; 

    $mailList=[];
    
    $costum = CacheHelper::getCostum();
    $granterId = (isset(Yii::app()->session["costum"][$costum["slug"]]["financorOf"]["_id"])) ? (string)Yii::app()->session["costum"][$costum["slug"]]["financorOf"]["_id"] : "";
    $opId = (isset(Yii::app()->session["costum"][$costum["slug"]]["operatorOf"]["_id"])) ? (string)Yii::app()->session["costum"][$costum["slug"]]["operatorOf"]["_id"] : "";

    $nbAnswer = (isset($answer["answers"][$form["id"]][$key])) ? count($answer["answers"][$form["id"]][$key]) : 0 ;
   // var_dump($nbAnswer);

    //$opId = (isset(Yii::app()->session["costum"][$costum["slug"]]["operatorOf"]["_id"])) ? (string)Yii::app()->session["costum"][$costum["slug"]]["financorOf"]["_id"] : "";
    

    $ans=(isset($answer["answers"]["deal3"]["deal36"])) ? $answer["answers"]["deal3"]["deal36"] : "" ;
    //var_dump($ans);exit;
    $nbAns=(isset($answer["answers"]["deal3"]["deal36"])) ? count($answer["answers"]["deal3"]["deal36"]) : 0 ;
 //   var_dump($nbAns);

    
    $map = [];

    if(!empty($ans)){
        foreach($ans as $id => $val){
            $org = Element::getElementById($id,Organization::COLLECTION);
            $map["$id"] = [
                "name" => $org["name"],
                "slug" => $org["slug"]
            ];
        }
    } 
    
    ?>


    <label for="<?php echo $key ?>" class="col-xs-12"><h3 class="col-xs-12" style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label ?></h3></label>
    <?php 

        

       
        // if (isset(Yii::app()->session["costum"][$costum["slug"]]["financorOf"]) && isset(Yii::app()->session["costum"][$costum["slug"]]["financorOf"]["slug"])){
            foreach($map as $idFin => $value){ ?>
                  
                
                        <h4 class="col-xs-12" style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $value["name"] ?></h4><br>
                        <span class="col-xs-6 text-center"><?php echo $answer["answers"]["deal3"]["deal36"][$idFin] ?> ‎€</span>
                        <?php if(empty($answer["answers"][$form["id"]][$key][$idFin]) && ($mode != "r")){ ?>
                            <a id="" class="validateSub btn alert-success col-xs-2" data-value="<?php echo $answer["answers"]["deal3"]["deal36"][$idFin] ?>" data-id="<?php echo $idFin ?>"  data-slug="<?php echo $value["slug"] ?>" data-name="<?php echo $value["name"] ?>"><i class="fa fa-check"></i></a>
                            <a class="refuseSub btn alert-danger col-xs-2 col-xs-offset-1" data-id="<?php echo $idFin ?>" data-slug="<?php echo $value["slug"] ?>" data-name="<?php echo $value["name"] ?>"><i class="fa fa-times"></i></a>
                        <?php }
                        else if(isset($answer["answers"][$form["id"]][$key][$idFin]["state"])){?>
                            <span class="col-xs-6">Le financement a été <?php echo Yii::t('survey', $answer["answers"][$form["id"]][$key][$idFin]["state"])?></span>
                    <?php   }
                        else{ ?>
                            <span class="col-xs-6">Le financeur ne s'est pas prononcé sur l'attribution ou non de la subvention</span>   
                    <?php    }
                        ?>    
                           
     <?php          
                
            } 
            
           //var_dump($granterId);exit;
            if (!empty($granterId)){           
                $granter = Element::getCommunityByTypeAndId(Organization::COLLECTION, $granterId);
                foreach ($granter as $keypers => $valuepers) {
                    if ($valuepers["type"] == "citoyens") {
                        $cytns = Person::getEmailById($keypers);
                        if ($cytns["email"] != "") {
                            array_push($mailList,$cytns["email"]);
                        }    
                    }
                }
            }    
            if (isset($answer["links"]["operators"])) {
                $operateurVid = $answer["links"]["operators"];
                foreach ($operateurVid as $vkey => $vvalue) {
                    $comV = Element::getCommunityByTypeAndId(Organization::COLLECTION, $vkey);
                    foreach ($comV as $keyp => $valuep) {
                        if ($valuep["type"] == "citoyens") {
                            $mailOp = Person::getEmailById($keyp);
                            if ($mailOp["email"] != "") {
                                array_push($mailList,$mailOp["email"]);
                            }    
                        }
                    }
                }
            }    

             
            
        $mailList = array_unique($mailList);
        
        ?>

    
    
</div>

<script type="text/javascript">
    
    
var paramsValue = {};
    
    var validatedOp = <?php echo json_encode($validatedOp) ?>;
   var granterId = <?php echo json_encode(isset($granterId)) ? json_encode($granterId) : "" ?>;
    var opId = <?php echo json_encode(isset($opId)) ? json_encode($opId) : "" ?>;
     var answerId = <?php echo json_encode((string)$answer['_id']) ?>;
     var userId = <?php echo json_encode(isset(Yii::app()->session["userId"])) ? json_encode(Yii::app()->session["userId"]) : "" ?>;



        $( document ).ready(function() {
                     
            $(".validateSub").click(function() {
                 var slug = $(this).data("slug");
                var name = $(this).data("name");
                var idFin = $(this).data("id");
                var contacttplMail = <?php echo json_encode(isset($mailList)) ? json_encode($mailList) : "" ?>;
                
                //|| (slug == "cgss" &&  validatedOp==opId)
                if(granterId == idFin || (slug == "cgss" || slug == "laReunion" || slug=="reunionHabitat" &&  validatedOp==opId)){
                     var paramsmail<?php echo $kunik ?> = {
                         tpl : "basic",
                         tplObject : "Validation du financement " + name ,
                         tplMail : contacttplMail,
                         html: "Le financement " + name + " a été validée",
                         notification : "La validation de votre financement a été prise en compte. Un e-mail d'information a été envoyé à l'opérateur en charge du dossier.",
                         btnRedirect : {
                             hash : "#answer.index.id." + answerId,
                             label : "Accéder au dossier"
                        }
                     };

          
                     if(paramsmail<?php echo $kunik ?>.tplMail!=[] && paramsmail<?php echo $kunik ?>.html!="" && paramsmail<?php echo $kunik ?>.tplObject!=""){
                         mylog.log('paramsmail', paramsmail<?php echo $kunik ?>);
                           ajaxPost(
                                 null,
                                 baseUrl+"/co2/mailmanagement/createandsend",
                                 paramsmail<?php echo $kunik ?>,
                                 function(data){ 
                                     if(paramsmail<?php echo $kunik ?>.notification!=""){
                                         toastr.success(paramsmail<?php echo $kunik ?>.notification);
                                     }
                                 }
                           ); 
                     }

                    tplCtx.id = answerId;  
                    tplCtx.collection =  "answers"; 
                    tplCtx.path = "answers.<?php echo $form["id"].".".$key ?>." +idFin ; 
                    tplCtx.value = {};
                    tplCtx.value.user = userId; 
                    var today = new Date();
                    today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear()+ ' ' + today.getHours()+ ':' + today.getMinutes();
                    tplCtx.value.date =today;
                    tplCtx.value.state = "validated";  

                    mylog.log("save tplCtx state",tplCtx);
                    dataHelper.path2Value( tplCtx, function(params) { 
                        <?php if(isset($answer["state"]) && isset($answer["state"]["refused"])) { ?>
                           toastr.success('Réponse enregistrée');
                            tplCtx.value = "";
                            tplCtx.path = "state"  ;   
                            
                            mylog.log("save tplCtx state",tplCtx);
                            dataHelper.path2Value( tplCtx, function(params) { 
                            } ); 

                   <?php } ?>
                        reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
                        reloadInput("<?php echo  "deal406" ?>", "<?php echo (string)$form["_id"] ?>");
                        setTimeout(toastr.success("Veuillez s'il vous plaît fournir la copie de l'arrêté préfectoral d'attribution de votre subvention."),5000);
                    
                        scrollintoDiv("questiondeal403", 2000);
                                
                    });
                    
                




                }
               else{

                   alert("Vous ne pouvez pas vous positionner sur cette ligne de financement");
               }

            }); 

             $(".refuseSub").off().on("click",function() { 
             var slug = $(this).data("slug");
                var name = $(this).data("name");
                var idFin = $(this).data("id");
                var contacttplMail = <?php echo json_encode(isset($mailList)) ? json_encode($mailList) : "" ?>; 

                if(granterId == idFin || (slug == "cgss" || slug == "laReunion" || slug=="reunionHabitat" &&  validatedOp==opId)){
                    var paramsValue = {
                        name : name,
                        contacttplMail : contacttplMail
                     }; 
                    tplCtx.id = answerId;  
                    tplCtx.collection =  "answers"; 
                    tplCtx.path = "answers.<?php echo $form["id"].".".$key ?>." +idFin  ; 
                    mylog.log("path",tplCtx.path);       
                    mylog.log("paramsValue",paramsValue);     
                
                    dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, paramsValue);
                }
                else{   
                    alert("Vous ne pouvez pas vous positionner sur cette ligne de financement");
                }        
            });           
           
         });

        sectionDyf.<?php echo $kunik ?> = {
        "jsonSchema" : {    
            "title" : "Refus de subvention",
            "icon" : "fa-times",
            "properties" : {
                reason : {
                    inputType : "textarea",
                    label : "Précisez les raisons de votre refus",
                },
                name : dyFInputs.inputHidden(),
                contacttplMail : dyFInputs.inputHidden()


            },
            save : function () {  
                tplCtx.value = {};
                tplCtx.value.user = userId; 
                var today = new Date();
                today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear()+ ' ' + today.getHours()+ ':' + today.getMinutes();
                tplCtx.value.date =today;
                tplCtx.value.state = "refused"; 
                tplCtx.value.reason = formData.reason;        
    
                // $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val){   
                //     tplCtx.value[k] = $("#"+k).val();
                //  });

                var name = formData.name;
                var contacttplMail = formData.contacttplMail;

                mylog.log("save tplCtx question",tplCtx);

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    toastr.success('Votre refus d\'attribution de subvention a été pris en compte');
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                    } );

                    tplCtx.value = tplCtx.path.split(".").pop();
                    tplCtx.path = "state.refused"  ;   
                    
                    mylog.log("save tplCtx state",tplCtx);
                    dataHelper.path2Value( tplCtx, function(params) { 
                       var paramsmail<?php echo $kunik ?> = {
                         tpl : "basic",
                         tplObject : "Refus du financement" + name ,
                         tplMail : contacttplMail,
                         html: "Le financement " + name + " a été refusé pour la raison suivante : " +  formData.reason,
                         notification : "Un mail notifiant le refus de votre financement a été envoyé à l'opérateur en charge du dossier.",
                         btnRedirect : {
                             hash : "#answer.index.id." + answerId,
                             label : "Accéder au dossier"
                        }
                     };

          
                         if(paramsmail<?php echo $kunik ?>.tplMail!=[] && paramsmail<?php echo $kunik ?>.html!="" && paramsmail<?php echo $kunik ?>.tplObject!=""){
                             mylog.log('paramsmail', paramsmail<?php echo $kunik ?>);
                               ajaxPost(
                                     null,
                                     baseUrl+"/co2/mailmanagement/createandsend",
                                     paramsmail<?php echo $kunik ?>,
                                     function(data){ 
                                         if(paramsmail<?php echo $kunik ?>.notification!=""){
                                             toastr.success(paramsmail<?php echo $kunik ?>.notification);
                                         }
                                     }
                               ); 
                         }
                    } );     
                    reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
                    reloadInput("<?php echo  "deal406" ?>", "<?php echo (string)$form["_id"] ?>");
                }

            }
        }
    };
</script>