<?php 

$inputs = Array();
$inputsPath = PHDB::findOne(Form::COLLECTION, ["id"=> $form["id"]]);

$commonPathAndValue = [];
$extraPathAndValue = [];

$paramsData = [
    "attributes" => [
        "attribute0" => [
            "attributeName" => "name", 
            "toAnswerPath" => ""
        ],
        "attribute1" => [
            "attributeName" => "type", 
            "toAnswerPath" => ""
        ],
        "attribute2" => [
            "attributeName" => "tags", 
            "toAnswerPath" => ""
        ],
        "attribute3" => [
            "attributeName" => "email", 
            "toAnswerPath" => ""
        ],
        "attribute4" => [
            "attributeName" => "shortDescription", 
            "toAnswerPath" => ""
        ],
        "attribute5" => [
            "attributeName" => "url", 
            "toAnswerPath" => ""
        ],
        "attribute5" => [
            "attributeName" => "adresse", 
            "toAnswerPath" => ""
        ]
    ]
];
$paramsD = [
    "admin" => "false",
    "labelBtn" => "OUI, Créer une organisation à partir de cette réponse",
    "sourceKey" => "",
    "defaultValue" => [],
    "urlRedirect" =>""
];

# Set parameters data
if( isset($parentForm["params"][$kunik]) ){
    foreach ($paramsData as $e => $v) {
        if (  isset($parentForm["params"][$kunik][$e]) ) {
            $paramsData[$e] = $parentForm["params"][$kunik][$e];
        }
    }
}
if( isset($parentForm["params"][$kunik]["config"]) ){
    foreach ($paramsD as $e => $v) {
        if (  isset($parentForm["params"][$kunik]["config"][$e]) ) {
            $paramsD[$e] = $parentForm["params"][$kunik]["config"][$e];
        }
    }
}
# Get and set form inputs
foreach($inputsPath["inputs"] as $inputKey => $inputValue){
    if($inputKey!=$key){
        array_push($inputs, [$inputKey => $inputValue["label"]]);
    }
}

# set organization's common and extra infos

if(isset($answer["answers"][$form["id"]])){
    foreach($answer["answers"][$form["id"]] as $answerKey => $answerValue){
        foreach($paramsData["attributes"] as $paramKey => $paramValue){

            if($answerKey == $paramValue["toAnswerPath"]){
                $paramToAnswerPath = $paramValue["toAnswerPath"];
            }else if($answerKey == "multiCheckboxPlus".$paramValue["toAnswerPath"]){
                $paramToAnswerPath = "multiCheckboxPlus".$paramValue["toAnswerPath"];
            }else{
                $paramToAnswerPath = "";
            }

            if($paramToAnswerPath!=""){
                if(in_array($paramValue["attributeName"],["name", "role", "tags", "email", "shortDescription", "url","adresse"])){
                    if($paramValue["attributeName"]=="tags"){
                        if(!isset($commonPathAndValue["tags"])){
                            $commonPathAndValue["tags"] = [];
                        }
                        if(is_array($answerValue)){
                            if(array_keys($answerValue) == range(0, count($answerValue)-1)){
                                foreach ($answerValue as $answerValueKey => $answerValuesValue) {
                                    if(array_keys($answerValuesValue) !== range(0, count($answerValuesValue)-1)){
                                        foreach ($answerValuesValue as $avk => $avv) {
                                            array_push($commonPathAndValue["tags"], $avk);
                                        }
                                    }else{
                                        array_push($commonPathAndValue["tags"], $answerValuesValue);
                                    }
                                }
                            }else{
                                foreach ($answerValue as $answerValueKey => $answerValuesValue) {
                                    array_push($commonPathAndValue["tags"], $answerValuesValue);
                                }
                            }
                            
                        }else{
                            array_push($commonPathAndValue["tags"], $answerValue);
                        }
                    }else {
                        $commonPathAndValue[$paramValue["attributeName"]] = $answerValue; 
                    }
                }else{
                    $extraPathAndValue[$paramValue["attributeName"]] = $answerValue;
                }
            }
        }
    }
}

# Setting some data
$extraPathAndValue["formAnswer"] = $answer["form"];
if(isset($commonPathAndValue["shortDescription"])){
    $extraPathAndValue["shortDescription"] = $commonPathAndValue["shortDescription"];
    $commonPathAndValue["shortDescription"] = substr( $commonPathAndValue["shortDescription"], 0, 140);
}
if (isset($commonPathAndValue["adresse"])) {
    $commonPathAndValue["coord"] = $commonPathAndValue["adresse"]["coord"];
    $commonPathAndValue["address"] = $commonPathAndValue["adresse"]["address"];
    $commonPathAndValue["geo"] = $commonPathAndValue["adresse"]["geo"];
    $commonPathAndValue["geoPosition"] = $commonPathAndValue["adresse"]["geoPosition"];
}

$commonPathAndValue["role"] = "admin";
$commonPathAndValue["type"] = "NGO";
$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik.".config' class=' param".$kunik."Params btn btn-lg btn-danger'><i class='fa fa-cog'></i> Paramètre </a>" : "";
$cofigure =($canEditForm) ?'<a role="button" class="btn btn-danger btn-lg config'.$kunik.'" data-id="'.$answer["form"].'" data-collection="'. Form::COLLECTION.'" data-path="params.'.$kunik.'"> Configurer les réponses </a>':'';
$createOrga ='<a role="button" class="btn btn-success btn-lg elementify'.$kunik .'" data-id="'.$contextId.'" data-collection="'.Organization::COLLECTION .'" data-path="allToRoot">'.$paramsD["labelBtn"].'</a>';
?>
<?php if ($paramsD["admin"] == "true"){ 
    if(Authorisation::isInterfaceAdmin()){ ?>
        <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
            <?php echo $label.$editQuestionBtn ?>        
        </h4>
        <?php echo $cofigure.$editParamsBtn.$createOrga?> 
    <?php }
} else { ?>
   <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
    <?php echo $label.$editQuestionBtn ?>        
</h4>
<?php echo $cofigure.$editParamsBtn.$createOrga?>
<?php } ?>
<script type="text/javascript">
    sectionDyf.<?php echo $key ?>ParamsData = <?php echo json_encode($paramsData); ?>;
    sectionDyf.<?php echo $key ?>ParamsD = <?php echo json_encode($paramsD); ?>;

    $(document).ready(function() {
        function saveLinksMemberof(citoyens,orga){
            var linkOf = {};
            linkOf.id = citoyens;
            linkOf.path = "links.memberOf."+orga;            
            linkOf.collection = "citoyens";
            linkOf.value = {
               type : "organizations",
               isAdmin : true
            }
            dataHelper.path2Value( linkOf, function(params) {
            });
        }
        function saveLinksMember(citoyens,orga){
            var linkM = {};
            linkM.id = orga;
            linkM.path = "links.members."+citoyens;
            linkM.collection = "organizations";
            linkM.value = {
              isAdmin : true,
              type : "citoyens"
            }
            dataHelper.path2Value( linkM, function(params) {

            });
        }
        $(".config<?php echo $kunik ?>").off().on("click",function() { 
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = $(this).data("path");

            sectionDyfObservatory = {
                "jsonSchema" : {
                    "title" : "Configuration des transformation",
                    "icon" : "fa-cog",
                    "text" : "Sélectionnez la réponse d'un champ text pour réprésenter la réponse en tant que titre d'un element",
                    "properties" : {
                        attributes : {
                            inputType : "lists",
                            label : "Mappage des attributes",
                            entries: {
                                attributeName: {
                                    type:"text",
                                    label:"Nom de l'attribut",
                                    class:"col-lg-5"
                                },
                                toAnswerPath: {
                                    type:"select",
                                    label:"réponse du formulaire",
                                    options:<?php echo json_encode($inputs) ?>,
                                    class:"col-lg-5"
                                }
                            }
                        }
                    },
                    save : function (data) { 

                        tplCtx.value = {};
                        
                        $.each( sectionDyfObservatory.jsonSchema.properties , function(k,val) {
                            if(k=="attributes"){
                                let attributes = {};

                                $.each(data.attributes, function(index, va){
                                    let attribute = {attributeName: va.attributeName, toAnswerPath: va.toAnswerPath};
                                    attributes["attribute"+index] = attribute;
                                });

                                tplCtx.value[k] = attributes;
                            }/**else{
                                tplCtx.value[k] = $("#"+k).val();
                            }*/
                        });

                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                location.reload();
                            });
                        }
                    }
                }
            }

            dyFObj.openForm( sectionDyfObservatory, null, sectionDyf.<?php echo $key ?>ParamsData );
        });

        // save as element
        $(".elementify<?php echo $kunik ?>").off().on("click", function(){
            tplCtx.path = $(this).data("path");
            answer = <?= json_encode($answer)?>;
            paramsD = <?= json_encode($paramsD)?>;
            console.log("ihgifgqriyfqri",<?php echo json_encode($commonPathAndValue) ?>);
            dyFObj.openForm(
                'organization', 
                null, 
                <?php echo json_encode($commonPathAndValue) ?>,
                null, 
                {
                    afterSave : function(element){

                        let extraPathAndValue = <?php echo json_encode($extraPathAndValue) ?>;
                        let data = { 
                            ...extraPathAndValue, 
                        };
                        if (paramsD.defaultValue != null) {
                            $.each(paramsD.defaultValue , function(kd,vald) {
                                if (kd == "tags") {
                                    if (typeof element.map.tags != "undefined") {
                                        (element.map.tags).push(vald.toAnswerPath);
                                        data.tags = element.map.tags;
                                    }else {
                                        data.tags = [vald.toAnswerPath];
                                    }
                                } else {
                                    data[vald.attributeName]= vald.toAnswerPath;
                                }
                            })
                        }
                        <?php if ($paramsD["sourceKey"] != "") {?>
                            data.source = {
                                key:"<?= $paramsD["sourceKey"]?>",
                                keys: ["<?= $paramsD["sourceKey"]?>"]
                            }
                        <?php } ?>
                       
                        saveLinksMember(answer.links.answered[0],element.id);
                        saveLinksMemberof(answer.links.answered[0],element.id);
                        tplCtx.id = element.id;
                        tplCtx.collection = element.map.collection;
                        tplCtx.value = data;
                        dataHelper.path2Value( tplCtx, function(params) {
                            <?php if(isset($paramsD["urlRedirect"]) && !empty($paramsD["urlRedirect"])){ ?>  
                                document.location.href="<?php echo $paramsD["urlRedirect"];?>";
                                if ( costum != null) {
                                    location.reload();
                                }
                            <?php } else{ ?>
                                urlCtrl.loadByHash(location.hash);
                            <?php }?>  
                        });
                    }
                }
                )
        });
        $(".param<?php echo $kunik ?>Params").off().on("click",function() { 
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = $(this).data("path");
            sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema" : {
                    "title" : "Configuration des transformation",
                    "icon" : "fa-cog",
                    "text" : "Sélectionnez la réponse d'un champ text pour réprésenter la réponse en tant que titre d'un element",
                    "properties" : {
                        "labelBtn" :{
                            inputType:"text",
                            label : "Label du bouton"
                        },
                        "sourceKey" :{
                            inputType:"text",
                            label : "Référence (sourceKey: slug de l'organisation référence)"
                        },
                        admin : {  
                            inputType : "checkboxSimple",
                            label : "Admin ",
                            subLabel : "Seul l'admin peut voir",
                            params : { onText : "Oui", offText : "Non", onLabel : "Oui", offLabel : "Non",
                            labelText : "Privé"},
                            checked : false 
                        },
                        urlRedirect : {
                            inputType : "url",
                            label : "Lien de redirection"
                        },
                        defaultValue : {
                            inputType : "lists",
                            label : "Valeur par defaut",
                            entries: {
                                attributeName: {
                                    type:"text",
                                    label:"Nom de l'attribut",
                                    class:"col-lg-5"
                                },
                                toAnswerPath: {
                                    type:"text",
                                    label:"valuer du formulaire",
                                    class:"col-lg-5"
                                }
                            }
                        }
                    },
                    save : function (data) { 
                        tplCtx.value = {};
                        $.each(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                            if(k=="defaultValue"){
                                let defaultValues = {};

                                $.each(data.defaultValue, function(index, va){
                                    let defaultValue = {attributeName: va.attributeName, toAnswerPath: va.toAnswerPath};
                                    defaultValues[va.attributeName] = defaultValue;
                                });

                                tplCtx.value[k] = defaultValues;
                            } else 
                                tplCtx.value[k] = $("#"+k).val();
                            
                        });
                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                location.reload();
                            });
                        }
                    }
                }
            }
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params, null, sectionDyf.<?php echo $key ?>ParamsD );
        });

    });
</script>
