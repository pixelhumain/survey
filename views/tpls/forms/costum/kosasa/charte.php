<?php 
	$paramsData = [
		"content" => "Lorem ipsum
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam scelerisque nisi vitae est commodo placerat. Proin semper arcu lacus, vitae imperdiet est consequat non. Nullam interdum nisi orci, vel varius odio dignissim et. Donec pretium arcu eu sollicitudin mattis. Vestibulum vitae mollis magna, at pulvinar enim. Integer tristique dolor eget magna sodales mattis. Duis fermentum tellus sit amet purus commodo fermentum. Ut lacus massa, venenatis vitae urna fringilla, viverra faucibus libero. Pellentesque vestibulum lorem et purus congue consectetur. Nunc pharetra, ex in tincidunt rhoncus, tortor est placerat lorem, in venenatis ligula elit sit amet nunc. Mauris molestie, turpis sed ultrices venenatis, est nulla vestibulum elit, ut imperdiet ex neque ut elit. Integer tristique egestas ipsum, quis lacinia odio imperdiet vel. Praesent nibh ex, consequat quis quam sit amet, semper mollis quam. Sed non est nulla.
		Vestibulum at lorem auctor quam pulvinar fermentum eget non nunc. Pellentesque vitae eros egestas, tempus sem nec, hendrerit sapien. Mauris eu viverra libero, in imperdiet dolor. Quisque lorem mi, sodales vitae dui nec, accumsan malesuada magna. Duis cursus varius quam a semper. Praesent varius tortor pellentesque lectus pretium, eu blandit dui sodales. Nunc in facilisis lorem, eget dapibus nibh. Ut pharetra elit sit amet luctus ornare. Nulla vel maximus risus. Pellentesque vel diam non arcu rhoncus fringilla nec cursus quam. Proin rhoncus pharetra orci nec finibus. Duis suscipit maximus gravida.
		Fusce facilisis, neque ac condimentum pellentesque, urna magna sagittis massa, ut volutpat urna erat quis leo. Nullam urna lorem, varius at dui vitae, condimentum egestas enim. Aliquam vulputate aliquam massa id egestas. Duis varius eros vitae aliquet aliquet. Fusce id accumsan lacus, sit amet porttitor erat. Nullam pharetra nisi nulla. Vivamus vitae ligula eu nibh condimentum mollis. Ut in dui id justo dapibus consectetur at at ligula."
	];
	if( isset($parentForm["params"][$kunik]) ){
		foreach ($paramsData as $e => $v) {
			if (  isset($parentForm["params"][$kunik][$e]) ) {
				$paramsData[$e] = $parentForm["params"][$kunik][$e];
			}
		}
	}
	$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";
?>
<style type="text/css">
	#question<?= $key?> {
		margin-bottom: -50px !important;
	}
	.content<?= $kunik?> {
		padding-left: 20px;
	}
</style>
<div class="col-xs-12 col-md-6 no-padding">
	<div >
		<label for="<?php echo $kunik ?>">
			<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
				<?php echo $label.$editQuestionBtn.$editParamsBtn ?>
			</h4>
		</label>
	</div>

	<div class="markdown content<?= $kunik?>"><?= $paramsData["content"]?></div>
</div>


<script type="text/javascript">
	sectionDyf.<?php echo $key ?>ParamsData = <?php echo json_encode($paramsData); ?>;

	$(document).ready(function() {
		sectionDyf.<?php echo $kunik ?>Params = {
			"jsonSchema" : {	
				"title" : "<?php echo $kunik ?> config",
				"icon" : "fa-cog",
				"properties" : {
					content : {
						inputType : "textarea",
						markdown :true,
						label : "contenu de la charte",
						values :  sectionDyf.<?php echo $key ?>ParamsData.content
					}
				},
				save : function () {  
					tplCtx.value = {};
					$.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
						
						tplCtx.value[k] = $("#"+k).val();
					});
					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
						dataHelper.path2Value( tplCtx, function(params) { 
							$("#ajax-modal").modal('hide');							
							urlCtrl.loadByHash( location.hash );
						} );
					}

				}
			}
		};
		$(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = $(this).data("path");
			dyFObj.openForm(sectionDyf.<?php echo $kunik ?>Params, null, sectionDyf.<?php echo $key ?>ParamsData);
		});
	})
</script>