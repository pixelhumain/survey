<style type="text/css">

	#question<?= $key?> .ckd-grp label {
		cursor: pointer;
		-webkit-tap-highlight-color: transparent;
		padding: 6px 8px;
		border-radius: 20px;
		float: left;
		transition: all 0.2s ease;
	}
	#question<?= $key?> .ckd-grp label:hover {
		background: rgba(125,100,247,0.06);
	}
	#question<?= $key?> .ckd-grp label:not(:last-child) {
		margin-right: 16px;
	}
	#question<?= $key?> .ckd-grp label span {
		vertical-align: middle;
	}
	#question<?= $key?> .ckd-grp label span:first-child {
		position: relative;
		display: inline-block;
		vertical-align: middle;
		width: 27px;
		height: 27px;
		background: #e8eaed;
		border-radius: 10px;
		transition: all 0.2s ease;
		margin-right: 8px; 
		padding: 3px;
	}
	#question<?= $key?> .ckd-grp label span:first-child:after {
		content: '';
		position: absolute;
		width: 16px;
		height: 16px;
		margin: 2px;
		background: #fff;
		border-radius: 6px;
		transition: all 0.2s ease;
	}


	#question<?= $key?> .ckb-grp label:hover span:first-child:after {
		padding: 0px;
		background: white;

	}

	#question<?= $key?> .ckd-grp input {
		display: none;
	}
	#question<?= $key?> .ckd-grp input:checked + label span:first-child {
		/*background: #7d64f7;*/
		background: #e8eaed;
	}
	#question<?= $key?> .ckd-grp input:checked + label span:first-child:after {
		background: #7d64f7;
	}

	#question<?= $key?> .multiChbtextInp{border: 0; padding: 7px 0; border-bottom: 1px solid #ccc;}

	#question<?= $key?> .multiChbtextInp ~ .focus-border{position: absolute; bottom: 0; left: 0; width: 0; height: 2px; background-color: #3399FF; transition: 0.4s;}
	#question<?= $key?> .multiChbtextInp:focus ~ .focus-border{width: 100%; transition: 0.4s;}

	 .paramsonebtn , .paramsonebtnP {
		font-size: 17px;
		display: none;
		padding: 5px;
	}

	#question<?= $key?> .paramsonebtn:hover {
		color: red;
	}

	#question<?= $key?> .paramsonebtnP:hover {
		color: blue;
	}

	#question<?= $key?> .thckd:hover .paramsonebtn, .thckd:hover .paramsonebtnP {
		display: inline-block;
	}

	#question<?= $key?> .multiChbtextInp:focus {
		outline: none !important;
	}

	#question<?= $key?> .responsemulticheckboxplus:before{
		content: '\f0a9';
		margin-right: 15px;
		font-family: FontAwesome;
		color: #d9534f;
	}

	#question<?= $key?> .ckboptinfo{
		font-size: 15px;
		color: #637381;
	}
</style>



<?php 
	$cle = $key ;
	$value = (!empty($answers)) ? $answers : "";
	$inpClass = " saveOneByOne"; 

	$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";


	$subkey = $key;


	$paramsData = [ 
		"list" => [	],
		"tofill" => [ ],
		"optinfo" => [ ],
		"placeholdersckb" => [ ],
		"type" => [
			"simple" => "Sans champ de saisie",
			"cplx" => "Avec champ de saisie"
		],
		'checked' => [],
		"width" => [
			"12" => "1",
			"6" => "2",
			"4" => "3"
		]
	];


	if( isset($parentForm["params"][$kunik]) ) {
		if( isset($parentForm["params"][$kunik]["global"]["list"]) ) { 
			$paramsData["list"] =  $parentForm["params"][$kunik]["global"]["list"];
			foreach ($paramsData["list"] as $k => $v) {
				if(isset($parentForm["params"][$kunik]["tofill"][$v]) ){
					$paramsData["tofill"] += array($v => $parentForm["params"][$kunik]["tofill"][$v]);
				} else {
					$paramsData["tofill"] += [$v => "simple"];
				}

				if(isset($parentForm["params"][$kunik]["optinfo"][$v]) ){
					$paramsData["optinfo"] += array($v => $parentForm["params"][$kunik]["optinfo"][$v]);
				} else {
					$paramsData["optinfo"] += [$v => ""];
				}

				$paramsData["optimage"][$v] = Document::getListDocumentsWhere(
					array(
						"id"=> $cle.$v,
						"type"=>'answers',
					), "file"
				);
			}
		}
	}

	if( isset($parentForm["params"][$kunik]) ) {
		if( isset($parentForm["params"][$kunik]["global"]["list"]) ) { 
			//$paramsData["list"] =  $parentForm["params"][$kunik]["global"]["list"];
			foreach ($paramsData["list"] as $k => $v) {
				if(isset($parentForm["params"][$kunik]["placeholdersckb"][$v]) ){
					$paramsData["placeholdersckb"] += array($v => $parentForm["params"][$kunik]["placeholdersckb"][$v]);
				} else {
					$paramsData["placeholdersckb"] += [$v => ""];
				}
			}
		}
	}


	if( isset($answer["answers"][$form["id"]][$subkey]) ) {
		$paramsData["checked"] =  $answer["answers"][$form["id"]][$subkey];
	}
	if($mode == "r")
	{ 
		?>
		<div class="" style="padding-top: 50px; text-transform: unset;">
			<label class="form-check-label" for="<?php echo $key ?>">
				<h4 style="text-transform: unset; color: <?php echo ($titleColor) ? $titleColor : "black"; ?>;"><?php echo $label ?>
			</h4>
		</label>	


		<?php 
		if( !isset($parentForm["params"][$kunik]["global"]['list']) ) 
		{
			echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> THIS FIELD HAS TO BE CONFIGURED FIRST </span>";
		} else  {
		?>
		
		<div class="">
			<table style="width: 100%">
				<tr class=""><th >
					<div class="col-md-12">
						<?php 
						foreach ($parentForm["params"][$kunik]["global"]["list"] as $ix => $lbl) 
						{ 
							$ckd = "";
							$place = "";
							foreach ($paramsData["checked"] as $key => $value) {
								foreach ($value as $ke => $va) {
									if($ke == $lbl){
										$ckd = "checked";
									}
								}
							}
							if ($ckd == "checked"){
								?>
									<div class="col-md-<?php echo (isset($parentForm["params"][$kunik]['global']['width'])) ? $parentForm["params"][$kunik]['global']['width'] : '12'; ?> col-xs-12 col-sm-12" style="min-height: 50px;">
										<div class="">

											<label for="<?php echo $subkey.$ix ?>" class="responsemulticheckboxplus">
												<span>

												</span>
												<span><?php echo $lbl ?>


												<?php 
												if (isset($paramsData["tofill"][$lbl]) && $paramsData["tofill"][$lbl] == "cplx") {
													?>
													<?php 
													if(isset($answer["answers"][$form["id"]][$kunik][0][$key]["textsup"]) && isset($answer["answers"][$form["id"]][$kunik][0][$key]["value"]) && $answer["answers"][$form["id"]][$kunik][0][$key]["value"] == $key )
													{
														echo $answer["answers"][$form["id"]][$kunik][0][$key]["textsup"] ;
													} 
													?>
													<?php
												}
												?>
												<?php 
												if($canEditForm) {
													echo " <a href='javascript:;' data-id='".$parentForm["_id"]."'  data-collection='".Form::COLLECTION."'  data-path='params.".$kunik."' class='previewTpl paramsonebtn editone".$kunik."Params".$ix." '><i class='fa fa-cog'></i> </a>";

													if (isset($paramsData["tofill"][$lbl]) && $paramsData["tofill"][$lbl] == "cplx") {
														echo " <a alt='placeholder' href='javascript:;' data-id='".$parentForm["_id"]."'  data-collection='".Form::COLLECTION."'  data-path='params.".$kunik."' class='previewTpl paramsonebtnP editonep".$kunik."Params".$ix." '><i class='fa fa-pencil'></i> </a>";
													} 
												}
												?>
											</span>
										</label> 
									</div>
								</div>
								<?php 
							}  }
							?>
						</div>
					</th></tr>
				</table>
			</div>
			<?php 
		}
		?>

		<?php 
		if(!empty($info))
		{ 
			?>
			<small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
			<?php
		}
		?>
	</div>

	<?php 
}else { ?>

	<div class="" style="padding-top: 50px; text-transform: unset;">
		<label class="form-check-label" for="<?php echo $key ?>">
			<h4 style="text-transform: unset; color: <?php echo ($titleColor) ? $titleColor : "black"; ?>;"><?php echo $label.$editQuestionBtn.$editParamsBtn ?>
		</h4>
	</label>	
	<?php 
	if( !isset($parentForm["params"][$kunik]["global"]['list']) ) 
	{
		echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> THIS FIELD HAS TO BE CONFIGURED FIRST ".$editParamsBtn."</span>";
	} else 
	{
		?>
		
		<div class="ckd-grp">
			<table style="width: 100%">
				<tr class=""><th >
					<div class="col-md-12">
						<?php 
						foreach ($parentForm["params"][$kunik]["global"]["list"] as $ix => $lbl) 
						{ 
							$ckd = "";
							$place = "";
							foreach ($paramsData["checked"] as $key => $value) {
								foreach ($value as $ke => $va) {
									if($ke == $lbl){
										$ckd = "checked";
									}
								}
							}
							?>

							<!-- <tr class="thckd"><theih > -->
								<div class="col-md-<?php echo (isset($parentForm["params"][$kunik]['global']['width'])) ? $parentForm["params"][$kunik]['global']['width'] : '12'; ?> col-xs-12 col-sm-12" style="min-height: 50px;">
									<div class="thckd">
										<input data-id="<?php echo $subkey ?>" class=" ckbCo <?php echo $kunik ?>"  id="<?php echo $subkey.$ix ?>" data-form='<?php echo $form["id"] ?>' <?php echo $ckd?> type="checkbox" name="<?php echo $kunik ?>" data-type="<?php 

										if (isset($paramsData["tofill"][$lbl]) && $paramsData["tofill"][$lbl] == "cplx")
										{
											echo "cplx";
											}else 
											{
												echo "simple";
											}
										?>" value="<?php echo $lbl ?>" />

										<label for="<?php echo $subkey.$ix ?>">
											<span>

											</span>
											<span><?php echo $lbl ?>
											<div style="position: relative; display: inline-block;">

												<?php 
												if (isset($paramsData["tofill"][$lbl]) && $paramsData["tofill"][$lbl] == "cplx") {
													?>
													<input data-impname="<?php echo $kunik ?>" class="multiChbtextInp inputckbCo <?php echo $kunik ?>" data-id="<?php echo $kunik ?>" data-form='<?php echo $form["id"] ?>' type="text" data-imp="<?php echo $lbl; ?>" placeholder="<?php echo $paramsData['placeholdersckb'][$lbl]; ?>" style="display: inline-block !important; position: relative;"  value="<?php 
													if(isset($answer["answers"][$form["id"]][$kunik][0][$lbl]["textsup"]) && isset($answer["answers"][$form["id"]][$kunik][0][$lbl]["value"]) && $answer["answers"][$form["id"]][$kunik][0][$lbl]["value"] == $lbl )
													{
														echo $answer["answers"][$form["id"]][$kunik][0][$key]["textsup"] ;
													} 
												?>">
												<span class="focus-border"></span>
												<!-- az -->
												<?php
											}
											?>
										</div>

										<?php 

										if($canEditForm)
										{
											echo " <a href='javascript:;' data-id='".$parentForm["_id"]."'  data-collection='".Form::COLLECTION."'  data-path='params.".$kunik."' class='previewTpl paramsonebtn editone".$kunik."Params".$ix." '><i class='fa fa-cog'></i> </a>";

											if (isset($paramsData["tofill"][$lbl]) && $paramsData["tofill"][$lbl] == "cplx") {

												echo " <a alt='placeholder' href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."'  data-path='params.".$kunik."' class='previewTpl paramsonebtnP editonep".$kunik."Params".$ix." '><i class='fa fa-pencil'></i> </a>";

											} 

										}
										?>
									</span>
									<?php
									if (isset($paramsData["optinfo"][$lbl]) && $paramsData["optinfo"][$lbl] != "") {
										?>		
										<div class="ckboptinfo"> <?php echo $paramsData["optinfo"][$lbl]?>  </div>
										<?php			
									}

									if (isset($paramsData["optimage"][$lbl]) && !empty($paramsData["optimage"][$lbl])) {
										foreach ($paramsData["optimage"][$lbl] as $oikey => $oivalue) {
											if (isset($oivalue["docPath"] )) {
												?>
												<div>
													<img class="img-responsive" src="<?php echo $oivalue["docPath"] ?>">
												</div>
												<?php
											}
										}
									}

									?>
								</label> 
							</div>
						</div>
						<?php 
					} 
					?>
				</div>
			</th></tr>
		</table>
	</div>
	<?php 
}
?>

<?php 
if(!empty($info))
{ 
	?>
	<small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
	<?php
}
?>
</div>

<script type="text/javascript">
	var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answer["answers"][$kunik])) ? $answer["answers"][$kunik] : null ); ?>;
	sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;



	sectionDyf.<?php echo $kunik ?>ParamsPlace = "";


	jQuery(document).ready(function() {
		mylog.log("render form input","/modules/costum/views/tpls/forms/cplx/multiCheckbox.php");

		answer = {};
		$('.ckbCo.<?php echo $kunik ?>').change(function(){
			var allckd = [];
    		// alert('<?php echo $kunik ?>');
    		$("input:checkbox[name='"+$(this).attr("name")+"']:checked").each(function(){
    			mylog.log("ckd", $(this).val());
    			var key = $(this).val();
    			var b = { [key] : { 'type' : $(this).data("type"), 'textsup' : $('input[data-imp="' + $(this).val() + '"]').val() }};
    			allckd.push(b);
    		});

			// alert(allckd);

			answer.path = "answers."+$(this).data("form")+"."+$(this).data("id");
			answer.collection = "answers" ;
			answer.id = "<?php echo $answer["_id"]; ?>";
			answer.value = allckd;
			dataHelper.path2Value(answer , function(params) { 
				reloadInput("<?php echo $cle ?>", "<?php echo (string)$form["_id"] ?>");
				toastr.success('Mise à jour enregistrée');
			});
		});

		$('.inputckbCo.<?php echo $kunik ?>').keyup(function(){ 
			if($('input[value="' + $(this).data("imp") + '"]').is(':checked')) {

				var allckd = [];

				$("input:checkbox[name='"+$(this).data("impname")+"']:checked").each(function(){
					mylog.log("ckd", $(this).val());
					var key = $(this).val();
					var b = { [key] : { 'value' : $(this).val(), 'type' : $(this).data("type"), 'textsup' : $('input[data-imp="' + $(this).val() + '"]').val() }};
					allckd.push(b);
				});

			// alert(allckd);

			answer.path = "answers."+$(this).data("form")+"."+$(this).data("id");
			answer.collection = "answers" ;
			answer.id = "<?php echo $answer["_id"]; ?>";
			answer.value = allckd;
			dataHelper.path2Value(answer , function(params) { 
				reloadInput("<?php echo $cle ?>", "<?php echo (string)$form["_id"] ?>");
				toastr.success('Mise à jour enregistrée');
			});

		}
	})


		sectionDyf.<?php echo $kunik ?>Params = {
			"jsonSchema" : {	
				"title" : "<?php echo $kunik ?> config",
				"icon" : "fa-cog",
				"properties" : {
					list : {
						inputType : "array",
						label : "Liste de bouton check",
						value :  sectionDyf.<?php echo $kunik ?>ParamsData.list
					},
					width : {
						inputType : "select",
						label : "Nombre d'element par ligne",
						options :  sectionDyf.<?php echo $kunik ?>ParamsData.width,
						value : "<?php echo (isset($parentForm["params"][$kunik]['global']['width']) and $parentForm["params"][$kunik]['global']['width'] != "") ? $paramsData["width"][strval($parentForm["params"][$kunik]['global']['width'])] : ''; ?>"
					}
				},
				save : function () {  
					tplCtx.value = {};
					$.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
						if(val.inputType == "properties")
							tplCtx.value = getPairsObj('.'+k+val.inputType);
						else if(val.inputType == "array")
							tplCtx.value[k] = getArray('.'+k+val.inputType);
						else if(val.inputType == "formLocality")
							tplCtx.value[k] = getArray('.'+k+val.inputType);
						else
							tplCtx.value[k] = $("#"+k).val();
					});
					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
						dataHelper.path2Value( tplCtx, function(params) { 
							$("#ajax-modal").modal('hide');
							reloadInput("<?php echo $cle ?>", "<?php echo (string)$form["_id"] ?>");
						} );
					}

				}
			}
		};


		<?php if(count($paramsData["list"]) != 0){
			foreach ($paramsData["list"] as $key => $value) {
	  		// if($lbl == $key){
	  		// 	if($value == "cplx"){
				?>
				sectionDyf.<?php echo $kunik ?>oneParams<?php echo $key ?> = {
					"jsonSchema" : {	
						"title" : "<?php echo $kunik ?> config",
						"icon" : "fa-cog",
						"properties" : {
							tofill : {
								inputType : "select",
								label : "Bouton check avec champ de saisie",
								options :  sectionDyf.<?php echo $kunik ?>ParamsData.type,
								value : "<?php echo (isset($paramsData["tofill"][$value]) ? $paramsData["tofill"][$value] : ''); ?>"
							},
							optinfo : {
								inputType : "text",
								label : "Information supplementaire",
								value : "<?php echo (isset($paramsData["optinfo"][$value]) ? $paramsData["optinfo"][$value] : ''); ?>"
							},
							optimage :{
								"inputType" : "uploader",
								"label" : "Image en fond",
								"docType": "image",
								"itemLimit" : 1,
								"filetypes": ["jpeg", "jpg", "gif", "png"],
								"showUploadBtn": false,

								initList : <?php echo json_encode($paramsData["optimage"][$value]) ?>
							}
						},
						beforeBuild : function(){
							uploadObj.set("answers","<?php echo $cle.$value ?>");
						},
						save : function () {  
							tplCtx.value = {};
							$.each( sectionDyf.<?php echo $kunik ?>oneParams<?php echo $key ?>.jsonSchema.properties , function(ko,valo) { 

								if (ko == "tofill") {

									tplCtx.path = 'params.<?php echo $kunik ?>.tofill';

									$.each(sectionDyf.<?php echo $kunik ?>ParamsData.tofill, function(ke, va) {

										if(ke == "<?php echo $value; ?>"){

											var azerazer = ke.toString();
											sectionDyf.<?php echo $kunik ?>ParamsData.tofill[azerazer] = $("#"+ko).val();
										}
									})

									tplCtx.value = sectionDyf.<?php echo $kunik ?>ParamsData.tofill;

									mylog.log("save tplCtx",tplCtx);

									if(typeof tplCtx.value == "undefined")
										toastr.error('value cannot be empty!');
									else {
										dataHelper.path2Value( tplCtx, function(params) { 

										} );
									}

								}else if(ko == "optinfo"){
									tplCtx.path = 'params.<?php echo $kunik ?>.optinfo';

									$.each(sectionDyf.<?php echo $kunik ?>ParamsData.optinfo, function(ke, va) {

										if(ke == "<?php echo $value; ?>"){

											var azerazer = ke.toString();
											sectionDyf.<?php echo $kunik ?>ParamsData.optinfo[azerazer] = $("#"+ko).val();
										}
									})

									tplCtx.value = sectionDyf.<?php echo $kunik ?>ParamsData.optinfo;

									mylog.log("save tplCtx",tplCtx);

									if(typeof tplCtx.value == "undefined")
										toastr.error('value cannot be empty!');
									else {
										dataHelper.path2Value( tplCtx, function(params) { 

										} );
									}

								} else if( ko == "optimage"){
									tplCtx.path = 'params.<?php echo $kunik ?>.optimage';
									tplCtx.value["optimage"] = $("#optimage").val();

									if(typeof tplCtx.value == "undefined")
										toastr.error('value cannot be empty!');
									else {
										dataHelper.path2Value( tplCtx, function(params) { 
											dyFObj.commonAfterSave(params,function(){

											});

										} );
									}
								} 

								$("#ajax-modal").modal('hide');
								reloadInput("<?php echo $cle ?>", "<?php echo (string)$form["_id"] ?>");

							});
						}
					}
				};

				$(".editone<?php echo $kunik ?>Params<?php echo $key ?>").off().on("click",function() {  
					tplCtx.id = $(this).data("id");
					tplCtx.collection = $(this).data("collection");
        //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>oneParams<?php echo $key ?>);
      });
				<?php
			}
		}
	//   	}
	// } 
		?>


		<?php if(count($paramsData["list"]) != 0){
			foreach ($paramsData["list"] as $key => $value) {
	  		// if($lbl == $key){
				if($paramsData["tofill"][$value] == "cplx"){
					?>

					sectionDyf.<?php echo $kunik ?>oneParamsPlaceholders<?php echo $key ?> = {
						"jsonSchema" : {	
							"title" : "<?php echo $kunik ?> config",
							"icon" : "fa-cog",
							"properties" : {
								placeholdersckb : {
									inputType : "text",
									label : "Modifier les textes à l'interieur du champs de saisie",
									value :  "<?php echo $paramsData["placeholdersckb"][$value]; ?>"
								}
							},
							save : function () {  
								tplCtx.value = {};
								$.each( sectionDyf.<?php echo $kunik ?>oneParamsPlaceholders<?php echo $key ?>.jsonSchema.properties , function(kk,vall) { 


									$.each(sectionDyf.<?php echo $kunik ?>ParamsData.placeholdersckb, function(ke, va) {
										if(ke == "<?php echo $value; ?>"){

											var azerazer = ke.toString();
											sectionDyf.<?php echo $kunik ?>ParamsData.placeholdersckb[azerazer] = $("#"+kk).val();

										}
									})


									tplCtx.value = sectionDyf.<?php echo $kunik ?>ParamsData.placeholdersckb;


								});
								mylog.log("save tplCtx",tplCtx);

								if(typeof tplCtx.value == "undefined")
									toastr.error('value cannot be empty!');
								else {
									dataHelper.path2Value( tplCtx, function(params) { 
										$("#ajax-modal").modal('hide');
										reloadInput("<?php echo $cle ?>", "<?php echo (string)$form["_id"] ?>");
									} );
								}

							}
						}
					};

					$(".editonep<?php echo $kunik ?>Params<?php echo $key ?>").off().on("click",function() {  
						tplCtx.id = $(this).data("id");
						tplCtx.collection = $(this).data("collection");
						tplCtx.path = $(this).data("path")+'.placeholdersckb';
        //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>oneParamsPlaceholders<?php echo $key ?>);
      });

					<?php
				}
			}
		}
	// } 
		?>

		$(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = $(this).data("path")+'.global';
        //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
        dyFObj.openForm(sectionDyf.<?php echo $kunik ?>Params);
      });


	});
</script>
<?php
}
?> <!-- else 164 -->