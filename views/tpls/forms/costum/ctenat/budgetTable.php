<?php 
//$styleT =  ( ($mode == "pdf") ? " border : 1px solid black ;" : "");
//var_dump($styleT);
$colspanplus  = 0 ;
if( $mode != "pdf" && ( $canAdminAnswer == true || $canEdit == true ) )
	$colspanplus  = 2;

if( $mode == "pdf" or $mode == "r"){
	$colspanplus = 0;
}

if( $mode == "pdf"){
	$classforpdf = "calendar-table";
}else {
	$classforpdf = "";
}


// *********** graphbuilder ***************** //
$labels = [];
	$labelsposte = [];

	$datasetsbudget = [];

	$datasetsbudgetnature = [];
	$datasetsbudgetposte = [];

	$yearLabels = [];

	if (sizeof($fromto) != 0) {
		$from = $fromto["from"];
		$to = $fromto["to"];
		while ( $from <= $to) {
			array_push($yearLabels, $from." (euros HT)");
	    	$from++;
		}
	}

	foreach ($answers as $i => $inp) {
		
		if(is_array($inp)){
			array_push($labels, $inp["poste"]);
			$dtbudgt = [];
			foreach ($inp as $x => $c) {
				if(strpos($x , "amount") !== false){
					array_push($dtbudgt , (int)$c);
				}
			}
			array_push($datasetsbudget, $dtbudgt);

		}
		
	}

	foreach ($answers as $i => $inp) {
		if (isset($inp["poste"]) && !in_array($inp["poste"], $labelsposte)) {
				array_push($labelsposte, $inp["poste"]);
        }
    }

    $defaultpost = "";
	foreach ($labelsposte as $key => $value) {
		$defaultpost = $value;
	}

	foreach ($paramsData["nature"] as $key => $value) {
		$deffaultnature = $value;
	}

	

	

	 

?>

<style type="text/css">
	.titlecolor<?php echo $kunik ?> {
		<?php 
			if( $mode != "pdf"){	
		?>
				color: <?php echo ($titleColor) ? $titleColor : "black"; ?>;
		<?php
			}
		?>
	}

/* Tabs panel */
	.tabbable-panel {
	  border:1px solid #eee;
	  padding: 10px;
	  margin: 25px 2px 20px 2px;
	}

	/* Default mode */
	.tabbable-line > .nav-tabs {
	  border: none;
	  margin: 0px;
	}
	.tabbable-line > .nav-tabs > li {
	  margin-right: 2px;
	}
	.tabbable-line > .nav-tabs > li > a {
	  border: 0;
	  margin-right: 0;
	  color: #737373;
	}
	.tabbable-line > .nav-tabs > li > a > i {
	  color: #a6a6a6;
	}
	.tabbable-line > .nav-tabs > li.open, .tabbable-line > .nav-tabs > li:hover {
	  border-bottom: 4px solid #fbcdcf;
	}
	.tabbable-line > .nav-tabs > li.open > a, .tabbable-line > .nav-tabs > li:hover > a {
	  border: 0;
	  background: none !important;
	  color: #333333;
	}
	.tabbable-line > .nav-tabs > li.open > a > i, .tabbable-line > .nav-tabs > li:hover > a > i {
	  color: #a6a6a6;
	}
	.tabbable-line > .nav-tabs > li.open .dropdown-menu, .tabbable-line > .nav-tabs > li:hover .dropdown-menu {
	  margin-top: 0px;
	}
	.tabbable-line > .nav-tabs > li.active {
	  border-bottom: 4px solid #f3565d;
	  position: relative;
	}
	.tabbable-line > .nav-tabs > li.active > a {
	  border: 0;
	  color: #333333;
	}
	.tabbable-line > .nav-tabs > li.active > a > i {
	  color: #404040;
	}
	.tabbable-line > .tab-content {
	  margin-top: -3px;
	  background-color: #fff;
	  border: 0;
	  border-top: 1px solid #eee;
	  padding: 15px 0;
	}
	.portlet .tabbable-line > .tab-content {
	  padding-bottom: 0;
	}

	/* Below tabs mode */

	.tabbable-line.tabs-below > .nav-tabs > li {
	  border-top: 4px solid transparent;
	}
	.tabbable-line.tabs-below > .nav-tabs > li > a {
	  margin-top: 0;
	}
	.tabbable-line.tabs-below > .nav-tabs > li:hover {
	  border-bottom: 0;
	  border-top: 4px solid #fbcdcf;
	}
	.tabbable-line.tabs-below > .nav-tabs > li.active {
	  margin-bottom: -2px;
	  border-bottom: 0;
	  border-top: 4px solid #f3565d;
	}
	.tabbable-line.tabs-below > .tab-content {
	  margin-top: -10px;
	  border-top: 0;
	  border-bottom: 1px solid #eee;
	  padding-bottom: 15px;
	}

</style>

<div class="form-group" style="overflow-x: auto;">

	<?php 
	if($mode == "r" || $mode == "pdf"){ ?>
		<div ><h4 class="titlecolor<?php echo $kunik ?> pdftittlecolor  text-center" style="padding-top:20px;"><?php echo $label ; ?></h4>

    <?php echo $info ?>
		</div>
	<?php 
	} else {
		?>
		<div ><h4 class="titlecolor<?php echo $kunik ?> pdftittlecolor" style="padding-top:20px;"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL ; ?>  <button type="button" class ="btn btn-default switch<?php echo $kunik; ?>"><i class="fa fa-pie-chart"></i></button> </h4>
		<?php echo $info ?>
		</div>
	<?php 
	} 
	if( $mode != "pdf"){

	?>
	<table class="table table-bordered table-hover directoryTable <?php echo $classforpdf; ?>"  id="<?php echo $kunik ; ?>table" style="width: 100%">
		
		<thead>	
			<?php 		
			if( count($answers)>0 ){ ?>
			<tr>
				<?php 
				
				foreach ($properties as $i => $inp) {
					echo "<th>".$inp["placeholder"]."</th>";
				} ?>

				<?php 
				if(	$mode != "pdf" && $mode != "r" && !empty($canAdminAnswer) && $canAdminAnswer == true ){ ?>
					<th></th>
				<?php
				} ?>
			</tr>
			<?php } ?>
		</thead>
		<tbody class="directoryLines">	
			<?php 
			$ct = 0;
			
			if(isset($answers)){
				foreach ($answers as $q => $a) {

					$tds = "";
					foreach ($properties as $i => $inp) {
						$tds .= "<td>";

						if( $i == "price" ) {
							if(!empty($a["price"]))
								$tds .= "<span id='price".$q."'>".$a["price"]."€</span>";
						} 
						else if( strpos($i, "amount") !== false && isset($a[$i]) ){
							$tds .= rtrim(rtrim(number_format($a[$i] , 3, ".", " "), '0'), '.')." €";
						}
						else if(isset($a[$i]))
							$tds .= $a[$i];
						
						$tds .= "</td>";
					}

					
						echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
							echo $tds;
							if(	$mode != "pdf" && 
								( 	$canAdminAnswer == true ||
									$canEdit == true ) ) {
								if( $mode != "pdf" and $mode != "r"){
						?>
							<td>
								<?php

								
									echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
										"canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
										"id" => $answer["_id"],
										"collection" => Form::ANSWER_COLLECTION,
										"q" => $q,
										"path" => $answerPath.$q,
										"kunik"=>$kunik ] );
								 ?>

								<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>','<?php echo $keyTpl ?>','<?php echo (string)$form["_id"] ?>')">
								<?php 
									echo PHDB::count(Comment::COLLECTION, [
										"contextId"=>(string)$answer["_id"],
										"contextType"=>"answers", 
										"path"=>$answer["_id"].$key.$q] ); ?> 
									<i class='fa fa-commenting'></i></a>
							</td>
					<?php 
							}
						}
						$ct++;
						echo "</tr>";
					
				}
			}

			$totalMap = [];
			foreach ( $properties as $i => $inp ) {
				if( isset($inp["propType"]) && $inp["propType"] == "amount" )
					$totalMap[$i] = 0;
			}

			if(isset($answers)){
				foreach ( $answers as $q => $a ) {	
					foreach ($totalMap as $i => $tot) {
						if(isset($a[$i]))
							$totalMap[$i] = $tot + $a[$i];
					}
				}
			}

			$colspNotpdf = 0;
			if( $mode != "pdf" and $mode != "r"){
				$colspNotpdf = 1;
			}

			$total = 0;
			foreach ( $totalMap as $i => $tot ) {
				if( $tot != 0 )
					$total = $total + $tot ;
			}

			if($total > 0){
				
				echo "<tr class='bold'>";
				echo 	'<td class="specialword" colspan="2" style="text-align:right"> TOTAL : </td>';
				foreach ($totalMap as $i => $tot) {
					echo 	"<td>".rtrim(rtrim(number_format($tot , 3, ".", " "), '0'), '.')." €</td>";
				}
					
				
				echo "</tr>";

				echo "<tr class='bold'>";
				echo 	'<td class="specialword" colspan="'.(count( $paramsData["amounts"] )).'" style="text-align:right"> BUDGET TOTAL : </td>';
				//if($mode != "r" && $mode != "pdf")
					echo 	'<td colspan="'.($colspanplus -1 + $colspNotpdf ).'">'.rtrim(rtrim(number_format($total , 3, ".", " "), '0'), '.').' €</td>';
				//else
					//echo "<td>".rtrim(rtrim(number_format($total , 3, ".", " "), '0'), '.')." €</td>";
				echo "</tr>";
			}

	

			?>
		</tbody>
	</table>

	<?php
	}else{
	?>

	<table class="table table-bordered table-hover directoryTable <?php echo $classforpdf; ?>"  id="<?php echo $kunik ; ?>table" style="width: 100%">
		
		<thead>	
			<?php 		
			if( count($answers)>0 ){ ?>

			<tr>
				<?php 
				
				foreach ($properties as $i => $inp) {
					
					if ($i != "poste" && $i != "nature") {
						echo "<th>".$inp["placeholder"]."</th>";
					}
					
				} ?>

			</tr>
			<?php } ?>
		</thead>
		<tbody class="directoryLines">	
			<?php 
			$ct = 0;
			
			if(isset($answers)){
			?>

			<?php
				foreach ($answers as $q => $a) {

					$tds = "";
					foreach ($properties as $i => $inp) {
					if ($i != "poste" && $i != "nature") {
						$tds .= "<td>";

						if( $i == "price" ) {
							if(!empty($a["price"]))
								$tds .= "<span id='price".$q."'>".$a["price"]."€</span>";
						} 
						else if(isset($a[$i]) && is_numeric($a[$i]) && strpos($i, "amount") !== false && isset($a[$i]) ){
							$tds .= rtrim(rtrim(number_format($a[$i] , 3, ".", " "), '0'), '.')." €";
						}
						else if(isset($a[$i]))
							$tds .= $a[$i];
						
						$tds .= "</td>";
					}
					}

					
						?>

						<tr><td colspan="<?php echo sizeof($yearLabels); ?>">
							<b> Nature de l’action : </b> <?php echo @$a["nature"] ?>  <br>
							<b> Poste de dépense : </b> <?php echo @$a["poste"] ?>
						</td></tr>

						<?php
							echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
							echo $tds;
			
						$ct++;
						echo "</tr>";
					
				}
			}

			$totalMap = [];
			foreach ( $properties as $i => $inp ) {
				if( isset($inp["propType"]) && $inp["propType"] == "amount" )
					$totalMap[$i] = 0;
			}

			if(isset($answers)){
				foreach ( $answers as $q => $a ) {	
					foreach ($totalMap as $i => $tot) {
						if(isset($a[$i]) && is_numeric($a[$i]))
							$totalMap[$i] = $tot + $a[$i];
					}
				}
			}

			$colspNotpdf = 0;
			if( $mode != "pdf" and $mode != "r"){
				$colspNotpdf = 1;
			}

			$total = 0;
			foreach ( $totalMap as $i => $tot ) {
				if( $tot != 0 )
					$total = $total + $tot ;
			}

			if($total > 0){

				echo "<tr class='bold'>";
					echo 	'<td class="specialword" colspan="'.(sizeof($yearLabels)).'"> TOTAL : </td>';
				echo "</tr>";
				
				echo "<tr class='bold'>";
				
				foreach ($totalMap as $i => $tot) {
					echo 	"<td>".rtrim(rtrim(number_format($tot , 3, ".", " "), '0'), '.')." €</td>";
				}
				
				echo "</tr>";


				echo "<tr class='bold'>";
					echo 	'<td class="specialword" colspan="'.(sizeof($yearLabels) - 1).'" style="text-align:right"> TOTAL : </td>';
					echo 	'<td>'.rtrim(rtrim(number_format($total , 3, ".", " "), '0'), '.').' €</td>';

				echo "</tr>";

			}

	

			?>
		</tbody>
	</table>

	<?php	
	}	
	?>




	<?php 
	if($mode != "pdf"){  
	?>
	<div class="hide" id="<?php echo $kunik ; ?>graph">	
		<div class="col-md-12 ">	
			 <div class="tabbable-panel">
				<div class="tabbable-line">
					<ul class="nav nav-tabs ">
						<li class="active">
							<a href="#tab_default" data-toggle="tab">
							Résumé </a>
						</li>
						<?php 
						foreach ($paramsData["graph"] as $panelId => $panelValue) { 
						?>
							<li>
								<a href="#tab_default_<?php echo $panelId; ?>" data-toggle="tab">
									<?php echo $panelValue["label"]; ?> 
								</a>
							</li>
						<?php 
						}
						?>
						<li>
							<a href="javascript:;" class="addgraph" style="color: #43a9b2;">
							Ajouter</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_default">
							<div class="row">
								<div class="col-md-10" id="<?php echo $kunik ; ?>graphcontainer">
									<span id="<?php echo $kunik ; ?>budgetchargement">En cours de chargement</span>
								</div>

								<div class="col-md-2 ">
									
								</div>
							</div>
						</div>

						<?php 
						foreach ($paramsData["graph"] as $panelId => $panelValue) { 
						?>
							<div class="tab-pane" id="tab_default_<?php echo $panelId; ?>">
								<div class="row">
									<div class="col-md-10" id="<?php echo $kunik.$panelId ; ?>graphcontainer">
										
									</div>
									<div class="col-md-2">
										<a href='javascript:;' data-id='<?php echo $parentForm["_id"]; ?>' data-collection='<?php echo Form::COLLECTION ?>' data-key='<?php echo $panelId; ?>' data-path='graph.<?php echo $panelId; ?>' class='deleteLine previewTpl btn btn-xs btn-danger'><i class='fa fa-times'></i></a>
										 <div class="form-group">
										 	<?php 
										 	$selectedOp = "";
										 	$subtypeOp = "default";

										 	if (isset($panelValue["subtype"])) {
										 		$subtypeOp = $panelValue["subtype"];
										 	} 
										 	if($panelValue["type"] == "amount") {
										 		if (!isset($panelValue["subtype"])) {
											 		$subtypeOp = $from;
											 	} 
										 	?>
										    
											    <select class="form-control changesubtype" id="<?php echo $panelId; ?>ControlSelect1" data-path="<?php echo $panelId; ?>.subtype">
												     <?php
												     		if (sizeof($fromto) != 0) {
																$frm = $fromto["from"];
																$tto = $fromto["to"];
																while ( $frm <= $tto) {
																	if($subtypeOp == $frm) {
																		$selectedOp = "selected";
																	}else {
																		$selectedOp = "";
																	}

																	echo "<option ".$selectedOp." value='".$frm."' > ".$frm."</option>";
															    	$frm++;
																}
															}
												     ?>
												      
											    </select>

											<?php 
												} elseif ($panelValue["type"] == "poste") {
													if (!isset($panelValue["subtype"])) {
												 		$subtypeOp = $defaultpost;
												 	} 
											?>
												<select class="form-control changesubtype" id="<?php echo $panelId; ?>ControlSelect1" data-path="<?php echo $panelId; ?>.subtype">
												     <?php
												     	foreach($answers as $pstid => $pstvalue){
												     		if($subtypeOp == $pstvalue['poste']) {
																		$selectedOp = "selected";
																	}else {
																		$selectedOp = "";
																	}
															echo "<option ".$selectedOp." value='".$pstvalue['poste']."' > ".$pstvalue['poste']."</option>";
														}
												     ?>
												      
											    </select>

											<?php 
												} elseif ($panelValue["type"] == "nature") {
													if (!isset($panelValue["subtype"])) {
												 		$subtypeOp = $deffaultnature;
												 	} 
											?>
												<select class="form-control changesubtype" id="<?php echo $panelId; ?>ControlSelect1" data-path="<?php echo $panelId; ?>.subtype">
												     <?php
												     	foreach($paramsData["nature"] as $pstid => $pstvalue){
												     		if($subtypeOp == $pstvalue) {
																		$selectedOp = "selected";
																	}else {
																		$selectedOp = "";
																	}
															echo "<option ".$selectedOp." value='".$pstvalue."' > ".$pstvalue."</option>";
														}
												     ?>
												      
											    </select>

											<?php	
													}
												?>
										  </div>
										
									</div>
								</div>
							</div>
						<?php 
						}
						?>

					</div>
				</div>
			</div>
		</div>	
	</div>
	<?php
	}
	?>
</div>


<?php 
	if($mode != "r" and $mode != "pdf"){ 
	?>

<script type="text/javascript">

	var yearLabels = <?php echo json_encode($yearLabels); ?>;

	var <?php echo $kunik ; ?>Datachart = {
		"label" : yearLabels ,
		"labels" : <?php echo json_encode($labels); ?>,
		"datasets" : <?php echo json_encode($datasetsbudget); ?>
	} ;

	<?php 
			foreach ($paramsData["graph"] as $panelId => $panelValue) { 
				if (!isset($panelValue["subtype"])) {
					if($panelValue["type"] == "nature") {
	?>
						var <?php echo $kunik.$panelId; ?>Data = {
							"label" : <?php echo json_encode($yearLabels); ?>,
							"dataset" : <?php echo json_encode(Ctenat::getdatasetsnature($deffaultnature, $labels, $datasetsbudget, $answers, $paramsData["nature"])); ?>,
							"title" : "<?php echo $deffaultnature; ?>"
						}
	<?php
					} elseif ($panelValue["type"] == "poste") {
	?>
						var <?php echo $kunik.$panelId; ?>Data = {
							"label" : <?php echo json_encode($yearLabels); ?>,
							"dataset" : <?php echo json_encode(Ctenat::getdatasetsposte($defaultpost, $labels, $datasetsbudget)); ?>,
							"title" : "<?php echo $defaultpost; ?>"
						}
	<?php
					} elseif ($panelValue["type"] == "amount") {
						if (sizeof($fromto) != 0) {
	?>
						var <?php echo $kunik.$panelId; ?>Data = {
							"label" : <?php echo json_encode($labelsposte); ?>,
							"dataset" : <?php echo json_encode(Ctenat::getdatasetsamount($from,$answers,$datasetsbudget, $fromto)); ?>,
							"title" : "<?php echo $from; ?>"
						}
	<?php
						}
					}
	?>

	<?php 
					} else {

					if($panelValue["type"] == "nature") {
	?>
						var <?php echo $kunik.$panelId; ?>Data = {
							"label" : <?php echo json_encode($yearLabels); ?>,
							"dataset" : <?php echo json_encode(Ctenat::getdatasetsnature($panelValue["subtype"], $labels, $datasetsbudget, $answers, $paramsData["nature"])); ?>,
							"title" : "<?php echo $panelValue["subtype"]; ?>"
						}
	<?php
					} elseif ($panelValue["type"] == "poste") {
	?>
						var <?php echo $kunik.$panelId; ?>Data = {
							"label" : <?php echo json_encode($yearLabels); ?>,
							"dataset" : <?php echo json_encode(Ctenat::getdatasetsposte($panelValue["subtype"], $labels, $datasetsbudget)); ?>,
							"title" : "<?php echo $panelValue["subtype"]; ?>"
						}
	<?php
					} elseif ($panelValue["type"] == "amount") {
						if (sizeof($fromto) != 0) {
	?>
						var <?php echo $kunik.$panelId; ?>Data = {
							"label" : <?php echo json_encode($labelsposte); ?>,
							"dataset" : <?php echo json_encode(Ctenat::getdatasetsamount($panelValue["subtype"],$answers,$datasetsbudget, $fromto)); ?>,
							"title" : "<?php echo $panelValue["subtype"]; ?>"
						}
	<?php
						}
					}
	

					}
				}
						?>
	
	$(document).ready(function() { 

		if (ajaxPost('#<?php echo $kunik ; ?>graphcontainer', baseUrl+'/graph/co/dash/g/graph.views.co.costum.ctenat.ctebarMany'+"/id/<?php echo $kunik ?>", null, function(){},"html")) 
		{
			$("$<?php echo $kunik ; ?>budgetchargement").addClass('hide');
		}

		<?php 
			foreach ($paramsData["graph"] as $panelId => $panelValue) {
		?> 
			ajaxPost('#<?php echo $kunik.$panelId ; ?>graphcontainer', baseUrl+'/graph/co/dash/g/graph.views.co.costum.ctenat.ctepie'+"/id/<?php echo $kunik.$panelId ?>", null, function(){},"html");
		<?php
			}
		?>

		$(".switch<?php echo $kunik ?>").off().on("click",function() {  
	        $("i", this).toggleClass("fa-table");
	        $("i", this).toggleClass("fa-pie-chart");
	        $("#<?php echo $kunik ; ?>graph").toggleClass("hide");
	        $("#<?php echo $kunik ; ?>table").toggleClass("hide");

		});

		
	});
</script>
<?php
} 
?>