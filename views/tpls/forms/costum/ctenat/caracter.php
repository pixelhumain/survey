<div id='caracter' class='col-sm-12 ' style='padding-bottom:40px'>
	<div class='col-xs-12'>
		
		<div class='col-xs-12'>
			<h3 class="text-center" style="color:#16A9B1;padding-top:20px;">Caractérisation 
			<?php if( $canEdit == true && ( $mode == "w" || $mode == "fa") ){ ?><a href='javascript:'  data-form='caracter' data-step='caracter' class='editCaracter btn btn-default'><i class='fa fa-pencil'></i></a><?php } ?></h3>
			<div class='margin-bottom-20' style="border-bottom:1px solid #666;padding-bottom: 20px;margin-bottom: 20px;">
			La caractérisation de l’action permet à l’ensemble des actions du territoire de s’inscrire dans un référentiel commun permettant ensuite le classement des actions dans le moteur de recherche, dans les graphiques du tableau de bord... Il vous est demandé de choisir parmis les propositions le périmètre d’application de l’action ainsi que les finalités de transition écologique (cibles développement durable) jugées prioritaires. Vos choix nous permettront par ailleurs de vous proposer des indicateurs de suivi. <!-- <a href="javascript:;" class="modalInfo" data-poi="5d1e033e40bb4ed6549bf68d"><i style="color: #16A9B1" class="fa fa-info-circle fa-2x"></i></a> -->
			</div>
		</div>
        
		<div class='col-xs-12'>
			<h3 class="text-center" style="color:#24A9B1;padding-top:20px;">SELECTION DES DOMAINES D’ACTION</h3>
			<div class='margin-bottom-20' style="border-bottom:1px solid #666;padding-bottom: 20px;margin-bottom: 20px;">
				Les domaines d’action indiquent la nature technique de l’action. Il peut s’agir de grands corps de métier. Une même action peut couvrir plusieurs domaines d’action. Le domaine d’action principal correspond au “coeur de métier” de l’action. Les domaines secondaires qualifient des facettes moins centrales de l’action.


				<style type="text/css">
					.action{
						font-size:1.5em; 
						font-weight:bold;
						background-color:#3D85C6;
						border-radius: 5px;
						padding:10px;
						margin: 20px;
						}
					.actionSub{
						font-size:1.5em; 
						font-weight:bold;
						border: 1px solid #3D85C6;
						border-radius: 5px;
						padding:10px;
						margin: 20px;
						}
					.wimg{
						width : 50px;
						margin:5px;
					}
				</style>

				<h4 class="margin-top-20">Domaine d’action principal (obligatoire)</h4>
				<?php 
				$answers = (isset($answer["answers"]["caracter"])) ? $answer["answers"]["caracter"] : null;
				
				$beflink="";
				$aflink="";

					$beflink = "<a href='javascript:'  data-form='caracter' data-step='caracter' class='editCaracter'>";
					$aflink = "</a>";


				if(isset($answers["actionPrincipal"])){		
					$img = "";
					foreach (@$this->costum["badges"]["domainAction"] as $ki => $vi) {
						if( $vi["name"] == $answers["actionPrincipal"] && isset($vi["profilThumbImageUrl"]) )
							$img = "<img class='wimg' src='".Yii::app()->createUrl($vi["profilThumbImageUrl"])."'/>";
					}
				} else 
					$answers["actionPrincipal"] = "<i class='fa fa-question-circle text-white'></i>";
				
				echo "<div class='text-center text-white action'>".
						$beflink.$answers["actionPrincipal"].$aflink.
					"</div>"; 
				
				?>

				<h4 class="margin-top-20">Domaine(s) d’action secondaire(s)</h4>
				<?php 
				if(!empty($answers["actionSecondaire"])){
					foreach ($answers["actionSecondaire"] as $k => $v ) {
						$img = "";
						foreach (@$this->costum["badges"]["domainAction"] as $ki => $vi) 
						{
							if( $vi["name"] == $v && isset($vi["profilThumbImageUrl"]) )
								$img = "<img class='wimg' src='".Yii::app()->createUrl($vi["profilThumbImageUrl"])."'/>";
						}
						echo "<div class='text-center actionSub'>".
							$beflink.$img.$v.$aflink.
						"</div>";
					}
				} else {
					echo "<div class='text-center actionSub'>".
							$beflink."<i class='fa fa-question-circle text-dark'></i>".$beflink.
						"</div>";
					} ?>
			</div>
		</div>


		<div class='col-xs-12'>
			<h3 class="text-center" style="color:#24A9B1;padding-top:20px;">SELECTION DES OBJECTIFS</h3>
			<div class='margin-bottom-20' style="border-bottom:1px solid #666;padding-bottom: 20px;margin-bottom: 20px;">
				Les objectifs de l’action qui vous sont proposés sont structurées autour des 5 finalités des agenda 21.
				
				<h4 class="margin-top-20">Objectif principal (obligatoire)</h4>
				<?php 
				if(isset($answers["cibleDDPrincipal"])){	
					$img = "";
					foreach (@$this->costum["badges"]["cibleDD"] as $ki => $vi) {
						if( $vi["name"] == $answers["cibleDDPrincipal"] && isset($vi["profilThumbImageUrl"]) )
							$img = "<img class='wimg' src='".Yii::app()->createUrl($vi["profilThumbImageUrl"])."'/>";
					}
					if(is_array($answers["cibleDDPrincipal"])){
						echo "<div class='text-center text-white action'>".
							$beflink.$img.$answers["cibleDDPrincipal"][0].$aflink.
						"</div>";
					} else {
						echo "<div class='text-center text-white action'>".
							$beflink.$img.$answers["cibleDDPrincipal"].$aflink.
						"</div>";
					}
					
				} else {
					echo "<div class='text-center text-white action'>".
							$beflink."<i class='fa fa-question-circle text-white'></i>".$aflink.
						"</div>";
				}
				?>

				<h4 class="margin-top-20">Objectif(s) secondaire(s)</h4>
				<?php  
				if(isset($answers["cibleDDSecondaire"])){
					foreach ($answers["cibleDDSecondaire"] as $k => $v ) {
						$img = "";
						foreach (@$this->costum["badges"]["cibleDD"] as $ki => $vi) {
							if( $vi["name"] == $v && isset($vi["profilThumbImageUrl"]) )
								$img = "<img class='wimg' src='".Yii::app()->createUrl($vi["profilThumbImageUrl"])."'/>";
						}
						echo "<div class='text-center text-white actionSub'>".$beflink.$img.$v.$aflink."</div>";
					}
				}else {
					echo "<div class='text-center actionSub'>".$beflink.
							"<i class='fa fa-question-circle text-dark'></i>".$aflink.
						"</div>";
					} ?>
			</div>
		</div>

        <?php
            $anns = [];
            $indicateurs = Ctenat::getIndicator();
            if(isset($answer["answers"]["murir"]["results"]) && !empty($valuei["indicateur"])){
                foreach ($answer["answers"]["murir"]["results"] as $keyi => $valuei) {
                    if ($keyi != "0") {
                        array_push($anns, $valuei["indicateur"]);
                    }
                }
            }


        ?>

		<div class='col-xs-12'>
			<h3 class="text-center" style="color:#24A9B1;padding-top:20px;">INDICATEUR</h3>
			<div class='margin-bottom-20' style="border-bottom:1px solid #666;padding-bottom: 20px;margin-bottom: 20px;">
				
				<h4 class="margin-top-20">Indicateur principal</h4>

				<div class='text-center text-white action'>
					<?php echo (isset($answer["answers"]["murir"]["results"]["0"]["indicateur"]) && isset($indicateurs[$answer["answers"]["murir"]["results"]["0"]["indicateur"]]) ) ? $beflink.$indicateurs[$answer["answers"]["murir"]["results"]["0"]["indicateur"]].$beflink : $beflink."<i class='fa fa-question-circle text-white'></i>".$aflink; ?>
				</div>

				<?php
				?>
				
			</div>
		</div>

        <?php
            $murirForm = PHDB::findOne(Form::COLLECTION, array('id' => 'murir'));
        ?>
		
	</div>
</div>

<?php
if ($mode != "pdf"){
?>

<script type="text/javascript">
    var caracter<?php echo $kunik ?>Data = <?php echo json_encode($answers); ?>;

    var caracterDF = {
        jsonSchema : {
            title : "Caractériser l’action",
            type : "object",
            properties : {
                actionPrincipal : {
                    "inputType" : "select",
                    "label" : "Domaine d'action principal",
                    "placeholder" : "Choisir un domaine d'action principal",
                    "groupOptions" : true,
                    "groupSelected" : false,
                    "list" : "domainAction",
                    "select2" : true,
                    "value" : caracter<?php echo $kunik ?>Data.actionPrincipal
                },
                actionSecondaire : {
                    "inputType" : "select",
                    "label" : "Domaines(s) d’action secondaire(s)",
                    "placeholder" : "Choisir un domaine d'action secondaire",
                    "groupOptions" : true,
                    "groupSelected" : false,
                    "list" : "domainAction",
                    "select2" : {
                        "multiple" : true
                    },
                    "value" : caracter<?php echo $kunik ?>Data.actionSecondaire
                },
                cibleDDPrincipal : {
                    "inputType" : "select",
                    "label" : "Objectif principal",
                    "placeholder" : "Choisir une cible de développement durable",
                    "groupOptions" : true,
                    "groupSelected" : false,
                    "list" : "cibleDD",
                    "select2" : true,
                    "value" : caracter<?php echo $kunik ?>Data.cibleDDPrincipal
                },
                cibleDDSecondaire : {
                    "inputType" : "select",
                    "label" : "Objectif(s) secondaire(s)",
                    "placeholder" : "Choisir une cible de développement durable secondaire",
                    "list" : "cibleDD",
                    "groupOptions" : true,
                    "groupSelected" : false,
                    "select2" : {
                        "multiple" : true
                    },
                    "value" : caracter<?php echo $kunik ?>Data.cibleDDSecondaire
                },
                indicateur : {
                    "inputType" : "select",
                    "label" : "Indicateur",
                    "placeholder" : "Indicateur",
                    "list" : "indicateursPrincipaux",
                    "select2" : {
                        "multiple" : false
                    },
                    "value" : "<?php echo (isset($answer["answers"]["murir"]["results"]["0"]["indicateur"])) ? $answer["answers"]["murir"]["results"]["0"]["indicateur"] : ''; ?>"
                }/*,
                indicateurSecondaire : {
                    "inputType" : "select",
                    "label" : "Indicateur Secondaire",
                    "placeholder" : "Indicateur Secondaire",
                    "list" : "indicateurs",
                    "select2" : {
                        "multiple" : true
                    },
                    "value" : <?php echo json_encode($anns); ?>
                }*/
            },
            save : function(){

                mylog.log("save caracter","answerId",answerId);
                data = {
                    collection : "answers",
                    id : answerId,
                    path : "answers.caracter",
                    value : {}
                }
                if($("#actionPrincipal").val() &&  $("#actionPrincipal").val() != "")
                    data.value.actionPrincipal = $("#actionPrincipal").val();
                if($("#actionSecondaire").val() && $("#actionSecondaire").val() != "")
                    data.value.actionSecondaire = $("#actionSecondaire").val();
                if($("#cibleDDPrincipal").val() && $("#cibleDDPrincipal").val()!= "")
                    data.value.cibleDDPrincipal = $("#cibleDDPrincipal").val();
                if($("#cibleDDSecondaire").val() && $("#cibleDDSecondaire").val() != "")
                    data.value.cibleDDSecondaire = $("#cibleDDSecondaire").val();

                mylog.log('save caracter ',data);  
                if( Object.keys(data.value).length > 0 )
                    dataHelper.path2Value( data, function(){
                        reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
                        reloadInput("results", "<?php echo (string)$murirForm["_id"] ?>");
                    } );


                mylog.log("save indicateur","answerId",answerId);
                dataind = {
                    collection : "answers",
                    id : answerId,
                    path : "answers.murir.results.0",
                    value : {}
                }

                if($("#indicateur").val() && $("#indicateur").val()!= ""){
                    dataind.value = { "indicateur" : $("#indicateur").val() } ;
                    mylog.log('save indicateur ',data);

                    if( Object.keys(dataind.value).length > 0 )
                        dataHelper.path2Value( dataind, function(){
                            if(Object.keys(data.value).length <= 0){
                                reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
                                reloadInput("results", "<?php echo (string)$murirForm["_id"] ?>");
                            }
                    } );

                }
    /*
                if($("#indicateurSecondaire").val()){

                    dataindsec = {
                        collection : "answers",
                        id : answerId
                    }
                    var indSec = [];

                    <?php
                        if(sizeof($anns) > 0) {
                    ?>
                        indSec = <?php echo json_encode($anns); ?>;
                    <?php
                        }
                    ?>

                    if ($("#indicateurSecondaire").val()) {

                        if(indSec <= $("#indicateurSecondaire").val()){
                            $.each($("#indicateurSecondaire").val(), function( index, value ) {
                                dataindsec.path = "answers.murir.results."+(index+1);
                                dataindsec.value = {};
                                dataindsec.value = { "indicateur" : value };
                                dataHelper.path2Value( dataindsec, function(){} );
                            });
                        } else {
                            $.each(indSec, function( index, value ) {
                                if(typeof $("#indicateurSecondaire").val()[index] != "undefiend"){
                                    dataindsec.path = "answers.murir.results."+(index+1);
                                    dataindsec.value = {};
                                    dataindsec.value = { "indicateur" : $("#indicateurSecondaire").val()[index] };
                                        dataHelper.path2Value( dataindsec, function(){} );
                                    } else {
                                    dataindsec.path = "answers.murir.results."+(index+1);
                                    dataindsec.value = {};
                                    dataHelper.path2Value( dataindsec, function(){} );
                                }
                            });
                        }
                    }

                }*/


                //alert("close ?")
                dyFObj.closeForm();
                // reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
                // reloadInput("results", "<?php echo (string)$murirForm["_id"] ?>");

            }
        }
    };

    $(document).ready(function() {

        mylog.log("render","/modules/survey/views/tpls/forms/costum/ctenat/caracter.php");
        $('.editCaracter').off().click(function() {
            dyFObj.dynFormCostum = null;
            dyFObj.openForm(caracterDF);

        });
    });

</script>

<?php
}
?>



