
<?php if( $mode != "pdf" and $mode != "r"){	?>
<div class='col-sm-offset-2 col-sm-8 margin-bottom-20' style="color:white; border:4px solid #0B5394; background-color: #3D85C6;">
	<b>Nota Bene</b> : les informations contenues dans cette étape ne sont pas publiques, elle seront visibles uniquement par les partenaires publics de votre projet de territoire  (collectivité(s) porteuse(s), services de l’Etat, ADEME, banque des territoires, CEREMA…) et par les partenaires que vous auriez vous-même ajouté (onglet “Administration”). Ces partenaires sont visibles dans l’onglet “Communauté” de l’action.<br/><br/>
	Une fois l’étape validée, tous ces partenaires pourront vous donner leur avis et vous apporter leur aide sous forme de commentaires.

</div>

<?php if(isset($canAdminAnswer) && !empty($canAdminAnswer)){ ?>
	<div class="col-xs-12 text-center padding-10">
		<button id="generateCopil" class="generateCopil  btn btn-primary" data-id="<?php echo $_GET["id"] ?>" data-date="<?php echo date("Y-m-d") ?>">Générer un copil</button>
	</div>
<?php 	} ?>

<div id="copilFile" class="col-xs-12">
  <div class="col-xs-12 table table-bordered table-hover  ">
    <h3 style="color:#16A9B1">Compte-rendu de COPIL</h3>
  <?php $files=Document::getListDocumentsWhere(array("id"=>$_GET["id"], "type"=>Form::ANSWER_COLLECTION, "doctype"=>"file", "subKey"=>array('$exists'=>false)),"file");
    if(!empty($files)){ ?>
      <div class="content-copil">
      <?php foreach($files as $k => $v){ ?>
        <div class='col-xs-12 padding-5 shadow2 margin-top-5 margin-bottom-5' id="<?php echo $k ?>">
          <a href='<?php echo $v["docPath"] ?>' target='_blank' class="link-files"><i class="fa fa-file-pdf-o text-red"></i> <?php echo $v["name"] ?></a>
          <?php if($canAdminAnswer){ ?>
            <a href='javascript:;' class="pull-right text-red btn-remove-document" data-id="<?php echo $k ?>"><i class="fa fa-trash"></i> <?php echo Yii::t("common","Delete") ?></a>
          <?php } ?>
        </div>
    <?php } ?>
      </div>
  <?php }else{
      echo '<div class="content-copil">';
      echo "<span class='italic noCopilFile'>Aucun copil n'a été généré</span>";
      echo "</div>";
    }
  ?>
  </div>
</div>

<div class="col-xs-12">
	<?php $countComment=PHDB::count(Comment::COLLECTION, array("contextId"=>$_GET["id"],"contextType"=>Form::ANSWER_COLLECTION, "path"=>"murir")); ?>
	<h3 style="color:#16A9B1">Commentaire général sur l'action</h3>
	<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('<?php echo Form::ANSWER_COLLECTION?>','<?php echo $_GET["id"] ?>','murir', 'Commentaires générales de la candidature')"><i class="fa fa-commenting"></i> <?php echo $countComment ?> commentaire<?php if($countComment>1) echo "s" ?></a>
</div>

<script type="text/javascript">
$(document).ready(function() { 
  documentManager.bindEvents();
	$(".generateCopil").off().click(function(){
		idAnswer=$(this).data("id"),
		dataNow=$(this).data("date");
		var titleCopil=$(this).data("title");
		var subKey=$(this).data("key")
		//window.open(baseUrl+"/costum/ctenat/generatecopil/id/"+idAnswer+"/title/COPIL010203/date/"+dataNow);
        msgBoot='<div class="row">  ' +
                      '<div class="col-md-12"> ' +
                        '<form class="form-horizontal"> ' ;
                          if(!notNull(titleCopil)){
                         msgBoot+= '<label class="col-md-12 no-padding" for="awesomeness">Nom du copil : </label><br> ' +                        
                          '<input type="text" id="nameFavorite" class="nameSearch wysiwygInput form-control" value="" style="width: 100%" placeholder="'+tradDynForm.addname+'..."></input>'
                      		}
                         msgBoot += "<br>"+
                    		'<label class="col-md-12 no-padding" for="awesomeness">Noms de participants : </label><br> ' +
                          	'<textarea class="col-xs-12" id="participantsCopil"></textarea><br>'+
                          	'<label class="col-md-12 no-padding" for="awesomeness">Date du copil à récupérer : </label><br> ' +
                          	'<input id="dateCopil" type="date" value="'+dataNow+'">'+
                    		'</div> </div>' +
                    '</form></div></div>';       
		bootbox.dialog({
            title: "Remplissez les informations du COPIL",
            message: msgBoot,
            buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {
                    	
                    	var dialog = bootbox.dialog ({
						    message: '<p class="text-center mb-0 loadingPdf"></p>',
						    closeButton: false
						});
            			
            			coInterface.showLoader(".loadingPdf", "Création du copil en cours");
            			$(".loadingPdf .processingLoader").removeClass("col-xs-12 margin-top-50");
                        
                        var formData={
                          "title": (notNull(titleCopil)) ? titleCopil : $("#nameFavorite").val(),
                          "participants":$("#participantsCopil").val(),
                          "date":$("#dateCopil").val(),
                          "copil":true
                        };
                        if(notNull(subKey))
                        	formData.subKey=subKey;
                        
                        $.ajax({
						  type: "POST",
						  url: baseUrl+"/costum/ctenat/generatecopil/id/"+idAnswer,
						  data: formData,
						  success: function(data){
						  		toastr.success("Le copil a été créé avec succès");
						  		targetDom=(notNull(subKey))? "#copilFile"+subKey+" .content-copil": "#copilFile .content-copil";
						  		$(targetDom+ ".noCopilFile").remove();
						  		$(targetDom).prepend("<div class='col-xs-12 padding-5 shadow2 margin-top-5 margin-bottom-5'>"+"<a href='"+baseUrl+"/"+data.docPath+"' target='_blank' class='link-files'><i class='fa fa-file-pdf-o text-red'></i> "+ data.fileName+"</a>"+
											"</div>");
						  		coInterface.scrollTo("#copilFile");
								// do something in the background
								dialog.modal('hide');

						  },
						  dataType: "json"
						});
                    }
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: function() {
                  }
                }
              }
      	});
	});
});

</script>
<?php }	?>