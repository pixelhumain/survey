<h1>Selecteur d'Indicateurs</h1>

<style type="text/css">
	.indicList{list-style: none;}
	.indicList li{
		border:1px solid #ccc;
		padding : 5px;
	}
	.searchIndicFilter {
		width:60%;
	}
</style>

<div id="ficheIndic" class="col-md-12 margin-20" >

</div>

<div class="col-md-12 margin-20" >
	<i class='fa fa-search  fa-2x padding-5'></i> <input name='searchIndic1' class='searchIndicFilter' placeholder='Filtrer'>
	<br/> <a href='javascript:$("#openModal").modal("hide");dyFObj.openForm("indicator");' class='btn btn-primary margin-top-5 margin-bottom-5'><i class='fa fa-plus'></i> Créer un nouvel indicateur </a>
</div>


<div class="col-md-4">
	<h2>Indicateurs Principaux</h2>	

	<ul class="indicList">
		<?php 
		echo count($poilistPrinci);
		
		foreach ($poilistPrinci as $indid => $i) {
			$liClass = (isset($results[$indid])) ? "bg-green" : "";
			$liLink = (isset($results[$indid])) ? $i["name"] : "<a href='javascript:indicInfo(\"".$indid."\")'>".$i["name"]."</a>";
			echo "<li class='".$liClass."'>".$liLink."</li>";
		}
		 ?>
	</ul>
</div>

<div class="col-md-4">
	<h2>Indicateurs Liés au domaine d'action</h2>

	<ul class="indicList">
		<?php 
		echo count($poilistDA);
		foreach ($poilistDA as $indid => $i) {
			$liClass = (isset($results[$indid])) ? "bg-green" : "";
			$liLink = (isset($results[$indid])) ? $i["name"] : "<a href='javascript:indicInfo(\"".$indid."\")'>".$i["name"]."</a>";
			echo "<li class='".$liClass."'>".$liLink."</li>";
		} ?>
	</ul>
</div>

<div class="col-md-4">
	<h2>Autres Indicateurs</h2>

	<ul class="indicList"
>		<?php 
		echo count($poilist);
		foreach ($poilist as $indid => $i) {
			$liClass = (isset($results[$indid])) ? "bg-green" : "";
			$liLink = (isset($results[$indid])) ? $i["name"] : "<a href='javascript:indicInfo(\"".$indid."\")'>".$i["name"]."</a>";
			echo "<li class='".$liClass."'>".$liLink."</li>";
		} ?>
	</ul>
</div>

<script type="text/javascript">
jQuery(document).ready(function() {	
	$('.searchIndicFilter').keyup(function(){

	    var that = this, $allListElements = $('ul.indicList > li');

	    var $matchingListElements = $allListElements.filter(function(i, li){
	        var listItemText = $(li).text().toUpperCase(), 
	            searchText = that.value.toUpperCase();
	        return ~listItemText.indexOf(searchText);
	    });

	    $allListElements.hide();
	    $matchingListElements.show();

	});
});

function emptyFiche(){
	$("#ficheIndic").html("");
}

function indicInfo(ind){

        $("#openModal").animate({ scrollTop: "0" }, 500);
	
	    ajaxPost(null, baseUrl+"/costum/ctenat/getindicateur/id/"+ind, null,
			function(data){
			console.log(data.result[ind]);
			var name = "";
			if(typeof data.result[ind].name != "undefined"){
				name = data.result[ind].name;
			}

			var htmlrender = '<div class="form-horizontal" style ="border:solid 1px;" >'+
								"<a href='javascript:;' class='pull-right' onclick='emptyFiche()'><i class='fa fa-times fa-3x text-red padding-10'></i> </a>"+
								'<h3>'+name+'</h3>';
			var htmlrenderlist = [];

			var titlelabel = {

				"domainAction" : "Domaine(s) d'action",
				"typectenat" : "Type d’indicateur",
				"definition" : "Définition",
				"objectifDD" :  "Objectif(s) de développement durable",
				"methodeCalcul" : "Méthode de calcul",
				"unity1" : "Unité",
				"sourceData1" : "Où trouver les données",
				"originIndicator" : "Origine de l’indicateur",
				"echelleApplication" : "A quelle échelle l’indicateur a-t-il du sens ? / peut elle etre calculée ?"

			}

			$.each(titlelabel, function( index, value ) {
			  	if(typeof data.result[ind][index] != "undefined"){
			  		if(Array.isArray(data.result[ind][index])){
			  			htmlrender += '<div class="form-group"><label for="" class="col-xs-3 control-label  " >'+titlelabel[index]+'</label><div class="col-xs-9"><label for="" class="control-label markdown" style = "text-align: left;font-weight: 100;">'+data.result[ind][index].join('<br/>')+'</label></div></div>';
			  		} else {
			  			htmlrender += '<div class="form-group"><label for="" class="col-xs-3 control-label " >'+titlelabel[index]+'</label><div class="col-xs-9"><label for="" class="control-label markdown" style = "text-align: left;font-weight: 100;">'+data.result[ind][index]+'</label></div></div>';
			  		}
				}
			});
			htmlrender += "<a href='javascript:addIndicator(\""+ind+"\");' class=' btn btn-primary margin-20' onclick='emptyFiche()'><i class='fa fa-plus'></i> Ajouter cet indicateur </a>";
			htmlrender += '</div>';


			
      $("#ficheIndic").html(htmlrender);
		}
		);

      
}

function addIndicator(id){
	tplCtx.value = {
        "indicateur" : id
    };
    
    mylog.log("save tplCtx",tplCtx);
    if(typeof tplCtx.value == "undefined")
    	toastr.error('value cannot be empty!');
    else {
        dataHelper.path2Value( tplCtx, function(params) { 
        	//dyFObj.closeForm();
           //urlCtrl.loadByHash( location.hash );
           $("#openModal").modal("hide");
           reloadInput("<?php echo $key ?>", "<?php echo $formId ?>" ); 
        } );
    }
}

</script>