<?php if($answer){ 	?>
<style type="text/css">
.titlecolor<?php echo $kunik ?> {
	<?php 
		if( $mode != "pdf"){	
	?>
			color: <?php echo ($titleColor) ? $titleColor : "black"; ?>;
	<?php
		} 
	?>
}

/* Tabs panel */
.tabbable-panel {
  border:1px solid #eee;
  padding: 10px;
  margin: 25px 2px 20px 2px;
}

/* Default mode */
.tabbable-line > .nav-tabs {
  border: none;
  margin: 0px;
}
.tabbable-line > .nav-tabs > li {
  margin-right: 2px;
}
.tabbable-line > .nav-tabs > li > a {
  border: 0;
  margin-right: 0;
  color: #737373;
}
.tabbable-line > .nav-tabs > li > a > i {
  color: #a6a6a6;
}
.tabbable-line > .nav-tabs > li.open, .tabbable-line > .nav-tabs > li:hover {
  border-bottom: 4px solid #fbcdcf;
}
.tabbable-line > .nav-tabs > li.open > a, .tabbable-line > .nav-tabs > li:hover > a {
  border: 0;
  background: none !important;
  color: #333333;
}
.tabbable-line > .nav-tabs > li.open > a > i, .tabbable-line > .nav-tabs > li:hover > a > i {
  color: #a6a6a6;
}
.tabbable-line > .nav-tabs > li.open .dropdown-menu, .tabbable-line > .nav-tabs > li:hover .dropdown-menu {
  margin-top: 0px;
}
.tabbable-line > .nav-tabs > li.active {
  border-bottom: 4px solid #f3565d;
  position: relative;
}
.tabbable-line > .nav-tabs > li.active > a {
  border: 0;
  color: #333333;
}
.tabbable-line > .nav-tabs > li.active > a > i {
  color: #404040;
}
.tabbable-line > .tab-content {
  margin-top: -3px;
  background-color: #fff;
  border: 0;
  border-top: 1px solid #eee;
  padding: 15px 0;
}
.portlet .tabbable-line > .tab-content {
  padding-bottom: 0;
}

/* Below tabs mode */

.tabbable-line.tabs-below > .nav-tabs > li {
  border-top: 4px solid transparent;
}
.tabbable-line.tabs-below > .nav-tabs > li > a {
  margin-top: 0;
}
.tabbable-line.tabs-below > .nav-tabs > li:hover {
  border-bottom: 0;
  border-top: 4px solid #fbcdcf;
}
.tabbable-line.tabs-below > .nav-tabs > li.active {
  margin-bottom: -2px;
  border-bottom: 0;
  border-top: 4px solid #f3565d;
}
.tabbable-line.tabs-below > .tab-content {
  margin-top: -10px;
  border-top: 0;
  border-bottom: 1px solid #eee;
  padding-bottom: 15px;
}
</style>
	<?php 
		if( $mode != "pdf" && $mode != "r")
		{
			
			$editBtnL = ( $canEdit == true && ( $mode == "w" || $mode == "fa") ) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";
			//var_dump($editBtnL);
			$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

			$classforpdf = "";
		} else {
			$editBtnL = "";
			$editParamsBtn = "";
			if ($mode == "pdf" ) {
				$classforpdf = "calendar-table";
			}else{
				$classforpdf = "";
			}
		}

		$paramsData = [ 
			"financerTypeList" => Ctenat::$financerTypeList,
			"limitRoles" =>["Financeur"],
			"graphfinancement" => []
		];

		$inputTypes = [
			"title" => "graphique sur un fond",
			"amount" => "graphique sur un amount",
			"financer" => "graphique sur un financier",
			"financertype" => "graphique sur un type de financier"
		];

		if( isset($parentForm["params"][$kunik]) ) {
			if( isset($parentForm["params"][$kunik]["tpl"]) ) 
				$paramsData["tpl"] =  $parentForm["params"][$kunik]["tpl"];
			if( isset($parentForm["params"][$kunik]["financerTypeList"]) ) 
				$paramsData["financerTypeList"] =  $parentForm["params"][$kunik]["financerTypeList"];
			if( isset($parentForm["params"][$kunik]["limitRoles"]) ) 
				$paramsData["limitRoles"] =  $parentForm["params"][$kunik]["limitRoles"];
			
		}
		
		if( isset($parentForm["graphfinancement"]) ) 
				$paramsData["graphfinancement"] =  $parentForm["graphfinancement"];

		$communityLinks = [
			Person::COLLECTION => [],
			Organization::COLLECTION => []
		];
		
		if (isset($answer["cterSlug"])) {
			$el = Slug::getElementBySlug($answer["cterSlug"]);
		}
		//var_dump( $el["el"]["links"] );

		if (isset($el["el"]["links"]["contributors"])) {
			foreach ( $el["el"]["links"]["contributors"] as $id => $c ) 
			{
				if( !empty($c["roles"] ) && 
					$paramsData["limitRoles"] && 
					in_array( $c["type"], [ Person::COLLECTION, Organization::COLLECTION ] ) )
				{
					foreach ($c["roles"] as $i => $r) {
						if( in_array($r, $paramsData["limitRoles"]) )
							$communityLinks[$c["type"]][] = $id;
					}		
				}
			}
		}

		$persons = Link::groupFindByType( Person::COLLECTION, $communityLinks[Person::COLLECTION],["name"] );
		$organizations = Link::groupFindByType( Organization::COLLECTION,$communityLinks[Organization::COLLECTION],["name","email"] );
		$financers = [];
		$all =array_merge( $persons, $organizations );
		foreach ($all as $k => $v) {
			$financers[$k] = $v["name"];
		}

		$listLabels = array_merge(Ctenat::$financerTypeList,$financers);

		$properties = [
              "financerType" => [
                "placeholder" => "Type de Financement",
                    "inputType" => "select",
                    "list" => "financerTypeList",
                    "rules" => [
                        "required" => true
                    ]
                ],
                "financer" => [
                    "placeholder" => "Quel Financeur",
                    "inputType" => "select",
                    "list" => "financersList",
                    "subLabel" => "Si financeur public, l’inviter dans la liste ci-dessous (au cas où il n’apparait pas demandez à votre référent territoire de le déclarer comme partenaire financeur"
                ],
                "title" => [
                    "inputType" => "text",
                    "label" => "Fonds, enveloppe ou budget mobilisé",
                    "placeholder" => "Fonds, enveloppe ou budget mobilisé",
                    "subLabel" => "préciser l’intitulé et la nature du financement",
                    "rules" => [ "required" => true ]
                ]
        ];

		$fromto = [];

		$num = sizeof($paramsData['graphfinancement']) +1;

		if(isset($parentForm["params"]["period"])){
			$from = $parentForm["params"]["period"]["from"]+1;
			$to = $parentForm["params"]["period"]["to"];
			$fromto = ["from" => $from, "to" => $to] ;
			while ( $from <= $to) {
				$properties["amount".$from] = [
			        "inputType" => "text",
			        "label" => $from." (euros HT)",
			        "placeholder" => $from." (euros HT)",
			        "propType" => "amount",
			        "rules" => [ "required" => true,"number" => true ]
			    ];
			    $paramsData["amounts"]["amount".$from] = $from." (euros HT)";
		    	$from++;
			}
		} 

		$labels = [];
		$labelstitle = [];
		$labelsfinancer = [];
		$labelsfinancertype = [];

		$datasetsbudget = [];

		$datasetsbudgetfinancer= [];
		$datasetsbudgetfinancertype= [];
		$datasetsbudgettitle = [];

		$yearLabels = [];

		if (sizeof($fromto) != 0) {
			$from = $fromto["from"];
			$to = $fromto["to"];
			while ( $from <= $to) {
				array_push($yearLabels, $from." (euros HT)");
		    	$from++;
			}
		}

		if(isset($answers) and sizeof($answers) > 0){
			foreach ($answers as $i => $inp) {
				
				if(is_array($inp)){
					array_push($labels, $inp["title"]);
					$dtbudgt = [];
					foreach ($inp as $x => $c) {
					if(strpos($x , "amount") !== false){
						array_push($dtbudgt , (int)$c);
					}
				}
				array_push($datasetsbudget, $dtbudgt);
				}
				
				
			}

			foreach ($answers as $i => $inp) {
				if (isset($inp["title"]) && !in_array($inp["title"], $labelstitle)) {
						array_push($labelstitle, $inp["title"]);
		        }
		    }

		    foreach ($answers as $i => $inp) {
				if (isset($inp["financer"]) && !in_array($inp["financer"], $labelsfinancer)) {
						array_push($labelsfinancer, $inp["financer"]);
		        }
		    }

		    foreach ($answers as $i => $inp) {
				if (isset($inp["financerType"]) && !in_array($inp["financerType"], $labelsfinancertype)) {
						array_push($labelsfinancertype, $inp["financerType"]);
		        }
		    }
		}

	    foreach ($labelstitle as $keylabelstitle => $value) {
			$deffaultitle = $value;
		}

		foreach ($labelsfinancer as $keylabelstitle => $value) {
			$deffaultfinancer = $value;
		}

		foreach ($labelsfinancertype as $keylabelstitle => $value) {
			$deffaultfinancertype = $value;
		}

	?>	
<div class="form-group" style="overflow-x: auto;">
	<?php 
	if($mode == "r" || $mode == "pdf"){ ?>
		<div ><h4 class="titlecolor<?php echo $kunik ?> pdftittlecolor text-center" style="padding-top:20px;"><?php echo $label ; ?></h4>

    <?php echo $info ?>
		</div>
	<?php 
	} else {
		?>
		<div ><h4 class="titlecolor<?php echo $kunik ?> pdftittlecolor text-center" style="padding-top:20px;"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL ; ?>  <button type="button" class ="btn btn-default switch<?php echo $kunik; ?>"><i class="fa fa-pie-chart"></i></button> </h4>
		<?php echo $info ?>
		</div>
	<?php 
	} 

	if( $mode != "pdf"){
	?>

	<table class="table table-bordered table-hover  directoryTable <?php echo $classforpdf ?>" id="<?php echo $kunik ; ?>table">
		
		<thead>	
			<?php if(isset($answers) && count($answers)>0){ ?>
			<tr>
				<?php 
				
				foreach ($properties as $i => $inp) {
					echo "<th>".$inp["placeholder"]."</th>";
				}

				if( $mode != "pdf" and $mode != "r"){
				?>
					<th></th>
				<?php } ?>
			</tr>
			<?php } ?>
		</thead>
		<tbody class="directoryLines">	
				<?php 
				$ct = 0;
				
				if(isset($answers)){
					foreach ($answers as $q => $a) {

						echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
						foreach ($properties as $i => $inp) {
							echo "<td>";
							if(isset($a[$i])) {
								if($i == "financer" && isset($all[$a[$i]]))
									echo $all[$a[$i]]["name"];
								else if($i == "financerType" && isset($listLabels[$a[$i]]))
									echo $listLabels[$a[$i]];
								else if(is_array($a[$i]))
									echo implode(",", $a["role"]);
								else if( strpos($i, "amount") !== false && isset($a[$i]) )
								    echo rtrim(rtrim(number_format($a[$i] , 3, ".", " "), '0'), '.')." €";
								else
									echo $a[$i];
							}
							echo "</td>";
						}
						if( $mode != "pdf" && $mode != "r"){

							?>
							<td>
								<?php 
									echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
										"canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]||in_array(Yii::app()->session["userId"], array_keys($financers))),
										"id" => $answer["_id"],
										"collection" => Form::ANSWER_COLLECTION,
										"q" => $q,
										"path" => $answerPath.$q,
										"kunik"=>$kunik
										] );
									?>
								<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo addslashes(@$a['step']) ?>','<?php echo $key ?>','<?php echo (string)$form["_id"] ?>')">
											<?php 
												echo PHDB::count(Comment::COLLECTION, array("contextId"=>(string)$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q)); ?> 
												<i class='fa fa-commenting'></i></a>
							</td>

							<?php 
							if(  ($canEdit == true && Authorisation::isUser(Yii::app()->session["userId"],$paramsData["limitRoles"])) || ( !empty($canAdminAnswer) && $canAdminAnswer == true ) ){	?>
								<td>
									<?php 
										$color = "default";
										$valbl = "?";
										$tool= "En attente de validation";
										if( isset($a["valid"]) ){
											if( $a["valid"] == "validated" ){
												$color = "success";
												$valbl = "V";
												$tool="Validé sans réserve";
											} 
											// else if( $a["valid"] == "reserved" ){
											// 	$color = "warning";
											// 	$valbl = "R";
											// 	$tool="Validé avec réserves";
											// } 
											else if( $a["valid"] == "refused" ){
												$color = "danger";
												$valbl = "NV";
												$tool="Non validé";
											}
										}

										echo "<a href='javascript:;' data-id='".$answer["_id"]."' data-budgetpath='".$answerPath."' data-form='".$form["id"]."' data-pos='".$q."'  class='btnValidFinance btn btn-xs btn-".$color." margin-left-5 padding-10 tooltips' data-toggle='tooltip' data-placement='left' data-original-title='".$tool."'>".$valbl."</a>";

									
										?>
								</td>

								<?php 
								}else{ ?>
									<td>
										<?php 
											if(!empty($a["validFinal"]["valid"]))
												echo $a["validFinal"]["valid"];
										?>
									</td>
								<?php
								}
								$ct++;
								
						}
						echo "</tr>";
					}

					$totalMap = [];
					$totalMapBudg = [];
					foreach ( $properties as $i => $inp ) {
						if(stripos($i, "amount") !== false ){
							$totalMap[$i] = 0;
							$totalMapBudg[$i] = 0;
						}
					}

					if(isset($answers)){
						foreach ( $answers as $q => $a ) {	
							foreach ($totalMap as $i => $tot) {
								if(isset($a[$i]))
									$totalMap[$i] = $tot + $a[$i];
							}
						}
					}
					if(isset($answer["answers"]["murir"]["budget"])){
						foreach ( $answer["answers"]["murir"]["budget"] as $q => $a ) {	
							foreach ($totalMapBudg as $i => $tot) {
								if(isset($a[$i]))
									$totalMapBudg[$i] = $tot + $a[$i];
							}
						}
					}

					$total = 0;
					$totalBudg = 0;
					foreach ( $totalMap as $i => $tot ) {
							$total = $total + $tot ;
					}
					foreach ( $totalMapBudg as $i => $tot ) {
							$totalBudg = $totalBudg + $tot ;
					}

					$colspNotpdf = 0;
					if( $mode != "pdf" and $mode != "r"){
						$colspNotpdf = 1;
					}


					if($total > 0){

						echo '<tr class="bold">';
							
							echo '<td class="specialword" colspan="3" style="text-align:right">TOTAL : </td>';
							foreach ($totalMap as $i => $tot) {
								
									echo '<td>'.(($tot == 0) ? '' : rtrim(rtrim(number_format($tot , 3, ".", " "), '0'), '.').'€').'</td>';
							}
							if( $mode != 'pdf' and $mode != 'r'){
								echo '<td></td>';
							}
						echo '</tr>';

						echo '<tr class="bold">';
						echo 	'<td class="specialword" colspan="'.((count( $totalMap ))+2).'" style="text-align:right">FINANCEMENT TOTAL : </td>';
						echo 	'<td colspan="'.(1+$colspNotpdf).'">'.rtrim(rtrim(number_format($total , 3, ".", " "), '0'), '.').' €</td>';
						echo '</tr>';
					}	

				}
					?>
		</tbody>
	</table>

	<?php
	}else{
	?>

	<table class="table table-bordered table-hover  directoryTable <?php echo $classforpdf ?>" id="<?php echo $kunik ; ?>table">
		
		<thead>	
			<?php if(isset($answers) && count($answers)>0){ ?>
			<tr>
				<?php 
				
				foreach ($properties as $i => $inp) {
					if ($i != "financerType" && $i != "financer" && $i != "title") {
						echo "<th>".$inp["placeholder"]."</th>";				
					}
				}
				?>

			</tr>
			<?php } ?>
		</thead>
		<tbody class="directoryLines">	
				<?php 
				$ct = 0;
				
				if(isset($answers)){
					foreach ($answers as $q => $a) {
						?>
						<tr><td colspan="<?php echo sizeof($yearLabels); ?>">
							<b> Type de Financement : </b> <?php echo @$a["financerType"] ?>  <br>
							<b> Financeur : </b> <?php echo ((isset($a["financer"]) && isset($all[$a["financer"]])) ? $all[$a["financer"]]["name"] : "")?>  <br>
							<b> Fonds, enveloppe ou budget mobilisé : </b> <?php echo @$a["title"] ?>
						</td></tr>


						<?php
						
						echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
						foreach ($properties as $i => $inp) {
							if ($i != "financerType" && $i != "financer" && $i != "title") {
								echo "<td>";
								if(isset($a[$i])) {
									if($i == "financer" && isset($all[$a[$i]]))
										echo $all[$a[$i]]["name"];
									else 
										if(is_array($a[$i]))
										echo implode(",", $a["role"]);
									else if (is_numeric($a[$i]))
										echo rtrim(rtrim(number_format($a[$i] , 3, ".", " "), '0'), '.')."€";
								}
								echo "</td>";
							}
						}
						echo "</tr>";
					}

					$totalMap = [];
					$totalMapBudg = [];
					foreach ( $properties as $i => $inp ) {
						if(stripos($i, "amount") !== false ){
							$totalMap[$i] = 0;
							$totalMapBudg[$i] = 0;
						}
					}

					if(isset($answers)){
						foreach ( $answers as $q => $a ) {	
							foreach ($totalMap as $i => $tot) {
								if(isset($a[$i]) && is_numeric($a[$i]))
									$totalMap[$i] = $tot + $a[$i];
							}
						}
					}
					if(isset($answer["answers"]["murir"]["budget"])){
						foreach ( $answer["answers"]["murir"]["budget"] as $q => $a ) {	
							foreach ($totalMapBudg as $i => $tot) {
								if(isset($a[$i]) && is_numeric($a[$i]))
									$totalMapBudg[$i] = $tot + $a[$i];
							}
						}
					}

					$total = 0;
					$totalBudg = 0;
					foreach ( $totalMap as $i => $tot ) {
							$total = $total + $tot ;
					}
					foreach ( $totalMapBudg as $i => $tot ) {
							$totalBudg = $totalBudg + $tot ;
					}

					$colspNotpdf = 0;
					if( $mode != "pdf" and $mode != "r"){
						$colspNotpdf = 1;
					}


					if($total > 0){
						echo '<tr class="bold">';
							echo '<td class="specialword" colspan="'.sizeof($yearLabels).'" >TOTAL : </td>';

						echo '</tr>';

						echo '<tr class="bold">';
							
							
							foreach ($totalMap as $i => $tot) {
									echo '<td>'.(($tot == 0) ? '0€' : rtrim(rtrim(number_format($tot , 3, ".", " "), '0'), '.').'€').'</td>';
							}
						echo '</tr>';

						echo '<tr class="bold">';
							echo '<td class="specialword" colspan="'.(sizeof($yearLabels) -1).'" style="text-align:right">TOTAL : </td>';
							echo 	'<td >'.rtrim(rtrim(number_format($total , 3, ".", " "), '0'), '.').' €</td>';

						echo '</tr>';

					}	

				}
					?>
		</tbody>
	</table>

	<?php 
	}
	if($mode != "pdf"){  
	?>
	<div class="hide" id="<?php echo $kunik ; ?>graph">	
		<div class="col-md-12 ">	
			 <div class="tabbable-panel">
				<div class="tabbable-line">
					<ul class="nav nav-tabs ">
						<li class="active">
							<a href="#tab_default_fin" data-toggle="tab">
							Résumé </a>
						</li>
						<?php 
						foreach ($paramsData["graphfinancement"] as $panelId => $panelValue) { 
						?>
							<li>
								<a href="#tab_default_<?php echo $panelId; ?>" data-toggle="tab">
									<?php echo $panelValue["label"]; ?> 
								</a>
							</li>
						<?php 
						}
						?>
						<li>
							<a href="javascript:;" class="addgraphfinancement" style="color: #43a9b2;">
							Ajouter</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_default_fin">
							<div class="row">
								<div class="col-md-10" id="<?php echo $kunik ; ?>graphcontainer">
									<span id="<?php echo $kunik ; ?>budgetchargement">En cours de chargement</span>
								</div>

								<div class="col-md-2 ">
									
								</div>
							</div>
						</div>

						<?php 
						foreach ($paramsData["graphfinancement"] as $panelId => $panelValue) { 
						?>
							<div class="tab-pane" id="tab_default_<?php echo $panelId; ?>">
								<div class="row">
									<div class="col-md-10" id="<?php echo $kunik.$panelId ; ?>graphcontainer">
										
									</div>
									<div class="col-md-2">
										 <div class="form-group">
										 	<?php 
										 	$selectedOp = "";
										 	$subtypeOp = "default";

										 	if (isset($panelValue["subtype"])) {
										 		$subtypeOp = $panelValue["subtype"];
										 	} 
										 	if($panelValue["type"] == "amount") {
										 		if (!isset($panelValue["subtype"])) {
											 		$subtypeOp = $from;
											 	} 
										 	?>
										    
											    <select class="form-control changesubtype" id="<?php echo $panelId; ?>ControlSelect1" data-path="<?php echo $panelId; ?>.subtype">
												     <?php
												     		if (sizeof($fromto) != 0) {
																$frm = $fromto["from"];
																$tto = $fromto["to"];
																while ( $frm <= $tto) {
																	if($subtypeOp == $frm) {
																		$selectedOp = "selected";
																	}else {
																		$selectedOp = "";
																	}

																	echo "<option ".$selectedOp." value='".$frm."' > ".$frm."</option>";
															    	$frm++;
																}
															}
												     ?>
												      
											    </select>

											<?php 
												} elseif ($panelValue["type"] == "title") {
													if (!isset($panelValue["subtype"])) {
												 		$subtypeOp = $deffaultitle;
												 	} 
											?>
												<select class="form-control changesubtype" id="<?php echo $panelId; ?>ControlSelect1" data-path="<?php echo $panelId; ?>.subtype">
												     <?php
												     	foreach($answers as $pstid => $pstvalue){
												     		if($subtypeOp == $pstvalue['title']) {
																		$selectedOp = "selected";
																	}else {
																		$selectedOp = "";
																	}
															echo "<option ".$selectedOp." value='".$pstvalue['title']."' > ".$pstvalue['title']."</option>";
														}
												     ?>
												      
											    </select>

											<?php 
												} elseif ($panelValue["type"] == "financer") {
													if (!isset($panelValue["subtype"])) {
												 		$subtypeOp = $deffaultfinancer;
												 	} 
											?>
												<select class="form-control changesubtype" id="<?php echo $panelId; ?>ControlSelect1" data-path="<?php echo $panelId; ?>.subtype">
												     <?php
												     	foreach($answers as $pstid => $pstvalue){
												     		if($subtypeOp == $pstvalue['financer']) {
																		$selectedOp = "selected";
																	}else {
																		$selectedOp = "";
																	}
															echo "<option ".$selectedOp." value='".$pstvalue['financer']."' > ".$pstvalue['financer']."</option>";
														}
												     ?>

												     <?php 
												} elseif ($panelValue["type"] == "financertype") {
													if (!isset($panelValue["subtype"])) {
												 		$subtypeOp = $deffaultfinancertype;
												 	} 
											?>
												<select class="form-control changesubtype" id="<?php echo $panelId; ?>ControlSelect1" data-path="<?php echo $panelId; ?>.subtype">
												     <?php
												     	foreach($answers as $pstid => $pstvalue){
												     		if($subtypeOp == $pstvalue['financerType']) {
																		$selectedOp = "selected";
																	}else {
																		$selectedOp = "";
																	}
															echo "<option ".$selectedOp." value='".$pstvalue['financerType']."' > ".$pstvalue['financerType']."</option>";
														}
												     ?>
												      
											    </select>

											<?php	
													}
												?>
										  </div>
										
									</div>
								</div>
							</div>
						<?php 
						}
						?>

					</div>
				</div>
			</div>
		</div>	
	</div>
	<?php
	}
	?>
</div>

<div class="form-prioritize" style="display:none;">
	  <form class="inputProritary" role="form">

		<a href="javascript:;" data-value="waiting" class="validateStatusFinance btn" style="width: 100%;">Attente de validation</a><br/><br/>
		<a href="javascript:;" data-value="validated" class="validateStatusFinance btn btn-success" style="width: 100%;">Validé sans réserve</a><br/><br/>
		<!-- <a href="javascript:;" data-value="reserved" class="validateStatusFinance btn btn-warning" style="width: 100%;">Validé avec réserves</a><br/><br/> -->
		<a href="javascript:;" data-value="refused"  class="validateStatusFinance btn btn-danger" style="width: 100%;">Non validé</a><br/><br/>
		<br/><br/>
	  </form>
	</div>

<?php 
$totalBudg = 0;
$totalMapBudg = [];
foreach ( $properties as $i => $inp ) {
	if(stripos($i, "amount") !== false ){
		$totalMap[$i] = 0;
		$totalMapBudg[$i] = 0;
	}
}
if(isset($answer["answers"]["murir"]["budget"])){
	foreach ( $answer["answers"]["murir"]["budget"] as $q => $a ) {	
		foreach ($totalMapBudg as $i => $tot) {
			if(isset($a[$i]) && is_numeric($a[$i]))
				$totalMapBudg[$i] = $tot + $a[$i];
		}
	}
	foreach ( $totalMapBudg as $i => $tot ) {
			$totalBudg = $totalBudg + $tot ;
	}
}

if( $mode != "pdf"){	
echo $this->renderPartial( "survey.views.tpls.forms.costum.ctenat.equibudget" , 
		[ "totalFin"   => (isset($total)) ? $total : 0 ,
		  "totalBudg" => (isset($totalBudg)) ? $totalBudg : 0,
		  "titleColor" => $titleColor,
		  "kunik" => $kunik,
		  "mode" => $mode ] );
}

if( $mode != "pdf" and $mode != "r"){	

 ?>

<script type="text/javascript">
if(typeof costum.lists == "undefined")
	costum.lists = {};
costum.lists.financerTypeList = <?php echo json_encode(Ctenat::$financerTypeList); ?>;
costum.lists.financersList = <?php echo json_encode($financers); ?>;
costum.lists.financersOrga = <?php echo json_encode($all); ?>;

var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

var yearLabels = <?php echo json_encode($yearLabels); ?>;

	var <?php echo $kunik ; ?>Datachart = {
		"label" : yearLabels ,
		"labels" : <?php echo json_encode($labels); ?>,
		"datasets" : <?php echo json_encode($datasetsbudget); ?>
	} ;

	<?php 
			foreach ($paramsData["graphfinancement"] as $panelId => $panelValue) { 
				if (!isset($panelValue["subtype"])) {
					if($panelValue["type"] == "financer") {
	?>
						var <?php echo $kunik.$panelId; ?>Data = {
							"label" : <?php echo json_encode($yearLabels); ?>,
							"dataset" : <?php echo json_encode(Ctenat::getdatasetsfinancier($deffaultfinancer, $labels, $datasetsbudget, $answers, $labelsfinancer)); ?>,
							"title" : "<?php echo $deffaultfinancer; ?>"
						}
	<?php
					} elseif($panelValue["type"] == "financertype") {
	?>
						var <?php echo $kunik.$panelId; ?>Data = {
							"label" : <?php echo json_encode($yearLabels); ?>,
							"dataset" : <?php echo json_encode(Ctenat::getdatasetsfinanciertype($deffaultfinancertype, $labels, $datasetsbudget, $answers, $labelsfinancertype)); ?>,
							"title" : "<?php echo $deffaultfinancertype; ?>"
						}
	<?php
					} elseif ($panelValue["type"] == "title") {
	?>
						var <?php echo $kunik.$panelId; ?>Data = {
							"label" : <?php echo json_encode($yearLabels); ?>,
							"dataset" : <?php echo json_encode(Ctenat::getdatasetstitle($deffaultitle, $labels, $datasetsbudget)); ?>,
							"title" : "<?php echo $deffaultitle; ?>"
						}
	<?php
					} elseif ($panelValue["type"] == "amount") {
						if (sizeof($fromto) != 0) {
	?>
						var <?php echo $kunik.$panelId; ?>Data = {
							"label" : <?php echo json_encode($labelstitle); ?>,
							"dataset" : <?php echo json_encode(Ctenat::getdatasetsamount($from,$answers,$datasetsbudget, $fromto)); ?>,
							"title" : "<?php echo $from; ?>"
						}
	<?php
						}
					}
	?>

	<?php 
					} else {

					if($panelValue["type"] == "financer") {
	?>
						var <?php echo $kunik.$panelId; ?>Data = {
							"label" : <?php echo json_encode($yearLabels); ?>,
							"dataset" : <?php echo json_encode(Ctenat::getdatasetsfinancier($panelValue["subtype"], $labels, $datasetsbudget, $answers, $labelsfinancer)); ?>,
							"title" : "<?php echo $panelValue["subtype"]; ?>"
						}
	<?php
					} elseif($panelValue["type"] == "financertype") {
	?>
						var <?php echo $kunik.$panelId; ?>Data = {
							"label" : <?php echo json_encode($yearLabels); ?>,
							"dataset" : <?php echo json_encode(Ctenat::getdatasetsfinanciertype($panelValue["subtype"], $labels, $datasetsbudget, $answers, $labelsfinancertype)); ?>,
							"title" : "<?php echo $panelValue["subtype"]; ?>"
						} 
	<?php
					} elseif ($panelValue["type"] == "title") {
	?>
						var <?php echo $kunik.$panelId; ?>Data = {
							"label" : <?php echo json_encode($yearLabels); ?>,
							"dataset" : <?php echo json_encode(Ctenat::getdatasetstitle($panelValue["subtype"], $labels, $datasetsbudget)); ?>,
							"title" : "<?php echo $panelValue["subtype"]; ?>"
						}
	<?php
					} elseif ($panelValue["type"] == "amount") {
						if (sizeof($fromto) != 0) {
	?>
						var <?php echo $kunik.$panelId; ?>Data = {
							"label" : <?php echo json_encode($labelstitle); ?>,
							"dataset" : <?php echo json_encode(Ctenat::getdatasetsamount($panelValue["subtype"],$answers,$datasetsbudget, $fromto)); ?>,
							"title" : "<?php echo $panelValue["subtype"]; ?>"
						}
	<?php
						}
					}
	

					}
				}
						?>

$(document).ready(function() { 

	<?php 
			foreach ($paramsData["graphfinancement"] as $panelId => $panelValue) {
		?> 
			ajaxPost('#<?php echo $kunik.$panelId ; ?>graphcontainer', baseUrl+'/graph/co/dash/g/graph.views.co.costum.ctenat.ctepie'+"/id/<?php echo $kunik.$panelId ?>", null, function(){},"html");
		<?php
			}
		?>

	if (ajaxPost('#<?php echo $kunik ; ?>graphcontainer', baseUrl+'/graph/co/dash/g/graph.views.co.costum.ctenat.ctebarMany'+"/id/<?php echo $kunik ?>", null, function(){},"html")) 
		{
			$("$<?php echo $kunik ; ?>budgetchargement").addClass('hide');
		}

			$('.changesubtype').on('change', function() {
				tplCtx.id = "<?php echo $parentForm["_id"]; ?>",
	            tplCtx.path = "graphfinancement."+$(this).data("path"),
	            tplCtx.collection = "<?php echo Form::COLLECTION ?>"; 
	            tplCtx.value = this.value
	            dataHelper.path2Value( tplCtx, function(params) {
	            	dyFObj.closeForm();
                    reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>", function(){ 
	                    	$("#questionauteur > #copyEquilibreBudgetaire").html($("#questionplanFinancement > #equilibreBudget").clone());
                    });
                    urlCtrl.loadByHash(location.hash);
                } );

			});

	       	$(".addgraphfinancement").off().on("click",function() { 
	            sectionDyfObservatory = {
	                "jsonSchema" : {
	                    "title" : "Ajouter un onglet",
	                    "icon" : "fa-cog",
	                    "text" : "Ajouter un onglet",
	                    "properties" : {
	                      	label : { label : "Titre de l'onglet" },
		                    type : {
		                    	label : "Type",
		                        inputType : "select",
		                        options : <?php echo json_encode($inputTypes); ?>
		                    }
		                },
	                    save : function () {
	                    	var tplCtx = {};

	                        tplCtx.id = "<?php echo $parentForm["_id"]; ?>",
	                        tplCtx.path = "graphfinancement.<?php echo $kunik."_".$num; ?>",
	                        tplCtx.collection = "<?php echo Form::COLLECTION ?>";  
	                        var today = new Date();
	                        tplCtx.value = {};
	                        $.each( sectionDyfObservatory.jsonSchema.properties , function(k,val) {
	                            tplCtx.value[k] = $("#"+k).val();
	                        });

	                        mylog.log("save tplCtx",tplCtx);
	                        if(typeof tplCtx.value == "undefined")
	                            toastr.error('value cannot be empty!');
	                        else {
	                            dataHelper.path2Value( tplCtx, function(params) {
	                                dyFObj.closeForm();
	                                reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>", function(){ 
	                    	$("#questionauteur > #copyEquilibreBudgetaire").html($("#questionplanFinancement > #equilibreBudget").clone());
                    });
                                    urlCtrl.loadByHash(location.hash);
	                            } );
	                        }

	                    }
	                }
            	};

            	dyFObj.openForm( sectionDyfObservatory );

			});
	
	sectionDyf.<?php echo $kunik ?> = {
		"jsonSchema" : {	
	        "title" : "Plan de Financement",
            "icon" : "fa-money",
            "text" : "Décrire ici les financements mobilisés ou à mobiliser. Les coûts doivent être en <b>hors taxe</b>.",
	        "properties" : <?php echo json_encode( $properties ); ?>,
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                	dyFObj.closeForm();
	                    if(notNull(tplCtx.value.financer) && typeof costum.lists.financersOrga[tplCtx.value.financer] != "undefined" )
	                    {
		                    var contactList = {};
		                    contactList[ tplCtx.value.financer ] = {
		                    	type:"<?php echo Organization::COLLECTION?>"
		                    };
		                    mylog.log("answer/sendmail",contactList);
		                    ajaxPost(
						        null,
						        baseUrl+"/survey/answer/sendmail/id/<?php echo (string)$answer['_id'] ?>",
						        { contactList : contactList, 
						          step:"<?php echo @$answer["step"] ?>" },
						        function(data){ 
						        	toastr.success("Un mail a été envoyé à toutes les parties prenantes afin de les notifier");
						      		reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>", function(){ 
                    		var $equi = $("#questionplanFinancement > #equilibreBudget").clone();
	                    	$("#questionauteur > #copyEquilibreBudgetaire").html($equi);
                    });
                                    urlCtrl.loadByHash(location.hash);
						        }
						    );
		                } else {
		                	reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>", function(){ 
	                    	$("#questionauteur > #copyEquilibreBudgetaire").html($("#questionplanFinancement > #equilibreBudget").clone());
                    });
                            urlCtrl.loadByHash(location.hash);
		                }
	                } );
	            }

	    	}
	    }
	};

	sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "<?php echo $kunik ?> config",
	        "icon" : "fa-cog",
	        "properties" : {
	            financerTypeList : {
	                inputType : "properties",
	                labelKey : "Clef",
	                labelValue : "Label affiché",
	                label : "Liste des type de Financeurs",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.financerTypeList
	            } , 
	            limitRoles : {
	                inputType : "array",
	                label : "Liste des roles financeurs",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.limitRoles
	            },
	            tpl : {
	                label : "Sub Template",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.tpl
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	        		if(val.inputType == "array")
	        		 	tplCtx.value[k] = getArray('.'+k+val.inputType);
	        		else if(val.inputType == "properties")
	        		 	tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
	        		else
	        		 	tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) {
	                	dyFObj.closeForm(); 
	                    reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>", function(){ 
	                    	$("#questionauteur > #copyEquilibreBudgetaire").html($("#questionplanFinancement > #equilibreBudget").clone());
                    });
                        urlCtrl.loadByHash(location.hash);
	                } );
	            }

	    	}
	    }
	};

	var max<?php echo $kunik ?> = 0;
	var nextValuekey<?php echo $kunik ?> = 0;

	if(notNull(<?php echo $kunik ?>Data)){
		$.each(<?php echo $kunik ?>Data, function(ind, val){
			if(parseInt(ind) > max<?php echo $kunik ?>){
				max<?php echo $kunik ?> = parseInt(ind);
			}
		});

		nextValuekey<?php echo $kunik ?> = max<?php echo $kunik ?> + 1;

	}

    mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

    //adds a line into answer
    $(".add<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path")+nextValuekey<?php echo $kunik ?>;
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
    });

    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
    });

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });


		$(".switch<?php echo $kunik ?>").off().on("click",function() {  
	        $("i", this).toggleClass("fa-table");
	        $("i", this).toggleClass("fa-pie-chart");
	        $("#<?php echo $kunik ; ?>graph").toggleClass("hide");
	        $("#<?php echo $kunik ; ?>table").toggleClass("hide");

		});

    $('.btnValidFinance').off().click(function() { 
		var posFinance = $(this).data("pos");
		prioModal = bootbox.dialog({
	        message: $(".form-prioritize").html(),
	        title: "État du Financement",
	        show: false,
	        onEscape: function() {
	          prioModal.modal("hide");
	        }
	    });
	    prioModal.modal("show");

	    $(".validateStatusFinance").off().on("click",function() { 
		
		
            if($(this).data("value")!="waiting"){
	        	//var today = new Date();
				//today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();
	            var formData = {
		    		id : answerId,
		    		collection : "answers",
		    		path : "answers.murir.planFinancement."+posFinance+".valid",
	        		value : $(this).data("value")/*{
	        			valid : $(this).data("value"),
	        			user : userId,
	        			date : today
	        		}*/
	            };
	                        
		    	

		  	 	dataHelper.path2Value( formData, function(params) { 
		  	 		prioModal.modal('hide');
		  	 		reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>", function(){ 
	                    	$("#questionauteur > #copyEquilibreBudgetaire").html($("#questionplanFinancement > #equilibreBudget").clone());
                    });
                    urlCtrl.loadByHash(location.hash);
		  	 	} );
		  	 }else
		  	 	prioModal.modal('hide');
		  	
      	});
	});
	
    
});
</script>

<?php } } ?>
