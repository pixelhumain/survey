
<script type="text/javascript">
//if(typeof form == "undefined ")
var form = <?php echo json_encode($form); ?>;

var formSession = "<?php //echo $_GET["session"]; ?>";
//if(typeof answers == "undefined ")
var answers  = <?php echo json_encode($answers); ?>;
var projects  = <?php echo json_encode(@$projects); ?>;
var projectsDetails  = <?php echo json_encode(@$projectsDetails); ?>;
var projectsList = {};
var projectsLink = {};
var scenarioKey = "scenario";
var answerCollection = "answers";
var answerIdx = "<?php echo $_GET["id"] ?>";
var answerSectionBase = "answers";
var answerSection = "";

var actionsIndicator = <?php echo json_encode($actionsIndicator); ?>;
var cibleIndicator = <?php echo json_encode($cibleIndicator); ?>;
var indicators = <?php echo json_encode($indicators); ?>;
var dialog = null;

$(document).ready(function() { 
	costum.lists.financerTypeList = <?php echo json_encode(Ctenat::$financerTypeList); ?>;
	costum.lists.financersList = <?php echo json_encode($orgs); ?>;
	costum.lists["indicators"] = indicators;
    mylog.log("render","/modules/survey/views/custom/ctenat/dossierEditCTENat")

  	coInterface.bindLBHLinks();
  	coInterface.bindTooltips();

  	<?php if( isset($adminAnswers["validation"]) &&
		  isset($adminAnswers["validation"]["ctenat"]) &&
		  isset($adminAnswers["validation"]["cter"]) &&
		  in_array( @$adminAnswers["validation"]["ctenat"]["valid"], ["valid", "validReserve"]) &&
		  in_array( @$adminAnswers["validation"]["cter"]["valid"], ["valid", "validReserve"])  ) { ?>
  	$("#suivre").html($("#murir").html())
	<?php } ?>

  	documentManager.bindEvents();
  	$("#copyEquilibreBudgetaire").html($("#equilibreBudget").html());
	if(projects != null){
		$.each(projects,function(i,el) {
			if(typeof answers.links != "undefined" &&
				typeof answers.links.projects != "undefined" &&
				typeof answers.links.projects[i] != "undefined")
				projectsLink[i] = el;
			else
				projectsList[i] = el;
		});
	}
	
	$(".generateCopil").click(function(){
		idAnswer=$(this).data("id"),
		dataNow=$(this).data("date");
		var titleCopil=$(this).data("title");
		var subKey=$(this).data("key")
		//window.open(baseUrl+"/costum/ctenat/generatecopil/id/"+idAnswer+"/title/COPIL010203/date/"+dataNow);
        msgBoot='<div class="row">  ' +
                      '<div class="col-md-12"> ' +
                        '<form class="form-horizontal"> ' ;
                          if(!notNull(titleCopil)){
                         msgBoot+= '<label class="col-md-12 no-padding" for="awesomeness">Nom du copil : </label><br> ' +                        
                          '<input type="text" id="nameFavorite" class="nameSearch wysiwygInput form-control" value="" style="width: 100%" placeholder="'+tradDynForm.addname+'..."></input>'
                      		}
                         msgBoot += "<br>"+
                    		'<label class="col-md-12 no-padding" for="awesomeness">Noms de participants : </label><br> ' +
                          	'<textarea class="col-xs-12" id="participantsCopil"></textarea><br>'+
                          	'<label class="col-md-12 no-padding" for="awesomeness">Date du copil à récupérer : </label><br> ' +
                          	'<input id="dateCopil" type="date" value="'+dataNow+'">'+
                    		'</div> </div>' +
                    '</form></div></div>';            
		bootbox.dialog({
            title: "Remplissez les informations du COPIL",
            message: msgBoot,
            buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {
                    	
                    	var dialog = bootbox.dialog ({
						    message: '<p class="text-center mb-0 loadingPdf"></p>',
						    closeButton: false
						});
            			
            			coInterface.showLoader(".loadingPdf", "Création du copil en cours");
            			$(".loadingPdf .processingLoader").removeClass("col-xs-12 margin-top-50");
                        
                        var formData={
                          "title": (notNull(titleCopil)) ? titleCopil : $("#nameFavorite").val(),
                          "participants":$("#participantsCopil").val(),
                          "date":$("#dateCopil").val(),
                          "copil":true
                        };
                        if(notNull(subKey))
                        	formData.subKey=subKey;
                        
                        $.ajax({
						  type: "POST",
						  url: baseUrl+"/costum/ctenat/generatecopil/id/"+idAnswer,
						  data: formData,
						  success: function(data){
						  		toastr.success("Le copil a été créé avec succès");
						  		targetDom=(notNull(subKey))? "#copilFile"+subKey+" .content-copil": "#copilFile .content-copil";
						  		$(targetDom+ ".noCopilFile").remove();
						  		$(targetDom).prepend("<div class='col-xs-12 padding-5 shadow2 margin-top-5 margin-bottom-5'>"+"<a href='"+baseUrl+"/"+data.docPath+"' target='_blank' class='link-files'><i class='fa fa-file-pdf-o text-red'></i> "+ data.fileName+"</a>"+
											"</div>");
						  		coInterface.scrollTo("#copilFile");
								// do something in the background
								dialog.modal('hide');

						  },
						  dataType: "json"
						});
                    }
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: function() {
                  }
                }
              }
      	});
	});

	$(".generateFicheAction").click(function(){
		idAnswer=$(this).data("id"),
		dataNow=$(this).data("date");
		var titleFA=$(this).data("title");
		var subKey=$(this).data("key")
		//window.open(baseUrl+"/costum/ctenat/generatecopil/id/"+idAnswer+"/title/COPIL010203/date/"+dataNow);
        msgBoot='<div class="row">' +
                  '<div class="col-md-12"> Générer le contenu de cette fiche action en l\'état' +    
                '</div></div>';            
		bootbox.dialog({
            title: "Figer cette fiche action",
            message: msgBoot,
            buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {
                    	var dialog = bootbox.dialog ({
						    message: '<p class="text-center mb-0 loadingPdf"></p>',
						    closeButton: false
						});
            			coInterface.showLoader(".loadingPdf", "Création de la fiche action en cours");
            			$(".loadingPdf .processingLoader").removeClass("col-xs-12 margin-top-50");
                        var formData = {
                          "title": titleFA,
                          "date":dataNow,
                          "save":true
                        };
                        if(notNull(subKey))
                        	formData.subKey=subKey;
                        $.ajax({
						  type: "POST",
						  url: baseUrl+"/co2/export/pdfelement/id/"+idAnswer+"/type/answers/",
						  data: formData,
						  success: function(data){
						  		toastr.success("La fiche action a été créé avec succès");
						  		$(".modal").modal('hide');
						  		urlCtrl.loadByHash(location.hash);
						  },

						  dataType: "json"
						});
                    }
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: function() {
                  }
                }
              }
      	});
	});
	
	$('#doc').html( dataHelper.markdownToHtml( $('#doc').html() ) );
	
	$.each($('.markdown'),function(i,el) { 
		$(this).html( dataHelper.markdownToHtml( $(this).html() ) );	
	});

	$('.btnValidFinance').off().click(function() { 
		var posFinance = $(this).data("pos");
		prioModal = bootbox.dialog({
	        message: $(".form-prioritize").html(),
	        title: "État du Financement",
	        show: false,
	        onEscape: function() {
	          prioModal.modal("hide");
	        }
	    });
	    prioModal.modal("show");

	    $(".validateStatusFinance").off().on("click",function() { 
		
		/*dialog = bootbox.dialog({
            title: "Valider ce financement",
            message: '<div class="row">  ' +
                      '<div class="col-md-12"> ' +
                        '<form class="form-horizontal text-center"> ' +
                          '<label class="col-md-12 no-padding" for="awesomeness">État du Financement : </label><br> ' +
                          '<select id="financeValid">'+
                          	'<option value="valid">Validé sans réserve</option>'+
                          	'<option value="reserved">Validé avec réserves</option>'+
                          	'<option value="refused">Non validé</option>'+
                          '</select>'+
                    	'</form></div></div>',
            buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {*/
            if($(this).data("value")!="waiting"){
	        	//var today = new Date();
				//today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();
	            var formData = {
		    		id : answerIdx,
		    		collection : "answers",
		    		path : "answers.murir.planFinancement."+posFinance+".valid",
	        		value : $(this).data("value")
	            };
	                        
		    	

		  	 	dataHelper.path2Value( formData, function(params) { 
		  	 		prioModal.modal('hide');
		  	 		urlCtrl.loadByHash(location.hash);
		  	 	} );
		  	 }else
		  	 	prioModal.modal('hide');
		  	 	
       // }		
         /*       },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: function() {
                  }
                }
              }*/
      	});
	});
	$('.createStep').off().click(function() { 
		mylog.log('createStep ',$(this).data("form"));
		var dfCostum = ( form.scenario[ $(this).data("form") ]['costum']) ? form.scenario[ $(this).data( "form" ) ]['costum']['dynFormCostum'] : null;
		updateForm = {
			form : $(this).data("form")
		};

		//il faut mettre a jour les answer avec les data de l'orga ou du project
		dfCostum.afterSave = function(elem){
			mylog.log("createStep afterSave Costum",elem);
			data = {
				collection : "answers",
				id : answerIdx,
				path : "answers."+updateForm.form,
				value : {
					"type" : updateForm.form ,
                    "id"   : elem.id ,
                    "name" : elem.map.name
				}
			}
			mylog.log('createStep ',data);
			globalCtx = data;

			dataHelper.path2Value( data, function(params) { 
				dyFObj.commonAfterSave(data,function(){
					answerAfterSave();						
				});
			} );
		}

		dyFObj.openForm( $(this).data("form"), null, true, null,dfCostum );



		dataHelper.path2Value( {
			collection : contextData.type,
			id : contextData.id,
			path : "showAAP",
			value : true
		}, function(){} );
		
	});
	$('.editStep').off().click(function() { 
		//alert('.editStep : '+$(this).data("form"));
		//editing typed elements like projects, organizations
		if( $(this).data("type") )
		{

			var dfCostum = ( form.scenario[ $(this).data("form") ]['costum']) ? form.scenario[ $(this).data("form") ]['costum']['dynFormCostum'] : null;
			mylog.log('.editStep','.editStep 1',$(this).data("form"),$(this).data("type"),$(this).data("id"),"dfCostum",dfCostum );
			dfCostum.afterSave = function(elem){
				 dyFObj.commonAfterSave(null,function(){
				 	answerAfterSave();
				});
			};
			dyFObj.editElement( $(this).data("type"), $(this).data("id"), $(this).data("type"),dfCostum );
		}
		else 
		{
			mylog.log('.editStep','.editStep 2',$(this).data("form"),$(this).data("type"),$(this).data("step"));

			//used for image upload 
			updateForm = {
				form : $(this).data("form"),
				step : $(this).data("step")	
			};

			mylog.log('.editStep',"path",$(this).data("form"));
			var editForm = form.scenario[$(this).data("form")].json;

			editForm.jsonSchema.onLoads = {
				onload : function(){
					dyFInputs.setHeader("bg-dark");
					$('.form-group div').removeClass("text-white");
					dataHelper.activateMarkdown(".form-control.markdown");
				}
			};
			

			editForm.jsonSchema.save = function(){
				//alert("save");
				data = {
	    			collection : "answers",
	    			id : answerIdx,
	    			path : "answers."+updateForm.form,
	    			value : arrayForm.getAnswers(editForm , true)
	    		}
	    		mylog.log(".editStep","data",data);
				globalCtx = data;

				dataHelper.path2Value( data, function(params) { 
					dyFObj.commonAfterSave(null,function(){
						answerAfterSave();
					});
				} );
			    
			};
			var editData = answers[$(this).data("form")];

			
			dyFObj.editStep( editForm , editData);	
		}
	});

	$('.deleteAF').off().click(function() { 
		answerSection = answerSectionBase+"."+$(this).data("step")+"."+$(this).data("q");
		arrayForm.del($(this).data("form"),$(this).data("step"),$(this).data("q"),$(this).data("pos"));
	});


	$('.addAF').off().click(function() { 
		answerSection = answerSectionBase+"."+$(this).data("step")+"."+$(this).data("q");
		dyFObj.afterAfterSave = answerAfterSave;
		arrayForm.add( $(this).data("form"),$(this).data("step"),$(this).data("q"));
	});

	$('.addAFAndNotify').off().click(function() { 
		answerSection = answerSectionBase+"."+$(this).data("step")+"."+$(this).data("q");
		dyFObj.afterAfterSave = notify;
		notifyType = $(this).data("notify");
		arrayForm.add( $(this).data("form"),$(this).data("step"),$(this).data("q"));
	});

	$('.editAF').off().click(function() { 
		answerSection = answerSectionBase+"."+$(this).data("step")+"."+$(this).data("q");
		dyFObj.afterAfterSave = answerAfterSave;
		arrayForm.edit($(this).data("form"),$(this).data("step"),$(this).data("q"),$(this).data("pos"));
	});
});
var notifyType = null;
function connectToAnswer( type,id,name, slug, email )  {  
	mylog.log( "connectToAnswer",type,id,name, slug, email );
	if(type == "organizations"){
		data = {
			collection : "answers",
			id : answerIdx,
			path : "answers.organizations.orgasLinked",
			arrayForm : true,
			value : {
                "type" : type,
                "id" : id,
                "name" : name
            }
		}

		mylog.log("connectToAnswer","data",data);
		globalCtx = data;
		dataHelper.path2Value( data, function(params) { 
			dyFObj.closeForm();
			urlCtrl.loadByHash(location.hash);
		} );
	}
}

function showStep(id){
    $(".stepperContent").addClass("hide");
    $(id).removeClass("hide");
    //alert(location.hash.split(".")[0]+"."+location.hash.split(".")[1]+"."+location.hash.split(".")[2]+".subview."+$(".stepperContent:not(.hide").attr("id"));
    history.pushState(null, null, location.hash.split(".")[0]+"."+location.hash.split(".")[1]+"."+location.hash.split(".")[2]+".subview."+$(".stepperContent:not(.hide").attr("id"));
}

var ctxDynForms = {
	organizations : {
		organizations : {
			orgasLinked : {
				title : "Ajouter un porteur",
	            icon : "fa-users",
				properties : "organization",
				costum : {
                    "beforeBuild" : {
                        "properties" : {
                        	"type" : {
                                "label" : "Type d'organisation",
                                "inputType" : "select",
                                "placeholder" : "---- Type d'organisation ----",
                                "options" : {
                                    "NGO":"Association",
                                    "LocalBusiness":"Entreprise",
                                    "GovernmentOrganization":"Service Public"
                                }
                            },
                            
                            "category" : {
                                "inputType" : "hidden",
                                "value" : "ficheAction"
                            },
                            "links[answers]" : {
                                "inputType" : "hidden"
                            },
                            "links[projects]" : {
                                "inputType" : "hidden"
                            },
                            "roles" : {
                                "inputType" : "hidden"
                            },
                            "actionPrincipal" : {
	                            "inputType" : "select",
	                            "label" : "Domaines d’action de votre organisation",
	                            "placeholder" : "Choisir un domaine d'action principal",
	                            "groupOptions" : true,
	                            "groupSelected" : false,
	                            "list" : "domainAction",
	                            "select2" : {
	                                "multiple" : true
	                            },
	                            "order" : 6
	                        },
	                        "shortDescription" : {
                                "inputType" : "textarea",
                                "label" : "Description courte de l’orga",
                            },
                            "sectionReferant" : {
				                "inputType" : "custom",
				                "html":"<hr/>",
				                 "order" : 12
				            },
	                        "sectionReferant" : {
				                "inputType" : "custom",
				                "html":"<hr/><div class='margin-top-20'><b>Informations relative à l’administration de cette organisation</b> (la personne recevra une notification par mail)</div>",
				                 "order" : 13
				            },
				            "referantName" : {
                                "inputType" : "text",
                                "label" : "Nom de l’administrateur de cette organisation",
                                "order" : 14
                            },
	                        "email" : {
	                        	"inputType" : "text",
						    	"label" : "Email de l’administrateur ",
						    	"placeholder" : "exemple@mail.com" ,
	                        	"order" : 15
	                        }
                        }
                    },
                    "onload" : {
                        "actions" : {
                            "html" : {
                            	"modal-title" : "Ajouter un porteur",
                                "nametext > label" : "Nom de l’organisation"
                            },
                            "addClass" :{
                            	"modal-header":"bg-dark"
                            },
                            "removeClass" :{
                            	"modal-header":"bg-green"
                            },
                            "presetValue" : {
                                "links[answers]" : answerIdx,
                                "links[projects]" : {
                                    "eval" : "contextData.id"
                                },
                                "role" : "creator",
                                "roles":"Porteur d\'action"
                            },
                            "hide" : {
                                "urltext" : 1,
                                "tagstags" : 1,
                                "parentfinder" : 1,
                                "publiccheckboxSimple" : 1,
                                "roleselect" : 1,
                                "infocustom":1
                            }
                        }
                    },
                    afterSave : function(elem){
						mylog.log("createStep afterSave Costum","answerIdx",answerIdx,"elem",elem);
						data = {
							collection : "answers",
							id : answerIdx,
							arrayForm : true,
							path : "answers.organizations.orgasLinked",
							value : {
								"type" : "organization" ,
			                    "id"   : elem.id ,
			                    "name" : elem.map.name
							}
						}
						mylog.log('createStep ',data);
						globalCtx = data;

						dataHelper.path2Value( data, function(params) { 
							dyFObj.commonAfterSave(data,function(){
								answerAfterSave();	
							});
						} );
					}
                }
				
			}
		}
	}
}

function answerAfterSave () { 
	location.hash = location.hash.split(".")[0]+"."+location.hash.split(".")[1]+"."+location.hash.split(".")[2]+".subview."+$(".stepperContent:not(.hide").attr("id");
	pageProfil.params.subview = $(".stepperContent:not(.hide").attr("id");
	pageProfil.views.answers();
		//urlCtrl.loadByHash(location.hash);
	dyFObj.closeForm();
}

function notify () { 
	
	answerAfterSave ();

	if(notifyType == "planFinancement" && dyFObj.path2Value && notEmpty(dyFObj.path2Value.params.value.financer)) {	
		

		//check financer is part of community and role Financer
		//launch action linking project to orga with role Financeur + orga to project
		// and createNotification to the financer
		//alert("ajax"+form.id+":"+dyFObj.path2Value.params.value.financer);
		ajaxPost( '' , baseUrl+'/costum/ctenat/notifinancer', { 
		  	cter : form.id,
		  	financer : dyFObj.path2Value.params.value.financer,
		  	project : answers.project.id
		  }, function(){toastr.success("Le copil a été créé avec succès");}, "json");
		
	}
}

</script>
