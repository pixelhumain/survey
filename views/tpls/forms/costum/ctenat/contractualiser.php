<?php 
$canAdminCter = Authorisation::isElementAdminBySlug( $answer["cterSlug"] , Yii::app()->session["userId"] );
if($canAdminCter || (isset($answer["priorisation"]) && in_array($answer["priorisation"] , [Ctenat::STATUT_ACTION_CONTRACT,Ctenat::STATUT_ACTION_VALID ]) ) ){ 

	?>

<div id="copyEquilibreBudgetaire"></div>

<table class="table table-bordered table-hover  directoryTable" >
	<tbody class="directoryLines">	
	
		<tr>
			<td colspan='2' ><h3 style="color:#16A9B1">Validation en Copil - réunion de finalisation</h3>
			</td>
		</tr>
		<?php 
		if(isset($answer["validation"]["cter"])){ ?>
		<tr>
			<td colspan='2' align="center">
			<?php if($canAdminCter){ ?>
				<button id="generateCopil2" class="generateCopil btn btn-primary" data-id="<?php echo $_GET["id"] ?>" data-date="<?php echo date("Y-m-d") ?>" data-title="Compte Rendu de la réunion de finalisation" data-key="copilReunionFinalisation">Générer le copil de la réunion finale</button>
			<?php } ?>
		<?php $ficheAction=Document::getListDocumentsWhere([ "id"=>$_GET["id"], "type"=>Form::ANSWER_COLLECTION, "doctype"=>"file", "subKey"=>"ficheActionReunionFinalisation" ] ,"file");
			if(empty($ficheAction) && $canAdminCter )
			{ ?>
				<button id="generateFicheAction" class="generateFicheAction btn btn-primary" data-id="<?php echo $_GET["id"] ?>" data-date="<?php echo date("Y-m-d") ?>" data-copil="ficheAction" data-title="Fiche Action Finale" data-key="ficheActionReunionFinalisation">Générer la version finale de la fiche action</button>	
			<?php } ?>
			</td>
		</tr>
		<?php } ?>
		<?php if(isset($answer["validation"]["cter"])){ 
			$color = "danger";
			$lbl = "Non validé";
			if(isset($answer["validation"]["cter"]["valid"])){
				if( $answer["validation"]["cter"]["valid"] == "valid" ){
					$color = "success";
					$lbl = "Validé sans réserves";
				} else if( $answer["validation"]["cter"]["valid"] == "validReserve" ){
					$color = "warning";
					$lbl = "Validé avec réserves";
				}
			}

			$editState = ( isset( $answer["validation"]["cter"] ) ) ? "" : " <a href='javascript:;' data-type='cter' class='validEdit btn btn-default btn-xs'><i class='fa fa-pencil'></i></a>";
			

			?>
		<tr>
			<td>Avis</td>
			<td><h4 class="label label-<?php echo $color?>"><?php echo $lbl?></h4> <?php echo $editState ?></td>
		</tr>
		<tr>
			<td>Commentaire global</td>
			<td class="markdown"><?php if( isset($answer["validation"]["cter"]["description"] ) ) echo $answer["validation"]["cter"]["description"]; ?></td>
		</tr>
		<tr>
			<td>Date</td>
			<td><?php if( isset($answer["validation"]["cter"]["date"] ) ) echo $answer["validation"]["cter"]["date"]; ?></td>
		</tr>
		<?php 
		if(!empty($ficheAction))
		{ 
				//$ficheAction = array_splice($ficheAction, count($ficheAction)-1);
				$histo = 0;
				foreach($ficheAction as $k => $v){ 
					if($histo < 1){?>
					<tr>
						<td>Fiche Action Finale</td>
						<td><a href='<?php echo $v["docPath"] ?>' target='_blank' class="link-files"><i class="fa fa-file-pdf-o text-red"></i> <?php echo $v["name"] ?></a></td>
					</tr>
				<?php	$histo++; }
				} 
			} ?>

		<?php }  ?>
		<?php if($canAdminCter){ ?>
		<tr>
			<td colspan='2' class="text-center" >
				<a href="javascript:;" data-type="cter" class="validEdit btn btn-primary">Valider cette action</a>				
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>

<?php if(isset($answer["validation"]["cter"])){ ?>
<div id="copilFilecopilReunionFinalisation" class="col-xs-12">
	<div class="col-xs-12 table table-bordered table-hover  ">
		<h3 style="color:#16A9B1">Compte-rendu de COPIL</h3>
	<?php $files=Document::getListDocumentsWhere([ "id"=>$_GET["id"], "type"=>Form::ANSWER_COLLECTION, "doctype"=>"file", "subKey"=>"copilReunionFinalisation" ] ,"file");
		if(!empty($files))
		{ ?>
			<div class="content-copil">
			<?php foreach($files as $k => $v){ ?>
				<div class='col-xs-12 padding-5 shadow2 margin-top-5 margin-bottom-5' id="<?php echo $k ?>">
					<a href='<?php echo $v["docPath"] ?>' target='_blank' class="link-files"><i class="fa fa-file-pdf-o text-red"></i> <?php echo $v["name"] ?></a>
					<a href='javascript:;' class="pull-right text-red btn-remove-document" data-id="<?php echo $k ?>"><i class="fa fa-trash"></i> <?php echo Yii::t("common","Delete") ?></a>
				</div>
		<?php	} ?>
			</div>
	<?php } else {
			echo '<div class="content-copil">';
			echo "<span class='italic noCopilFile'>Aucun copil n'a été généré</span>";
			echo "</div>";
		}
	?>
	</div>
</div>
<?php } ?>




<?php if( isset($answer["validation"]["cter"]) ){

		if( isset($answer["validation"]["cter"]["valid"]) && 
		  in_array( $answer["validation"]["cter"]["valid"], ["valid", "validReserve"])  ) { ?>
		
<table class="table table-bordered table-hover  directoryTable" >
	<tbody class="directoryLines">	
	<?php if( @$answer["priorisation"] != Ctenat::STATUT_ACTION_CONTRACT ){ ?>
	<thead>
		<tr>
			<td><h3 style="color:red"><i class='fa fa-gavel'></i> le Contrat </h3>
			</td>
		</tr>	
	</thead>
	<?php } ?>

	<body>
		<tr>
			<td class="text-center">
				<?php if( @$answer["priorisation"] == Ctenat::STATUT_ACTION_VALID ){ ?>
					<h1 style="color:#24284D"><?php echo Ctenat::STATUT_ACTION_VALID ?></h1><br/>
					<img width=150 src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/PictoCTE.png">
					<img width=150 src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/Logo_republique-francaise.png">
				<?php } else {?>
					<a href="javascript:;" data-type="ctenat" class="changerStatus btn btn-primary">Contractualiser</a>
				<?php } ?>
			</td>
		</tr>		
	</tbody>
</table>
<?php  } else { ?>
<div class="col-xs-12 text-center" >
	<h1 style="color:#24284D"><?php echo Ctenat::STATUT_ACTION_REFUSE ?></h1><br/>
	<img width=150 src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/planClimat.png">
	<img width=150 src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/Logo_republique-francaise.png">
</div>
<?php }
 } ?>

<?php } else {?>
	<div class="col-xs-12">
		Seuls les référents du territoire, le porteur de l’action et l’équipe nationale ont accès à cette étape.
	</div>
<?php } ?>


<script type="text/javascript">

    function openWindowWithPost(url, data) {
        var form = document.createElement("form");
        form.target = "_blank";
        form.method = "POST";
        form.action = url;
        form.style.display = "none";

        for (var key in data) {
            var input = document.createElement("input");
            input.type = "hidden";
            input.name = key;
            input.value = data[key];
            form.appendChild(input);
        }

        document.body.appendChild(form);
        form.submit();
        document.body.removeChild(form);
    }

$(document).ready(function() { 

    mylog.log("render","/modules/survey/views/custom/ctenat/contractualiser.php");
    documentManager.bindEvents(function(){urlCtrl.loadByHash(location.hash);});
    typeObj.validation = {
		    jsonSchema : {
			    title : "Validation",
			    icon : "gavel",
			    onLoads : {
			    	sub : function(){
		    		 	dyFInputs.setSub("bg-red");
		    		},
			    },
			    save : function() { 	    	
			   
					var formData = $("#ajaxFormModal").serializeFormJSON();
					var today = new Date();
					today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();
			    	data = {
			    		id : answerId,
			    		collection : "answers",
			    		path : "validation."+formData.who,
                		value : {
                			//who : formData.who,
                			valid : formData.valid,
                			description : formData.description,
                			date : today
                		}
			    	};
			    	
			    	

			  	 	dataHelper.path2Value( data, function(params) { 
			  	 		dyFObj.closeForm();
			  	 		urlCtrl.loadByHash(location.hash);
			  	 	} );

						
			    },
			    properties : {
			    	info : {
		                inputType : "custom",
		                html:"<p></p>",
		            },
		            valid : {
                        "label" : "Valider",
                        "inputType" : "select",
                        "placeholder" : "---- Selectionner Valider ----",
                        "options" : {
                        	"notValid":"Non validé",
                            "validReserve" : "Validé avec réserves",
                            "valid" : "Validé sans réserve"
                        }
                    },
		            description : dyFInputs.textarea( tradDynForm["description"], "..." ),
		      		//  date : {
				    // 	inputType : "date",
				    // 	label : "Date",
				    // 	placeholder : "Date de Validation",
				    // 	rules : { 
				    // 		required : true,
				    // 		greaterThanNow : ["DD/MM/YYYY"]
				    // 	}
				    // },
		            who : dyFInputs.inputHidden()
			    }
			}
		};
    $('.validEdit').off().on("click", function() {
    	var setVal = { who : $(this).data("type")};
		dyFObj.openForm( typeObj.validation, null, setVal );
	});

	$('.changerStatus').off().on("click", function() {
		data = {
    		id : answerId,
    		collection : "answers",
    		path : "priorisation",
    		value : "<?php echo Ctenat::STATUT_ACTION_VALID ?>"
    	};

  	 	dataHelper.path2Value( data, function(params) { 
  	 		urlCtrl.loadByHash(location.hash);
  	 	} );
	});
    $("#copyEquilibreBudgetaire").html($("#equilibreBudget").html());

    <?php if( isset($answer["validation"]) &&
		  isset($answer["validation"]["cter"]) &&
		  in_array( @$answer["validation"]["cter"]["valid"], ["valid", "validReserve"])  ) { ?>
  	$("#suivre").html($("#murir").html())
  	$("#suivre .sectionStepTitle").html("Suivre l'action");
	<?php } ?>

		$(".generateFicheAction").click(function(){
		idAnswer=$(this).data("id"),
		dataNow=$(this).data("date");
		var titleFA=$(this).data("title");
		var subKey=$(this).data("key")
		//window.open(baseUrl+"/costum/ctenat/generatecopil/id/"+idAnswer+"/title/COPIL010203/date/"+dataNow);
        msgBoot='<div class="row">' +
                  '<div class="col-md-12"> Générer le contenu de cette fiche action en l\'état' +    
                '</div></div>';            
		bootbox.dialog({
            title: "Figer cette fiche action",
            message: msgBoot,
            buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {
                    	var dialog = bootbox.dialog ({
						    message: '<p class="text-center mb-0 loadingPdf"></p>',
						    closeButton: false
						});
            			coInterface.showLoader(".loadingPdf", "Création de la fiche action en cours");
            			$(".loadingPdf .processingLoader").removeClass("col-xs-12 margin-top-50");
                        var formData = {
                          "title": titleFA,
                          "date":dataNow,
                          "save":true
                        };
                        if(notNull(subKey))
                        	formData.subKey=subKey;
                        /* $.ajax({
						  type: "POST",
						  url: baseUrl+"/co2/export/pdfelement/id/"+idAnswer+"/type/answers/",
						  data: formData,
						  success: function(data){
						  		toastr.success("La fiche action a été créé avec succès");
						  		$(".modal").modal('hide');
						  		urlCtrl.loadByHash(location.hash);
						  },

						  dataType: "json"
						});
						*/
                        $(".modal").modal('hide');
                        if (openWindowWithPost(baseUrl+"/co2/export/pdfelement/id/"+idAnswer+"/type/answers/",formData )){
                            toastr.success("La fiche action a été créé avec succès");


                        }
                        urlCtrl.loadByHash(location.hash);

                    }
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: function() {
                  }
                }
              }
      	});
	});

	$(".generateCopilbtn").off().click(function(){
		idAnswer=$(this).data("id"),
		dataNow=$(this).data("date");
		var titleCopil=$(this).data("title");
		var subKey=$(this).data("key")
		//window.open(baseUrl+"/costum/ctenat/generatecopil/id/"+idAnswer+"/title/COPIL010203/date/"+dataNow);
        msgBoot='<div class="row">  ' +
                      '<div class="col-md-12"> ' +
                        '<form class="form-horizontal"> ' ;
                          if(!notNull(titleCopil)){
                         msgBoot+= '<label class="col-md-12 no-padding" for="awesomeness">Nom du copil : </label><br> ' +                        
                          '<input type="text" id="nameFavorite" class="nameSearch wysiwygInput form-control" value="" style="width: 100%" placeholder="'+tradDynForm.addname+'..."></input>'
                      		}
                         msgBoot += "<br>"+
                    		'<label class="col-md-12 no-padding" for="awesomeness">Noms de participants : </label><br> ' +
                          	'<textarea class="col-xs-12" id="participantsCopil"></textarea><br>'+
                          	'<label class="col-md-12 no-padding" for="awesomeness">Date du copil à récupérer : </label><br> ' +
                          	'<input id="dateCopil" type="date" value="'+dataNow+'">'+
                    		'</div> </div>' +
                    '</form></div></div>';            
		bootbox.dialog({
            title: "Remplissez les informations du COPIL",
            message: msgBoot,
            buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {
                    	
                    	var dialog = bootbox.dialog ({
						    message: '<p class="text-center mb-0 loadingPdf"></p>',
						    closeButton: false
						});
            			
            			coInterface.showLoader(".loadingPdf", "Création du copil en cours");
            			$(".loadingPdf .processingLoader").removeClass("col-xs-12 margin-top-50");
                        
                        var formData={
                          "title": (notNull(titleCopil)) ? titleCopil : $("#nameFavorite").val(),
                          "participants":$("#participantsCopil").val(),
                          "date":$("#dateCopil").val(),
                          "copil":true
                        };
                        if(notNull(subKey))
                        	formData.subKey=subKey;
                        
                /*        $.ajax({
						  type: "POST",
						  url: baseUrl+"/costum/ctenat/generatecopil/id/"+idAnswer,
						  data: formData,
						  success: function(data){
						  		toastr.success("Le copil a été créé avec succès");
						  		targetDom=(notNull(subKey))? "#copilFile"+subKey+" .content-copil": "#copilFile .content-copil";
						  		$(targetDom+ ".noCopilFile").remove();
						  		$(targetDom).prepend("<div class='col-xs-12 padding-5 shadow2 margin-top-5 margin-bottom-5'>"+"<a href='"+baseUrl+"/"+data.docPath+"' target='_blank' class='link-files'><i class='fa fa-file-pdf-o text-red'></i> "+ data.fileName+"</a>"+
											"</div>");
						  		coInterface.scrollTo("#copilFile");
								// do something in the background
								dialog.modal('hide');
								urlCtrl.loadByHash(location.hash);

						  },
						  dataType: "json"
						});*/

                        openWindowWithPost(baseUrl+"/costum/ctenat/generatecopil/id/"+idAnswer,formData );
                    }
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: function() {
                  }
                }
              }
      	});
	});
});
</script>	