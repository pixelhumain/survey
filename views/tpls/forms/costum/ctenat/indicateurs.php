<?php if($answer){ 
	if (isset($answers) && is_array($answers)) {
		foreach ($answers as $kans => $vans) {
			if (is_array($vans)) {
				foreach ($vans as $kval => $val) {
					if (is_string($val)) {
						// $val = str_replace("'", "\'", $val);
						$answers[$kans][$kval] = str_replace('"', '&quot;', $val);
					}
				}
			}
		}
	}


	$keyTpl = "indicateurs";
	$kunik = $keyTpl.$key;
	?>
<div class="form-group" style="overflow-x: auto;">
	
<style type="text/css">
	.titlecolor<?php echo $kunik ?> {
		<?php 
			if( $mode != "pdf"){	
		?>
				color: <?php echo ($titleColor) ? $titleColor : "black"; ?>;
		<?php
			}
		?>
	}
</style>

<?php 
	if( $mode != "pdf" and $mode != "r"){
		$editBtnL = ($canEdit == true) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."'  class='openIndicSelect".$kunik." btn btn-default'><i class='fa fa-plus'></i> Ajouter un indicateur </a>" : "";
		
		$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

		$indbtn = (Yii::app()->session["userId"] == $answer["user"]) ? "<a href='javascript:;' onclick='dyFObj.openForm('indicator');' class='btn btn-default margin-top-5 margin-bottom-5'><i class='fa fa-plus'></i> Créer un nouvel indicateur </a>" : "";

		$classforpdf = "";
	} else {
		$editBtnL = "";
		$editParamsBtn = "";
		if ($mode == "pdf" ) {
			$classforpdf = "calendar-table";
		}else{
			$classforpdf = "";
		}
	}

$paramsData = [ "titles" => [ "Indicateur",
							"Objectif / Réalisé",
							// "Réf. 2018",
							// "Résultat 2019",
							// "Résultat 2020",
							// "Résultat 2021",
							// "Résultat 2022"
							],
				"keys" => [ 
					//"res2018","res2019","res2020","res2021","res2022"
					] ];
foreach (Ctenat::$years as $i => $y) {
	$txt = ($i == 0) ? "Réf. ".$y : "Résultat ";
	$paramsData["titles"][] = $txt.$y;
	$paramsData["keys"][] = "res".$y;
}
				

//temporaire car specific au CTE
$indicateurs = Ctenat::getIndicator();

// foreach ($indicateurs as $key => $value) {
// 	$pois = PHDB::findbyIds(Poi::COLLECTION, $key);
// 	var_dump($pois);
// }

//var_dump($indicateurs);

 //we add a global period params in forms@ctenatForm.params.period
if(isset($parentForm["params"]["period"])){
	$paramsData[ "titles"] = ["Indicateur",
							"Objectif / Réalisé"
	];
	$paramsData[ "keys"] = [];
	$initFrom = $parentForm["params"]["period"]["from"];
	$from = $parentForm["params"]["period"]["from"];
	$to = $parentForm["params"]["period"]["to"];
	while ( $from <= $to) {
		if($initFrom == $from)
			$paramsData[ "titles"][] = "Réf. ".$from;
		else 
			$paramsData[ "titles"][] = "Résultat ".$from;

		$paramsData[ "keys"][] = "res".$from;
    	$from++;
	}
} else {
	if( isset($parentForm["params"][$kunik]) ) 
		$paramsData =  $parentForm["params"][$kunik];
}
$properties = [
	"indicateur" => [
        "inputType" => "select",
        "label" => "Indicateur",
        "placeholder" => "Indicateur",
        "list" => "indicateurs",
        "select2" => [
            "multiple" => false
        ],
        "rules" => [ "required" => true ]
    ],
    "info" => [
    	"inputType" => "custom",
    	"html" => '<div id="fiche'.$kunik.'" ></div>'
    ]
];

?>	
<style type="text/css">
	.bgGrey{background-color: grey;}
</style>
<div ><h4 class="titlecolor<?php echo $kunik ?> pdftittlecolor text-center"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?>
	
</h4>
<?php 
if($mode != "pdf"){
?>
Choisissez des indicateurs parmi ceux déjà proposés par les territoires (essayez dans la mesure du possible d’utiliser des indicateurs existant)
<br/>

<?php if($canEdit){ ?>
	Au besoin, vous pouvez déclarer un nouvel indicateur
	<a href="javascript:;" onclick="dyFObj.openForm('poi',null,null,null,indicatorCostumPoi);" class="btn btn-default margin-top-5 margin-bottom-5"><i class="fa fa-plus"></i> Créer un nouvel indicateur </a>
	<?php 
	}
}
?>
</div>

<?php 
if( $mode != "pdf"){
?>

<table class="table table-bordered table-hover  directoryTable <?php echo $classforpdf ?>" id="panelAdmin<?php echo $key?>">
	<thead>	
		<?php if(isset($answers)){ ?>
		<tr>
			<?php foreach ($paramsData["titles"] as $keyt => $t) {
				echo "<th>".$t."</th>";
			} 
			if( $mode != "pdf" and $mode != "r"){
			?>
				<th></th>
			<?php } ?>
		</tr>
		<?php } ?>
	</thead>
	<tbody class="directoryLines">	
		<!-- <tr> -->
		<?php 
		$ct = 0;
		//always show indicateur emploi 
		$emploiExists = false;
		if(isset($answers)){
		
		//var_dump($indicateurs); //exit;
		foreach ($answers as $q => $a) {
			if(isset($a["indicateur"])){
				//foreach ($a["indicateur"] as $kA => $valA) {
				echo "<tr>".
					//"<td id='indic".$ct."' rowspan='2' style='vertical-align : middle;text-align:center;'>".( !empty($indicateurs[$valA]) ?  $indicateurs[$valA] : "" )."</td>".
					"<td id='indic".$ct."' rowspan='2' style='vertical-align : middle;text-align:center;'>".( !empty($indicateurs[$a["indicateur"]]) ?  $indicateurs[$a["indicateur"]] : "" )."</td>".
					"<td>Objectif</td>".
					'<td style="vertical-align : middle;text-align:center;background-color:grey;"></td>';
					foreach ($paramsData["keys"] as $i => $k) {
						if($i>0)
							echo "<td class='editContent' data-key='".$k."' data-indic='indic".$ct."' data-pos='".$q."' data-type='objectif'>".(isset($a["objectif"][$k]) ? $a["objectif"][$k]:"")."</td>";	
					}

					if( $mode != "pdf" and $mode != "r"){	
					?>
					
					<td rowspan='2' >
						<?php
						if(($mode == "w" || $mode == "fa") && ( $canAdminAnswer == true || $canEdit == true ) ){
								$keyTplB = (isset($kunik)) ? $kunik : $keyTpl;
								?>
							<a href='javascript:;'  data-id='<?php echo $answer["_id"]?>' data-collection='<?php echo Form::ANSWER_COLLECTION ?>' data-key='<?php echo $q?>' data-path='<?php echo $answerPath.$q ?>' class='edit<?php echo $keyTplB ?> previewTpl btn btn-xs btn-default'><i class='fa fa-pencil'></i></a>

							<a href='javascript:;' data-id='<?php echo $answer["_id"]?>' data-collection='<?php echo Form::ANSWER_COLLECTION ?>' data-key='<?php echo $keyTplB.$q ?>' data-path='<?php echo $answerPath.$q ?>' class='deleteIndicLine<?php echo $kunik ?> previewTpl btn btn-xs btn-danger'><i class='fa fa-times'></i></a>

							<!-- $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
								"canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
								"id" => $answer["_id"],
								"collection" => Form::ANSWER_COLLECTION,
								"q" => $q,
								"path" => $answerPath.$q,
								"kunik"=>$kunik ] ); -->
						<?php
						} ?>

						<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>','<?php echo $key ?>','<?php echo (string)$form["_id"] ?>')">
									<?php 
										echo PHDB::count(Comment::COLLECTION, array("contextId"=>(string)$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q)); ?> 
										<i class='fa fa-commenting'></i></a>
					</td>
				<?php
				}
				echo "</tr>";
				$editableClass = ( !empty( $isSuivi ) ) ? "editContent" : "bgGrey" ;
				echo "<tr>".
					"<td>Réalisé</td>";
					foreach ($paramsData["keys"] as $i => $k) {
						echo "<td class='editContent' data-key='".$k."' data-indic='indic".$ct."' data-pos='".$q."' data-type='reality'>".(isset($a["reality"][$k]) ? $a["reality"][$k]:"")."</td>";	
					}
				echo "</tr>";

				$ct++;
				//}
			}
		}
		}

		 ?>

	</tbody>
</table>

<?php
	}else{
	?>

<table class="table table-bordered table-hover  directoryTable <?php echo $classforpdf ?>" id="panelAdmin<?php echo $key?>">
	<thead>	
		<?php if(isset($answers)){ ?>
		<tr>
			<?php foreach ($paramsData["titles"] as $keyt => $t) {
				if ($t != "Indicateur") 
				echo "<th>".$t."</th>";
			} 
			?>
		</tr>
		<?php } ?>
	</thead>
	<tbody class="directoryLines">	
		<!-- <tr> -->
		<?php 
		$ct = 0;
		//always show indicateur emploi 
		$emploiExists = false;
		if(isset($answers)){
		
		//var_dump($indicateurs); //exit;
		foreach ($answers as $q => $a) {
			if(isset($a["indicateur"])){
				//foreach ($a["indicateur"] as $kA => $valA) {
				echo "<tr>".
					//"<td id='indic".$ct."' rowspan='2' style='vertical-align : middle;text-align:center;'>".( !empty($indicateurs[$valA]) ?  $indicateurs[$valA] : "" )."</td>".
					'<td id="indic'.$ct.'" colspan="'.(sizeof($paramsData["titles"]) - 1).'">'.( !empty($indicateurs[$a["indicateur"]]) ?  $indicateurs[$a["indicateur"]] : "" ).'</td></tr>';


				echo "<tr>".
					//"<td id='indic".$ct."' rowspan='2' style='vertical-align : middle;text-align:center;'>".( !empty($indicateurs[$valA]) ?  $indicateurs[$valA] : "" )."</td>".
					"<td>Objectif</td>".
					'<td style="vertical-align : middle;text-align:center;background-color:grey;"></td>';
					foreach ($paramsData["keys"] as $i => $k) {
						if($i>0)
							echo "<td class='editContent' data-key='".$k."' data-indic='indic".$ct."' data-pos='".$q."' data-type='objectif'>".(isset($a["objectif"][$k]) ? $a["objectif"][$k]:"")."</td>";	
					}

					if( $mode != "pdf" and $mode != "r"){	
					?>
					
					<td rowspan='2' >
						<?php
						if(($mode == "w" || $mode == "fa") && ( $canAdminAnswer == true || $canEdit == true ) ){
							echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
								"canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
								"id" => $answer["_id"],
								"collection" => Form::ANSWER_COLLECTION,
								"q" => $q,
								"path" => $answerPath.$q,
								"kunik"=>$kunik ] );
						} ?>

						<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>','<?php echo $keyTpl ?>','<?php echo (string)$form["_id"] ?>')">
									<?php 
										echo PHDB::count(Comment::COLLECTION, array("contextId"=>(string)$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q)); ?> 
										<i class='fa fa-commenting'></i></a>
					</td>
				<?php
				}
				echo "</tr>";
				$editableClass = ( !empty( $isSuivi ) ) ? "editContent" : "bgGrey" ;
				echo "<tr>".
					"<td>Réalisé</td>";
					foreach ($paramsData["keys"] as $i => $k) {
						echo "<td class='editContent' data-key='".$k."' data-indic='indic".$ct."' data-pos='".$q."' data-type='reality'>".(isset($a["reality"][$k]) ? $a["reality"][$k]:"")."</td>";	
					}
				echo "</tr>";

				$ct++;
				//}
			}
		}
		}

		 ?>

	</tbody>
</table>

<?php
	}
	?>

</div>

<?php
	//}
	$caracterForm = PHDB::findOne(Form::COLLECTION, array('id' => 'caracter'));
?>

<?php 
	if($mode != "r" and $mode != "pdf"){ 
	?>



<script type="text/javascript">

var indicatorCostumPoi = {
    "beforeBuild" : {
        "properties" : {
            "name" : {
                "label":"Intitulé de l’indicateur",
                "placeholder":"Intitulé de l’indicateur",
                "rules" : {
                    "required" : true
                }
            },
            
            "domainAction" : {
                "inputType" : "select",
                "label" : "Domaines(s) d’action",
                "placeholder" : "Choisir un domaine d'action",
                "groupOptions" : true,
                "groupSelected" : true,
                "list" : "domainAction",
                "select2" : {
                    "multiple" : true
                },
                "rules" : {
                    "required" : true
                }
            },
            "objectifDD" : {
                "inputType" : "select",
                "label" : "Objectif(s) de développement durable",
                "groupOptions" : true,
                "groupSelected" : true,
                "placeholder" : "Choisir un objectif(s) de développement durable",
                "list" : "cibleDD",
                "select2" : {
                    "multiple" : true
                },
                "rules" : {
                    "required" : true
                }
            },
            "typectenat" : {
                "inputType" : "select",
                "label" : "Type d’indicateur",
                "groupOptions" : false,
                "groupSelected" : false,
                "placeholder" : "Choisir un type d’indicateur",
                "list" : "typeindicateur",
                "select2" : {
                    "multiple" : false
                },
                "rules" : {
                    "required" : true
                }
            },

            //New Indicator fields 
     //        "nameLong" : {
     //            "label":"Intitulé Long de l’indicateur",
     //            "placeholder":"Intitulé Long de l’indicateur"
     //        },
     //        "isIndicateurPrincipale" :{
     //        	"inputType" : "hidden",
     //        	"value" : true
     //        },
     //        "perimetreGeo" : {
     //            "inputType" : "select",
     //            "label" : "Périmètre Géo",
     //            "groupOptions" : false,
     //            "groupSelected" : false,
     //            "placeholder" : "Périmètre Géo",
     //            "options" : {
     //            	"Action" :"Action", 
     //    			"EPCI" : "EPCI",
     //    			"Quartier" : "Quartier",
     //    			"CTE" : "CTE" ,
     //    			"région" : "région" ,
     //    			"France" : "France" ,
     //    			"Autre" : "Autre"
     //    		},
     //            "select2" : {
     //                "multiple" : true
     //            }
     //        },
     //        "periode" : {
     //            "inputType" : "select",
     //            "label" : "Periode",
     //            "groupOptions" : false,
     //            "groupSelected" : false,
     //            "placeholder" : "Periode",
     //            "options" : {
     //            	"Evolution" :"Evolution", 
     //    			"Ponctuel" : "Ponctuel",
	    //     		"Projet" : "Projet",
					// "Action terminée" : "Action terminée",
					// "A échéance du CTE" : "A échéance du CTE",
					// "Annuelle" : "Annuelle"
				 // },
     //            "select2" : {
     //                "multiple" : true
     //            }
     //        },
            //--- End New Indicator fields 


            "definition" : {
                "inputType" : "textarea",
                "label" : "Définition",
                "placeholder" : "Définition",
                "rules" : {
                    "required" : true
                }
            },
            "methodeCalcul" : {
                "inputType" : "textarea",
                "label" : "Méthode de calcul",
                "placeholder" : "Méthode de calcul",
                "rules" : {
                    "required" : true
                }
            },
            "unity1" : {
                "inputType" : "text",
                "label" : "Unité",
                "placeholder" : "Unité",
                "rules" : {
                    "required" : true
                }
            },
            "sourceData1" : {
                "inputType" : "textarea",
                "label" : "Où trouver les données ?",
                "placeholder" : "Où trouver les données ?",
                "rules" : {
                    "required" : true
                }
            },
            "originIndicator" : {
                "inputType" : "text",
                "label" : "Origine de l’indicateur",
                "placeholder" : "Origine de l’indicateur"
            },
            "echelleApplication" : {
                "inputType" : "select",
                "label" : "A quelle échelle l’indicateur a-t-il du sens ? / peut elle etre calculée ?",
                "placeholder" : "A quelle échelle l’indicateur a-t-il du sens ? / peut elle etre calculée ?",
                "list" : "echelle",
                "select2" : {
                    "multiple" : true
                },
                "rules" : {
                    "required" : true
                }
            }
        }
    },
    "onload" : {
        "actions" : {
            "setTitle" : "Créer un nouvel indicateur",
            "html" : {
                "infocustom" : ""
            },
            "presetValue" : {
                "type" : "indicator"
            },
            "hide" : {
                "urlsarray" : 1,
                "locationlocation" : 1,
                "formLocalityformLocality" : 1,
                "parentfinder" : 1,
                "tagstags" : 1,
                "descriptiontextarea" : 1,
                "imageuploader" : 1,
                "parentfinder" : 1,
                "breadcrumbcustom" : 1
            }
        }
    },
    afterSave : function(){
    	dyFObj.closeForm();
    	//urlCtrl.loadByHash( location.hash );
    	alert("link direct to action");
    }
};


if(typeof costum.lists == "undefined")
	costum.lists = {};
costum.lists.indicateurs = <?php echo json_encode($indicateurs); ?>;

var <?php echo $kunik?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
 var ind;

$(document).ready(function() { 
	mylog.log("render","/var/www/dev/modules/costum/views/tpls/forms/<?php echo $kunik
	 ?>.php");
	
	sectionDyf.<?php echo $kunik ?> = {
		"jsonSchema" : {	
	        "title" : "Résultats attendus de l'action",
            "description" : "Décrivez ici la manière dont nous pourrons évaluer, dans 1 an / 2 ans / 3 ans / 4 ans, que le projet aura réussi : quels objectifs chiffrés, et quelle méthode pour mesurer les progrès.",
            "icon" : "fa-calendar-check-o",
	        "properties" : <?php echo json_encode( $properties ); ?>,
	         onLoads : { 
		    	onload : function(){
		    		$('#indicateur').on('change', function() {
				      ind = $(this).val();


				      ajaxPost(null, baseUrl+"/costum/ctenat/getindicateur/id/"+ind, null,
		   				function(data){
		    				console.log(data.result[ind]);
		    				var name = "";
		    				if(typeof data.result[ind].name != "undefined"){
		    					name = data.result[ind].name;
		    				}

		    				var htmlrender = '<div class="form-horizontal" style ="border:solid 1px;" ><h5>'+name+'</h5>';
		    				var htmlrenderlist = [];

		    				var titlelablel = {

		    					"domainAction" : "Domaine(s) d'action",
		    					"typectenat" : "Type d’indicateur",
		    					"definition" : "Définition",
		    					"objectifDD" :  "Objectif(s) de développement durable",
		    					"methodeCalcul" : "Méthode de calcul",
		    					"unity1" : "Unité",
		    					"sourceData1" : "Où trouver les données",
		    					"originIndicator" : "Origine de l’indicateur",
		    					"echelleApplication" : "A quelle échelle l’indicateur a-t-il du sens ? / peut elle etre calculée ?"

		    				}

		    				$.each(titlelablel, function( index, value ) {
							  	if(typeof data.result[ind][index] != "undefined"){
							  		if(Array.isArray(data.result[ind][index])){
							  			htmlrender += '<div class="form-group"><label for="" class="col-xs-3 control-label " >'+titlelablel[index]+'</label><div class="col-xs-9"><label for="" class="control-label" style = "text-align: left;font-weight: 100;">'+data.result[ind][index].join('<br/>')+'</label></div></div>';
							  		} else {
							  			htmlrender += '<div class="form-group"><label for="" class="col-xs-3 control-label " >'+titlelablel[index]+'</label><div class="col-xs-9"><label for="" class="control-label" style = "text-align: left;font-weight: 100;">'+data.result[ind][index]+'</label></div></div>';
							  		}
		    					}
							});

							htmlrender += '</div>';

		    				
				      $("#fiche<?php echo $kunik; ?>").html(htmlrender);
		    			}
		    			);

				      
				    });
		    	}
    	   	},
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik
	             ?>.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                	dyFObj.closeForm();
	                   reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>"); 
	                   reloadInput("caracter", "<?php echo (string)$caracterForm["_id"] ?>");
	                } );
	            }

	    	}
	    }
	};

	sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "<?php echo $kunik ?> config",
	        "icon" : "fa-cog",
	        "properties" : {
	            titles : {
	                inputType : "array",
	                label : "Liste des titres",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.titles
	            },
	            keys : {
	                inputType : "array",
	                label : "Liste des clefs",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.keys
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	        		if(val.inputType == "array")
	        		 	tplCtx.value[k] = getArray('.'+k+val.inputType);
	        		else
	        		 	tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                	dyFObj.closeForm();
	                    reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>"); 
	                    reloadInput("caracter", "<?php echo (string)$caracterForm["_id"] ?>");
	                } );
	            }

	    	}
	    }
	};

	var max<?php echo $kunik ?> = 0;
	var nextValuekey<?php echo $kunik ?> = 0;
	var havePrincipal = false;

	if(notNull(<?php echo $kunik ?>Data)){
		
		$.each(<?php echo $kunik ?>Data, function(ind, val){
			if(parseInt(ind) == 0){
				havePrincipal = true;
			}
		});
		if(!havePrincipal){
			$.each(<?php echo $kunik ?>Data, function(ind, val){
				if(parseInt(ind) > max<?php echo $kunik ?>){
					max<?php echo $kunik ?> = parseInt(ind);
				}
			});
		}
	}
	if(!havePrincipal){
		nextValuekey<?php echo $kunik ?> = 0;
	}else{
		nextValuekey<?php echo $kunik ?> = max<?php echo $kunik ?> + 1;
	}


	//adds a line into answer
    $(".add<?php echo $kunik ?>").off().on("click",function() {  

        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data)) ? <?php echo $kunik ?>Data.length : 0);
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
    });

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  	
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

	$('.editContent').off().click( function() {
		//mylog.log("ping ping");
		tplCtx.id = "<?php echo $answer["_id"] ?>";
		tplCtx.collection = "<?php echo Form::ANSWER_COLLECTION ?>";      
		tplCtx.path = "<?php echo $answerPath ?>"+$(this).data('pos')+"."+$(this).data('type')+"."+$(this).data('key');

		mylog.log(".editContent", "<?php echo $kunik ?>","tplCtx",tplCtx);
		var indic = $('#'+$(this).data('indic')).html();
		bootbox.prompt({
				inputType: 'number',
	            title: "Valeur ? <span style='color:red'>(uniquement des chiffres)</span><br/>"+indic, 
	            callback : function(result){ 
	                if (result === null) {
				    	//alert("null");
				    } 
				    else 
				    {
		                tplCtx.value = result;
			    		mylog.log(".editContent", "<?php echo $kunik ?>","tplCtx",tplCtx);
						
						dataHelper.path2Value( tplCtx, function(params) { 
							dyFObj.closeForm();
							reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>"); 
						} );
					}
	            }
	        });

	 	});

	$(".openIndicSelect<?php echo $kunik ?>").off().on("click",function() {  

        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data)) ? <?php echo $kunik ?>Data.length : 0);
        smallMenu.openAjaxHTML(baseUrl+"/costum/ctenat/selectindicateur/id/"+tplCtx.id+"/key/<?php echo $key ?>/formId/<?php echo (string)$form["_id"] ?>");
    });

    $(".deleteIndicLine<?php echo $kunik ?>").off().on("click", function() {
    	formId = $(this).data("id");
      key = $(this).data("key");
      pathLine = $(this).data("path");
      collection = $(this).data("collection");
      if (typeof $(this).data("parentid") != "undefined") {
          var parentfId = $(this).data("parentid");
      }
      bootbox.dialog({
          title: trad.confirmdelete,
          message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
          buttons: [
            {
              label: "Ok",
              className: "btn btn-primary pull-left",
              callback: function() {
                var formQ = {
      				  	value:null,
      				  	collection : collection,
      				  	id : formId,
      				  	path : pathLine
      				  };

            if (typeof parentfId != "undefined") {
                formQ["formParentId"] = parentfId;
            }
				  
				  dataHelper.path2Value( formQ , function(params) { 
						// $("#"+key).remove();
                        //location.reload();
                        reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
	    				reloadInput("caracter", "<?php echo (string)$caracterForm["_id"] ?>");
					} );
              }
            },
            {
              label: "Annuler",
              className: "btn btn-default pull-left",
              callback: function() {}
            }
          ]
      });
    })
    

    });

  	</script>
<?php } } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>