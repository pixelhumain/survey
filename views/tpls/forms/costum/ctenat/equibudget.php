<?php 
	
	$percent = ($totalBudg) ? $totalFin*100/$totalBudg : 0;
	$delta = $totalFin - $totalBudg ;
	$col = "";
	$percol = "primary";
	if($delta > 0){
		$col = "text-green";
		$percol = "success";
	}
	else if($delta < 0 ){
		$col = "text-red";
		$percol = "danger";

	}

	if( $mode == "pdf"){
		$classforpdf = "calendar-table";
	}else {
		$classforpdf = "";
	}
			
 ?>


 <style type="text/css">

</style>
<div class='col-xs-12 ' id="equilibreBudget">
	<h4 class="titlecolor<?php echo $kunik ?> pdftittlecolor text-center">EQUILIBRE BUDGETAIRE</h4>
	L'équilibre budgétaire doit être atteint pour pouvoir passer à la phase de contractualisation
	<div class="progress">
	  <div class="progress-bar progress-bar-<?php echo $percol ?>" style="width: <?php echo $percent ?>%">
	    <span class="sr-only"><?php echo $percent ?>% Complete (success)</span>
	  </div>
	</div>
	<table class="table table-bordered table-hover <?php echo $classforpdf ?>">
		<tbody class="">
			<tr>
				<td>BUDGET prévisionnel</td>
				<td><?php echo rtrim(rtrim(number_format($totalBudg , 3, ".", " "), '0'), '.') ?>€</td>
			</tr>
			<tr>
				<td>Financements acquis</td>
				<td><?php echo rtrim(rtrim(number_format($totalFin , 3, ".", " "), '0'), '.') ?> €</td>
			</tr>
			<tr>
				<td>Delta</td>
				<td><?php echo "<span class='".$col."'>".rtrim(rtrim(number_format($delta , 3, ".", " "), '0'), '.')." € </span>" ?> </td>
			</tr>
		</tbody>
	</table>
</div>