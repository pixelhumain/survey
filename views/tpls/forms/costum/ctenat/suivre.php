<style type="text/css">
.swMain ul li > a.done .stepNumber {
    border-color: #0A2F62;
    background-color: #16A9B1; 
}

swMain > ul li > a.selected .stepDesc, .swMain li > a.done .stepDesc {
 color: #0A2F62;  
 font-weight: bolder; 
}

.swMain > ul li > a.selected::before, .swMain li > a.done::before{
  border-color: #0A2F62;      
}
</style>
Cette étape ne sera accessible qu’une fois l’action complétée et validée.



<script type="text/javascript">

$(document).ready(function() {
<?php if( isset($adminAnswers["validation"]) &&
		  isset($adminAnswers["validation"]["ctenat"]) &&
		  isset($adminAnswers["validation"]["cter"]) &&
		  in_array( @$adminAnswers["validation"]["ctenat"]["valid"], ["valid", "validReserve"]) &&
		  in_array( @$adminAnswers["validation"]["cter"]["valid"], ["valid", "validReserve"])  ) { ?>

  	$("#suivre").html($("#murir").html());
  	$("#suivre .sectionStepTitle").html("Suivre l'action");
	
<?php } ?>
$("#suivre .sectionStepTitle").html("Suivre l'action");

});

</script>
