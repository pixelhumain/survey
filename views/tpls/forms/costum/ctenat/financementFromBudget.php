<div class="form-group">
	<?php 
	if($mode == "r" || $mode == "pdf"){ ?>
		<label ><h4 style="color:<?php echo (!empty($titleColor) ? $titleColor : "black" ); ?>"><?php echo $label ; ?></h4></label>
    <?php echo $info ?>
	<?php 
	} else {
		?>
		<label ><h4 style="color:<?php echo (!empty($titleColor) ? $titleColor : "black" ); ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL ; ?></h4></label>
		<?php echo $info ;
		if( !isset($budgetKey) ) 
					echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> THIS FIELD HAS TO BE CONFIGURED FIRST ".$editParamsBtn."</span>";?>
	<?php 
	} ?>
<table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>">
	<thead>
		<?php 		
		if( count($answers)>0 ){ ?>
		<tr>
			<?php 
			
			foreach ($properties as $i => $inp) {
				echo "<th>".$inp["placeholder"]."xxxx</th>";
			} ?>

			<?php 
			if($mode != "r" && $mode != "pdf"){ ?>
				<th></th>
			<?php } else { ?>
				<th>State</th>
			<?php } ?>
		</tr>
		<?php } ?>
	</thead>
	<tbody class="directoryLines">	
		<?php 
		$ct = 0;
		$totalFunded = 0;
		$total = 0;
		if($answers){
			foreach ($answers as $q => $a) {

				//prepare td content
				$trStyle = "";
				$tds = "";
				foreach ($properties as $i => $inp) 
				{
					$size = (isset($inp["size"])) ? "col-xs-".$inp["size"] : "";
					$tds .= "<td class='".$size."'>";

					if( $i == "price" && isset($a[$i]) ){
						$tds .= rtrim(rtrim(number_format($a[$i] , 3, ".", " "), '0'), '.')." €";;
						if(isset($percentDiv)){
							$tds .= $percentDiv;
							$tds .= "<br/> <span class='label label-primary'>".floor($percentAmountFunded)."%</span>";
							$percentDiv = "";
							$percentAmountFunded = 0;
						}
					}
					else if( $i == "financer"  )
					{
						
						$totalAmountFunded = 0;
						if( isset($a["financer"]) )
						{
							//if(count($a["financer"])>1){}
							$totalAmountFunded = 0;
							$tds .= "<table class='table table-bordered table-hover  directoryTable'>";
							foreach ($a["financer"] as $ix => $fin) {
								$tds .= "<tr>";
								
									$tds .= "<td class='col-xs-4'>";
										if(!empty($fin["id"])){
											$o = PHDB::findOne(Organization::COLLECTION,[ "_id" => new MongoId($fin["id"]) ],
																					 	["name","slug"]);
											$tds .= $o["name"];
										} else if( isset( $fin["email"] ) && isset( $fin["name"] ) ){
											$tds .= $fin["name"]."<br/>".$fin["email"];
										}else 
											$tds .= "unkknown";	
									$tds .= "</td>";

									$tds .= "<td  class='col-xs-5'>";
										if(!empty($fin["line"]))
											$tds .= $fin["line"];
									$tds .= "</td>";

									$tds .= "<td class='col-xs-2'>";
										if(!empty($fin["amount"])){
											$tds .= rtrim(rtrim(number_format($fin["amount"] , 3, ".", " "), '0'), '.')." €";
											$totalAmountFunded += (int)$fin["amount"];
											
										}
									$tds .= "</td>";

									$tds .= "<td class='col-xs-2'>";
										if(!empty($fin["amount"]) && isset($a["price"])){
											
											$percent = $fin["amount"] * 100 / (int)$a["price"];
											$tds .= " <span class='pull-right label label-primary'>".floor($percent)."%</span>";
										}
									$tds .= "</td>";
								
								$tds .= "</tr>";
							}

							$totalFunded += $totalAmountFunded;

							$tds .= "</table>";
							if($totalAmountFunded>0 && isset( $a["price"] )){
								$percentAmountFunded =  $totalAmountFunded * 100 / (int)$a["price"];
								$total += (int)$a["price"];
								$percol = "danger";
								$trStyle = "background-color:#ffdada";
								if( $percentAmountFunded >= 50 ){
									$percol = "warning";
									$trStyle = "background-color:#ffe7d1";
								}
								if( $percentAmountFunded >= 100 ){
									$percol = "success";
									$trStyle = "background-color:#e5ffe5";
								}

								$percentDiv = '<div class="progress btnFinancer"  data-id="'.$answer["_id"].'" data-budgetpath="'.$copy.'" data-form="'.$copyF.'" data-pos="'.$q.'"  style="cursor:pointer;margin-bottom: 0px;">'.
											  '<div class="progress-bar progress-bar-'.$percol.'" style="width:'.$percentAmountFunded.'%">'.
												    '<span class="sr-only">'.$percentAmountFunded.'% Complete</span>'.
											  '</div>'.
											'</div>';
							}
						}

						$tds .= "<div class='col-xs-12 text-center'>".
									" <div class='col-xs-12'>";
									if($canEdit){
										$tds .= "<a href='javascript:;' data-id='".$answer["_id"]."' data-budgetpath='".$copy."' data-form='".$copyF."' data-pos='".$q."'  class='btn btn-xs btn-primary btnFinancer '><i class='fa fa-plus'></i> Ajouter un financeur</a>";
									}
						$tds .= "</div></div>";
					}
					else if( isset($a[$i]) )
						$tds .= $a[$i];
					// else if( $i == "amount" && isset( $a["financer"]["amount"] ) ) 
					// 	$tds .= $a["financer"]["amount"];
					// else if( $i == "line" && isset( $a["financer"]["line"] ) ) 
					// 	$tds .= $a["financer"]["line"];
					
					$tds .= "</td>";
				} 
					
			

				echo "<tr id='".$kunik.$q."' class='".$kunik."Line text-center' style='".$trStyle."'>";
				echo $tds;	
				?>
				<td>XXXXXXX</td>
				<?php
				if( true ){	?>
					<td>
						<?php 
							$color = "default";
							$valbl = "?";
							$tool= "En attente de validation";
							if( isset($a["validFinal"]["valid"]) ){
								if( $a["validFinal"]["valid"] == "validated" ){
									$color = "success";
									$valbl = "V";
									$tool="Validé sans réserve";
								} else if( $a["validFinal"]["valid"] == "reserved" ){
									$color = "warning";
									$valbl = "R";
									$tool="Validé avec réserves";
								} else if( $a["validFinal"]["valid"] == "refused" ){
									$color = "danger";
									$valbl = "NV";
									$tool="Non validé";
								}
							}

							echo "<a href='javascript:;' data-id='".$answer["_id"]."' data-budgetpath='".$copy."' data-form='".$copyF."' data-pos='".$q."'  class='btnValidFinance btn btn-xs btn-".$color." margin-left-5 padding-10 tooltips' data-toggle='tooltip' data-placement='left' data-original-title='".$tool."'>".$valbl."</a>";

						
							?>
						<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>','<?php echo $keyTpl ?>','<?php echo (string)$form["_id"] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
					</td>

				<?php 
				}else{ ?>
					<td>
						<?php 
							if(!empty($a["validFinal"]["valid"]))
								echo $a["validFinal"]["valid"];
						?>
					</td>
				<?php
				}
				$ct++;
				echo "</tr>";
			}
		}



if($total > 0){

	echo "<tr class='bold'>";
	echo 	"<td colspan=2 style='text-align:right'>TOTAL À FINANCER : </td>";
	echo 	"<td colspan=2>".rtrim(rtrim(number_format($total , 3, ".", " "), '0'), '.')." €</td>";
	echo "</tr>";

}

if($totalFunded > 0){

	echo "<tr class='bold'>";
	echo 	"<td colspan=2 style='text-align:right'>TOTAL FINANCÉ: </td>";
	echo 	"<td colspan=2>".rtrim(rtrim(number_format($totalFunded , 3, ".", " "), '0'), '.')." €</td>";
	echo "</tr>";

}






?>
		</tbody>
	</table>
</div>



<?php 
$percol = "warning";
$financedPercentage = (!empty($total)) ? $totalFunded * 100 / $total: 0;
if( $financedPercentage >= 100 ){
	$percol = "success";
}
echo "<h4 style='color:".(($titleColor) ? $titleColor : "black")."'>Pourcentage de Financement Globale</h4>".
'<div class="progress " style="cursor:pointer" >'.
  '<div class="progress-bar progress-bar-'.$percol.'" style="width:'.$financedPercentage.'%">'.
	    '<span class="sr-only">'.$financedPercentage.'% Complete</span>'.
  '</div>'.
'</div>'; ?>

<table class="table table-bordered table-hover  ">
	<tbody class="">
		<tr>
			<td>Total à financer</td>
			<td><?php echo rtrim(rtrim(number_format($total , 3, ".", " "), '0'), '.') ?> €</td>
		</tr>
		<tr>
			<td>Total financé</td>
			<td><?php echo rtrim(rtrim(number_format($totalFunded , 3, ".", " "), '0'), '.') ?> €</td>
		</tr>
		<tr>
			<td>Pourcentage financé</td>
			<td><?php echo floor($financedPercentage) ?> %</td>
		</tr>
		<?php if($total>$totalFunded){ ?>
		<tr>
			<td>Reste à financer </td>
			<td><?php echo rtrim(rtrim(number_format(floor($total-$totalFunded) , 3, ".", " "), '0'), '.') ?> €</td>
		</tr>
		<tr>
			<td>% Reste à financer </td>
			<td><?php echo floor(100-$financedPercentage)  ?> %</td>
		</tr>	
		<?php } ?>	
	</tbody>
</table>