<style type="text/css">
	.profile-page .card {
            background: #fff;
            margin-bottom: 30px;
            transition: .5s;
            border: 0;
            border-radius: .1875rem;
            display: inline-block;
            position: relative;
            width: 100%;
            box-shadow: none;
            padding: 20px 0px 20px 0px;
            box-shadow: 0px 10px 20px #354c57;
            position: relative;
    		overflow: hidden;
        }
        .profile-page .card:after {
		    content: '';
		    display: block;
		    width: 190px;
		    height: 350px;
		    background: #16A9B1;
		    position: absolute;
		    animation: rotatemagic 0.75s cubic-bezier(0.425, 1.04, 0.47, 1.105) 1s both;
		}
        .profile-page .card .body {
            font-size: 14px;
            color: #424242;
            padding: 0px 20px;
            font-weight: 400;
        }
        .profile-page .profile-header {
            position: relative
        }

        .profile-page .profile-header .profile-image img {
            border-radius: 50%;
            width: 90px;
            height: 90px;
            border: 3px solid #fff;
            box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23)
        }

        .profile-page .profile-header h4 {
            margin-top: 5px;
            border-bottom: 1px solid #354c57;
            padding-bottom: 5px;
            font-size: 20px;
            font-weight: 400;
            text-transform: none;
        }
        .profile-page .profile-header .job_post, .profile-page .profile-header .type {
            font-size: 18px;
            padding: 8px 0px 0px;
            display: block;
        }
        .profile-page .profile-header .job_post i{
            color: #354c57;
        }
        .profile-page .profile-header .avatar-contain, .profile-page .profile-header .about-contain, .profile-page .profile-header .type-elt, .profile-page .profile-header .btn-contain {
             z-index: 2;
        }
        @media (max-width: 767px) {
            .profile-page .profile-header .avatar-contain, .profile-page .profile-header .about-contain {
                text-align: center!important;
            }
            .profile-page .profile-header .avatar-contain{
                margin-bottom: 20px;
            }
            .profile-page .profile-header .profile-image img {
	            border-radius: 50%;
	            width: 70px;
	            height: 70px;
	        }

        }


        .card-project {
            background: #202940;
        }
        .card-project {
            box-shadow: 0 1px 4px 0 rgb(0 0 0 / 14%);
        }
        .card-project {
            border: 0;
            margin-bottom: 30px;
            margin-top: 40px;
            border-radius: 6px;
            color: #333;
            background: #fff;
            width: 100%;
            box-shadow: 0px 10px 20px #354c57;
        }
        .card-project {
            position: relative;
            display: flex;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-color: #fff;
            background-clip: border-box;
            border: 1px solid #eee;
            border-radius: .25rem;
        }
        .card-project [class*=card-header-]:not(.card-header-icon):not(.card-header-text):not(.card-header-image) {
            border-radius: 3px;
            margin-top: -30px;
            padding: 15px;
        }
        .card-project .card-header {
            background: linear-gradient(
                    60deg
                    ,#16A9BF,#16A9B1);
        }
        .card-project .card-header-primary .card-icon, .card .card-header-primary .card-text, .card-project .card-header-primary:not(.card-header-icon):not(.card-header-text) {
            box-shadow: 0 4px 20px 0 rgb(0 0 0 / 14%), 0 7px 10px -5px rgb(156 39 176 / 40%);
        }
        .card-project [class*=card-header-] {
            margin: 0 15px;
            padding: 0;
            position: relative;
        }
        .card-project .card-header {
            z-index: 2!important;
        }
        .card-project .card-title a {
            color: #fff;
            text-transform: none;
            font-weight: 500;
        }
        .card-project .card-body {
            padding: .9375rem 20px;
            /* position: relative; */
        }
        .card-project .card-body .description, .card-project .card-body .description p {
            font-size: 16px!important;
        }
        .form-group {
        	overflow-x: unset!important;
        }
        .card-project .card-body .tags {
        	width: 100%;
    		display: flex;
        }
        .card-project .card-body .tags span {
        	font-size: 15px;
		    display: block;
		    border: 2px solid #16A9B1;
		    width: fit-content;
		    padding: 2px 10px;
		    border-radius: 15px;
		    margin: 2px 5px;
        }
	.btn-element {
		color: #59abb1;
	    background-color: #354c57;
	    border-color: #59abb1;
	    text-transform: none;
	}
	.btn-element:hover {
		color: #354c57;
	    background-color: #59abb1;
	    border-color: #354c57;
	}

</style>
<?php if($answer){ ?>
<div class="form-group" style="overflow-x: auto;">
		
	<?php 
		
		$paramsData = [ 
			"type" => [
		    	Organization::COLLECTION => "Structure",
		    	Project::COLLECTION 	 => "Action"
		    ],
			"limit" => 0,
			"typeStruct" => [
	            "associe" => "Partenaire",
	            "porteuse" => "Porteuse"
	        ]
	    ];
		
		if( isset($parentForm["params"][$kunik]) ) {
			if( isset($parentForm["params"][$kunik]["limit"]) ) 
				$paramsData["limit"] =  $parentForm["params"][$kunik]["limit"];
		}

		$properties = [
                "name" => [
                    "label" => "Nom de l'organisation",
                    "placeholder" => "Qui...",
                ],
                "type" => [
                    "label" => "Type",
                    "placeholder" => "...type...",
                ]
	        ];

	    $propertiesproject = [
	    	"name" => [
                    "label" => "Nom",
                    "placeholder" => "Nom",
             ],
            "description" => [
                    "label" => "Description",
                    "placeholder" => "Description",
             ],
            "tags" => [
                    "label" => "Tags",
                    "placeholder" => "Tags",
             ],
	    ];

	    if( $mode != "pdf" and $mode != "r"){
	    	$editBtnL = ($canEdit == true
						&& isset($parentForm["params"][$kunik])
						&& ( $paramsData["limit"] == 0 || 
							!isset($answers) || 
							( isset($answers) && $paramsData["limit"] > count($answers) ))) 
				? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default'><i class='fa fa-plus'></i> Ajouter</a>" 
				: "";
			
			$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a> " : "";


		} else {
			$editBtnL = "";
			$editParamsBtn = "";
		}

		$viewtype = "";


		if( isset($parentForm["params"][$kunik]["type"])) {
			if( $parentForm["params"][$kunik]["type"] == Project::COLLECTION ){
				$viewtype = "project";

				$paramsData["limit"] =  1;

				if(	sizeof($answers) > 0 and $mode != "pdf" and $mode != "r"){
					if(isset($answers))
					{
						foreach ($answers as $q => $a) 
						{
							$editBtnL = "<a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-key='".$q."' data-path='".$answerPath.$q."' class='edit".$kunik." btn btn-default pull-right btn-element' style='margin-left : 5px' ><i class='fa fa-plus'></i> Modifier </a> ";
						}
					}
				}

			} else {
				$viewtype = "organisation";
			}
		}
	?>	

	<div>
		<h4 style="color:#16A9B1;padding-top:20px;"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4> 
	</div>

	<div>
		<?php
         echo $info; ?>
	</div>

	<div>
		<?php
			if( !isset($parentForm["params"][$kunik]['type']) && $mode != "pdf" )
					echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> THIS FIELD HAS TO BE CONFIGURED FIRST ".$editParamsBtn."</span>";
		?>
	</div>
<?php //var_dump($answers); ?>


	<!-- <table class="table table-hover  directoryTable" style = "margin-top: 20px" id="<?php echo $kunik?>"> -->
	<!-- <thead>
		<?php 	/*if(isset($answers) && count($answers)>0){ 
					if($viewtype == "organisation"){
		?>
						<tr>
							<th></th>
							<?php 
							
							foreach ($properties as $i => $inp) {
								echo "<th>".$inp["label"]."</th>";
							} 
							if( $mode != "pdf" and $mode != "r"){
							?>
							<th></th>
							<?php 
								}
							?>
						</tr>
		<?php 
					}

				} */
		?>
	</thead> -->
	<!-- <tbody class="directoryLines">	 -->
		
		<?php

		$ct = 0;
		
		if(isset($answers))
		{
			foreach ($answers as $q => $a) 
			{
				//var_dump($a);
				//if( $paramsData["limit"] == 0 || $paramsData["limit"] > $q )
				//{

					if($viewtype == "organisation")	
					{

						//echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
						echo "<div class='profile-page directoryLines'>
								<div id='".$kunik.$q."' class='col-xs-12 col-sm-12 col-md-6 ".$kunik."Line'>
						            <div class='card profile-header'>
						                <div class='body'>
						                    <div class='row'>";

						foreach ($properties as $i => $inp) 
						{
							if( $i == "name" && isset($a["slug"]))
							{

								$el = Slug::getElementBySlug($a["slug"]);
								if (isset($el["el"]["profilImageUrl"]))
								{
	                                 
	    ?>
						        	<!-- <td><img src="<?php //echo Yii::app()->createUrl('/'.$el["el"]["profilImageUrl"]); ?>" class="img-circle" width='60px' height='60px' /></td> -->
						        	 <div class="col-xs-12 col-sm-3 col-md-3 avatar-contain">
			                            <div class="profile-image"> <img src="<?php echo Yii::app()->createUrl('/'.$el["el"]["profilImageUrl"]); ?>" alt=""> </div>
			                        </div>
	    <?php 
	                            } else 
	                            {
	    ?>
	                                <!-- <td><img src="<?php //echo Yii::app()->getModule('co2')->assetsUrl.'/images/thumb/default_organizations.png'; ?>" class="img-circle" width='60px' height='60px' /></td> -->
	                                <div class="col-xs-12 col-sm-3 col-md-3 avatar-contain">
			                            <div class="profile-image"> <img src="<?php echo Yii::app()->getModule('co2')->assetsUrl.'/images/thumb/default_organizations.png'; ?>" alt=""> </div>
			                        </div>
	                                
	    <?php
	                            }
	                            /*<div class='col-xs-12 col-sm-9 col-md-9 about-contain'>
                            		<h4> name</h4>
                            		<span class='job_post'><i class='fa fa-tags'></i> Ui UX Designer</span>
                            		<span class='type'><i class='fa fa-users'></i> Service publique</span>
		                            <div class='padding-top-15'>
		                                <button class="btn btn-primary"><i class="fa fa-pencil"></i> Modifier</button>
		                                <button class="btn btn-danger">Supprimer <i class="fa fa-trash"></i> </button>
		                            </div>
		                        </div>*/
	                            


								/*echo "<td><a href='#page.type.".$el["type"].".id.".$el["id"]."' class='lbh-preview-element' >".$el["el"]["name"]."</a><span style='font-size: 15px;display: block;border: 2px solid #16A9B1;width: fit-content;padding: 2px 10px;border-radius: 15px;'>";*/
								echo "<div class='col-xs-12 col-sm-9 col-md-9 about-contain'>
                            			<h4><a href='#page.type.".$el["type"].".id.".$el["id"]."' class='lbh-preview-element' >".$el["el"]["name"]."</a></h4>
                            			<span class='job_post'><i class='fa fa-tags'></i> ";
											if(isset($el["el"]["typeStruct"])){
												echo $paramsData["typeStruct"][$el["el"]["typeStruct"]];
											} else if(isset($a["typeStruct"])){
												echo $paramsData["typeStruct"][$a["typeStruct"]];
											}
								  echo "</span>";	

								//echo "</span></td>";
								echo "<span class='type'><i class='fa fa-users'></i> ".Yii::t("common", $el["el"]["type"])."</span></div>";
							} else if($i!="type"){
								if(isset($a[$i]))
									echo "<div class='col-xs-12 type-elt text-center'><span class='type'>".$a[$i]."</span></div>";
							}
							
						}
						?>
						
						
						<?php  if( $mode != "pdf" and $mode != "r"){?>
						<div class="padding-top-15 col-xs-12 text-center btn-contain">
							<?php 
							if ($viewtype != "project") {
							 	echo $this->renderPartial( "survey.views.tpls.forms.cplx.editUnsetLineBtn" , [
												"canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
												"id" => $answer["_id"],
												"collection" => Form::ANSWER_COLLECTION,
												"q" => $q,
												"path" => $answerPath.$q,
												"kunik"=>$kunik,
												"label" => true ]
												);
							}
							?>
						</div>
						<?php 
							}
							$ct++;
							//echo "</tr>";
							echo "</div></div></div></div></div>";


					} else {

						echo "<div class='col-xs-12'>
    								<div class='card-project'>";

						foreach ($propertiesproject as $i => $inp) 
						{
                            if (!empty($a["id"])){
                                $el = Element::getElementById($a["id"],Project::COLLECTION,null,["name","description","tags"] );

                                if( $i == "name" ) {


                                    /*echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";

                                        echo "<th>".$inp["label"]."</th>";

                                        echo "<td style='text-align : center'><a href='#page.type.".$a["type"].".id.".$a["id"]."' class='lbh-preview-element' >".$el["name"]."</a></td>";

                                    echo "</tr>";*/

                                    echo "<div id='".$kunik.$q."' class='card-header card-header-primary ".$kunik."Line'>
							            <h4 class='card-title'><a href='#page.type.".@$a["type"].".id.".@$a["id"]."' class='lbh-preview-element' >".@$el["name"]."</a></h4>
							            <p class='card-category'></p>
							        </div>";


                                }

                                elseif ( $i == "description" ) {

                                    if(isset($el["description"])){
                                        /*echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
                                            echo "<th>".$inp["label"]."</th>";
                                            echo "<td style='text-align: center' id='markdownDesc'>".$el["description"]."</td>";
                                        echo "</tr>";*/
                                        echo "<div id='".$kunik.$q."' class='card-body ".$kunik."Line'>";
                                        echo "<div id='markdownDesc' class='description markdown'>".$el["description"]."</div>";
                                    }
                                }
                                elseif ( $i == "tags" ) {
                                    if(isset($el["tags"])){

                                        /*echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
                                            echo "<th>".$inp["label"]."</th>";
                                            echo "<td style='text-align: center;     justify-content: center; display: flex;'>";
                                            foreach ($el["tags"] as $key => $value) {
                                                echo "<span style='font-size: 15px;display: block;    border: 2px solid #16A9B1; width: fit-content;padding: 2px 10px;border-radius: 15px;margin: 2px 5px;'><i class='fa fa-tags' style='padding: 2px 5px;'></i>".$value."</span>";
                                            }
                                            echo "</td>";
                                        echo "</tr>";*/

                                        echo "<hr style='border-top: 2px solid #354C57;'>
									 <div id='".$kunik.$q."' class='tags ".$kunik."Line'>";
                                        foreach ($el["tags"] as $key => $value) {
                                            echo "<span><i class='fa fa-tags' style='padding: 2px 5px;'></i>".$value."</span>";
                                        }
                                        echo "</div>";

                                    }
                                }
                            }
						}

						echo "</div></div></div>";

							$ct++;

					}
					
				//}
			}
		}
		 ?>
		
</div>
<?php  if( $mode != "pdf" and $mode != "r"){?>

<script type="text/javascript">
var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
var <?php echo $kunik ?>DataAnswer = <?php echo json_encode( (isset($answer)) ? $answer : null ); ?>;

<?php $indicateursPrincipaux = Ctenat::getIndicator( null, null, ["name","unity1"], null,true); ?>
if(typeof costum.lists == "undefined")
	costum.lists = {};
costum.lists.indicateursPrincipaux = <?php echo json_encode($indicateursPrincipaux); ?>;

sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

	var max<?php echo $kunik ?> = 0;
	var nextValuekey<?php echo $kunik ?> = 0;

	if(notNull(<?php echo $kunik ?>Data)){
		$.each(<?php echo $kunik ?>Data, function(ind, val){
			if(parseInt(ind) > max<?php echo $kunik ?>){
				max<?php echo $kunik ?> = parseInt(ind);
			}
		});

		nextValuekey<?php echo $kunik ?> = max<?php echo $kunik ?> + 1;

	}



var costumInputs = {};

costumInputs.elementparents = {
    "beforeBuild" : {
        "properties" : {
            "name" : {
                "label" : "Nom de l'organisation",
                "subLabel" : "si l’action a plusieurs porteurs, indiquez ici un premier porteur, vous pourrez ajouter les autres une fois votre action créée"
            },
            "type" : {
                "label" : "Type d'organisation",
                "inputType" : "select",
                "placeholder" : "---- Type d'organisation ----",
                // "options" : {
                //     "NGO" : "Association",
                //     "LocalBusiness" : "Entreprise",
                //     "GovernmentOrganization" : "Service Public"
                // }
            },
            "shortDescription" : {
                "inputType" : "textarea",
                "label" : "Description courte de l’orga",
                "order" : 5
            },
            "links[answers]" : {
                "inputType" : "hidden",
                value : answerObj._id.$id
            },
            "links[projects]" : {
                "inputType" : "hidden",
                value : contextData.id
            },
            "typeStruct" : {
                "inputType" : "select",
                "label" : "Type de structure",
                "options" : {
                	"associe" : "Partenaire",
                	"porteuse" : "Porteuse"
                },
                "order" : 0
            }
        }
    },
    "onload" : {
        "actions" : {
            "html" : {
                "modal-title" : "Ajouter un porteur associé",
                "nametext > label" : "Nom de l’organisation"
            },
            "addClass" : {
                "modal-header" : "bg-dark"
            },
            "removeClass" : {
                "modal-header" : "bg-green"
            },
            "presetValue" : {
                "role" : "admin",
                "public" : false,
                "typeStruct" : "porteuse"
            },
            "hide" : {
                "urltext" : 1,
                "tagstags" : 1,
                "parentfinder" : 1,
                "publiccheckboxSimple" : 1,
                "roleselect" : 1,
                "infocustom" : 1
            }
        }
    },
    afterSave : function(data, searchExists) {
    	// alert( "afterSave"+data.map.slug+" , "+ data.typeStruct );
    	// if(searchExists){
			var ex = false;
			//create link only if doesn't allready exist
			if( jsonHelper.notNull("answerObj.answers.action.parents") ){
				$.each( answerObj.answers.action.parents, function( i , o ){
					//alert(data.id+" | "+o.id)
					if( o != null && data.id == o.id )
						ex = true;
				})
			}
			//alert( "afterSave"+ex );
	    	if(!ex) {
	    		
	    		//alert( "save link"+data.map.slug+" , "+ data.typeStruct );
	    		tplCtx = {
					collection : "answers",
					id : answerId,
					arrayForm : true,
					path : "answers.action.parents",
					value : {
						type : data.type ,
	                    id   : data.id ,
	                    name : data.name ,
	                    slug : data.map.slug ,
	                    typeStruct : data.typeStruct
					}
				}

				mylog.log('afterSave structure porteuse',tplCtx);

				dataHelper.path2Value( tplCtx, function(params) {
					$("#ajax-modal").modal('hide');
					//coInterface.showLoader("#central-container")
					urlCtrl.loadByHash(location.hash);
				} );
	    	} 
	    	else 
	    	{
	    		$("#ajax-modal").modal('hide');
	    		urlCtrl.loadByHash(location.hash);
	    	}
	    // } else {
	    // 	$("#ajax-modal").modal('hide');
	    // 	urlCtrl.loadByHash(location.hash);
	    // }
    	
		
		
		
		
    }
};

costumInputs.elementproject = {
    "beforeBuild" : {
        "properties" : {
            "shortDescription" : {
                "inputType" : "textarea",
                "label" : "Description courte de l’action",
                "subLabel" : "N’hésitez pas à être précis, Exemple : Installation de 400 m² de panneaux photovoltaïques sur le toit de l’école communale … le projet consiste à accompagner la transition vers le bio de 10 exploitations agricoles… etc.",
                "order" : 4
            },
            "description" : {
                "inputType" : "textarea",
                "label" : "Description détaillée",
                "subLabel" : "Expliquez l’intérêt du projet. Précisez l’historique et les caractéristiques principales du projet : d’où vient-il ? où en est-il ? N’hésitez pas à indiquer les chiffres (ou les fourchettes) qui permettront de comprendre sa dimension : quel coût, quelle superficie, combien de personnes bénéficiaires, etc.",
                "markdown" : true,
                "order" : 5
            },
            "expected" : {
                "inputType" : "textarea",
                "label" : "Vos attentes vis-à-vis du dispositif",
                "markdown" : true,
                "order" : 12
            },
            "category" : {
                "inputType" : "hidden",
                "value" : "ficheAction"
            },
            "dispositif" : {
                "inputType" : "hidden",
                "value" : contextData.dispositif
            },
            "links[answers]" : {
                "inputType" : "hidden",
                value : answerObj._id.$id
            },
            "links[projects]" : {
                "inputType" : "hidden",
                value : contextData.id
            },
            "actionPrincipal" : {
                "inputType" : "select",
                "label" : "Domaine d'action principal",
                "placeholder" : "Choisir un domaine d'action principal",
                "groupOptions" : true,
                "groupSelected" : false,
                "list" : "domainAction",
                "select2" : true,
                "rules" : {"required" : "true"},
                "value" : "<?php if(isset($answer["answers"]["caracter"]["actionPrincipal"])) echo $answer["answers"]["caracter"]["actionPrincipal"]; ?>"
            },
            "cibleDDPrincipal" : {
                "inputType" : "select",
                "label" : "Objectif principal",
                "placeholder" : "Choisir une cible de développement durable",
                "groupOptions" : true,
                "groupSelected" : false,
                "list" : "cibleDD",
                "select2" : true,
                "rules" : {"required" : "true"},
                "value" : "<?php if(isset($answer["answers"]["caracter"]["cibleDDPrincipal"])) echo $answer["answers"]["caracter"]["cibleDDPrincipal"]; ?>"
            },
            "indicateur" : {
		        "inputType" : "select",
		        "label" : "Indicateur",
		        "placeholder" : "Indicateur",
		        "list" : "indicateursPrincipaux",
		        "select2" : {
		            "multiple" : false
		        },
		        "rules" : {"required" : "true"},
		        "value" : "<?php if(isset($answer["answers"]["murir"]["results"]["0"]["indicateur"])) echo $answer["answers"]["murir"]["results"]["0"]["indicateur"]; ?>"
		    }
        }
    },
    "onload" : {
        "actions" : {
            "html" : {
                "i:section2 c:nametext > label" : "Titre de votre Action",
                "i:ajax-modal-modal-title" : "Editer la description de l’action"
            },
            "presetValue" : {
                "public" : false
            },
            "hide" : {
                "urltext" : 1,
                "parentfinder" : 1,
                "publiccheckboxSimple" : 1
            }
        }
    },
    afterSave : function(data, searchExists) {
    	tplCtx.afterdataind = {
				collection : "answers",
				id : answerId,
				path : "answers.murir.results",
				value : {
					//"0" : { "indicateur" : "<?php echo Ctenat::INDICATOR_EMPLOI_ID ?>" }
				}
		}
		tplCtx.afterdataaction = {
				collection : "answers",
				id : answerId,
				path : "answers.caracter.actionPrincipal",
				value : {}
		}
		tplCtx.afterdatacibledd = {
				collection : "answers",
				id : answerId,
				path : "answers.caracter.cibleDDPrincipal",
				value : {}
		}
		if(typeof <?php echo $kunik ?>DataAnswer.answers != "undefined" 
		&& typeof <?php echo $kunik ?>DataAnswer.answers.murir != "undefined" 
		&& typeof <?php echo $kunik ?>DataAnswer.answers.murir.results != "undefined" 
		&& typeof <?php echo $kunik ?>DataAnswer.answers.murir.results["0"] != "undefined" && $("#indicateur").val()){
			tplCtx.afterdataind.path = "answers.murir.results.0" ;
			tplCtx.afterdataind.value = { "indicateur" : $("#indicateur").val() } ;
		}else if($("#indicateur").val())
			tplCtx.afterdataind.value["0"] = { "indicateur" : $("#indicateur").val() } ;
    	if($("#actionPrincipal").val())
			tplCtx.afterdataaction.value = $("#actionPrincipal").val();   	
		if($("#cibleDDPrincipal").val())
			tplCtx.afterdatacibledd.value = $("#cibleDDPrincipal").val();

		if($("#cibleDDPrincipal").val())
			dataHelper.path2Value( tplCtx.afterdatacibledd, function(params) {} );
		if($("#indicateur").val())
			dataHelper.path2Value( tplCtx.afterdataind, function(params) {} );
		if($("#actionPrincipal").val())
			dataHelper.path2Value( tplCtx.afterdataaction, function(params) {} );


		
		var exp = false;
			//create link only if doesn't allready exist
			if( jsonHelper.notNull("answerObj.answers.action.parents") ){
				$.each( answerObj.answers.action.parents, function( i , o ){
					//alert(data.id+" | "+o.id)
					if( o != null && data.id == o.id )
						exp = true;
				})
			}
			//alert( "afterSave"+ex );
	    	if(!exp) {
	    		
	    		//alert( "save link"+data.map.slug+" , "+ data.typeStruct );
	    		tplCtx = {};
	    		tplCtx = {
					collection : "answers",
					id : answerId,
					path : "answers.action.project",
					value : [{
						type : data.map.collection ,
	                    id   : data.id ,
	                    name : data.map.name ,
	                    slug : data.map.slug 
					}]
				}

				mylog.log("dfdf",data);

				dataHelper.path2Value( tplCtx, function(params) {
					$("#ajax-modal").modal('hide');
					//coInterface.showLoader("#central-container")
				} );

				tplCtx = {
					collection : "answers",
					id : answerId,
					path : "answers.project",
					value : [{
						type : data.map.collection ,
	                    id   : data.id ,
	                    name : data.map.name ,
	                    slug : data.map.slug 
					}]
				}

				dataHelper.path2Value( tplCtx, function(params) {
					$("#ajax-modal").modal('hide');
					//coInterface.showLoader("#central-container")
					urlCtrl.loadByHash(location.hash);
				} );
	    	} 
	    	else 
	    	{
	    		$("#ajax-modal").modal('hide');
	    		urlCtrl.loadByHash(location.hash);
	    	}
    }
};

$(document).ready(function() { 
	mylog.log("render","/modules/costum/views/tpls/forms/element.php");

	if(!jsonHelper.notNull("answerObj.answers.action.parents"))
		$("#questionproject").hide();

	if(jsonHelper.notNull("answerObj.answers.action.project") && costumInputs.elementproject)
	{
		//remove links answers and projects
		delete costumInputs.elementproject.beforeBuild.properties["links[answers]"];
        delete costumInputs.elementproject.beforeBuild.properties["links[projects]"];

        // if( $("#markdownDesc").length )
		// 	$("#markdownDesc").html(dataHelper.markdownToHtml($("#markdownDesc").html()) );
	} 
	
	//can be hacked to apply further costumization
	//is used like a dynFormCostumIn in openForm
	costum.<?php echo $kunik ?> = {
		onload : {"actions" : { "setTitle" : "<?php echo $input["label"] ?>"}},
		afterSave : function(data) { 
			mylog.log("element afterSave",data);
			//costum.<?php echo $kunik ?>.connectToAnswer(data);
			
			if(typeof costumInputs.<?php echo $kunik ?>.afterSave != 'undefined')
				costumInputs.<?php echo $kunik ?>.afterSave(data);
			else {
				$("#ajax-modal").modal('hide');
				//coInterface.showLoader("#central-container")
				//reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
				urlCtrl.loadByHash(location.hash);
			}
		},
		// connectToAnswer : function (data) { 
		// 	mylog.log("costum.<?php echo $kunik ?>.connectToAnswer",data)
		// 	tplCtx.value = {
		// 		type : (typeof data != "undefined" && typeof data.type != "undefined") ? data.type : "<?php echo (isset($parentForm["params"][$kunik]['type'])) ? $parentForm["params"][$kunik]['type'] : ''; ?>",
		// 		id : data.id,
		// 		name : data.name,
		// 		slug : data.map.slug
		// 	};

		//     mylog.log("save tplCtx",tplCtx);
		    
		//     if(typeof tplCtx.value == "undefined")
		//     	toastr.error('value cannot be empty!');
		//     else {
		//         dataHelper.path2Value ( tplCtx, function(params) {} );
		//     }
	 //    }
	  // onload : {
	  // 	"actions" : {
	  //    	"hide": {
	  //           		"parentfinder" : 1
	  //           	}
	  //           }
	  //       }
	};
	//on rempli costum pour avoir accés au variable globalement 
	if(costumInputs.<?php echo $kunik ?>){
		$.each( Object.keys( costumInputs.<?php echo $kunik ?>) ,function(i,k){
			costum.<?php echo $kunik ?>[k] = costumInputs.<?php echo $kunik ?>[k]; 
		})
		
	}
	costum.searchExist = function (type,id,name,slug,email) { 
		//alert("searchExist")
		mylog.log("costum searchExist : "+type+", "+id+", "+name+", "+slug+", "+email); 
		var data = {
			type : type,
			id : id,
			name : name,
			map : { slug : slug }
		}

		if(type == "projects")
			costumInputs.elementproject.afterSave(data);
		else{
			data.typeStruct = $("#ajaxFormModal #typeStruct").val(); 
			costumInputs.elementparents.afterSave(data,true);
		}
			
	};


	sectionDyf.<?php echo $kunik ?>Params = {
		jsonSchema : {	
	        title : "Element config",
	        icon : "fa-cog",
	        properties : {
	            type : {
	                inputType : "select",
	                label : "Définir un type d'élément",
	                options :  sectionDyf.<?php echo $kunik ?>ParamsData.type,
	                value : "<?php echo (isset($parentForm["params"][$kunik]['type'])) ? $parentForm["params"][$kunik]['type'] : ''; ?>"
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);

	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    $("#ajax-modal").modal('hide');
	                    //urlCtrl.loadByHash(location.hash);
	                    //coInterface.showLoader("#central-container")
	                    reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
	                } );
	            }

	    	}
	    }
	};


    //adds a line into answer

    <?php if( isset($parentForm["params"][$kunik]['type']) ) { ?>
    $(".add<?php echo $kunik ?>").off().on("click",function() {  
        // tplCtx.id = $(this).data("id");
        // tplCtx.collection = $(this).data("collection");            
        // tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
        dyFObj.openForm( "<?php echo Element::getControlerByCollection($parentForm["params"][$kunik]['type']); ?>",null,null,null,costum.<?php echo $kunik ?> );
    });

    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
      dyFObj.editElement( <?php echo $kunik ?>Data[$(this).data("key")].type,<?php echo $kunik ?>Data[$(this).data("key")].id,null,costum.<?php echo $kunik ?>);
    });
    <?php } ?>

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData); 
    });

    $(".unsetLine").off().on("click",function (){
        //alert(".unsetLine")
        $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
        var linePath = $(this).data("path");
        var id = $(this).data("id");
        var key = $(this).data("key");
        var type = $(this).data("collection");

        bootbox.confirm(trad.areyousuretodelete,
            function(result){
                if (!result) {
                  btnClick.empty().html('<i class="fa fa-trash"></i>');
                  return;
                } 
                else {
                    //alert("unsetLine "+linePath);
                    tplCtx = {
                        collection : "answers" ,
                        id : answerId ,
                        path : linePath ,
                        value : null,
                        pull : "answers.action.parents"
                    }
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#"+key).remove();
                        toastr.success('Ligne effacé! ');
                    } );
                }
            }
        );
    });

    
});
</script>
<?php
}
 } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
}?>
