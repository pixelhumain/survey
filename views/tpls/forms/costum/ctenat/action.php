<?php if($answer){
    ?>
    <div class="form-group">


            <?php
            $editBtnL = ($canEdit and $mode != "r" and $mode != "pdf" and count($answers) == 0 ) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default'><i class='fa fa-plus'></i> Ajouter</a>" : "";
        $editParamsBtn = ($canEditForm and $mode != "r" || $mode != "pdf") ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

            $paramsData = [];
            
            $properties = [
                "nom" => [
                    "label" => "Nom",
                    "placeholder" => "Nom",
                    "inputType" => "text",
                    "rules" => [ "required" => true ]
                ],
                "description" => [
                    "label" => "Description",
                    "placeholder" => "Description",
                    "inputType" => "wysiwyg",
                    "rules" => [ "required" => true ]
                ],
                "tags" => [
                    "label" => "tags",
                    "placeholder" => "tags",
                    "inputType" => "tags",
                    "rules" => [ "required" => true ]
                ],


            ];

            $propertiesParams = [
                "labels"=>[],
                "placeholders"=>[],
            ];

            foreach ($properties as $k => $v) {
                if( isset($v["label"])){
                    if( isset($parentForm["params"][$kunik]["labels"][$k]) )
                        $properties[$k]["label"] = $parentForm["params"][$kunik]["labels"][$k];
                    $propertiesParams["labels"][$k] = $properties[$k]["label"];
                }

                if( isset($v["placeholder"])){
                    if(isset($parentForm["params"][$kunik]["placeholders"][$k]) )
                       $properties[$k]["placeholder"] = $parentForm["params"][$kunik]["placeholders"][$k];
                    $propertiesParams["placeholders"][$k] = $properties[$k]["placeholder"];
                }
            }

            ?>

            <?php if(  $mode != "pdf" ){ ?>
            </div>


            <div>
                <div>
                   <h4> <?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
                </div>
                <div>
                    <?php echo $info ?>
               
                

                <?php  
                if( isset($answers)) { 
                foreach ($answers as $q => $a) {
                if( $mode != "r"){?>
                    
                                <?php
                                echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
                                    "canEdit"=>($canEdit),
                                    "id" => $answer["_id"],
                                    "collection" => Form::ANSWER_COLLECTION,
                                    "q" => $q,
                                    "path" => $answerPath.$q,
                                    "keyTpl"=>$kunik
                                ] ); ?>

                                
                           
                <?php 
                }
                }
                }?>
                </div>
            </div>
            <table class="table table-hover  directoryTable" id="<?php echo $kunik?>">
            <tbody class="directoryLines">
            <?php
            $ct = 0;

            if(isset($answers)){
                foreach ($answers as $q => $a) {

                    foreach ($properties as $i => $inp) {

                        echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";

                            echo "<td style='width : 20px'>".$inp["placeholder"]."</td>";

                            echo "<td>";
                            if(isset($a[$i])) {
                                if(is_array($a[$i])){
                                    echo implode(",", $a[$i]);
                                }
                                else{
                                    echo $a[$i];
                                }
                            }
                            echo "</td>";
                        echo "</tr>";
                }
                }
            }

            ?>
            </tbody>
        </table>
        <?php
            } else {
            ?>
            <div>
                <label>
                    <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
                                <?php echo $info ?>
                </label>
            </div>

            <table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>">
            
            <thead>
                <tr>
                    <?php
                        if( count($answers)>0 ){ 
                            foreach ($properties as $i => $inp) {
                                if (isset($inp["placeholder"])){
                                    echo "<th>".$inp["placeholder"]."</th>";
                                }
                            } ?>
                    <?php } ?>
                </tr>
            </thead>

            <tbody class="directoryLines">
            
            <?php
            if(isset($answers)){
                foreach ($answers as $q => $a) {

                    echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
                    foreach ($properties as $i => $inp) {
                         if($i == "location"){
                        } else {
                        echo "<td>";
                        if(isset($a[$i])) {
                            if(is_array($a[$i])){
                                echo implode(",", $a["port"]);
                            }
                            else{
                                echo $a[$i];
                            }
                        }
                        echo "</td>";
                    }
                }

                echo "</tr>";
                }
            }

            ?>
            </tbody>
        </table>

    <?php } ?>




    </div>
    <?php  if( $mode != "pdf"){?>
    <script type="text/javascript">

        var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsProperty = {
            labels : <?php echo json_encode( $propertiesParams["labels"] ); ?>,
            placeholders : <?php echo json_encode( $propertiesParams["placeholders"] ); ?>
        };

        $(document).ready(function() {

            sectionDyf.<?php echo $kunik ?> = {
                "jsonSchema" : {
                    "title" : "Les acteurs de l’encadrement de l’activité",
                    "icon" : "fa-globe",
                    "text" : "Décrire ici les principaux postes de dépenses : à quoi correspondent les coûts ? <br/>Préciser ce qui relève des dépenses de fonctionnement et des dépenses d’investissement.<br/>Les coûts doivent être en <b>hors taxe</b>.",
                    "properties" : <?php echo json_encode( $properties ); ?>,
                    save : function () {
                        var today = new Date();
                        tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
                        $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) {
                            if(val.inputType == "properties")
                                tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                            else if(val.inputType == "array")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else if(val.inputType == "formLocality")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else
                                tplCtx.value[k] = $("#"+k).val();
                        });



                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");//$("#ajax-modal").modal('hide');
                                location.reload();
                            } );
                        }

                    }
                }
            };

            sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema" : {
                    "title" : "<?php echo $kunik ?> config",
                    "description" : "Liste de question possible",
                    "icon" : "fa-cog",
                    "properties" : {
                        labels : {
                            inputType : "properties",
                            labelKey : "Clef",
                            labelValue : "Libellé du champs",
                            label : "Modifier les libélés des Questions",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsProperty.labels
                        },
                        placeholders : {
                            inputType : "properties",
                            labelKey : "Clef",
                            labelValue : "Texte dans le champs",
                            label : "Modifier les textes à l'interieur du champs de saisie",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsProperty.placeholders
                        },
                        statut : {
                            inputType : "array",
                            label : "Liste des statuts",
                            value :  sectionDyf.<?php echo $kunik ?>ParamsData.statut
                        },
                    },
                    save : function (formData) {
                        mylog.log("save tplCtx formData",formData);
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                            if(val.inputType == "properties")
                                tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                            else if(val.inputType == "array")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else
                                tplCtx.value[k] = $("#"+k).val();
                            mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                        });

                        if(typeof formData != "undefined" && typeof formData.geo != "undefined"){
                            tplCtx.value["pointGPS"] = formData.geo;
                            tplCtx.value["address"] = formData.address;
                            tplCtx.value["geo"] = formData.geo;
                            tplCtx.value["geoPosition"] = formData.geoPosition;

                            if(typeof formData.addresses != "undefined")
                                tplCtx.value["addresses"] = formData.addresses;
                        }
                        
                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                                location.reload();
                            } );
                        }

                    }
                }
            };


            mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

            //adds a line into answer
            $(".add<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
            });

            $(".edit<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
            });

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                //if no params config on the element.costum.form.params.<?php echo $kunik ?>
                //then we load default values available in forms.inputs.<?php echo $kunik ?>xxx.params
                //mylog.log(".editParams",sectionDyf.<?php echo $kunik ?>Params,calData);
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            });


        });
    </script>
<?php } } ?>