<style type="text/css">
    .descSec<?= $key ?> {
        display: none;
    }
</style>

<?php 
$value = (!empty($answers)) ? ' value="'.$answers.'" ' : '';

$inpClass = "form-control";

if($type == "tags") 
    $inpClass = "select2TagsInput";

if($saveOneByOne)
    $inpClass .= " saveOneByOne";

if($mode == "r" || $mode == "pdf"){ ?>
    <div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
        <label for="<?php echo $kunik ?>">
            <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
                <?php echo $label ?>
            </h4>
        </label><br/>
        <p class="ans_val_by_i"><?php echo $answers; ?></p>
    </div>
    <?php 
}else{
    ?>
    <div class="col-md-6 no-padding" id='validate<?= $key ?>'>
        <div class="form-group">
            <label for="<?php echo $key ?>">
                <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
                    <?php echo $label.$editQuestionBtn ?>
                </h4>
            </label>
            <br/>

            <?php 

            if ($value == "" && $answer["user"]==$_SESSION["userId"]) {
                $value = "value='".$_SESSION["userEmail"]."'";
            }

            ?>
            <input type="<?php echo (!empty($type)) ? $type : 'text' ?>" 
            class="<?php echo $inpClass ?>" 
            id="<?php echo $key ?>" 
            aria-describedby="<?php echo $key ?>Help" 
            placeholder="<?php echo (isset($placeholder)) ? $placeholder : '' ?>" 
            data-form='<?php echo $form["id"] ?>' <?php echo $value ?> 
            />
            <?php if(!empty($info)){ ?>
                <small id="<?php echo $key ?>Help" class="form-text text-muted">
                    <?php echo $info ?>
                </small>
            <?php } ?>
        </div>
        <p id='result<?= $key ?>'></p>

    </div>

    <script type="text/javascript">
        function validateEmail(email) {
          const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return re.test(email);
      }

      function validate() {
          const $result = $("#result<?= $key ?>");
          const email = $("#<?= $key ?>").val();
          $result.text("");
          if (email != "") {
             if (!validateEmail(email)) {
                $result.text("Adresse email invalide :(");
                $result.css("color", "red");
                $(".finishInput").prop( "disabled", true );

            }
            return false;
        }        
    }
    $("#validate<?= $key ?>").hover(function(){
      validate()  
    });
    <?php if(!Authorisation::isInterfaceAdmin()){ ?>
        $(".menu-xs-cplx").remove();

        jQuery(document).ready(function() {
         mylog.log("render form input","/modules/costum/views/tpls/forms/text.php");
         if($("#<?php echo $key ?>").val() != "" ){
            answer = new Object;
            answer.path = "answers."+$("#<?php echo $key ?>").data("form")+"."+"<?php echo $key ?>";
            answer.collection = "answers" ;
            answer.id = "<?php echo $answer["_id"]; ?>";
            answer.value = $("#<?php echo $key ?>").val();
            mylog.log("Alefa",answer);
            dataHelper.path2Value(answer , function(params) { 
                    //toastr.success('saved');
                });
        }
    });
    <?php } ?>
</script>
<?php } ?>

