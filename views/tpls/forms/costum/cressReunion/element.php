<?php if($answer){ ?>
<div class="form-group">
	<table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>">
		
	<?php 
		$paramsData = [ "type" => [
					    	Organization::COLLECTION => "Organization",
					    	Person::COLLECTION 		 => "Person",
					    	Event::COLLECTION 		 => "Event",
					    	Project::COLLECTION 	 => "Project",
							News::COLLECTION 		 => "News",
					    	//Need::COLLECTION 		 => "Need",
					    	City::COLLECTION 		 => "City",
					    	Thing::COLLECTION 		 => "Thing",
					    	Poi::COLLECTION 		 => "Poi",
					    	Classified::COLLECTION   => "Classified",
					    	Product::COLLECTION 	 => "Product",
					    	Service::COLLECTION   	 => "Service",
					    	Survey::COLLECTION   	 => "Survey",
					    	Bookmark::COLLECTION   	 => "Bookmark",
					    	Proposal::COLLECTION   	 => "Proposal",
					    	Room::COLLECTION   	 	 => "Room",
					    	Action::COLLECTION   	 => "Action",
					    	Network::COLLECTION   	 => "Network",
					    	Url::COLLECTION   	 	 => "Url",
					    	Risk::COLLECTION   => "Risk",
					    	Badge::COLLECTION   => "Badge",
					    ],
					    "limit" => 0 ];
		
		if( isset($parentForm["params"][$kunik]) ) {
			if( isset($parentForm["params"][$kunik]["limit"]) ) 
				$paramsData["limit"] =  $parentForm["params"][$kunik]["limit"];
		}

		$properties = [
                "qui" => [
                    "label" => "Nom de votre structure",
                    "placeholder" => "Qui...",
                ],
                "type" => [
                    "label" => "Type",
                    "placeholder" => "...type...",
                ]
	        ];

		$editBtnL = ($canEdit === true
					&& isset($parentForm["params"][$kunik])
					&& ( $paramsData["limit"] == 0 || 
						!isset($answers) || 
						( isset($answers) && $paramsData["limit"] > count($answers) ))) 
			? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default '><i class='fa fa-plus'></i> Ajouter un élément </a>" 
			: "";
		
		$editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";
	?>	
	<thead>
		<tr>
			<td colspan='<?php echo count( $properties)+2?>' ><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
				<?php echo $info;
				if( !isset($parentForm["params"][$kunik]['type']) ) 
					echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> THIS FIELD HAS TO BE CONFIGURED FIRST ".$editParamsBtn."</span>";
				 ?>

			</td>
		</tr>	
		<?php if(isset($answers) && count($answers)>0){ ?>
		<tr>
			</th>
			<?php 
			
			foreach ($properties as $i => $inp) {
				echo "<th>".$inp["placeholder"]."</th>";
			} ?>
			<th></th>
		</tr>
		<?php } ?>
	</thead>
	<tbody class="directoryLines">	
		<?php 
		$ct = 0;
		
		if(isset($answers))
		{
			foreach ($answers as $q => $a) 
			{
				if( $paramsData["limit"] == 0 || $paramsData["limit"] > $q )
				{
					if(isset($a["slug"]))
						$el = Slug::getElementBySlug($a["slug"],["name","typeEtablissement"]);
					echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
					foreach ($properties as $i => $inp) 
					{
						if( $i == "qui" && isset($a["slug"])) {
							echo "<td><a href='#page.type.".$el["type"].".id.".$el["id"]."' class='lbh-preview-element' >".$el["el"]["name"]."</a></td>";
						} 
						else if($i == "type" && isset($a["slug"])){
							echo "<td>".@$el["el"]["typeEtablissement"]."</a></td>";
						}
						// else 
						// 	echo "<td>".$a[$i]."</td>";
					}
				?>
				<td>
					<?php 
					echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
										"canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
										"id" => $answer["_id"],
										"collection" => Form::ANSWER_COLLECTION,
										"q" => $q,
										"path" => $answerPath.$q,
										"kunik"=>$kunik ] );
					?>
					<!-- <a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')">
								<?php 
									echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q)); ?> 
									<i class='fa fa-commenting'></i></a> -->
				</td>
				<?php 
					$ct++;
					echo "</tr>";
				}
			}
		}
		 ?>
		</tbody>
	</table>
</div>
<script type="text/javascript">

var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

$(document).ready(function() { 
	mylog.log("render","/modules/costum/views/tpls/forms/element.php");
	if(<?php echo $kunik ?>Data!=null && <?php echo $kunik ?>Data.length < 0){
		$("#question<?php echo $key ?>").removeClass("hide");
	}
	//can be hacked to apply further costumization
	//is used like a dynFormCostumIn in openForm
	costum.<?php echo $kunik ?> = {
		onload : {"actions" : { "setTitle" : "<?php echo $input["label"] ?>"}},
		afterSave : function(data) { 
			mylog.log("element afterSave",data)
			costum.<?php echo $kunik ?>.connectToAnswer(data);
		},
		connectToAnswer : function ( data ) { 
			mylog.log("costum.<?php echo $kunik ?>.connectToAnswer",data)
			tplCtx.value = {
				type : (data.type) ? data.type : "<?php echo (isset($parentForm["params"][$kunik]['type'])) ? $parentForm["params"][$kunik]['type'] : ''; ?>",
				id : data.id,
				slug : data.map.slug
			};

		    mylog.log("save tplCtx",tplCtx);
		    ;
		    if(typeof tplCtx.value == "undefined")
		    	toastr.error('value cannot be empty!');
		    else {
		        dataHelper.path2Value ( tplCtx, function(params) { 
		            $("#ajax-modal").modal('hide');
		            
		            reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
                                      });
		        	var form=$('#question<?php echo $key ?>').data("form");
                                      answer.path = "answers."+form+".multitextvalidationcressReunion1622021_1332_01";
                                      answer.collection = "answers" ;
                                      answer.id = "<?php echo $answer["_id"]; ?>";
                                      answer.value = data.map.siret;
                                      dataHelper.path2Value(answer , function(params) { 
                                          toastr.success('saved');
                                          reloadInput("cressReunion1622021_1332_01", "<?php echo (string)$form["_id"] ?>");
                                      });
                                      
                                      answer.path = "answers."+form+".cressReunion1622021_1332_02";
                                      answer.value = data.map.name;

                                      dataHelper.path2Value(answer , function(params) { 
                                          toastr.success('saved');
                                          reloadInput("cressReunion1622021_1332_02", "<?php echo (string)$form["_id"] ?>");
                                      });
                                      $("#questioncressReunion1622021_1332_02").removeClass("hide");
                                      
                                      answer.path = "answers."+form+".cressReunion1622021_1332_04";
                                      answer.value = data.map.typeEtablissement;

                                      dataHelper.path2Value(answer , function(params) { 
                                          toastr.success('saved');
                                          reloadInput("cressReunion1622021_1332_04", "<?php echo (string)$form["_id"] ?>");
                                      });
                                      $("#questioncressReunion1622021_1332_04").removeClass("hide");
                                      
                                      if (typeof data.map.url!="undefined" && data.map.url!=""){
                                        answer.path = "answers."+form+".cressReunion1622021_1332_07";
                                        answer.value = data.map.url;
                                        dataHelper.path2Value(answer , function(params) { 
                                          toastr.success('saved');
                                          reloadInput("cressReunion1622021_1332_07", "<?php echo (string)$form["_id"] ?>");
                                        });
                                      }		        

		    }
	    }
	  // onload : {
	  // 	"actions" : {
	  //    	"hide": {
	  //           		"parentfinder" : 1
	  //           	}
	  //           }
	  //       }
	};
	costum.searchExist = function (type,id,name,slug,email) { 
		mylog.log("costum searchExist : "+type+", "+id+", "+name+", "+slug+", "+email); 
		var data = {
			type : type,
			id : id,
			map : { slug : slug }
		}
		costum.<?php echo $kunik ?>.connectToAnswer(data);
	};


	sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "Element config",
	        "icon" : "fa-cog",
	        "properties" : {
	            type : {
	                inputType : "select",
	                label : "Définir un type d'élément",
	                options :  sectionDyf.<?php echo $kunik ?>ParamsData.type,
	                value : "<?php echo (isset($parentForm["params"][$kunik]['type'])) ? $parentForm["params"][$kunik]['type'] : ''; ?>"
	            },
	            limit : {
	                label : "Combien d'éléments peuvent être ajoutés (0 si pas de limite)",
	                value : "<?php echo (isset($parentForm["params"][$kunik]['limit'])) ? $parentForm["params"][$kunik]['limit'] : ''; ?>"
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);

	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    $("#ajax-modal").modal('hide');
	                    urlCtrl.loadByHash(location.hash);
	                } );
	            }

	    	}
	    }
	};


    
	var dynformCostumAnswer = costum.<?php echo $kunik ?>;
    //adds a line into answer

   <?php if(isset($parentForm["params"][$kunik]["type"])) { ?>
	   		if(costum.typeObj.<?php echo $parentForm["params"][$kunik]["type"] ?>.dynFormCostum!="undefined"){
	   		 dynformCostumAnswer = $. extend({}, costum.typeObj.<?php echo $parentForm["params"][$kunik]["type"] ?>.dynFormCostum, dynformCostumAnswer);
	   		}
 <?php  } ?>




    <?php if( isset($parentForm["params"][$kunik]['type']) ) { ?>
    $(".add<?php echo $kunik ?>").off().on("click",function() { 
    	mylog.log("dynformCostumAnswer",dynformCostumAnswer); 
        tplCtx.id = $(this).data("id");
        form= $(this).data("form");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
        dyFObj.openForm( "<?php echo Element::getControlerByCollection($parentForm["params"][$kunik]['type']); ?>",null,null,null,dynformCostumAnswer);
    });

    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.editElement( <?php echo $kunik ?>Data[$(this).data("key")].type,<?php echo $kunik ?>Data[$(this).data("key")].id, null, dynformCostumAnswer);
    });
    <?php } ?>

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    
});
</script>
<?php } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>