<?php if($answer){ ?>
<div class="form-group">
	<table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>">
		
	<?php 
		$paramsData = [ "type" => [
					    	Organization::COLLECTION => "Organization",
					    	Person::COLLECTION 		 => "Person",
					    	Event::COLLECTION 		 => "Event",
					    	Project::COLLECTION 	 => "Project",
							News::COLLECTION 		 => "News",
					    	//Need::COLLECTION 		 => "Need",
					    	City::COLLECTION 		 => "City",
					    	Thing::COLLECTION 		 => "Thing",
					    	Poi::COLLECTION 		 => "Poi",
					    	Classified::COLLECTION   => "Classified",
					    	Product::COLLECTION 	 => "Product",
					    	Service::COLLECTION   	 => "Service",
					    	Survey::COLLECTION   	 => "Survey",
					    	Bookmark::COLLECTION   	 => "Bookmark",
					    	Proposal::COLLECTION   	 => "Proposal",
					    	Room::COLLECTION   	 	 => "Room",
					    	Action::COLLECTION   	 => "Action",
					    	Network::COLLECTION   	 => "Network",
					    	Url::COLLECTION   	 	 => "Url",
					    	Circuit::COLLECTION   	 => "Circuit",
					    	Risk::COLLECTION   => "Risk",
					    	Badge::COLLECTION   => "Badge",
					    ],
					    "limit" => 0 ];
		
		if( isset($parentForm["params"][$kunik]) ) {
			if( isset($parentForm["params"][$kunik]["limit"]) ) 
				$paramsData["limit"] =  $parentForm["params"][$kunik]["limit"];
		}

		$properties = [
                "qui" => [
                    "label" => "Qui...?",
                    "placeholder" => "Qui...",
                ],
                "type" => [
                    "label" => "...Type ?",
                    "placeholder" => "...type...",
                ]
	        ];

		$editBtnL = ($canEdit === true
					&& isset($parentForm["params"][$kunik])
					&& ( $paramsData["limit"] == 0 || 
						!isset($answers) || 
						( isset($answers) && $paramsData["limit"] > count($answers) ))) 
			? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default '><i class='fa fa-plus'></i> Ajouter un élément </a>" 
			: "";
		
		$editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";
	?>	
	<thead>
		<tr>
			<td colspan='<?php echo count( $properties)+2?>' ><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
				<?php echo $info;
				if( !isset($parentForm["params"][$kunik]['type']) ) 
					echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> THIS FIELD HAS TO BE CONFIGURED FIRST ".$editParamsBtn."</span>";
				 ?>

			</td>
		</tr>	
		<?php if(isset($answers) && count($answers)>0){ ?>
		<tr>
			</th>
			<?php 
			
			foreach ($properties as $i => $inp) {
				echo "<th>".$inp["placeholder"]."</th>";
			} ?>
			<th></th>
		</tr>
		<?php } ?>
	</thead>
	<tbody class="directoryLines">	
		<?php 
		$ct = 0;
		
		if(isset($answers))
		{
			foreach ($answers as $q => $a) 
			{
				if( $paramsData["limit"] == 0 || $paramsData["limit"] > $q )
				{
					echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
					foreach ($properties as $i => $inp) 
					{
						if( $i == "qui" && isset($a["slug"])) {
							$el = Slug::getElementBySlug($a["slug"]);
							echo "<td><a href='#page.type.".$el["type"].".id.".$el["id"]."' class='lbh-preview-element' >".$el["el"]["name"]."</a></td>";
						} else 
							echo "<td>".$a[$i]."</td>";
					}
				?>
				<td>
					<?php 
					$this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
										"canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
										"id" => $answer["_id"],
										"collection" => Form::ANSWER_COLLECTION,
										"q" => $q,
										"path" => $answerPath.$q,
										"kunik"=>$kunik ] );
					?>
					<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')">
								<?php 
									echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q)); ?> 
									<i class='fa fa-commenting'></i></a>
				</td>
				<?php 
					$ct++;
					echo "</tr>";
				}
			}
		}
		 ?>
		</tbody>
	</table>
</div>
<script type="text/javascript">

var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

$(document).ready(function() { 
	mylog.log("render","/modules/costum/views/tpls/forms/element.php");
	//can be hacked to apply further costumization
	//is used like a dynFormCostumIn in openForm
	costum.<?php echo $kunik ?> = {
		beforeBuild : {
                    properties : {
                        name : dyFInputs.name(<?php echo (isset($parentForm["params"][$kunik]['type'])) ? json_encode(Element::getControlerByCollection($parentForm["params"][$kunik]['type'])) : json_encode(''); ?>,null,null,<?php echo json_encode($kunik) ?>),
                        "formLocality": {
			              "rules": {
			                "required": true
			              }
			            },
			            "url": {
			              "label": "Site internet (commençant par \" http:// \")"
			            },
			            "email": {
			              "label": "Email contact du tiers-lieu"
			            },
			            "shortDescription": {
			              "order": 3
			            },
			            "typePlace": {
			              "order": 4,
			              "inputType": "select",
			              "label": "Type de lieu",
			              "placeholder": "Choisir un type de lieu",
			              "list": "typePlace",
			              "groupOptions": false,
			              "groupSelected": false,
			              "optionsValueAsKey": true,
			              "select2": {
			                "multiple": true
			              }
			            },
			            "services": {
			              "inputType": "select",
			              "order": 5,
			              "label": "Services complémentaires",
			              "placeholder": "Choisir un service",
			              "list": "services",
			              "groupOptions": false,
			              "groupSelected": false,
			              "optionsValueAsKey": true,
			              "select2": {
			                "multiple": true
			              }
			            },
			            "manageModel": {
			              "inputType": "select",
			              "order": 6,
			              "label": "Mode de gestion",
			              "placeholder": "Quel(s) type(s) de structure(s) gère(nt) et anime(nt) votre espace ?",
			              "list": "manageModel",
			              "groupOptions": false,
			              "groupSelected": false,
			              "optionsValueAsKey": true,
			              "select2": {
			                "multiple": false
			              },
			              "rules": {
			                "required": true
			              }
			            },
			            "state": {
			              "inputType": "select",
			              "label": "Etat du projet",
			              "order": 7,
			              "placeholder": "Choisir un état d'avancement",
			              "list": "state",
			              "groupOptions": false,
			              "groupSelected": false,
			              "optionsValueAsKey": true,
			              "select2": {
			                "multiple": false
			              }
			            },
			            "spaceSize": {
			              "inputType": "select",
			              "order": 8,
			              "label": "Taille de l'espace",
			              "placeholder": "Choisir une taille",
			              "list": "spaceSize",
			              "groupOptions": false,
			              "groupSelected": false,
			              "optionsValueAsKey": true,
			              "select2": {
			                "multiple": false
			              }
			            },
			            "greeting": {
			              "inputType": "select",
			              "order": 10,
			              "label": "Actions d’accueil, d’orientation et d’accompagnement que vous mettez en place pour d’autres porteurs de projet de tiers-lieux",
			              "placeholder": "Sélectionner la(es) action(s)",
			              "list": "greeting",
			              "groupOptions": false,
			              "groupSelected": false,
			              "optionsValueAsKey": true,
			              "select2": {
			                "multiple": true
			              }
			            },
			            "certification": {
			              "inputType": "select",
			              "order": 11,
			              "label": "Etes-vous Fabrique de Territoire ?",
			              "placeholder": "Choisir un label",
			              "list": "certification",
			              "groupOptions": false,
			              "groupSelected": false,
			              "optionsValueAsKey": true,
			              "select2": {
			                "multiple": false
			              }
			            },
			            "compagnon": {
			              "inputType": "select",
			              "order": 12,
			              "label": "Vous souhaitez être inscrit comme “tiers-lieu compagnon” et ainsi accueillir des porteurs de projet qui souhaitent en savoir plus sur les tiers-lieux, bénéficier de vos conseils et de votre expérience ?",
			              "placeholder": "Si oui, sélectionnez 'Compagnon FTL' ci-dessous",
			              "info": "<a href='https://francetierslieux.fr/wp-content/uploads/2020/12/Charte_TLCompagnon.pdf' target='_blank'>Qu’est-ce qu’un tiers-lieu compagnon ?</a>",
			              "list": "compagnon",
			              "groupOptions": false,
			              "groupSelected": false,
			              "optionsValueAsKey": true,
			              "select2": {
			                "multiple": false
			              }
			            },
			            "territory": {
			              "inputType": "select",
			              "order": 13,
			              "label": "Territoire d'implantation de votre tiers-lieu",
			              "placeholder": "Territoire d'implantation",
			              "list": "territory",
			              "groupOptions": false,
			              "groupSelected": false,
			              "optionsValueAsKey": true,
			              "select2": {
			                "multiple": false
			              },
			              "rules": {
			                "required": true
			              }
			            },
			            "description": {
			              "inputType": "textarea",
			              "markdown": true,
			              "label": "description longue"
			            },
			            "mobile": {
			              "label": "Contact téléphone",
			              "placeholder": "Renseigner un numéro de téléphone",
			              "inputType": "text"
			            }
                    }
        },
        searchExist : function (type,id,name,slug,email) { 
			mylog.log("costum searchExist : "+type+", "+id+", "+name+", "+slug+", "+email); 
			var data = {
				type : type,
				id : id,
				map : { slug : slug }
			}

			costum.<?php echo $kunik ?>.connectToAnswer(data);
			// $("#similarLink").hide();
			// $("#ajaxFormModal #name").val("");


			// // TODO - set a condition ONLY if can edit element (authorization)
			// dyFObj.editElement( type,id, null,dynformCostumAnswer);
		},               
		onload : {
			"actions" : {
				"setTitle" : "<?php echo $input["label"] ?>"
			}
		},		
		formData : function(data){
			//alert("formdata");
			mylog.log("formData",data);
			//if(dyFObj.editMode){
			$.each(data, function(e, v){
				//alert(e, v){
				if(typeof costum.lists[e] != "undefined"){
					if(notNull(v)){
						
						if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
						if(typeof v == "string")
							data.tags.push(v);
						else{
							$.each(v, function(i,tag){
								data.tags.push(tag);	
							});
						}
					}
					delete data[e];
				}
			});
			data.tags.push("TiersLieux");
				//delete data.mainTag;
			
			return data;
		},
		afterSave : function(data) { 
			mylog.log("element afterSave",data)
			costum.<?php echo $kunik ?>.connectToAnswer(data);
		},
		connectToAnswer : function ( data ) { 
			mylog.log("costum.<?php echo $kunik ?>.connectToAnswer",data)
			tplCtx.value = {
				type : (data.type) ? data.type : "<?php echo (isset($parentForm["params"][$kunik]['type'])) ? $parentForm["params"][$kunik]['type'] : ''; ?>",
				id : data.id,
				slug : data.map.slug
			};

		    mylog.log("save tplCtx",tplCtx);
		    
		    if(typeof tplCtx.value == "undefined")
		    	toastr.error('value cannot be empty!');
		    else {
		        dataHelper.path2Value ( tplCtx, function(params) { 
		            $("#ajax-modal").modal('hide');
		            urlCtrl.loadByHash(location.hash);
		        } );
		    }
	    }
	  // onload : {
	  // 	"actions" : {
	  //    	"hide": {
	  //           		"parentfinder" : 1
	  //           	}
	  //           }
	  //       }
	};
	// if(!costum.searchExist && typeof costum.searchExist!="function"){
	// 	costum.searchExist = function (type,id,name,slug,email) { 
	// 		mylog.log("costum searchExist : "+type+", "+id+", "+name+", "+slug+", "+email); 
	// 		var data = {
	// 			type : type,
	// 			id : id,
	// 			map : { slug : slug }
	// 		}
	// 		costum.<?php echo $kunik ?>.connectToAnswer(data);
	// 	};
	// }	


	sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "Element config",
	        "icon" : "fa-cog",
	        "properties" : {
	            type : {
	                inputType : "select",
	                label : "Définir un type d'élément",
	                options :  sectionDyf.<?php echo $kunik ?>ParamsData.type,
	                value : "<?php echo (isset($parentForm["params"][$kunik]['type'])) ? $parentForm["params"][$kunik]['type'] : ''; ?>"
	            },
	            limit : {
	                label : "Combien d'éléments peuvent être ajoutés (0 si pas de limite)",
	                value : "<?php echo (isset($parentForm["params"][$kunik]['limit'])) ? $parentForm["params"][$kunik]['limit'] : ''; ?>"
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);

	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    $("#ajax-modal").modal('hide');
	                    urlCtrl.loadByHash(location.hash);
	                } );
	            }

	    	}
	    }
	};


    
	var dynformCostumAnswer = costum.<?php echo $kunik ?>;
    //adds a line into answer

   <?php if(isset($parentForm["params"][$kunik]["type"])) { ?>
	   		if(costum && costum.typeObj && costum.typeObj.<?php echo $parentForm["params"][$kunik]["type"] ?> && costum.typeObj.<?php echo $parentForm["params"][$kunik]["type"] ?>.dynFormCostum!="undefined"){
	   		 dynformCostumAnswer = $. extend({}, costum.typeObj.<?php echo $parentForm["params"][$kunik]["type"] ?>.dynFormCostum, dynformCostumAnswer);
	   		}
 <?php  } ?>




    <?php if( isset($parentForm["params"][$kunik]['type']) ) { ?>
    $(".add<?php echo $kunik ?>").off().on("click",function() { 
    	mylog.log("dynformCostumAnswer",dynformCostumAnswer); 
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
        dyFObj.openForm( "<?php echo Element::getControlerByCollection($parentForm["params"][$kunik]['type']); ?>",null,null,null,dynformCostumAnswer);
    });

    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( "<?php echo $parentForm["params"][$kunik]['type']; ?>",null, <?php echo $kunik ?>Data[$(this).data("key")]);
    });
    <?php } ?>

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    var modelList = {
    	"typePlace": [
      "Coworking",
      "Ateliers artisanaux partagés",
      "Fablab / Atelier de Fabrication Numérique",
      "Tiers-lieu culturel",
      "Tiers-lieu agricole",
      "Cuisine partagée / Foodlab",
      "LivingLab / Laboratoire d'innovation sociale"
    ],
    "services": [
      "Accompagnement des publics",
      "Action sociale",
      "Aiguillage / Orientation",
      "Bar / café",
      "Boutique / Épicerie",
      "Coopérative d'Activités et d'Emploi / Groupement d'employeur",
      "Cantine / restaurant",
      "Centre de ressources",
      "Chantier participatif",
      "Complexe évènementiel",
      "Conciergerie ",
      "Domiciliation",
      "Espace de stockage",
      "Espace détente",
      "Espace enfants",
      "Formation / Transfert de savoir-faire / Éducation",
      "Habitat",
      "Incubateur",
      "Lieu d'éducation populaire et nouvelles formes d'apprentissage",
      "Maison de services au public",
      "Marché",
      "Média et son",
      "MediaLab",
      "Médiation numérique",
      "Offre artistique ou culturelle",
      "Pépinière d'entreprises",
      "Point d'appui à la vie associative",
      "Point Information Jeunesse",
      "Point d'information touristique",
      "Pratiques de soin (art thérapie, massage, médecine douce, méditation...)",
      "Résidences d'artistes",
      "Ressourcerie / recyclerie",
      "Service enfance-jeunesse",
      "Services liés à la mobilité"
    ],
    "manageModel": [
      "Association",
      "Collectif citoyen",
      "SARL-SA-SAS",
      "SCIC-SCOP",
      "Société Publique Locale",
      "Pôle d'Equilibre Territorial Rural",
      "Département",
      "Région",
      "Groupement d'Intérêt Public",
      "Intercommunalité",
      "Université",
      "Collège",
      "Lycée"
    ],
    "state": [
      "En projet",
      "En fonctionnement"
    ],
    "spaceSize": [
      "Moins de 60m²",
      "Entre 60 et 200m²",
      "Plus de 200m²"
    ],
    "network": [
      "A+ C'est Mieux",
      "Actes‐IF",
      "Artfactories/Autresparts",
      "Bienvenue dans la Canopée",
      "Cap Tiers‐Lieux",
      "Cédille Pro",
      "Collectif des Tiers‐Lieux Ile‐de‐France",
      "Collectif Hybrides",
      "Compagnie des Tiers‐Lieux",
      "Coopérative des Tiers‐Lieux",
      "Coordination Nationale des Lieux Intermédiaires et Indépendants (CNLII)",
      "Cowork'in Tarn",
      "Coworking Grand Lyon",
      "CRLII Occitanie",
      "GIP Recia",
      "Hub France Connectée",
      "L'ALIM (l'Assemblée des lieux intermédiaires marseillais)",
      "La Trame 07",
      "Label Tiers‐Lieux Occitanie",
      "Label C3",
      "Label Tiers‐Lieux Normandie",
      "Le DOG",
      "Le LIEN",
      "Lieux Intermédiaires en région Centre",
      "Réseau des tiers‐lieux Bourgogne Franche Comté",
      "Réseau Français des Fablabs",
      "Réseau Médoc",
      "Réseau TELA",
      "Tiers‐Lieux Edu"
    ],
    "greeting": [
      "Conseil et orientation avec contact téléphonique",
      "Point d'échange physique proposé pour monter en compétences respectivement",
      "Voyage apprenant pour découverte de lieux",
      "Accueil d'élus pour acculturation",
      "Visioconférence de présentations",
      "Étude d'opportunité",
      "Aide à la réalisation de dossiers pour AMI ou subventions",
      "Conseil projet",
      "Incubation de projets",
      "Mentoring / coaching de porteurs de projet",
      "Parcours de formation pour des porteurs de projet"
    ],
    "certification": [
      "Fabrique de Territoire",
      "Fabrique Numérique de Territoire"
    ],
    "compagnon": [
      "Compagnon France Tiers-Lieux"
    ],
    "territory": [
      "En agglomérations",
      "En métropole",
      "En milieu rural",
      "En ville moyenne (entre 20000 et 100000 habitants)"
    ]
    };

    var cList=(typeof costum.lists!="undefined") ? costum.lists : {};
    if(typeof costum!="undefined"){
    	costum.lists=$.extend(cList,modelList);
    }

    
});
</script>
<?php } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>