# OPEN FORMS
schema [[https://docs.google.com/presentation/d/1jwi3pYEzL_r8IH1AEpzlJe4jTt2kG7BZS5Hyq1kZHeE/edit#slide=id.g33f0c79dba_0_0]]
(doc org)[openForm.org]

## costum Template (costumTpl)
uses a classic costum engine with a json associated 
editable blocks in the *costumTpl* are made editable like this 
```
<?php 
if(isset($this->costum["cms"]["title1"])){
  echo htmlentities($this->costum["cms"]["title1"]);
} else { ?> Open Form <?php } 
if($canEdit)
  echo "<a class='btn btn-xs btn-danger editBtn' href='javascript:;' data-key='title1' data-path='costum.cms.title1'><i class='fa fa-pencil'></i></a>";
?>
```

## costum Form Template (costumFormTpl)
when a *costumTpl* contains an *openForm* it becomes a *costumFormTpl*
and has an entry in *forms* collection [[/var/www/dev/modules/costum/data/forms/openForm.json]]

### Form Template (formTpl)
each input of this form corresponds to a :formTpl: listed inside `survey.views.tpls.forms`
*formTpls* are defined by a single html file
can be used by default as it is 
or can be configured with specific parameters by click the cog btn
congiguration are saved on the parent element inside `costum.form.params` ex .calendar

### Form Template Answers 
*Answers* to *costumFormTpls* are saved inside the *answers* collection
exemple
```
"_id" : ObjectId("5e0db794539f22ac1567c354"),
    "formId" : "openForm1",
    "answers" : {
        "email" : "qfdfds fqd",
        "talk" : "qf ds fqs",
        "sure" : "on",
        "chooki" : "#000000",
        "openForm15" : "",
        "openForm16" : "dq sd sqd qsd sqd",
        "calendar" : {
            "0" : {
                "step" : " fqsdf sqdf qsdf ",
                "startDate" : "10/01/2020",
                "endDate" : "17/12/2020"
            },
            "1" : {
                "step" : "f dsfqsdf qsdf",
                "startDate" : "07/01/2020",
                "endDate" : "23/02/2023"
            }
        }
    },
    "user" : "5534fd9da1aa14201b0041cb",
    "parentSlug" : "safranLaCapucine",
    "created" : NumberLong(1577957268),
    "voteUp" : { },
    "updated" : NumberLong(1578075851),
    "modified" : ISODate("2020-01-03T18:24:11.000Z"),
    "voteUpCount" : NumberLong(1),
    "fund" : { },
    "fundCount" : NumberLong(5)
}
```


# json 
[[~d/modules/costum/data/freeform.json]]
# views 
[[~d/modules/costum/views/custom/co/freeform.php]] 
  [[~d/modules/costum/views/custom/co/formbuilder.php]]
    ** basic form tpls
    [[~d/modules/costum/views/tpls/forms/text.php]]
    [[~d/modules/costum/views/tpls/forms/select.php]]
    [[~d/modules/costum/views/tpls/forms/textarea.php]]
    [[~d/modules/costum/views/tpls/forms/checkbox.php]]
    ** complexe tpls forms
    [[~d/modules/costum/views/tpls/forms/calendar.php]]
  [[~d/modules/costum/views/custom/co/answerList.php]]

# data structure 
## collections used 
    *db.costum* : contains all details for the costum 
      [[~d/modules/costum/data/freeform.json]]
    *db.forms* : contains question creation definition of an openForm
      [[~d/modules/costum/data/openForm.json]]
    *db.element* : contains all elements specific settings for the costum and the form, overloading whatever is redefined in element.costum and linked to a costum through costum.slug (cf. [[~d/modules/costum/README.md]])

    ex : 
    ```
    "costum" : {
        "colors" : {
            "pink" : "#233234",
            "dark" : "#436577"
        },
        "slug" : "freeform",
        "form" : {
            "id" : "openForm1",
            "canModifyAnswer" : true,
            "canVote" : true,
            "canFund" : true,
            "showDate" : true,
            "params" : {
                "calendar" : {
                    "sectionTitles" : [ 
                        "1er<br/>Sem<br/>2020", 
                        "2ème<br/>Sem<br/>2020", 
                        "1er<br/>Sem<br/>2021", 
                        "2ème<br/>Sem<br/>2021", 
                        "1er<br/>Sem<br/>2022", 
                        "2ème<br/>Sem<br/>2022"
                    ],
                    "dateSections" : [ 
                        "01/01/2020", 
                        "01/07/2020", 
                        "01/01/2021", 
                        "01/07/2021", 
                        "01/01/2022", 
                        "01/07/2022", 
                        "01/01/2023"
                    ]
                },
                "financement" : {
                    "tpl" : "tpls.forms.equibudget"
                }
            }
        },
        "cms" : {
            "title1" : "PIna colada des alternatives",
            "subtitle1" : "Tout est possible si on y croit <br/> Meme quand tout le monde mange de la coco ",
            "textIntro" : "c<wxc<wxc<wxc<wx"
        }
    }
    ```
    db.answer : contains all answers per user to the form connected to the costum connected to the element 

# DYNFORMS

*keyTpl* is the type of the *formTpl*
adding data and content uses classic dynform definition files 
```
sectionDyf.<?php echo $keyTpl ?>
```

configuring parameters for a given *formTpl* 
``` 
sectionDyf.<?php echo $keyTpl ?>Params 
```