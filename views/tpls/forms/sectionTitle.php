<div class="col-xs-12 no-padding text-center margin-top-20 margin-bottom-20">
    <h2 class="padding-bottom-20" style="border-bottom: 1px solid gray"><?php echo $label.$editQuestionBtn ?></h2>
    <?php if(!empty($info)){ ?>
        <div class="markdown-<?php echo $kunik ?> small"><?php echo $info ?></div>
    	<small id="<?php echo $key ?>Help" class="form-text text-muted hidden"><?php echo $info ?></small>
    <?php } ?>
</div>

<?php if($mode != "pdf"){ ?>
<script type="text/javascript">
jQuery(document).ready(function() {
    mylog.log("render form input","/modules/costum/views/tpls/forms/sectionTitle.php");
    var descHtml = dataHelper.markdownToHtml($(".markdown-<?php echo $kunik ?>").html(),{
        parseImgDimensions:true,
        simplifiedAutoLink:true,
        strikethrough:true,
        tables:true,
        openLinksInNewWindow:true
    });
    $(".markdown-<?php echo $kunik ?>").html(descHtml);
});
</script>
<?php } ?>