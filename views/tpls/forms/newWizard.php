<?php
$ignore = array('_file_', '_params_', '_obInitialLevel_' ,'ignore');
$params = array_diff_key(get_defined_vars() + array_flip($ignore), array_flip($ignore));
?>

<ul class="nav">
    <?php
    $stepnum = 1;
    foreach ($steps as $step) {
    ?>
        <li class="nav-item">
            <a class="nav-link" href="#step-<?= $step["step"] ?>">
                <div class="num"><?= $stepnum ?></div>
                <div class="wstext"><?= $step["name"] ?></div>
            </a>
        </li>
    <?php
        $stepnum++;
    }
    ?>
</ul>

<div class="wstextmobile">

</div>

<script type="text/javascript">
    if(typeof coForm_FormWizardParams == "undefined") {
        var coForm_FormWizardParams = <?php echo json_encode($params); ?>;
    }
    const currentFormWizardParams = <?php echo json_encode($params); ?>;
    if(currentFormWizardParams.parentForm && currentFormWizardParams.parentForm._id && (/^[0-9a-fA-F]{24}$/).test(currentFormWizardParams.parentForm._id.$id)) {
        coForm_FormWizardParams = <?php echo json_encode($params); ?>;
    }

    jQuery(document).ready(function() {
        var coForm_actualStep = Object.values(coForm_FormWizardParams["steps"]).length > 0 ? Object.values(coForm_FormWizardParams["steps"])[0]["step"] : null;
        var coForm_actualState = "first";
        if(notNull(localStorage.getItem("stepCoForm_id_"+coForm_FormWizardParams.answer?.["_id"]?.["$id"]))){
            coForm_actualStep = localStorage.getItem("stepCoForm_id_"+coForm_FormWizardParams["answer"]["_id"]["$id"]);
        }
        var coForm_actualStep_id = Object.values(coForm_FormWizardParams["steps"]).map(function(o) { return o.step; }).indexOf(coForm_actualStep);
        if(notNull(localStorage.getItem("stateCoForm_"+coForm_FormWizardParams.answer?.["_id"]?.["$id"]))){
            coForm_actualState = localStorage.getItem("stateCoForm_"+coForm_FormWizardParams["answer"]["_id"]["$id"]);
        }
        ajaxPost(null, baseUrl+'/survey/answer/getparamswizard/answer/'+coForm_FormWizardParams["answer"]["_id"]["$id"]+'/form/'+coForm_FormWizardParams["parentForm"]["_id"]["$id"]+'/standAlone/'+coForm_FormWizardParams["isStandalone"],
            { url : window.location.href },
            function(data) {
                if(typeof swDisabledSteps != "undefined" && typeof swHiddenSteps != "undefined"){
                    swDisabledSteps = JSON.parse(data).disabledSteps;
                    swHiddenSteps = JSON.parse(data).hiddenSteps;
                }else{
                    var swDisabledSteps = JSON.parse(data).disabledSteps;
                    var swHiddenSteps = JSON.parse(data).hiddenSteps;
                }
                $('#smartwizard').smartWizard({
                    selected: 0, // Initial selected step, 0 = first step
                    theme: 'dots', // 'arrows', 'square', 'round', 'dots'
                    justified: true, // Nav menu justification. true/false
                    autoAdjustHeight: false, // Automatically adjust content height
                    backButtonSupport: true, // Enable the back button support
                    enableUrlHash: false, // Enable selection of the step based on url hash
                    transition: {
                        animation: 'slideHorizontal', // Animation effect on navigation, none|fade|slideHorizontal|slideVertical|slideSwing|css(Animation CSS class also need to specify)
                        speed: '50', // Animation speed. Not used if animation is 'css'
                        easing: '', // Animation easing. Not supported without a jQuery easing plugin. Not used if animation is 'css'
                        prefixCss: '', // Only used if animation is 'css'. Animation CSS prefix
                        fwdShowCss: '', // Only used if animation is 'css'. Step show Animation CSS on forward direction
                        fwdHideCss: '', // Only used if animation is 'css'. Step hide Animation CSS on forward direction
                        bckShowCss: '', // Only used if animation is 'css'. Step show Animation CSS on backward direction
                        bckHideCss: '', // Only used if animation is 'css'. Step hide Animation CSS on backward direction
                    },
                    style: {
                        btnCss: 'aap-sw-btn',
                        toolbarCss: 'toolbar dropup'
                        /*btnNextCss: 'sw-btn-next',
                        btnPrevCss: 'sw-btn-prev sw-btn'*/
                    },
                    toolbar: {
                        position: 'bottom', // none|top|bottom|both
                        showNextButton: true, // show/hide a Next button
                        showPreviousButton: true, // show/hide a Previous button
                        extraHtml: `<button class="btn btn-success disabled aap-sw-btn sw-btn-finish" disabled onclick="onFinishStep()">${trad.finished}</button>
                                    <button class="btn btn-secondary disabled aap-sw-btn sw-btn-cancel" onclick="onCancelStep()">${ trad.cancel }</button>
                                    <button class="btn btn-secondary disabled aap-sw-btn sw-btn-draft" onclick="onDraftStep()">Enregistrer comme brouillon</button>
                                    <button type="button" class="btn btn-secondary aap-sw-btn sw-btn-obl dropdown-toggle disabled" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ><i class="fa fa-exclamation-triangle"></i><span class="badge badgeobl">0</span> </button>
                                    <div class="dropdown-menu obldroplist">

                                    </div>
` // Extra html to show on toolbar
                    },
                    anchor: {
                        enableNavigation: true, // Enable/Disable anchor navigation
                        enableNavigationAlways: true, // Activates all anchors clickable always
                        enableDoneState: false, // Add done state on visited steps
                        markPreviousStepsAsDone: false, // When a step selected by url hash, all previous steps are marked done
                        unDoneOnBackNavigation: false, // While navigate back, done state will be cleared
                        enableDoneStateNavigation: false, // Enable/Disable the done state navigation
                        hideButtonsOnDisabled : true
                    },
                    keyboard: {
                        keyNavigation: false, // Enable/Disable keyboard navigation(left and right keys are used if enabled)
                        keyLeft: [37], // Left key code
                        keyRight: [39] // Right key code
                    },
                    lang: { // Language variables for button
                        next: tradAdmin.nextStep,
                        previous: tradAdmin.previousStep
                    },
                    hideButtonsOnDisabled : true,
                    disabledSteps: JSON.parse(data).disabledSteps, // Array Steps disabled
                    errorSteps: [], // Array Steps error
                    warningSteps: [], // Array Steps warning
                    hiddenSteps: JSON.parse(data).hiddenSteps, // Hidden steps
                    getContent: provideContent, // Callback function for content loading

                });
                $("#smartwizard").on("showStep", function (e, anchorObject, stepIndex, stepDirection, stepPosition) {
                    $('.sw-toolbar-elm').css('display', 'block');
                    localStorage.setItem("stepCoForm_id_" + coForm_FormWizardParams["answer"]["_id"]["$id"], Object.values(coForm_FormWizardParams["steps"])[stepIndex]["step"]);
                    localStorage.setItem("stateCoForm_" + coForm_FormWizardParams.answer?.["_id"]?.["$id"], stepPosition);
                    $("#smartwizard .nav-item").removeClass("active");
                    $($($(e.currentTarget).children(".nav")[0]).children('.nav-item')[stepIndex]).addClass("active");
                    $(".tab-content").height("100%");
                    if(typeof bindDnDEvent != 'undefined') {
                        bindDnDEvent()
                    }
                    if(typeof updateWizardBtn != 'undefined') {
                        updateWizardBtn(coForm_FormWizardParams, stepIndex , stepPosition , true);
                    }
                    if(typeof eventScrollAlreadyCalled != 'undefined' && eventScrollAlreadyCalled == false && viewMode != 'r' && viewMode != 'pdf' && viewMode != 'fa') {
                        eventScrollAlreadyCalled = true;
                        $(window).on('resize scroll', function(e) {
                            buildScrollValidation(inputObliList, formKey)
                        });
                    }
                    if(typeof bindSectionCollapseEvent != "undefined") {
                        bindSectionCollapseEvent()
                    }
                    $('.wstextmobile').html('');
                    $('.wstextmobile').html(Object.values(coForm_FormWizardParams.steps)[$('#smartwizard').smartWizard("getStepInfo").currentStep].name);
                });
                $("#smartwizard .nav-link.hidden").parent("#smartwizard .nav-item").addClass('hidden');
                $("#smartwizard .nav-link.disabled").parent("#smartwizard .nav-item").addClass('disabled');
                $("#smartwizard .nav-link.disabled .num").html('<i class="fa fa-lock"></i>');
            },"html");
    });


</script>

