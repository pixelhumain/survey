<?php 
$cssJS = array(
    '/plugins/jQuery-Knob/js/jquery.knob.js',
    //'/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
    //'/plugins/jQuery-Smart-Wizard/styles/smart_wizard.css',
  // SHOWDOWN
  '/plugins/showdown/showdown.min.js',
  // MARKDOWN
  '/plugins/to-markdown/to-markdown.js',
  //Float button
  '/plugins/float-button-st-panel/css/st.action-panel.css',
  '/plugins/float-button-st-panel/js/st.action-panel.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);
HtmlHelper::registerCssAndScriptsFiles(array( 
  '/js/answer.js',
  ), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );

$poiList = array();
$coformQuestionIcons = [
  "text" => "text.png",
  "textarea" => "textarea.png",
  "tpls.forms.checkbox" => "icones_checkbox.png",
  "color" => "icones_color picker.png",
  "email" => "icones_email.png",
  "tpls.forms.cplx.address" => "icones_geocode.png",
  "url" => "icones_lien.png",
  "tpls.forms.cplx.calendar" => "icones_list actions date.png",
  "tpls.forms.cplx.multiCheckboxPlus" => "icones_liste choix.png",
  "tpls.forms.cplx.multiCheckbox" => "icones_liste choix.png",
  "tpls.forms.cplx.checkboxNew" => "icones_liste choix.png",
  "tpls.forms.cplx.categorizedCheckbox" => "icones_liste choix.png",
  "tpls.forms.cplx.financement" => "icones_liste financement.png",
  "tpls.forms.cplx.budget" => "icones_listes dépenses.png",
  "tpls.forms.cplx.multiRadio" => "icones_radio button.png",
  "tpls.forms.cplx.radioNew" => "icones_radio button.png",
  "tpls.forms.tags" => "icones_tags.png",
  "tel" => "icones_telephone.png",
  "tpls.forms.emailUser" => "icones_email.png",
  "date" => "simple_input.png",
  "date" => "simple_input.png",
  "datetime-local" => "simple_input.png",
  "month" => "simple_input.png",
  "week" => "simple_input.png",
  "time" => "simple_input.png",
];
$coformDesc = [
  "text" => "c’est un input de type texte, avec lequel on peut saisir des textes courtes",
];

if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
    $poiList = PHDB::find(Poi::COLLECTION, 
                    array( "parent.".$this->costum["contextId"] => array('$exists'=>1), 
                           "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"],
                           "type"=>"cms") );
}
?>

<style type="text/css">
  @font-face{
      font-family: "montserrat";
       src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/montserrat-regular.woff") format("woff"),
       url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/montserrat-regular.ttf") format("ttf")
  }.mst{font-family: 'montserrat'!important;}

  @font-face{
      font-family: "CoveredByYourGrace";
      src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/CoveredByYourGrace.ttf")
  }.cbyg{font-family: 'CoveredByYourGrace'!important;}
    

  .switchTopButton{
    position: fixed;
    /*padding: 5px;*/
    right:-3px;
    font-size: 18px;

    border-radius: 20%;
    z-index:1;
  }  

.switchTopButton a, .switchTopButton span{
    font-size: 20px
  } 
  
    
  #customHeader{
    margin-top: 0px;
  }
  #costumBanner{
   /* max-height: 375px; */
  }
  #costumBanner h1{
    position: absolute;
    color: white;
    background-color: rgba(0,0,0,0.4);
    font-size: 29px;
    bottom: 0px;
    padding: 20px;
  }
  #costumBanner h1 span{
    color: #eeeeee;
    font-style: italic;
  }
  #costumBanner img{
    min-width: 100%;
  }
  .btn-main-menu{
    background: <?php echo @$this->costum["colors"]["pink"]; ?>;
    border-radius: 10px;
    padding: 10px !important;
    color: white;
    cursor: pointer;
    border:3px solid transparent;
    font-size: 1.5em
    /*min-height:100px;*/
  }
  .btn-main-menuW{
    background: white;
    color: <?php echo @$this->costum["colors"]["pink"]; ?>;
    border:none;
    cursor:text ;
  }
  .btn-main-menu:hover{
    border:2px solid <?php echo @$this->costum["colors"]["pink"]; ?>;
    background-color: white;
    color: <?php echo @$this->costum["colors"]["pink"]; ?>;
  }
  .btn-main-menuW:hover{
    border:none;
  }
  @media screen and (min-width: 450px) and (max-width: 1024px) {
    .logoDescription{
      width: 60%;
      margin:auto;
    }
  }

  @media (max-width: 1024px){
    #customHeader{
      margin-top: -1px;
    }
  }
  #customHeader #newsstream .loader{
    display: none;
  }

  .mr-4{
    margin-right: 1em !important;
  }
</style>

<script type="text/javascript">
    var sectionDyf = {};
    
</script>
<?php
  // $params = [  "tpl" => $this->costum["slug"],
  //       "slug"=>$el["slug"],
  //       "canEdit"=>$canEdit,
  //       "el"=>$el  ];
  // echo $this->renderPartial("survey.views.tpls.acceptAndAdmin", $params, true ); 
?>

<div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">

<?php
$color1 = "#E63458";
if(isset($this->costum["cms"]["color1"]))
  $color1 = $this->costum["cms"]["color1"];
// if($canEdit)
//   echo "<a class='btn btn-xs btn-danger editBtn' href='javascript:;' data-key='color1' data-type='color'  data-path='costum.cms.color1' data-label='Couleur Principale '><i class='fa fa-pencil'></i></a>";
?>

<style type="text/css">
      .monTitle{
        border-top: 1px dashed <?php echo @$this->costum["colors"]["pink"]; ?>; 
        border-bottom: 1px dashed <?php echo @$this->costum["colors"]["pink"]; ?>;
            /*margin-top: -20px;*/
      }
      .modal.preview-modal {
        margin: 15vh auto;
        width: 53vw;
        height: 70vh !important;
        max-height: 70vh !important;
        overflow-y: auto !important;
      }

      .modal.preview-modal .modal-content {
        transition: all 0.3s ease 0s;
      }

      .modal.preview-modal .modal-header {
        position: sticky;
        top: 0;
      }

      .modal.preview-modal .modal-body {
        display: flex;
        justify-content: center;
        flex-wrap: wrap;
      }

      .modal.preview-modal .modal-body .preview-img {
        width: 95%;
      }

      .p-0 {
        padding: 0;
      }

</style>

<?php $formSmallSize =  12; ?>
  <div class="col-md-12 col-lg-<?php echo $formSmallSize?> no-padding "><br/>

  <script type="text/javascript">
      var formInputs = {};
      var answerObj = <?php echo (!empty($answer)) ? json_encode( $answer ) : "null"; ?>;
      var form = <?php echo (!empty($form)) ? json_encode( $form ) : "null"; ?>;
  </script>

  <div class="col-xs-12 margin-top-20 coFormbody">
            <?php
            $wizardUid = (String) $form["_id"];
			if($canEdit === true && $mode != "fa" && empty($answer["validated"]) ){
				$nameMode = "mode read";
				if($mode == "w")
					$nameMode = "mode write";

				echo '<div class="col-xs-12 margin-bottom-15" id="modeSwitch" ></div>';
        echo '<div id="bottomModeSwitch" class="switchTopButton" style="bottom:50%"></div>';
			
      echo '<div id="arrowTop" class="switchTopButton" style="bottom:5%"></div>';
    
    } 

			if($mode != "fa" && !empty($parentForm["answersTpl"])){
				$params = [
					"parentForm"=>$parentForm,
					"el" => $el,
					"color1" => $color1,
					"canEdit" => $canEdit,
					"answer"=>$answer,
					"forms"=>$forms,
					"allAnswers"=>@$allAnswers,
					"what" => "dossiers",
					"wizid"=> $wizardUid
				];
				echo $this->renderPartial($parentForm["answersTpl"],$params);
			}

			if( $mode == "fa" && $canEditForm === true ){
				$params = [
                    "parentForm"=>$parentForm,
					"canEditForm"=>$canEditForm,
					"mode" => $mode,
					"form" => $form,
					"el" => $el
				];
				echo $this->renderPartial("survey.views.tpls.forms.config",$params);
        if((isset($parentForm["useBannerImg"]) && $parentForm["useBannerImg"] == true) || (isset($parentForm["useBannerText"]) && $parentForm["useBannerText"] == true)) {
        ?>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?php
              echo $this->renderPartial("survey.views.tpls.forms.formBanner" , array(
                  "isajax"=>$isajax ?? false,
                  "mode" => $mode,
                  "isStandalone" => $isStandalone ?? false,
                  "parentForm" =>$parentForm,
                  "canAdminAnswer" => $canAdminAnswer
              ));
            ?>
          </div>
        <?php 
        }
			}
      ?>
      <div id="wizardcontainer">
      <?php
			if( isset($answer) && !empty($showForm) && $showForm === true  ) {
        //var_dump($canEditForm);exit;
				$params = [
					"parentForm"=>$parentForm,
					"form" => $form, //identicall to parentForm kept on refactor
					"forms"=>$forms,
					"el" => $el,
					"active" => "all",
					"color1" => @$this->costum["colors"]["dark"],
					"color2" => @$this->costum["colors"]["pink"],
					"canEdit" => $canEdit,
					"canEditForm" => $canEditForm,
					"canAdminAnswer" => $canAdminAnswer,
					"answer"=>$answer,
					"showForm" => $showForm,
					"mode" => $mode,
					"showWizard"=>true,
					"wizid"=> $wizardUid,
          "isNew" => @$isNew,
          "contextId" => @$contextId,
          "contextType" => @$contextType,
          "usersAnswered" => @$usersAnswered
				];

				echo $this->renderPartial("survey.views.tpls.forms.wizard",$params); 
			}
            ?>
            </div>
            </div>

      </div>


</div>
<?php 
  if($mode != 'r' && isset($canEditForm) && $canEditForm && $mode == 'fa') {
?>
  <div class="main-menu st-actionContainer right-bottom">
      <div class="settings st-panel">
        <div class="searchBar-filters" style="width: 100%;margin-top: 10px; margin-bottom: 8px">
        <input type="text" class="form-control text-center main-search-bar search-bar searchInputCoform" data-field="text" placeholder="<?php echo Yii::t("common", "what are you looking for ?") ?>">
        <span class="text-white input-group-addon pull-left main-search-bar-addon searchBtnCoform" data-field="text">
            <i class="fa fa-arrow-circle-right" style="font-size: 1em !important;"></i>
            </span>
        </div>
        <div class="scrollbar" id="style-1">
          <ul id="inputsCoformList" style="padding: 0">
              <?php foreach (Form::inputTypes() as $key => $value) {
              ?>
                  <li class="questionDraggable" data-type="<?php echo $key ?>">
                      <a class="clearfix questionPreview" href="javascript:;"  data-toggle='modal' data-target='#questionPreview' data-preview="<?php echo Yii::app()->getModule('survey')->assetsUrl . '/images/question-preview/'.$key.'.png'; ?>" data-title="<?php echo $value ?>" data-desc="<?php echo isset($coformDesc[$key]) ? $coformDesc[$key] : '#todo' ?>">
                      <div id="questionDraggablePreview" class="d-none inputPreview"><img src="<?php echo Yii::app()->getModule('survey')->assetsUrl . '/images/question-preview/'.$key.'.png'; ?>" alt="setting-ico"></div>
                      <i id="questionDraggableIcon" class="" style="vertical-align: middle;"><img style="width: 30px; height: 30px;" src="<?php if(isset($coformQuestionIcons[$key])) echo Yii::app()->getModule('survey')->assetsUrl . '/images/questions-icon/'.$coformQuestionIcons[$key]; else echo Yii::app()->getModule('survey')->assetsUrl . '/images/gear.png '; ?>" alt="setting-ico"></i>
                      <span class="nav-text"><?php echo $value ?></span>
                      </a>
                  </li>
              <?php } ?>
          </ul>
        </div>
      </div>
      <div class="st-btn-container right-bottom">
        <div class="st-button-main"><i class="fa fa-gear fa-3x" aria-hidden="true"></i></div>
      </div>
    </div>
  </div>
<?php
  }
?>

<!-- Modal -->
<div class="modal preview-modal fade" id="questionPreview" tabindex="-1" role="dialog" aria-labelledby="QuestionPreview">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        
        </div>
    </div>
</div>

<script type="text/javascript">
//to edit costum page pieces 
var configDynForm = <?php echo json_encode(@$this->costum['dynForm']); ?>;
var answerId = <?php echo json_encode((String)$answer['_id']); ?>;
var coFormId = <?php echo json_encode((String)$form["_id"]); ?>;
var mode = <?php echo json_encode($mode); ?>;
var canAdminAnswer = <?php echo json_encode($canAdminAnswer); ?>;
var elTest = <?php echo json_encode($el); ?>;
//information and structure of the form in this page  
var tplCtx = {};
if (typeof costum!="undefined" && notNull(costum)!=false && typeof costum.app !="undefined" && typeof costum.app[location.hash]!="undefined" && typeof costum.app[location.hash].hash!="undefined" && typeof costum.app[location.hash].urlExtra!="undefined"){
  var fullUrl=  costum.app[location.hash].hash + costum.app[location.hash].urlExtra;
  fullUrl=fullUrl.replace(/[/]/g,".");
  
  if(fullUrl.indexOf("#answer.index.id.new")>=0){
    history.replaceState(location.hash, "", "#answer.index.id."+answerId+".mode."+mode);
  }
}

if(location.hash.indexOf("#answer.index.id.new")>=0){
    history.replaceState("#answer.index.id.new", "", "#answer.index.id."+answerId+".mode."+mode);
}
var strListMode = "";
var strLowListMode = "";
var strArrowTop = "";

function reloadWizard(callback = null){
    coInterface.showLoader("#wizardcontainer");
    var reloadWizardData = {
        "answerId" : "<?php echo (string)$answer["_id"] ?>",
        "formId" : "<?php echo (string)$form["_id"] ?>",
        "forms": <?php echo json_encode($forms) ?>,
        "active" : <?php echo json_encode("all") ?>,
        "color1" : <?php echo json_encode(@$this->costum["colors"]["dark"]) ?>,
        "color2" : <?php echo json_encode(@$this->costum["colors"]["pink"]) ?>,
        "canEdit" : <?php echo json_encode($canEdit) ?>,
        "canEditForm" : <?php echo json_encode($canEditForm) ?>,
        "canAdminAnswer" : <?php echo json_encode($canAdminAnswer) ?>,
        "showForm" : <?php echo json_encode($showForm) ?>,
        "mode" : <?php echo json_encode($mode) ?>,
        "showWizard": true,
        "wizid": <?php echo json_encode($wizardUid) ?>,
        "contextId" : <?php echo json_encode(@$contextId) ?>,
        "contextType" : <?php echo json_encode(@$contextType) ?>,
        "standalone" : true
    }

    if (reloadWizardData.contextType == "") {
        delete reloadWizardData.contextType;
    }
    if (reloadWizardData.contextId == "") {
        delete reloadWizardData.contextId;
    }

    ajaxPost(
        "#wizardcontainer",
        baseUrl+"/survey/answer/reloadwizard",
        reloadWizardData,
        function(){
            if(typeof callback == "function")
                callback();
        }
    ,"html");
}

    function unlockStep(step){
      if(notNull(form['subForms']) && form['subForms'].indexOf(step) > 0){
        $.each(form['subForms'] , function(index , value){
          if(index < form['subForms'].indexOf(step) + 1 ){
            $('#'+value+'stepper a').addClass("done");
            $('#'+value+'stepper a').attr("onclick" , "showStepForm('#"+value+"')");
          }
        });
        showStepForm("#"+form['subForms'][form['subForms'].indexOf(step)]);
      }
    }

jQuery(document).ready(function() {
    mylog.log("render","modules/survey/views/tpls/forms/formWizard.php");
    $('.st-actionContainer').launchBtn({defaultState: true, documentClick: true});

    stPanelInterval = setInterval(function() {
      // if($(".st-panel").length < 1) {
        $(".st-panel").show().animate({opacity: 1}, 300)
        clearInterval(stPanelInterval)
      // }
    }, 700)


    
    if(typeof pageProfil != "undefined" && typeof pageProfil.form != "undefined" && pageProfil.form != null){

        if(mode == "w")
        strListMode = '<a href="javascript:;" data-id="'+answerId+'" data-mode="r" data-form="'+form._id.$id+'" class="btnAnswer btn btn-primary pull-right"><i class="fa fa-eye"></i> '+tradForm.readOnly+'</a>';
        if(mode == "r")
            strListMode = '<a href="javascript:;" data-id="'+answerId+'" data-mode="w" data-form="'+form._id.$id+'" class="btnAnswer btn btn-primary pull-right">'+trad.update+'</a>';
        if(canAdminAnswer){
            strListMode += '<a href="javascript:;" data-form="'+form._id.$id+'" data-id="'+form._id.$id+'" class="btnForm btn btn-primary pull-right"><i class="fa fa-cog"></i> '+tradForm.configure+'</a>';
        }
    } else {
        strArrowTop= '<a class="topButton btn btn-primary tooltips"><i class="fa fa-chevron-up" data-toggle="tooltip" data-placement="left" data-original-title="'+tradForm.startOfForm+'"></i></a>';
        if(mode == "w"){
          strListMode = '<a href="#answer.index.id.'+answerId+'.mode.r" class="lbh btn btn-primary pull-right">Lecture Seule</a>';
          strLowListMode = '<a href="#answer.index.id.'+answerId+'.mode.r" class="lbh btn btn-primary tooltips" data-toggle="tooltip" data-placement="left" data-original-title="'+tradForm.readOnly+'"><i class="fa fa-eye"></i></a>';
        }  
        if(mode == "r"){
            strListMode = '<a href="#answer.index.id.'+answerId+'.mode.w" class="lbh btn btn-primary pull-right"><i class="fa fa-pencil"></i> '+trad.update+' </a>';
            strLowListMode = '<a href="#answer.index.id.'+answerId+'.mode.w" class="lbh btn btn-primary tooltips" data-toggle="tooltip" data-placement="left" data-original-title="'+trad.update+'"><i class="fa fa-pencil"></i></a>';
        }    
    }

<?php // if (empty($parentForm["type"]) || ($parentForm["type"] == "aap" && $parentForm == "aapConfig")){ ?>
    $("#modeSwitch").html(strListMode);

  if(strLowListMode!="" && strArrowTop!=""){
      $(document).on('scroll',function(){
        var scrollTop = $(window).scrollTop();
        if (scrollTop > 100) {
            $("#bottomModeSwitch").html(strLowListMode);
            coInterface.bindLBHLinks();
        }
        else{
           $("#bottomModeSwitch").empty(); 
        }

        if(scrollTop > 500){
           $("#arrowTop").html(strArrowTop);
           coInterface.bindLBHLinks();
        }else{
           $("#arrowTop").empty();
        }
        
      });
  }
  <?php // } ?>

    $("#arrowTop").off().on("click",function(){
            scrollintoDiv("wizardLinks",2000);
        }); 

    
    if(typeof pageProfil != "undefined" && typeof pageProfil.form != "undefined" && pageProfil.form != null){
        pageProfil.form.events.answers(pageProfil.form);
    }   

    $(".questionPreview").on("click", function() {
      var that = this;
      $("#questionPreview .modal-content").empty();
      $("#questionPreview .modal-content").append(`
        <div class="col-xs-12 content p-0">
          <div class="modal-header bg-dark text-center">
              <button type="button" class="close color-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">${$(that).attr("data-title")}</h4>
          </div>
          <div class="modal-body">
            <img class="preview-img" src="${$(that).attr("data-preview")}"></img>
            <div class="col-xs-12">
              <h5>Description</h5>
              <p>${$(that).attr("data-desc")}</p>
            </div>
          </div>
        </div>
      `);

      var contentHeight = setInterval(function() {
        const contentHeight = $(".modal.preview-modal .content").height();
        $("#questionPreview .modal-content").css({height: contentHeight});
        clearInterval(contentHeight);
      }, 200)
      $("#questionPreview").modal('show')
    })
});
</script>

