<?php
$ignore = array('_file_', '_params_', '_obInitialLevel_' ,'ignore');
$params = array_diff_key(get_defined_vars() + array_flip($ignore), array_flip($ignore));

if(!$isAsking){
    HtmlHelper::registerCssAndScriptsFiles(array(
        '/css/standalone.css'
    ), Yii::app()->getModule(Survey::MODULE)->getAssetsUrl());

    $bannerTitleStandalone = @$parentForm["name"];
    $bannerFooterStandalone = "<p style='font-size:23px'>".ucfirst(Yii::t("common", "the holder"))." : <a href='#page.type.".$context["collection"].".id.".(string)$context["_id"]."' class=' lbh-preview-element'>".ucfirst($context["name"])."</a></p>";
    if (isset($blockCms["bannerTitleStandalone"])){
        $bannerTitleStandalone = $blockCms["bannerTitleStandalone"];
    }

    $bgImg = Yii::app()->getModule("survey")->assetsUrl."/images/oceco_background.png";
    $bgColor = !empty($parentForm["standaloneBgColor"]) && !empty($parentForm["useBackgroundColor"]) ? $parentForm["standaloneBgColor"] : "--aap-primary-color";
    if(!empty($parentForm["useBackgroundImg"])){
        $bgArr = Document::getListDocumentsWhere(
            array(
                "id"=> (string)$parentForm["_id"],
                "type"=>'form',
                "subKey"=>'bgStandalone',
            ), "image"
        );
        if(!empty($bgArr))
        $bgImg = $bgArr[0]["imagePath"];
    }
?>
<style>
    .bg-contain{
        background-image: url("<?= $bgImg ?>");
        background-color: url("<?= $bgColor ?>");
    }
</style>
    
    <div class="col-xs-12 bg-contain">
        <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-1 contain-offset no-padding">
            <div class="banner-section">
                <div class="row">
                    <?php
                    if(isset($parentForm["params"]["standAlone"]["leftImg"]) && !filter_var(    $parentForm["params"]["standAlone"]["leftImg"], FILTER_VALIDATE_BOOLEAN)){
                        ?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h1 class="header-title"><?= $bannerTitleStandalone  ?></h1>
                        </div>
                        <?php
                    }else{
                        if(!empty($parentForm["useBannerImg"]) && $isajax){
                            echo $this->renderPartial("survey.views.tpls.forms.formBanner" , array(
                                "isajax"=>$isajax,
                                "mode" => $mode,
                                "isStandalone" => $isStandalone,
                                "parentForm" =>$parentForm,
                                "canAdminAnswer" => $canAdminAnswer
                            ));
                        }else if ($isAap && empty($parentForm["useBannerImg"])){ ?>
                            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                                <h1 class="header-title padding-left-10"><?= $bannerTitleStandalone  ?></h1>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12" >
                                <img class="img-responsive" src="<?php echo Yii::app()->getModule("survey")->assetsUrl ?>/images/aap_banner2.jpg" alt="Book Icon">
                            </div>
                        <?php }else if (!$isAap && empty($parentForm["useBannerImg"])){ ?>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h1 class="header-title"><?= $bannerTitleStandalone  ?></h1>
                            </div>
                        <?php } 
                    }
                    ?>
                </div>
            </div>
            <hr style = "margin: 0">
            <div class="body-section">
                <div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">
                    <?php
                    $color1 = "#E63458";
                    if(isset($this->costum["cms"]["color1"]))
                        $color1 = $this->costum["cms"]["color1"];
                    ?>

                    <?php $formSmallSize =  12; ?>
                    <div class="col-md-12 col-lg-<?php echo $formSmallSize?> no-padding "><br/>

                        <div class="col-xs-12 margin-top-20 coFormbody" id="coFormbody">

                        </div>
                    </div>


                </div>
                <div class="col-xs-12">
                    <?= $bannerFooterStandalone  ?>
                </div>
            </div>
            <hr>
        </div>
    </div>
    
    <div id="btn-copylink">
        <a href="javascript:;" class="copylink tooltips" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?php echo Yii::t("common", "Copy form's link") ?>"><i class="fa fa-copy"></i> </a>
    </div>

<?php
}
?>
<script type="text/javascript">
    if(typeof coForm_FormWizardParams == "undefined") {
        var coForm_FormWizardParams = <?php echo json_encode($params); ?>;
    }
    jQuery(document).ready(function() {
        if(coForm_FormWizardParams.isNew && coForm_FormWizardParams.isAsking){
            let oneresponse = <?= isset($parentForm["oneAnswerPerPers"]) && $parentForm["oneAnswerPerPers"] ? $parentForm["oneAnswerPerPers"] : "false" ?>;
            let addResponseBtn = "";
            if (!oneresponse){
                addResponseBtn = `<button class="btn btn-success btn-block" id="new-anwers-popup"><i class="fa fa-plus"></i>${ucfirst(trad["Generate a new answer"])}</button>`;
            }
            let dialogNewAns = bootbox.dialog({
                message: `<div class="">
                            <p>${tradForm['Please check on the list below if you have already added an answer. If not, click on button below']}</p>
                            <table class="table">
                                <tbody id="user-exists-answers" style="font-size:15px">

                                </tbody>
                            </table>
                            ${addResponseBtn}
                        </div>`,
                closeButton: false
            });
            dialogNewAns.on('shown.bs.modal', function(e){
                dialogNewAns.attr("id", "new-anwers-dialog");
                $("#new-anwers-dialog").css("background-color","rgb(0 0 0 / 54%)");
                var html = '';
                var i = 1;
                $.each(coForm_FormWizardParams.userAnswers,function(k,v){
                    v.customTitle = "";
                    if(exists(v.answers) && exists(v.answers.aapStep1) && exists(v.answers.aapStep1.titre))
                        v.customTitle = "<b>"+ucfirst(v.answers.aapStep1.titre)+"</b> /";
                    html+= `<tr id="ans-${k}">
                                <td>
                                    ${i+"-  "}
                                </td>
                                <td>
                                    <a href="javascript:;" onclick="window.open('${location.href.split("#")[0]}/#answer.answer.id.${k}.form.${coForm_FormWizardParams.parentForm._id.$id}.mode.w.standalone.true')" >${v.customTitle} ${ucfirst(moment.unix(v.created).format('dddd,Do MMMM YYYY h:mm:ss A'))} </a>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-xs btn-danger delete-answer-popup" data-id="${k}"><i class="fa fa-times"></i></button>
                                </td>
                            </tr>`;
                    i++;
                })
                html += '';
                $('#user-exists-answers').html(html);
                $("#new-anwers-popup").off().on("click",function(){
                    window.open(location.href.replace("ask.true",""),"_self")
                    location.reload();
                });
                $(".delete-answer-popup").off().on('click',function(){
                    var idans = $(this).data("id");
                    bootbox.dialog({
                        title: trad.confirm,
                        message: "<span class='text-red bold'><i class='fa fa-warning'></i>"+trad.actionirreversible+"</span>",
                        buttons: [{
                            label: "Ok",
                            className: "btn btn-primary pull-left",
                            callback: function () {
                                getAjax("", baseUrl + "/survey/co/delete/id/" + idans, function (data) {
                                    if (data.result) {
                                        toastr.success(trad.deleted);
                                        $("#ans-"+idans).hide(500)
                                    }
                                }, "html");


                            }
                        },
                            {
                                label: "Annuler",
                                className: "btn btn-default pull-left",
                                callback: function () { }
                            }
                        ]
                    });
                })
            });
        }

        if(coForm_FormWizardParams.isAsking == false) {
            ajaxPost("#coFormbody", baseUrl + '/survey/answer/answer/id/' + coForm_FormWizardParams["answer"]["_id"]["$id"] + '/form/' + coForm_FormWizardParams["parentForm"]["_id"]["$id"],
                {url: window.location.href},
                function (data) {
                }, "html");
        }
    });

</script>
