<style>
	.label-<?php echo $kunik ?>{
		border-bottom:1px dashed;
		border-top:1px dashed;
		display:block;
		text-align: center;
		background-color: #ddd;
	}
</style>
<div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
	<label for="<?php echo $kunik ?>" class="label-<?php echo $kunik ?>">
		<h2 style="color:<?php echo (!empty($titleColor)) ? $titleColor : "black"; ?>">
			<?php echo $label ?>
		</h2>
		<h5 class="no-margin"><i class="fa fa-angle-down"></i></h5>
	</label><br/>
</div>



