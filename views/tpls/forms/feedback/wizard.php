<style>
    .sectionStepTitle {
        background: #fff;
        display: inline-block;
        font-weight: 600;
        line-height: 40px;
        margin: 0;
        padding: 0 30px;
        position: relative;
        text-transform: uppercase;
        border: 3px solid <?php echo $color2; ?>;
        border-radius: 25px;
    }

    .title-header:before {
        border-top: 3px solid <?php echo $color2; ?>;
        content: "";
        left: 0;
        position: absolute;
        right: 0;
        top: 50%;
    }

</style>
<?php
$defaultColor = "#354C57";
$structField = "structags";

$keyTpl = "wizard";

$paramsData = ["title" => "", "color" => "", "background" => "", "nbList" => 2, "defaultcolor" => "#354C57", "tags" => "structags"];

if (isset($this->costum["tpls"][$keyTpl]))
{
    foreach ($paramsData as $i => $v)
    {
        if (isset($this->costum["tpls"][$keyTpl][$i])) $paramsData[$i] = $this->costum["tpls"][$keyTpl][$i];
    }
}

if (!empty($parentForm['hasStepValidations']) && is_numeric($parentForm['hasStepValidations']) && !(is_int($parentForm['hasStepValidations'])))
    $parentForm['hasStepValidations'] = intval($parentForm['hasStepValidations']);

//var_dump($parentForm);exit;
//var_dump($canEdit);
if (isset($parentForm["wizardIntroTpl"])) echo $this->renderPartial($parentForm["wizardIntroTpl"]);

if (!empty($parentForm["type"]) && ($parentForm["type"] == "aap" || $parentForm["type"] == "aapConfig"))
{

    if ($parentForm["type"] == "aap")
    {
        $configEl = PHDB::findOneById(Form::COLLECTION, $parentForm["config"]);
    }
    elseif ($parentForm["type"] == "aapConfig")
    {
        $configEl = $parentForm;
    }

    foreach ($configEl["subForms"] as $idConfigEl => $valueConfigEl)
    {
        $forms[$idConfigEl] = $valueConfigEl;
        if (isset($parentForm["params"][$idConfigEl])){
            $forms[$idConfigEl]["params"] = $parentForm["params"][$idConfigEl] ;
        }

    }



    $form["subForms"] = array_keys($configEl["subForms"]);
}

if (!empty($parentForm["type"]) && $parentForm["type"] == "aapConfig")
{
    $formsubForms = [];
    foreach ($configEl["subForms"] as $ksubF => $vsubF)
    {
        if (isset($vsubF["active"]) && $vsubF["active"] == true) // check if aapCOnfig subForms (step) is active and push to subforms array
            $formsubForms[] = $ksubF;
    }
}
else
{
    if (!empty($form["subForms"]))
    {
        $formsubForms = $form["subForms"];
    }
}

if (!empty($parentForm["projectGeneration"]) && $parentForm["projectGeneration"] == "onStart"){

    if (isset($parentForm["mapping"])) {
        if (isset($parentForm["mapping"]["proposition"])) {
            $namemap = explode(".", $parentForm["mapping"]["proposition"]);
        }
        if (isset($parentForm["mapping"]["description"])) {
            $descrmap = explode(".", $parentForm["mapping"]["description"]);
        }
        if (isset($parentForm["mapping"]["depense"])) {
            $depensemap = explode(".", $parentForm["mapping"]["depense"]);
        }
    }

    if (!empty($namemap) && isset($answer[$namemap[0]][$namemap[1]][$namemap[2]])) {
        $namepro = $answer[$namemap[0]][$namemap[1]][$namemap[2]];
    }
    if (!empty($descrmap) && isset($answer[$descrmap[0]][$descrmap[1]][$descrmap[2]])) {
        $descrpro = $answer[$descrmap[0]][$descrmap[1]][$descrmap[2]];
    }
    if (!empty($depensemap) && isset($answer[$depensemap[0]][$depensemap[1]][$depensemap[2]])) {
        foreach ($answer[$depensemap[0]][$depensemap[1]][$depensemap[2]] as $keyf => $valuef) {
            if (isset($valuef["financer"])) {
                foreach ($valuef["financer"] as $keyi => $valuei) {
                    if (isset($valuei["id"])) {
                        array_push($financer, $valuei["id"]);
                    }
                }
            }
        }
        if (isset($answer[$descrmap[0]][$descrmap[1]][$descrmap[2]])) {
            $descr = $answer[$descrmap[0]][$descrmap[1]][$descrmap[2]];
        }
    }


}


    echo "<div class='col-xs-12'>";
    echo '<h2 class="text-center sectionStepTitle" style="color:'.$color1.'" >'.@$forms[$parentForm["subForms"][0]]["name"].'</h2><br>';
    echo '<div class="markdown" style="color:'.$color1.'" >'.@$forms[$parentForm["subForms"][0]]["info"].'</div>';
    if(!isset($preview) && $canEditForm === true){
        echo "<div class='text-center col-xs-12'>";
        echo '<a href="javascript:;" class="editFormBtn" data-id="' . $forms[$parentForm["subForms"][0]]["_id"] . '" data-formid="' . $parentForm["subForms"][0] . '"><i class="fa fa-pencil text-dark"></i></a> ';
        echo '<a href="javascript:;" class="deleteFormBtn" data-id="' . $forms[$parentForm["subForms"][0]]["_id"] . '" data-formid="' . $parentForm["subForms"][0] . '"><i class="fa fa-trash text-red"></i></a> ';
        echo "</div>";
    }

    //echo "<div class='markdown'>";
    echo "<div class=''>";
    echo $this->renderPartial("survey.views.tpls.forms.formSection", ["contextId" => @$contextId, "contextType" => @$contextType, "parentForm" => $parentForm, "formId" => $parentForm["subForms"][0], "form" => $forms[$parentForm["subForms"][0]], "wizard" => true, "answer" => $answer, "mode" => @$mode, "showForm" => $showForm, "canEdit" => $canEdit, "canEditForm" => @$canEditForm, "canAdminAnswer" => @$canAdminAnswer, "el" => $el], true);
    echo "</div>";
    echo "</div>";

?>
</div>

<script type="text/javascript">
    var formsData = <?php echo (!empty($forms)) ? json_encode($forms) : "null"; ?>;

    var formInputsHere = formInputs;

    var isNew = <?php echo (!empty($isNew) ? $isNew : "false") ?>;

    function showStep(id){
        $(".stepperContent").addClass("hide");
        $(id).removeClass("hide");
        //alert(location.hash.split(".")[0]+"."+location.hash.split(".")[1]+"."+location.hash.split(".")[2]+".subview."+$(".stepperContent:not(.hide").attr("id"));
        history.pushState(null, null, location.hash.split(".")[0]+"."+location.hash.split(".")[1]+"."+location.hash.split(".")[2]+".subview."+$(".stepperContent:not(.hide").attr("id"));
    }

    function showStepForm(id){
        if (!($(id).hasClass("lkdstep"))) {
            $(".stepDesc, .editFormBtn, .deleteFormBtn").addClass("hidden-xs");
            $(id + "-stepDesc, " + id + "-editFormBtn, " + id + "-deleteFormBtn").removeClass("hidden-xs");

            $("#explain").addClass("hide");
            $(".sectionStep").addClass("hide");
            $(id).removeClass("hide");
            localStorage.setItem("wizardStep_<?php echo (string)@$answer["_id"] ?>", id);
            var topScrollForm = ($("#customHeader").length > 0) ? $("#customHeader").offset().top : 0;
            $('html, body').animate({scrollTop: topScrollForm}, 500);
        }
    }

    var canShowStepList = <?php echo json_encode(@$canShowStepList) ?>;

    var haveProject = <?php echo ((!empty($parentForm["projectGeneration"]) && $parentForm["projectGeneration"] == "onStart") ? "true" : "false") ?>;

    var namepro = "<?php echo ((!empty($parentForm["projectGeneration"]) && $parentForm["projectGeneration"] == "onStart" && !empty($namemap[2])) ? $namemap[2] : "") ?>";

    var descrpro = "<?php echo ((!empty($parentForm["projectGeneration"]) && $parentForm["projectGeneration"] == "onStart" && !empty($descrmap[2])) ? $descrmap[2] : "") ?>";

    if(!notNull(answerObj)){
        var answerObj = <?php echo (!empty($answer) ? json_encode($answer) : "{}" ); ?>;
    }

    var idproans =  "<?php echo (!empty($answer["project"]["id"]) ? $answer["project"]["id"] : "null"); ?>";

    jQuery(document).ready(function() {

        $('#modeSwitch').hide();
        if (notNull(namepro) && namepro != "" && notNull(idproans) && idproans != ""){
            $("#"+namepro).unbind().blur( function( event ) {
                var tthis = $(this);
                event.preventDefault();
                //toastr.info('saving...'+$(this).attr("id"));
                //$('#openForm118').parent().children("label").children("h4").css('color',"green")
                if (notNull(answerObj)) {

                    var projectgn = {
                        collection: "projects",
                        id: idproans,
                        path: "name",
                        value :  $("#"+namepro).val()
                    };

                    dataHelper.path2Value( projectgn , function(params) {
                        toastr.success('Nom du projet associé modifié');
                    } );
                }
            });

            $("#"+descrpro).unbind().blur( function( event ) {
                var tthis = $(this);
                event.preventDefault();
                //toastr.info('saving...'+$(this).attr("id"));
                //$('#openForm118').parent().children("label").children("h4").css('color',"green")
                if (notNull(answerObj)) {

                    var projectgn = {
                        collection: "projects",
                        id: idproans,
                        path: "shortDescription",
                        value :  $("#"+descrpro).val()
                    };

                    dataHelper.path2Value( projectgn , function(params) {
                        toastr.success('Description courte du projet associé modifié');
                    } );
                }
            });
        }

        mylog.log("render","survey.views.tpls.forms.wizard");
        var urlPreview      = location.hash;
        mylog.log("urlPreview",urlPreview.indexOf("preview"));
        $.each($(".markdown"), function(k,v){
            descHtml = dataHelper.markdownToHtml($(v).html());
            $(v).html(descHtml);
        });

        showStepForm('#aapStep1');

        if ($("#explain").length && !($("#explain").hasClass("hide")) ){
            $(".sectionStep").addClass("hide");
        }


        $('.editFormBtn').off().click( function(){
            tplCtx.id = $(this).data("id");
            tplCtx.collection = "forms";
            dyFObj.openForm( subformDF , null, formsData[$(this).data("formid")] )
        });
        $('.deleteFormBtn').off().click( function(){
            tplCtx.id = $(this).data("id");
            bootbox.dialog({
                title: trad.confirmdelete,
                message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                buttons: [
                    {
                        label: "Ok",
                        className: "btn btn-primary pull-left",
                        callback: function() {
                            getAjax("",baseUrl+"/survey/form/delete/id/"+tplCtx.id,function(res){
                                if(res.result)
                                    toastr.success("Le form été supprimée avec succès");
                                else
                                    toastr.error(res.msg);
                                urlCtrl.loadByHash(location.hash);
                            },"html");
                        }
                    },
                    {
                        label: "Annuler",
                        className: "btn btn-default pull-left",
                        callback: function() {}
                    }
                ]
            });
        });
    });

    var subformDF = {
        jsonSchema : {
            title : "Configurer cette section",
            description : "Tout est possible, faut juste poser les bonnes questions",
            icon : "fa-question",
            properties : {
                name : { label : "Nom du formulaire"},
                info : { label : "Description",
                    inputType: "textarea",
                    markdown: true
                }
            },
            save : function (formData) {
                mylog.log('save tplCtx formData', formData)

                delete formData.collection ;
                tplCtx.path = "allToRoot";
                tplCtx.value = {};
                tplCtx.value.name = formData.name;
                tplCtx.value.info = formData.info;

                mylog.log("save tplCtx",tplCtx);

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) {
                        urlCtrl.loadByHash(location.hash);
                    } );
                }

            }
        }
    };
</script>
