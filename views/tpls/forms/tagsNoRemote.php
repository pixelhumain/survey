<?php
$inpClass = "";
if($saveOneByOne)
    $inpClass = " "; 

$paramsData = [ "options" => [ ] ];

if( isset($parentForm["tags"]) ) 
    $paramsData["tags"] =  $parentForm["tags"];

if(!isset($options) && isset($parentForm["tags"]))
    $options = $parentForm["tags"];

if($mode == "r" || $mode == "pdf"){ ?>
    <div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
        <label for="<?php echo $kunik ?>">
            <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label ?></h4>
        </label><br/>
        <?php echo (!empty($options) && !empty($options[$answers])) ? $options[$answers] : "" ; ?>
    </div>
<?php 
}else{

$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$key."' class='previewTpl edit".$key."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";
?>
<style>
	#s2id_<?php echo $kunik ?> .select2-search-field input{
		display: none;
		padding-bottom: 10px;
	}
	#s2id_<?php echo $kunik ?> .select2-choices .select2-search-choice{
		margin:8px 5px !important;
	}
</style>

<div class="form-group col-xs-12 col-md-6 no-padding">
	<label for="<?php echo $key ?>">
        <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
            <?php echo $label.$editQuestionBtn.$editParamsBtn ?>
        </h4>
    </label>

    <?php if( !isset($options) ) 
        echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> THIS FIELD HAS TO BE CONFIGURED FIRST ".$editParamsBtn."</span>"; 
        else { 
            
            ?>
            <!-- <select multiple class="select2Input <?php echo $inpClass ?>" id="<?php echo $key ?>" data-form='<?php echo $form["id"] ?>' required>
            	<option value="">Selectionner</option>
            	<?php /*foreach ($options as $k => $v) {
            		echo '<option value="'.$k.'" '.((!empty($answers) && (string)$k == $answers ) ? "selected" : "").' >'.$v.'</option>';
            	}*/ ?>
            </select> -->
			<input type="text" class="select2Input <?php echo $inpClass ?>" id="<?php echo $kunik ?>" data-form='<?php echo $form["id"] ?>' data-key='<?php echo $key ?>'>
    
	<?php } if(!empty($info)){ ?>
    	<small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
    <?php } ?>
</div>
<?php } ?>
<?php if($mode != "pdf"){ ?>
<script type="text/javascript">
sectionDyf.<?php echo $key ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
jQuery(document).ready(function() {
    mylog.log("render form input","/modules/costum/views/tpls/forms/select.php");
	function matchStart (term, text) {
		if (text.toUpperCase().indexOf(term.toUpperCase()) == 0) {
			return true;
		}
		return false;
	}
	
	$("#<?= $kunik ?>").val("<?= is_array($answers) ? implode(",",$answers) : "" ?>").select2(
		{
			"tags": Array.isArray(sectionDyf.<?php echo $key ?>ParamsData["tags"]) ? sectionDyf.<?php echo $key ?>ParamsData["tags"] : [],
			allowClear : false,
			"tokenSeparators": [','],
			minimumResultsForSearch: -1
		}
	).on('change',()=>{
		var tplCtx = {
			id : "<?php echo (string) $answer["_id"] ?>",
			path : "answers"+"."+$("#<?php echo $kunik ?>").data('form')+"."+$("#<?php echo $kunik ?>").data('key'),
			value : $("#<?= $kunik ?>").val().split(","),
			collection : "<?= Form::ANSWER_COLLECTION ?>"
		}
		dataHelper.path2Value(tplCtx, function () {})
	});
    sectionDyf.<?php echo $key ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo $key ?> config",
            "description" : "Liste de question possible",
            "icon" : "fa-cog",
            "properties" : {
                tags : {
                    inputType : "array",
                    label : "Liste des titres",
                }
            },
            save : function () {  
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $key ?>Params.jsonSchema.properties , function(k,val) { 
                    if(val.inputType == "array")
                        tplCtx.value[k] = getArray('.'+k+val.inputType);
                    else
                        tplCtx.value[k] = $("#"+k).val();
                 });
                mylog.log("save tplCtx",tplCtx);
                
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
                        location.reload();
                    } );
                }

            }
        }
    };

    $(".edit<?php echo $key ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $key ?>Params,null, sectionDyf.<?php echo $key ?>ParamsData);
    });
});
</script>
<?php } ?>