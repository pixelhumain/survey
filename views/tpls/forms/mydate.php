<?php
    $value = (!empty($answers)) ? ' value="'.@$answers.'" ' : '';
    if($mode == "r" || $mode == "pdf"){ ?>
    <div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
        <label for="<?php echo $kunik ?>">
        	<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
        		<?php echo $label ?>
        	</h4>
        </label><br/>
        <?php echo $answers; ?>
    </div>
<?php 
    }else{
?>
    <div class="col-md-6 no-padding">
        <div class="form-group">
            <label for="<?php echo $key ?>">
                <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
                    <?php echo $label.$editQuestionBtn ?>
                </h4>
            </label>
            <br/>
            <input type="date" 
                    class="form-control" 
                    id="<?php echo $key ?>" 
                    aria-describedby="<?php echo $key ?>Help" 
                    placeholder="<?php echo (isset($placeholder)) ? $placeholder : '' ?>" 
                    data-form='<?php echo $form["id"] ?>'  <?php echo @$value ?> 
            />
            <?php if(!empty($info)){ ?>
                <small id="<?php echo $key ?>Help" class="form-text text-muted">
                    <?php echo $info ?>
                </small>
            <?php } ?>
        </div>

	</div>
<?php } ?>

<script type="text/javascript">
// $("#<?php echo $key ?>").unbind().blur(function() {
//    mylog.log("value", $("#<?php echo $key ?>").val()) 
// })
$(document).ready(function() {
    $("#<?php echo $key ?>").unbind().blur(function() {
        var answer = {
            collection : "answers",
            id : answerObj._id.$id,
            path : "answers.<?php echo $form['id'].'.'.$key ; ?>",
            value: $("#<?php echo $key ?>").val()
        };

        mylog.log("date.saveOnblur", answer );

        dataHelper.path2Value( answer , function(params) {
            toastr.success('saved');
        } );
    })
})
    
</script>