<script type="text/javascript">
  
jQuery(document).ready(function() {

 $('.deleteLine').off().click( function(){
      formId = $(this).data("id");
      key = $(this).data("key");
      pathLine = $(this).data("path");
      collection = $(this).data("collection");
      var withreload = $(this).data("withreload");
      if (typeof $(this).data("parentid") != "undefined") {
          var parentfId = $(this).data("parentid");
      }
      bootbox.dialog({
          title: trad.confirmdelete,
          message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
          buttons: [
            {
              label: "Ok",
              className: "btn btn-primary pull-left",
              callback: function() {
                var formQ = {
      				  	value:null,
      				  	collection : collection,
      				  	id : formId,
      				  	path : pathLine
      				  };

            if (typeof parentfId != "undefined") {
                formQ["formParentId"] = parentfId;
            }
				  
				  dataHelper.path2Value( formQ , function(params) { 
						$("#"+key).remove();
            if(typeof formsInputs != "undefined") {
              if(params && params.elt && params.elt.inputs && params.id) {
                formsInputs[params.id] = params.elt.inputs
              }
            }
            if(params && params.elt && params.elt.id)
              if(typeof formInputsfor != "undefined" && formInputsfor[params.elt.id] && formInputsfor[params.elt.id][key.replace(/question/g, "")]) {
                  delete formInputsfor[params.elt.id][key.replace(/question/g, "")]
              }
						if (typeof withreload != "undefined" && withreload == "true" && typeof reloadWizard != "undefined"){
                            reloadWizard(key, "<?php echo @$form["id"] ?>");
                        }

                      //location.reload();
					} );
              }
            },
            {
              label: "Annuler",
              className: "btn btn-default pull-left",
              callback: function() {}
            }
          ]
      });
    });
});
</script>