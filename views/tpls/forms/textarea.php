<style>
	.md-<?= $kunik ?>{
		/* border: 1px solid red; */
		border-radius: 5px;
		padding: 0 22px;
		/* background: #d9534f; */
		color: #fff;
	}
</style>
<?php 
$inpClass = "";
if($saveOneByOne)
	$inpClass = " saveOneByOne";

if($mode == "r" || $mode == "pdf"){ ?>
    <div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
        <label for="<?php echo $kunik ?>"><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label ?></h4></label><br/>
        <div class="markdown-<?php echo $kunik ?>"><?php echo $answers; ?></div>
    </div>
<?php 
} else {
?>
	<div class="form-group col-xs-12 col-md-12 no-padding text-left">
	    <label for="<?php echo $key ?>">
				<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn ?>
				<?php //if($mode == "fa"){
					// if($mode != "pdf"){
					// 	echo '<label class="bg-green checkbox-inline md-'.$kunik.'" > <input type="checkbox" id="md-'.$kunik.'"  style="right: initial;" '.(@$input["markdown"]==true ? "checked":"").'><small class="text-white capitalize">Markdown</small></label>';
					// }
				//} ?>
			</h4>
		</label>
		<br/>
		<?php if(!empty($info)){ ?>
	    	<small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
	    <?php } ?>
	    <br/><textarea class="form-control active-markdown<?= $kunik ?> <?php echo $inpClass ?>" id="<?php echo $key ?>" aria-describedby="<?php echo $key ?>Help" placeholder="<?php echo (isset($placeholder)) ? $placeholder : '' ?>" data-form='<?php echo $form["id"] ?>' ><?php echo (!empty($answers)) ? $answers : "" ?></textarea>
	    
	</div>
<?php } ?>



<?php if($mode != "pdf"){ ?>
<script type="text/javascript">
	jQuery(document).ready(function() {
	    mylog.log("render form input","/modules/costum/views/tpls/forms/textarea.php");
		<?php //if(@$input["markdown"]==true){?>
			dataHelper.activateMarkdown2('.active-markdown<?= $kunik ?>');
		<?php //} ?>
		$('#md-<?= $kunik ?>').change(function() { 
			var tplCtx = {
				id: "<?= isset($input["parentid"])?$input["parentid"]:$form["id"] ?>",
				collection : "<?= Form::INPUTS_COLLECTION ?>",
				path : 'inputs.<?=  $key ?>.markdown',
				format:true
			}
			if (this.checked)
				tplCtx.value=true
			else 
				tplCtx.value=false

			dataHelper.path2Value( tplCtx, function(params) {
				if(params.result)
					toastr.success(trad.save);
				reloadWizard();
            } );
		});

            var descHtml = dataHelper.markdownToHtml($(".markdown-<?php echo $kunik ?>").html(),{
                parseImgDimensions:true,
                simplifiedAutoLink:true,
                strikethrough:true,
                tables:true,
                openLinksInNewWindow:true
            });
            $(".markdown-<?php echo $kunik ?>").html(descHtml);
	});
</script>
<?php } ?>