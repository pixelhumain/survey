<?php
$ignore = array('_file_', '_params_', '_obInitialLevel_' ,'ignore');
$params = array_diff_key(get_defined_vars() + array_flip($ignore), array_flip($ignore));
?>

<div class="tab-content">
    <?php
    if(empty($step)){
        foreach ($steps as $step) {
            if($params["isAap"] && !empty($params["configForm"]["subForms"][$step["step"]]["params"]["haveEditingRules"]) && $params["configForm"]["subForms"][$step["step"]]["params"]["haveEditingRules"]) {
                if (
                    !Form::getAnswerAuthorisation($params["answer"], $params["parentForm"], $params["context"])["canAdminAnswer"] &&
                    !empty($params["configForm"]["subForms"][$step["step"]]["params"]["canEdit"]) &&
                    sizeof(array_intersect(Form::getAnswerAuthorisation($params["answer"], $params["parentForm"], $params["context"])["roles"], $params["configForm"]["subForms"][$step["step"]]["params"]["canEdit"])) <= 0
                ) {
                    $params["mode"] = "r";
                }
            }else{

            }
    ?>
            <div id="step-<?= $step["step"] ?>" class="tab-pane coformstep" data-form="<?= (string)$params["parentForm"]["_id"] ?>" data-answer="<?= (string)$params["answer"]["_id"] ?>"  data-step="<?= $step["step"] ?>" role="tabpanel" aria-labelledby="step-<?= $step["step"] ?>">
                <?php
                    $params["step"] = $step;
                    //echo $this->renderPartial("survey.views.tpls.forms.step" , $params);
                ?>
            </div>
    <?php
        }
    }else{
        if($params["isAap"] && !empty($params["configForm"]["subForms"][$step["step"]]["params"]["haveEditingRules"]) && $params["configForm"]["subForms"][$step["step"]]["params"]["haveEditingRules"]) {
            if (
                !Form::getAnswerAuthorisation($params["answer"], $params["parentForm"], $params["context"])["canAdminAnswer"] &&
                !empty($params["configForm"]["subForms"][$step["step"]]["params"]["canEdit"]) &&
                sizeof(array_intersect(Form::getAnswerAuthorisation($params["answer"], $params["parentForm"], $params["context"])["roles"], $params["configForm"]["subForms"][$step["step"]]["params"]["canEdit"])) <= 0
            ) {
                $params["mode"] = "r";
            }
        }else{

        }
    ?>
            <div id="step-<?= $step["step"] ?>" class="coformstep standalonestep" data-form="<?= (string)$params["parentForm"]["_id"] ?>" data-answer="<?= (string)$params["answer"]["_id"] ?>"  data-step="<?= $step["step"] ?>">
                <?php
                $params["step"] = $step;
                echo $this->renderPartial("survey.views.tpls.forms.step" , $params);
                ?>
            </div>
    <?php
    }
    ?>
</div>
