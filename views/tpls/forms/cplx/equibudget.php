<?php 
	
	$percent = ($totalBudg) ? $totalFin*100/$totalBudg : 0;
	$delta = $totalFin - $totalBudg ;
	$col = "";
	$percol = "primary";
	if($delta > 0){
		$col = "text-green";
		$percol = "success";
	}
	else if($delta < 0 ){
		$col = "text-red";
		$percol = "danger";

	}
 ?>
<div class='col-xs-12 ' id="equilibreBudget">
	<h3 style="color:#16A9B1">EQUILIBRE BUDGETAIRE</h3>
	L'équilibre budgétaire doit être atteint pour pouvoir passer à la phase de contractualisation
	<div class="progress">
	  <div class="progress-bar progress-bar-<?php echo $percol ?>" style="width: <?php echo $percent ?>%">
	    <span class="sr-only"><?php echo $percent ?>% Complete (success)</span>
	  </div>
	</div>
	<table class="table table-bordered table-hover  ">
		<tbody class="">
			<tr>
				<td>BUDGET prévisionnel</td>
				<td><?php echo trim(strrev(chunk_split(strrev($totalBudg),3, ' '))) ?>€</td>
			</tr>
			<tr>
				<td>Financements acquis</td>
				<td><?php echo trim(strrev(chunk_split(strrev($totalFin),3, ' '))) ?> €</td>
			</tr>
			<tr>
				<td>Delta</td>
				<td><?php echo "<span class='".$col."'>".trim(strrev(chunk_split(strrev($delta),3, ' ')))." € </span>" ?> </td>
			</tr>
		</tbody>
	</table>
</div>