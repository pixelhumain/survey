<?php if($answer){ 
	// var_dump($answer["answers"][$form["id"]][$kunik]);
	
	$keyTpl = "finder";


?>


	
		
		
	<?php 
		// $editBtnL = ($canEdit === true && isset($parentForm["params"][$keyTpl.$key])  ) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='answers.".$keyTpl.$key.".' class='add".$keyTpl." btn btn-default'><i class='fa fa-plus'></i> Ajouter un élément </a>" : "";

		// $editBtnL = ($canEdit === true && isset($parentForm["params"][$keyTpl.$key])  ) ? " <a href='javascript:;' class='form-control col-xs-6 selectParent btn-success margin-bottom-10' data-id='parent' data-types='organizations,projects' data-multiple='true' data-open='true'>Ajouter à la liste</a>" : "";

		// <a href='javascript:;' class='form-control col-xs-6 selectParent btn-success margin-bottom-10' data-id='parent' data-types='organizations,projects' data-multiple='true' data-open='true'>Ajouter à la liste</a>
		
		$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

		$elementType=[
					Organization::COLLECTION => "Organization",
					Person::COLLECTION 		 => "Person",
					Event::COLLECTION 		 => "Event",
					Project::COLLECTION 	 => "Project",
					News::COLLECTION 		 => "News",
					City::COLLECTION 		 => "City",
					Thing::COLLECTION 		 => "Thing",
					Poi::COLLECTION 		 => "Poi",
					Classified::COLLECTION   => "Classified",
					Product::COLLECTION 	 => "Product",
					Service::COLLECTION   	 => "Service",
					Survey::COLLECTION   	 => "Survey",
					Bookmark::COLLECTION   	 => "Bookmark",
					Proposal::COLLECTION   	 => "Proposal",
					Room::COLLECTION   	 	 => "Room",
					Action::COLLECTION   	 => "Action",
					Network::COLLECTION   	 => "Network",
					Url::COLLECTION   	 	 => "Url",
					Circuit::COLLECTION   	 => "Circuit",
					Risk::COLLECTION   => "Risk",
					Badge::COLLECTION   => "Badge"
			];
		
		$paramsData = [ 
			"type" => Organization::COLLECTION,
			"filter"   => [],
		    "notSourceKey" => true,
		    "myContacts"   => false,
		    "elementLabel" => "Elément",
		    "field" => "element",
		    "multiple" => true,
		    "addNew" => false,
		    "invite" => false,
		    "linkToAnswer" => false,
		    "singleAnswerPerElement" => false,
		    "msgSingleAnswerPerElement" => "",
		    "editElement" =>false
		];

		if(isset($parentForm["params"][$kunik])){
			foreach ($paramsData as $e => $v) {
		        if ( isset($parentForm["params"][$kunik][$e]) ) {
		            $paramsData[$e] = $parentForm["params"][$kunik][$e];
		        }
		    }
		}

		//var_dump($paramsData);

		// if (isset($blockCms)) {
		//     foreach ($paramsData as $e => $v) {
		//         if (  isset($blockCms[$e]) ) {
		//             $paramsData[$e] = $blockCms[$e];
		//         }
		//     }
		// }
		//var_dump($paramsData);

		
	?>	
	<label>
		<h4><?php echo $label.$editQuestionBtn.$editParamsBtn ?></h4>
	</label>
	

<script type="text/javascript">

var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answer["answers"][$kunik])) ? $answer["answers"][$kunik] : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
var mode = "<?php echo $mode ?>";
var step=<?php echo json_encode($form["id"]) ?>;
var ansKey= <?php echo json_encode($kunik) ?> ;



var answers= <?php echo json_encode( (isset($answer["answers"][$form["id"]][$kunik])) ? $answer["answers"][$form["id"]][$kunik] :null ); ?>;


		saveElementFinder<?php echo $kunik ?>=function(id,name,type){	
			var valData = {
				"id" : id,
				"name" : name,
				"type" : type
			};

			var savedInfo={};
            savedInfo[id]=valData;

            var answer={};

			answer.path = (sectionDyf.<?php echo $kunik ?>ParamsData.multiple=="false") ? "answers.<?= $form["id"] ?>.<?= $kunik ?>" : "answers.<?= $form["id"] ?>.<?= $kunik ?>."+id;
			answer.collection = "answers" ;
	        answer.id = "<?php echo $answer["_id"]; ?>";
	        answer.value = (sectionDyf.<?php echo $kunik ?>ParamsData.multiple=="false") ? savedInfo : valData;

	        // save the element in the answer
	        dataHelper.path2Value(answer , function(params) { 
	           
	            reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
	            //alert("okok");
	            if(typeof stepValidationReload<?php echo $form["id"] ?> == "function"){
                        stepValidationReload<?php echo $form["id"] ?>();   
                }
                toastr.success(trad['saved']);

	        });

	        // link the element to the answer
	        if(sectionDyf.<?php echo $kunik ?>ParamsData.linkToAnswer=="true" || sectionDyf.<?php echo $kunik ?>ParamsData.linkToAnswer==true){
	            answer.path="links."+type+"."+id;
	            answer.value = {"name" : name,"type":type};
	            dataHelper.path2Value(answer , function(params) { 

	            });
	        }



        };    

	finder.addInForm<?php echo $kunik ?> = function(keyForm, id, type, name, img, data,firstLoad=false){
		mylog.log("finder.addInForm", keyForm, id, type, name, img);
		img= (notEmpty(img)) ? baseUrl + img : modules.co2.url + "/images/thumb/default_"+type+".png";

		// --------------------- Finir editElement -----------------------------
		var nameElem=(sectionDyf.<?php echo $kunik ?>ParamsData.editElement==true || sectionDyf.<?php echo $kunik ?>ParamsData.editElement=="true") ?
		'<a href="javascript:;" class="updateElementInfo<?php echo $kunik ?>" data-id="'+id+'" data-type="'+type+'">'+name+'</a>' : name;

		// (sectionDyf.<?php echo $kunik ?>ParamsData.editElement==true || sectionDyf.<?php echo $kunik ?>ParamsData.editElement=="true") ?
		// '<a href=`javascript:;` onclick=`dyFObj.editElement('+type+', '+id+','+null+','+JSON.stringify(costum.typeObj.organizations.dynFormCostum)+');return false;`>'+name+'</a>' : name;
		//img= (img != "") ? img : modules.co2.url + "/images/thumb/default_"+type+".png";
		var str="";
		str="<div class='col-xs-12 element-finder element-finder-"+id+" shadow2 padding-10'>"+
					'<img src="'+ img+'" class="img-circle pull-left margin-right-10" height="35" width="35">'+
					'<span class="info-contact pull-left margin-right-5">' +
						'<span class="name-contact text-dark text-bold">' + nameElem + '</span>'+
						'<br/>'+
						'<span class="cp-contact text-light pull-left">' + trad[((type == "citoyens" ) ? "citizens" : type)]+ '</span>'+
					'</span>';
		if(mode=="w"){			
		str +=			'<button class="bg-red text-white pull-right" style="border: none;font-size: 15px;border-radius: 6px;padding: 5px 10px !important;" onclick="finder.removeFromForm<?php echo $kunik ?>(\''+keyForm+'\', \''+id+'\',\''+type+'\')"><i class="fa fa-times"></i></button>';
		}			
		str+=	"</div>";
		$(".finder-"+keyForm+" .form-list-finder").append(str);

		$(".updateElementInfo<?php echo $kunik ?>").off().on("click",function(){
	        dyFObj.editElement($(this).data("type"),$(this).data("id"),null,costum.typeObj.organizations.dynFormCostum);
		});	

		finder.object[keyForm]={};
		finder.object[keyForm][id]={"type" : type, "name" : name};


		if(notNull(finder.finderPopulation[keyForm]) && notNull(finder.finderPopulation[keyForm][id]) && notNull(finder.finderPopulation[keyForm][id].email)){
			finder.object[keyForm][id].email = finder.finderPopulation[keyForm][id].email;
		}

		if(notNull(finder.roles) && notNull(finder.roles[keyForm])){
			finder.object[keyForm][id].roles = finder.roles[keyForm];
		}

		if(notNull(finder.search) && notNull(finder.search[keyForm]) && notNull(finder.search[keyForm].filterBy)){
			
			mylog.log("filterBy split", split, finder.selectedItems[keyForm][id][finder.search.filterBy], notNull( finder.selectedItems[keyForm][id][finder.search.filterBy]) );
			if(notNull( finder.selectedItems[keyForm][id][finder.search.filterBy] ) ) {
				var fBy = {} ;
				var split = id.split(".");
				fBy[finder.search[keyForm].filterBy] = finder.selectedItems[keyForm][id][finder.search.filterBy] ;
				finder.object[keyForm][split[0]]=Object.assign({}, finder.object[keyForm][id], fBy);
				delete finder.object[keyForm][id];
			}
		}

		var answers= <?php echo json_encode( (isset($answer["answers"][$form["id"]][$kunik])) ? $answer["answers"][$form["id"]][$kunik] :null ); ?>;

		mylog.log("answers",answers);

		// if(notNull(answers) && (typeof answers[id]=="undefined" || Object.keys(answers[id])==0)){
        if((sectionDyf.<?php echo $kunik ?>ParamsData.singleAnswerPerElement=="true" || sectionDyf.<?php echo $kunik ?>ParamsData.singleAnswerPerElement==true) && (sectionDyf.<?php echo $kunik ?>ParamsData.linkToAnswer=="true" || sectionDyf.<?php echo $kunik ?>ParamsData.linkToAnswer==true)){
        	        
        	        var answerId="<?php echo (string)$answer["_id"] ?>";
						ajaxPost(
			                null,
							baseUrl+"/costum/cressreunion/answerlink",
							{form:form._id.$id,
							currentAnswer:answerId,
							linkedElementType:type,
							linkedElementId : id
						    },
							function(data){
								
								var specMsg = (typeof sectionDyf.<?php echo $kunik ?>ParamsData.msgSingleAnswerPerElement!="undefined" && sectionDyf.<?php echo $kunik ?>ParamsData.msgSingleAnswerPerElement!="") ? sectionDyf.<?php echo $kunik ?>ParamsData.msgSingleAnswerPerElement : "Un questionnaire a déjà été rempli et enregistré pour cette structure."
								var dataLinkedAnsw=data;
								// mylog.log("objectkeys",Object.keys(dataLinkedAnsw.data));
								if(Object.keys(dataLinkedAnsw.data).length>0){
								bootbox.dialog({
								            closeButton: false,
								            title: "Une réponse liée à votre structure existe déjà",
								            message: specMsg,
								            buttons: {
								                success:{
								                    label: "Ok",
								                    callback: function(){
								                    	urlCtrl.loadByHash("#welcome");

								                    }
								                }
								            }       
								        });
							    }else if(firstLoad==false){
                                    saveElementFinder<?php echo $kunik ?>(id,name,type);
							    }


							});		
        }						
		else if(firstLoad==false){	
            saveElementFinder<?php echo $kunik ?>(id,name,type);
		}
		
		
	};	

			

	finder.removeFromForm<?php echo $kunik ?> = function(keyForm, id, type){
		//mylog.log("finder.removeFromForm", keyForm, id);
        var nbAnswer=$(".finder-"+keyForm+" .form-list-finder .element-finder").length;

		$(".finder-"+keyForm+" .form-list-finder .element-finder-"+id).remove();
		//delete finder.object[keyForm][id];
        

		var answer={};
		answer.path = nbAnswer>1 ? "answers.<?= $form["id"] ?>.<?= $kunik ?>."+id : "answers.<?= $form["id"] ?>.<?= $kunik ?>";
		answer.collection = "answers" ;
		answer.value="";
	    answer.id = "<?php echo $answer["_id"]; ?>";
	    dataHelper.path2Value(answer , function(params) { 
	            
	            if(typeof stepValidationReload<?php echo $form["id"] ?> == "function") {
                        stepValidationReload<?php echo $form["id"] ?>();
                }
                toastr.success(trad['deleted']);
	        });

	    if(sectionDyf.<?php echo $kunik ?>ParamsData.linkToAnswer=="true" || sectionDyf.<?php echo $kunik ?>ParamsData.linkToAnswer==true){
	            answer.path="links."+type+"."+id;
	            answer.value = "";
	            dataHelper.path2Value(answer , function(params) { 

	            });
	        }
	};

	finder.addSelectedToForm=  function(keyForm, multiple){
		mylog.log("finder.addSelectedToForm", keyForm, multiple);
		var kunik = $("."+keyForm+"finder").parent().data("key");
		kunik="<?= $keyTpl ?>"+kunik ;
		if(Object.keys(finder.selectedItems[keyForm]).length > 0){
			if(!multiple){
				finder.object[keyForm]={};
				$(".finder-"+keyForm+" .form-list-finder").html("");
			}
			$.each(finder.selectedItems[keyForm], function(e, v){
				typeCol=(typeof v.collection != "undefined") ? v.collection : v.type; 
				window["finder"]["addInForm"+kunik](keyForm, e, typeCol, v.name, v.profilThumbImageUrl);
			});
			if(typeof finder.callback[keyForm] != "undefined") finder.callback[keyForm](finder.selectedItems[keyForm]);
		}
	};

	bindAddNew<?php echo $kunik ?> = function(){
		mylog.log("finder bindAddNews");
		$(".add-new-element").off().on("click", function(){
			var filters = $(this).data("filters");
			var field = $(this).data("field");
			mylog.log("filters",filters);
			//filters=JSON.parse(filters);
			//var category = $(this).data("category");
			var type = $(this).data("type");

			var customForm = {
					"beforeBuild" : {
			            "properties" : {
			                        
			            }    
					}

				};
			//mylog.log("category",category);
			
			if(typeof filters!="undefined" &&  typeof filters=="object" && filters!={}){
				mylog.log("filters",filters);
				$.each(filters,function(k,v){
					customForm.beforeBuild.properties[k] = {
						inputType : "hidden",
						label : k,
						value : v,
						class :" form-control"
					};
					if(k=="tags"){
						customForm.beforeBuild.properties[k].inputType="tags";
					}
					else if(k=="source.key"){
						delete customForm.beforeBuild.properties[k];
					}
				});

			}

			
				customForm.afterSave = function(data){
					mylog.log("afterSave reference", data);
					dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
					if(field=="thirdPlace"){
						ajaxPost(
							null,
							baseUrl+"/"+moduleId+"/admin/setsource/action/add/set/reference",
							{id:data.id,type:data.map.collection,sourceKey:"franceTierslieux"},
							function(data){
								mylog.log("reférencé!",data);
								toastr.success("Tiers-lieu référencé sur Ftl");
							}
							
						);

						ajaxPost(
							null,
							baseUrl+"/"+moduleId+"/admin/setsource/action/add/set/reference",
							{id:data.id,type:data.map.collection,sourceKey:"tiersLieux"},
							function(data){
								mylog.log("reférencé!",data);
								toastr.success("Tiers-lieu référencé sur tiersLieux");
							}
							
						);
					}
					
					finder.addInForm<?php echo $kunik ?>(sectionDyf.<?php echo $kunik ?>ParamsData.field,data.map._id.$id,data.map.collection,data.map.name);	

					if(costum!=null){
						if(typeof costum[costum.slug].organizations !="undefined" && typeof costum[costum.slug].organizations.afterSave=="function"){
							costum[costum.slug].organizations.afterSave(data);
						}
					}
					
				};

				mylog.log("customForm",customForm);

				var extendedForm=customForm;

				if(typeof costum!="undefined" && costum!=null && costum.typeObj!="undefined" && costum.typeObj[type]!="undefined" && costum.typeObj[type].dynFormCostum!="undefined") 
					extendedForm=$.extend(true,costum.typeObj[type].dynFormCostum,customForm);
			
			var type = type.slice(0, -1);
		

			mylog.log("extendedForm",extendedForm);
			dyFObj.openForm(type,null,null, null, extendedForm);
			reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
			$(".bootbox").modal('hide');
				
		});



	};

$(document).ready(function() { 
	mylog.log("answers ready",answers);
	mylog.log("paramsData<?php echo $kunik?>",sectionDyf.<?php echo $kunik ?>ParamsData);

		// dyFObj.openFormFinder = function  (type, afterLoad,data, isSub, dynFormCostumIn,mode) { 
		// 	//mylog.clear();
		// 	dyFObj.dynFormCostum = null; //was some times persistant between forms 
		// 	mylog.warn("openForm","--------------- Open Form ",type, afterLoad,data, isSub, dynFormCostumIn);
		// 	$.unblockUI();
		// 	$("#openModal").modal("hide");
			
		// 	mylog.dir(data);
		// 	uploadObj.contentKey="profil"; 
		// 	if(notNull(data)){
		// 		if(typeof data.images != "undefined")
		// 			uploadObj.initList.image=data.images;
		// 		if(typeof data.files != "undefined" )
		// 			uploadObj.initList.file=data.files;

		// 		data = dyFObj.prepData(data);

		// 	}else{
		// 		uploadObj.initList={};
		// 	}
		// 	dyFObj.activeElem = (isSub) ? "subElementObj" : "elementObj";
		// 	dyFObj.activeModal = (isSub) ? "#openModal" : "#ajax-modal";

		// 	//enables setting dynform customization 
		// 	//sample used in costum/views/tpls/wizard
		// 	//dyFObj.dynFormCostum = null;
		// 	if( typeof dynFormCostumIn != "undefined"){
		// 		mylog.warn("openForm", "dynFormCostum",dynFormCostumIn);
		// 		dyFObj.dynFormCostum = dynFormCostumIn;
		// 	} else {
		// 		mylog.warn("openForm", "no dynFormCostum");
		// 	}

		// 	// if(notNull(finder))
		// 	// 	finder.initVar();

		// 	if(notNull(scopeObj))
		// 		scopeObj.selected = {};

		// 	dyFInputs.locationObj.initVar();

		// 	if(dyFObj.unloggedMode || userId)
		// 	{
		// 		if(typeof formInMap != 'undefined')
		// 			formInMap.formType = type;

		// 		if(typeof dyFObj.formInMap != 'undefined')
		// 			dyFObj.formInMap.formType = type;

		// 		dyFObj.getDynFormObj(type, function() { 
		// 			dyFObj.startBuild(afterLoad,data);
		// 		},afterLoad, data, dynFormCostumIn);
		// 	} else {
		// 		dyFObj.openFormAfterLogin = {
		// 			type : type, 
		// 			afterLoad : afterLoad,
		// 			data : data
		// 		};
		// 		toastr.error(tradDynForm.mustbeconnectforcreateform);
		// 		$('#modalLogin').modal("show");
		// 	}
		// };

		finder.searchAndPopulateFinder = function(keyForm, text, typeSearch, multiple){
			mylog.log("finder.searchAndPopulateFinder", keyForm, text, typeSearch, multiple);
			//finder.isSearching=true;

			var dataSearch = {
	        	searchType : typeSearch, 
	        	name: text
	        };

	        if(notNull(finder.search) && notNull(finder.search[keyForm])){
	        	dataSearch = Object.assign({}, dataSearch, finder.search[keyForm]);
	        }
			if(finder.filters[keyForm]){
				dataSearch['filters'] = finder.filters[keyForm];
			}
	  		$.ajax({
				type: "POST",
		        url: baseUrl+"/"+moduleId+"/search/globalautocomplete",
		        data: dataSearch,
		        dataType: "json",
		        success: function(retdata){
		        	mylog.log("retdata",retdata);
		        	// mylog.log("finder.extraParams[keyForm].addNew",finder.extraParams[keyForm].addNew);
		        	mylog.log("etdata.results.length",retdata.results.length);
			        	mylog.log("finder.invite[keyForm]",finder.invite[keyForm]);
		        	if(!retdata){
		        		toastr.error(retdata.content);
		        	} else {
			        	if(retdata.results.length == 0 && (finder.invite[keyForm] == true || finder.invite[keyForm]=="true" || (notNull(finder.invite[keyForm]) && typeof finder.invite[keyForm]=="object"))){
			        		$("#form-invite").removeClass("hidden");
			        		$("#list-finder-selection").addClass("hidden");

			        		var search =  "#finderSelectHtml #populateFinder" ;
			        		var id = "#finderSelectHtml #form-invite" ; 
			        		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
							if(emailReg.test( $(search).val() )){
								$(id+' #inviteEmail').val( $(search).val());
								var nameEmail = $(search).val().split("@");
								$(id+" #inviteName").val(nameEmail[0]);
							}else{
								$(id+" #inviteName").val($(search).val());
								$(id+" #inviteEmail").val("");
							}

							var invitor = (typeof userConnected !="undefined" && userConnected!=null) ? userConnected.name : "un.e citoyen.ne";

							finder.invite[keyForm]={};
							finder.invite[keyForm].callback=function(data){
								mylog.log("invite callback", data)
			        			var listInvite={};
			        			listInvite.invites={};
			        			listInvite.invites[data.id] = { 
			        				name : data.name,
			        				profilThumbImageUrl : parentModuleUrl + "/images/thumb/default_"+data.type+".png",
			        				mail : data.mail,
			        				msg : data.msg
			        			} 
			        			var params = {
			        				parentId : costum.contextId,
			        				parentType : costum.contextType,
			        				listInvite : listInvite
			        			};
			        			
			        			 ajaxPost("",baseUrl+'/'+moduleId+"/link/multiconnect",params,function(data){
			        			 		var invited=(typeof trad.keyForm!="undefined") ? trad.keyForm : keyForm;
			        			 		toastr.success(invited +' bien invité.e');
			        			 	},
			        			 	function(data){
			        			 		toastr.error(trad.somethingwrong);
			        			 	}
			        			); 	
			        		};

		        		} else if (retdata.results.length == 0 && (finder.extraParams[keyForm].addNew=="true" || finder.extraParams[keyForm].addNew==true)){
		        				$("#form-invite").addClass("hidden");
		        				$("#list-finder-selection .population-elt-finder").hide();	
		        				var addNew='<a href="javascript:;" data-type="'+finder.typeAuthorized[keyForm][0]+'" data-field="'+keyForm+'" data-filters='+JSON.stringify(finder.search[keyForm].filters)+' class=" add-new-element text-red"><i class="fa fa-more "></i> <span>Ajouter un(e) '+keyForm+'</span></a>';
		        				if($('#finderSelectHtml .add-new-element').length == 0) {
		        					$("#list-finder-selection").prepend(addNew);
		        					var inputName=$("#"+keyForm).parent().parent().parent().data("key");
		        					inputName="<?php echo $keyTpl ?>"+inputName;
		        					window["bindAddNew"+inputName]();
		        				}else{
		        					$('#finderSelectHtml .add-new-element').removeClass("hidden");
		        				}	
		        				
		        		}
		        		
		        		else{
		        			$("#form-invite").addClass("hidden");
		        			$('#finderSelectHtml .add-new-element').addClass("hidden")
		        			$("#list-finder-selection").removeClass("hidden");
			        		finder.populateFinder(keyForm, retdata.results, multiple);
		        		}
		  			}
				}	
			});
		};

		var filters = {};
		if(typeof sectionDyf.<?php echo $kunik ?>ParamsData.filter!="undefined" && typeof sectionDyf.<?php echo $kunik ?>ParamsData.filter=="object"){
			$.each(sectionDyf.<?php echo $kunik ?>ParamsData.filter,function(k,v){
				filters[v.attributeName]=v.valueName;
			});
		}

		dyFObj.buildInputField("#question<?php echo $key ?>",sectionDyf.<?php echo $kunik ?>ParamsData.field,{
						"inputType" : "finder",
                        "label" : "",
                        "initMe":false,
                        "buttonLabel" : "Rechercher puis sélectionner ou ajouter mon Espace du Faire",
                        "placeholder":"Saisir le nom de mon Espace du Faire",
                        "initContext" : false,
                        "initType": [sectionDyf.<?php echo $kunik ?>ParamsData.type],
                        "initBySearch": true,
                        "initContacts" : false,
                        "openSearch" :true,
                        "search": {
                        	"filters":filters,
                        	"notSourceKey":sectionDyf.<?php echo $kunik ?>ParamsData.notSourceKey
                        },
                        "multiple" : sectionDyf.<?php echo $kunik ?>ParamsData.multiple,
                        "invite" : sectionDyf.<?php echo $kunik ?>ParamsData.invite,
                        "topClass" : "col-xs-10 col-xs-offset-1",
                        "extraParams" : {
                        	"addNew" : sectionDyf.<?php echo $kunik ?>ParamsData.addNew
                        }
                        // "filters" : {
                        // 	"category":"network",
                        // 	"notSourceKey":true
                        // }
                    } ,{},null
        );

		dyFObj.initFieldOnload[sectionDyf.<?php echo $kunik ?>ParamsData.field+"Finder"]();

        if(mode!=="w"){
        	setTimeout(
        		$("#question<?php echo $key ?> .selectParent").hide()
        		,1000);
        }
		// setTimeout($("#populateFinder").off().on("change",function(){
		//     	mylog.log("finder.showPanel keyup new");
		//     	if($(this).val().length < 3){
		//     		finder.filterPopulation($(this).val());
		//     	}else{
		//     		if(notNull(open) && open){
		//     			finder.filterPopulation($(this).val());
		//     			finder.searchAndPopulateFinder(keyForm,$(this).val(), typeSearch, multiple);
		//     		}else{
		//     			finder.filterPopulation($(this).val());
		//     		}
		//     	}
		//     }),5000);



		

		var answers= <?php echo json_encode( (isset($answer["answers"][$form["id"]][$kunik])) ? $answer["answers"][$form["id"]][$kunik] :null ); ?>;

    	if(typeof answers!="undefined" && notNull(answers) ){
           	$.each(answers,function(k,v){
           		mylog.log("answer elem",v);
           		if(typeof sectionDyf.<?php echo $kunik ?>ParamsData.field!="undefined" && notNull(sectionDyf.<?php echo $kunik ?>ParamsData.field)){
           			finder.addInForm<?php echo $kunik ?>(sectionDyf.<?php echo $kunik ?>ParamsData.field,v.id,v.type,v.name,null,null,true);
           		}	
            });
        }    



	
	sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "<?php echo $kunik ?> config",
	        "icon" : "fa-cog",
	        "properties" : {
	            type : {
	                inputType : "select",
	                label : "Définir un type d'élément",
	                options :  <?php echo json_encode($elementType) ?>
	            },
	            filter : {
	                inputType : "lists",
                    label : "Filtres appliqués",
                    entries: {
                        attributeName: {
                            type:"select",
                            label:"Attribut",
                            options : ["category","type","tags","source.key"],
                            class:"col-lg-5"
                        },
                        valueName: {
                            type:"text",
                            label:"valeur",
                            class:"col-lg-5"
                        }
                    }
	            },
	            notSourceKey : dyFInputs.checkboxSimple(sectionDyf.<?php echo $kunik ?>ParamsData.notSourceKey, "notSourceKey", 
            										 {"onText" : trad.yes,
            										  "offText": trad.no,
            										  "onLabel" : "Recherche globale",
            										  "offLabel": "Recherche sourcée",
            										  "labelText" : "Recherche globale"}
				),
	            myContacts : dyFInputs.checkboxSimple(sectionDyf.<?php echo $kunik ?>ParamsData.myContacts, "myContacts", 
            										 {"onText" : trad.yes,
            										  "offText": trad.no,
            										  "onLabel" : "Recherches dans mes contacts",
            										  "offLabel": "Ne pas chercher dans mes contacts",
            										  "labelText" : "Rechercher parmi mes contacts"}
				),
	           elementLabel : {
	                inputType : "text",
	                label : "Label de l'élément recherché"
	            },
	            field : {
	                inputType : "text",
	                label : "Nom du champ"
	            },
	            multiple : dyFInputs.checkboxSimple(sectionDyf.<?php echo $kunik ?>ParamsData.multiple, "multiple", 
            										 {"onText" : trad.yes,
            										  "offText": trad.no,
            										  "onLabel" : "Ajouter plusieurs éléments",
            										  "offLabel": "N'ajouter qu'un seul élément",
            										  "labelText" : "Permettre l'ajout de plusieurs éléments"}
            	),
            	addNew : dyFInputs.checkboxSimple(sectionDyf.<?php echo $kunik ?>ParamsData.addNew, "addNew", 
            										 {"onText" : trad.yes,
            										  "offText": trad.no,
            										  "onLabel" : "Ajouter de nouveaux éléments",
            										  "offLabel": "Ne pas ajouter de nouveaux éléments",
            										  "labelText" : "Permettre l'ajout de nouveaux éléments"}
            	),
            	invite : dyFInputs.checkboxSimple(sectionDyf.<?php echo $kunik ?>ParamsData.invite,"invite",
            										 {"onText" : trad.yes,
            										  "offText": trad.no,
            										  "onLabel" : "Afficher les invitations",
            										  "offLabel": "Ne pas afficher les invitations",
            										  "labelText" : "Permettre l'affichage des invitations"}
            	),
            	linkToAnswer : dyFInputs.checkboxSimple(sectionDyf.<?php echo $kunik ?>ParamsData.linkToAnswer,"linkToAnswer",
            										 {"onText" : trad.yes,
            										  "offText": trad.no,
            										  "onLabel" : "Lier l'élément à la réponse",
            										  "offLabel": "Ne pas l'élément à la réponse",
            										  "labelText" : "Créer un lien entre l'élément et la réponse"}
            	),
            	singleAnswerPerElement : dyFInputs.checkboxSimple(sectionDyf.<?php echo $kunik ?>ParamsData.singleAnswerPerElement,"singleAnswerPerElement",
            										 {"onText" : trad.yes,
            										  "offText": trad.no,
            										  "onLabel" : "Réponse unique par élément (impossible d'aller plus loin de le formulaire",
            										  "offLabel": "Plusieurs réponses possibles par élément",
            										  "labelText" : "N'autoriser qu'une seule réponse associée à un même élément"}
            	),
            	msgSingleAnswerPerElement : {
            		inputType : "textarea",
                    label : "Contenu du message d'alerte signifiant l'existance d'une réponse relative à un élement"
            	},
            	editElement : dyFInputs.checkboxSimple(sectionDyf.<?php echo $kunik ?>ParamsData.editElement,"editElement",
            										 {"onText" : trad.yes,
            										  "offText": trad.no,
            										  "onLabel" : "L'édition de l'élément est possible",
            										  "offLabel": "L'édition de l'élément n'est pas autorisée",
            										  "labelText" : "Edition de élément"}
            	)




	     //        "category"   => null,
		    // "notSourceKey" => true,
		    // "myContacts"   => false,
		    // "elementLabel" => "Elément"
		    // "field" => "element"
	        },
	        save : function (data) {  
	        	mylog.log("save sectionDyf",data);
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	            	if(k=="filter"){
	        		    var filters = {};

	        		    $.each(data.filter, function(index, va){
	        		        var filter = {attributeName: va.attributeName, valueName: va.valueName};
	        		        filters["filter"+index] = filter;
	        		    });

	        		    tplCtx.value[k] = filters;
	        		}
	        		else{
	        		tplCtx.value[k] = $("#"+k).val();
		        		if(tplCtx.value[k]=="true") {
		        			tplCtx.value[k]=true;
		        		}
		        		if(tplCtx.value[k]=="false") {
		        			tplCtx.value[k]=false;
		        		}
		        	}	
	        		
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
	                    reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
	                    //urlCtrl.loadByHash(location.hash);
	                } );
	            }

	    	}
	    }
	};


    mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

    //adds a line into answer

    <?php if( isset($parentForm["params"][$kunik]['type']) ) { ?>


    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( "<?php echo $parentForm["params"][$kunik]['type']; ?>",null, <?php echo $kunik ?>Data[$(this).data("key")]);
    });
    <?php } ?>

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        //if no params config on the element.costum.form.params.<?php echo $keyTpl ?>
        //then we load default values available in forms.inputs.<?php echo $keyTpl ?>xxx.params
        //mylog.log(".editParams",sectionDyf.<?php echo $keyTpl ?>Params,calData);
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    
});
</script>
<?php } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>