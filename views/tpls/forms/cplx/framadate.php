<style>
    .input-pref {
        width: 6em;
        margin: 6px;
        display: initial;
    }
    .text-center {
        text-align: center;
    }
    .mb-2 {
        padding-bottom: 2em;
        margin-bottom: 4em;
    }
    .my-form-input {
        width: 100%;
        border-width: 0px 0px 1px 0px !important;
        border-color: #DADADA !important;
        font-size: 13px;
    }
    .my-th {
        min-width: 95px;
        font-size: 14px;
        vertical-align: inherit !important;
    }
</style>
<?php
    $value = (!empty($answers)) ? @$answers : new stdClass;
    $allformanswers = PHDB::find(Form::ANSWER_COLLECTION, array("form" => (string)$parentForm['_id']));
    $myValueTest = array(
        "value" => array("valuedate0" => "11/01/2022", "valuedate1" => "12/01/2022"),
        "subvalue" => array(array("subValue" => "ito n value 1", "parent" => "valuedate0"), array("subValue" => "ito n value 1", "parent" => "valuedate1"))
    );
    if($mode == "r" || $mode == "pdf") { ?>
    <div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
        <label>
        	<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
        		<?php echo $label ?>
        	</h4>
        </label><br/>
        <div id="readMode">

        </div>
    </div>
<?php
    } else { 
?>
        <div class="col-md-6 no-padding">
            <div class="form-group">
                <label>
                    <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
                        <?php echo $label.$editQuestionBtn ?>
                    </h4>
                </label>
                <br/>
                <div id="dateFram">
                </div>
                <div id="btnDate" class="text-center">
                    <a class="btn btn-xs btn-success addDateBlock" id="addDateBlock" data-idparent="dateFram" data-id="" href="javascript:;">
                        <i class="fa fa-plus"></i> Ajouter date
                    </a>
                </div>
                <div id="resultFram">

                </div>
            </div>
        </div>
<?php } ?>
<!-- <script src="survey/assets/js/inputs/framadate.js"></script> -->
<script type="text/javascript">
    $(document).ready(function() {
        var answer = <?php echo json_encode($value); ?>;
        var mode = <?php echo json_encode($mode); ?>;
        // mylog.log("value", <?php echo json_encode($allformanswers); ?>);
        
        var allAnswers = Object.values(<?php echo json_encode($allformanswers); ?>);
        if(allAnswers.length > 0) {
            // mylog.log('mis reponses', allAnswers);
            // mylog.log("input id <?php echo $form['id'].'.'.$key ; ?>");
            var answerOfFramadate = [];
            <?php
                foreach($allformanswers as $elem) {
                    if(isset($elem['answers'])) {
                        if(isset($elem['answers'][$form['id']])) {
                            if(isset($elem['answers'][$form['id']][$key])) { ?>
                                var oneUser = <?php echo json_encode(PHDB::find(Person::COLLECTION, array('_id' => new MongoId($elem['user'])), array("name", "username", "slug"))); ?>;
                                answerOfFramadate.push({
                                    user: Object.values(oneUser)[0],
                                    answer: <?php echo json_encode($elem['answers'][$form['id']][$key]); ?>
                                });
                                
                            <?php }
                        }
                    }
                }
            ?>
            /* var answerOfFramadate = allAnswers.reduce(function(result, item, index) {
                if(item['answers']) {
                    if(item['answers']['<?php echo $form['id'] ; ?>']) {
                        if(item['answers']['<?php echo $form['id'] ; ?>']['<?php echo $key ; ?>']) {
                            var oneUser = <?php echo json_encode($userTest = PHDB::find(Person::COLLECTION, array('_id' => new MongoId("61c042ea4a1d9e7885695cb6")), array("name", "username", "slug"))); ?>;
                            return result.concat({
                                user: oneUser,
                                answer: item['answers']['<?php echo $form['id'] ; ?>']['<?php echo $key ; ?>']
                            })
                        }
                    }
                }
                return result
            }, []); */
            mylog.log('value pre a manipuler', answerOfFramadate);
            /* var header = [];
            if(answerOfFramadate.length > 0) {
                answerOfFramadate.forEach((item, index) => {
                    if(item.answer.value) {
                        const parentKey = Object.keys(item.answer.value);
                        Object.values(item.answer.value).forEach((itemVal, indexVal) => {
                            // const indexParentFinded = header.findIndex(elem => elem.parentValue == itemVal);
                            // mylog.log("parent", itemVal);
                            if(item.answer.subValue.length > 0) {
                                const subValue = item.answer.subValue.filter(elemSubVal => elemSubVal.parent == parentKey[indexVal]);
                                // mylog.log("subvalue filtered", subValue);
                                if(subValue.length > 0) {
                                    subValue.forEach(itemSubVal => {
                                        const indexFinded = header.findIndex(elemSubValue => elemSubValue.subValue == itemSubVal.subValue && elemSubValue.parentValue == itemVal);
                                        if(indexFinded > -1) {
                                            header[indexFinded].count += 1
                                        } else {
                                            header.push({
                                                parentValue: itemVal,
                                                subValue: itemSubVal.subValue,
                                                count: 1
                                            })
                                        }
                                    })
                                    
                                }
                            }
                        })
                    }
                    
                });
                mylog.log("header", header)
            }
            var headerHtml = '', bodyHtml = '',footerHtml = '';
            answerOfFramadate.forEach((item, index) => {
                if(item.user) {
                    header.forEach((itemHeader, indexH) => {
                        const indexParentFinded = Object.values(item.answer.value).findIndex(elemParent => elemParent == itemHeader.parentValue);
                        var indexSubFinded = -1;
                        if(indexParentFinded > -1) indexSubFinded = item.answer.subValue.findIndex(elemSub => elemSub.subValue == itemHeader.subValue && itemHeader.parentValue == item.answer.value[elemSub.parent]);
                        if(index == 0) {
                            if(indexH == 0) {
                                headerHtml = `<thead><tr><td style="min-width: 100px"></td>`;
                                footerHtml = `<tfoot><tr><td class='my-th'>Total<br>${answerOfFramadate.length} Votant</td>`
                            }

                            headerHtml += `<td class='my-th'>${itemHeader.parentValue}<br> ${itemHeader.subValue}</td>`;


                            footerHtml += `<td class="my-th">${itemHeader.count}</td>`;

                            if(indexH == header.length - 1) {
                                headerHtml += '</tr></thead>';
                                footerHtml += '</tr></tfoot>'
                            }
                        }
                        
                        if(indexH == 0) bodyHtml += `<tr><td>${item.user.name}</td>`;

                        if(indexSubFinded > -1) bodyHtml += `<td><i class="fa fa-check"></i></td>`;
                        else bodyHtml += '<td></td>';

                        if(indexH == header.length - 1) bodyHtml += '</tr>';
                    });
                    if(index == answerOfFramadate.length -1) {
                        const blockRes = `
                            <table class="table table-bordered">
                                ${headerHtml}
                                <tbody>${bodyHtml}</tbody>
                                ${footerHtml}
                            </table>
                        `;
                        $('#resultFram').append(blockRes)
                        
                    }
                }
            }); */
        }
        function createDateBlock(idUnik) {
            $(
                `<div id="dateContainer${idUnik}" class="mb-2">
                    <div class="col-xs-12" id="blockdate${idUnik}">
                        <div id='dateinput${idUnik}' class='col-xs-10'>
                            <input type='date' id='date_${idUnik}' data-id="date_${idUnik}" class="my-form-input date-from-value"/>
                        </div>
                        <div id='blockoption${idUnik}' class='col-xs-2'>
                            <a class="btn btn-xs btn-danger removeBlock" data-id="dateContainer${idUnik}" href="javascript:;">
                                <i class="fa fa-trash"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12" id="blockPref${idUnik}">
                        <div class="col-xs-10" id="inputsPref${idUnik}">
                            <input type="time" class="input-pref form-control" id="inputPref0_${idUnik}" data-inputparent="date_${idUnik}" data-parent="parent"/>
                        </div>
                        <div class="col-xs-2" style="padding-top: 8px">
                            <a class="btn btn-xs btn-danger removeElement" id="inputsPref${idUnik}Remove" data-idparent="inputsPref${idUnik}" data-key="${idUnik}" data-id="inputPref0_${idUnik},inputPref1_${idUnik}" href="javascript:;">
                                <i class="fa fa-minus"></i>
                            </a>
                            <a class="btn btn-xs btn-success addElement" id="inputsPref${idUnik}Add" data-idparent="inputsPref${idUnik}" data-key="${idUnik}" data-id="inputPref0_${idUnik},inputPref1_${idUnik}" href="javascript:;">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>    
                `
            ).appendTo("#dateFram");
            var idBlock = $("#addDateBlock").attr("data-id").split(',');
            if(idBlock[0] == '') idBlock.length = 0;
            idBlock.push(`dateContainer${idUnik}`);
            $("#addDateBlock").attr("data-id", idBlock.toString());
        }

        function retrieveDateBlock(idUnik, value, subValue) {
            var block = `<div id="dateContainer${idUnik}" class="mb-2">
                            <div class="col-xs-12" id="blockdate${idUnik}">
                                <div id='dateinput${idUnik}' class='col-xs-10'>
                                    <input type='date' id='date_${idUnik}' data-id="date_${idUnik}" class="my-form-input date-from-value" value="${value}"/>
                                </div>
                                <div id='blockoption${idUnik}' class='col-xs-2'>
                                    <a class="btn btn-xs btn-danger removeBlock" data-id="dateContainer${idUnik}" href="javascript:;">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-xs-12" id="blockPref${idUnik}">
                                <div class="col-xs-10" id="inputsPref${idUnik}">`;
            var dataId = "";
            if(subValue.length == 0) {
                block += `</div>
                            <div class="col-xs-2" style="padding-top: 8px">
                                <a class="btn btn-xs btn-danger removeElement" id="inputsPref${idUnik}Remove" data-idparent="inputsPref${idUnik}" data-key="${idUnik}" data-id="${dataId}" href="javascript:;">
                                    <i class="fa fa-minus"></i>
                                </a>
                                <a class="btn btn-xs btn-success addElement" id="inputsPref${idUnik}Add" data-idparent="inputsPref${idUnik}" data-key="${idUnik}" data-id="${dataId}" href="javascript:;">
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                            </div>
                        </div>`
            } else {
                for(var i = 0; i < subValue.length; i++) {
                    block += `<input type="time" class="input-pref form-control" id="inputPref${i}_${idUnik}" value="${subValue[i].subValue}" data-inputparent="date_${idUnik}" data-parent="parent"/>`;
                    dataId += `inputPref${i}_${idUnik}`;
                    if(i === subValue.length - 1) {
                        block += `</div>
                                <div class="col-xs-2" style="padding-top: 8px">
                                    <a class="btn btn-xs btn-danger removeElement" id="inputsPref${idUnik}Remove" data-idparent="inputsPref${idUnik}" data-key="${idUnik}" data-id="${dataId}" href="javascript:;">
                                        <i class="fa fa-minus"></i>
                                    </a>
                                    <a class="btn btn-xs btn-success addElement" id="inputsPref${idUnik}Add" data-idparent="inputsPref${idUnik}" data-key="${idUnik}" data-id="${dataId}" href="javascript:;">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>    
                        `
                    } else dataId += ","
                }
            }
            
            $(block).appendTo("#dateFram");
            var idBlock = $("#addDateBlock").attr("data-id").split(',');
            if(idBlock[0] == '') idBlock.length = 0;
            idBlock.push(`dateContainer${idUnik}`);
            $("#addDateBlock").attr("data-id", idBlock.toString());
        }

        function buildResult(answerObject) {
            var block = `<table class="table table-bordered" style="width: 50%">
                            <tr>
                                <th>Date</th>
                                <th>Preferences</th>
                            </tr>`;
            var dataId = "";
            for(var [index, [key, value]] of Object.entries(Object.entries(answerObject.value))) {
                block += `<tr>
                            <td>${value}</td>`;
                var subValue = answerObject.subValue.filter(item => { return item.parent == key});
                if(subValue.length > 0) {
                    block += '<td>'
                    for(var i = 0; i < subValue.length; i++) {
                        block += `${subValue[i].subValue}`;
                        if(i === subValue.length - 1) block += '</td>';
                        else block += '<br>'
                    }
                } else {
                    block += `<td>Aucun pref a afficher</td>`
                }
                block += `</tr>` 
            }
            
            $(block).appendTo("#readMode");
        }

        if(mode === 'r' || mode === 'pdf') {
            if(Object.keys(answer).length > 0) buildResult(answer)
        } else {
            if(Object.keys(answer).length == 0) {
                for(var i = 0; i < 1; i++) {
                    createDateBlock(i)
                }   
            } else {
                for(var [index, [key, value]] of Object.entries(Object.entries(answer.value))) {
                    retrieveDateBlock(index, value, answer.subValue.filter(item => { return item.parent == key}))
                    // mylog.log(key, ":", value, ',', index);
                    // mylog.log()
                }
            }

            $('.addDateBlock').click(function() {
                var idBlock = $(this).attr('data-id').split(',');
                if(idBlock[0] === '') {
                    idBlock.length = 0;
                    createDateBlock(0);
                } else {
                    createDateBlock(idBlock[idBlock.length - 1].slice(-1)*1+1);
                }
            });

            $('#dateFram').on('click', '.removeBlock', function() {
                var id = $(this).attr("data-id");
                $(`#${id}`).remove();
                var idBlock = $('#addDateBlock').attr("data-id").split(',');
                var index = idBlock.indexOf(id);
                index > -1 ? idBlock.splice(index, 1) : "";
                $(`#addDateBlock`).attr("data-id", idBlock.toString())
            });

            $('#dateFram').on('click', '.removeElement', function() {
                var id = $(this).attr("data-id").split(',');
                if(id.length > 0){
                    $(`#${id[id.length - 1]}`).remove();
                    id.pop();
                    $(this).attr("data-id", id.toString());
                    $(`#${$(this).attr("data-idparent")}Add`).attr("data-id", id.toString())
                }
            });

            $('#dateFram').on('click', '.addElement', function() {
                var id = $(this).attr("data-id").split(',');
                keyUnik = $(this).attr("data-key");
                $(
                    `<input type="time" class="input-pref form-control" id="inputPref${id.length}_${keyUnik}" data-inputparent="date_${keyUnik}" data-parent="parent"/>
                `).appendTo(`#${$(this).attr("data-idparent")}`);
                var idEntry = $(`#${$(this).attr("data-idparent")}Remove`).attr("data-id");
                id.push(`inputPref${id.length}_${keyUnik}`);
                $(this).attr("data-id", id.toString());
                $(`#${$(this).attr("data-idparent")}Remove`).attr("data-id", id.toString());
            });
        
            $('#dateFram').blur('DOMSubtreeModified').bind('DOMSubtreeModified', function() {
                // toastr.info('modif');
                var res = [];
                var jsonValue = $('input.date-from-value').map(function() {
                    var self = this;
                    $(`input[data-inputparent='${$(this).attr('data-id')}']`).map(function() {
                        var subSelf = this;
                        return res.push({
                            subValue: $(subSelf).val(),
                            parent: $(self).attr('data-id')
                        })
                    }).get();
                    return {
                        [$(this).attr('data-id')] : $(this).val()
                    }
                }).get();
                mylog.log("value before", jsonValue);
                var valueUtil = jsonValue.reduce((obj, item) => (obj[Object.keys(item)[0]] = Object.values(item)[0], obj), {})
                mylog.log("value", valueUtil);
                var fromModel = {
                    value : valueUtil,
                    subValue: res
                }
                mylog.log("subvalue", res);
                var answer = {
                    collection : "answers",
                    id : answerObj._id.$id,
                    path : "answers.<?php echo $form['id'].'.'.$key ; ?>",
                    value: {
                        value : valueUtil,
                        subValue: res
                    }
                };

                dataHelper.path2Value( answer , function(params) {
                    mylog.log("answer sended", answer);
                    mylog.log("params", params);
                    toastr.success('saved');
                } );
            });
        }



    })
</script>