<style type="text/css">
    .form-control{
        padding: .5em !important;
    }
</style>

<?php if($answer){ ?>

    <div class="col-md-6">

        <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
            <?php echo $label.$editQuestionBtn; ?>
        </h4>

        <?php echo $info ?>

        <?php 

            $email = "";

            if(isset($answer["answers"][$form["id"]]["email"])){
                $email = $answer["answers"][$form["id"]]["email"];
            }else if($answer["user"]==$_SESSION["userId"]){
                $email = $_SESSION["userEmail"];
            }else{
                $email = "";
            }

        ?>
        
        <br>
        <div class="form-group">
            <?php if($mode!="r"){ ?>
                <input type="text" id="<?= $key ?>" data-form="<?= $form["id"]?>" value="<?php echo $email ?>" class="form-control saveOneByOne">  
            <?php }else{ 
                echo $email;
             } ?>
                    
        </div>
    </div>

    <script type="text/javascript">
        
        $(document).ready(function(){
            let kunik = "<?= $kunik ?>";
            
            //adds a line into answer
            $(".add<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
            });

            $(".edit<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
            });
        });

    </script>
    <?php } else {
        //echo "<h4 class='text-red'>evaluation works with existing answers</h4>";
    } ?>