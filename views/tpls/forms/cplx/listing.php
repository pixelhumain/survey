
<?php if($answer){ ?>
    <style type="text/css">
        .responselisting:before{
            content: '\f0a9';
            margin-right: 15px;
            font-family: FontAwesome;
            color: #d9534f;
        }
    </style>

    <div class="form-group">
        <?php
            # Initialize parameters data
            $paramsData = [
                "defaultInputNumber" => 3,
                "placeholder" => "Contenu"
            ];

            # Set parameters data
            if( isset($parentForm["params"][$kunik]) ){
                foreach ($paramsData as $e => $v) {
                    if (  isset($parentForm["params"][$kunik][$e]) ) {
                        $paramsData[$e] = $parentForm["params"][$kunik][$e];
                    }
                }
            }
            
            $editParamsBtn = "";

            $editParamsBtn = ($editQuestionBtn!="") ? " <a href='javascript:;' 
                data-id='".$parentForm["_id"]."' 
                data-collection='".Form::COLLECTION."' 
                data-path='params.".$kunik."' 
                class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'>
                <i class='fa fa-cog'></i> 
            </a>" : "";
        ?>

        <h4 class="padding-20" style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
            <?php echo $label.$editQuestionBtn.$editParamsBtn; ?>
        </h4>
        <?php 
        $info=(isset($info) &!empty($info))?$info."<br>":"";
        echo $info; ?>
        <div class="form-horizontal padding-20">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8 col-xs-12">
                        <div id="inputListing<?= $kunik ?>"></div>
                        <?php if($mode=="w"){ ?>
                        <a role="button" class="btn btn-success bg-light padding-left-20 padding-right-20 addLine<?= $kunik ?>" data-target="inputListing<?= $kunik ?>">
                            <i class="fa fa-plus-circle"></i> AJOUTER
                        </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script type="text/javascript">
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
        var formId=<?php echo json_encode($formId) ?>;
        mylog.log("params listing <?= $kunik?>",sectionDyf.<?php echo $kunik ?>ParamsData);


        $(document).ready(function(){
            bindAutoSave<?= $kunik ?> = function(){
                $("#inputListing<?= $kunik ?> .saveOneByOne").unbind().blur( function( event ) {
                    var tthis = $(this);
                    mylog.log("autosave listing");
                    //event.preventDefault();
                    //toastr.info('saving...'+$(this).attr("id"));
                    //$('#openForm118').parent().children("label").children("h4").css('color',"green")
                    if( notNull(answerObj)){

                        var answer = {
                            collection : "answers",
                            id : answerObj._id.$id,
                            path : "answers."+$(this).data("form")+"."+$(this).attr("id")
                        };

                        if(answerObj.form)
                            answer.path = "answers."+$(this).data("form")+"."+$(this).attr("id");           
                          
                        if($(this).attr("type") == "checkbox")
                            answer.value = $(this).is(":checked");
                        else
                            answer.value = $(this).val().trim();
                          
                        mylog.log("saveOneByOne",$(this).attr("id"), answer );
                          
                        dataHelper.path2Value( answer , function(params) {
                            toastr.success('Mise à jour enregistrée');
                            saveLinks(answerObj._id.$id,"updated",userId);
                            // if (typeof ${stepValidationReload}+formId !== "undefined") {
                            //     ${stepValidationReload}+formId();
                            // }
                        } );

                        
                    } else {
                        toastr.error('answer cannot be empty, on saveOneByOne!');
                    }
                });
            };

            
            let inputNb = {};
            let defaultInputNumber = "<?php echo $paramsData['defaultInputNumber'] ?>";
            let kunik = "<?php echo $kunik ?>";
            let userAnswer = null;
            if(answerObj["answers"] && answerObj["answers"]["<?php echo $form["id"]?>"]){
                userAnswer = answerObj["answers"]["<?php echo $form["id"]?>"];
            }
            
            createQuadrant(userAnswer);
            
            $(".addLine<?= $kunik ?>").click(function(){
                let index = inputNb["inputListing<?= $kunik ?>"]+1;
                inputNb["inputListing<?= $kunik ?>"] = index;
                let input = `<div class="form-group">
                                <div class="col-xs-12">
                                    <input type="text" 
                                        data-form='<?php echo $form["id"] ?>' 
                                        id="${kunik}.${index}"
                                        class="form-control ${kunik+index} saveOneByOne inputList" 
                                        placeholder="`+sectionDyf.<?php echo $kunik ?>ParamsData.placeholder+` `+($("#inputListing<?= $kunik ?> .inputList").length+1)+`">
                                </div>
                            </div>`;
                $("#inputListing<?= $kunik ?>").append(input);
                bindAutoSave<?= $kunik ?>();
                let interval = window.setInterval(function(){
                    if($("."+kunik+index).length!=0){ //DOM is much more efficient
                        window.clearInterval(interval);
                        $("."+kunik+index).focus();
                    }
                    mylog.log("Interval");
                }, 100)
            });

            $(document).on("keypress", ".inputList", function(e){
                if(e.keyCode == 13) {
                    //$(this).off().blur();
                    let index = inputNb["inputListing<?= $kunik ?>"]+1;
                    inputNb["inputListing<?= $kunik ?>"] = index;
                    let input = `<div class="form-group">
                                    <div class="col-xs-12">
                                        <input type="text" 
                                            data-form='<?php echo $form["id"] ?>' 
                                            id="${kunik}.${index}"
                                            class="form-control ${kunik+index} saveOneByOne inputList" 
                                            placeholder=""
                                            >
                                    </div>
                                </div>`;
                    $("#inputListing<?= $kunik ?>").append(input);
                    
                    let interval = window.setInterval(function(){
                    if($("."+kunik+index).length!=0){ //DOM is much more efficient
                        window.clearInterval(interval);
                        $("."+kunik+index).focus();
                    }
                    mylog.log("Interval");
                }, 100)
                }
            });

            $(".saveOneByOne").on("keypress", function(e){
                if(e.keyCode == 13) {
                    //$(this).off().blur();
                    let index = inputNb["inputListing<?= $kunik ?>"]+1;
                    inputNb["inputListing<?= $kunik ?>"] = index;
                    let input = `<div class="form-group">
                                    <div class="col-xs-12">
                                        <input type="text" 
                                            data-form='<?php echo $form["id"] ?>' 
                                            id="${kunik}.${index}"
                                            class="form-control ${kunik+index} saveOneByOne inputList" 
                                            placeholder=""
                                            >
                                    </div>
                                </div>`;
                    $("#inputListing<?= $kunik ?>").append(input);
                    let interval = window.setInterval(function(){
                    if($("."+kunik+index).length!=0){ //DOM is much more efficient
                        window.clearInterval(interval);
                        $("."+kunik+index).focus();
                    }
                    mylog.log("Interval");
                }, 100)
                }
            });

            $(document).on("blur", '.inputList', function(event){
                event.preventDefault();
                if($(this).val()==""){
                    $(this).parent().parent().remove();
                }
            });

            function createQuadrant(userAnswer){
                let defaultLength = isNaN(defaultInputNumber)?3:defaultInputNumber;
                let hasAnswered = false;
                
                if(userAnswer && typeof userAnswer[kunik]!="undefined"){
                    defaultLength = notNull(userAnswer[kunik])? Object.keys(userAnswer[kunik]).length : defaultLength;
                    hasAnswered = true;
                }

                for (let index = 0; index < defaultLength; index++) {
                    if(userAnswer && typeof userAnswer[kunik]!="undefined" && notNull(userAnswer[kunik])){
                        if(typeof userAnswer[kunik][Object.keys(userAnswer[kunik])[index]] == "undefined"){
                            hasAnswered = false;
                        }
                    }
                    var multiList = (typeof defaultLength!="undefined" && defaultLength>1) ? (index+1).toString() : "" ; 
                    let input = `<div class="form-group">
                                <div class="col-md-12">
                                    <?php if($mode=="r"){ ?>
                                        <div class="responselisting form-control-static">${(hasAnswered)?userAnswer[kunik][Object.keys(userAnswer[kunik])[index]]:""}</div>
                                    <?php }else{ ?>
                                        <input type="text" 
                                            value="${(hasAnswered)?userAnswer[kunik][Object.keys(userAnswer[kunik])[index]]:""}" 
                                            data-form='<?php echo $form["id"] ?>'
                                            id="${kunik}.${(hasAnswered)?Object.keys(userAnswer[kunik])[index]:index}" 
                                            class="form-control saveOneByOne inputList" 
                                            placeholder="`+sectionDyf.<?php echo $kunik ?>ParamsData.placeholder+` `+multiList+`"
                                        />
                                    <?php } ?>
                                </div>
                            </div>`;

                    $("#inputListing<?= $kunik ?>").append(input);

                    if(hasAnswered && !isNaN(parseInt(Object.keys(userAnswer[kunik])[index]))){
                        inputNb["inputListing<?= $kunik ?>"] = parseInt(Object.keys(userAnswer[kunik])[index]);
                    }else{
                        inputNb["inputListing<?= $kunik ?>"] = defaultLength;
                    }
                }
            }

            sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema" : {
                    "title" : "Paramétrage du listing",
                    "icon" : "cog",
                    "properties" : {
                        defaultInputNumber : {
                            inputType : "number",
                            label : "Nombre de champs par défaut",
                            value :  sectionDyf.<?php echo $kunik ?>ParamsData.defaultInputNumber
                        },
                        placeholder : {
                            inputType : "text",
                            label : "Explication du champ à remplir",
                            value :  sectionDyf.<?php echo $kunik ?>ParamsData.placeholder
                        }
                     },
                    save : function (data) {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                            mylog.log("params value",'.'+k+val.inputType,tplCtx.value[k]);
                        });

                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == undefined)
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                // $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                                dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
                                reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
                            } );
                        }
                    }
                }
            };

            //adds a line into answer
            $(".add<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
            });

            $(".edit<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
            });

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            });

            $(".btn-step").off().on("click", function(){
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                tplCtx.value = $(this).data("step");
                dataHelper.path2Value( tplCtx, function(params) {
                    // $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                    // location.reload();
                    dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
                    reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
                });
            });

        });
    </script>
    <?php } else {
        //echo "<h4 class='text-red'>evaluation works with existing answers</h4>";
    } ?>