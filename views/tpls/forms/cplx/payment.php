
<?php if($answer){ ?>

    <div class="col-md-6">
        <?php
            # Initialize parameters data
            $paramsData = [
                "types" => [
                    "type0" => [
                        "label" => "Acteurs producteurs", 
                        "droits" => [
                            "droit" => [
                                "value" => 0,
                                "label" => "Acteurs producteurs",
                            ]
                        ]
                    ],
                    "type1" => [
                        "label" => "Béneficiaires",
                        "droits" => [
                            "value" => "Acteurs producteurs",
                            "label" => "Acteurs producteurs",
                        ]
                    ]
                ]
            ];

            # Set parameters data
            if( isset($form["params"][$kunik]["types"]) )
                $paramsData["types"] = $form["params"][$kunik]["types"];

            # Get answers if exist
            # !!!  Warning for update !!! 
            # The answers here are the types, If you want add other input (no type) you should edit the code below

            // remove the to test directily (dev) "draft" => ["$exists" => false ]]
            $theAnswers = PHDB::find("answers", array("user"=>Yii::app()->session["userId"], "form"=>$parentForm["_id"]->{'$id'}, "draft" => ['$exists' => false ]));
            
            //$myAnswers = reset($theAnswers);
            //$myAnswers = PHDB::find("answers", array("context.".$this->costum['contextId'].".name"=>$this->costum['name'], "user" => Yii::app()->session["userId"]));
            /**if(!empty($myAnswers["answers"][$form["id"]][$kunik])){
                $myEvaluation = $myAnswers["answers"][$form["id"]][$kunik];
            }else{
                $myEvaluation = [];
            }*/

            
            $editParamsBtn = ($canEdit) ? "<a href='javascript:;' 
                                                data-id='".$form["_id"]."' 
                                                data-collection='".Form::COLLECTION."' 
                                                data-path='params.".$kunik."' 
                                                class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'>
                                                <i class='fa fa-cog'></i> 
                                            </a>" : "";
            
            $editParamsCheckbox = ($canEditForm) ? "<a href='javascript:;'
                                                data-id='".$parentForm["_id"]."' 
                                                data-collection='".Form::COLLECTION."' 
                                                data-path='params.".$kunik."' 
                                                class='previewTpl edit".$kunik."Checkbox btn btn-danger'>
                                                <i class='fa fa-cog'></i> 
                                               </a>" : "";

        ?>

        <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
            <?php echo $label.$editQuestionBtn." ".$editParamsBtn?>
        </h4>

        <?php echo $info ?>

        <br>
        <div class="input-group">
            <div class="input-group-btn">
                <button class="btn btn-primary">Payement</button>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
        var selectedType = "";

        $(document).ready(function(){
            let kunik = "<?= $kunik ?>";
            let droit = {};
            let evaluations = "<?php echo ""; //json_encode( $myEvaluation ); ?>";
            let evaluationtypes = "<?php echo ""; // json_encode( $paramsData["types"] ); ?>";
            let options = [];
            
            sectionDyf.<?php echo $kunik ?>ParamsSelect = {
                "jsonSchema" : {
                    "title" : "Paramétrage du <?php echo $kunik ?>",
                    "description" : "xxxxxxxxxxxxxxxxxxxxxxxx",
                    "icon" : "fa-cog",
                    "properties" : {
                        modes : {
                            inputType : "lists",
                            label : "Mode de payement",
                            entries: {
                                label: {
                                    label:"Mode ",
                                    type:"text",
                                },
                                label: {
                                    label:"Mode ",
                                    type:"text",
                                }
                            },
                        }
                    },
                    save : function (data) {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>ParamsSelect.jsonSchema.properties , function(k,val) {
                            if(val.inputType == "properties")
                                tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                            else if(val.inputType == "array")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else
                                tplCtx.value[k] = $("#"+k).val();

                            mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                        });
                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == undefined)
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                                location.reload();
                            } );
                        }

                    }
                }
            }

            //adds a line into answer
            $(".add<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
            });

            $(".edit<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
            });

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                //if no params config on the element.costum.form.params.<?php echo $kunik ?>
                //then we load default values available in forms.inputs.<?php echo $kunik ?>xxx.params
                //mylog.log(".editParams",sectionDyf.<?php echo $kunik ?>Params,calData);
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>ParamsSelect,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            });
        });
    </script>
    <?php } else {
        //echo "<h4 class='text-red'>evaluation works with existing answers</h4>";
    } ?>