<?php if($answer){ ?>

<style type="text/css">
	.titlecolor<?php echo $kunik ?> {
		<?php 
			if( $mode != "pdf"){	
		?>
		color: <?php echo ($titleColor) ? $titleColor : "black"; ?>;
		<?php
			}
		?>
	}
</style>

	<?php 
		if( $mode != "pdf" and $mode != "r"){
			$editBtnL = ($canEdit == true) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default'><i class='fa fa-plus'></i> ".Yii::t("common","Add line")." </a>" : "";
			
			$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";
			$classforpdf = "";
		} else {
			$editBtnL = "";
			
			$editParamsBtn = "";
			if ($mode == "pdf" ) {
				$classforpdf = "calendar-table";
			}else{
				$classforpdf = "";
			}
			
		}

		$paramsData = [ "listQuestion" => [	"acquis" => "Acquis",
							                        "discussion" => "En discussion",
							                        "adiscuter" => "À discuter"],
							    "role" => [ "financeur" => "Financeur",
					                        "expertise" => "Expertise",
					                        "expertise" => "Expertise",
					                        "ressources" => "Ressources" ] ];

		if( isset($parentForm["params"][$kunik]) ) 
			$paramsData =  $parentForm["params"][$kunik];
		$properties = [
                "qui" => [
                    "inputType" => "text",
                    "label" => "Qui...?",
                    "placeholder" => "Qui...",
                    "rules" => [ "required" => true ]
                ],
                "partner" => [
                    "label" => "Quel partenaire ?",
                    "placeholder" => "...Quel partenaire...",
                    "inputType" => "select",
	                "list" => "partnerList",
	                "rules" => [ "required" => true ]
                ],
                "engagement" => [
                    "inputType" => "text",
                    "label" => "...s'engage à quoi ?",
                    "placeholder" => "...s'engage à quoi...",
                    "rules" => [ "required" => true ]
                ],
                "statut" => [
                    "inputType" => "select",
                    "label" => "A cette date, le partenariat est-il acquis ?",
                    "placeholder" => "A cette date est-il acquis? en discussion ? à discuter ?",
                    "options" => $paramsData["listQuestion"],
                    "rules" => [ "required" => true ]
                ],
                "next" => [
                    "inputType" => "text",
                    "label" => "Prochaine étape / action à entreprendre ?",
                    "placeholder" => "Prochaine étape / action à entreprendre",
                    "rules" => [ "required" => true ]
                ],
                "role" => [
                    "inputType" => "select",
                    "select2" => [
                        "multiple" => true
                    ],
                    "label" => "Role",
                    "placeholder" => "Role ?",
                    "options" => $paramsData["role"],
                    "rules" => [ "required" => true ]
                ]
	        ];

	    $partnerList = [];
	    if(isset($answer["answers"]["action"]["parents"])){
	    	$partnerListIds = [];
	    	foreach ($answer["answers"]["action"]["parents"] as $i => $o) {
	    		$partnerListIds[] = new MongoId( $o["id"] );
	    		//var_dump($o["id"]);
	    	}

	    	$cter = PHDB::findOne( Project::COLLECTION, [ "slug" => $answer['cterSlug'] ] ); 
	    	//var_dump($cter["links"]['contributors']);
			if(isset($cter["links"]['contributors'])){
		    	foreach ( $cter["links"]['contributors'] as $id => $o) {
		    		if( $o['type'] == Organization::COLLECTION ){
		    			$partnerListIds[] = new MongoId( $id );	
		    			//var_dump($id);
		    		}

		    	}
		    }

	    	$partnerList = [];
	    	$orgs = PHDB::find( Organization::COLLECTION, array( "_id" => array( '$in'=>$partnerListIds ) ), ["name"] );
	    	foreach ( $orgs as $id => $o) {
	    		$partnerList[$id] = $o["name"];
	    	}

		}
	?>	
<div class="form-group" style="overflow-x: auto;">
	<div ><h4 class="titlecolor<?php echo $kunik ?> pdftittlecolor"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
				<?php echo $info ?>
	</div>
	<table class="table table-bordered table-hover  directoryTable <?php echo $classforpdf?>" id="<?php echo $kunik?>">
		
	<thead>
		<?php if(isset($answers) && count($answers)>0){ ?>
		<tr>
			<?php 
			
			foreach ($properties as $i => $inp) {
				echo "<th>".$inp["placeholder"]."</th>";
			}
			if( $mode != "pdf" and $mode != "r"){	?>
				<th></th>
			<?php } ?>
		</tr>
		<?php } ?>
	</thead>
	<tbody class="directoryLines">	
		<?php 
		$ct = 0;
		
		if(isset($answers)){
			foreach ($answers as $q => $a) {

				echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
				foreach ($properties as $i => $inp) {
					echo "<td>";
					if(isset($a[$i])) {
						if(is_array($a[$i])){
							foreach ($a["role"] as $i => $r) {
								$a["role"][$i] = $paramsData[ "role"][$r];
							}
							echo implode(",", $a["role"]);
						} else {
							if($i == "statut")
								echo $paramsData["listQuestion"][$a[$i]];
							else if($i == "partner" && isset($partnerList[$a[$i]]))
								echo $partnerList[$a[$i]];
							else
								echo $a[$i];
						}
					}
					echo "</td>";
				}
				if( $mode != "pdf" and $mode != "r"){	
			?>
			<td>
				<?php 
					echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
						"canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
						"id" => $answer["_id"],
						"collection" => Form::ANSWER_COLLECTION,
						"q" => $q,
						"path" => $answerPath.$q,
						"kunik"=>$kunik
						] );
					?>
				<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>','<?php echo $keyTpl ?>','<?php echo (string)$form["_id"] ?>')">
								<?php 
									echo PHDB::count(Comment::COLLECTION, array("contextId"=>(string)$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q)); ?> 
									<i class='fa fa-commenting'></i></a>
			</td>
			<?php 
				}
				$ct++;
				echo "</tr>";
			}
		}
		 ?>
		</tbody>
	</table>
</div>

<?php
if( $mode != "pdf" and $mode != "r"){	
?>
<script type="text/javascript">

if(costum){
	if(typeof costum.lists == "undefined"){
		costum.lists = {};
	}
	costum.lists.partnerList = <?php echo json_encode($partnerList); ?>;
} 

var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

$(document).ready(function() { 
	
	sectionDyf.<?php echo $kunik ?> = {
		"jsonSchema" : {	
	        "title" : "Les partenaires et leurs engagements réciproques",
	        "description" : "Etapes clefs de la fiche action",
	        "icon" : "fa-group",
	        "properties" : <?php echo json_encode( $properties ); ?>,
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    dyFObj.closeForm();
                    reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
	                } );
	            }

	    	}
	    }
	};

	sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "<?php echo $kunik ?> config",
	        "description" : "Liste de question possible",
	        "icon" : "fa-cog",
	        "properties" : {
	            listQuestion : {
	                inputType : "properties",
	                labelKey : "Clef de la question ",
	                labelValue : "Label de la question affiché",
	                label : "Liste de question possible",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.listQuestion
	            },
	            role : {
	                inputType : "properties",
	                labelKey : "Clef du role affiché",
	                labelValue : "Label du role affiché",
	                label : "Liste de role",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.role
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	        		if(val.inputType == "properties")
	        		 	tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
	        		else
	        		 	tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    dyFObj.closeForm();
                    reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
	                } );
	            }

	    	}
	    }
	};

	var max<?php echo $kunik ?> = 0;
	var nextValuekey<?php echo $kunik ?> = 0;

	if(notNull(<?php echo $kunik ?>Data)){
		$.each(<?php echo $kunik ?>Data, function(ind, val){
			if(parseInt(ind) > max<?php echo $kunik ?>){
				max<?php echo $kunik ?> = parseInt(ind);
			}
		});

		nextValuekey<?php echo $kunik ?> = max<?php echo $kunik ?> + 1;

	}


    mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

    //adds a line into answer
    $(".add<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path")+nextValuekey<?php echo $kunik ?>;
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
    });

    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
    });

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        //if no params config on the element.costum.form.params.<?php echo $kunik ?>
        //then we load default values available in forms.inputs.<?php echo $kunik ?>xxx.params
        //mylog.log(".editParams",sectionDyf.<?php echo $kunik ?>Params,calData);
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    
});
</script>
<?php }} else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>