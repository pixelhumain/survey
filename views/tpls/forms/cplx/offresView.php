<?php 

$colspanplus  = (($mode == "r") ? 0 : 2);
?>
<div class="form-group">

	<?php 
	if($mode == "r"){ ?>
		<label ><h4 style="color:<?php echo (!empty($titleColor) ? $titleColor : "black" ); ?>"><?php echo $label ; ?></h4></label>
    <?php echo $info ?>
	<?php 
	} else {
		?>
		<label ><h4 style="color:<?php echo (!empty($titleColor) ? $titleColor : "black" ); ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL ; ?></h4></label>
		<?php echo $info ?>
	<?php 
	} ?>
	<table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>">
		
	<thead>	
		<?php 		
		if( count($answers)>0 ){ ?>
		<tr>
			<?php 
			
			foreach ($properties as $i => $inp) {
				echo "<th>".$inp["placeholder"]."</th>";
				if( $i == "price" ) 
					echo "<th>Commandes</th>";
			} ?>

			<?php 
			if($mode != "r"){ ?>
				<th></th>
			<?php } ?>
		</tr>
		<?php } ?>
	</thead>
	<tbody class="directoryLines">	
		<?php 
		$ct = 0;
		$total = 0;
		$totalPerPerson = [];
		$orderState = (isset($answer["state"])) ? $answer["state"] : "order"; // get || done
		if(isset($answers)){
			foreach ($answers as $q => $a) {

				$tds = "";
				$qtyOrdered = 0;
				foreach ($properties as $i => $inp) {
					$tds .= "<td class='text-center'>";

					if( $i == "price" ) {
						if(!empty($a["price"]))
							$tds .= "<span id='price".$q."'>".$a["price"]."€</span>";
						if( $paramsData["buy"] ) 
						{ 
							$tds .= "</td>";
							$tds .= "<td class='text-center' style='padding:0;'>";
							
							if( isset($a["buys"] ))	
							{ 
								$totalLineOrdered = 0;
								$tds .= "<table class='table table-bordered table-hover  directoryTable'>";

								$tds .= "<tr>";
									$tds .= "<th><a href='javascript:;' class='showbuyers' data-line='".$q."' >Acheteurs (".count($a["buys"]).")</th>";
									$tds .= "<th>Quantité</th>";
									$tds .= "<th>Prix</th>";
									if(isset($answer["state"]))
										$tds .= "<th>Livraison</th>";
								$tds .= "</tr>";

								foreach ( $a["buys"] as $uid => $buy ) {
									if($canEdit || (Yii::app()->session["userId"] == $answer["user"]) )
									{
										$unit = (isset($a["unit"])) ? $a["unit"] : "kg";
										if( !isset( $totalPerPerson[$uid] ) )
											$totalPerPerson[$uid] = ["name"=>$buy["name"],"total"=>0,"qty"=>0];

										$price = $buy["qty"]*$a["price"];
										$totalPerPerson[$uid]["total"] += $price;
										$totalPerPerson[$uid]["qty"] += $buy["qty"];
										$tds .= "<tr class='".$uid." buyerLine line".$q."'>";
										
											$tds .= "<td class='col-xs-6 text-center'>";
												$tds .= "<a href='javascript:;' class='filterbuyer' data-who='".$uid."' >".$buy["name"]."</a>";	
											$tds .= "</td>";

											$tds .= "<td  class='col-xs-2 text-center'>";
													$tds .= $buy["qty"].$unit;
											$tds .= "</td>";

											$tds .= "<td  class='col-xs-2 text-center'>";
													$tds .= $price."€";
											$tds .= "</td>";

											if(isset($answer["state"])){
												$tds .= "<td  class='col-xs-2 text-center'>";
													$selected = ( isset($buy["done"]) ) ? "success" : "default"; 
													$tds .= "<a href='javascript:;' data-id='".$answer["_id"]."' data-uid='".$uid."' data-qty='".$buy["qty"]."' data-key='".$key."' data-form='".$form["id"]."' data-pos='".$q."'  class='btn btn-".$selected." btnValidateDelivery'>Livré</a>";
												$tds .= "</td>";
											}
										$tds .= "</tr>";
									}
									$qtyOrdered += $buy["qty"];
									$totalLineOrdered += $price;
								}
								$total += $totalLineOrdered;
								$tds .= "</table>";
								
							}
							$reste = (int)$a["qtyMax"] - $qtyOrdered;
							if( ($reste > 0 && !isset($answer["state"]) ) || $canEdit ) 
								$tds .= "<a href='javascript:;' data-id='".$answer["_id"]."' data-key='".$key."' data-form='".$form["id"]."' data-pos='".$q."'  class='btn btn-danger btnbuy margin-top-10'><i class='fa fa-plus'></i> Commander</a>";
						} 	
					} else if( $i == "qtyMax" ) {
						if(!empty($a["qtyMax"])){
							$reste = (int)$a["qtyMax"] - $qtyOrdered;
							$tds .= "<span>Reste : ".$reste.$unit."</span>";
							$tds .= "<br/><span>Commandé : ".$qtyOrdered.$unit."</span>";
						}
							
					}
					else if(isset($a[$i]))
						$tds .= $a[$i];
					
					$tds .= "</td>";
				}

				
					echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
						echo $tds;
						if($mode != "r")
						{ ?>
						<td>
							<?php 
							if(Yii::app()->session["userId"] == $answer["user"] && !isset($answer["state"] )){
								echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
									"canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
									"id" => $answer["_id"],
									"collection" => Form::ANSWER_COLLECTION,
									"q" => $q,
									"path" => $answerPath.$q,
									"kunik"=>$kunik ] ); 
									}?>

							<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
						</td>
				<?php 
						}
					$ct++;
					echo "</tr>";
				
			}
		}


if($total > 0){

	
	foreach ($totalPerPerson as $uid => $utot) {
		echo "<tr class='bold'>";
		echo 	"<td colspan=2 style='text-align:right'> <a href='javascript:;' class='filterbuyer' data-who='".$uid."' >TOTAL ".$utot["name"]."</a> : </td>";
		echo 	"<td>".$utot["total"]." €</td>";
		echo 	"<td>".$utot["qty"]." kg(ou unité)</td>";
		echo "</tr>";		
	}

	echo "<tr class='bold'>";
	echo 	"<td colspan=2 style='text-align:right'> <a href='javascript:;' class='filterbuyer' data-who='' >TOTAL Commandés </a> : </td>";
	echo 	"<td>".$total." €</td>";
	echo "</tr>";

	echo "<tr class='bold'>";
	echo 	"<td colspan=2 style='text-align:right'> État de la Commande : </td>";
	echo 	"<td>";

	
	$state = "danger";
	$lbl = "COMMANDER";
	$value = "get";
	if($orderState == "get"){
		$state = "warning";
		$lbl = "VENEZ RÉCUPÉRER";
		$value = "done";
	}
	else if($orderState == "done"){
		$state = "success";
		$lbl = "TERMINÉ";	
		$value = null;
	}
	$orderStateClass = ($value) ? "btnOrderState" : "";
	echo "<a href='javascript:;' data-id='".$answer["_id"]."' data-uid='".$uid."' data-value='".$value."' class='btn btn-".$state." ".$orderStateClass."'>".$lbl."</a>";
	echo "</td>";
	echo "</tr>";

}

	

?>
		</tbody>
	</table>
</div>

<script type="text/javascript">
$(document).ready(function() { 
mylog.log("render","/modules/survey/views/tpls/forms/cplx/offresView.php");
	$(".filterbuyer").click(function() { 
		$(".buyerLine").fadeOut();
		if($(this).data("who") != "")
			$("."+$(this).data("who")).fadeIn();
		else 
			$(".buyerLine").fadeIn();
	})
	$(".showbuyers").click(function() { 
		$(".line"+$(this).data("line")).slideToggle();
	})
	$(".buyerLine").fadeOut();
})
</script>