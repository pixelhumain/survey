<?php
    $cssAndScriptFiles = array(
        // datetimepicker
        '/plugins/xdan.datetimepicker/jquery.datetimepicker.min.css',
        '/plugins/xdan.datetimepicker/jquery.datetimepicker.min.js',
        '/plugins/xdan.datetimepicker/jquery.datetimepicker.full.min.js',
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFiles, Yii::app()->request->baseUrl);
    $cssAndScriptSurveyFiles = array(
        '/css/inputs-style.css',
        '/css/space-layout.css',
        '/css/codate/codate.css'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAndScriptSurveyFiles, Yii::app()->getModule(Survey::MODULE)->getAssetsUrl());

    $cle = $key ;
    
    $inpClass = " saveOneByOne";

    $editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger' data-toggle='modal' data-target='#configSondage".$kunik."'><i class='fa fa-cog'></i> </a>" : "";

    $paramsData = [ 
        "alldates" => new stdClass,
        "eventduration" => ''
    ];

    $currentAnswer = (!empty($answer) && isset($answer["answers"][$form["id"]][$key])) ? $answer["answers"][$form["id"]][$key] : [];

    $configView = '';

     if( isset($parentForm["params"][$kunik]) ) {
        if( isset($parentForm["params"][$kunik]["alldates"]) ) {
            $paramsData["alldates"] =  $parentForm["params"][$kunik]["alldates"];
        }
        if(isset($parentForm["params"][$kunik]["eventduration"])) $paramsData["eventduration"] =  $parentForm["params"][$kunik]["eventduration"];
     }

     if($mode == 'r') {
        $ownAnswerBlock = '';
        /* $allFormAnswers = PHDB::find(Form::ANSWER_COLLECTION, array("form" => (string)$parentForm['_id']), ['answers', 'user']);
        $reslutView = array();
        $nbVote = 0;
        $userConcerned;
        $bodyUsersAnswers = '';
        $tabView = '<table class="codate table-merite"><thead><tr><th rowspan="2"></th>';
        $trSubBlock ='';
        $bodyBlock = '';
        $iterationCount = 0;
        $footerBlock = '';
        $maxChoose = 0;
        $ownAnswerBlock = '';
        $myIsAdmin = false;
        if(isset(Yii::app()->session["userId"])) {
            $myIsAdmin = (isset($parentForm["parent"][Yii::app()->session["userId"]])) ? true : false;
        }
        foreach($allFormAnswers as $iter => $elem) {
            $userConcerned = PHDB::findOneById(Citoyen::COLLECTION, $elem['user'], ['name', 'username']);
            $userConcerName = isset($userConcerned['username']) ? $userConcerned['username'] : "";
            $bodyBlock .= '<tr><th>'.$userConcerName.'</th>';
            foreach ($paramsData['alldates'] as $indexKey => $item) {
                if($iterationCount == 0) {
                    $tabView .= '
                            <th colspan="'.count($item).'">'.$indexKey.'</th>
                        ';
                }
                foreach ($item as $itemIter => $timeValue) {
                    $colUnikId = uniqid();
                    
                    if($iterationCount == 0) {
                        $createEventBtn = '<a href="javascript:;" id="'.$colUnikId.'" class="createEvent" data-id="'.new MongoId().'" data-date="'.$indexKey.'" data-time="'.$timeValue.'" data-titleQuestion="'.$label.'"><i class="fa fa-calendar"></i></a>';
                        if($myIsAdmin) $trSubBlock .= '
                            <th>'.$timeValue.'<br>'.$createEventBtn.'</th>
                        ';
                        else $trSubBlock .= '
                            <th>'.$timeValue.'</th>
                        ';
                        // $trSubBlock .= ($canEditForm) ?
                        //     '
                        //         <th>'.$timeValue.'<br>'.$createEventBtn.'</th>
                        //     '
                        //     :'
                        //         <th>'.$timeValue.'</th>
                        //     ';
                        $reslutView[$indexKey][$timeValue] = 0;
                    }
                    $valChecked = '';
                    if(isset($elem['answers'][$form['id']][$key])) {
                        if(isset($elem['answers'][$form['id']][$key][$indexKey])) {
                            if(array_search($timeValue, $elem['answers'][$form['id']][$key][$indexKey]) > -1) {
                                $valChecked =  "checked";
                                if(isset($reslutView[$indexKey][$timeValue])) $reslutView[$indexKey][$timeValue]++;
                                else $reslutView[$indexKey][$timeValue] = 1;
                                if($reslutView[$indexKey][$timeValue] > $maxChoose) $maxChoose = $reslutView[$indexKey][$timeValue];
                            }
                        }
                        
                    }
                    $bodyBlock .= '
                        <td>
                            <input type="checkbox" id="cbr_'.$colUnikId.'" name="codateR'.$kunik.'" class="animate-check" disabled '.$valChecked.' />
                            <label for="cbr_'.$colUnikId.'" class="check-box"></label> 
                        </td>
                    ';
                    if(count($allFormAnswers) - 1 == $iterationCount) {
                        $classColMax = '';
                        if($reslutView[$indexKey][$timeValue] == $maxChoose && $maxChoose != 0) {
                            $classColMax = 'max-votant';
                            $maxChoose = 99999999999;
                        }
                        $footerBlock .= '
                            <td class="'.$classColMax.'" >'.$reslutView[$indexKey][$timeValue].'</td>
                        ';
                    }
                }
            }
            $bodyBlock .= '</tr>';
            $iterationCount++;
        }
        $tabView .= '
            </tr>
            <tr class="fs">
                '.$trSubBlock.'
            </tr>
            </thead>
            <tbody>
                '.$bodyBlock.'
            </tbody>
            <tfoot>
                <tr>
                    <th>'.Yii::t("common","Total voters ").$iterationCount.'</th>
                    '.$footerBlock.'
                </tr>
            </tfoot>
            </table>
        '; */
        foreach ($answers as $answersKey => $answersItem) {
            $ownAnswerBlock .= '
                <span class="text-bold">'.$answersKey.' : </span>
            ';
            foreach ($answersItem as $timeIndex => $timeVal) {
                $ownAnswerBlock .= '
                    <span class="badge own-time ml-2">'.$timeVal.'</span>
                ';
            }
            $ownAnswerBlock .= '<br>';
            
        }
?>
        <div class="pt-1">
            <div class="col-xs-12">
                <label for="form-check-label" for="<?php echo $key ?>">
                    <h4 style="text-transform: unset; color: <?php echo ($titleColor) ? $titleColor : "black"; ?>;"><?php echo $label ?>
                    </h4>
                </label>
            </div>
            <div class="col-xs-6 ml-1 mb-3">
                <?php echo $ownAnswerBlock; ?>
            </div>
            <!-- <div class="col-xs-12">
                <div class="col-xs-12 p-0 card">
                    <div class="card-header">
                        <h4><?php echo Yii::t("common","All result"); ?></h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            
                        </div>
                    </div>
                </div>
            </div> -->
        </div> 

        <!-- Modal -->
        <div class="modal config-sondage fade" id="createNewEvent<?php echo $kunik ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header bg-dark text-center">
                    <button type="button" class="close color-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo $label ?> Create new event</h4>
                </div>
                <div class="col-xs-12 ma-21" id="allDateContainer<?php echo $kunik ?>" style="max-height: 50vh; overflow-y: auto">
                    <form id="ajaxFormModalEvent">
                        <div class=" form-group nametext">
                            <label class="col-xs-12 text-left control-label no-padding" for="name">
                                <i class="fa fa-chevron-down"></i> Nom de votre événement *
                            </label>
                            <input type="text" class="form-control " name="name" id="name" value="" placeholder="Nom de votre événement  ...*" data-type="event">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t("survey","Close"); ?></button>
                    <button type="button" class="btn btn-primary saveDateSondage" data-id="<?php echo isset($parentForm["_id"]) ? $parentForm['_id'] : ''?>" data-collection="<?php echo Form::COLLECTION ?>" data-path="params.<?php echo $kunik?>" data-datecontainerid='allDateContainer<?php echo $kunik ?>'><?php echo Yii::t("survey","Save"); ?></button>
                </div>
                </div>
            </div>
        </div>
<?php
     } else if($mode == "pdf") {
     } else {
        $allFormAnswers = PHDB::find(Form::ANSWER_COLLECTION, array("form" => (string)$parentForm['_id']), ['answers', 'user', 'updated', "name"]);
        $reslutView = array();
        $nbVote = 0;
        $userConcerned;
        $bodyUsersAnswers = '';
        $tabViewAllResult = '<table class="codate table-merite" style="margin-bottom:0; width: unset;"><thead style="display: none"><tr><th rowspan="2"></th>';
        $trSubBlock ='';
        $bodyBlock = '';
        $iterationCount = 0;
        $footerBlock = '';
        $maxChoose = 0;
        $ownAnswerBlock = '';
        $myIsAdmin = false;
        $valideVoters = 0;
        /* if(isset(Yii::app()->session["userId"])) {
            $myIsAdmin = (isset($parentForm["parent"][Yii::app()->session["userId"]])) ? true : false;
        } */
        if(count($allFormAnswers) > 0) {
            foreach($allFormAnswers as $iter => $elem) {
                
                if(isset($elem['updated'])) {
                    $valideVoters++;
                    $userConcerned = PHDB::findOneById(Citoyen::COLLECTION, $elem['user'], ['name', 'username', "roles"]);
                    if(isset($userConcerned['name'])) {
                        if(isset($userConcerned["roles"])){
                            if(isset($userConcerned["roles"]["tobeactivated"])) {
                                isset($elem["name"]) ? $bodyBlock .= '<tr><th class="bg-name">'.$elem['name'].'</th>' : $bodyBlock .= '<tr><th class="bg-name">'.$userConcerned['name'].'</th>';
                            } else $bodyBlock .= '<tr><th class="bg-name">'.$userConcerned['name'].'</th>';
                        } else {
                            $bodyBlock .= '<tr><th class="bg-name">'.$userConcerned['name'].'</th>'; 
                        }
                    } else   $bodyBlock .= '<tr><th class="bg-name">'.'Not logged'.'</th>';
                    
                    foreach ($paramsData['alldates'] as $indexKey => $item) {
                        if($iterationCount == 0) {
                            $tabViewAllResult .= '
                                    <th colspan="'.count($item).'">'.$indexKey.'</th>
                                ';
                        }
                        foreach ($item as $itemIter => $timeValue) {
                            $colUnikId = uniqid();
                            
                            if($iterationCount == 0) {
                                $createEventBtn = '<a href="javascript:;" id="'.$colUnikId.'" class="createEvent" data-id="'.new MongoId().'" data-date="'.$indexKey.'" data-time="'.$timeValue.'" data-titleQuestion="'.$label.'"><i class="fa fa-calendar"></i></a>';
                                $trSubBlock .= '
                                    <th>'.$timeValue.'</th>
                                ';
                                $reslutView[$indexKey][$timeValue] = 0;
                            }
                            $valChecked = '';
                            if(isset($elem['answers'][$form['id']][$key])) {
                                if(isset($elem['answers'][$form['id']][$key][$indexKey])) {
                                    if(array_search($timeValue, $elem['answers'][$form['id']][$key][$indexKey]) > -1) {
                                        $valChecked =  "checked";
                                        if(isset($reslutView[$indexKey][$timeValue])) $reslutView[$indexKey][$timeValue]++;
                                        else $reslutView[$indexKey][$timeValue] = 1;
                                        if($reslutView[$indexKey][$timeValue] > $maxChoose) $maxChoose = $reslutView[$indexKey][$timeValue];
                                    }
                                }
                                
                            }
                            $bodyBlock .= '
                                <td>
                                    <input type="checkbox" id="cbr_'.$colUnikId.'" name="codateR'.$kunik.'" class="animate-check" disabled '.$valChecked.' />
                                    <label for="cbr_'.$colUnikId.'" class="check-box"></label> 
                                </td>
                            ';
                        }
                    }

                    $btnEditResponse = $elem["user"] == Yii::app()->session["userId"] ? '<a href="javascript:;" class="editResponseCodateBtn" data-id="'.$iter.'" data-formid="'.$form['id'].'" data-checkboxname="valueCodate'.$kunik.'">
                                            <i class="fa fa-pencil text-dark"></i>
                                        </a>' : "";
                    $bodyBlock .= '
                            <td class="border-hide">
                                '.$btnEditResponse.'
                            </td>
                        </tr>';
                }
                if(count($allFormAnswers) - 1 == $iterationCount) {
                    foreach ($paramsData['alldates'] as $indexKey => $item) {
                        foreach ($item as $itemIter => $timeValue) {
                                $classColMax = '';
                                $maxIcon = '';
                                if(isset($reslutView[$indexKey])){
                                    if(isset($reslutView[$indexKey][$timeValue])){
                                        if($reslutView[$indexKey][$timeValue] == $maxChoose && $maxChoose != 0) {
                                            $classColMax = 'max-votant';
                                            $maxIcon = '<i class="fa fa-star"></i>';
                                            // $maxChoose = 99999999999;
                                        }
                                        $footerBlock .= '
                                            <td class="'.$classColMax.'" >'.$maxIcon.' '.$reslutView[$indexKey][$timeValue].'</td>
                                        ';
                                    } else {
                                        $footerBlock .= '
                                            <td class="fix-width '.$classColMax.'" > 0</td>
                                        ';
                                    }
                                } else {
                                    $footerBlock .= '
                                        <td class="fix-width '.$classColMax.'" > 0</td>
                                    ';
                                }
                        }
                    }
                }
                $iterationCount++;
            }
        }
        $tabViewAllResult .= '
            </tr>
            <tr class="fs">
                '.$trSubBlock.'
                <th class="border-hide">
                    <a href="javascript:;" class="editResponseCodateBtn" data-id="'.$iter.'" data-formid="'.$form['id'].'">
                        <i class="fa fa-pencil text-dark"></i>
                    </a>
                </th>
            </tr>
            </thead>
            <tbody class="fix-width">
                '.$bodyBlock.'
            </tbody>
            <tfoot class="fix-width footer-sticky">
                <tr>
                    <th class="bg-name">'.Yii::t("common","Total voters ").$valideVoters.'</th>
                    '.$footerBlock.'
                    <th class="border-hide"></th>
                </tr>
            </tfoot>
            </table>
        ';
?>
        <div class="pt-1">
            <label for="form-check-label" for="<?php echo $key ?>">
                <h4 style="text-transform: unset; color: <?php echo ($titleColor) ? $titleColor : "black"; ?>;"><?php echo $label.$editQuestionBtn.$editParamsBtn ?>
                </h4>
            </label>
        </div>
        <?php
            if(!isset($parentForm["params"][$kunik]["alldates"])) {
                $idDateContainer = uniqid();
                $idInputPref = uniqid();
                $timeInputs = '';
                $timeInputsId = '';
                for($i = 0; $i < 3; $i++) {
                    $inputPref = uniqid();
                    $creneauPlaceholder = $i + 1;
                    $timeInputs .= '<input type="text" class="input-pref form-control myTimeInput" id="inputPref_'.$inputPref.'" placeholder="créneau '. $creneauPlaceholder .'" data-inputparent="date_'.$idDateContainer.'" data-parent="inputsPref'.$idInputPref.'"/>';
                    $timeInputsId .= 'inputPref_'.$inputPref;
                    if($i < 2) $timeInputsId .= ',';
                }
                $configView = '
                <div class="col-xs-12 mb-3 p-xs-0 padding-lr-config-input">
                    <label for="eventDuration'.$idInputPref.'">'.Yii::t("survey", "Event duration").' ('.Yii::t("survey","in")." ".Yii::t("survey", "Hour").')</label>
                    <input type="number" id="eventDuration'.$idInputPref.'" class="form-control eventtimeduration" placeholder="'.Yii::t("survey", "Add a duration for the event to be created").'"/>
                </div>
                <div id="dateContainer'.$idDateContainer.'" class="col-xs-12 mb-2 p-xs-0">
                    <div class="col-xs-12 p-xs-0" id="blockdate'.$idDateContainer.'">
                        <div id="dateinput'.$idDateContainer.'" class="col-xs-10 pl-xs-0">
                            <label for="date_'.$idDateContainer.'">Date</label>
                            <input type="text" id="date_'.$idDateContainer.'" data-id="date_'.$idDateContainer.'" placeholder="'.Yii::t("survey", "Select a date").'" class="my-form-input date-from-value myDateTimeInput"/>
                            <span class="border"></span>
                        </div>
                        <div id="blockoption'.$idDateContainer.'" class="col-xs-2 pr-xs-0" style="position: absolute; left: 10%">
                            <a class="btn btn-xs btn-danger removeDateBlock" data-id="dateContainer'.$idDateContainer.'" href="javascript:;">
                                <i class="fa fa-trash"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12 p-xs-0" id="blockPref'.$idDateContainer.'">
                        <div class="col-xs-10 pl-xs-0 timeInputContainer" id="inputsPrefBlock'.$idInputPref.'">
                            '.$timeInputs.'
                        </div>
                        <div class="col-xs-2 pr-xs-0" style="padding-top: 8px">
                            <a class="btn btn-xs btn-danger removeTimeElmt" id="inputsPref'.$idInputPref.'Remove" data-idparent="inputsPrefBlock'.$idInputPref.'" data-keyparent="date_'.$idDateContainer.'" data-key="'.$idInputPref.'" data-id="'.$timeInputsId.'" href="javascript:;">
                                <i class="fa fa-minus"></i>
                            </a>
                            <a class="btn btn-xs btn-success addTimeElmt" id="inputsPref'.$idInputPref.'Add" data-idparent="inputsPrefBlock'.$idInputPref.'" data-keyparent="date_'.$idDateContainer.'" data-key="'.$idInputPref.'" data-id="'.$timeInputsId.'" href="javascript:;">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>';
                /* <div class="col-xs-12" style="margin-top: -10px">
                            <span class="input-placeholder">'.Yii::t("survey", "Add time slots").'</span>
                    </div> */
                echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> ".Yii::t("survey","THIS FIELD HAS TO BE CONFIGURED FIRST")." ".$editParamsBtn."</span>";
            } else {
                if(!(array) $paramsData['alldates']) {
                    $idDateContainer = uniqid();
                    $idInputPref = uniqid();
                    $timeInputs = '';
                    $timeInputsId = '';
                    for($i = 0; $i < 3; $i++) {
                        $inputPref = uniqid();
                        $creneauPlaceholder = $i + 1;
                        $timeInputs .= '<input type="text" class="input-pref form-control myTimeInput" id="inputPref_'.$inputPref.'" placeholder="créneau '.$creneauPlaceholder .'" data-inputparent="date_'.$idDateContainer.'" data-parent="inputsPref'.$idInputPref.'"/>';
                        $timeInputsId .= 'inputPref_'.$inputPref;
                        if($i < 2) $timeInputsId .= ',';
                    }
                    $eventDuration = isset($paramsData['eventduration']) ? $paramsData['eventduration'] : '';
                    $configView = '
                    <div class="col-xs-12 mb-3 p-xs-0 padding-lr-config-input">
                        <label for="eventDuration'.$idInputPref.'">'.Yii::t("survey", "Event duration").' ('.Yii::t("survey","in")." ".Yii::t("survey", "Hour").')</label>
                        <input type="number" id="eventDuration'.$idInputPref.'" class="form-control eventtimeduration" value="'.$eventDuration.'" placeholder="'.Yii::t("survey", "Add a duration for the event to be created").'"/>
                    </div>
                    <div id="dateContainer'.$idDateContainer.'" class="col-xs-12 mb-2 p-xs-0">
                        <div class="col-xs-12 p-xs-0" id="blockdate'.$idDateContainer.'">
                            <div id="dateinput'.$idDateContainer.'" class="col-xs-10 pl-xs-0">
                                <label for="date_'.$idDateContainer.'">Date</label>
                                <input type="text" id="date_'.$idDateContainer.'" data-id="date_'.$idDateContainer.'" placeholder="'.Yii::t("survey", "Select a date").'" class="my-form-input date-from-value myDateTimeInput"/>
                                <span class="border"></span>
                            </div>
                            <div id="blockoption'.$idDateContainer.'" class="col-xs-2 pr-xs-0" style="position: absolute; left: 10%">
                                <a class="btn btn-xs btn-danger removeDateBlock" data-id="dateContainer'.$idDateContainer.'" href="javascript:;">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 p-xs-0" id="blockPref'.$idDateContainer.'">
                            <div class="col-xs-10 pl-xs-0 timeInputContainer" id="inputsPrefBlock'.$idInputPref.'">
                                '.$timeInputs.'
                            </div>
                            <div class="col-xs-2 pr-xs-0" style="padding-top: 8px">
                                <a class="btn btn-xs btn-danger removeTimeElmt" id="inputsPref'.$idInputPref.'Remove" data-idparent="inputsPrefBlock'.$idInputPref.'" data-keyparent="date_'.$idDateContainer.'" data-key="'.$idInputPref.'" data-id="'.$timeInputsId.'" href="javascript:;">
                                    <i class="fa fa-minus"></i>
                                </a>
                                <a class="btn btn-xs btn-success addTimeElmt" id="inputsPref'.$idInputPref.'Add" data-idparent="inputsPrefBlock'.$idInputPref.'" data-keyparent="date_'.$idDateContainer.'" data-key="'.$idInputPref.'" data-id="'.$timeInputsId.'" href="javascript:;">
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                    </div>';
                    /* <div class="col-xs-12" style="margin-top: -10px">
                                <span class="input-placeholder">'.Yii::t("survey", "Add time slots").'</span>
                            </div> */
                } else {
                    $configView = '';
                    $tabView = '<table class="codate table-merite" style="margin-bottom:0 !important; width: unset; position: sticky; top: 0; z-index: 2; background-color: white"><thead class="fix-width"><tr><th class="bg-name" rowspan="2"></th>';
                    $trSubBlock ='';
                    $bodyBlock = '';
                    $iteration = 0;
                    $loggedUser = isset(Yii::app()->session['userId']) ? PHDB::findOneById(Citoyen::COLLECTION, Yii::app()->session['userId'], ['name', 'username', "roles"]) : null;
                    if(isset($loggedUser["roles"]["tobeactivated"])) {
                        if(isset($loggedUser['name']))  {
                            if(isset($answer['name'])) {
                                $bodyBlock .= '<tr><th class="bg-name"> <input type="text" class="form-control userNotLoggedName" data-path="allToRoot" data-collection="'.Form::ANSWER_COLLECTION.'" value="'.$answer['name'].'" data-formcle="'.$cle.'" data-formid="'.$form['id'].'" /></th>' ;
                            } else $bodyBlock .= '<tr><th class="bg-name"> <input type="text" class="form-control userNotLoggedName" data-path="allToRoot" data-collection="'.Form::ANSWER_COLLECTION.'" value="'.$loggedUser["name"].'" data-formcle="'.$cle.'" data-formid="'.$form['id'].'" /></th>' ;
                        } else $bodyBlock .= '<tr><th class="bg-name"><input type="text" value="" /></th>';
                    } else {
                        isset($loggedUser['name']) ? $bodyBlock .= '<tr><th class="bg-name">'.$loggedUser["name"].'</th>' : $bodyBlock .= '<tr><th class="bg-name">'.'User not logged'.'</th>';
                    }
                    foreach ($paramsData['alldates'] as $indexKey => $item) {
                        $idDateContainer = uniqid();
                        $tabView .= '
                            <th colspan="'.count($item).'">'.$indexKey.'</th>
                        ';
                        $eventDuration = isset($paramsData['eventduration']) ? $paramsData['eventduration'] : '';
                        if($iteration == 0) {
                            $configView .= '
                            <div class="col-xs-12 mb-3 p-xs-0 padding-lr-config-input">
                                <label for="eventDuration'.$idDateContainer.'">'.Yii::t("survey", "Event duration").' ('.Yii::t("survey","in")." ".Yii::t("survey", "Hour").')</label>
                                <input type="number" id="eventDuration'.$idDateContainer.'" class="form-control eventtimeduration" value="'.$eventDuration.'" placeholder="'.Yii::t("survey", "Add a duration for the event to be created").'"/>
                            </div>
                            ';
                        }
                        $configView .= '
                        <div id="dateContainer'.$idDateContainer.'" class="col-xs-12 mb-2 p-xs-0">
                            <div class="col-xs-12 p-xs-0" id="blockdate'.$idDateContainer.'">
                            <div id="dateinput'.$idDateContainer.'" class="col-xs-10 pl-xs-0">
                                <label for="date_'.$idDateContainer.'">Date</label>
                                <input type="text" id="date_'.$idDateContainer.'" value="'.$indexKey.'" data-id="date_'.$idDateContainer.'" placeholder="'.Yii::t("survey", "Select a date").'" class="my-form-input date-from-value myDateTimeInput"/>
                                <span class="border"></span>
                            </div>
                            <div id="blockoption'.$idDateContainer.'" class="col-xs-2 pr-xs-0" style="position: absolute; left: 10%">
                                <a class="btn btn-xs btn-danger removeDateBlock" data-id="dateContainer'.$idDateContainer.'" href="javascript:;">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 p-xs-0" id="blockPref'.$idDateContainer.'">
                        ';
                        $dataIds = [];
                        $prefBlockAction = '';
                        foreach ($item as $index => $value) {
                            $trSubBlock .='
                                <th>'.$value.'</th>
                            ';
                            $idInputPref = uniqid();
                            $valChecked = isset($answers[$indexKey]) ? (array_search($value, $answers[$indexKey]) > -1 ? "checked" : "") : "";
                            $bodyBlock .= '
                            <td>
                                <input type="checkbox" id="cb_'.$idInputPref.'" name="valueCodate'.$kunik.'" class="animate-check" data-date="'.$indexKey.'" data-time="'.$value.'" '.$valChecked.' />
                                <label for="cb_'.$idInputPref.'" class="check-box"></label> 
                            </td>
                            ';
                            if($index == 0) {
                                $configView .= '<div class="col-xs-10 pl-xs-0 timeInputContainer" id="inputsPrefBlock'.$idInputPref.'">';
                                $prefBlockAction = $idInputPref;
                            }
                            $configView .= '
                                <input type="text" class="input-pref form-control myTimeInput" id="inputPref_'.$idInputPref.'" value="'.$value.'" data-inputparent="date_'.$idDateContainer.'" data-parent="inputsPref'.$idInputPref.'"/>
                            ';
                            array_push($dataIds, 'inputPref_'.$idInputPref);
                            if(count($item) -1 == $index) {
                                $configView .= '
                                            </div>
                                            <div class="col-xs-2 pr-xs-0" style="padding-top: 8px">
                                            <a class="btn btn-xs btn-danger removeTimeElmt" id="inputsPref'.$prefBlockAction.'Remove" data-idparent="inputsPrefBlock'.$prefBlockAction.'" data-keyparent="date_'.$idDateContainer.'" data-key="'.$prefBlockAction.'" data-id="'.implode(',', $dataIds).'" href="javascript:;">
                                                <i class="fa fa-minus"></i>
                                            </a>
                                            <a class="btn btn-xs btn-success addTimeElmt" id="inputsPref'.$prefBlockAction.'Add" data-idparent="inputsPrefBlock'.$prefBlockAction.'" data-keyparent="date_'.$idDateContainer.'" data-key="'.$prefBlockAction.'" data-id="'.implode(',', $dataIds).'" href="javascript:;">
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                ';
                            }
                        }
                        $iteration++;
                    }
                    $bodyBlock .= '
                            <th class="border-hide">
                                <button type="button" class="coDateSaveVote btn btn-success btn-secondary btn-xs" data-checkboxname="valueCodate'.$kunik.'" data-path="answers.'.$form['id'].'.'.$key.'" data-collection="'.Form::ANSWER_COLLECTION.'" data-formcle="'.$cle.'" data-formid="'.$form['id'].'">
                                    '.Yii::t("common","Save").'
                                </button>
                            </th>
                        </tr>';
                    $tabView .= '
                        <th class="border-hide"></th>
                    </tr>
                    <tr class="fs">
                        '.$trSubBlock.'
                        <th class="border-hide"></th>
                    </tr>
                    </thead>
                    <tbody class="fix-width">
                        '.$bodyBlock.'
                    </tbody>
                    </table>
                    ';
                    ?>
                    <div class="col-xs-12 p-0 card">
                        <div class="card-body">
                            <div id="codateContainer<?php echo $form['id'] ?>" class="codate table-responsive codateContainer">
                                <button type="button" class="btn btn-sm btn-link codate scroll-left hidden-print disabled" title="Faire défiler à gauche" data-id="codateContainer<?php echo $form["id"] ?>" aria-hidden="true">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </button>
                                <button type="button" class="btn  btn-sm btn-link codate scroll-right hidden-print" title="Faire défiler à droite" data-id="codateContainer<?php echo $form["id"] ?>" aria-hidden="true">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </button>
                                <?php echo $tabView; ?>
                                <?php echo $tabViewAllResult ?>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="col-xs-12 mb-3">
                                <?php if(!empty($info)){ ?>
                                    <small id="<?php echo $key ?>Help" class="form-text text-muted">
                                        <?php echo $info ?>
                                    </small>
                                <?php } else { ?>
                                    <small id="<?php echo $key ?>Help" class="form-text text-muted">
                                        <?php echo Yii::t("survey", "Please select time slots") ?>
                                    </small>
                                <?php } ?>
                                <button type="button" class="btn btn-success btn-secondary coDateSaveVote pull-right" data-checkboxname="valueCodate<?php echo $kunik?>" data-path="answers.<?php echo $form['id'].'.'.$key ; ?>" data-collection="<?php echo Form::ANSWER_COLLECTION ?>" data-formcle="<?php echo $cle; ?>" data-formid="<?php echo $form['id']; ?>" ><?php echo Yii::t("common","Save"); ?></button>
                            </div>
                        </div>
                    </div>
        <?php
                }
        ?>

        <?php
            }
        ?>
        
        <!-- Modal -->
        <div class="modal config-sondage fade" id="configSondage<?php echo $kunik ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header bg-dark text-center">
                    <button type="button" class="close color-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo $label ?> config</h4>
                </div>
                <div class="col-xs-12 ma-21" id="allDateContainer<?php echo $kunik ?>" style="max-height: 50vh; overflow-y: auto">
                    <?php echo $configView ?>
                </div>
                <div class="col-xs-12 text-center mb-xs-3">
                    <a class="btn btn-xs btn-success addDateBlock" id="addDateBlock" data-idparent="allDateContainer<?php echo $kunik ?>" href="javascript:;">
                        <i class="fa fa-plus"></i> <?php echo Yii::t('common','Add line') ?>
                    </a> <br>
                    <div class="col-xs-12 d-flex justify-content-center mt-2">
                        <small class="pt-2"><?php echo Yii::t('survey',"Days's increment") ?></small> <input type="number" name="days_increment" value="1" class="ml-2" style="width: 40px; border: none; font-weight: 600; cursor: pointer" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t("survey","Close"); ?></button>
                    <button type="button" class="btn btn-primary saveDateSondage" data-id="<?php echo isset($parentForm["_id"]) ? $parentForm['_id'] : ''?>" data-collection="<?php echo Form::COLLECTION ?>" data-path="params.<?php echo $kunik?>" data-datecontainerid='allDateContainer<?php echo $kunik ?>'><?php echo Yii::t("survey","Save"); ?></button>
                </div>
                </div>
            </div>
        </div>
<?php
     }
?>

<script type="text/javascript">
    function generateDateBlock(idContainerUnik, idInputPrefUnik, idContainer = null, daysIncrement = 1) {
        const addDays = function(date = new Date(), days) {
            let res = new Date(date.getTime());
            res.setTime(res.getTime() + days * 24 * 60 * 60 * 1000);
            return res != "Invalid Date" ? res.toLocaleString('fr', {day: 'numeric', month: 'numeric', year: 'numeric'}) : null;
        }
        let timeInputs = '';
        let timeInputsId = '';
        const timeInputsContainer = $("#"+idContainer).find(".timeInputContainer").toArray();
        const dateInputs = $("#"+idContainer).find("input[type='text'].myDateTimeInput").toArray();
        let lastTimeInputs = timeInputsContainer.length > 0 ? timeInputsContainer[timeInputsContainer.length - 1] : null;
        const lastDateInput = dateInputs.length > 0 ? dateInputs[dateInputs.length - 1] : null;
        const [d, m, y] = lastDateInput ? 
                                        ($(lastDateInput).val() != '' && $(lastDateInput).val() != null ? $(lastDateInput).val().split("/") : new Date().toLocaleString('fr', {day: 'numeric', month: 'numeric', year: 'numeric'}).split("/"))
                                    : new Date().toLocaleString('fr', {day: 'numeric', month: 'numeric', year: 'numeric'}).split("/");
        const newInputDate = addDays(new Date(`${y}-${m}-${d}`), daysIncrement);
        const endBoucle = lastTimeInputs ? 
                                    ($(lastTimeInputs).find("input[type='text']").toArray().length > 2 ? $(lastTimeInputs).find("input[type='text']").toArray().length: 3) 
                                : 3
        for(let i = 0; i < endBoucle; i++) {
            let lastCreneauVal = '';
            lastCreneauVal = lastTimeInputs ? ($(lastTimeInputs).find("input[type='text']").toArray()[i] ? $($(lastTimeInputs).find("input[type='text']").toArray()[i]).val() : '') : '';
            // mylog.log("input time val", lastCreneauVal);
            const inputPref = Date.now().toString(36) + Math.random().toString(36).substring(2);
            timeInputs += `<input type="text" class="input-pref form-control myTimeInput" id="inputPref_${inputPref}" placeholder="créneau ${i+1}" value="${lastCreneauVal}" data-inputparent="date_${idContainerUnik}" data-parent="inputsPref${idInputPrefUnik}"/>`
            timeInputsId += 'inputPref_'+inputPref;
            if(i < endBoucle -1) {
                timeInputsId += ',';
            }
        }
        return `
            <div id="dateContainer${idContainerUnik}" class="col-xs-12 mb-2 p-xs-0">
                <div class="col-xs-12 p-xs-0" id="blockdate${idContainerUnik}">
                    <div id="dateinput${idContainerUnik}" class="col-xs-10 pl-xs-0">
                        <label for="date_${idContainerUnik}">Date</label>
                        <input type="text" id="date_${idContainerUnik}" data-id="date_${idContainerUnik}" value="${newInputDate}" placeholder="${tradForm.selectDate}" class="my-form-input date-from-value myDateTimeInput"/>
                        <span class="border"></span>
                    </div>
                    <div id="blockoption${idContainerUnik}" class="col-xs-2 pr-xs-0" style="position: absolute; left: 10%">
                        <a class="btn btn-xs btn-danger removeDateBlock" data-id="dateContainer${idContainerUnik}" href="javascript:;">
                            <i class="fa fa-trash"></i>
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 p-xs-0" id="blockPref${idContainerUnik}">
                    <div class="col-xs-10 pl-xs-0 timeInputContainer" id="inputsPrefBlock${idInputPrefUnik}">
                        ${timeInputs}
                    </div>
                    <div class="col-xs-2 pr-xs-0" style="padding-top: 8px">
                        <a class="btn btn-xs btn-danger removeTimeElmt" id="inputsPref${idInputPrefUnik}Remove" data-idparent="inputsPrefBlock${idInputPrefUnik}" data-keyparent="date_${idContainerUnik}" data-key="${idInputPrefUnik}" data-id="${timeInputsId}" href="javascript:;">
                            <i class="fa fa-minus"></i>
                        </a>
                        <a class="btn btn-xs btn-success addTimeElmt" id="inputsPref${idInputPrefUnik}Add" data-idparent="inputsPrefBlock${idInputPrefUnik}" data-keyparent="date_${idContainerUnik}" data-key="${idInputPrefUnik}" data-id="${timeInputsId}" href="javascript:;">
                            <i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
        `;
        /* <div class="col-xs-12" style="margin-top: -10px">
                        <span class="input-placeholder">${tradForm.addTimeSlots}</span>
                    </div> */
    }
    
    $(document).ready(function () {
        let formCleSondageD = <?php echo json_encode($cle); ?>;
        let formIdSondageD = <?php echo isset($form["id"]) ? json_encode((string)$form["id"]) : 'null'; ?>;
        var coDateAnswres = <?php echo json_encode( $answers ); ?>;
        var ownParentForm =  <?php echo json_encode($parentForm) ?>;
        let kunikCodate = <?php echo json_encode($form['id']); ?>;
        $.datetimepicker.setLocale('fr');

        $.fn.hasScrollWidthBar = function() {
            return this.get(0) ? this.get(0).scrollWidth > this.width() : false;
        }

        function checkIfCanScrollLeftOrRight(idContainer) {
            let $container = $(idContainer);
            if(($container.width() + $container.scrollLeft()) == $container.width()) {
                return 'tooleft'
            }
            if((Math.round(($container.width() + $container.scrollLeft())*2) / 2).toFixed()*1 == $container[0].scrollWidth) {
                return 'tooright'
            }
        }

        $(".myDateTimeInput").datetimepicker({
            lazyInit: true,
            timepicker: false,
            lang: "fr",
            format: "d/m/Y",
            timepicker:false
        }, function(event) {event.stopImmediatePropagation()});
        $(".myTimeInput").datetimepicker({
            lazyInit: true,
            datepicker: false,
            lang: "fr",
            format: "H:i"
        }, function(event) {event.stopImmediatePropagation()});

        setTimeout(() => {
            $.each($(".codateContainer"), function() {
                if($(this).hasScrollWidthBar()) {
                    let that = this;
                    $.each($(that).find("button.codate"), function() {
                        $(this).show();
                    })
                } else {
                    let that = this;
                    $.each($(that).find("button.codate"), function() {
                        $(this).hide();
                    })
                }
            })
        }, 756); 
        $(window).on('resize', function(event) {
            event.stopImmediatePropagation()
        
            $.each($(".codateContainer"), function() {
                if($(this).hasScrollWidthBar()) {
                    let that = this;
                    $.each($(that).find("button.codate"), function() {
                        $(this).show();
                    })
                } else {
                    let that = this;
                    $.each($(that).find("button.codate"), function() {
                        $(this).hide();
                    })
                }
            })   
        });

        $("div.stepNumber").off().on("click", function(event) {
            setTimeout(() => {
                $.each($(".codateContainer"), function() {
                    if($(this).hasScrollWidthBar()) {
                        let that = this;
                        $.each($(that).find("button.codate"), function() {
                            $(this).show();
                        })
                    } else {
                        let that = this;
                        $.each($(that).find("button.codate"), function() {
                            $(this).hide();
                        })
                    }
                })
            }, 756);
        });

        $("span.stepDesc").off().on("click", function() {
            setTimeout(() => {
                $.each($(".codateContainer"), function() {
                    if($(this).hasScrollWidthBar()) {
                        let that = this;
                        $.each($(that).find("button.codate"), function() {
                            $(this).show();
                        })
                    } else {
                        let that = this;
                        $.each($(that).find("button.codate"), function() {
                            $(this).hide();
                        })
                    }
                })
            }, 756);
        })

        $(document).on('click', '.removeDateBlock', function(event) {
            event.stopImmediatePropagation();
            let self = this;
            bootbox.dialog({
                title: trad.confirmdelete,
                message: "<span class='text-red bold'><i class='fa fa-warning'></i> "+trad.actionirreversible+"</span>",
                buttons: [
                {
                    label: "Ok",
                    className: "btn btn-primary pull-left",
                    callback: function() {
                        $(`#${$(self).attr('data-id')}`).remove()         
                    }
                },
                {
                    label: "Annuler",
                    className: "btn btn-default pull-left",
                    callback: function() {}
                }
                ]
            });
        });

        $(document).on('click', '.removeTimeElmt', function(event) {
            event.stopImmediatePropagation();
            let self = this;
            let allInputsPrefId = $(self).attr('data-id').split(',');
            bootbox.dialog({
                    title: trad.confirmdelete,
                    message: "<span class='text-red bold'><i class='fa fa-warning'></i> "+trad.actionirreversible+"</span>",
                    buttons: [
                    {
                        label: "Ok",
                        className: "btn btn-primary pull-left",
                        callback: function() {
                            if(allInputsPrefId.length > 0) {
                                $(`#${allInputsPrefId[allInputsPrefId.length - 1]}`).remove();
                                allInputsPrefId.splice(allInputsPrefId.length - 1, 1);
                                // mylog.log("pref id", allInputsPrefId);
                                $(self).attr('data-id', allInputsPrefId.toString());
                                $(`#inputsPref${$(self).attr('data-key')}Add`).attr('data-id', allInputsPrefId.toString())
                            }
                        }
                    },
                    {
                        label: "Annuler",
                        className: "btn btn-default pull-left",
                        callback: function() {}
                    }
                    ]
                });
        });

        $(document).on('click', '.addTimeElmt', function(event) {
            event.stopImmediatePropagation();
            const inputPref = Date.now().toString(36) + Math.random().toString(36).substring(2);
            let self = this;
            let allInputsPrefId = $(self).attr('data-id').split(',');
            allInputsPrefId[0] == '' ? allInputsPrefId.splice(0,1) : '';
            $(`#${$(this).attr('data-idparent')}`).append(`
                <input type="text" class="input-pref form-control myTimeInput" id="inputPref_${inputPref}" placeholder="créneau ${allInputsPrefId.length+1}" data-inputparent="${$(self).attr('data-keyparent')}" data-parent="inputsPref${$(self).attr('data-key')}"/>
            `);
            allInputsPrefId.push(`inputPref_${inputPref}`);
            $(self).attr('data-id', allInputsPrefId.toString());
            $(`#inputsPref${$(self).attr('data-key')}Remove`).attr('data-id', allInputsPrefId.toString());
            $(".myTimeInput").datetimepicker({
                lazyInit: true,
                datepicker: false,
                lang: "fr",
                format: "H:i"
            }, function(event) {event.stopImmediatePropagation()});
        });
        $('.addDateBlock').off().on('click', function() {
            const idContainerUnik = Date.now().toString(36) + Math.random().toString(36).substring(2);
            const idInputPrefUnik = Date.now().toString(36) + Math.random().toString(36).substring(2);
            const idParent = $(this).attr('data-idparent');
            const daysIncrement = $(this).parent().find("input").val();
            $(`#${idParent}`).append(generateDateBlock(idContainerUnik, idInputPrefUnik, idParent, daysIncrement));
            $(".myDateTimeInput").datetimepicker({
                lazyInit: true,
                timepicker: false,
                lang: "fr",
                format: "d/m/Y",
                timepicker:false
            }, function(event) {event.stopImmediatePropagation()});
            $(".myTimeInput").datetimepicker({
                lazyInit: true,
                datepicker: false,
                lang: "fr",
                format: "H:i"
            }, function(event) {event.stopImmediatePropagation()});
        });

        $('.saveDateSondage').on('click', function(event) {
            event.stopImmediatePropagation();
            let jsonValue = $(`#${$(this).attr('data-datecontainerid')} input.date-from-value`).map(function() {
                let self = this;
                let subValues = $(`input[data-inputparent='${$(this).attr('data-id')}']`).map(function() {
                    var subSelf = this;
                    if($(subSelf).val()) return [$(subSelf).val()]
                }).get();
                
                if($(this).val() && subValues.length > 0)
                    return {
                        [$(this).val()] : subValues
                    }
            }).get();
            let resObj = jsonValue.reduce((obj, item) => {
                if(obj[Object.keys(item)[0]]) {
                    obj[Object.keys(item)[0]] = Array.from(new Set(obj[Object.keys(item)[0]].concat(Object.values(item)[0]).sort()));
                } else {
                    obj[Object.keys(item)[0]] = Object.values(item)[0].sort();
                }
                return obj
            }, {});
            let valueUtil = Object.keys(resObj).sort(function(a, b) {
                const newA = a.split('/').reverse().join('-');
                const newB = b.split('/').reverse().join('-');
                return +new Date(newA) - +new Date(newB)
            }).reduce((acc, key) => {
                acc[key] = resObj[key];
                return acc
            }, {})
            if(Object.keys(valueUtil).length > 0) {
                const valueToSend = {
                    id : $(this).data("id"),
                    collection: $(this).data("collection"),
                    path: $(this).data("path"),
                    value: {
                        alldates: valueUtil,
                        eventduration: $(`#${$(this).attr('data-datecontainerid')} input.eventtimeduration`).val()
                    }
                };
                dataHelper.path2Value( valueToSend, function(params) { 
                    $(".config-sondage").modal('hide');
                    reloadInput(formCleSondageD, formIdSondageD);
                    //urlCtrl.loadByHash(location.hash)
                } );
            } else toastr.warning(tradDynForm['This field is required.'])
            // mylog.log("valueUtil", valueUtil);
        });

        $('.coDateSaveVote').on('click', function(event) {
            event.stopImmediatePropagation();
            let self = this;
            let checkBoxName = $(this).attr('data-checkboxname');
            let own = {};
            let answerToSend = {};
            $.each($(`input[name='${checkBoxName}']:checked`), function() {
                const keyDate = $(this).attr('data-date');
                const valueTime = $(this).attr('data-time');
                own[keyDate] ? own[keyDate].push(valueTime) : own[keyDate] = [valueTime];
            });
            // mylog.log("own value to send", own);
            answerToSend.path = $(this).attr("data-path");
            answerToSend.collection = $(this).attr("data-collection");
            answerToSend.id = $(this).attr("data-answerid") ? $(this).attr("data-answerid") : answerObj._id.$id;
            answerToSend.value = own;
            // mylog.log("dataToSend to send", answerToSend);
            dataHelper.path2Value(answerToSend , function(params) { 
                toastr.success(tradForm.modificationSave);
                
                reloadInput($(self).attr("data-formcle"), $(self).attr("data-formid"));
            } );
        });

        $('.editResponseCodateBtn').on('click', function(event) {
            event.stopImmediatePropagation();
            let resInputs = $(this).closest("tr").find($("input[type='checkbox']")).toArray();
            let voteInputs = $(`input[name='${$(this).attr("data-checkboxname")}']`);
            $(`button[data-checkboxname='${$(this).attr("data-checkboxname")}']`).attr("data-answerid", $(this).attr('data-id'));
            $.each(resInputs, function(key, val) {
                $(this)[0].checked ? $(voteInputs[key]).prop("checked", true) : $(voteInputs[key]).removeAttr("checked")
            });
        })

        $('.userNotLoggedName').on('blur', function(event) {
            event.stopImmediatePropagation();
            const citoyenToSave = {
                id: answerObj._id.$id,
                path: $(this).attr('data-path'),
                collection: $(this).attr("data-collection"),
                value: {name: $(this).val()}
            }
            dataHelper.path2Value(citoyenToSave , function(params) { 
                toastr.success(tradForm.modificationSave);
                
                reloadInput($(self).attr("data-formcle"), $(self).attr("data-formid"));
            } )
        });

        $('.scroll-right').on("click", function(event) {
            event.stopImmediatePropagation();
            const pos = $("#"+$(this).attr("data-id")).scrollLeft();
            const toScroll = $(window).width() < 576 ? 100 : 200;
            $("#"+$(this).attr("data-id")).animate({scrollLeft: pos + toScroll}, 800);
            $("#"+$(this).attr("data-id")).find(".scroll-left").removeClass("disabled");
            if(checkIfCanScrollLeftOrRight("#"+$(this).attr("data-id")) == 'tooright') $(this).addClass("disabled")
        });

        $('.scroll-left').on("click", function(event) {
            event.stopImmediatePropagation();
            const pos = $("#"+$(this).attr("data-id")).scrollLeft();
            const toScroll = $(window).width() < 576 ? 100 : 200;
            $("#"+$(this).attr("data-id")).animate({scrollLeft: pos - toScroll}, 800);
            $("#"+$(this).attr("data-id")).find(".scroll-right").removeClass("disabled");
            if(checkIfCanScrollLeftOrRight("#"+$(this).attr("data-id")) == 'tooleft') $(this).addClass("disabled")
        });

        $(".codateContainer").on("scroll", function(event) {
            event.stopImmediatePropagation();
            if(checkIfCanScrollLeftOrRight("#"+$(this).attr("id")) == 'tooleft') 
                $("#"+$(this).attr("id")).find(".scroll-left").addClass("disabled");
                else $("#"+$(this).attr("id")).find(".scroll-left").removeClass("disabled");
            if(checkIfCanScrollLeftOrRight("#"+$(this).attr("id")) == 'tooright') 
                $("#"+$(this).attr("id")).find(".scroll-right").addClass("disabled");
                else $("#"+$(this).attr("id")).find(".scroll-right").removeClass("disabled")
        })

        $(".createEvent").on("click", function(event) {
            // dyFObj.openForm('event')
            let self = this;
            event.stopImmediatePropagation();
            bootbox.dialog({
                    title: trad.confirm,
                    message: `<span class='text-success bold'><i class='fa fa-info'></i> ${tradForm.wouldYouLikeToCreateTheEvent} ${$(self).attr("data-titleQuestion")}<br><span class="text-black">${$(self).attr("data-date")} ${trad.on} ${$(self).attr("data-time")}</span> </span>`,
                    buttons: [
                    {
                        label: "Ok",
                        className: "btn btn-primary pull-left",
                        callback: function() {
                            const allParent = ownParentForm["parent"] ? ownParentForm["parent"] : undefined;
                            const oneParent = allParent ? {[Object.keys(allParent)[0]]: allParent[[Object.keys(allParent)[0]]]} : null
                            const [day, month, year] = $(self).attr("data-date") ? $(self).attr("data-date").split("/") : null;
                            let startDate = `${$(self).attr("data-date")} ${$(self).attr("data-time")}`;
                            let endDate = `${$(self).attr("data-date")} 23:59`;
                            let dataToSend = {
                                id : $(self).attr("data-id"),
                                name : $(self).attr("data-titleQuestion"),
                                type : 'workshop',
                                organizer : oneParent,
                                startDate : startDate,
                                endDate : endDate,
                                collection : "events",
                                key : "event",
                                public : true,
                                timeZone : "Africa/Bamako",
                                preferences : {
                                    isOpenData : true,
                                    isOpenEdition : true
                                },
                            };

                            ajaxPost(
                                null,
                                baseUrl+"/co2/search/globalautocomplete",
                                {
                                    name: $(self).attr("data-titleQuestion"),
                                    searchType: 'events',
                                    indexMin: 0,
                                    indexMax: 50
                                },
                                function(res) {
                                    if(res) {
                                       if(Object.values(res.results).length > 0) {
                                        toastr.warning(trad.checkItemAlreadyExist);
                                       } else {
                                        ajaxPost(null, baseUrl+"/co2/element/save", dataToSend, function(data) {
                                            if(data.results) toastr.success(tradForm.modificationSave)
                                        }, "json");
                                       }
                                    }
                                }
                            )
                            // ajaxPost(null, baseUrl+"/co2/element/save", dataToSend, function(data) {mylog.log(data)}, "json");
                            // dataHelper.path2Value(dataToSend, function() {

                            // });
                            // mylog.log("data create event to send", dataToSend)
                        }
                    },
                    {
                        label: "Annuler",
                        className: "btn btn-default pull-left",
                        callback: function() {}
                    }
                    ]
                });
        })
    });
</script>