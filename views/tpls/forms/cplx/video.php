<?php //var_dump($canEdit); ?>
<style>
    .video-<?= $key ?>{
        display: flex;
        flex-direction: row;
        overflow-x: auto;
    }
    .frame-container-<?= $key ?>{
        position: relative;
        width : 300px;
    }
    .title-<?= $key ?>{
        text-decoration: none !important;
        white-space: nowrap !important;
        overflow: hidden !important;
        text-overflow: ellipsis !important;
        max-width: 462px !important;
        margin-bottom: 0;
    }
</style>
<div class="col-xs-12 no-padding">
    <?php if($mode == "r" || $mode == "pdf"){ ?>
    <label for="<?php echo $kunik ?>">
        <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
            <?php echo $label ?>
            <span class='video-count-<?= $key ?>'></span>
        </h4>
    </label><br/>
    <?php }else{ ?>
        <label for="<?php echo $kunik ?>">
            <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
                <?php echo $label.$editQuestionBtn ?>
                <span class='video-count-<?= $key ?>'></span>
                <?php if(Form::canAdmin((string) $parentForm["_id"])){ ?>
                    <a href="javascript:;" data-id="<?= (string) $answer["_id"] ?>" data-collection="answers" data-path="<?= $answerPath ?>" data-action="add" class="add-edit-video-<?= $key ?> btn btn-default"><i class="fa fa-plus"></i> <?php echo Yii::t("common","Add") ?> video URL </a>
                <?php } ?>
            </h4>
        </label><br/>
    <?php } ?>
    <div class="video-<?= $key ?> margin-10">
    </div>
</div>
<script>
    $(function(){
        let youtubeObj<?= $key ?> = {
            formId : <?= json_encode((string) $parentForm["_id"]) ?>,
            answerId : <?= json_encode((string) $answer["_id"]) ?>,
            answer : {},
            answerPath : <?= json_encode($answerPath) ?>.slice(0, -1),
            youtubeLink : [],
            canEditForm : <?= json_encode(Form::canAdmin((string) $parentForm["_id"]) && $mode=="fa" ) ?>,
            canEditAnswer : <?= json_encode(Form::canAdmin((string) $parentForm["_id"]) || json_encode(Yii::app()->session['userId'] == $answer["use"]) ) ?>,
            init : function(yobj){
                yobj.views.showVideo(yobj);
                yobj.events(yobj);
            },
            ajax : {
                getAnswer : function(yobj,callback){
                    ajaxPost(
                        null,
                        baseUrl+"/co2/element/get/type/answers/id/"+yobj.answerId,
                        null,
                        function(data){
                            mylog.log(data,"datatoa")
                            if(data.result){
                                yobj.answer = data.map;
                                callback(yobj.answer);
                            } 
                        },
                        function (data){
                                    
                        },null,{async:true}
                    ); 
                }
            },
            views : {
                showVideo : function(yobj){
                    yobj.ajax.getAnswer(yobj,function(data){
                        if(exists(jsonHelper.getValueByPath(data,yobj.answerPath))){
                            var counter = Object.keys(jsonHelper.getValueByPath(data,yobj.answerPath)).length;
                            var html = "";
                            $.each(jsonHelper.getValueByPath(data,yobj.answerPath),function(k,v){
                                var videoId = "";
                                var src = "";
                                

                                if (notEmpty(v.url) && v.url.indexOf("=") != -1) {
                                    videoId = (((v.url).split("=")).reverse())[0];
                                } else if(notEmpty(v.url)){
                                    videoId = (((v.url).split("/")).reverse())[0];
                                }

                                if (notEmpty(v.url) && v.url.indexOf("vimeo") != -1) {
                                    src = "https://player.vimeo.com/video/"+videoId
                                }else if (notEmpty(v.url) && v.url.indexOf("youtu") != -1) {
                                    src = "https://www.youtube.com/embed/"+videoId
                                }else if (notEmpty(v.url) && v.url.indexOf("dailymotion") != -1) {
                                    src = "https://www.dailymotion.com/embed/video/"+videoId
                                }else if (notEmpty(v.url) && v.url.indexOf("peertube.communecter") != -1) {
                                    src = "https://peertube.communecter.org/videos/embed/"+videoId
                                }
                                html+=`
                                    <div class="frame-container-<?= $key ?>">
                                        <p class="title-<?= $key ?> padding-left-0 margin-bottom-0">${v?.title}`;
                                            if(yobj.canEditAnswer){
                                                html+=`<div class="btn-group" role="group" aria-label="...">
                                                        <button type="button" data-pos="${k}" data-action="edit"   class="btn btn-xs btn-default add-edit-video-<?= $key ?>">${trad.edit}</button>
                                                        <button type="button" data-pos="${k}" class="btn btn-xs btn-default delete-video-<?= $key ?>">${trad.delete}</button>
                                                    </div>`;
                                            }
                                html+=  `</p>
                                        <iframe width="300" height="" src="${src}" allowfullscreen="allowfullscreen">
                                        </iframe>
                                    </div>
                                `
                            })
                            $(".video-<?= $key ?>").html(html);
                            yobj.events(yobj);
                            if(counter!=0)
                                $(".video-count-<?= $key ?>").text("("+counter+")");
                        }
                    });
                }
            },
            dynform :  function(yobj,action,pos=null){
                var df = {
                    "jsonSchema" : {
                        "title" : "Configuration",
                        "description" : "video",
                        "icon" : "cog",
                        "properties" : {
                            title : {
                                inputType : "text",
                                label : tradDynForm.titleurl,
                            },
                            /*description : {
                                inputType : "text",
                                label : tradDynForm.shortDescription,
                            },*/
                            url : {
                                inputType : "text",
                                label : "URL video"
                            }
                        },
                        save : function (data) {
                            if(yobj.canEditAnswer){
                                delete data.collection; delete data.scope;
                                var tplCtx = {
                                    id : answerId,
                                    collection : "answers",
                                    value : data
                                }
                                if(action=="add"){
                                    tplCtx.arrayForm = true;
                                    tplCtx.edit = "false";
                                    tplCtx.path = yobj.answerPath;
                                }else if(action=="edit"){
                                    tplCtx.path = yobj.answerPath+"."+pos;
                                }

                                if(typeof tplCtx.value == "undefined")
                                    toastr.error('value cannot be empty!');
                                else {
                                    dataHelper.path2Value( tplCtx, function(params) {
                                        toastr.success(trad.saved);
                                        dyFObj.closeForm();
                                        yobj.views.showVideo(yobj);
                                        yobj.events(yobj);
                                    });
                                }
                            }
                        }
                    }
                }
                return df;
            },
            events: function(yobj){
                if(yobj.canEditAnswer){
                    $('.add-edit-video-<?= $key ?>').off().on('click',function(){
                        var btn = $(this);
                        if(btn.data('action') == "add"){
                            dyFObj.openForm(yobj.dynform(yobj,"add"),null,null,null,null,{
                                        type: "bootbox",
                                        notCloseOpenModal:true
                                    });
                        }else if(btn.data('action') == "edit"){
                            mylog.log(jsonHelper.getValueByPath(yobj.answer,yobj.answerPath+"."+btn.data("pos")),"stalonne")
                            dyFObj.openForm(
                                yobj.dynform(yobj,"edit",btn.data("pos")),
                                null,
                                jsonHelper.getValueByPath(yobj.answer,yobj.answerPath+"."+btn.data("pos")),
                                null,null,
                                {
                                    type: "bootbox",
                                    notCloseOpenModal:true
                                }
                            );
                        }
                    })
                    $('.delete-video-<?= $key ?>').off().on('click',function(){
                        var btn = $(this);
                        bootbox.confirm(`<h6 class="text-center text-danger">${trad.areyousure}</h6>`, function(result){ 
                            if(result){
                                var tplCtx = {
                                    id : yobj.answerId,
                                    collection : "answers",
                                    path : yobj.answerPath+"."+btn.data("pos"),
                                    pull : yobj.answerPath,
                                    value : null,
                                }
                                dataHelper.path2Value(tplCtx, function(params) { 
                                    if(params.result){
                                        toastr.success(trad.deleted)
                                        yobj.views.showVideo(yobj);
                                        yobj.events(yobj);
                                    }
                                })
                            }
                        });
                    })
                }
            }
        }
        youtubeObj<?= $key ?>.init(youtubeObj<?= $key ?>);
    })
</script>