<style type="text/css">
	.ckd-grp {
  /*position: absolute;
  top: calc(50% - 10px);*/
}

    .ckd-grp .container<?= $kunik ?> {
	display: block;
	position: relative;
	padding-left: 35px;
	padding-top: 0px;
	margin-bottom: 12px;
	cursor: pointer;
	font-size: 22px;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	}

	/* Hide the browser's default radio button */
	.ckd-grp .ckbCo.<?= $kunik ?> {
	position: absolute;
	opacity: 0;
	cursor: pointer;
	}

	/* Create a custom radio button */
	.ckd-grp .checkmark<?= $kunik ?> {
	position: absolute;
	top: 0;
	left: 0;
	height: 25px;
	width: 25px;
	background-color: #eee;
	}

	/* On mouse-over, add a grey background color */
	.container<?= $kunik ?>:hover .checkmark<?= $kunik ?>{
	background-color: #ccc;
	}

	/* When the radio button is checked, add a blue background */
	input.ckbCo.<?= $kunik ?>:checked + .container<?= $kunik ?> .checkmark<?= $kunik ?>, input.ckbCo.<?= $kunik ?>:active + .container<?= $kunik ?> .checkmark<?= $kunik ?>{
	background-color: #2196F3;
	}

	/* Create the indicator (the dot/circle - hidden when not checked) */
	input.ckbCo.<?= $kunik ?> + .container<?= $kunik ?> .checkmark<?= $kunik ?>:after {
	    content: "";
	    position: absolute;
	    display: none;
		font-size: 15px;
		color: white;
	}
	.rank:after {
		content: attr(data-rank) !important;
	}

	/* Show the indicator (dot/circle) when checked */
	input.ckbCo.<?= $kunik ?>:checked + .container<?= $kunik ?> .checkmark<?= $kunik ?>:after,  input.ckbCo.<?= $kunik ?>:active + .container<?= $kunik ?> .checkmark<?= $kunik ?>:after{
	    display: block;
	}




/*.ckd-grp label {
  cursor: pointer;
  -webkit-tap-highlight-color: transparent;
  padding: 6px 8px;
  border-radius: 20px;
  float: left;
  transition: all 0.2s ease;
}
.ckd-grp label:hover {
  background: rgba(125,100,247,0.06);
}
.ckd-grp label:not(:last-child) {
  margin-right: 16px;
}
.ckd-grp label span {
  vertical-align: middle;
}
.ckd-grp label span:first-child {
  position: relative;
  display: inline-block;
  vertical-align: middle;
  width: 27px;
  height: 27px;
  background: #e8eaed;
  border-radius: 10px;
  transition: all 0.2s ease;
  margin-right: 8px;
   padding: 3px;
}
.ckd-grp label span:first-child:after {
  content: '';
  position: absolute;
  width: 16px;
  height: 16px;
  margin: 2px;
  background: #fff;
  border-radius: 6px;
  transition: all 0.2s ease;
}
.ckd-grp label:hover span:first-child {
  /*background: #7d64f7;
}*/

.ckb-grp label:hover span:first-child:after {
  /*background: #7d64f7;*/
  /*background: #7d64f7;*/
  padding: 0px;
  background: white;

}

.ckd-grp input {
  display: none;
}
.ckd-grp input:checked + label span:first-child {
  /*background: #7d64f7;*/
  background: #e8eaed;
}
.ckd-grp input:checked + label span:first-child:after {
  /*transform: scale(0.5);*/
  /* background: #7d64f7; */
  background: transparent;
}

/*
couleur rouge
background:#f5b7b8

lightblue : background-color: #deeffd;;
*/

.multiChbtextInp{
	padding: 0px 5px;
	border: 0;
	/*background-color:unset;*/
}

.ckd-grp input:checked + label .multiChbtextInp{border-bottom: solid 1px #2196F3;}
.ckd-grp input:checked + label .multiChbtextInp:hover{cursor:text;}

.ckd-grp input + label .multiChbtextInp{border-bottom: solid 1px #eee;}

.ckd-grp input + label .multiChbtextInp:hover{cursor:pointer;}

/*.multiChbtextInp{ border-bottom: 1px solid #ccc;}*/

/*.ckd-grp input:checked + label .multiChbtextInp:not(:placeholder-shown){border-bottom: 1px; }*/


/*.multiChbtextInp ~ .focus-border{position: absolute; bottom: 0; left: 0; width: 0; height: 2px; background-color: #3399FF; transition: 0.4s;}*/

.ckd-grp input:checked + label .multiChbtextInp:not[value=""]{background-color: unset;}

.ckd-grp input:checked + label .multiChbtextInp:focus, .ckd-grp input:checked + label .multiChbtextInp[value=""]{background-color: #deeffd;}

.paramsonebtn , .paramsonebtnP {
	font-size: 17px;
	display: none;
	padding: 5px;
}

.paramsonebtn:hover {
	color: red;
}

.paramsonebtnP:hover {
	color: blue;
}

.thckd:hover .paramsonebtn, .thckd:hover .paramsonebtnP {
	display: inline-block;
}

.multiChbtextInp:focus {
	outline: none !important;
}

.responsemulticheckboxplus:before{
	content: '\f0a9';
	margin-right: 15px;
	font-family: FontAwesome;
	color: #d9534f;
}

.ckboptinfo{
	font-size: 15px;
	color: #637381;
}

/* radio bouton for rank */
.srblist {
	 min-width: 3rem;
	 padding: 7px;
}
.srblist__item {
	 position: relative;
}
.srblist__item:hover > .srblabel {
	 color: #2196F3;
}
.srblist__item:hover > .srblabel::before {
	 border: 0.5rem solid #2196F3;
	 margin-right: 1.2rem;
}
.radio-btn {
	 position: absolute;
	 opacity: 0;
	 visibility: hidden;
}
.radio-btn:checked + .srblabel {
	 color: #2196F3;
}
.radio-btn:checked + .srblabel::before {
	 margin-right: 1.2rem;
	 border: 0.5rem solid #2196F3;
	 background: #2b2a2a;
}
.srblabel {
	 display: flex;
	 align-items: center;
	 padding: 0.75rem 0;
	 color: #2b2a2a;
	 font-size: 1.6rem;
	 text-transform: uppercase;
	 cursor: pointer;
	 transition: all 0.25s linear;
}
.srblabel::before {
	 display: inline-block;
	 content: "";
	 height: 1.6rem;
	 width: 1.6rem;
	 margin-right: 0.625rem;
	 border: 0.5rem solid #2b2a2a;
	 border-radius: 50%;
	 transition: all 0.25s linear;
}

</style>



<?php
$cle = $key ;
$value = (!empty($answers)) ? " value='".$answers."' " : "";
$inpClass = " saveOneByOne";

$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";


$subkey = $key;


$paramsData = [
	"list" => [	],
	"tofill" => [ ],
	"optinfo" => [ ],
	"placeholdersckb" => [ ],
	"type" => [
		"simple" => "Sans champ de saisie",
		"cplx" => "Avec champ de saisie"
	],
	'checked' => [],
	"width" => [
		"12" => "1",
		"6" => "2",
		"4" => "3"
	],
	'dependOn' => [],
	'nbAnswersMax' => 20,
	'rank' => 'false',
	'addValue' => 'false',
	'mandatoryCplx' =>true
];

# Depended Answer
$checkboxs = Array();
$inputs = Array();
//$inputsPath = PHDB::findOne(Form::COLLECTION, ["id"=> $form["id"]]);
$inputsPath = $form;
if(!empty($inputsPath["inputs"]))
	foreach($inputsPath["inputs"] as $inputKey => $inputValue){
		if($inputKey!=$key && strpos($inputValue["type"], 'multiCheckbox') !== false){
			$inputs[$inputKey] = $inputValue["label"];
		}
	}
$paramsData["dependOn"] = $inputs;
$allRanks = [];

if (isset($input["conditional"])) {
 	foreach ($input["conditional"] as $key => $value) {

 ?>
 	<script type="text/javascript">
 		jQuery(document).ready(function() {
	 		$("#<?php echo $value["value"] ?>").change(function() {
			    if($(this).prop('checked')) {
			        $('#question<?php echo $value["input"] ?>').removeClass("hide");
			        //scrollintoDiv("question<?php echo $value["input"] ?>", 1000);
			    }
			});
			if($("#<?php echo $value["value"] ?>").is(':checked')) {
			    $('#question<?php echo $value["input"] ?>').removeClass("hide");
			    // scrollintoDiv("question<?php echo $value["input"] ?>");
			}

			$('.ckbCo.<?php echo $kunik ?>').change(function(){
				if(!$("#<?php echo $value["value"] ?>").prop('checked')) {
			        $('#question<?php echo $value["input"] ?>').addClass("hide");
			    }
			});

		});
 	</script>
 	<style type="text/css">
 		#question<?php echo $value["input"] ?> {
		    -webkit-transition: all 2s ease;
		    -moz-transition: all 2s ease;
		    -o-transition: all 2s ease;
		    transition: all 2s ease;
		}
 	</style>

 <?php
	}
 }

if(isset($parentForm["params"][$kunik]["global"]["dependOn"]) && $parentForm["params"][$kunik]["global"]["dependOn"]!=""){
	$dependedOn = $parentForm["params"][$kunik]["global"]["dependOn"];
	//var_dump($parentForm["params"][$kunik]["global"]["dependOn"]);
	if(isset($answer["answers"][$form["id"]]["multiCheckboxPlus".$dependedOn])){
		//var_dump($answer["answers"][$form["id"]]["multiCheckboxPlus".$dependedOn]);exit;
		foreach ($answer["answers"][$form["id"]]["multiCheckboxPlus".$dependedOn] as $dependKey => $dependValue) {
			$dependValue=key($dependValue);
			//var_dump($dependValue);exit;
			array_push($checkboxs, $dependValue);
		}
	}
}else{
	# No answer to depended on
	if(isset($parentForm["params"][$kunik]["global"]['list']))
		$checkboxs = $parentForm["params"][$kunik]["global"]['list'];
}


if( isset($parentForm["params"][$kunik]) ) {
	if(isset($parentForm["params"][$kunik]['mandatoryCplx'])){
		$paramsData['mandatoryCplx']=$parentForm["params"][$kunik]['mandatoryCplx'];
	}
	if(isset($parentForm["params"][$kunik]['global']['nbAnswersMax'])) {
		$paramsData['nbAnswersMax'] = $parentForm["params"][$kunik]['global']['nbAnswersMax'];
	}
	if(isset($parentForm["params"][$kunik]['global']['rank'])) {
		$paramsData['rank'] = $parentForm["params"][$kunik]['global']['rank'];
	}
	if(isset($parentForm["params"][$kunik]['global']['addValue'])) {
		$paramsData['addValue'] = $parentForm["params"][$kunik]['global']['addValue'];
	}

	if( isset($parentForm["params"][$kunik]["global"]["list"]) ) {
		$paramsData["list"] =  $parentForm["params"][$kunik]["global"]["list"];
		foreach ($paramsData["list"] as $k => $v) {
			if(isset($parentForm["params"][$kunik]["tofill"][$v]) ){
			    $paramsData["tofill"] += array($v => $parentForm["params"][$kunik]["tofill"][$v]);
			} else {
			    $paramsData["tofill"] += [$v => "simple"];
			}

			if(isset($parentForm["params"][$kunik]["optinfo"][$v]) ){
			    $paramsData["optinfo"] += array($v => $parentForm["params"][$kunik]["optinfo"][$v]);
			} else {
			    $paramsData["optinfo"] += [$v => ""];
			}

			$paramsData["optimage"][$v] = null;
		}

	}
}

if( isset($parentForm["params"][$kunik]) ) {
	if( isset($parentForm["params"][$kunik]["global"]["list"]) ) {
		//$paramsData["list"] =  $parentForm["params"][$kunik]["global"]["list"];
		foreach ($paramsData["list"] as $k => $v) {
			if(isset($parentForm["params"][$kunik]["placeholdersckb"][$v]) ){
			    $paramsData["placeholdersckb"] += array($v => $parentForm["params"][$kunik]["placeholdersckb"][$v]);
			} else {
			    $paramsData["placeholdersckb"] += [$v => ""];
			}
		}
	}
}

if( isset($answer["answers"][$form["id"]][$kunik]) ) {
	$paramsData["checked"] =  $answer["answers"][$form["id"]][$kunik];
}

if($paramsData['rank'] == 'true') {
?>
<style>
	/* Style the indicator (dot/circle) */
	.container<?= $kunik ?> .checkmark<?= $kunik ?>:after {
		left: 30%;
		right: auto;
		bottom: 1px;
	}
</style>
<?php
} else {
?>
<style>
	/* Style the indicator (dot/circle) */
	.container<?= $kunik ?> .checkmark<?= $kunik ?>:after {
		left: 9px;
	    top: 5px;
	    width: 5px;
	    height: 10px;
	    border: solid white;
	    border-width: 0 3px 3px 0;
	    -webkit-transform: rotate(45deg);
	    -ms-transform: rotate(45deg);
	    transform: rotate(45deg);
	}
</style>
<?php
}

if($mode == "r")
{
?>
   <div class="" style="padding-top: 50px; text-transform: unset;">
    	<label class="form-check-label" for="<?php echo $key ?>">
    		<h4 style="/*text-transform: unset; */color: <?php echo ($titleColor) ? $titleColor : "black"; ?>;"><?php echo $label ?>
    		</h4>
    	</label>
    	<?php
		if(!empty($info))
		{
		?>
	    	<br/><small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
	    <?php
	    }
    	?>

    	<?php

    	if(count($checkboxs)==0 && isset($dependedOn) && $dependedOn!="")
    	{
    		echo "<br/><span class='text-secondary text-center'> CETTE QUESTION DEPENDS DE VOTRE RÉPONSE SUR <a class='text-info goto".$kunik."' data-path='question".$dependedOn."' href='javascript:;'>CELLE CI</a></span>";
    	}
    	else if(count($checkboxs)==0 && !isset($dependedOn))
    	{
    		echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> VOUS DEVEZ D'ABORD AJOUTER DES OPTIONS </span>";
    	}else { ?>

			<div class="">
    			<table style="width: 100%">
  				<tr class=""><th >
  				<div id="multiResultContainer<?php echo $kunik ?>" class="col-md-12">
	    			<?php
		    		foreach ($checkboxs as $ix => $lbl)
					{
						$ckd = "";
						$place = "";
						$rankData = "";
						foreach ($paramsData["checked"] as $key => $value) {
							foreach ($value as $ke => $va) {
								if($ke == $lbl){
									$ckd = "checked";
									if(isset($va['rank'])) {
										$rankData = $va['rank'];
									} else {
										$rankData = $key+1;
									}
								}
							}
						}
						if ($ckd == "checked"){
					?>

		    			<!-- <tr class="thckd"><theih > -->
		    			<div class="col-md-<?php echo (isset($parentForm["params"][$kunik]['global']['width'])) ? $parentForm["params"][$kunik]['global']['width'] : '12'; ?> col-xs-12 col-sm-12 no-padding multiAnswer<?php echo $kunik ?>" style="min-height: 50px;" data-rank="<?php echo $rankData ?>">
		    			<div class="">

	  						<label for="<?php echo $kunik.$ix ?>" class="responsemulticheckboxplus">
	  							<span>

	  							</span>
	  							<span><?php echo $lbl ?> <?php if($paramsData['rank'] == 'true') {?> <span class="badge"><?php echo $rankData ?></span> <?php } ?>


		  								<?php
		  								if (isset($paramsData["tofill"][$lbl]) && $paramsData["tofill"][$lbl] == "cplx") {
		  								?>
		  											<?php
		  											if(isset($answer["answers"][$form["id"]][$kunik])){
		  												foreach($answer["answers"][$form["id"]][$kunik] as $ians => $vans){
		  													foreach($vans as $kans => $val){
		  														if($kans==$lbl && isset($val["value"]) && isset($val["textsup"])){
		  															//var_dump($val);exit;
		  															echo $val["textsup"];
		  														}
		  													}

		  												}
				  									}
		  											?>


				  						<?php
				  						}
				  						?>

		  							<?php

		  							if($canEditForm)
		  							{


		  								echo " <a href='javascript:;' data-id='".$parentForm["_id"]."'  data-collection='".Form::COLLECTION."'  data-path='params.".$kunik."' class='previewTpl paramsonebtn editone".$kunik."Params".$ix." '><i class='fa fa-cog'></i> </a>";



		  								if (isset($paramsData["tofill"][$lbl]) && $paramsData["tofill"][$lbl] == "cplx") {
		  												echo " <a alt='placeholder' href='javascript:;' data-id='".$parentForm["_id"]."'  data-collection='".Form::COLLECTION."'  data-path='params.".$kunik."' class='previewTpl paramsonebtnP editonep".$kunik."Params".$ix." '><i class='fa fa-pencil'></i> </a>";
		  						 				}

		  									}
		  							?>
								</span>
							</label>
	  					</div>
	  					</div>
		    		<?php
		    		}  }
		    		?>
		    	</div>
		    	</th></tr>
				</table>
			</div>
		<?php
		}
		?>

	</div>
	<script type="text/javascript">
		$(document).ready(function() {
			isRankEnabled<?php echo $kunik ?> = <?php echo json_encode( $paramsData['rank'] ); ?>;
			function sortResult() {
				var checkedElmt = $('div.multiAnswer<?php echo $kunik ?>');
				if(checkedElmt.length > 0) {
					checkedElmt.sort(function(a,b){
						return $(a).attr('data-rank')*1 - $(b).attr('data-rank')*1;
					})
					$('#multiResultContainer<?php echo $kunik ?>').empty();
					checkedElmt.each(function() {
						$('#multiResultContainer<?php echo $kunik ?>').append($(this))
					})
				}
			}
			if(isRankEnabled<?php echo $kunik ?> == 'true' || isRankEnabled<?php echo $kunik ?> == true) {
				sortResult();
			}
		});
	</script>
<?php }else { ?>


	<div class="" style="padding-top: 50px; text-transform: unset;">
    	<label class="form-check-label" for="<?php echo $key ?>">
    		<h4 style="/*text-transform: unset;*/ color: <?php echo ($titleColor) ? $titleColor : "black"; ?>;"><?php echo $label.$editQuestionBtn.$editParamsBtn ?>
    		</h4>
    	</label>
    	<?php
		if(!empty($info))
		{
		?>
	    	<br/><small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
	    <?php
	    }
	    ?>
    	<div id="checkboxList<?php echo $kunik ?>" class="ckd-grp">

    	<?php

    	if(count($checkboxs)==0 && isset($dependedOn) && $dependedOn!="")
    	{
    		echo "<br/><span class='text-secondary text-center'> CETTE QUESTION DEPEND DE VOTRE RÉPONSE SUR <a class='text-info goto".$kunik."' data-path='question".$dependedOn."' href='javascript:;'>CELLE CI</a></span>";
    	}
    	else if(count($checkboxs)==0 && !isset($dependedOn))
    	{
    		echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> VOUS DEVEZ D'ABORD AJOUTER DES OPTIONS </span>";
    	} else {
    	?>
    		<table style="width: 100%">
  				<tr class=""><th >
  				<div id="checkboxContainter<?php echo $kunik ?>" class="col-md-12 no-padding">
	    		<?php
		    		foreach ($checkboxs as $ix => $lbl)
					{
						$ckd = "";
						$place = "";
						$rankData = "";
						foreach ($paramsData["checked"] as $key => $value) {
							foreach ($value as $ke => $va) {
								if($ke == $lbl){
									$ckd = "checked";
									if(isset($va['rank'])) {
										$rankData = $va['rank'];
										$allRanks[$kunik.$ix]['rank'] = $va['rank']*1;
										$allRanks[$kunik.$ix]['value'] = $ke;
									} else {
										$rankData = $key+1;
										$allRanks[$kunik.$ix]['rank'] = $key+1;
										$allRanks[$kunik.$ix]['value'] = $ke;
									}
								}
							}
						}


					?>
		    			<!-- <tr class="thckd"><theih > -->
		    			<div class="parent-thckd col-md-<?php echo (isset($parentForm["params"][$kunik]['global']['width'])) ? $parentForm["params"][$kunik]['global']['width'] : '12'; ?> col-xs-12 col-sm-12 no-padding" style="min-height: 50px;">
		    			<div class="thckd">
			    			<input data-id="<?php echo $kunik ?>" class="ckbCo <?php echo $kunik ?>"  id="<?php echo $kunik.$ix ?>" data-form='<?php echo $form["id"] ?>' <?php echo $ckd?> type="checkbox" name="<?php echo $kunik ?>" data-type="<?php

			    				if (isset($paramsData["tofill"][$lbl]) && $paramsData["tofill"][$lbl] == "cplx")
			    					{
	  											echo "cplx";
	  								}else
	  								{
	  											echo "simple";
	  								}
	  						?>" value="<?php echo $lbl ?>" />
	  						<label class="<?php echo $paramsData['rank'] == 'true' ? 'container'.$kunik : 'container'.$kunik ?>" for="<?php echo $kunik.$ix ?>">
	  							<span class="<?php echo $paramsData['rank'] == 'true' ? 'checkmark'.$kunik.' rank' : 'checkmark'.$kunik ?> " data-rank="<?php echo $rankData ?>">

	  							</span>
	  							<span class="lblAction<?php echo $kunik.$ix ?>"><?php echo $lbl ?>
		  							<div style="position: relative; display: inline-block;">

		  								<?php
		  								if (isset($paramsData["tofill"][$lbl]) && $paramsData["tofill"][$lbl] == "cplx") {
		  								?>
		  											<input data-impname="<?php echo $kunik ?>" class="multiChbtextInp inputckbCo <?php echo $kunik ?>" data-id="<?php echo $kunik ?>" data-form='<?php echo $form["id"] ?>' type="text" data-imp="<?php echo $lbl; ?>" placeholder="<?php echo $paramsData['placeholdersckb'][$lbl]; ?>" style="display: inline-block !important; position: relative;"  value="<?php
		  											if(isset($answer["answers"][$form["id"]][$kunik])){
		  												$disabled=true;
		  												foreach($answer["answers"][$form["id"]][$kunik] as $ians => $vans){
		  													foreach($vans as $kans => $val){
		  														if($kans==$lbl && isset($val["value"]) && isset($val["textsup"])){
		  															echo $val["textsup"];
		  															$disabled=false;
		  														}
		  													}
		  												}
				  									}
		  											?>"
		  											<?php
		  											$disabled=(isset($disabled) && $disabled==true) ? "disabled" : "";
		  											// echo $disabled;

		  											?>

		  											/>
		  								 			<span class="focus-border"></span>

				  						<?php
				  						}
				  						?>
		  					 		</div>

		  							<?php

		  							if($canEditForm)
		  							{
		  								echo " <a href='javascript:;' data-id='".$parentForm["_id"]."'  data-collection='".Form::COLLECTION."'  data-path='params.".$kunik."' class='previewTpl paramsonebtn editone".$kunik."Params".$ix." '><i class='fa fa-cog'></i> </a>";

		  								if (isset($paramsData["tofill"][$lbl]) && $paramsData["tofill"][$lbl] == "cplx") {

		  									echo " <a alt='placeholder' href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."'  data-path='params.".$kunik."' class='previewTpl paramsonebtnP editonep".$kunik."Params".$ix." '><i class='fa fa-pencil'></i> </a>";

		  						 		}

		  							}
									if($ckd == "checked" && $paramsData['rank'] == 'true') {
										echo "  
												<a href='javascript:;' data-id='".$parentForm["_id"]."' data-form='".$form["id"]."' data-toggle='dropdown' data-collection='".Form::COLLECTION."' data-inputId='".$kunik.$ix."'  data-path='params.".$kunik."' data-kunik='".$kunik."' class='previewTpl paramsonebtn dropdown-toggle editRank' ><i class='fa fa-th-list tooltips' data-placement='bottom' data-original-title='Modifier rang'></i> </a>
												<ul class='dropdown-menu srblist' id='rankMenu".$kunik.$ix."'>
												</ul>
											   
											 ";
									}
		  							?>
								</span>
								<?php
							if (isset($paramsData["optinfo"][$lbl]) && $paramsData["optinfo"][$lbl] != "") {
		  						?>
								<div class="ckboptinfo"> <?php echo $paramsData["optinfo"][$lbl]?>  </div>
								<?php
									}

								if (isset($paramsData["optimage"][$lbl]) && !empty($paramsData["optimage"][$lbl])) {
									foreach ($paramsData["optimage"][$lbl] as $oikey => $oivalue) {
										if (isset($oivalue["docPath"] )) {
								?>
								<div>
									<img class="img-responsive" src="<?php echo $oivalue["docPath"] ?>">
								</div>
								<?php
										}
									}
								}

								?>
							</label>
	  					</div>
	  					</div>
		    		<?php
		    		}
		    		?>
					<?php if ($paramsData['addValue'] == "true" || $paramsData['addValue'] == true) { ?>
						<a class="btn btn-default addValuebtn<?php echo $kunik ?>" data-id="<?php echo $kunik ?>" style="border-radius: 100%;"><i class="fa fa-plus"></i></a>
						<input class="multiChbtextInp addValueInput<?php echo $kunik ?>" type="text" placeholder="Nouvelle valeur" style="display: inline-block !important; position: relative; border-bottom: solid 1px darkgrey;" value=""> 
					<?php }  ?>
		    	</div>
		    	</th></tr>
				</table>
		<?php
		}
		?>
    </div>

	</div>
<script type="text/javascript">

var <?php echo $kunik ?>Options = <?php echo json_encode( (isset($paramsData["list"])) ? $paramsData["list"] : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
isRankEnabled<?php echo $kunik ?> = <?php echo json_encode( $paramsData['rank'] ); ?>;

var canEditForm=<?php echo json_encode($canEditForm)?>;
if(typeof allRanks == 'undefined') {
	var allRanks = {
		[`<?php echo $kunik ?>`] : <?php echo json_encode($allRanks) ?>
	};
} else {
	allRanks[`<?php echo $kunik ?>`] = <?php echo json_encode($allRanks) ?>;
}


sectionDyf.<?php echo $kunik ?>ParamsPlace = "";


jQuery(document).ready(function() {
    mylog.log("render form input","/modules/costum/views/tpls/forms/cplx/multiCheckbox.php");

    answer = {};



	function changeRank(subForm = null, kunik = null) {
		var allckd = [];
		var iteration = 1;
		$("input:checkbox[name='"+kunik+"']:checked").each(function(){
			mylog.log("ckd", $(this).val());
			var key = $(this).val();
			var id =$(this).data("id");
			var rank = iteration;
			if(allRanks && allRanks[kunik] && allRanks[kunik][$(this).attr('id')] && allRanks[kunik][$(this).attr('id')]['rank']) {
				rank = allRanks[kunik][$(this).attr('id')]['rank'];
			}
			var b = { [key] : {'value' : key, 'type' : $(this).data("type"), 'rank': rank, 'textsup' : $('input[data-impname="' + id + '"]:input[data-imp="' + $(this).val() + '"]').val() }};
			allckd.push(b);
			iteration++;
		});

		answer.path = "answers."+subForm+"."+kunik;
		answer.collection = "answers" ;
		answer.id = "<?php echo $answer["_id"]; ?>";
		answer.value = allckd;
		saveMultiCheckboxPlus(answer , function(params) {
			mylog.log("callback answer",params);
			// toastr.success('Mise à jour enregistrée');
		});
	}

	$(".addValuebtn<?php echo $kunik ?>").click(function() {
		var path2ValueParams = {};
		var inputKey = $(this).data("id");
		var formId = "<?php echo (string)$parentForm["_id"] ?>";
		var subForm = "<?php echo (string)$form["id"] ?>";
		var inputValue = $("input.addValueInput" + inputKey).val();
		var allInputsValues = (jsonHelper.getValueByPath(sectionDyf, inputKey + "ParamsData.list") != undefined && Array.isArray(jsonHelper.getValueByPath(sectionDyf, inputKey + "ParamsData.list")) && jsonHelper.getValueByPath(sectionDyf, inputKey + "ParamsData.list").length > 0) ? jsonHelper.getValueByPath(sectionDyf, inputKey + "ParamsData.list") : [];
		if (inputValue != undefined && typeof inputValue == "string" && notEmpty(inputValue)) {
			allInputsValues.push(inputValue);
			jsonHelper.setValueByPath(path2ValueParams, "collection", "forms");
			jsonHelper.setValueByPath(path2ValueParams, "id", formId);
			jsonHelper.setValueByPath(path2ValueParams, "path", "params."+ inputKey +".global.list");
			jsonHelper.setValueByPath(path2ValueParams, "value", allInputsValues);
			dataHelper.path2Value( path2ValueParams, function(params) {
				reloadInput(inputKey.replace("multiCheckboxPlus", ""), subForm);
				//toastr.success('Mise à jour enregistrée');
				$("input.addValueInput" + inputKey).val("");
			});
		} else {
			toastr.error('value cannot be empty!');
		}
	})

	$(".ckbCo.<?php echo $kunik ?>").change(function(e){
		e.stopImmediatePropagation();
		var allckd = [];
		var option = $(this).val();
		var domId= $(this).attr("id");
		const kunik = $(this).attr("data-id");
		var iteration = 1;
		var isReOrdered = false;
		var rank = 0;
		if(!$("#"+domId).is(':checked')){
			isReOrdered = true;
			if(allRanks[kunik] && allRanks[kunik][domId]) {
				delete allRanks[kunik][domId];
			}
			var sortable = [];
			for (var r in allRanks[kunik]) {
				sortable.push([r, allRanks[kunik][r]]);
			}

			sortable.sort(function(a, b) {
				return a[1]['rank'] - b[1]['rank'];
			});
			allRanks[kunik] = Object.fromEntries(sortable);

		}
		const val = $("input:checkbox[name='"+$(this).attr("name")+"']:checked").length;
		if(typeof ownAnswer != 'undefined') {
			if(ownAnswer[$(this).data("form")] && val > 0) {
				ownAnswer[$(this).data("form")][kunik] = val
			} else if(ownAnswer[$(this).data("form")]){
				ownAnswer[$(this).data("form")][kunik] = ""
			}
		}
		if(typeof removeError != 'undefined') {
			if(val > 0) {
				removeError(
					$(`li#question${kunik.replace('multiCheckboxPlus', '')}`),
					{
						'border' : 'none'
					},
					'error-msg'
				)
			}
		}
		var allRanksKeys = allRanks[kunik] ? Object.keys(allRanks[kunik]) : [];
		$("input:checkbox[name='"+$(this).attr("name")+"']:checked").each(function(){
			mylog.log("ckd", $(this).val());
			var key = $(this).val();
			var id =$(this).data("id");
			rank = iteration;
			if(allRanks && allRanks[kunik] && allRanks[kunik][$(this).attr('id')] && allRanks[kunik][$(this).attr('id')]['rank']) {
				if(!isReOrdered) {
					rank = allRanks[kunik][$(this).attr('id')]['rank'];
				} else {
					if(allRanksKeys.indexOf($(this).attr('id')) > -1) {
						rank = allRanksKeys.indexOf($(this).attr('id')) + 1;
						allRanks[kunik][$(this).attr('id')]['rank'] = allRanksKeys.indexOf($(this).attr('id')) + 1;
					} else {
						rank = -1;
					}
					$(this).closest('.thckd').find('span.rank').attr('data-rank', rank)
				}
			} else {
				if(answerObj && answerObj.answers && answerObj.answers && answerObj.answers[$(this).data("form")] && answerObj.answers[$(this).data("form")][$(this).data("id")]) {
					if(!(answerObj.answers[$(this).data("form")][$(this).data("id")].length >= (sectionDyf.<?php echo $kunik ?>ParamsData['nbAnswersMax'])) && $(this).is(':checked')) {
						allRanks[kunik][$(this).attr('id')] = {
							'rank' : allRanksKeys.length + 1,
							'value' : $(this).val()
						};
					}
				}
				rank = allRanksKeys.length + 1
			}
			var b = { [key] : {'value' : key, 'type' : $(this).data("type"), 'rank': rank, 'textsup' : $('input[data-impname="' + id + '"]:input[data-imp="' + $(this).val() + '"]').val() }};
			allckd.push(b);
			iteration++;
		});

		answer.path = "answers."+$(this).data("form")+"."+$(this).data("id");
		answer.collection = "answers" ;
		answer.id = "<?php echo $answer["_id"]; ?>";
		answer.value = allckd;
		const dataForm = $(this).data("form");
		if(answerObj && answerObj.answers && answerObj.answers && answerObj.answers[$(this).data("form")] && answerObj.answers[$(this).data("form")][$(this).data("id")]) {
			if(answerObj.answers[$(this).data("form")][$(this).data("id")].length >= (sectionDyf.<?php echo $kunik ?>ParamsData['nbAnswersMax']) && $(this).is(':checked')) {
				toastr.warning('Le nombre de réponse possible est de '+ sectionDyf.<?php echo $kunik ?>ParamsData['nbAnswersMax'])
				$(this).prop('checked', false);
			} else {
				var viewRank = allRanksKeys.length + 1
				if(allRanks && allRanks[kunik] && allRanks[kunik][$(this).attr('id')] && allRanks[kunik][$(this).attr('id')]['rank']) {
					viewRank = allRanks[kunik][$(this).attr('id')]['rank'];
				}
				$(this).closest('.thckd').find('span.rank').attr('data-rank', viewRank);
				if(!isReOrdered && (isRankEnabled<?php echo $kunik ?> == 'true' || isRankEnabled<?php echo $kunik ?> == true)) {
					$('.lblAction'+domId).find('a.editRank').remove();
					$('.lblAction'+domId).find('ul.srblist').remove();
					$('.lblAction'+domId).append(`
						<a href='javascript:;' data-id='<?php echo $parentForm["_id"] ?>' data-form='<?php echo $form["id"] ?>' data-toggle='dropdown' data-collection='<?php echo Form::COLLECTION ?>' data-inputId='${domId}'  data-path='params.${kunik}' data-kunik='${kunik}' class='previewTpl paramsonebtn dropdown-toggle editRank' ><i class='fa fa-th-list tooltips' data-placement='bottom' data-original-title='Modifier rang'></i> </a>
						<ul class='dropdown-menu srblist' id='rankMenu${domId}'>
						</ul>
					`);
					bindRankEvents();
					$('.tooltips').tooltip();
				} else {
					$('.lblAction'+domId).find('a.editRank').remove();
					$('.lblAction'+domId).find('ul.srblist').remove()
				}
				saveMultiCheckboxPlus(answer , function(params) {
					mylog.log("callback answer",params);
					// toastr.success('Mise à jour enregistrée');

					if($("#"+domId).is(':checked')){
                        mylog.log("--------cehcked not empty--------");
						$('.inputckbCo.<?php echo $kunik ?>[data-imp="'+option+'"]').prop("disabled", false).focus();
						$('.inputckbCo.<?php echo $kunik ?>[data-imp="'+option+'"]').focusout(function () {
							if(<?php echo json_encode($paramsData["mandatoryCplx"]) ?>==true || <?php echo json_encode($paramsData["mandatoryCplx"]) ?>=="true"){
							    if($(this).val()==""){
								    setTimeout(
								    scrollintoDiv(domId,1000)
									,500);
								//$('body').scrollTo('#'+domId);
								    $('.inputckbCo.<?php echo $kunik ?>[data-imp="'+option+'"]').attr("placeholder","Champ obligatoire").css("background-color", "#f5b7b8").focus();

							    }else{
								    // $('.inputckbCo.<?php echo $kunik ?>[data-imp="'+option+'"]').attr("placeholder","").css("background", "lightskyblue");
							    }
							}
						});
					}else{
						mylog.log("--------not cehcked not empty--------");
						$('.inputckbCo.<?php echo $kunik ?>[data-imp="'+option+'"]').off("focusout");
						$('.inputckbCo.<?php echo $kunik ?>[data-imp="'+option+'"]').attr("placeholder","").css("background-color", "unset").val("");
						//.prop("disabled", true);
					}
					if(typeof reloadStepValidationInputGlobal != "undefined") {
						reloadStepValidationInputGlobal(dataForm)
					}
					if(typeof newReloadStepValidationInputGlobal != "undefined") {
						newReloadStepValidationInputGlobal({
							inputKey : domId,
							inputType : "tpls.forms.cplx.multiCheckboxPlus" 
						})
					}
				});
			}
		} else {
			$(this).closest('.thckd').find('span.rank').attr('data-rank', allRanksKeys.length + 1);
			allRanks[kunik][$(this).attr('id')] = {
				'rank' : allRanksKeys.length + 1,
				'value' : $(this).val()
			};
			if(isRankEnabled<?php echo $kunik ?> == 'true' || isRankEnabled<?php echo $kunik ?> == true) {
				$('.lblAction'+domId).find('a.editRank').remove();
				$('.lblAction'+domId).find('ul.srblist').remove();
				$('.lblAction'+domId).append(`
					<a href='javascript:;' data-id='<?php echo $parentForm["_id"] ?>' data-form='<?php echo $form["id"] ?>' data-toggle='dropdown' data-collection='<?php echo Form::COLLECTION ?>' data-inputId='${domId}'  data-path='params.${kunik}' data-kunik='${kunik}' class='previewTpl paramsonebtn dropdown-toggle editRank' ><i class='fa fa-th-list tooltips' data-placement='bottom' data-original-title='Modifier rang'></i> </a>
					<ul class='dropdown-menu srblist' id='rankMenu${domId}'>
					</ul>
				`);
				bindRankEvents();
				$('.tooltips').tooltip();
			}
			saveMultiCheckboxPlus(answer , function(params) {
			mylog.log("callback answer",params);
			// toastr.success('Mise à jour enregistrée');

			if(typeof reloadStepValidationInputGlobal != "undefined") {
				reloadStepValidationInputGlobal(dataForm)
			}
			if(typeof newReloadStepValidationInputGlobal != "undefined") {
				newReloadStepValidationInputGlobal({
					inputKey : domId,
					inputType : "tpls.forms.cplx.multiCheckboxPlus" 
				})
			}
			if($("#"+domId).is(':checked')){

				$('.inputckbCo.<?php echo $kunik ?>[data-imp="'+option+'"]').prop("disabled", false).focus();
				$('.inputckbCo.<?php echo $kunik ?>[data-imp="'+option+'"]').focusout(function () {
			      if(<?php echo json_encode($paramsData["mandatoryCplx"]) ?>==true || <?php echo json_encode($paramsData["mandatoryCplx"]) ?>=="true"){
					if($(this).val()==""){
						mylog.log("--------cehcked --------", option);
						setTimeout(
							scrollintoDiv(domId,1000)
							,500);
							//$('body').scrollTo('#'+domId);
							$('.inputckbCo.<?php echo $kunik ?>[data-imp="'+option+'"]').attr("placeholder","Champ obligatoire").css("background-color", "#f5b7b8").focus();

					}else{
						mylog.log("--------not cehcked not empty--------");

						$('.inputckbCo.<?php echo $kunik ?>[data-imp="'+option+'"]').off("focusout");
						$('.inputckbCo.<?php echo $kunik ?>[data-imp="'+option+'"]').attr("placeholder","").css("background-color", "unset");
					}
				  }
				});
			}else{
				$('.inputckbCo.<?php echo $kunik ?>[data-imp="'+option+'"]').off("focusout");
				$('.inputckbCo.<?php echo $kunik ?>[data-imp="'+option+'"]').attr("placeholder","").css("border", "none").val("").prop("disabled", true);
			}
			//$('.inputckbCo.<?php echo $kunik ?>').prop('required',true);
			// if (typeof stepValidationReload<?php echo @$formId?> !== "undefined") {
//                 stepValidationReload<?php echo @$formId?>();
//             }
			});
		}
	});

	function delay(callback, ms) {
		var timer = 0;
		return function() {
			var context = this, args = arguments;
			clearTimeout(timer);
			timer = setTimeout(function () {
				callback.apply(context, args);
			}, ms || 0);
		};
	}

	function bindRankEvents() {
		$(".editRank").off().on("click", function(e) {
			const kunik = $(this).attr('data-kunik');
			const inputId = $(this).attr('data-inputId');
			var ranks = allRanks[kunik] ? allRanks[kunik] : {};
			var sortable = [];
			const subForm = $(this).attr('data-form');
			for (var r in ranks) {
				sortable.push([r, ranks[r]]);
			}

			sortable.sort(function(a, b) {
				return a[1]['rank'] - b[1]['rank'];
			});
			ranks = Object.fromEntries(sortable);
			var ranksHtml = ``;
			for([rankKey, rankValue] of Object.entries(ranks)) {
				const checked = rankKey == inputId ? 'checked' : '';
				ranksHtml += `
							<li class="srblist__item">
								<input type="radio" class="radio-btn rank-choice${inputId}" ${checked} data-inputId="${rankKey}" data-kunik="${kunik}" data-rank="${rankValue['rank']}" name="choice" id="a-opt${inputId}${rankKey}" />
								<label for="a-opt${inputId}${rankKey}" class="srblabel">${rankValue['rank']}. ${rankValue['value']}</label>
							</li>`
			}
			$(`#rankMenu${inputId}`).html(ranksHtml);
			$('.rank-choice'+inputId).on('change', function(event) {
				const inputRankId = $(this).attr('data-inputId');
				const inputRank = allRanks[kunik][inputRankId]['rank'];
				const valTemp = {
					rank : allRanks[kunik][inputId]['rank']
				};
				mylog.log($(this));
				allRanks[kunik][inputId]['rank'] = inputRank*1;
				allRanks[kunik][inputRankId]['rank'] = valTemp['rank'];

				// change rank view
				const inputClickedElem = $('input#'+inputId);
				const inputToChangeElem = $('input#'+inputRankId);
				inputClickedElem.closest('.thckd').find('span.rank').attr('data-rank', inputRank*1);
				inputToChangeElem.closest('.thckd').find('span.rank').attr('data-rank', valTemp['rank']);
				changeRank(subForm, kunik)
			})
		});
	}

	bindRankEvents();

    $('.inputckbCo.<?php echo $kunik ?>').keyup(delay(function(e){

    		if($('input[value="' + $(this).data("imp") + '"]').is(':checked')) {

    			var allckd = [];
				const kunik = $(this).attr("data-id");

    			var val=$(this).val();

    			var impname=$(this).data("impname");

    			if(val!=""){
    				$(this).css("background-color","unset");
    			}else{
    				$(this).css("background-color","");
    			}

    			$("input:checkbox[name='"+$(this).data("impname")+"']:checked").each(function(){
	    			mylog.log("ckd", $(this).val());
	    			var key = $(this).val();
	    			var rank = 1;
					if(allRanks[kunik][$(this).attr('id')] && allRanks[kunik][$(this).attr('id')]['rank'])
						rank = allRanks[kunik][$(this).attr('id')]['rank']
	    			var b = { [key] : { 'value' : key, 'type' : $(this).data("type"), 'rank': rank, 'textsup' : $('input[data-impname="'+impname+'"][data-imp="' + $(this).val() + '"]').val() }};
	    			allckd.push(b);
				});

				answer.path = "answers."+$(this).data("form")+"."+$(this).data("id");
	    		answer.collection = "answers" ;
	    		answer.id = "<?php echo $answer["_id"]; ?>";
	    		answer.value = allckd;
	    		saveMultiCheckboxPlus(answer);

	    	}
	    }, 1000 ));

    $(".inputckbCo.<?php echo $kunik ?>").on("click",function(){
        $(this).parent().parent().parent().siblings(".ckbCo").attr("checked",true).trigger("change");
    });

    function saveMultiCheckboxPlus(ans, callback){
    	dataHelper.path2Value(ans , function(params) {
    		//toastr.success(tradForm['modificationSave']);
    		if(params && params.elt && params.elt.answers && typeof answerObj != 'undefined') {
				answerObj['answers'] = params.elt.answers
			}
			if(typeof callback=="function"){
    			callback();
    		}
    		if(costum!=null && typeof costum["<?php echo $kunik ?>Callback"]=="function"){
    			costum["<?php echo $kunik ?>Callback"](params);
    		}
    		mylog.log("aftercallBack",params);

		},false);
    }


    if(canEditForm && location.hash.indexOf("#answer")==-1){
        $(document).on("keyup", ".addmultifield", function() {
    	    let value = $(this).val();
 			    let regex = /[.\$]/g;
 			    if (value.match(regex)) {
   			    value = value.replace(regex, "");
   			    $(this).val(value);
   			    toastr.info("Caractère non autorisé");
 			    }
		    });
    }
    else{
    	$(".addmultifield").off("keyup","**");
    }

    sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {
	        "title" : "<?php echo $label ?> config",
	        "icon" : "cog",
	        "properties" : {
	            list : {
	                inputType : "array",
	                label : "Liste de bouton check",
	                value :  sectionDyf.<?php echo $kunik ?>ParamsData.list
	            },
				nbAnswersMax : {
					inputType: "text",
					label: "Nombre de choix possible",
					value: sectionDyf.<?php echo $kunik ?>ParamsData['nbAnswersMax'] ? sectionDyf.<?php echo $kunik ?>ParamsData['nbAnswersMax'] : 8,
				},
				rank : {
                    label : "Il y d'ordre d'importance de choix",
                    inputType: 'radio',
                    options: {
                        true: {
                            lbl: 'Oui',
                            icon: 'check',
                            class: sectionDyf.<?php echo $kunik ?>ParamsData['rank'] == 'true' ? 'active class-sty' : 'class-sty',

                        },
                        false: {
                            lbl: 'Non',
                            icon: 'times',
                            class: sectionDyf.<?php echo $kunik ?>ParamsData['rank'] == 'false' ? 'active class-sty' : 'class-sty',
                        }
                    },
                    value: sectionDyf.<?php echo $kunik ?>ParamsData['rank'] ? sectionDyf.<?php echo $kunik ?>ParamsData['rank'] : 'false',
                },
	            dependOn : {
	            	inputType : "select",
	              label : "Sur quelle réponse depends cette question",
	              class : "form-control",
	              options : sectionDyf.<?php echo $kunik ?>ParamsData.dependOn,
	              value: "<?php echo (isset($parentForm["params"][$kunik]['global']['dependOn']) and $parentForm["params"][$kunik]['global']['dependOn'] != "") ? $parentForm["params"][$kunik]['global']['dependOn']:''; ?>"
	            },
	            width : {
					inputType : "select",
					class : "form-control",
					label : "Nombre d'element par ligne",
					options :  sectionDyf.<?php echo $kunik ?>ParamsData.width,
					value : "<?php echo (isset($parentForm["params"][$kunik]['global']['width']) && $parentForm["params"][$kunik]['global']['width'] != "") ? $paramsData["width"][strval($parentForm["params"][$kunik]['global']['width'])] : ''; ?>"
            	},
				addValue : {
                    label : "Ajout de valeur dans le champ",
                    inputType: 'radio',
                    options: {
                        true: {
                            lbl: 'activer',
                            icon: 'check',
                            class: (sectionDyf.<?php echo $kunik ?>ParamsData['addValue'] == 'true' || sectionDyf.<?php echo $kunik ?>ParamsData['addValue'] == true) ? 'active class-sty' : 'class-sty',

                        },
                        false: {
                            lbl: 'desactiver',
                            icon: 'times',
                            class: (sectionDyf.<?php echo $kunik ?>ParamsData['addValue'] == 'false' || sectionDyf.<?php echo $kunik ?>ParamsData['addValue'] == false) ? 'active class-sty' : 'class-sty',
                        }
                    },
                    value: sectionDyf.<?php echo $kunik ?>ParamsData['rank'] ? sectionDyf.<?php echo $kunik ?>ParamsData['rank'] : 'false',
                },
	        },
	        save : function () {
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
	        		 if(val.inputType == "properties")
                                tplCtx.value = getPairsObj('.'+k+val.inputType);
                            else if(val.inputType == "array")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else if(val.inputType == "formLocality")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
							else if(val.inputType == "radio")
								tplCtx.value[k] = $('.'+k+val.inputType).find(`div label.active input[name='${k}']`).val();
                            else
                                tplCtx.value[k] = $("#"+k).val();
	        	});
	            mylog.log("save tplCtx",tplCtx);

	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) {
	                    dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
	                    reloadInput("<?php echo $cle ?>", "<?php echo (string)$form["id"] ?>");
	                } );
	            }

	    	}
	    }
	};


   	<?php if(count($paramsData["list"]) != 0){
	  	foreach ($paramsData["list"] as $key => $value) {
	  		// if($lbl == $key){
	  		// 	if($value == "cplx"){
			$valueFormated = strtolower(preg_replace('/[^a-z0-9]+/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $value)));
	?>
	sectionDyf.<?php echo $kunik ?>oneParams<?php echo $key ?> = {
		"jsonSchema" : {
	        "title" : "<?php echo $label ?> config",
	        "icon" : "cog",
	        "properties" : {
	            tofill : {
	                inputType : "select",
	                label : "Bouton check avec champ de saisie",
	                options :  sectionDyf.<?php echo $kunik ?>ParamsData.type,
                    value : "<?php echo (isset($paramsData["tofill"][$value]) ? $paramsData["tofill"][$value] : ''); ?>"
	            },
	            optinfo : {
	            	inputType : "text",
	            	label : "Information supplementaire",
	            	value : "<?php echo (isset($paramsData["optinfo"][$value]) ? $paramsData["optinfo"][$value] : ''); ?>"
	            },
	            optimage : {
		            "inputType" : "uploader",
		            "label" : "Image",
		            "docType": "image",
		            "itemLimit" : 1,
		            "filetypes": ["jpeg", "jpg", "gif", "png"],
		            "showUploadBtn": false,
		            initList : {}
		          }
	        },
	        beforeBuild : function(){
				uploadObj.set("answers","<?php echo (string)$form["_id"]; ?>", "", "<?php echo $cle ?>", "", "<?php echo $valueFormated ?>");
			},
	        save : function () {
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>oneParams<?php echo $key ?>.jsonSchema.properties , function(ko,valo) {

	            		if (ko == "tofill") {

	            			tplCtx.path = 'params.<?php echo $kunik ?>.tofill';

                            $.each(sectionDyf.<?php echo $kunik ?>ParamsData.tofill, function(ke, va) {

                            	if(ke == "<?php echo $value; ?>"){

                            		var azerazer = ke.toString();
                            		sectionDyf.<?php echo $kunik ?>ParamsData.tofill[azerazer] = $("#"+ko).val();
                            	}
                            })

                            tplCtx.value = sectionDyf.<?php echo $kunik ?>ParamsData.tofill;

                            mylog.log("save tplCtx",tplCtx);

				            if(typeof tplCtx.value == "undefined")
				            	toastr.error('value cannot be empty!');
				            else {
				                dataHelper.path2Value( tplCtx, function(params) {

				                } );
				            }

				        }else if(ko == "optinfo"){
				        	tplCtx.path = 'params.<?php echo $kunik ?>.optinfo';

                            $.each(sectionDyf.<?php echo $kunik ?>ParamsData.optinfo, function(ke, va) {

                            	if(ke == "<?php echo $value; ?>"){

                            		var azerazer = ke.toString();
                            		sectionDyf.<?php echo $kunik ?>ParamsData.optinfo[azerazer] = $("#"+ko).val();
                            	}
                            })

                            tplCtx.value = sectionDyf.<?php echo $kunik ?>ParamsData.optinfo;

                            mylog.log("save tplCtx",tplCtx);

				            if(typeof tplCtx.value == "undefined")
				            	toastr.error('value cannot be empty!');
				            else {
				                dataHelper.path2Value( tplCtx, function(params) {

				                } );
				            }

				        } else if( ko == "optimage"){
							var value = "<?= $value ?>".replace("'", "#");
				        	tplCtx.path = 'params.<?php echo $kunik ?>.optimage.' + value.replace("#", "'");
				        	tplCtx.value["optimage"] = $("#optimage").val();

							if(typeof tplCtx.value == "undefined")
				            	toastr.error('value cannot be empty!');
				            else {
				                dataHelper.path2Value( tplCtx, function(params) {
									dyFObj.commonAfterSave(params,function(){});
				                } );
				            }
				        }

						dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
				        reloadInput("<?php echo $cle ?>", "<?php echo (string)$form["id"] ?>");

	        	});
	    	}
	    }
	};

	$(".editone<?php echo $kunik ?>Params<?php echo $key ?>").off().on("click",function() {
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
		ajaxPost(
			null,
			baseUrl+"/"+moduleId+"/document/getlistdocumentswhere",
			{
				params : {
					id: "<?= (string)$form["_id"] ?>",
					type: "answers",
					contentKey : "<?= "".$cle.$valueFormated ?>"
				},
				limit: 100,
				indexMin : 0
			},
			function(data){
				sectionDyf.<?php echo $kunik ?>oneParams<?php echo $key ?>.jsonSchema.properties.optimage.initList = data;
			},
			null,
			"json",
			{
				async: false
			}
		)
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>oneParams<?php echo $key ?>);
    });
	<?php
	  			}
	  		}
	//   	}
	// }
	?>


<?php if(count($paramsData["list"]) != 0){
	  	foreach ($paramsData["list"] as $key => $value) {
	  		// if($lbl == $key){
	  			if($paramsData["tofill"][$value] == "cplx"){
	?>

	sectionDyf.<?php echo $kunik ?>oneParamsPlaceholders<?php echo $key ?> = {
		"jsonSchema" : {
	        "title" : "<?php echo $label ?> config",
	        "icon" : "cog",
	        "properties" : {
	            placeholdersckb : {
                    inputType : "text",
                    label : "Modifier les textes à l'interieur du champs de saisie",
                    value :  "<?php echo $paramsData["placeholdersckb"][$value]; ?>"
                }
	        },
	        save : function () {
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>oneParamsPlaceholders<?php echo $key ?>.jsonSchema.properties , function(kk,vall) {


                            $.each(sectionDyf.<?php echo $kunik ?>ParamsData.placeholdersckb, function(ke, va) {
                            	if(ke == "<?php echo $value; ?>"){

                            		var azerazer = ke.toString();
                            		sectionDyf.<?php echo $kunik ?>ParamsData.placeholdersckb[azerazer] = $("#"+kk).val();

                            	}
                            })


                            tplCtx.value = sectionDyf.<?php echo $kunik ?>ParamsData.placeholdersckb;


	        	});
	            mylog.log("save tplCtx",tplCtx);

	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) {
	                    dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
	                    reloadInput("<?php echo $cle ?>", "<?php echo (string)$form["id"] ?>");
	                } );
	            }

	    	}
	    }
	}


	$(".editonep<?php echo $kunik ?>Params<?php echo $key ?>").off().on("click",function() {
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path")+'.placeholdersckb';
        //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>oneParamsPlaceholders<?php echo $key ?>);
    });

<?php
	  			}
	  		}
	   	}
	// }
?>

	$(".edit<?php echo $kunik ?>Params").off().on("click",function() {
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path")+'.global';
        //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
        dyFObj.openForm(sectionDyf.<?php echo $kunik ?>Params);
    });

	<?php if(isset($dependedOn) && $dependedOn!=""){ ?>
		let nbCheckbox<?php echo $key ?> = 0;

		if($("input:checkbox[name='<?php echo $kunik ?>']").length){
			nbCheckbox<?php echo $key ?> = $("input:checkbox[name='<?php echo $kunik ?>']").length;
		}

		$("#<?php echo "question".$parentForm["params"][$kunik]["global"]["dependOn"] ?>").on("click", "input[type='checkbox']",function(){

				let childCount = 0;
				childCount = $("input:checkbox[name='"+$(this).attr("name")+"']:checked").length;

				if(nbCheckbox<?php echo $key ?> <= childCount){
					nbCheckbox<?php echo $key ?> = childCount;
					let checkboxContainer = "";
					if($("#checkboxContainter<?php echo $kunik ?>").length){
						checkboxContainer = "#checkboxContainter<?php echo $kunik ?>";
					}else{
						checkboxContainer = "#checkboxList<?php echo $kunik ?>";
					}

					$(checkboxContainer).append(`
	        	<div class="parent-thckd col-md-<?php echo (isset($parentForm["params"][$kunik]['global']['width'])) ? $parentForm["params"][$kunik]['global']['width'] : '12'; ?> col-xs-12 col-sm-12" style="min-height: 50px;">
	      		<div class="thckd">
	      			<input data-id="<?php echo $kunik ?>"
	      				class="newCkbCo ckbCo <?php echo $kunik ?>"
	      				id="<?php echo $kunik ?>${childCount}"
	      				data-form="<?php echo $form["id"] ?>"
	      				type="checkbox"
	      				name="<?php echo $kunik ?>"
	      				data-type="${$(this).data("type")}"
	      				value="${$(this).val()}"/>
	      			<label for="<?php echo $kunik ?>${childCount}">
	  						<span></span>
	  						<span>${$(this).val()}</span>
	  					</label>
	  				</div></div>`);

						$(".newCkbCo.<?php echo $kunik ?>").on("change", function(){
			    		var allckd = [];
			    		$("input:checkbox[name='"+$(this).attr("name")+"']:checked").each(function(){
			    			mylog.log("ckd", $(this).val());
			    			var key = $(this).val();
			    			var b = { [key] : { 'type' : $(this).data("type"), 'textsup' : $('input[data-imp="' + $(this).val() + '"]').val() }};
			    			allckd.push(b);
							});

							answer.path = "answers."+$(this).data("form")+"."+$(this).data("id");
				    		answer.collection = "answers" ;
				    		answer.id = "<?php echo $answer["_id"]; ?>";
				    		answer.value = allckd;
				    		dataHelper.path2Value(answer , function(params) {
				    			//toastr.success('Mise à jour enregistrée');
				    			if (typeof stepValidationReload<?php echo @$formId?> !== "undefined") {
                                    stepValidationReload<?php echo @$formId?>();
                                }
							});
			    	});
				}else{
					nbCheckbox<?php echo $key ?> = childCount;
					if($("input[value='"+$(this).val()+"'][name='<?php echo $kunik ?>']").prop(":checked")){
						$("input[value='"+$(this).val()+"'][name='<?php echo $kunik ?>']").change();
					}
					$("input[value='"+$(this).val()+"'][name='<?php echo $kunik ?>']").parent().parent().remove();
				}
    });


		$(".goto<?php echo $kunik ?>").on("click", function(){
			scrollintoDiv($(this).data('path'), 2000);
		});
		//#question<?php echo str_replace("multiCheckboxPlus","",$kunik); ?>


	<?php } ?>
});
</script>
<?php
}
?> <!-- else 164 -->
