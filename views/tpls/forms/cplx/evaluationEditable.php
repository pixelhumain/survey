<style type="text/css">
    [contenteditable]:focus {
        outline: 0.5px solid darkgrey;
    }
    [contentEditable=true]:empty:not(:focus):before{
        content:attr(data-text);
        color:#888;
    }    
    [contentEditable=true]:empty{
        display: inline-block;
        height: 25px;
        cursor: text;
        min-width: 25px;
    }

    .hide-this {
        display: none !important;
        min-width: 150px !important;
    }
    .elem-title:hover .hide-this {
        display: inline-block !important;
        position: absolute;
        bottom: 30px;
        background-color: aliceblue;
        color: #3d85c6;
        border: solid 1px;
        border-radius: 5px;
        padding: 2px;
        width: fit-content;
        z-index: 2;
    }
    .expans-this {
        display: none !important;
    }
    .can-expans:hover .expans-this {
        display: inline-block !important;
    }

    .display-flex{
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        position: relative;
    }
    @media (max-width: 768px) {
        .display-flex {
            flex-direction: column;
            width: 100% !important;
            padding-left: 0 !important;
            padding-right: 0 !important;
        }
        .saveEval{
            text-align: left !important;
        }
        .show-bg-image {
            background-image: none !important;
        }

        .mobile-1 {
            order: 1;
        }
        .mobile-2 {
            order: 2;
        }
        .mobile-3 {
            order: 3;
        }
        .mobile-4 {
            order: 4;
        }
        .mobile-5 {
            order: 5;
        }
        .mobile-6 {
            order: 6;
        }
        .mobile-7 {
            order: 7;
        }
        .mobile-8 {
            order: 8;
        }
        .mobile-9 {
            order: 9;
        }
        .transformation-label{
            width: 100%;
        }
        .full-screen-setter{
            align-content: flex-end !important;
            position: absolute !important;
        }
        .btn-step{
            width: 50%;
        }
        .inputEvaluation {
            padding: 5px 0px 0px 10px;
        }
        .quadrant {
            border: solid 2px;
            margin-bottom: 20px;
            border-radius: 0px 10px 10px 10px;
        }
        .main-eval-container .legend{
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            padding-right: 15px !important;
            padding-left: 15px !important;

        }
        #central-container .wrap{
            display: none;
        }
    }
    .full-width{
        width: 100%;
    }

    .show-bg-image{
        background-image: url("<?php echo Yii::app()->getModule("survey")->assetsUrl; ?>/images/custom/evaluation/brainstorm-bg.png") ;    
        background-repeat: no-repeat;
        background-position: center;
        background-size: contain;
        min-height: 600px;
    }

    .eval-body .control-label{
        width: 110px;
        justify-content: flex-end;
    }

    .eval-body .flex-start{
        align-content: flex-start;
    }

    .eval-body .content-center{
        justify-content: center;
    }
    .eval-body .flex-end{
        justify-content: right;
        align-content: flex-start;
    }

    .quadrant-top-left .saveEval{
        text-align: right;
    }
    .quadrant-bottom-left .saveEval{
        text-align: right;
    }
    .quadrant-top-right .saveEval{
        text-align: left;
    }
    .quadrant-bottom-right .saveEval{
        text-align: left;
    }

    .toggle input[type="checkbox"] + .label-text:before{
        content: "\f204";
        font-size: 25px;
        font-family: "FontAwesome";
        speak: none;
        font-style: normal;
        font-weight: normal;
        font-variant: normal;
        text-transform: none;
        line-height: 1;
        -webkit-font-smoothing:antialiased;
        width: 1em;
        display: inline-block;
        margin-right: 10px;

    }

    .toggle input[type="checkbox"]:checked + .label-text:before{
        content: "\f205";
        color: #16a085;
        animation: effect 250ms ease-in;
    }

    .toggle input[type="checkbox"]:disabled + .label-text{
        color: #aaa;
    }

    .toggle input[type="checkbox"]:disabled + .label-text:before{
        content: "\f204";
        color: #ccc;
    }

    .checkmark {
        position: absolute;
        height: 25px;
        width: 25px;
        background-color: #eee;
    }

    .container-check:hover input ~ .checkmark {
        background-color: #ccc;
        cursor: pointer;
    }

    .container-check input:checked ~ .checkmark {
        background-color: #2196F3;
    }

    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }

    .container-check input:checked ~ .checkmark:after {
        display: block;
    }

    .container-check .checkmark:after {
        left: 9px;
        top: 5px;
        width: 5px;
        height: 10px;
        border: solid white;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }

    .user-typing {
        border: solid #3d85c6 1px;
        color: lightgray;
    }
    .admin-typing {
        border: solid black 1px;
    }

    .inputEvaluation:hover {
        border: solid 1px;
    }

    .typing-animation {
        display: flex;
        align-items: baseline;
        color: #999;
    }

    .dot {
        width: 5px;
        height: 5px;
        border-radius: 50%;
        background-color: #999;
        margin: 0 2px;
        opacity: 0;
        animation: dot-animation 1s infinite;
    }

    .dot.one {
        animation-delay: 0.1s;
    }

    .dot.two {
        animation-delay: 0.2s;
    }

    .dot.three {
        animation-delay: 0.3s;
    }

    @keyframes dot-animation {
        0% {
            opacity: 0;
        }
        50% {
            opacity: 1;
        }
        100% {
            opacity: 0;
        }
    }

    .online {
        filter: none !important;
    }

    .main-eval-container .legend {
        padding: 5px;
        font-size: 17px;
    }

    .desabled {
        pointer-events: none;
        background-color: whitesmoke !important;
        color : #ccc !important;
    }

    .not-editable {
        pointer-events: none;
        cursor: pointer;
    }

    [data-dismiss='modal']:not(.close-modal) {
        display: none;
    }
    .portfolio-modal .full-screen {
        display: none;
    }

    .transformation-label {
        height: max-content;
        padding: 5px;
    }

    .bgground {
        float: left;
        padding: 5px 20px;
        border-radius: 20px;
    }
    .breadcrumbs {
        list-style: none;
        margin: 0;
        padding: 0;
    }
    .breadcrumbs li:first-child {
        font-size: 19px;
    }
    .breadcrumbs li {
        text-shadow: 1px -1px 20px #8BC34A;
        margin: 0;
        padding: 0;
        float: left;
        font-family: Helvetica Neue, sans-serif;
        font-size: 14px;
        text-transform: uppercase;
        font-weight: 700;
        letter-spacing: 0.05em;
        line-height: 20px;
        color: #2c3e50;
    }

    .breadcrumbs li a {    
        display: block;
        padding: 0 40px 0 0px;
        color: #9fbd38;
        text-decoration: none;
        height: 20px;
        position: relative;
        perspective: 700px;
    }

    .breadcrumbs li a:after {
        content: "";
        width: 20px;
        height: 20px;
        border-color: #9fbd38;
        border-style: solid;
        border-width: 1px 1px 0 0;
        -webkit-backface-visibility: hidden;
        outline: 1px solid transparent;
        position: absolute;
        right: 20px;
        -webkit-transition: all 0.15s ease;
        -moz-transition: all 0.15s ease;
        -ms-transition: all 0.15s ease;
        transition: all 0.15s ease;
        -webkit-transform: rotateZ(45deg) skew(10deg, 10deg);
        -moz-transform: rotateZ(45deg) skew(10deg, 10deg);
        -ms-transform: rotateZ(45deg) skew(10deg, 10deg);
        transform: rotateZ(45deg) skew(10deg, 10deg);
    }

    .breadcrumbs li a:hover:after {
        right: 15px;
        -webkit-transform: rotateZ(45deg) skew(-10deg, -10deg);
        -moz-transform: rotateZ(45deg) skew(-10deg, -10deg);
        -ms-transform: rotateZ(45deg) skew(-10deg, -10deg);
        transform: rotateZ(45deg) skew(-10deg, -10deg);
    }

    .fade_rule {
        height: 1px;
        background-color: #E6E6E6;
        width: 100%;
        margin: 0 auto;
        background-image: linear-gradient(left , white 2%, #E6E6E6 50%, white 98%);
        background-image: -o-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%);
        background-image: -moz-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%);
        background-image: -webkit-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%);
        background-image: -ms-linear-gradient(left , white 2%, #E6E6E6 50%, white 98%);
        background-image: -webkit-gradient( linear, left bottom, right bottom, color-stop(0.02, white), color-stop(0.5, #9fbd38), color-stop(0.98, white) );
    }
    #wizardcontainer, .coFormbody{
        padding: 0!important;
    }

</style>
<?php 
$allMember = array();
$aboutMember = array();
if($answer){
    //Initialise Coform's btn setting
    $editParamsBtn = "";
    if($form["creator"]==Yii::app()->session["userId"]){
        $editParamsBtn = ($editQuestionBtn!="") ? " <a href='javascript:;' 
        data-id='".$parentForm["_id"]."' 
        data-collection='forms' 
        data-path='params.".$kunik."' 
        class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'>
        <i class='fa fa-cog'></i> 
        </a>" : "";
    }

    if (empty($answer["links"]["updated"])) {
        $answer["links"] = [
            "updated" => [
                Yii::app()->session["userId"] => [
                    "date" => "now"
                ]
            ]
        ];

    };

    $answerOwner = "";
    foreach ($usersAnswered as $user) {
        if ($user["_id"]->__toString() == $answer["links"]["answered"][0]) {
            $answerOwner = $user["name"];
            break;   
        }
    }
    
    foreach($usersAnswered as $m => $mv){
        $aboutMember[] = ["name" => $mv["name"],"profilImageUrl" => isset($mv["profilImageUrl"])?$mv["profilImageUrl"]:"","id" => $mv["_id"]->{'$id'}];

    }
    $answer["objectif"] = isset($answer["objectif"]) ? $answer["objectif"] : "";
    $answer["objet"] = isset($answer["objet"]) ? $answer["objet"] : "";
    $answer["answers"][$form["id"]][$kunik] = isset($answer["answers"][$form["id"]][$kunik]) ? $answer["answers"][$form["id"]][$kunik] : [];
    $answer["disableBrainstorm"] = isset($answer["disableBrainstorm"]) ? $answer["disableBrainstorm"] : false;
    $answer["params"][$kunik]["activeStep"] = isset($answer["params"][$kunik]["activeStep"]) ? $answer["params"][$kunik]["activeStep"] : "Transformation";
    $answer["params"][$kunik]["quadrants"]["quadrant0"]["label"] = isset($answer["params"][$kunik]["quadrants"]["quadrant0"]["label"]) ? $answer["params"][$kunik]["quadrants"]["quadrant0"]["label"] : "Ce qui est obsolète";
    $answer["params"][$kunik]["quadrants"]["quadrant0"]["description"] = isset($answer["params"][$kunik]["quadrants"]["quadrant0"]["description"]) ? $answer["params"][$kunik]["quadrants"]["quadrant0"]["description"] : "C’est dépassé, périmé, derrière eux, n’est ples d’actualité,…";
    $answer["params"][$kunik]["quadrants"]["quadrant1"]["label"] = isset($answer["params"][$kunik]["quadrants"]["quadrant1"]["label"]) ? $answer["params"][$kunik]["quadrants"]["quadrant1"]["label"] : "Ce qui est valide";
    $answer["params"][$kunik]["quadrants"]["quadrant1"]["description"] = isset($answer["params"][$kunik]["quadrants"]["quadrant1"]["description"]) ? $answer["params"][$kunik]["quadrants"]["quadrant1"]["description"] : "Ce qui fonctionne aujourd’hui dans leur management, c’est fiable, c’est efficace pour tous, intégré…";
    $answer["params"][$kunik]["quadrants"]["quadrant2"]["label"] = isset($answer["params"][$kunik]["quadrants"]["quadrant2"]["label"]) ? $answer["params"][$kunik]["quadrants"]["quadrant2"]["label"] : "Ce qui est anticipé";
    $answer["params"][$kunik]["quadrants"]["quadrant2"]["description"] = isset($answer["params"][$kunik]["quadrants"]["quadrant2"]["description"]) ? $answer["params"][$kunik]["quadrants"]["quadrant2"]["description"] : "En émergence, ce sont toutes les idées, propositions, pas suffisamment construit, état embryonaire, avant expérimentation, pas concret …";
    $answer["params"][$kunik]["quadrants"]["quadrant3"]["label"] = isset($answer["params"][$kunik]["quadrants"]["quadrant3"]["label"]) ? $answer["params"][$kunik]["quadrants"]["quadrant3"]["label"] : "Ce qui est innovant";
    $answer["params"][$kunik]["quadrants"]["quadrant3"]["description"] = isset($answer["params"][$kunik]["quadrants"]["quadrant3"]["description"]) ? $answer["params"][$kunik]["quadrants"]["quadrant3"]["description"] : "Ce que vous avez initié de nouveau ces derniers temps, ce que vous avez expérimenté de différent d’avant…)";

    $answerKunik = "answer".$answer["_id"];
    ?>

<div class="main-body">
    <div class="eval-body display-flex full-width <?= $answerKunik ?>">
        <div class="evals-header padding-bottom-10 display-flex full-width">
            <div class="eval-title full-width">
                <ul class="centermiddledown breadcrumbs">
                    <div class="bgground">
                        <li class="hidden-xs"><img style="position: absolute;left: -25px;top: 0;" height="32px" src="<?= Yii::app()->getModule( "co2" )->assetsUrl.'/images/ocecotools/evaluation_ariane.jpg' ?>"></li>
                        <li><a class="lbh" href="#<?= $el["slug"] ?>">#<?= $el["slug"] ?></a></li>
                        <li><a href="javascript:;" data-id="<?= (string)$parentForm["_id"] ?>" class="observatory"><?= $parentForm["name"] ?></a></li>
                        <li class="can-expans"><?= $answerOwner ?>
                        <i style="font-weight:400"> (<?= date("d-m-Y", $answer["created"]) ?><span class="expans-this">, <?= date("H:i:s", $answer["created"]) ?></span>)
                        </i>
                    </li>
                    </div>
                </ul>
                <?= $editQuestionBtn.$editParamsBtn ?>
                <div id="active-users" class="pull-right display-flex" style="flex-direction: row;justify-content: center;"></div>
               <!--  <h4 class="padding-20" style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
                    <?php echo $label.$editQuestionBtn.$editParamsBtn.">".$parentForm["name"].">".$answerOwner.''; ?>
                </h4> -->
            </div>
            <div class="eval-contributor"></div>
        </div>
        <!-- <div class="bg-dark full-width" style="height: 1px;"></div> -->

        <div class="fade_rule"></div> 
        <div class="eval-description display-flex full-width padding-top-10">
            <div class="display-flex padding-bottom-20 full-width">
                <?php if(isset($answer["objectif"])){ ?>
                    <div class=" display-flex full-width padding-bottom-20" style="flex-wrap: initial;">
                        <strong class="text-left control-label display-flex" for="objectif<?= $kunik ?>">OBJECTIF :</strong>
                        <div class="display-flex padding-left-20" style="width:100%">
                            <div 
                                data-path='objectif' 
                                class="setting-editable eval-editable full-width text-left"
                                style="text-align: justify;"
                            <?php if(Authorisation::isInterfaceAdmin() || @$answer["user"] == Yii::app()->session["userId"]) { ?> 
                                contenteditable="true"
                                data-text="Ecrire une texte...."
                                data-collection='answers'
                                data-id='<?=$answer["_id"]?>'
                                data-path='objectif' 
                                class="setting-editable eval-editable"
                            <?php } ?>
                            ><?php echo $answer["objectif"];?></div>
                        </div>
                    </div>
                <?php } ?>
                <?php if(isset($answer["objet"])){ ?>
                    <div class=" display-flex transformInput full-width" style="flex-wrap: initial;">
                        <strong class="text-left control-label display-flex" for="objet<?= $kunik ?>">OBJET :</strong>
                        <div class=" display-flex padding-left-20 full-width" style="width:100%">
                            <div 
                                data-path='objet' 
                                class="setting-editable eval-editable full-width text-left"
                                style="text-align: justify;"
                            <?php if(Authorisation::isInterfaceAdmin() || @$answer["user"] == Yii::app()->session["userId"]) { ?> 
                                contenteditable="true"
                                data-id='<?=$answer["_id"]?>'
                                data-text="Ecrire une texte...."
                                data-collection='answers'
                            <?php } ?>
                            ><?php echo $answer["objet"]?></div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

        <div class="eval-switcher full-width display-flex" style="justify-content: space-between;">
            <div class="display-flex">
                <?php if(Authorisation::isInterfaceAdmin() || @$answer["user"] == Yii::app()->session["userId"]){ ?>
                    <label class="control-label display-flex" for="objet<?= $kunik ?>">ÉTAPE :</label>
                    <div class="display-flex"  style="flex-direction: row !important;">
                        <button class="btn <?php echo (@$answer['params'][$kunik]['activeStep']=='Brainstorm')?'bg-primary':'' ?> btn-step" data-path="<?='params.'.$kunik.'.activeStep'?>" data-id='<?=$answer["_id"]?>' data-collection='answers' data-step="Brainstorm">
                            Brainstorm
                        </button>

                        <button class="btn <?php echo (@$answer['params'][$kunik]['activeStep']=='Transformation')?'bg-primary':'' ?> btn-step" data-path="<?='params.'.$kunik.'.activeStep'?>" data-id='<?=$answer["_id"]?>' data-collection='answers' data-step="Transformation">
                            Transformer
                        </button>
                        <label class="container-check">
                          <input class="check-brainstorms hidden" type="checkbox" <?php if(!empty($answer["disableBrainstorm"])) echo "checked='checked'"?>>
                          <span class="checkmark"></span>
                          <span style="left: 35px;position: relative;">Desactiver les brainstorms </span>
                      </label>
                  </div>    
              <?php } ?>      
          </div>
          <div class="display-flex flex-end full-screen-setter"><i style="cursor: pointer; " class="fa fa-expand full-screen"></i></div>
      </div>

        <div class="main-eval-container display-flex full-width content-center show-bg-image padding-top-20">
            <div class="eval-container display-flex content-center" style="background-color: #ffffff9e;">
                <div class="quadrantTransform0 display-flex full-width content-center mobile-6 <?= $kunik ?>"></div>                
                <div class="display-flex text-center full-width content-center mobile-5" style="align-items: end;">
                    <div class="transformation-label" style="background-color: #337ab7; color:white;">Quitter</div>
                </div>
                <div class="display-flex mobile-4" style="width: 50%;">
                    <div class="quadrant-container quadrant-top-left display-flex flex-end padding-right-30 padding-top-20 full-width" style="flex-direction: column;">
                        <div class="eval-description display-flex full-width flex-end">
                            <div class="elem-title index-quadrant0" style="width: max-content;" class="parentInput">                                
                                <span class="mb-2 hide-this setting-editable" 
                                data-path='params.<?=$kunik?>.quadrants.quadrant0.description'
                                style="width: 100%;left: 0;"
                                <?php if(Authorisation::isInterfaceAdmin()) { ?> 
                                    contenteditable="true"
                                    data-text="Ecrire une texte...."
                                    data-id='<?= @$answer["_id"]?>'
                                    data-collection='answers'
                                <?php } ?>
                                ><?= @$answer["params"][$kunik]["quadrants"]["quadrant0"]["description"] ?></span>

                                <div class="bg-dark setting-editable legend"
                                data-path='params.<?=$kunik?>.quadrants.quadrant0.label'
                                <?php if(Authorisation::isInterfaceAdmin()) { ?> 
                                    contenteditable="true"
                                    data-text="Ecrire une texte...."
                                    data-id='<?=@$answer["_id"]?>'
                                    data-collection='answers'
                                <?php } ?>
                                ><?= @$answer["params"][$kunik]["quadrants"]["quadrant0"]["label"] ?> </div>
                            </div>
                        </div>
                        <div class="quadrant quadrant0 display-flex flex-start full-width">
                            
                        </div>
                        <!-- <fieldset class="col-xs-12 quadrant height-center tooltips fieldset-<?=$kunik?>">
                            <div class="parentInput" id="quadrant0Inputsquadrant0" data-mainquadrant="quadrant0" style="text-align: justify;"></div>
                            <div class="col-xs-12">
                                <button title="Copier les textes" class="btnCopy btn btn-xs pull-right" data-targetid="quadrant0Inputsquadrant0"><i class="fa fa-copy"></i></button>
                            </div>
                        </fieldset> -->
                    </div>
                </div>
                <div class="display-flex mobile-1" style="width: 50%;">
                    <div class="quadrant-container quadrant-top-right display-flex padding-left-30 padding-top-20 full-width" style="flex-direction: column;">
                        <div class="eval-description  display-flex full-width">
                            <div class="elem-title index-quadrant1" style="width: max-content;" class="parentInput">         

                                <div class="bg-dark setting-editable legend"
                                data-path='params.<?=$kunik?>.quadrants.quadrant1.label'
                                <?php if(Authorisation::isInterfaceAdmin()) { ?> 
                                    contenteditable="true"
                                    data-text="Ecrire une texte...."
                                    data-id='<?=@$answer["_id"]?>'
                                    data-collection='answers'
                                <?php } ?>
                                ><?= @$answer["params"][$kunik]["quadrants"]["quadrant1"]["label"] ?> </div>

                                <span class="mb-2 hide-this setting-editable" 
                                data-path='params.<?=$kunik?>.quadrants.quadrant1.description'
                                <?php if(Authorisation::isInterfaceAdmin()) { ?> 
                                    contenteditable="true"
                                    data-text="Ecrire une texte...."
                                    data-id='<?=@$answer["_id"]?>'
                                    data-collection='answers'
                                <?php } ?>
                                ><?= @$answer["params"][$kunik]["quadrants"]["quadrant1"]["description"] ?></span>
                            </div>
                        </div>
                        <div class="quadrant quadrant1 display-flex flex-start full-width">
                            
                        </div>
                    </div>
                </div>  
                <div class="display-flex mobile-8" style="align-items: center; width: 50%;">                
                    <div class="quadrantTransform1 mobile-2 display-flex <?= $kunik ?>" style="border : 0;"></div>
                    <div class="transformation-label mobile-1" style="order:1;background-color: #337ab7; color:white;height: max-content;">Créer</div>    
                </div>     
                <div class="display-flex flex-end mobile-2" style="align-items: center; width: 50%;">
                   <div class="transformation-label" style="background-color: #337ab7; color:white;height: max-content;">Garder</div>
                   <div class="quadrantTransform2 display-flex <?= $kunik ?>"></div>
               </div> 
                <div class="display-flex mobile-7" style="width: 50%;">
                    <div class="quadrant-container quadrant-bottom-left display-flex flex-end padding-right-30 padding-top-20 full-width" style="flex-direction: column;">
                        <div class="eval-description display-flex full-width flex-end">
                            <div class="elem-title index-quadrant2 flex-end" style="width: max-content;" class="parentInput">                                
                                <span class="mb-2 hide-this setting-editable" 
                                data-path='params.<?=$kunik?>.quadrants.quadrant2.description'
                                style="width: 100%;left: 0;"
                                <?php if(Authorisation::isInterfaceAdmin()) { ?> 
                                    contenteditable="true"
                                    data-text="Ecrire une texte...."
                                    data-id='<?=@$answer["_id"]?>'
                                    data-collection='answers'
                                <?php } ?>
                                ><?= @$answer["params"][$kunik]["quadrants"]["quadrant2"]["description"] ?></span>

                                <div class="bg-dark setting-editable legend"
                                data-path='params.<?=$kunik?>.quadrants.quadrant2.label'
                                <?php if(Authorisation::isInterfaceAdmin()) { ?> 
                                    contenteditable="true"
                                    data-text="Ecrire une texte...."
                                    data-id='<?=@$answer["_id"] ?>'
                                    data-collection='answers'
                                <?php } ?>
                                ><?= @$answer["params"][$kunik]["quadrants"]["quadrant2"]["label"] ?> </div>
                            </div>
                        </div>
                        <div class="quadrant quadrant2 display-flex flex-start full-width">
                            
                        </div>
                    </div>
                </div>
                <div class="display-flex mobile-8" style="width: 50%;">
                    <div class="quadrant-container quadrant-bottom-right display-flex padding-left-30 padding-top-20  full-width" style="flex-direction: column;">
                        <div class="eval-description display-flex">
                            <div class="elem-title index-quadrant2 flex-end" style="width: max-content;" class="parentInput">                                
                                <span class="mb-2 hide-this setting-editable" 
                                data-path='params.<?=$kunik?>.quadrants.quadrant3.description'
                                <?php if(Authorisation::isInterfaceAdmin()) { ?> 
                                    contenteditable="true"
                                    data-text="Ecrire une texte...."
                                    data-id='<?=@$answer["_id"]?>'
                                    data-collection='answers'
                                <?php } ?>
                                ><?= @$answer["params"][$kunik]["quadrants"]["quadrant3"]["description"] ?></span>

                                <div class="bg-dark setting-editable legend"
                                data-path='params.<?=$kunik?>.quadrants.quadrant3.label'
                                <?php if(Authorisation::isInterfaceAdmin()) { ?> 
                                    contenteditable="true"
                                    data-text="Ecrire une texte...."
                                    data-id='<?=@$answer["_id"] ?>'
                                    data-collection='answers'
                                <?php } ?>
                                ><?= @$answer["params"][$kunik]["quadrants"]["quadrant3"]["label"] ?> </div>
                            </div>
                        </div>
                        <div class="quadrant quadrant3 display-flex flex-start full-width">
                            
                        </div>
                    </div>
                </div>
                <div class="display-flex text-center full-width content-center mobile-9" style="align-items: end;">
                    <div class="transformation-label" style="background-color: #337ab7; color:white;">Introduire</div>
                </div>
                <div class="quadrantTransform3 display-flex full-width content-center mobile-9 <?= $kunik ?>"></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var formKey = '<?php echo $form["id"] ?>';
var kunik   = '<?= $kunik ?>';
var inputValue = null;
var isCollectifEval = true;
var answerId = '<?=$answer["_id"]?>';
var allMember = <?php echo json_encode($aboutMember); ?>;
var listMember = {};
var reloadKey = '<?= $key?>'
var answerObj = <?php echo json_encode($answer); ?>;
var reloadFormId = "<?php echo (string)$form["_id"] ?>"
    answerObj.disableBrainstorm = Boolean(<?= @$answer["disableBrainstorm"] == true ? true : 0 ?>);
if("<?= @$answer['params'][$kunik]['activeStep'] ?>" === "Transformation") {
    // $(".show-bg-image").addClass("show-bg-image-transf");
    // $(".show-bg-image").removeClass("show-bg-image");
}
// mylog.log("isBrainstorm",isBrainstorm)
//Initialize brainstorm    
$(".<?= $answerKunik ?>").ready(function(){
// Initialize answers
    function generateQuadrantInput(params,editable=false,text=""){
         var $input = `<div 
                    id="${params.key}" 
                    style="${params.order};
                    border-top-color: #${params.creator ? params.creator.substring(0, 6)+" !important;" : ''};
                    border-right-color: #${params.creator ? params.creator.substring(0, 6)+" !important;" : ''};
                    border-bottom-color: #${params.creator ? params.creator.substring(0, 6)+" !important;" : ''};
                    border-left-color: #${params.creator ? params.creator.substring(0, 6)+" !important;" : ''};
                    background-color: #${params.creator ? params.creator.substring(0, 6)+"10 !important;" : ''}"
                    class="saveEval inputEvaluation full-width"
                    contenteditable="${editable}"
                    data-text="Ecrire une texte...."
                    data-form='${params.formKey}' 
                    data-quadrant='${params.quadrant}'
                    data-creator='${params.creator}'
                    data-id='${answerId}'>${text}</div>`
        return $input
    }
    function deleteQuadrantInput(selector){
        $(selector).remove()
    }

    function SetCursorTotheEnd(selector){
        var range = document.createRange();
        range.selectNodeContents(selector[0]);
        range.collapse(false); 
        var selection = window.getSelection();
        selection.removeAllRanges();
        selection.addRange(range);
    }

    function emitAction(target,action,inputValue,quadrant=""){
        if (typeof creatorId === "undefined") {
            creatorId = userId;
            $(this).data("creator",creatorId)
        }

        mylog.log("hum sk","wSocket ") 
        
        if (isCollectifEval) {
            if(userId && coWsConfig.enable){
                coWsConfig.wsEvaluation<?php echo $answerKunik ?>.emit("new-evaluation", {
                    userId: userId,
                    answerIds: answerId,
                    emeter: currentUser.name,
                    profilImageUrl: currentUser.profilImageUrl,
                    quadrant:quadrant,
                    creator:creatorId,
                    target: ".<?= $answerKunik ?> "+target,
                    action:action,
                    value:inputValue
                })  
            }
        }
    }

    function refraishTransformation (){
        $(".<?= $answerKunik ?> .inputEvaluation").remove()
        $(".<?= $answerKunik ?> .transform").remove()
        if (notNull(answerObj.answers[formKey])) {
            const keysQuadrantTransform = ["quadrantTransform0","quadrantTransform1","quadrantTransform2","quadrantTransform3"];
            const keyQuadrant           = ["quadrant0","quadrant1","quadrant2","quadrant3"];
        // if (answerObj.answers.step[kunik]["activeStep"]) {}
        // alert(answerObj.params[kunik]["activeStep"])
            if (answerObj['params'][kunik]['activeStep'] =="Transformation" ) {            
                $.each(keysQuadrantTransform, function(id, transform) {
                    if (typeof answerObj.answers[formKey][kunik][transform] == "undefined") {
                        answerObj.answers[formKey][kunik][transform] =  [ {"text" : ""}]
                    }
                // $.each(answerObj.answers[formKey][kunik][transform],function(key,val){  
                    var $textarea = 
                    $(`<textarea 
                        data-text="Ecrire une texte...."
                        style="color: red;" id="" 
                        data-form='${formKey}' 
                        data-quadrant='${transform}'
                        data-creator='${userId}'
                        data-id='${answerId}'
                        class="saveEval transform">
                        </textarea>`) 
                    $textarea.val(answerObj.answers[formKey][kunik][transform].text)
                    $("."+transform).html($textarea)
                // })
                });
                $(".<?= $answerKunik ?> .transformation-label").show()
            }else{
                $(".<?= $answerKunik ?> .transformation-label").hide()
            }
            $.each(keyQuadrant, function(id, quadrant) {
                if (typeof answerObj.answers[formKey][kunik][quadrant] != "undefined") {
                    $.each(answerObj.answers[formKey][kunik][quadrant],function(key,val){  
                        var params = {
                            key      : key,
                            formKey  : formKey,
                            quadrant : quadrant,
                            userId   : userId,
                            order    : val.order ? val.order : "",
                            creator  : val.creator
                        }
                        // if (!answerObj.disableBrainstorm) {                        
                            $("."+quadrant).append(generateQuadrantInput(params,!answerObj.disableBrainstorm,val.text))
                        // }else{                        
                        //     $("."+quadrant).append(generateQuadrantInput(params,false,val.text))
                        // }
                    })
                }else{
                    var params = {
                        key      : id+answerId,
                        formKey  : formKey,
                        quadrant : quadrant,
                        userId   : userId,
                        order    : "0",
                        creator  : userId
                    }
                    $("."+quadrant).append(generateQuadrantInput(params,!answerObj.disableBrainstorm,""))
                }

            });

        }
        resetInput ()
    }

    function resetInput (){
        $(".<?= $answerKunik ?> .saveEval").each(function() {
            if($(this).prop('tagName') == "TEXTAREA"){
                emitAction("[data-quadrant='"+$(this).data("quadrant")+"']","blur",$(this).val())
            }else{ 
                emitAction("#"+$(this).attr("id"),"blur",$(this).text())
            } 
        });  

        $(".<?= $answerKunik ?> .setting-editable").each(function() {
            emitAction(".setting-editable[data-path='"+$(this).data("path")+"']","blur",$(this).text())
        });  
    }

    //**************Event******************
    // User connected
    function userProfile(user) {
        var profileHtml = ` ${user.id == answerObj.user?"<small style='position: relative;bottom: -35px;left: 63px;text-transform: capitalize;font-family: monospace'></small>":""}
        <span class="tooltips" style="filter: opacity(0.2);" data-toggle="tooltip" id="user${user.id}" title="${user.name}${user.id == answerObj.user?" (créateur)":""}">
        ${(user.profilImageUrl && user.profilImageUrl!="")?'<img src="'+user.profilImageUrl+'" height="25" width="25"  class="active-users" style="border-radius: 50px;border: 3px solid #'+user.id.substring(0, 6)+'; object-fit:cover; background-color:#'+user.id.substring(0, 6)+'">':'<i class="fa fa-user" style="border: 3px solid #'+user.id.substring(0, 6)+'; color:#'+user.id.substring(0, 6)+'; padding:0px 3px; border-radius: 50px;font-size: 18px;"></i>'}</span>
        `;
        return profileHtml;
    }

    for (var i = 0; i < allMember.length; i++) {
        listMember["user"+allMember[i].id] = allMember[i];
        $("#active-users").append(userProfile({id:allMember[i].id,profilImageUrl:allMember[i].profilImageUrl, name : allMember[i].name}))
        // $("#active-users").append(` ${allMember[i].id == answerObj.user?"<small style='position: relative;bottom: -35px;left: 63px;text-transform: capitalize;font-family: monospace'></small>":""}
        //     <span class="tooltips" style="filter: opacity(0.2);" data-toggle="tooltip" id="user${allMember[i].id}" title="${allMember[i].name}${allMember[i].id == answerObj.user?" (créateur)":""}">
        //     ${(allMember[i].profilImageUrl && allMember[i].profilImageUrl!="")?'<img src="'+allMember[i].profilImageUrl+'" height="25" width="25"  class="active-users" style="border-radius: 50px;border: 3px solid #'+allMember[i].id.substring(0, 6)+'; object-fit:cover; background-color:#'+allMember[i].id.substring(0, 6)+'">':'<i class="fa fa-user" style="border: 3px solid #'+allMember[i].id.substring(0, 6)+'; color:#'+allMember[i].id.substring(0, 6)+'; padding:0px 3px; border-radius: 50px;"></i>'}</span>
        //     `);
        // if (allMember[i].id == userId) {
        //     $("#user"+allMember[i].id+"state").addClass("online");
        //     $("#user"+allMember[i].id+"state").removeClass("offline");
        // }

    }

    // function checkScreenSize(){
    //     var windowWidth = $(window).width();
    //     if(windowWidth <= 768){
    //         console.log("Screen size is less than or equal to 768px");
    //         $(".evals-header").append($("#primary-nav"))
    //     }
    //     else {
    //         console.log("Screen size is greater than 768px");
    //         // $("#header .container-fluid .col-md-12").append($("#primary-nav"))
    //     }
    // }

    // Objectif & object
    $(".<?= $answerKunik ?>").on("focusin", ".setting-editable" ,function(e){
        e.stopImmediatePropagation()
        inputValue = $(this).html();
        $(this).addClass("admin-typing-textarea")
        emitAction(".setting-editable[data-path='"+$(this).data("path")+"']","focus",currentUser.name)
    })
    $(".<?= $answerKunik ?>").on('blur',".setting-editable", function(e) {
        let $this = $(this);
        if (notNull(inputValue) && inputValue != $this.html()) {
            inputValue = $this.html()
            tplCtx = {};
            tplCtx.id = $this.data("id");
            tplCtx.collection = $this.data("collection");
            tplCtx.path = $this.data("path"); 
            tplCtx.value = $this.html()
            dataHelper.path2Value( tplCtx, function(params) { 
                toastr.success("Modification enrégistré");
                $this.removeClass("admin-typing-textarea")
            } );
        }
        emitAction(".setting-editable[data-path='"+$this.data("path")+"']","blur",inputValue)
        e.stopImmediatePropagation()
    });

    $(".<?= $answerKunik ?>").on("keydown",".setting-editable", function(event){
        if (event.key === 'Enter') {
            event.preventDefault(); 
            document.execCommand('insertHTML', false, '<br>');
        }
        event.stopImmediatePropagation(); 
    });

 // Quadrant & transformation
    $(".<?= $answerKunik ?>").on("keydown", ".<?= $answerKunik ?> .inputEvaluation", function(e){
        
        e.stopImmediatePropagation(); 
        var $this = $(this)
        var quadrant = $this.data("quadrant");
        var timStamp = Date.now();
        if (quadrant.substring(0, 14) !== "transformation") {
            // Create new input when tap on ENTER KEY
            if (e.which == 13 && $this.text() != "") {
                emitAction("#"+$this.attr("id"),"blur",$this.html())
                e.preventDefault();
                e.stopImmediatePropagation(); 
                setTimeout(function () {
                    var params = {
                        key      : "evaluation"+timStamp,
                        formKey  : formKey,
                        quadrant : quadrant,
                        userId   : userId,
                        creator  : userId
                    }
                    var newInput  = generateQuadrantInput(params,true,"-")
                    var $newInput = $(newInput)
                    $($newInput).insertAfter($this)
                    $($newInput).focus()
                    if (quadrant == "quadrant1" || quadrant == "quadrant3") {
                        SetCursorTotheEnd($newInput)
                    }
                    emitAction("#"+$this.attr("id"),"newInput",newInput)
                },100)
                

        // If BACKSPACE typed and there is no more text in the current input, Move up into above input 
            }else if(e.which == 8){
                if ($this.text() == "" ) {
                    if ($this.prev('.inputEvaluation').length != 0) {
                        $this.prev('.inputEvaluation').focus()                    
                        SetCursorTotheEnd($this.prev('.inputEvaluation'))                        
                    }
                    if ($("."+$this.data("quadrant")+" .inputEvaluation").length > 1) {
                        $this.blur()
                        deleteQuadrantInput($this)                    
                        emitAction("#"+$this.attr("id"),"removedInput","")
                    }
                    e.preventDefault();
                }
            }else if (e.key === 'Tab') {
                e.preventDefault();
                var nextElementEditable = $this.next(); 

                while (nextElementEditable.length && !nextElementEditable.is('[contenteditable="true"]')) {
                    nextElementEditable = nextElementEditable.next(); 
                }

                if (nextElementEditable.length) {
                    nextElementEditable.focus()
                    if (nextElementEditable.data("quadrant") == "quadrant1" || nextElementEditable.data("quadrant") == "quadrant3") {
                        SetCursorTotheEnd(nextElementEditable)
                    }
                } else {
                    if ($this.data("quadrant") == "quadrant1") {
                        $('.inputEvaluation[data-quadrant="quadrant0"][contenteditable="true"]').first().focus()
                    }else if($this.data("quadrant") == "quadrant0"){                        
                        $('.inputEvaluation[data-quadrant="quadrant2"][contenteditable="true"]').first().focus()
                    }else if ($this.data("quadrant") == "quadrant2") {
                        $('.inputEvaluation[data-quadrant="quadrant3"][contenteditable="true"]').first().focus()
                    }else if($this.data("quadrant") == "quadrant3"){
                        $('.inputEvaluation[data-quadrant="quadrant1"][contenteditable="true"]').first().focus()
                    }
                }
            }
        }

    });

    $(".<?= $answerKunik ?>").on("focusin", ".inputEvaluation", function(e){  
        inputValue = $(this).text()
        e.stopImmediatePropagation()
        emitAction("#"+$(this).attr("id"),"focus",currentUser.name)

    });
 

    $(".<?= $answerKunik ?>").on("blur", ".inputEvaluation", function(e){  
        var valeur = $(this).text()
        mylog.log($(this))
        if (notNull(inputValue) && inputValue != valeur ) {
            inputValue = valeur            
            var inputData = {} 
            $(".<?= $answerKunik ?> ."+$(this).data("quadrant")+" .inputEvaluation").each(function(id, item) {
                inputData[$(item).attr("id")] = {                
                    order   : id,
                    text    : $(item).text(),
                    creator : $(item).data("creator")
                }
            });  

            let answerEval = {
                collection : "answers",
                id : answerId,
                path : "answers."+formKey+"."+kunik+"."+$(this).data("quadrant"),
                value : inputData
            };

            mylog.log("hum",kunik, answerEval)
            dataHelper.path2Value( answerEval , function(params) { }) 
            if($("#user"+userId).length==0){
                $("#active-users").append(userProfile({id:userId,profilImageUrl:userConnected.profilImageUrl, name : userConnected.name}))
            }
            let today = new Date();
            today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear()+ ' ' + today.getHours()+ ':' + today.getMinutes();
            let saveUser = {
                collection : "answers",
                id : answerId,
                path : "links.updated."+userId
            };
            saveUser.value = {}
            saveUser.value.date = today;
            saveUser.value.name = userConnected.name;          
            dataHelper.path2Value( saveUser , function(pSaveUser) { })  

                        // emitAction("#"+$this.attr("id"),"removedInput","")

        }

        e.stopImmediatePropagation()
        emitAction("#"+$(this).attr("id"),"blur",valeur)

    });

     $(".<?= $answerKunik ?>").on("blur",".transform", function(e){
        var valeur = $(this).val()
        if (notNull(inputValue) && inputValue != valeur) {
            inputValue = valeur   
            let answerEval = {
                collection : "answers",
                id : answerId,
                path : "answers."+formKey+"."+kunik+"."+$(this).data("quadrant")+".text",
                value : inputValue
            };

            mylog.log("hum",kunik, answerEval)
            dataHelper.path2Value( answerEval , function(params) { }) 

            let today = new Date();
            today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear()+ ' ' + today.getHours()+ ':' + today.getMinutes();
            let saveUser = {
                collection : "answers",
                id : answerId,
                path : "links.updated."+userId
            };
            saveUser.value = {}
            saveUser.value.date = today;
            saveUser.value.name = userConnected.name;          
            dataHelper.path2Value( saveUser , function(pSaveUser) { })  
            if($(".<?= $answerKunik ?> #user"+userId).length==0){
                $(".<?= $answerKunik ?> #active-users").append(userProfile({id:userId,profilImageUrl:userConnected.profilImageUrl, name : userConnected.name}))
            }

        }
        emitAction(".transform[data-quadrant='"+$(this).data("quadrant")+"'","blur",valeur)
        e.stopImmediatePropagation()
    })

    $(".<?= $answerKunik ?>").on("focusin",".transform", function(e){ 
        inputValue = $(this).val()
        emitAction(".transform[data-quadrant='"+$(this).data("quadrant")+"'","focus",currentUser.name)
        e.stopImmediatePropagation()
    })

    $(".<?= $answerKunik ?>").on("keydown",".transform", function(e){
        var $this = $(this)
        if (e.key === 'Tab') {
            e.preventDefault();
            if ($this.data("quadrant") == "quadrantTransform0") {
                $('.transform[data-quadrant="quadrantTransform1"]').focus()
            }else if($this.data("quadrant") == "quadrantTransform1"){                        
                $('.transform[data-quadrant="quadrantTransform3"]').focus()
            }else if ($this.data("quadrant") == "quadrantTransform3") {
                $('.transform[data-quadrant="quadrantTransform2"]').focus()
            }else if($this.data("quadrant") == "quadrantTransform2"){
                $('.transform[data-quadrant="quadrantTransform0"]').focus()
            }
        }
        e.stopImmediatePropagation()
    })



    // Step switcher
    $(".<?= $answerKunik ?> .btn-step").off().on("click", function(e){
        e.stopImmediatePropagation()
        let $this = $(this); 
        tplCtx.id = $this.data("id");
        tplCtx.collection = $this.data("collection");
        tplCtx.path = $this.data("path");
        tplCtx.value = $this.data("step");
        dataHelper.path2Value( tplCtx, function(params) {
            var newValue = {}
            newValue[kunik] = { activeStep : $this.data("step")}
            answerObj.params = newValue
            mylog.log("answerObj", answerObj)
            mylog.log("answerObj", newValue)
            emitAction("params","reload",newValue)
            reloadInput(reloadKey, reloadFormId)
            // refraishTransformation()                   

        });
        $(".btn-step").removeClass("bg-primary")
        $this.addClass("bg-primary")
        event.preventDefault();
    });

    //Active & desactive brainstorms

    $(".<?= $answerKunik ?> .check-brainstorms").change(function(event) {
        tplCtx = {};
        tplCtx.id = answerId;
        tplCtx.collection = "answers";
        tplCtx.path = "disableBrainstorm"; 
        tplCtx.value = ""
        if (this.checked) {
            $(".<?= $answerKunik ?> .editable-parent .inputEvaluation").attr("contenteditable", "false");
            tplCtx.value = "true";
            dataHelper.path2Value( tplCtx, function(params) { 
                toastr.success("Modification enrégistré");
                answerObj.disableBrainstorm = true;
                // refraishTransformation()
                reloadInput(reloadKey, reloadFormId);
                emitAction("disableBrainstorm","reload",true)
        } );
        } else {
            $(".<?= $answerKunik ?> .editable-parent .inputEvaluation").attr("contenteditable", "true");
            tplCtx.value = "false";
            dataHelper.path2Value( tplCtx, function(params) { 
                toastr.success("Modification enrégistré");
                reloadInput(reloadKey, reloadFormId);
                 answerObj.disableBrainstorm = false;
                // refraishTransformation()
                emitAction("disableBrainstorm","reload",false)
        } );
        }
        event.preventDefault();
        event.stopImmediatePropagation(); 
    });

// Online & offline state
    $(".central-section").off().on("mouseenter", ".<?= $answerKunik ?>",function() {  
        emitAction("","connected","")
    })
    document.addEventListener("visibilitychange", function() {
        if (document.visibilityState === 'visible') {
            emitAction("","connected","")
        } else {
            emitAction("","disconnected","")
        }
    });

// Full screen
    $(".full-screen").on("click",function() {
        smallMenu.open($(".<?= $answerKunik ?>"))
    })

    $(document).on("click", ".close-modal", function (){
        $(".main-body").append($(".<?= $answerKunik ?>"))
    })

// Required for content editable
// Remove any HTML tags from the pasted text
    $(".<?= $answerKunik ?>").on('paste','[contenteditable="true"]', function(event) {
        event.preventDefault();
        var pastedText = (event.originalEvent || event).clipboardData.getData('text/plain');
        var plainText = pastedText.replace(/<[^>]+>/g, '');
        document.execCommand('insertText', false, plainText);
    });

    //  checkScreenSize();

    // // Check screen size on window responsive
    // $(window).resize(function(){
    //     checkScreenSize();
    // });

    // SocketIO    

    if (isCollectifEval) {          
        if(userId && coWsConfig.enable && typeof coWsConfig.wsEvaluation<?php echo $answerKunik ?> == "undefined"){   

            coWsConfig.wsEvaluation<?php echo $answerKunik ?> = io(coWsConfig.serverUrl, {
                query:{
                    userId: userId,
                    isEvaluation: true,
                    room: answerId
                }
            });  
            coWsConfig.wsEvaluation<?php echo $answerKunik ?>.off('new-evaluation').on('new-evaluation', function(data){  
                if (data.emeter != currentUser.name) {                    
                    mylog.log("hum sk",data)
                    var typing = '<span class="typing-animation">'+data.value+'<span class="dot one"></span><span class="dot two"></span><span class="dot three"></span></span>'
                    if (data.action == "focus") {
                        if($(data.target).prop('tagName') == "TEXTAREA"){
                            $(data.target).val(data.value+"...")
                            $(data.target).addClass("desabled")
                        }else{ 
                            $(data.target).addClass("user-typing")
                            $(data.target).html(typing)
                            $(data.target).attr("contenteditable", "false");
                        }  
                    }
                    if (data.action == "blur") {
                        if (!data.target.includes('setting-editable') && answerObj.disableBrainstorm == false) {                            
                            $(data.target).attr("contenteditable", "true");
                        }else if (canAdminAnswer) {
                            $(data.target).attr("contenteditable", "true");
                        }
                        $(data.target).removeClass("user-typing")                          
                        if($(data.target).prop('tagName') == "TEXTAREA"){
                            $(data.target).val(data.value)                            
                            $(data.target).removeClass("desabled")
                        }else {                                       
                            $(data.target).html(data.value)
                        }                      
                    }
                    if (data.action == "newInput") {
                        var inserted = $(data.value).insertAfter($(data.target))
                        inserted.html('<span class="typing-animation">- <span class="dot one"></span><span class="dot two"></span><span class="dot three"></span></span>')
                    }
                    if (data.action == "removedInput") {
                        $(data.target).remove()
                    }

                    if (data.action == "reload") {
                        answerObj[data.target] = data.value
                        // refraishTransformation()
                        reloadInput(reloadKey, reloadFormId)
                        if (data.userId !== "<?= Yii::app()->session["userId"] ?>" && typeof data.value != "object")
                            toastr.info(data.emeter+" a changé les brainstorms");
                        else
                            toastr.info(data.emeter+" a changé l'etape");
                    }
                }
                if(data.action == "connected"){                           
                    $("#user"+data.userId).addClass("online"); 
                    if($(".eval-body").length !=0 && $("#user"+data.userId).length==0){
                        $("#active-users").append(userProfile({id:data.userId,profilImageUrl:data.profilImageUrl, name : data.emeter}))
                        if (data.emeter != currentUser.name) 
                            toastr.info(data.emeter+" a rejoint le canal d'évaluation");
                    }                                 
                }else if(data.action == "disconnected"){                  
                    $("#user"+data.userId).removeClass("online");                      
                }
                
            })

        }
    }          
    refraishTransformation()
})


</script>
<?php } else {
        //echo "<h4 class='text-red'>evaluation works with existing answers</h4>";
} ?>