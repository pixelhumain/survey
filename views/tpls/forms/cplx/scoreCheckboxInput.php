<style>
.show-checked-checkbox {
	border: 2px solid darkgrey;
	padding: 20px;
	border-radius: 20px; 
	overflow-x: auto; 
	text-align: center;
}
.result {
	margin-top: 150px;
}
</style>
<div class="show-checked-checkbox">
	<button type="button" class="btn score<?php echo $kunik ?>Params">
		Nombre des cases cochées 
	</button> <?php echo $editQuestionBtn; ?>
</div>
<script>
	$(".score<?php echo $kunik ?>Params").click(function() {
		let result = 0;
		let htmlResult = ""
        $('input[type="checkbox"][data-form="<?= @$form["id"] ?>"]').each(function() {
            if($(this).prop('checked'))
                result ++;
        })	
		htmlResult = `
			<div class='result'>
				<h2>NOMBRE TOTAL DES CASES COCHÉES:<h2>
				<h1 class="score-result">${result}</h1>
			</div>
			<div class='scoreBtn'>
				<a class="btn btn-primary btn-reset-score" data-dismiss="modal" >Recommencer</a>
			</div>
			
		`;
		smallMenu.open(htmlResult);
		$('.btn-reset-score').click(function () { 
			let params = {
				id : "<?= (string)$answer["_id"] ?>",
				collection : "answers",
				value: {},
				path: "answers.<?= @$form["id"] ?>"
			}
			$('input[type="checkbox"][data-form="<?= $form["id"] ?>"]').each(function() {
                $(this).prop('checked',false);
            }) 
			dataHelper.path2Value(params, function(params){
				toastr.success(tradForm.modificationSave);
				if(typeof reloadStepValidationInputGlobal != "undefined") {
					answerObj.answers.<?= @$form["id"] ?> = {};
				}
				if(typeof newReloadStepValidationInputGlobal != "undefined") {
					answerObj.answers.<?= @$form["id"] ?> = {};
					newReloadStepValidationInputGlobal({
						inputKey : <?php echo json_encode($key); ?>,
						inputType : "tpls.forms.cplx.scoreCheckboxInput" 
					})
				}
			});
		})
	}) 
</script>