<?php 
$value = (!empty($answers)) ? ' value="'.@$answers.'" ' : '';

$editParamsBtn = ($canEdit && $mode=="fa") ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

$elementType=[
			Organization::COLLECTION => "Organization",
			Person::COLLECTION 		 => "Person",
			Event::COLLECTION 		 => "Event",
			Project::COLLECTION 	 => "Project"
	];

$paramsData = [ 
	"searchType" => Organization::COLLECTION
];
 
if(isset($parentForm["params"][$kunik])){
	foreach ($paramsData as $e => $v) {
        if ( isset($parentForm["params"][$kunik][$e]) ) {
            $paramsData[$e] = $parentForm["params"][$kunik][$e];
        }
    }
}

$inpClass = "form-control";

if($type == "tags") 
	$inpClass = "select2TagsInput";

if($saveOneByOne)
	$inpClass .= " saveOneByOne";

if($mode == "r" || $mode == "pdf"){ ?>
    <div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
        <label for="<?php echo $kunik ?>">
        	<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
        		<?php echo $label ?>
        	</h4>
        </label><br/>
        <?php echo $answers; ?>
    </div>
<?php 
}else{
?>
	<div class="col-md-6 no-padding">
			<div class="form-group">
	    <label for="<?php echo $key ?>">
	    	<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
	    		<?php echo $label.$editQuestionBtn.$editParamsBtn ?>
	    	</h4>
	    </label>
	    <br/>
	    <?php if( !isset($parentForm["params"][$kunik]['searchType']) ){
					echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> ".Yii::t("survey","THIS FIELD HAS TO BE CONFIGURED FIRST").$editParamsBtn."</span>";
		}else{
		?>		 
	    <input type="<?php echo (!empty($type)) ? $type : 'text' ?>" 
	    		class="form-control" 
	    		id="<?php echo $key ?>" 
	    		aria-describedby="<?php echo $key ?>Help" 
	    		placeholder="<?php echo (isset($placeholder)) ? $placeholder : '' ?>" 
	    		data-form='<?php echo $form["id"] ?>'  <?php echo @$value ?> 
	    />
	    <?php if(!empty($info)){ ?>
	    	<small id="<?php echo $key ?>Help" class="form-text text-muted">
	    		<?php echo $info ;
				?>
	    	</small>
	    <?php }
	    } ?>
	</div>
	<div class="similarLink <?php echo $kunik ?>">
		<div  class='listSameName <?php echo $kunik ?>' style='overflow-y: scroll; height:150px;border: 1px solid black;display:none;' ></div>
		<a href='javascript:;' style='display:none;' onclick="document.querySelectorAll('.similarLink.<?php echo $kunik ?>')[0].style.display='none';"><?php echo Yii::t("survey", "No, none of the items in the list correspond to my answer")?></a></div>

	</div>

<?php } ?>


<?php if($mode != "pdf"){ ?>

<?php 


$searchType=(isset($parentForm["params"][$kunik]['searchType'])) ? $parentForm["params"][$kunik]['searchType'] : "" ;
	   		
?>
<script type="text/javascript">
	jQuery(document).ready(function() {
	   mylog.log("render form input","/modules/costum/views/tpls/forms/cplx/searchExist.php");

	   var searchType = <?php echo json_encode(!empty($searchType) ? $searchType : null ) ?>;
	   var kunik = <?php echo json_encode($kunik) ?>;

	   $("#<?php echo $key ?>").off().on("blur",function(){
	   		mylog.log("bluer");
	   		var name = $(this).val();

	   		if (searchType!=null){ 
	   			dyFObj.searchExist(name,[ searchType ], false ,kunik);
	   		}else{
	   			toastr.error("Please set the element type of research");
	   		}

	   		var answer = {
	   		                collection : "answers",
	   		                id : answerObj._id.$id,
	   		                path : "answers."+$(this).attr("id"),
	   		                value : name
	   		            };

			if(answerObj.form)
	   		    answer.path = "answers."+$(this).data("form")+"."+$(this).attr("id");  

	   		dataHelper.path2Value( answer , function(params) { 
	   		    toastr.success(tradForm.modificationSave);
	   		    saveLinks(answerObj._id.$id,"updated",userId);
	   		    if (typeof stepValidationReload<?php echo $formId?> !== "undefined") {
	   		        stepValidationReload<?php echo $formId?>();
	   		    }
	   		} );



	   });

	   if(typeof costum!="undefined" && costum!=null){
		   costum.<?php echo $kunik ?> = {
		   		searchExist : function (type,id,name,slug,email) { 
				mylog.log("costum searchExist : "+type+", "+id+", "+name+", "+slug+", "+email); 
				var data = {
					type : type,
					id : id,
					name : name,
					slug : slug 
				}

				costum.<?php echo $kunik ?>.connectToAnswer(data);
				// $("#similarLink").hide();
				// $("#ajaxFormModal #name").val("");


				// // TODO - set a condition ONLY if can edit element (authorization)
				// dyFObj.editElement( type,id, null,dynformCostumAnswer);
				},
				connectToAnswer : function(data){
					var answer = {
		   		                collection : "answers",
		   		                id : answerObj._id.$id,
		   		                path : "answers.<?php echo $form['id']?>.<?php echo $key ?>",
		   		                value : data.name
					};
					
					dataHelper.path2Value( answer , function(params) { 
					    toastr.success(tradForm.modificationSave);				    
					    if (typeof stepValidationReload<?php echo $formId?> !== "undefined") {
					        stepValidationReload<?php echo $formId?>();
					    }
					    reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
					} );
					if(searchType!=null){
						answer.path="links."+searchType;
						answer.value={}
						answer.value[data.id]={
							name : data.name,
							type: data.type
						};

						dataHelper.path2Value( answer , function(params) { 
						    toastr.success(tradForm.responseRelatedToElement);
						    
						} );
					}	

				}
			};
		}

	   sectionDyf.<?php echo $kunik ?>Params = {
	   	"jsonSchema" : {	
	           "title" : "<?php echo $label ?> config",
	           "icon" : "cog",
	           "properties" : {
	           		searchType : {
	                   inputType : "select",
	                   label : tradForm.definElementType,
	                   options :  <?php echo json_encode($elementType) ?>
	               }
	           },
	           save : function (data) {  
	           		tplCtx.value = {};
	           		$.each(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
	           			tplCtx.value[k] = $("#"+k).val();
	           		});	
	           		mylog.log("save tplCtx",tplCtx);

	           		if(typeof tplCtx.value == "undefined")
	           			toastr.error('value cannot be empty!');
	           		else {
	           		    dataHelper.path2Value( tplCtx, function(params) { 
							dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
	           		        urlCtrl.loadByHash(location.hash);
	           		    } );
	           		}
	           }	
	       }
	   };        

	   $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
	   //var tplCtx = {};
	       tplCtx.id = $(this).data("id");
	       tplCtx.collection = $(this).data("collection");
	       tplCtx.path = $(this).data("path");
	       dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
	   });


	});

	

</script>
<?php } ?>

