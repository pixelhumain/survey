<style type="text/css">
    select option, select optgroup{
        font-size: 12pt !important;
        padding-top: 3em !important;
    }

    .form-control{
        padding: .5em !important;
    }

    .nottopay{
        display: none;
    }

    .p-1{
        padding: .5em;
    }

    .align-bottom{
        vertical-align: bottom;
    }

    #paid-panel{
        position: fixed;
        width:100%;
        height:100%;
        top:0;
        bottom:0;
        right:0;
        left:0;
        background: white;
        z-index:9;
    }
</style>

<?php if($answer){ ?>

    <div class="col-md-6">
        <?php
            $types = array();
            if(!function_exists("get_types")){
                function get_types(&$types, $label){
                    $exist = false;
                    foreach ($types as $k => &$v) {
                        if($v==$label){
                            $exist = true;
                        }
                    }

                    if(!$exist){
                        array_push($types, "$label");
                    }
                }
            }

            # Initialize parameters data
            $paramsData = [
                "types" => [
                    "type0" => [
                        "type" => "Acteurs producteurs",
                        "label" => "",
                        "droit" => 0
                                
                    ],
                    "type1" => [
                        "type" => "Bénéficiaires",
                        "label" => "",
                        "droit" => 0
                    ]
                ],
                "toPay" => true,
                "configured" => false,
                "isSetApiTest" => false,
                "isSetApiLive" => false,
            ];

            # Set parameters data
            if( isset($form["params"][$kunik]["types"]) ){
                $paramsData["types"] = $form["params"][$kunik]["types"];
            }

            if(isset($form["params"][$kunik]["congratalation"])){
                $paramsData["congratalation"] = $form["params"][$kunik]["congratalation"];
            }

            if(isset($form["params"][$kunik]["apiTest"])){
                $paramsData["apiTestValue"] = $form["params"][$kunik]["apiTest"];
            }

            if(isset($form["params"][$kunik]["apiLive"])){
                $paramsData["apiLiveValue"] = $form["params"][$kunik]["apiLive"];
            }

            if(isset($form["params"][$kunik]["toPay"])){
                $paramsData["toPay"] = $form["params"][$kunik]["toPay"];
            }

            if(isset($form["params"][$kunik]["mollieMode"])){
                $mode = $form["params"][$kunik]["mollieMode"];
                $paramsData["mollieMode"] = $mode; 

                if(isset($form["params"][$kunik]["api".$mode]) && $form["params"][$kunik]["api".$mode]!=""){
                    $paramsData["configured"] = true;
                }

                if(isset($form["params"][$kunik]["apiLive"]) && $form["params"][$kunik]["apiLive"]!=""){
                    $paramsData["isSetApiLive"] = true;
                }

                if(isset($form["params"][$kunik]["apiTest"]) && $form["params"][$kunik]["apiTest"]!=""){
                    $paramsData["isSetApiTest"] = true;
                }
            }


            foreach ($paramsData["types"] as $tk => $tv){
                get_types($types, $tv["type"]);
            }
            
            
            // remove the to test directily (dev) "draft" => ["$exists" => false ]]
            //$theAnswers = PHDB::find("answers", array("user"=>Yii::app()->session["userId"], "form"=>$parentForm["_id"]->{'$id'}, "draft" => ['$exists' => false ]));
            
            
            $editParamsBtn = ($canEditForm) ? "<a href='javascript:;' 
                                                data-id='".$form["_id"]."' 
                                                data-collection='".Form::COLLECTION."' 
                                                data-path='params.".$kunik."' 
                                                class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'>
                                                <i class='fa fa-cog'></i> 
                                            </a>" : "";

            $isPaid = isset($answer["answers"]["isPaid"])?$answer["answers"]["isPaid"]:false;
            $paymentStatus = isset($answer["answers"]["status"])?$answer["answers"]["status"]:false;

            if($isPaid==true){
                $currentUser = PHDB::findOneById(Citoyen::COLLECTION, $_SESSION["userId"], ['links']);
                if(!isset($currentUser["links"]["memberOf"][$this->costum["contextId"]])){
                    $currentUser["links"]["memberOf"][$this->costum["contextId"]] = Array("type" => "organizations", "roles"=>["associé"]);
                    PHDB::update(Citoyen::COLLECTION, ["_id"=>$currentUser["_id"]], ['$set' => array("links.memberOf"=>$currentUser["links"]["memberOf"])]);
                }
            }
        ?>
            <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
                <?php echo $label.$editQuestionBtn." ".$editParamsBtn?>
            </h4>
        <?php echo $info; ?>

        <?php 
            $selected_type = (isset($answer["answers"][$form["id"]]["selecttype"]))?$answer["answers"][$form["id"]]["selecttype"]:""; 
            $nb_part = (isset($answer["answers"][$form["id"]]["nbPart"]))?$answer["answers"][$form["id"]]["nbPart"]:1;
        ?>

        <br>

        <div class="form-group">
                <select id="select<?= $key ?>" data-form="<?= $form["id"]?>" class="form-control saveOneByOne">
                    <option></option>
                    <?php foreach ($types as $gkey => $gvalue) { ?>
                        <optgroup label="<?php echo $gvalue ?>">
                            <?php foreach ($paramsData["types"] as $tkey => $tvalue) {
                                if($tvalue["type"]==$gvalue){
                                    echo '<option value="'.base64_encode(json_encode($tvalue)).'" '.(($selected_type==base64_encode(json_encode($tvalue)))?"selected":"").'>'.$tvalue["label"].(($paramsData["toPay"]!="false")?' ( '.$tvalue["droit"].' €  )':'').'</option>';
                                }
                            } ?>
                        </optgroup>
                    <?php } ?>
                </select>
        </div>

    <?php if(($paramsData["toPay"]!="false")){ ?>
        <div class="form-group">
            <label>NOMBRE DE PART :</label>
            <input type="number" id="nbPart" data-form="<?= $form["id"]?>" name="nbPart" min="1" value="1" class="col-sm-10 col-md-6 form-control saveOneByOne">
        </div>
        <br>
    <?php if(!$isPaid){ ?>
        <hr>
        <div class="form-group">
            <b>Montant Total à Payer : </b> 
            <span id="total">0 €</span>
        </div>
        <hr>
    <?php } ?>
    <?php if(isset($answer["answers"]["isPaid"]) && isset($answer["answers"]["status"])){ ?>
        <div class="form-group">
            
            <?php 
                $isPaid=$answer["answers"]["isPaid"]; 
                $paymentStatus = $answer["answers"]["status"]; 

                $statusTrad = "";
                $statusColor = "";

                if($paymentStatus=="open"){
                    $statusTrad = "Pas Terminé";
                    $statusColor = "warning";
                }else if($paymentStatus=="paid"){
                    $statusTrad = "Payé";
                    $statusColor = "success";
                }else if($paymentStatus=="expired"){
                    $statusTrad = "Expiré";
                    $statusColor = "danger";
                }else if($paymentStatus=="canceled"){
                    $statusTrad = "Annulé";
                    $statusColor = "danger";
                }else if($paymentStatus=="failed"){
                    $statusTrad = "Echoué";
                    $statusColor = "danger";
                }else if($paymentStatus=="settled"){
                    $statusTrad = "Payé";
                    $statusColor = "success";
                }else if($paymentStatus=="refunded"){
                    $statusTrad = "Remboursé";
                    $statusColor = "warning";
                }else if($paymentStatus=="partially refunded"){
                    $statusTrad = "Remboursé partiellement";
                    $statusColor = "warning";
                }else if($paymentStatus=="pending"){
                    $statusTrad = "En cours de transaction";
                    $statusColor = "info";
                }else{
                    $statusTrad = $paymentStatus;
                    $statusColor = "warning";
                }
            ?>

        <?php if(!$isPaid){ ?>
            <b>Statut : </b>
            <span class="label label-<?php echo $statusColor ?>">
                <?php echo mb_strtoupper($statusTrad); ?>
            </span>
        <?php } ?>
        </div>
        <?php if($isPaid==true){ ?>
            <div id="paid-panel" class="container">
                <br><br><br><br><br>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="panel panel-success">
                            <div class="panel-heading text-center">
                                <h4>Félicitation</h4>
                            </div>
                            <div class="panel-body text-center">
                                Le paiement de votre droit d'associé (<b id="total-paid"></b> ) a réussi. <br><br>

                                <p class="markdown"><?php echo isset($paramsData["congratalation"])?$paramsData["congratalation"]:"Merci de votre confiance" ?></p>

                                <div class="text-center text-success">
                                    <span class="fa fa-handshake-o" style="font-size:25pt"></span>
                                </div>
                            </div>
                            <div class="panel-footer bg-white text-center">
                                <a id="btn-facturation" class="btn btn-lg" href="javascript:;" onclick="$('#paid-panel').hide()" style="text-decoration : none;"> 
                                    Aller sur le formulaire
                                </a>
                                &nbsp; | &nbsp;
                                <a id="btn-facturation" class="btn btn-lg" href="<?= Yii::app()->baseUrl; ?>/survey/payment/invoice?id=<?= $this->costum['contextId'] ?>&slug=<?= $this->costum['slug']; ?>&user=<?= $_SESSION['userId']; ?>&form=<?= $answer["form"]; ?>" target="_blank" style="text-decoration : none;"> 
                                    Télécharger Facture &nbsp; <span class="fa fa-download"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    <?php } ?>
    <?php if(isset($paramsData["configured"]) && $paramsData["configured"]==true){ ?>
        <div class="form-group">
            <?php if(!$isPaid){ ?>
                <a id="btn-payement" disabled class="btn btn-success btn-lg" style="text-decoration : none;"> 
                    Proceder au Paiement &nbsp; <span class="fa fa-arrow-right"></span>
                </a>
                <img src="<?php echo Yii::app()->getModule("survey")->assetsUrl; ?>/images/secure-payement.png">
            <?php } ?>
            <span id="load-payment"></span>
        </div>
    <?php }else{ ?>
        <div class="alert alert-info">
            L'action de dévenir sociétaire est temporairement indisponible, veuillez revenir ultérieurement.
        </div>
    <?php } ?>

<?php } ?>
</div>

<div class="col-md-6 align-bottom">
    <div id="methods"></div>
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    
    var typeAssocie = <?php echo json_encode( $types ); ?>;        
    var isPaid = <?php echo json_encode($isPaid); ?>;
    var paymentStatus = <?php echo json_encode($paymentStatus); ?>;

    $(document).ready(function(){

        let kunik = "<?= $kunik ?>";
        let droit = {};
        let options = [];
        let thisForm = "<?=$form["id"]?>";
        let isAPIConfigured = <?= json_encode($paramsData['configured'])?>;
        let isSetApiTest = <?= json_encode($paramsData['isSetApiTest'])?>;
        let isSetApiLive = <?= json_encode($paramsData['isSetApiLive'])?>;
        
        // Initialize from answers
        $("#nbPart").val("<?= $nb_part ?>");

        $("#select<?=$key?>").val("<?= $selected_type ?>");

        if(isPaid || paymentStatus=="pending" || paymentStatus=="settled"){
            $("#btn-payement").attr("disabled", true);
            $("#btn-payement").hide();
            $(".saveOneByOne").attr("disabled", true);
            $("#selectCountry").attr("disabled", true);
            $("#inputAdd").attr("disabled", true);
            $("#select<?=$key?>").attr("disabled", true);
            $("#nbPart").attr("disabled", true);
        }else{
            if($("#select<?=$key?>").val()!=""){
                $("#btn-payement").attr("disabled", false);
            }

            <?php if(isset($paramsData["apiLiveValue"]) && isset($paramsData["apiTestValue"]) && $paramsData["apiLiveValue"]!="" && $paramsData["apiTestValue"]!="" && $paramsData["toPay"]!="false"){ ?>

            ajaxPost(
                null, 
                baseUrl+"/survey/payment/getpaymentmethods",
                {param1:thisForm, param2:kunik},
                function(response){
                    if(response["0"]){
                        $("#methods").append("<h4 class='description p-1'>Nous acceptons : </h4>");
                    }

                    for(const index in response) {
                        $("#methods").append('<img src="'+response[index]["image"]["size2x"]+'" width="65px" class="p-1">');
                    }
                },
                function(error){
                    //toastr.warning("Veuillez recharger la page pour voir tout les methodes de payments");
                }
            );
            <?php } ?>
        }

        if($("#select<?=$key?>").val()!=""){
            const st = JSON.parse(atob($("#select<?= $key ?>").val()));
            $("#total, #total-paid").text(st.droit*$("#nbPart").val()+" €");
        }

        sectionDyf.<?php echo $kunik ?>ParamsSelect = {
            "jsonSchema" : {
                "title" : "Paramétrage du <?php echo $label ?>",
                "description" : "",
                "icon" : "cog",
                "properties" : {
                    apiTest:{
                        inputType: "text",
                        placeholder: (isSetApiTest==true)?"******************":"",
                        label : "Votre clé api mollie pour teste : "
                    },

                    apiLive:{
                        inputType: "text",
                        placeholder: (isSetApiLive==true)?"******************":"",
                        label : "Votre clé api mollie pour live :"
                    },

                    mollieMode : {
                        "inputType" : "select",
                        "label" : "Mode d'utilisation mollie :",
                        "class" : "form-control",
                        "options": {
                            "Test":"Teste",
                            "Live":"Live",
                        },
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.mollieMode
                    },

                    congratalation : {
                        "inputType" : "textarea",
                        "label" : "Texte de félicitation après paiement:",
                        "class" : "form-control",
                        "markdown": true,
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.congratalation
                    },

                    types : {
                        inputType : "lists",
                        label : "Type d'associé",
                        entries: {
                            type: {
                                label:"type :",
                                type:"text",
                                class: "col-md-3 autocomplete"
                            },
                            label: {
                                label:"Sous type :",
                                type:"textarea",
                                class:"col-md-6"
                            },
                            droit: {
                                label:"Droit :",
                                type:"text",
                                class:"col-md-2",
                                placeholder:"En €"
                            }
                        },
                    },
                    toPay : {
                        "label" : "Avec payement",
                        "inputType" : "checkboxSimple",
                        "rules" : {
                            "required" : true
                        },
                        "params" : {
                            "onText" : "Oui",
                            "offText" : "Non",
                            "onLabel" : "Oui",
                            "offLabel" : "Non"
                        },
                        "checked" : false,
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.toPay
                    },
                },
                save : function (data) {
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>ParamsSelect.jsonSchema.properties , function(k,val) {

                        if(k=="types"){
                            let types = {};
                            $.each(data.types, function(index, va){
                                let tpa = {type: va.type,label: va.label, droit: va.droit};
                                types["type"+index] = tpa;
                            });
                            tplCtx.value[k] = types;
                        }else{
                            tplCtx.value[k] = $("#"+k).val();
                        }

                        if(k=="apiLive" || k=="apiTest"){
                            if(sectionDyf.<?php echo $kunik?>ParamsData[k]!=""){
                                tplCtx.value[k] = sectionDyf.<?php echo $kunik?>ParamsData[k+"Value"];
                            }else{
                                tplCtx.value[k] = btoa($("#"+k).val());
                            }
                        }

                        mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                    });
                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == undefined)
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                            location.reload();
                        } );
                    }
                }
            }
        };

        <?php if($paramsData["toPay"]!="false"){ ?>
        $(document).on('change','#select<?= $key ?>, #nbPart',function(){
            var nbPartValue = (!isNaN($("#nbPart").val()) && $("#nbPart").val()!=0)?$("#nbPart").val():1;
            var st = ($("#select<?= $key ?>").val()!="")?JSON.parse(atob($("#select<?= $key ?>").val())):0;
            //alert(st.droit+" - "+nbPartValue);
            $("#total").text(st.droit*$("#nbPart").val()+" €");
            
            if(st.droit*$("#nbPart").val()==0){
                $("#btn-payement").attr("disabled", true);
            }else{
                $("#btn-payement").text("Proceder au paiement de "+st.droit*$("#nbPart").val()+" €");
                $("#btn-payement").attr("disabled", false);
            }

            $(this).blur();
        });

        $("#btn-payement").off().on("click", function(){
            // Save all before pay
            
            var isValid = true;

            $('.saveOneByOne').each(function() {
                if ( $(this).val() === '' ){
                    isValid = false;
                }   
            });

            if(isValid){
                $(".saveOneByOne").blur();
                coInterface.showLoader("#load-payment");
                const st = JSON.parse(atob($("#select<?= $key ?>").val()));
                
                let paymentData = {
                    'amount' :{
                        'currency' : "EUR",
                        'value' : ""+parseFloat(st.droit*$("#nbPart").val()).toFixed(2)+""
                    },
                    'description': "Droit d'associé en tant que "+st.type+" à titre de "+st.label,
                    'page' : window.location.href,
                    'source': "<?=$answer["form"]?>",
                    'nombre' : $("#nbPart").val(),
                    'orga': contextId,
                    'inputSource': thisForm+"."+kunik
                };

                ajaxPost(
                    null,
                    baseUrl+"/survey/payment/pay",
                    paymentData,
                    function(response){
                        if(response.url){
                            location = response.url;
                        }
                        if(response.message){
                            toastr.error(response.message);
                        }
                    },
                    function(xhr,textStatus,errorThrown,data){
                        toastr.error("Une erreur s'est produite, veuillez signaler ce problème à notre administrateur");
                    }
                );
            }else{
                toastr.warning("Veuillez remplir toutes avant de payer votre droit d'associé ")
            }
        });

    <?php } ?>

        //adds a line into answer
        $(".add<?php echo $kunik ?>").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
        });

        $(".edit<?php echo $kunik ?>").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = $(this).data("path");
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
        });

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = $(this).data("path");
            //if no params config on the element.costum.form.params.<?php echo $kunik ?>
            //then we load default values available in forms.inputs.<?php echo $kunik ?>xxx.params
            //mylog.log(".editParams",sectionDyf.<?php echo $kunik ?>Params,calData);
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>ParamsSelect,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });

</script>
<?php } else {
    //echo "<h4 class='text-red'>evaluation works with existing answers</h4>";
} ?>