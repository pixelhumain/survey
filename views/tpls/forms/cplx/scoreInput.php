<style>
.show-score {
	border: 2px solid darkgrey;
	padding: 20px;
	border-radius: 20px; 
	overflow-x: auto; 
	text-align: center;
}

.scoreContent {
	margin-top: 150px;
}
</style>
<?php 
	if($mode == "fa"){
		$editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";
	}
	
	$paramsData = [ "selectStep" => ""];
	if(isset($parentForm["params"][$kunik]["selectStep"]))
		$paramsData["selectStep"] = $parentForm["params"][$kunik]["selectStep"];

	$getSelected = "";
	if($paramsData["selectStep"] != "")
	$getSelected = PHDB::findOneById(Form::COLLECTION, (string)$paramsData["selectStep"], ['id','inputs','name']);

	$answerResult = 0;
	if (isset($getSelected["inputs"])) { 
		foreach ($getSelected["inputs"] as $key => $value) {
			if (isset($getSelected["id"]) && isset($answer["answers"][$getSelected["id"]]) && isset($answer["answers"][$getSelected["id"]][$key])) {			
				if (isset($value["type"]) && strpos($value["type"], "radioNew") !== false && !is_nan($answer["answers"][$getSelected["id"]][$key])) {
					$answerResult = $answerResult + intval($answer["answers"][$getSelected["id"]][$key]);
				}
				if (isset($value["type"]) && strpos($value["type"], "checkboxNew") !== false && is_array($answer["answers"][$getSelected["id"]][$key])) {
					$answerResult = $answerResult + count($answer["answers"][$getSelected["id"]][$key]);
				}
			}
		}
	}

	$sameFormId = false;
	if (!empty($getSelected["id"]) && $getSelected["id"] == $form["id"]) {
		$sameFormId = true;
	}

	$scoreId = (isset($getSelected["_id"]) && !empty($getSelected["_id"])) ? $getSelected["_id"] : $form["_id"];
	$labelStep = (isset($getSelected["name"])) ? $getSelected["name"] : $form["name"];
	$labelStep = '"'.$labelStep.'"';

	$redo = ($mode == "fa" || $mode == "w") ? "<a href='javascript:;' class='refreshBtn".$kunik." btn btn-xs btn-danger'><i class='fa fa-refresh'></i> Recommencer </a>" : "";
?>
<div class="show-score">
	<h4 class="score<?php echo $kunik ?>Params"> Score total <span><?= $labelStep ?></span> :  <span class="score<?= $scoreId ?>"></span></h4> <?php echo $editQuestionBtn.@$editParamsBtn; ?>
	<?= $redo ?>
</div>
<script>
	let sameForm = "<?= $sameFormId ?>"
	let selectedStep = "";
	<?php if ($getSelected != "") { ?>
		selectedStep = "<?= $getSelected["id"] ?>";
	<?php } ?>

	// refresh
	let formid = (selectedStep != "") ? selectedStep : "<?= $form["id"] ?>";
	$('.refreshBtn<?= $kunik ?>').click(function () { 
		let params = {
			id : "<?= (string)$answer["_id"] ?>",
			collection : "answers",
			value: {},
			path: (selectedStep != "") ? "answers." + selectedStep : "answers." + formid	
		}

		$('input[type="checkbox"][data-form="'+formid+'"]').each(function() {
            $(this).prop('checked',false);
        }) 
		dataHelper.path2Value(params, function(params){
		toastr.success(tradForm.modificationSave);
			if (typeof formid != "undefined") {
				jsonHelper.setValueByPath(answerObj, "answers." + formid, {});
			}
		});
		reloadWizard();
	})

	// test 
	$(".score<?= $scoreId ?>").text("0")

	// if radioNew 
	if (("<?= @$getSelected["_id"] ?>" == "") || sameForm == 1) {
		const countResult = (selector) => {
			radioResult = 0;
			try {
			let allAnswer = answerObj.answers["<?= @$form["id"] ?>"];	
				if (typeof selector != "undefined") {
					answerObj.answers["<?= @$form["id"] ?>"][$(selector).attr("name").replace("radioNew","")] = $(selector).data('value');
				}
				$.each(allAnswer, function(k,v){
					if (!isNaN(v)) {
						radioResult = radioResult + Number(v)
					}
				});
			} catch (e) { }
			$(".score<?= $scoreId ?>").text(radioResult);
		}
		countResult();
		$('input[type="radio"][data-form="<?= @$form["id"] ?>"]').click(function() {
			countResult(this);
		})

	// if checkbox
		let checkboxResult = 0
		const countChecked = (selector) => {
			if($(selector).prop('checked')) {
				checkboxResult ++;
			}
			$(".score<?= $scoreId ?>").text(checkboxResult);
		} 

		$('input[type="checkbox"][data-form="<?= $form["id"] ?>"]').each(function() {
			countChecked(this);
		})

		$('input[type="checkbox"][data-form="<?= $form["id"] ?>"]').click(function() {
			if(!$(this).prop('checked')) {
				checkboxResult --;
			} 
			// alert($(this).data("value"))
			
			countChecked(this);
		}) 
	} else {
		$(".score<?= $scoreId ?>").text("<?= $answerResult ?>");
	}
	// alert("<?= @$getSelected["_id"] ?>")
	sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "<?php echo $label ?> config",
	        "icon" : "cog",
	        "properties" : {
				selectStep : {
                    label : 'Séléctionner une étape où ajouter les questions',
                    inputType: 'select',
                    select2 : true,
                    placeholder : 'Sélectionner une étape',
                    options : (typeof completSubForms != "undefined") ? completSubForms : "",
                    value : ("<?= @$getSelected["_id"] ?>" != "") ? "<?= @$getSelected["_id"] ?>" : "<?= $form["_id"] ?>",
                }
	        },
	        save : function () {  
	            tplCtx.value = {};
				let result = (jsonHelper.pathExists("formData.selectStep")) ? formData.selectStep : sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties.selectStep.value;
				jsonHelper.setValueByPath(tplCtx, "value.selectStep", result);
				
	            mylog.log("save tplCtx",tplCtx);
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
	                    urlCtrl.loadByHash(location.hash);
	                } );
	            }

	    	}
	    }
	};

	// edit btn
	$(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
		dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
	})
</script>