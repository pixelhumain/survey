<?php if($answer){ 
	?>
<div class="form-group">
	<table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>">
		
<?php 
$editBtnL = ($canEdit === true) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";

$editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$el["_id"]."' data-collection='".$this->costum["contextType"]."' data-path='costum.form.params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

$paramsData = [ 
	"role" => [
          "developement",
          "graphiste"
    ] ];

if( isset($this->costum["form"]["params"][$kunik]["role"]) ) 
	$paramsData["role"] =  $this->costum["form"]["params"][$kunik]["role"];

$properties = [
		"titre_sous_action" => [
            "label" => "Titre de la sous action",
            "placeholder" => "Titre de la sous action",
            "inputType" => "text",
            "rules" => [ "required" => true ]
        ],
        "role" => [
            "placeholder" => "Role",
            "inputType" => "select",
            "options" => $paramsData["role"],
            "rules" => [ "required" => true ]
        ],
        "assigned" => [
            "inputType" => "text",
            "label" => "Assigné",
            "placeholder" => "Assigné",
            "rules" => [ "required" => true  ]
        ],
        "jours_homme" => [
            "inputType" => "text",
            "label" => "Jours homme",
            "placeholder" => "Jours homme",
            "rules" => [ "required" => true, "number" => true ]
        ],
        "prix" => [
            "inputType" => "text",
            "label" => "Prix",
            "placeholder" => "Prix",
            "rules" => [ "required" => true, "number" => true ]
        ]
    ];

?>	

	<thead>
		<tr>
			<td colspan='<?php echo count( $properties)+2?>' ><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
				<?php echo $info ?>
			</td>
		</tr>	
		<?php 
		
		if( count($answers)>0 ){ ?>
		<tr>
			</th>
			<?php 
			
			foreach ($properties as $i => $inp) {
				echo "<th>".$inp["placeholder"]."</th>";
			} ?>
			<th></th>
		</tr>
		<tr></tr>
		<?php } ?>
	</thead>
<tbody class="directoryLines">	
		<?php 
		$ct = 0;
		
		if(isset($answers)){
			foreach ($answers as $q => $a) {

				echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
				foreach ($properties as $i => $inp) {
					echo "<td>";
					if(isset($a[$i])) {
						if(is_array($a[$i]))
							echo implode(",", $a["role"]);
						else
							echo $a[$i];
					}
					echo "</td>";
				}
			?>
			<td>
				<?php 
					echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [

						"canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
						"id" => $answer["_id"],
						"collection" => Form::ANSWER_COLLECTION,
						"q" => $q,
						"path" => $answerPath.$q,
						"keyTpl"=>$kunik
					] ); ?>

				<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
			</td>
			<?php 
				$ct++;
				echo "</tr>";
			}
		}

?>
		</tbody>
	</table>
</div>
<script type="text/javascript">

var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

$(document).ready(function() { 
	
	sectionDyf.<?php echo $kunik ?> = {
		"jsonSchema" : {	
	        "title" : "Budget prévisionnel",
            "icon" : "fa-money",
            "text" : "Décrire ici les principaux postes de dépenses : à quoi correspondent les coûts ? <br/>Préciser ce qui relève des dépenses de fonctionnement et des dépenses d’investissement.<br/>Les coûts doivent être en <b>hors taxe</b>.",
	        "properties" : <?php echo json_encode( $properties ); ?>,
	        save : function () {  
	        	var today = new Date();
	            tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
	            $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });

	            mylog.log("save tplCtx",tplCtx);
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");//$("#ajax-modal").modal('hide');
	                    location.reload();
	                } );
	            }

	    	}
	    }
	};

	sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "<?php echo $kunik ?> config",
	        "description" : "Liste de question possible",
	        "icon" : "fa-cog",
	        "properties" : {
	            role : {
	                inputType : "array",
	                label : "Liste des roles",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.role
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	        		if(val.inputType == "properties")
	        		 	tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
	        		else if(val.inputType == "array")
	        		 	tplCtx.value[k] = getArray('.'+k+val.inputType);
	        		else
	        		 	tplCtx.value[k] = $("#"+k).val();
	        		 mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
	                    location.reload();
	                } );
	            }

	    	}
	    }
	};


    mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

    //adds a line into answer
    $(".add<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
    });

    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
    });

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        //if no params config on the element.costum.form.params.<?php echo $kunik ?>
        //then we load default values available in forms.inputs.<?php echo $kunik ?>xxx.params
        //mylog.log(".editParams",sectionDyf.<?php echo $kunik ?>Params,calData);
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    
});
</script>
<?php } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>