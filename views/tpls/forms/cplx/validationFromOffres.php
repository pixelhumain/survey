<?php if($answer){ 
	$copy = "opalProcess1.depense";

	if( isset($this->costum["form"]["params"][$kunik]["offresCopy"]) ) 
		$copy = $this->costum["form"]["params"][$kunik]["offresCopy"];
	//var_dump($copy);
	$copyT = explode(".", $copy);
	$copyF = $copyT[0];
	$offresKey = $copyT[1];
	$answers = null;	
	//var_dump($offresKey);
	if($wizard){
		if( $offresKey ){
			if( isset($answer["answers"][$copyF][$offresKey]) && count($answer["answers"][$copyF][$offresKey])>0)
				$answers = $answer["answers"][$copyF][$offresKey];
		} else if( isset($answer["answers"][$copyF][$kunik]) && count($answer["answers"][$copyF][$offresKey])>0 )
			$answers = $answer["answers"][$copyF][$kunik];
	} else {
		if($offresKey)
			$answers = $answer["answers"][$offresKey];
		else if(isset($answer["answers"][$kunik]))
			$answers = $answer["answers"][$kunik];
	}

	
$editBtnL =  "";

$editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$el["_id"]."' data-collection='".$this->costum["contextType"]."' data-path='costum.form.params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";


$paramsData = [ 
	"group" => ["Feature","Costum","Chef de Projet","Data","Mantenance"],
	"nature" => [
        "investissement" => "Investissement",
        "fonctionnement" => "Fonctionnement"
    ],
	"amounts" => [
    	"price" => "Price"
    ]
];

if( isset($this->costum["form"]["params"][$kunik]) ) {
	if( isset($this->costum["form"]["params"][$offresKey]["group"]) ) 
		$paramsData["group"] =  $this->costum["form"]["params"][$offresKey]["group"];
	if( isset($this->costum["form"]["params"][$offresKey]["nature"]) ) 
		$paramsData["nature"] =  $this->costum["form"]["params"][$offresKey]["nature"];
	if( isset($this->costum["form"]["params"][$offresKey]["amounts"]) ) 
		$paramsData["amounts"] =  $this->costum["form"]["params"][$kunik]["amounts"];
}

$communityLinks = Element::getCommunityByTypeAndId($this->costum["contextType"],$this->costum["contextId"]);
$organizations = Link::groupFindByType( Organization::COLLECTION,$communityLinks,["name","links"] );

$orgs = [];

foreach ($organizations as $id => $or) {
	$roles = $or["links"]["memberOf"][$this->costum["contextId"]]["roles"];
	if( $paramsData["limitRoles"] && !empty($roles))
	{
		foreach ($roles as $i => $r) {
			if( in_array($r, $paramsData["limitRoles"]) )
				$orgs[$id] = $or["name"];
		}		
	}
}
//var_dump($orgs);exit;
$listLabels = array_merge($orgs);//Ctenat::$financerTypeList,

$properties = [
    
    "poste" => [
        "inputType" => "text",
        "label" => "Contexte",
        "placeholder" => "Contexte",
        "rules" => [ "required" => true ]
    ]
];
foreach ($paramsData["amounts"] as $k => $l) {
	$properties[$k] = [ "inputType" => "text",
			            "label" => $l,
			            "propType" =>"amount",
			            "placeholder" => $l,
			            "rules" => [ "required" => true, "number" => true ]
			        ];
}
$properties["votes"] = [
    "inputType" => "text",
    "label" => "Votes",
    "placeholder" => "Votes",
    "rules" => [ "required" => true,"number" => true  ]
];

	?>	
<div class="form-group">
	<table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>">

	<thead>
		<tr>
			<td colspan='<?php echo count( $properties)+2?>' ><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
				<?php echo $info;
				if( !isset($this->costum["form"]["params"][$kunik]["offresCopy"]) ) 
					echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> THIS FIELD HAS TO BE CONFIGURED FIRST ".$editParamsBtn."</span>"; ?>
			</td>
		</tr>	
		<?php if($answers){ ?>
		<tr>
			</th>
			<?php 
			
			foreach ($properties as $i => $inp) {
				echo "<th>".$inp["placeholder"]."</th>";
			} ?>
			<th></th>
		</tr>
		<?php } ?>
	</thead>
	<tbody class="directoryLines">	
		<?php 
		$ct = 0;
		$totalVotes = 0;
		$bigTotal = 0;
		$totalPayed = 0;
		$validDecision = 0;
		$invalidDecision = 0;
		$payedWork = 0;
		if($answers){
			foreach ($answers as $q => $a) {

				$trStyle = "";
				$tds = "";
				foreach ($properties as $i => $inp) 
				{
					$tds .= "<td style='vertical-align:middle'>";
					
					if( $i == "poste") 
						$tds .= $a["poste"];
					if( $i == "price" ) 
						$tds .= $a["price"]."€";
					else if( $i == "workType" && isset( $a["worker"]["workType"] ) ) 
						$tds .= $a["worker"]["workType"];
					else if( $i == "votes"){
						$votes = (!empty($a["votes"]) ) ? (int)$a["votes"] : 0;
						

						$thisVoteTotal = 0;
						$percol = "danger";
						$youVoted = "";
						if(!empty($a["votes"]) ){
							$youVoted = "<span class='label label-default'>".count($a["votes"])."voter(s)"."</span>";
							foreach ($a["votes"] as $uid => $v) {
								$thisVoteTotal += (int)$v["vote"];
							}
							$thisVoteTotal = $thisVoteTotal / count($a["votes"]);
							if( $thisVoteTotal > 50 ){
								$percol = "warning";
								$validDecision++;
							}
							if( $thisVoteTotal > 75 )
								$percol = "success";
							if( $thisVoteTotal < 50 )
								$invalidDecision++;


							if( isset( $a["votes"][ Yii::app()->session["userId"] ] ) ){
								$col = "danger";
								if( (int)$a["votes"][Yii::app()->session["userId"]]["vote"] > 50 )
									$col = "success";
								$youVoted .= "<br/><span class='label label-".$col."'> you : ".$a["votes"][Yii::app()->session["userId"]]["vote"]."%</span>";
							}
						}
						$totalVotes += $thisVoteTotal;
						
						$tds .= "<a href='javascript:;' data-id='".$answer["_id"]."' data-offrespath='".$copy."' data-form='".$copyF."' data-pos='".$q."'  class=' btnVote margin-left-5 padding-10'>".$thisVoteTotal."%</a>";
						$tds .= '<div class="progress btnVote"  data-id="'.$answer["_id"].'" data-offrespath="'.$copy.'" data-form="'.$copyF.'" data-pos="'.$q.'"  style="cursor:pointer;margin-bottom: 0px;">'.
						  '<div class="progress-bar progress-bar-'.$percol.'" style="width:'.$thisVoteTotal.'%">'.
							    '<span class="sr-only">'.$thisVoteTotal.'% Complete</span>'.
						  '</div>'.
						'</div>'.$youVoted;
					}
					else if( isset( $a[$i] ) ) 
						$tds .= $a[$i];
					
					$tds .= "</td>";
				}
					
			?>
			
			<?php 
				$ct++;
				echo "<tr id='".$kunik.$q."' class='".$kunik."Line text-center' style='".$trStyle."'>";
				echo $tds;?>
				<td>
					<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
				</td>
			</tr>
			<?php 
			}
		}
?>
		</tbody>
	</table>

<?php 
$percol = "danger";
$totalVotes = (!empty($answers)) ? $totalVotes / count($answers) : 0;
if( $totalVotes > 50  ){
	$percol = "success";
}
echo "<h4 style='color:".(($titleColor) ? $titleColor : "black")."'>Décision Globale</h4>".
'<div class="progress " style="cursor:pointer" >'.
  '<div class="progress-bar progress-bar-'.$percol.'" style="width:'.$totalVotes.'%">'.
	    '<span class="sr-only">'.$totalVotes.'% Complete</span>'.
  '</div>'.
'</div>'; ?>

<table class="table table-bordered table-hover  ">
	<tbody class="">
		<tr>
			<td>Pour</td>
			<td><?php echo floor($totalVotes) ?>%</td>
		</tr>
		<tr>
			<td>Contre</td>
			<td><?php echo floor(100-$totalVotes)+1 ?>%</td>
		</tr>
		<tr>
			<td>Nombres de Décisions Validés </td>
			<td><?php echo $validDecision."/".count($answers) ?></td>
		</tr>
		<tr>
			<td>Nombres de Décisions Refusés </td>
			<td><?php echo $validDecision."/".count($answers) ?></td>
		</tr>
		
	</tbody>
</table>


</div>

<div class="form-worker" style="display:none;">
	<select id="worker" style="width:100%;">
		<option>Choisir un dfgd maitre d'ouvrage</option>
		<?php foreach ($orgs as $v => $f) {
			echo "<option value='".$v."'>".$f."</option>";
		} ?>
	</select>
	<br>
	Type de travaux effectués : <br/>
	<input type="text" id="workType" name="workType" style="width:100%;">
	<br><br>
	<span class="bold">Organisme qui effectue les travaux , s'il n'existe pas, créez le et ajoutez le à la communauté ici 
		<a class="btn btn-primary">Ajouter un maitre d'ouvrage</a>
	</span>
	<br>
</div>

<div class="form-votes" style="display:none;">
	<select id="votes" style="width:100%;">
		<option> VOTEZ pour cette partie </option>
		<?php foreach ([0,10,20,30,40,50,60,70,80,90,100] as $v => $f) {
			if($f == 0)
				$lbl = "Je ne suis pas pour!";
			else 
				$lbl = "Je suis pour à ".$f."%";
			echo "<option value='".$f."'>".$lbl."</option>";
		} ?>
	</select>
  
</div>





<?php 
if( isset($this->costum["form"]["params"]["financement"]["tpl"])){
	//if( $this->costum["form"]["params"]["financement"]["tpl"] == "tpls.forms.equioffres" )
		echo $this->renderPartial( "costum.views.".$this->costum["form"]["params"]["financement"]["tpl"] , 
		[ "totalFin"   => $total,
		  "totalBudg" => Yii::app()->session["totaloffres"]["totaloffres"] ] );
	// else 
	// 	$this->renderPartial( "costum.views.".$this->costum["form"]["params"]["financement"]["tpl"]);
}
 ?>

<script type="text/javascript">
if(typeof costum.lists == "undefined")
	costum.lists = {};
//costum.lists.financerTypeList = <?php //echo json_encode(Ctenat::$financerTypeList); ?>;
costum.lists.workerList = <?php echo json_encode($orgs); ?>;


var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answer["answers"][$copy])) ? $answer["answers"][$copy] : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

$(document).ready(function() { 
	
	sectionDyf.<?php echo $kunik ?> = {
		"jsonSchema" : {	
	        "title" : "Plan de Financement",
            "icon" : "fa-money",
            "text" : "Décrire ici les financements mobilisés ou à mobiliser. Les coûts doivent être en <b>hors taxe</b>.",
	        "properties" : <?php echo json_encode( $properties ); ?>,
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");//$("#ajax-modal").modal('hide');
	                    location.reload();
	                } );
	            }

	    	}
	    }
	};

	sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "<?php echo $kunik ?> config",
	        "icon" : "fa-cog",
	        "properties" : {
	            limitRoles : {
	                inputType : "array",
	                label : "Liste des roles financeurs",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.limitRoles
	            },
	            tpl : {
	                label : "Sub Template",
	                value :  sectionDyf.<?php echo $kunik ?>ParamsData.tpl
	            },
	            offresCopy : {
	            	label : "Input Bugdet",
	            	inputType : "select",
	            	options :  costum.lists.offresInputList
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	        		if(val.inputType == "array")
	        		 	tplCtx.value[k] = getArray('.'+k+val.inputType);
	        		else if(val.inputType == "properties")
	        		 	tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
	        		else
	        		 	tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
	                    location.reload();
	                } );
	            }

	    	}
	    }
	};


    mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

    //adds a line into answer
    $(".add<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
    });

    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
    });

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    
    $('.btnValidateWork').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.offrespath = $(this).data("offrespath");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.form = $(this).data("form");
		prioModal = bootbox.dialog({
	        message: $(".form-validate-work").html(),
	        title: "Validation des travaux",
	        show: false,
	        buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {
			            
			        	var formInputsHere = formInputs;
			        	tplCtx.path = "answers";
			        	if( notNull(formInputs [tplCtx.form]) )
			        		tplCtx.path = "answers."+tplCtx.form;    

			        	tplCtx.path = tplCtx.path+"."+tplCtx.offrespath+"."+tplCtx.pos+".validFinal";	   
			        	
			        	var today = new Date();
						today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();	
			            tplCtx.value = { 
		        			valid : $(".bootbox #validDecision").val(),
		        			user : userId,
		        			date : today
		        		};
		        		delete tplCtx.pos;
		        		delete tplCtx.offrespath;
				    	mylog.log("btnValidateWork save",tplCtx);
				  	 	dataHelper.path2Value( tplCtx, function(){
				  	 		saveLinks(answerObj._id.$id,"validated",userId,closePrioModalRel);
				  	 	} );
				  	 }		
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: closePrioModal
                }
              },
		        onEscape: closePrioModal
	    });
	    prioModal.modal("show");
	});

	$('.btnVote').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.offrespath = $(this).data("offrespath");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.form = $(this).data("form");
		prioModal = bootbox.dialog({
	        message: $(".form-votes").html(),
	        title: "Voter pour partie",
	        show: false,
	        buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {
			        	tplCtx.path = "answers."+tplCtx.offrespath+"."+tplCtx.pos+".votes."+userId; 

			        	var today = new Date();
						today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();	
			            tplCtx.value = {
			            	vote : $(".bootbox #votes").val(),
			            	date : today
			            };

		        		delete tplCtx.pos;
		        		delete tplCtx.offrespath;
				    	mylog.log("btnProgress save",tplCtx);
				  	 	dataHelper.path2Value( tplCtx, function(){
				  	 		saveLinks(answerObj._id.$id,"voted",userId);
				  	 	} );
			        }		
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: closePrioModal
                }
              },
		        onEscape: closePrioModal
	    });
	    prioModal.modal("show");
	});

});
function closePrioModal(){
	prioModal.modal('hide');
}
function closePrioModalRel(){
	closePrioModal();
	location.reload();
}
</script>
<?php } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>