<?php 
if($saveOneByOne)
	$inpClass = " ";
	if($mode == "fa"){
		$editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";
	}
	$paramsData = [ "list" => [	] ];
	$currentAnswer = (!empty($answer) && isset($answer["answers"][$form["id"]][$key])) ? $answer["answers"][$form["id"]][$key] : [];
	if( isset($parentForm["params"][$kunik]) ) {
		if( isset($parentForm["params"][$kunik]["list"]) ) 
			$paramsData["list"] =  $parentForm["params"][$kunik]["list"];
	}
?>
<style>
	/* The container */
	.container<?= $kunik ?> {
		display: block;
		position: relative;
		padding-left: 30px;
		margin-bottom: 12px;
		cursor: pointer;
		font-size: 1.6rem;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}

	/* Hide the browser's default checkbox */
	.container<?= $kunik ?> input {
	position: absolute;
	opacity: 0;
	cursor: pointer;
	height: 0;
	width: 0;
	}

	/* Create a custom checkbox */
	.checkmark<?= $kunik ?> {
	position: absolute;
	top: 0;
	left: 0;
	height: 20px;
	width: 20px;
	background-color: #eee;
	}

	/* On mouse-over, add a grey background color */
	.container<?= $kunik ?>:hover input ~ .checkmark<?= $kunik ?> {
	background-color: #ccc;
	}

	/* When the checkbox is checked, add a blue background */
	.container<?= $kunik ?> input:checked ~ .checkmark<?= $kunik ?> {
	background-color: #2196F3;
	}

	/* Create the checkmark/indicator (hidden when not checked) */
	.checkmark<?= $kunik ?>:after {
	content: "";
	position: absolute;
	display: none;
	}

	/* Show the checkmark when checked */
	.container<?= $kunik ?> input:checked ~ .checkmark<?= $kunik ?>:after {
	display: block;
	}

	/* Style the checkmark/indicator */
	.container<?= $kunik ?> .checkmark<?= $kunik ?>:after {
	left: 7px;
	top: 4px;
	width: 5px;
	height: 10px;
	border: solid white;
	border-width: 0 3px 3px 0;
	-webkit-transform: rotate(45deg);
	-ms-transform: rotate(45deg);
	transform: rotate(45deg);
	}
</style>
<?php  
    if($mode == "r" || $mode == "pdf"){ ?>
    <div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
        <label for="<?php echo $kunik ?>">
        	<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
        		<?php echo $label ?>
        	</h4>
        </label><br/>
		<ul>
		<?php foreach ($answers as $kans => $vans) {
			echo "<li>".$vans."</li>";
		} ?>
		</ul>
    </div>
<?php }else{ ?>
	<div class="form-check text-left">
		<label class="form-check-label" for="<?php echo $key ?>">
			<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.@$editParamsBtn ?></h4>
		</label><br/>	
		<?php if(!empty($info)){ ?>
			<small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
		<?php } ?>
		<?php 
		if( !isset($parentForm["params"][$kunik]['list']) ) {
			echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i>  ".Yii::t("survey","THIS FIELD HAS TO BE CONFIGURED FIRST").@$editParamsBtn."</span>";
		} else {
			foreach ($parentForm["params"][$kunik]["list"] as $ix => $lbl) { 
				$checkedValue =  "";
				if(in_array($lbl,$currentAnswer))
					$checkedValue =  "checked";
				?>
				<label class="container<?= $kunik ?>"><?= $lbl ?>
					<input type="checkbox" class="form-check-input <?php echo $inpClass ?>  <?= $kunik ?>"  id="<?php echo $kunik.$ix ?>" data-value="<?= $lbl ?>" data-form='<?php echo $form["id"] ?>' <?php echo $checkedValue?>  >
					<span class="checkmark<?= $kunik ?>"></span>
				</label>
			<?php } 
		}?>
	</div>

	<script type="text/javascript">
	var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answer["answers"][$kunik])) ? $answer["answers"][$kunik] : null ); ?>;
	sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

	jQuery(document).ready(function() {
		mylog.log("render form input","/modules/costum/views/tpls/forms/cplx/multiCheckbox.php");
		var currentAnswer = <?php echo json_encode( $currentAnswer ); ?>;
		$('.<?= $kunik ?>').change(function(e) {
			e.stopImmediatePropagation();
			var value<?= $kunik ?> = $(this).data("value");
			var path<?= $kunik ?> = "answers."+$(this).data("form")+".<?= $key ?>";
			const dataId = "<?= $key ?>"
			const val = $(`input.checkboxNew${dataId}[type='checkbox']:checked`).length;
			if(typeof newReloadStepValidationInputGlobal != "undefined") {
				// newReloadStepValidationInputGlobal(null)
			} else {
				if(typeof ownAnswer != 'undefined' && typeof ownAnswer[$(this).attr("data-form")] != 'undefined' && val > 0) {
					ownAnswer[$(this).attr("data-form")][dataId] = val
				} else if(typeof ownAnswer != 'undefined' && typeof ownAnswer[$(this).attr("data-form")] != 'undefined'){
					ownAnswer[$(this).attr("data-form")][dataId] = ""
				}
			}
			if(typeof removeError != "undefined") {
				const elem = $(`li#question${dataId.replace('checkboxNew', '')},#question_${dataId.replace('checkboxNew', '')}`);
				if(val > 0){
					removeError(
						elem, 
						{
							'border' : 'none'
						},
						'error-msg'
					)
				} else if(typeof addError != 'undefined'){
					// addError(
					// 	elem,
					// 	{
					// 		'border' : '1px solid #a94442'
					// 	},
					// 	'error-msg'
					// )
				}
			}
			if (this.checked) {
				dataHelper.path2Value({
					id : "<?= (string)$answer["_id"] ?>",
					collection : "answers",
					arrayForm: true,
					edit:false,
					value: value<?= $kunik ?>,
					path: path<?= $kunik ?>
				}, function(params){
					//toastr.success(tradForm.modificationSave);
					if(typeof newReloadStepValidationInputGlobal != "undefined") {
						newReloadStepValidationInputGlobal({
							inputKey : <?php echo json_encode($key); ?>,
							inputType : "tpls.forms.cplx.checkboxNew" 
						})
					}
				} );
			} else {
				dataHelper.path2Value({
					id : "<?= (string)$answer["_id"] ?>",
					collection : "answers",
					pull : path<?= $kunik ?>,
					value: null,
					path: path<?= $kunik ?>+"."+currentAnswer.indexOf(value<?= $kunik ?>)
				}, function(params){
					//toastr.success(tradForm.modificationSave);
					if(typeof newReloadStepValidationInputGlobal != "undefined") {
						newReloadStepValidationInputGlobal({
							inputKey : <?php echo json_encode($key); ?>,
							inputType : "tpls.forms.cplx.checkboxNew" 
						})
					}
				} );
			}
		});
		sectionDyf.<?php echo $kunik ?>Params = {
			"jsonSchema" : {	
				"title" : "<?php echo $label ?> config",
				"icon" : "cog",
				"properties" : {
					list : {
						inputType : "array",
						label : tradForm.checkboxList,
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.list,
						init : function() {
							$(`<input type="text" class="form-control copyConfig" placeholder="vous pouvez copier le liste ici, séparé par des virgules; ou utiliser le button ajout ci-dessous"/>`).insertBefore('.listarray .inputs.array');
							$(".copyConfig").off().on("blur", function() {
								let textVal = $(this).val().length > 0 ? $(this).val().split(",") : [];
								textVal.forEach((el, index) => {
									dyFObj.init.addfield('.listarray', el, 'list')
								});
								$(this).val('')
							})
						}
					}
				},
				save : function () {  
					tplCtx.value = {};
					$.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = getArray('.'+k+val.inputType);
					});
					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
						dataHelper.path2Value( tplCtx, function(params) { 
							dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
							urlCtrl.loadByHash(location.hash);
						} );
					}

				}
			}
		};
		$(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = $(this).data("path");
			dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
		});
	});
	</script>
<?php } ?>