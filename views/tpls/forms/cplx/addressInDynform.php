<?php 
$value = "";
if(!empty($answer) && isset($answer["answers"][$form["id"]][$key])) 
	$value =  $answer["answers"][$form["id"]][$key];
else if(!empty($answer) && isset($answer["answers"][$key])) 
	$value =  $answer["answers"][$key];
?>
<style type="text/css">
	.<?= $kunik?> .opendyn<?= $kunik?>{
		background-color: #93C020;
		color: white;
		padding: 6px;
		margin-left: 10px

	}
	.<?= $kunik?> .add<?= $kunik?>{
		margin-left: 10px;
		padding: 7px 4px 11px 7px;
		border-radius: 19px;
	}
	
</style>

<div class="<?= $kunik?>">
	<?php if($mode == "r" || $mode == "pdf"){  ?>
		<div>
			<label for="<?php echo $kunik ?>">
				<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
					<?php echo $label ?>
				</h4>
			</label> <br>
			<span class="pull-left locationEl0 locel text<?= $kunik?> bold">
				<i class="fa fa-home fa-2x"></i> <?php if ($value != null) {
					echo $value["address"]["addressCountry"].",".$value["address"]["postalCode"].",".$value["address"]["addressLocality"];
				}?>
			</span>
		</div>
	<?php }else { ?>

		<div>
			<label for="<?php echo $kunik ?>">
				<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
					<?php echo $label.$editQuestionBtn ?>
				</h4>
			</label>
		</div>

		<div>
			<?php if ($value == null) {?>
				<a href="javascript:;" class="opendyn<?= $kunik?> add<?= $kunik?>">
					<i class="fa fa-plus"></i>
					<?php echo Yii::t("common", "Add an address") ?>
				</a>
			<?php } else {   ?>
				<div class="col-xs-12" style="margin-bottom: 15px ;display: inline-block;">
					<span class="pull-left locationEl0 locel text<?= $kunik?> bold">
						<i class="fa fa-home fa-2x"></i> <?php if ($value != null) {
							echo @$value["address"]["addressCountry"].",".@$value["address"]["postalCode"].",".@$value["address"]["addressLocality"];
						}?>
					</span> 

					<a href="javascript:;" class="opendyn<?= $kunik?> btn-xs">
						<i class="fa fa-pencil"></i>

					</a>
				</div>
			<?php } ?>
		</div>


	<?php } ?>
</div>
<script type="text/javascript">
	var pathAdd<?php echo $kunik ?> ="<?php echo $form['id'].'.'.$key ; ?>";
	var data<?= $kunik?> = <?= json_encode($value);?>;
	jQuery(document).ready( function() {
		dynForm = {
			jsonSchema : {
				title : tradDynForm.addANewAddress,
				icon : "map-marker",
				type : "object",
				properties : {
					formLocality : dyFInputs.formLocality(tradDynForm.addLocality, tradDynForm.addLocality),
					location : dyFInputs.location
				},
				save : function () {  
					tplCtx.value = {};
					$.each( dynForm.jsonSchema.properties , function(k,val) { 
						if(val.inputType == "formLocality"){
							tplCtx.value[k] = getArray('.'+k+val.inputType);
							console.log(tplCtx.value[k]);
						}else 
						tplCtx.value[k] = $("#"+k).val();
						if(typeof formData != "undefined" && typeof formData.geo != "undefined"){
							tplCtx.value["coord"] = formData.geo;
							tplCtx.value["address"] = formData.address;
							tplCtx.value["geo"] = formData.geo;
							tplCtx.value["geoPosition"] = formData.geoPosition;

							if(typeof formData.addresses != "undefined")
								tplCtx.value["addresses"] = formData.addresses;
						}
					});
					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
						dataHelper.path2Value( tplCtx, function(params) {
							dyFObj.commonAfterSave(params,function(){
								toastr.success(tradForm.modificationSave);
								dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
								urlCtrl.loadByHash(location.hash);
							});
						} );
					}

				}
			}
		}
		$(".opendyn<?= $kunik?>").off().on("click",function() { 
			tplCtx.collection = "answers";
			tplCtx.id = answerObj._id.$id;
			tplCtx.path = "answers."+pathAdd<?php echo $kunik ?>;
			dyFObj.openForm( dynForm,null, data<?= $kunik?>);
		})
	})
</script>