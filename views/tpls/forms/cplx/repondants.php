<?php
    $cssAndScriptSurveyFiles = array(
        '/css/inputs-style.css',
        '/css/space-layout.css',
        '/css/codate/codate.css'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAndScriptSurveyFiles, Yii::app()->getModule(Survey::MODULE)->getAssetsUrl());

    $cle = $key ;
    $allFormAnswers = PHDB::distinct(Form::ANSWER_COLLECTION, 'user', array("form" => (string)$parentForm['_id'], "updated" => ['$exists' => 1]));
?>

    <div class="pt-1">
        <label for="form-check-label" for="<?php echo $key ?>">
            <h4 style="text-transform: unset; color: <?php echo ($titleColor) ? $titleColor : "black"; ?>;"><?php if($mode == 'r' || $mode == "pdf") echo $label; else echo $label.$editQuestionBtn ?>
            </h4>
        </label>
    </div>
    <div class="col-xs-12 p-0 card table-responsive p-2">
        <table class="codate table-merite">
            <thead>
                <tr>
                    <th><?= Yii::t("common","Username") ?></th>
                    <th><?= Yii::t("common","Name") ?></th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    foreach($allFormAnswers as $iter => $elem) {
                        $userConcerned = PHDB::findOneById(Citoyen::COLLECTION, $elem['user'], ['name', 'username']);
                ?>
                    <tr>
                        <td><?= $userConcerned["username"] ?></td>
                        <td><?= $userConcerned["name"] ?></td>
                    </tr>
                <?php
                    } 
                ?>
            </tbody>
            <tfoot>
                <th>Total</th>
                <td><?= count($allFormAnswers) ?></td>
            </tfoot>
        </table>
        <?php if(!empty($info)){ ?>
            <small id="<?php if(isset($key)) echo $key; else echo uniqid(); ?>Help" class="form-text text-muted">
                <?php echo $info ?>
            </small>
        <?php } ?>
    </div>