<?php 
if($saveOneByOne)
	$inpClass = " ";
	if($mode == "fa"){
		$editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";
	}
	$paramsData = [ 
        "list" => [],
        "sublist" => [
            "notDefined" => [
                "option A",
                "option B",
            ]
        ]
    ];
	$currentAnswer = (!empty($answer) && isset($answer["answers"][$form["id"]][$key])) ? $answer["answers"][$form["id"]][$key] : [];
	if(isset($parentForm["params"][$kunik])) {
		if(isset($parentForm["params"][$kunik]["list"])) { 
			$paramsData["list"] =  $parentForm["params"][$kunik]["list"];
        }
        if(isset($parentForm["params"][$kunik]["sublist"])) { 
            $paramsData["sublist"] =  $parentForm["params"][$kunik]["sublist"];
        }
	}
?>
<style>
	/* The container */
	.container<?= $kunik ?> {
		display: block;
		position: relative;
		padding-left: 30px;
		margin-bottom: 12px;
		cursor: pointer;
		font-size: 1.6rem;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}

	/* Hide the browser's default checkbox */
	.container<?= $kunik ?> input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
	}

	.container<?= $kunik ?> .paramsonebtn {
		font-size: 17px;
		display: none;
		padding: 5px;
	}
	.container<?= $kunik ?> .paramsonebtn:hover {
		color: #e31717;
	}

	/* Create a custom checkbox */
	.checkmark<?= $kunik ?> {
        position: absolute;
        top: 0;
        left: 0;
        height: 20px;
        width: 20px;
        background-color: #eee;
	}

	/* On mouse-over, add a grey background color */
	.container<?= $kunik ?>:hover input ~ .checkmark<?= $kunik ?> {
	    background-color: #ccc;
	}

	/* When the checkbox is checked, add a blue background */
	.container<?= $kunik ?> input:checked ~ .checkmark<?= $kunik ?>,
	.container<?= $kunik ?> .checkmark<?= $kunik ?>.checked {
	    background-color: #2196F3;
	}

	/* Create the checkmark/indicator (hidden when not checked) */
	.checkmark<?= $kunik ?>:after {
        content: "";
        position: absolute;
        display: none;
	}

	/* Show the checkmark when checked */
	.container<?= $kunik ?> input:checked ~ .checkmark<?= $kunik ?>:after,
	.container<?= $kunik ?> .checkmark<?= $kunik ?>.checked:after {
	    display: block;
	}

	/* Style the checkmark/indicator */
	.container<?= $kunik ?> .checkmark<?= $kunik ?>:after {
        left: 7px;
        top: 4px;
        width: 5px;
        height: 10px;
        border: solid white;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
	}
	.this-cursor-initial {
		cursor: initial !important;
	}
    .thckd:hover .container<?= $kunik ?> .paramsonebtn,
    .nested-box .paramsonebtn {
        display: inline-flex;
    }
    .ctz-box {
        list-style: none;
        padding-left: 0;
    }
    .ctz-box .line {
        display: grid;
        justify-items: start;
        justify-content: start;
        align-content: center;
        grid-template-columns: min-content 1fr;
        grid-template-rows: min-content 1fr;
    }
</style>
<?php  
    if($mode == "r" || $mode == "pdf"){ ?>
		<div class="form-check text-left mode-r">
			<label class="form-check-label" for="<?php echo $key ?>">
				<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.@$editParamsBtn ?></h4>
			</label><br/>	
			<?php if(!empty($info)){ ?>
				<small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
			<?php } ?>
			<?php
				echo "<ul class='bs-pl-0 ctz-box '>";
					foreach ($parentForm["params"][$kunik]["list"] as $ix => $lbl) { 
						$checkedValue =  "";
						$thisLblSlugkey = $ix . "_" . InflectorHelper::slugify($lbl);
						if(isset($currentAnswer["list"]) && in_array($thisLblSlugkey, $currentAnswer["list"]))
							$checkedValue =  "checked";
						?>
						<li class="line">
							<i class="fa <?= $checkedValue == "checked" ? 'fa-chevron-up' : 'fa-chevron-down' ?> toggle-btn bs-mr-2 cursor-pointer"></i>
							<div class="thckd">
								<label class="container<?= $kunik ?> toggle-btn-lbl">
									<span>
										<?= $lbl ?>
										<?php 
											if ($canEditForm && $mode == "fa") {
												echo " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-list-parent='$thisLblSlugkey' data-label='". str_replace(['"', "'"], ['&quot;', '&#39;'], $lbl) ."'  data-collection='".Form::COLLECTION."'  data-path='params.$kunik.sublist.$thisLblSlugkey' class='previewTpl paramsonebtn editOne$kunik'><i class='fa fa-cog'></i> </a>";
											}
										?>
									</span>
									<span class="checkmark<?= $kunik." ".$checkedValue ?>"></span>
								</label>
							</div>
							<span></span>
							<ul class="nested-box bs-pl-5 nested-<?= $kunik ?>" style="display: <?= $checkedValue == "checked" ? 'block': 'none' ?>;">
								<?php
									if (isset($paramsData["sublist"][$thisLblSlugkey])) {
										foreach ($paramsData["sublist"][$thisLblSlugkey] as $subIndex => $subLbl) {
											$thisSubLblSlugkey = $subIndex . "_" . InflectorHelper::slugify($subLbl);
											$subCheckedValue =  "";
											if(isset($currentAnswer["sublist"][$thisLblSlugkey]) && in_array($thisSubLblSlugkey, $currentAnswer["sublist"][$thisLblSlugkey]))
												$subCheckedValue =  "checked";
											?>
											<label class="container<?= $kunik ?> this-cursor-initial">
												<span>
													<?= $subLbl ?>
												</span>
												<span class="checkmark<?= $kunik." ".$subCheckedValue ?>"></span>
											</label>
											<?php
										}
									} else if ($mode == "fa") {
										echo "<a href='javascript:;' data-id='".$parentForm["_id"]."' data-list-parent='$thisLblSlugkey' data-label='". str_replace(['"', "'"], ['&quot;', '&#39;'], $lbl) ."'  data-collection='".Form::COLLECTION."'  data-path='params.$kunik.sublist.$thisLblSlugkey' class='previewTpl btn btn-danger bold editOne$kunik'><i class='fa fa-cog'></i> ". Yii::t("survey", "Add list") ."</a>";
									}
								?>
							</ul>
						</li>
					<?php } 
				echo "</ul>";
			?>
		</div>
		<script type="text/javascript">
			$(function(){
				$(".mode-r .toggle-btn, .mode-r .toggle-btn-lbl").off("click").on("click", function(e) {
					e.stopImmediatePropagation();
					let nestedList = $(this).closest(".line").find(".nested-box");
					nestedList.slideToggle();
					if ($(this).hasClass(".toggle-btn")) {
						$(this).toggleClass("fa-chevron-down fa-chevron-up");
					} else {
						$(this).closest(".line").find(".toggle-btn").toggleClass("fa-chevron-down fa-chevron-up");
					}
				});
			})
		</script>
<?php }else{ ?>
	<div class="form-check text-left">
		<label class="form-check-label" for="<?php echo $key ?>">
			<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.@$editParamsBtn ?></h4>
		</label><br/>	
		<?php if(!empty($info)){ ?>
			<small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
		<?php } ?>
		<?php 
		if( !isset($parentForm["params"][$kunik]['list']) ) {
			echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i>  ".Yii::t("survey","THIS FIELD HAS TO BE CONFIGURED FIRST").@$editParamsBtn."</span>";
		} else {
            echo "<ul class='bs-pl-0 ctz-box '>";
                foreach ($parentForm["params"][$kunik]["list"] as $ix => $lbl) { 
                    $checkedValue =  "";
                    $thisLblSlugkey = $ix . "_" . InflectorHelper::slugify($lbl);
                    if(isset($currentAnswer["list"]) && in_array($thisLblSlugkey, $currentAnswer["list"]))
                        $checkedValue =  "checked";
                    ?>
                    <li class="line">
                        <i class="fa <?= $checkedValue == "checked" ? 'fa-chevron-up' : 'fa-chevron-down' ?> toggle-btn bs-mr-2 cursor-pointer"></i>
                        <div class="thckd">
                            <label class="container<?= $kunik ?>">
                                <span>
                                    <?= $lbl ?>
                                    <?php 
                                        if ($canEditForm && $mode == "fa") {
                                            echo " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-list-parent='$thisLblSlugkey' data-label='". str_replace(['"', "'"], ['&quot;', '&#39;'], $lbl) ."'  data-collection='".Form::COLLECTION."'  data-path='params.$kunik.sublist.$thisLblSlugkey' class='previewTpl paramsonebtn editOne$kunik'><i class='fa fa-cog'></i> </a>";
                                        }
                                    ?>
                                </span>
                                <input type="checkbox" class="form-check-input <?php echo $inpClass ?>  <?= $kunik ?>"  id="<?php echo $kunik.$ix ?>" data-value="<?= $thisLblSlugkey ?>" data-form='<?php echo $form["id"] ?>' <?php echo $checkedValue?>  >
                                <span class="checkmark<?= $kunik ?>"></span>
                            </label>
                        </div>
                        <span></span>
                        <ul class="nested-box bs-pl-5 nested-<?= $kunik ?>" style="display: <?= $checkedValue == "checked" ? 'block': 'none' ?>;">
                            <?php
                                if (isset($paramsData["sublist"][$thisLblSlugkey])) {
                                    foreach ($paramsData["sublist"][$thisLblSlugkey] as $subIndex => $subLbl) {
                                        $thisSubLblSlugkey = $subIndex . "_" . InflectorHelper::slugify($subLbl);
                                        $subCheckedValue =  "";
                                        if(isset($currentAnswer["sublist"][$thisLblSlugkey]) && in_array($thisSubLblSlugkey, $currentAnswer["sublist"][$thisLblSlugkey]))
                                            $subCheckedValue =  "checked";
                                        ?>
                                        <label class="container<?= $kunik ?>">
                                            <span>
                                                <?= $subLbl ?>
                                            </span>
                                            <input type="checkbox" class="form-check-input nested-checkbox <?php echo $inpClass ?>  <?= $kunik."_".$thisLblSlugkey ?>"  id="<?php echo $kunik.$thisLblSlugkey.$thisSubLblSlugkey ?>" data-input-parentid="#<?= $kunik.$ix ?>" data-parentslugkey="<?= $thisLblSlugkey ?>" data-value="<?= $thisSubLblSlugkey ?>" data-form='<?php echo $form["id"] ?>' <?= $subCheckedValue ?>>
                                            <span class="checkmark<?= $kunik ?>"></span>
                                        </label>
                                        <?php
                                    }
                                } else if ($mode == "fa") {
                                    echo "<a href='javascript:;' data-id='".$parentForm["_id"]."' data-list-parent='$thisLblSlugkey' data-label='". str_replace(['"', "'"], ['&quot;', '&#39;'], $lbl) ."'  data-collection='".Form::COLLECTION."'  data-path='params.$kunik.sublist.$thisLblSlugkey' class='previewTpl btn btn-danger bold editOne$kunik'><i class='fa fa-cog'></i> ". Yii::t("common", "Add") ." ". strtolower(Yii::t("common", "List")) ."</a>";
                                }
                            ?>
                        </ul>
                    </li>
                <?php } 
            echo "</ul>";
		}?>
	</div>

	<script type="text/javascript">
	var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answer["answers"][$kunik])) ? $answer["answers"][$kunik] : null ); ?>;
	sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

	jQuery(document).ready(function() {
		const _phpVar = {
            kunik: "<?= $kunik ?>",
            key: "<?= $key ?>",
            parentForm: JSON.parse(JSON.stringify(<?= json_encode(isset($parentForm) ? $parentForm : new stdClass()) ?>)),
            inputLabel: "<?= $label ?>",
            lineOnEdition: {
                key : "notDefined",
                label : "",
                path: "",
            },
        }
		let currentAnswer = <?php echo json_encode( $currentAnswer ); ?>;
		$('.<?= $kunik ?>').change(function(e) {
			e.stopImmediatePropagation();
			var value<?= $kunik ?> = $(this).data("value");
			const path = "answers." + $(this).data("form") + "." + _phpVar.key + ".list";
			const dataId = "<?= $key ?>"
			const val = $(`input.categorizedCheckbox${dataId}[type='checkbox']:checked`).length;
			if(typeof newReloadStepValidationInputGlobal != "undefined") {
				// newReloadStepValidationInputGlobal(null)
			} else {
				if(typeof ownAnswer != 'undefined' && typeof ownAnswer[$(this).attr("data-form")] != 'undefined' && val > 0) {
					ownAnswer[$(this).attr("data-form")][dataId] = val
				} else if(typeof ownAnswer != 'undefined' && typeof ownAnswer[$(this).attr("data-form")] != 'undefined'){
					ownAnswer[$(this).attr("data-form")][dataId] = ""
				}
			}
			if(typeof removeError != "undefined") {
				const elem = $(`li#question${dataId.replace('categorizedCheckbox', '')},#question_${dataId.replace('categorizedCheckbox', '')}`);
				if(val > 0){
					removeError(
						elem, 
						{
							'border' : 'none'
						},
						'error-msg'
					)
				} else if(typeof addError != 'undefined'){
					// addError(
					// 	elem,
					// 	{
					// 		'border' : '1px solid #a94442'
					// 	},
					// 	'error-msg'
					// )
				}
			}
			if (this.checked) {
                const currentLine = $(this).closest(".line");
                const nestedList = currentLine.find(".nested-box");
                if (!nestedList.is(":visible")) {
                    nestedList.slideToggle();
                    $(this).toggleClass("fa-chevron-down fa-chevron-up");
                }
				dataHelper.path2Value({
					id : "<?= (string)$answer["_id"] ?>",
					collection : "answers",
					arrayForm: true,
					edit:false,
					value: value<?= $kunik ?>,
					path: path
				}, function(params){
					//toastr.success(tradForm.modificationSave);
					if(typeof newReloadStepValidationInputGlobal != "undefined") {
						newReloadStepValidationInputGlobal({
							inputKey : <?php echo json_encode($key); ?>,
							inputType : "tpls.forms.cplx.categorizedCheckbox" 
						})
					}
				} );
			} else {
				dataHelper.path2Value({
					id : "<?= (string)$answer["_id"] ?>",
					collection : "answers",
					pull : path,
					value: null,
					path: path + "." + currentAnswer.list.indexOf(value<?= $kunik ?>)
				}, function(params){
					//toastr.success(tradForm.modificationSave);
					if(typeof newReloadStepValidationInputGlobal != "undefined") {
						newReloadStepValidationInputGlobal({
							inputKey : <?php echo json_encode($key); ?>,
							inputType : "tpls.forms.cplx.categorizedCheckbox" 
						})
					}
				} );
			}
		});
        $(`.nested-${ _phpVar.kunik } .nested-checkbox`).off("change").on("change", function(e) {
            e.stopImmediatePropagation();
			let value = $(this).data("value");
            const parentSlugkey = $(this).data("parentslugkey");
			const path = "answers." + $(this).data("form") + "." + _phpVar.key + ".sublist." + parentSlugkey;
            const inputParentId = $(this).data("input-parentid");
			const dataId = "<?= $key ?>"
			const val = $(`input.categorizedCheckbox${dataId}[type='checkbox']:checked`).length;
			if(typeof newReloadStepValidationInputGlobal != "undefined") {
				newReloadStepValidationInputGlobal(null)
			} else {
				// if(typeof ownAnswer != 'undefined' && typeof ownAnswer[$(this).attr("data-form")] != 'undefined' && val > 0) {
				// 	ownAnswer[$(this).attr("data-form")][dataId] = val
				// } else if(typeof ownAnswer != 'undefined' && typeof ownAnswer[$(this).attr("data-form")] != 'undefined'){
				// 	ownAnswer[$(this).attr("data-form")][dataId] = ""
				// }
			}
			if(typeof removeError != "undefined") {
				// const elem = $(`li#question${dataId.replace('categorizedCheckbox', '')},#question_${dataId.replace('categorizedCheckbox', '')}`);
				// if(val > 0){
				// 	removeError(
				// 		elem, 
				// 		{
				// 			'border' : 'none'
				// 		},
				// 		'error-msg'
				// 	)
				// } else if(typeof addError != 'undefined'){
				// 	// addError(
				// 	// 	elem,
				// 	// 	{
				// 	// 		'border' : '1px solid #a94442'
				// 	// 	},
				// 	// 	'error-msg'
				// 	// )
				// }
			}
			if (this.checked) {
                if (!$(inputParentId)[0].checked) {
                    $(inputParentId).prop("checked", true);
                    $(inputParentId).trigger("change");
                }
				dataHelper.path2Value({
					id : "<?= (string)$answer["_id"] ?>",
					collection : "answers",
					arrayForm: true,
					edit:false,
					value: value,
					path: path
				}, function(params){
					//toastr.success(tradForm.modificationSave);
					if(typeof newReloadStepValidationInputGlobal != "undefined") {
						newReloadStepValidationInputGlobal({
							inputKey : <?php echo json_encode($key); ?>,
							inputType : "tpls.forms.cplx.categorizedCheckbox" 
						})
					}
				} );
			} else {
                const nestedList = $(this).closest(".nested-box");
                if ($(inputParentId)[0].checked && nestedList.find("input[type=checkbox]:checked").length == 0) {
                    $(inputParentId).prop("checked", false);
                    $(inputParentId).trigger("change");
                }
				dataHelper.path2Value({
					id : "<?= (string)$answer["_id"] ?>",
					collection : "answers",
					pull : path,
					value: null,
					path: path + "." + currentAnswer.sublist[parentSlugkey].indexOf(value)
				}, function(params){
					//toastr.success(tradForm.modificationSave);
					if(typeof newReloadStepValidationInputGlobal != "undefined") {
						newReloadStepValidationInputGlobal({
							inputKey : <?php echo json_encode($key); ?>,
							inputType : "tpls.forms.cplx.categorizedCheckbox" 
						})
					}
				} );
			}
        });
        $(".toggle-btn").off("click").on("click", function (e) {
			e.stopImmediatePropagation();
            let nestedList = $(this).siblings(".nested-box");
            nestedList.slideToggle();
            
            $(this).toggleClass("fa-chevron-down fa-chevron-up");
        });
		sectionDyf.<?php echo $kunik ?>Params = {
			"jsonSchema" : {	
				"title" : "<?php echo $label ?> config",
				"icon" : "cog",
				"properties" : {
					list : {
						inputType : "array",
						label : tradForm.checkboxList,
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.list,
						init : function() {
							$(`<input type="text" class="form-control copyConfig" placeholder="vous pouvez copier le liste ici, séparé par des virgules; ou utiliser le button ajout ci-dessous"/>`).insertBefore('.listarray .inputs.array');
							$(".copyConfig").off().on("blur", function() {
								let textVal = $(this).val().length > 0 ? $(this).val().split(",") : [];
								textVal.forEach((el, index) => {
									dyFObj.init.addfield('.listarray', el, 'list')
								});
								$(this).val('')
							})
						}
					}
				},
				save : function () {  
					tplCtx.value = {};
					$.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = getArray('.'+k+val.inputType);
					});
					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
						dataHelper.path2Value( tplCtx, function(params) { 
							dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
							reloadInput("<?php echo $key ?>", "<?php echo (string)$form["id"] ?>");
						} );
					}

				}
			}
		};
        sectionDyf[_phpVar.kunik+"oneParams"] = {
            "jsonSchema" : {
                "title" : "<?= $label ?> - ",
                "icon" : "cog",
                "properties" : {
                    sublist : {
                        inputType : "array",
                        label : "Liste de bouton check"
                    },
                },
                save : function () {
                    let paramsToSend = {
                        id : _phpVar.parentForm?._id?.$id ? _phpVar.parentForm._id.$id : "",
                        collection : "forms",
                        value : {},
                        path : _phpVar.lineOnEdition.path
                    }
                    $.each( sectionDyf[_phpVar.kunik+"oneParams"].jsonSchema.properties , function(k,val) { 
						paramsToSend.value = getArray('.'+k+val.inputType);
					});
                    mylog.log("check dataToSend", paramsToSend);
                    if(!notEmpty(paramsToSend.value)) {
                        toastr.warning('value cannot be empty!');
                        let changeBtnTimeout = setTimeout(function() {
                            $("#ajaxFormModal #btn-submit-form")
                                .prop("disabled", false)
                                .html(`
                                    ${ tradDynForm.submit }
                                    <i class="fa fa-arrow-circle-right"></i>
                                `)
                            clearTimeout(changeBtnTimeout);
                        }, 300);
                    } else {
                        dataHelper.path2Value( paramsToSend, function(params) {
                            dyFObj.closeForm();
                            reloadInput("<?php echo $key ?>", "<?php echo (string)$form["id"] ?>");
                        } );
                    }

                }
            }
        };
		$(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = $(this).data("path");
			dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
		});
        $(`.editOne${ _phpVar.kunik }`).off("click").on("click",function() {
            const listParent = typeof $(this).data("list-parent") != "undefined" ? $(this).data("list-parent") : "notDefined";
            _phpVar.lineOnEdition.key = listParent;
            _phpVar.lineOnEdition.label = $(this).data("label");
            _phpVar.lineOnEdition.path = $(this).data("path");
            const defaultData = {
                sublist: sectionDyf[_phpVar.kunik+"ParamsData"]?.sublist?.[listParent] ? [...sectionDyf[_phpVar.kunik+"ParamsData"]?.sublist?.[listParent]] : []
            }
            mylog.log("check sublist", sectionDyf[_phpVar.kunik+"ParamsData"])
            if (sectionDyf[_phpVar.kunik+"oneParams"]?.jsonSchema?.title) {
                sectionDyf[_phpVar.kunik+"oneParams"].jsonSchema.title = `${_phpVar.inputLabel} - ${_phpVar.lineOnEdition.label}`;
            }
            dyFObj.openForm( sectionDyf[_phpVar.kunik+"oneParams"], null, defaultData);
        });
	});
	</script>
<?php } ?>