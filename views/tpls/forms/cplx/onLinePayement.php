<style type="text/css">
    select option, select optgroup{
        font-size: 12pt !important;
        padding-top: 3em !important;
    }

    .form-control{
        padding: .5em !important;
    }

    .bg-pay {
        background-color: #31778f!important;
        color: white;
        padding-left: 45px;
        padding-right: 46px;
        padding-top: 15px;
        padding-bottom: 15px;
        text-transform: uppercase;
        margin-bottom: 20px;
        cursor: pointer;
    }

    #coFormbody, h2 {
        background-color: #31778f!important;
        color: white;
        padding: 20px;
        margin-left: 10%;
        margin-right: 10%;
        margin-bottom: 0;
        box-shadow: 0 3px 6px rgb(0 0 0 / 28%);
    }
    .questionList {
        padding-left: 10%;
        padding-right: 10%;
    }

    .pay-container{
        padding: 50px;
    }
</style>

<?php 


//$childForm = PHDB::find("forms", array("parent.".$this->costum['contextId']=>['$exists'=>true]));
$contextId = array_key_first($parentForm["parent"]);
$context = array("id" => $contextId, "slug" => $parentForm["parent"][$contextId]["name"]);
$childForm = PHDB::find("forms", array("parent.".$context['id'] => ['$exists'=>true]));
    # If user has submited the coform


if($answer){ ?>
<div class="pay-container">
    <div class="col-md-6">
        <?php
        $types = array();
        if(!function_exists("get_types")){
            function get_types(&$types, $label){
                $exist = false;
                foreach ($types as $k => &$v) {
                    if($v==$label){
                        $exist = true;
                    }
                }

                if(!$exist){
                    array_push($types, "$label");
                }
            }
        }


            # Initialize parameters data
        $paramsData = [
            "link" => "",
            "successMsg" => "",
            "types" => [
                "type0" => [
                    "type" => "",
                    "label" => "",
                    "droit" => 0

                ],
                "type1" => [
                    "type" => "",
                    "label" => "",
                    "droit" => 0
                ]
            ]
        ];

            # Set parameters data
        if( isset($form["params"][$kunik]["types"]) )
            $paramsData["types"] = $form["params"][$kunik]["types"]; 
        if ( isset($form["params"][$kunik]["link"]) ) 
            $paramsData["link"] = $form["params"][$kunik]["link"];  
        if ( isset($form["params"][$kunik]["successMsg"]) ) 
            $paramsData["successMsg"] = $form["params"][$kunik]["successMsg"];        

        $arrayAnswersSource = PHDB::find("answers", array("source.keys" => $context['slug'], 'form' => $paramsData["link"] ,  "user" => Yii::app()->session["userId"], "draft"=>['$exists' => false ]));  

        $isPaid = 0;
        $AnswerSourceId = 0;
        if (isset(array_keys($arrayAnswersSource)[0])) {
            $AnswerSourceId = array_keys($arrayAnswersSource)[0];
            $AnswerSource = $arrayAnswersSource[$AnswerSourceId];
            $paid = PHDB::findOne("payments", array('user' => $_SESSION["userId"], 'tracktrace' => $AnswerSourceId,"payment.status"=>"paid"));
            $p = PHDB::find("payments", array('user' => $_SESSION["userId"], 'tracktrace' => $AnswerSourceId, "payment.status"=>['$exists' => true ]));

        }
        $theAnswers = PHDB::find("answers", array("user"=>Yii::app()->session["userId"], "form"=>$parentForm["_id"]->{'$id'}, "draft" => ['$exists' => false ]));
        $answerId = (string)$answer["_id"];

        $editParamsBtn = ($canEditForm) ? "<a href='javascript:;' 
        data-id='".$form["_id"]."' 
        data-collection='".Form::COLLECTION."' 
        data-path='params.".$kunik."' 
        class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'>
        <i class='fa fa-cog'></i> 
        </a>" : "";

        //Change api key into "Apikey_**************vwxyz"
        $apiT = isset($form["params"][$kunik]["apiTest"]) ? $form["params"][$kunik]["apiTest"] : "";
        $apiTencod = base64_decode($apiT);
        $rmBeg = str_replace(substr($apiTencod, 0,5),'',$apiTencod);
        $rmEnd = str_replace(substr($apiTencod, -4),'',$rmBeg);
        $apiTestHided = substr($apiTencod, 0,5)."".str_repeat("*",strlen($rmEnd))."".substr($apiTencod, -5);

        $apiL= isset($form["params"][$kunik]["apiLive"]) ? $form["params"][$kunik]["apiLive"] : "";
        $apiLivencod = base64_decode($apiL);
        $rmLBeg = str_replace(substr($apiLivencod, 0,5),'',$apiLivencod);
        $rmLEnd = str_replace(substr($apiLivencod, -5),'',$rmLBeg);
        $apiLiveHided = substr($apiLivencod, 0,5)."".str_repeat("*",strlen($rmLEnd))."".substr($apiLivencod, -5);
        $apiMode = isset($form["params"][$kunik]["mollieMode"]) ? $form["params"][$kunik]["mollieMode"] : "";
        $apiToUse ="";
        if ($apiMode == "Live") {
            $apiToUse == $apiLivencod;
        }elseif ($apiMode == "Test") {
            $apiToUse == $apiTencod;
        }

        ?>

        <?php echo $info ?>

        <?php 
        $selected_type = (isset($answer["answers"][$form["id"]]["type"]))?$answer["answers"][$form["id"]]["type"]:""; 

        if (empty($paid) == true) { 

        if(Authorisation::isInterfaceAdmin() == false ){ 
        echo "Paiement de votre questionnaire du ". gmdate("Y-m-d", $AnswerSource['created']);
        echo "<br>Voir la <a href='#answer.index.id.".$AnswerSourceId.".mode.r' class='text-red lbh'>Questionnaire</a>";
         }?>
            <h4 style="color: #31778f; text-transform: inherit;font-weight: normal;">
                <?php echo $label.$editQuestionBtn." ".$editParamsBtn?>
            </h4>
            <?php
        ?>
            <div class="form-group padding-top-20">
                <select id="<?= $key ?>" data-form="<?= $form["id"]?>" class="form-control saveOneByOne" onchange="myChoose()">
                    <option></option>
                    <?php foreach ($paramsData["types"] as $tkey => $tvalue) {
                        $l = (isset($tvalue["label"])?$tvalue["label"]:"");
                        echo '<option value="'.base64_encode(json_encode($tvalue)).'">'.$l.'</option>';
                    } ?>
                </select>
            </div>
            <hr>
        <?php if(Authorisation::isInterfaceAdmin() == false ){ ?>
            <div class="form-group padding-bottom-20">
                <b style="color: #31778f;">Montant à payer </b>
            </div>
            <div class="form-group" style="padding-bottom: 45px">
                <span id="total" style="border: 1px solid #ddd;" class="padding-20">0 €</span>
            </div>
            <div class="form-group">
                <?php if ( $apiTencod != "" || $apiLivencod != "") { ?>
                    <a id="btn-payement" class="bg-pay"> 
                        Valider
                    </a>
                     <img src="<?php echo Yii::app()->getModule("survey")->assetsUrl; ?>/images/secure-payement.png">
                <?php } ?>
                <span id="load-payment"></span>
            </div>
        <?php } ?>
    </div>
    <div class="col-md-6">        
        <p><i class="fa fa-clock-o"></i> Historique de paiement</p>
        <div class="panel-group">
          <div class="">
            <?php if (!empty($p)) {
                 foreach ($p as $pkey => $pvalue) { //var_dump($pvalue);*
                $histheme = "info";
                if ($pvalue["payment"]["status"] == "failed") {
                    $histheme = "danger";
                    $isPaid = "trying";
                }elseif ($pvalue["payment"]["status"] == "paid") {
                    $histheme = "success";
                    $isPaid = "trying";
                }elseif ($pvalue["payment"]["status"] == "canceled") {
                    $histheme = "warning";
                    $isPaid = "trying";
                }
                ?>                
                <div class="panel">
                    <ul class="list-group">
                      <a  data-toggle="collapse" href="#<?= $pkey ?>" class="list-group-item list-group-item-<?= $histheme ?>"><?php 
                        if ($pvalue["payment"]["status"] == "failed") { ?>                           
                            Paiement échoué <?php echo gmdate("Y-m-d",$pvalue['orderId']); ?>
                       <?php }elseif ($pvalue["payment"]["status"] == "canceled") { ?>                            
                            Paiement Annulé <?php echo gmdate("Y-m-d",$pvalue['orderId']); ?>
                        <?php } ?>   </a>
                  </ul>
              </div>
              <div id="<?= $pkey ?>" class="panel-collapse collapse">
                <ul class="list-group">
                    <li class="list-group-item text-<?= $histheme ?>">Paiement de <?= @$pvalue["payment"]["value"]." ".$pvalue["payment"]["currency"] ?>
                    <p class="text-<?= $histheme ?>"><?= @$pvalue["payment"]["description"] ?></p>
                    <p class="text-<?= $histheme ?>">
                        <?php 
                        if ($pvalue["payment"]["status"] == "failed") { ?>                           
                            <span class="label label-danger">Echoué</span>
                        <?php }elseif ($pvalue["payment"]["status"] = "canceled") { ?>                            
                            <span class="label label-warning">Annulé</span>
                        <?php } ?>
                    </p> 
                    <p>Si le problème persiste, contactez l'administrateur!</p>
                    </li>                
                    </ul>
            </div>  
        <?php } 
            } ?>       

    </div>
</div>
</div>
</div>

<?php }else{
    $isPaid = true; 
    ?>
    <style type="text/css">
        #coFormbody, h2 {
            background-color: #31778f!important;
            border-top-left-radius: 40px;
            border-top-right-radius: 40px;
            font-weight: normal;
            color: white;
            padding: 20px;
            margin-left: 10%;
            margin-right: 10%;
            margin-bottom: 0;
            box-shadow: 0 3px 6px rgb(0 0 0 / 28%);
        }

        .pay-container .col-md-6 {
            width: 100%;
        }

        .questionBlock{
         border: 1px solid #ddd;
         margin-bottom: 10px;
         border-bottom-left-radius: 80px;
         border-bottom-right-radius: 80px;
         background-color: #f6feff;
        }

 

    </style>
    <div class="">
        <div class="col-md-12 padding-20" style="font-weight: normal;font-size: x-large;">            
            <article class="text-center" style="font-size: x-large;"><?php echo $paid["payment"]["description"] ." le ". gmdate("Y-m-d", $paid['orderId']); ?></article>
            <article class="text-center" style="white-space: pre-line; font-weight: bold;font-size:30px;"><?php echo $form["params"][$kunik]["successMsg"]; ?>
            <br>
            <i class="fa fa-check-circle" style="font-size: 90px;color: #31778f"></i></article>

        </div>
    </div>
    <script type="text/javascript">        
        $("#modeSwitch").remove();
        $("#bottomModeSwitch").remove();
    </script>
<?php } ?>

<script type="text/javascript">
  var isPaid = "<?= $isPaid ?>";

  if (isPaid != 1) {            
    function myChoose() {
      var x = JSON.parse(atob(document.getElementById("<?=$key?>").value));
      mylog.log("loza ty", x.droit);
      document.getElementById("total").innerHTML = x.droit+" €";
  }

  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

      var typeAssocie = <?php echo json_encode( $types ); ?>;

    

        // Initialize from answers
        $("#<?=$key?>").val("<?= $selected_type ?>");

        if($("#<?=$key?>").val()!=""){
            const st = JSON.parse(atob($("#<?= $key ?>").val()));
            $("#total").text(st.droit+" €");
        }

        $(document).ready(function(){
            let kunik = "<?= $kunik ?>";
            let droit = {};
            let options = [];
            let thisForm = "<?=$form["id"]?>";

            <?php if(Authorisation::isInterfaceAdmin() ){ ?>      
                let apiTestHided = "<?=$apiTestHided?>";
                let apiLiveHided = "<?=$apiLiveHided?>";
                let apiMode = "<?=$apiMode?>";

                sectionDyf.<?php echo $kunik ?>ParamsSelect = {
                    "jsonSchema" : {
                        "title" : "Paramétrage du payment",
                        "description" : "",
                        "icon" : "cog",
                        "properties" : {
                         link : {
                            label : "Selectionner le form lié au payment",
                            "inputType" : "select",
                            "class" : "form-control",
                            "options": {
                              <?php 
                              foreach($childForm as $keyf => $value) { 
                                echo  '"'.$keyf.'" : "'.$value["name"].'",';
                            }    
                            ?>
                        },
                        values : sectionDyf.<?php echo $kunik ?>ParamsData.link
                    },
                    apiTest:{
                        inputType: "text",
                        label : "Votre clé api mollie pour teste :",
                        placeholder : apiTestHided
                    },

                    apiLive:{
                        inputType: "text",
                        label : "Votre clé api mollie pour live :",
                        placeholder : apiLiveHided
                    },

                    mollieMode : {
                        "placeholder" : "Sélectionner",
                        "inputType" : "select",
                        "label" : "Mode d'utilisation mollie :",
                        "class" : "form-control",
                        "options": {
                            "Test":"Teste",
                            "Live":"Live",
                        },
                        "value" : apiMode,
                        rules:{
                            "required":true
                        }
                    },

                    successMsg:{
                        inputType: "textarea",
                        label : "Message court à afficher dans le formuliare quand le paiement est réussi:",
                        placeholder : "Message quand le paiement réussi",
                        values : sectionDyf.<?php echo $kunik ?>ParamsData.successMsg
                    },


                    types : {
                        inputType : "lists",
                        label : "Type de droit",
                        entries: {
                            label: {
                                label:"Label sur le choix:",
                                type:"textarea",
                                class:"col-md-6"
                            },
                            droit: {
                                label:"Droit :",
                                type:"text",
                                class:"col-md-2",
                                placeholder:"En €"
                            }
                        },
                    }
                },
                save : function (data) {
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>ParamsSelect.jsonSchema.properties , function(k,val) {

                        if(k=="types"){
                            let types = {};
                            $.each(data.types, function(index, va){
                                let tpa = {label: va.label, droit: va.droit};
                                types["type"+index] = tpa;
                            });
                            tplCtx.value[k] = types;
                        }else{
                            tplCtx.value[k] = $("#"+k).val();
                        }

                        mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                    });
                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == undefined)
                        toastr.error('value cannot be empty!');
                    else{
                        if (tplCtx.value.apiLive == "") {
                            tplCtx.value.apiLive =  "<?=$apiL?>";
                            mylog.log("Loza",apiLiveHided);
                        }else{
                            tplCtx.value.apiLive = btoa(tplCtx.value.apiLive);
                        }
                        if (tplCtx.value.apiTest == "") {
                            tplCtx.value.apiTest =  "<?=$apiT?>";
                            mylog.log("Loza",apiTestHided);
                        }else{
                            tplCtx.value.apiTest = btoa(tplCtx.value.apiTest);
                        }
                        mylog.log("Loza",tplCtx);
                        dataHelper.path2Value( tplCtx, function(params) {
                            $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                            location.reload();
                        } );
                    }
                }
            }
        };
    <?php } ?>

            // $("input[data-entry=type]").off().on("focus", function() {
            //    alert( "Handler for .focus() called." );
            // });


            $("#<?= $key ?>").off().on("change", function(){
                alert($("#<?= $key ?>").val());
                // const st = ($("#<?= $key ?>").val()!="")?JSON.parse(atob($("#<?= $key ?>").val())):0;

                
                // $("#total").text(st.droit+" €");
                
                // $("#btn-payement").text("Proceder au paiement de "+st.droit+" €");

                $(this).blur();
            });

            $("#btn-payement").off().on("click", function(){
                mylog.log("Loza 1",($("#<?= $key ?>").val()));
                // Save all before pay
                $(".saveOneByOne").blur();

                if($(".saveOneByOne").val()!=""){
                    coInterface.showLoader("#load-payment");
                    const st = JSON.parse(atob($("#<?= $key ?>").val()));
                    
                    let paymentData = {
                        'amount' :{
                            'currency' : "EUR",
                            'value' : ""+parseFloat(st.droit).toFixed(2)+""
                        },
                        'description': "Droit d'inscription coaching de "+st.label,
                        'page' : window.location.href,
                        'source': "<?=$answer["form"]?>",
                        'sourceChild' : "<?=$paramsData["link"]?>",
                        'nombre' : 1,
                        'orga': contextData.slug,
                        'inputSource': thisForm+"."+kunik,
                        'tracktrace' : '<?=$AnswerSourceId?>'
                    };

                    ajaxPost(
                        null,
                        baseUrl+"/survey/payment/pay",
                        paymentData,
                        function(response){
                            if(response.url){
                                location = response.url;
                            }
                            if(response.message){
                                toastr.error(response.message);
                            }
                        },
                        function(xhr,textStatus,errorThrown,data){
                            toastr.error("Une erreur s'est produite, veuillez signaler ce problème à notre administrateur");
                        }
                        );
                }else{
                    toastr.warning("Veuillez remplir toutes avant de payer votre droit d'associé")
                }
            });

            $("#btn-facturation").off().on("click", function(){

            });

            //adds a line into answer
            $(".add<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
            });

            $(".edit<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
            });

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                //if no params config on the element.costum.form.params.<?php echo $kunik ?>
                //then we load default values available in forms.inputs.<?php echo $kunik ?>xxx.params
                //mylog.log(".editParams",sectionDyf.<?php echo $kunik ?>Params,calData);
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>ParamsSelect,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            });
        });
}
    

    </script>
<?php }else {
        //echo "<h4 class='text-red'>evaluation works with existing answers</h4>";
}


?>