<?php if($answer){ 
	$keyTpl = "element";?>
<div class="form-group">
	<table class="table table-bordered table-hover  directoryTable" id="<?php echo $keyTpl.$key?>">
		
	<?php 
		
		$editBtnL = ($canEdit === true && isset($parentForm["params"][$keyTpl.$key])  ) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='answers.".$keyTpl.$key.".' class='add".$keyTpl." btn btn-default'><i class='fa fa-plus'></i> Ajouter un élément </a>" : "";
		
		$editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$keyTpl.$key."' class='previewTpl edit".$keyTpl."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

		$paramsData = [ "type" => [
					    	Organization::COLLECTION => "Organization",
					    	Person::COLLECTION 		 => "Person",
					    	Event::COLLECTION 		 => "Event",
					    	Project::COLLECTION 	 => "Project",
							News::COLLECTION 		 => "News",
					    	Need::COLLECTION 		 => "Need",
					    	City::COLLECTION 		 => "City",
					    	Thing::COLLECTION 		 => "Thing",
					    	Poi::COLLECTION 		 => "Poi",
					    	Classified::COLLECTION   => "Classified",
					    	Product::COLLECTION 	 => "Product",
					    	Service::COLLECTION   	 => "Service",
					    	Survey::COLLECTION   	 => "Survey",
					    	Bookmark::COLLECTION   	 => "Bookmark",
					    	Proposal::COLLECTION   	 => "Proposal",
					    	Room::COLLECTION   	 	 => "Room",
					    	Action::COLLECTION   	 => "Action",
					    	Network::COLLECTION   	 => "Network",
					    	Url::COLLECTION   	 	 => "Url",
					    	Circuit::COLLECTION   	 => "Circuit",
					    	Risk::COLLECTION   => "Risk",
					    	Badge::COLLECTION   => "Badge",
					    ] ];

		$properties = [
                "qui" => [
                    "label" => "Qui...?",
                    "placeholder" => "Qui...",
                ],
                "type" => [
                    "label" => "...Type ?",
                    "placeholder" => "...type...",
                ]
	        ];
	?>	
	<thead>
		<tr>
			<td colspan='<?php echo count( $properties)+2?>' ><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
				<?php echo $info;
				if( !isset($parentForm["params"][$keyTpl.$key]['type']) ) 
					echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> THIS FIELD HAS TO BE CONFIGURED FIRST ".$editParamsBtn."</span>";
				 ?>

			</td>
		</tr>	
		<?php if(isset($answer["answers"][$keyTpl.$key]) && count($answer["answers"][$keyTpl.$key])>0){ ?>
		<tr>
			</th>
			<?php 
			
			foreach ($properties as $i => $inp) {
				echo "<th>".$inp["placeholder"]."</th>";
			} ?>
			<th></th>
		</tr>
		<?php } ?>
	</thead>
	<tbody class="directoryLines">	
		<?php 
		$ct = 0;
		
		if(isset($answer["answers"][$keyTpl.$key])){
			foreach ($answer["answers"][$keyTpl.$key] as $q => $a) {

				echo "<tr id='".$keyTpl.$q."' class='".$keyTpl."Line'>";
				foreach ($properties as $i => $inp) {
					echo "<td>";
					if(isset($a[$i])) {
						if(is_array($a[$i]))
							echo implode(",", $a["role"]);
						else
							echo $a[$i];
					}
					echo "</td>";
				}
					
			?>
			<td>
				<?php if ($canEdit){ ?>
				<a href='javascript:;'  data-id='<?php echo $answer["_id"]?>' data-collection='<?php echo Form::ANSWER_COLLECTION?>' data-key='<?php echo $q?>' data-path='answers.<?php echo $keyTpl.$key ?>.<?php echo $q?>' class='edit<?php echo $keyTpl ?> previewTpl btn btn-xs btn-default'><i class='fa fa-pencil'></i></a>

				<a href='javascript:;' data-id='<?php echo $answer["_id"]?>' data-collection='<?php echo Form::ANSWER_COLLECTION?>' data-key='<?php echo $keyTpl ?><?php echo $q?>' data-path='answers.<?php echo $keyTpl.$key ?>.<?php echo $q?>' class='deleteLine previewTpl btn btn-xs btn-danger'><i class='fa fa-times'></i></a>

				<?php } ?>
				<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
			</td>
			<?php 
				$ct++;
				echo "</tr>";
			}
		}
		 ?>
		</tbody>
	</table>
</div>
<script type="text/javascript">

var <?php echo $keyTpl ?>Data = <?php echo json_encode( (isset($answer["answers"][$keyTpl.$key])) ? $answer["answers"][$keyTpl.$key] : null ); ?>;
sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

$(document).ready(function() { 
	
	sectionDyf.<?php echo $keyTpl ?>Params = {
		"jsonSchema" : {	
	        "title" : "<?php echo $keyTpl ?> config",
	        "icon" : "fa-cog",
	        "properties" : {
	            type : {
	                inputType : "select",
	                label : "Définir un type d'élément",
	                options :  sectionDyf.<?php echo $keyTpl ?>ParamsData.type
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
	                    urlCtrl.loadByHash(location.hash);
	                } );
	            }

	    	}
	    }
	};


    mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $keyTpl ?>.php");

    //adds a line into answer

    <?php if( isset($parentForm["params"][$keyTpl.$key]['type']) ) { ?>
    $(".add<?php echo $keyTpl ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path")+((notNull(<?php echo $keyTpl ?>Data) ? <?php echo $keyTpl ?>Data.length : "0"));
        dyFObj.openForm( "<?php echo $parentForm["params"][$keyTpl.$key]['type']; ?>" );
    });

    $(".edit<?php echo $keyTpl ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( "<?php echo $parentForm["params"][$keyTpl.$key]['type']; ?>",null, <?php echo $keyTpl ?>Data[$(this).data("key")]);
    });
    <?php } ?>

    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        //if no params config on the element.costum.form.params.<?php echo $keyTpl ?>
        //then we load default values available in forms.inputs.<?php echo $keyTpl ?>xxx.params
        //mylog.log(".editParams",sectionDyf.<?php echo $keyTpl ?>Params,calData);
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
    });

    
});
</script>
<?php } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>