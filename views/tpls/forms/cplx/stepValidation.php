<?php 
if( $mode != "pdf" ){
	//if( Form::canAccess( ['roles'=>["Financeur"] ], @$parentForm["parent"])  ){
	if( $canEdit ){
		if( isset($form['id']) )
		{ 
			$editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

			$paramsData = [ 
				"canValidRoles" =>["Financeur"],
				"canRequestRoles" =>["Opérateur"],
			];

			if( isset($parentForm["params"][$kunik]) ) {
				if( isset($parentForm["params"][$kunik]["canValidRoles"]) ) 
					$paramsData["canValidRoles"] =  $parentForm["params"][$kunik]["canValidRoles"];
				if( isset($parentForm["params"][$kunik]["canRequestRoles"]) ) 
					$paramsData["canRequestRoles"] =  $parentForm["params"][$kunik]["canRequestRoles"];
			}

			?>

			<div id="copyEquilibreBudgetaire"></div>

			<table class="table table-bordered table-hover  directoryTable" >
				<tbody class="directoryLines">	
				
					<tr>
						<td colspan='2' ><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">Validation</h4>
						</td>
					</tr>
				<?php 
				if( isset( $answer["validation"][ $form['id'] ]["valid"] ) && in_array($answer["validation"][$form['id']]["valid"], ["valid","validReserve"] ) ){ ?>
					<tr>
						<td colspan='2' align="center">
							<button id="generateCopil2" class="generateCopil btn btn-primary" data-id="<?php echo (string)$answer["_id"] ?>" data-date="<?php echo date("Y-m-d") ?>" data-title="Compte Rendu de la réunion de finalisation" data-key="copilReunionFinalisation">Générer le dossier validé</button>
					<?php $pdf=Document::getListDocumentsWhere([ "id"=>(string)$answer["_id"], "type"=>Form::ANSWER_COLLECTION, "doctype"=>"file", "subKey"=>"pdf".$form['id'] ] ,"file");
						 ?>
						</td>
					</tr>
				<?php   
				}


				if(isset($answer["validation"][$form['id']])){ 
					$color = "danger";
					$lbl = "Non validé";
					if(isset($answer["validation"][$form['id']]["valid"])){
						if( $answer["validation"][$form['id']]["valid"] == "valid" ){
							$color = "success";
							$lbl = "Validé sans réserves";
						} else if( $answer["validation"][$form['id']]["valid"] == "validReserve" ){
							$color = "warning";
							$lbl = "Validé avec réserves";
						}else if( $answer["validation"][$form['id']]["valid"] == "request" ){
							$color = "default";
							$lbl = "Demande de Validation";
						}
					}

					$editState = ( isset( $answer["validation"][$form['id']] ) ) ? "" : " <a href='javascript:;' data-type='".$form['id']."' class='validEdit btn btn-default btn-xs'><i class='fa fa-pencil'></i></a>";
					?>
					<tr>
						<td>Avis</td>
						<td><h4 class="label label-<?php echo $color?>"><?php echo $lbl?></h4> <?php echo $editState ?></td>
					</tr>
					<tr>
						<td>Commentaire global</td>
						<td><?php if( isset($answer["validation"][$form['id']]["description"] ) ) echo $answer["validation"][$form['id']]["description"]; ?></td>
					</tr>
					<tr>
						<td>Date</td>
						<td><?php if( isset($answer["validation"][$form['id']]["date"] ) ) echo $answer["validation"][$form['id']]["date"]; ?></td>
					</tr>
					<?php 
					if(!empty($ficheAction)){ 
						//$ficheAction = array_splice($ficheAction, count($ficheAction)-1);
						$histo = 0;
						foreach($ficheAction as $k => $v){ 
							if($histo < 1){?>
							<tr>
								<td>Fiche Action Finale</td>
								<td><a href='<?php echo $v["docPath"] ?>' target='_blank' class="link-files"><i class="fa fa-file-pdf-o text-red"></i> <?php echo $v["name"] ?></a></td>
							</tr>
						<?php	$histo++; }
						} 
					} ?>

				<?php 
				}  
				?>

				<tr>
					<td colspan='2' class="text-center" >
						<?php
						// // var_dump($this->costum["hasRoles"][0]);
						// var_dump($paramsData['canRequestRoles']);
						// // var_dump($el["links"]["members"][Yii::app()->session["userId"]]["roles"]);
						$canRequest = false;

						if (isset(Yii::app()->session["costum"][$el["costum"]["slug"]]["hasRoles"])) {
							$r = array_intersect($paramsData["canRequestRoles"], Yii::app()->session["costum"][$el["costum"]["slug"]]["hasRoles"]);
							// var_dump($r);
							// var_dump(Yii::app()->session["costum"][$el["costum"]["slug"]]["hasRoles"]);
							if (count($r) > 0) {
								$canRequest = true;
							}


						}

						// var_dump($canRequest);

						if( ( !isset($answer["validation"][$form['id']]) || 
							 ( isset( $answer["validation"][ $form['id'] ]["valid"] ) && $answer["validation"][ $form['id'] ]["valid"] == "notValid") ) && 
							
							$canRequest )
							echo '<a href="javascript:;" data-type="'.$form['id'].'" class="requestValid btn btn-primary">Étape terminé , Demander la Validation</a>';	 
						else if( isset( $answer["validation"][ $form['id'] ]["valid"] ) && 
								 $answer["validation"][ $form['id'] ]["valid"] == "request" && 
								 isset( $this->costum["hasRoles"][0] ) && 
								 in_array($this->costum["hasRoles"][0], $paramsData['canValidRoles'])  )
							echo '<a href="javascript:;" data-type="'.$form['id'].'" class="validEdit btn btn-primary">Valider cette Étape</a>';
						else {
							if(isset( $answer["validation"][ $form['id'] ] ) && 
								 $answer["validation"][ $form['id'] ]["valid"] == "request")
								echo "Étape en cours de validation ";
							else 	
								echo "Étape en cours de traitement";
						}
						 ?>
					</td>
				</tr>

				</tbody>
			</table>
		 
 


		
	<?php 
		} ?>




	<script type="text/javascript">

	sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

	$(document).ready(function() { 

	    mylog.log("render","/modules/survey/views/tpls/forms/cplx/stepValidation.php");

	    sectionDyf.<?php echo $kunik ?>Params = {
			"jsonSchema" : {	
		        "title" : "<?php echo $kunik ?> config",
		        "icon" : "fa-cog",
		        "properties" : {
		        	useValidationRoles : {
		                inputType : "checkboxSimple",
	                    label : "Utiliser des Role de validation",
	                    subLabel : "Permet de définir des demandes et de validations",
	                    params : {
	                        onText : "Oui",
	                        offText : "Non",
	                        onLabel : "Oui",
	                        offLabel : "Non",
	                        labelText : "Obligatoire"
	                    },
	                    checked : false
		            },
		            canValidRoles : {
		                inputType : "array",
		                label : "Liste des roles qui pourront valider cette étape",
		                values :  sectionDyf.<?php echo $kunik ?>ParamsData.canValidRoles
		            },
		            canRequestRoles : {
		                inputType : "array",
		                label : "Liste des roles qui pourront valider cette étape",
		                values :  sectionDyf.<?php echo $kunik ?>ParamsData.canRequestRoles
		            }
		        },
		        save : function () {  
		            tplCtx.value = {};
		            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
		        		if(val.inputType == "array")
		        		 	tplCtx.value[k] = getArray('.'+k+val.inputType);
		        		else if(val.inputType == "properties")
		        		 	tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
		        		else
		        		 	tplCtx.value[k] = $("#"+k).val();
		        	 });
		            mylog.log("save tplCtx",tplCtx);
		            
		            if(typeof tplCtx.value == "undefined")
		            	toastr.error('value cannot be empty!');
		            else {
		                dataHelper.path2Value( tplCtx, function(params) { 
		                    urlCtrl.loadByHash(location.hash);
		                } );
		            }

		    	}
		    }
		};


	   $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
	        tplCtx.id = $(this).data("id");
	        tplCtx.collection = $(this).data("collection");
	        tplCtx.path = $(this).data("path");
	        //if no params config on the element.costum.form.params.<?php echo $kunik ?>
	        //then we load default values available in forms.inputs.<?php echo $kunik ?>xxx.params
	        //mylog.log(".editParams",sectionDyf.<?php echo $kunik ?>Params,calData);
	        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
	    });

	    //documentManager.bindEvents();

	    typeObj.validation<?php echo $form['id'] ?> = {
		    jsonSchema : {
			    title : "Validation",
			    icon : "gavel",
			    onLoads : {
			    	sub : function(){
		    		 	dyFInputs.setSub("bg-red");
		    		},
			    },
			    save : function() { 	    	
			   
					var formData = $("#ajaxFormModal").serializeFormJSON();
					var today = new Date();
					today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();

			    	ctxTpl = {
			    		id : "<?php echo (string)$answer['_id'] ?>",
			    		collection : "answers",
			    		path : "validation."+formData.form,
	            		value : {
	            			//who : formData.who,
	            			valid : formData.valid,
	            			description : formData.description,
	            			date : today , 
	            			user : userId
	            		}
			    	};
			    	if(form.subForms.length == $.inArray( "<?php echo $form['id'] ?>", form.subForms )+1 && formData.valid == "valid")
						ctxTpl.value.finished = true;
			    	
			    	mylog.log("validation save",ctxTpl);

			  	 	dataHelper.path2Value( ctxTpl, function(params) { 
			  	 		dyFObj.closeForm();
			  	 		//only increment step form to next step if step valid
			  	 		if( ctxTpl.value.valid == "valid" || ctxTpl.value.valid == "validReserve" ){
				  	 		step = 0;
				  	 		if(form.subForms.length == $.inArray( "<?php echo $form['id'] ?>", form.subForms )+1)
				  	 			step = "all";
				  	 		else 
				  	 			step = form.subForms[$.inArray( "<?php echo $form['id'] ?>", form.subForms )+1];

				  	 		ctxTpl = {
					    		id    : "<?php echo (string)$answer['_id'] ?>",
					    		collection : "answers",
					    		path  : "step",
					    		value : step
					    	};
					    	mylog.log("save step save",ctxTpl);
					  	 	dataHelper.path2Value( ctxTpl, function(params) { 
					  	 		ajaxPost(
							        null,
							        baseUrl+"/survey/answer/sendmail/id/<?php echo (string)$answer['_id'] ?>",
							        { step : step },
							        function(data){ 
							        	toastr.success("Un mail a été envoyé à toutes les parties prenantes afin de les notifier");
							      		urlCtrl.loadByHash(location.hash);      
							        }
							    );
					  	 		urlCtrl.loadByHash(location.hash);
					  	 	} );
					  	 } else 
					  	 	urlCtrl.loadByHash(location.hash);
			  	 	} );

						
			    },
			    properties : {
			    	info : {
		                inputType : "custom",
		                html:"<p></p>",
		            },
		            valid : dyFInputs.inputHidden(),
		            description : dyFInputs.textarea( tradDynForm["description"], "..." ),
		            form : dyFInputs.inputHidden()
			    }
			}
		};
	    $('.validEdit').off().on("click", function() {
	    	var setVal = { form : $(this).data("type")};
	    	typeObj[ 'validation'+setVal.form ].jsonSchema.properties.valid = {
	                    "label" : "Valider",
	                    "inputType" : "select",
	                    "placeholder" : "---- Selectionner Valider ----",
	                    "options" : {
	                    	"notValid":"Non validé",
	                        "validReserve" : "Validé avec réserves",
	                        "valid" : "Validé sans réserve"
	                    }
	                };
			dyFObj.openForm( typeObj[ 'validation'+setVal.form ], null, setVal );
		});

		$('.requestValid').off().on("click", function() {
	    	var setVal = { form : $(this).data("type")};
	    	typeObj[ 'validation'+setVal.form ].jsonSchema.properties.valid = dyFInputs.inputHidden("request");
			dyFObj.openForm( typeObj[ 'validation'+setVal.form ], null, setVal );
		});
		

		
	    
	});
	</script>
	<?php 
	} else { ?>
		<div class="col-xs-12">
			Seuls les référents du territoire, le porteur de l’action et l’équipe nationale ont accès à cette étape.
		</div>
	<?php } 
}
?>
