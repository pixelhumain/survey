<?php 

if($saveOneByOne)
	$inpClass = " saveOneByOne"; 

	$editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

	$paramsData = [ "list" => [	] ];

	if( isset($parentForm["params"][$kunik]) ) {
		if( isset($parentForm["params"][$kunik]["list"]) ) 
			$paramsData["list"] =  $parentForm["params"][$kunik]["list"];
	}
?>
<div class="form-check">
    <label class="form-check-label" for="<?php echo $key ?>"><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn ?></h4></label><br/>	
    
    <?php 
    if( !isset($parentForm["params"][$kunik]['list']) ) {
    	echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> THIS FIELD HAS TO BE CONFIGURED FIRST ".$editParamsBtn."</span>";
    } else {
	    foreach ($parentForm["params"][$kunik]["list"] as $ix => $lbl) { 
	    	$value =  "";
			if(!empty($answer) && isset($answer["answers"][$form["id"]][$kunik.$ix]) && $answer["answers"][$form["id"]][$kunik.$ix] )
				$value =  "checked";
			else if(!empty($answer) && isset($answer["answers"][$kunik]) && $answer["answers"][$kunik] )
				$value =  "checked";
	    	?>
	    	<input type="checkbox" class="form-check-input <?php echo $inpClass ?>"  id="<?php echo $kunik.$ix ?>" data-form='<?php echo $form["id"] ?>' <?php echo $value?>  > <?php echo $lbl ?>	<br/>
	    <?php } 
	}?>
    
	<?php if(!empty($info)){ ?>
    	<small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
    <?php } ?>
</div>

<script type="text/javascript">
var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answer["answers"][$kunik])) ? $answer["answers"][$kunik] : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

jQuery(document).ready(function() {
    mylog.log("render form input","/modules/costum/views/tpls/forms/cplx/multiCheckbox.php");

    sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "<?php echo $kunik ?> config",
	        "icon" : "fa-cog",
	        "properties" : {
	            list : {
	                inputType : "array",
	                label : "Liste de Checkbox",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.list
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = getArray('.'+k+val.inputType);
	        	});
	            mylog.log("save tplCtx",tplCtx);

	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
	                    urlCtrl.loadByHash(location.hash);
	                } );
	            }

	    	}
	    }
	};
	$(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });
});
</script>