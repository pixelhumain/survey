<?php if($answer){ 
	?>
<div class="form-group">
	<table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>">
		
	<?php 
		$editBtnL = ($canEdit === true) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default'><i class='fa fa-plus'></i> ".Yii::t("common","Add line")." </a>" : "";
		
		$editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

		$communityLinks = array();
		if(!empty($form["parent"])){
			foreach ($form["parent"] as $key => $value) {
				$cl= Element::getCommunityByTypeAndId($key,$value["type"]);
				if(!empty($cl))
					$communityLinks = array_merge($communityLinks, $cl);
			}
		}
		$persons = Link::groupFindByType( Person::COLLECTION,$communityLinks,["name","links"] );
		$organizations = Link::groupFindByType( Organization::COLLECTION,$communityLinks,["name","links"] );
		$financers = array_merge( $persons, $organizations );

		$orgs = [];
		if( !empty($or["links"]["memberOf"][$this->costum["contextId"]]["roles"]) ) {
			foreach ($financers as $id => $or) {
				$roles = $or["links"]["memberOf"][$this->costum["contextId"]]["roles"];
				if( isset($parentForm["params"][$kunik]["limitRoles"]) && !empty($roles))
				{
					foreach ($roles as $i => $r) {
						if( in_array($r, $parentForm["params"][$kunik]["limitRoles"]) )
							$orgs[$id] = $or["name"];
					}		
				}
			}
		}
		//var_dump($orgs);exit;
		$listLabels = array_merge(Ctenat::$financerTypeList,$orgs);

		$paramsData = [ 
			"financerTypeList" => Ctenat::$financerTypeList,
			"limitRoles" =>["Financeur"]
		];

		if( isset($parentForm["params"][$kunik]) ) {
			if( isset($parentForm["params"][$kunik]["tpl"]) ) 
				$paramsData["tpl"] =  $parentForm["params"][$kunik]["tpl"];
			if( isset($parentForm["params"][$kunik]["financerTypeList"]) ) 
				$paramsData["financerTypeList"] =  $parentForm["params"][$kunik]["financerTypeList"];
			if( isset($parentForm["params"][$kunik]["limitRoles"]) ) 
				$paramsData["financersList"] =  $parentForm["params"][$kunik]["limitRoles"];
		}

		$properties = [
              "financerType" => [
                "placeholder" => Yii::t("survey","Type of Financing"),
                    "inputType" => "select",
                    "list" => "financerTypeList",
                    "rules" => [
                        "required" => true
                    ]
                ],
                "financer" => [
                    "placeholder" => Yii::t("survey","Which Financier") ,
                    "inputType" => "select",
                    "list" => "financersList",
                    "subLabel" => Yii::t("survey","If you are a public financier, invite him/her in the list below (if he/she does not appear, ask your territory representative to declare him/her as a funding partner)")
                ],
                "title" => [
                    "inputType" => "text",
                    "label" => Yii::t("survey","Fund, envelope or budget mobilized"),
                    "placeholder" => Yii::t("survey","Fund, envelope or budget mobilized"),
                    "subLabel" => Yii::t("survey","specify the title and nature of the funding"),
                    "rules" => [ "required" => true ]
                ]
        ];

if(isset($parentForm["params"]["period"])){
	$from = $parentForm["params"]["period"]["from"]+1;
	$to = $parentForm["params"]["period"]["to"];
	while ( $from <= $to) {
		$properties["amount".$from] = [
	        "inputType" => "text",
	        "label" => $from." (euros HT)www",
	        "placeholder" => $from." (euros HT)",
	        "rules" => [ "required" => true,"number" => true ]
	    ];
    	$from++;
	}
} else {
	$properties["amount2019"] = [
        "inputType" => "text",
        "label" => "2019 (euros HT)",
        "placeholder" => "2019 (euros HT)",
        "rules" => [ "required" => true,"number" => true ]
    ];
    $properties["amount2020"] = [
        "inputType" => "text",
        "label" => "2020 (euros HT)",
        "placeholder" => "2020 (euros HT)",
        "rules" => [ "required" => true,"number" => true  ]
    ];
    $properties["amount2021"] = [
        "inputType" => "text",
        "label" => "2021 (euros HT)",
        "placeholder" => "2021 (euros HT)",
        "rules" => [ "number" => true  ]
    ];
    $properties["amount2022"] = [
        "inputType" => "text",
        "label" => "2022 (euros HT)",
        "placeholder" => "2022 (euros HT)",
        "rules" => [ "number" => true  ]
    ];
    $properties["amount2023"] = [
        "inputType" => "text",
        "label" => "2023 (euros HT)",
        "placeholder" => "2023 (euros HT)",
        "rules" => [ "number" => true  ]
    ];
}

	?>	
	<thead>
		<tr>
			<td colspan='<?php echo count( $properties)+2?>' ><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
				<?php echo $info ?>
			</td>
		</tr>	
		<?php if(isset($answers) && count($answers)>0){ ?>
		<tr>
			</th>
			<?php 
			
			foreach ($properties as $i => $inp) {
				echo "<th>".$inp["placeholder"]."</th>";
			} ?>
			<th></th>
		</tr>
		<?php } ?>
	</thead>
	<tbody class="directoryLines">	
		<?php 
		$ct = 0;
		
		if(isset($answers)){
			foreach ($answers as $q => $a) {

				echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
				foreach ($properties as $i => $inp) {
					echo "<td>";
					if(isset($a[$i])) {
						if(is_array($a[$i]))
							echo implode(",", $a["role"]);
						else
							echo $a[$i];
					}
					echo "</td>";
				}
					
			?>
			<td>
				<?php 
					echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
						"canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
						"id" => $answer["_id"],
						"collection" => Form::ANSWER_COLLECTION,
						"q" => $q,
						"path" => $answerPath.$q,
						"kunik"=>$kunik
						] );
					?>
				<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
			</td>
			<?php 
				$ct++;
				echo "</tr>";
			}
		}

$totalMap = [];
foreach ( $properties as $i => $inp ) {
	if(stripos($i, "amount") !== false )
		$totalMap[$i] = 0;
}

if(isset($answers)){
	foreach ( $answers as $q => $a ) {	
		foreach ($totalMap as $i => $tot) {
			if(isset($a[$i]))
				$totalMap[$i] = $tot + $a[$i];
		}
	}
}

$total = 0;
foreach ( $totalMap as $i => $tot ) {
	if( $tot != 0 )
		$total = $total + $tot ;
}

if($total > 0){

	echo "<tr class='bold'>";
		echo "<td></td>";
		echo "<td>TOTAL : </td>";
		foreach ($totalMap as $i => $tot) {
			if( $tot != 0 )
				echo "<td>".(($tot == 0) ? "" : trim(strrev(chunk_split(strrev($tot),3, ' ')))."€")."</td>";
		}
		echo "<td></td>";
	echo "</tr>";

	echo "<tr class='bold'>";
	echo 	"<td colspan='5' style='text-align:right'>FINANCEMENT TOTAL : </td>";
	echo 	"<td colspan='2'>".trim(strrev(chunk_split(strrev($total),3, ' ')))." €</td>";
	echo "</tr>";

}

	

?>
		</tbody>
	</table>
</div>

<?php 
if( isset($parentForm["params"]["financement"]["tpl"])){
	//if( $parentForm["params"]["financement"]["tpl"] == "tpls.forms.equibudget" )
		echo $this->renderPartial( "costum.views.".$parentForm["params"]["financement"]["tpl"] , 
		[ "totalFin"   => $total,
		  "totalBudg" => Yii::app()->session["totalBudget"]["totalBudget"] ] );
	// else 
	// 	$this->renderPartial( "costum.views.".$parentForm["params"]["financement"]["tpl"]);
}
 ?>

<script type="text/javascript">
if(typeof dyFObj.elementObjParams == "undefined")
	dyFObj.elementObjParams = {};
dyFObj.elementObjParams.financerTypeList = <?php echo json_encode(Ctenat::$financerTypeList); ?>;
dyFObj.elementObjParams.financersList = <?php echo json_encode($orgs); ?>;

var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

$(document).ready(function() { 
	
	sectionDyf.<?php echo $kunik ?> = {
		"jsonSchema" : {	
	        "title" : tradForm.financingPlan,
            "icon" : "fa-money",
            "text" : tradForm.describeHereFinancingMobilized+ "<b>" +tradForm.exclusiveTax +"</b>.",
	        "properties" : <?php echo json_encode( $properties ); ?>,
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    urlCtrl.loadByHash(location.hash);
	                } );
	            }

	    	}
	    }
	};

	sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "<?php echo $kunik ?> config",
	        "icon" : "fa-cog",
	        "properties" : {
	            financerTypeList : {
	                inputType : "properties",
	                labelKey : "Clef",
	                labelValue : tradForm.labelDisplayes,
	                label : tradForm.listTypesFinancing,
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.financerTypeList
	            } , 
	            limitRoles : {
	                inputType : "array",
	                label : tradForm.listFinancingRoles,
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.limitRoles
	            },
	            tpl : {
	                label : tradForm.subTemplate,
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.tpl
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	        		if(val.inputType == "array")
	        		 	tplCtx.value[k] = getArray('.'+k+val.inputType);
	        		else if(val.inputType == "properties")
	        		 	tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
	        		else
	        		 	tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    urlCtrl.loadByHash(location.hash);
	                } );
	            }

	    	}
	    }
	};


    mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

    //adds a line into answer
    $(".add<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
    });

    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
    });

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    
});
</script>
<?php } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>