<style>
    th:focus, td:focus, th:active, td:active{
        border-radius: 0px;
        outline:none;
        border:1px solid #3B9076 !important;
        border-top:1px solid #3B9076 !important;
        border-left:1px solid #3B9076 !important;
    }

    .btn-co-green{
        background: #9FBD38;
    }

    [placeholder]:empty::before {
        content: attr(placeholder);
        color: #eee; 
    }

    [placeholder]:empty:focus::before {
        content: "";
    }

    .checkable{
        cursor: pointer;
        border:  2px solid #ccc;
    }

</style>

<?php if($answer){ ?>
    <div class="form-group">
  
        <?php
        # Initialize parameters data
        $paramsData = [
            "tableName" => "Titre",
            "columns" => [
                [
                    "label" => "Outils/Usage",
                    "type" => "Text"
                ],
                [
                    "label" => "Communication Interne",
                    "type" => "Case à cocher"
                ],
                [
                    "label" => "Communication Externe",
                    "type" => "Case à cocher"
                ],
                [
                    "label" => "Gestion de tâches",
                    "type" => "Case à cocher"
                ],
                [
                    "label" => "Note (sur 10)",
                    "type" => "Nombre"
                ],
                [
                    "label" => "Rémarque",
                    "type" => "Text"
                ]
            ],
            "rows" => [
                [
                    "label" => "Communecter"
                ],
                [
                    "label" => "OCECO"
                ]
            ],
            "activeNewLine" => true,
            "singleAnswerByLine" => true
        ];

        if(isset($parentForm["params"][$kunik])){
            if(isset($parentForm["params"][$kunik]["tableName"])){
                $paramsData["tableName"] = $parentForm["params"][$kunik]["tableName"];
            }

            if(isset($parentForm["params"][$kunik]["columns"])){
                $paramsData["columns"] = $parentForm["params"][$kunik]["columns"];
            }

            if(isset($parentForm["params"][$kunik]["rows"])){
                $paramsData["rows"] = $parentForm["params"][$kunik]["rows"];
            }

            if(isset($parentForm["params"][$kunik]["activeNewLine"])){
                $paramsData["activeNewLine"] = $parentForm["params"][$kunik]["activeNewLine"];
            }else{
                $paramsData["activeNewLine"] = false;
            }

            if(isset($parentForm["params"][$kunik]["singleAnswerByLine"])){
                $paramsData["singleAnswerByLine"] = $parentForm["params"][$kunik]["singleAnswerByLine"];
            }else{
                $paramsData["singleAnswerByLine"] = false;
            }


        }

        $editParamsBtn = ($editQuestionBtn!="")?" <a href='javascript:;' data-id='".$parentForm["_id"]."'
            data-collection='".Form::COLLECTION."' 
            data-path='params.".$kunik."' 
            class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> 
                                            </a>" : "";
        ?>

        <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
            <?php echo $label.$editQuestionBtn.$editParamsBtn;?>
        </h4>

        <?php echo $info;?>

        <br/>
        
        <div style="overflow-x: scroll;" id="tableContainer<?= $kunik ?>" class="tableContainerSimpleTable"></div>

        <?php if($paramsData["activeNewLine"]=="true"){ ?>
            <div>
                <button id="addRow<?= $kunik ?>" type="button" class="btn btn-success btn-block btn-co-green my-0">
                    <i class="fa fa-plus-circle"></i> AJOUTER UNE LIGNE
                </button>
            </div>
        <?php } ?>
    </div>

    <script type="text/javascript">
        $('.editable<?= $kunik ?>').remove();
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

        let tableData<?= $kunik ?> = [];
      
        $(document).ready(function(){

            let columns = <?php echo json_encode($paramsData["columns"]); ?>;
            let rows = <?php echo json_encode( $paramsData["rows"] ); ?>;
            columns.unshift({label:"<?php echo $paramsData['tableName']; ?>", type:"Text"});

            <?php if(empty($answers) || (empty($answers) && $paramsData["activeNewLine"]=="false")){ ?>
                for(var i = 0; i <= rows.length; i++) {
                    tableData<?= $kunik ?>[i] = [];
                    for(var k = 0; k < columns.length; k++) {
                        if(i==0){
                            // titles
                            tableData<?= $kunik ?>[i][k] = columns[k]["label"];
                        }else{
                            // Data
                            tableData<?= $kunik ?>[i][k] = (k==0)?rows[i-1]["label"]:"";
                        }
                    }
                }
            <?php }else{ ?>
                tableData<?= $kunik ?> = <?php echo json_encode( $answers ); ?>;
                
                for(var k = 0; k < columns.length; k++) {
                    tableData<?= $kunik ?>[0][k] = columns[k]["label"];
                }
            <?php } ?>

            let formmode = "<?= $mode ?>";

            let simpleTalbe<?= $kunik ?> = createTable($("#tableContainer<?= $kunik ?>"), tableData<?= $kunik ?>);

            if(formmode=="w"){
                $(document).on("click", "#addRow<?= $kunik ?>", function(){
                    var defaultRow = [];
                    for(var index = 0; index < columns.length; index++) {
                        if(index==0){
                            defaultRow[index]= "Entrée";
                        }else{
                            defaultRow[index] = "";
                        }
                    }

                    appendTableRow(simpleTalbe<?= $kunik ?>, defaultRow);
                });

                $(document).on("click", ".editable<?= $kunik ?>[data-valuetype='Case à cocher']", function(){
                    
                    <?php if($paramsData["singleAnswerByLine"]=="true"){ ?>

                        $(this).parent().children("td").text("");
                        mylog.log("tablee",tableData<?= $kunik ?>);
                    
                    <?php } ?>
                    //alert(line);
                     // for(var k = 1; k < columns.length; k++){
                     //     tableData<?= $kunik ?>[line][k]="";
                     // } 
                 

                    if($(this).text()!=""){
                        $(this).text("");
                    }else{
                        $(this).text("x");
                    }
                    $(this).blur();
                });

                $(document).on("click", "#remove<?= $kunik ?>", function(){
                    $(this.parentNode.parentNode).remove();
                    saveSpreadsheetData<?= $kunik ?>();
                });

                $(document).on("keyup", "td[data-valuetype='Nombre']", function(){
                    if($(this).text()!="" && isNaN($(this).text())){
                        $(this).text("");
                        toastr.warning("Veuillez ajouter de "+$(this).data("valuetype"));
                    }
                });

                setTimeout(()=>{
                    $(".editable<?=$kunik?>").off().one("blur", function(e){
                    saveSpreadsheetData<?= $kunik ?>();
                    });
                }, 400);
            }    

            sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema" : {
                    "title" : "Paramétrage",
                    "description" : "",
                    "icon" : "cog",
                    "properties" : {
                        tableName: {
                            inputType : "text",
                            label : "Nom du tableau",
                            class : "col-xs-10",
                            value : sectionDyf.<?php echo $kunik ?>ParamsData.tableName
                        },
                        columns : {
                            inputType : "lists",
                            label : "Titre des colonnes du tableau",
                            entries: {
                                label: {
                                    type:"text",
                                    label:"Colonne :"
                                },
                                type: {
                                    label:"Nature de données",
                                    type:"select",
                                    class:"col-xs-5",
                                    options:[
                                        "Text",
                                        "Case à cocher",
                                        "Nombre"
                                    ],
                                    value:"Case à cocher"
                                }
                            }
                        },
                        rows : {
                            inputType : "lists",
                            label : "Titre des lignes par defauts",
                            entries: {
                                label: {
                                    type:"text",
                                    label:"Titre :"
                                }
                            }
                        },
                        activeNewLine:{
                            inputType:"checkboxSimple",
                            label : "Activer l'ajout de ligne",
                            params : {
                                "onText" : trad.yes,
                                "offText" : trad.no,
                                "onLabel" : trad.yes,
                                "offLabel" : trad.no
                            },
                            checked:false
                        },
                        singleAnswerByLine:{
                            inputType:"checkboxSimple",
                            label : "Une seule réponse par ligne (cases à cocher)",
                            params : {
                                "onText" : trad.yes,
                                "offText" : trad.no,
                                "onLabel" : trad.yes,
                                "offLabel" : trad.no
                            },
                            checked:false
                        }
                    },
                    save : function (data) {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                            if(val.inputType == "array")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else
                                tplCtx.value[k] = $("#"+k).val();
                            
                            if(k=="columns"){
                                let pColumns = {};
                                $.each(data.columns, function(index, va){
                                    let pColumn = {label: va.label, type: va.type};
                                    pColumns[index] = pColumn;
                                });
                                tplCtx.value[k] = pColumns;
                            }

                            if(k=="rows"){
                                let pRows = {};
                                $.each(data.rows, function(index, va){
                                    let pRow = {label: va.label};
                                    pRows[index] = pRow;
                                });
                                tplCtx.value[k] = pRows;
                            }

                            mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                        });
                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == undefined)
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                                location.reload();
                            });
                        }
                    }
                }
            };

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            });

            "use strict";
            function createTable(container, data) {
                var table = $("<table/>").addClass('table table-bordered table-responsive-md table-striped text-center');
                $.each(data, function(rowIndex, r) {
                    var row = $("<tr/>");
                    
                    $.each(r, function(colIndex, c) { 
                        var cell = $("<t"+((rowIndex == 0 || colIndex==0) ?  "h" : "d")+"/>").text(c);
                        if(rowIndex != 0){
                            if(columns[colIndex]){
                                var isEditable = (columns[colIndex]["type"]!="Case à cocher")?true:false;
                                cell.attr({"contenteditable":isEditable, "placeholder":(isEditable)?columns[colIndex]["type"]:"", "data-valuetype":columns[colIndex]["type"], "id":"id"+rowIndex+colIndex})
                                .addClass("editable<?= $kunik ?>");

                                if(isEditable==false){
                                    cell.addClass("checkable");
                                }
                            }
                        }
                        row.append(cell);
                    });

                    if(rowIndex!=0 && formmode=="w" && sectionDyf.<?php echo $kunik ?>ParamsData.activeNewLine=="true"){
                        row.append("<td><button id='remove<?= $kunik ?>' class='btn btn-danger' >&times</button></td>");
                    }

                    table.append(row);
                });

                return container.append(table);
            }

            function appendTableRow(table, rowData) {
                var lastRow = $('<tr/>').appendTo(table.find('tbody:last'));
                
                $.each(rowData, function(colIndex, c) { 
                    var isEditable = (columns[colIndex]["type"]!="Case à cocher")?true:false;
                    lastRow.append($("<t"+(colIndex==0 ?  "h" : "d")+"/>")
                            .text(c)
                            .attr({"contenteditable":isEditable, "placeholder":columns[colIndex]["type"], "data-valuetype":columns[colIndex]["type"]})
                            .addClass("editable<?= $kunik ?>"));
                });
                if(sectionDyf.<?php echo $kunik ?>ParamsData.activeNewLine=="true")
                lastRow.append("<td><button id='remove<?= $kunik ?>' class='btn btn-danger' >&times</button></td>");
                return lastRow;
            }

            function getTableData(table) {
                var data = [];
                table.find('tr').each(function (rowIndex, r) {
                    var cols = [];
                
                    $(this).find('th,td').each(function (colIndex, c) {
                        if($(c).children().length==0){
                            cols.push(c.textContent);
                        }
                    });
                    
                    data.push(cols);
                });

                return data;
            }

            function getTableHead(table) {
                var data = [];
                //var autre = {};
                table.find('tr').each(function (rowIndex, r) {
                    var cols = [];

                    $(this).find('th').each(function (colIndex, c) {
                        
                        /**if(rowIndex==0 && colIndex!=0){
                            autre[c.textContent] = {};
                        }

                        if(colIndex==0){
                            let valueIndexO = c.textContent;
                            autre[c.textContent][valueIndexO] = {};
                        }*/
                        
                        cols.push(c.textContent);
                    });
                    
                    data.push(cols);
                });

                return data;
            }

            function formatTableDataToJSON(header, dataTable) {
                var jsonData = {};
                
                for(var k=1; k < dataTable.length; k++){
                    //jsonData[k] = {label:};
                }

                return jsonData;
            }

            function saveSpreadsheetData<?= $kunik ?>(){
                if( notNull(answerObj)){
                    var answer = {
                        collection : "answers",
                        id : answerObj._id.$id,
                        path : "answers.<?= $key ?>"
                    };

                    if(answerObj.form){
                        answer.path = "answers.<?= $form["id"]?>.<?= $key ?>";           
                    }
                  
                    answer.value = getTableData(simpleTalbe<?= $kunik ?>);
                
                    dataHelper.path2Value( answer , function(params) {
                        toastr.success('Modification enregistrée');
                        //stepValidationReload<?php echo $form["id"]?>();
                        reloadInput("<?php echo $key ?>", "<?php echo (string)$form["id"] ?>");
                    });

                } else {
                    toastr.error('answer cannot be empty, on saveOneByOne!');
                }
            }

            jQuery(function($){
                $("[contenteditable]").focusout(function(){
                    var element = $(this);        
                    if (!element.text().trim().length) {
                        element.empty();
                    }
                });
            });
        });

    </script>
<?php } ?>