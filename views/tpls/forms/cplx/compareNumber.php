<?php

	$formList = [];
	$paramsData = [
        "sign" => "",
        "one" => "",
        "two" => "",
        "oneValue" => "",
        "twoValue" => ""
  ];

  $sign = [
  	"more" => "N1 > N2",
  	"moreEqual" => "N1 >= N2",
  	"less" => "N1 < N2",
  	"lessEqual" => "N1 <= N2",
  	"equal" => "N1 = N2"
  ];

	$editParamsBtn = ($canEditForm and $mode != "r" || $mode != "pdf") ? "Comparateur <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";


  if( isset($parentForm["params"][$kunik]["sign"]) ){
       $paramsData["sign"] =  $parentForm["params"][$kunik]["sign"];
  }

  if( isset($parentForm["params"][$kunik]["one"]) ){
      $paramsData["one"] = $parentForm["params"][$kunik]["one"];
      
      if (isset($answer["answers"][$form["id"]])) {
          
          foreach ($answer["answers"][$form["id"]] as $kAns => $vAns) {
            // var_dump("(",$kAns,")");
            // var_dump(is_numeric($vAns));
            if(strpos($kAns, $parentForm["params"][$kunik]["one"]) !== false  && is_numeric($vAns) ){
                $paramsData["oneValue"] =  (int)$vAns;
            }
          }

      }
  }

  if( isset($parentForm["params"][$kunik]["two"]) ){
      $paramsData["two"] = $parentForm["params"][$kunik]["two"];
      
      if (isset($answer["answers"][$form["id"]])) {
          
          foreach ($answer["answers"][$form["id"]] as $kAns => $vAns) {
            if(strpos($kAns, $parentForm["params"][$kunik]["two"]) !== false  && is_numeric($vAns) ){
                $paramsData["twoValue"] =  (int)$vAns;
            }
          }

      }
  }

	if (isset($form)) {
		foreach ($form["inputs"] as $fid => $fvalue) {
			if ($fid != $key) {
				$formList[$fid] = $fvalue["label"];
			}
		}
	}

	function isValid($sign,$oneV,$twoV){
    if ($sign == "more" && $oneV > $twoV) {
      return true;
    } elseif ($sign == "moreEqual" && $oneV >= $twoV) {
      return true;
    } elseif ($sign == "less" && $oneV < $twoV) {
      return true;
    } elseif ($sign == "lessEqual" && $oneV <= $twoV) {
      return true;
    }
    return false;
	}

  echo $editParamsBtn;
  $reloadIn = "";
  foreach ($form["inputs"] as $keyfI => $valuefI) {
       if ($valuefI["type"] == "tpls.forms.cplx.validateStep") {
            $reloadIn = $keyfI;
       }
  }
?>

<script type="text/javascript">

	var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
 
      $("form :input").on('blur', function() {
          mylog.log("aaaaaaaa");
         reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
      });
   

	$(document).ready(function() {

    <?php 
      if ($paramsData["sign"] != "" && $paramsData["one"] != "" && $paramsData["two"] != "" && $paramsData["oneValue"] != "" && $paramsData["twoValue"] != "" && $answers == null && isValid($paramsData["sign"], $paramsData["oneValue"],$paramsData["twoValue"]) && $reloadIn != "") {
    ?>
        
          $.when(validateNumberInput<?php echo $kunik ?>()).then(
              reloadInput("<?php echo $reloadIn ?>", "<?php echo (string)$form["_id"] ?>")
          );

    <?php
          }
    ?>

		sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {
                "title" : "<?php echo $kunik ?> config",
                "description" : tradForm.possibleQuestionList,
                "icon" : "fa-cog",
                "properties" : {
                	sign : {
		                inputType : "select",
		                label : tradForm.function,
		                options :  <?php echo json_encode($sign) ?>,
		                values :  sectionDyf.<?php echo $kunik ?>ParamsData.sign
		            },
                	one : {
		                inputType : "select",
		                label : "N1",
		                options :  <?php echo json_encode($formList) ?>,
		                values :  sectionDyf.<?php echo $kunik ?>ParamsData.one
		            },
		            two : {
		                inputType : "select",
		                label : "N2",
		                options :  <?php echo json_encode($formList) ?>,
		                values :  sectionDyf.<?php echo $kunik ?>ParamsData.two
		            }

                },
                save : function () {
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                        if(val.inputType == "properties")
                            tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                        else if(val.inputType == "array")
                            tplCtx.value[k] = getArray('.'+k+val.inputType);
                        else
                            tplCtx.value[k] = $("#"+k).val();
                        mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                    });
                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined"){
                        toastr.error('value cannot be empty!');
                    }
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.closeForm();
                            urlCtrl.loadByHash(location.hash);
                        } );
                    }

                }
            }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
		    tplCtx.id = $(this).data("id");
		    tplCtx.collection = $(this).data("collection");
		    tplCtx.path = $(this).data("path");
		    dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
		});

		function validateNumberInput<?php echo $kunik ?>() {
              tplCtx = {};
              tplCtx.id = "<?php echo $answer["_id"] ?>",
              tplCtx.path = "<?php echo "answers.".$form["id"].".".$key ?>",
              tplCtx.collection = "<?php echo Form::ANSWER_COLLECTION ?>"; 
              tplCtx.value = "validée";
              dataHelper.path2Value( tplCtx, function(params) {
                  reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
              } );
    };   

	});
</script>