<?php 
$value = (!empty($answers)) ? " value='".$answers."' " : "";

$inpClass = "form-control";


if($saveOneByOne)
    $inpClass .= " saveOneByOne";

if($mode == "r"){ ?>
    <div class="col-xs-12" id="<?php echo $kunik ?>">
        <label for="<?php echo $kunik ?>"><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label ?></h4></label>
        <?php echo $answers; ?>
    </div>
<?php 
}else{
?>
     <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    <div class="form-group">
        <label for="<?php echo $key ?>"><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn ?></h4></label>
        
        <br/>
        <select id="tags<?php echo $kunik ?>" multiple>
          <option  selected="selected">orange</option>
          <option>white</option>
          <option>purple</option>
        </select>
        <?php if(!empty($info)){ ?>
            <small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
        <?php } ?>
    </div>

     <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>


    <script type="text/javascript">
    let optionSelect2<?php echo $kunik ?> = {
        templateResult: formatResult,
        closeOnSelect: false,
        width: '90%',
        placeholder: 'Saisir un territoire'
    };
    
    let $select2<?php echo $kunik ?> = $("#tags<?php echo $kunik ?>").select2(optionSelect2<?php echo $kunik ?>);
    </script>
<?php } ?>