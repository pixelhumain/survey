<style type="text/css">
	.titlecolor<?php echo $kunik ?> {
		<?php 
			if( $mode != "pdf"){	
		?>
		color: <?php echo ($titleColor) ? $titleColor : "black"; ?>;
		<?php
			}
		?>
	}

	.calendar-container {
		padding: 10px;
		<?php 
			if( $mode != "pdf"){	
		?>
		border: 2px solid #ddd;
		<?php
			}
		?>
	}

	.calendar-table {
		padding: 10px;
		width: 100%;
		table-layout: fixed;
		border-collapse: collapse;
		<?php 
			if( $mode == "pdf"){	
		?>
		font-size: 15px;
		<?php
			}
		?>
	}

	.calendar-table th {
		border-bottom: 2px solid #195391; 
		text-align: center
	}

	.calendar-table td {
		border-bottom:  2px solid #ddd;
	}

	.calendar-table tr th:nth-child(1){
       width: 100px;
    }

    .calendar-table tr td:nth-child(1){
       width: 100px;
       border-right: 3px solid #ddd;
    }

    .calendar-table tr td+td{
       font-size: 15px;
    }

    .calendar-table tr th:last-child{
       width: 50px;
    }

    .calendar-table .col-bordered {
    	border-right: 3px solid #ddd;
    }

</style>

	<?php if($answer){ ?>
		<?php if (isset($datetype)) {
			var_dump($datetype);
		}
		?>

	<?php 
	$paramsData = [ 
	 "sectionTitles" => [ 
            "1er<br/>Sem<br/>2018", 
            "2ème<br/>Sem<br/>2018", 
            "1er<br/>Sem<br/>2019", 
            "2ème<br/>Sem<br/>2019", 
            "1er<br/>Sem<br/>2020", 
            "2ème<br/>Sem<br/>2020", 
            "1er<br/>Sem<br/>2021", 
            "2ème<br/>Sem<br/>2021", 
            "1er<br/>Sem<br/>2022", 
            "2ème<br/>Sem<br/>2022"
        ],
        "dateSections" => [ 
            "01/01/2018", 
            "01/07/2018", 
            "01/01/2019", 
            "01/07/2019", 
            "01/01/2020", 
            "01/07/2020", 
            "01/01/2021", 
            "01/07/2021", 
            "01/01/2022", 
            "01/07/2022", 
            "01/01/2023"
        ]];
        //we add a global period params in forms@ctenatForm.params.period
        if(isset($parentForm["params"]["period"])){
        	
        } else {
			$dateSections = (isset($parentForm["params"][$kunik]["dateSections"])) ? $parentForm["params"][$kunik]["dateSections"] : $paramsData["dateSections"] ;
			$sectionTitles = (isset($parentForm["params"][$kunik]["sectionTitles"])) ? $parentForm["params"][$kunik]["sectionTitles"] : $paramsData["sectionTitles"];
		}
		if( $mode != "pdf" and $mode != "r"){	
			$editBtnL = ($canEdit === true) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default'><i class='fa fa-plus'></i> ".Yii::t('common','Add line')." </a>" : "";
			
			$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

			$widthpdf = "";
		} else {
			$editBtnL = "";
			
			$editParamsBtn = "";

			$widthpdf = 'style="width:15%;border-right: 3px solid #ddd;"';
		}

		if(!isset($datetype)){
			$datetype = "date";
		}
	?>	
<?php
	$thYear = [];
	if(isset($parentForm["params"]["period"])){
        $from = $parentForm["params"]["period"]["from"];
        $to = $parentForm["params"]["period"]["to"];
        while ( $from <= $to) {
        		$sectionTitles[] = "1erS";
				$sectionTitles[] = "2emS";
				$dateSections[] = "01/01/".$from;
            	$dateSections[] = "01/07/".$from;

            	array_push($thYear, $from);
            	$from++;
        	}
        	$dateSections[] = "01/01/".$from;
        }
?>

<div class="form-group">
<div ><h4 class="titlecolor<?php echo $kunik ?> pdftittlecolor" ><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
				<?php echo $info ?>
</div>

<div class="calendar-container">
	<table class="calendar-table">
		<thead>
			<tr>
				<th <?php echo $widthpdf; ?>>
					<?php echo Yii::t("cooperation", "Actions") ?>
				</th>
				<?php
					foreach ($thYear as $idYear => $valueYear) {
						echo '<th colspan="2">'.$valueYear.'</th>';
					}
				?>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php 
		$ct = 0;
		
		if(isset($answers)){
			foreach ($answers as $q => $a) {

				echo '<tr id="'.$kunik.$q.'" class="'.$kunik.'Line">'.
					'<td '.$widthpdf.'>'.@$a["step"]."<br/>".$a["startDate"]."<br/>".$a["endDate"]."</td>";
				$bgColor = "white";
				$td = "";
				$tdValue = "";
				foreach ($dateSections as $sa => $sv) {

					if($sa%2){
						$colClass = "col-bordered";
					} else {
						$colClass = "";
					}

					if( $bgColor == "white"){
						if( isset( $dateSections[$sa+1] ) && isset($a["startDate"]) && 
							strtotime(str_replace('/', '-',"01/".$a["startDate"])) < strtotime(str_replace('/', '-',$dateSections[$sa+1]))  ) {
							$bgColor = "#43a9b2";
							$td = $sectionTitles[$sa];
						}
					} else if( $bgColor == "#43a9b2"){
						if( isset($a["endDate"]) && strtotime(str_replace('/', '-',"28/".$a["endDate"])) < strtotime(str_replace('/', '-',$sv))  ) {
							$bgColor = "";
							$td = "";
						}
					}

					if($bgColor == "#43a9b2" && !empty($sectionTitles[$sa])){
						$td = $sectionTitles[$sa];
					} else {
						$td = "";
					}


					if( $sa != sizeof( $dateSections ) - 1 ){
					?>
						<td class="text-center <?php echo $colClass; ?>" style="background-color: <?php echo $bgColor ?>" > <?php //echo $td ?></td>
						<?php
					}
				}
				if( $mode != "pdf" and $mode != "r"){	
			?>
			<td>
				<?php 
					echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
						"canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
						"id" => $answer["_id"],
						"collection" => Form::ANSWER_COLLECTION,
						"q" => $q,
						"path" => $answerPath.$q ,
						"keyTpl"=>$kunik,
						"mode" => $mode
						] );
					
					?>
				<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo addslashes(@$a['step']) ?>','<?php echo $key ?>','<?php echo (string)$form["_id"]; ?>')">
					<?php 
						echo PHDB::count(Comment::COLLECTION, array("contextId"=>(string)$answer["_id"],"contextType"=>Answer::COLLECTION, "path"=>(string)$answer["_id"].$key.$q));?> 
						<i class='fa fa-commenting'></i></a>
			</td>
			<?php 
				}
				$ct++;
				echo "</tr>";
			}
		}
		 ?>
		</tbody>
	</table>
</div>
<?php
if( $mode != "pdf" and $mode != "r"){	
?>
<script type="text/javascript">
var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;

$(document).ready(function() { 

	sectionDyf.<?php echo $kunik ?> = {
		"jsonSchema" : {	
	        "title" : trad.calendar,
	        "description" : tradForm.keyStepOfActionSheet,
	        "icon" : "fa-calendar",
	        "properties" : {
	            "step" : {
	                "inputType" : "text",
	                "label" : trad.actions,
	                "placeholder" : trad.actions,
	                "rules" : { "required" : true }
	            },
	            "startDate" : {
	                "inputType" : "monthyear",
	                "label" : trad.start,
	                "rules" : { "required" : true }
	            },
	            "endDate" : {
	                "inputType" : "monthyear",
	                "label" : trad.end,
	                "rules" : { "required" : true }
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    urlCtrl.loadByHash(location.hash);
	                } );
	            }

	    	}
	    }
	};

	sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "<?php echo $kunik ?> config",
	        "description" : tradForm.labelAndDataSection,
	        "icon" : "fa-cog",
	        "properties" : {
	            sectionTitles : {
	                "inputType" : "array",
	                "label" : tradForm.labelEveryPeriod
	            },
	            dateSections : {
	                "inputType" : "array",
	                "label" : tradForm.dateEveryPeriod
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	        		if(val.inputType == "array")
	        		 	tplCtx.value[k] = getArray('.'+k+val.inputType);
	        		else
	        		 	tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    urlCtrl.loadByHash(location.hash);
	                } );
	            }

	    	}
	    }
	};

	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( ( isset($parentForm["params"][$kunik]) ) ? $parentForm["params"][$kunik] : $paramsData); ?>;

    mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

    //adds a line into answer
    $(".add<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");       
        tplCtx.path = $(this).data("path")+(notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length :"0");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
    });

    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
    });

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    
});
</script>
<?php }} else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
}?>