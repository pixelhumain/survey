<?php if($answer){ 
$debug = false;
$editBtnL = (Yii::app()->session["userId"] == $answer["user"]) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";

$dynForm = PHDB::findOne(Form::COLLECTION,["id"=>$key]);
if(!isset($dynForm)){
	echo "<h4>Form not found key : ".$key."</h4>";
	exit;
}


$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$dynForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='inputs' class='previewTpl edit".$dynForm["id"]."Dynform btn btn-xs btn-danger'><i class='fa fa-cog'></i> Editer le formulaire </a>" : "";

//var_dump( PHDB::findOne( Form::COLLECTION,["id"=>"arrayDynFormPixel"] ) );
$properties = ( isset($dynForm["inputs"]) ) ? $dynForm["inputs"] : [];
?>	

<?php
echo $this->renderPartial("survey.views.tpls.forms.cplx.openDynFormTable",
	                      [ 
	                        "form" => $form,
	                        "wizard" => true, 
	                        "answers"=>$answers,
	                        "answer"=>$answer,
                            "mode" => $mode,
                            "kunik" => $kunik,
                            "answerPath"=>$answerPath,
                            "key" => $key,
                            "titleColor" => $titleColor,
                            "properties" => $properties,


                            "label" => $label,
                            "editQuestionBtn" => $editQuestionBtn,
                            "editParamsBtn" => $editParamsBtn,
                            "editBtnL" => $editBtnL,
                            "info" => $info,
	                        //"showForm" => $showForm,
	                        "canEdit" => $canEdit,  
	                        //"el" => $el 
	                    ] ,true );

if($mode != "r" && $mode != "pdf"){
?>
<?php if( $mode == "fa" ) {  ?>
<style type="text/css">
	#dynformUl{list-style: none;}
	#dynformUl li{  padding: 3px; }
	.miniTxt{font-size: 0.7em}
</style>
<div id="form-buildDynform<?php echo $dynForm["id"]?>" style="display:none; ">
	<h4 class='text-center' style="margin-top: 50px">Construteur de formulaire</h4>
	<ul id="dynformUl">
		<?php 
		//var_dump($properties);
		//echo "<li> open dyn form _id >".$dynForm["_id"]."</li>";
		foreach ( $properties as $k => $inp ) {
			echo "<li id='input".$kunik.$k."'> > ".
					$inp["label"]."<span class='miniTxt'>( ".@$inp["inputType"]." ) ".@$inp["placeholder"]." </span> ".
						//Edit Input
						"<a href='javascript:;' data-path='inputs.".$k."' class='pull-right btn btn-xs btn-primary editDynformInput' ><i class='fa fa-pencil'></i></a>".
						//Delete Input
						" <a href='javascript:;'  data-path='inputs.".$k."' data-inputid='".$kunik.$k."' class='deleteInput pull-right btn btn-xs btn-danger' ><i class='fa fa-trash'></i></a><li>";
		} ?>
		<li class='text-center'>
			<a href='javascript:;' data-id='<?php echo $dynForm["_id"]?>' data-collection='<?php echo Form::COLLECTION?>' data-path='inputs' class='addInputDynform_<?php echo $dynForm["id"]?> btn btn-primary' ><i class="fa fa-plus"></i> Ajouter une Question au formulaire </a> 
		</li>
	</ul>
	
</div>
<?php } ?>

<script type="text/javascript">

var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;

$(document).ready(function() { 
	
	sectionDyf.<?php echo $kunik ?> = {
		jsonSchema : {	
	        title : "<?php echo $input["label"]?>",
            icon : "fa-lists",
	        properties : <?php echo json_encode( $properties ); ?>,
	        save : function (formData) {
	        	mylog.log('save tplCtx formData', formData)

	        	delete formData.collection ;
	            tplCtx = {
	            	collection : "forms",
	            	value : formData
	            };
	            delete tplCtx.value.formid;
	            tplCtx.value.id = tplCtx.value.formid;
	            mylog.log("save tplCtx",tplCtx);
	            
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                   urlCtrl.loadByHash(location.hash);
	                } );
	            }

	    	}
	    }
	};

	sectionDyf.newDFInput<?php echo $dynForm["id"]?> = {
		jsonSchema : {	
	        title : "Ajouter un Question",
            icon : "fa-plus",
            text : "lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum ",
            onLoads : {
	    		onload : function () { 
	    			bb.modal('hide');	    			
	    		}
	    	},
	        properties : {
	        	inputType : { 
                	label : "Type de cette question",
                	inputType : "select",
                    options : <?php echo json_encode(Form::$dynformInputTypes); ?>,
                    value : "text" ,
		        	init:function(){ 
		        	 	$("#ajaxFormModal #inputType").on( "change", function(){
		        	 		$("#ajaxFormModal .listarray,.multiplecheckboxSimple, #ajaxFormModal .openSearchcheckboxSimple, #ajaxFormModal .invitecheckboxSimple,#ajaxFormModal .maxlengthtext,#ajaxFormModal .onTexttext, #ajaxFormModal .offTexttext, #ajaxFormModal .initValuecheckboxSimple, #ajaxFormModal .labelKeytext, #ajaxFormModal .labelValuetext").addClass("hide");

		        	 		if( $.inArray( $(this).val(), [ "select","finder","radio","tags"] ) >= 0 )
		        	 			$("#ajaxFormModal .listarray").removeClass("hide");

		        	 		if( $(this).val() == "finder" )
		        	 			$("#ajaxFormModal .finderTypesselect, #ajaxFormModal .multiplecheckboxSimple, #ajaxFormModal .openSearchcheckboxSimple, #ajaxFormModal .invitecheckboxSimple").removeClass("hide");

		        	 		if($.inArray( $(this).val(), [ "textarea","tags"] )>=0)
		        	 			$("#ajaxFormModal .maxlengthtext").removeClass("hide");
		        	 		
		        	 		if($(this).val() == "checkboxSimple" )
		        	 			$( "#ajaxFormModal .onTexttext, #ajaxFormModal .offTexttext, #ajaxFormModal .initValuecheckboxSimple" ).removeClass("hide");
		        	 		
		        	 		if( $(this).val() == "properties" )
		        	 			$( "#ajaxFormModal .labelKeytext, #ajaxFormModal .labelValuetext" ).removeClass("hide");
		        	 		
		        	 	})
		        	 }
			    },
			    label : { label : "Titre de la Question",rules : {required : true} },
	        	subLabel : { label : "Texte sous la la Question" },
                placeholder : { label : "Texte dans la Question" },
                info : { 
                    inputType : "textarea",
                    label : "Information complémentaire sur la Question" },
                required : {
	                inputType : "checkboxSimple",
                    label : "Obligatoire",
                    subLabel : "Ce champs est obligatoire",
                    params : {
                        onText : "Oui",
                        offText : "Non",
                        onLabel : "Oui",
                        offLabel : "Non",
                        labelText : "Obligatoire"
                    },
                    checked : false
	            },
                
			    
			    //config inputs activate later by type 
			    initType : {
                	inputType : "select",
                	label : "Les types possible du finder",
                	placeholder : "types d'éléments",
                	options : {
                		"persons" : "Personnes",
                		"organizations":"Organisations",
                		"projects":"Projets",
                		"events":"Evennements",
                	},
                	topClass : "hide"
                },
                multiple : {
	                inputType : "checkboxSimple",
                    label : "Multiple",
                    subLabel : "Configuré le Finder",
                    params : {
                        onText : "Oui",
                        offText : "Non",
                        onLabel : "Oui",
                        offLabel : "Non",
                        labelText : "Multiple"
                    },
                    checked : true,
                    topClass : "hide"
	            },
	            openSearch : {
	                inputType : "checkboxSimple",
                    label : "Chercher Partout",
                    subLabel : "Configuré le Finder",
                    params : {
                        onText : "Oui",
                        offText : "Non",
                        onLabel : "Oui",
                        offLabel : "Non",
                        labelText : "Chercher Partout"
                    },
                    checked : true,
                    topClass : "hide"
	            },
	            invite : {
	                inputType : "checkboxSimple",
                    label : "Invité",
                    subLabel : "Configuré le Finder",
                    params : {
                        onText : "Oui",
                        offText : "Non",
                        onLabel : "Oui",
                        offLabel : "Non",
                        labelText : "Invité"
                    },
                    checked : true,
                    topClass : "hide"
	            },
                list : {
                    placeholder : "Liste des options | roles",
	                label : "Liste des options pour les select et role pour les finders",
			    	inputType : "array",
			    	topClass : "hide", 
			        value : []
                },
                maxlength : { 
                	label : "Limiter le texte",
                	inputType : "text",
                	topClass : "hide" 
                },
                //"rules" : {"required":true,"maxlength" : 200},
                onText : { label : "Texte de la Valeur Oui", inputType : "text", topClass : "hide" },
                offText : { label : "Texte de la Valeur Non", inputType : "text", topClass : "hide" },
                initValue : {
	                inputType : "checkboxSimple",
                    label : "Valeur d'ouverture",
                    subLabel : "Valeur d'ouverture",
                    params : {
                        onText : "Oui",offText : "Non",onLabel : "Oui",offLabel : "Non",labelText : "Valeur d'ouverture"
                    },
                    checked : true,
                    topClass : "hide"
	            },
	            labelKey : { label : "Label pour la clef", inputType : "text", topClass : "hide" },
                labelValue : { label : "Label pour la valeur", inputType : "text", topClass : "hide" }

	        },
	        save : function (formData) {
	        	mylog.log('save tplCtx formData', formData)

	        	delete formData.collection ;
	        	delete formData.id ;

	            tplCtx.value = {};
	            tplCtx.value.label = formData.label;
	            tplCtx.value.inputType = formData.inputType;

	            if(formData.subLabel)
	            	tplCtx.value.subLabel = formData.subLabel;

	            if(formData.info)
	            	tplCtx.value.info = formData.info;

	            if(formData.placeholder)
	            	tplCtx.value.placeholder = formData.placeholder;

	             if(formData.required ) {
	            	tplCtx.value.rules = {};
	            	tplCtx.value.rules.required = formData.required;
	            }
	            //alert(tplCtx.value.inputType);

	            // TYPE SELECT && RADIO
	            // ****************************
	            if( $.inArray( tplCtx.value.inputType , [ "select","radio"] )>=0 ) {
	            	tplCtx.value.options = {};
	            	//alert(tplCtx.value["list[]"].length);
	            	$.each( formData["list[]"], function(i,v) { 
	            		tplCtx.value.options[ slugify(v) ] = v;
	            	} );		            
	            }
	            // TYPE TAGS and ARRAY
	            // ****************************
	            if( $.inArray( tplCtx.value.inputType , [ "tags", "array" ] ) >=0 ){
            		tplCtx.value.values = [];
            		if(formData["list[]"]){
		            	$.each( formData["list[]"], function(i,v) { 
		            		tplCtx.value.values.push(v);
		            	} );
		            }
            	}

	            // TYPE FINDER
	            // ****************************
	            if( tplCtx.value.inputType == "finder" ) {
	            	if(notNull(formData["list[]"]))
		            	tplCtx.value.roles = ($.isArray(formData["list[]"])) ? formData["list[]"] : [formData["list[]"] ];
		            
		            tplCtx.value.initType = [ "persons",  "organizations","projects", "events"];
		            if(notNull(formData["finderTypes"])){
		            	tplCtx.value.initType = ($.isArray(formData.finderTypes)) ? formData.finderTypes : [formData.finderTypes ];	
		            }
	            	tplCtx.value.multiple = parseBool( formData.multiple );
	            	tplCtx.value.invite = parseBool( formData.invite );
	            	tplCtx.value.openSearch = parseBool( formData.openSearch );
	            }

	            // TYPE TEXTAREA 
	            // ****************************
	            if( $.inArray( tplCtx.value.inputType , [ "textarea","tags"] )>=0 && $("#ajaxFormModal #maxlength").val() != "" ) {
	            	if(tplCtx.value.inputType == "textarea"){
		            	if(typeof tplCtx.value.rules != "undefined")
		            		tplCtx.value.rules = {};
		            	tplCtx.value.rules.maxlength = $("#ajaxFormModal #maxlength").val();
		            } 
	            	else if (tplCtx.value.inputType == "tags"){
		            	// TYPE TAGS
			            // ****************************
			            tplCtx.value.minimumInputLength = ($("#ajaxFormModal #maxlength").val() != "") ? $("#ajaxFormModal #maxlength").val() : 3
	            	}
	            }

	            // TYPE CHECKBOX
	            // ****************************
	            if( tplCtx.value.inputType == "checkboxSimple" ){
	            	tplCtx.value.params = {
					    onText : formData.onText,
					    offText : formData.offText,
					    onLabel : formData.onText,
					    offLabel : formData.offText,
					    labelText : formData.label
					};
	            	tplCtx.value.checked = parseBool(formData.initValue);
	            }

	            if( tplCtx.value.inputType == "array" )
	            	tplCtx.value.value = [""];

	            if( tplCtx.value.inputType == "properties" ){
	            	tplCtx.value.values = {"test":""};
	            	tplCtx.value.labelKey = formData.labelKey;
	            	tplCtx.value.labelValue = formData.labelValue;
	            }
	            // TYPE FORMLOCATION
	            // ****************************
	            // TYPE IMAGE
	            // ****************************
	            	

	            mylog.log("save tplCtx",tplCtx);
	            
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                   urlCtrl.loadByHash(location.hash);
	                } );
	            }

	    	}
	    }
	};


    mylog.log("render","/modules/costum/views/tpls/forms/cplx/<?php echo $kunik ?>.php");

    //adds a line into answer
    $( ".add<?php echo $kunik ?>" ).off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
    });

    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
    });

    $(".edit<?php echo $dynForm["id"]?>Dynform").off().click(function() { 
    	tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        smallMenu.open( $("#form-buildDynform<?php echo $dynForm["id"]?>").html(),"bootbox",null,function() { 
	        
	        $(".addInputDynform_<?php echo $dynForm["id"]?>").off().click(function() { 
				tplCtx.path = "inputs.q<?php echo "_".time()."_".count( $properties ); ?>";
		  		mylog.log("addInput tplCtx",tplCtx);
				dyFObj.openForm( sectionDyf.newDFInput<?php echo $dynForm["id"]?> );
			});

			$(".editDynformInput").off().on("click",function() {  
		        tplCtx.id = "<?php echo $dynForm["_id"]?>";
		        tplCtx.collection = "<?php echo Form::COLLECTION;?>"; 
		        tplCtx.path = $(this).data("path");
		        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
		    });

			$(".deleteInput").off().on("click",function() {  
		        tplCtx.id = "<?php echo $dynForm["_id"]?>";
		        tplCtx.collection = "<?php echo Form::COLLECTION;?>"; 
		        tplCtx.path = $(this).data("path");
		        tplCtx.inputId = $(this).data("inputid");
		        tplCtx.value = null;
		        mylog.log('deleteInput tplCtx', tplCtx)

		        if($('li#input'+tplCtx.inputId).hasClass('bg-red')){
					dataHelper.path2Value( tplCtx, function(params) { 
			           $('li#input'+tplCtx.inputId).slideUp();//urlCtrl.loadByHash(location.hash);
			        } );
		        } else
		        	$('li#input'+tplCtx.inputId).addClass('bg-red');
		        
		    });

		});

        //on pourrait utiliser DF directement en changeant les btn save et ajoutant edit et suppr
		//dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")],null,null,"build");
	});

	


	

	
	



});
</script>
<?php } 


}

?>