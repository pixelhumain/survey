<?php 
$value = (!empty($answer) && isset($answer["answers"][$key]) && $answer["answers"][$key] ) ? "selected" : "";
$inpClass = "";
if($saveOneByOne)
    $inpClass = " saveOneByOne"; 

$paramsData = [ "options" => [ ] ];

if( isset($parentForm["params"][$key]) ) 
    $paramsData =  $parentForm["params"][$key];

$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$key."' class='previewTpl edit".$key."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";
?>

<div class="form-group">
	<label for="<?php echo $key ?>"><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn ?></h4></label>

    
            <select class="form-control select2Input <?php echo $inpClass ?>" id="<?php echo $key ?>" data-form='<?php echo $form["id"] ?>' required>
            	<option value="">Selectionner</option>
            	<option value="">un</option>
                <option value="">deu</option>
                <option value="">troi</option>
            </select>
            <select class="form-control select2Input <?php echo $inpClass ?>" id="<?php echo $key ?>" data-form='<?php echo $form["id"] ?>' required>
                <option value="">Selectionner</option>
                <option value="">un</option>
                <option value="">deu</option>
                <option value="">troi</option>
            </select>
	<?php 
    if(!empty($info)){ ?>
    	<small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
    <?php } ?>
</div>
<script type="text/javascript">
sectionDyf.<?php echo $key ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
jQuery(document).ready(function() {
    mylog.log("render form input","/modules/costum/views/tpls/forms/select.php");

    sectionDyf.<?php echo $key ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo $key ?> config",
            "description" : "Liste de question possible",
            "icon" : "fa-cog",
            "properties" : {
                options : {
                    inputType : "array",
                    label : "Liste des titres",
                    values :  sectionDyf.<?php echo $key ?>ParamsData.options
                }
            },
            save : function () {  
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $key ?>Params.jsonSchema.properties , function(k,val) { 
                    if(val.inputType == "array")
                        tplCtx.value[k] = getArray('.'+k+val.inputType);
                    else
                        tplCtx.value[k] = $("#"+k).val();
                 });
                mylog.log("save tplCtx",tplCtx);
                
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
                        urlCtrl.loadByHash(location.hash);
                    } );
                }

            }
        }
    };

    $(".edit<?php echo $key ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $key ?>Params,null, sectionDyf.<?php echo $key ?>ParamsData);
    });
});
</script>
