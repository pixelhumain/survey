<?php 
$styleT =  ( ($mode == "pdf") ? " border : 1px solid black ;" : "");
//var_dump($styleT);
$colspanplus  = (($mode == "r" || $mode == "pdf") ? 0 : 2);
?>
<div class="form-group">

	<?php 
	if($mode == "r" || $mode == "pdf"){ ?>
		<label ><h4 style="color:<?php echo (!empty($titleColor) ? $titleColor : "black" ); ?>"><?php echo $label ; ?></h4></label>
    <?php echo $info ?>
	<?php 
	} else {
		?>
		<label ><h4 style="color:<?php echo (!empty($titleColor) ? $titleColor : "black" ); ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL ; ?></h4></label>
		<?php echo $info ?>
	<?php 
	} ?>
	<table class="table table-bordered table-hover  directoryTable"  id="<?php echo $kunik ; ?>" 
		 >
		
	<thead>	
		<?php 		
		if( count($answers)>0 ){ ?>
		<tr>
			<?php 
			
			foreach ($properties as $i => $inp) {
				echo "<th>".$inp["placeholder"]."</th>";
			} ?>

			<?php 
			if($mode != "r" && $mode != "pdf"){ ?>
				<th></th>
			<?php } ?>
		</tr>
		<?php } ?>
	</thead>
	<tbody class="directoryLines">	
		<?php 
		$ct = 0;
		
		if(isset($answers)){
			foreach ($answers as $q => $a) {

				$tds = "";
				foreach ($properties as $i => $inp) {
					$tds .= "<td>";

					if(isset($a[$i]))
						$tds .= $a[$i];
					
					$tds .= "</td>";
				}

				
					echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
						echo $tds;
						if($mode != "r" && $mode != "pdf"){
					?>
						<td>
							<?php 
							//echo $answerPath.$q;
								echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
									"canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
									"id" => $answer["_id"],
									"collection" => Form::ANSWER_COLLECTION,
									"q" => $q,
									"path" => $answerPath.$q,
									"kunik"=>$kunik ] ); ?>

							<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
						</td>
				<?php 
						}
					$ct++;
					echo "</tr>";
				
			}
		}
	

?>
		</tbody>
	</table>
</div>