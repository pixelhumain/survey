<?php if($answer){ 
	$keyTpl = "depnseHabitat";
	$kunik = $keyTpl.$key;
	?>
<div class="form-group">
	<table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>">
		
<?php 
$editBtnL = ($canEdit === true) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='answers.".$kunik.".' class='add".$keyTpl." btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";

$editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$keyTpl."' class='previewTpl edit".$keyTpl."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

$paramsData = [ "nature" => [
		                        "investissement" => "Investissement",
		                        "fonctionnement" => "Fonctionnement"
		                    ] ];

if( isset($parentForm["params"][$keyTpl]) ) 
	$paramsData =  $parentForm["params"][$keyTpl];

$properties = [
         "nature" => [
            "placeholder" => "Nature de l’action",
            "inputType" => "select",
            "options" => $paramsData["nature"],
            "rules" => [ "required" => true ]
        ],
        "poste" => [
            "inputType" => "text",
            "label" => "Poste de dépense",
            "placeholder" => "Poste de dépense",
            "rules" => [ "required" => true  ]
        ],
        "amount2019" => [
            "inputType" => "text",
            "label" => "2019 (euros HT)",
            "placeholder" => "2019 (euros HT)",
            "rules" => [ "required" => true, "number" => true ]
        ],
        "amount2020" => [
            "inputType" => "text",
            "label" => "2020 (euros HT)",
            "placeholder" => "2020 (euros HT)",
            "rules" => [ "required" => true, "number" => true ]
        ],
        "amount2021" => [
            "inputType" => "text",
            "label" => "2021 (euros HT)",
            "placeholder" => "2021 (euros HT)",
            "rules" => [ "number" => true  ]
        ],
        "amount2022" => [
            "inputType" => "text",
            "label" => "2022 (euros HT)",
            "placeholder" => "2022 (euros HT)",
            "rules" => [ "number" => true  ]
        ]
    ];
?>	
	<thead>
		<tr>
			<td colspan='<?php echo count( $properties)+2?>' ><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
				<?php echo $info ?>
			</td>
		</tr>	
		<?php if(isset($answers) && count($answers)>0){ ?>
		<tr>
			</th>
			<?php 
			
			foreach ($properties as $i => $inp) {
				echo "<th>".$inp["placeholder"]."</th>";
			} ?>
			<th></th>
		</tr>
		<?php } ?>
	</thead>
	<tbody class="directoryLines">	
		<?php 
		$ct = 0;
		
		if(isset($answers)){
			foreach ($answers as $q => $a) {

				echo "<tr id='".$keyTpl.$q."' class='".$keyTpl."Line'>";
				foreach ($properties as $i => $inp) {
					echo "<td>";
					if(isset($a[$i])) {
						if(is_array($a[$i]))
							echo implode(",", $a["role"]);
						else
							echo $a[$i];
					}
					echo "</td>";
				}
					
			?>
			<td>
				<?php 
					echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
						"canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
						"id" => $answer["_id"],
						"collection" => Form::ANSWER_COLLECTION,
						"q" => $q,
						"path" => "answers.".$kunik.".".$q,
						"keyTpl"=>$keyTpl
						] );
					?>
				<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
			</td>
			<?php 
				$ct++;
				echo "</tr>";
			}
		}

$totalMap = [];
foreach ( $properties as $i => $inp ) {
	if(stripos($i, "amount") !== false )
		$totalMap[$i] = 0;
}

if(isset($answers)){
	foreach ( $answers as $q => $a ) {	
		foreach ($totalMap as $i => $tot) {
			if(isset($a[$i]))
				$totalMap[$i] = $tot + $a[$i];
		}
	}
}

$total = 0;
foreach ( $totalMap as $i => $tot ) {
	if( $tot != 0 )
		$total = $total + $tot ;
}

if($total > 0){

	echo "<tr class='bold'>";
		echo "<td></td>";
		echo "<td>TOTAL : </td>";
		foreach ($totalMap as $i => $tot) {
			if( $tot != 0 )
				echo "<td>".(($tot == 0) ? "" : trim(strrev(chunk_split(strrev($tot),3, ' ')))."€")."</td>";
		}
		echo "<td></td>";
	echo "</tr>";

	echo "<tr class='bold'>";
	echo 	"<td colspan='5' style='text-align:right'>BUDGET TOTAL : </td>";
	echo 	"<td colspan='2'>".trim(strrev(chunk_split(strrev($total),3, ' ')))." €</td>";
	echo "</tr>";

	Yii::app()->session["totalBudget"] = $total;

}

	

?>
		</tbody>
	</table>
</div>
<script type="text/javascript">

var <?php echo $keyTpl ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

$(document).ready(function() { 
	
	sectionDyf.<?php echo $keyTpl ?> = {
		"jsonSchema" : {	
	        "title" : "Budget prévisionnel",
            "icon" : "fa-money",
            "text" : "Décrire ici les principaux postes de dépenses : à quoi correspondent les coûts ? <br/>Préciser ce qui relève des dépenses de fonctionnement et des dépenses d’investissement.<br/>Les coûts doivent être en <b>hors taxe</b>.",
	        "properties" : <?php echo json_encode( $properties ); ?>,
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $keyTpl ?>.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    urlCtrl.loadByHash(location.hash);
	                } );
	            }

	    	}
	    }
	};

	sectionDyf.<?php echo $keyTpl ?>Params = {
		"jsonSchema" : {	
	        "title" : "<?php echo $keyTpl ?> config",
	        "description" : "Liste de question possible",
	        "icon" : "fa-cog",
	        "properties" : {
	            nature : {
	                inputType : "properties",
	                labelKey : "Clef",
	                labelValue : "Label affiché",
	                label : "Liste des natures possibles",
	                values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.nature
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
	        		if(val.inputType == "array")
	        		 	tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
	        		else
	        		 	tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    urlCtrl.loadByHash(location.hash);
	                } );
	            }

	    	}
	    }
	};


    mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $keyTpl ?>.php");

    //adds a line into answer
    $(".add<?php echo $keyTpl ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path")+((notNull(<?php echo $keyTpl ?>Data) ? <?php echo $keyTpl ?>Data.length : "0"));
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?> );
    });

    $(".edit<?php echo $keyTpl ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>,null, <?php echo $keyTpl ?>Data[$(this).data("key")]);
    });

    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        //if no params config on the element.costum.form.params.<?php echo $keyTpl ?>
        //then we load default values available in forms.inputs.<?php echo $keyTpl ?>xxx.params
        //mylog.log(".editParams",sectionDyf.<?php echo $keyTpl ?>Params,calData);
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
    });

    
});
</script>
<?php } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>