<style type="text/css">
    .form-control{
        padding: .5em !important;
    }
</style>

<?php if($answer){ ?>

    <div class="col-md-6">

        <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
            <?php echo $label.$editQuestionBtn; ?>
        </h4>

        <?php echo $info ?>

        <?php 
            $name = "";

            if(!empty($answer["answers"][$form["id"]]["name"])){
                $name = $answer["answers"][$form["id"]]["name"];
            }else if($answer["user"]==$_SESSION["userId"]){
                $name = $_SESSION["user"]["name"];
            }else{
                $name = "";
            }

        ?>

        <br>
        
        <div class="form-group">
            <?php if($mode!="r"){ ?>
                <input type="text" id="<?= $key ?>" data-form="<?= $form["id"]?>" value="<?php echo $name ?>" class="form-control saveOneByOne" required>
            <?php }else{ 
                echo $name;
             } ?>
        </div>
    </div>

    <script type="text/javascript">

        $(document).ready(function(){
            let kunik = "<?= $kunik ?>";
             $("#<?= $key ?>").blur();
            
            //adds a line into answer
            $(".add<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
            });

            $(".edit<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
            });
        });

    </script>
    <?php } else {
        //echo "<h4 class='text-red'>evaluation works with existing answers</h4>";
    } ?>