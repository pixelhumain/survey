<style>
    /** Field set */
    fieldset.quadrant {
        border: 2px solid #eeeeee;
        display: block;
        margin-top: 1em;
        margin-bottom: 2em;
        padding-top: 0.2em;
        padding-bottom: 0.4em;
        padding-left: 0.5em;
        padding-right: 0.5em;

        display: flex; 
        flex-direction: column-reverse;
    }

    fieldset.height-x{
        max-height: 400px;
        height: 400px;
        overflow-y: auto !important;
        overflow-x: hidden;
    }

    fieldset.height-y{
        max-height: 400px;
        overflow-y: auto !important;
        overflow-x: hidden;
    }

    fieldSet.height-center{
        max-height: 300px;
        height: 300px;
        overflow-y: auto !important;
        overflow-x: hidden;
    }

    fieldset.quadrant legend {
        color:white;
        width:auto;
        font-size: 12pt;
        font-weight: bolder !important;
        padding: 5px 10px;
        border-radius: 2px;
    }

    label.toggle{
        position: relative;
        cursor: pointer;
        color: #666;
    }

    label.toggle span{
        font-size: 18px;
    }

    input[type="checkbox"], input[type="checkbox"]{
        position: absolute;
        right: 9000px;
    }

    /*checkbox Toggle*/
    .toggle input[type="checkbox"] + .label-text:before{
        content: "\f204";
        font-size: 25px;
        font-family: "FontAwesome";
        speak: none;
        font-style: normal;
        font-weight: normal;
        font-variant: normal;
        text-transform: none;
        line-height: 1;
        -webkit-font-smoothing:antialiased;
        width: 1em;
        display: inline-block;
        margin-right: 10px;

    }

    .toggle input[type="checkbox"]:checked + .label-text:before{
        content: "\f205";
        color: #16a085;
        animation: effect 250ms ease-in;
    }

    .toggle input[type="checkbox"]:disabled + .label-text{
        color: #aaa;
    }

    .toggle input[type="checkbox"]:disabled + .label-text:before{
        content: "\f204";
        color: #ccc;
    }

    @keyframes effect{
        0%{transform: scale(0);}
        25%{transform: scale(1.3);}
        75%{transform: scale(1.4);}
        100%{transform: scale(1);}
    }

    .active-users{ 
        border-radius:50px !important;
    }

    .mb-2{
        margin-bottom: 1em;
    }

    .pl-0{
        padding-left: 0em;
    }

    .pr-0{
        padding-right: 0em;
    }

    .centered-content { 
        margin: auto; 
        max-width: 300px;
    }
</style>

<?php if($answer){ ?>

    <div class="form-group">
        <?php
            # Initialize parameters data
            $paramsData = [
                "objet" => "A modifier dans la paramètrage",
                "objectif" => "A modifier dans la paramètrage", 
                "quadrants" => [
                    "quadrant0" => [
                        "label" => "Ce qui est obsolète", 
                        "description" => "C’est dépassé, périmé, derrière eux, n’est ples d’actualité,…", 
                        "inputNb"=>2
                    ],
                    "quadrant1" => [
                        "label" => "Ce qui est valide", 
                        "description" => "Ce qui fonctionne aujourd’hui dans leur management, c’est fiable, c’est efficace pour tous, intégré…", 
                        "inputNb"=>2
                    ],
                    "quadrant2" => [
                        "label" => "Ce qui est anticipé", 
                        "description" => "En émergence, ce sont toutes les idées, propositions, pas suffisamment construit, état embryonaire, avant expérimentation, pas concret …", 
                        "inputNb"=>2
                    ],
                    "quadrant3" => [
                        "label" => "Ce qui est innovant", 
                        "description" => "Ce que vous avez initié de nouveau ces derniers temps, ce que vous avez expérimenté de différent d’avant…)", 
                        "inputNb"=>2
                    ]
                ],
                "isCollectif"=>false,
                "deleteOthersProposition"=>"false",
                "enableSteps"=>true,
                "activeStep"=>"Brainstorm",
                "transformationQuadrants" => [
                    "quadrantTransform0" => [
                        "label" => "CRÉER", 
                        "type" => "textarea", 
                        "description" => "À Créer",
                        "inputNb"=>1
                    ],
                    "quadrantTransform1" => [
                        "label" => "QUITTER", 
                        "type" => "textarea",
                        "description" => "À Quitter", 
                        "inputNb"=>1
                    ],
                    "quadrantTransform2" => [
                        "label" => "INTRODUIRE", 
                        "type" => "textarea", 
                        "description" => "À Introduire", 
                        "inputNb"=>1
                    ],
                    "quadrantTransform3" => [
                        "label" => "GARDER", 
                        "type" => "textarea", 
                        "description" => "À Garder", 
                        "inputNb"=>1
                    ]
                ],
            ];

            # Set parameters data
            if( isset($parentForm["params"][$kunik]) ){
                foreach ($paramsData as $e => $v) {
                    if (  isset($parentForm["params"][$kunik][$e]) ) {
                        $paramsData[$e] = $parentForm["params"][$kunik][$e];
                    }
                }
            }

            $enableSteps = $paramsData["enableSteps"];

            # Get answers if exist
            # !!!  Warning for update !!! 
            # The answers here are the quadrants, If you want add other input (no quadrant) you should edit the code below

            // remove the to test directily (dev) "draft" => ["$exists" => false ]]
            $creatorId = ($paramsData["isCollectif"]=="false")?Yii::app()->session["userId"]:$answer["user"];
            
            $theAnswers = PHDB::find("answers", array("user"=>$creatorId, "form"=>$parentForm["_id"]->{'$id'}, "draft" => ['$exists' => false ]));
            
            $myAnswers = reset($theAnswers);
            //$myAnswers = PHDB::find("answers", array("context.".$this->costum['contextId'].".name"=>$this->costum['name'], "user" => Yii::app()->session["userId"]));
            if(!empty($myAnswers["answers"][$form["id"]][$kunik])){
                $myEvaluation = $myAnswers["answers"][$form["id"]][$kunik];
            }else{
                $myEvaluation = [];
            }

            $editParamsBtn = "";

            if($form["creator"]==Yii::app()->session["userId"]){
                $editParamsBtn = ($editQuestionBtn!="") ? " <a href='javascript:;' 
                                                    data-id='".$parentForm["_id"]."' 
                                                    data-collection='".Form::COLLECTION."' 
                                                    data-path='params.".$kunik."' 
                                                    class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'>
                                                    <i class='fa fa-cog'></i> 
                                                </a>" : "";
            }

            $qCount = 0;
            foreach ($paramsData["transformationQuadrants"] as $tQuadrant => $tValue) {
                ${"tQuadrant".$qCount} = $tQuadrant;
                ${"tValue".$qCount} = $tValue;
                ${"tText".$qCount} = "";
                if(isset($myEvaluation[$tQuadrant]["evaluation".$qCount])){
                    //var_dump($myEvaluation[$tQuadrant]);
                    //var_dump($tValue);
                    ${"tText".$qCount} = $myEvaluation[$tQuadrant]["evaluation".$qCount];
                }
                $qCount++;
            }
        ?>

        <h4 class="padding-20" style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
            <?php echo $label.$editQuestionBtn.$editParamsBtn.'<span id="active-users" class="pull-right"></span>'; ?>
        </h4>

        <?php echo $info ?>

        <br>
        <div id="quadrantInputs<?= $kunik ?>" class="form-horizontal">
            <?php if($paramsData["objectif"]!=""){ ?>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="objectif<?= $kunik ?>">OBJECTIF :</label>
                    <div class="col-sm-10">
                        <label><?php echo $paramsData["objectif"]?></label>
                    </div>
                </div>
            <?php } ?>
            <?php if($paramsData["objet"]!=""){ ?>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="objet<?= $kunik ?>">OBJET :</label>
                    <div class="col-sm-10">
                        <label><?php echo $paramsData["objet"] ?></label>
                    </div>
                </div>
            <?php } ?>
            <?php if( ($form["creator"]==Yii::app()->session["userId"]) && $paramsData["enableSteps"]=="true"){ ?>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="objet<?= $kunik ?>">ÉTAPE :</label>
                    <div class="col-sm-10">
                        <button class="btn <?php echo ($paramsData['activeStep']=='Brainstorm')?'bg-primary':'' ?> btn-step" data-path="<?='params.'.$kunik.'.activeStep'?>" data-id='<?=$parentForm["_id"]?>' data-collection='<?=Form::COLLECTION?>' data-step="Brainstorm">
                            Brainstorm
                        </button> &nbsp;

                        <button class="btn <?php echo ($paramsData['activeStep']=='Transformation')?'bg-primary':'' ?> btn-step" data-path="<?='params.'.$kunik.'.activeStep'?>" data-id='<?=$parentForm["_id"]?>' data-collection='<?=Form::COLLECTION?>' data-step="Transformation">
                            Transformer
                        </button>
                    </div>
                </div>
            <?php } ?>
            <div class="container-fluid">
                <?php if($enableSteps == "true" && isset($tValue0["label"]) && $paramsData["activeStep"]=="Transformation"){ ?>
                    <div class="row">
                        <div class="col-xs-4"></div>
                        <div class="col-xs-4">
                            <fieldset class="quadrant height-y">
                                <?php if(isset($tValue0["label"])){ ?>
                                    <legend class="bg-primary"><?= $tValue0["label"] ?> :</legend>
                                <?php } ?>
                                <div>
                                    <div id="<?= $tQuadrant0 ?>Inputs<?= $kunik ?>">
                                        <textarea 
                                            id="<?=$kunik?>.<?=$tQuadrant0?>.evaluation0" 
                                            data-form='<?php echo $form["id"] ?>' 
                                            data-quadrant='transformation0'
                                            rows="5"
                                            class="form-control saveOneByOne inputEvaluation <?=$tQuadrant0?>-evaluation0" rows="5"><?= (isset($tText0))?$tText0:''; ?></textarea>
                                    </div>
                                    <!--a role="button" class="btn btn-default btn-block bg-light addEvaluation<?= $kunik ?> btn<?=$kunik?><?=$tQuadrant0?>" data-step="transformation" data-quadrant="<?= $tQuadrant0 ?>"><i class="fa fa-plus"></i> ajouter une case</a-->
                                </div>
                                <?php if(isset($tValue0["description"])){ ?>
                                    <div class="mb-2"><?= $tValue0["description"] ?></div>
                                <?php } ?>
                            </fieldset>
                        </div>
                        <div class="col-xs-4"></div>
                    </div>
                <?php } ?>

                <div class="row">
                    <?php if($enableSteps == "true" && isset($tValue1["label"]) && $paramsData["activeStep"]=="Transformation"){ ?>
                    <div class="col-xs-3">
                        <fieldset class="quadrant height-x">
                            <legend class="bg-primary"><?= $tValue1["label"] ?> :</legend>
                            <div>
                                <div id="<?= $tQuadrant1 ?>Inputs<?= $kunik ?>">
                                    <textarea 
                                    id="<?=$kunik?>.<?=$tQuadrant1?>.evaluation1"
                                    data-form='<?php echo $form["id"] ?>' 
                                    data-quadrant='transformation1' 
                                    rows="15" 
                                    class="form-control saveOneByOne inputEvaluation <?=$tQuadrant1?>-evaluation1"><?= (isset($tText1))?$tText1:''; ?></textarea>
                                </div>
                                <!--a role="button" class="btn btn-default btn-block bg-light addEvaluation<?= $kunik ?> btn<?=$kunik?><?=$tQuadrant1?>" data-step="transformation" data-quadrant="<?= $tQuadrant1 ?>"><i class="fa fa-plus"></i> ajouter une case</a-->
                            </div>
                            <?php if(isset($tValue1["description"])){ ?>
                                <div class="mb-2"><?= $tValue1["description"] ?></div>
                            <?php } ?>
                        </fieldset>
                    </div>
                    <?php } ?>

                    <div class="<?php echo ($paramsData["activeStep"]=='Transformation')?'col-xs-6':'col-xs-12'; ?>">
                        <?php foreach ($paramsData["quadrants"] as $quadrant => $value) {  ?>
                            <div class="col-xs-6">
                                <fieldset class="quadrant height-center">
                                    <?php if(isset($value["label"])){ ?>
                                    <legend class="bg-dark"><?= $value["label"] ?> :</legend>
                                    <?php } ?>
                                    <div>
                                        <div id="<?= $quadrant ?>Inputs<?= $kunik ?>"></div>
                                        <a role="button" class="btn btn-default btn-block bg-light addEvaluation<?= $kunik ?> btn<?=$kunik?><?=$quadrant?>" data-step="brainstorm" data-quadrant="<?= $quadrant ?>"><i class="fa fa-plus"></i> ajouter une case</a>
                                    </div>
                                    <?php if(isset($value["description"])){ ?>
                                        <div class="mb-2"><?= $value["description"] ?></div>
                                    <?php } ?>
                                </fieldset>
                            </div>
                        <?php  } ?>
                    </div>

                    <?php if($enableSteps == "true" && isset($tValue2["label"]) && $paramsData["activeStep"]=="Transformation"){ ?>
                    <div class="col-xs-3">
                        <fieldset class="quadrant height-x">
                            <legend class="bg-primary"><?= $tValue2["label"] ?> :</legend>
                            <div>
                                <div id="<?= $tQuadrant2 ?>Inputs<?= $kunik ?>">
                                    <textarea data-form='<?php echo $form["id"] ?>' 
                                data-quadrant='transformation2'
                                 rows="15"
                                id="<?=$kunik?>.<?=$tQuadrant2?>.evaluation2" 
                                class="form-control saveOneByOne inputEvaluation <?=$tQuadrant2?>-evaluation2"><?= (isset($tText2))?$tText2:''; ?></textarea>
                                </div>
                                <!--a role="button" class="btn btn-default btn-block bg-light addEvaluation<?= $kunik ?> btn<?=$kunik?><?=$tQuadrant2?>" data-step="transformation" data-quadrant="<?= $tQuadrant2 ?>"><i class="fa fa-plus"></i> ajouter une case</a-->
                            </div>
                            <?php if(isset($tValue2["description"])){ ?>
                                <div class="mb-2"><?= $tValue2["description"] ?></div>
                            <?php } ?>
                        </fieldset>
                    </div>
                    <?php } ?>

                </div>
                <?php if($enableSteps == "true" && isset($tValue3["label"]) && $paramsData["activeStep"]=="Transformation"){ ?>
                <div class="row">
                    <div class="col-xs-4"></div>
                    <div class="col-xs-4">
                        <fieldset class="quadrant height-y">
                            <legend class="bg-primary"><?= $tValue3["label"] ?> :</legend>
                            <div>
                                <div id="<?= $tQuadrant3 ?>Inputs<?= $kunik ?>">
                                    <textarea data-form='<?php echo $form["id"] ?>' 
                                data-quadrant='transformation3'
                                rows="5"
                                id="<?=$kunik?>.<?=$tQuadrant3?>.evaluation3" 
                                class="form-control saveOneByOne inputEvaluation <?=$tQuadrant3?>-evaluation3"><?= (isset($tText3))?$tText3:''; ?></textarea>
                                </div>
                                <!--a role="button" class="btn btn-default btn-block bg-light addEvaluation<?= $kunik ?> btn<?=$kunik?><?=$tQuadrant3?>" data-step="transformation" data-quadrant="<?= $tQuadrant3 ?>"><i class="fa fa-plus"></i> ajouter une case</a-->
                            </div>
                            <?php if(isset($tValue3["description"])){ ?>
                                <div class="mb-2"><?= $tValue3["description"] ?></div>
                            <?php } ?>
                        </fieldset>
                    </div>
                    <div class="col-xs-4"></div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

        var inputNb = {};
        var kunik = "<?= $kunik ?>";
        
        $("#quadrantInputs<?= $kunik ?>").ready(function(){

        <?php if($paramsData["isCollectif"]=="true"){ ?>
            var wsEvaluation<?php echo $kunik ?> = null;
            
            if(userId && coWsConfig.enable){
                wsEvaluation<?php echo $kunik ?> = io(coWsConfig.serverUrl, {
                    query:{
                        userId: userId,
                        isEvaluation: true,
                        room: kunik
                    }
                });
                
                wsEvaluation<?php echo $kunik ?>.on('new-evaluation', function(data){
                    let itIsMe = (data.emeter==currentUser.name);
                    let quadrant =  data.target.split(".");
                    
                    if(!itIsMe && !data.userMarker){
                        if($("."+quadrant[1]+"-"+quadrant[2]).length == "1"){
                            if(data.value==""){
                                $("."+quadrant[1]+"-"+quadrant[2]).parent().parent().remove();
                            }else{
                                $("."+quadrant[1]+"-"+quadrant[2]).css({"border":"2px solid #ccc"});
                                $("."+quadrant[1]+"-"+quadrant[2]).val(data.value);
                            }
                            if($("."+quadrant[1]+"-"+quadrant[2]).val()!=data.value){
                                toastr.info(data.emeter+" a modifié une évaluation");
                            }
                        }else{
                            inputNb[quadrant[1]]++;

                            let input = `<div class="form-group" id="${quadrant[1]}Input${inputNb[quadrant[1]]}">
                                <div class="${(itIsMe)?"col-xs-10":"col-sm-12"}">
                                    <input type="text" 
                                        data-form='<?php echo $form["id"] ?>' 
                                        value = '${data.value}'
                                        data-quadrant='${"evaluation"+inputNb[quadrant[1]]}'
                                        id="${data.target}" 
                                        class="form-control saveOneByOne inputEvaluation user-border-color ${quadrant[1]}-${quadrant[2]}" >
                                </div>`;
                            
                            if(itIsMe){
                                input += `<div class="col-md-2 col-sm-1 col-xs-2 text-right">
                                    <a role="button" 
                                        class="btn btn-danger remove${kunik} removeEvaluation" 
                                        data-code="${inputNb[quadrant[1]]}" 
                                        data-target="${data.target}" 
                                        data-quadrant="${quadrant[1]}">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </div>`
                            }
                            input+='</div>';
                            $("#"+quadrant[1]+"Inputs<?= $kunik ?>").append(input);
                            toastr.info(data.emeter+" a ajouté une évaluation");   
                        } 
                    }

                    if($("."+quadrant[1]+"-"+quadrant[2]).length == "1"){
                        let markerColor = (data.userMarker)?"#"+data.userId.substring(0, 6):"#ccc";
                        $("."+quadrant[1]+"-"+quadrant[2]).css({"border":"2px solid "+markerColor});
                    }

                    if($("#user"+data.userId).length==0){
                        $("#active-users").append(`
                            <span id="user${data.userId}" title="${data.emeter}">
                                ${(data.profilImageUrl && data.profilImageUrl!="")?'<img src="'+data.profilImageUrl+'" height="50" width="50"  class="active-users" style="border: 3px solid #'+data.userId.substring(0, 6)+'; object-fit:cover;">':'<span class="fa fa-user" style="border: 3px solid #'+data.userId.substring(0, 6)+'; color:#'+data.userId.substring(0, 6)+'; padding:13px 15px; border-radius: 50px;"></span>'}</span>
                        `);

                        toastr.info(data.emeter+" rejoint le canal d'évaluation");
                    }
                });
            }
        <?php } ?>


            let evaluations = <?php echo json_encode( $myEvaluation ); ?>;
            let evaluationQuadrants = {};
            evaluationQuadrants["brainstorm"] = <?php echo json_encode( $paramsData["quadrants"] ); ?>;
            evaluationQuadrants["transformation"] = <?php echo json_encode( $paramsData["transformationQuadrants"] ); ?>;
            let options = [];
            
            createQuadrant(evaluationQuadrants["brainstorm"]);
            //createQuadrant(evaluationQuadrants["transformation"]);

            $("#quadrantInputs<?= $kunik ?>").on("click", '.removeEvaluation', function(event){
                event.preventDefault();
                $("."+$(this).data("target")).val("").blur();
                $("#"+$(this).data("quadrant")+"Input"+$(this).data("code")).hide();
            });

        
            $("#quadrantInputs<?= $kunik ?>").on("blur", '.inputEvaluation', function(event){
                event.preventDefault();
                $(this).css({"border":"2px solid #ccc"});

                <?php if($paramsData["isCollectif"]=="true"){ ?>
                    if(coWsConfig.enable){
                        wsEvaluation<?php echo $kunik ?>.emit("new-evaluation", {
                            userId: userId,
                            emeter: currentUser.name,
                            quadrant:$(this).data("quadrant"),
                            target:$(this).attr("id"),
                            value:$(this).val()
                        });
                    }
                <?php } ?>

                if($(this).val()==""){
                    if(!$(this).is("textarea")){
                        $(this).parent().parent().remove();
                    }
                }
            });

        <?php if($paramsData["isCollectif"]=="true"){ ?>
            $("#quadrantInputs<?= $kunik ?>").on("click", '.inputEvaluation', function(event){
                event.preventDefault();
                if($(this).val()!="" && coWsConfig.enable){
                    wsEvaluation<?php echo $kunik ?>.emit("new-evaluation", {
                        userId: userId,
                        profilImageUrl: currentUser.profilImageUrl,
                        emeter: currentUser.name,
                        quadrant: $(this).data("quadrant"),
                        target: $(this).attr("id"),
                        userMarker: true
                    });
                }
            });
        <?php } ?>

        $(".addEvaluation<?= $kunik ?>").click(function(){
        //$("#quadrantInputs<?= $kunik ?>").on('keypress', '.inputEvaluation', function(e) {
            
            //if(e.which == 13 || e.which == 9) {
                //e.preventDefault();
                //.focus();
                inputNb[$(this).data("quadrant")]++;
                let input = `<div class="form-group" id="${$(this).data("quadrant")}Input${inputNb[$(this).data("quadrant")]}">
                        <div class="col-xs-12">
                            <input type="text" 
                                data-form='<?php echo $form["id"] ?>' 
                                data-quadrant='${"evaluation"+inputNb[$(this).data("quadrant")]}'
                                id="${kunik}.${$(this).data("quadrant")}.evaluation${inputNb[$(this).data("quadrant")]}${userId}" 
                                class="form-control saveOneByOne inputEvaluation ${$(this).data("quadrant")}.evaluation${inputNb[$(this).data("quadrant")]}${userId}" 
                                placeholder="">
                        </div>`;
                        
                        $(this).blur();
                        //alert($(this).data("quadrant")+"Inputs<?= $kunik ?>");
                $("#"+$(this).data("quadrant")+"Inputs<?= $kunik ?>").append(input);
                $(`#${kunik}.${$(this).data("quadrant")}.evaluation${inputNb[$(this).data("quadrant")]}${userId}`).focus();
            //}
        });


        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {
                "title" : "Paramétrage de l'évaluation",
                "description" : "Indication des informations nécessaire pour l'évaluation",
                "icon" : "fa-cog",
                "properties" : {
                    objet : {
                        inputType : "text",
                        label : "Objet de l'évaluation",
                        value :  sectionDyf.<?php echo $kunik ?>ParamsData.objet
                    },
                    objectif : {
                        inputType : "text",
                        label : "Objectif de l'évaluation",
                        value : sectionDyf.<?php echo $kunik ?>ParamsData.objectif
                    },
                    quadrants : {
                        inputType : "lists",
                        label : "Les quadrants de l'évaluation",
                        entries: {
                            label: {
                                type:"text",
                                label:"Quadrant",
                                class:"col-lg-3"
                            },
                            description: {
                                type:"textarea",
                                label:"Description",
                                class:"col-lg-5",
                                placeholder: "Décrire le quadrant"
                            },
                            inputNb: {
                                type:"text",
                                label:"champs",
                                class:"col-lg-3",
                                placeholder: "Ex. 2 ou [option 1; option 2; ...]"
                            }
                        }
                    },
                    deleteOthersProposition : {
                        inputType : "select",
                        label : "Suppression de proposition des autres",
                        class : "form-control",
                        options: {
                            "true":"Oui",
                            "false":"Non",
                            "admin":"Administrateur seulement"
                        },
                        value : sectionDyf.<?php echo $kunik ?>ParamsData.deleteOthersProposition
                    },
                    isCollectif : {
                        inputType : "checkboxSimple",
                        label : "Evaluation collectif",
                        params : {
                            "onText" : trad.yes,
                            "offText" : trad.no,
                            "onLabel" : trad.yes+" <span><b></b></a>",
                            "offLabel" : trad.no+" <span><b></b></a>"
                        },
                        checked: sectionDyf.<?php echo $kunik ?>ParamsData.isCollectif
                    },
                    enableSteps : {
                        inputType : "checkboxSimple",
                        label : "ajouter une étape de transformation",
                        params : {
                            "onText" : trad.yes,
                            "offText" : trad.no,
                            "onLabel" : trad.yes,
                            "offLabel" : trad.no
                        },
                        checked: sectionDyf.<?php echo $kunik ?>ParamsData.enableSteps
                    },
                    transformationQuadrants : {
                        inputType : "lists",
                        label : "Les quadrants de transformation",
                        entries: {
                            label: {
                                type:"text",
                                label:"Quadrant",
                                class:"col-lg-3"
                            },
                            description: {
                                type:"textarea",
                                label:"Description",
                                class:"col-lg-5",
                                placeholder: "Décrire le quadrant"
                            },
                            inputNb: {
                                type:"text",
                                label:"champs",
                                class:"col-lg-3",
                                placeholder: "Ex. 2 ou [option 1; option 2; ...]"
                            }
                        }
                    },
                },
                save : function (data) {
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                        if(val.inputType == "properties")
                            tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                        else if(val.inputType == "array")
                            tplCtx.value[k] = getArray('.'+k+val.inputType);
                        else
                            tplCtx.value[k] = $("#"+k).val();
                        
                        if(k=="quadrants" || k=="transformationQuadrants"){
                            let quadrants = {};
                            $.each(data[k], function(index, va){
                                let quadrant = {label: va.label, description: va.description, inputNb: va.inputNb};
                                let tindex = (k=="transformationQuadrants")?"Transform":"";
                                quadrants["quadrant"+tindex+index] = quadrant;
                            });
                            tplCtx.value[k] = quadrants;
                        }

                        mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                    });
                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == undefined)
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                            location.reload();
                        } );
                    }
                }
            }
        };

        //adds a line into answer
        $(".add<?php echo $kunik ?>").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
        });

        $(".edit<?php echo $kunik ?>").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = $(this).data("path");
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
        });

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = $(this).data("path");
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });

        $(".btn-step").off().on("click", function(){
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = $(this).data("path");
            tplCtx.value = $(this).data("step");
            dataHelper.path2Value( tplCtx, function(params) {
                $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                location.reload();
            });
        });

        function createQuadrant(quadrantData){
            for (let [key, value] of Object.entries(quadrantData)) {
                let hasAnswered = false;
                let defaultLength = value.inputNb;
                // Test if options or input number
                if(isNaN(Number(value.inputNb))){
                    options = value.inputNb.split(";").map(item=>item.trim());
                    defaultLength = options.length;
                    $(".btn"+kunik+key).remove();
                }else{
                    if(evaluations[key]){
                        defaultLength = (Object.keys(Object.keys(evaluations[key])).length==0)?parseInt(value.inputNb):Object.keys(Object.keys(evaluations[key])).length;
                        hasAnswered = (Object.keys(evaluations[key]).length!=0);
                    }
                }
                
                for (let index = 0; index < defaultLength; index++) {
                    let input = "";
                    let activeDelete = "";

                    activeDelete= (sectionDyf.<?php echo $kunik ?>ParamsData.deleteOthersProposition!="false" && canEdit)?`<div class="col-md-2 text-right pl-0">
                        <a role="button" 
                            class="btn btn-danger ${kunik}${key}${(hasAnswered)?Object.keys(evaluations[key])[index]:"evaluation"+index} removeEvaluation remove${kunik}"
                            data-code="${index}" 
                            data-target="${kunik}${key}${(hasAnswered)?Object.keys(evaluations[key])[index]:"evaluation"+index}" 
                            data-quadrant="${key}">
                            <i class="fa fa-trash"></i>
                        </a>
                    </div>`:'';

                    input = (options.length==0)?`<div class="form-group row" id="${key}Input${index}">
                                    <div class="${(activeDelete!='')?'col-md-10':"col-md-12"}">
                                        <input type="text" 
                                            value="${(hasAnswered)?evaluations[key][Object.keys(evaluations[key])[index]]:""}" 
                                            data-form='<?php echo $form["id"] ?>' 
                                            data-quadrant='${key}' 
                                            id="${kunik}.${key}.${(hasAnswered)?Object.keys(evaluations[key])[index]:"evaluation"+index+userId}" 
                                            class="form-control ${kunik}${key}${(hasAnswered)?Object.keys(evaluations[key])[index]:"evaluation"+index} inputEvaluation saveOneByOne ${key}-${(hasAnswered)?Object.keys(evaluations[key])[index]:"evaluation"+index+userId}" 
                                            placeholder="${(value.label)?(value.label):''}">
                                    </div>
                                    ${activeDelete}
                                </div>`:
                                `<div class="col-md-12 col-sm-12 col-xs-12 form-check">
                                    <label class="toggle">
                                        <input type="checkbox" 
                                            data-form='<?php echo $form["id"] ?>' 
                                            name="evaluation${kunik}"
                                            data-quadrant='${key}' 
                                            data-key="${(hasAnswered)?Object.keys(evaluations[key])[index]:"evaluation"+index}"
                                            id="${kunik}.${key}.evaluation${index}${userId}" 
                                            class="form-check-input evaluation${index} inputEvaluation saveOneByOne ${key}.evaluation${index}${userId}">
                                            <span class="label-text">${options[index]}</span>
                                        </label>
                                    </div>`;


                    $("#"+key+"Inputs<?= $kunik ?>").append(input);
                    


                    // Get max index to avoid conflict
                    if(hasAnswered){
                        inputNb[key] = parseInt(Object.keys(evaluations[key])[index].substring(10));
                    }else{
                        inputNb[key] = value.inputNb;
                    }

                    let checkboxId = "evaluation"+index;
                    // For Checkbox
                    if(options.length!=0 && evaluations[key]!=undefined ){
                        for (let k = 0; k < Object.keys(evaluations[key]).length; k++) {
                            $("."+Object.keys(evaluations[key])[k]).attr('checked', true);
                        }
                    }
                }
                options = [];
            }
        }


            /**
            $(document).on('keypress', 'input', function(e) {
                if((e.keyCode == 13 && e.target.type !== 'submit') || e.keyCode == 9) {
                    e.preventDefault();
                    return $(e.target).blur().focus();
                }
            });

            */
        });
    </script>
    <?php } else {
        //echo "<h4 class='text-red'>evaluation works with existing answers</h4>";
    } ?>