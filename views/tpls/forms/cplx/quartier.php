<style>
	.select2-container{
		width: 100%;
	}
</style>
<?php 
if($mode == "r" || $mode == "pdf"){ ?>
    <div class="col-xs-12 no-padding" id="<?= $kunik ?>">
        <label for="<?= $kunik ?>">
        	<h4 style="color:<?= ($titleColor) ? $titleColor : "black"; ?>">
        		<?= $label ?>
        	</h4>
            <span class="qvalue-<?= $key ?>"></span>
        </label><br/>
    </div>
<?php 
}else{
?>
	<div class="col-md-12 no-padding">
		<div class="form-group">
            <label for="<?= $key ?>">
                <h4 style="color:<?= ($titleColor) ? $titleColor : "black"; ?>">
                    <?= $label.$editQuestionBtn ?>
                </h4>
            </label>
            <br/>
            <input type="text'" 
                    class="select2TagsInput" 
                    id="<?= $key ?>" 
                    placeholder="<?= (isset($placeholder)) ? $placeholder : '' ?>" 
                    data-form='<?= $form["id"] ?>' 
                    value= ""
            />

            <?php if(!empty($info)){ ?>
                <small id="<?= $key ?>Help" class="form-text text-muted">
                    <?= $info ?>
                </small>
            <?php } ?>
        </div>
	</div>

<?php } ?>

<?php if($mode != "pdf"){ ?>
<script type="text/javascript">
	jQuery(document).ready(function() {
        let quartierObj<?= $key ?> = {
            formId : <?= json_encode((string) $parentForm["_id"]) ?>,
            answerId : <?= json_encode((string) $answer["_id"]) ?>,
            answerPath : <?= json_encode($answerPath) ?>.slice(0, -1),
            answer : <?= json_encode($answer) ?>,
            currentQuarierIds : [],
            currentQuarierValues :[],
            canEditForm : <?= json_encode(Form::canAdmin((string) $parentForm["_id"]) && $mode=="fa" ) ?>,
            canEditAnswer : <?= json_encode(Form::canAdmin((string) $parentForm["_id"]) || json_encode(Yii::app()->session['userId'] == $answer["use"]) ) ?>,
            defaultFilters : <?= !empty($input["filters"]) ? json_encode($input["filters"]) : json_encode([]) ?>,
            init : function(qobj){
                qobj.initCurrentValue(qobj);
                qobj.getCurrentValue(qobj);
                qobj.initS2(qobj);
                qobj.events(qobj);
            },
            initCurrentValue : function(qobj){
                if(exists(jsonHelper.getValueByPath(qobj.answer,qobj.answerPath))){
                    var currentValue = jsonHelper.getValueByPath(qobj.answer,qobj.answerPath);
                    if(Array.isArray(currentValue)){
                        qobj.currentQuarierIds = currentValue;
                        currentValue = currentValue.join();
                        $("#<?= $key ?>").val(currentValue);
                    }
                }
            },
            getCurrentValue : function(qobj){
                if(Array.isArray(qobj.currentQuarierIds) && qobj.currentQuarierIds.length != 0){
                    ajaxPost(
                        null,
                        baseUrl+"/" + moduleId + "/search/globalautocomplete",
                        {
                            searchType : ["zones"],
                            notSourceKey: true,
                            indexStep : 0,
                            filters : {
                                "_id" : {'$in' : qobj.currentQuarierIds},
                            },
                            fields : ["type"]
                        },
                        function(data){
                            qobj.currentQuarierValues = data.results;
                            var html = '';
                            $.each(qobj.currentQuarierValues,function(k,v){
                                html += `<div class="label label-default ">${v.name}</div> `
                            })
                            $(".qvalue-<?= $key ?>").html(html);
                        },
                        function (data){
                                    
                        },null,{async:false}
                    );
                }
            },
            initS2 : function(qobj){
                mylog.log(...qobj.defaultFilters,"qobj.defaultFilters")
                $("#<?= $key ?>").select2({
                    minimumInputLength: 3,
                    tags: true,
                    multiple: true,
                    "tokenSeparators": [','],
                    createSearchChoice: function (term, data) {
                        if (!data.length)
                            return {
                                id: term,
                                text: term
                            };
                    },
                    initSelection : function (element, callback) {
                        var data = [];
                        $(element.val().split(",")).each(function () {
                            data.push({id: this, text:  qobj.currentQuarierValues[this]["name"]});
                        });
                        callback(data);
                    },
                    ajax: {
                        url: baseUrl+"/" + moduleId + "/search/globalautocomplete",
                        dataType: 'json',
                        type: "GET",
                        quietMillis: 50,
                        data: function (term) {
                            return {
                                searchType : ["zones"],
                                notSourceKey: true,
                                name : term,
                                filters : {
                                    '$or': {
                                            '$and' : [
                                                {"type" : {'$in' : ["QPV","quartier"]}},
                                                //{"insee" : {'$regex' :"97411"}}
                                                ...qobj.defaultFilters
                                            ] 
                                        
                                    }
                                },
                                fields : ["type"] 
                            };
                        },
                        results: function (data) {
                            return {
                                results: $.map(data.results, function (item,k) {
                                    return {
                                        text: item.name,
                                        id: k
                                    }
                                })
                            };
                        }
                    }
                })
                .on('change',()=>{
                    var tplCtx = {
                        id : qobj.answerId,
                        path : qobj.answerPath,
                        value : $("#<?= $key ?>").val().split(","),
                        collection : "<?= Form::ANSWER_COLLECTION ?>"
                    }
                    dataHelper.path2Value(tplCtx, function (params) {
                            
                    })
                });
            },
            events : function(qobj){

            }
        }
        quartierObj<?= $key ?>.init(quartierObj<?= $key ?>);
	});
</script>
<?php } ?>

