<?php
	if($mode == "fa"){
		$editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";
	}
?>
<div class="qpv-chooser<?= $kunik ?> form-check text-left">
	<label class="form-check-label" for="<?php echo $key ?>">
		<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
		<?= $label.$counQPV.$editQuestionBtn.@$editParamsBtn ?>
		</h4>
	</label> <br/>
	<?php if(!empty(count($quarties))){ ?>
		<div class="input-group no-padding form-check-label col-xs-12 col-md-4">
			<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
			<input type="text" class="form-control search-quartier" placeholder="<?= Yii::t("common","Search") ?> quartier">
		</div>
		<br>
	<?php } ?>	
	<?php if(!empty($info)){ ?>
		<small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
	<?php } ?>
	<?php 
	if( empty($quarties))  {
		echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i>  QPV Vide</span>";
	} else {
		foreach ($quarties as $idZone => $lbl) { 
			$checkedValue =  "";
			if(in_array($idZone,$currentAnswer))
				$checkedValue =  "checked";
			?>
			<label class="container<?= $kunik ?>">	
				<?= $lbl["name"] . (isset($lbl["codeQPV"]) ? "<span class='text-red'> (QPV)</span>" :"")/*." ".$idZone*/  ?>
				<input type="checkbox" class="form-check-input <?php echo $inpClass ?>  <?= $kunik ?>"  id="<?php echo $kunik.$idZone ?>" data-value="<?= $idZone ?>" data-form='<?php echo $form["id"] ?>' <?php echo $checkedValue?>  >
				<span class="checkmark<?= $kunik ?>"></span>
			</label>
		<?php } 
	}?>
</div>

<script type="text/javascript">
var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answer["answers"][$kunik])) ? $answer["answers"][$kunik] : null ); ?>;
var current<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
var q = <?= json_encode( $query ); ?>; 
console.log(q,"qqqqqq");
jQuery(document).ready(function() {
	mylog.log("render form input","/modules/costum/views/tpls/forms/cplx/multiCheckbox.php");
	var currentAnswer = <?php echo json_encode( $currentAnswer ); ?>;
	$('.<?= $kunik ?>').change(function(e) {
		e.stopImmediatePropagation();
		var value<?= $kunik ?> = $(this).data("value");
		var path<?= $kunik ?> = "answers."+$(this).data("form")+".<?= $key ?>";
		const dataId = "<?= $key ?>"
		const val = $(`input.checkboxNew${dataId}[type='checkbox']:checked`).length;
		if(typeof newReloadStepValidationInputGlobal != "undefined") {
			// newReloadStepValidationInputGlobal(null)
		} else {
			if(typeof ownAnswer != 'undefined' && typeof ownAnswer[$(this).attr("data-form")] != 'undefined' && val > 0) {
				ownAnswer[$(this).attr("data-form")][dataId] = val
			} else if(typeof ownAnswer != 'undefined' && typeof ownAnswer[$(this).attr("data-form")] != 'undefined'){
				ownAnswer[$(this).attr("data-form")][dataId] = ""
			}
		}
		if(typeof removeError != "undefined") {
			const elem = $(`li#question${dataId.replace('checkboxNew', '')},#question_${dataId.replace('checkboxNew', '')}`);
			if(val > 0){
				removeError(
					elem, 
					{
						'border' : 'none'
					},
					'error-msg'
				)
			} else if(typeof addError != 'undefined'){
				// addError(
				// 	elem,
				// 	{
				// 		'border' : '1px solid #a94442'
				// 	},
				// 	'error-msg'
				// )
			}
		}
		if (this.checked) {
			dataHelper.path2Value({
				id : "<?= (string)$answer["_id"] ?>",
				collection : "answers",
				arrayForm: true,
				edit:false,
				value: value<?= $kunik ?>,
				path: path<?= $kunik ?>
			}, function(params){
				toastr.success(tradForm.modificationSave);
				if(typeof newReloadStepValidationInputGlobal != "undefined") {
					newReloadStepValidationInputGlobal({
						inputKey : <?php echo json_encode($key); ?>,
						inputType : "tpls.forms.cplx.checkboxNew" 
					})
				}
			} );
		} else {
			dataHelper.path2Value({
				id : "<?= (string)$answer["_id"] ?>",
				collection : "answers",
				pull : path<?= $kunik ?>,
				value: null,
				path: path<?= $kunik ?>+"."+currentAnswer.indexOf(value<?= $kunik ?>)
			}, function(params){
				toastr.success(tradForm.modificationSave);
				if(typeof newReloadStepValidationInputGlobal != "undefined") {
					newReloadStepValidationInputGlobal({
						inputKey : <?php echo json_encode($key); ?>,
						inputType : "tpls.forms.cplx.checkboxNew" 
					})
				}
			} );
		}
	});

	$(".qpv-chooser<?= $kunik ?> .search-quartier").on("keyup", function() {
		setTimeout(() => {
			var searchKey = $(this).val();
			$('.container<?= $kunik ?>').each(function() {
				var txt = $(this).text();
				if (txt.match(new RegExp(searchKey, "gi"))) {
					$(this).show(500)
				} else {
					$(this).hide(500);
				}
			})
		}, 1100);
	})

	var dynform<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
			"title" : "Configuration des quartiers",
			"icon" : "fa-cog",
			"properties" : {	
				typeQuariter : {
					inputType: "selectMultiple",
					isSelect2:true,
					label : "Type de quartier",
					placeholder : "Type de quartier",
					options:{
						"QPV" : "Quartiers prioritaires de la politique de la ville",
						"gdQuartier" : "Grands quartiers",
						"quartier" : "Autres quartiers"
					}
				},
				codeInseeQuartier : {
					inputType : "array",
					label : "code INSEE",
					init : function() {
						$(`<input type="text" class="form-control copyConfig" placeholder="vous pouvez copier le liste ici, séparé par des virgules; ou utiliser le button ajout ci-dessous"/>`).insertBefore('.listarray .inputs.array');
						$(".copyConfig").off().on("blur", function() {
							let textVal = $(this).val().length > 0 ? $(this).val().split(",") : [];
							textVal.forEach((el, index) => {
								dyFObj.init.addfield('.listarray', el, 'list')
							});
							$(this).val('')
						})
					}
				},
				otherQuartier : {
					inputType : "finder",
					initMe :false,
					initContext : false,
					initBySearch : true,
					initContacts : false,
					label : "Choisir un autre quartier",
					info : "Choisir un autre quartier si votre quartier n'est pas dans le code INSEE si dessus",
					multiple : true,
					initType: ["zones"],
					openSearch :true,
					search : {
						filters: {
							'type' : {'$in' : ["QPV","gdQuartie","quartier"]}
						}
					},
				}
			},
			save : function (data) {  
				tplCtx.value = {};
				$.each( dynform<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
					if(val.inputType == "properties")
						tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
					else if(val.inputType == "array")
						tplCtx.value[k] = getArray('.'+k+val.inputType);
					else if(val.inputType == "formLocality")
						tplCtx.value[k] = getArray('.'+k+val.inputType);
					else if(val.inputType == "finder" && k == "otherQuartier")
						tplCtx.value[k] = data.otherQuartier;
					else tplCtx.value[k] = $("#"+k).val();
				});

				if(typeof tplCtx.value == "undefined")
					toastr.error('value cannot be empty!');
				else {
					dataHelper.path2Value( tplCtx, function(params) { 
						dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
						if(typeof reloadInput != "undefined" && stepObj && stepObj?.bindEvent?.inputEvents) {
							reloadInput(<?= json_encode($key) ?>, <?= json_encode($formId) ?>, () => stepObj.bindEvent.inputEvents(stepObj))
						}
						toastr.success(trad.saved);
					} );
				}
			}
		}
	};

	$(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
		tplCtx.id = $(this).data("id");
		tplCtx.collection = $(this).data("collection");
		tplCtx.path = $(this).data("path");
		mylog.log(current<?php echo $kunik ?>ParamsData,"hiala aminao");
		dyFObj.openForm(dynform<?php echo $kunik ?>Params,null,current<?php echo $kunik ?>ParamsData);
	});
});
</script>