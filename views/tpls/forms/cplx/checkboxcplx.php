<style type="text/css">
    .ckd-grp {
        /*position: absolute;
        top: calc(50% - 10px);*/
    }
    .ckd-grp label {
        cursor: pointer;
        -webkit-tap-highlight-color: transparent;
        /* padding: 6px 8px; */
        border-radius: 20px;
        float: left;
        transition: all 0.2s ease;
    }
    .ckd-grp label:hover {
        background: rgba(125,100,247,0.06);
    }
    .ckd-grp label:not(:last-child) {
        margin-right: 16px;
    }
    .ckd-grp label span {
        vertical-align: middle;
    }
    .ckd-grp label span:first-child {
        position: relative;
        display: inline-block;
        vertical-align: middle;
        width: 27px;
        height: 27px;
        background: #e8eaed;
        border-radius: 10px;
        transition: all 0.2s ease;
        margin-right: 8px;
        padding: 3px;
    }
    .ckd-grp label span:first-child:after {
        content: '';
        position: absolute;
        width: 16px;
        height: 16px;
        margin: 2px;
        background: #fff;
        border-radius: 6px;
        transition: all 0.2s ease;
    }
    .ckd-grp label:hover span:first-child {
        /*background*/: #7d64f7;
    }

    .ckb-grp label:hover span:first-child:after {
        /*background: #7d64f7;*/
        /*background: #7d64f7;*/
        padding: 0px;
        background: white;

    }

    .ckd-grp input {
        display: none;
    }
    .ckd-grp input:checked + label span:first-child {
        /*background: #7d64f7;*/
        background: #e8eaed;
    }
    .ckd-grp input:checked + label span:first-child:after {
        /*transform: scale(0.5);*/
        background: #7d64f7;
    }

    .multiChbtextInp{border: 0; padding: 7px 0; border-bottom: 1px solid #ccc;}

    .multiChbtextInp ~ .focus-border{position: absolute; bottom: 0; left: 0; width: 0; height: 2px; background-color: #3399FF; transition: 0.4s;}
    .multiChbtextInp:focus ~ .focus-border{width: 100%; transition: 0.4s;}

    .paramsonebtn , .paramsonebtnP {
        font-size: 17px;
        display: none;
        padding: 5px;
    }

    .paramsonebtn:hover {
        color: red;
    }

    .paramsonebtnP:hover {
        color: blue;
    }

    .thckd:hover .paramsonebtn, .thckd:hover .paramsonebtnP {
        display: inline-block;
    }

    .multiChbtextInp:focus {
        outline: none !important;
    }

    .responsemulticheckboxplus:before{
        content: '\f0a9';
        margin-right: 15px;
        font-family: FontAwesome;
        color: #d9534f;
    }

    .ckboptinfo{
        font-size: 15px;
        color: #637381;
    }

</style>



<?php
$cle = $key ;
$value = (!empty($answers)) ? " value='".$answers."' " : "";
$inpClass = " saveOneByOne";

$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";


$subkey = $key;


$paramsData = [
    "list" => [	],
    "tofill" => [ ],
    "optinfo" => [ ],
    "placeholdersckb" => [ ],
    "type" => [
        "simple" => "Sans champ de saisie",
        "cplx" => "Avec champ de saisie"
    ],
    'checked' => [],
    "width" => [
        "12" => "1",
        "6" => "2",
        "4" => "3"
    ],
    'dependOn' => []
];

# Depended Answer
$checkboxs = Array();

$inputs = Array();
//$inputsPath = PHDB::findOne(Form::COLLECTION, ["id"=> $form["id"]]);
$inputsPath = $form;
if(!empty($inputsPath["inputs"]))
    foreach($inputsPath["inputs"] as $inputKey => $inputValue){
        if($inputKey!=$key && strpos($inputValue["type"], 'multiCheckbox') !== false){
            $inputs[$inputKey] = $inputValue["label"];
        }
    }
$paramsData["dependOn"] = $inputs;

if (isset($input["conditional"])) {
    foreach ($input["conditional"] as $key => $value) {

        ?>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                $("#<?php echo $value["value"] ?>").change(function() {
                    if($(this).prop('checked')) {
                        $('#question<?php echo $value["input"] ?>').removeClass("hide");
                        scrollintoDiv("question<?php echo $value["input"] ?>", 1000);
                    }
                });
                if($("#<?php echo $value["value"] ?>").is(':checked')) {
                    $('#question<?php echo $value["input"] ?>').removeClass("hide");
                    // scrollintoDiv("question<?php echo $value["input"] ?>");
                }

                $('.ckbCo.<?php echo $kunik ?>').change(function(){
                    if(!$("#<?php echo $value["value"] ?>").prop('checked')) {
                        $('#question<?php echo $value["input"] ?>').addClass("hide");
                    }
                });

            });
        </script>
        <style type="text/css">
            #question<?php echo $value["input"] ?> {
                -webkit-transition: all 2s ease;
                -moz-transition: all 2s ease;
                -o-transition: all 2s ease;
                transition: all 2s ease;
            }
        </style>

        <?php
    }
}

if(isset($parentForm["params"][$kunik]["global"]["dependOn"]) && $parentForm["params"][$kunik]["global"]["dependOn"]!=""){
    $dependedOn = $parentForm["params"][$kunik]["global"]["dependOn"];
    if(isset($answer["answers"][$form["id"]]["multiCheckboxPlus".$dependedOn])){
        foreach ($answer["answers"][$form["id"]]["multiCheckboxPlus".$dependedOn] as $dependKey => $dependValue) {
            $dependValue=key($dependValue);
            array_push($checkboxs, $dependValue);
        }
    }
}else{
    # No answer to depended on
    if(isset($parentForm["params"][$kunik]["global"]['list']))
        $checkboxs = $parentForm["params"][$kunik]["global"]['list'];
}


if( isset($parentForm["params"][$kunik]) ) {
    if( isset($parentForm["params"][$kunik]["global"]["list"]) ) {
        $paramsData["list"] =  $parentForm["params"][$kunik]["global"]["list"];
        foreach ($paramsData["list"] as $k => $v) {
            if(isset($parentForm["params"][$kunik]["tofill"][$v]) ){
                $paramsData["tofill"] += array($v => $parentForm["params"][$kunik]["tofill"][$v]);
            } else {
                $paramsData["tofill"] += [$v => "simple"];
            }

            if(isset($parentForm["params"][$kunik]["optinfo"][$v]) ){
                $paramsData["optinfo"] += array($v => $parentForm["params"][$kunik]["optinfo"][$v]);
            } else {
                $paramsData["optinfo"] += [$v => ""];
            }

            $paramsData["optimage"][$v] = null;
        }

    }
}

if( isset($parentForm["params"][$kunik]) ) {
    if( isset($parentForm["params"][$kunik]["global"]["list"]) ) {
        //$paramsData["list"] =  $parentForm["params"][$kunik]["global"]["list"];
        foreach ($paramsData["list"] as $k => $v) {
            if(isset($parentForm["params"][$kunik]["placeholdersckb"][$v]) ){
                $paramsData["placeholdersckb"] += array($v => $parentForm["params"][$kunik]["placeholdersckb"][$v]);
            } else {
                $paramsData["placeholdersckb"] += [$v => ""];
            }
        }
    }
}

if( isset($answer["answers"][$form["id"]][$kunik]) ) {
    $paramsData["checked"] =  $answer["answers"][$form["id"]][$kunik];
}

if($mode == "r")
{
    ?>
    <div class="text-left" style="padding-top: 50px;">
        <label class="form-check-label" for="<?php echo $key ?>">
            <h4 style=" color: <?php echo ($titleColor) ? $titleColor : "black"; ?>;"><?php echo $label ?>
            </h4>
        </label>
        <?php
        if(!empty($info))
        {
            ?>
            <br/><small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
            <?php
        }
        ?>

        <?php

        if(count($checkboxs)==0 && isset($dependedOn) && $dependedOn!="")
        {
            echo "<br/><span class='text-secondary text-center'> CETTE QUESTION DEPENDS DE VOTRE RÉPONSE SUR <a class='text-info goto".$kunik."' data-path='question".$dependedOn."' href='javascript:;'>CELLE CI</a></span>";
        }
        else if(count($checkboxs)==0 && !isset($dependedOn))
        {
            echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> VOUS DEVEZ D'ABORD AJOUTER DES OPTIONS </span>";
        }else {
            ?>

            <div class="">
                <div class="">
                                <?php
                                foreach ($checkboxs as $ix => $lbl)
                                {

                                    $ckd = "";
                                    $place = "";
                                    foreach ($paramsData["checked"] as $ke => $va) {
                                        if(isset($ke["value"]) && $ke["value"] == $lbl){
                                            $ckd = "checked";
                                        }
                                    }
                                    if ($ckd == "checked"){
                                        ?>

                                        <!-- <tr class="thckd"><theih > -->
                                        <div class="" style="min-height: 50px;">
                                            <div class="">

                                                <label for="<?php echo $kunik.$ix ?>" class="responsemulticheckboxplus">
	  							<span>

	  							</span>
                                                    <span><?php echo $lbl ?>


                                                        <?php
                                                        if (isset($paramsData["tofill"][$lbl]) && $paramsData["tofill"][$lbl] == "cplx") {
                                                            ?>
                                                            <?php
                                                            if(isset($answer["answers"][$form["id"]][$kunik])){
                                                                foreach($answer["answers"][$form["id"]][$kunik] as $kans => $val){
                                                                    if(isset($val["value"]) && $val["value"] ==$lbl && isset($val["choiceinfo"])){
                                                                        echo $val["choiceinfo"];
                                                                    }
                                                                }
                                                            }
                                                            ?>


                                                            <?php
                                                        }
                                                        ?>

                                                        <?php

                                                        if($canEditForm)
                                                        {


                                                            echo " <a href='javascript:;' data-id='".$parentForm["_id"]."'  data-collection='".Form::COLLECTION."'  data-path='params.".$kunik."' class='previewTpl paramsonebtn editone".$kunik."Params".$ix." '><i class='fa fa-cog'></i> </a>";



                                                            if (isset($paramsData["tofill"][$lbl]) && $paramsData["tofill"][$lbl] == "cplx") {
                                                                echo " <a alt='placeholder' href='javascript:;' data-id='".$parentForm["_id"]."'  data-collection='".Form::COLLECTION."'  data-path='params.".$kunik."' class='previewTpl paramsonebtnP editonep".$kunik."Params".$ix." '><i class='fa fa-pencil'></i> </a>";
                                                            }

                                                        }
                                                        ?>
								</span>
                                                </label>
                                            </div>
                                        </div>
                                        <?php
                                    }  }
                                ?>
                            </div>
            </div>
            <?php
        }
        ?>

    </div>

<?php }else { ?>


    <div class="text-left" style="padding-top: 50px;">
        <label class="form-check-label" for="<?php echo $key ?>">
            <h4 style=" color: <?php echo ($titleColor) ? $titleColor : "black"; ?>;"><?php echo $label.$editQuestionBtn.$editParamsBtn ?>
            </h4>
        </label>
        <?php
        if(!empty($info))
        {
            ?>
            <br/><small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
            <?php
        }
        ?>
        <div id="checkboxList<?php echo $kunik ?>" class="ckd-grp">

            <?php

            if(count($checkboxs)==0 && isset($dependedOn) && $dependedOn!="")
            {
                echo "<br/><span class='text-secondary text-center'> CETTE QUESTION DEPENDS DE VOTRE RÉPONSE SUR <a class='text-info goto".$kunik."' data-path='question".$dependedOn."' href='javascript:;'>CELLE CI</a></span>";
            }
            else if(count($checkboxs)==0 && !isset($dependedOn))
            {
                echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> VOUS DEVEZ D'ABORD AJOUTER DES OPTIONS </span>";
            } else {
                ?>
                            <div id="checkboxContainter<?php echo $kunik ?>" class="">
                                <?php
                                foreach ($checkboxs as $ix => $lbl)
                                {
                                    $ckd = "";
                                    $place = "";
                                    foreach ($paramsData["checked"] as $ke => $va) {
                                        if(isset($va["value"]) && $va["value"] == $lbl){
                                            $ckd = "checked";
                                        }
                                    }


                                    ?>
                                    <!-- <tr class="thckd"><theih > -->
                                    <div class="" style="min-height: 50px;">
                                        <div class="thckd">
                                            <input data-id="<?php echo $kunik ?>" class="ckbCo <?php echo $kunik ?>"  id="<?php echo $kunik.$ix ?>" data-form='<?php echo $form["id"] ?>' <?php echo $ckd?> type="checkbox" name="<?php echo $kunik ?>" data-type="<?php

                                            if (isset($paramsData["tofill"][$lbl]) && $paramsData["tofill"][$lbl] == "cplx")
                                            {
                                                echo "cplx";
                                            }else
                                            {
                                                echo "simple";
                                            }
                                            ?>" value="<?php echo $lbl ?>" />
                                            <label for="<?php echo $kunik.$ix ?>">
	  							<span>

	  							</span>
                                                <span><?php echo $lbl ?>
		  							<div style="position: relative; display: inline-block;">

		  								<?php
                                        if (isset($paramsData["tofill"][$lbl]) && $paramsData["tofill"][$lbl] == "cplx") {
                                            ?>
                                            <input data-impname="<?php echo $kunik ?>" class="multiChbtextInp inputckbCo <?php echo $kunik ?>" data-id="<?php echo $kunik ?>" data-form='<?php echo $form["id"] ?>' type="text" data-imp="<?php echo $lbl; ?>" placeholder="<?php echo $paramsData['placeholdersckb'][$lbl]; ?>" style="display: inline-block !important; position: relative;"  value="<?php
                                            if(isset($answer["answers"][$form["id"]][$kunik])){
                                                foreach($answer["answers"][$form["id"]][$kunik] as $kans => $val){
                                                    if(isset($val["value"]) && $val["value"] ==$lbl && isset($val["choiceinfo"])){
                                                        echo $val["choiceinfo"];
                                                    }
                                                }
                                            }
                                            ?>"

                                            />
                                            <span class="focus-border"></span>

                                            <?php
                                        }
                                        ?>
		  					 		</div>

		  							<?php

                                    if($canEditForm)
                                    {
                                        echo " <a href='javascript:;' data-id='".$parentForm["_id"]."'  data-collection='".Form::COLLECTION."'  data-path='params.".$kunik."' class='previewTpl paramsonebtn editone".$kunik."Params".$ix." '><i class='fa fa-cog'></i> </a>";

                                        if (isset($paramsData["tofill"][$lbl]) && $paramsData["tofill"][$lbl] == "cplx") {

                                            echo " <a alt='placeholder' href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."'  data-path='params.".$kunik."' class='previewTpl paramsonebtnP editonep".$kunik."Params".$ix." '><i class='fa fa-pencil'></i> </a>";

                                        }

                                    }
                                    ?>
								</span>
                                                <?php
                                                if (isset($paramsData["optinfo"][$lbl]) && $paramsData["optinfo"][$lbl] != "") {
                                                    ?>
                                                    <div class="ckboptinfo"> <?php echo $paramsData["optinfo"][$lbl]?>  </div>
                                                    <?php
                                                }

                                                if (isset($paramsData["optimage"][$lbl]) && !empty($paramsData["optimage"][$lbl])) {
                                                    foreach ($paramsData["optimage"][$lbl] as $oikey => $oivalue) {
                                                        if (isset($oivalue["docPath"] )) {
                                                            ?>
                                                            <div>
                                                                <img class="img-responsive" src="<?php echo $oivalue["docPath"] ?>">
                                                            </div>
                                                            <?php
                                                        }
                                                    }
                                                }

                                                ?>
                                            </label>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </th></tr>
                <?php
            }
            ?>
        </div>

    </div>
    <script type="text/javascript">

        var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answer["answers"][$kunik])) ? $answer["answers"][$kunik] : null ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;



        sectionDyf.<?php echo $kunik ?>ParamsPlace = "";


        jQuery(document).ready(function() {
            mylog.log("render form input","/modules/costum/views/tpls/forms/cplx/multiCheckbox.php");

            answer = {};

            $(".ckbCo.<?php echo $kunik ?>").change(function(){
                var allckd = [];
                $("input:checkbox[name='"+$(this).attr("name")+"']:checked").each(function(){
                    mylog.log("ckd", $(this).val());
                    var key = $(this).val();
                    var id =$(this).data("id");
                    var b =  { 'value' : key, 'choiceinfo' : $('input[data-impname="' + id + '"]:input[data-imp="' + $(this).val() + '"]').val() };
                    allckd.push(b);
                });

                answer.path = "answers."+$(this).data("form")+"."+$(this).data("id");
                answer.collection = "answers" ;
                answer.id = "<?php echo $answer["_id"]; ?>";
                answer.value = allckd;
                dataHelper.path2Value(answer , function(params) {
                    //toastr.success('Mise à jour enregistrée');
                });
            });

            $('.inputckbCo.<?php echo $kunik ?>').blur(function(){
                if($('input[value="' + $(this).data("imp") + '"]').is(':checked')) {

                    var allckd = [];

                    var val=$(this).val();

                    var impname=$(this).data("impname");

                    $("input:checkbox[name='"+$(this).data("impname")+"']:checked").each(function(){
                        mylog.log("ckd", $(this).val());
                        var key = $(this).val();
                        var b = { 'value' : key, 'choiceinfo' : $('input[data-impname="'+impname+'"][data-imp="' + $(this).val() + '"]').val() };
                        allckd.push(b);
                    });

                    answer.path = "answers."+$(this).data("form")+"."+$(this).data("id");
                    answer.collection = "answers" ;
                    answer.id = "<?php echo $answer["_id"]; ?>";
                    answer.value = allckd;
                    dataHelper.path2Value(answer , function(params) {
                        //toastr.success('Mise à jour enregistrée');
                    });

                }
            })

            $(document).on("blur", ".addmultifield", function() {
                let value = $(this).val();
                let regex = /[.\$]/g;
                if (value.match(regex)) {
                    value = value.replace(regex, "");
                    $(this).val(value);
                    toastr.info("Carractère non autorisé");
                }
            });

            sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema" : {
                    "title" : "<?php echo $kunik ?> config",
                    "icon" : "fa-cog",
                    "properties" : {
                        list : {
                            inputType : "array",
                            label : "Liste de bouton check",
                            value :  sectionDyf.<?php echo $kunik ?>ParamsData.list
                        },
                        dependOn : {
                            inputType : "select",
                            label : "Sur quelle réponse depends cette question",
                            class : "form-control",
                            options : sectionDyf.<?php echo $kunik ?>ParamsData.dependOn,
                            value: "<?php echo (isset($parentForm["params"][$kunik]['global']['dependOn']) and $parentForm["params"][$kunik]['global']['dependOn'] != "") ? $parentForm["params"][$kunik]['global']['dependOn']:''; ?>"
                        },
                        width : {
                            inputType : "select",
                            class : "form-control",
                            label : "Nombre d'element par ligne",
                            options :  sectionDyf.<?php echo $kunik ?>ParamsData.width,
                            value : "<?php echo (isset($parentForm["params"][$kunik]['global']['width']) && $parentForm["params"][$kunik]['global']['width'] != "") ? $paramsData["width"][strval($parentForm["params"][$kunik]['global']['width'])] : ''; ?>"
                        }
                    },
                    save : function () {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                            if(val.inputType == "properties")
                                tplCtx.value = getPairsObj('.'+k+val.inputType);
                            else if(val.inputType == "array")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else if(val.inputType == "formLocality")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else
                                tplCtx.value[k] = $("#"+k).val();
                        });
                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                dyFObj.closeForm(); // $("#ajax-modal").modal('hide');
                                reloadInput("<?php echo $cle ?>", "<?php echo (string)$form["id"] ?>");
                            } );
                        }

                    }
                }
            };


            <?php if(count($paramsData["list"]) != 0){
            foreach ($paramsData["list"] as $key => $value) {
            // if($lbl == $key){
            // 	if($value == "cplx"){
            ?>
            sectionDyf.<?php echo $kunik ?>oneParams<?php echo $key ?> = {
                "jsonSchema" : {
                    "title" : "<?php echo $kunik ?> config",
                    "icon" : "fa-cog",
                    "properties" : {
                        tofill : {
                            inputType : "select",
                            label : "Bouton check avec champ de saisie",
                            options :  sectionDyf.<?php echo $kunik ?>ParamsData.type,
                            value : "<?php echo (isset($paramsData["tofill"][$value]) ? $paramsData["tofill"][$value] : ''); ?>"
                        },
                        optinfo : {
                            inputType : "text",
                            label : "Information supplementaire",
                            value : "<?php echo (isset($paramsData["optinfo"][$value]) ? $paramsData["optinfo"][$value] : ''); ?>"
                        },
                        optimage :{
                            "inputType" : "uploader",
                            "label" : "Image en fond",
                            "docType": "image",
                            "itemLimit" : 1,
                            "filetypes": ["jpeg", "jpg", "gif", "png"],
                            "showUploadBtn": false,

                            initList : <?php echo json_encode($paramsData["optimage"][$value]) ?>
                        }
                    },
                    beforeBuild : function(){
                        uploadObj.set("answers","<?php echo $cle.$value ?>");
                    },
                    save : function () {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>oneParams<?php echo $key ?>.jsonSchema.properties , function(ko,valo) {

                            if (ko == "tofill") {

                                tplCtx.path = 'params.<?php echo $kunik ?>.tofill';

                                $.each(sectionDyf.<?php echo $kunik ?>ParamsData.tofill, function(ke, va) {

                                    if(ke == "<?php echo $value; ?>"){

                                        var azerazer = ke.toString();
                                        sectionDyf.<?php echo $kunik ?>ParamsData.tofill[azerazer] = $("#"+ko).val();
                                    }
                                })

                                tplCtx.value = sectionDyf.<?php echo $kunik ?>ParamsData.tofill;

                                mylog.log("save tplCtx",tplCtx);

                                if(typeof tplCtx.value == "undefined")
                                    toastr.error('value cannot be empty!');
                                else {
                                    dataHelper.path2Value( tplCtx, function(params) {

                                    } );
                                }

                            }else if(ko == "optinfo"){
                                tplCtx.path = 'params.<?php echo $kunik ?>.optinfo';

                                $.each(sectionDyf.<?php echo $kunik ?>ParamsData.optinfo, function(ke, va) {

                                    if(ke == "<?php echo $value; ?>"){

                                        var azerazer = ke.toString();
                                        sectionDyf.<?php echo $kunik ?>ParamsData.optinfo[azerazer] = $("#"+ko).val();
                                    }
                                })

                                tplCtx.value = sectionDyf.<?php echo $kunik ?>ParamsData.optinfo;

                                mylog.log("save tplCtx",tplCtx);

                                if(typeof tplCtx.value == "undefined")
                                    toastr.error('value cannot be empty!');
                                else {
                                    dataHelper.path2Value( tplCtx, function(params) {

                                    } );
                                }

                            } else if( ko == "optimage"){
                                tplCtx.path = 'params.<?php echo $kunik ?>.optimage';
                                tplCtx.value["optimage"] = $("#optimage").val();

                                if(typeof tplCtx.value == "undefined")
                                    toastr.error('value cannot be empty!');
                                else {
                                    dataHelper.path2Value( tplCtx, function(params) {
                                        dyFObj.commonAfterSave(params,function(){

                                        });

                                    } );
                                }
                            }

                            dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
                            reloadInput("<?php echo $cle ?>", "<?php echo (string)$form["id"] ?>");

                        });
                    }
                }
            };

            $(".editone<?php echo $kunik ?>Params<?php echo $key ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>oneParams<?php echo $key ?>);
            });
            <?php
            }
            }

            ?>


            <?php if(count($paramsData["list"]) != 0){
            foreach ($paramsData["list"] as $key => $value) {
            // if($lbl == $key){
            if($paramsData["tofill"][$value] == "cplx"){
            ?>

            sectionDyf.<?php echo $kunik ?>oneParamsPlaceholders<?php echo $key ?> = {
                "jsonSchema" : {
                    "title" : "<?php echo $kunik ?> config",
                    "icon" : "fa-cog",
                    "properties" : {
                        placeholdersckb : {
                            inputType : "text",
                            label : "Modifier les textes à l'interieur du champs de saisie",
                            value :  "<?php echo $paramsData["placeholdersckb"][$value]; ?>"
                        }
                    },
                    save : function () {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>oneParamsPlaceholders<?php echo $key ?>.jsonSchema.properties , function(kk,vall) {


                            $.each(sectionDyf.<?php echo $kunik ?>ParamsData.placeholdersckb, function(ke, va) {
                                if(ke == "<?php echo $value; ?>"){

                                    var azerazer = ke.toString();
                                    sectionDyf.<?php echo $kunik ?>ParamsData.placeholdersckb[azerazer] = $("#"+kk).val();

                                }
                            })


                            tplCtx.value = sectionDyf.<?php echo $kunik ?>ParamsData.placeholdersckb;


                        });
                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
                                reloadInput("<?php echo $cle ?>", "<?php echo (string)$form["id"] ?>");
                            } );
                        }

                    }
                }
            }


            $(".editonep<?php echo $kunik ?>Params<?php echo $key ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+'.placeholdersckb';
                //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>oneParamsPlaceholders<?php echo $key ?>);
            });

            <?php
            }
            }
            }
            ?>

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+'.global';
                //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
                dyFObj.openForm(sectionDyf.<?php echo $kunik ?>Params);
            });

            <?php if(isset($dependedOn) && $dependedOn!=""){ ?>
            let nbCheckbox<?php echo $key ?> = 0;

            if($("input:checkbox[name='<?php echo $kunik ?>']").length){
                nbCheckbox<?php echo $key ?> = $("input:checkbox[name='<?php echo $kunik ?>']").length;
            }

            $("#<?php echo "question".$parentForm["params"][$kunik]["global"]["dependOn"] ?>").on("click", "input[type='checkbox']",function(){

                let childCount = 0;
                childCount = $("input:checkbox[name='"+$(this).attr("name")+"']:checked").length;

                if(nbCheckbox<?php echo $key ?> <= childCount){
                    nbCheckbox<?php echo $key ?> = childCount;
                    let checkboxContainer = "";
                    if($("#checkboxContainter<?php echo $kunik ?>").length){
                        checkboxContainer = "#checkboxContainter<?php echo $kunik ?>";
                    }else{
                        checkboxContainer = "#checkboxList<?php echo $kunik ?>";
                    }

                    $(checkboxContainer).append(`
	        	<div class="" style="min-height: 50px;">
	      		<div class="thckd">
	      			<input data-id="<?php echo $kunik ?>"
	      				class="newCkbCo ckbCo <?php echo $kunik ?>"
	      				id="<?php echo $kunik ?>${childCount}"
	      				data-form="<?php echo $form["id"] ?>"
	      				type="checkbox"
	      				name="<?php echo $kunik ?>"
	      				data-type="${$(this).data("type")}"
	      				value="${$(this).val()}"/>
	      			<label for="<?php echo $kunik ?>${childCount}">
	  						<span></span>
	  						<span>${$(this).val()}</span>
	  					</label>
	  				</div></div>`);

                    $(".newCkbCo.<?php echo $kunik ?>").on("change", function(){
                        var allckd = [];
                        $("input:checkbox[name='"+$(this).attr("name")+"']:checked").each(function(){

                            var key = $(this).val();
                            var b = { 'value' : key , 'choiceinfo' : $('input[data-imp="' + $(this).val() + '"]').val() };
                            allckd.push(b);
                        });

                        answer.path = "answers."+$(this).data("form")+"."+$(this).data("id");
                        answer.collection = "answers" ;
                        answer.id = "<?php echo $answer["_id"]; ?>";
                        answer.value = allckd;
                        dataHelper.path2Value(answer , function(params) {
                            //toastr.success('Mise à jour enregistrée');
                        });
                    });
                }else{
                    nbCheckbox<?php echo $key ?> = childCount;
                    if($("input[value='"+$(this).val()+"'][name='<?php echo $kunik ?>']").prop(":checked")){
                        $("input[value='"+$(this).val()+"'][name='<?php echo $kunik ?>']").change();
                    }
                    $("input[value='"+$(this).val()+"'][name='<?php echo $kunik ?>']").parent().parent().remove();
                }
            });


            $(".goto<?php echo $kunik ?>").on("click", function(){
                scrollintoDiv($(this).data('path'), 2000);
            });
            //#question<?php echo str_replace("multiCheckboxPlus","",$kunik); ?>


            <?php } ?>
        });
    </script>
    <?php
}
?> <!-- else 164 -->
