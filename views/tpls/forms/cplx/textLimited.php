<?php
$paramsData = [
	"defaultValue" => "",
	"type" => $type,
	"rules" => [],
	"minValue" => 0,
	"maxValue" => 0
];
if(!empty($input["defaultValue"]))
	$paramsData["defaultValue"] = $input["defaultValue"];
if(isset($parentForm["params"][$kunik]["minValue"]))
	$paramsData["minValue"] = $parentForm["params"][$kunik]["minValue"];
if(isset($parentForm["params"][$kunik]["maxValue"]))
	$paramsData["maxValue"] = $parentForm["params"][$kunik]["maxValue"];

$paramsData["rules"] = [
	"minValue" => $paramsData["minValue"],
	"maxValue" => $paramsData["maxValue"]
];
$value = (!empty($answers) && is_string($answers)) ? ' value="'.$answers.'" ' : ' value="'.@$paramsData["defaultValue"].'" ';
$inpClass = "form-control";

if($mode == "r" || $mode == "pdf"){ ?>
    <div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
        <label for="<?php echo $kunik ?>">
        	<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
        		<?php echo $label ?>
        	</h4>
        </label><br/>
        <?php
			if($type=="url"){
				echo '<a href="'.$answers.'" target="_blank">'.$answers.'</a>';
			}else{
				echo $answers;
			}
		 ?>
    </div>
<?php
}else{
?>
	<div class="col-md-12 no-padding">
			<div class="form-group">
	    <label for="<?php echo $key ?>">
	    	<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>" class="<?= $canEditForm && $mode="fa" ? "" : $type ?>">
	    		<?php echo $label.$editQuestionBtn ?>
				<?php if($canEditForm){ ?>
                        <a href="javascript:;" class="btn btn-xs btn-danger config<?= $kunik ?>"><i class="fa fa-cog"></i></a>
                    <?php } ?>
	    	</h4>
	    </label>
	    <?php if(!empty($info)){ ?>
	    	<br/>
	    	<small id="<?php echo $key ?>Help" class="form-text text-muted">
	    		<?php echo $info ?>
	    	</small>
	    <?php } ?>
	    <br/>
	    <input type="number"
	    		class="<?php echo $inpClass ?>"
	    		id="<?php echo $key ?>"
	    		aria-describedby="<?php echo $key ?>Help"
	    		placeholder="<?php echo (isset($placeholder)) ? $placeholder : '' ?>"
	    		data-form='<?php echo $form["id"] ?>'  <?php echo @$value ?>
	    />
	    <div class="rules-container<?php echo $key ?>" data-rules="<?php echo str_replace('"', "'", json_encode($paramsData['rules'])) ?>">

		</div>
	</div>

	</div>

<?php } ?>


<?php if($mode != "pdf"){ ?>
<script type="text/javascript">
	const thisKunik = <?= json_encode($kunik) ?>;
	const thisKey = <?= json_encode($key) ?>;
	const parentFormId = <?= isset($parentForm["_id"]) ? json_encode((string) $parentForm["_id"]) : json_encode(null) ?>;
	let thisParamsData = <?= json_encode($paramsData) ?>;
	if(typeof thisConfigValue == "undefined")
		var thisConfigValue = {}
	thisConfigValue[thisKunik] = {};

	if(typeof checkMinMax == "undefined") {
		function checkMinMax(args) {
			let res = false;
			mylog.log("min max", args.min, args.max);
			if(typeof args != "undefined" && args.min && args.max) {
				if(args.max > 0 && args.min < args.max)
					res = true;
				if(args.max === 0)
					res = true;
			}
			return res;
		}
	}

	jQuery(document).ready(function() {

		$(".config"+thisKunik).off("click").on("click", function() {
			$("#coformRightPanel").empty();
			mylog.log("check val", thisParamsData)
			coformBuilder?.initTabs ? coformBuilder.initTabs({
				container : "#coformRightPanel",
				headerTitle : <?= json_encode($label) ?> + " config",
				kunik : thisKunik,
				onSave : () => {
					if(thisConfigValue[thisKunik] && (thisConfigValue[thisKunik]["minValue"] || thisConfigValue[thisKunik]["maxValue"])) {
						let configToSend = {
							id: parentFormId,
							collection : "forms",
							path : "params."+thisKunik,
							value : thisConfigValue[thisKunik],
						}
						if(thisConfigValue[thisKunik]["minValue"] && thisConfigValue[thisKunik]["maxValue"] && typeof checkMinMax != "undefined") {
							if(checkMinMax({
								min : thisConfigValue[thisKunik]["minValue"]*1,
								max : thisConfigValue[thisKunik]["maxValue"]*1
							})) {
								configToSend.setType = [
									{
										path : "minValue",
										type : (thisConfigValue[thisKunik]["minValue"] + "").indexOf(".") > -1 ? "float" : "int"
									},
									{
										path : "maxValue",
										type : (thisConfigValue[thisKunik]["maxValue"] + "").indexOf(".") > -1 ? "float" : "int"
									}
								]
								dataHelper.path2Value( configToSend, function(params) {
									$("#coformRightPanel").empty();
									$(".btn-toggle-coformbuilder-sidepanel").trigger("click");
									toastr.success(trad["Properties updated successfully"])
									reloadInput("<?= $key ?>", "<?= (string)$form["id"] ?>");
								} );
							} else {
								toastr.warning(trad["Maximum value must be greater than minimum"])
							}
						} else {
							configToSend.setType = {};
							if(thisConfigValue[thisKunik]["minValue"]) {
								configToSend.setType = (thisConfigValue[thisKunik]["minValue"] + "").indexOf(".") > -1 ? "float" : "int";
								configToSend.path += ".minValue";
								configToSend.value = thisConfigValue[thisKunik]["minValue"]
								if(thisParamsData["maxValue"] > 0) {
									if(!checkMinMax({
										min: configToSend.value,
										max: thisParamsData["maxValue"]
									})) {
										toastr.warning(trad["Maximum value must be greater than minimum"])
										return false
									}
								}
							}
							if(thisConfigValue[thisKunik]["maxValue"]) {
								configToSend.setType = (thisConfigValue[thisKunik]["maxValue"] + "").indexOf(".") > -1 ? "float" : "int";
								configToSend.path += ".maxValue";
								configToSend.value = thisConfigValue[thisKunik]["maxValue"];
								if(thisParamsData["maxValue"] > 0) {
									if(!checkMinMax({
										min: thisParamsData["minValue"],
										max: configToSend.value
									})) {
										toastr.warning(trad["Maximum value must be greater than minimum"])
										return false
									}
								}
							}
							dataHelper.path2Value( configToSend, function(params) {
								$("#coformRightPanel").empty();
								$(".btn-toggle-coformbuilder-sidepanel").trigger("click");
								toastr.success(trad["Properties updated successfully"]);
								reloadInput("<?= $key ?>", "<?= (string)$form["id"] ?>");
							} );
						}
					} else {
						toastr.error(trad["somethingwrong"])
					}
				}
			}) : "";
			var coInput = new CoInput({
				//options
				container:"#coformRightPanel .side-tabs-body",
				inputs:[
					{
						type : "inputSimple",
						options: {
							name : "minValue",
							label : trad["Minimum value"],
							type : "number",
							defaultValue : thisParamsData.minValue ? thisParamsData.minValue : "0",
							icon : "chevron-down",
						},
					},
					{
						type : "inputSimple",
						options: {
							name : "maxValue",
							label : trad["Maximum value"],
							type : "number",
							defaultValue : thisParamsData.maxValue ? thisParamsData.maxValue : "0",
							icon : "chevron-down",
						},
					}
				],
				onchange:function(name, value){
					$("#coformRightPanel .save-coform").removeClass("disabled");
					if(thisConfigValue[thisKunik])
						jsonHelper.setValueByPath(thisConfigValue[thisKunik], name, value*1)
				}
			})
			if(!$(".coformbuilder-side-content.coformbuilder-right-content").hasClass("active"))
				$(".btn-toggle-coformbuilder-sidepanel").trigger("click")
		});
		$(`input#${thisKey}`).off("blur").on("blur", function(event){
			var tthis = $(this);
            event.preventDefault();
            const myKey = $(this).attr('id');
            var parentElem = $('div#question_'+myKey);
            var errorElem = parentElem.find(`div.rules-container${myKey}`);
            const rulesAttr = parentElem.find(`div.rules-container${myKey}`).attr('data-rules');
            var rules = {};
            var strError = '';
            var hasError = false;
            if(typeof rulesAttr != 'undefined') {
                rules = JSON.parse(rulesAttr.replace(/'/g, '"'));
            }
            if(notEmpty(rules)) {
                if(tthis.val().trim()*1 < rules.minValue) {
                    strError = `<small class="text-danger">${ trad["The value must not be less than"] } ${ rules.minValue }</small> </br>`;
                    hasError = true;
                }
                if(rules.maxValue && rules.maxValue > 0 && tthis.val().trim()*1 > rules.maxValue) {
                    strError = `<small class="text-danger">${ trad["The value must not be greater than"] } ${ rules.maxValue }</small> </br>`;
                    hasError = true;
                }
            }
			mylog.log("check val", tthis.val().trim()*1)
            if(hasError) {
                tthis.css('border-color', '#a94442')
            } else {
                tthis.css('border-color', '#ddd')
            }
            errorElem.empty();
            errorElem.append(strError);

            let answerToSend = {
                collection : "answers",
                id : coForm_FormWizardParams.answer._id.$id,
                path : "answers."+$(this).attr("form")+"."+$(this).attr("id")
            };

            if(answerObj.form)
                answerToSend.path = "answers."+$(this).data("form")+"."+$(this).attr("id");

			answerToSend.value = $(this).val().trim()*1;
			answerToSend.setType = (answerToSend.value + "").indexOf(".") > -1 ? "float" : "int";

            if(!hasError) {
                dataHelper.path2Value( answerToSend , function(params) {
					toastr.success('Mise à jour enregistrée');
                    saveLinks(answerObj._id.$id,"updated",userId);
                    newReloadStepValidationInputGlobal({
                        inputKey : tthis.attr("id")
                    })
                } );
            }
		})
	});
</script>
<?php } ?>

