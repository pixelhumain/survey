<?php if($answer){ 
$debug = false;
$editBtnL = (Yii::app()->session["userId"] == $answer["user"]) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";


$editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='costum.form.params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";


$paramsData = [ 
	"buy" => true 
];

if( isset( $parentForm["params"][$kunik]["buy"]) ) 
	$paramsData["buy"] =  $parentForm["params"][$kunik]["buy"];


// if(isset($answers)){
// 	foreach ($answers as $q => $a) {
// 		if(isset($a["group"]))
// 			$paramsData["group"][] = $a["group"];
// 	}
// }


$properties = [
		"what" => [
            "inputType" => "text",
            "label" => "Qu'offrez vous",
            "placeholder" => "Nom du Produit",
            "rules" => [ "required" => true  ]
        ],
        "price" => [
            "inputType" => "text",
            "label" => "Prix",
            "placeholder" => "Prix",
            "rules" => [ "required" => true  ]
        ],
        "qtyMax" => [
            "inputType" => "text",
            "placeholder" => "Quantité Restante / Commandée",
            "label" => "Quantité Maximum"
        ],
        "unit" => [
            "inputType" => "text",
            "placeholder" => "Unité",
            "label" => "Unité"
        ]
    ];
    
    if($debug)var_dump($answers);
    if($debug)var_dump($paramsData);
?>	

<?php
echo $this->renderPartial("survey.views.tpls.forms.cplx.offresView",
	                      [ 
	                        "form" => $form,
	                        "wizard" => true, 
	                        "answers"=>$answers,
	                        "answer"=>$answer,
                            "mode" => $mode,
                            "kunik" => $kunik,
                            "key" => $key,
                            "titleColor" => $titleColor,
                            "properties" => $properties,


                            "label" => $label,
                            "editQuestionBtn" => $editQuestionBtn,
                            "editParamsBtn" => $editParamsBtn,
                            "editBtnL" => $editBtnL,
                            "info" => $info,
	                        //"showForm" => $showForm,
	                        "paramsData" => $paramsData,
	                        "canEdit" => $canEdit,  
	                        //"el" => $el 
	                    ] ,true );

?>

<?php if( $paramsData["buy"] ) {  ?>
<div class="form-buy" style="display:none;">
	Pour : <br/>
	<input type="text" id="namebuy" name="namebuy" style="width:100%;">
	acheteur éxistants<select id="namebuylist">
		<option value=""></option>
		<?php 
		$buyers = [];
		foreach ($form["inputs"] as $k => $inp) {
			if( $inp["type"] == "tpls.forms.cplx.offres" ){
				if(isset($answer["answers"][$form["id"]][$k])){
					foreach ($answer["answers"][$form["id"]][$k] as $ki => $line) {
						if(isset($line["buys"])){
							foreach ($line["buys"] as $idx => $b) {
								if(!in_array($b["name"],$buyers)){
									$buyers[] = $b["name"];
									echo "<option value='".$b["name"]."'>".$b["name"]."</option>";
								}
							}
						}
					}
				}
			}
		} ?>
	</select><br/>
	Quantité : <br/>
	<input type="text" id="qtybuy" name="qtybuy" style="width:100%;">
</div>
<?php } 

if($mode != "r"){
?>

<script type="text/javascript">

var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

$(document).ready(function() { 
	
	sectionDyf.<?php echo $kunik ?> = {
		"jsonSchema" : {	
	        "title" : "Votre Offre",
            "icon" : "fa-question-o",
            "text" : "Décrire ce que vous avez à proposer</b>.",
	        "properties" : <?php echo json_encode( $properties ); ?>,
	        save : function () {  
	        	var today = new Date();
	            tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
	            $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });

	            mylog.log("save tplCtx",tplCtx);
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");//$("#ajax-modal").modal('hide');
	                    location.reload();
	                } );
	            }

	    	}
	    }
	};

    mylog.log("render","/modules/costum/views/tpls/forms/cpl/<?php echo $kunik ?>.php");


    //adds a line into answer
    $(".add<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
    });

    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
    });



<?php if( $paramsData["buy"] ) {  ?>

    $('.btnbuy').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.key = $(this).data("key");
		tplCtx.form = $(this).data("form");
		prioModal = bootbox.dialog({
	        message: $(".form-buy").html(),
	        title: "Voter pour partie",
	        show: false,
	        buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {

                    	tplCtx.path = "answers";
			        	if( notNull(formInputs [tplCtx.form]) )
			        		tplCtx.path = "answers."+tplCtx.form;    
			        	
			        	var uId = "";
			        	var uName = "";
			        	if( $(".bootbox #namebuy").val() != "" ) {
			        		uId = slugify( $(".bootbox #namebuy").val() );
			        		uName = $(".bootbox #namebuy").val();
			        	}
			        	else if( $(".bootbox #namebuylist").val() != "" ) {
			        		uId = slugify( $(".bootbox #namebuylist").val());
							uName = $(".bootbox #namebuylist").val();
						}
			        	else {
			        		uId = userId;
			        		uName = userConnected.name;
			        	}
			        	

			        	tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".buys."+uId; 

			        	var today = new Date();
						today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();	
			            tplCtx.value = {
			            	qty : $(".bootbox #qtybuy").val(),
			            	name :  uName,
			            	date : today
			            };

				    	mylog.log("btnbuy save",tplCtx);
				  	 	dataHelper.path2Value( tplCtx, function(){
				  	 		saveLinks(answerObj._id.$id,"purchase",userId);
				  	 		closePrioModalRel();
				  	 	} );
			        }		
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: closePrioModal
                }
              },
		        onEscape: closePrioModal
	    });
	    prioModal.modal("show");
	});


	$('.btnValidateDelivery').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.key = $(this).data("key");
		tplCtx.form = $(this).data("form");
		tplCtx.qty = $(this).data("qty");
		$(this).removeClass('btn-default').addClass("btn-success");

		tplCtx.pathBase = "answers";
    	if( notNull(formInputs [tplCtx.form]) )
    		tplCtx.pathBase = "answers."+tplCtx.form;    

    	tplCtx.path = tplCtx.pathBase+"."+tplCtx.key+"."+tplCtx.pos+".buys."+$(this).data("uid")+".done"; 	
        tplCtx.value = true;

		mylog.log("btnValidateDelivery save",tplCtx);
  	 	dataHelper.path2Value( tplCtx, function(){
  	 		toastr.success('ok!');
  	 	} );
	});	

	$('.btnOrderState').off().click(function() { 
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.value= $(this).data("value");
		tplCtx.path = "state"; 	
		
		bootbox.dialog({
          title: "Confirmez",
          message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
          buttons: [
            {
              label: "Ok",
              className: "btn btn-primary pull-left",
              callback: function() {
                mylog.log("btnValidateDelivery save",tplCtx);
		  	 	dataHelper.path2Value( tplCtx, function(){
		  	 		toastr.success('ok!');
		  	 		location.reload();
		  	 	} );
              }
            },
            {
              label: "Annuler",
              className: "btn btn-default pull-left",
              callback: function() {}
            }
          ]
      });

    	

		
	});	
<?php } ?>
function closePrioModal(){
	prioModal.modal('hide');
}
function closePrioModalRel(){
	closePrioModal();
	location.reload();
}

});
</script>
<?php } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} 


}

?>