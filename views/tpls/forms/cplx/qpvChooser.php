<?php 
if($saveOneByOne)
	$inpClass = " ";
	$ignore = array('_file_', '_params_', '_obInitialLevel_' ,'ignore', 'configForm', 'steps', 'context', 'contextConfig', 'communityCitoyenGroup', 'step', 'coformQuestionIcons', 'userAnswers');
	$paramsCheck = array_diff_key(get_defined_vars() + array_flip($ignore), array_flip($ignore));
?>
<style>
	/* The container */
	.container<?= $kunik ?> {
	display: block;
	position: relative;
	padding-left: 35px;
	margin-bottom: 12px;
	cursor: pointer;
	font-size: 22px;
	/*-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;*/
	}

	/* Hide the browser's default checkbox */
	.container<?= $kunik ?> input {
	position: absolute;
	opacity: 0;
	cursor: pointer;
	height: 0;
	width: 0;
	}

	/* Create a custom checkbox */
	.checkmark<?= $kunik ?> {
	position: absolute;
	top: 0;
	left: 0;
	height: 25px;
	width: 25px;
	background-color: #eee;
	}

	/* On mouse-over, add a grey background color */
	.container<?= $kunik ?>:hover input ~ .checkmark<?= $kunik ?> {
	background-color: #ccc;
	}

	/* When the checkbox is checked, add a blue background */
	.container<?= $kunik ?> input:checked ~ .checkmark<?= $kunik ?> {
	background-color: #2196F3;
	}

	/* Create the checkmark/indicator (hidden when not checked) */
	.checkmark<?= $kunik ?>:after {
	content: "";
	position: absolute;
	display: none;
	}

	/* Show the checkmark when checked */
	.container<?= $kunik ?> input:checked ~ .checkmark<?= $kunik ?>:after {
	display: block;
	}

	/* Style the checkmark/indicator */
	.container<?= $kunik ?> .checkmark<?= $kunik ?>:after {
	left: 9px;
	top: 5px;
	width: 5px;
	height: 10px;
	border: solid white;
	border-width: 0 3px 3px 0;
	-webkit-transform: rotate(45deg);
	-ms-transform: rotate(45deg);
	transform: rotate(45deg);
	}
	.qpv-chooser<?= $kunik ?>.form-check.text-left{
		height:500px;
		overflow:auto;
		display:flex;
		flex-direction:column
	}
	.qpv-chooser<?= $kunik ?> .search-quartier,.qpv-chooser<?= $kunik ?> .input-group-addon{
		background-color: #787878fa;
		color:#fff;
	}
	.qpv-chooser<?= $kunik ?> .search-quartier::placeholder{
		color:#fff;
	}
	.qpv-chooser<?= $kunik ?> .form-control:focus{
		border: none;
		box-shadow: none;
	}
</style>
<?php  
    if($mode == "r" || $mode == "pdf"){ ?>
    <div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
        <label for="<?php echo $kunik ?>">
        	<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
        		<?php echo $label.$counQPV ?>
        	</h4>
        </label><br/>
		<ul>
		<?php foreach ($answers as $kans => $vans) {
			echo "<li>".$vans."</li>";
		} ?>
		</ul>
    </div>
<?php }else{ ?>
	<div id="qpv-chooser-container<?= $kunik ?>" class="no-padding">
		
	</div>

	<script type="text/javascript">
	coInterface.showLoader("#qpv-chooser-container<?= $kunik ?>");
	var inputGlobalParams = <?= json_encode($paramsCheck); ?>;
	const inputGlopalParentForm = inputGlobalParams?.parentForm;
	const inputGlobalParamsForm = inputGlobalParams?.form;
	inputGlobalParams?.parentForm ? delete inputGlobalParams.parentForm : ""
	inputGlobalParams?.form ? delete inputGlobalParams.form : ""
	inputGlobalParams.parentForm = {
		"_id" : inputGlopalParentForm?.['_id']?.['$id']
	}
	inputGlobalParams.form = {
		id : inputGlobalParamsForm?.id
	}
	if(inputGlobalParams?.answer?.["_id"]?.["$id"])
		inputGlobalParams.answer["_id"] = inputGlobalParams?.answer?.["_id"]?.["$id"]
	ajaxPost(
		"#qpv-chooser-container<?= $kunik ?>",
		baseUrl+'/survey/form/getinput/request/getQPVChooser',
		{
			inputGlobalParams : inputGlobalParams
		},
		function() {
			stepObj && stepObj.bindEvent.inputEvents ? stepObj.bindEvent.inputEvents(stepObj) : ""
		},
		"html"
	);
	</script>
<?php } ?>