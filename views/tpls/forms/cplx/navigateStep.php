<div id="navigateStep" class="col-xs-12 text-center padding-20 ">

<?php 

$isFormNavigable = isset($parentForm["subForms"]);
$buttonClass = " btn btn-primary text-white padding-top-10 padding-bottom-10 padding-right-30 padding-left-30 margin-right-5 margin-left-5 ";
if($isFormNavigable){
    $formSteps = $parentForm["subForms"];
    $currentIndex = array_search($form["id"], $parentForm["subForms"]);

    if($currentIndex > 0){
        echo '<a href="javascript:;" class="'.$buttonClass.' navigateStep'.$kunik.'" data-step="'.$formSteps[$currentIndex-1].'">'.Yii::t("common", "Previous step").'</a>';
    }

    if($currentIndex < count($formSteps)-1){
        echo '<a href="javascript:;" class="'.$buttonClass.' navigateStep'.$kunik.'" data-step="'.$formSteps[$currentIndex+1].'">'.Yii::t("common", "Next step").'</a>';
    }

    if($currentIndex == count($formSteps)-1){
        echo '<a href="javascript:;" class="'.$buttonClass.' navigateStep'.$kunik.'" data-step="finish">'.Yii::t("survey", "Send").'</a>';
    }
} 
?>

</div>

<script>
    (function(){
        let isValidated = "<?php echo isset($answer["validated"])?$answer["validated"]:false ?>";
        $(".navigateStep<?php echo $kunik ?>").off().on("click",function(e) {
            if($(this).data("step")=="finish"){
                let message = "<?php echo Yii::t("common", "Thank you for participating in our survey! Your responses have been recorded") ?>";
                if(isValidated){
                    message = "<?php echo Yii::t("common", "Thank you for participating in our survey! Your responses have been recorded") ?>";
                    bootbox.alert("<div style='font-size:25pt;' class='text-center'><i style='color:green;font-size:40pt;' class='fa fa-check-circle'></i><br/><p>"+message+"</p></div>");

                }else{
                    tplCtx = {};
                    tplCtx.id = "<?php echo $answer["_id"] ?>";
                    tplCtx.path = "validated";
                    tplCtx.collection = "<?php echo Form::ANSWER_COLLECTION ?>";
                    tplCtx.value = true;

                    dataHelper.path2Value( tplCtx, function(params) {
                        reloadWizard();
                        bootbox.alert("<div style='font-size:25pt;' class='text-center'><i style='color:green;font-size:40pt;' class='fa fa-check-circle'></i><br/><p>"+message+"</p></div>");
                    });
                }
            }else if(typeof $(this).data("step") != "undefined"){
                showStepForm('#'+$(this).data("step"));
            }
        });
    })()
    
</script>