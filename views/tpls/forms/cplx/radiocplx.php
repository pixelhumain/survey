
<style type="text/css">
    .rdo-grp {
        /*position: absolute;
        top: calc(50% - 10px);*/
    }
    .rdo-grp label {
        cursor: pointer;
        -webkit-tap-highlight-color: transparent;
        /* padding: 6px 8px; */
        border-radius: 20px;
        float: left;
        transition: all 0.2s ease;
    }
    .rdo-grp label:hover {
        background: rgba(125,100,247,0.06);
    }
    .rdo-grp label:not(:last-child) {
        margin-right: 16px;
    }
    .rdo-grp label span {
        vertical-align: middle;
    }
    .rdo-grp label span:first-child {
        position: relative;
        display: inline-block;
        vertical-align: middle;
        width: 27px;
        height: 27px;
        background: #e8eaed;
        border-radius: 50%;
        transition: all 0.2s ease;
        margin-right: 8px;
    }
    .rdo-grp label span:first-child:after {
        content: '';
        position: absolute;
        width: 19px;
        height: 19px;
        margin: 4px;
        background: #fff;
        border-radius: 50%;
        transition: all 0.2s ease;
    }
    .rdo-grp label:hover span:first-child {
        background: #7d64f7;
    }
    .rdo-grp input {
        display: none;
    }
    .rdo-grp input:checked + label span:first-child {
        background: #7d64f7;
    }
    .rdo-grp input:checked + label span:first-child:after {
        transform: scale(0.7);
    }

    .effect-1{border: 0; padding: 7px 0; border-bottom: 1px solid #ccc;}

    .effect-1 ~ .focus-border{position: absolute; bottom: 0; left: 0; width: 0; height: 2px; background-color: #3399FF; transition: 0.4s;}
    .effect-1:focus ~ .focus-border{width: 100%; transition: 0.4s;}

    .paramsonebtn , .paramsonebtnP {
        font-size: 17px;
        display: none;
        padding: 5px;
    }

    .paramsonebtn:hover {
        color: red;
    }

    .paramsonebtnP:hover {
        color: blue;
    }

    .thradio:hover .paramsonebtn, .thradio:hover .paramsonebtnP {
        display: inline-block;
    }

    .effect-1:focus {
        outline: none !important;
    }

    .responsemultiradio:before{
        content: '\f0a9';
        margin-right: 15px;
        font-family: FontAwesome;
        color: #d9534f;
    }

</style>



<?php
$cle = $key ;
$value = (!empty($answers)) ? " value='".$answers."' " : "";
$inpClass = " saveOneByOne";

$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";



$paramsData = [
    "list" => [	],
    "tofill" => [ ],
    "placeholdersradio" => [ ],
    "type" => [
        "simple" => "Sans champ de saisie",
        "cplx" => "Avec champ de saisie"
    ],
    "width" => [
        "12" => "1",
        "6" => "2",
        "4" => "3",
        "2" => "6",
        "1" => "12"
    ]
];

if (isset($input["conditional"])) {
    foreach ($input["conditional"] as $key => $value) {

        ?>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                $("#<?php echo $value["value"] ?>").change(function() {
                    if($(this).prop('checked')) {
                        $('#question<?php echo $value["input"] ?>').removeClass("hide");
                        scrollintoDiv("question<?php echo $value["input"] ?>", 1000);
                    }
                });
                if($("#<?php echo $value["value"] ?>").is(':checked')) {
                    $('#question<?php echo $value["input"] ?>').removeClass("hide");
                    // scrollintoDiv("question<?php echo $value["input"] ?>");
                }

                $('.radioCo.<?php echo $kunik ?>').change(function(){
                    if(!$("#<?php echo $value["value"] ?>").prop('checked')) {
                        $('#question<?php echo $value["input"] ?>').addClass("hide");
                    }
                });

            });
        </script>
        <style type="text/css">
            #question<?php echo $value["input"] ?> {
                -webkit-transition: all 2s ease;
                -moz-transition: all 2s ease;
                -o-transition: all 2s ease;
                transition: all 2s ease;
            }
        </style>

        <?php
    }
}

if( isset($parentForm["params"][$kunik]) ) {
    if( isset($parentForm["params"][$kunik]["global"]["list"]) ) {
        $paramsData["list"] =  $parentForm["params"][$kunik]["global"]["list"];
        foreach ($paramsData["list"] as $k => $v) {
            if(isset($parentForm["params"][$kunik]["tofill"][$v]) ){
                $paramsData["tofill"] += array($v => $parentForm["params"][$kunik]["tofill"][$v]);
            } else {
                $paramsData["tofill"] += [$v => "simple"];
            }
        }
    }
}



if( isset($parentForm["params"][$kunik]) ) {
    if( isset($parentForm["params"][$kunik]["global"]["list"]) ) {
        //$paramsData["list"] =  $parentForm["params"][$kunik]["global"]["list"];
        foreach ($paramsData["list"] as $k => $v) {
            if(isset($parentForm["params"][$kunik]["placeholdersradio"][$v]) ){
                $paramsData["placeholdersradio"] += array($v => $parentForm["params"][$kunik]["placeholdersradio"][$v]);
            } else {
                $paramsData["placeholdersradio"] += [$v => ""];
            }
        }
    }
}


if($mode == "r")
{
    ?>
    <div class="text-left" style="padding-top: 50px; ">
        <label class="form-check-label" for="<?php echo $key ?>">
            <h4 style=" color: <?php echo ($titleColor) ? $titleColor : "black"; ?>;"> <?php echo $label?>
            </h4>
        </label>


        <?php
        if( !isset($parentForm["params"][$kunik]["global"]["list"]) )
        {
            echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> THIS FIELD HAS TO BE CONFIGURED FIRST </span>";
        } else
        {
            ?>

            <div class="">
                <div class="">
                    <?php
                        foreach ($parentForm["params"][$kunik]["global"]["list"] as $ix => $lbl)
                        {

                            if(isset($answer["answers"][$form["id"]][$kunik]["value"]) && $answer["answers"][$form["id"]][$kunik]["value"] == $lbl)
                            {

                                ?>
                                <!-- <tr class="thradio"><theih > -->
                                <div class="" style="min-height: 50px;">
                                    <div class="">

                                        <label for="<?php echo $kunik.$ix ?>" class="responsemultiradio">
                                            <span>

                                            </span>
                                            <span>
                                                <?php echo $lbl ?>
                                                <?php
                                                if(count($paramsData["tofill"]) != 0)
                                                {
                                                    foreach ($paramsData["tofill"] as $key => $value)
                                                    {
                                                        if($lbl == $key && $value == "cplx")
                                                        {
                                                            ?>
                                                            <?php
                                                            if(isset($answer["answers"][$form["id"]][$kunik]["choiceinfo"]) && isset($answer["answers"][$form["id"]][$kunik]["value"]) && $answer["answers"][$form["id"]][$kunik]["value"] == $key ){
                                                                echo $answer["answers"][$form["id"]][$kunik]["choiceinfo"] ;
                                                            } ?>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                            ?>

                                            </span>
                                        </label>
                                    </div>
                                </div>
                            <?php
                            } 
                        }
                    ?>
                </div>
            </div>
            <?php
        }
        ?>

        <?php
        if(!empty($info))
        {
            ?>
            <small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
            <?php
        }
        ?>
    </div>

    <?php
        }else
        {
    ?>


    <div class="text-left" style="padding-top: 50px; ">
        <label class="form-check-label" for="<?php echo $key ?>">
            <h4 style=" color: <?php echo ($titleColor) ? $titleColor : "black"; ?>;"><?php echo $label.$editQuestionBtn.$editParamsBtn ?>
            </h4>
        </label>


        <?php
        if( !isset($parentForm["params"][$kunik]["global"]["list"]) )
        {
            echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> THIS FIELD HAS TO BE CONFIGURED FIRST ".$editParamsBtn."</span>";
        } else
        {
            ?>

            <div class="rdo-grp">
                <div class="">
                    <?php
                    foreach ($parentForm["params"][$kunik]["global"]["list"] as $ix => $lbl)
                    {
                        $ckd =  "";
                        if(isset($answer["answers"][$form["id"]][$kunik]["value"]) && $answer["answers"][$form["id"]][$kunik]["value"] == $lbl)
                        {
                            $ckd =  "checked";
                        }

                        ?>
                                <!-- <tr class="thradio"><theih > -->
                        <div class="" style="min-height: 50px;">
                            <div class="thradio">
                                <input data-id="<?php echo $kunik ?>" class=" radioCo <?php echo $kunik ?>"  id="<?php echo $kunik.$ix ?>" data-form='<?php echo $form["id"] ?>' <?php echo $ckd?> type="radio" name="<?php echo $kunik ?>" data-type="<?php

                                if(count($paramsData["tofill"]) != 0)
                                {
                                    foreach ($paramsData["tofill"] as $key => $value)
                                    {
                                        if($lbl == $key)
                                        {
                                            if($value == "cplx")
                                            {
                                                echo "cplx";
                                            }else
                                            {
                                                echo "simple";
                                            }
                                        }
                                    }
                                }
                                ?>" value="<?php echo $lbl ?>" />

                                <label for="<?php echo $kunik.$ix ?>">
                                    <span>

                                    </span>
                                    <span><?php echo $lbl ?>
                                        <div style="position: relative; display: inline-block;">

                                            <?php
                                            if(count($paramsData["tofill"]) != 0)
                                            {
                                                foreach ($paramsData["tofill"] as $lilkey => $value)
                                                {
                                                    if($lbl == $lilkey && $value == "cplx")
                                                    {
                                                        ?>
                                                        <input class="effect-1 inputRadioCo <?php echo $kunik ?>" data-id="<?php echo $kunik ?>" data-form='<?php echo $form["id"] ?>' type="text" data-imp="<?php echo $lilkey; ?>" placeholder="<?php echo $paramsData['placeholdersradio'][$lilkey]; ?>" style="display: inline-block !important; position: relative;"  value="<?php
                                                        if(isset($answer["answers"][$form["id"]][$kunik]["choiceinfo"]) && isset($answer["answers"][$form["id"]][$kunik]["value"]) && $answer["answers"][$form["id"]][$kunik]["value"] == $lilkey ){
                                                            echo $answer["answers"][$form["id"]][$kunik]["choiceinfo"] ;
                                                        } ?>" >

                                                        <span class="focus-border"></span>
                                                        <!-- az -->
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>
                                        </div>
                                        <?php

                                        if($canEditForm)
                                        {


    // <<<<<<< HEAD
                                            echo " <a href='javascript:;' data-id='".$parentForm["_id"]."'  data-collection='".Form::COLLECTION."'  data-path='params.".$kunik."' class='previewTpl paramsonebtn editone".$kunik."Params".$ix." '><i class='fa fa-cog'></i> </a>";
    // =======
    // 		  								echo " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl paramsonebtn editone".$kunik."Params".$ix." '><i class='fa fa-cog'></i> </a>";
    // >>>>>>> 129c8a9de4164acd99bc3d39bf09929f69b9bbaa



                                            if(count($paramsData["tofill"]) != 0)
                                            {
                                                foreach ($paramsData["tofill"] as $lilkey => $value)
                                                {
                                                    if($lbl == $lilkey)
                                                    {
                                                        if($value == "cplx")
                                                        {
    //<<<<<<< HEAD
                                                            echo " <a alt='placeholder' href='javascript:;' data-id='".$parentForm["_id"]."'  data-collection='".Form::COLLECTION."'  data-path='params.".$kunik."' class='previewTpl paramsonebtnP editonep".$kunik."Params".$ix." '><i class='fa fa-pencil'></i> </a>";
    // =======
    // 		  												echo " <a alt='placeholder' href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl paramsonebtnP editonep".$kunik."Params".$ix." '><i class='fa fa-pencil'></i> </a>";
    // >>>>>>> 129c8a9de4164acd99bc3d39bf09929f69b9bbaa
                                                        }
                                                    }
                                                }
                                            }

                                        }
                                        ?>
                                    </span>
                                </label>
                            </div>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        <?php
        }
        ?>

        <?php
        if(!empty($info))
        {
            ?>
            <small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
            <?php
        }
        ?>
    </div>

    <script type="text/javascript">
        var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answer["answers"][$kunik])) ? $answer["answers"][$kunik] : null ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;



        sectionDyf.<?php echo $kunik ?>ParamsPlace = "";


        jQuery(document).ready(function() {
            mylog.log("render form input","/modules/costum/views/tpls/forms/cplx/multiCheckbox.php");

            answer = {};
            $('.radioCo.<?php echo $kunik ?>').change(function(){
                if($(this).data("type") == "simple"){
                    answer.path = "answers."+$(this).data("form")+"."+$(this).data("id");
                    answer.collection = "answers" ;
                    answer.id = "<?php echo $answer["_id"]; ?>";
                    answer.value = {"value" : $(this).val()};
                    dataHelper.path2Value(answer , function(params) {
                        //toastr.success('Mise à jour enregistrée');
                    } );
                } else if ($(this).data("type") == "cplx"){
                    answer.path = "answers."+$(this).data("form")+"."+$(this).data("id");
                    answer.collection = "answers" ;
                    answer.id = "<?php echo $answer["_id"]; ?>";
                    answer.value = {"value" : $(this).val() , "choiceinfo" : $('input[data-imp="' + $(this).val() + '"]').val()};
                    dataHelper.path2Value(answer , function(params) {
                        //toastr.success('Mise à jour enregistrée');
                    } );
                }

                var radioChecked = $(this);

                // 		 $('#question<?php echo $cle ?> .radioCo').each(function(){
                //      $(this).prop('checked', false);
                //      alert('sdfsd');
                // });

                // $(this).prop('checked', true);
            });


            $('.inputRadioCo.<?php echo $kunik ?>').blur(function(){
                if($('input[value="' + $(this).data("imp") + '"]').is(':checked')) {

                    answer.path = "answers."+$(this).data("form")+"."+$(this).data("id");

                    answer.collection = "answers" ;
                    answer.id = "<?php echo $answer["_id"]; ?>";
                    answer.value = {"value" : $(this).data("imp") , "choiceinfo" : $(this).val()};
                    dataHelper.path2Value(answer , function(params) {
                        //toastr.success('Mise à jour enregistrée');
                    } );
                }
            });

            $(document).on("blur", ".addmultifield", function() {
                let value = $(this).val();
                let regex = /[.\$]/g;
                if (value.match(regex)) {
                    value = value.replace(regex, "");
                    $(this).val(value);
                    toastr.info("Carractère non autorisé");
                }
            });

            sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema" : {
                    "title" : "<?php echo $kunik ?> config",
                    "icon" : "fa-cog",
                    "properties" : {
                        list : {
                            inputType : "array",
                            label : "Liste de bouton radio",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsData.list
                        },
                        width : {
                            inputType : "select",
                            label : "Nombre d'element par ligne",
                            options :  sectionDyf.<?php echo $kunik ?>ParamsData.width,
                            value : "<?php echo (isset($parentForm["params"][$kunik]['global']['width']) and $parentForm["params"][$kunik]['global']['width'] != "") ? $paramsData["width"][strval($parentForm["params"][$kunik]['global']['width'])] : ''; ?>"
                        }
                    },
                    save : function () {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                            if(val.inputType == "properties")
                                tplCtx.value = getPairsObj('.'+k+val.inputType);
                            else if(val.inputType == "array")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else if(val.inputType == "formLocality")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else
                                tplCtx.value[k] = $("#"+k).val();
                        });
                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
                                reloadInput("<?php echo $cle ?>", "<?php echo (string)$form["id"] ?>");
                            } );
                        }

                    }
                }
            };


            <?php if(count($paramsData["list"]) != 0){
            foreach ($paramsData["list"] as $key => $value) {

            // if($lbl == $key){
            // 	if($value == "cplx"){
            ?>

            sectionDyf.<?php echo $kunik ?>oneParams<?php echo $key ?> = {
                "jsonSchema" : {
                    "title" : "<?php echo $kunik ?> config",
                    "icon" : "fa-cog",
                    "properties" : {
                        tofill : {
                            inputType : "select",
                            label : "Type de l'option",
                            options :  sectionDyf.<?php echo $kunik ?>ParamsData.type,
                            value : "<?php echo (isset($paramsData["tofill"][$value])) ? $paramsData["tofill"][$value] : ''; ?>"
                        }
                    },
                    save : function () {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>oneParams<?php echo $key ?>.jsonSchema.properties , function(ko,valo) {

                            $.each(sectionDyf.<?php echo $kunik ?>ParamsData.tofill, function(ke, va) {

                                if(ke == "<?php echo $value; ?>"){

                                    var azerazer = ke.toString();
                                    sectionDyf.<?php echo $kunik ?>ParamsData.tofill[azerazer] = $("#"+ko).val();
                                }
                            })

                            tplCtx.value = sectionDyf.<?php echo $kunik ?>ParamsData.tofill;


                        });
                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
                                reloadInput("<?php echo $cle ?>", "<?php echo (string)$form["id"] ?>");
                            } );
                        }

                    }
                }
            };

            $(".editone<?php echo $kunik ?>Params<?php echo $key ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+'.tofill';
                //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>oneParams<?php echo $key ?>,null, sectionDyf.<?php echo $kunik ?>oneParams<?php echo $key ?>);
            });
            <?php
            }
            }
            //   	}
            // }
            ?>


            <?php if(count($paramsData["list"]) != 0){
            foreach ($paramsData["list"] as $key => $value) {
            // if($lbl == $key){
            if($paramsData["tofill"][$value] == "cplx"){
            ?>

            sectionDyf.<?php echo $kunik ?>oneParamsPlaceholders<?php echo $key ?> = {
                "jsonSchema" : {
                    "title" : "<?php echo $kunik ?> config",
                    "icon" : "fa-cog",
                    "properties" : {
                        placeholdersradio : {
                            inputType : "text",
                            label : "Modifier les textes à l'interieur du champs de saisie",
                            values :  "<?php echo $paramsData["placeholdersradio"][$value]; ?>"
                        }
                    },
                    save : function () {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>oneParamsPlaceholders<?php echo $key ?>.jsonSchema.properties , function(kk,vall) {


                            $.each(sectionDyf.<?php echo $kunik ?>ParamsData.placeholdersradio, function(ke, va) {
                                if(ke == "<?php echo $value; ?>"){

                                    var azerazer = ke.toString();
                                    sectionDyf.<?php echo $kunik ?>ParamsData.placeholdersradio[azerazer] = $("#"+kk).val();

                                }
                            })


                            tplCtx.value = sectionDyf.<?php echo $kunik ?>ParamsData.placeholdersradio;


                        });
                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
                                reloadInput("<?php echo $cle ?>", "<?php echo (string)$form["_id"] ?>");
                            } );
                        }

                    }
                }
            };

            $(".editonep<?php echo $kunik ?>Params<?php echo $key ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+'.placeholdersradio';
                //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>oneParamsPlaceholders<?php echo $key ?>,null, sectionDyf.<?php echo $kunik ?>oneParamsPlaceholders<?php echo $key ?>);
            });

            <?php
            }
            }
            }
            // }
            ?>

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path")+'.global';
                //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            });


        });
    </script>
<?php } ?>