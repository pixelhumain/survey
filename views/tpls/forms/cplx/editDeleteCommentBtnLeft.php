<style >
    .btnleft{
        list-style-type: none;
    }

    .jumbotron {
        background: #6b7381;
        color: #bdc1c8;
    }
    .jumbotron h1 {
        color: #fff;
    }
    .example {
        margin: 4rem auto;
    }
    .example > .row {
        margin-top: 2rem;
        height: 5rem;
        vertical-align: middle;
        text-align: center;
        border: 1px solid rgba(189,193,200,0.5);
    }
    .example > .row:first-of-type {
        border: none;
        height: auto;
        text-align: left;
    }
    .example h3 {
        font-weight: 400;
    }
    .example h3 > small {
        font-weight: 200;
        font-size: .75em;
        color: #939aa5;
    }
    .example h6 {
        font-weight: 700;
        font-size: .65rem;
        letter-spacing: 3.32px;
        text-transform: uppercase;
        color: #bdc1c8;
        margin: 0;
        line-height: 5rem;
    }
    .example .btn-toggle {
        top: 50%;
        transform: translateY(-50%);
    }
    .btn-toggle {
        margin: 0 4rem;
        padding: 0;
        position: relative;
        border: none;
        height: 1.5rem;
        width: 3rem;
        border-radius: 1.5rem;
        color: #6b7381;
        background: #bdc1c8;
    }
    .btn-toggle:focus, .btn-toggle:focus.active, .btn-toggle.focus, .btn-toggle.focus.active {
        outline: none;
    }
    .btn-toggle:before, .btn-toggle:after {
        line-height: 1.5rem;
        width: 4rem;
        text-align: center;
        font-weight: 600;
        font-size: .75rem;
        text-transform: uppercase;
        letter-spacing: 2px;
        position: absolute;
        bottom: 0;
        transition: opacity .25s;
    }
    .btn-toggle:before {
        content: 'Off';
        left: -4rem;
    }
    .btn-toggle:after {
        content: 'On';
        right: -4rem;
        opacity: .5;
    }
    .btn-toggle > .handle {
        position: absolute;
        top: 0.1875rem;
        left: 0.1875rem;
        width: 1.125rem;
        height: 1.125rem;
        border-radius: 1.125rem;
        background: #fff;
        transition: left .25s;
    }
    .btn-toggle.active {
        transition: background-color .25s;
    }
    .btn-toggle.active > .handle {
        left: 1.6875rem;
        transition: left .25s;
    }
    .btn-toggle.active:before {
        opacity: .5;
    }
    .btn-toggle.active:after {
        opacity: 1;
    }
    .btn-toggle.btn-sm:before, .btn-toggle.btn-sm:after {
        line-height: -0.5rem;
        color: #fff;
        letter-spacing: .75px;
        left: 0.4125rem;
        width: 2.325rem;
    }
    .btn-toggle.btn-sm:before {
        text-align: right;
    }
    .btn-toggle.btn-sm:after {
        text-align: left;
        opacity: 0;
    }
    .btn-toggle.btn-sm.active:before {
        opacity: 0;
    }
    .btn-toggle.btn-sm.active:after {
        opacity: 1;
    }
    .btn-toggle.btn-xs:before, .btn-toggle.btn-xs:after {
        display: none;
    }
    .btn-toggle:before, .btn-toggle:after {
        color: #6b7381;
    }
    .btn-toggle.active {
        background-color: #29b5a8;
    }
    .btn-toggle.btn-lg {
        margin: 0 5rem;
        padding: 0;
        position: relative;
        border: none;
        height: 2.5rem;
        width: 5rem;
        border-radius: 2.5rem;
    }
    .btn-toggle.btn-lg:focus, .btn-toggle.btn-lg:focus.active, .btn-toggle.btn-lg.focus, .btn-toggle.btn-lg.focus.active {
        outline: none;
    }
    .btn-toggle.btn-lg:before, .btn-toggle.btn-lg:after {
        line-height: 2.5rem;
        width: 5rem;
        text-align: center;
        font-weight: 600;
        font-size: 1rem;
        text-transform: uppercase;
        letter-spacing: 2px;
        position: absolute;
        bottom: 0;
        transition: opacity .25s;
    }
    .btn-toggle.btn-lg:before {
        content: 'Off';
        left: -5rem;
    }
    .btn-toggle.btn-lg:after {
        content: 'On';
        right: -5rem;
        opacity: .5;
    }
    .btn-toggle.btn-lg > .handle {
        position: absolute;
        top: 0.3125rem;
        left: 0.3125rem;
        width: 1.875rem;
        height: 1.875rem;
        border-radius: 1.875rem;
        background: #fff;
        transition: left .25s;
    }
    .btn-toggle.btn-lg.active {
        transition: background-color .25s;
    }
    .btn-toggle.btn-lg.active > .handle {
        left: 2.8125rem;
        transition: left .25s;
    }
    .btn-toggle.btn-lg.active:before {
        opacity: .5;
    }
    .btn-toggle.btn-lg.active:after {
        opacity: 1;
    }
    .btn-toggle.btn-lg.btn-sm:before, .btn-toggle.btn-lg.btn-sm:after {
        line-height: 0.5rem;
        color: #fff;
        letter-spacing: .75px;
        left: 0.6875rem;
        width: 3.875rem;
    }
    .btn-toggle.btn-lg.btn-sm:before {
        text-align: right;
    }
    .btn-toggle.btn-lg.btn-sm:after {
        text-align: left;
        opacity: 0;
    }
    .btn-toggle.btn-lg.btn-sm.active:before {
        opacity: 0;
    }
    .btn-toggle.btn-lg.btn-sm.active:after {
        opacity: 1;
    }
    .btn-toggle.btn-lg.btn-xs:before, .btn-toggle.btn-lg.btn-xs:after {
        display: none;
    }
    .btn-toggle.btn-sm {
        margin: 0 .5rem;
        padding: 0;
        position: relative;
        border: none;
        height: 1.5rem;
        width: 3rem;
        border-radius: 1.5rem;
    }
    .btn-toggle.btn-sm:focus, .btn-toggle.btn-sm:focus.active, .btn-toggle.btn-sm.focus, .btn-toggle.btn-sm.focus.active {
        outline: none;
    }
    .btn-toggle.btn-sm:before, .btn-toggle.btn-sm:after {
        line-height: 1.5rem;
        width: .5rem;
        text-align: center;
        font-weight: 600;
        font-size: .55rem;
        text-transform: uppercase;
        letter-spacing: 2px;
        position: absolute;
        bottom: 0;
        transition: opacity .25s;
    }
    .btn-toggle.btn-sm:before {
        content: 'Off';
        left: -0.5rem;
    }
    .btn-toggle.btn-sm:after {
        content: 'On';
        right: -0.5rem;
        opacity: .5;
    }
    .btn-toggle.btn-sm > .handle {
        position: absolute;
        top: 0.1875rem;
        left: 0.1875rem;
        width: 1.125rem;
        height: 1.125rem;
        border-radius: 1.125rem;
        background: #fff;
        transition: left .25s;
    }
    .btn-toggle.btn-sm.active {
        transition: background-color .25s;
    }
    .btn-toggle.btn-sm.active > .handle {
        left: 1.6875rem;
        transition: left .25s;
    }
    .btn-toggle.btn-sm.active:before {
        opacity: .5;
    }
    .btn-toggle.btn-sm.active:after {
        opacity: 1;
    }
    .btn-toggle.btn-sm.btn-sm:before, .btn-toggle.btn-sm.btn-sm:after {
        line-height: -0.5rem;
        color: #fff;
        letter-spacing: .75px;
        left: 0.4125rem;
        width: 2.325rem;
    }
    .btn-toggle.btn-sm.btn-sm:before {
        text-align: right;
    }
    .btn-toggle.btn-sm.btn-sm:after {
        text-align: left;
        opacity: 0;
    }
    .btn-toggle.btn-sm.btn-sm.active:before {
        opacity: 0;
    }
    .btn-toggle.btn-sm.btn-sm.active:after {
        opacity: 1;
    }
    .btn-toggle.btn-sm.btn-xs:before, .btn-toggle.btn-sm.btn-xs:after {
        display: none;
    }
    .btn-toggle.btn-xs {
        margin: 0 0;
        padding: 0;
        position: relative;
        border: none;
        height: 1rem;
        width: 2rem;
        border-radius: 1rem;
    }
    .btn-toggle.btn-xs:focus, .btn-toggle.btn-xs:focus.active, .btn-toggle.btn-xs.focus, .btn-toggle.btn-xs.focus.active {
        outline: none;
    }
    .btn-toggle.btn-xs:before, .btn-toggle.btn-xs:after {
        line-height: 1rem;
        width: 0;
        text-align: center;
        font-weight: 600;
        font-size: .75rem;
        text-transform: uppercase;
        letter-spacing: 2px;
        position: absolute;
        bottom: 0;
        transition: opacity .25s;
    }
    .btn-toggle.btn-xs:before {
        content: 'Off';
        left: 0;
    }
    .btn-toggle.btn-xs:after {
        content: 'On';
        right: 0;
        opacity: .5;
    }
    .btn-toggle.btn-xs > .handle {
        position: absolute;
        top: 0.125rem;
        left: 0.125rem;
        width: 0.75rem;
        height: 0.75rem;
        border-radius: 0.75rem;
        background: #fff;
        transition: left .25s;
    }
    .btn-toggle.btn-xs.active {
        transition: background-color .25s;
    }
    .btn-toggle.btn-xs.active > .handle {
        left: 1.125rem;
        transition: left .25s;
    }
    .btn-toggle.btn-xs.active:before {
        opacity: .5;
    }
    .btn-toggle.btn-xs.active:after {
        opacity: 1;
    }
    .btn-toggle.btn-xs.btn-sm:before, .btn-toggle.btn-xs.btn-sm:after {
        line-height: -1rem;
        color: #fff;
        letter-spacing: .75px;
        left: 0.275rem;
        width: 1.55rem;
    }
    .btn-toggle.btn-xs.btn-sm:before {
        text-align: right;
    }
    .btn-toggle.btn-xs.btn-sm:after {
        text-align: left;
        opacity: 0;
    }
    .btn-toggle.btn-xs.btn-sm.active:before {
        opacity: 0;
    }
    .btn-toggle.btn-xs.btn-sm.active:after {
        opacity: 1;
    }
    .btn-toggle.btn-xs.btn-xs:before, .btn-toggle.btn-xs.btn-xs:after {
        display: none;
    }
    .btn-toggle.btn-secondary {
        color: #6b7381;
        background: #bdc1c8;
    }
    .btn-toggle.btn-secondary:before, .btn-toggle.btn-secondary:after {
        color: #6b7381;
    }
    .btn-toggle.btn-secondary.active {
        background-color: #ff8300;
    }

    ul.propositionAction {
        -webkit-transition:all 0.2s ease-in;
        -moz-transition:all 0.2s ease-in;
        -ms-transition:all 0.2s ease-in;
        -o-transition:all 0.2s ease-in;
        transition:all 0.2s ease-in;
    }

    ul.propositionAction.td-action-maximum .texteduboutton{
        display: block;
    }

    li.propositionAction{
        text-align: center;
    }

    ul.propositionAction.td-action-maximum li.propositionAction {
        display: block;
        background-color: #bddba3
    }

    .texteduboutton {
        display: none;
        font-size: 12px !important;
        font-weight: bold !important;
    }

    ul.propositionAction.td-action-maximum li.propositionAction:not(:last-child) {
        border-bottom: 1px solid #ccc !important;
    }


</style>

<?php
if ($canEdit){
    $keyTpl = (isset($kunik)) ? $kunik : $keyTpl;
}
?>

<ul class="btnleft propositionAction">
    <li class="propositionAction">
        <a href='javascript:;' data-actionid='<?php echo @$actionid?>' data-id='<?php echo $id?>' data-collection='<?php echo $collection?>' data-key='<?php echo $q?>' data-path='<?php echo $path?>' class='edit<?php echo $keyTpl ?> previewTpl btn btn-xs bd-enable-btnbudgetbudget ' data-toggle="tooltip" data-placement="bottom" data-original-title="Modifier cette ligne">
            <i class='fa fa-pencil'></i>
            <span class="texteduboutton">Modifier cette ligne</span>
        </a>
    </li>
    <li class="propositionAction">
        <a href='javascript:;' data-id='<?php echo $id?>' data-collection='<?php echo $collection?>' data-key='<?php echo $keyTpl.$q?>' data-path='<?php echo $path ?>' class='inputdeleteLine previewTpl btn btn-xs bd-enable-btnbudgetbudget ' data-toggle="tooltip" data-placement="bottom" data-original-title="Supprimer cette ligne">
            <i class='fa fa-times'></i>
            <span class="texteduboutton">Supprimer cette ligne</span>
        </a>
    </li>
    <li class="propositionAction">
        <a href="javascript:;" class="btn btn-xs openAnswersComment bd-enable-btnbudgetbudget tooltips" data-toggle="tooltip" data-placement="bottom" data-original-title="Commenter cette ligne"
           onclick="commentObj.openPreview('answers','<?php /*echo $answer["_id"] */?>','<?php /*echo $answer["_id"] . $key . $depId */?>', '<?php /*echo @$dep['step'] */?>')">

            <i class='fa fa-commenting'></i>
            <span class="texteduboutton">Commenter cette ligne</span>
        </a>
    </li>
    <li class="propositionAction">
        <a href="javascript:;" data-id="<?= $answer['_id'] ?>" data-key="<?= $key ?>" data-form="<?= $form['id'] ?>" data-kunik="<?= $kunik ?>" data-pos="<?= $depId ?>" type="button" class="btn btn-xs newbtnestimate_candidate propositionAction" data-loc="edit" type="button" data-toggle="tooltip" data-placement="bottom" data-hover="Assigner et/ou proposer un prix">
            <i class="fa fa-group"></i>
            <span class="texteduboutton">Proposer des candidats</span>
        </a>
    </li>
    <li class="propositionAction">
        <a href="javascript:;" data-id="<?= $answer['_id'] ?>" data-key="<?= $key ?>" data-form="<?= $form['id'] ?>" data-kunik="<?= $kunik ?>" data-pos="<?= $depId ?>" type="button" class="btn btn-xs newbtnestimate_price propositionAction" data-loc="edit" type="button" data-toggle="tooltip" data-placement="bottom" data-hover="Assigner et/ou proposer un prix">
            <i class="fa fa-money"></i>
            <span class="texteduboutton">Proposer un prix</span>
        </a>
    </li>
    <li class="propositionAction">
        <a href="javascript:;" class="btn btn-xs bd-enable-btn<?= $kunik ?>" data-loc="edit" data-id="badge<?= (string)$answer["_id"].$depId ?>" data-key="<?= $key ?>" data-pos="<?= $depId ?>" data-answerid="<?= (string)$answer["_id"] ?>" type="button" >
            <i class="fa fa-id-badge"></i>
            <span class="texteduboutton">Modifier Badge requis</span>
        </a>
    </li>
    <?php
   /* if( empty($dep["estimates"][Yii::app()->session["userId"]] ))
        {*/
            $candidatenumber = !empty($esti["candidateNumber"]) ? $esti["candidateNumber"] : 1;
    ?>
            <li class="propositionAction" >
                <a href="javascript:;" data-id='<?=$answer["_id"]?>'  data-candidatenumber='<?=$candidatenumber?>' data-key='<?=$key?>' data-form='<?=$form["id"]?>' data-pos='<?=$depId?>' type="button" class="btn candidatenumber  btn-xs" data-loc="edit" type="button" >
                    <i class="fa fa-user-plus"></i>
                    <span class="texteduboutton">Nombre de personne sur cette proposition </span>
                </a>
            </li>
    <?php
        //}

    if(!empty($switchAmountAttr)){
    ?>
    <li class="propositionAction" >
        <button type="button" class="btn btn-xs btn-toggle switchAmountAttr <?= (!empty($switchAmountAttr) && $switchAmountAttr == "assignBudget") ? ''  : 'active' ?>" data-id='<?php echo $id?>' data-collection='<?php echo $collection?>' data-key='<?php echo $q?>' data-path='<?= @$switchAmountAttrPath ?>' data-val="<?= (!empty($switchAmountAttr) && $switchAmountAttr == "assignBudget") ? 'percentage'  : 'assignBudget' ?>" data-toggle="button tooltip" aria-pressed="false" autocomplete="off" data-placement="bottom" data-original-title="<?= @$switchAmountAttrlabel ?>">
            <div class="handle"></div>
        </button>
        <span class="texteduboutton"><?= @$switchAmountAttrlabel ?></span>
    </li>
    <?php
    }
    $havepriceestimate = false;
    if( isset($dep["estimates"] ))
    {

        $countpriceestimate = Aap::countPriceEstimate($dep);
        $candidatePart = count($dep["estimates"]);
        if ($countpriceestimate != 0) {
            $havepriceestimate = true;
        }
    }
    if( $havepriceestimate ){
    ?>

        <li class="propositionAction" >
            <div class="dropdown budgetcustom-dropdown">
                <button class="btn-toogle-list btn btn-xs btn-custombadge action-list-toggle" data-position="<?= $depId ?>" type="button" id="about-us" data-target="price" data-active="false" >
                    <i class="fa fa-hand-pointer-o"></i>
                    <span class="number"><?= $countpriceestimate ?></span>
                    <span class="texteduboutton">Choisir un prix</span>
                </button>
            </div>
        </li>

    <?php
    }

    $havecandidatdenied = false;
    if( isset($dep["estimates"] ))
    {
        $countdenied = 0;
        foreach ( $dep["estimates"] as $uid => $esti )
        {
            if( isset($esti["deny"] ) && $esti["deny"] == true )
            {
                $havecandidatdenied = true;
                $countdenied += 1;
            }
        }
    }
    if( $havecandidatdenied ){
    ?>

    <li class="propositionAction" >
        <div class="dropdown budgetcustom-dropdown">
            <button class="btn-toogle-list btn btn-xs btn-custombadge action-list-toggle" data-position="<?= $depId ?>" type="button" id="about-us" data-target="deny" data-active="false" >
                <i class="fa fa-refresh"></i>
                <span class="number"><?= $countdenied ?></span>
                <span class="texteduboutton">Candidat recalé</span>
            </button>
        </div>

    </li>

    <?php
    }
    ?>
</ul>
