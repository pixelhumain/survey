<?php 
if ($canEdit){ 
	$keyTpl = (isset($kunik)) ? $kunik : $keyTpl;
	?>
<a href='javascript:;'  data-id='<?php echo $id?>' data-collection='<?php echo $collection?>' data-key='<?php echo $q?>' data-path='<?php echo $path?>' class='edit<?php echo $keyTpl ?> previewTpl btn  btn-primary'><i class='fa fa-pencil'></i> <span class="<?php echo (@$label==true) ? '' : 'hidden'; ?>"><?php echo Yii::t("common", "Edit") ?></span></a>

<a href='javascript:;' data-id='<?php echo $id?>' data-collection='<?php echo $collection?>' data-key='<?php echo $keyTpl.$q?>' data-path='<?php echo $path ?>' class='unsetLine previewTpl btn btn-danger'><i class='fa fa-times'> <span class="<?php echo (@$label==true) ? '' : 'hidden'; ?>"><?php echo Yii::t("common", "Delete")?></span></i></a>

<?php } ?>