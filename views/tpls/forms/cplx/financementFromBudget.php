<?php if($answer){ 
	
$copy = "opalProcess1.depense";

if( isset($parentForm["params"][$kunik]["budgetCopy"]) ) 
	$copy = $parentForm["params"][$kunik]["budgetCopy"];
else if( count(Yii::app()->session["budgetInputList"]) == 1 )
	$copy = array_keys( Yii::app()->session["budgetInputList"])[0];

$copyT = explode(".", $copy);
$copyF = $copyT[0];
$budgetKey = $copyT[1];
$answers = null;	
//var_dump($budgetKey);
if($wizard){
	if( $budgetKey ){
		if( isset($answer["answers"][$copyF][$budgetKey]) && count($answer["answers"][$copyF][$budgetKey])>0)
		$answers = $answer["answers"][$copyF][$budgetKey];
	} else if( isset($answer["answers"][$copyF][$budgetKey]) && count($answer["answers"][$copyF][$budgetKey])>0 )
		$answers = $answer["answers"][$copyF][$kunik];
} else {
	if(!empty($budgetKey) && !empty($answer["answers"][$budgetKey]) )
		$answers = $answer["answers"][$budgetKey];
	else if(isset($answers))
		$answers = $answers;
}
	
$editBtnL =  "";

$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";


$paramsData = [ 
	"financerTypeList" => Ctenat::$financerTypeList,
	"limitRoles" =>["Financeur"],
	"openFinancing" => true 
];

if( isset($parentForm["params"][$kunik]) ) {
	if( isset($parentForm["params"][$kunik]["tpl"]) ) 
		$paramsData["tpl"] =  $parentForm["params"][$kunik]["tpl"];
	if( isset($parentForm["params"][$kunik]["budgetCopy"]) ) 
		$paramsData["budgetCopy"] =  $parentForm["params"][$kunik]["budgetCopy"];
	if( isset($parentForm["params"][$kunik]["financerTypeList"]) ) 
		$paramsData["financerTypeList"] =  $parentForm["params"][$kunik]["financerTypeList"];
	if( isset($parentForm["params"][$kunik]["limitRoles"]) ) 
		$paramsData["limitRoles"] =  $parentForm["params"][$kunik]["limitRoles"];
	if( isset($parentForm["params"][$kunik]["openFinancing"]) ) 
		$paramsData["openFinancing"] =  $parentForm["params"][$kunik]["openFinancing"];
}

$communityLinks = Element::getCommunityByParentTypeAndId( $parentForm["parent"] );
$organizations = Link::groupFindByType( Organization::COLLECTION,$communityLinks,["name","links"] );

$orgs = [];
foreach ($organizations as $id => $or) {
	$roles = null;
	if( isset( $communityLinks[$id]["roles"] ) )
		$roles = $communityLinks[$id]["roles"];
	if( $paramsData["limitRoles"] && !empty($roles) )
	{
		foreach ($roles as $i => $r) 
		{
			if( in_array($r, $paramsData["limitRoles"]) ) 
				$orgs[$id] = $or["name"];
		}		
	}
}

//var_dump($orgs); exit;
$listLabels = array_merge(Ctenat::$financerTypeList,$orgs);

$properties = [
      // "financerType" => [
      //   "placeholder" => "Type de Financement",
      //       "inputType" => "select",
      //       "list" => "financerTypeList",
      //       "rules" => [
      //           "required" => true
      //       ]
      //   ],
		"poste" => [
            "inputType" => "text",
            "label" => Yii::t("survey","Context"),
            "placeholder" => Yii::t("survey","Context"),
            "size" => 4,
            "rules" => [ "required" => true ]
        ],
        "financer" => [
            "placeholder" => Yii::t("survey","Which Financier"),
            "inputType" => "select",
            "list" => "financersList",
			"subLabel" => Yii::t("survey","If you are a public funder, invite him/her in the list below (if he/she does not appear, ask your territory representative to declare him/her as a funding partner)"),
			"size" => 6,
        ]
];
$amounts = (isset($parentForm["params"][$budgetKey]["amounts"])) ? $parentForm["params"][$budgetKey]["amounts"] : ["price" => "Montant Financé"] ; 
foreach ( $amounts as $k => $l) {
	$properties[$k] = [ "inputType" => "text",
			            "label" => $l,
			            "placeholder" => $l,
			            "propType" => "amount"
			        ];
}


	?>	
<?php
echo $this->renderPartial("survey.views.tpls.forms.cplx.financementFromBudgetTable",
	                      [ 
	                        "form" => $form,
	                        "wizard" => true, 
	                        "answers"=>$answers,
	                        "answer"=>$answer,
                            "mode" => $mode,
                            "kunik" => $kunik,
                            "answerPath"=>$answerPath,
                            "key" => $key,
                            "titleColor" => $titleColor,
                            "properties" => $properties,
                            "copy" => $copy,
                            "copyF" => $copyF,
                            "budgetKey" => $budgetKey,

                            "label" => $label,
                            "editQuestionBtn" => $editQuestionBtn,
                            "editParamsBtn" => $editParamsBtn,
                            "editBtnL" => $editBtnL,
                            "info" => $info,
	                        //"showForm" => $showForm,
	                        "paramsData" => $paramsData,
	                        "canEdit" => $canEdit,
	                        "canAdminAnswer"  => $canAdminAnswer,
	                        //"el" => $el 
	                    ] ,true );

?>





<div class="form-prioritize" style="display:none;">
  	<select id="validWork" style="width:100%;">
		<option> Valider ces travaux </option>
		<?php foreach (["validated" => Yii::t("survey","Unreservedly validated"),
						"reserved" => Yii::t("survey","Validated with reservations"),
						"refused" => Yii::t("survey","Not validated") ] as $v => $f) {
			echo "<option value='".$v."'>".$f."</option>";
		} ?>
		</select>
 
</div>

<div class="form-financer" style="display:none;">
	<span class="bold"><?php echo Yii::t("survey","Existing financing in community")?></span><br/>
	<select id="financer" style="width:100%;">
		<option value=0 ><?php echo Yii::t("survey","Which Financier")?></option>
		<?php foreach ($orgs as $v => $f) {
			echo "<option value='".$v."'>".$f."</option>";
		} ?>
	</select>
	<br><span class="bold"><?php echo Yii::t("cms","or") ." ".Yii::t("survey","Non-existent financier in the community")?>ou Financeur inexistant dans la communauté</span><br/>
	<?php //if(!(boolean)$paramsData["openFinancing"]) { ?>
	<span class="bold"><?php echo Yii::t("survey","Name of the financier Free")?></span>
	<br>
	<input type="text" id="financerName" name="financerName" style="width:100%;">

	<span class="bold">Email</span>
	<br>
	<input type="text" id="financerEmail" name="financerEmail" style="width:100%;">
	<?php //} ?>

	<br><br>
	<span class="bold">Fonds, enveloppe ou budget mobilisé</span>
	<br>
	<input type="text" id="financeLine" name="financeLine" style="width:100%;">
	<span class="bold">Montant Financé</span>
	<br>
	<input type="text" id="financeAmount" name="financeAmount" style="width:100%;">
</div>



<?php 
if( isset($parentForm["params"]["financement"]["tpl"])){
	//if( $parentForm["params"]["financement"]["tpl"] == "tpls.forms.equibudget" )
		echo $this->renderPartial( "costum.views.".$parentForm["params"]["financement"]["tpl"] , 
		[ "totalFin"  => $total,
		  "totalBudg" => Yii::app()->session["totalBudget"]["totalBudget"] ] );
	// else 
	// 	$this->renderPartial( "costum.views.".$parentForm["params"]["financement"]["tpl"]);
}
 ?>
<?php if($mode != "pdf"){ ?>
<script type="text/javascript">

if(typeof dyFObj.elementObjParams == "undefined")
	dyFObj.elementObjParams = {};

dyFObj.elementObjParams.budgetInputList = <?php echo json_encode( Yii::app()->session["budgetInputList"] ); ?>;
dyFObj.elementObjParams.financerTypeList = <?php echo json_encode(Ctenat::$financerTypeList); ?>;
dyFObj.elementObjParams.financersList = <?php echo json_encode($orgs); ?>;


var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answer["answers"][$budgetKey])) ? $answer["answers"][$budgetKey] : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

$(document).ready(function() { 
	
	sectionDyf.<?php echo $kunik ?> = {
		"jsonSchema" : {	
	        "title" : "Plan de Financement",
            "icon" : "fa-money",
            "text" : "Décrire ici les financements mobilisés ou à mobiliser. Les coûts doivent être en <b>hors taxe</b>.",
	        "properties" : <?php echo json_encode( $properties ); ?>,
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, closePrioModalRel );
	            }

	    	}
	    }
	};

	sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "<?php echo $kunik ?> config",
	        "icon" : "fa-cog",
	        "properties" : {
	            financerTypeList : {
	                inputType : "properties",
	                labelKey : "Clef",
	                labelValue : "Label affiché",
	                label : "Liste des type de Fiannceurs",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.financerTypeList
	            } , 
	            limitRoles : {
	                inputType : "array",
	                label : "Liste des roles financeurs",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.limitRoles
	            },
	            tpl : {
	                label : "Sub Template",
	                value :  sectionDyf.<?php echo $kunik ?>ParamsData.tpl
	            },
	            budgetCopy : {
	            	label : "Input Bugdet",
	            	inputType : "select",
	            	options :  dyFObj.elementObjParams.budgetInputList
	            },
	            openFinancing : {
	                inputType : "checkboxSimple",
                    label : "Financement Ouvert",
                    subLabel : "Le Finacement est ouvert aux personnes ou organisations non inscrites dans la communauté",
                    params : {
                        onText : "Oui",
                        offText : "Non",
                        onLabel : "Oui",
                        offLabel : "Non",
                        labelText : "Financement Ouvert"
                    },
                    checked : sectionDyf.<?php echo $kunik ?>ParamsData.estimate
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	        		if(val.inputType == "array")
	        		 	tplCtx.value[k] = getArray('.'+k+val.inputType);
	        		else if(val.inputType == "properties")
	        		 	tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
	        		else
	        		 	tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    urlCtrl.loadByHash(location.hash);
	                } );
	            }

	    	}
	    }
	};


    mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

    //adds a line into answer
    $(".add<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
    });

    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
    });

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

 

	$('.btnValidFinance').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.budgetpath = $(this).data("budgetpath");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.form = $(this).data("form");
		prioModal = bootbox.dialog({
	        message: $(".form-validate-work").html(),
	        title: "État du Financement",
	        show: false,
	        buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {
			            
			        	var formInputsHere = formInputs;
			        	tplCtx.path = "answers";
			        	if( notNull(formInputs [tplCtx.form]) )
			        		tplCtx.path = "answers";    

			        	tplCtx.path = tplCtx.path+"."+tplCtx.budgetpath+"."+tplCtx.pos+".validFinal";	   
			        	
			        	var today = new Date();
						today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();	
			            tplCtx.value = { 
		        			valid : $(".bootbox #validWork").val(),
		        			user : userId,
		        			date : today
		        		};
		        		delete tplCtx.pos;
		        		delete tplCtx.budgetpath;
				    	mylog.log("btnValidateWork save",tplCtx);
				  	 	dataHelper.path2Value( tplCtx, function(){
				  	 		saveLinks(answerObj._id.$id,"financeValidated",userId,closePrioModalRel);
				  	 	} );
				  	 }		
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: closePrioModal
                }
              },
		        onEscape: closePrioModal
	    });
	    prioModal.modal("show");
	});
    
    $('.btnFinancer').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.budgetpath = $(this).data("budgetpath");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.form = $(this).data("form");
		prioModal = bootbox.dialog({
	        message: $(".form-financer").html(),
	        title: "Ajouter un Financeur",
	        show: false,
	        buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {
			            
			        	//var formInputsHere = formInputs;
			        	var financersCount = ( typeof eval("answerObj.answers."+tplCtx.budgetpath+"["+tplCtx.pos+"].financer") != "undefined" ) ? eval("answerObj.answers."+tplCtx.budgetpath+"["+tplCtx.pos+"].financer").length : 0;
			        	tplCtx.path = "answers."+tplCtx.budgetpath+"."+tplCtx.pos+".financer."+financersCount;
			        	// if( notNull(formInputs [tplCtx.form]) )
			        	// 	tplCtx.path = "answers."+tplCtx.budgetpath+"."+tplCtx.pos+".financer."+financersCount;	       

			        	var today = new Date();
						today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();	
			            tplCtx.value = { 
		        			line   : $(".bootbox #financeLine").val(),
		        			amount : $(".bootbox #financeAmount").val(),
		        			user   : userId,
		        			date   : today
		        		};

		        		if($(".bootbox #financer").val() != 0 ){
		        			tplCtx.value.id = $(".bootbox #financer").val();
		        			tplCtx.value.name = $(".bootbox #financer option:selected").text();
		        		}else if($(".bootbox #financerName").val() != "" ){
		        			tplCtx.value.name = $(".bootbox #financerName").val();
		        			tplCtx.value.email = $(".bootbox #financerEmail").val();
		        		}

		        		delete tplCtx.pos;
		        		delete tplCtx.budgetpath;
				    	mylog.log("btnFinancer save",tplCtx);
				  	 	dataHelper.path2Value( tplCtx, function(params) { 
				  	 		prioModal.modal('hide');
				  	 		saveLinks(answerObj._id.$id,"financerAdded",userId,function(){urlCtrl.loadByHash(location.hash);});
				  	 		
				  	 	} );
			        }		
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: function() {
                  }
                }
              },
		        onEscape: function() {
		          prioModal.modal("hide");
		        }
	    });
	    prioModal.modal("show");
	});
});
function closePrioModal(){
	prioModal.modal('hide');
}
function closePrioModalRel	(){
	closePrioModal();
	urlCtrl.loadByHash(location.hash);
}
</script>
<?php } ?>
<?php } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>