<?php 
if($saveOneByOne)
	$inpClass = " ";
	if($mode == "fa"){
		$editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";
	}
	$paramsData = [ "list" => [	], "attributeValueRadio" => false, "listNumber" => [] ];
	$currentAnswer = (!empty($answer) && isset($answer["answers"][$form["id"]][$key])) ? $answer["answers"][$form["id"]][$key] : "";
	$currentEvalAnswer = (!empty($answer) && isset($answer["answers"][$form["id"]][$key."_multiEval"])) ? $answer["answers"][$form["id"]][$key."_multiEval"] : [];
	if( isset($parentForm["params"][$kunik]) ) {
		if( isset($parentForm["params"][$kunik]["list"]) ) 
			$paramsData["list"] =  $parentForm["params"][$kunik]["list"];
		
		if( isset($parentForm["params"][$kunik]["listNumber"]) ) 
			$paramsData["listNumber"] =  $parentForm["params"][$kunik]["listNumber"];

		if( isset($parentForm["params"][$kunik]["attributeValueRadio"])) 
			$paramsData["attributeValueRadio"] =  $parentForm["params"][$kunik]["attributeValueRadio"];
	}
?>
<style>
	/* The container */
	.container<?= $kunik ?> {
	display: block;
	position: relative;
	padding-left: 35px;
	margin-bottom: 12px;
	cursor: pointer;
	font-size: 1.6rem;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	}

	/* Hide the browser's default radio button */
	.container<?= $kunik ?> input {
	position: absolute;
	opacity: 0;
	cursor: pointer;
	}

	/* Create a custom radio button */
	.checkmark<?= $kunik ?> {
	position: absolute;
	top: 0;
	left: 0;
	height: 20px;
	width: 20px;
	background-color: #eee;
	border-radius: 50%;
	}

	/* On mouse-over, add a grey background color */
	.container<?= $kunik ?>:hover input ~ .checkmark<?= $kunik ?> {
	background-color: #ccc;
	}

	/* When the radio button is checked, add a blue background */
	.container<?= $kunik ?> input:checked ~ .checkmark<?= $kunik ?> {
	background-color: #2196F3;
	}

	/* Create the indicator (the dot/circle - hidden when not checked) */
	.checkmark<?= $kunik ?>:after {
	content: "";
	position: absolute;
	display: none;
	}

	/* Show the indicator (dot/circle) when checked */
	.container<?= $kunik ?> input:checked ~ .checkmark<?= $kunik ?>:after {
	display: block;
	}

	/* Style the indicator (dot/circle) */
	.container<?= $kunik ?> .checkmark<?= $kunik ?>:after {
		top: 6px;
		left: 6px;
		width: 8px;
		height: 8px;
		border-radius: 50%;
		background: white;
	}

	span.point {
		font-size: 16px;
		font-style: italic;
	}
</style>
<?php
    if($mode == "r" || $mode == "pdf"){ ?>
    <div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
        <label for="<?php echo $kunik ?>">
        	<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
        		<?php echo $label ?>
        	</h4>
        </label><br/>
		<?php echo @$currentAnswer ?>
    </div>
<?php }else{ ?>
<div class="form-check text-left">
    <label class="form-check-label" for="<?php echo $key ?>"><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.@$editParamsBtn ?></h4></label><br/>	
	<?php if(!empty($info)){ ?>
    	<small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
    <?php } ?>
    <?php 
    if( !isset($parentForm["params"][$kunik]['list']) ) {
    	echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> ".Yii::t("survey","THIS FIELD HAS TO BE CONFIGURED FIRST").@$editParamsBtn."</span>";
    } else {
	    foreach ($parentForm["params"][$kunik]["list"] as $ix => $lbl) { 
			$currentValUnik = $ix . "_" . InflectorHelper::slugify($lbl);
	    	$checkedValue =  "";
			$indexNumber = 0;
			$showNumber = false;

			if(strtolower($lbl) == strtolower($currentAnswer))
				$checkedValue =  "checked";
		
			if (isset($parentForm["params"][$kunik]["listNumber"][$ix])) {
				if (isset($answer["answers"][$form["id"]][$key]) && strtolower($parentForm["params"][$kunik]["listNumber"][$ix]) == strtolower($answer["answers"][$form["id"]][$key])) {
					$checkedValue =  "checked";
				}
				$indexNumber = $parentForm["params"][$kunik]["listNumber"][$ix];
				if ($indexNumber == "0" || $indexNumber == "1")
					$indexNumberPoint = ". <span class='point'>( ".$indexNumber." point )</span>";
				else 
					$indexNumberPoint = ". <span class='point'>( ".$indexNumber." points )</span>";
			}

			$checkedMultiple = "";
			if (isset($input["activeMultieval"]) && filter_var($input["activeMultieval"], FILTER_VALIDATE_BOOLEAN) && !empty(Yii::app()->session["userId"])) {
				$checkedValue = "";
				$checkedMultiple = isset($currentEvalAnswer[Yii::app()->session["userId"]]["answer"]) && strtolower($currentValUnik) == strtolower($currentEvalAnswer[Yii::app()->session["userId"]]["answer"]) ? "checked" : "";
			}
			
	    	?>
			<label class="container<?= $kunik ?>"> <?= $lbl ?> <?= ($paramsData["attributeValueRadio"]) ? $indexNumberPoint : "" ?>
				<input type="radio" name="<?= $kunik ?>" class="form-check-input <?php echo $inpClass ?>  <?= $kunik ?>"  id="<?php echo $kunik.$ix ?>" data-value="<?= ($paramsData["attributeValueRadio"]) ? $indexNumber : $lbl ?>" data-valueunik="<?= $currentValUnik ?>" data-form='<?php echo $form["id"] ?>' <?php echo $checkedValue ." ". $checkedMultiple ?>  >
				<span class="checkmark<?= $kunik ?>"></span>
			</label>
	    <?php }  
	}?>

</div>

<script type="text/javascript">
var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answer["answers"][$kunik])) ? $answer["answers"][$kunik] : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
const _php = {
	kunik : "<?= $kunik ?>",
	key : "<?= $key ?>",
	isMultiEvalActive : <?= json_encode(isset($input["activeMultieval"]) && filter_var($input["activeMultieval"], FILTER_VALIDATE_BOOLEAN) ? "true" : "false") ?>,
	currentEvalAnswer : <?= json_encode($currentEvalAnswer) ?>,
	currentUserEvalAnswer : []
}
if (_php.currentEvalAnswer[userId]) {
	_php.currentUserEvalAnswer = _php.currentEvalAnswer[userId]
}
var radioNumber = [];
const showListArray = () => {
	$(".listNumberarray .inputs.array").html("");
	let htmlNumber = "";		
	$("div.listarray input.addmultifield").each(function(index, value) {
		let labelValue = $($("div.listarray input.addmultifield")[index]).val()
		if (labelValue !== "") {
			let str = $(value).attr("class");
			let selectors = str.split(' ').map(c => '.' + c).join('');
			let radioNumberKey = labelValue + index;
			let radioNumberValue = (typeof radioNumber[index] != "undefined")? radioNumber[index] : 0;
			htmlNumber += `<div class="col-xs-12 no-padding margin-top-10" style="display: block;">
				<div class="col-xs-4 no-padding">
					<label>${labelValue}</label>
				</div>	
				<div class="col-xs-6 no-padding">
					<input type="number" name="listNumber[]" onkeyup="radioNumber[${index}] = $(this).val()" class="${str}" value="${radioNumberValue}" placeholder="...">
				</div>
			</div>`;
		}
	}) 	
	$(".listNumberarray .inputs.array").html(htmlNumber);
}

jQuery(document).ready(function() {
    mylog.log("render form input","/modules/costum/views/tpls/forms/cplx/multiCheckbox.php");
	var currentAnswer = <?php echo json_encode( $currentAnswer ); ?>;
	$('.<?= $kunik ?>').change(function(e) {
		e.stopImmediatePropagation();
		var value<?= $kunik ?> = $(this).data("value");
		var path<?= $kunik ?> = "answers."+$(this).data("form")+".<?= $key ?>";
		if (_php.isMultiEvalActive && /true/.test(_php.isMultiEvalActive)) {
			value<?= $kunik ?> = {
				answer : $(this).data("valueunik"),
				date : "now"
			};
			path<?= $kunik ?> = "answers."+$(this).data("form")+".<?= $key ?>_multiEval."+userId;
		}
		const dataId = "<?= $key ?>"
		const dataForm = $(this).data("form");
		if(typeof ownAnswer != 'undefined' && ownAnswer[$(this).attr("data-form")]) {
			ownAnswer[$(this).attr("data-form")][dataId] = $(this).data("value")
		}
		if(typeof removeError != "undefined") {
			const elem = $(`li#question${dataId.replace('radioNew', '')}`);
			removeError(
				elem, 
				{
					'border' : 'none'
				},
				'error-msg'
			)
		}
		if (this.checked) {
			var answerToSend = {
				id : "<?= (string)$answer["_id"] ?>",
				collection : "answers",
				// arrayForm: true,
				// edit:false,
				value: value<?= $kunik ?>,
				path: path<?= $kunik ?>
			};
			if (_php.isMultiEvalActive && /true/.test(_php.isMultiEvalActive)) {
				answerToSend.setType = [{
					path: "date",
					type: "isoDate"
				}]
			}
			dataHelper.path2Value(answerToSend, function(params){
				//toastr.success(tradForm.modificationSave);
				if(typeof reloadStepValidationInputGlobal != "undefined") {
					if(typeof answerObj.answers=="undefined"){
						answerObj.answers={};
					}
					answerObj.answers[dataForm] = params.elt.answers[dataForm];
					reloadStepValidationInputGlobal(dataForm);
				}
				if(typeof newReloadStepValidationInputGlobal != "undefined") {
					if(typeof answerObj.answers=="undefined"){
						answerObj.answers={};
					}
					answerObj.answers[dataForm] = params.elt.answers[dataForm];
					newReloadStepValidationInputGlobal({
						inputKey : <?php echo json_encode($key); ?>,
						inputType : "tpls.forms.cplx.radioNew" 
					})
				}
			} );
		} else {
			dataHelper.path2Value({
				id : "<?= (string)$answer["_id"] ?>",
				collection : "answers",
				//pull : path<?= $kunik ?>,
				value: null,
				path: _php.isMultiEvalActive && /true/.test(_php.isMultiEvalActive) ? path<?= $kunik ?> + "." + _php.currentUserEvalAnswer.indexOf(value<?= $kunik ?>) + ".answer" : path<?= $kunik ?>+"."+currentAnswer.indexOf(value<?= $kunik ?>)
			}, function(params){
				//toastr.success(tradForm.modificationSave);
				if(typeof reloadStepValidationInputGlobal != "undefined") {
					reloadStepValidationInputGlobal(dataForm)
				}
				if(typeof newReloadStepValidationInputGlobal != "undefined") {
					newReloadStepValidationInputGlobal({
						inputKey : <?php echo json_encode($key); ?>,
						inputType : "tpls.forms.cplx.radioNew" 
					})
				}
			} );
		}
	});
	let listNumberContent = 
		{
			inputType : "array",
			label : "Nombre de point",
			values :  sectionDyf.<?php echo $kunik ?>ParamsData.listNumber,
			init : function () {
				$(".listNumberarray .addPropBtn").remove();
			}
		}
	
    sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "<?php echo $label ?> config",
	        "icon" : "cog",
	        "properties" : {
	            list : {
	                inputType : "array",
	                label : tradForm.checkboxList,
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.list,
					init : function() {
							var testArray = {};
							$(`<input type="text" class="form-control copyConfig" placeholder="vous pouvez copier le liste ici, séparé par des virgules; ou utiliser le button ajout ci-dessous"/>`).insertBefore('.listarray .inputs.array');
							$(".copyConfig").off().on("blur", function() {
								let textVal = $(this).val().length > 0 ? $(this).val().split(",") : [];
								textVal.forEach((el, index) => {
									dyFObj.init.addfield('.listarray', el, 'list')
								});
								$(this).val('')
							})

							$(".listarray").bind("DOMSubtreeModified", function(e) {
								let removeBtnLength = $("div.listarray .removePropLineBtn").length
								let temp = 0;
								let indexDel = 0;
								$("div.listarray .removePropLineBtn").each(function (index, value){
									$("div.listarray a.removePropLineBtn").eq(index).off("click") 
									$("div.listarray a.removePropLineBtn").eq(index).on("click",function() {
										if (typeof radioNumber[index] == "undefined") {
											radioNumber[index] = 0;
										}
										radioNumber.splice(index, 1);
										$(this).parent().parent().remove();
									})
								})
								showListArray();
							})
						}
	            },
				attributeValueRadio : {
					label: "Attribuer un nombre de point sur chaque radio",
					inputType : "checkboxSimple",
					checked : sectionDyf.<?php echo $kunik ?>ParamsData.attributeValueRadio,
					"params" : 
						{
							"onText" : trad.yes,
							"offText" : trad.no,
							"onLabel" : trad.yes,
							"offLabel" : trad.no,
							"labelText" : tradCms.label
						},
            	},
				listNumber : listNumberContent
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
					if (k == "list" || k == "listNumber")
						tplCtx.value[k] = getArray('.'+k+val.inputType);
					else 
						tplCtx.value[k] = sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties.attributeValueRadio.checked;
				});

				// const arrayToObj = (keys, values) => {
				// 	const obj = {};
				// 	for (let i = 0; i < keys.length; i++) {
				// 		obj[values[i]] = keys[i];
				// 	}
				// 	return obj;
				// }
				// if (tplCtx.value.attributeValueRadio) {
				// 	tplCtx.value.listNumber = arrayToObj (tplCtx.value.listNumber, tplCtx.value.list)
				// }
				
	            mylog.log("save tplCtx",tplCtx);
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
	                    urlCtrl.loadByHash(location.hash);
	                } );
	            }

	    	}
	    }
	};

	$(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");

        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
		
		$(".close-modal, .mainDynFormCloseBtn").click(function() {
			radioNumber = {};
		})


		if (jsonHelper.pathExists('sectionDyf.<?php echo $kunik ?>ParamsData.listNumber') && Object.keys(sectionDyf.<?php echo $kunik ?>ParamsData.listNumber).length > 0) {
			radioNumber = sectionDyf.<?php echo $kunik ?>ParamsData.listNumber;	
			showListArray();
		}

		<?php if ($paramsData["attributeValueRadio"]) {?>
			$(".listNumberarray").show();
		<?php } else { ?>
			$(".listNumberarray").hide();	
			radioNumber = {};	
		<?php } ?>
		
		$('.attributeValueRadiocheckboxSimple').find("a[data-checkval='true']").click(() => {
			sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties.attributeValueRadio.checked = true;
			sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties["listNumber"] = listNumberContent;
			showListArray();
			$(".listNumberarray").show();
		});

		$('.attributeValueRadiocheckboxSimple').find("a[data-checkval='false']").click(() => {
			sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties.attributeValueRadio.checked = false;
			delete sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties.listNumber;
			$(".listNumberarray").hide();
		});

    });
});
</script>
<?php } ?>