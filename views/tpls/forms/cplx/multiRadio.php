<style type="text/css">
/* style nouveau radio */
    .rdo-grp .container<?= $kunik ?> {
	display: block;
	position: relative;
	padding-left: 35px;
	padding-top: 0px;
	margin-bottom: 12px;
	cursor: pointer;
	font-size: 22px;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	}

	/* Hide the browser's default radio button */
	.rdo-grp .radioCo.<?= $kunik ?> {
	position: absolute;
	opacity: 0;
	cursor: pointer;
	}

	/* Create a custom radio button */
	.rdo-grp .checkmark<?= $kunik ?> {
	position: absolute;
	top: 0;
	left: 0;
	height: 25px;
	width: 25px;
	background-color: #eee;
	border-radius: 50%;
	}

	/* On mouse-over, add a grey background color */
	.container<?= $kunik ?>:hover .checkmark<?= $kunik ?>{
	background-color: #ccc;
	}

	/* When the radio button is checked, add a blue background */
	input.radioCo.<?= $kunik ?>:checked + .container<?= $kunik ?> .checkmark<?= $kunik ?>, input.radioCo.<?= $kunik ?>:active + .container<?= $kunik ?> .checkmark<?= $kunik ?>{
	    background-color: #2196F3;
	}

	/* Create the indicator (the dot/circle - hidden when not checked) */
	input.radioCo.<?= $kunik ?> + .container<?= $kunik ?> .checkmark<?= $kunik ?>:after {
	    content: "";
	    position: absolute;
	    display: none;
	}

	/* Show the indicator (dot/circle) when checked */
	input.radioCo.<?= $kunik ?>:checked + .container<?= $kunik ?> .checkmark<?= $kunik ?>:after, input.radioCo.<?= $kunik ?>:active + .container<?= $kunik ?> .checkmark<?= $kunik ?>:after {
	    display: block;
	}

	/* Style the indicator (dot/circle) */
	.container<?= $kunik ?> .checkmark<?= $kunik ?>:after {
		top: 9px;
		left: 9px;
		width: 8px;
		height: 8px;
		border-radius: 50%;
		background: white;
	}




/*style originel */
	.rdo-grp {
	/*position: absolute;
	top: calc(50% - 10px);*/
	}
	/*.rdo-grp label {
	cursor: pointer;
	-webkit-tap-highlight-color: transparent;
	padding: 6px 8px;
	border-radius: 20px;
	float: left;
	transition: all 0.2s ease;
	}
	.rdo-grp label:hover {
	background: rgba(125,100,247,0.06);
	}
	.rdo-grp label:not(:last-child) {
	margin-right: 16px;
	}
	.rdo-grp label span {
	vertical-align: middle;
	}
	.rdo-grp label span:first-child {
	position: relative;
	display: inline-block;
	vertical-align: middle;
	width: 27px;
	height: 27px;
	background: #e8eaed;
	border-radius: 50%;
	transition: all 0.2s ease;
	margin-right: 8px;
	}
	.rdo-grp label span:first-child:after {
	content: '';
	position: absolute;
	width: 19px;
	height: 19px;
	margin: 4px;
	background: #fff;
	border-radius: 50%;
	transition: all 0.2s ease;
	}
	.rdo-grp label:hover span:first-child {
	background: #7d64f7;
	}
	.rdo-grp input {
	display: none;
	}
	.rdo-grp input:checked + label span:first-child {
	background: #7d64f7;
	}
	.rdo-grp input:checked + label span:first-child:after {
	transform: scale(0.7);
	}*/

	.effect-1{border: 0; padding: 0px 5px; border-bottom: 1px solid #ccc;}

	.effect-1 ~ .focus-border{position: absolute; bottom: 0; left: 0; width: 0; height: 2px; background-color: #3399FF; transition: 0.4s;}
	.effect-1:focus ~ .focus-border{width: 100%; transition: 0.4s;}

	.paramsonebtn , .paramsonebtnP {
		font-size: 17px;
		display: none;
		padding: 5px;
	}

	.paramsonebtn:hover {
		color: red;
	}

	.paramsonebtnP:hover {
		color: blue;
	}

	.thradio:hover .paramsonebtn, .thradio:hover .paramsonebtnP {
		display: inline-block;
	}

	.effect-1:focus {
		outline: none !important;
	}

	.responsemultiradio:before{
		content: '\f0a9';
		margin-right: 15px;
		font-family: FontAwesome;
		color: #d9534f; 
	}

</style>



<?php 
$cle = $key ;
$value = (!empty($answers)) ? " value='".$answers."' " : "";
$inpClass = " saveOneByOne"; 

$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";



$paramsData = [ 
	"list" => [	],
	"tofill" => [ ],
	"placeholdersradio" => [ ],
	"type" => [
		"simple" => "Sans champ de saisie",
		"cplx" => "Avec champ de saisie"
	],
	"width" => [
		"12" => "1",
		"6" => "2",
		"4" => "3",
		"2" => "6",
		"1" => "12"
	]
 ];

 if (isset($input["conditional"])) {
 	$valStepKey="";
 	$validInputList=[];
 	foreach($form["inputs"] as $key=>$val){
 		if(str_contains($val["type"],"validateStep")){
 			$valStepKey=$key;
 			$validInputList=isset($parentForm["params"]["validateStep".$valStepKey]["inputList"]) ? $parentForm["params"]["validateStep".$valStepKey]["inputList"] : [];
 			//array_push($validInputList,$valStepKey); 			
 			break;
 		}
 	}
 	if (!function_exists("multiCondition")){
 	 	function multiCondition($array){
 	       if (!is_array($array)) 
 	           return false; 

 	       $r_valeur = Array();

 	       $array_unique = array_unique($array); 

 	       if (count($array) - count($array_unique)){ 
 	           for ($i=0; $i<count($array); $i++) {
 	               if (!array_key_exists($i, $array_unique)) 
 	                   $r_valeur[] = $array[$i];
 	           } 
 	       } 
 	       return array_unique($r_valeur);
 	     }
 	}
 	    $condInput=array_column($input["conditional"],"input");
 	    $multiCondInput=multiCondition($condInput);
 	    $distinctCondInput=array_diff($condInput, $multiCondInput);
 
 	// scroll to the first appearing conditional input if several 
 	foreach ($input["conditional"] as $key => $value) {
 		$samePrevVal=false;
 		if($key>0){
 		    $prevKey=$key-1;
 			if($value["value"]==$input["conditional"][$prevKey]["value"]){
 		        $samePrevVal=true;
 			}    
 		}
 ?>
 	<script type="text/javascript">	
 		jQuery(document).ready(function() {
 			
 			var valStepKey = "<?php echo $valStepKey ?>";
 			var multiCondInput=<?php echo json_encode($multiCondInput) ?>;
 			var distinctCondInput=<?php echo json_encode($distinctCondInput) ?>;
 			    var validInputList= <?php echo json_encode($validInputList) ?>;
 			    var currentForm = <?php echo json_encode($form) ?>;
 			    var input=<?php echo json_encode($input) ?>;
 			    var parentForm= <?php echo json_encode($parentForm) ?>;
	 			var samePrevVal = <?php echo json_encode($samePrevVal) ?>;
	 			var required = <?php echo json_encode( (isset($value["required"]) && $value["required"]) ? true : false ) ?>;
	 		$("#<?php echo $value["value"] ?>").change(function() {
	 			
			    if($(this).prop('checked')) {
			    	mylog.log("samePrevVal",samePrevVal);
			        $('#question<?php echo $value["input"] ?>').removeClass("hide");
	// --- to do : find a solution so that it is mandatory ! ---------		    
			    setTimeout(function() {
                    //alert("focus");
                  if(required && currentForm.inputs[<?php echo $value["input"] ?>].type=="textarea"){  
			        $('.active-markdowntextarea<?php echo $value["input"] ?>').focus();
			        $('.active-markdowntextarea<?php echo $value["input"] ?>').blur(function (e) {
			        	var valInput=$(this).val().trim();
			          
			        	if(valInput==""){
			        		setTimeout(
			        			scrollintoDiv(e.target.id,1000)
			        		,500); 
			        			     //$('body').scrollTo('#'+domId);
			        		$('.active-markdowntextarea<?php echo $value["input"] ?>').attr("placeholder","Champ obligatoire").css("background-color", "#f5b7b8").focus();

			        	}else if(typeof input.conditional[<?php echo $key+1?>] !="undefined" && <?php echo $value["value"] ?> == input.conditional[<?php echo $key+1?>].value){
			        		$('#'+input.conditional[<?php echo $key+1?>].input).trigger("blur");
			            	     
			            }
			          
			        });
			      }
			    },500);


			        if(!samePrevVal){
			            scrollintoDiv("question<?php echo $value["input"] ?>", 1000);
			        }
			        //alert(required);

// --- to do : find a solution so that it is mandatory ! ---------
			        // if(required){
			        // 	//alert("ok");y
			        // 	mylog.log("validInputList",validInputList);
			        // 	validInputList.push('<?php echo $value["input"] ?>');
			        // 	mylog.log("validInputList2",Object.values(validInputList));
           //              var reqCondInput = {
           //              	id : parentForm._id.$id,
           //              	collection : "forms",
           //              	path : "params.validateStep"+valStepKey+".inputList",
           //              	value : validInputList

           //              };
           //              dataHelper.path2Value(reqCondInput , function(params) { 
           //                  reloadInput(valStepKey, "<?php echo $formId ?>");
           //              } );
                        //mylog.log("reqCondInput",reqCondInput);
			    //    }
			           
			    }else{
			    	$('.active-markdowntextarea<?php echo $value["input"] ?>').off("blur");
			    	$('.active-markdowntextarea<?php echo $value["input"] ?>').attr("placeholder","").css("background-color", "unset").val("");

			    }
			});
			if($("#<?php echo $value["value"] ?>").is(':checked')) {
				
			    $('#question<?php echo $value["input"] ?>').removeClass("hide");
			    
			    // scrollintoDiv("question<?php echo $value["input"] ?>");
			}

			$('.radioCo.<?php echo $kunik ?>').change(function(){
				if(!$("#<?php echo $value["value"] ?>").prop('checked') && multiCondInput.length==0) {
			        $('#question<?php echo $value["input"] ?>').addClass("hide");
			        	// --- TO DO : remove the required field on conditional if answer changes		        
			        			        // if(required){
			        			        // 	//alert("ok");y
			        			        // 	mylog.log("validInputList",validInputList);

			        			        // 	validInputList.pop();
			        			        // 	validInputList=(notEmpty(validInputList)) ? validInputList: [];
			        			        // 	mylog.log("validInputList2",validInputList);
			                   //              var reqCondInput = {
			                   //              	id : parentForm._id.$id,
			                   //              	collection : "forms",
			                   //              	path : "params.validateStep"+valStepKey+".inputList",
			                   //              	value : validInputList

			                   //              };
			                   //              dataHelper.path2Value(reqCondInput , function(params) { 
			                   //                  reloadInput(valStepKey, "<?php  $formId ?>");
			                   //              } );
			                   //              //mylog.log("reqCondInput",reqCondInput);
			        			        // }
			    }
			});

		});
 	</script>
 	<style type="text/css">
 		#question<?php echo $value["input"] ?> {
		    -webkit-transition: all 2s ease;
		    -moz-transition: all 2s ease;
		    -o-transition: all 2s ease;
		    transition: all 2s ease;
		}
 	</style>

 <?php
	}
 }

if( isset($parentForm["params"][$kunik]) ) {
		if( isset($parentForm["params"][$kunik]["global"]["list"]) ) { 
			$paramsData["list"] =  $parentForm["params"][$kunik]["global"]["list"];
			foreach ($paramsData["list"] as $k => $v) {
			    if(isset($parentForm["params"][$kunik]["tofill"][$v]) ){
			    	$paramsData["tofill"] += array($v => $parentForm["params"][$kunik]["tofill"][$v]);
			    } else {
			    	$paramsData["tofill"] += [$v => "simple"];
			    }
			}
		}
}



if( isset($parentForm["params"][$kunik]) ) {
		if( isset($parentForm["params"][$kunik]["global"]["list"]) ) { 
			//$paramsData["list"] =  $parentForm["params"][$kunik]["global"]["list"];
			foreach ($paramsData["list"] as $k => $v) {
			    if(isset($parentForm["params"][$kunik]["placeholdersradio"][$v]) ){
			    	$paramsData["placeholdersradio"] += array($v => $parentForm["params"][$kunik]["placeholdersradio"][$v]);
			    } else {
			    	$paramsData["placeholdersradio"] += [$v => ""];
			    }
			}
		}
}


if($mode == "r")
{ 
?>
   <div class="" style="padding-top: 50px; ">
    	<label class="form-check-label" for="<?php echo $key ?>">
    		<h4 style="/*text-transform: unset;*/ color: <?php echo ($titleColor) ? $titleColor : "black"; ?>;"> <?php echo $label?>
    		</h4>
    	</label>	
    	<?php 
    	if(!empty($info)){ 
        ?>
        <br/>
    	<small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
    	<?php
    	}
    	?>    	
    

    	<?php 
    	if( !isset($parentForm["params"][$kunik]["global"]["list"]) ) 
    	{
    		echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> THIS FIELD HAS TO BE CONFIGURED FIRST </span>";
    	} else 
    	{
    	?>
		
			<div class="">
    			<table style="width: 100%">
  				<tr class=""><th >
  				<div class="col-md-12">
	    			<?php 
		    		foreach ($parentForm["params"][$kunik]["global"]["list"] as $ix => $lbl) 
		    		{ 
		    			
						if(isset($answer["answers"][$form["id"]][$kunik]["value"]) && $answer["answers"][$form["id"]][$kunik]["value"] == $lbl)
						{
							
		    		?>
		    			<!-- <tr class="thradio"><theih > -->
		    			<div class="parent-thradio col-md-<?php echo (isset($parentForm["params"][$kunik]['global']['width'])) ? $parentForm["params"][$kunik]['global']['width'] : '12'; ?> col-xs-12 col-sm-12" style="min-height: 50px;">
		    			<div class="">
			    			
	  						<label for="<?php echo $kunik.$ix ?>" class="responsemultiradio">
	  							<span>
	  								
	  							</span>
	  							<span><?php echo $lbl ?> 
		  							
		  				
		  								<?php 
		  								if(count($paramsData["tofill"]) != 0)
		  								{
		  									foreach ($paramsData["tofill"] as $key => $value)
		  									{
		  										if($lbl == $key && $value == "cplx")
		  										{
		  								?>
		  											<?php 
		  											if(isset($answer["answers"][$form["id"]][$kunik]["textsup"]) && isset($answer["answers"][$form["id"]][$kunik]["value"]) && $answer["answers"][$form["id"]][$kunik]["value"] == $key ){
		  								 				echo $answer["answers"][$form["id"]][$kunik]["textsup"] ;
		  												} ?>

		  								 			
	           																	
				  						<?php
				  								}
				  							}
				  						}
				  						?>
		  					 		
								</span>
							</label> 
	  					</div>
	  					</div>
		    		<?php 
		    		} } 
		    		?>
		    	</div>
		    	</th></tr>
				</table>
			</div>
		<?php 
		}
		?>
	</div>

<?php 
}else
{
?>


	<div class="" style="padding-top: 50px; ">
    	<label class="form-check-label" for="<?php echo $key ?>">
    		<h4 style="/*text-transform: unset;*/ color: <?php echo ($titleColor) ? $titleColor : "black"; ?>;"><?php echo $label.$editQuestionBtn.$editParamsBtn ?>
    		</h4>
    	</label>
    	<?php 
    	if(!empty($info)){ 
        ?>
    	<br/><small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
    	<?php
    	}
    	?> 		
    
    	<?php 
    	if( !isset($parentForm["params"][$kunik]["global"]["list"]) ) 
    	{
    		echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> THIS FIELD HAS TO BE CONFIGURED FIRST ".$editParamsBtn."</span>";
    	} else 
    	{
    	?>
		
			<div class="rdo-grp">
    			<table style="width: 100%">
  				<tr class=""><th >
  				<div class="col-md-12 no-padding">
	    			<?php 
		    		foreach ($parentForm["params"][$kunik]["global"]["list"] as $ix => $lbl) 
		    		{ 
		    			$ckd =  "";
						if(isset($answer["answers"][$form["id"]][$kunik]["value"]) && $answer["answers"][$form["id"]][$kunik]["value"] == $lbl)
						{
							$ckd =  "checked";
						}
					
		    		?>
		    			<!-- <tr class="thradio"><theih > -->
		    			<div class="parent-thradio col-md-<?php echo (isset($parentForm["params"][$kunik]['global']['width'])) ? $parentForm["params"][$kunik]['global']['width'] : '12'; ?> col-xs-12 col-sm-12 no-padding" style="min-height: 50px;">
		    			<div class="thradio">
			    			<input data-id="<?php echo $kunik ?>" class=" radioCo <?php echo $kunik ?>"  id="<?php echo $kunik.$ix ?>" data-form='<?php echo $form["id"] ?>' <?php echo $ckd?> type="radio" name="<?php echo $kunik ?>" data-type="<?php 

			    				if(count($paramsData["tofill"]) != 0)
			    				{
	  								foreach ($paramsData["tofill"] as $key => $value)
	  								{
	  									if($lbl == $key)
	  									{
	  										if($value == "cplx")
	  										{
	  											echo "cplx";
	  										}else 
	  										{
	  											echo "simple";
	  										}
	  									}
	  								}
	  						 	} 
	  						?>" value="<?php echo $lbl ?>" />

	  						<label for="<?php echo $kunik.$ix ?>" class="container<?php echo $kunik ?>">
	  							<span class="checkmark<?php echo $kunik ?>">
	  								
	  							</span>
	  							<span><?php echo $lbl ?>
		  							<div style="position: relative; display: inline-block;">
		  				
		  								<?php 
		  								if(count($paramsData["tofill"]) != 0)
		  								{
		  									foreach ($paramsData["tofill"] as $lilkey => $value)
		  									{
		  										if($lbl == $lilkey && $value == "cplx")
		  										{
		  								?>
		  											<input disabled class="effect-1 inputRadioCo <?php echo $kunik ?>" data-id="<?php echo $kunik ?>" data-form='<?php echo $form["id"] ?>' type="text" data-imp="<?php echo $lilkey; ?>" placeholder="<?php echo $paramsData['placeholdersradio'][$lilkey]; ?>" style="display: inline-block !important; position: relative;"  value="<?php 
		  											if(isset($answer["answers"][$form["id"]][$kunik]["textsup"]) && isset($answer["answers"][$form["id"]][$kunik]["value"]) && $answer["answers"][$form["id"]][$kunik]["value"] == $lilkey ){
		  								 				echo $answer["answers"][$form["id"]][$kunik]["textsup"] ;
		  												} ?>" >

		  								 			<span class="focus-border"></span>
	           																	<!-- az -->
				  						<?php
				  								}
				  							}
				  						}
				  						?>
		  					 		</div>

		  							<?php 

		  							if($canEditForm)
		  							{


// <<<<<<< HEAD
		  								echo " <a href='javascript:;' data-id='".$parentForm["_id"]."'  data-collection='".Form::COLLECTION."'  data-path='params.".$kunik."' class='previewTpl paramsonebtn editone".$kunik."Params".$ix." '><i class='fa fa-cog'></i> </a>";
// =======
// 		  								echo " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl paramsonebtn editone".$kunik."Params".$ix." '><i class='fa fa-cog'></i> </a>";
// >>>>>>> 129c8a9de4164acd99bc3d39bf09929f69b9bbaa

							

		  								if(count($paramsData["tofill"]) != 0)
		  								{
		  									foreach ($paramsData["tofill"] as $lilkey => $value)
		  									{
		  										if($lbl == $lilkey)
		  										{
		  											if($value == "cplx")
		  											{
//<<<<<<< HEAD
		  												echo " <a alt='placeholder' href='javascript:;' data-id='".$parentForm["_id"]."'  data-collection='".Form::COLLECTION."'  data-path='params.".$kunik."' class='previewTpl paramsonebtnP editonep".$kunik."Params".$ix." '><i class='fa fa-pencil'></i> </a>";
// =======
// 		  												echo " <a alt='placeholder' href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl paramsonebtnP editonep".$kunik."Params".$ix." '><i class='fa fa-pencil'></i> </a>";
// >>>>>>> 129c8a9de4164acd99bc3d39bf09929f69b9bbaa
		  											}
		  										}
		  									}
		  						 		} 

		  							}
		  							?>
								</span>
							</label> 
	  					</div>
	  					</div>
		    		<?php 
		    		} 
		    		?>
		    	</div>
		    	</th></tr>
				</table>
			</div>
		<?php 
		}
		?>
	</div>

<script type="text/javascript">
var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answer["answers"][$kunik])) ? $answer["answers"][$kunik] : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

var canEditForm=<?php echo json_encode($canEditForm)?>;


sectionDyf.<?php echo $kunik ?>ParamsPlace = "";


jQuery(document).ready(function() {
    mylog.log("render form input","/modules/costum/views/tpls/forms/cplx/multiCheckbox.php");

    	answer = {};
    	$('.radioCo.<?php echo $kunik ?>').change(function(e){
    		mylog.log("événment changement bouton radio",e);
    		 var select = $(this);
    		var option = $(this).val();
    		var domId=e.target.id;
    		// if($(this).data("type") == "simple"){
    		// 	answer.path = "answers."+$(this).data("form")+"."+$(this).data("id");
	    	// 	answer.collection = "answers" ;
	    	// 	answer.id = "<?php echo $answer["_id"]; ?>";
	    	// 	answer.value = {"value" : $(this).val(), "type" : "simple"};
	    	// 	dataHelper.path2Value(answer , function(params) { 
	     //        	toastr.success('Mise à jour enregistrée');
	     //    	} );
    		// } else if ($(this).data("type") == "cplx"){
				var self = this;
    			answer.path = "answers."+$(this).data("form")+"."+$(this).data("id");
	    		answer.collection = "answers" ;
	    		answer.id = "<?php echo $answer["_id"]; ?>";
	    		answer.value = ($(this).data("type") == "cplx") ? {"value" : $(this).val(), "type" : "cplx", "textsup" : $('input[data-imp="' + $(this).val() + '"]').val()} : {"value" : $(this).val(), "type" : "simple"} ;
				if(typeof ownAnswer != 'undefined') {
					if(ownAnswer[$(self).attr("data-form")] && ownAnswer[$(self).attr("data-form")][$(self).attr("data-id")]) {
						ownAnswer[$(self).attr("data-form")][$(self).attr("data-id")] = answer.value;
					} else if(ownAnswer[$(self).attr("data-form")]){
							ownAnswer[$(self).attr("data-form")][$(self).attr("data-id")] = answer.value 
					}
				}
				if(typeof removeError != "undefined") {
					const elem = $(`li#question${$(self).attr("data-id").replace('multiRadio', '')}`);
					removeError(
						elem, 
						{
							'border' : 'none'
						},
						'error-msg'
					)
				}
	    		dataHelper.path2Value(answer , function(params) { 
	            	//toastr.success('Mise à jour enregistrée');
	            	//stepValidationReload<?php echo $form["id"]?>();
	            		if($("#"+domId).is(':checked')){
	            			$('.inputRadioCo.<?php echo $kunik ?>[data-imp="'+option+'"]').prop("disabled", false).focus();
	            		    $('.inputRadioCo.<?php echo $kunik ?>[data-imp="'+option+'"]').focusout(function () {
	            			    if($(this).val()==""){
	            			    	setTimeout(
	            				     scrollintoDiv(domId,1000)
	            				    ,500); 
	            				     //$('body').scrollTo('#'+domId);
	            				     $('.inputRadioCo.<?php echo $kunik ?>[data-imp="'+option+'"]').attr("placeholder","Champ obligatoire").css("background-color", "#f5b7b8").focus();

	            			    }else{
	            	    	        $('.inputRadioCo.<?php echo $kunik ?>[data-imp="'+option+'"]').css("background-color", "unset").attr("placeholder","");
	            	            }
	            		    });
	            		    mylog.log("unset other cplx option");
	            		    $('.inputRadioCo.<?php echo $kunik ?>').each(function(){
	            		    	if($(this).parent().parent().parent().siblings(".radioCo").attr("id")!=domId){
	            		    		$(this).off("focusout").attr("placeholder","").css("background-color", "unset").val("").prop("disabled", true);
	            		    	}
	            	    		//alert($(this).parent().attr("id"));
	            	    		
	            	    	});	
	            	    }else{
	            	    	mylog.log("unset cplx option");
	            	    	$('.inputRadioCo.<?php echo $kunik ?>[data-imp="'+option+'"]').off("focusout");
	            	    	$('.inputRadioCo.<?php echo $kunik ?>[data-imp="'+option+'"]').attr("placeholder","").css("background-color", "unset").val("");
	            	    	
	            	    }
	        	}, false );
    		//}

    		var radioChecked = $(this);

    // 		 $('#question<?php echo $cle ?> .radioCo').each(function(){
			 //      $(this).prop('checked', false);
			 //      alert('sdfsd');
			 // });

			 // $(this).prop('checked', true);
    	});

    	
    	$('.inputRadioCo.<?php echo $kunik ?>').keyup(function(){ 
    		if($('input[value="' + $(this).data("imp") + '"]').is(':checked')) {

    			answer.path = "answers."+$(this).data("form")+"."+$(this).data("id");
    			
	    		answer.collection = "answers" ;
	    		answer.id = "<?php echo $answer["_id"]; ?>";
	    		answer.value = {"value" : $(this).data("imp"), "type" : "cplx", "textsup" : $(this).val()};
	    		dataHelper.path2Value(answer , function(params) { 
	            	//toastr.success('Mise à jour enregistrée');
	        	} );
	    	}
    	});

    if(canEditForm && location.hash.indexOf("#answer")==-1){
    	$(document).on("keyup", ".addmultifield", function() {
	    	let value = $(this).val();
	 			let regex = /[.\$]/g;
	 			if (value.match(regex)) {
	   			value = value.replace(regex, "");
	   			$(this).val(value);
	   			toastr.info("Carractère non autorisé");
	 			}
			});
    }
    else{
    	$(".addmultifield").off("keyup","**");
    }	

    	sectionDyf.<?php echo $kunik ?>Params = {
			"jsonSchema" : {	
		        "title" : "<?php echo $label ?> config",
		        "icon" : "cog",
		        "properties" : {
		            list : {
		                inputType : "array",
		                label : "Liste de bouton radio",
		                values :  sectionDyf.<?php echo $kunik ?>ParamsData.list
		            },
		            width : {
	                    inputType : "select",
	                    label : "Nombre d'element par ligne",
	                    options :  sectionDyf.<?php echo $kunik ?>ParamsData.width,
	                    value : "<?php echo (isset($parentForm["params"][$kunik]['global']['width']) and $parentForm["params"][$kunik]['global']['width'] != "") ? $paramsData["width"][strval($parentForm["params"][$kunik]['global']['width'])] : ''; ?>"
	                }
		        },
		        save : function () {  
		            tplCtx.value = {};
		            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
		        		 if(val.inputType == "properties")
	                                tplCtx.value = getPairsObj('.'+k+val.inputType);
	                            else if(val.inputType == "array")
	                                tplCtx.value[k] = getArray('.'+k+val.inputType);
	                            else if(val.inputType == "formLocality")
	                                tplCtx.value[k] = getArray('.'+k+val.inputType);
	                            else
	                                tplCtx.value[k] = $("#"+k).val();
		        	});
		            mylog.log("save tplCtx",tplCtx);

		            if(typeof tplCtx.value == "undefined")
		            	toastr.error('value cannot be empty!');
		            else {
		                dataHelper.path2Value( tplCtx, function(params) { 
		                    dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
		                    reloadInput("<?php echo $cle ?>", "<?php echo (string)$form["id"] ?>");
		                } );
		            }

		    	}
		    }
		};


   	<?php if(count($paramsData["list"]) != 0){
	  	foreach ($paramsData["list"] as $key => $value) {
	  		
	  		// if($lbl == $key){
	  		// 	if($value == "cplx"){
	?>
	
	sectionDyf.<?php echo $kunik ?>oneParams<?php echo $key ?> = {
		"jsonSchema" : {	
	        "title" : "<?php echo $label ?> config",
	        "icon" : "cog",
	        "properties" : {
	            tofill : {
	                inputType : "select",
	                label : "Type de l'option",
	                options :  sectionDyf.<?php echo $kunik ?>ParamsData.type,
                  value : "<?php echo (isset($paramsData["tofill"][$value])) ? $paramsData["tofill"][$value] : ''; ?>"
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>oneParams<?php echo $key ?>.jsonSchema.properties , function(ko,valo) { 

                            $.each(sectionDyf.<?php echo $kunik ?>ParamsData.tofill, function(ke, va) {
                            	
                            	if(ke == "<?php echo $value; ?>"){
                            		
                            		var azerazer = ke.toString();
                            		sectionDyf.<?php echo $kunik ?>ParamsData.tofill[azerazer] = $("#"+ko).val();
                            	}
                            })

                            tplCtx.value = sectionDyf.<?php echo $kunik ?>ParamsData.tofill;


	        	});
	            mylog.log("save tplCtx",tplCtx);
	            
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
	                    reloadInput("<?php echo $cle ?>", "<?php echo (string)$form["id"] ?>");
	                } );
	            }

	    	}
	    }
	};
	
	$(".editone<?php echo $kunik ?>Params<?php echo $key ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path")+'.tofill';
        //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>oneParams<?php echo $key ?>,null, sectionDyf.<?php echo $kunik ?>oneParams<?php echo $key ?>);
    });
	<?php
	  			}
	  		}
	//   	}
	// } 
	?>


<?php if(count($paramsData["list"]) != 0){
	  	foreach ($paramsData["list"] as $key => $value) {
	  		// if($lbl == $key){
	  			if($paramsData["tofill"][$value] == "cplx"){
	?>

	sectionDyf.<?php echo $kunik ?>oneParamsPlaceholders<?php echo $key ?> = {
		"jsonSchema" : {	
	        "title" : "<?php echo $label ?> config",
	        "icon" : "cog",
	        "properties" : {
	            placeholdersradio : {
                    inputType : "text",
                    label : "Modifier les textes à l'interieur du champs de saisie",
                    values :  "<?php echo $paramsData["placeholdersradio"][$value]; ?>"
                }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>oneParamsPlaceholders<?php echo $key ?>.jsonSchema.properties , function(kk,vall) { 

	            			
                            $.each(sectionDyf.<?php echo $kunik ?>ParamsData.placeholdersradio, function(ke, va) {
                            	if(ke == "<?php echo $value; ?>"){
                            		
                            		var azerazer = ke.toString();
                            		sectionDyf.<?php echo $kunik ?>ParamsData.placeholdersradio[azerazer] = $("#"+kk).val();
                            		
                            	}
                            })
                            

                            tplCtx.value = sectionDyf.<?php echo $kunik ?>ParamsData.placeholdersradio;


	        	});
	            mylog.log("save tplCtx",tplCtx);

	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
	                    reloadInput("<?php echo $cle ?>", "<?php echo (string)$form["_id"] ?>");
	                } );
	            }

	    	}
	    }
	};

	$(".editonep<?php echo $kunik ?>Params<?php echo $key ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path")+'.placeholdersradio';
        //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>oneParamsPlaceholders<?php echo $key ?>,null, sectionDyf.<?php echo $kunik ?>oneParamsPlaceholders<?php echo $key ?>);
    });

<?php
	  			}
	  		}
	   	}
	// } 
?>

	$(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path")+'.global';
        //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    
});
</script>
<?php } ?>