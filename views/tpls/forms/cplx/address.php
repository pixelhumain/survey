<style type="text/css">
    .dropdown-menu.dropdown-address{
        padding:unset !important;

    }

</style>

<?php
    $value = "";
    if(!empty($answer) && isset($answer["answers"][$form["id"]][$key])) 
    	$value =  $answer["answers"][$form["id"]][$key];
    else if(!empty($answer) && isset($answer["answers"][$key])) 
	    $value =  $answer["answers"][$key];

    if($mode == "r" || $mode == "pdf"){ 

?>
        <div class="col-xs-12" id="<?php echo $kunik ?>">
            <label for="<?php echo $kunik ?>">
                <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
                    <?php echo $label ?>
                </h4>
            </label>
            <br>
            <?php 
            if(!empty($value)){
                if(!empty($value["address"]) && !empty($value["address"]["name"]) ){
                    echo $value["address"]["name"]."<br>";
                }
                if(!empty($value["addresses"]) ){
                    foreach ($value["addresses"] as $keyAdd => $valAdd) {
                        if( !empty($valAdd["name"]) )
                            echo @$valAdd["name"]."<br>";
                    }
                }
            }
            ?>
        </div>
<?php 
    }else{

        $cssAnsScriptFilesModule = array( 
            '/js/address.js',
        );

        HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl() );

        $inpClass = "form-control";
?>
    <div class="col-md-12">
        <div class="form-group" id="<?php echo $kunik ?>" data-inputtype="address" data-mapobj="cplxAddObj<?php echo $kunik ?>">
            <label for="<?php echo $kunik ?>">
                <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
                    <?php echo $label.$editQuestionBtn ?>
                    <?php if($canEditForm && $mode="fa"){ ?>
                        <a href="javascript:;" class="btn btn-xs btn-danger config<?= $kunik ?>"><i class="fa fa-cog"></i></a>
                    <?php } ?>
                </h4>
            </label>
            <?php if(!empty($info)){ ?>
                <small id="<?php echo $kunik ?>Help" class="col-xs-12 margin-10 form-text text-muted"><?php echo $info ?></small>
            <?php } ?>

            <div id="addressForm<?php echo $kunik ?>"></div>
        </div>
    </div>

    <script type="text/javascript">
       // 137 rue des villages, Cangey
        var cplxAddObj<?php echo $kunik ?> = {};
        var keyAdd<?php echo $kunik ?> ="<?php echo $kunik ?>";
        var pathAdd<?php echo $kunik ?> ="<?php echo $form['id'].'.'.$key ; ?>";
        var valueAdd<?php echo $kunik ?> =<?php echo json_encode( $value ); ?>;
        
        jQuery(document).ready(function() {
            mylog.log("render form input","/modules/costum/views/tpls/forms/cplx/address.php");
            
            var paramsInitAddress<?php echo $kunik ?> = {
                container : "#addressForm"+"<?php echo $kunik ?>",
                keyUnik : "<?php echo $kunik ?>",
                result : valueAdd<?php echo $kunik ?>,
                input : {
                    id : 'inputAdd',
                    class : "<?php echo $inpClass ?>",
                    placeholder : "<?php echo (isset($placeholder)) ? $placeholder : '' ?>",
                    "aria-describedby" : "<?php echo $kunik ?>Help",

                },
                config : <?= !empty($parentForm["params"][$kunik]) ? json_encode($parentForm["params"][$kunik]) : "{}" ?>
            };

            var countryCode = <?php echo (isset($form["inputs"][$key]["country"])) ? json_encode($form["inputs"][$key]["country"]) : "{}" ; ?>;

            mylog.log("paramscountrycode",countryCode);
            
            addressObj.countryCodeList = countryCode;

            mylog.log("paramscountry",addressObj.countryCodeList);

            cplxAddObj<?php echo $kunik ?> = addressObj.init(paramsInitAddress<?php echo $kunik ?>);
            
            cplxAddObj<?php echo $kunik ?>.saveAuto = function(data){
                var answer = {
                    collection : "answers",
                    id : answerObj._id.$id,
                    path : "answers."+pathAdd<?php echo $kunik ?>
                };

                answer.value =  cplxAddObj<?php echo $kunik ?>.getResultToFormData(cplxAddObj<?php echo $kunik ?>);

                mylog.log("addressObj.saveAuto", answer );

                dataHelper.path2Value( answer , function(params) {
                    toastr.success('saved');
                } );
            };
            
            $('.config<?= $kunik ?>').off().on('click',function(){
                var config<?=$kunik ?>ParamsData = <?= !empty($parentForm["params"][$kunik]) ? json_encode($parentForm["params"][$kunik]) : "{}" ?>;
                var config<?=$kunik ?>Params = {
                    "jsonSchema" : {	
                        "title" : "Configuration \"<?php echo $label ?>\"",
                        "icon" : "cog",
                        "properties" : {
                            defaultCountry : {
                                inputType : "selectMultiple",
                                select2:true,
                                label : tradDynForm["Pre-selected country"],
                                options : {}
                            },
                            availableCountry : {
                                inputType : "selectMultiple",
                                select2:true,
                                label : tradDynForm["Available country"],
                                options : {}
                            }
                        },
                        save : function () {  
                            var tplCtx = {
                                id : "<?= (string) @$parentForm["_id"] ?>",
                                path : "params.<?= $kunik ?>",
                                collection : "<?= Form::COLLECTION ?>",
                                value : {}
                            };

                            $.each( config<?=$kunik ?>Params.jsonSchema.properties , function(k,val) { 
                                if(val.inputType == "properties")
                                    tplCtx.value = getPairsObj('.'+k+val.inputType);
                                else if(val.inputType == "array")
                                    tplCtx.value[k] = getArray('.'+k+val.inputType);
                                else if(val.inputType == "formLocality")
                                    tplCtx.value[k] = getArray('.'+k+val.inputType);
                                else
                                    tplCtx.value[k] = $("#"+k).val();
                            });
                            mylog.log("save tplCtx",tplCtx);

                            if(typeof tplCtx.value == "undefined")
                                toastr.error('value cannot be empty!');
                            else {
                                dataHelper.path2Value( tplCtx, function(params) { 
                                    dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
                                    reloadInput("<?= $key ?>", "<?= (string)$form["id"] ?>");
                                } );
                            }

                        }
                    }
                };
                $.ajax({
                    type: "POST",
                    url: baseUrl+"/"+moduleId+"/opendata/getcountries/",
                    dataType: "json",
                    success: function(data){
                        var objCountry = {};
                        if(data!=null){
                            $.each(data, function(key, v){
                                mylog.log(v.countryCode,"objCountry");
                                objCountry[v.countryCode] = v.name;
                            });
                        }
                        
                        config<?=$kunik ?>Params.jsonSchema.properties.defaultCountry.options = objCountry;
                        config<?=$kunik ?>Params.jsonSchema.properties.availableCountry.options = objCountry;
                        dyFObj.openForm(config<?=$kunik ?>Params,null,config<?=$kunik ?>ParamsData);
                    },
                    error: function(error){
                        mylog.log("addressObj.initHTML error", error);
                    }
                });
            })
            

        //    allMaps.maps[keyAdd] = addressObj.init(paramsInitAddress);

        //    allMaps.maps[keyAdd].saveAuto = function(data){

            //  var answer = {
            //      collection : "answers",
            //      id : answerObj._id.$id,
            //      path : "answers."+keyAdd
            //  };

            //  answer.value =  allMaps.maps[keyAdd].result;

            //  mylog.log("addressObj.saveAuto", answer );

            //  dataHelper.path2Value( answer , function(params) {
            //      toastr.success('saved');
            //  } );
            // };
        });
    </script>
<?php } ?>