<?php if($answer){
    ?>
    <div class="form-group" >
        <?php
        $i = 0;
        $show = true;
        $arrayTitleList = [];
        if (isset($answers)) {
            foreach ($answers as $q => $a) {
                if (isset($a["type"])) {
                    if ($a["type"] == "multiList") {
                        $i++;
                        $arrayTitleList = $arrayTitleList + array($a["title_list"] => $a["title_list"]);
                    }
                }
            }
        }
        $cnt = 1;
        if(isset($answers)) {
            foreach ($answers as $q => $a) {
                $cnt++;
            }
        }
        if(isset($this->costum["form"]["params"][$kunik]['num_row']) and $this->costum["form"]["params"][$kunik]['num_row'] == "0" and isset($this->costum["form"]["params"][$kunik]["nbmax"]) and ((int)$this->costum["form"]["params"][$kunik]["nbmax"]) <= $i){
            $show = false;
        }
        $editBtnL="";

        $editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$el["_id"]."' data-collection='".$this->costum["contextType"]."' data-path='costum.form.params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

        $paramsData = [
            "nbmax" => "2", "num_row" => ["limité","illimité"], "nblist_max" => "2" ,"num_list" => ["limité","illimité"], "width" => ["1" => "1","2" => "2","2" => "2","3" => "3"]
        ];

        if( isset($this->costum["form"]["params"][$kunik]["nbmax"]) )
            $paramsData["nbmax"] =  $this->costum["form"]["params"][$kunik]["nbmax"];
        if( isset($this->costum["form"]["params"][$kunik]["limited"]) )
            $paramsData["num_row"] =  $this->costum["form"]["params"][$kunik]["num_row"];
        if( isset($this->costum["form"]["params"][$kunik]["nblist_max"]) )
            $paramsData["nblist_max"] =  $this->costum["form"]["params"][$kunik]["nblist_max"];
        if( isset($this->costum["form"]["params"][$kunik]["num_list"]) )
            $paramsData["num_list"] =  $this->costum["form"]["params"][$kunik]["num_list"];
        if( isset($this->costum["form"]["params"][$kunik]["width"]) )
            $paramsData["width"] =  $this->costum["form"]["params"][$kunik]["width"];

        $cssRow = "6";
        if( isset($this->costum["form"]["params"][$kunik]["width"]) ){
            if($paramsData["width"] == "1"){
                $cssRow =  "12";
            }elseif ($paramsData["width"] == "2"){
                $cssRow =  "6";
            }elseif ($paramsData["width"] == "3"){
                $cssRow =  "4";
            }else{
                $cssRow =  "6";
            }
        }



        $properties = [
            "title_list" => [
                "label" => "Titre",
                "placeholder" => "Titre",
                "inputType" => "text",
                "rules" => [ "required" => true ]
            ],
            "type" => [
                "placeholder" => "Type",
                "inputType" => "hidden",
                "rules" => [ "required" => true ]
            ],
            "parent" => [
                "placeholder" => "Sélectionner parent",
                "inputType" => "hidden",
                "rules" => [ "required" => false ]
            ]

        ];
        ?>
        <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
        <?php echo $info ?>

        <style>
            .s19{list-style: none;}
            .s19 li:before{
                content: '\f0a9';
                margin-right: 15px;
                font-family: FontAwesome;
                color: #d9534f;
            }
        </style>
        <div id="container-fluid">
            <div class="row come-in" style="
    white-space: normal;
">
        <?php
        $ct = 0;
        if(isset($answers)){
        foreach ($answers as $q => $a) {
            if($a['type'] == "multiList") {
                if($show){
                    $editBtnL = (Yii::app()->session["userId"] == $answer["user"]) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath.$cnt."' data-type='multiList' data-parent='".$a['title_list']."' class='add".$kunik." btn btn-default'><i class='fa fa-plus'></i> Ajouter une liste </a>" : "";
                }
                ?>
                <div class="col-lg-<?php echo $cssRow;?> col-md-6 col-sm-12 col-xs-12">
                    <div class="panel panel-info " style="
    white-space: pre-line;
">
                        <div
                            class="panel-heading" style="
    white-space: normal;
"><?php echo $a['title_list']; ?>
                            <div class="pull-right"><?php
                                echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [
                                    "canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
                                    "id" => $answer["_id"],
                                    "collection" => Form::ANSWER_COLLECTION,
                                    "q" => $q,
                                    "path" => $answerPath.$q,
                                    "keyTpl"=>$kunik
                                ] ); ?>
                                <a href="javascript:;" data-id='<?php echo $answer["_id"] ?>' data-collection='<?php echo Form::ANSWER_COLLECTION;?>' data-path='<?php echo $answerPath.$cnt; ?>' data-type="list" data-parent="<?php echo $a['title_list'];?>" class="btn btn-xs btn-primary add<?php echo $kunik; ?>"> <i class='fa fa-plus'></i></a></div>
                        </div>
                        <div class="panel-body" style="white-space: normal">
                            <ol class="s19" style="
    white-space: normal;
">
                                   <?php  foreach ($answers as $qq => $aa) {
                                        if($aa['type'] == "list" and $aa['parent'] == $a['title_list']) {?>
                                            <li> <?php echo $aa['title_list'] ?>
                                                <div class="pull-right"><?php
                                                    echo $this->renderPartial( "survey.views.tpls.forms.cplx.editDeleteLineBtn" , [

                                                        "canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
                                                        "id" => $answer["_id"],
                                                        "collection" => Form::ANSWER_COLLECTION,
                                                        "q" => $qq,
                                                        "path" => $answerPath.$qq,
                                                        "keyTpl"=>$kunik
                                                    ] ); ?></div>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                            </ol>

                        </div>
                    </div>
                </div>
                <?php
            }
            
        }

        }

        ?>
            </div>
        </div>

    </div>
    <script type="text/javascript">

        var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

        $(document).ready(function() {

            sectionDyf.<?php echo $kunik ?> = {
                "jsonSchema" : {
                    "title" : "MultiListe",
                    "icon" : "fa-list",
                    "text" : "Configurer liste",
                    "properties" : <?php echo json_encode( $properties ); ?>,
                    save : function () {
                        var today = new Date();
                        tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
                        $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                        });

                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");//$("#ajax-modal").modal('hide');
                                location.reload();
                            } );
                        }
                    }
                }
            };

            sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema" : {
                    "title" : "<?php echo $kunik ?> config",
                    "description" : "Liste de question possible",
                    "icon" : "fa-cog",
                    "properties" : {
                        num_row : {
                            inputType : "select",
                            label : "Limitation",
                            options :  sectionDyf.<?php echo $kunik ?>ParamsData.num_row,
                            value : "<?php echo (isset($this->costum["form"]["params"][$kunik]['num_row'])) ? $this->costum["form"]["params"][$kunik]['num_row'] : ''; ?>"

                        },
                        nbmax : {
                            inputType : "text",
                            label : "Nombre d'element maximal",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsData.nbmax
                        },
                        width : {
                            inputType : "text",
                            label : "Nombre d'element par ligne",
                            options :  sectionDyf.<?php echo $kunik ?>ParamsData.width,
                            value : "<?php echo (isset($this->costum["form"]["params"][$kunik]['width'])) ? $this->costum["form"]["params"][$kunik]['width'] : ''; ?>"
                        }
                    },
                    save : function () {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                            if(val.inputType == "properties")
                                tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                            else if(val.inputType == "array")
                                tplCtx.value[k] = getArray('.'+k+val.inputType);
                            else
                                tplCtx.value[k] = $("#"+k).val();
                            mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                        });
                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                                location.reload();
                            } );
                        }

                    }
                }
            };


            mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");


            //adds a line into answer
            $(".add<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                if ($(this).data("type")) {
                     tplCtx.type = $(this).data("type");
                }
                if ($(this).data("parent")) {
                    tplCtx.parent = $(this).data("parent");
                }
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, {"type" : tplCtx.type, "parent" : tplCtx.parent});
            });

            $(".edit<?php echo $kunik ?>").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
            });

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                //if no params config on the element.costum.form.params.<?php echo $kunik ?>
                //then we load default values available in forms.inputs.<?php echo $kunik ?>xxx.params
                //mylog.log(".editParams",sectionDyf.<?php echo $kunik ?>Params,calData);
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            });


        });
    </script>
<?php } else {
    //echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>