<?php 
$value = (!empty($answers)) ? " value='".$answers."' " : "";

$inpClass = "form-control";

if($type == "tags") 
	$inpClass = "select2TagsInput";

if($saveOneByOne)
	$inpClass .= " saveOneByOne";

if($mode == "pdf"){ 
   
}else{
?>
	<div class="">
	    <label for="<?php echo $key ?>"><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn ?></h4></label>
	    <div>
	    	<button class="btn btn-primary" id="generate<?php echo $kunik ?>">generate</button>
	    </div>
	</div>

<script type="text/javascript">
	jQuery(document).ready(function() {
	    mylog.log("render form input","/modules/costum/views/tpls/forms/text.php");
	});

	var generate<?php echo $kunik ?> = new Object;

	$("#generate<?php echo $kunik ?>").click(function(){
		var canGenerate = true;

		if (typeof answerObj.answers.aressForm1 != 'undefined') {
	  		if (typeof answerObj.answers.aressForm1.multitextvalidationaressForm11 != 'undefined') {
	  			generate<?php echo $kunik ?>.name = answerObj.answers.aressForm1.multitextvalidationaressForm11;
	  			
	  			var output = answerObj.answers.aressForm1.multitextvalidationaressForm11.replace(/(\w+)(?:\s+|$)/g, function(_, word) {
	  				// uppercase first letter and add rest unchanged
	  				return word.charAt(0).toUpperCase() + word.substr(1);
				});

				generate<?php echo $kunik ?>.slug = output[0].toLowerCase() +  
	            output.slice(1); 

			} else {
				canGenerate = false;
			}

			if (typeof userId != 'undefined') {
	            generate<?php echo $kunik ?>.creator = userId;
	        } else {
				canGenerate = false;
			}

	        generate<?php echo $kunik ?>.preference = {
		        "isOpenData" : "true",
		        "isOpenEdition" : "true"
		    }

		    generate<?php echo $kunik ?>.role = "admin";
	     	generate<?php echo $kunik ?>.type = "NGO";
	     	generate<?php echo $kunik ?>.links = { "members" : {}};
		    generate<?php echo $kunik ?>.links.members[generate<?php echo $kunik ?>.creator] = {
		                "isAdmin" : true,
		                "type" : "citoyens"
		            };
		    
		        
		    
		} else {
			canGenerate = false;
		}

		mylog.log("here", canGenerate);



	});
	
</script>
<?php } ?>