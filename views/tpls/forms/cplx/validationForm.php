<?php 

if($mode != "pdf"){
	//var_dump($input);
?>
<div class="">
    <label for="validation<?php echo $kunik ?>"><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn ?></h4></label>
    <div class="text-center">
    	<a href="javascript:;" class="btn btn-primary" id="validation<?php echo $kunik ?>">Validation</a>
    </div>
</div>

<script type="text/javascript">
	var paramsInput = <?php echo json_encode($input); ?>;
	jQuery(document).ready(function() {
	    mylog.log("render form input","/modules/costum/views/tpls/forms/cplx/validate.php");
	    $("#question<?php echo $key ?> #validation<?php echo $kunik ?>").off().click(function(){
			var paramsValidate = {
				answerId : answerId,
				input : paramsInput
			};

			ajaxPost(
		        null,
		        baseUrl+"/survey/answer/validate",
		        paramsValidate,
		        function(data){
		        	toastr.success("Le formulaire à été soumis");
		        	if(typeof data.html != "undefined")
		        		$("#customHeader").html(data.html);
		        },
		        function(data){
		        	toastr.error("Un soucis est apparue. Contacter l'administrateur");    
		        }
		    );

		});
	});
</script>
<?php } ?>
