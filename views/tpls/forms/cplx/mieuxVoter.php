<?php
    $cssAndScriptFiles = array(
        '/css/inputs-style.css',
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFiles, Yii::app()->getModule(Survey::MODULE)->getAssetsUrl());
    $cle = $key ;
    
    $inpClass = " saveOneByOne";

    $editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

    $paramsData = [ 
        "list" => [	],
        "mention" => [
            "5" => "5",
            "6" => "6",
            "7" => "7"
        ],
        "mentions" => [
            "0" => "Excellent",
            "1" => "Très bien",
            "2" => "Bien",
            "3" => "Assez bien",
            "4" => "Passable",
            "5" => "Insuffisant",
            "6" => "À rejeter"
        ]
     ];

     if( isset($parentForm["params"][$kunik]) ) {
        if( isset($parentForm["params"][$kunik]["global"]["list"]) ) {
            $paramsData["list"] =  $parentForm["params"][$kunik]["global"]["list"];
            
        }
     }

     function countAttrVal($arrData, $attr) {
         $resTemp = 0;
         foreach($arrData as $elem) {
             $resTemp += $arrData[$attr];
         }
         return $resTemp;
     }

     if($mode == "r") {
        $allFormAnswers = PHDB::find(Form::ANSWER_COLLECTION, array("form" => (string)$parentForm['_id']));
        $reslutView = array();
        $nbVote = 0;
        foreach($allFormAnswers as $elem) {
            if(isset($elem['answers'][$form['id']][$key])) {
                if(count($elem['answers'][$form['id']][$key])) {
                    foreach($elem['answers'][$form['id']][$key] as $index => $item) {
                        // $keyFound = array_search($item['propos'], array_column(json_decode(json_encode($reslutView), TRUE), 'propos'));
                        // if($keyFound > -1) {
                        //     if($reslutView[$keyFound]['niveau'] == $item['niveau']) {
                        //        $reslutView[$keyFound]['count']++;
                        //     } else {
                        //         $reslutView[] = $item;
                        //     }
                        // } else {
                        //     $item['count'] = 1;
                        //     $reslutView[] = $item;
                        // }
                        if(isset($reslutView[$item['propos']])) {
                            // $nbVote++;
                            $keyFound = array_search($item['niveau'], array_column(json_decode(json_encode($reslutView[$item['propos']]), TRUE), 'niveau'));
                            if($keyFound > -1) {
                                $reslutView[$item['propos']][$keyFound]['count']++;
                            } else {
                                array_push($reslutView[$item['propos']], array('niveau' => $item['niveau'], 'count' => 1));
                            }
                        } else {
                            $reslutView[$item['propos']][0] = array('niveau' => $item['niveau'], 'count' => 1);
                            // $nbVote++;
                        }
                    }
                }
            }
            $nbVote++;
        }
        // $indexOfMaxVote = array_search(max(array_column($reslutView, 'count')), array_column(json_decode(json_encode($reslutView), TRUE), 'count'));
 ?>
 <style>
    @media (max-width: 575px) {
        .vote-flex-sm-wrap{
            flex-wrap: wrap !important;
        }
    }
 </style>
        <div class="pt-1">
            <div class="col-xs-12">
                <label for="form-check-label" for="<?php echo $key ?>">
                    <h4 style="text-transform: unset; color: <?php echo ($titleColor) ? $titleColor : "black"; ?>;"><?php echo $label ?>
                    </h4>
                </label>
            </div>
            <div class="col-xs-6 ml-1 mb-1">
                <?php 
                    foreach($answers as $index => $val) {
                ?>
                    <div>
                        <span class="text-bold"><?php echo $val['propos'] ?></span> : <small class="tab-mieuxvoter n<?php echo $val['niveau']; ?> badge"><?php echo $paramsData["mentions"][$val['niveau']] ?></small>
                    </div>
                <?php
                    }
                ?>
            </div>
            <br>
            <div class="col-xs-12">
                <?php 
                    if(count($reslutView)) {
                ?>
                        <div class="col-xs-12 p-0 card">
                            <div class="card-header">
                                <h4>Profil de mérites</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="profiles table-merite">
                                        <?php
                                            foreach($paramsData['list'] as $index => $item) {
                                                if($index == 0) {
                                        ?>
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <?php
                                                                foreach($paramsData['mentions'] as $i => $mentItem) {
                                                            ?>
                                                                    <th><small class="tab-mieuxvoter n<?php echo $i; ?> badge"><?php echo $mentItem; ?></small></th>
                                                            <?php
                                                                }
                                                            ?>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                        <?php
                                                }
                                        ?>
                                                    <tr>
                                                        <th><?php echo $item; ?></th>
                                                        <?php
                                                            if(isset($reslutView[$item])){
                                                                foreach($paramsData['mentions'] as $idMent => $mentItem) {
                                                                    $indexFound = array_search($idMent, array_column(json_decode(json_encode($reslutView[$item]), TRUE), 'niveau'));
                                                                    if($indexFound > -1) {
                                                        ?>
                                                                        <td>
                                                                            <?php echo round(($reslutView[$item][$indexFound]['count']*100)/$nbVote, 2, PHP_ROUND_HALF_EVEN); ?>
                                                                            %
                                                                        </td>
                                                        <?php
                                                                    } else {
                                                        ?>
                                                                        <td>0%</td>
                                                        <?php
                                                                    }
                                                                }
                                                            }
                                                        ?>
                                                    </tr>
                                        <?php
                                            }
                                        ?>
                                                    </tbody>
                                    </table>
                                </div>
                            </div>
                                
                            <span class="ml-1 text-nowrap">Nombre de votes: <span class="text-bold"><?php echo $nbVote ?></span></span>
                        </div>
                <?php
                    } 
                ?>
            </div>
         </div>
 <?php 
     } else {
?>
         <div class="pt-1">
            <label for="form-check-label" for="<?php echo $key ?>">
                <h4 style="text-transform: unset; color: <?php echo ($titleColor) ? $titleColor : "black"; ?>;"><?php echo $label.$editQuestionBtn.$editParamsBtn ?>
                </h4>
            </label>
         </div>
         
         <?php
            if(!isset($parentForm["params"][$kunik]["global"]["list"])) {
                echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> ".Yii::t("survey","THIS FIELD HAS TO BE CONFIGURED FIRST")." ".$editParamsBtn."</span>";
            } else {
        ?>
                            <!-- d-flex justify-content-center flex-xs-column flex-sm-column flex-md-row flex-lg-row -->
                <div class="col-xs-12 row d-flex justify-content-between vote-flex-sm-wrap" style="overflow: auto">
                    <?php
                        foreach ($parentForm["params"][$kunik]["global"]["list"] as $ix => $lbl) {
                    ?>
                        <div class="col-xs-5 col-sm-5 mr-auto ml-aut col-md-5 col-lg-3 card-vote" style="min-width: 156px">
                            <h5 class="m-0" id="<?php echo $kunik.$ix ?>"><?php echo $lbl ?></5>
                            <hr>
                            <div class="custom-radios">
                                <?php
                                    foreach($paramsData["mentions"] as $elem => $value) {
                                ?>
                                    <div>
                                        <input 
                                            type="radio" 
                                            id="<?php echo $lbl.'_'.$ix.'-'.$elem.'_'.$kunik ?>" 
                                            class="<?php echo 'n'.$elem ?>" 
                                            name="<?php echo $lbl.'_'.$ix.'_'.$kunik ?>" 
                                            value="<?php echo $elem ?>"
                                            <?php if(count($answers) > $ix) {
                                                    if(strtolower($answers[$ix]['propos']) == strtolower($lbl) && $answers[$ix]['niveau'] == $elem) echo 'checked' ;
                                                }
                                            ?>
                                        >
                                        <label for="<?php echo $lbl.'_'.$ix.'-'.$elem.'_'.$kunik ?>">
                                            <span>
                                                <img src="<?php echo Yii::app()->getModule("survey")->getAssetsUrl() ?>/images/tpls/forms/cplx/mieux-voter/check-icn.svg" alt="Checked Icon" />
                                            </span>
                                            <small class="nowrap ml-2 bold badge"><?php echo $value ?></small>
                                        </label>
                                    </div>
                                <?php
                                    }
                                ?>
                            </div>
                        </div>
                    <?php
                        }
                    ?>
                </div>
                <div class="col-xs-12 text-center">
                    <button type="button" class="btn btn-success btn-secondary saveVote<?php echo $kunik ?>" >Enregistrer vote</button>
                </div>

        <?php
            }
        ?>
<?php
     }

?>

<script type="text/javascript">
    var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answer["answers"][$kunik])) ? $answer["answers"][$kunik] : null ); ?>;
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    sectionDyf.<?php echo $kunik ?>ParamsPlace = "";
    var kunikMieuVoter = <?php echo json_encode($kunik) ?>;

    $(document).ready(function() {
        answer = {};
        sectionDyf.<?php echo $kunik ?>Params = {
			"jsonSchema" : {	
		        "title" : "<?php echo $label ?> config",
		        "icon" : "cog",
		        "properties" : {
		            list : {
		                inputType : "array",
		                label : "Candidats/Propositions",
		                values :  sectionDyf.<?php echo $kunik ?>ParamsData.list,
                        init : function() {
							$(`<input type="text" class="form-control copyConfig" placeholder="vous pouvez copier le liste ici, séparé par des virgules; ou utiliser le button ajout ci-dessous"/>`).insertBefore('.listarray .inputs.array');
							$(".copyConfig").off().on("blur", function() {
								let textVal = $(this).val().length > 0 ? $(this).val().split(",") : [];
								textVal.forEach((el, index) => {
									dyFObj.init.addfield('.listarray', el, 'list')
								});
								$(this).val('')
							})
						}
		            }
		            // mention : {
	                //     inputType : "select",
	                //     label : "Mention",
	                //     options :  sectionDyf.<?php echo $kunik ?>ParamsData.mention,
	                //     value : "<?php echo (isset($parentForm["params"][$kunik]['global']['mention']) and $parentForm["params"][$kunik]['global']['mention'] != "") ? $paramsData["mention"][strval($parentForm["params"][$kunik]['global']['mention'])] : ''; ?>"
	                // }
		        },
		        save : function () {  
		            tplCtx.value = {};
		            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
		        		 if(val.inputType == "properties")
	                                tplCtx.value = getPairsObj('.'+k+val.inputType);
	                            else if(val.inputType == "array")
	                                tplCtx.value[k] = getArray('.'+k+val.inputType);
	                            else if(val.inputType == "formLocality")
	                                tplCtx.value[k] = getArray('.'+k+val.inputType);
	                            else
	                                tplCtx.value[k] = $("#"+k).val();
		        	});
		            // mylog.log("save tplCtx",tplCtx);

		            if(typeof tplCtx.value == "undefined")
		            	toastr.error('value cannot be empty!');
		            else {
		                dataHelper.path2Value( tplCtx, function(params) { 
		                    dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
		                    // reloadInput("<?php echo $cle ?>", "<?php echo (string)$form["id"] ?>");
                            urlCtrl.loadByHash(location.hash)
		                } );
		            }

		    	}
		    }
		};

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = $(this).data("path")+'.global';
            //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
        $(".saveVote<?php echo $kunik ?>").on("click", function() {
            const propositions = sectionDyf.<?php echo $kunik ?>ParamsData.list;
            toastr.options.progressBar = true;
            toastr.options.preventDuplicates = true;
            toastr.options.closeButton = true;
            
            var lastRes = true, responses = [];
            propositions.forEach((elem, index) => {
                // mylog.log("name : ", `${elem}_${index}_${kunikMieuVoter}`)
                if(nivValue = $(`input[name='${elem}_${index}_${kunikMieuVoter}']:checked`).val()) {
                    responses.push({propos: elem, niveau: nivValue})   
                    if(lastRes) {
                        lastRes = true
                    }
                } else {
                    lastRes = false
                }
                if(index == propositions.length - 1) {
                    if(!lastRes) {
                        // mylog.log("responses", responses)
                        toastr.warning("Vous devez évaluer tous les candidats/propositions");
                    } else {
                        // mylog.log("responses", responses)
                        answer.path = "answers.<?php echo $form['id'].'.'.$key ; ?>";
                        answer.collection = "answers" ;
                        answer.id = answerObj._id.$id;
                        answer.value = responses;
                        dataHelper.path2Value(answer , function(params) { 
                            toastr.success(tradForm.modificationSave)
                        } );
                    }
                }
            })
        })
    })
</script>