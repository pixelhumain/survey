<style type="text/css">
    [slider] {
        position: relative;
        height: 12px;
        border-radius: 10px;
        text-align: left;
        margin: 45px 0 10px 0;
    }

    [slider] > div {
        position: absolute;
        left: 13px;
        right: 15px;
        height: 12px;
    }

    [slider] > div > [inverse-left] {
        position: absolute;
        left: 0;
        height: 12px;
        border-radius: 10px;
        background-color: #CCC;
        margin: 0 7px;
    }

    [slider] > div > [inverse-right] {
        position: absolute;
        right: 0;
        height: 12px;
        border-radius: 10px;
        background-color: #CCC;
        margin: 0 7px;
    }

    [slider] > div > [range] {
        position: absolute;
        left: 0;
        height: 12px;
        border-radius: 14px;
        background-color: #9FBD38;
    }

    [slider] > div > [thumb] {
        position: absolute;
        top: -7px;
        z-index: 2;
        height: 28px;
        width: 28px;
        text-align: left;
        margin-left: -11px;
        cursor: pointer;
        box-shadow: 0 3px 8px rgba(0, 0, 0, 0.4);
        background-color: #FFF;
        border-radius: 50%;
        outline: none;
    }

    [slider] > input[type=range] {
        position: absolute;
        pointer-events: none;
        -webkit-appearance: none;
        z-index: 3;
        height: 12px;
        top: -2px;
        width: 100%;
        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
        filter: alpha(opacity=0);
        -moz-opacity: 0;
        -khtml-opacity: 0;
        opacity: 0;
    }

    div[slider] > input[type=range]::-ms-track {
        -webkit-appearance: none;
        background: transparent;
        color: transparent;
    }

    div[slider] > input[type=range]::-moz-range-track {
        -moz-appearance: none;
        background: transparent;
        color: transparent;
    }

    div[slider] > input[type=range]:focus::-webkit-slider-runnable-track {
        background: transparent;
        border: transparent;
    }

    div[slider] > input[type=range]:focus {
        outline: none;
    }

    div[slider] > input[type=range]::-ms-thumb {
        pointer-events: all;
        width: 28px;
        height: 28px;
        border-radius: 0px;
        border: 0 none;
        background: red;
    }

    div[slider] > input[type=range]::-moz-range-thumb {
        pointer-events: all;
        width: 28px;
        height: 28px;
        border-radius: 0px;
        border: 0 none;
        background: red;
    }

    div[slider] > input[type=range]::-webkit-slider-thumb {
        pointer-events: all;
        width: 28px;
        height: 28px;
        border-radius: 0px;
        border: 0 none;
        background: red;
        -webkit-appearance: none;
    }

    div[slider] > input[type=range]::-ms-fill-lower {
        background: transparent;
        border: 0 none;
    }

    div[slider] > input[type=range]::-ms-fill-upper {
        background: transparent;
        border: 0 none;
    }

    div[slider] > input[type=range]::-ms-tooltip {
        display: none;
    }

    [slider] > div > [sign] {
        opacity: 0;
        position: absolute;
        margin-left: -11px;
        top: -39px;
        z-index:3;
        background-color: #9FBD38;
        color: #fff;
        width: 28px;
        height: 28px;
        border-radius: 28px;
        -webkit-border-radius: 28px;
        align-items: center;
        -webkit-justify-content: center;
        justify-content: center;
        text-align: center;
    }

    [slider] > div > [sign]:after {
        position: absolute;
        content: '';
        left: 0;
        border-radius: 16px;
        top: 19px;
        border-left: 14px solid transparent;
        border-right: 14px solid transparent;
        border-top-width: 16px;
        border-top-style: solid;
        border-top-color: #9FBD38;
    }

    [slider] > div > [sign] > span {
        font-size: 12px;
        font-weight: 700;
        line-height: 28px;
    }

    [slider] > div > [sign] {
        opacity: 1;
    }
</style>

<?php if($answer){ ?>
    <div class="form-group">  
    <?php
        # Initialize parameters data
        $paramsData = [
            "min" => 1,
            "max" => 100,
            "step" => 1
        ];

        if(isset($parentForm["params"][$kunik])){
            if(isset($parentForm["params"][$kunik]["min"]))
                $paramsData["min"] = $parentForm["params"][$kunik]["min"];

            if(isset($parentForm["params"][$kunik]["max"]))
                $paramsData["max"] = $parentForm["params"][$kunik]["max"];
      
            if(isset($parentForm["params"][$kunik]["step"]))
                $paramsData["step"] = $parentForm["params"][$kunik]["step"];
        }
        
        $hpTotal = ($paramsData["max"]-$paramsData["min"])+1;

        $valueMin = (!empty($answers) && isset($answers["min"]))?$answers["min"]:$paramsData["min"];
        $valueMax = (!empty($answers) && isset($answers["max"]))?$answers["max"]:$paramsData["max"];

        $editParamsBtn = ($canEditForm) ? "
            <a href='javascript:;' data-id='".$parentForm["_id"]."'
                data-collection='".Form::COLLECTION."' 
                data-path='params.".$kunik."' 
                class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> 
                                        </a>" : "";
    ?>

    <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
        <?php echo $label.$editQuestionBtn.$editParamsBtn?>
    </h4>

    <?php echo $info; ?>

    <div class="row">
        <div slider id="slider-distance" class="col-md-8">
            <div>
                <div inverse-left style="width:<?=100-($valueMin*100)/$hpTotal?>%;"></div>
                <div inverse-right style="width:<?=100-($valueMin*100)/$hpTotal?>%;"></div>
                <div range style="left:<?=($valueMin*100)/$hpTotal?>%;right:<?=100-($valueMax*100)/$hpTotal?>%;"></div>
                
                <span thumb style="left:<?=($valueMin*100)/$hpTotal?>%;"></span>
                <span thumb style="left:<?=($valueMax*100)/$hpTotal?>%;"></span>
        
                <div sign style="left:<?=($valueMin*100)/$hpTotal?>%;">
                    <span id="value"><?=$valueMin?></span>
                </div>
        
                <div sign style="left:<?=($valueMax*100)/$hpTotal?>%;">
                    <span id="value"><?=$valueMax?></span>
                </div>
            </div>
            <input id="rangeMin<?= $kunik ?>" type="range" tabindex="0" value="<?=$valueMin?>" max="<?=$paramsData['max']?>" min="<?=$paramsData['min']?>" step="<?=$paramsData['step']?>" />
            <input id="rangeMax<?= $kunik ?>" type="range" tabindex="0" value="<?=$valueMax?>" max="<?=$paramsData['max']?>" min="<?=$paramsData['min']?>" step="<?=$paramsData['step']?>" />
        </div>
    </div>
</div>


<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  
    $(document).ready(function(){
        let kunik = "<?= $kunik ?>";

        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {
                "title" : "Paramétrage",
                "description" : "",
                "icon" : "cog",
                "properties" : {
                    min : {
                        inputType : "numeric",
                        rules:{
                            required:true,
                            number:true
                        },
                        label : "Valeur Minimum (valeur numérique)"
                    },
                    max : {
                        inputType : "numeric",
                        rules:{
                            required:true,
                            number:true
                        },
                        label : "Valeur Maximum (Valeur numérique)"
                    },
                    step : {
                        inputType : "numeric",
                        rules:{
                            required:true,
                            number:true
                        },
                        label : "Etape de changement de valeur (numérique)"
                    }
                },
                save : function (data) {
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                        if(val.inputType == "array")
                            tplCtx.value[k] = getArray('.'+k+val.inputType);
                        else
                            tplCtx.value[k] = $("#"+k).val();
                    });
                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == undefined)
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                            location.reload();
                        });
                    }
                }
            }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = $(this).data("path");
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });

        $(document).on("blur", "#rangeMin<?= $kunik ?>, #rangeMax<?= $kunik ?>", function(){
            if( notNull(answerObj)){

                var answer = {
                    collection : "answers",
                    id : answerObj._id.$id,
                    path : "answers.<?= $key ?>"
                };

                if(answerObj.form){
                    answer.path = "answers.<?= $form["id"]?>.<?= $key ?>"; 
                }
              
                answer.value = {min:$("#rangeMin<?= $kunik ?>").val(), max:$("#rangeMax<?= $kunik ?>").val()};
                
                dataHelper.path2Value( answer , function(params) { 
                    toastr.success('Modification enregistrée');
                    saveLinks(answerObj._id.$id,"updated",userId);
                });
              
            } else {
                toastr.error('answer cannot be empty, veuillez vérifier!');
            }
        });

        $(document).on("input", "#rangeMin<?= $kunik ?>", function(){
            this.value=Math.min(this.value,this.parentNode.childNodes[5].value-1);
            var value=(100/(parseInt(this.max)-parseInt(this.min)))*parseInt(this.value)-(100/(parseInt(this.max)-parseInt(this.min)))*parseInt(this.min);
            var children = this.parentNode.childNodes[1].childNodes;
            children[1].style.width=value+'%';
            children[5].style.left=value+'%';
            children[7].style.left=value+'%';children[11].style.left=value+'%';
            children[11].childNodes[1].innerHTML=this.value;
        });

        $(document).on("input", "#rangeMax<?= $kunik ?>", function(){
            this.value = Math.max(this.value,this.parentNode.childNodes[3].value-(-1));
            var value = (100/(parseInt(this.max)-parseInt(this.min)))*parseInt(this.value)-(100/(parseInt(this.max)-parseInt(this.min)))*parseInt(this.min);
            var children = this.parentNode.childNodes[1].childNodes;
            children[3].style.width=(100-value)+'%';
            children[5].style.right=(100-value)+'%';
            children[9].style.left=value+'%';children[13].style.left=value+'%';
            children[13].childNodes[1].innerHTML=this.value;
        });
    });
</script>
<?php } ?>