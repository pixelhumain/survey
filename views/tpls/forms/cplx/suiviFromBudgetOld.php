<div class="form-group">
	<table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>">

	<thead>
		<tr>
			<td colspan='<?php echo count( $properties)+2?>' ><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
				<?php echo $info;
				if( !isset($budgetKey) ) 
					echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> THIS FIELD HAS TO BE CONFIGURED FIRST ".$editParamsBtn."</span>"; ?>
			</td>
		</tr>	
		<?php if($answers){ ?>
		<tr>
			</th>
			<?php 
			
			foreach ($properties as $i => $inp) {
				echo "<th>".$inp["placeholder"]."</th>";
			} ?>
			<th></th>
		</tr>
		<?php } ?>
	</thead>
	<tbody class="directoryLines">	
		<?php 
		$ct = 0;
		$totalProgress = 0;
		$bigTotal = 0;
		$totalPayed = 0;
		$validWork = 0;
		$payedWork = 0;
		$totalTodos = 0;
		$totalTodosDone = 0;
		if($answers){
			foreach ($answers as $q => $a) {

				$trStyle = "";
				$tds = "";
				$todoDone  = 0;
				$todo = 0;
				foreach ($properties as $i => $inp) 
				{
					$tds .= "<td>";
					
					if( $i == "worker"  )
					{
						$valbl = "?";
						$class= "btn btn-default";
						if( isset( $a["worker"] ) )
						{
							$o = PHDB::findOne(Organization::COLLECTION,["_id"=>new MongoId($a["worker"]["id"])],["name","slug"]);	
							$valbl = $o["name"];
							$class="";
						}	
						$tds .= "<a href='javascript:;' data-id='".$answer["_id"]."' data-budgetpath='".$copy."' data-form='".$copyF."' data-pos='".$q."'  class='".$class." btnWorker margin-left-5 padding-10'>".$valbl."</a>";
					}

					if( $i == "todo"  )
					{
						$valbl = "<i class='fa fa-plus'></i>";
						$class= "btn btn-default";
						if( isset( $a["todo"] ) ){
							$valbl = count( $a["todo"] );
							$totalTodos += count($a["todo"]);
							foreach ($a["todo"] as $tix => $do) {
								if(isset($do["done"]) && $do["done"]=="1")
									$todoDone++;
								else 
									$todo++;
							}
							$totalTodosDone += $todoDone;
							$valbl = $todoDone."/".count( $a["todo"] );
						}
						$tds .= "<a href='javascript:;' id='btnTodo".$q."' data-id='".$answer["_id"]."' data-budgetpath='".$copy."' data-form='".$copyF."' data-pos='".$q."'  class='".$class." btnTodo margin-left-5 padding-10'>".$valbl."</a>";
					}
					else if( $i == "line" && isset( $a["financer"]["line"] ) ) 
						$tds .= $a["financer"]["line"];
					else if( $i == "workType" && isset( $a["worker"]["workType"] ) ) 
						$tds .= $a["worker"]["workType"];
					else if( $i == "progress"){
						if(isset($a["todo"])){
							$progress = floor($todoDone*100/count($a["todo"]));
						}
						else {
							$progress = (!empty($a["progress"]) ) ? (int)$a["progress"] : 0;
						}
						$totalProgress += $progress;
						$percol = "warning";
						if( $progress == 100 ){
							$percol = "success";
						}
						$tds .= "<a href='javascript:;' id='todoPerc".$q."' data-id='".$answer["_id"]."' data-budgetpath='".$copy."' data-form='".$copyF."' data-pos='".$q."'  class=' btnProgress margin-left-5 padding-10'>".$progress."%</a>";
						$tds .= '<div class="progress btnProgress"  data-id="'.$answer["_id"].'" data-budgetpath="'.$copy.'" data-form="'.$copyF.'" data-pos="'.$q.'"  style="cursor:pointer" >'.
						  '<div id="todoProgress'.$q.'" class="progress-bar progress-bar-'.$percol.'" style="width:'.$progress.'%">'.
							    '<span class="sr-only">'.$progress.'% Complete</span>'.
						  '</div>'.
						'</div>';
					}
					else if( $i == "total"){ 
						$total = 0;
						$totalPayedHere = 0;
						$amounts = (isset($parentForm["params"][$budgetKey]["amounts"])) ? $parentForm["params"][$budgetKey]["amounts"] : ["price" => "Price"] ; 
				        foreach ( $amounts as $k => $l) {
					    	if(!empty($a[$k]))
								$total += (int)$a[$k];	
					    }
						
						$bigTotal += $total;
						$color = "default";
						if(isset($a["payed"])){
							if($a["payed"]["status"] == "total"){
								$color = "success";
								$totalPayedHere = $total;
								$payedWork++;
							}
							else if($a["payed"]["status"] == "partly"){
								$color = "warning";
								$totalPayedHere = $a["payed"]["amount"];
							}
							else if($a["payed"]["status"] == "accompte"){
								$color = "warning";
								$totalPayedHere = $a["payed"]["amount"];
							}
							$totalPayed += $totalPayedHere;
						}
						$tds .= "<a href='javascript:;' data-id='".$answer["_id"]."' data-budgetpath='".$copy."' data-form='".$copyF."' data-pos='".$q."'  class='btnPay margin-left-5 padding-10'>".$total."€</a>";

						$percol = "default";
						$payedPercent = 0;
						if($totalPayedHere != 0){
							$payedPercent = $totalPayedHere * 100 / $total;
							$percol = "warning";
						}
						
						if( $payedPercent == 100 ){
							$percol = "success";
						}
						$tds .= '<div class="progress btnPay" data-id="'.$answer["_id"].'" data-budgetpath="'.$copy.'" data-form="'.$copyF.'" data-pos="'.$q.'"  style="cursor:pointer" >'.
						  '<div class="progress-bar progress-bar-'.$percol.'" style="width:'.$payedPercent.'%">'.
							    '<span class="sr-only">'.$payedPercent.'% Complete</span>'.
						  '</div>'.
						'</div>';
					}
					else if( $i == "validation"){ 
						$color = "default";
						$valbl = "?";
						$tool= "En attente de validation";
						if( isset($a["validFinal"]) ){
							if( $a["validFinal"]["valid"] == "validated" ){
								$color = "success";
								$valbl = "V";
								$trStyle = "background-color:#e5ffe5";
								$validWork++;
								$tool="Validé sans réserve";
							} else if( $a["validFinal"]["valid"] == "reserved" ){
								$color = "warning";
								$valbl = "R";
								$tool="Validé avec réserves";
							} else if( $a["validFinal"]["valid"] == "refused" ){
								$color = "danger";
								$valbl = "NV";
								$tool="Non validé";
							}
						}

						$tds .= "<a href='javascript:;' data-id='".$answer["_id"]."' data-budgetpath='".$copy."' data-form='".$copyF."' data-pos='".$q."'  class='btnValidateWork btn btn-xs btn-".$color." margin-left-5 padding-10 tooltips' data-toggle='tooltip' data-placement='left' data-original-title='".$tool."'>".$valbl."</a>";
					}else if( isset( $a[$i] ) && is_array($a[$i]) ) 
						$tds .= implode(" , ", $a[$i]);
					else if( isset( $a[$i] ) ) 
						$tds .= $a[$i];
					
					$tds .= "</td>";
				}
					
			?>
			
			<?php 
				$ct++;
				echo "<tr id='".$kunik.$q."' class='".$kunik."Line text-center' style='".$trStyle."'>";
				echo $tds;?>
				<td>
					<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
				</td>
			</tr>
			<?php 
			}
		}
?>
		</tbody>
	</table>

<?php 
$percol = "warning";
$totalProgress = (!empty($answers)) ? $totalProgress / count($answers) : 0;
if( $totalProgress == 100 ){
	$percol = "success";
}
echo "<h4 style='color:".(($titleColor) ? $titleColor : "black")."'>Pourcentage d'avancement Globale</h4>".
'<div class="progress " style="cursor:pointer" >'.
  '<div class="progress-bar progress-bar-'.$percol.'" style="width:'.$totalProgress.'%">'.
	    '<span class="sr-only">'.$totalProgress.'% Complete</span>'.
  '</div>'.
'</div>'; ?>

<table class="table table-bordered table-hover  ">
	<tbody class="">
		<tr>
			<td>Pourcentage d'avancement</td>
			<td><?php echo floor($totalProgress) ?>%</td>
		</tr>
		<tr>
			<td>Resta à faire </td>
			<td><?php echo floor(100-$totalProgress) ?>%</td>
		</tr>
		<tr>
			<td>Nombres de Travaux Validés </td>
			<td><?php echo $validWork."/".count($answers) ?></td>
		</tr>
		<tr>
			<td>Nombres de Taches Cloturés </td>
			<td><?php echo $totalTodosDone."/".$totalTodos ?></td>
		</tr>
		
	</tbody>
</table>

<?php 
$percol = "warning";
$bigTotalPercent = (!empty($bigTotal)) ? $totalPayed * 100 / $bigTotal : 0;
if( $bigTotalPercent == 100 ){
	$percol = "success";
}
echo "<h4 style='color:".(($titleColor) ? $titleColor : "black")."'>Suivi des dépenses Globale</h4>".
'<div class="progress " style="cursor:pointer" >'.
  '<div class="progress-bar progress-bar-'.$percol.'" style="width:'.$bigTotalPercent.'%">'.
	    '<span class="sr-only">'.$bigTotalPercent.'% Complete</span>'.
  '</div>'.
'</div>'; ?>

<table class="table table-bordered table-hover  ">
	<tbody class="">
		<tr>
			<td>BUDGET TOTAL</td>
			<td><?php echo trim(strrev(chunk_split(strrev($bigTotal),3, ' '))) ?>€</td>
		</tr>
		<tr>
			<td>TRAVAUX PAYÉS</td>
			<td><?php echo trim(strrev(chunk_split(strrev($totalPayed),3, ' '))) ?> €</td>
		</tr>
		<tr>
			<td>DELTA</td>
			<td><?php echo trim(strrev(chunk_split(strrev($bigTotal-$totalPayed),3, ' '))) ?> €</td>
		</tr>
		<tr>
			<td>Nombres de Travaux Cloturés </td>
			<td><?php echo $payedWork."/".count($answers) ?></td>
		</tr>
	</tbody>
</table>

</div>