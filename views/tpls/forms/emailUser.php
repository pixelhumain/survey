<?php
$value = (!empty($answers)) ? $answers : "";

if($mode == "r" || $mode == "pdf"){ ?>
    <div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
        <label for="<?php echo $kunik ?>"><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label ?></h4></label><br/>
        <?php echo $answers; ?>
    </div>
<?php
}else{
?>
	<div class="form-group">
	    <label for="<?php echo $key ?>"><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn ?></h4></label>
	    <br/><input type="<?php echo (!empty($type)) ? $type : 'text' ?>" class="form-control saveOneByOne" id="<?php echo $key ?>" aria-describedby="<?php echo $key ?>Help" placeholder="<?php echo (isset($placeholder)) ? $placeholder : '' ?>" data-form='<?php echo $form["id"] ?>'  value="<?php echo $value ?>" >
	    <?php if(!empty($info)){ ?>
	    	<small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
	    <?php } ?>
	</div>
<?php } ?>

<?php if($mode != "pdf"){ ?>
<script type="text/javascript">
	var users = [];
	var valueEmailAnswer=<?php echo json_encode($value) ?>;
	var formId = <?php echo json_encode($form["id"]) ?>;
	var inputKey = <?php echo json_encode($key) ?>;
	jQuery(document).ready(function() {
	    mylog.log("render form input","/modules/costum/views/tpls/forms/emailUser.php");

	    if($("#<?php echo $key ?>").val() == "" || valueEmailAnswer=="" ){
		    if (typeof userConnected.email !== 'undefined') {
			    $("#<?php echo $key ?>").val(userConnected.email).blur();
			    answer = new Object;
			    answer.path = "answers."+formId+"."+"<?php echo $key ?>";
		    	answer.collection = "answers" ;
		    	answer.id = "<?php echo $answer["_id"]; ?>";
		    	answer.value = userConnected.email;
		    	dataHelper.path2Value(answer , function(params) {
                    if (typeof stepValidationReload<?php echo @$formId?> !== "undefined") {
                        stepValidationReload<?php echo @$formId?>();
                    }
		        });
		    }
		}

	});




</script>
<?php }
// }
 ?>
