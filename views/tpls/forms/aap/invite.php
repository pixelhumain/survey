<style>
    .invite<?=$kunik?> .select2-container-multi:not(.aapstatus-container) .select2-choices .select2-search-choice{
        color: #000 !important;
        border: 1px #fff solid !important;
    }
    .invite<?=$kunik?> .select2-container {
        display: block !important;
    }
</style>

<?php 
    $showAllUsers = $parentForm["showAllUsersInvite"] ?? false;
    if($mode == "r" || $mode == "pdf"){ ?>
    <div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
        <label for="<?php echo $kunik ?>">
        	<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
        		<?php echo $label ?>
        	</h4>
        </label><br/>
        <?php if(!empty($answer["links"]["contributors"])){
            $contrbutorsId = array_keys($answer["links"]["contributors"]);
            $contrbutorsId = array_map(function($val){
                return new MongoId($val);
            },$contrbutorsId);
            $contributors = PHDB::find(Person::COLLECTION,array("_id" => ['$in'=>$contrbutorsId]),array("name"));
            foreach($contributors as $ck => $vk){
                echo "<span class='badge'>".$vk["name"]."</span> ";
            }
        } ?>
    </div>
<?php }else{ ?>
    <div class="col-md-12 no-padding invite<?=$kunik?>">
        <div class="form-group">
            <label for="<?php echo $key ?>" style="display:block">
                <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>" class="<?= $canEditForm && $mode="fa" ? "" : $type ?>">
                    <?php echo $label.$editQuestionBtn ?>
                    <?php if($canEditForm && $mode="fa"){ ?>
                        <a href="javascript:;" class="btn btn-xs btn-danger config<?= $kunik ?>"><i class="fa fa-cog"></i></a>
                    <?php } ?>
                </h4>
            </label>
            <input type="text" class="margin-right-10 exclude-input" value="" id="search-person" placeholder="">
        </div>
    </div>

<script>
    $(function(){      
        var _inviteObj = {
            answerId : <?= json_encode((string)$answer["_id"]) ?>,
            contributors : <?= json_encode(@$answer["links"]["contributors"]) ?>,
            answerCollection : <?= json_encode((string)$answer["collection"]) ?>,
            select2: function(){
                
            }

        }
        const showAllUsers = <?= json_encode( filter_var($showAllUsers, FILTER_VALIDATE_BOOLEAN)) ?>;
        const _thisAnswer = <?= json_encode($answer) ?>;
        var currentValue = "";
        if (_thisAnswer && _thisAnswer.project && _thisAnswer.project.id) {
            ajaxPost(null, baseUrl + "/costum/project/action/request/all_contributors", 
                {id : _thisAnswer.project.id},
                function(contributors){
                    contributors.map(function(contributor) {
                        if (_inviteObj && !_inviteObj.contributors[contributor.id] && contributor.name) {
                            _inviteObj.contributors[contributor.id] = {
                                name : contributor.name,
                                type : "citoyens"
                            }
                        }
                    })
                },null,null,{async:false}
            )
        }
        if(notNull(_inviteObj.contributors) && notEmpty(_inviteObj.contributors))
            currentValue = Object.keys(_inviteObj.contributors).join(',');
        if(notNull(_inviteObj.contributors) && notEmpty(_inviteObj.contributors))
            ajaxPost('',baseUrl+"/co2/element/get/type/citoyens", 
                {id : Object.keys(_inviteObj.contributors),fields:["name","profilMediumImageUrl"]},
                function(data){
                    $.each(data.map,function(k,v){
                        if(exists(_inviteObj.contributors[k]) && notNull(_inviteObj.contributors[k]) && exists(v.name))
                            _inviteObj.contributors[k].name = v.name
                    })
                },null,null,{async:false}
            )

        $("#search-person").val(currentValue).select2({
            minimumInputLength: 3,
            //tags: true,
            multiple:true,
            "tokenSeparators": [','],
            createSearchChoice: function (term, data) {
                // if (!data.length)
                //     return {
                //         id: term,
                //         text: term
                //     };
            },
            initSelection : function (element, callback) {
                var data = [];
                $(element.val().split(",")).each(function () {
                    //data.push({id: this, text: this});_inviteObj
                    data.push({id: this, text: exists(_inviteObj.contributors[this].name) ? _inviteObj.contributors[this].name :""});
                });
                callback(data);
            },
            ajax: {
                url: baseUrl+"/"+moduleId+"/search/globalautocomplete",
                dataType: 'json',
                type: "POST",
                quietMillis: 50,
                data: function (term) {
                    return {
                        name: term,
                        searchType : ["citoyens"],
                        filters : {
                            '$or' : !showAllUsers ? {
                                "links.projects.<?= array_keys($parentForm["parent"])[0] ?>":{'$exists':true},
                                "links.memberOf.<?= array_keys($parentForm["parent"])[0] ?>":{'$exists':true}
                            } : {
                                "name" : {'$exists': true},
                            }
                        }
                    };
                },
                results: function (data) {
                    return {
                        results: $.map(Object.values(data.results), function (item) {
                            return {
                                text: item.name,
                                id: item._id.$id
                            }
                        })
                    };
                }
            }
        }).on('change',function(e){
            const thisAnswer = <?= json_encode($answer) ?>;
            if(exists(e.added)){
                var params = {
                    parentId : _inviteObj.answerId,
                    parentType : "answers",
                    listInvite : {
                        citoyens :{},
                        organizations:{}
                    }
                }
                params.listInvite.citoyens[e.added.id] = e.added.text
                ajaxPost("",baseUrl+'/'+moduleId+"/link/multiconnect",params,function(data){
                    toastr.success(trad.saved);
                    if(typeof newReloadStepValidationInputGlobal != "undefined") {
						newReloadStepValidationInputGlobal({
							inputKey : <?php echo json_encode($key); ?>,
							inputType : "tpls.forms.aap.invite" 
						})
					}
                    if(thisAnswer?.project?.id) {
                        ajaxPost("",baseUrl+'/'+moduleId+"/link/multiconnect",{
                            parentId : thisAnswer?.project?.id,
                            parentType : "projects",
                            listInvite : {...params.listInvite}
                        },function(data){})
                    }
                    ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/addperson/answerid/' + (answerId ? answerId : _inviteObj.answerId),
                        {
                            pers : params?.listInvite?.citoyens,
                            url : window.location.href
                        },
                        function (data) {

                        }, "html");
                })
            }

            if(exists(e.removed)){
                var params = {
                    listInvite : {
                        citoyens :{},
                        organizations:{}
                    }
                }
                params.listInvite.citoyens[e.removed.id] = e.removed.text
                links.disconnect('answers',_inviteObj.answerId,e.removed.id,'citoyens','contributors', (data) => {
                    if(thisAnswer?.project?.id) {
                        links.disconnectAjax('projects',thisAnswer?.project?.id,e.removed.id,'citoyens','contributors', null, () => {

                        }, {
                            showNotification : false
                        })
                    }
                    if(typeof newReloadStepValidationInputGlobal != "undefined") {
						newReloadStepValidationInputGlobal({
							inputKey : <?php echo json_encode($key); ?>,
							inputType : "tpls.forms.aap.invite" 
						})
					}
                })
                ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/rmperson/answerid/' + (answerId ? answerId : _inviteObj.answerId),
                    {
                        pers : params?.listInvite?.citoyens,
                        url : window.location.href
                    },
                    function (data) {

                    }, "html");
            }

        });
    })
</script>
<?php } ?>