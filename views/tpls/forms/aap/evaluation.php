<?php 
    HtmlHelper::registerCssAndScriptsFiles(['/plugins/rater/rater-js.js'], Yii::app()->request->baseUrl);
?>

<style>
    .aap-evaluation td,.aap-evaluation tr{
        text-align: left;
        font-size: 14px;
    }
    .aap-evaluation tbody tr td input{
        border:0
    }
    .aap-evaluation .table,.aap-evaluation .alert-info{
        margin-top: 30px;
    }
    .aap-evaluation .dropdown{
        display: inline-block !important;
    }
    .aap-evaluation .dropdown .fa{
        cursor:pointer
    }
    .aap-evaluation .dropdown-menu {
        text-align: center;
    }
    .aap-evaluation .dropdown-menu li:not(:first-child){
        display: inline;
    }
</style>
<!-- <pre> -->
<?php 
    $formConfig = PHDB::findOneById(Form::COLLECTION,$parentForm["config"]);
    $step = "aapStep2";
    if(isset($formConfig["subForms"]["aapStep2"]["params"]["config"]["criterions"]))
        $step = "aapStep2";
    elseif(isset($formConfig["subForms"]["aapStep3"]["params"]["config"]["criterions"]))
        $step = "aapStep3";

    if(isset($parentForm["evaluationCriteria"]["activateLocalCriteria"]) && ($parentForm["evaluationCriteria"]["activateLocalCriteria"] == true || $parentForm["evaluationCriteria"]["activateLocalCriteria"] == "true")){
        if(isset($parentForm["evaluationCriteria"]["type"]))
            @$formConfig["subForms"][$step]["params"]["config"]["type"] = $parentForm["evaluationCriteria"]["type"];
        if(isset($parentForm["evaluationCriteria"]["criterions"]))
            @$formConfig["subForms"][$step]["params"]["config"]["criterions"] = $parentForm["evaluationCriteria"]["criterions"];
    }

    $me = isset(Yii::app()->session['userId']) ? Person::getById(Yii::app()->session['userId']) : null;
    $evaluation = !empty(@$answers[(string) $me["_id"]]) ? @$answers[(string) $me["_id"]] : @$formConfig["subForms"][$step]["params"]["config"]["criterions"];
    $parentElement = array_keys($parentForm["parent"])[0];
    $allowedRoles = (isset($formConfig["subForms"][$step]["params"]["canEdit"]) && is_array($formConfig["subForms"][$step]["params"]["canEdit"])) ? @$formConfig["subForms"][$step]["params"]["canEdit"] : array();
    if(isset($parentForm["evaluationCriteria"]["activateLocalCriteria"]) && isset($parentForm["evaluationCriteria"]["whoCanEvaluate"]) && is_array($parentForm["evaluationCriteria"]["whoCanEvaluate"]) && ($parentForm["evaluationCriteria"]["activateLocalCriteria"] == true))
        $allowedRoles = $parentForm["evaluationCriteria"]["whoCanEvaluate"];
    $allowedRoles[] = "Evaluateur";
    if($parentForm["parent"][$parentElement]["type"]=="organizations"){
        $roles = isset($me["links"]["memberOf"][$parentElement]["roles"]) ? $me["links"]["memberOf"][$parentElement]["roles"] : [];
        $isAdmin = isset($me["links"]["memberOf"][$parentElement]["isAdmin"]) ? $me["links"]["memberOf"][$parentElement]["isAdmin"] :false;
    }elseif($parentForm["parent"][$parentElement]["type"]=="projects"){
        $roles = isset($me["links"]["projects"][$parentElement]["roles"]) ? $me["links"]["projects"][$parentElement]["roles"] : [];
        $isAdmin = isset($me["links"]["projects"][$parentElement]["isAdmin"]) ? $me["links"]["projects"][$parentElement]["isAdmin"] :false;
    }
    $canEvaluate =  false;
    $intersectRoles = array_intersect($allowedRoles,$roles);
    if(count($intersectRoles) != 0 || $isAdmin)
        $canEvaluate =true;

    $type =  $parentForm["parent"][$parentElement]["type"];
    $communityWithAdmin = Element::getCommunityByTypeAndId($type, $parentElement, "all", "isAdmin",null, null, ["name", "profilMediumImageUrl"]);
    $community = Element::getCommunityByTypeAndId($type, $parentElement, "all", null,$allowedRoles, null, ["name", "profilMediumImageUrl"]);
    $community = array_unique(array_merge($communityWithAdmin,$community),SORT_REGULAR);
    $allAnswers = PHDB::find(Form::ANSWER_COLLECTION,array("form"=>(string)$parentForm["_id"],"answers" => ['$exists'=>true]));

    $toBeValidated = false;
    if($parentForm["parent"][$parentElement]["type"] == Organization::COLLECTION){
        $links = isset(Yii::app()->session['userId']) ? Person::getPersonLinksByPersonId(Yii::app()->session['userId']) :null;
        if(isset($currentRoles[Organization::COLLECTION][$parentElement]["toBeValidated"]) /*&& array_intersect($intersectRoles,$roles) != 0*/)
            $toBeValidated = true;
    }
    if($parentForm["parent"][$parentElement]["type"] == Project::COLLECTION){
        $links = isset(Yii::app()->session['userId']) ? Person::getPersonLinksByPersonId(Yii::app()->session['userId']) :null;
        if(isset($currentRoles[Project::COLLECTION][$parentElement]["toBeValidated"]) /*&& array_intersect($intersectRoles,$roles) != 0*/)
            $toBeValidated = true;
    }

    // foreach ($community as $kcommunity => $vcommunity) {
    //     $community[$kcommunity]["evaluationCounter"] = PHDB::count(Form::ANSWER_COLLECTION,array(
    //         "answers.".$step.".evaluation.".$kcommunity => ['$exists' => true],
    //         "form" => (string)$parentForm["_id"]
    //         )
    //     );
    //     $haveMadeAvote = false;
    //     foreach ($allAnswers as $kallans => $vallans) {
    //         if(isset($vallans["answers"][$step]["evaluation"][$kcommunity]))
    //             $haveMadeAvote = true;
    //     }
    //     if($haveMadeAvote == false)
    //         unset($community[$kcommunity]);
    // }
    // echo $this->renderPartial("survey.views.tpls.forms.aap.evaluator", array(
    //         "community" => $community,
    //         "stepEvaluation" => $step,
    //         "form" => $parentForm,
    //         "answer" => @$answer["answers"]
    // ));
?>
<!-- </pre> -->
<!-- <h4 style="color:<?php //echo ($titleColor) ? $titleColor : "black"; ?>">
    <?php //echo $label.$editQuestionBtn ?>
</h4> -->

<?php if($mode == "fa"){ ?>
    <button type="button" class="btn bg-green hover-white addEvaliationCriteria"><i class="fa fa-cog"></i></button>
    <!-- <div class="btn btn-primary btn-sm addEvaliationCriteria pull-left margin-bottom-15 margin-right-5" >
        Config champs évaluation
    </div> -->
<?php } ?>
    <button type="button" class="btn bg-green hover-white votant<?= $kunik ?>" ><i class="fa fa-users"></i></button>

<?php if(isset($formConfig["subForms"][$step]["params"]["config"]["type"])){ ?>
    <div class="aap-evaluation">
            <?php if(!empty($answer["answers"]["aapStep1"]["titre"])){
                $proposition = $answer["answers"]["aapStep1"]["titre"];
                ?>

                <?php if($canEvaluate == false && $toBeValidated==false){ ?>
                    <div class="alert alert-info alert-to-be-evaluator">
                        Vous n'avez pas de rôle d'évaluateur, cliquer <a href="javascript:;" id="toBeEvaluator"><b class="tooltips" data-toggle="tooltip" data-original-title="Envoyer ma démande" data-placement="bottom">Ici</b></a> pour démander un role d'évaluateur. <br>
                        Seul <b>les admins , <?= implode(', ',array_unique($allowedRoles)) ?></b> peuvent faire l'évaluation
                    </div>
                <?php } ?>
                <div class="alert alert-success margin-top-35 alert-to-be-validated <?= $toBeValidated==true ? "" : "hidden" ?>">
                    <?= Yii::t("common","waiting for validation") ?>
                </div>

                <table class="table table-striped table-bordered table">
                    <thead>
                    <tr>
                        <th>Nom des critères</th>
                        <?php if(@$formConfig["subForms"][$step]["params"]["config"]["type"]=="noteCriterionBased") echo "<th>Note sur 10 : " .$proposition. "</th>"?>
                        <?php if(@$formConfig["subForms"][$step]["params"]["config"]["type"]=="starCriterionBased")  echo "<th>Note sur 5 : " .$proposition. "</th>"?>
                        
                    </tr>
                    </thead>
                    <tbody>
                        <?php if(isset($formConfig["subForms"][$step]["params"]["config"]["criterions"]) && is_array(@$formConfig["subForms"][$step]["params"]["config"]["criterions"]) && count(@$formConfig["subForms"][$step]["params"]["config"]["criterions"])!=0){ ?>
                            <?php 
                            foreach (@$formConfig["subForms"][$step]["params"]["config"]["criterions"] as $kconfig => $vconfig) { 
                                    $class= preg_replace('/\s+/', '', $vconfig["label"].'-'.$kconfig);
                                ?>
                                <tr>
                                    <td><?= $vconfig["label"] ?></td>
                                    <td>
                                        <?php if(@$formConfig["subForms"][$step]["params"]["config"]["type"]=="noteCriterionBased"){ ?>
                                            <?php if( $canEvaluate){ ?>
                                                <input 
                                                    class="evaluateField"
                                                    type="text"
                                                    data-id= "<?= $answer["_id"] ?>"
                                                    data-path = "<?= $answerPath.(string) $me["_id"].".".$kconfig ?>"
                                                    data-label="<?= @$vconfig["label"] ?>"
                                                    data-min ="<?= @$vconfig["min"] ?>"
                                                    data-max ="<?= @$vconfig["max"] ?>"
                                                    data-coeff ="<?= @$vconfig["coeff"] ?>"
                                                    value="<?= @$vconfig["label"] == @$evaluation[$kconfig]["label"] ? @$evaluation[$kconfig]["note"] : @$vconfig["note"]  ?>"
                                                >
                                            <?php }else{
                                                echo @$evaluation[$kconfig]["note"];
                                            } ?>
                                        <?php } ?>

                                        <?php if(@$formConfig["subForms"][$step]["params"]["config"]["type"]=="starCriterionBased"){  
                                                $starIcon = "";
                                                $starValueFloat = @$vconfig["label"] == @$evaluation[$kconfig]["label"] ? (float) @$evaluation[$kconfig]["note"] : (float) @$vconfig["note"];
                                            ?>
                                            <div class="rater-evaluator" <?= ($canEvaluate) ? '' : 'data-status="readonly"' ?> 
                                                data-rating='<?= $starValueFloat?>'
                                                data-id="<?= $answer["_id"] ?>"
                                                data-path = "<?= $answerPath.(string) $me["_id"].".".$kconfig ?>"
                                                data-label="<?= @$vconfig["label"] ?>"
                                                data-coeff ="<?= @$vconfig["coeff"] ?>">
                                            </div>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php }else{
                            echo "<p class='text-center'>Vous n'avez pas de critères d'évaluation pour ce champ</p>";
                        } ?>
                    </tbody>
                </table>
            <?php }else{
                    echo "<p class='text-center'>Vous n'avez pas encore fait de proposition à l'étape 1</p>";
                } ?>
    </div>

    <script>
        $(function(){
            var $parentId = <?= json_encode($parentElement) ?>;
            var $parentType = <?= json_encode($parentForm["parent"][$parentElement]["type"]) ?>;

            $('[data-toggle="popover"]').popover();
            $('.evaluateField').off().on('blur',function(){
                var id = $(this).data('id');
                var path = $(this).data('path');
                var label = $(this).data('label');
                var coeff = $(this).data('coeff');
                var note = $(this).val().trim();
                if(note > 10){
                    $(this).val(0);
                    return  toastr.error("Donne une note entre 0 à 10. Merci");
                }
                    
                
                var tplCtx = {
                    id : id ,
                    collection : "answers",
                    path : path,
                    value : {
                        label : label,
                        note : note,
                        coeff:coeff
                    },
                    setType :[
                        {
                            "path": "note",
                            "type": "float"
                        },
                        {
                            "path": "coeff",
                            "type": "int"
                        }
                    ]
                };
                dataHelper.path2Value( tplCtx, function(params){
                    toastr.success("Enregistré");
                })
            });
            $('.rater-evaluator').each(function(i, obj) {
                var opts = {
                    rating : $(obj).data("rating"),
                    starSize:20,
                    step:0.5,
                    element:obj,
                    rateCallback : function rateCallback(rating, done) {
                        var $this = this;
                        this.setRating(rating); 
                            var tplCtx ={
                                id : $(obj).data("id"),
                                collection : "answers",
                                path : $(obj).data("path"),
                                value : {
                                    label : $(obj).data("label"),
                                    note : rating,
                                    coeff : $(obj).data("coeff")
                                },
                                setType :[
                                    {
                                        "path": "note",
                                        "type": "float"
                                    },
                                    {
                                        "path": "coeff",
                                        "type": "int"
                                    }
                                ]
                            }
                            dataHelper.path2Value(tplCtx, function (params) {
                                if(params.result)
                                    toastr.success("Ok");
                            });
                        done(); 
                    }
                };
                if($(obj).data("status")=="readonly"){
                    opts["readOnly"] = true;
                    delete opts.rateCallback;
                }
                var starRating = raterJs(opts);
            });

            $('#toBeEvaluator').on('click',function(){
                var params = {
					childId: userId,
                    childType: "citoyens",
                    parentType: $parentType,
                    parentId: $parentId,
                    connectType: "member",
                    roles : ["Evaluateur"]
				};
				
				 ajaxPost("",baseUrl+'/'+moduleId+"/link/connect",params,function(data){
                    $('.alert-to-be-validated').removeClass("hidden");
                    $('.alert-to-be-evaluator').hide();
                    toastr.success("<?= Yii::t("common", "Your request has been sent to other admins.") ?>")
                 })
            })

            $(".votant<?= $kunik ?>").off().on('click',function(){
            ajaxPost('',baseUrl+'/co2/aap/getelementbyid/type/answers/id/<?= (string) $answer["_id"]  ?>', 
            null,
            function(data){
                if(exists(data.answers) && exists(data.answers.aapStep2.evaluation)){
                    var votants = [];
                    $.each(data.answers.aapStep2.evaluation,function(k,v){
                        votants.push(k);
                    })
                    ajaxPost('',baseUrl+'/co2/aap/getelementbyid/type/citoyens', 
                    {id : votants,fields:["name","profilMediumImageUrl"]},
                    function(dataVotans){
                        
                    })
                        mylog.log(dataVotans,"votants");
                        var html = "<ul style='list-style-type:none'>";
                        $.each(dataVotans,function(kv,vv){
                            html += 
                            `<li>
                                <img src="${vv.profilMediumImageUrl}" alt="${vv.name}"  width="50" height="50" style="border-radius: 100%,object-fit:cover"/>
                                <span>${vv.name}</span>
                            </li>`;
                        })
                        html += "</ul>";
                        bootbox.alert(html);
                    })
                   
                }
			});
        })
        })

        $('.addEvaliationCriteria').off().on('click',()=>{
            var existEvaluationCriteria = <?= json_encode(@$parentForm["evaluationCriteria"]) ?>;
            var newEvaluationCriteria = {
                jsonSchema :{
                    title : "Configurer le champs évaluation",
                    description : "",
                    properties : {
                        "activateLocalCriteria" : {
                            label: "Activer ces configurations",
                            inputType: "checkboxSimple",
                            params: checkboxSimpleParams,
                        },
                        "whoCanEvaluate" : {
                            inputType: "tags",
                            label : "Ajouter des rôles d'évaluateurs",
                            info:  "<small>Seuls ceux qui ont les rôles mentionnés ici et les admin peuvent évaluer.(ex: Evaluateur,Financeur,...)</small>",
                            options:{
                                "evaluateur" : "Evaluateur",
                                "financeur" : "Financeur",
                            }
                        },
                        "type" :{
                            inputType: "select",
                            label : "Type d'évaluation",
                            options:{
                                "noteCriterionBased" : "Évaluation par note",
                                "starCriterionBased" : "Évaluation par étoile",
                                "forOrAgainst" : "Pour ou contre"
                            }
                        },
                        "criterions" :{
                            inputType: "lists",
                            label: "Critères d'évaluation",
                            "entries":{
                                "label":{
                                    "type":"text",
                                    "label" : "Nom du critère {num}",
                                    "class":"col-md-5 col-sm-5 col-xs-10"
                                },
                                "coeff":{
                                    "label":"Coeff",
                                    "type":"text",
                                    "value":"1",
                                    "class":"col-md-2 col-sm-5 col-xs-10"
                                },
                                "note":{
                                    "label":"",
                                    "type":"text",
                                    "value":"0",
                                    "class":"col-md-2 col-sm-5 col-xs-10 hidden"
                                }

                            }
                        }
                    },
                    onLoads:{
                        onload : function(){

                            if($('#activateLocalCriteria').val()=="true")
                                $('.whoCanEvaluatetags,.typeselect,.criterionslists').show();
                            else
                                $('.whoCanEvaluatetags,.typeselect,.criterionslists').hide();

                            $('.activateLocalCriteriacheckboxSimple .btn-dyn-checkbox').on('click',function(){
                                if($('#activateLocalCriteria').val()=="true")
                                    $('.whoCanEvaluatetags,.typeselect,.criterionslists').fadeIn(1000);
                                else
                                    $('.whoCanEvaluatetags,.typeselect,.criterionslists').fadeOut(800);
                            })
                        }
                    },
                    save : function(formData){
                        var tplCtx = {
                            id : "<?= (string)$parentForm["_id"] ?>",
                            collection: "forms",
                            value: {},
                            path: "evaluationCriteria",
                            setType : [],
                        };
                        $.each(newEvaluationCriteria.jsonSchema.properties,function(k,v){
                            if(k == "criterions"){
                                tplCtx.value[k] = formData["criterions"];
                                $.each(tplCtx.value[k],function(i,j){
                                    tplCtx.setType.push({
                                        "path": "criterions."+i+".coeff",
                                        "type": "int"
                                    })
                                    tplCtx.setType.push({
                                        "path": "criterions."+i+".note",
                                        "type": "float"
                                    })
                                });
                            }
                            else
                                tplCtx.value[k] = $("#" + k).val();
                            
                        });
                        if (typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value(tplCtx, function (params) {
                                urlCtrl.loadByHash(location.hash);
                            });
                        }
                    }
                }
            }
            dyFObj.openForm(newEvaluationCriteria,null,existEvaluationCriteria);
        })
        
    </script>
<?php }else{
    echo "<p class='text-center text-danger'>Vous n'avez pas d'exigences pour ce champ,Veuillez ajouter des critères d'évaluation dans la configuration du template</p>";
} ?>
