<div class="row padding-20">
    <div class="col-xs-12">
        <!-- <h4>Organisations ou projets liés <button type="button" class="btn btn-sm btn-info addContext"><i class="fa fa-pencil"></i></button></h4> -->
        <label for="<?php echo $key ?>">
            <h4 style="color:<?php echo (@$titleColor) ? @$titleColor : "black"; ?>">
                <?php echo $label.$editQuestionBtn ?>
                <button type="button" class="btn btn-default addContext"><i class="fa fa-plus"></i> Ajouter</button>
            </h4>
        </label>
    </div>
    <div class="col-xs-12">
        <?php 
            if(isset($answer["context"]))
                foreach($answer["context"] as $kc => $vc){
                    echo  '<a href="#page.type.'.$vc["type"].'.id.'.$kc.'" class="lbh-preview-element" style="text-decoration:none">
                            <span class="label label-default padding-5"><i class="fa fa-lightbulb-o"></i> '.$vc["name"].'</span>
                        </a>';
                }
        ?>
    </div>
</div>
<script>
    $(function(){
        var tplCtx = {};
        var contextDf = {
          "jsonSchema" : {    
            "title" : "Organisations ou projets",
            "description" : "Rattacher cette proposition à un(des) organisation(s) ou projet(s)",
            "icon" : "fa-cog",
            "properties" : {
                "context" : {
                    "inputType" : "finder",
                    "id" : "parent",
                    "label" : "Qui relie à cet projet",
                    "initMe" : true,
                    "placeholder" : "Sélectionner des organisations et projets",
                    "rules" : {
                        "required" : false,
                        /*"lengthMin" : [ 
                            1, 
                            "parent"
                        ]*/
                    },
                    "initType" : [ 
                        "organizations", 
                        "projects"
                    ],
                    "openSearch" : true
                },
            },
            save : function (formData) {  
                tplCtx.value = {};
                $.each( contextDf.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                    if (k == "context")
                        tplCtx.value[k] = formData.context;
                });
                console.log("save tplCtx",tplCtx);
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) {
                        if(params.result){
                            toastr.success("Bien Ajouté");
                            dyFObj.closeForm();
                            reloadWizard();
                        }
                    } );
                }
            }
          }
        };
        $(".addContext").off().on("click",function() {  
            tplCtx.id = "<?= (string) $answer["_id"] ?>";
            tplCtx.collection = "<?= Form::ANSWER_COLLECTION ?>";
            tplCtx.path = "allToRoot";
            dyFObj.openForm( contextDf,null,<?= json_encode($answer) ?>);
        });
        coInterface.bindLBHLinks();
    })
</script>