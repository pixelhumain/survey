<style>
    .container.fixed-breadcrumb-filters,
    .container.fixed-breadcrumb-filters{
        display: none !important;
    }
</style>
<?php 
if($mode == "r" || $mode == "pdf"){ ?>
    <div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
        <label for="<?php echo $kunik ?>">
        	<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
        		<?php echo $label ?>
        	</h4>
        </label><br/>
        <?php echo $answers; ?>
    </div>
<?php 
}else{
?>
	<div class="col-md-12 no-padding">
        <div class="form-group">
            <label for="<?php echo $key ?>">
                <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>" class="<?= $canEditForm && $mode="fa" ? "" : $type ?>">
                    <?php echo $label.$editQuestionBtn ?>
                    <?php if($canEditForm && $mode="fa"){ ?>
                            <a href="javascript:;" class="btn btn-xs btn-danger config<?= $kunik ?>"><i class="fa fa-cog"></i></a>
                        <?php } ?>
                </h4>
            </label>
            <br/>
            <?php if(!empty($info)){ ?>
                <small id="<?php echo $key ?>Help" class="form-text text-muted">
                    <?php echo $info ?>
                </small>
            <?php } ?>
            
            <script type="text/javascript">
                var formStandalone = true;
                var checkboxSimpleParams = {
                    "onText" : trad.yes,
                    "offText" : trad.no,
                    "onLabel" : trad.yes,
                    "offLabel" : trad.no,
                    "labelText" : tradCms.label
                }
                jQuery(document).ready(function() {
                    $("#wizardcontainer").removeClass('container').addClass("container-fluid");
                });
            </script>


            <?php
                $slug = $el['slug'];
                if(!empty($parentForm["config"])){
                    $formConfig = PHDB::findOneById(Form::COLLECTION,$parentForm["config"],array("parent"));
                    $keyParent = array_keys($formConfig["parent"]);
                    if($formConfig[$keyParent]["type"] == Organization::COLLECTION)
                        $elConfig = PHDB::findOneById(Organization::COLLECTION,$keyParent,array("slug"));
                    elseif($formConfig[$keyParent]["type"] == Project::COLLECTION)
                        $elConfig = PHDB::findOneById(Project::COLLECTION,$keyParent,array("slug"));
                    
                    $slug = $elConfig["slug"];
                }
                echo $this->renderPartial("costum.views.custom.appelAProjet.callForProjects.main",
                    array(
                        //"paramsData" => $paramsData,
                        //"blockCms"=>$blockCms,
                        //"blockKey" => $blockKey,
                        "kunik" => "kunik".rand(),
                        "slug" => "coSindniSmarterre",
                        "clienturi" => Yii::app()->getRequest()->getBaseUrl(true)."/co/index/slug/coSindniSmarterre#welcome.slug.coSindniSmarterre.formid.620a5e534f94bf26f34c5be8.page.list?view=compact",
                        "formStandalone" => true,
                        "slug" => "coSindniSmarterre", //$slug,
                        //"clienturi" => Yii::app()->getRequest()->getBaseUrl(true)."/co/index/slug/".$slug."#welcome.slug.".$el["slug"].".formid.".(string)$parentForm["_id"].".page.list?view=compact",
                        )
                );
            ?>
        </div>

	</div>

<?php } ?>




