
<style>
    
    @media (min-width: 768px){
        #modalFinancer .modal-dialog {
            width: 600px;
            margin: 30px auto;
        }
         #modalFinancer.in .modal-dialog {
            top: 110px;
        }
    }
    #modalFinancer.modal {
        background-color: rgba(44, 62, 80, 0.8);
    }

    /*List*/

    @media (max-width: 767px) {
        .visible-xs {
            display: inline-block !important;
        }
        .block {
            display: block !important;
            width: 100%;
            height: 1px !important;
        }
    }
    #back-to-bootsnipp {
        position: fixed;
        top: 10px; right: 10px;
    }


    #modalFinancer .c-search > .form-control {
       border-radius: 0px;
       border-width: 0px;
       border-bottom-width: 1px;
       font-size: 1.3em;
       padding: 12px 12px;
       height: 44px;
       outline: none !important;
    }
    #modalFinancer .c-search > .form-control:focus {
        outline:0px !important;
        -webkit-appearance:none;
        box-shadow: none;
    }
    #modalFinancer .c-search > .input-group-btn .btn {
       border-radius: 0px;
       border-width: 0px;
       border-left-width: 1px;
       border-bottom-width: 1px;
       height: 44px;
    }


    #modalFinancer .title {
        font-size: 25px;
        font-weight: bold;
    }
    #modalFinancer ul.c-controls {
        list-style: none;
        margin: 0px;
        min-height: 44px;
    }

    #modalFinancer ul.c-controls li {
        margin-top: 8px;
        float: left;
    }

    #modalFinancer ul.c-controls li a {
        font-size: 1.7em;
        padding: 11px 10px 6px;   
    }
    #modalFinancer ul.c-controls li a i {
        min-width: 24px;
        text-align: center;
    }

    #modalFinancer ul.c-controls li a:hover {
        background-color: rgba(51, 51, 51, 0.2);
    }

    #modalFinancer .c-toggle {
        font-size: 1.7em;
    }

    #modalFinancer .name {
        font-size: 20px;
        font-weight: 700;
    }
    #modalFinancer .img-contain img {
        width: 70px;
        height: 70px;
    }

    #modalFinancer .c-info {
        padding: 5px 10px;
        font-size: 1.25em;
    }
    #modalFinancer #contact-list {
        max-height: 350px;
        overflow-y: auto;
    }

</style>
<?php 
$id = array_keys($parentForm["parent"])[0];
$type =  $parentForm["parent"][$id]["type"];
     $community = Element::getCommunityByTypeAndId($type, $id, "all", null,"Financeur", null, ["name", "profilMediumImageUrl"]);


//var_dump($community );
 ?>
<div class="container">
    <div class="row">
    
    <div id="modalFinancer" class="modal fade in">
        <div class="modal-dialog ">
            <div class="modal-content">
 
                <div class="modal-header">
                    <span class="title">Liste des Financeur</span>
                </div>
                <div class="modal-body no-padding">
                
                    <ul class="list-group co-scroll" id="contact-list">
                        <?php 
                            if (isset($community)) {
                                foreach ($community as $key => $value) {?> 
                                <li class="list-group-item">
                                    <div class="col-xs-12 col-sm-3 img-contain">
                                        <?php if (isset($value["profilMediumImageUrl"])) { ?>
                                            <img src="<?= $value["profilMediumImageUrl"] ?>" alt="<?= $value["name"] ?>" class="img-responsive img-circle" />
                                         <?php } else {?> 
                                         <img src="https://img.icons8.com/clouds/2x/money-bag.png" alt="<?= $value["name"] ?>" class="img-responsive img-circle" />
                                         <?php } ?>     
                                    </div>
                                    <div class="col-xs-12 col-sm-9">
                                        <span class="name"><?= $value["name"] ?></span><br/>
                                        <span class="glyphicon glyphicon-map-marker text-muted c-info" data-toggle="tooltip" title="5842 Hillcrest Rd"></span>
                                        <span class="visible-xs"> <span class="text-muted">7396 E North St</span><br/></span>
                                        <span class="glyphicon glyphicon-earphone text-muted c-info" data-toggle="tooltip" title="(870) 288-4149"></span>
                                        <span class="visible-xs"> <span class="text-muted">(870) 288-4149</span><br/></span>
                                        <!-- <span class="fa fa-comments text-muted c-info" data-toggle="tooltip" title="scott.stevens@example.com"></span>
                                        <span class="visible-xs"> <span class="text-muted">scott.stevens@example.com</span><br/></span> -->
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                        <?php }
                            }
                         ?>
                        
                    </ul>
                     </div>
                
                <div class="modal-footer">
                    <div class="btn-group">
                        <button class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fermer</button>
                        <!-- <button class="btn btn-primary"><span class="glyphicon glyphicon-check"></span> Save</button> -->
                    </div>
                </div>
 
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dalog -->
    </div><!-- /.modal -->
    
<a data-toggle="modal" href="#modalFinancer" class="btn btn-primary">Financeur</a>


    </div>
</div>

<script type="text/javascript">

    $('[data-toggle="tooltip"]').tooltip();
    

</script>