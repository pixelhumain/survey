<?php if($answer){
	//var_dump($mode); exit;
    $isAapProject = false;
    foreach ($parentForm["parent"] as $parId => $parValue ){
        $contextId = $parId;
        $contextType = $parValue["type"];
    }

$debug = false;
$editBtnL = ( $canEdit == true && ( $mode == "w" || $mode == "fa") ) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";

$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

$paramsData = [ 
	"group" => [ 
		"Feature", 
		"Costum", 
		"Chef de Projet", 
		"Data", 
		"Mantenance" 
	],
	"nature" => [
		"investissement" => "Investissement",
		"fonctionnement" => "Fonctionnement"
	],
	"amounts" => [
		"price" => "Montant"
	],
	"estimate" => true
];

// function is_true($val, $return_null=false){
//     $boolval = ( is_string($val) ? filter_var($val, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) : (bool) $val );
//     return ( $boolval===null && !$return_null ? false : $boolval );
// }

if( isset($parentForm["params"][$kunik]["group"]) ) 
	$paramsData["group"] =  $parentForm["params"][$kunik]["group"];
if( isset($parentForm["params"][$kunik]["nature"]) ) 
	$paramsData["nature"] =  $parentForm["params"][$kunik]["nature"];
if( isset($parentForm["params"][$kunik]["amounts"]) ) 
	$paramsData["amounts"] =  $parentForm["params"][$kunik]["amounts"];
if( isset($parentForm["params"][$kunik]["estimate"]) ) 
	$paramsData["estimate"] =  Answer::is_true($parentForm["params"][$kunik]["estimate"]);

// if(isset($answers)){
// 	foreach ($answers as $q => $a) {
// 		if(isset($a["group"]))
// 			$paramsData["group"][] = $a["group"];
// 	}
// }



$properties = [
		"group" => [
            "placeholder" => "Groupé",
            "inputType" => "select",
            "options" => $paramsData["group"],
            "rules" => [ "required" => true ]
        ],
        "nature" => [
            "placeholder" => "Nature de l’action",
            "inputType" => "select",
            "options" => $paramsData["nature"],
            "rules" => [ "required" => true ]
        ],
        "poste" => [
            "inputType" => "text",
            "label" => "Poste de dépense",
            "placeholder" => "Poste de dépense",
            "rules" => [ "required" => true  ]
        ]
    ];
    foreach ($paramsData["amounts"] as $k => $l) {
    	$properties[$k] = [ "inputType" => "number",
				            "label" => $l,
				            "propType" =>"amount",
				            "placeholder" => $l,
				            //"rules" => [ "required" => true, "number" => true ]
				        ];
    }
    if($debug)var_dump($answers);
    if($debug)var_dump($paramsData);
?>	

<?php

echo $this->renderPartial("survey.views.tpls.forms.ocecoform.budgetTable",
	                      [ 
	                        "form" => $form,
	                        "wizard" => true, 
	                        "answers"=>$answers,
	                        "answer"=>$answer,
                            "mode" => $mode,
                            "kunik" => $kunik,
                            "answerPath"=>$answerPath,
                            "key" => $key,
                            "titleColor" => $titleColor,
                            "properties" => $properties,
                            "canAdminAnswer"  => $canAdminAnswer,
                            "label" => $label,
                            "editQuestionBtn" => $editQuestionBtn,
                            "editParamsBtn" => $editParamsBtn,
                            "editBtnL" => $editBtnL,
                            "info" => $info,
	                        //"showForm" => $showForm,
	                        "paramsData" => $paramsData,
	                        "canEdit" => $canEdit,  
	                        //"el" => $el 
	                    ] ,true );

?>

<style type="text/css">
	.oceco-styled-table {
	    border-collapse: collapse;
	    margin: 25px 0;
	    font-size: 0.9em;
	    font-family: sans-serif;
	    min-width: 400px;
	    box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
	}

	.oceco-styled-table thead tr {
	    background-color: #009879;
	    color: #ffffff;
	    text-align: left;
	}

	.oceco-styled-table th,
	.oceco-styled-table td {
	    padding: 12px 15px;
	}

	.oceco-styled-table tbody tr {
	    border-bottom: 1px solid #dddddd;
	}

	.oceco-styled-table tbody tr:nth-of-type(even) {
	    background-color: #f3f3f3;
	}

	.oceco-styled-table tbody tr:nth-of-type(odd) {
	    background-color: #e5ffe5;
	}

	.oceco-styled-table tbody tr:last-of-type {
	    border-bottom: 2px solid #009879;
	}

	.oceco-styled-table tbody tr.active-row {
	    font-weight: bold;
	    color: #009879;
	}

	.oceco-styled-table tbody.oceco-blanc-table tr{
		background-color: #fff;
	}

	.btn-group.special {
	  display: flex;
	}

	.special .btn {
	  flex: 1
	}
</style>

    <?php if( $paramsData["estimate"] ) {  ?>
<div id="" class="newform-estimate" style="display: none">
    <header>
        <h1>Proposer</h1>
    </header>
    <main class="mainc">
    	<div class="ocecowebview">
	    	<label class="label-esti">Proposition de prix</label>
	    	<label class="label-esti">Durée</label>
    	</div>
        <div class="">
            <label class="label-mobi">Proposition de prix</label>
            <input id="" class="todoinput e-price" type="number" spellcheck="false" placeholder="Proposition de prix" onfocus="this.placeholder=''" onblur="this.placeholder='Fonds, enveloppe ou budget mobilisé'" />
            <label class="label-mobi">Durée</label>
            <input id="new-credit-value" class="e-days todoinput" type="number" spellcheck="false" placeholder="Durée" onfocus="this.placeholder=''" onblur="this.placeholder='Durée'" />
            <div style="clear: both;"></div>
    	</div>

        <div id="single-line"></div>
        
        <ul id="estimate-list">
        </ul>
    </main>
</div>
<?php } 


if($mode != "r" && $mode != "pdf"){
?>

<script type="text/javascript">

var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

var FiData = <?php echo json_encode( (isset($answer["answers"]["aapStep1"]["depense"])) ? $answer["answers"]["aapStep1"]["depense"] : null ); ?>;

var projectId = "<?php echo (!empty($answer["project"]["id"]) ? $answer["project"]["id"] : "null"); ?>";

var answerId = "<?php echo (string)$answer["_id"]; ?>";
var cntxtId = "<?php echo $contextId; ?>";
var cntxtType = "<?php echo $contextType; ?>";

if (FiData != null) {
	var estimateList = FiData.reduce(function(accumulator, item){
	      if (item != null && typeof item["estimates"] != "undefined") {
	        accumulator.push(item["estimates"]);
	        return accumulator;
	      } else {
	      	accumulator.push([]);
	        return accumulator;
	      }
	},[]);
} else {
	var estimateList = {};
}


$(document).ready(function() { 
	
	sectionDyf.<?php echo $kunik ?> = {
		jsonSchema : {	
	        title : "Budget prévisionnel",
            icon : "fa-money",
            text : "Décrire ici les principaux postes de dépenses : à quoi correspondent les coûts ? <br/>Préciser ce qui relève des dépenses de fonctionnement et des dépenses d’investissement.<br/>Les coûts doivent être en <b>hors taxe</b>.",
	        properties : <?php echo json_encode( $properties ); ?>,
	        save : function () {  
	        	var today = new Date();
	            tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
	            $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });
	            
	            var connectedData = ["financer","todo","payed","progress","worker","validFinal","votes",];
	            $.each( connectedData , function(k,attr) { 
	        		if(notNull("answerObj."+tplCtx.path+"."+attr))
	            		tplCtx.value[attr] = jsonHelper.getValueByPath(answerObj,tplCtx.path+"."+attr);
	        	 });

	            mylog.log("save tplCtx",tplCtx);
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                	dyFObj.closeForm();

	                    tplCtx.value = {state: "on"}

	                    tplCtx.path = "status.progress";
	                    dataHelper.path2Value( tplCtx, function(params){});

	                    tplCtx.path = "status.vote";
	                    dataHelper.path2Value( tplCtx, function(params){});

	                    tplCtx.path = "status.finance";
	                    dataHelper.path2Value( tplCtx, function(params){});

	                    tplCtx.path = "status.call";
	                    dataHelper.path2Value( tplCtx, function(params){});

	                    if (projectId != "null") {

	                    	tplCtx.path = "status.newaction";
	                    	dataHelper.path2Value( tplCtx, function(params){});

		                   	ajaxPost("", baseUrl+'/survey/form/generateproject/answerId/'+answerId+'/parentId/'+cntxtId+'/parentType/'+cntxtType, 
							null,
							function(data){},"html");
		                }

	                    reloadWizard();
                        showStepForm(localStorage.wizardStep);
	                } );

	            }
	    	}
	    }
	};

	sectionDyf.<?php echo $kunik ?>Params = {
		jsonSchema : {	
	        title : "<?php echo $kunik ?> config",
	        description : "Liste de question possible",
	        icon : "fa-cog",
	        properties : {
	            group : {
	                inputType : "array",
	                label : "Liste des groups",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.group
	            },
	            nature : {
	                inputType : "properties",
	                labelKey : "Clef",
	                labelValue : "Label affiché",
	                label : "Liste des natures possibles",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.nature
	            },
	            amounts : {
	                inputType : "properties",
	                labelKey : "Clef",
	                labelValue : "Label affiché",
	                label : "Liste des prix(ex:par année)",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.amounts
	            },
	            estimate : {
	                inputType : "checkboxSimple",
                    label : "estimate Prices",
                    params : {
                        onText : "Oui",
                        offText : "Non",
                        onLabel : "Oui",
                        offLabel : "Non",
                        labelText : "estimate Prices"
                    },
                    checked : sectionDyf.<?php echo $kunik ?>ParamsData.estimate
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	        		if(val.inputType == "properties")
	        		 	tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
	        		else if(val.inputType == "array")
	        		 	tplCtx.value[k] = getArray('.'+k+val.inputType);
	        		else
	        		 	tplCtx.value[k] = $("#"+k).val();
	        		 mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                	dyFObj.closeForm();
	                   reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
	                } );
	            }

	    	}
	    }
	};

	var max<?php echo $kunik ?> = 0;
	var nextValuekey<?php echo $kunik ?> = 0;

	if(notNull(<?php echo $kunik ?>Data)){
		$.each(<?php echo $kunik ?>Data, function(ind, val){
			if(parseInt(ind) > max<?php echo $kunik ?>){
				max<?php echo $kunik ?> = parseInt(ind);
			}
		});

		nextValuekey<?php echo $kunik ?> = max<?php echo $kunik ?> + 1;

	}

    mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

    //adds a line into answer
    $( ".add<?php echo $kunik ?>" ).off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path")+ nextValuekey<?php echo $kunik ?>;
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
    });

    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
    });

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    $('.newbtnestimate').click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.key = $(this).data("key");
		tplCtx.form = $(this).data("form");
		prioModal = bootbox.dialog({
	        message: $(".newform-estimate").html(),
	        show: false,
	        size: "large",
	        className: 'estimatedialog',
	        buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {

                    	tplCtx.path = "answers";
			        	// if( notNull(formInputs [tplCtx.form]) )
			        	tplCtx.path = "answers."+tplCtx.form;    

			        	tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+userId; 

			        	var today = new Date();
						today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();	
			            tplCtx.value = {
			            	price : $(".bootbox .e-price").val(),
			            	days : $(".bootbox .e-days").val(),
			            	name :  userConnected.name,
			            	date : today
			            };

				    	mylog.log("btnEstimate save",tplCtx);
				  	 	dataHelper.path2Value( tplCtx, function(){
				  	 		saveLinks(answerObj._id.$id,"estimated",userId);
							prioModal.modal('hide');
							reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");

				  	 	} );
			        }		
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: function(){
							prioModal.modal('hide');
							}
                }
            },
		    onEscape: function(){
							prioModal.modal('hide');
			}
	    });

	    var selectedErow = $(this).data("pos");

	   	prioModal.on('shown.bs.modal', function (e) {

	    	$('.estimatedialog #add-button').data("pos", selectedErow );

	    });

	    prioModal.modal("show");
	});

	$('.estibtnedit').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.key = $(this).data("key");
		tplCtx.form = $(this).data("form");
		tplCtx.price = $(this).data("price");

		var days = $(this).data("days");
		var price = $(this).data("price");
	
		prioModal = bootbox.dialog({
	        message: $(".newform-estimate").html(),
	        show: false,
	        size: "large",
	        className: 'estimatedialog',
	        buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {

                    	tplCtx.path = "answers";
			        	// if( notNull(formInputs [tplCtx.form]) )
			        	tplCtx.path = "answers."+tplCtx.form;    

			        	tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+userId; 

			        	var today = new Date();
						today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();	
			            tplCtx.value = {
			            	price : $(".bootbox .e-price").val(),
			            	days : $(".bootbox .e-days").val(),
			            	name :  userConnected.name,
			            	date : today
			            };

				    	mylog.log("btnEstimate save",tplCtx);
				  	 	dataHelper.path2Value( tplCtx, function(){
				  	 		saveLinks(answerObj._id.$id,"estimated",userId);
							prioModal.modal('hide');
							reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");

				  	 	} );
			        }		
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: function(){
							prioModal.modal('hide');
							}
                }
            },
		    onEscape: function(){
							prioModal.modal('hide');
			}
	    });

	   	prioModal.on('shown.bs.modal', function (e) {
	   		$(".e-price").val(price);
	    	$(".e-days").val(days);
	    });

	    prioModal.modal("show");

	});

	$('.estibtndelete').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.key = $(this).data("key");
		tplCtx.form = $(this).data("form");
		tplCtx.uid = $(this).data("uid");

		prioModal = bootbox.dialog({
	    	title: trad.confirmdelete,
	    	show: false,
	        message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
	        buttons: [
	            {
	            	label: "Ok",
	            	className: "btn btn-primary pull-left",
	            	callback: function() {
	                	var esti_pos = FiData[tplCtx.pos]["estimates"];
	                	console.log("azer", esti_pos);
	                	delete esti_pos[tplCtx.uid];

	                	tplCtx.path = "answers";
			        	// if( notNull(formInputs [tplCtx.form]) )
			        	tplCtx.path = "answers."+tplCtx.form;    

			        	tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".estimates"; 

			        	tplCtx.value = esti_pos;

			        	mylog.log("btnEstimate save",tplCtx);
				  	 	dataHelper.path2Value( tplCtx, function(){
				  	 		saveLinks(answerObj._id.$id,"estimated",userId);
							prioModal.modal('hide');
							reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");

				  	 	} );
	            	}
	            },
	            {
	              label: "Annuler",
	              className: "btn btn-default pull-left",
	              callback: function() {}
	            }
	        ]
	      });
	
		prioModal.modal("show");
	});


<?php if( $paramsData["estimate"] ) {  ?>
	
	$('.btnEstimateSelected').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.key = $(this).data("key");
		tplCtx.form = $(this).data("form");
		tplCtx.price = $(this).data("price");

		$(this).removeClass('btn-default').addClass("btn-success");

		//remove all selected
		var esti_pos = FiData[tplCtx.pos]["estimates"];
    	
    	$.each(esti_pos, function(eid, evl){
    		if (typeof evl["selected"] != "undefined" && evl["selected"] == true) {
    			esti_pos[eid]["selected"] = false;
    		}
    	})

    	tplCtx.path = "answers";
    	// if( notNull(formInputs [tplCtx.form]) )
    	tplCtx.path = "answers."+tplCtx.form;

        tplCtx.pos = $(this).data("pos");
        tplCtx.collection = "answers";
        tplCtx.id = $(this).data("id");
        tplCtx.key = $(this).data("key");
        tplCtx.form = $(this).data("form");
        tplCtx.price = $(this).data("price");

    	tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".estimates";

    	tplCtx.value = esti_pos;

    	mylog.log("btnEstimate save",tplCtx);
  	 	dataHelper.path2Value( tplCtx, function(){
  	 		saveLinks(answerObj._id.$id,"estimated",userId);
			
  	 	} );
  	 	//end remove all selected

        tplCtx.pos = $(this).data("pos");
        tplCtx.collection = "answers";
        tplCtx.id = $(this).data("id");
        tplCtx.key = $(this).data("key");
        tplCtx.form = $(this).data("form");
        tplCtx.price = $(this).data("price");

		tplCtx.pathBase = "answers."+tplCtx.form;

    	tplCtx.path = tplCtx.pathBase+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+$(this).data("uid")+".selected"; 	
        tplCtx.value = true;
        var thisbtn = $(this);
		mylog.log("btnEstimateSelected save",tplCtx);
  	 	dataHelper.path2Value( tplCtx, function(){
            tplCtx.pos = thisbtn.data("pos");
            tplCtx.collection = "answers";
            tplCtx.id = thisbtn.data("id");
            tplCtx.key = thisbtn.data("key");
            tplCtx.form = thisbtn.data("form");
            tplCtx.price = thisbtn.data("price");

  	 		tplCtx.path = tplCtx.pathBase+"."+tplCtx.key+"."+tplCtx.pos+".price"; 	
	        tplCtx.value = tplCtx.price;

			mylog.log("btnEstimateSelected save",tplCtx);
	  	 	dataHelper.path2Value( tplCtx, function(){
	  	 		$("#price"+tplCtx.pos).html( tplCtx.price+"€" );
	  	 		saveLinks(answerObj._id.$id,"intentValidated",userId);
	  	 		reloadInput("<?php echo $key ?>", "<?php echo @$form["id"] ?>");
	  	 	 } );
  	 	} );	
	});

<?php } ?>


});


</script>
<?php } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} 


}

?>
