
<style>
    
    @media (min-width: 768px){
        #modalValidater .modal-dialog {
            width: 600px;
            margin: 30px auto;
        }
         #modalValidater.in .modal-dialog {
            top: 110px;
        }
    }
    #modalValidater.modal {
        background-color: rgba(44, 62, 80, 0.8);
    }

    /*List*/

    @media (max-width: 767px) {
        .visible-xs {
            display: inline-block !important;
        }
        .block {
            display: block !important;
            width: 100%;
            height: 1px !important;
        }
    }
    #back-to-bootsnipp {
        position: fixed;
        top: 10px; right: 10px;
    }


    #modalValidater .c-search > .form-control {
       border-radius: 0px;
       border-width: 0px;
       border-bottom-width: 1px;
       font-size: 1.3em;
       padding: 12px 12px;
       height: 44px;
       outline: none !important;
    }
    #modalValidater .c-search > .form-control:focus {
        outline:0px !important;
        -webkit-appearance:none;
        box-shadow: none;
    }
    #modalValidater .c-search > .input-group-btn .btn {
       border-radius: 0px;
       border-width: 0px;
       border-left-width: 1px;
       border-bottom-width: 1px;
       height: 44px;
    }


    #modalValidater .title {
        font-size: 25px;
        font-weight: bold;
    }
    #modalValidater ul.c-controls {
        list-style: none;
        margin: 0px;
        min-height: 44px;
    }

    #modalValidater ul.c-controls li {
        margin-top: 8px;
        float: left;
    }

    #modalValidater ul.c-controls li a {
        font-size: 1.7em;
        padding: 11px 10px 6px;   
    }
    #modalValidater ul.c-controls li a i {
        min-width: 24px;
        text-align: center;
    }

    #modalValidater ul.c-controls li a:hover {
        background-color: rgba(51, 51, 51, 0.2);
    }

    #modalValidater .c-toggle {
        font-size: 1.7em;
    }

    #modalValidater .name {
        font-size: 20px;
        font-weight: 700;
    }
    #modalValidater .img-contain img {
        width: 70px;
        height: 70px;
        object-fit: cover;
    }

    #modalValidater .c-info {
        padding: 5px 10px;
        font-size: 1.25em;
    }
    #modalValidater #contact-list {
        max-height: 350px;
        overflow-y: auto;
    }
    #wizardLinks{
        z-index: 0;
    }

</style>
    <div id="modalValidater" class="modal fade in" >
        <div class="modal-dialog" style="top:165px">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="title">Liste des evaluateurs</span>
                </div>
                <div class="modal-body no-padding">
                
                    <ul class="list-group co-scroll" id="contact-list">
                        <?php
                            // usort($community, function ($a, $b) {
                            //     return (@$a["evaluationCounter"] >= @$b["evaluationCounter"]) ? -1 : 1;
                            //   });
                            if (isset($community)) {
                                foreach ($community as $key => $value) {  ?> 
                                <?php if(isset($answer[$stepEvaluation]["evaluation"][$key])){ ?>
                                    <li class="list-group-item">
                                    <a href="#page.type.citoyens.id.<?= (string)$value["_id"] ?>" class="lbh-preview-element tooltips">
                                        <div class="col-xs-12 col-sm-3 img-contain">
                                            <?php if (isset($value["profilMediumImageUrl"])) { ?>
                                                <img src="<?= $value["profilMediumImageUrl"] ?>" alt="<?= $value["name"] ?>" class="img-responsive img-circle" />
                                            <?php } else {?> 
                                            <img src="<?= Yii::app()->getModule( "co2" )->assetsUrl."/images/thumbnail-default.jpg"; ?>" alt="<?= $value["name"] ?>" class="img-responsive img-circle" />
                                            <?php } ?>     
                                        </div>
                                        <div class="col-xs-12 col-sm-9">
                                            <span class="name"><?= $value["name"] ?></span><br/> 
                                            <!-- <span class="label label-success" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Nombre d'évaluation : <?php // $value["evaluationCounter"] ?>"> <?php // $value["evaluationCounter"] ?> / <?php // echo count($answers) ?></span> -->
                                            <!-- <span class="visible-xs"> <span class="text-muted">Nombre d'évaluation : <?php //$value["evaluationCounter"] ?></span><br/></span> -->
                                            <!-- <span class="glyphicon glyphicon-earphone text-muted c-info" data-toggle="tooltip" title="(870) 288-4149"></span>
                                            <span class="visible-xs"> <span class="text-muted">(870) 288-4149</span><br/></span>
                                            <span class="fa fa-comments text-muted c-info" data-toggle="tooltip" title="scott.stevens@example.com"></span>
                                            <span class="visible-xs"> <span class="text-muted">scott.stevens@example.com</span><br/></span> -->
                                        </div>
                                        <div class="clearfix"></div>
                                    </a>
                                </li>
                        <?php   }
                                }
                            }
                         ?>
                        
                    </ul>
                     </div>
                
                <div class="modal-footer">
                    <div class="btn-group">
                        <button class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fermer</button>
                        <!-- <button class="btn btn-primary"><span class="glyphicon glyphicon-check"></span> Save</button> -->
                    </div>
                </div>
 
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dalog -->
    </div><!-- /.modal -->
    
<a data-toggle="modal" href="#modalValidater" class="btn btn-sm btn-primary pull-left margin-bottom-15 margin-right-5">Voir la liste des evaluateurs</a>


<script type="text/javascript">
    $('[data-toggle="tooltip"]').tooltip();
</script>