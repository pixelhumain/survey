<?php
    /*$me = isset(Yii::app()->session['userId']) ? Person::getById(Yii::app()->session['userId']) : null;
    $financement = $answers;
    $parentFormId = array_keys($parentForm["parent"])[0];
    $allowedRoles = (isset($form["params"]["canEdit"]) && is_array($form["params"]["canEdit"])) ?
    $form["params"]["canEdit"] : array();
    $roles = isset($me["links"]["memberOf"][$parentFormId]["roles"]) ? $me["links"]["memberOf"][$parentFormId]["roles"] : null;
    $isAdmin = isset($me["links"]["memberOf"][$parentFormId]["isAdmin"]) ? $me["links"]["memberOf"][$parentFormId]["isAdmin"] :false;
    $canFinance =  false;
    $intersectRoles = array_intersect($allowedRoles,$roles);
    if(count($intersectRoles) != 0 || $isAdmin)
        $canFinance =true;*/
?>
<?php if($answer){

$isAapProject = false;
if(isset($parentForm["type"]) && ($parentForm["type"] == "aapConfig" || $parentForm["type"] == "aap")){
    foreach ($parentForm["parent"] as $parId => $parValue ){
        $contextId = $parId;
        $contextType = $parValue["type"];
        $isAapProject = true;
    }
}

$copy = "aapStep1.depense";

if( isset($parentForm["params"][$kunik]["budgetCopy"]) )
    $copy = $parentForm["params"][$kunik]["budgetCopy"];
else if( count(Yii::app()->session["budgetInputList"]) == 1 )
    $copy = array_keys( Yii::app()->session["budgetInputList"])[0];

$copyT = explode(".", $copy);
$copyF = $copyT[0];
$budgetKey = @$copyT[1];
$answers = null;

if($answer){
if (isset($parentForm["mapping"]["depense"])) {
    $mapping = $parentForm["mapping"]["depense"];
    $mappingExplode = explode(".", $mapping);

    if (isset($answer[$mappingExplode[0]][$mappingExplode[1]][$mappingExplode[2]])) {
        $answers = $answer[$mappingExplode[0]][$mappingExplode[1]][$mappingExplode[2]];
    }
}
elseif(isset($parentForm["type"]) && ($parentForm["type"] == "aapConfig" || $parentForm["type"] == "aap")){
    if( $parentForm["type"] == "aap"){
        $configEl = PHDB::findOneById(Form::COLLECTION, $parentForm["config"]);
    }elseif ( $parentForm["type"] == "aapConfig"){
        $configEl = $parentForm;
    }
    if (isset($parentForm["mapping"]["depense"])) {
        $mapping = $configEl["mapping"]["depense"];
        $mappingExplode = explode(".", $mapping);

        if (isset($answer[$mappingExplode[0]][$mappingExplode[1]][$mappingExplode[2]])) {
            $answers = $answer[$mappingExplode[0]][$mappingExplode[1]][$mappingExplode[2]];
        }
    }
}
}

//var_dump($budgetKey);
if($wizard){
if( $budgetKey ){
    if( isset($answer["answers"][$copyF][$budgetKey]) && count($answer["answers"][$copyF][$budgetKey])>0)
    $answers = $answer["answers"][$copyF][$budgetKey];
} else if( isset($answer["answers"][$copyF][$budgetKey]) && count($answer["answers"][$copyF][$budgetKey])>0 )
    $answers = $answer["answers"][$copyF][$kunik];
} else {
if(!empty($budgetKey) && !empty($answer["answers"][$budgetKey]) )
    $answers = $answer["answers"][$budgetKey];
else if(isset($answers))
    $answers = $answers;
}

$editBtnL =  "";

$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";


$paramsData = [
"financerTypeList" => Ctenat::$financerTypeList,
"limitRoles" =>["Financeur"],
"openFinancing" => true
];

if( isset($parentForm["params"][$kunik]) ) {
if( isset($parentForm["params"][$kunik]["tpl"]) )
    $paramsData["tpl"] =  $parentForm["params"][$kunik]["tpl"];
if( isset($parentForm["params"][$kunik]["budgetCopy"]) )
    $paramsData["budgetCopy"] =  $parentForm["params"][$kunik]["budgetCopy"];
if( isset($parentForm["params"][$kunik]["financerTypeList"]) )
    $paramsData["financerTypeList"] =  $parentForm["params"][$kunik]["financerTypeList"];
if( isset($parentForm["params"][$kunik]["limitRoles"]) )
    $paramsData["limitRoles"] =  $parentForm["params"][$kunik]["limitRoles"];
if( isset($parentForm["params"][$kunik]["openFinancing"]) )
    $paramsData["openFinancing"] =  $parentForm["params"][$kunik]["openFinancing"];
}
if (!empty($contextId)) {
$communityLinks = Element::getCommunityByParentTypeAndId( [@$contextId => ["type" => @$contextType]] );
}else{
$communityLinks = [];
}
$organizations = Link::groupFindByType( Organization::COLLECTION,$communityLinks,["name","links"] );

$orgs = [];
foreach ($organizations as $id => $or) {
$roles = null;

if( isset( $communityLinks[$id]["roles"] ) )
    $roles = $communityLinks[$id]["roles"];


if( $paramsData["limitRoles"] && !empty($roles) )
{
    foreach ($roles as $i => $r)
    {
        if( in_array($r, $paramsData["limitRoles"]) )
            $orgs[$id] = $or["name"];
    }
}
}


$listLabels = array_merge(Ctenat::$financerTypeList,$orgs);

$properties = [
  // "financerType" => [
  //   "placeholder" => "Type de Financement",
  //       "inputType" => "select",
  //       "list" => "financerTypeList",
  //       "rules" => [
  //           "required" => true
  //       ]
  //   ],
    "poste" => [
        "inputType" => "text",
        "label" => "Contexte",
        "placeholder" => "Contexte",
        "size" => 4,
        "rules" => [ "required" => true ]
    ],
    "financer" => [
        "placeholder" => "Financeur",
        "inputType" => "select",
        "list" => "financersList",
        "subLabel" => "Si financeur public, l’inviter dans la liste ci-dessous (au cas où il n’apparait pas demandez à votre référent territoire de le déclarer comme partenaire financeur",
        "size" => 6,
    ]
];
$amounts = (isset($parentForm["params"][$budgetKey]["amounts"])) ? $parentForm["params"][$budgetKey]["amounts"] : ["price" => "Montant Financé"] ;
foreach ( $amounts as $k => $l) {
$properties[$k] = [ "inputType" => "text",
                    "label" => $l,
                    "placeholder" => $l,
                    "propType" => "amount"
                ];
}
?>

<?php
echo $this->renderPartial("survey.views.tpls.forms.ocecoform.financementFromBudgetTable",
                      [
                        "form" => $form,
                        "wizard" => true,
                        "answers"=>$answers,
                        "answer"=>$answer,
                        "mode" => $mode,
                        "kunik" => $kunik,
                        "answerPath"=>$answerPath,
                        "key" => $key,
                        "titleColor" => $titleColor,
                        "properties" => $properties,
                        "copy" => $copy,
                        "copyF" => $copyF,
                        "budgetKey" => $budgetKey,

                        "label" => $label,
                        "editQuestionBtn" => $editQuestionBtn,
                        "editParamsBtn" => $editParamsBtn,
                        "editBtnL" => $editBtnL,
                        "info" => $info,
                        //"showForm" => $showForm,
                        "paramsData" => $paramsData,
                        "canEdit" => $canEdit,
                        "canAdminAnswer"  => $canAdminAnswer,
                        "mappingForm" => @$mappingExplode[1],
                        "mappingInput" => @$mappingExplode[2]
                        //"el" => $el
                    ] ,true );

?>

<div class="form-financer" style="display:none;">
<span class="bold">Financeur existant dans la communauté</span><br/>
<select id="financer" style="width:100%;">
    <option value=0 >Quel financeur</option>
    <?php foreach ($orgs as $v => $f) {
        echo "<option value='".$v."'>".$f."</option>";
    } ?>
</select>
<br><span class="bold">ou Financeur inexistant dans la communauté</span><br/>
<?php //if(!(boolean)$paramsData["openFinancing"]) { ?>
<span class="bold">Nom du Financeur Libre</span>
<br>
<input type="text" id="financerName" name="financerName" style="width:100%;">

<span class="bold">Email</span>
<br>
<input type="text" id="financerEmail" name="financerEmail" style="width:100%;">
<?php //} ?>

<br><br>
<span class="bold">Fonds, enveloppe ou budget mobilisé</span>
<br>
<input type="text" id="financeLine" name="financeLine" style="width:100%;">
<span class="bold">Montant Financé</span>
<br>
<input type="text" id="financeAmount" name="financeAmount" style="width:100%;">
</div>

<div id="container" class="new-form-financer" style="display: none">
<header>
    <h1>Financeur</h1>
</header>
<main class="mainc">
    <div id="switchcom">
        <div class="btn-group depenseoceco">
            <button type="button" id="dcombtn" class="dcombtn switchcombtn btn active" data-switchactbtn="dcombtn" data-removebtn="icombtn" data-switchactdiv="dcomdiv" data-removediv="icomdiv" >Depuis la communauté </button>

            <button type="button" id="icombtn" class="icombtn switchcombtn btn " data-switchactbtn="icombtn" data-removebtn="dcombtn" data-switchactdiv="icomdiv" data-removediv="dcomdiv">Inexistant dans la communauté</button>
        </div>

        <div style="clear: both;"></div>
    </div>

    <div class="dcomdiv">
        <label class="label-esti">Financeur</label>
    </div>
    <div id="dcomdiv" class="dcomdiv switchcomdiv">
        <select id="financer" class="fi-financer todoinput">
            <option value=0 >Quel financeur</option>
            <?php foreach ($orgs as $v => $f) {
                echo "<option value='".$v."'>".$f."</option>";
            } ?>
        </select>
        <button class="btn btn-default sup-btn">Ajouter</button>

        <div style="clear: both;"></div>
    </div>

    <div class="icomdiv ocecowebview  hide">
        <label class="label-esti">Nom</label>
        <label class="label-esti">Email</label>
    </div>
    <div class="icomdiv hide">
        <!-- <label class=" spanfi-name">Nom</label><label class=" spanfi-mail">Email</label> -->
        <label class="label-mobi">Nom</label>
        <input id="" class="todoinput fi-name" type="text" spellcheck="false" placeholder="Nom" onfocus="this.placeholder=''" onblur="this.placeholder='Nom'" />
        <label class="label-mobi">Email</label>
        <input id="new-credit-value" class="fi-mail todoinput" type="text" spellcheck="false" placeholder="Email" onfocus="this.placeholder=''" onblur="this.placeholder='Email'" />
        <div style="clear: both;"></div>

       <!--  <label class=" spanfi-fond">Fonds, enveloppe ou budget mobilisé</label><label class=" spanfi-montant">Montant Financé</label><label class=" spanfi-btn"></label> -->

    </div>

    <div class="ocecowebview">
        <label class="label-esti">Fonds, enveloppe ou budget mobilisé</label>
        <label class="label-esti">Montant Financé</label>
    </div>

    <div class="">

        <label class="label-mobi">Fonds, enveloppe ou budget mobilisé</label>
        <input id="" class="todoinput fi-fond" type="text" spellcheck="false" placeholder="Fonds, enveloppe ou budget mobilisé" onfocus="this.placeholder=''" onblur="this.placeholder='Fonds, enveloppe ou budget mobilisé'" />
        <label class="label-mobi">Montant Financé</label>
        <input id="new-credit-value" class="fi-montant todoinput" type="number" spellcheck="false" placeholder="Montant Financé" onfocus="this.placeholder=''" onblur="this.placeholder='Montant Financé'" />

        <div style="clear: both;"></div>
    </div>

    <div id="single-line"></div>

    <ul id="financer-list">
    </ul>
</main>
</div>

<?php
if( isset($parentForm["params"]["financement"]["tpl"])){
//if( $parentForm["params"]["financement"]["tpl"] == "tpls.forms.equibudget" )
    $this->renderPartial( "costum.views.".$parentForm["params"]["financement"]["tpl"] ,
    [ "totalFin"  => $total,
      "totalBudg" => Yii::app()->session["totalBudget"]["totalBudget"] ] );
// else
// 	$this->renderPartial( "costum.views.".$parentForm["params"]["financement"]["tpl"]);
}
?>
<?php if($mode != "pdf"){ ?>
<script type="text/javascript">

if(typeof dyFObj.elementObjParams == "undefined")
dyFObj.elementObjParams = {};

dyFObj.elementObjParams.budgetInputList = <?php echo json_encode( Yii::app()->session["budgetInputList"] ); ?>;
dyFObj.elementObjParams.financerTypeList = <?php echo json_encode(Ctenat::$financerTypeList); ?>;
dyFObj.elementObjParams.financersList = <?php echo json_encode($orgs); ?>;


var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answer["answers"][$budgetKey])) ? $answer["answers"][$budgetKey] : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

var FiData = <?php echo json_encode( (isset($answer["answers"]["aapStep1"]["depense"])) ? $answer["answers"]["aapStep1"]["depense"] : null ); ?>;

if (FiData != null) {
var financerList = FiData.reduce(function(accumulator, item){
      if (item != null && typeof item["financer"] != "undefined") {
        accumulator.push(item["financer"]);
        return accumulator;
      } else {
          accumulator.push([]);
        return accumulator;
      }
},[]);
} else {
var financerList = {};
}

var cntxtId = "<?php echo $contextId; ?>";
var cntxtType = "<?php echo $contextType; ?>";

var supelementparents = {
afterSave : function(data) {
    var sendDataM = {
        parentId : cntxtId,
        parentType : cntxtType,
        listInvite : {
            organizations : {}
        }
    };
    sendDataM.listInvite.organizations[data.id] = {};
    sendDataM.listInvite.organizations[data.id]["name"] = data.map.name;
    sendDataM.listInvite.organizations[data.id]["roles"] = ["Financeur"];

    ajaxPost("",
        baseUrl+"/co2/link/multiconnect",
        sendDataM,
        function(data) {
            dyFObj.closeForm();
            //reloadInput("<?php //echo $key ?>//", "<?php //echo @$form["id"] ?>//");
            reloadWizard();
        },
        null,
        "json"
    );
}
};

$(document).ready(function() {
sectionDyf.<?php echo $kunik ?> = {
    "jsonSchema" : {
        "title" : "Plan de Financement",
        "icon" : "fa-money",
        "text" : "Décrire ici les financements mobilisés ou à mobiliser. Les coûts doivent être en <b>hors taxe</b>.",
        "properties" : <?php echo json_encode( $properties ); ?>,
        save : function () {
            tplCtx.value = {};
            $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) {
                tplCtx.value[k] = $("#"+k).val();
             });
            mylog.log("save tplCtx",tplCtx);
            if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
            else {
                dataHelper.path2Value( tplCtx, closePrioModalRel );
            }

        }
    }
};

sectionDyf.<?php echo $kunik ?>Params = {
    "jsonSchema" : {
        "title" : "<?php echo $kunik ?> config",
        "icon" : "fa-cog",
        "properties" : {
            financerTypeList : {
                inputType : "properties",
                labelKey : "Clef",
                labelValue : "Label affiché",
                label : "Liste des type de Financeurs",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.financerTypeList
            } ,
            limitRoles : {
                inputType : "array",
                label : "Liste des roles financeurs",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.limitRoles
            },
            tpl : {
                label : "Sub Template",
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.tpl
            },
            budgetCopy : {
                label : "Input Bugdet",
                inputType : "select",
                options :  dyFObj.elementObjParams.budgetInputList
            },
            openFinancing : {
                inputType : "checkboxSimple",
                label : "Financement Ouvert",
                subLabel : "Le Finacement est ouvert aux personnes ou organisations non inscrites dans la communauté",
                params : {
                    onText : "Oui",
                    offText : "Non",
                    onLabel : "Oui",
                    offLabel : "Non",
                    labelText : "Financement Ouvert"
                },
                checked : sectionDyf.<?php echo $kunik ?>ParamsData.estimate
            }
        },
        save : function () {
            tplCtx.value = {};
            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                if(val.inputType == "array")
                     tplCtx.value[k] = getArray('.'+k+val.inputType);
                else if(val.inputType == "properties")
                     tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
                else
                     tplCtx.value[k] = $("#"+k).val();
             });
            mylog.log("save tplCtx",tplCtx);

            if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
            else {
                dataHelper.path2Value( tplCtx, function(params) {
                    prioModal.modal('hide');
                    //reloadInput("<?php //echo $key ?>//", "<?php //echo @$form["id"] ?>//");
                    reloadWizard();
                } );
            }

        }
    }
};

mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");


$(".edit<?php echo $kunik ?>Params").off().on("click",function() {
    tplCtx.id = $(this).data("id");
    tplCtx.collection = $(this).data("collection");
    tplCtx.path = $(this).data("path");
    //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
    dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
});

$('.btnValidFinance').off().click(function() {
    tplCtx.pos = $(this).data("pos");
    tplCtx.budgetpath = $(this).data("budgetpath");
    tplCtx.collection = "answers";
    tplCtx.id = $(this).data("id");
    tplCtx.form = $(this).data("form");
    prioModal = bootbox.dialog({
        message: $(".form-validate-work").html(),
        title: "État du Financement",
        show: false,
        buttons: {
            success: {
                label: trad.save,
                className: "btn-primary",
                callback: function () {

                    var formInputsHere = formInputs;
                    tplCtx.path = "answers";
                    if( notNull(formInputs [tplCtx.form]) )
                        tplCtx.path = "answers";

                    tplCtx.path = tplCtx.path+".aapStep1."+tplCtx.pos+".validFinal";

                    var today = new Date();
                    today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();
                    tplCtx.value = {
                        valid : $(".bootbox #validWork").val(),
                        user : userId,
                        date : today
                    };
                    delete tplCtx.pos;
                    delete tplCtx.budgetpath;
                    mylog.log("btnValidateWork save",tplCtx);
                       dataHelper.path2Value( tplCtx, function(){
                           saveLinks(answerObj._id.$id,"financeValidated",userId,closePrioModalRel);
                       } );
                   }
            },
            cancel: {
              label: trad.cancel,
              className: "btn-secondary",
              callback: closePrioModal
            }
          },
            onEscape: closePrioModal
    });
    prioModal.modal("show");
});

$('.rebtnFinancer').off().click(function() {
    tplCtx.pos = $(this).data("pos");
    tplCtx.budgetpath = $(this).data("budgetpath");
    tplCtx.collection = "answers";
    tplCtx.id = $(this).data("id");
    tplCtx.form = $(this).data("form");
    prioModal = bootbox.dialog({
        message: $(".new-form-financer").html(),
        title: "Ajouter un Financeur",
        className: 'financerdialog',
        show: false,
        size: "large",
        buttons: {
            success: {
                label: trad.save,
                className: "btn-primary",
                callback: function () {

                    //var formInputsHere = formInputs;
                    // var financersCount = ( typeof eval("answerObj.answers."+tplCtx.budgetpath+"["+tplCtx.pos+"].financer") != "undefined" ) ? eval("answerObj.answers."+tplCtx.budgetpath+"["+tplCtx.pos+"].financer").length : 0;

                    tplCtx.path = "answers.aapStep1";

                    tplCtx.path = tplCtx.path+"."+tplCtx.budgetpath+"."+tplCtx.pos+".financer."+financerList[tplCtx.pos].length;

                    // if( notNull(formInputs [tplCtx.form]) )
                    // 	tplCtx.path = "answers."+tplCtx.budgetpath+"."+tplCtx.pos+".financer."+financersCount;

                    var today = new Date();
                    today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();
                    tplCtx.value = {
                        line   : $(".financerdialog .fi-fond").val(),
                        amount : $(".financerdialog .fi-montant").val(),
                        user   : userId,
                        date   : today
                    };

                    if($(".financerdialog .fi-financer").val() != 0 ){
                        tplCtx.value.id = $(".financerdialog .fi-financer").val();
                        tplCtx.value.name = $(".financerdialog .fi-financer option:selected").text();
                    }else if($(".financerdialog .fi-name").val() != "" ){
                        tplCtx.value.name = $(".financerdialog .fi-name").val();
                        tplCtx.value.email = $(".financerdialog .fi-mail").val();
                    }

                    delete tplCtx.pos;
                    delete tplCtx.budgetpath;
                    mylog.log("btnFinancer save",tplCtx);
                       dataHelper.path2Value( tplCtx, function(params) {
                           prioModal.modal('hide');
                           saveLinks(answerObj._id.$id,"financerAdded",userId,function(){prioModal.modal('hide');
                            //reloadInput("<?php //echo $key ?>//", "<?php //echo @$form["id"] ?>//");
                            });
                               reloadWizard();
                       } );
                }
            },
            cancel: {
              label: trad.cancel,
              className: "btn-secondary",
              callback: function() {
              }
            }
          },
            onEscape: function() {
              prioModal.modal("hide");
            }
    });
    prioModal.modal("show");

    prioModal.on('shown.bs.modal', function (e) {

        $(".sup-btn").off().on("click",function() {
            prioModal.modal("hide");
            dyFObj.openForm("organization",null,null,null,supelementparents );
        });

        $(".switchcombtn").on("click",function() {
            $(".financerdialog").find("."+$(this).data("switchactbtn")).addClass("active");
            $(".financerdialog").find("."+$(this).data("removebtn")).removeClass("active");
            $(".financerdialog").find("."+$(this).data("switchactdiv")).removeClass("hide");
            $(".financerdialog").find("."+$(this).data("removediv")).addClass("hide");
        });

    });

});

$('.fibtnedit').off().click(function() {
    tplCtx.pos = $(this).data("pos");
    tplCtx.key = $(this).data("key");
    tplCtx.collection = "answers";
    tplCtx.id = $(this).data("id");
    tplCtx.form = $(this).data("form");
    tplCtx.uid = $(this).data("uid");

    if (typeof $(this).data("finid") != "undefined") {
        var finid = $(this).data("finid");
    }else {
        var finname = $(this).data("finname");
        var finmail = $(this).data("finmail");
    }

    var finfond = $(this).data("finfond");
    var finmontant = $(this).data("finamount");

    prioModal = bootbox.dialog({
        message: $(".new-form-financer").html(),
        title: "Ajouter un Financeur",
        className: 'financerdialog',
        show: false,
        size: "large",
        buttons: {
            success: {
                label: trad.save,
                className: "btn-primary",
                callback: function () {

                    //var formInputsHere = formInputs;
                    // var financersCount = ( typeof eval("answerObj.answers."+tplCtx.budgetpath+"["+tplCtx.pos+"].financer") != "undefined" ) ? eval("answerObj.answers."+tplCtx.budgetpath+"["+tplCtx.pos+"].financer").length : 0;

                    tplCtx.path = "answers."+tplCtx.form;

                    tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".financer."+tplCtx.uid;

                    // if( notNull(formInputs [tplCtx.form]) )
                    // 	tplCtx.path = "answers."+tplCtx.budgetpath+"."+tplCtx.pos+".financer."+financersCount;

                    var today = new Date();
                    today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();
                    tplCtx.value = {
                        line   : $(".financerdialog .fi-fond").val(),
                        amount : $(".financerdialog .fi-montant").val(),
                        user   : userId,
                        date   : today
                    };

                    if($(".financerdialog .fi-financer").val() != 0 ){
                        tplCtx.value.id = $(".financerdialog .fi-financer").val();
                        tplCtx.value.name = $(".financerdialog .fi-financer option:selected").text();
                    }else if($(".financerdialog .fi-name").val() != "" ){
                        tplCtx.value.name = $(".financerdialog .fi-name").val();
                        tplCtx.value.email = $(".financerdialog .fi-mail").val();
                    }

                    delete tplCtx.pos;
                    delete tplCtx.budgetpath;
                    mylog.log("btnFinancer save",tplCtx);
                       dataHelper.path2Value( tplCtx, function(params) {
                           prioModal.modal('hide');
                           saveLinks(answerObj._id.$id,"financerAdded",userId,function(){prioModal.modal('hide');
                            //reloadInput("<?php //echo $key ?>//", "<?php //echo @$form["id"] ?>//");
                            });
                        reloadWizard();
                       } );
                }
            },
            cancel: {
              label: trad.cancel,
              className: "btn-secondary",
              callback: function() {
              }
            }
          },
            onEscape: function() {
              prioModal.modal("hide");
            }
    });
    prioModal.modal("show");

    prioModal.on('shown.bs.modal', function (e) {
        if (typeof finid != "undefined") {
            $('.fi-financer option[value="' + finid +'"]').prop("selected", true);

            $(".financerdialog").find(".dcombtn").addClass("active");
            $(".financerdialog").find(".icombtn").removeClass("active");
            $(".financerdialog").find(".dcomdiv").removeClass("hide");
            $(".financerdialog").find(".icomdiv").addClass("hide");
        }else {
            $(".fi-name").val(finname);
            $(".fi-mail").val(finmail);

            $(".financerdialog").find(".icombtn").addClass("active");
            $(".financerdialog").find(".dcombtn").removeClass("active");
            $(".financerdialog").find(".icomdiv").removeClass("hide");
            $(".financerdialog").find(".dcomdiv").addClass("hide");
        }
           $(".fi-fond").val(finfond);
        $(".fi-montant").val(finmontant);
    });

    $(".switchcombtn").on("click",function() {
        $(".financerdialog").find("."+$(this).data("switchactbtn")).addClass("active");
        $(".financerdialog").find("."+$(this).data("removebtn")).removeClass("active");
        $(".financerdialog").find("."+$(this).data("switchactdiv")).removeClass("hide");
        $(".financerdialog").find("."+$(this).data("removediv")).addClass("hide");
    });

});

$('.fibtndelete').off().click(function() {
    tplCtx.pos = $(this).data("pos");
    tplCtx.collection = "answers";
    tplCtx.id = $(this).data("id");
    tplCtx.key = $(this).data("key");
    tplCtx.form = $(this).data("form");
    tplCtx.uid = $(this).data("uid");

    prioModal = bootbox.dialog({
        title: trad.confirmdelete,
        show: false,
        message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
        buttons: [
            {
                label: "Ok",
                className: "btn btn-primary pull-left",
                callback: function() {
                    var fi_pos = FiData[tplCtx.pos]["financer"];

                    // delete fi_pos[tplCtx.uid];
                    fi_pos.splice(parseInt(tplCtx.uid), 1);

                    tplCtx.path = "answers";
                    // if( notNull(formInputs [tplCtx.form]) )
                    tplCtx.path = "answers."+tplCtx.form;

                    tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".financer";

                    tplCtx.value = fi_pos;

                    mylog.log("btnEstimate save",tplCtx);
                       dataHelper.path2Value( tplCtx, function(){
                           saveLinks(answerObj._id.$id,"estimated",userId);
                        prioModal.modal('hide');
                        //reloadInput("<?php //echo $key ?>//", "<?php //echo @$form["id"] ?>//");
                        reloadWizard();
                       } );
                }
            },
            {
              label: "Annuler",
              className: "btn btn-default pull-left",
              callback: function() {}
            }
        ]
      });

    prioModal.modal("show");
});


});

function closePrioModal(){
prioModal.modal('hide');
}

var flistObj = {
data : null,

buildList : function (pos) {

    if (typeof financerList[pos] !== "undefined") {
        $('.financerdialog #financer-list').html("");
        $.each(financerList[pos], function(fid, fval){

            var fline = {};

            if ( typeof fval.line != "undefined" )
                fline.line = fval.line;

            if ( typeof fval.amount != "undefined" ){
                fline.amount = fval.amount;
            } else {
                fline.amount = "(pas précisé)";
            }

            if ( typeof fval.name != "undefined" ){
                fline.name = fval.name;
            } else {
                fline.name = "(pas précisé)";
            }

            if ( typeof fval.email != "undefined" ){
                fline.email = fval.email;
            } else {
                fline.email = "(pas précisé)";
            }


            var str = '<li class="" data-pos='+pos+'>'+
                '<div class="todo-content">'+
                            '<div class="td-title">'+
                                '<h5 class="mb-0">'+fline.name+'</h5>'+
                                '<div class="td-datetime">'+

                                    '<div><i class="fa fa-money"></i><span class="td-datetimelabel">'+fline.amount+'</span></div>'+
                                    '<div class="td-datetimelabel2"><i class="fa fa-calendar-o"></i><span class="td-datetimelabel">'+fline.line+'</span></div>'+
                                    '<div class="td-datetimelabel2"><i class="fa fa-user"></i><span class="td-datetimelabel">'+fline.email+'</span></div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="todo-btn">'+
                            '<button class="td-datetimelabel2 edittodoItem actionbtntodoedit" data-idpos="'+pos+' " ><i class="fa fa-pencil"></i></button>'+
                            '<button class="td-datetimelabel2 deletetodoItem actionbtntododelete" data-idpos="'+pos+' " ><i class="fa fa-trash"></i></button>'+
                            '</div>'+
                    '</li>';

            $('.financerdialog #financer-list').prepend(str);
        })
    }

    $('.addtodoItem').off().click(function() {
        listObj.addItem($(this).data("name"));
    });

    $('.edittodoItem').off().click(function() {
        listObj.editItem($(this).data("name"), parseInt($(this).data("idpos")));
    });

    $('.deletetodoItem').off().click(function() {
        listObj.deleteItem($(this).data("name"), parseInt($(this).data("idpos")));
    });

    $('.tododialog #modif-button').on('click', function(e){
        e.stopImmediatePropagation();

        var idpos = $(this).data('idpos');
        var name = $(this).data('name');
        var worker = [];

        if ( $('.tododialog .taskname').val() )
            tasks[name][idpos].task = $('.tododialog .taskname').val();

        if ( $('.tododialog .credit').val() )
            tasks[name][idpos].credits = $('.tododialog .credit').val();

        if ( $('.tododialog .duedate').val() ){
            var formattedDatetodo =  $('.tododialog .duedate').val().split("-");
            var ytodo = formattedDatetodo[0];
            var mtodo  =  formattedDatetodo[1];
            var dtodo  = formattedDatetodo[2];

            tasks[name][idpos].endDate = dtodo +'/'+mtodo +'/'+ytodo;
        }

        if ( $('.tododialog #new-worker-value').val() ){

            tasks[name][idpos].contributors = {};
            if ($('.tododialog #new-worker-value').val() ){

                worker = $('.tododialog #new-worker-value').val();
                $.each(worker, function(workerid, workerusername){
                    $.each(member, function(memberid, membervalue){
                        if (membervalue["username"] == workerusername) {
                            tasks[name][idpos].contributors[memberid] = {
                                "type" : membervalue.collection,
                            }
                        }
                    })
                })
            }
        }

        //post edit
        $('.tododialog .taskname').val("");
        $('.tododialog .credit').val("");
        $('.tododialog .duedate').val("");
        $('.selectMultiple > div a').each(function(){
            $(this).trigger('click');
        });

        $('.tododialog #modif-button').hide();
        $('.tododialog #add-button').show();

        listObj.buildList(name);
    });

    $('.tododialog .checkicon').on('click', function(e){
        listObj.tooglecheckItem($(this).data("name"), parseInt($(this).data("idpos")));

        $(this).toggleClass("fa-check-circle");
        $(this).toggleClass("fa-circle-o");
    });
},

addItem : function (name) {

    if ( $('.tododialog .taskname').val() &&  $('.tododialog .taskname').val() != "" && $('.tododialog .taskname').val() != undefined) {
        var today = new Date();
        today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();

        var worker = [];

        var todo = {
            createdAt : today,
            userId : userId,
            checked : "false",
        };

        if ( $('.tododialog .taskname').val() )
            todo.task = $('.tododialog .taskname').val();

        if ( $('.tododialog .credit').val() )
            todo.credits = $('.tododialog .credit').val();

        if ( $('.tododialog .duedate').val() ){
            todo.endDate = $('.tododialog .duedate').val();
        }

        if ( $('.tododialog .duedate').val() ){
            var formattedDatetodo =  $('.tododialog .duedate').val().split("-");
            var ytodo = formattedDatetodo[0];
            var mtodo  =  formattedDatetodo[1];
            var dtodo  = formattedDatetodo[2];
            todo.endDate = dtodo +'/'+mtodo +'/'+ytodo;
        }

        if ( $('.tododialog #new-worker-value').val() ){

            todo.contributors = {};
            if ($('.tododialog #new-worker-value').val() ){

                worker = $('.tododialog #new-worker-value').val();
                $.each(worker, function(workerid, workerusername){
                    $.each(member, function(memberid, membervalue){
                        if (membervalue["username"] == workerusername) {
                            todo.contributors[memberid] = {
                                "type" : membervalue.collection
                            }
                        }
                    })
                })
            }
        }

        mylog.log("addItem",todo, tplCtx.editTaskpos);

        if(todo.task =='') {
            alert("Please write what you need to do!");
        } else {
            var str = '<li class="" data-pos="'+tasks[name].length+'">'+
                    '<div class="todo-content"><i class="fa fa-circle-o checkicon"></i>'+
                                '<div class="td-title">'+
                                    '<h5 class="mb-0">'+todo.task+'</h5>'+
                                    '<div class="td-datetime">'+

                                        '<div><i class="fa fa-money"></i><span class="td-datetimelabel">'+todo.credits+'</span></div>'+
                                        '<div class="td-datetimelabel2"><i class="fa fa-calendar-o"></i><span class="td-datetimelabel">'+todo.endDate+'</span></div>'+
                                        '<div class="td-datetimelabel2"><i class="fa fa-user"></i><span class="td-datetimelabel">'+worker+'</span></div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="todo-btn">'+
                                '<button class="td-datetimelabel2 edittodoItem actionbtntodoedit" data-name="'+name+'" data-idpos="'+tasks[name].length+'" ><i class="fa fa-pencil"></i></button>'+
                                '<button class="td-datetimelabel2 deletetodoItem actionbtntododelete" data-name="'+name+'" data-idpos="'+tasks[name].length+'" ><i class="fa fa-trash"></i></button>'+
                                '</div>'+
                        '</li>';

            $('.tododialog #todos-list').prepend(str);
        }

          // listObj.buildList(todo, tplCtx.editTaskpos);
          // listObj.btnInit();

          // if( notNull(tplCtx.form) && notNull(formInputs [tplCtx.form]) ){
            if( typeof tasks[name] == "undefined")
                tasks[name] = [];
            tasks[name].push(todo);
        // }
        // else if( notNull(tplCtx.budgetpath) ) {
        // 	if( typeof answerObj.answers[tplCtx.budgetpath][tplCtx.pos].todo == "undefined")
        // 		answerObj.answers[tplCtx.form][tplCtx.budgetpath][tplCtx.pos].todo = [];
        //   	answerObj.answers[tplCtx.budgetpath][tplCtx.pos].todo.push(todo);
        // }

        $('.tododialog .taskname', '.tododialog .credit', '.tododialog .duedate').val("");

         // $(".bootbox #new-todo-item, .bootbox #new-todo-item-date, .bootbox #new-todo-item-price").val("");
     }
    $('.selectMultiple todoinput').removeClass("open");
},

deleteItem : function (name, pos) {
    bootbox.dialog({
              title: trad.confirmdelete,
              message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
              buttons: [
                {
                  label: "Ok",
                  className: "btn btn-primary pull-left",
                  callback: function() {
                      $('li[data-pos="'+pos+'"]').fadeOut('slow', function() {
                        tasks[name].splice(pos, 1);
                        listObj.buildList(name);
                    });
                  }
                },
                {
                  label: "Annuler",
                  className: "btn btn-default pull-left",
                  callback: function() {}
                }
              ]
    });
},

editItem : function (name, idpos) {

    if ( typeof tasks[name][idpos].task !== "undefined" )
        $('.tododialog .taskname').val(tasks[name][idpos].task);

    if ( typeof tasks[name][idpos].credits !== "undefined" ){
        $('.tododialog .credit').val(tasks[name][idpos].credits);
    }else {
        $('.tododialog .credit').val("");
    }

    if ( typeof tasks[name][idpos].endDate !== "undefined" ){
        var formattedDatetodo =  tasks[name][idpos].endDate.split("/");
        var dtodo = formattedDatetodo[0];
        var mtodo  =  formattedDatetodo[1];
        var ytodo  = formattedDatetodo[2];
        $('.tododialog .duedate').val(ytodo +'-'+mtodo +'-'+dtodo);
    }else{
        $('.tododialog .duedate').val("");
    }


    $('.selectMultiple > div a').each(function(){
        $(this).trigger('click');
    });

    if ( typeof tasks[name][idpos].contributors !== "undefined" ){
        var worker = [];

        $.each(tasks[name][idpos].contributors, function(cbId, cbValue){
            if (typeof member[cbId] !== "undefined") {
                $('.selectMultiple ul li:contains('+member[cbId].name+')').trigger("click");
            }
        })

        // $('.tododialog #new-worker-value').val(worker);
    }else{
        $('.tododialog #new-worker-value').val([]);
    }

    $('.tododialog #modif-button').data("idpos", idpos);
    $('.tododialog #modif-button').data("name", name);

    $('.tododialog #modif-button').show();
    $('.tododialog #add-button').hide();

},

tooglecheckItem : function (name, idpos) {

    if ( typeof tasks[name][idpos].checked !== "undefined" && tasks[name][idpos].checked == "true" ){
        tasks[name][idpos].checked = "false";

    } else {
        tasks[name][idpos].checked = "true";
        tasks[name][idpos].checkedUserId = userId;
    }

},

// editItem : function (e, item) {
//   e.preventDefault();
// 	tplCtx.editTaskpos = $(item).parent().parent().data("pos");
//   $(".bootbox #new-todo-item").val( $(item).parent().parent().find(".liText").text() );
//   $(".bootbox #new-todo-item-date").val( $(item).parent().parent().data("when") );
//   $(".bootbox #new-todo-item-price").val( $(item).parent().parent().data("price") );
//   $(".bootbox #new-todo-item-who option").prop("selected",false)
//   if($(item).parent().parent().data("who").indexOf(",")){
//   	whosT = $(item).parent().parent().data("who").split(",");
//   	$.each(whosT,function(i,u){
//   		$(".bootbox #new-todo-item-who option[value='"+u+"']").prop("selected",true)
//   	})
//   }else
// 	  $(".bootbox #new-todo-item-who").val( $(item).parent().parent().data("who") );
// },

btnInit : function() {
    // $(".bootbox #todo-list").on('click', '.todo-item-delete', function(e){
    // 	var item = this;
    // 	listObj.deleteItem(e, item)
    // })
    // $(".bootbox #todo-list").on('click', '.todo-item-edit', function(e){
    // 	var item = this;
    // 	listObj.editItem(e, item)
    // })
    // $(".bootbox .todo-item-done").off().on('click', listObj.completeItem);
    // // $(".bootbox .liTodo").on('click', function(e){
    // // 	var item = this;
    // // 	editItem(e, item)
    // // })
}
}

</script>
<?php } ?>
<?php } else {
//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>
