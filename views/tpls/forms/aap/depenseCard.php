<style>
    :root {
        --pfpb-primary-green-color: #9fbd38 ;
        --pfpb-primary-grey-color: #c1c3c1;
        --pfpb-secondary-grey-color: #e6e1e1;
        --pfpb-tertiary-grey-color: #e4e4e4;
        --pfpb-primary-blue-color: #2271dd;
        --pfpb-secondary-blue-color: #0096D1;
        --pfpb-tertiary-blue-color: #3EBDC6;
        --pfpb-primary-red-color: rgb(238,122,122);
        --pfpb-secondary-red-color: #f1e2e2;
        --pfpb-primary-purple-color: #a946c0;
        --pfpb-secondary-purple-color: #ffd4fe;
    }

    .one-depense-card .newDepenseListCard {
        background-color: rgb(255, 255, 255);
        position: relative;
        font-family: "montserrat";
        padding: 10px 10px;
        margin: 10px;
        border-radius: 12px;
        max-width: 90%;
        box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 3px, rgba(0, 0, 0, 0.24) 0px 1px 2px;
        z-index: 99;
    }
    .one-depense-card .newDL-entete {
        display: inline-flex;
        width: 100%;
    }
    .one-depense-card .newDL-entete-image {
        height: 13rem;
        width: 13rem;
    }
    .one-depense-card .newDL-entete-image img {
        height: 12rem;
        border-radius: 8px;
        width: 12rem;
    }
    .one-depense-card .newDL-entete-depense {
        position: relative;
        display: inline-flex;
        width: -webkit-fill-available;
    }
    .one-depense-card .newDL-entete-percentage {
        display: flex;
        flex-direction: vertical;
        flex-wrap: nowrap;
        width: 70px;
        text-align: left;
        height: 50px;
        position: relative;
        letter-spacing: -5px;
    }
    .one-depense-card .newDL-entete-percentage .newDL-entete-perc {
        height: 50px;
        padding: 0;
        color: var(--pfpb-primary-blue-color);
        font-size: 30px;
        position: absolute;
        width: 50px;
        text-align: right;
        top: 15px;
        right: 20px;
    }
    .one-depense-card .newDL-entete-percentage .newDL-entete-perc-sign {
        font-size: 15px !important;
        padding: 0;
        color: var(--pfpb-primary-green-color);
        width: 50px;
        text-align: right;
        position: absolute;
        height: 50px;
        top: 30px;
        right: 10px;
        font-weight: bold;
        stroke: 10px;
        z-index: 20;
    }
    .one-depense-card .newDL-entete-date-sp:after {
        content: '';
        background: transparent;
        border: 2px solid var(--pfpb-tertiary-grey-color);
        border-radius: 20px;
        height: 50px;
        position: absolute;
    }
    .one-depense-card .newDL-entete-date-sp-financ-container {
        height: 50px;
        position: relative;
    }
    .one-depense-card .newDL-entete-date-sp-financ {
        content: '';
        background: transparent;
        border: 2px solid var(--pfpb-secondary-blue-color);
        border-radius: 20px;
        position: absolute;
        z-index: 8;
        bottom: 0;
    }
    .one-depense-card .newDL-entete-info {
        min-height: 50px;
        width: 100%;
        padding-left: 12px;
    }
    .one-depense-card .newDL-entete-info .newDL-entete-titre {
        color: var(--pfpb-primary-green-color);
        font-size: 16px;
        height: 30px;
        overflow: hidden;
        display: -webkit-box;
        -webkit-line-clamp: 1;
        line-clamp: 1;
        -webkit-box-orient: vertical;
        padding: 3px 0px;
        font-weight: bold;
    }
    .one-depense-card .newDL-entete-price {
        width: -webkit-fill-available;
        color: var(--pfpb-secondary-blue-color);
    }
    .one-depense-card .ftlcase, .newDL-entete-price .entete-financement {
        color: var(--pfpb-primary-grey-color);
    }
    .one-depense-card .newDL-entete-price .entete-financement {
        font-weight: bold;
        width: -webkit-fill-available;
        color: var(--pfpb-primary-blue-color);
    }
    .one-depense-card .entete-part{
        display: none;
    }

    .one-depense-card .entete-part.active{
        display: block;
    }
</style>
<div class="col-xs-12 one-depense-card">

</div>
<script type="text/javascript">
    const thisPhp = {
        params : JSON.parse(JSON.stringify(<?= json_encode($params) ?>)),
        depenselistobj : {
            data : {
                depenseSubType : 'finance'
            },
            action : {
                addSpace(number){
                    if(notEmpty(number)) {
                        number = number.toString();
                        var pattern = /(-?\d+)(\d{3})/;
                        while (pattern.test(number))
                            number = number.replace(pattern, "$1 $2");
                    }else {
                        number = 0;
                    }
                    return number+'€';
                },
                getDepenseExtraData : function (depenses) {
                    var returndep = {};
                    if (notEmpty(depenses) && depenses != null) {
                        returndep = depenses.map(function (depense,index){
                            if (notEmpty(depense) && depense != null) {
                                var lvl1 = {
                                    uid: index,
                                    poste: notEmpty(depense.poste) ? depense.poste : "",
                                    imageKey: depense.imageKey ? depense.imageKey : "",
                                    price: depense.price,
                                    date: depense.date,
                                    description: depense.description,
                                    user: depense.user,
                                    financer: depense.financer ? depense.financer : [],
                                    financementCount: depense.financer ? depense.financer.length : 0,
                                    financement: depense.financer ? depense.financer.reduce((n, {amount}) => n + parseInt(amount), 0) : 0,
                                    reste: depense.price - (depense.financer ? depense.financer.reduce((n, {amount}) => n + parseInt(amount), 0) : 0),
                                    part: 0
                                }

                                lvl1.financers = {};
                                if (notEmpty(depense.financer)) {
                                    $.each(depense.financer, function (id, fin) {
                                        if (lvl1.financers[fin.name]) {
                                            lvl1.financers[fin.name] += parseInt(fin.amount)
                                        } else {
                                            lvl1.financers[fin.name] = parseInt(fin.amount)
                                        }
                                    });
                                }

                                lvl1.percentage = 0;
                                if (lvl1.price != 0 && lvl1.financement != 0) {
                                    lvl1.percentage = Math.round((lvl1.financement / lvl1.price) * 100);
                                }
                                return lvl1;
                            }
                        });
                    }
                    return returndep;
                }
            }
        }
    }
    if (thisPhp.params?.answer?.answers?.aapStep1?.depense) {
        thisPhp.params.depense = thisPhp.depenselistobj.action.getDepenseExtraData(thisPhp.params.answer.answers.aapStep1.depense)[thisPhp.params.depenseId];
    }

    const depenseCard = {
        getDepensePanel : function(depenselistobj, index,depense){
            var html = '';
            mylog.log("check call", thisPhp)
            if(notEmpty(depense)){
                html += `
                    <div class="newDepenseListCard" data-subtype="${depenselistobj.data.depenseSubType}" data-uid="${index}" >
                        <div class="newDL-entete" >
                            <div class="newDL-entete-image" >
                                    <img class="" src="${ (notEmpty(depense.imageKey) && notEmpty(thisPhp.params?.depenseImg?.[depense.imageKey])) ? thisPhp.params?.depenseImg?.[depense.imageKey].imagePath : modules.co2.url +'/images/thumbnail-default.jpg'}" >
                            </div>
                            <div class="newDL-entete-data bs-ml-2" >
                                <div class="newDL-entete-depense" >
                                    <div class="newDL-entete-percentage ${depenselistobj.data.depenseSubType === 'depense' ? 'hide' : ''}" >
                                        <div class="newDL-entete-perc" >
                                            ${depense.percentage}
                                        </div>
                                        <div class="newDL-entete-perc-sign" >
                                            %
                                        </div>
                                    </div>
                                    <div class="newDL-entete-date-sp" data-subtype="${depenselistobj.data.depenseSubType}">
                                    </div>
                                    <div class="newDL-entete-date-sp-financ-container ${depenselistobj.data.depenseSubType === 'depense' ? 'hide' : ''}" data-subtype="${depenselistobj.data.depenseSubType}">
                                        <div style="height: ${depense.percentage}%" class="newDL-entete-date-sp-financ ${depenselistobj.data.depenseSubType === 'depense' ? 'hide' : ''}" data-subtype="${depenselistobj.data.depenseSubType}">
                                        </div>
                                    </div>
                                    <div class="newDL-entete-info" >
                                        <div class="newDL-entete-titre"  >
                                            ${depense.poste}
                                        </div>
                                        <div class="newDL-entete-price" >
                                            <span class="entete-financement"><i class="fa fa-money"></i> ${depenselistobj.action.addSpace(depense.financement)} <span class="entete-part"><i class="fa fa-plus"></i> ${depenselistobj.action.addSpace(depense.part)} </span>  / </span> ${depenselistobj.action.addSpace(depense.price)}
                                        </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                `;
            }
            return html;
        }
    }
    $(function () {
        $(".one-depense-card").html(depenseCard.getDepensePanel(thisPhp.depenselistobj, thisPhp.params.depenseId, thisPhp.params.depense));
    });
</script>