<style>
    .wrapper{
    display: inline-flex;
    background: #fff;
    height: 100px;
    width: 400px;
    align-items: center;
    justify-content: space-evenly;
    border-radius: 5px;
    padding: 20px 15px;
    box-shadow: 5px 5px 30px rgba(0,0,0,0.2);
    }
    .wrapper .option{
    background: #fff;
    height: 100%;
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: space-evenly;
    margin: 0 10px;
    border-radius: 5px;
    cursor: pointer;
    padding: 0 10px;
    border: 2px solid lightgrey;
    transition: all 0.3s ease;
    }
    .wrapper .option .dot{
    height: 20px;
    width: 20px;
    background: #d9d9d9;
    border-radius: 50%;
    position: relative;
    }
    .wrapper .option .dot::before{
    position: absolute;
    content: "";
    top: 4px;
    left: 4px;
    width: 12px;
    height: 12px;
    border-radius: 50%;
    opacity: 0;
    transform: scale(1.5);
    transition: all 0.3s ease;
    }
    input[type="radio"]{
    display: none;
    }
    #option-1:checked:checked ~ .option-1{
        border-color: #5cb85c;
        background: #5cb85c;
    }
    #option-2:checked:checked ~ .option-2{
        border-color: #cd1616;
        background: #cd1616;
    }
    .wrapper .option-1 .dot::before{
        background: #5cb85c;
    }
    .wrapper .option-2 .dot::before{
        background: #cd1616;
    }
    #option-1:checked:checked ~ .option-1 .dot,
    #option-2:checked:checked ~ .option-2 .dot{
    background: #fff;
    }
    #option-1:checked:checked ~ .option-1 .dot::before,
    #option-2:checked:checked ~ .option-2 .dot::before{
    opacity: 1;
    transform: scale(1);
    }
    .wrapper .option span{
    font-size: 20px;
    color: #808080;
    }
    #option-1:checked:checked ~ .option-1 span,
    #option-2:checked:checked ~ .option-2 span{
    color: #fff;
    }
</style>
<?php
    HtmlHelper::registerCssAndScriptsFiles(['/plugins/rater/rater-js.js'], Yii::app()->request->baseUrl);
    $elId = array_keys($parentForm["parent"])[0];
    $el = PHDB::findOneById($parentForm["parent"][$elId]["type"],$elId,array("slug"));
    $inputs = PHDB::findOne(Form::INPUTS_COLLECTION,array("formParent" => $answer["form"],"step" => "aapStep1"));

    $predefinedCriteria = isset($parentForm["params"]["configSelectionCriteria"]) ? $parentForm["params"]["configSelectionCriteria"] : array();
    $voteType = "starCriterionBased"; if(!empty($predefinedCriteria["type"])) $voteType = $predefinedCriteria["type"];
    $noteMax = !empty($predefinedCriteria["noteMax"]) ? $predefinedCriteria["noteMax"] : 10;
    if($voteType == "starCriterionBased") $noteMax = 5;
    $parsedPredefinedCriteria = [];

    /**for default criteria ************************************/
    if(!empty($predefinedCriteria["criterions"])){
        foreach ($predefinedCriteria["criterions"] as $kcrit => $vcrit) {
            $coeff = is_int($vcrit["coeff"]) ? $vcrit["coeff"] : 1;
            $predefinedCriteria["criterions"][$kcrit]["coeff"] = $coeff;
            if(isset($vcrit["fieldKey"]))
                $parsedPredefinedCriteria[$vcrit["fieldKey"]] = array(
                    "coeff" => $coeff,
                    "fieldLabel" => @$vcrit["fieldLabel"]);
        }
    }

    $acceptedPreparedData = [];

    $allPreparedData = [];
    if(isset($inputs["inputs"]) && is_array($inputs["inputs"]))
        $allPreparedData = $inputs["inputs"];



    foreach($parsedPredefinedCriteria as $kPcrit => $vPcrit){
        $valueAnswer = !empty($answer["answers"]["aapStep1"][$kPcrit]) ? $answer["answers"]["aapStep1"][$kPcrit] : "";
        if($kPcrit == "depense" || $kPcrit == "budget"){
            $price = 0;
            foreach ($valueAnswer as $kdepense => $vdepense) {
                if(!empty($vdepense["price"]))
                    $price += (float) $vdepense["price"];
            }
            $valueAnswer = $price;
        }
        $acceptedPreparedData[$kPcrit] = [
            "label" => $inputs["inputs"][$kPcrit]["label"],
            "fieldLabel" => @$vPcrit["fieldLabel"],
            "value" => $valueAnswer,
            "coeff" => is_int($vPcrit["coeff"]) ? $vPcrit["coeff"] : 1,

        ];
    }

    /**for personalized criteria ************************************/
    if(!empty($predefinedCriteria["unassociatedCriterions"])){
        foreach ($predefinedCriteria["unassociatedCriterions"] as $key => $value) {
            $coeff = is_int($value["coeff"]) ? $value["coeff"] : 1;
            $predefinedCriteria["unassociatedCriterions"][$key]["coeff"] = $coeff;
            if(isset($value["fieldKey"]))
                $acceptedPreparedData[$value["fieldKey"]] = [
                    "fieldLabel" => @$value["fieldLabel"],
                    "value" => /*'Critère libre'*/'',
                    "coeff" => $coeff,
                ];
        }
    }

    $showAdmissibility = true;
    if(isset($predefinedCriteria["admissibility"]) && !$predefinedCriteria["admissibility"])
        $showAdmissibility = false;
    else
        $predefinedCriteria["admissibility"] = true;

?>
<div class="row">
    <div class="col-xs-12">
        <div class="col-xs-12 padding-bottom-30">
        <?php if($mode == "fa"){ ?>
            <button type="button" class="btn btn-lg bg-green tooltips hover-white configuration<?= $kunik ?>" data-toggle="tooltip" data-placement="bottom" data-original-title="Configurer"><i class="fa fa-cog"></i></button>
        <?php } ?>
            <button type="button" class="btn btn-lg tooltips hover-white votant<?= $kunik ?>" data-toggle="tooltip" data-placement="bottom" data-original-title="Liste d'évaluateurs">
                <i class="fa fa-users"></i> <?= Yii::t('common', "See the list of evaluators and their opinions")?>
            </button>
        </div>
        <table class="table table-striped">
            <thead>
                <th><?= Yii::t('common', "Criteria")?> <span class="label label-success pull-right">Coeff</span></th>
                <th><?= Yii::t('common', "Value")?></th>
                <th><?= Yii::t('common', 'Score out of') ?> <?= $noteMax ?></th>
            </thead>
            <tbody>
                <?php foreach ($acceptedPreparedData as $kprep => $vprep) {
                    $currentNote =  isset($answer["answers"][$form['id']]["selection"][Yii::app()->session['userId']][$kprep]) ? $answer["answers"][$form['id']]["selection"][Yii::app()->session['userId']][$kprep] : "";
                    $showValues = "";
                    if(!(array_keys($vprep["value"]) !== range(0, count($vprep["value"]) - 1)) && is_array($vprep["value"])){ //sequantial array
                        foreach ($vprep["value"] as $kk => $vv) {
                            $showValues .= "- ".$vv."<br>";
                        }
                    }elseif(is_string($vprep["value"]) || is_int($vprep["value"]) || is_float($vprep["value"])){
                        $showValues = $vprep["value"];
                    }

                    //specific auto evaluation
                    if(@$this->costum["contextSlug"]=="coSindniSmarterre"){
                        if(
                            ($kprep =="axesTFPB" && is_string($showValues) && strpos(strtolower($showValues),"axe 5") !== false) ||
                            ($kprep =="aapStep1l1xfdtuazek4jhgcco" && is_string($showValues) && strpos(strtolower($showValues),strtolower("Aucune de ces spécificités")) === false && $showValues !="")
                            ){
                            $currentNote = 5;
                            PHDB::update(Form::ANSWER_COLLECTION,
                                        array("_id" => new MongoId((string)$answer["_id"])),
                                        array('$set'=>["answers.".$form['id'].".selection.".Yii::app()->session['userId'].".".$kprep => $currentNote])
                            );
                        }
                    }
                ?>
                    <tr>
                        <td><b><?= !empty(@$vprep["fieldLabel"]) ?@$vprep["fieldLabel"] : @$vprep["label"] ?></b> <span class="label label-success pull-right">(<?= @$vprep["coeff"] ?>)</span></td>
                        <td><?= $showValues ?></td>

                        <!-- note -->
                        <?php if($voteType == "noteCriterionBased"){ ?>
                            <td <?= !empty($mode) && in_array($mode, ['w', 'fa']) ? 'contenteditable="true"' : '' ?> class="<?= $kunik ?>"
                                data-id="<?= (string) $answer["_id"] ?>"
                                data-collection="<?= Form::ANSWER_COLLECTION ?>"
                                data-path="answers.<?= $form['id']?>.selection.<?=Yii::app()->session['userId']?>.<?= $kprep ?>"
                            >
                                <b>
                                    <?= $currentNote  ?>
                                </b>
                            </td>
                        <?php } ?>

                        <!-- star -->
                        <?php if($voteType == "starCriterionBased"){ ?>
                            <td>
                                <div class="rater-evaluator<?= $kunik ?>"
                                    data-rating='<?= $currentNote?>'
                                    data-id="<?= (string) $answer["_id"] ?>"
                                    data-collection="<?= Form::ANSWER_COLLECTION ?>"
                                    data-path="answers.<?= $form['id']?>.selection.<?=Yii::app()->session['userId']?>.<?= $kprep ?>">
                                </div>
                            </td>
                        <?php } ?>
                    </tr>
                <?php } ?>
                <tr class="">
                    <td>&nbsp;</td>
                    <td class="h4 bold"> <?= Yii::t("common","My vote") ?> : </td>
                    <td class="mean<?= $kunik ?> h4 bold"></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="h4 bold"><?= Yii::t("common","Tous les votants") ?> :</td>
                    <td class="allMean<?= $kunik ?> h4 bold"></td>
                </tr>
            </tbody>
        </table>
    </div>
    <?php if($showAdmissibility && $mode == "w"){ ?>
    <div class="col-xs-12 text-center margin-top-25">
        <div class="col-xs-12">
            <h4>Admissibilité</h4>
        </div>
        <div class="wrapper">
            <?php
                $checkedAdmissible = "";
                $checkedIndmissible="";
                if(isset($answer["answers"][$form["id"]]["admissibility"][Yii::app()->session['userId']]) && $answer["answers"][$form["id"]]["admissibility"][Yii::app()->session['userId']]=="admissible")
                    $checkedAdmissible = "checked";
                else
                    $checkedIndmissible = "checked";
            ?>
            <input type="radio" name="select" value="admissible" id="option-1" class="admissibility<?= $kunik ?> exclude-input" <?= $checkedAdmissible ?> >
            <input type="radio" name="select" value="inadmissible" id="option-2" class="admissibility<?= $kunik ?> exclude-input" <?= $checkedIndmissible ?> >
            <label for="option-1" class="option option-1">
                <div class="dot"></div>
                <span>OUI</span>
                </label>
            <label for="option-2" class="option option-2">
                <div class="dot"></div>
                <span>NON</span>
            </label>
        </div>
    </div>
    <?php } ?>
</div>
<script>
    $(function(){
        var options<?= $kunik ?> = <?= isset($allPreparedData) ? json_encode($allPreparedData) : "{}" ?>;
        var sumCoeff = 0;
        var keyCoeff = {};
        var configOptions<?= $kunik ?> = {};
        var noteMax = <?= $noteMax ?>;
        var showAdmissibility = <?= json_encode($showAdmissibility)?>;

        $.each(options<?= $kunik ?>,function(k,v){
            configOptions<?= $kunik ?>[k] = v.label;
        })

        $('.<?= $kunik ?>').off().on('blur',function(){
            var field = $(this);
            if(field.text() > noteMax || field.text() < 0 ) return toastr.error("Donne une note entre 0 à "+noteMax+". Merci");
            var tplCtx = {
                id : field.data('id'),
                collection : field.data('collection'),
                path : field.data('path'),
                value : field.text().trim(),
                setType : "float"
            }
            dataHelper.path2Value(tplCtx, function(params) {
                if(params.result){
                    if(typeof newReloadStepValidationInputGlobal != "undefined") {
                        newReloadStepValidationInputGlobal({
                            inputKey : <?php echo json_encode($key) ?>,
                            inputType : "tpls.forms.aap.selection"
                        })
                    }
                    toastr.success("Ok");
                    mean<?= $kunik ?>()
                }
            } );
        })

        var mode = JSON.parse(JSON.stringify(<?= json_encode($mode ?? 'r') ?>));
        $('.rater-evaluator<?= $kunik ?>').each(function(i, obj) {
            var opts = {
                rating : $(obj).data("rating"),
                starSize:20,
                step:0.5,
                readOnly: !['fa', 'w'].includes(mode),
                element:obj,
                rateCallback : function rateCallback(rating, done) {
                    var $this = this;
                    this.setRating(rating);
                    var tplCtx ={
                        id : $(obj).data("id"),
                        collection : $(obj).data("collection"),
                        path : $(obj).data("path"),
                        value : rating,
                        setType : "float"
                    }
                    dataHelper.path2Value(tplCtx, function (params) {
                        if(params.result){
                            if(typeof newReloadStepValidationInputGlobal != "undefined") {
                                newReloadStepValidationInputGlobal({
                                    inputKey : <?php echo json_encode($key) ?>,
                                    inputType : "tpls.forms.aap.selection"
                                })
                            }
                            ajaxPost("", baseUrl + "/co2/aap/commonaap/action/notifyAddEvaluation", {answerId:tplCtx.id}, function(data){
                                
                            }, function(error){
                                mylog.log("ajaxPost error", error)
                            }, "json")
                            toastr.success("Ok");
                            mean<?= $kunik ?>();
                        }
                    });
                    done();
                }
            };
            if($(obj).data("status")=="readonly"){
                opts["readOnly"] = true;
                delete opts.rateCallback;
            }
            var starRating = raterJs(opts);
        });

        $('.configuration<?= $kunik ?>').on('click',()=>{
            var existEvaluationCriteria = <?= isset($parentForm["params"]["configSelectionCriteria"]) ? json_encode(@$parentForm["params"]["configSelectionCriteria"]) : "{}" ?>;
            var newEvaluationCriteria = {
                jsonSchema :{
                    title : "Configurer le champs évaluation",
                    description : "",
                    properties : {
                        /*"whoCanEvaluate" : {
                            inputType: "tags",
                            label : "Ajouter des rôles d'évaluateurs",
                            info:  "<small>Seuls ceux qui ont les rôles mentionnés ici et les admin peuvent évaluer.(ex: Evaluateur,Financeur,...)</small>",
                            options:{
                                "evaluateur" : "Evaluateur",
                                "financeur" : "Financeur",
                            }
                        },*/
                        "type" :{
                            inputType: "select",
                            label : "Type d'évaluation",
                            options:{
                                "noteCriterionBased" : "Évaluation par note",
                                "starCriterionBased" : "Évaluation par étoile",
                                //"forOrAgainst" : "Pour ou contre"
                            }
                        },
                        "noteMax":{
                            inputType: "select",
                            label : "<?= Yii::t('common', 'Score out of') ?>",
                            options:{
                                "5" : "5",
                                "10" : "10",
                                "20" : "20",
                            }
                        },
                        "criterions" :{
                            inputType: "lists",
                            label: "Critères d'évaluation associés à la réponse",
                            "entries":{
                                "fieldKey":{
                                    "type":"select",
                                    "label" : "Nom du critère {num}",
                                    "class":"col-md-5 col-sm-5 col-xs-10",
                                    options : configOptions<?= $kunik ?>
                                },
                                "fieldLabel":{
                                    "type":"text",
                                    "label" : "Renommer {num} (optionnel)",
                                    "class":"col-md-4 col-sm-4 col-xs-10",
                                },
                                "coeff":{
                                    "label":"Coeff",
                                    "type":"text",
                                    "value":"1",
                                    "class":"col-md-2 col-sm-2 col-xs-10"
                                }
                            }
                        },
                        "unassociatedCriterions" :{
                            inputType: "lists",
                            label: "Critères d'évaluation libre",
                            "entries":{
                                "fieldKey":{
                                    "type":"hidden",
                                    "label" : "",
                                    "class":"",
                                },
                                "fieldLabel":{
                                    "type":"text",
                                    "label" : "Nom du critère {num}",
                                    "class":"col-md-9 col-sm-9 col-xs-10",
                                },
                                "coeff":{
                                    "label":"Coeff",
                                    "type":"text",
                                    "class":"col-md-2 col-sm-2 col-xs-10"
                                }
                            }
                        },
                        "admissibility" : {
                            label: "Afficher le champs admissibilité",
							inputType: "checkboxSimple",
							params: checkboxSimpleParams,
                        }
                    },
                    onLoads:{
                        onload : function(){
                            var childLength = $("#listContainerunassociatedCriterions").children().length;
                            var syncArr = function(childLength){
                                setTimeout(() => {
                                    for(let i=0; i<=childLength; i++){
                                        $("[name='unassociatedCriterionsfieldLabel"+i+"']").keyup(function(){
                                            console.log("mandeha"+i)
                                            var d = new Date();
                                            var val = $(this).val()+Math.floor( d / 1000 );
                                            $("[name='unassociatedCriterionsfieldKey"+i+"']").val("fieldKey"+i);
                                        })
                                    }
                                }, 700);
                            }
                            syncArr(childLength)
                            $(".addListLineBtnunassociatedCriterions")[0].addEventListener('click',function(){
                                childLength++;
                                syncArr(childLength);
                            })
                            $('#listContainerunassociatedCriterions .removeListLineBtn')[0].addEventListener('click',function(){
                                childLength--;
                                syncArr(childLength);
                            })



                           /* if($('#activateLocalCriteria').val()=="true")
                                $('.whoCanEvaluatetags,.typeselect,.criterionslists').show();
                            else
                                $('.whoCanEvaluatetags,.typeselect,.criterionslists').hide();

                            $('.activateLocalCriteriacheckboxSimple .btn-dyn-checkbox').on('click',function(){
                                if($('#activateLocalCriteria').val()=="true")
                                    $('.whoCanEvaluatetags,.typeselect,.criterionslists').fadeIn(1000);
                                else
                                    $('.whoCanEvaluatetags,.typeselect,.criterionslists').fadeOut(800);
                            })*/
                        }
                    },
                    save : function(formData){
                        var tplCtx = {
                            id : "<?= (string)$parentForm["_id"] ?>",
                            collection: "forms",
                            value: {},
                            path: "params.configSelectionCriteria",
                            setType:[
                                {
                                    "path": "noteMax",
                                    "type": "int"
                                }
                            ]
                        };
                        $.each(newEvaluationCriteria.jsonSchema.properties,function(k,v){
                            if(v.inputType == "lists"){
                                tplCtx.value[k] = formData[k];
                                $.each(tplCtx.value[k],function(i,j){
                                    tplCtx.setType.push({
                                        "path": k+"."+i+".coeff",
                                        "type": "int"
                                    })
                                });
                            }
                            else
                                tplCtx.value[k] = $("#" + k).val();

                        });
                        if (typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value(tplCtx, function (params) {
                                urlCtrl.loadByHash(location.hash);
                            });
                        }
                    }
                }
            }
            dyFObj.openForm(newEvaluationCriteria,null,existEvaluationCriteria);
        })
        $(".votant<?= $kunik ?>").on('click',function(){
            ajaxPost('',baseUrl+"/co2/element/get/type/answers/id/<?= (string) $answer["_id"]  ?>",
            null,
            function(data){
                data=data.map;
                if(exists(data.answers) && exists(data.answers.<?= $form['id']?>) && exists(data.answers.<?= $form['id']?>.selection)){
                    var votants = [];
                    $.each(data.answers.<?= $form['id']?>.selection,function(k,v){
                        votants.push(k);
                    })
                    ajaxPost('',baseUrl+"/co2/element/get/type/citoyens",
                        {
                            id : votants,
                            fields:["name","profilMediumImageUrl"]
                        },
                        function(dataVotans){
                            dataVotans=dataVotans.map;
                            mylog.log(dataVotans,"dataVotans");
                            mean<?= $kunik ?>();
                            countAdmissible = 0;
                            countInadmissible=0;
                            $.each(dataVotans,function(kv,vv){

                                if(exists(data.answers) && exists(data.answers.<?= $form['id']?>) && exists(data.answers.<?= $form['id']?>.selection) && exists(data.answers.<?= $form['id']?>.selection[kv])){
                                    dataVotans[kv]["selection"] = data.answers.<?= $form['id']?>.selection[kv];
                                    dataVotans[kv]["note"] = 0;
                                    $.each(dataVotans[kv]["selection"],function(ksel,vsel){
                                        if(keyCoeff[ksel])
                                            dataVotans[kv]["note"] += (vsel*keyCoeff[ksel])
                                    })
                                    dataVotans[kv]["note"] = dataVotans[kv]["note"]/sumCoeff;
                                }


                                if(exists(data.answers) && exists(data.answers.<?= $form['id']?>) && exists(data.answers.<?= $form['id']?>.admissibility) && exists(data.answers.<?= $form['id']?>.admissibility[kv])){
                                    dataVotans[kv]["admissibility"] = data.answers.<?= $form['id']?>.admissibility[kv];
                                    if(dataVotans[kv]["admissibility"] == "admissible")
                                        countAdmissible ++;
                                    else
                                        countInadmissible ++;
                                }

                                if(exists(data.answers) && exists(data.answers.<?= $form['id']?>) && exists(data.answers.<?= $form['id']?>.admissibilityTime) && exists(data.answers.<?= $form['id']?>.admissibilityTime[kv])){
                                    dataVotans[kv]["admissibilityTime"] = data.answers.<?= $form['id']?>.admissibilityTime[kv];
                                    vv.admissibilityTime = moment.unix(vv.admissibilityTime).format('DD/MM/YYYY HH:mm')
                                }
                                //mylog.log(vv.admissibilityTime,moment.unix(vv.admissibilityTime).format('DD/MM/YYYY HH:mm'),"admissibilityTime")

                            })

                            let html = '';
                            if(showAdmissibility){
                                html += `
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <button class="btn btn-success show-admissibility" data-value="admissible">${countAdmissible +" "+trad.yes}</button>
                                            <button class="btn btn-danger show-admissibility" data-value="inadmissible">${(Object.keys(dataVotans).length - countAdmissible) +" "+trad.no}</button>
                                            <button class="btn btn-default show-admissibility" data-value="all">${(Object.keys(dataVotans).length)+" "+trad.all}</button>
                                        </div>
                                    </div>
                                `
                            }

                            html += `<table class='table table-striped table<?= $kunik ?>'>
                                    <thead>
                                        <tr>
                                            <th>Photo</th>
                                            <th>${trad["Name"]}</th>
                                            <th>Note</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>`;
                                        mylog.log(dataVotans,"dataVotans")
                                        $.each(dataVotans,function(kv,vv){
                                            html +=
                                            `<tr class="${(exists(vv.admissibility) && vv.admissibility == "admissible") ? "yes<?= $kunik ?>" : "no<?= $kunik ?>"}">
                                                <td>
                                                    <img src="${exists(vv.profilMediumImageUrl) ? vv.profilMediumImageUrl : defaultImage}" alt=""  width="30" height="30" style="border-radius: 100%;object-fit:cover"/>
                                                </td>
                                                <td>
                                                    <span>${vv.name}</span>
                                                </td>
                                                <td>
                                                    <span class="pull-right label ${(exists(vv.admissibility) && vv.admissibility == "admissible") ? "label-success" : "label-danger"}  ${!showAdmissibility ? 'hidden' :''}">
                                                    <b>
                                                        ${(exists(vv.admissibility) && vv.admissibility == "admissible") ? trad.yes :  trad.no}
                                                        ${(exists(vv.admissibilityTime)) ? '<small>'+vv.admissibilityTime+'</small>' :  ""}
                                                    </b>
                                                    </span>
                                                    <span class=""><b>${vv.note}</b></span>
                                                <td>
                                            </tr>`;
                                        })
                            html +=
                                    `<tbody>
                                </table>`;
                            bootbox.alert(html);
                            if(typeof newReloadStepValidationInputGlobal != "undefined") {
                                newReloadStepValidationInputGlobal({
                                    inputKey : <?php echo json_encode($key) ?>,
                                    inputType : "tpls.forms.aap.selection"
                                })
                            }
                            $(document).on('click','.show-admissibility',function(){
                                var val = $(this).data("value");
                                if(val=="admissible"){
                                    $('.yes<?= $kunik ?>').show();
                                    $('.no<?= $kunik ?>').hide();
                                }else if(val=="inadmissible"){
                                    $('.yes<?= $kunik ?>').hide();
                                    $('.no<?= $kunik ?>').show();
                                }else if(val=="all"){
                                    $('.yes<?= $kunik ?>').show();
                                    $('.no<?= $kunik ?>').show();
                                }
                            })
                        }
                    )
                } else {
                    toastr.info(
                        trad["No valuations available yet"],
                        trad.information,
                        {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": true,
                            "positionClass": "toast-bottom-right",
                            "preventDuplicates": true,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "3000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    )
                }
			});
        })

        $(".admissibility<?= $kunik ?>").off().on('click',function(){
            var value = $(this).val();
            var changeStateEvaluation = function(){
                if(value ==  "admissible"){
                    dataHelper.path2Value({
                        id: "<?= (string)$answer["_id"] ?>",
                        collection : "<?= Form::ANSWER_COLLECTION ?>",
                        path : "status",
                        value: "vote",
                        arrayForm: true,
                        edit:false,
                    }, function() {
                        dataHelper.path2Value({
                            id : "<?= (string)$answer["_id"] ?>",
                            collection : "<?= Form::ANSWER_COLLECTION ?>",
                            path : "statusInfo.vote",
                            value : {
                                user : null,
                                action : "add",
                                updated : new Date().toLocaleString('fr', {day : 'numeric', month : 'numeric', year : 'numeric', hour : 'numeric', minute : 'numeric', timeZone : 'UTC', timeZoneName : 'short',}),
                            },
                            setType : [{
                                "path" : "updated",
                                "type" : "isoDate"
                            }]
                        }, function(response) {
                            ajaxPost(null, typeof coWsConfig != "undefined" && coWsConfig.pingRefreshViewUrl ? coWsConfig.pingRefreshViewUrl : null, {
                                action : "statusUpdated",
                                answerId : "<?= (string)$answer["_id"] ?>",
                                userSocketId : typeof aapObj != "undefined" && aapObj?.userSocketId ? aapObj?.userSocketId : "",
                                responses : response,
                            }, null, null, {contentType : 'application/json'});
                            
                            ajaxPost("", baseUrl + "/co2/aap/commonaap/action/notifyAddEvaluation", {answerId:tplCtx.id}, function(data){
                                
                            }, function(error){
                                mylog.log("ajaxPost error", error)
                            }, "json")
                        })
                    });
                }
            }
            dataHelper.path2Value({
                id: "<?= (string)$answer["_id"] ?>",
                collection : "<?= Form::ANSWER_COLLECTION ?>",
                path : "answers.<?= $form['id']?>.admissibility."+userId,
                value: value,
            }, function(params) {
                if(typeof newReloadStepValidationInputGlobal != "undefined") {
                    newReloadStepValidationInputGlobal({
                        inputKey : <?php echo json_encode($key) ?>,
                        inputType : "tpls.forms.aap.selection"
                    })
                }
                dataHelper.path2Value({
                    id: "<?= (string)$answer["_id"] ?>",
                    collection : "<?= Form::ANSWER_COLLECTION ?>",
                    path : "answers.<?= $form['id']?>.admissibilityTime."+userId,
                    value: "updatedTime",
                },function(){})
                if(params.result){
                    toastr.success("Ok");
                    changeStateEvaluation();
                    mean<?= $kunik ?>();
                }
            });
        })

        var mean<?= $kunik ?> =  function(){
            var predefinedCriteria = <?= json_encode($predefinedCriteria) ?>;
            sumCoeff = 0;
            keyCoeff = {};
            $.each(predefinedCriteria,function(k,v){
                if(k =="criterions" || k == "unassociatedCriterions"){
                    $.each(v,function(kk,vv){
                        keyCoeff[vv.fieldKey] = vv["coeff"]
                    })
                }
            })

            ajaxPost('',baseUrl+"/co2/element/get/type/answers/id/<?= (string)$answer["_id"] ?>",
            null,
            function(answer){
                answer = answer.map;
                if(exists(answer['answers']) && exists(answer['answers']["<?= $form['id']?>"]) && exists(answer['answers']["<?= $form['id']?>"]['selection'])){
                    $.each(keyCoeff,function(k,v){
                            sumCoeff += parseInt(v);
                    })
                    var allUserMean = 0;
                    var myMean=0;
                    var selection = answer['answers']["<?= $form['id']?>"]['selection'];
                    var evaluatorCouter = Object.keys(selection).length;
                    $.each(selection,function(kUserId,v){
                        if(exists(selection[kUserId])){
                            var current = 0
                            $.each(selection[kUserId],function(kk,vv){
                                if(exists(keyCoeff[kk]))
                                    current = current + (vv * keyCoeff[kk]);
                            })
                            current = current / sumCoeff;
                            if(kUserId==userId){
                                myMean = Number((current).toFixed(3))
                                $('.mean<?= $kunik ?>').html(myMean+"<span class='pull-right'>/"+noteMax+"</span>");
                            }

                            allUserMean = (allUserMean + current);
                        }
                    })
                    allUserMean = allUserMean/evaluatorCouter;
                    allUserMean = Number((allUserMean).toFixed(3));
                    $('.allMean<?= $kunik ?>').html(allUserMean+"<span class='pull-right'>/"+noteMax+"</span>");
                    dataHelper.path2Value({
                        id: "<?= (string)$answer["_id"] ?>",
                        collection : "<?= Form::ANSWER_COLLECTION ?>",
                        path : "answers.<?= $form['id']?>.allVotes",
                        value: allUserMean,
                        setType : "float"
                    }, function (params) {});
                }
            },null,null,{async:false})
        }
        mean<?= $kunik ?>();
        $('.tooltips').tooltip();
    })
</script>
