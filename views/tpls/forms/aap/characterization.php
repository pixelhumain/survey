<?php 
    /*$idEl = array_keys($parentForm["parent"])[0];
    $typeEl = $parentForm["parent"][$idEl]["type"];
    $el = Element::getElementById($idEl, $typeEl);
    $lists = [];
    $objective = null;
    $domainAction = null;

    if(isset($el["badges"])){
        $objectIdArray = [];
        foreach (array_keys($el["badges"]) as $keyId) {
            $objectIdArray[] = new MongoId($keyId);
        }
        $badges = PHDB::find(Badge::COLLECTION,["_id" => array('$in'=>  $objectIdArray)]);
        $domainAction = Badge::categorizeBadge ($badges,"domainAction");
        $objective = Badge::categorizeBadge ($badges,"cibleDD");
    }*/
?>
<div class="col-md-12">
    <!-- <button type="button" class="editCharacter"><i class="fa fa-edit"></i></button> -->
    <h1 class="text-center text-success">selection des domaines d'action</h1>
    <div class="col-xs-12">
        <h4>DOMAINE D’ACTION PRINCIPAL (OBLIGATOIRE)</h4>
        <p class="text-default" style="font-size:19px">Les domaines d’action indiquent la nature technique de l’action. Il peut s’agir de grands corps de métier. Une même action peut couvrir plusieurs domaines d’action. Le domaine d’action principal correspond au “coeur de métier” de l’action. Les domaines secondaires qualifient des facettes moins centrales de l’action.</p>
        <a href="javascript:dyFObj.openForm(caracterDF);" class="btn btn-lg btn-block btn-primary" >
            <?= !empty(@$answer["answers"]["aapStep2"]["caracter"]["actionPrincipal"])? @$answer["answers"]["aapStep2"]["caracter"]["actionPrincipal"] : "<i class='fa fa-question-circle'></i>" ?>
        </a>
    </div> 

    <div class="col-xs-12 padding-bottom-25">
        <h4>DOMAINE(S) D’ACTION SECONDAIRE(S)</h4>
        <a href="javascript:dyFObj.openForm(caracterDF);" class="btn btn-lg btn-block btn-primary" >
            <?= !empty(@$answer["answers"]["aapStep2"]["caracter"]["actionSecondaire"])? implode("<br>",@$answer["answers"]["aapStep2"]["caracter"]["actionSecondaire"]) : "<i class='fa fa-question-circle'></i>" ?>
        </a>
    </div>

    <h1 class="text-center text-success">SELECTION DES OBJECTIFS</h1>
    <div class="col-xs-12">
        <h4>OBJECTIF PRINCIPAL (OBLIGATOIRE)</h4>
        <a href="javascript:dyFObj.openForm(caracterDF);" class="btn btn-lg btn-block btn-primary" >
            <?= !empty(@$answer["answers"]["aapStep2"]["caracter"]["cibleDDPrincipal"])? @$answer["answers"]["aapStep2"]["caracter"]["cibleDDPrincipal"] : "<i class='fa fa-question-circle'></i>" ?>
        </a>
    </div>

    <div class="col-xs-12">
        <h4>OBJECTIF SECONDAIRE(S)</h4>
        <a href="javascript:dyFObj.openForm(caracterDF);" class="btn btn-lg btn-block btn-primary" >
            <?= !empty(@$answer["answers"]["aapStep2"]["caracter"]["cibleDDSecondaire"])? implode("<br>",@$answer["answers"]["aapStep2"]["caracter"]["cibleDDSecondaire"]) : "<i class='fa fa-question-circle'></i>" ?>
        </a>
    </div>
</div>

<script type="text/javascript">
var da = <?= isset($parentForm["actionDomains"]) ? json_encode($parentForm["actionDomains"]) : "{}"  ?>;
var obj = <?= isset($parentForm["objectives"]) ? json_encode($parentForm["objectives"]) : "{}" ?>;
var domainAction= {
    "Domaine d'action" : {},
};
var objective= {
    "Objectifs" : {},
};
$.each(da,function(k,v){
    domainAction["Domaine d'action"][k] = v
})
$.each(obj,function(k,v){
    objective["Objectifs"][k] = v
})

var caracterDF = {
    jsonSchema : {
        title : "Caractériser l’action",
        type : "object",
        properties : {
            actionPrincipal : {
                "inputType" : "select",
                "label" : "Domaine d'action principal",
                "placeholder" : "Choisir un domaine d'action principal",
                "groupOptions" : domainAction,
                "groupSelected" : false,
                "list" : "domainAction",
                "select2" : true,
                "value" : <?= json_encode(@$answer["answers"]["aapStep2"]["caracter"]["actionPrincipal"]) ?>
            },
            actionSecondaire : {
                "inputType" : "select",
                "label" : "Domaines(s) d’action secondaire(s)",
                "placeholder" : "Choisir un domaine d'action secondaire",
                /*"groupOptions" : <?php // json_encode($domainAction) ?>*/
                "groupOptions" : domainAction,
                "groupSelected" : false,
                "list" : "domainAction",
                "select2" : {
                    "multiple" : true
                },
                "value" : <?= json_encode(@$answer["answers"]["aapStep2"]["caracter"]["actionSecondaire"]) ?>
            },
            cibleDDPrincipal : {
                "inputType" : "select",
                "label" : "Objectif principal",
                "placeholder" : "Choisir une cible de dévelopement durable",
                "groupOptions" : objective,
                "groupSelected" : false,
                "list" : "cibleDD",
                "select2" : true,
                "value" : <?= json_encode(@$answer["answers"]["aapStep2"]["caracter"]["cibleDDPrincipal"]) ?>
            },
            cibleDDSecondaire : {
                "inputType" : "select",
                "label" : "Objectif(s) secondaire(s)",
                "placeholder" : "Choisir une cible de dévelopement durable secondaire",
                "list" : "cibleDD",
                "groupOptions" : objective,
                "groupSelected" : false,
                "select2" : {
                    "multiple" : true
                },
                "value" : <?= json_encode(@$answer["answers"]["aapStep2"]["caracter"]["cibleDDSecondaire"]) ?>
            },
            /*indicateur : {
		        "inputType" : "select",
		        "label" : "Indicateur",
		        "placeholder" : "Indicateur",
		        "list" : "indicateursPrincipaux",
		        "select2" : {
		            "multiple" : false
		        }
		    }*/
        },
        save : function(){
            mylog.log("save caracter","answerId",answerId);
			data = {
				collection : "answers",
				id : answerId,
				path : "answers.aapStep2.caracter",
				value : {}
			}
			if($("#actionPrincipal").val())
				data.value.actionPrincipal = $("#actionPrincipal").val();
			if($("#actionSecondaire").val())
				data.value.actionSecondaire = $("#actionSecondaire").val();
			if($("#cibleDDPrincipal").val())
				data.value.cibleDDPrincipal = $("#cibleDDPrincipal").val();
			if($("#cibleDDSecondaire").val())
				data.value.cibleDDSecondaire = $("#cibleDDSecondaire").val();

			mylog.log('save caracter ',data);

			if( Object.keys(data.value).length > 0 )
				dataHelper.path2Value( data, function(){
                    toastr.success("Enregistré");
                    reloadWizard();
                } );
        }
    }
};
$(document).ready(function() { 
    mylog.log("render","/modules/survey/views/tpls/forms/costum/ctenat/caracter.php");
    $('.editCharacter').off().click(function() { 
    	//dyFObj.dynFormCostum = null;
    	dyFObj.openForm(caracterDF);

    });
});

</script>