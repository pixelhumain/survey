<?php
    
?>
<style>
    
    .wrapper<?= $kunik ?> {
        display: inline-flex;
        flex-wrap: wrap;
        background: #fff;
        height: 11rem;
        width: 400px;
        justify-content: center;
        align-items: center;
        border-radius: 5px;
        padding: 20px 15px;
        box-shadow: 5px 5px 30px rgba(0,0,0,0.2);
    }
    .wrapper<?= $kunik ?> .options {
        height: 5.5rem;
        width: 95%;
        display: inline-flex;
        align-items: center;
        justify-content: space-evenly;
    }
    .wrapper<?= $kunik ?> .option {
        background: #fff;
        height: 100%;
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: space-evenly;
        margin: 0 10px;
        border-radius: 5px;
        padding: 0 10px;
        border: 2px solid lightgrey;
        transition: all 0.3s ease;
    }
    .wrapper<?= $kunik ?> .option .dot {
        height: 20px;
        width: 20px;
        background: #d9d9d9;
        border-radius: 50%;
        position: relative;
    }
    .wrapper<?= $kunik ?> .option .dot::before {
        position: absolute;
        content: "";
        top: 4px;
        left: 4px;
        width: 12px;
        height: 12px;
        border-radius: 50%;
        opacity: 0;
        transform: scale(1.5);
        transition: all 0.3s ease;
    }
    .wrapper<?= $kunik ?> input[type="radio"] {
        display: none;
    }
    .wrapper<?= $kunik ?> #option-1:checked:checked ~ .option-1 {
        border-color: var(--clr_var);
        background: var(--clr_var);
    }
    .wrapper<?= $kunik ?> #option-2:checked:checked ~ .option-2 {
        border-color: #cd1616;
        background: #cd1616;
    }
    .wrapper<?= $kunik ?> .option-1 .dot::before {
        background: var(--clr_var);
    }
    .wrapper<?= $kunik ?> .option-2 .dot::before {
        background: #cd1616;
    }
    .wrapper<?= $kunik ?> #option-1:checked:checked ~ .option-1 .dot,
    .wrapper<?= $kunik ?> #option-2:checked:checked ~ .option-2 .dot{
        background: #fff;
    }
    .wrapper<?= $kunik ?> #option-1:checked:checked ~ .option-1 .dot::before,
    .wrapper<?= $kunik ?> #option-2:checked:checked ~ .option-2 .dot::before{
        opacity: 1;
        transform: scale(1);
    }
    .wrapper<?= $kunik ?> .option span{
        font-size: 20px;
        text-transform: none !important;
        color: #808080;
    }
    .wrapper<?= $kunik ?> #option-1:checked:checked ~ .option-1 span,
    .wrapper<?= $kunik ?> #option-2:checked:checked ~ .option-2 span{
        color: #fff;
    }
    @media (max-width: 768px) {
        .wrapper<?= $kunik ?> {
            width: 100%;
            height: 20%;
        }
    }
</style>
<div class="col-xs-12 no-padding text-center">
    <div class="form-group">
        <div class="col-xs-12 bs-mb-2">
            <label class="col-xs-12 no-padding">
                <h4 class="bs-mb-0" style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
                    <?= $label.$editQuestionBtn ?>
                </h4>
            </label>
            <?php if(!empty($info)) { ?>
                <small id="<?= $key ?>Help" class="form-text text-muted"><?= $info ?></small>
            <?php } ?>
        </div>
        <div class="wrapper<?= $kunik ?>">
            <div class="options bs-mb-3">
                <input type="radio" name="select" value="selected" id="option-1" class="choose<?= $kunik ?> exclude-input">
                <input type="radio" name="select" value="notselected" id="option-2" class="choose<?= $kunik ?> exclude-input" checked>
                <label <?= $mode != "fa" && $mode != "r" ? 'for="option-1"' : "" ?> class="option option-1 <?= $mode != "fa" && $mode != "r" ? "cursor-pointer" : "" ?>">
                    <div class="dot"></div>
                    <span><?= Yii::t("common", "Yes") ?></span>
                    </label>
                <label <?= $mode != "fa" && $mode != "r" ? 'for="option-2"' : "" ?> class="option option-2 <?= $mode != "fa" && $mode != "r" ? "cursor-pointer" : "" ?>">
                    <div class="dot"></div>
                    <span><?= Yii::t("common", "No") ?></span>
                </label>
            </div>
            <small class="form-text col-xs-12 contextName<?= $kunik ?>">

            </small>
        </div>
    </div>
</div>
<script type="text/javascript">
    const _php = {
        kunik: "<?= $kunik ?>",
        form: JSON.parse(JSON.stringify(<?= json_encode($form) ?>)),
        key: "<?= $key ?>",
        mode: "<?= $mode ?>",
        collection: "<?= Form::ANSWER_COLLECTION  ?>",
        answer: JSON.parse(JSON.stringify(<?= json_encode($answer) ?>)),
        context: JSON.parse(JSON.stringify(<?= json_encode($context) ?>)),
        currentContext: {
            id: null,
            type: null,
            name: null,
        }
    };
    if (typeof costum != "undefined" && costum && costum.contextId) {
        _php.currentContext = {
            id: costum.contextId,
            type: costum.contextType,
            name: costum.title ? costum.title : null,
        }
    } else if (typeof contextData != "undefined" && contextData && contextData.name) {
        _php.currentContext = {
            id: contextData.id ? contextData.id : (contextData._id ? contextData._id.$id : null),
            type: contextData.type ? contextData.type : (contextData.collection ? contextData.collection : null),
            name: contextData.name,
        }
    } else if (_php.context && _php.context._id) {
        _php.currentContext = {
            id: _php.context._id.$id,
            type: _php.context.collection,
            name: _php.context.name,
        }
    }

    jQuery(document).ready(function() {
        if (_php.answer?.answers?.[_php.form?.id]?.[_php.key]?.[_php.currentContext.id]?.value == "selected") {
            $(`.choose${ _php.kunik }#option-1`).prop("checked", true)
            _php.answer?.answers?.[_php.form?.id]?.[_php.key]?.[_php.currentContext.id]?.name ? 
                $(`.contextName${ _php.kunik }`).text(`${ trad.Context } : ${ _php.answer?.answers?.[_php.form?.id]?.[_php.key]?.[_php.currentContext.id]?.name }`) : null
        } else {
            $(`.choose${ _php.kunik }#option-2`).prop("checked", true)
            $(`.contextName${ _php.kunik }`).hide()
        }
        $(`.choose${ _php.kunik }`).off("change").on("change", function(event) {
            const thisRadio = $(this)
            if (thisRadio.val() == "selected") {
                _php.currentContext?.name ? 
                    $(`.contextName${ _php.kunik }`).show().text(`${ trad.Context } : ${ _php.currentContext?.name }`) : null
            } else {
                $(`.contextName${ _php.kunik }`).text("")
                $(`.contextName${ _php.kunik }`).hide()
            }
            dataHelper.path2Value({
                id: _php.answer._id.$id,
                collection : _php.collection,
                path : `answers.${_php.form.id}.${_php.key}.${_php.currentContext.id}`,
                value: {
                    value: thisRadio.val(),
                    type: _php.currentContext.type,
                    name: _php.currentContext.name
                },
            }, function(params) {
                if(typeof newReloadStepValidationInputGlobal != "undefined") {
                    newReloadStepValidationInputGlobal({
                        inputKey : <?php echo json_encode($key) ?>,
                        inputType : "tpls.forms.aap.chooseProposal"
                    })
                }
            });
        })
    });
</script>