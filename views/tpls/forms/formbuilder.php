
<style type="text/css">

    .questionBlockplaceholder {
        padding: 0px;
        background: transparent;
        border: 1px dashed rgb(100, 150, 150);
    }

    .questionBlockhover {
        border: 1px solid #0a91ff;
    }

    .div-pulse{
        box-shadow: 0 0 0 0 var(--colortitle);
        -webkit-animation: pulse 1.25s infinite cubic-bezier(0.66, 0, 0, 1);
        -moz-animation: pulse 1.25s infinite cubic-bezier(0.66, 0, 0, 1);
        -ms-animation: pulse 1.25s infinite cubic-bezier(0.66, 0, 0, 1);
        animation: pulse 1.25s infinite cubic-bezier(0.66, 0, 0, 1);
    }

    @-webkit-keyframes pulse {to {box-shadow: 0 0 0 45px rgba(232, 76, 61, 0);}}
    @-moz-keyframes pulse {to {box-shadow: 0 0 0 45px rgba(232, 76, 61, 0);}}
    @-ms-keyframes pulse {to {box-shadow: 0 0 0 45px rgba(232, 76, 61, 0);}}
    @keyframes pulse {to {box-shadow: 0 0 0 45px rgba(232, 76, 61, 0);}}

    .submitans {
        width: max-content;
        margin: auto;
        display: flex;
    }

    .submitansbtn {
        margin-left: 10px;
        margin-right : 10px;
    }

    #pageContent.coFormbody {

    }
    .buildBlockPlaceholder {
        background-color: #eeeeee;
        opacity: 0.6;
        float:left;
    }
    .bg-txt {
        color: #4d4d49;
        opacity: 0.6;
        position: absolute;
        padding: 1em 3em;
        top: 0;
        left: 0;
        bottom: 0;
        text-align: center;
    }
</style>

<?php

$saveOneByOne = (isset($saveOneByOne)) ? $saveOneByOne :false;
if(isset($parentForm["openForm"]) && $parentForm["openForm"] == true && isset($contextId) && $contextId != null){
    $inputsEl = PHDB::findOne(Form::INPUTS_COLLECTION, array( "parent.".$contextId => array('$exists'=>1), "subForm" => $formId ));
}

if (isset($parentForm["type"]) && ($parentForm["type"] == "aap" || $parentForm["type"] == "aapConfig")){
    if( $parentForm["type"] == "aap"){
        $configEl = PHDB::findOneById(Form::COLLECTION, $parentForm["config"]);
    }elseif ( $parentForm["type"] == "aapConfig"){
        $configEl = $parentForm;
    }

    if (!empty($configEl) && !empty($configEl["subForms"][$formId]["inputs"])){
        $inputsObl = PHDB::findOneById(Form::INPUTS_COLLECTION, $configEl["subForms"][$formId]["inputs"]);
        if (!empty($inputsObl["inputs"])){
            $form["inputs"] = $inputsObl["inputs"];
            foreach ($form["inputs"] as $idInputs => $valInputs){
                $form["inputs"][$idInputs]["canAdmin"] = Authorisation::isParentAdmin((string)$configEl["_id"], Form::COLLECTION , Yii::app()->session["userId"]);
                $form["inputs"][$idInputs]["parentid"] = (string)$inputsObl["_id"];
                $form["inputs"][$idInputs]["docinputsid"] = $configEl["subForms"][$formId]["inputs"];
            }
        }
    }

    if ($parentForm["type"] == "aap"){
        $inputsapp = PHDB::findOne(Form::INPUTS_COLLECTION, array("formParent" => (string)$parentForm["_id"] , "step" => $formId));

        if (!empty($inputsapp["inputs"])) {
            foreach ($inputsapp["inputs"] as $idInputs => $valInputs) {
                $inputsapp["inputs"][$idInputs]["canAdmin"] = Authorisation::isParentAdmin((string)$parentForm["_id"], Form::COLLECTION, Yii::app()->session["userId"]);
                $inputsapp["inputs"][$idInputs]["parentid"] = (string)$inputsapp["_id"];
                $inputsapp["inputs"][$idInputs]["docinputsid"] = (string)$inputsapp['_id'];
            }
        }

        if (!empty($form["inputs"]) && !is_string($form["inputs"]) && !empty($inputsapp["inputs"])){
            $form["inputs"] = array_merge($form["inputs"] , $inputsapp["inputs"]);
        } elseif (!empty($inputsapp["inputs"]) ){
            $form["inputs"] = $inputsapp["inputs"];
        }
    }

    $form["id"] = $formId;

//    $inputsEl = PHDB::findOne(Form::INPUTS_COLLECTION, array( "parent.".$contextId => array('$exists'=>1), "subForm" => $formId ));
}

if( isset( $form["inputs"] ) ){
    if (isset($inputsEl["inputs"])) {
        $form["inputs"] = array_merge($form["inputs"] , $inputsEl["inputs"]);
    }
?>

<form id="formQuest" data-archi="la balise form d'un coform">
    <?php

    $initValues = [];
    $ct = 1;

    $orderInputs = [];
    $orderInputsKeys = [];
    foreach ( $form["inputs"] as $key => $input) {
        if( stripos( @$input["type"] , "tpls.forms.cplx" ) !== false )
            $saveOneByOne = true;
    }

    foreach ( $form["inputs"] as $key => $input) {
        if(isset($input["position"])){
            $orderInputs[(int)$input["position"]] = $input;
            $orderInputsKeys[(int)$input["position"]] = $key;
        }

    }

    $inV = $form["inputs"];

    if (!empty($parentForm["type"]) && $parentForm["type"] == 'aap') {
        foreach ($form["inputs"] as $key => $input) {
            if (!empty($input['positions'][$parentForm['_id']])) {
                unset($form["inputs"][$key]);
            } else {
                unset($inV[$key]);
            }
        }
    } else {
        foreach ($form["inputs"] as $key => $input) {
            if (!isset($input["position"])) {
                unset($form["inputs"][$key]);
            } else {
                unset($inV[$key]);
            }
        }
    }


    if(!function_exists('sortByPosOrder'))
    {
        if (empty($parentForm["type"]) || $parentForm["type"] != 'aap'){
            function sortByPosOrder($a, $b) {
                return (int)$a['position'] - (int)$b['position'];
            }
        }
    }

    if (!empty($parentForm["type"]) && $parentForm["type"] == 'aap') {

        if (empty($sorter)){
            $sorter = new Answer();
            $sorter->parentFo = $parentForm;
        }

        uasort($form["inputs"], array($sorter, 'sortByPosOrder'));
    } else {
        uasort($form["inputs"], sortByPosOrder);
    }

    $form["inputs"] = array_merge($form["inputs"], $inV);

    //nouvelle repositionnement




    //fin nouvelle repositionnement
   //echo json_encode($form["inputs"]) ;

    $dragNDrop = ($canEditForm) ? "dragNDrop" : "" ;
    $idSub = isset($form['_id']) ? $form['_id'] : null;
    if (isset($parentForm["type"]) && ($parentForm["type"] == "aap" || $parentForm["type"] == "aapConfig")){
        if ($parentForm["type"] == "aap"){
            if (!empty($inputsapp['_id'])){
                $idSub = (string)$inputsapp['_id'];
            }
        } elseif($parentForm["type"] == "aapConfig") {
            $idSub = (string)$inputsObl['_id'];
        }
    }

    if(count($form['inputs']) == 0) {
        echo "<ul id='questionList" . $formId . "' data-id='" . $formId . "' data-fpid='" . @$parentForm['_id'] . "' data-ids='" . $idSub . "' class='questionList col-xs-12  text-left no-padding" . $dragNDrop . "' style='height: 10vh'>";
        if(isset($canEditForm) && $canEditForm)
            echo '<span class="col-md-12 bg-txt">'.Yii::t("survey", "Drop your question here").'</span>';
    } else {
        echo "<ul id='questionList" . $formId . "' data-id='" . $formId . "' data-fpid='" . @$parentForm['_id'] . "' data-ids='" . $idSub . "' class='questionList col-xs-12  text-left no-padding" . $dragNDrop . "'>";
    }
    foreach ( $form["inputs"] as $key => $input) {
        $kunikT = explode( ".",  @$input["type"]);
        if(in_array( "multiDecide", $kunikT ) && isset($parentForm["inputConfig"]["multiDecide"])  ){
            $kunikDecide = explode( ".", $parentForm["inputConfig"]["multiDecide"]);
            $kunikDecideName = $kunikDecide[count($kunikDecide) - 1];
            $form["inputs"][$kunikDecideName] = $input;
            $form["inputs"][$kunikDecideName]["type"] = $parentForm["inputConfig"]["multiDecide"];
            $form["inputs"][$key]["border"] = false;
        }
    }

    // Mandatory questions List
    $inputTypes=array_column($form["inputs"],"type");
    $validateStepIndex=array_search("tpls.forms.cplx.validateStep",$inputTypes);
    $validateStepKey=array_keys($form["inputs"])[$validateStepIndex];
    $mandatoryQuestions=[];
    if(isset($parentForm["params"])  && isset(
        $parentForm["params"]["validateStep".$validateStepKey]) && isset($parentForm["params"]["validateStep".$validateStepKey]["inputList"])){
        $mandatoryQuestions=$parentForm["params"]["validateStep".$validateStepKey]["inputList"];
    }
    //mandatoryLabel
    $mandatoryMention="<span class='text-danger' style='font-size:small'> * ".Yii::t('survey','Required question')."</span>";
    if(in_array($key, $mandatoryQuestions)==true){
        if(isset($input["label"])){
            $input["label"]=$input["label"].$mandatoryMention;
        }
    }

    $isCoform = isset($parentForm['subType']) ? false : true;
    $stepValidationInput = null;
    $stepValidationInputGlobal = null;
    foreach ( $form["inputs"] as $key => $input) {
        $canAnswer = (Yii::app()->session["userId"] == $answer["user"] || !empty($parentForm["anyOnewithLinkCanAnswer"])) ? true :false;

        //var_dump($canEditForm);

            $params = [  "tpl" => @$this->costum["slug"],
                "slug"=>@$el["slug"],
                "canEdit"=>$canEdit,
                "el"=>$el  ];



        $tpl = @$input["type"];

        if(in_array($tpl, ["textarea","markdown","wysiwyg"]))
            $tpl = "tpls.forms.textarea";
        else if(empty($tpl) || in_array($tpl, ["text","button","color","date","datetime-local","email","image","month","number","radio","range","tel","time","url","week","tags","hidden"]))
            $tpl = "tpls.forms.text";
        else if(in_array($tpl, ["sectionTitle"]))
            $tpl = "tpls.forms.".$tpl;

        if( stripos( $tpl , "tpls.forms." ) !== false )
            $tplT = explode(".", @$input["type"]);

        $kunikT = explode( ".", @$input["type"]);
        $keyTpl = ( count($kunikT)>1 ) ? $kunikT[ count($kunikT)-1 ] : @$input["type"];
/*        var_dump($kunikT);
        var_dump(in_array( $kunikT, "multiDecide"));*/
/*        if(in_array( "multiDecide", $kunikT ) && isset($answer["inputConfig"]["multiDecide"])  ){
            $tpl = $answer["inputConfig"]["multiDecide"];
            $kunikT = explode( ".", $tpl);
            $keyTpl = $keyTpl = ( count($kunikT)>1 ) ? $kunikT[ count($kunikT)-1 ] : $input["type"];
        }*/
//        var_dump($tpl);
        $kunik = $keyTpl.$key;
        $answers = null;
        $answerPath = "answers.".$key.".";


        if(isset($wizard))
        {
            if($wizard){
                $answerPath = "answers.".$formId.".".$key.".";
                if( isset($answer["answers"][$formId][$key]) && count($answer["answers"][$formId][$key])>0 )
                    $answers = $answer["answers"][$formId][$key];
            }
            else {
                if(isset($answer["answers"][$key]))
                    $answers = $answer["answers"][$key];
            }
        }

        if (isset($answers) && is_array($answers)) {
            foreach ($answers as $kans => $vans) {
                if (is_array($vans)) {
                    foreach ($vans as $kvalAns => $valAns) {
                        if (is_string($valAns)) {
                            $answers[$kans][$kvalAns] = str_replace('\"', '"', $valAns);
                            $answers[$kans][$kvalAns] = str_replace('"', '&quot;', $valAns);
                            // $answers[$kans][$kval] = addslashes($val);
                        }
                    }
                }
            }
        }


        $p = [
            "el" => $el,
            "parentForm"=> $parentForm,
            "form"      => $form,
            "formId"    => $formId,
            "subFs"     => $subFs,
            "key"       => $key,
            "keyTpl"       => $keyTpl,
            "kunik"     => $kunik,
            "input"     => $input,
            "type"      => @$input["type"],
            "answerPath"=> $answerPath,
            "answer"    => $answer,
            "answers"   => $answers ,//sub answers for this input
            "label"     => @$input["label"] ,//$ct." - ".$input["label"] ,
            "titleColor"=> (isset($this->costum["colors"]["pink"])) ? $this->costum["colors"]["pink"] : "#000",
            "info"            => isset($input["info"]) ? $input["info"] : "" ,
            "placeholder"     => isset($input["placeholder"]) ? $input["placeholder"] : "" ,
            "mode"            => $mode,
            "canEdit"         => $canEdit,
            "canEditForm"     => @$canEditForm, //to change questions and configutarion
            "canEditOpenform" => @$canEditOpenform,
            "canAdminAnswer"  => $canAdminAnswer, //btn de config > Authorisation::isElementAdmin($parent["id"], $parent["type"]
            "canAnswer"       => $canAnswer,
            "editQuestionBtn" => @$editQuestionBtn,
            "saveOneByOne"    => $saveOneByOne,
            "wizard"          => (isset($wizard) && $wizard == true),
            "tpl"             => $tpl,
            "contextId" => @$contextId,
            "contextType" => @$contextType,
            "usersAnswered" => @$usersAnswered

        ];

        if (isset($input["canAdmin"])){
            $p["canAdminAnswer"] = $canAdminAnswer = $input["canAdmin"];
            if (isset($mode) && $mode == "fa")
                $p["canEditForm"] = $input["canAdmin"];
        }

        if (isset($parentForm["type"]) && ($parentForm["type"] == "aap" || $parentForm["type"] == "aapConfig")){
            $labelaap = isset($input["label"]) ? $input["label"] : "";
            $infoaap = isset($input["info"]) ? $input["info"] : "";
            $placeholderaap = isset($input["info"]) ? $input["info"] : "";
            $typeaap = isset($input["type"]) ? $input["type"] : "";
            /* $editQuestionBtn = $p["editQuestionBtn"] = ($p["canEditForm"] && $mode == "fa") ? " <a class='btn btn-xs btn-danger descSec".$key." editQuestionaap' href='javascript:;' data-position='".@$input['position']."' data-positions='".json_encode(@$input['positions'])."' data-collection='".Form::INPUTS_COLLECTION."' data-key='".$key."' data-path='inputs.".$key."' data-id='".$input['parentid']."' data-parentid='".(isset($input['parentid']) ? $input['parentid'] : (string)$parentForm['_id'])."' data-label='".$labelaap."' data-info='".$infoaap."' data-placeholder='".$placeholderaap."' data-type='".$typeaap."'><i class='fa fa-pencil'></i></a>".
                " <a class='btn btn-xs btn-danger deleteLine' href='javascript:;'  data-id='".$input['parentid']."' data-collection='".Form::INPUTS_COLLECTION."' data-key='question".$key."' data-path='inputs.".$key."' data-parentid='".(isset($input['parentid']) ? $input['parentid'] : (string)$parentForm['_id'])."'><i class='fa fa-trash'></i></a>" : ""; */
            $editQuestionBtn = $p["editQuestionBtn"] = ($p["canEditForm"] && $mode == "fa") ? " <a class='btn btn-xs btn-danger descSec".$key." editQuestionaap' href='javascript:;' data-position='".@$input['position']."' data-positions='".json_encode(@$input['positions'])."' data-collection='".Form::INPUTS_COLLECTION."' data-key='".$key."' data-path='inputs.".$key."' data-id='".$input['parentid']."' data-parentid='".(string)$parentForm['_id']."' data-label='".$labelaap."' data-info='".$infoaap."' data-placeholder='".$placeholderaap."' data-type='".$typeaap."'><i class='fa fa-pencil'></i></a>".
                " <a class='btn btn-xs btn-danger deleteLine' href='javascript:;'  data-id='".$input['parentid']."' data-collection='".Form::INPUTS_COLLECTION."' data-key='question".$key."' data-path='inputs.".$key."' data-parentid='".(string)$parentForm['_id']."'><i class='fa fa-trash'></i></a>" : "";
        } else {
            $editQuestionBtn = $p["editQuestionBtn"] = ($canEditForm) ? " <a class='btn btn-xs btn-danger descSec".$key." editQuestion' href='javascript:;' data-position='".@$input['position']."' data-positions='".json_encode(@$input['positions'])."' data-form='".$formId."' data-id='".$form["_id"]."' data-collection='".Form::COLLECTION."' data-key='".$key."' data-path='inputs.".$key."' data-parentid='".(string)$parentForm['_id']."'><i class='fa fa-pencil'></i></a>".
                " <a class='btn btn-xs btn-danger deleteLine' href='javascript:;' data-id='".$form["_id"]."' data-collection='".Form::COLLECTION."' data-key='question".$key."' data-path='inputs.".$key."' data-parentid='".(string)$parentForm['_id']."'><i class='fa fa-trash'></i></a>" : "";
        }

        if(isset($tplT[2]) && $tplT[2] == "select" && isset($input["options"]))
            $p["options"] = $input["options"];

        if(@$input["type"] == "tags")
            $initValues[$key] = $input;

        if(isset($input["hide"]) && $input["hide"] == true){
            $conditionalHide = "hide";
        } else {
            $conditionalHide = "";
        }

        if(isset($input["border"]) && $input["border"] == false){
            $borderstyle = "";
        } else {
            $borderstyle = "border:1px solid #ddd;";
        }
        if($ct == 1 && $tpl == "tpls.forms.cplx.validateStep" && $isCoform) {
            $stepValidationInput = [
                'key' => $key,
                'conditionalHide' => $conditionalHide,
                'tpl' => $tpl,
                'p' => $p,
                'docinputsid' => isset($input['docinputsid']) ? $input['docinputsid'] : null
            ];
        } else {
            // echo "<li id='question".$key."' class='col-xs-12 no-padding questionBlock ".$conditionalHide."'  data-id='".@$form['_id']."' data-path='inputs.".$key.".position' data-key='".$key."' data-docinputsid='".@$input['docinputsid']."' data-form='".$formId."' style='margin-bottom:0px; margin-bottom:10px;'>";
            if($tpl == 'tpls.forms.cplx.validateStep' || $tpl == 'tpls.forms.costum.franceTierslieux.validateStep' || $tpl == 'tpls.forms.costum.franceTierslieux.validateStepFtl') {
                $stepValidationInputGlobal = [
                    'key' => $key,
                    'conditionalHide' => $conditionalHide,
                    'tpl' => $tpl,
                    'p' => $p,
                    'docinputsid' => isset($input['docinputsid']) ? $input['docinputsid'] : null
                ];
            }
            if(!isset($input["hideInForm"])){
                echo "<li id='question" . $key . "' class='col-xs-12 ui-corner-all no-padding questionBlock " . $conditionalHide . "'  data-id='" . @$form['_id'] . "' data-path='inputs." . $key . ".position' data-key='" . $key . "' data-docinputsid='" . @$input['docinputsid'] . "' data-form='" . $formId . "' style='margin-bottom:0px; margin-bottom:10px;'>";

                echo '<div class="col-md-12 col-sm-12 col-xs-12 navigationarrow" style="display: none"> 
                        <button type="button" class="btn navigationarrowbtn" data-dir="up">&#8593; '.Yii::t("survey","go up").' </button>
                        <button type="button" class="btn navigationarrowbtn" data-dir="down">&#8595;  '.Yii::t("survey","go to").'</button>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">';
                echo $this->renderPartial("survey.views.tpls.forms.coFormjs", $params, true );

                echo $this->renderPartial( "survey.views.".$tpl , $p , true );
                echo "</div></li>";
            }
        }
        if($ct == count($form['inputs']) && $isCoform && !empty($stepValidationInput)) {
            echo "<li id='question".$stepValidationInput['key']."' class='col-xs-12 ui-corner-all no-padding questionBlock ".$stepValidationInput['conditionalHide']."'  data-id='".@$form['_id']."' data-path='inputs.".$stepValidationInput['key'].".position' data-key='".$stepValidationInput['key']."' data-docinputsid='".$stepValidationInput['docinputsid']."' data-form='".$formId."' style='margin-bottom:0px; margin-bottom:10px;'>";

            echo '<div class="col-md-12 col-sm-12 col-xs-12 navigationarrow" style="display: none"> 
                    <button type="button" class="btn navigationarrowbtn" data-dir="up">&#8593; '.Yii::t("survey","go up").' </button>
                    <button type="button" class="btn navigationarrowbtn" data-dir="down">&#8595;  '.Yii::t("survey","go to").'</button>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">';
            echo $this->renderPartial("survey.views.tpls.forms.coFormjs", $params, true );

            echo $this->renderPartial( "survey.views.".$stepValidationInput['tpl'] , $stepValidationInput['p'] , true );
            echo "</div></li>";
        }

        $ct++;
    }
    echo "</ul>";

    $id = (!empty($answer)) ? " data-id='".$answer["_id"]."' " : "";

    echo '<div class="submitans" >';

    if(isset($parentForm["validateBtn"]) && $parentForm["validateBtn"] == true ){
            if ($mode != "fa") {
     ?>

            <!-- <a href="javascript:;" id=""  class="submitForm btn btn-success submitansbtn ">Valider le formulaire</a> -->
    <?php
    }else {?>
            <!-- <a href="javascript:;" id=""  class=" btn btn-success submitansconfigbtn ">Configurer le bouton valider </a> -->

        <?php
        }
        }
        if(isset($parentForm["feedbackBtn"]) && $parentForm["feedbackBtn"] == true && $mode != "fa"){
            $btn_fb_label = Yii::t("survey","Send feedback");
            // if (isset($answer["feedback"]["star"]) && $answer["feedback"]["star"] == (string)$i) {
            //     for ($i=0; $i > 0; $i--) {
            //     $btn_fb_label = "";
            // }
        ?>

             <!-- <a href="javascript:;" id=""  class="feedbackForm btn btn-default "> Envoyer un feedback </a> -->
        <?php
    }


    echo '</div>';

// ------ buttons previous && next steps at the bottom of the form ----
    echo '<div class="prevNext col-xs-12 prevNext text-center">';
        if(isset($answer["step"])){
            $currentStep=$answer["step"];
            $stepKey=array_search($currentStep,array_keys($subFs));
            $subFKey=array_search($formId,array_keys($subFs));
            $stepsArr=array_keys($subFs);
            $nbSteps=sizeof($stepsArr);
            $strPrevNext="";
            if($subFKey>0){
                $strPrevNext.= '<a href="javascript:;" onclick="showStepForm(`#'.$stepsArr[$subFKey-1].'`); return false;"><button class="btn margin-10">'.Yii::t("common","Previous step").'</button></a>';
            }
            if($stepKey>$subFKey && $subFKey<$nbSteps-1){
                 $strPrevNext.= '<a href="javascript:;" onclick="showStepForm(`#'.$stepsArr[$subFKey+1].'`); return false;"><button class="btn margin-10">'.Yii::t("common","Next step").'</button></a>';
            }
            echo $strPrevNext;
        }
    echo '</div>';

    //if form contains just one cplx input then all saves are made oneByone

    if(!$saveOneByOne){ ?>
    <div style="display: flex;">
       <!--  <a href="javascript:;" id="openFormSubmit" <?php echo $id ?> class=" btn btn-primary">Envoyer</a> -->
    </div>
    <?php } ?>
</form>
<?php } else if(isset($canEditForm) && $canEditForm){
        $dragNDrop = ($canEditForm) ? "dragNDrop" : "";
        $idSub = isset($form['_id']) ? $form['_id'] : '';
        if (isset($parentForm["type"]) && ($parentForm["type"] == "aap" || $parentForm["type"] == "aapConfig")){
            if ($parentForm["type"] == "aap"){
                if (!empty($inputsapp['_id'])){
                    $idSub = (string)$inputsapp['_id'];
                }
            } elseif($parentForm["type"] == "aapConfig") {
                $idSub = (string)$inputsObl['_id'];
            }
        }
?>
        <form id="formQuest" data-archi="la balise form d'un coform">
            <ul id="questionList<?php echo $formId; ?>" data-id="<?php echo $formId; ?>" data-fip="<?php echo isset($parentForm['id']) ? $parentForm['id'] : ''; ?>" data-ids="<?php echo $idSub; ?>" class="questionList col-xs-12 text-left no-padding <?php echo $dragNDrop; ?>" style="height: 10vh;">
                <span class="col-md-12 bg-txt"><?php echo Yii::t("survey", "Drop your question here"); ?></span>
            </ul>
        </form>
<?php
    }

if (isset($inputsEl["inputs"])) {
    $form["inputs"] = $inputsEl["inputs"];
}

if (isset($canEditForm) && $canEditForm) {
    if (isset($parentForm["type"]) && ($parentForm["type"] == "aap" || $parentForm["type"] == "aapConfig")){
        if ($parentForm["type"] == "aap"){
        ?>
            <div class="text-center">
                <?php
                if (!empty($inputsapp['_id'])){ ?>
                    <a href="javascript:;" class="addQuestionaap btn btn-danger" data-id="<?php echo (string)$inputsapp['_id'] ?>" data-form="<?php echo (string)$formId ?>" data-formparentid="<?php echo (string)$parentForm["_id"] ?>" ><i class="fa fa-plus"></i><?php echo Yii::t("survey","Add a specific question") ?></a>
                <?php
                }
                ?>
            </div>

        <?php
        } elseif($parentForm["type"] == "aapConfig") {
        ?>
            <div class="text-center">
                <a href="javascript:;" class="addQuestionaap btn btn-danger" data-id="<?php echo (string)$inputsObl['_id'] ?>" data-form="<?php echo (string)$formId ?>" data-formparentid="<?php echo (string)$parentForm["_id"] ?>"><i class="fa fa-plus"></i> <?php echo Yii::t("survey","Add a Required Question") ?></a>
            </div>

        <?php
        }
    } else {
    ?>
<div class="text-center">
    <a href="javascript:;" class="addQuestion btn btn-danger" data-form="<?php echo $formId ?>"  data-id="<?php echo (string)$form['_id'] ?>" data-parentid="<?php echo (string)$parentForm['_id'] ?>" ><i class="fa fa-plus"></i> <?php echo Yii::t("survey","Add Question") ?></a>
</div>
<?php
    }
}
if (isset($mode) && $mode != "pdf") {
?>

<script type="text/javascript">
formInputs = <?php echo (isset($form["inputs"]) ? json_encode($form["inputs"]) : "[]") ?>;

formInputsedit = <?php echo (isset($form["inputs"]) ? json_encode($form["inputs"]) : "[]") ?>;

var dataformParent = <?php echo (isset($parentForm) ? json_encode($parentForm) : "[]") ?>;
var canEditForm = <?php echo $canEditForm ? $canEditForm : "false" ?>;
var formKey = <?php echo json_encode($formId); ?>;
let parentFormType = <?php echo isset($parentForm['type']) ? json_encode($parentForm['type']) : "null"; ?>;
var idParentForm = <?php echo json_encode((string)$parentForm['_id']); ?>;
var idForm = '';
var formIsCoform = <?php echo isset($parentForm['subType']) ? json_encode('false') : json_encode('true'); ?>;
if(parentFormType != null && (parentFormType == 'aap' || parentFormType == 'aapConfig')) {
    formKey = <?php echo json_encode((String)$formId); ?>;
    if(parentFormType == 'aap') {
        idForm = <?php echo !empty($inputsapp['_id']) ? json_encode((string)$inputsapp['_id']) : "null"; ?>;
    } else if(parentFormType == 'aapConfig') {
        idForm = <?php echo !empty($inputsObl['_id']) ? json_encode((string)$inputsObl['_id']) : "null"; ?>;
    }
} else {
    idForm = <?php echo json_encode((string)$form['id']); ?>;
}

if (formInputsfor)
    formInputsfor["<?php echo (isset($form["id"]) ? $form["id"] : "") ?>"] = <?php echo (isset($form["inputs"]) ? json_encode($form["inputs"]) : "{}") ?>;
else {
    var formInputsfor = {};
    formInputsfor["<?php echo(isset($form["id"]) ? $form["id"] : "") ?>"] = <?php echo(isset($form["inputs"]) ? json_encode($form["inputs"]) : "{}") ?>;
}

if (typeof inputsList != "undefined"){
    inputsList["<?php echo @$formId ?>"] = <?php echo (isset($form["inputs"]) ? json_encode(@$form["inputs"]) : "[]") ?>;
} else {
    var inputsList = {};
    inputsList["<?php echo @$formId ?>"] = <?php echo (isset($form["inputs"]) ? json_encode($form["inputs"]) : "[]") ?>;
}

var initValues = <?php echo (!empty($initValues)) ? json_encode( $initValues ) : "null"; ?>;

var answerStateOption = <?php echo (!empty($parentForm["params"]["answerStateOption"]) ?json_encode( $parentForm["params"]["answerStateOption"] ) : "null"  ) ; ?>;

var answerState = '<?php echo (!empty($answer["submited"]["answerState"]) ? $answer["submited"]["answerState"] : "") ; ?>';

if(typeof stepValidationInput == 'undefined') {
    var stepValidationInput = {}
}

if(typeof inputObliList == 'undefined') {
    var inputObliList = {}
}

if(typeof formInputRules == 'undefined') {
    var formInputRules = {}
}

if(typeof viewMode == 'undefined') {
    var viewMode = ""
}

if(typeof formAllInputs == 'undefined') {
    var formAllInputs = {};
}

viewMode = <?php echo json_encode($mode); ?>;

if(typeof ownAnswer == 'undefined') {
    var ownAnswer = {};
}
ownAnswer[formKey] = <?php echo isset($answer['answers'][$form['id']]) ? json_encode($answer['answers'][$form['id']]) : json_encode(array()) ?>;

stepValidationInput[formKey] = <?php echo (!empty($stepValidationInputGlobal)) ? json_encode( $stepValidationInputGlobal ) : "{}"; ?>;

$('#questionList<?php echo $formId ?>').bind('DOMSubtreeModified', function() {
    if($(this).children().length == 0) {
        $(this).css('height', '10vh');
        $(this).append(`
            <span class="col-md-12 bg-txt"><?php echo Yii::t("survey", "Drop your question here"); ?></span>
        `)
    }
});

    sectionDyf.submitans = {
        "jsonSchema" : {
            "title" : tradForm.progressOfForm,
            "icon" : "fa-check",
            "text" : "",
            "properties" : {
                "answerState" : {
                    "label" : tradForm.progressOfForm,
                    "placeholder" : "",
                    "inputType" : "select",
                    "options" : answerStateOption ,
                    "value" : answerState
                }
            },
            save : function () {

                var subanswer = {
                    collection : "answers",
                    id : answerObj._id.$id
                };

                var today = new Date();
                subanswer.value = {
                    date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() ,
                    answerState: $("#answerState").val()
                };

                subanswer.path = "submited";

                dataHelper.path2Value( subanswer , function(params) {
                    toastr.success('Envoyé');
                    reloadWizard();
                } );

                mylog.log("save tplCtx",tplCtx);
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) {
                        reloadWizard();
                    } );
                }

            }
        }
    };

    sectionDyf.submitansconfig = {
        "jsonSchema" : {
            "title" : tradForm.configFormProgressBtn,
            "icon" : "fa-cog",
            "text" : "",
            "properties" : {
                "answerStateOption" : {
                    "label" : tradForm.possibleOptions,
                    "placeholder" : "",
                    "inputType" : "array",
                    "value" : answerStateOption
                }
            },
            save : function () {

                var subanswer = {
                    collection : "forms",
                    id : answerObj.form
                };

                subanswer.value = getArray('.answerStateOption'+k+val.inputType);

                subanswer.path = "params.answerStateOption";

                dataHelper.path2Value( subanswer , function(params) {
                    toastr.success('Envoyé');
                    reloadWizard();
                } );

            }
        }
    };

    if(typeof checkVisible == "undefined") {
		function checkVisible(elm) {
			var rect = elm.getBoundingClientRect();
			// var rect = elm.offset();
			var viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
			return !(rect.top - viewHeight/2 >= 0);
		}
	}
	if(typeof addError == "undefined") {
		function addError(elem, cssValue = {}, className = "") {
			if(elem instanceof jQuery){
				if(Object.keys(cssValue).length > 0) {
					for([key, value] of Object.entries(cssValue)) {
						elem.css(key, value)
					}
				}
				if(className != "") {
					elem.addClass(className)
				}
			}
		}
	}
	if(typeof removeError == "undefined") {
		function removeError(elem, cssValue = {}, className = "") {
			if(elem instanceof jQuery){
				if(Object.keys(cssValue).length > 0) {
					for([key, value] of Object.entries(cssValue)) {
						elem.css(key, value)
					}
				}
				if(className != "") {
					elem.removeClass(className)
				}
			}
		}
	}

    if(typeof buildValidationGlobal == 'undefined') {
        function buildValidationGlobal(subFormId = '') {
            var showMsg = true;
            if(typeof stepValidationInput != 'undefined' && stepValidationInput[subFormId] && stepValidationInput[subFormId]['p'] && stepValidationInput[subFormId]['p']['kunik']) {
                const validateStepKunik = stepValidationInput[subFormId]['p']['kunik'];
                if(!formInputRules['rules'+subFormId+validateStepKunik])
                    formInputRules['rules'+subFormId+validateStepKunik] = [];
                if(stepValidationInput[subFormId]['p']['parentForm'] && stepValidationInput[subFormId]['p']['parentForm']['params'] && stepValidationInput[subFormId]['p']['parentForm']['params'][validateStepKunik] && stepValidationInput[subFormId]['p']['parentForm']['params'][validateStepKunik]['inputList']) {
                    inputObliList[validateStepKunik] = stepValidationInput[subFormId]['p']['parentForm']['params'][validateStepKunik]['inputList'];
                }
                if(stepValidationInput[subFormId]['p']['parentForm'] && stepValidationInput[subFormId]['p']['parentForm']['params'] && stepValidationInput[subFormId]['p']['parentForm']['params'][validateStepKunik] && stepValidationInput[subFormId]['p']['parentForm']['params'][validateStepKunik]['showMsg']) {
                    showMsg = stepValidationInput[subFormId]['p']['parentForm']['params'][validateStepKunik]['showMsg'];
                }
                formAllInputs['inputs'+validateStepKunik] = formInputs;
                if(inputObliList[validateStepKunik]) {
                    inputObliList[validateStepKunik].forEach((inVal, inIndex) => {
                        formInputRules['rules'+subFormId+validateStepKunik].push(inVal);

                        mylog.log("tafiditr ato")
                        const eventRulesInterval = setInterval(function() {

                            if($(`li#question${inVal}`).length > 0 && $(`li#question${inVal}`).is(":visible") && viewMode != 'r' && viewMode != 'pdf') {
                                if($(`li#question${inVal} textarea`).length > 0) {
                                    $(`li#question${inVal} textarea`).on('blur', function(e) {
                                        if($(this).val().trim() == '' || $(this).val().trim() == null) {
                                            $(`small#${validateStepKunik}_${inVal}Error`).remove();
                                            $(`li#question${inVal}`).find('.CodeMirror.cm-s-paper.CodeMirror-wrap').css('border-color', '#a94442');
                                            /* $(`li#question${inVal}`).find('.CodeMirror.cm-s-paper.CodeMirror-wrap').after(`
                                                <small id="${validateStepKunik}_${inVal}Error" class="text-danger">Champ obligatoire</small>
                                            `) */
                                        } else {
                                            var elem = $(`li#question${inVal}`);
                                            $(`small#${validateStepKunik}_${inVal}Error`).remove();
                                            removeError(
                                                elem,
                                                {
                                                    'border' : 'none',
                                                },
                                                'error-msg'
                                            );
                                            $(`li#question${inVal}`).find('.CodeMirror.cm-s-paper.CodeMirror-wrap').css('border-color', '#ddd');
                                        }
                                    });
                                    $(`li#question${inVal} textarea`).on('keyup', function(e) {
                                        if($(this).val().trim() != '' || $(this).val().trim() != null) {
                                            var elem = $(`li#question${inVal}`);
                                            $(`small#${validateStepKunik}_${inVal}Error`).remove();
                                            removeError(
                                                elem,
                                                {
                                                    'border' : 'none',
                                                },
                                                'error-msg'
                                            );
                                            $(`li#question${inVal}`).find('.CodeMirror.cm-s-paper.CodeMirror-wrap').css('border-color', '#ddd');
                                        }
                                    })
                                }
                                if($(`li#question${inVal} input`).length > 0 && formAllInputs && formAllInputs['inputs'+validateStepKunik] &&
                                    formAllInputs['inputs'+validateStepKunik][inVal] &&
                                    formAllInputs['inputs'+validateStepKunik][inVal]['type'] &&
                                    (
                                        formAllInputs['inputs'+validateStepKunik][inVal]['type'] != 'tpls.forms.uploader' &&
                                        formAllInputs['inputs'+validateStepKunik][inVal]['type'] != 'tpls.forms.cplx.address'
                                    )) {
                                    $(`li#question${inVal} input`).on('blur', function(e) {
                                        if($(this).val().trim() == '' || $(this).val().trim() == null) {
                                            $(`small#${validateStepKunik}_${inVal}Error`).remove();
                                            $(this).css('border-color', '#a94442')
                                            /* $(this).after(`
                                                <small id="${validateStepKunik}_${inVal}Error" class="text-danger">Champ obligatoire</small>
                                            `) */
                                        } else {
                                            var elem = $(`li#question${inVal}`);
                                            $(`small#${validateStepKunik}_${inVal}Error`).remove();
                                            removeError(
                                                elem,
                                                {
                                                    'border' : 'none',
                                                },
                                                'error-msg'
                                            );
                                            var errorAttr = $(`li#question${inVal}`).find(`.rules-container${inVal}`).attr('data-rules');
                                            if(typeof errorAttr != 'undefined') {
                                                var rulesAttr = JSON.parse(errorAttr.replace(/'/g, '"'));
                                                if(
                                                    ((typeof rulesAttr == 'object' && rulesAttr.width && rulesAttr.width.min && rulesAttr.width.min > 0) || (typeof rulesAttr == 'object' && rulesAttr.width && rulesAttr.width.max && rulesAttr.width.max > 0)) ||
                                                    (typeof rulesAttr == 'object' && rulesAttr.syntaxe)
                                                ) {

                                                } else {
                                                    $(this).css('border-color', '#ddd')
                                                }
                                            } else
                                                $(this).css('border-color', '#ddd')
                                            
                                        }
                                    })
                                    $(`li#question${inVal} input`).on('keyup', function(e) {
                                        if($(this).val().trim() != '' || $(this).val().trim() != null) {
                                            var elem = $(`li#question${inVal}`);
                                            $(`small#${validateStepKunik}_${inVal}Error`).remove();
                                            removeError(
                                                elem,
                                                {
                                                    'border' : 'none',
                                                },
                                                'error-msg'
                                            );
                                            $(this).css('border-color', '#ddd')
                                        }
                                    })
                                }
                                if(
                                    formAllInputs && formAllInputs['inputs'+validateStepKunik] &&
                                    formAllInputs['inputs'+validateStepKunik][inVal] &&
                                    formAllInputs['inputs'+validateStepKunik][inVal]['type'] &&
                                    viewMode != 'r' && viewMode != 'pdf' &&
                                    (showMsg == 'true' || showMsg == true)
                                    // formAllInputs['inputs'+validateStepKunik][inVal]['type'] != 'tpls.forms.costum.franceTierslieux.finder'
                                ) {
                                    $(window).on('resize scroll', function(e) {
                                        // e.stopImmediatePropagation();
                                        if(document.querySelectorAll(`li#question${inVal}`).length > 0) {
                                            var elem = $(`li#question${inVal}`);
                                            /* if(checkVisibleAgain(document.querySelectorAll(`li#question${inVal}`)[0]) == true) {
                                                if(ownAnswer && ownAnswer[subFormId] && ownAnswer[subFormId][inVal]) {
                                                    if(notEmpty(ownAnswer[subFormId][inVal])) {
                                                        removeError(
                                                            elem,
                                                            {
                                                                'border' : 'none',
                                                            },
                                                            'error-msg'
                                                        );
                                                    }
                                                }
                                            } */
                                            if(checkVisible(document.querySelectorAll(`li#question${inVal}`)[0]) == true) {
                                                var inputKey = "";
                                                const inputTypeArr = formAllInputs['inputs'+validateStepKunik][inVal]['type'].split('.');
                                                inputKey = inputTypeArr[inputTypeArr.length -1] ? inputTypeArr[inputTypeArr.length -1] : 'none';
                                                if(ownAnswer && ownAnswer[subFormId] && (ownAnswer[subFormId][inVal]) || ownAnswer[subFormId][inputKey+inVal]) {
                                                    if(ownAnswer[subFormId][inVal]) {
                                                        if(notEmpty(ownAnswer[subFormId][inVal])) {
                                                            removeError(
                                                                elem,
                                                                {
                                                                    'border' : 'none',
                                                                },
                                                                'error-msg'
                                                            )
                                                        } else {
                                                            addError(
                                                                elem,
                                                                {
                                                                    'border' : '1px solid #a94442'
                                                                },
                                                                'error-msg'
                                                            )
                                                        }
                                                    }
                                                    if(ownAnswer[subFormId][inputKey+inVal]) {
                                                        if(notEmpty(ownAnswer[subFormId][inputKey+inVal])) {
                                                            removeError(
                                                                elem,
                                                                {
                                                                    'border' : 'none',
                                                                },
                                                                'error-msg'
                                                            )
                                                        } else {
                                                            addError(
                                                                elem,
                                                                {
                                                                    'border' : '1px solid #a94442'
                                                                },
                                                                'error-msg'
                                                            )
                                                        }
                                                    }
                                                } else {
                                                    addError(
                                                        elem,
                                                        {
                                                            'border' : '1px solid #a94442'
                                                        },
                                                        'error-msg'
                                                    )
                                                }
                                            }
                                        }
                                    })
                                }
                                clearInterval(eventRulesInterval)
                            }
                        }, 700)
                    })
                }
            }
        }
    }

    if(typeof reloadStepValidationInputGlobal == 'undefined') {
        function reloadStepValidationInputGlobal(subFormId) {
            var inputKey = '';
            if(stepValidationInput && stepValidationInput[subFormId] && stepValidationInput[subFormId]['key']) {
                inputKey = stepValidationInput[subFormId]['key'];
            }
            if(inputKey != '')
                reloadInput(inputKey, subFormId)
        }
    }

    if(typeof bindBtnEditEvent == 'undefined') {
        function bindBtnEditEvent() {
            $(".editQuestion").off().on("click",function() {

                var formInputsHere = inputsList;
                //alert(JSON.stringify(inputsList[  $(this).data("form") ]));
                if( notNull(inputsList [ $(this).data("form")]) ){
                    formInputsHere = inputsList[  $(this).data("form") ];
                }


                var activeForm = {
                    "jsonSchema" : {
                        "title" : tradForm.modifyConfigQuestion,
                        "type" : "object",
                        "properties" : {
                            label : {
                                label : tradForm.titleQuestion,
                                value :  (typeof formInputsHere[ $(this).data("key") ]["label"] != "undefined") ? formInputsHere [ $(this).data("key") ]["label"] : ""
                            },
                            placeholder : {
                                label : tradForm.titleInQuestion,
                                value :  (typeof formInputsHere[ $(this).data("key") ]["placeholder"] != "undefined") ? formInputsHere [ $(this).data("key") ]["placeholder"] : ""
                            },
                            info : {
                                inputType : "textarea",
                                label : tradForm.additionalInfoQuestion,
                                value :  (typeof formInputsHere[ $(this).data("key") ]["info"] != "undefined") ? formInputsHere [ $(this).data("key") ]["info"] : "",
                                markdown : true,
                            },
                            type : { label : tradForm.typeOfQuestion,
                                    inputType : "select",
                                    options : <?php echo json_encode(Form::inputTypes()); ?>,
                                    value : (typeof formInputsHere[ $(this).data("key") ]["type"] != "undefined") ? formInputsHere [ $(this).data("key") ]["type"] : "text"
                            },
                            isRequired : {
                                "label": "Est-ce une question obligatoire ?",
                                "inputType": "checkboxSimple",
                                "params": {
                                    "onText": "Oui",
                                    "offText": "Non",
                                    "onLabel": "Oui",
                                    "offLabel": "Non",
                                    "labelText": "Est-ce une question obligatoire ?"
                                },
                                "checked": (typeof formInputsHere[ $(this).data("key") ]["isRequired"] != "undefined") ? (/true/i).test(formInputsHere[ $(this).data("key") ]["isRequired"]) : false,
                                "optionsValueAsKey": true
                            }
                        }
                    }
                };

                mylog.log("tplCtx formInputs activeForm.jsonSchema.properties",formInputsHere,activeForm.jsonSchema.properties);
                tplCtx.id = $(this).data("id");
                tplCtx.key = $(this).data("key");
                tplCtx.formParentId = $(this).data("parentid");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                dataSubformId = $(this).attr("data-form");
                tplCtx["updatePartial"] = true;
                mylog.log("tplCtx",tplCtx);
                //alert(tplCtx.collection+":"+tplCtx.path);

                var thisbtn = $(this);

                activeForm.jsonSchema.save = function () {
                    tplCtx.value = {};

                    tplCtx.value["label"] = $("#label").val();
                    $("#placeholder").val() ? tplCtx.value["placeholder"] = $("#placeholder").val() : "";
                    $("#info").val() ? tplCtx.value["info"] = $("#info").val() : "";
                    $("#type").val() ? tplCtx.value["type"] = $("#type").val() : "";
                    if($("#isRequired").val()){
                        tplCtx.value["isRequired"] = $("#isRequired").val();
                    }
                    if(thisbtn.data("position")){
                        tplCtx.value["position"] = thisbtn.data("position");
                    }

                    if(thisbtn.data("positions")){
                        tplCtx.value["positions"] = thisbtn.data("positions");
                    }

                    //alert("#"+tplCtx.key+" : "+$( "#"+tplCtx.key ).val());
                    mylog.log("save tplCtx",tplCtx);
                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.closeForm();
                            // $("#ajax-modal").modal('hide');
                            // urlCtrl.loadByHash( location.hash );
                            // reloadWizard();
                            if(typeof reloadInput != "undefined") {
                                if(params && params.elt && params.elt.inputs && params.elt.inputs[tplCtx.key]) {
                                    if(typeof formInputsfor != "undefined" && formInputsfor[dataSubformId]) {
                                        formInputsfor[dataSubformId][tplCtx.key] = JSON.parse(JSON.stringify(params.elt.inputs[tplCtx.key]))
                                    }
                                    if(inputsList && inputsList[dataSubformId]) {
                                        inputsList[dataSubformId][tplCtx.key] = JSON.parse(JSON.stringify(params.elt.inputs[tplCtx.key]))
                                    }
                                }
                                reloadInput(tplCtx.key, dataSubformId, () => {
                                    if(typeof bindBtnEditEvent != "undefined")
                                        bindBtnEditEvent()
                                })
                            }
                        } );
                    }
                }
                dyFObj.openForm( activeForm );
            });
        }
    }

jQuery(document).ready(function() {
    $('.tooltips').tooltip();

    buildValidationGlobal(formKey);

    $('.feedbackForm').off().click(function() {

        prioModal = bootbox.dialog({
            message: $(".feedback-form").html(),
            show: false,
            size: "large",
            onEscape: closePrioModal
        });
        prioModal.modal("show");

        prioModal.on('shown.bs.modal', function (e) {
            $.each([1,2,3,4,5], function(index,value){
                    if ($('.s'+value).hasClass("staractive")) {
                        $('.starlvl'+value).show();
                    }else {
                        $('.starlvl'+value).hide();
                    }
                });

            $('.star-r').mouseenter(function() {
                var lvl = $(this).data("level");
                $.each([1,2,3,4,5], function(index,value){
                    $('.starlvl'+value).hide();
                });
                $('.starlvl'+lvl).show();
            });

            $('.star-r').mouseleave(function() {
                $.each([1,2,3,4,5], function(index,value){
                    if ($('.s'+value).hasClass("staractive")) {
                        $('.starlvl'+value).show();
                    }else {
                        $('.starlvl'+value).hide();
                    }
                });
            });
            $('.star-r').click(function() {
                var ito = $(this);
                var lvl = ito.data("level");
                $.each([1,2,3,4,5], function(index,value){
                    $('.starlvl'+value).hide();
                });
                $.each($('.star-r'), function(){
                    $(this).removeClass("staractive");
                });
                ito.addClass("staractive");
                $('.starlvl'+lvl).show();
                $('.sendfeedb').show();
                $('.sendfeedb').data("starlvl",lvl);
            });

            $('.sendfeedb').click(function() {
                var subanswer = {
                    collection : "answers",
                    id : answerId
                };

                subanswer.value = {
                    date : new Date(),
                    star : $(this).data("starlvl")
                };

                subanswer.path = "feedback."+userId;

                dataHelper.path2Value( subanswer , function(params) {
                    toastr.success('Envoyé');
                    prioModal.modal('hide');
                    reloadWizard();
                } );
            });
        });
    });

    $('.submitansbtn').off().on("click",function(){
            // var answer = {
            //     collection : "answers",
            //     id : answerObj._id.$id
            // };

            // answer.path = "submited";

            // answer.value = "submited";


            // dataHelper.path2Value( answer , function(params) {
            //     toastr.success('Envoyé');
            //     reloadWizard();
            // } );
            dyFObj.openForm( sectionDyf.submitans);
    });

    $('.submitansconfigbtn').off().on("click",function(){
            dyFObj.openForm( sectionDyf.submitansconfig);
    });

    $.each($(".markdown"), function(k,v){
        descHtml = dataHelper.markdownToHtml($(v).html());
        $(v).html(descHtml);
    });

    mylog.log("render","/modules/survey/views/tpls/forms/formbuilder.php");

    $("#formQuest input").on('keyup keypress', function(e) {
          var keyCode = e.keyCode || e.which;
          if (keyCode === 13) {
            e.preventDefault();
            return false;
          }
        });

    //saveVisited();
    $("#questionList<?=@$formId ?> .saveOneByOne").unbind().blur( function( event ) {
        var tthis = $(this);
        event.preventDefault();
        const myKey = $(this).attr('id');
        var parentElem = $('li#question'+myKey);
        var errorElem = parentElem.find(`div.rules-container${myKey}`);
        const rulesAttr = parentElem.find(`div.rules-container${myKey}`).attr('data-rules');
        var rules = {};
        var strError = '';
        var hasError = false;
        if(typeof rulesAttr != 'undefined') {
            rules = JSON.parse(rulesAttr.replace(/'/g, '"'));
        }
        if(notEmpty(rules) && rules.width) {
            if(rules.width.min && rules.width.min > 0 && tthis.val().trim().length < rules.width.min) {
                strError = `<small class="text-danger">${rules.width.min} ${trad.minCharacters}</small> </br>`;
                hasError = true;
            }
            if(rules.width.min && rules.width.max && rules.width.max > 0 && tthis.val().trim().length > rules.width.max) {
                strError = `<small class="text-danger">${rules.width.max} ${trad.maxCharacters}</small> </br>`;
                hasError = true;
            }
        }
        if(notEmpty(rules) && rules.syntaxe) {
            if(rules.syntaxe && rules.syntaxe.errormsg && rules.syntaxe.errormsg != "" && rules.syntaxe.regexp && rules.syntaxe.regexp != "") {
                matchRegex = new RegExp(rules.syntaxe.regexp);
                if(tthis.val().trim().length > 0 && !tthis.val().trim().match(matchRegex)) {
                    strError += `<small class="text-danger">${ trad[rules.syntaxe.errormsg] ? trad[rules.syntaxe.errormsg] : rules.syntaxe.errormsg}</small>`;
                    hasError = true;
                }
            }
        }
        if(hasError) {
            tthis.css('border-color', '#a94442')
        } else {
            tthis.css('border-color', '#ddd')
        }
        errorElem.empty();
        errorElem.append(strError);
        //toastr.info('saving...'+$(this).attr("id"));
        //$('#openForm118').parent().children("label").children("h4").css('color',"green")
        if( notNull(answerObj)){

            var answer = {
                collection : "answers",
                id : answerObj._id.$id,
                path : "answers."+$(this).attr("id")
            };

            if(answerObj.form)
                answer.path = "answers."+$(this).data("form")+"."+$(this).attr("id");

            if($(this).attr("type") == "checkbox")
                answer.value = $(this).is(":checked");
            else
                answer.value = $(this).val().trim();

            mylog.log("saveOneByOne",$(this).attr("id"), answer );

            if(!hasError) {
            dataHelper.path2Value( answer , function(params) {
                if(tthis.prop("tagName") == "TEXTAREA" && tthis.css("display") == "none"){
                    tthis.parent().find(".editor-statusbar").prepend('&nbsp;<b class="text-success alert-saved"><i class="fa fa-check"></i><span class="cursor">'+trad.saved+'</span></b>');
                    setTimeout(function() { tthis.parent().find(".alert-saved").remove(); }, 1500);
                }else{
                   // toastr.success('Mise à jour enregistrée');
                }
                saveLinks(answerObj._id.$id,"updated",userId);
                if (typeof stepValidationReload<?php echo @$formId?> !== "undefined") {
                    stepValidationReload<?php echo @$formId?>();
                }
            } );
            }


        } else {
            toastr.error('answer cannot be empty, on saveOneByOne!');
        }
    });

    if(initValues){
        $.each(initValues, function(k,inp) {
            if(inp.type == "tags"){
                $("#"+k).select2({
                    "tags": inp.data ,
                    "tokenSeparators": [','],
                    //"minimumInputLength" : (inp.limit) ? inp.limit : 3,
                    "placeholder" : inp.placeholder
                });
            }
        })
    }



<?php if(@$canEdit) { ?>

    // var myOtherformInputsHere = formInputs;
    //
    // if( notNull(formInputs [ tplCtx.form ]) )
    //     myformInputsHere = formInputs[ tplCtx.form ];
    <?php if(isset($wizard)) { ?>
    var myWizard = '<?php echo $wizard ?>';
    <?php }else{ ?>
    var myWizard = '0';
    <?php } ?>


/*    if(myWizard === '1'){
        if (typeof dataformParent['type'] != 'undefined' && dataformParent['type'] == 'aap'){
            $.each(formInputs, function (index, value) {

                var arrayPosition = [];
                var arrayMissingV = 0;
                var i = 1;

                arrayPosition.push(0);

                $.each(value, function (indexNiv2, valueNiv2) {

                    if(notNull(valueNiv2) && typeof valueNiv2.positions != 'undefined' && notNull(valueNiv2.positions) && typeof valueNiv2.positions.dataformParent['_id']["$id"] != 'undefined' && notNull(valueNiv2.positions.dataformParent['_id']["$id"])){
                        arrayPosition.push(parseInt(valueNiv2.positions.dataformParent['_id']["$id"]));
                    } else {
                        arrayMissingV++;
                    }
                });

                var maxValueInArray = Math.max.apply(Math, arrayPosition);

                if(arrayMissingV !== 0){
                    $.each(value, function (indexNiv2, valueNiv2) {
                        if(arrayMissingV !== 0){
                            if(notNull(valueNiv2) && (typeof valueNiv2.positions == 'undefined' || (typeof valueNiv2.positions != 'undefined' && typeof valueNiv2.positions.dataformParent['_id']["$id"] == "undefined") )){
                                valueNiv2.positions = {};
                                valueNiv2.positions.dataformParent['_id']["$id"] = (maxValueInArray + i).toString();
                                i++;
                                arrayMissingV--;
                            }
                        }
                    });
                }

            });
        }else {
            $.each(formInputs, function (index, value) {

                var arrayPosition = [];
                var arrayMissingV = 0;
                var i = 1;

                arrayPosition.push(0);

                $.each(value, function (indexNiv2, valueNiv2) {

                    if (notNull(valueNiv2) && notNull(valueNiv2.position)) {
                        arrayPosition.push(parseInt(valueNiv2.position));
                    } else {
                        arrayMissingV++;
                    }
                });

                var maxValueInArray = Math.max.apply(Math, arrayPosition);

                if (arrayMissingV !== 0) {
                    $.each(value, function (indexNiv2, valueNiv2) {
                        if (arrayMissingV !== 0) {
                            if (notNull(valueNiv2) && !notNull(valueNiv2.position)) {
                                valueNiv2.position = (maxValueInArray + i).toString();
                                i++;
                                arrayMissingV--;
                            }
                        }
                    });
                }

            });
        }
    } else {

        if (typeof dataformParent['type'] != 'undefined' && dataformParent['type'] == 'aap'){

            var arrayPosition = [];
            var arrayMissingV = 0;
            var i = 1;

            arrayPosition.push(0);

            $.each(formInputs, function (index, value) {

                if (notNull(value) && typeof value.positions != "undefined"  && notNull(value.positions) && typeof value.positions.dataformParent['_id']["$id"] != "undefined" && notNull(value.positions.dataformParent['_id']["$id"])) {
                    arrayPosition.push(parseInt(value.positions.dataformParent['_id']["$id"]));
                } else {
                    arrayMissingV++;
                }
            });

            var maxValueInArray = Math.max.apply(Math, arrayPosition);

            if (arrayMissingV !== 0) {
                $.each(formInputs, function (index, value) {
                    if (arrayMissingV !== 0) {
                        if(notNull(value) && (typeof value.positions == 'undefined' || (typeof value.positions != 'undefined' && typeof value.positions.dataformParent['_id']["$id"] == "undefined")) ){
                            value.positions = {};
                            value.positions.dataformParent['_id']["$id"] = (maxValueInArray + i).toString();
                            i++;
                            arrayMissingV--;
                        }
                    }
                });
            }

        } else {

            var arrayPosition = [];
            var arrayMissingV = 0;
            var i = 1;

            arrayPosition.push(0);

            $.each(formInputs, function (index, value) {

                if (notNull(value) && notNull(value.position)) {
                    arrayPosition.push(parseInt(value.position));
                } else {
                    arrayMissingV++;
                }
            });

            var maxValueInArray = Math.max.apply(Math, arrayPosition);

            if (arrayMissingV !== 0) {
                $.each(formInputs, function (index, value) {
                    if (arrayMissingV !== 0) {
                        if (notNull(value) && !notNull(value.position)) {
                            value.position = (maxValueInArray + i).toString();
                            i++;
                            arrayMissingV--;
                        }
                    }
                });
            }

        }

    }*/

    //questions can be ordered by drag n drop
/*    $( ".dragNDrop" ).sortable({
        stop: function(event, ui) {
            //alert("New position: " + ui.item.index()+ ui.item.data("key")+ ui.item.data("form"));
            tplCtx.id = ui.item.data("id");
            tplCtx.collection = "forms";
            tplCtx.path = ui.item.data("path");
            tplCtx.key = ui.item.data("key");
            tplCtx.form = ui.item.data("form");
            tplCtx.value = ""+ui.item.index();
            //apres la ligne 186
            //il faut traverser tout les formInputs
            //et si formInputs.xxx.position est >= tplCtx.value alors
            //formInputs.xxx.position = formInputs.xxx.position+1

            mylog.dir(tplCtx );

              dataHelper.path2Value( tplCtx , function(params) {
                    toastr.success('move saved!');

                var myformInputsHere = formInputs;

                if( notNull(formInputs [ tplCtx.form ]) )
                    myformInputsHere = formInputs[ tplCtx.form ];

                    var distance =0;

                    if(parseInt(tplCtx.value) + 1 < parseInt(myformInputsHere[tplCtx.key]["position"]) ){ // Go up
                        distance = parseInt(myformInputsHere[tplCtx.key]["position"]) - parseInt(tplCtx.value);
                        distance = distance - 1;
                            $.each(myformInputsHere, function (index, value) {
                                if (distance !== 0){
                                    if (value.position >= (parseInt(tplCtx.value) + 1).toString()) {
                                      value.position = (parseInt(value.position) + 1).toString();
                                      distance--;
                                  }
                              }
                          });
                      myformInputsHere[tplCtx.key]["position"] = (parseInt(tplCtx.value)+1).toString();
                    }else { // Go down
                      distance = parseInt(tplCtx.value) - parseInt(myformInputsHere[tplCtx.key]["position"]);
                      distance = distance + 1;
                      for (let [key, value] of Object.entries(myformInputsHere).reverse()) {
                          if (distance !== 0){
                              if (value.position <= (parseInt(tplCtx.value) + 1).toString()) {
                                  value.position = (parseInt(value.position) - 1).toString();
                                  distance--;
                              }
                          }
                      }
                      myformInputsHere[tplCtx.key]["position"] = (parseInt(tplCtx.value)+1).toString();
                    }

                  tplCtx.path = "inputs";
                  tplCtx.value = myformInputsHere;
                  dataHelper.path2Value( tplCtx , function(params) {
                      toastr.success('edit saved!');
                  });


                  //formInputs[tplCtx.form][tplCtx.key]["position"] = tplCtx.value;
                    var clearDoublePos = false;
                    //if input exists on same position delete position attribute
                    $.each( formInputs[tplCtx.form], function(k,inp) {
                        mylog.log("incPositions",k,inp.position)
                        if( k != tplCtx.key && inp.position == tplCtx.value ){
                            clearDoublePos = true;

                            tplCtx.path = "inputs."+k+".position";
                            tplCtx.value = null;
                            dataHelper.path2Value( tplCtx , function(params) {
                                delete formInputs[tplCtx.form][k]["position"];
                            } );
                        }
                    })
                    if(clearDoublePos)
                        toastr.info('conflicting positions , please reload the page!');
                } );

        }
    });*/

    bindBtnEditEvent();

    $(".editQuestionaap").off().on("click",function() {

        var eqb = $(this);

       var activeForm = {
            "jsonSchema" : {
                "title" : tradForm.modifyConfigQuestion,
                "type" : "object",
                "properties" : {
                    label : {
                        label : tradForm.titleOfQuestion,
                        value :  (typeof eqb.data("label") != "undefined") ? eqb.data("label") : ""
                    },
                    placeholder : {
                        label : tradForm.titleInQuestion,
                        value :  (typeof eqb.data("placeholder") != "undefined") ? eqb.data("placeholder") : ""
                    },
                    info : {
                        inputType : "textarea",
                        label : tradForm.additionalInfoQuestion,
                        value :  (typeof eqb.data("info") != "undefined") ? eqb.data("info") : ""
                    }
                }
            }
        };

        tplCtx.id = eqb.data("id");
        tplCtx.collection = eqb.data("collection");
        tplCtx.formParentId = eqb.data("parentid");
        tplCtx.path = eqb.data("path");
        tplCtx["updatePartial"] = true;
        mylog.log("tplCtx",tplCtx);
        //alert(tplCtx.collection+":"+tplCtx.path);
        activeForm.jsonSchema.save = function () {
            tplCtx.value = {};

            tplCtx.value["label"] = $("#label").val();
            tplCtx.value["placeholder"] = $("#placeholder").val();
            tplCtx.value["info"] = $("#info").val();
            tplCtx.value["type"] = eqb.data("type");

            if(eqb.data("position")){
                tplCtx.value["position"] = eqb.data("position");
            }

            if(eqb.data("positions")){
                tplCtx.value["positions"] = eqb.data("positions");
            }

            mylog.log("save tplCtx",tplCtx);
            if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
            else {
                dataHelper.path2Value( tplCtx, function(params) {
                    $("#ajax-modal").modal('hide');
                    // urlCtrl.loadByHash( location.hash );
                    reloadWizard();
                } );
            }
        }
        dyFObj.openForm( activeForm );
    });

    $(".addQuestionaap").off().on("click",function() {
        var activeForm = {
            "jsonSchema" : {
                "title" : tradForm.addQuestion,
                "type" : "object",
                "properties" : {
                    label : { label : tradForm.titleOfQuestion
                    },
                    placeholder : { label : tradForm.titleInQuestion },
                    info : {
                        inputType : "textarea",
                        label : tradForm.additionalInfoQuestion,
                        markdown : true
                    },
                    type : { label : tradForm.typeOfQuestion,
                        inputType : "select",
                        options : <?php echo json_encode(Form::inputTypes()); ?>,
                        value : "text"
                    }
                }
            }
        };

        tplCtx.id = $(this).data("id");
        tplCtx.collection = "<?php echo Form::INPUTS_COLLECTION; ?>";
        tplCtx.form = $(this).data("form");
        tplCtx.formParentId = $(this).data("formparentid");

        activeForm.jsonSchema.save = function () {

            //var inputCt = nextValuekeyform;
            var inputCt = Date.now().toString(36) + Math.random().toString(36).substring(2);

            if(notNull(inputsList[tplCtx.form][tplCtx.form+inputCt]))
                inputCt = inputCt+"x";
            //alert("inputs."+tplCtx.form+inputCt);i

            tplCtx.path = "inputs."+tplCtx.form+inputCt;
            tplCtx.inputId = tplCtx.form+inputCt;
            tplCtx.value = {
                label : $("#label").val(),
                type : $("#type").val()
            };
            if( $("#placeholder").val() != "" )
                tplCtx.value.placeholder = $("#placeholder").val();
            if( $("#info").val() != "" )
                tplCtx.value.info = $("#info").val();


            delete tplCtx.form;
            mylog.log("activeForm save tplCtx",tplCtx);
            if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
            else {
                dataHelper.path2Value( tplCtx, function(params) {
                    if(tplCtx.value.type == "tpls.forms.cplx.openDynform" ){
                        delete tplCtx.path;
                        delete tplCtx.id;
                        tplCtx.value = {
                            id : tplCtx.inputId,
                            name : tplCtx.value.label,
                            type : "dynform"
                        };
                        mylog.log("create openDynform save tplCtx",tplCtx);
                        dataHelper.path2Value( tplCtx, function(params) {
                            $("#ajax-modal").modal('hide');
                            // urlCtrl.loadByHash(location.hash);
                            reloadWizard();
                        } );

                    } else {
                        $("#ajax-modal").modal('hide');
                        // urlCtrl.loadByHash(location.hash);
                        reloadWizard();
                    }

                } );
            }

        }

        dyFObj.openForm( activeForm );
    });

    $(".addQuestion").off().on("click",function() {
        var activeForm = {
            "jsonSchema" : {
                "title" : tradForm.addQuestion,
                "type" : "object",
                "properties" : {
                    label : { label : tradForm.titleOfQuestion
                },
                    placeholder : { label : tradForm.titleInQuestion },
                    info : {
                        inputType : "textarea",
                        label : tradForm.additionalInfoQuestion,
                        markdown : true
                    },
                    type : { label : tradForm.typeOfQuestion,
                             inputType : "select",
                             options : <?php echo json_encode(Form::inputTypes()); ?>,
                             value : "text"
                    },
                    isRequired : {
                        "label": "Est-ce une question obligatoire ?",
                        "inputType": "checkboxSimple",
                        "params": {
                            "onText": "Oui",
                            "offText": "Non",
                            "onLabel": "Oui",
                            "offLabel": "Non",
                            "labelText": "Est-ce une question obligatoire ?"
                        },
                        "optionsValueAsKey": true
                    }
                }
            }
        };

        tplCtx.id = $(this).data("id");
        tplCtx.formParentId = $(this).data("parentid");
        tplCtx.key = $(this).data("key");
        tplCtx.collection = "<?php echo Form::COLLECTION ?>";
        tplCtx.form = $(this).data("form");

        activeForm.jsonSchema.save = function () {
            var formInputsHere = formInputsfor;
            if(typeof formInputsfor[tplCtx.form] != "undefined" && notNull(formInputsfor[tplCtx.form]) ) {
                formInputsHere = formInputsfor[tplCtx.form];
            }

            // var inputCt = (formInputsHere == null) ? "1" : (Object.keys(formInputsHere).length+1);
            // if(notNull(formInputsHere[tplCtx.form+inputCt]))
            //     inputCt = inputCt+"x";

            var inputCt = Date.now().toString(36) + Math.random().toString(36).substring(2);

            //var inputCt = nextValuekeyform;
            /*if(typeof formInputsHere[tplCtx.form+inputCt] != "undefined")
                inputCt = inputCt+"x";*/
            //alert("inputs."+tplCtx.form+inputCt);i

            tplCtx.path = "inputs."+tplCtx.form+inputCt;
            tplCtx.inputId = tplCtx.form+inputCt;
            tplCtx.value = {
                label : $("#label").val(),
                type : $("#type").val()
            };
            if( $("#placeholder").val() != "" )
                tplCtx.value.placeholder = $("#placeholder").val();
            if( $("#info").val() != "" )
                tplCtx.value.info = $("#info").val();


            delete tplCtx.form;
            mylog.log("activeForm save tplCtx",tplCtx);
            if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
            else {
                dataHelper.path2Value( tplCtx, function(params) {
                    if(tplCtx.value.type == "tpls.forms.cplx.openDynform" ){
                        delete tplCtx.path;
                        delete tplCtx.id;
                        tplCtx.value = {
                            id : tplCtx.inputId,
                            name : tplCtx.value.label,
                            type : "dynform"
                        };
                        mylog.log("create openDynform save tplCtx",tplCtx);
                        dataHelper.path2Value( tplCtx, function(params) {
                            $("#ajax-modal").modal('hide');
                            // urlCtrl.loadByHash(location.hash);
                            reloadWizard();
                        } );

                    } else {
                        $("#ajax-modal").modal('hide');
                        // urlCtrl.loadByHash(location.hash);
                        reloadWizard();
                    }

                } );
            }

        }

        dyFObj.openForm( activeForm );
    });
<?php } ?>

});

function saveLinks(id,linkType,uid,callB){
    var today = new Date();
    today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear()+ ' ' + today.getHours()+ ':' + today.getMinutes();
    //alert(uid+":"+today);
    if( typeof answerObj.links != "undefined" &&
        typeof answerObj.links[linkType] != "undefined" &&
        typeof answerObj.links[linkType][uid] != "undefined" //||typeof answerObj.links[linkType][uid] != today
        )
    {
        var incVal = parseInt(answerObj.links[linkType][uid].inc)+1 ;
        var link = {
            collection : "answers",
            id : answerObj._id.$id,
            path : "links."+linkType+"."+uid,
            value : {
                date : today,
                name : userConnected.name,
                inc : incVal
            }
        };

        mylog.log("saveLinks", link );

        dataHelper.path2Value( link , function(params) {
            mylog.log("saveLinks saved",params );
            if(typeof callB == "function" )
                callB();
        } );
    } else if(typeof callB == "function" )
        callB();
}

/*function saveVisited(){

    var incVal = (jsonHelper.notNull("answerObj.visits")) ? parseInt(answerObj.visits)+1 : 1;
    var view = {
        collection : "answers",
        id : answerObj._id.$id,
        path : "visits",
        value : incVal
    };

    mylog.log("saveVisited", view );

    dataHelper.path2Value( view , function(params) {
        mylog.log("saveVisited saved",params );
    } );

}*/

function reloadInput(key, formId, _reloadCallback, showLoader=false, thisExtraParams = {}){
    if(showLoader){
        coInterface.showLoader("#question"+key);
    }
    var reloadCall;
    if(typeof _reloadCallback !== 'undefined' && typeof _reloadCallback == 'function'){
        reloadCall = function(){

            _reloadCallback();

            $('.deleteLine').off().click( function(){
              formId = $(this).data("id");
              key = $(this).data("key");
              pathLine = $(this).data("path");
              collection = $(this).data("collection");
              if (typeof $(this).data("parentid") != "undefined") {
                  var parentfId = $(this).data("parentid");
              }
              bootbox.dialog({
                  title: trad.confirmdelete,
                  message: "<span class='text-red bold'><i class='fa fa-warning'></i> "+trad.actionirreversible+"</span>",
                  buttons: [
                    {
                      label: "Ok",
                      className: "btn btn-primary pull-left",
                      callback: function() {
                        var formQ = {
                                value:null,
                                collection : collection,
                                id : formId,
                                path : pathLine
                              };

                    if (typeof parentfId != "undefined") {
                        formQ["formParentId"] = parentfId;
                    }

                          dataHelper.path2Value( formQ , function(params) {
                                $("#"+key).remove();
                                //location.reload();
                                if(typeof formInputsfor != "undefined" && formInputsfor[formId] && formInputsfor[formId][key.replace(/question/g, "")]) {
                                    delete formInputsfor[formId][key.replace(/question/g, "")]
                                }
                            } );
                      }
                    },
                    {
                      label: "Annuler",
                      className: "btn btn-default pull-left",
                      callback: function() {}
                    }
                  ]
              });
            });
            $('.tooltips').tooltip();
        };
    }else{
        reloadCall = function(){
            $('.deleteLine').off().click( function(){
              formId = $(this).data("id");
              key = $(this).data("key");
              pathLine = $(this).data("path");
              collection = $(this).data("collection");
              if (typeof $(this).data("parentid") != "undefined") {
                  var parentfId = $(this).data("parentid");
              }
              bootbox.dialog({
                  title: trad.confirmdelete,
                  message: "<span class='text-red bold'><i class='fa fa-warning'></i> "+ trad.actionirreversible +"</span>",
                  buttons: [
                    {
                      label: "Ok",
                      className: "btn btn-primary pull-left",
                      callback: function() {
                        var formQ = {
                                value:null,
                                collection : collection,
                                id : formId,
                                path : pathLine
                              };

                    if (typeof parentfId != "undefined") {
                        formQ["formParentId"] = parentfId;
                    }

                          dataHelper.path2Value( formQ , function(params) {
                                $("#"+key).remove();
                                //location.reload();
                                if(typeof formInputsfor != "undefined" && formInputsfor[formId] && formInputsfor[formId][key.replace(/question/g, "")]) {
                                    delete formInputsfor[formId][key.replace(/question/g, "")]
                                }
                            } );
                      }
                    },
                    {
                      label: "Annuler",
                      className: "btn btn-default pull-left",
                      callback: function() {}
                    }
                  ]
              });
            });
            $('.tooltips').tooltip();
        };
    };

    let thisEditQuestionBtn = `
        <a class="btn btn-xs btn-danger descSec${key} editQuestion" href="javascript:;" data-position="${thisExtraParams.position ? thisExtraParams.position : 'null'}" data-positions="${thisExtraParams.positions ? thisExtraParams.positions : 'null'}" data-form="${formId}" data-id="${thisExtraParams.idSubForm ? thisExtraParams.idSubForm : 'null'}" data-collection="forms" data-key="${key}" data-path="inputs.${key}" data-parentid="${idParentForm ? idParentForm : 'null'}"><i class="fa fa-pencil"></i></a>
        <a class="btn btn-xs btn-danger deleteLine" href="javascript:;" data-id="${thisExtraParams.idSubForm ? thisExtraParams.idSubForm : 'null'}" data-collection="forms" data-key="question${key}" data-path="inputs.${key}" data-parentid="${idParentForm ? idParentForm : 'null'}"><i class="fa fa-trash"></i></a>
    `;

    var reloadinputData = {
        "answerId" : "<?php echo (string)@$answer["_id"] ?>",
        "key" : key,
        "formId" : formId,
        "wizard" : "<?php echo @$wizard ?>",
        "mode" : "<?php echo @$mode ?>",
        "canEdit" : <?php echo(isset($canEdit) && $canEdit != "" ? $canEdit : "false"); ?>,
        "canEditForm" : <?php echo(isset($canEditForm) && $canEditForm != "" ? $canEditForm : "false"); ?>,
        "canAdminAnswer" : <?php echo(isset($canAdminAnswer) && $canAdminAnswer != "" ? $canAdminAnswer : "false"); ?>,
        "canAnswer" : <?php echo(isset($canAnswer) && $canAnswer != "" ? $canAnswer : "false"); ?>,
        "saveOneByOne" : <?php echo(isset($saveOneByOne) && $saveOneByOne != "" ? $saveOneByOne : "false"); ?>,
        "contextId" : "<?php echo(isset($contextId) && $contextId != "" ? $contextId : ""); ?>",
        "contextType" : "<?php echo(isset($contextType) && $contextType != "" ? $contextType : ""); ?>",
        "editQuestionBtn" : $(`#question${key} .editQuestion`).length > 0 && $(`#question${key} .deleteLine`).length > 0 ?  (" "+$(`#question${key} .editQuestion`)[0].outerHTML + "\n" + $(`#question${key} .deleteLine`)[0].outerHTML).replace(/\'/g, '"') : (notEmpty(thisExtraParams) ? thisEditQuestionBtn.replace(/\'/g, '"') : <?php echo json_encode(isset($editQuestionBtn) && $editQuestionBtn != "" ? str_replace("'" , '"', $editQuestionBtn) : ""); ?>),
        "titleColor" : "<?php echo(isset($p["titleColor"]) && $p["titleColor"] != "" ? $p["titleColor"] : "black"); ?>"
    }

    if (reloadinputData.contextType == "") {
        delete reloadinputData.contextType;
    }
    if (reloadinputData.contextId == "") {
        delete reloadinputData.contextId;
    }

    ajaxPost(
        "#question"+key,
        baseUrl+"/survey/answer/reloadinput",
        reloadinputData,
        reloadCall,
        reloadCall
        ,"html");
}

function scrollintoDiv(div, t_out){
    var selector = "#"+div+ `,#${div.replace("_", "")}`;
    $([document.documentElement, document.body]).animate({
        scrollTop: $(selector).offset().top - (( $(window).height() - $(selector).outerHeight(true) ) / 2)
    }, 500);
    // $("#"+div ).attr('data-colortitle', '"<?php //echo (isset($this->costum["colors"]["pink"])) ? $this->costum["colors"]["pink"] : "#d9534f"?>"');
    $(selector ).get(0).style.setProperty("--colortitle", "<?php echo (isset($this->costum["colors"]["pink"])) ? $this->costum["colors"]["pink"] : "#d9534f"?>");

    $( selector ).addClass('div-pulse');
    setTimeout(function() {
        $( selector ).removeClass('div-pulse');
    }, t_out);
}

</script>

<?php }
    if(isset($canEditForm) && $canEditForm) {
        HtmlHelper::registerCssAndScriptsFiles(array(
            '/js/inputDraggable.js',
        ), Yii::app()->getModule(Survey::MODULE)->getAssetsUrl());
    }
?>
