<?php
$ignore = array('_file_', '_params_', '_obInitialLevel_' ,'ignore');
$params = array_diff_key(get_defined_vars() + array_flip($ignore), array_flip($ignore));
HtmlHelper::registerCssAndScriptsFiles(["/css/aap/aap.css"], Yii::app()->getModule('co2')->assetsUrl);
HtmlHelper::registerCssAndScriptsFiles(array(
    '/css/newFormWizard.css',
), Yii::app()->getModule(Survey::MODULE)->getAssetsUrl());
?>

<style type="text/css">

</style>
<div class="">
        <?php
           $params["mode"] = "r";
           $access = Form::getParamsWizard($params["parentForm"], $params["answer"], $params["isStandalone"] , $params["configForm"] );
           if(!in_array($step["step"], $access["disabledStepsId"]) && !in_array($step["step"], $access["hiddenStepsId"])){
               echo $this->renderPartial( "survey.views.".$params["input"]["tpl"] , $params , true );
           }
        ?>
    </div>

</div>
</div>

<script type="application/javascript">
    if(typeof coForm_FormWizardParams == "undefined") {
        var coForm_FormWizardParams = <?php echo json_encode($params); ?>;
    }
    jQuery(document).ready(function() {
        $(".ap-starter-btn-sheet").off().click(function() {
            aapObj.common.modalProposalDetail(aapObj, coForm_FormWizardParams.answer._id.$id , true );
            $(".portfolio-modal .modal-content").css({
                cssText : `
                            border-radius: 10px;
                            overflow-y: unset;
                            `
            });
            return false;
        });
    });
</script>
