<?php if($answer){	

    $keyTpl = "adherentFinder"; 

    if( $mode != "pdf" and $mode != "r"){
        $editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";
    }
    
    $elementType=[
        Organization::COLLECTION => "Organization",
        Person::COLLECTION 		 => "Person"
    ];
    
    $paramsData = [ 
        "type" => Organization::COLLECTION,
        "filter"   => [],
        "notSourceKey" => true,
        "myContacts"   => true,
        "elementLabel" => "adhérent",
        "field" => "element",
        "multiple" => false,
        "addNew" => true,
        "invite" => false
    ];

    if(isset($parentForm["params"][$kunik])){
        foreach ($paramsData as $e => $v) {
            if ( isset($parentForm["params"][$kunik][$e]) ) {
                $paramsData[$e] = $parentForm["params"][$kunik][$e];
            }
        }
    }
?>	

<style>
    .btn-success{
        background-color: #00c07b;
        border-color: #00c07b;
        padding: 10px 15px !important;
        border-radius: 3px !important;
        display: inline;
        height: 40px;
    }
</style>
	
<label>
    <h4><?php echo $label.$editQuestionBtn.$editParamsBtn ?></h4>
</label>
	
<script type="text/javascript">
    if(typeof answerPaymentData == "undefined"){
        var answerPaymentData = <?= json_encode($answer) ?>;
    }
	
$(document).ready(function() { 
    var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answer["answers"][$kunik])) ? $answer["answers"][$kunik] : null ); ?>;
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

    var step=<?php echo json_encode($form["id"]) ?>;
    var ansKey<?php echo $kunik ?>= <?php echo json_encode($kunik) ?> ;

    var answers= <?php echo json_encode( (isset($answer["answers"][$form["id"]][$kunik])) ? $answer["answers"][$form["id"]][$kunik] :null ); ?>;

    finder.addInForm<?= $kunik ?> = function(keyForm, id, type, name, img, data,firstLoad=false){
		mylog.log("finder.addInForm", keyForm, id, type, name, img);
		img= (notEmpty(img)) ? baseUrl + img : modules.co2.url + "/images/thumb/default_"+type+".png";
		//img= (img != "") ? img : modules.co2.url + "/images/thumb/default_"+type+".png";
		var str="";
		str="<div class='col-xs-12 element-finder element-finder-"+id+" shadow2 padding-10'>"+
					'<img src="'+ img+'" class="img-circle pull-left margin-right-10" height="35" width="35">'+
					'<span class="info-contact pull-left margin-right-5">' +
						'<span class="name-contact text-dark text-bold">' + name + '</span>'+
						'<br/>'+
						'<span class="cp-contact text-light pull-left">' + trad[((type == "citoyens" ) ? "citizens" : type)]+ '</span>'+
					'</span>' +
					'<button class="bg-red text-white pull-right" style="border: none;font-size: 15px;border-radius: 6px;padding: 5px 10px !important;" onclick="finder.removeFromForm<?php echo $kunik ?>(\''+keyForm+'\', \''+id+'\')"><i class="fa fa-times"></i></button>'+
			"</div>";
		$(".finder-"+keyForm+" .form-list-finder").append(str);

		finder.object[keyForm]={};
		finder.object[keyForm][id]={"type" : type, "name" : name};


		if(notNull(finder.finderPopulation[keyForm]) && notNull(finder.finderPopulation[keyForm][id]) && notNull(finder.finderPopulation[keyForm][id].email)){
			finder.object[keyForm][id].email = finder.finderPopulation[keyForm][id].email;
		}

		if(notNull(finder.roles) && notNull(finder.roles[keyForm])){
			finder.object[keyForm][id].roles = finder.roles[keyForm];
		}

		if(notNull(finder.search) && notNull(finder.search[keyForm]) && notNull(finder.search[keyForm].filterBy)){
			
			mylog.log("filterBy split", split, finder.selectedItems[keyForm][id][finder.search.filterBy], notNull( finder.selectedItems[keyForm][id][finder.search.filterBy]) );
			if(notNull( finder.selectedItems[keyForm][id][finder.search.filterBy] ) ) {
				var fBy = {} ;
				var split = id.split(".");
				fBy[finder.search[keyForm].filterBy] = finder.selectedItems[keyForm][id][finder.search.filterBy] ;
				finder.object[keyForm][split[0]]=Object.assign({}, finder.object[keyForm][id], fBy);
				delete finder.object[keyForm][id];
			}
		}

		//if(!notNull(answers) || typeof answers[id]=="undefined"){
		if(firstLoad==false){	
			var valData = {
				"id" : id,
				"name" : name,
				"type" : type
			};

			var answer={};

			// let elKey=$("."+keyForm+"finder").parent().attr("data-key");
			// elKey="<?php echo $keyTpl ?>"+elKey;
			answer.path = "answers.<?= $form["id"] ?>.<?= $kunik ?>.adherent";
			answer.collection = "answers" ;
	        answer.id = "<?php echo $answer["_id"]; ?>";
	        answer.value = valData;
	        dataHelper.path2Value(answer , function(params) { 
				if(typeof answerPaymentData["answers"] == "undefined"){
					answerPaymentData["answers"] = {"<?= $form["id"] ?>":{"<?= $kunik ?>":{"adherent":valData}}}
				}
				if(typeof answerPaymentData["answers"]["<?= $form["id"] ?>"] == "undefined"){
					answerPaymentData["answers"]["<?= $form["id"] ?>"] = {"<?= $kunik ?>":{"adherent":valData}}
				}
				if(typeof answerPaymentData["answers"]["<?= $form["id"] ?>"]["<?= $kunik ?>"] == "undefined"){
					answerPaymentData["answers"]["<?= $form["id"] ?>"]["<?= $kunik ?>"] = {"adherent":valData}
				}
				answerPaymentData["answers"]["<?= $form["id"] ?>"]["<?= $kunik ?>"] = {"adherent":valData}
	        });

        }
	};	

	finder.removeFromForm<?php echo $kunik ?> = function(keyForm, id){
		$(".finder-"+keyForm+" .form-list-finder .element-finder-"+id).remove();

		var answer={};
		answer.path = "answers.<?= $form["id"] ?>.<?= $kunik ?>.adherent";
		answer.collection = "answers" ;
		answer.value=null;
	    answer.id = "<?php echo $answer["_id"]; ?>";
	    dataHelper.path2Value(answer , function(params) { 
			toastr.success('Effacé.e');
				if (typeof stepValidationReload<?php echo @$formId?> !== "undefined") {
					stepValidationReload<?php echo @$formId?>();
					if(typeof answerPaymentData["answers"] == "undefined"){
					answerPaymentData["answers"] = {"<?= $form["id"] ?>":{"<?= $kunik ?>":{"adherent":null}}}
				}
				if(typeof answerPaymentData["answers"]["<?= $form["id"] ?>"] == "undefined"){
					answerPaymentData["answers"]["<?= $form["id"] ?>"] = {"<?= $kunik ?>":{"adherent":null}}
				}
				if(typeof answerPaymentData["answers"]["<?= $form["id"] ?>"]["<?= $kunik ?>"] == "undefined"){
					answerPaymentData["answers"]["<?= $form["id"] ?>"]["<?= $kunik ?>"] = {"adherent":null}
				}
				answerPaymentData["answers"]["<?= $form["id"] ?>"]["<?= $kunik ?>"] = {"adherent":null}
			}
		});
	};

	finder.addSelectedToForm=  function(keyForm, multiple){
		mylog.log("finder.addSelectedToForm", keyForm, multiple);
		var kunik = $("."+keyForm+"finder").parent().data("key");
		kunik="<?= $keyTpl ?>"+kunik ;

		var valueToSave = {};
		if(Object.keys(finder.selectedItems[keyForm]).length > 0){
			if(!multiple){
				finder.object[keyForm]={};
				$(".finder-"+keyForm+" .form-list-finder").html("");
			}
			$.each(finder.selectedItems[keyForm], function(e, v){
				typeCol=(typeof v.collection != "undefined") ? v.collection : v.type; 
				valueToSave = {id:e, type:typeCol, name:v.name, email:(v.email||userConnected.email), logo:v.profilThumbImageUrl};
				finder["addInForm"+kunik](keyForm, e, typeCol, v.name, v.profilThumbImageUrl);
			});
			if(typeof finder.callback[keyForm] != "undefined") finder.callback[keyForm](finder.selectedItems[keyForm]);
		}

		var answer={};
		answer.path = "answers.<?= $form["id"] ?>.<?= $kunik ?>.adherent";
		answer.collection = "answers" ;
		answer.value= valueToSave;
	    answer.id = "<?php echo $answer["_id"]; ?>";
	    dataHelper.path2Value(answer , function(params) { 
			if(typeof answerPaymentData["answers"] == "undefined"){
				answerPaymentData["answers"] = {"<?= $form["id"] ?>":{"<?= $kunik ?>":{"adherent":valueToSave}}}
			}
			if(typeof answerPaymentData["answers"]["<?= $form["id"] ?>"] == "undefined"){
				answerPaymentData["answers"]["<?= $form["id"] ?>"] = {"<?= $kunik ?>":{"adherent":valueToSave}}
			}
			if(typeof answerPaymentData["answers"]["<?= $form["id"] ?>"]["<?= $kunik ?>"] == "undefined"){
				answerPaymentData["answers"]["<?= $form["id"] ?>"]["<?= $kunik ?>"] = {"adherent":valueToSave}
			}
			answerPaymentData["answers"]["<?= $form["id"] ?>"]["<?= $kunik ?>"] = {"adherent":valueToSave}

			//reloadInput("<?= $key ?>", "<?= (string)$form["_id"] ?>");
		});
	};

	bindAddNew<?php echo $kunik ?> = function(){
		$(".add-new-element").off().on("click", function(){
			var filters = $(this).data("filters");
			var field = $(this).data("field");
			mylog.log("filters",filters);
			//filters=JSON.parse(filters);
			//var category = $(this).data("category");
			var type = $(this).data("type").slice(0, -1);

			var customForm = {
					"beforeBuild" : {
			            "properties" : {
			                        
			            }    
					}

				};
			//mylog.log("category",category);
			
			if(typeof filters!="undefined" && typeof filters=="object"){
				$.each(filters,function(k,v){
					customForm.beforeBuild.properties[v.attributeName] = {
						inputType : "text",
						label : v.attributeName,
						value : v.valueName,
						class :" form-control"
					};
				});

			}

			customForm.afterSave = function(data){
				mylog.log("afterSave reference", data);
				$("#ajax-modal").modal('hide');
				finder.addInForm<?php echo $kunik ?>(sectionDyf.<?php echo $kunik ?>ParamsData.field,data.map._id.$id,data.map.collection,data.map.name);
	               reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");

				if(field=="thirdPlace"){
					ajaxPost(
						null,
						baseUrl+"/"+moduleId+"/admin/setsource/action/add/set/reference",
						{id:data.id,type:data.map.collection,sourceKey:"franceTierslieux"},
						function(data){
							mylog.log("reférencé!",data);
						}
					);

					ajaxPost(
						null,
						baseUrl+"/"+moduleId+"/admin/setsource/action/add/set/reference",
						{id:data.id,type:data.map.collection,sourceKey:"tiersLieux"},
						function(data){
							mylog.log("reférencé!",data);
						}
					);
				}	
					
			};			

			mylog.log("customForm",customForm);
			dyFObj.openForm(type,null,null, null, customForm);
			$(".bootbox").modal('hide');
				
		});



	};


	mylog.log("answers ready",answers);
	mylog.log("finder paramsData<?php echo $kunik?>",sectionDyf.<?php echo $kunik ?>ParamsData);

		finder.searchAndPopulateFinder = function(keyForm, text, typeSearch, multiple){
			mylog.log("finder.searchAndPopulateFinder", keyForm, text, typeSearch, multiple);
			//finder.isSearching=true;

			var dataSearch = {
	        	searchType : typeSearch, 
	        	name: text
	        };

	        if(notNull(finder.search) && notNull(finder.search[keyForm])){
	        	dataSearch = Object.assign({}, dataSearch, finder.search[keyForm]);
	        }
			if(finder.filters[keyForm]){
				dataSearch['filters'] = finder.filters[keyForm];
			}
	  		$.ajax({
				type: "POST",
		        url: baseUrl+"/"+moduleId+"/search/globalautocomplete",
		        data: dataSearch,
		        dataType: "json",
		        success: function(retdata){
		        	mylog.log("retdata",retdata);
		        	mylog.log("finder.extraParams[keyForm].addNew",finder.extraParams[keyForm].addNew);
		        	mylog.log("etdata.results.length",retdata.results.length);
			        	mylog.log("finder.invite[keyForm]",finder.invite[keyForm]);
		        	if(!retdata){
		        		toastr.error(retdata.content);
		        	} else {
			        	if(retdata.results.length == 0 && (finder.invite[keyForm] == true || finder.invite[keyForm]=="true" || (notNull(finder.invite[keyForm]) && typeof finder.invite[keyForm]=="object"))){
			        		$("#form-invite").removeClass("hidden");
			        		$("#list-finder-selection").addClass("hidden");

			        		var search =  "#finderSelectHtml #populateFinder" ;
			        		var id = "#finderSelectHtml #form-invite" ; 
			        		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
							if(emailReg.test( $(search).val() )){
								$(id+' #inviteEmail').val( $(search).val());
								var nameEmail = $(search).val().split("@");
								$(id+" #inviteName").val(nameEmail[0]);
							}else{
								$(id+" #inviteName").val($(search).val());
								$(id+" #inviteEmail").val("");
							}

							var invitor = (typeof userConnected !="undefined" && userConnected!=null) ? userConnected.name : "un.e citoyen.ne"; 

							$(id+" #inviteText").val("Vous avez été invité.e par "+invitor+ " au sein du collectif \"Les Communs des Tiers-Lieux \" en tant que personne intéressée par le défi suivant : "+window.location.href+".mode.r");
							$("#btnInviteNew").text("Inviter");

							finder.invite[keyForm]={};
							finder.invite[keyForm].callback=function(data){
								mylog.log("invite callback", data)
			        			var listInvite={};
			        			listInvite.invites={};
			        			listInvite.invites[data.id] = { 
			        				name : data.name,
			        				profilThumbImageUrl : parentModuleUrl + "/images/thumb/default_"+data.type+".png",
			        				mail : data.mail,
			        				msg : data.msg
			        			} 
			        			var params = {
			        				parentId : costum.contextId,
			        				parentType : costum.contextType,
			        				listInvite : listInvite
			        			};
			        			
			        			 ajaxPost("",baseUrl+'/'+moduleId+"/link/multiconnect",params,function(data){
			        			 		var invited=(typeof trad.keyForm!="undefined") ? trad.keyForm : keyForm;
			        			 		toastr.success(invited +' bien invité.e');
			        			 	},
			        			 	function(data){
			        			 		toastr.error(trad.somethingwrong);
			        			 	}
			        			); 	
			        		};

		        		} else if (retdata.results.length == 0 && (finder.extraParams[keyForm].addNew=="true" || finder.extraParams[keyForm].addNew==true)){
		        				$("#form-invite").addClass("hidden");
		        				$("#list-finder-selection .population-elt-finder").hide();	
		        				var addNew='<a href="javascript:;" data-type="'+finder.typeAuthorized[keyForm][0]+'" data-field="'+keyForm+'" data-filters='+JSON.stringify(finder.search[keyForm].filters)+' class=" add-new-element text-red"><i class="fa fa-more "></i> <span>Ajouter un(e) '+keyForm+'</span></a>';
		        				if($('#finderSelectHtml .add-new-element').length == 0) {
		        					$("#list-finder-selection").prepend(addNew);
		        					var inputName=$("#"+keyForm).parent().parent().parent().data("key");
		        					inputName="<?php echo $keyTpl ?>"+inputName;
		        					window["bindAddNew"+inputName]();
		        				}else{
		        					$('#finderSelectHtml .add-new-element').removeClass("hidden");
		        				}	
		        				
		        		}
		        		
		        		else{
		        			$("#form-invite").addClass("hidden");
		        			$('#finderSelectHtml .add-new-element').addClass("hidden")
		        			$("#list-finder-selection").removeClass("hidden");
			        		finder.populateFinder(keyForm, retdata.results, multiple);
		        		}
		  			}
				}	
			});
		};

		var filters = {};
		if(typeof sectionDyf.<?php echo $kunik ?>ParamsData.filter!="undefined" && typeof sectionDyf.<?php echo $kunik ?>ParamsData.filter=="object"){
			$.each(sectionDyf.<?php echo $kunik ?>ParamsData.filter,function(k,v){
				filters[v.attributeName]=v.valueName;
			});
		}
		
		filters["links.memberOf."+((costum!=null && costum.contextId)?costum.contextId:contextData.id)] = {'$exists': true}

		dyFObj.buildInputField("#question<?php echo $key ?>",sectionDyf.<?php echo $kunik ?>ParamsData.field,{
            "inputType" : "finder",
            "label" : "Sélectionner l'"+sectionDyf.<?php echo $kunik ?>ParamsData.elementLabel,
            "initMe":false,
            "placeholder":"Rechercher un "+sectionDyf.<?php echo $kunik ?>ParamsData.elementLabel,
            "initContext" : false,
            "initType": [sectionDyf.<?php echo $kunik ?>ParamsData.type],
            "initBySearch": true,
            "initContacts" : true,
            "openSearch" :true,
            "search": {
                "filters":filters,
				"notSourceKey":true
            },
            "multiple" : sectionDyf.<?php echo $kunik ?>ParamsData.multiple,
            "invite" : sectionDyf.<?php echo $kunik ?>ParamsData.invite,
            "topClass" : "col-xs-10 col-xs-offset-1",
            "extraParams" : {
                "addNew" : sectionDyf.<?php echo $kunik ?>ParamsData.addNew
            }
            // "filters" : {
            // 	"category":"network",
            // 	"notSourceKey":true
            // }
        } ,{},null
        );

		dyFObj.initFieldOnload[sectionDyf.<?php echo $kunik ?>ParamsData.field+"Finder"]();		

    	if(typeof answers!="undefined" && notNull(answers) ){
           	$.each(answers,function(k,v){
				if(k=="adherent"){
					const field = sectionDyf.<?php echo $kunik ?>ParamsData.field;
					if(typeof field!="undefined" && notNull(field)){
						finder.addInForm<?php echo $kunik ?>(field,v.id,v.type,v.name,null,null,true);
					}
				}	
            });
        }

	
	sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "Configuration des éléments recherchés",
	        "icon" : "fa-cog",
	        "properties" : {
	            type : {
	                inputType : "select",
	                label : "Définir un type d'élément",
	                options :  <?php echo json_encode($elementType) ?>
	            },
	            filter : {
	                inputType : "lists",
                    label : "Filtres appliqués",
                    entries: {
                        attributeName: {
                            type:"select",
                            label:"Attribut",
                            options : ["category","type","tags","source.key"],
                            class:"col-lg-5"
                        },
                        valueName: {
                            type:"text",
                            label:"valeur",
                            class:"col-lg-5"
                        }
                    }
	            },
	            /*notSourceKey : dyFInputs.checkboxSimple(sectionDyf.<?php echo $kunik ?>ParamsData.notSourceKey, "notSourceKey", 
            										 {"onText" : trad.yes,
            										  "offText": trad.no,
            										  "onLabel" : "Recherche globale",
            										  "offLabel": "Recherche sourcée",
            										  "labelText" : "Recherche globale"}
				),*/
	            myContacts : dyFInputs.checkboxSimple(sectionDyf.<?php echo $kunik ?>ParamsData.myContacts, "myContacts", 
            										 {"onText" : trad.yes,
            										  "offText": trad.no,
            										  "onLabel" : "Recherches dans mes contacts",
            										  "offLabel": "Ne pas chercher dans mes contacts",
            										  "labelText" : "Rechercher parmi mes contacts"}
				),
	           elementLabel : {
	                inputType : "text",
	                label : "Label de l'élément recherché"
	            },
	            field : {
	                inputType : "text",
	                label : "Nom du champ"
	            },
	            /*multiple : dyFInputs.checkboxSimple(sectionDyf.<?php echo $kunik ?>ParamsData.multiple, "multiple", 
            										 {"onText" : trad.yes,
            										  "offText": trad.no,
            										  "onLabel" : "Ajouter plusieurs éléments",
            										  "offLabel": "N'ajouter qu'un seul élément",
            										  "labelText" : "Permettre l'ajout de plusieurs éléments"}
            	),*/
            	addNew : dyFInputs.checkboxSimple(sectionDyf.<?php echo $kunik ?>ParamsData.addNew, "addNew", 
					{
						"onText" : trad.yes,
						"offText": trad.no,
						"onLabel" : "Ajouter de nouveaux éléments",
						"offLabel": "Ne pas ajouter de nouveaux éléments",
						"labelText" : "Permettre l'ajout de nouveaux éléments"
					}
            	),
            	invite : dyFInputs.checkboxSimple(sectionDyf.<?php echo $kunik ?>ParamsData.invite,"invite",
            			{
							"onText" : trad.yes,
							"offText": trad.no,
							"onLabel" : "Afficher les invitations",
							"offLabel": "Ne pas afficher les invitations",
							"labelText" : "Permettre l'affichage des invitations"
						}
            	)
	        },
	        save : function (data) {  
	        	mylog.log("save sectionDyf",data);
	            tplCtx.value = {};
                const dynFormParams = sectionDyf.<?php echo $kunik ?>Params;
	            $.each( dynFormParams.jsonSchema.properties , function(k,val) { 
	            	if(k=="filter"){
	        		    var filters = {};
	        		    $.each(data.filter, function(index, va){
	        		        var filter = {attributeName: va.attributeName, valueName: va.valueName};
	        		        if(va.valueName!=""){
	        		        	filters["filter"+index] = filter;
	        		    	}
	        		    });
	        		    tplCtx.value[k] = filters;
	        		}
	        		else{
	        		tplCtx.value[k] = $("#"+k).val();
		        		if(tplCtx.value[k]=="true") {
		        			tplCtx.value[k]=true;
		        		}
		        		if(tplCtx.value[k]=="false") {
		        			tplCtx.value[k]=false;
		        		}
		        	}	
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    $("#ajax-modal").modal('hide');

	                    reloadInput("<?php echo $key ?>", "<?php echo (string)$form["_id"] ?>");
	                    //urlCtrl.loadByHash(location.hash);
	                } );
	            }

	    	}
	    }
	};

    //adds a line into answer
    <?php if( isset($parentForm["params"][$kunik]['type']) ) { ?>

    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( "<?php echo $parentForm["params"][$kunik]['type']; ?>",null, <?php echo $kunik ?>Data[$(this).data("key")]);
    });
    <?php } ?>

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        const dynFormParams = sectionDyf.<?php echo $kunik ?>Params
        const dynFormParamsData = sectionDyf.<?php echo $kunik ?>ParamsData;
        dyFObj.openForm( dynFormParams,null, dynFormParamsData);
    });
});
</script>
<?php } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>