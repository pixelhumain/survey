
<?php if($answer){

    if( $mode != "pdf" and $mode != "r"){
        $editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";
    }

    if( isset($parentForm["params"][$kunik]) ){
        $paramsData =  $parentForm["params"][$kunik];
    }

    $properties = [
        "publicAPIKey" => [
            "inputType" => "text",
            "label" => "Ajouter Votre Clé PUBLIQUE Stripe",
            "placeholder" => "Clé API PUBLIQUE Stripe",
            "rules" => [ "required" => true ]
        ]
    ];
?>
<style>
    #element-card-<?= $kunik ?> .CardField-input-wrapper {        
        font-size: 20pt !important;
        font-weight: bold !important;
    }
    .text-green {
        color: #00c07b;
    }
</style>
<div class="form-group" style="margin-bottom: 0px;">
    <div class="margin-bottom-10">
        <h4 class="titlecolor<?= $kunik ?> pdftittlecolor" style="align-items: center;">
            <?= $label." SéCURISé".$editQuestionBtn.$editParamsBtn?>
        </h4>
        <?= $info ?>
    </div>
    
    <div class="margin-top-20">
        <div class="list-group">
            <div class="list-group-item padding-10">
                <div><b>Nom : </b> <span id="adherent-name-<?= $kunik ?>"></span></div>
                <div><b>Email : </b> <span id="adherent-email-<?= $kunik ?>"></span></div>            
            </div>
        </div>
        <div class="list-group">
            <div class="list-group-item padding-20">
                <!-- div id="element-mode-<?= $kunik ?>"></div>
                <div id="element-currency-<?= $kunik ?>"></div -->
                <div id="element-card-<?= $kunik ?>">Préparation en cours ...</div>        
            </div>
        </div>
    </div>
    <div id="error-message-<?= $kunik ?>" class="text-danger"></div>
    <div id="success-message-<?= $kunik ?>" class="text-success"></div>
    <button id="btn-submit-payment-<?= $kunik ?>" class="btn btn-lg first-button btn-success margin-top-10">Valider mon adhésion</button>
</div>

<script>
   
    if(typeof answerPaymentData == "undefined"){
        var answerPaymentData = <?= json_encode($answer) ?>;
    }

	sectionDyf.<?= $kunik ?>ParamsData = <?= json_encode( (isset($paramsData)?$paramsData:null) ); ?>;
    
    $(document).ready(function() {

        $("#adherent-name-<?= $kunik ?>").html(userConnected.name);
        $("#adherent-email-<?= $kunik ?>").html(userConnected.email);
        
        function loadStripeScript(callback) {
            var script = document.createElement('script');
            script.src = "https://js.stripe.com/v3/";
            script.onload = callback;  // Appelle le callback une fois le script chargé
            document.body.appendChild(script);  // Ajoute le script à la fin de <body>
        }

        const params = sectionDyf.<?= $kunik ?>ParamsData;

    if(params!=null && (params.publicAPIKey==null || params.publicAPIKey=="")){
        $('#error-message-<?= $kunik ?>').text("Vous devez configurer la Clé API Publique");
        $("#btn-submit-payment-<?= $kunik ?>").attr("disabled", false);
    }else{
        loadStripeScript(function() {            
            const stripe = Stripe(params.publicAPIKey);
            const elements = stripe.elements();
            const cardElement = elements.create('card');
            //const modeElement = elements.create('paymentMethodMessaging', { theme: 'stripe', appearance: { theme: 'stripe' }, payment_method_types: ['card'], currency:"EUR", countryCode: "FR" });
            //const currencyElement = elements.create('payment');

            cardElement.mount('#element-card-<?= $kunik ?>');        
            //modeElement.mount('#element-mode-<?= $kunik ?>');        
            //currencyElement.mount('#element-currency-<?= $kunik ?>');        

            async function submitPayment(event){
                event.preventDefault();
                if(typeof answerPaymentData.answers != "undefined"){
                    let fieldsValues = {}
                    $.each(answerPaymentData.answers, function(index, value){
                        let values = Object.values(value);
                        $.each(values, function(fieldKunik, fieldValue){
                            fieldsValues = {...fieldsValues, ...fieldValue}
                        })
                    })
                    // à génériser
                    let paramsData = {}
                    if(fieldsValues.type!=null){
                        paramsData["type"] = fieldsValues.type.name;
                        paramsData["amount"] = parseFloat(fieldsValues.type.amount)*parseInt(fieldsValues.type.coeficient);
                        paramsData["duration"] = parseInt(fieldsValues.type.coeficient)*parseInt(fieldsValues.type.period);
                        paramsData["reason"] = "Droit d'adhésion pour être "+fieldsValues.type.name;
                        paramsData["contextId"] = (costum && costum.contextId)?costum.contextId:contextData._id.$id;
                    }else{
                        $('#error-message-<?= $kunik ?>').text("Vous devez sélectionner un type d'adhésion");
                        $('#success-message-<?= $kunik ?>').html("");
                        $("#btn-submit-payment-<?= $kunik ?>").attr("disabled", false);
                        return;
                    }

                    if(userConnected != "undefined"){
                        paramsData["name"] = userConnected.name; //fieldsValues.adherent.name;
                        paramsData["email"] = userConnected.email; //fieldsValues.adherent.email;
                        paramsData["adherentType"] = userConnected.collection; //fieldsValues.adherent.type;
                        paramsData["adherentId"] = userConnected._id.$id; //fieldsValues.adherent.id
                    }else{
                        $('#error-message-<?= $kunik ?>').text("Vous devez être connecté pour adhérer");
                        $('#success-message-<?= $kunik ?>').html("");
                        $("#btn-submit-payment-<?= $kunik ?>").attr("disabled", false);
                        return;
                    }
                    await intentPayment(paramsData);
                }
            }

            async function intentPayment(data){
                $('#error-message-<?= $kunik ?>').empty();
                let clientSecret = null;
                let error = null;
                ajaxPost( null, baseUrl+'/co2/stripe-payment/create-payment-intent',
                    {"amount":data.amount, "contextId":data.contextId},
                    function(data){
                        if(data.error){
                            error = data.error;
                        }else{
                            clientSecret = data.clientSecret;
                        }
                    },
                    null,
                    "json",
                    {async:false}
                );

                if (error) {
                    $('#error-message-<?= $kunik ?>').text(error);
                    $('#success-message-<?= $kunik ?>').html("");
                    $("#btn-submit-payment-<?= $kunik ?>").attr("disabled", false);
                    return;
                }

                // Confirmez le paiement
                const { paymentIntent, error: stripeError } = await stripe.confirmCardPayment(clientSecret, {
                    payment_method: {
                        card: cardElement,
                        billing_details: {
                            name: data.name,
                            email: data.email
                        }
                    },
                });

                if (stripeError) {
                    $('#error-message-<?= $kunik ?>').text(stripeError.message);
                    $('#success-message-<?= $kunik ?>').html("");
                    $("#btn-submit-payment-<?= $kunik ?>").attr("disabled", false);
                } else if (paymentIntent.status === 'succeeded') {
                    $("#success-message-<?= $kunik ?>").text('Paiement réussi !');
                    tplCtx.id = "<?= $answer["_id"] ?>";
                    tplCtx.collection = "answers";
                    tplCtx.path = "allToRoot";
                    tplCtx.value = {
                        payment:paymentIntent,
                        elementId: data.adherentId
                    };
                    dataHelper.path2Value( tplCtx, function(params) { 
	                    tplCtx.id = ((costum!=null && costum.contextId) ? costum.contextId : contextData.id);
                        tplCtx.collection =((costum!=null && costum.contextType) ? costum.contextType : contextData.type);
                        tplCtx.path = "links.members."+data.adherentId+".adhesion";
                        tplCtx.value = {
                                type: data.type,
                                duration: data.duration,
                                amount: data.amount,
                                startDate: moment().format()
                        };
                        tplCtx.updateCache = true;
                        dataHelper.path2Value( tplCtx, function(params) { 
                            tplCtx.id = data.adherentId
                            tplCtx.collection = data.adherentType;
                            tplCtx.path = "links.memberOf."+((costum!=null && costum.contextId) ? costum.contextId : contextData.id)+".adhesion";
                            tplCtx.value = {
                                type: data.type,
                                duration: data.duration,
                                amount: data.amount,
                                startDate: moment().format()
                            };
                            dataHelper.path2Value( tplCtx, function(params) { 
                                $('.content-step-0').animate({width:"toggle"}, function(){
                                    $('.final-content').animate({width:"toggle"});
                                });
                            });
	                    });
	                });
                }
            }

            $("#btn-submit-payment-<?= $kunik ?>").off().on("click", function(e){    
                $('#success-message-<?= $kunik ?>').html('<i class="fa fa-spin fa-spinner"></i> En cours de traitement...');
                $(this).attr("disabled", true);
                submitPayment(e)
            });
            //$("#payment-form-<?= $kunik ?>").on("submit", function(e){submitPayment(e)});
        });
    }

    sectionDyf.<?= $kunik ?> = {
		"jsonSchema" : {	
	        "title" : "Type d'adhésion",
	        "description" : "",
	        "icon" : "fa-group",
	        "properties" : <?= json_encode( $properties ); ?>,
	        save : function () { 
	            tplCtx.value = {};
	            $.each( sectionDyf.<?= $kunik ?>.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    dyFObj.closeForm();
                        toastr.success('Paramètres sauvegardés !');
                        let costumUpdateData = {
                            id: (costum!=null && costum.contextId)?costum.contextId:contextData.id,
                            collection: (costum!=null && costum.contextType)?costum.contextType:contextData.type,
                            path: "costum.htmlConstruct.adminPanel.menu.membership",
                            value: {view:"membership", icon:"users", label:"Adhésions"},
                            updateCache: true
                        }
                        dataHelper.path2Value( costumUpdateData, function(params) {
                            reloadInput("<?= $key ?>", "<?= (string)$form["_id"] ?>");
                        }) 
	                });
	            }
	    	}
	    }
	};

    $(".edit<?= $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        const dynFormParams = sectionDyf.<?= $kunik ?>;
        const dynFormParamsData = sectionDyf.<?php echo $kunik ?>ParamsData;
        dyFObj.openForm( dynFormParams, null, sectionDyf.<?= $kunik ?>ParamsData);
    });
});
</script>
<?php } ?>