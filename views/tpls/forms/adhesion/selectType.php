<?php if($answer){ ?>

<style type="text/css">
	.nb-mounth-<?= $kunik ?>{
		font-weight: bold;
	}
	.btn-lg-minus{
		font-weight: bold;
		border: 2px solid #ccc;
	}
	.btn-lg-plus{
		font-weight: bold;
		border: 2px solid #00c07b;
	}
	.list-group-item, .list-group-item:hover{
		border-width: 2px;
	}
	.btn-minus-<?= $kunik ?>, .btn-plus-<?= $kunik ?>, .coFormbody .btn{
		padding: 5px 15px !important;
		border-radius: 3px !important;
	}

	.selected{
		border-left: 2px solid #00c07b;
		border-color: #00c07b;
		color:#00c07b;
		background-color: #00c07b20
	}
	.list-group-item-text{
		color:#666;
		vertical-align: bottom;
	}
</style>
	<?php 
		if( $mode != "pdf" and $mode != "r"){
			$editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$kunik.".types' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";
			$classforpdf = "";
		} else {
			$editParamsBtn = "";
			if ($mode == "pdf" ) {
				$classforpdf = "calendar-table";
			}else{
				$classforpdf = "";
			}
		}

		if( isset($parentForm["params"][$kunik]) ){
			$paramsData =  $parentForm["params"][$kunik];
        }

		$properties = [
            "name" => [
                "inputType" => "text",
                "label" => "Type d'adhésion",
                "placeholder" => "Membre ",
                "rules" => [ "required" => true ]
            ],/*
            "description" => [
                "label" => "Description",
                "placeholder" => "",
                "inputType" => "textarea"
            ],*/
            "amount" => [
                "inputType" => "numeric",
                "label" => "Montant (en euros)",
                "placeholder" => "",
                "rules" => [ "required" => true ]
            ],
            "period" => [
                "inputType" => "numeric",
                "label" => "Période (en mois)",
                "placeholder" => "",
                "rules" => [ "required" => true ]
            ]
        ];

		// dv = default value
		if(isset($answers["type"])){
			$dv = $answers["type"];
		}
	?>	
<div class="form-group">
	<div class="margin-bottom-10">
		<h4 class="titlecolor<?= $kunik ?> pdftittlecolor">
			<?= $label.$editQuestionBtn.$editParamsBtn?></h4>
		<?= $info ?>
	</div>
	<div class="row no-padding">
	<?php if (isset($paramsData["types"])) {?>
		<?php foreach ($paramsData["types"] as $typeKey => $typeValue) {?>
			<div class="list-group col-xs-4" >
				<a href="javascript:;" 
					id="type<?= $typeKey ?>"
					data-id="<?= $answer["_id"] ?>" 
					data-key="<?= $typeKey ?>"
					data-path="<?= $answerPath ?>type"
					data-collection = "<?= Form::ANSWER_COLLECTION ?>"
					title="<?= $typeValue["name"] ?>"
					data-toggle="tooltip"
					data-placement="bottom"
					data-original-title="<?= $typeValue["name"] ?>"
					class="list-group-item selectType<?= $kunik ?> padding-10 <?= ($kunik) ?> <?= (isset($dv) && isset($dv["name"]) && $dv["name"]==$typeValue["name"]) ? "selected" : "" ?>">
					<span class="tooltips-menu-btn"><?= $typeValue["name"] ?></span>	
					<p class="list-group-item-text"><?= $typeValue["name"] ?></p>
						<h3 class="list-group-item-heading"><?= $typeValue["amount"] ?> €</h3>
					<!--div class="row">
						< div class="col-xs-12 col-md-9 col-lg-6">
							<h4 class="list-group-item-heading"><?= $typeValue["name"]??"" ?></h4>
							<p class="list-group-item-text"><?= $typeValue["description"]??"" ?></p>
						</div >
						<div class="col-xs-12 col-md-12 col-lg-3">
							
						</div>
						< div class="col-xs-12 col-md-3 col-lg-3 text-right">
							<button 
								type="button"
								class="btn btn-lg btn-lg-minus btn-minus-<?= $kunik ?>"
								data-key="<?= $typeKey ?>" > - </button>
							<span id="nb-mount-<?= $typeKey ?>-<?= $kunik ?>"
								 class="padding-10 nb-mounth-<?= $kunik ?>"><?= (isset($dv) && isset($dv["name"]) && isset($dv["coeficient"]) && $dv["name"]==$typeValue["name"]) ? $dv["coeficient"] : 0 ?></span>
							<button 
								type="button"
								class="btn btn-lg btn-lg-plus btn-plus-<?= $kunik ?>"
								data-key="<?= $typeKey ?>"> + </button>
							<p class="list-group-item-text">Coeficient périodique</p>
						</div>
					</div -->
					<?php if($canEditForm && $mode!="r" && $mode!="pdf"){ ?>
						<button 
							type="button"
							class="btn btn-lg btn-danger btn-remove-<?= $kunik ?>" style="position: absolute; top:0; right:0;"
							data-id='<?=$parentForm["_id"]?>' data-collection='<?=Form::COLLECTION ?>' data-path='params.<?=$kunik?>.types' data-key="<?= $typeKey ?>" > x </button>
					<?php } ?>
				</a>
			</div>
		<?php } ?>
	<?php } ?>
	</div>
</div>

<?php
if( $mode != "pdf" and $mode != "r"){	
?>
<script type="text/javascript">
	if(typeof answerPaymentData == "undefined"){
        var answerPaymentData = <?= json_encode($answer) ?>;
    }
$(document).ready(function() {
	var selectedTypeAdhesion = <?= json_encode((isset($dv)?$dv:null)) ?>;
	var <?= $kunik ?>Data = <?= json_encode( (isset($answers)) ? $answers : null ); ?>;
	sectionDyf.<?= $kunik ?>ParamsData = <?= json_encode( (isset($paramsData)?$paramsData:null) ); ?>;

	sectionDyf.<?= $kunik ?> = {
		"jsonSchema" : {	
	        "title" : "Ajouter Type d'adhésion",
	        "description" : "",
	        "icon" : "fa-group",
	        "properties" : <?= json_encode( $properties ); ?>,
	        save : function () { 
	            tplCtx.value = {};
				let typeSlug = slugify($("#name").val(), $("#name").val());

	            $.each( sectionDyf.<?= $kunik ?>.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });
	            tplCtx.path = tplCtx.path+"."+typeSlug;
				mylog.log("save tplCtx",tplCtx);
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    dyFObj.closeForm();
                    	reloadInput("<?= $key ?>", "<?= (string)$form["_id"] ?>");
	                });
	            }

	    	}
	    }
	};

	$(".selectType<?= $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path");

		if($("#nb-mount-"+$(this).data("key")+"-<?= $kunik ?>").text()=="0"){
			$("#nb-mount-"+$(this).data("key")+"-<?= $kunik ?>").text("1")
		}

		setSelectedTypeAdhesion($(this).data("key"));

		tplCtx.value = selectedTypeAdhesion;

		dataHelper.path2Value( tplCtx, function(params) { 
			if(typeof answerPaymentData["answers"] == "undefined"){
				answerPaymentData["answers"] = {"<?= $form["id"] ?>":{"<?= $kunik ?>":{"type":selectedTypeAdhesion}}}
			}
			if(typeof answerPaymentData["answers"]["<?= $form["id"] ?>"] == "undefined"){
				answerPaymentData["answers"]["<?= $form["id"] ?>"] = {"<?= $kunik ?>":{"type":selectedTypeAdhesion}}
			}
			if(typeof answerPaymentData["answers"]["<?= $form["id"] ?>"]["<?= $kunik ?>"] == "undefined"){
				answerPaymentData["answers"]["<?= $form["id"] ?>"]["<?= $kunik ?>"] = {"type":selectedTypeAdhesion}
			}
		});
		$(this).addClass("selected");
    });

    $(".edit<?= $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?= $kunik ?>,null, <?= $kunik ?>Data[$(this).data("key")]);
    });

    $(".edit<?= $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path")
        dyFObj.openForm( sectionDyf.<?= $kunik ?> );
    });

	$(".btn-plus-<?= $kunik ?>").off().on("click",function() {  
		var nb = parseInt($("#nb-mount-"+$(this).data("key")+"-<?= $kunik ?>").text()) + 1;
		$("#nb-mount-"+$(this).data("key")+"-<?= $kunik ?>").text(nb);
		var coef = parseInt($("#coef-"+$(this).data("key")+"-<?= $kunik ?>").text());
		setSelectedTypeAdhesion($(this).data("key"));
	});
	
	$(".btn-minus-<?= $kunik ?>").off().on("click",function() {  
		var nb = parseInt($("#nb-mount-"+$(this).data("key")+"-<?= $kunik ?>").text()) -1;
		$("#nb-mount-"+$(this).data("key")+"-<?= $kunik ?>").text(nb);
		setSelectedTypeAdhesion($(this).data("key"));
	});

	$(".btn-remove-<?= $kunik ?>").off().on("click",function() {  
		tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path")
		let paramsTypes = sectionDyf.<?= $kunik ?>ParamsData.types;
		delete paramsTypes[$(this).data("key")];
		tplCtx.value = paramsTypes;
		dataHelper.path2Value( tplCtx, function(params) { 
			toastr.success("Type Adhesion supprimé");
			reloadInput("<?= $key ?>", "<?= (string)$form["_id"] ?>");
		});
	});

	function setSelectedTypeAdhesion(key) {
		const type = sectionDyf.<?= $kunik ?>ParamsData.types[key];
		if(selectedTypeAdhesion!=null && selectedTypeAdhesion["name"]!=type["name"]) {
			const lastSlug = slugify(selectedTypeAdhesion["name"], selectedTypeAdhesion["name"]);
			$("#nb-mount-"+lastSlug+"-<?= $kunik ?>").text("0");
			$("#type"+lastSlug).removeClass("selected");
			selectedTypeAdhesion = type;
		}
		$("#type"+key).addClass("selected");
		selectedTypeAdhesion = selectedTypeAdhesion!=null ? selectedTypeAdhesion: type;
		selectedTypeAdhesion["coeficient"] = 1;//parseInt($("#nb-mount-"+key+"-<?= $kunik ?>").text());
	}
});
</script>
<?php }} else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>