0<style type="text/css">
    .div-pulse{
        box-shadow: 0 0 0 0 var(--colortitle);
        -webkit-animation: pulse 1.25s infinite cubic-bezier(0.66, 0, 0, 1);
        -moz-animation: pulse 1.25s infinite cubic-bezier(0.66, 0, 0, 1);
        -ms-animation: pulse 1.25s infinite cubic-bezier(0.66, 0, 0, 1);
        animation: pulse 1.25s infinite cubic-bezier(0.66, 0, 0, 1);
    }

    @-webkit-keyframes pulse {to {box-shadow: 0 0 0 45px rgba(232, 76, 61, 0);}}
    @-moz-keyframes pulse {to {box-shadow: 0 0 0 45px rgba(232, 76, 61, 0);}}
    @-ms-keyframes pulse {to {box-shadow: 0 0 0 45px rgba(232, 76, 61, 0);}}
    @keyframes pulse {to {box-shadow: 0 0 0 45px rgba(232, 76, 61, 0);}}

    .submitans {
        width: max-content;
        margin: auto;
        display: flex;
    }

    .submitansbtn {
        margin-left: 10px;
        margin-right : 10px;
    }

    .star-wrapper {
        direction: rtl;
    }

    .questionList {
        list-style-type: none;
    }

    ul.questionList{
        display: grid;
    }

    li.questionList{
        margin-left: auto;
        margin-right: auto;
    }

    /*    .star-wrapper {
          top: 50%;
          left: 50%;
          transform: translate(-50%, -50%);
          position: absolute;
          direction: rtl;
        }*/
    .star-wrapper a {
        font-size: 4em;
        color: #b7b4b4;
        text-decoration: none;
        transition: all 0.5s;
        margin: 4px;
    }
    .star-wrapper a:hover {
        color: gold;
        transform: scale(1.3);
    }
    .s1:hover ~ a {
        color: gold;
    }
    .s2:hover ~ a {
        color: gold;
    }
    .s3:hover ~ a {
        color: gold;
    }
    .s4:hover ~ a {
        color: gold;
    }
    .s5:hover ~ a {
        color: gold;
    }

    .s1label, .s2label, .s3label , .s4label, .s5label, .sendfeedb {
        display: none;
    }

    .staractive {
        color: gold !important;
    }

    .staractive ~ a {
        color: gold;
    }

</style>

<?php
$saveOneByOne = (isset($saveOneByOne)) ? $saveOneByOne :false;

$formId = $formStep;


if (isset($parentForm["type"]) && ($parentForm["type"] == "aap" || $parentForm["type"] == "aapConfig")){
    if( $parentForm["type"] == "aap"){
        $configEl = PHDB::findOneById(Form::COLLECTION, $parentForm["config"]);
    }elseif ( $parentForm["type"] == "aapConfig"){
        $configEl = $parentForm;
    }

    if (!empty($configEl) && !empty($configEl["subForms"][$formId]["inputs"])){
        $inputsObl = PHDB::findOneById(Form::INPUTS_COLLECTION, $configEl["subForms"][$formId]["inputs"]);
        if (!empty($inputsObl["inputs"])){
            $form["inputs"] = $inputsObl["inputs"];
            foreach ($form["inputs"] as $idInputs => $valInputs){
                $form["inputs"][$idInputs]["canAdmin"] = Authorisation::isParentAdmin((string)$configEl["_id"], Form::COLLECTION , Yii::app()->session["userId"]);
                $form["inputs"][$idInputs]["parentid"] = (string)$inputsObl["_id"];
            }
        }
    }

    if ($parentForm["type"] == "aap"){
        $inputsapp = PHDB::findOne(Form::INPUTS_COLLECTION, array("formParent" => (string)$parentForm["_id"] , "step" => $formId));

        if (!empty($inputsapp["inputs"])) {
            foreach ($inputsapp["inputs"] as $idInputs => $valInputs) {
                $inputsapp["inputs"][$idInputs]["canAdmin"] = Authorisation::isParentAdmin((string)$parentForm["_id"], Form::COLLECTION, Yii::app()->session["userId"]);
                $inputsapp["inputs"][$idInputs]["parentid"] = (string)$inputsapp["_id"];
            }
        }

        if (!empty($form["inputs"]) && !is_string($form["inputs"]) && !empty($inputsapp["inputs"])){
            $form["inputs"] = array_merge($form["inputs"] , $inputsapp["inputs"]);
        } elseif (!empty($inputsapp["inputs"]) ){
            $form["inputs"] = $inputsapp["inputs"];
        }
    }

    $form["id"] = $formId;

//    $inputsEl = PHDB::findOne(Form::INPUTS_COLLECTION, array( "parent.".$contextId => array('$exists'=>1), "subForm" => $formId ));
}

if( isset( $form["inputs"] ) ){
    if (isset($inputsEl["inputs"])) {
        $form["inputs"] = array_merge($form["inputs"] , $inputsEl["inputs"]);
    }
    ?>
    <?php

    $initValues = [];
    $ct = 1;

    $orderInputs = [];
    $orderInputsKeys = [];
    foreach ( $form["inputs"] as $key => $input) {
        if( stripos( $input["type"] , "tpls.forms.cplx" ) !== false )
            $saveOneByOne = true;
    }

    foreach ( $form["inputs"] as $key => $input) {
        $kunikT = explode( ".", $input["type"]);
        if(in_array( "multiDecide", $kunikT ) && isset($parentForm["inputConfig"]["multiDecide"])  ){
            $kunikDecide = explode( ".", $parentForm["inputConfig"]["multiDecide"]);
            $kunikDecideName = $kunikDecide[count($kunikDecide) - 1];
            $form["inputs"][$kunikDecideName] = $input;
            $form["inputs"][$kunikDecideName]["type"] = $parentForm["inputConfig"]["multiDecide"];
            $form["inputs"][$key]["border"] = false;
        }
    }



    ?>
    <ul class='questionList col-xs-12' >
    <?php
    //var_dump($form);exit;
    if(isset($form["name"])){
        echo '<h2 class="col-xs-12 text-left" style="10px margin:auto;">'.$form["name"].'</h2>';
    }
    if(isset($form["description"])){
        echo '<p class="col-xs-12" style="margin:10px auto;text-align:justify;">'.$form["description"].'</p>';
    }

    foreach ( $form["inputs"] as $key => $input) { //debut boucle input

        $canAnswer = (Yii::app()->session["userId"] == $answer["user"] || !empty($parentForm["anyOnewithLinkCanAnswer"])) ? true :false;

        //var_dump($canEditForm);

        $params = [  "tpl" => @$this->costum["slug"],
            "slug"=>@$el["slug"],
            "canEdit"=>$canEdit,
            "el"=>$el  ];
        echo $this->renderPartial("survey.views.tpls.forms.coFormjs", $params, true );

        $tpl = $input["type"];

        if(in_array($tpl, ["textarea","markdown","wysiwyg"]))
            $tpl = "tpls.forms.textarea";
        else if(empty($tpl) || in_array($tpl, ["text","button","color","date","datetime-local","email","image","month","number","radio","range","tel","time","url","week","tags"]))
            $tpl = "tpls.forms.text";
        else if(in_array($tpl, ["sectionTitle"]))
            $tpl = "tpls.forms.".$tpl;

        if( stripos( $tpl , "tpls.forms." ) !== false )
            $tplT = explode(".", $input["type"]);

        $kunikT = explode( ".", $input["type"]);
        $keyTpl = ( count($kunikT)>1 ) ? $kunikT[ count($kunikT)-1 ] : $input["type"];
        /*        var_dump($kunikT);
                var_dump(in_array( $kunikT, "multiDecide"));*/
        /*        if(in_array( "multiDecide", $kunikT ) && isset($answer["inputConfig"]["multiDecide"])  ){
                    $tpl = $answer["inputConfig"]["multiDecide"];
                    $kunikT = explode( ".", $tpl);
                    $keyTpl = $keyTpl = ( count($kunikT)>1 ) ? $kunikT[ count($kunikT)-1 ] : $input["type"];
                }*/
    //        var_dump($tpl);
        $kunik = $keyTpl.$key;
        $answers = null;
        $answerPath = "answers.".$key.".";


        if(isset($wizard))
        {
            if($wizard){
                $answerPath = "answers.".$formId.".".$key.".";
                if( isset($answer["answers"][$formId][$key]) && count($answer["answers"][$formId][$key])>0 )
                    $answers = $answer["answers"][$formId][$key];
            }
            else {
                if(isset($answer["answers"][$key]))
                    $answers = $answer["answers"][$key];
            }
        }

        if (isset($answers) && is_array($answers)) {
            foreach ($answers as $kans => $vans) {
                if (is_array($vans)) {
                    foreach ($vans as $kvalAns => $valAns) {
                        if (is_string($valAns)) {
                            $answers[$kans][$kvalAns] = str_replace('\"', '"', $valAns);
                            $answers[$kans][$kvalAns] = str_replace('"', '&quot;', $valAns);
                            // $answers[$kans][$kval] = addslashes($val);
                        }
                    }
                }
            }
        }


        $p = [
            "el" => $el,
            "parentForm"=> $parentForm,
            "form"      => $form,
            "key"       => $key,
            "keyTpl"       => $keyTpl,
            "kunik"     => $kunik,
            "input"     => $input,
            "type"      => $input["type"],
            "answerPath"=> $answerPath,
            "answer"    => $answer,
            "answers"   => $answers ,//sub answers for this input
            "label"     => @$input["label"] ,//$ct." - ".$input["label"] ,
            "titleColor"=> (isset($this->costum["colors"]["pink"])) ? $this->costum["colors"]["pink"] : "#000",
            "info"            => isset($input["info"]) ? $input["info"] : "" ,
            "placeholder"     => isset($input["placeholder"]) ? $input["placeholder"] : "" ,
            "mode"            => $mode,
            "canEdit"         => $canEdit,
            "canEditForm"     => @$canEditForm, //to change questions and configutarion
            "canEditOpenform" => @$canEditOpenform,
            "canAdminAnswer"  => $canAdminAnswer, //btn de config > Authorisation::isElementAdmin($parent["id"], $parent["type"]
            "canAnswer"       => $canAnswer,
            "editQuestionBtn" => @$editQuestionBtn,
            "saveOneByOne"    => $saveOneByOne,
            "wizard"          => (isset($wizard) && $wizard == true),
            "tpl"             => $tpl,
            "rendermodal"     => true
        ];

        if (isset($input["canAdmin"])){
            $p["canAdminAnswer"] = $canAdminAnswer = $input["canAdmin"];
            if (isset($mode) && $mode == "fa")
                $p["canEditForm"] = $input["canAdmin"];
        }

        if (isset($parentForm["type"]) && ($parentForm["type"] == "aap" || $parentForm["type"] == "aapConfig")){
            $labelaap = isset($input["label"]) ? $input["label"] : "";
            $infoaap = isset($input["info"]) ? $input["info"] : "";
            $placeholderaap = isset($input["info"]) ? $input["info"] : "";
            $typeaap = isset($input["type"]) ? $input["type"] : "";
            $editQuestionBtn = $p["editQuestionBtn"] = ($p["canEditForm"] && $mode == "fa") ? " <a class='btn btn-xs btn-danger descSec".$key." editQuestionaap' href='javascript:;' data-collection='".Form::INPUTS_COLLECTION."' data-key='".$key."' data-path='inputs.".$key."' data-parentid='".$input['parentid']."' data-label='".$labelaap."' data-info='".$infoaap."' data-placeholder='".$placeholderaap."' data-type='".$typeaap."'><i class='fa fa-pencil'></i></a>".
                " <a class='btn btn-xs btn-danger deleteLine' href='javascript:;' data-collection='".Form::INPUTS_COLLECTION."' data-key='question".$key."' data-path='inputs.".$key."' data-parentid='".$input["parentid"]."'><i class='fa fa-trash'></i></a>" : "";
        } else {
            $editQuestionBtn = $p["editQuestionBtn"] = ($canEditForm) ? " <a class='btn btn-xs btn-danger descSec".$key." editQuestion' href='javascript:;' data-form='".$formId."' data-id='".$form["_id"]."' data-collection='".Form::COLLECTION."' data-key='".$key."' data-path='inputs.".$key."' data-parentid='".(string)$parentForm['_id']."'><i class='fa fa-pencil'></i></a>".
                " <a class='btn btn-xs btn-danger deleteLine' href='javascript:;' data-id='".$form["_id"]."' data-collection='".Form::COLLECTION."' data-key='question".$key."' data-path='inputs.".$key."' data-parentid='".(string)$parentForm['_id']."'><i class='fa fa-trash'></i></a>" : "";
        }

        if(isset($tplT[2]) && $tplT[2] == "select" && isset($input["options"]))
            $p["options"] = $input["options"];

        if($input["type"] == "tags")
            $initValues[$key] = $input;

        if(isset($input["hide"]) && $input["hide"] == true){
            $conditionalHide = "hide";
        } else {
            $conditionalHide = "";
        }

        if(isset($input["border"]) && $input["border"] == false){
            $borderstyle = "";
        } else {
            $borderstyle = "border:1px solid #ddd;";
        }

        echo "<li id='question".$key."' class='col-xs-12 col-sm-10 no-padding questionBlock questionList ".$conditionalHide."'  data-id='".@$form['_id']."' data-path='inputs.".$key.".position' data-key='".$key."' data-form='".$formId."' style='margin-bottom:0px; margin-bottom:10px;'>";

        $this->renderPartial( "survey.views.".$tpl , $p );
        echo "</li>";

        $ct++;
    } //fin boucle inputs
    echo '<hr style="width:33%;"><button type="button" style="margin:20px auto;" class="btn btn-success sendStep" >Envoyer le formulaire</button>';
    echo "</ul>";

    $id = (!empty($answer)) ? " data-id='".$answer["_id"]."' " : "";



    echo '<div class="submitans" >';


}
?>

<script type="text/javascript">
    formInputs = <?php echo (isset($form["inputs"]) ? json_encode($form["inputs"]) : "[]") ?>;

    if (typeof inputsList != "undefined"){
        inputsList["<?php echo $formId ?>"] = <?php echo (isset($form["inputs"]) ? json_encode($form["inputs"]) : "[]") ?>;
    } else {
        var inputsList = {};
        inputsList["<?php echo $formId ?>"] = <?php echo (isset($form["inputs"]) ? json_encode($form["inputs"]) : "[]") ?>;
    }

    var initValues = <?php echo (!empty($initValues)) ? json_encode( $initValues ) : "null"; ?>;

    var answerStateOption = <?php echo (!empty($parentForm["params"]["answerStateOption"]) ?json_encode( $parentForm["params"]["answerStateOption"] ) : "null"  ) ; ?>;

    var answerState = '<?php echo (!empty($answer["submited"]["answerState"]) ? $answer["submited"]["answerState"] : "") ; ?>';

    var answerId = <?php echo json_encode((String)$answer['_id']); ?>;
    var mode = <?php echo json_encode($mode); ?>;
    $(document).ready(function() {

        if($("#form_addproposition").children(".questionList").size() <1 ){
            history.replaceState(location.hash, "", "#answer.index.id."+answerId+".mode."+mode);
        }
    });

    $(".sendStep").off().on("click",function(){
        toastr.success(tradForm["Your form has been successfully submitted"]);
        urlCtrl.loadByHash("#welcome");
    });

    function reloadInput(key, formId, _reloadCallback ){
        coInterface.showLoader("#question"+key);
        var reloadCall;
        if(typeof _reloadCallback !== 'undefined'){
            reloadCall = function(){

                _reloadCallback;

                $('.deleteLine').off().click( function(){
                    formId = $(this).data("id");
                    key = $(this).data("key");
                    pathLine = $(this).data("path");
                    collection = $(this).data("collection");
                    if (typeof $(this).data("parentid") != "undefined") {
                        var parentfId = $(this).data("parentid");
                    }
                    bootbox.dialog({
                        title: "Confirmez la suppression",
                        message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                        buttons: [
                            {
                                label: "Ok",
                                className: "btn btn-primary pull-left",
                                callback: function() {
                                    var formQ = {
                                        value:null,
                                        collection : collection,
                                        id : formId,
                                        path : pathLine
                                    };

                                    if (typeof parentfId != "undefined") {
                                        formQ["formParentId"] = parentfId;
                                    }

                                    dataHelper.path2Value( formQ , function(params) {
                                        $("#"+key).remove();
                                        //location.reload();
                                    } );
                                }
                            },
                            {
                                label: "Annuler",
                                className: "btn btn-default pull-left",
                                callback: function() {}
                            }
                        ]
                    });
                });

                $('.tooltips').tooltip();
            };
        }else{
            reloadCall = function(){
                $('.deleteLine').off().click( function(){
                    formId = $(this).data("id");
                    key = $(this).data("key");
                    pathLine = $(this).data("path");
                    collection = $(this).data("collection");
                    if (typeof $(this).data("parentid") != "undefined") {
                        var parentfId = $(this).data("parentid");
                    }
                    bootbox.dialog({
                        title: "Confirmez la suppression",
                        message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                        buttons: [
                            {
                                label: "Ok",
                                className: "btn btn-primary pull-left",
                                callback: function() {
                                    var formQ = {
                                        value:null,
                                        collection : collection,
                                        id : formId,
                                        path : pathLine
                                    };

                                    if (typeof parentfId != "undefined") {
                                        formQ["formParentId"] = parentfId;
                                    }

                                    dataHelper.path2Value( formQ , function(params) {
                                        $("#"+key).remove();
                                        //location.reload();
                                    } );
                                }
                            },
                            {
                                label: "Annuler",
                                className: "btn btn-default pull-left",
                                callback: function() {}
                            }
                        ]
                    });
                });

                $('.tooltips').tooltip();
            };
        };

        reloadinputData = {
            "answerId" : "<?php echo (string)$answer["_id"] ?>",
            "key" : key,
            "formId" : formId,
            "wizard" : "<?php echo (isset($wizard) && $wizard == true) ? "true" : "false" ?>",
            "mode" : "<?php echo $mode ?>",
            "canEdit" : <?php echo(isset($canEdit) && $canEdit != "" ? $canEdit : "false"); ?>,
            "canEditForm" : <?php echo(isset($canEditForm) && $canEditForm != "" ? $canEditForm : "false"); ?>,
            "canAdminAnswer" : <?php echo(isset($canAdminAnswer) && $canAdminAnswer != "" ? $canAdminAnswer : "false"); ?>,
            "canAnswer" : <?php echo(isset($canAnswer) && $canAnswer != "" ? $canAnswer : "false"); ?>,
            "saveOneByOne" : <?php echo(isset($saveOneByOne) && $saveOneByOne != "" ? $saveOneByOne : "false"); ?>,
            "contextId" : "<?php echo(isset($contextId) && $contextId != "" ? $contextId : ""); ?>",
            "contextType" : "<?php echo(isset($contextType) && $contextType != "" ? $contextType : ""); ?>",
            "editQuestionBtn" : "<?php echo(isset($editQuestionBtn) && $editQuestionBtn != "" ? $editQuestionBtn : ""); ?>",
            "titleColor" : "<?php echo(isset($p["titleColor"]) && $p["titleColor"] != "" ? $p["titleColor"] : "black"); ?>"
        }

        if (reloadinputData.contextType == "") {
            delete reloadinputData.contextType;
        }
        if (reloadinputData.contextId == "") {
            delete reloadinputData.contextId;
        }

        ajaxPost(
            "#question"+key,
            baseUrl+"/survey/answer/reloadinput",
            reloadinputData,
            reloadCall,
            reloadCall
            ,"html");
    }
</script>
