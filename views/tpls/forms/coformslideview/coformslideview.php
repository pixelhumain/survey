<?php
    $ignore = array('_file_', '_params_', '_obInitialLevel_' ,'ignore');
    $params = array_diff_key(get_defined_vars() + array_flip($ignore), array_flip($ignore));
    HtmlHelper::registerCssAndScriptsFiles(["/css/aap/aap.css"], Yii::app()->getModule('co2')->assetsUrl);
    HtmlHelper::registerCssAndScriptsFiles(array(
        '/css/coformslideview/coformslideview.css',
        '/js/coformslideview/coformslideview.js'
    ), Yii::app()->getModule(Survey::MODULE)->getAssetsUrl());
    $cssJS = array(
        '/plugins/swiper/swiper.min.css',
        '/plugins/swiper/swiper.min.js',
    );
    HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);
?>

<div class="slide-financer-bg">
    <div class="co-popup-slide-container">
        <input type="checkbox" class="checkbox-slide-start2 hide">
        <div class="co-popup-slide">
            <input type="checkbox" class="checkbox-slide-start hide">
        </div>
    </div>

</div>
</div>

<script type="application/javascript">
    var coForm_FormWizardParams = <?php echo json_encode($params); ?>;
    jQuery(document).ready(function() {
        var slideObj = coformSlideObj.init(coForm_FormWizardParams);
    });
</script>
