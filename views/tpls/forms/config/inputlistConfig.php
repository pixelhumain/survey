<?php
$ignore = array('_file_', '_params_', '_obInitialLevel_' ,'ignore');
$params = array_diff_key(get_defined_vars() + array_flip($ignore), array_flip($ignore));
$params["mode"] = "fa";
$params["answer"]["_id"] = new MongoId($params["parentForm"]["_id"]);
$params["answer"]["form"] = $params["parentForm"]["_id"];
$params["answer"]["collection"] = "answers";
echo $this->renderPartial("survey.views.tpls.forms.newFormWizard" , $params);