<?php $checkBlock = PHDB::find(Cms::COLLECTION, ["page" => "manda"]); ?>

<style>
	.campagne-head {
		display: flex;
		justify-content: space-between;
		width: 100%;
		position: relative;
		align-items: center;
	}

	#myModalCampagne {
		z-index: 999999999999999;
	}

	#myModalCampagne .modal-footer {
		border-top: none;
	}

	.numericInput.inputError {
		border-color: red;
	}

	.numericInput.inputError:focus {
		border-color: red;
	}

	.input-porteur .select2-container .select2-choice {
		height: 33px !important;
	}

	.input-porteur .select2-container, .input-paie-info .select2-container {
		display: block !important;
		padding: 0 !important;
	}
	.input-paie-info .select2-container {
		height: 40px;
	}

	.btn-new-type {
		margin-left: 10px;
	}

	.new-form {
		margin: 20px 0;
		padding: 10px;
		border: 1px solid gray;
		border-radius: 5px;
	}
	.input-paie-type {
		margin-bottom: 20px;
	}

	.type-list-content {
		display: flex;
		flex-wrap: wrap;
		position: relative;
	}
	.type-item {
		margin: 5px;
		border: 1px solid gray;
		border-radius: 10px;
		padding: 10px;
		display: block;
		position: relative;
		box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;
	}
	.type-config span {
		display: block;
		font-size: 16px;
	}

	.type-name {
		display: block;
		font-size: 16px;
		font-weight: 600;
	}
	.delete-payment-item {
		position: absolute;
		display: flex;
		justify-content: center;
		align-items: center;
		top: -8px;
		right: -8px;
		width: 20px;
		height: 20px;
		background-color: red;
		color: #fff;
		border-radius: 50%;
		cursor: pointer;
	}
	.clr-field {
		display: block;
	}
	.clr-field button {
		border-radius: 5px;
	}
</style>

<div id="myModalCampagne" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Nouvelle campagne</h4>
			</div>
			<div class="modal-body">
				<div class="col-xs-12">
					<div class="form-group">
						<label for=""> Nom de la campagne </label>
						<input type="text" class="form-control" data-path="name" value="" data-collection="" id="campName" placeholder="">
					</div>
				</div>
				<div class="col-md-6" id="parentformstartdate">
					<div class="form-group">
						<label for=""> Date debut </label>
						<input type="text" class="form-control" data-setType="isoDate" data-path="startDate" value="" data-collection="" id="campDebut" placeholder="" />
					</div>
				</div>
				<div class="col-md-6" id="parentformenddate">
					<div class="form-group">
						<label for=""> Date fin </label>
						<input type="text" class="form-control" data-setType="isoDate" data-path="endDate" value="" data-id="" data-collection="" id="campEnd" placeholder="" />
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
				<button type="button" class="btn btn-success" id="saveCamp" data-dismiss="modal">Enregistrer</button>
			</div>
		</div>

	</div>
</div>
<div class="container">
	<div class="campagne-head">
		<div class="">
			<h3 class="title">
				<span class="main">Liste des campagnes</span>
				<span class="status"> </span>
			</h3>
		</div>
		<div class="">
			<button class="btn btn-primary" data-toggle="modal" data-target="#myModalCampagne">Nouvelle campagne</button>
		</div>
	</div>
</div>
<?php
$where = array("collection" => "organizations");
// $orga = PHDB::findAndLimitAndIndex(Organization::COLLECTION, $where, 50);
$output = PHDB::findOneById(Form::COLLECTION, $parentForm["config"]);
if (isset($output['campagne']))
{
	foreach ($output['campagne'] as $key => $value)
	{
?>
		<div class="container <?= $parentForm["config"] ?>">
			<form>
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="form-group">
							<div class="checkbox checbox-switch switch-success">
								<label for="">
									<?php echo $value['name'] ?>
								</label>

								<label class="pull-right">

									<input <?= !empty($value["activated"]) && $value["activated"] ? "checked" : "" ?> data-slug="<?= !empty($value['slug']) ? $value['slug'] : '' ?>" data-id="<?php echo $key ?>" data-name="<?php echo $value['name'] ?>" class="activecampagne" id="activecampagne" type="checkbox" data-callback="" />
									<span></span>

								</label>

							</div>
							<small id="" class="text-muted">
								Pour activer la campagne de <?php echo $value['name'] ?>
							</small>
						</div>
						<div class="col-xs-12 input-porteur" data-id="<?= $key ?>" data-key="<?= !empty($value['campPorteur']) ? $value['campPorteur']['id'] : '' ?>" data-value="<?= !empty($value['campPorteur']) ? $value['campPorteur']['text'] : '' ?>">
							<div class="form-group">
								<label for=""> Porteur de la campagne </label>
								<input type="text" class="form-control campPorteur" data-id="<?= $key ?>" data-path="campPorteur" value="" data-collection="" id="campPorteur" placeholder="">
								<small id="" class="text-muted">
									Selectionner le porteur de la campagne
								</small>
							</div>
						</div>
						<div class="col-md-6" id="parentformstartdate">
							<div class="form-group">
								<label for=""> Date debut </label>
								<input type="text" class="form-control startdatecampagne startDate<?= $key ?>" data-id="<?= $key ?>" value="<?= !empty($value['startDate']) ? $value['startDate']->toDateTime()->setTimeZone(new DateTimeZone(Yii::app()->session["timezone"] ?? "UTC"))->format('Y-m-d H:i') : '' ?>" data-setType="isoDate" data-path="startDate" data-collection="" id="startdatecampagne" placeholder="" />
								<small id="" class="text-muted">
									Date du début de la campagne
								</small>
							</div>
						</div>
						<div class="col-md-6" id="parentformenddate">
							<div class="form-group">
								<label for=""> Date fin </label>
								<input type="text" class="form-control enddatecampagne endDate<?= $key ?>" data-id="<?= $key ?>" value="<?= !empty($value['endDate']) ? $value['endDate']->toDateTime()->setTimeZone(new DateTimeZone(Yii::app()->session["timezone"] ?? "UTC"))->format('Y-m-d H:i') : '' ?>" data-setType="isoDate" data-path="endDate" data-collection="" id="enddatecampagne" placeholder="" />
								<small id="" class="text-muted">
									Date du fin de la campagne
								</small>
							</div>
						</div>
						<div class="col-md-6" id="cofinancedate">
							<div class="form-group">
								<label for=""> Cofinancement </label>
								<input type="text" class="form-control cofinancedate startDate<?= $key ?>" data-id="<?= $key ?>" value="<?= !empty($value['cofinancedate']) ? $value['cofinancedate']->toDateTime()->setTimeZone(new DateTimeZone(Yii::app()->session["timezone"] ?? "UTC"))->format('Y-m-d H:i') : '' ?>" data-setType="isoDate" data-path="cofinancedate" data-collection="" id="cofinancedatecampagne" placeholder="" />
								<small id="" class="text-muted">
									Date début du cofinancement
								</small>
							</div>
						</div>
						<div class="col-md-6" id="panierdate">
							<div class="form-group">
								<label for=""> Panier </label>
								<input type="text" class="form-control panierdate endDate<?= $key ?>" data-id="<?= $key ?>" value="<?= !empty($value['panierdate']) ? $value['panierdate']->toDateTime()->setTimeZone(new DateTimeZone(Yii::app()->session["timezone"] ?? "UTC"))->format('Y-m-d H:i') : '' ?>" data-setType="isoDate" data-path="panierdate" data-collection="" id="panierdatecampagne" placeholder="" />
								<small id="" class="text-muted">
									Date d'ouverture du panier
								</small>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="form-group">
								<label for=""> Couleur</label>
								<input type="text" id="campColor" data-coloris data-collection="" data-id="<?= $key ?>" data-path="campColor" class="form-control campColor coloris" value="<?=  isset($value["campColor"]) ? $value["campColor"] : "#4623c9" ?>">
								<small id="" class="text-muted">
									Couleur de la campagne
								</small>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="form-group">
								<label for="campType"> Type de campagne </label>
								<select class="form-control campType" data-id="<?= $key ?>" data-path="campType" value="<?= !empty($value['campType']) ? $value['campType'] : '' ?>" data-collection="" id="campType">
									<option value="simple" <?= !empty($value['campType']) && $value['campType'] == "simple" ? 'selected' : '' ?>>Simple</option>
									<option value="doublonnage" <?= !empty($value['campType']) && $value['campType'] == "doublonnage" ? 'selected' : '' ?>>Doublonnage</option>
									<option value="doublonnagePromesse" <?= !empty($value['campType']) && $value['campType'] == "doublonnagePromesse" ? 'selected' : '' ?>>Doublonnage sur promesse</option>
								</select>
								<small id="" class="text-muted">
									Choisir le type de la campagne
								</small>
							</div>
						</div>
						<div class="col-xs-12 hide-simple<?= $key ?> <?= isset($value['campType']) ? (($value['campType'] == "simple" || $value['campType'] == "") ? 'd-none' : '') : 'd-none' ?>">
							<div class="form-group">
								<label for=""> Montant total</label>
								<input type="text" class="form-control numericInput campMontant" data-id="<?= $key ?>" data-path="campMontant" value="<?= !empty($value['campMontant']) ? $value['campMontant'] : '' ?>" data-collection="" id="campMontant" placeholder="">
								<small id="" class="text-muted">
									Montant total de la campagne
								</small>
							</div>
						</div>
						<div class="col-xs-12 hide-simple<?= $key ?> <?= isset($value['campType']) ? (($value['campType'] == "simple" || $value['campType'] == "") ? 'd-none' : '') : 'd-none' ?>">
							<div class="form-group">
								<label for=""> Financement max à doublonner</label>
								<input type="text" class="form-control numericInput campFinanc" data-id="<?= $key ?>" data-path="campFinanc" value="<?= !empty($value['campFinanc']) ? $value['campFinanc'] : '' ?>" data-collection="" id="campFinanc" placeholder="">
								<small id="" class="text-muted">
									Pour tout financement max à cette somme, FTL financera la meme somme
								</small>
							</div>
						</div>
						<!-- <div class="col-xs-12 input-paie-info" data-id="<?= $key ?>" data-key="<?= !empty($value['paie-type']) ? $value['paie-type']['id'] : '' ?>" data-value="<?= !empty($value['paie-type']) ? $value['paie-type']['text'] : '' ?>">
							<div class="form-group">
								<label for=""> Type de paiement </label>
								<input class="form-control input-paie-type" data-id="<?= $key ?>" data-path="input-type" value="" data-collection="" id="paie-type" placeholder="">
								
								<small id="" class="text-muted">
									Selectionner les types de paiement
								</small>
							</div>
						</div> -->

						<div class="col-xs-12">
							<div class="form-group">
								<label for="campType">Type de paiement </label>
								<button type="button" class="btn-new-type btn btn-primary btn-sm"><i class="fa fa-plus"></i></button>
							</div>
						</div>
						<div class="col-xs-12 type-paie-list">
							<div class="type-list-content">
								
							</div>
						</div>

						<div class="form-type-paie">

							<!-- <div class="col-xs-12 type-paie-template">
								<div class="form-group">
									<label for="campType"> Type de paiement </label>
									<select class="form-control input-paie-type" data-id="<?= $key ?>" data-path="campType" value="" data-collection="" id="input-paie-type">
										<option value="virement">Virement bancaire</option>
										<option value="helloasso">Hello Asso</option>
										<option value="stripe">Stripe</option>
									</select>
									<small id="" class="text-muted">
										Selectionner les types de paiement
									</small>
								</div>
							</div>

							<div class="col-xs-12 hide-simple<?= $key ?> ">
								<div class="form-group">
									<label for=""> IBAN de la campagne</label>
									<input type="text" class="form-control campIban" data-id="<?= $key ?>" data-path="iban" value="<?= $value['iban'] ?? '' ?>" data-collection="" id="iban" placeholder="">
									<small id="" class="text-muted">
										Tous les virements bancaires à la campagne seront versés à ce compte.
									</small>
								</div>
							</div>
							<div class="col-xs-12 hide-simple<?= $key ?> ">
								<div class="form-group">
									<label for=""> Slug</label>
									<input type="text" class="form-control slugHelloasso" data-id="<?= $key ?>" data-path="slugHelloasso" value="<?= $value['slugHelloasso'] ?? '' ?>" data-collection="" id="slugHelloasso" placeholder="">
									<small id="" class="text-muted">
										Slug de l'organisation dans helloasso.
									</small>
								</div>
							</div>
							<div class="col-xs-12 hide-simple<?= $key ?> ">
								<div class="form-group">
									<label for=""> Client id</label>
									<input type="text" class="form-control client-id" data-id="<?= $key ?>" data-path="clientId" value="<?= $value['clientId'] ?? '' ?>" data-collection="" id="clientId" placeholder="">
									<small id="" class="text-muted">
										Description
									</small>
								</div>
							</div>
							<div class="col-xs-12 hide-simple<?= $key ?> ">
								<div class="form-group">
									<label for=""> Client secret</label>
									<input type="text" class="form-control client-secret" data-id="<?= $key ?>" data-path="clientSecret" value="<?= $value['clientSecret'] ?? '' ?>" data-collection="" id="clientSecret" placeholder="">
									<small id="" class="text-muted">
										Description
									</small>
								</div>
							</div>
							<div class="col-xs-12 hide-simple<?= $key ?> ">
								<div class="form-group">
									<label for=""> Secret key</label>
									<input type="text" class="form-control secret-key" data-id="<?= $key ?>" data-path="secretKey" value="<?= $value['secretKey'] ?? '' ?>" data-collection="" id="secretKey" placeholder="">
									<small id="" class="text-muted">
										Description
									</small>
								</div>
							</div>
							<div class="col-xs-12 hide-simple<?= $key ?> ">
								<div class="form-group">
									<label for=""> Webhook key</label>
									<input type="text" class="form-control webhook-key" data-id="<?= $key ?>" data-path="webhookKey" value="<?= $value['webhookKey'] ?? '' ?>" data-collection="" id="webhookKey" placeholder="">
									<small id="" class="text-muted">
										Description
									</small>
								</div>
							</div> -->

						</div>
						
					</div>
				</div>
			</form>
		</div>
<?php
	}
}
?>
<script>
	var today = new Date();
	var todayFormatted = today.toISOString().split('T')[0];
	var debut = {}
	var dataPaie = <?= json_encode(@$value['campPaieType']) ?>;
	dataPaie = dataPaie ? dataPaie : [];
	var keyPaie = []
	keyPaie = dataPaie.map(item => item.id);
	mylog.log('TYPE', dataPaie);
	function loadColorisCSS() {
		$('<link>', {
			rel: 'stylesheet',
			type: 'text/css',
			href: baseUrl+'/plugins/Coloris-main/src/coloris.css',
		}).appendTo('head');
	}
	function loadColorisJS() {
		$('<script>', {
			type: 'text/javascript',
			src: baseUrl+'/plugins/Coloris-main/src/coloris.js',
		}).appendTo('head');
	}
	loadColorisCSS(); 
	loadColorisJS(); 
	Coloris({
		el: '.coloris',
		swatches: ['#264653','#2a9d8f','#e9c46a','#f4a261','#e76f51','#d62828','#023e8a','#0077b6','#0096c7','#00b4d8','#48cae4',],
		theme: 'large',
		themeMode: 'dark',
		alpha : false,
		wrap : true
	});
	$('.coloris').on('change', function(){
		console.log($(this), 'value')
	})
	$('.startdatecampagne').each(function() {
		debut[$(this).data('id')] = $(this).val()
	});
	var fin = {}
	$('.enddatecampagne').each(function() {
		fin[$(this).data('id')] = $(this).val()
	});
	function maskWord(word) {
		if (word.length <= 6) {
			return word;
		}

		var firstThree = word.slice(0, 3);
		var lastThree = word.slice(-3);  
		var middle = word.slice(3, -3).replace(/./g, '*'); 

		return firstThree + middle + lastThree;
	}

	$(".btn-new-type").click(function() {
		var newform = $('<div>')
			.addClass('new-form col-xs-12');
		
		var typeTemplate = $('<select>')
			.addClass('form-control input-paie-type');

		var typeOption = `<option value="">Selectionner le type de paiement</option>
						<option value="virement">Virement bancaire</option>
						<option value="helloasso">Hello Asso</option>
						<option value="stripe">Stripe</option>`;
		typeTemplate.append(typeOption);
		newform.append(typeTemplate);

		$('.form-type-paie').append(newform);

		var formStripe = $('<div>')
			.addClass('form-stripe');
		
		var inputStripe = `
			<div class="col-xs-12">
				<div class="form-group">
					<label for=""> Secret key</label>
					<input type="text" class="form-control secret-key" placeholder="Entrez votre Secret key">
					<small class="text-muted">
						Description
					</small>
				</div>
			</div>
			<div class="col-xs-12">
				<div class="form-group">
					<label for=""> Webhook key</label>
					<input type="text" class="form-control webhook-key" placeholder="Entrez votre Webhook key">
					<small class="text-muted">
						Description
					</small>
				</div>
			</div>
		`;
		var btnValidateStripe = $(`<button type="button">`)
			.addClass('btn-validate btn btn-success col-xs-12')
			.text('valider')
		formStripe.append(inputStripe);
		formStripe.append(btnValidateStripe)

		var formHelloasso = $('<div>')
			.addClass('form-helloasso');
		
		var inputHelloasso = `
			<div class="col-xs-12">
				<div class="form-group">
					<label for=""> Slug</label>
					<input type="text" class="form-control slug-helloasso" data-id="<?= $key ?>" data-path="slugHelloasso" value="" data-collection="" id="slugHelloasso" placeholder="">
					<small id="" class="text-muted">
						Slug de l'organisation dans helloasso.
					</small>
				</div>
			</div>
			<div class="col-xs-12">
				<div class="form-group">
					<label for=""> Client id</label>
					<input type="text" class="form-control client-id" data-id="<?= $key ?>" data-path="clientId" value="" data-collection="" id="clientId" placeholder="">
					<small id="" class="text-muted">
						Description
					</small>
				</div>
			</div>
			<div class="col-xs-12">
				<div class="form-group">
					<label for=""> Client secret</label>
					<input type="text" class="form-control client-secret" data-id="<?= $key ?>" data-path="clientSecret" value="" data-collection="" id="clientSecret" placeholder="">
					<small id="" class="text-muted">
						Description
					</small>
				</div>
			</div>
		`;
		var btnValidateHelloasso = $(`<button type="button">`)
			.addClass('btn-validate btn btn-success col-xs-12')
			.text('valider')
		formHelloasso.append(inputHelloasso);
		formHelloasso.append(btnValidateHelloasso)

		var formVirement = $('<div>')
			.addClass('form-helloasso');
		
		var inputVirement = `
			<div class="col-xs-12 hide-simple<?= $key ?> ">
				<div class="form-group">
					<label for=""> IBAN de la campagne</label>
					<input type="text" class="form-control campIban" data-id="<?= $key ?>" data-path="iban" value="" data-collection="" id="iban" placeholder="">
					<small id="" class="text-muted">
						Tous les virements bancaires à la campagne seront versés à ce compte.
					</small>
				</div>
			</div>
		`;
		var btnValidateVirement = $(`<button type="button">`)
			.addClass('btn-validate btn btn-success col-xs-12')
			.text('valider')
		formVirement.append(inputVirement);
		formVirement.append(btnValidateVirement)

		typeTemplate.change(function() {
			var self = $(this);
			if (self.val() === 'stripe') {
				newform.append(formStripe);
			} else {
				formStripe.remove();
			}

			if (self.val() === 'helloasso') {
				newform.append(formHelloasso);
			} else {
				formHelloasso.remove();
			}

			if (self.val() === 'virement') {
				newform.append(formVirement);
			} else {
				formVirement.remove();
			}
		});
	});
	$(document).on('click', '.btn-validate', function(e) {
		e.stopImmediatePropagation();
		var parentForm = $(this).closest('.new-form');
		var paramsData = {
			type: parentForm.find(".input-paie-type").val(),
			parent: {
				id: costum.contextId,
				type: costum.contextType
			}
		};
		if (parentForm.find('.secret-key').length) {
			var secretKey = parentForm.find('.secret-key').val();
			var webhookKey = parentForm.find('.webhook-key').val();
			paramsData.config = { 
				secretKey : secretKey, 
				webhookKey : webhookKey
			}
		}

		if (parentForm.find('.slug-helloasso').length) {
			var slugHelloasso = parentForm.find('.slug-helloasso').val();
			var clientId = parentForm.find('.client-id').val();
			var clientSecret = parentForm.find('.client-secret').val();
			paramsData.config = { 
				clientId : clientId, 
				clientSecret : clientSecret,
				slug: slugHelloasso
			}
		}

		if (parentForm.find('.campIban').length) {
			var campIban = parentForm.find('.campIban').val();
			paramsData.config = { 
				iban : campIban
			}
		}
		ajaxPost(
			null,
			baseUrl + "/co2/paymentMethod/save", 
			paramsData,
			function(data) {
				if(data.result == true){
					if(parentForm.find('.slug-helloasso').length){
						var itemType = `<div class="type-item" id="item-paye-${data.data._id.$id}">
									<span class="delete-payment-item" data-payment="${data.data._id.$id}"><i class="fa fa-times"></i></span>
									<span class="type-name">Hello Asso</span>
									<div class="type-config">
										<span>Client id : ${maskWord(paramsData.config.clientId)}</span>
										<span>Client Secret : ${maskWord(paramsData.config.clientSecret)}</span>
										<span>Slug de l'association : ${paramsData.config.slug}</span>
									</div>
								</div>`;
					}else if(parentForm.find('.secret-key').length){
						var itemType = `<div class="type-item" id="item-paye-${data.data._id.$id}">
									<span class="delete-payment-item" data-payment="${data.data._id.$id}"><i class="fa fa-times"></i></span>
									<span class="type-name">Stripe</span>
									<div class="type-config">
										<span>Secret key : ${maskWord(paramsData.config.secretKey)}</span>
										<span>Webhook key : ${maskWord(paramsData.config.webhookKey)}</span>
									</div>
								</div>`;
					}else if(parentForm.find('.campIban').length){
						var itemType = `<div class="type-item" id="item-paye-${data.data._id.$id}">
									<span class="delete-payment-item" data-payment="${data.data._id.$id}"><i class="fa fa-times"></i></span>
									<span class="type-name">Virement Bancaire</span>
									<div class="type-config">
										<span>IBAN : ${maskWord(paramsData.config.iban)}</span>
									</div>
								</div>`;
					}
					var itemParent = $('.type-list-content')
					itemParent.append(itemType)
					parentForm.remove()
				}else{
					toastr.error(data.msg);
				}
			}
		);
	});
	function sanitazeString(str) {
		str = str.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
		str = str.replace(/\s+/g, '-');
		str = str.toLowerCase()
		return str;
	}

	$(".campPorteur").empty().select2({
		minimumInputLength: 1,
		//tags: true,
		multiple: false,
		"tokenSeparators": [','],
		createSearchChoice: function(term, data) {

		},
		initSelection: function(element, callback) {
			mylog.log('ELEMENT CLASSE', $(element).attr('class'))
			var keys = $(element).parent().parent().data('key')
			var choise = $(element).parent().parent().data('value')
			callback({
				id: keys,
				text: choise,
				img: '',
				slug: ''
			});
		},
		ajax: {
			url: baseUrl + "/" + moduleId + "/search/globalautocomplete",
			dataType: 'json',
			type: "POST",
			quietMillis: 50,
			data: function(term) {
				return {
					name: term,
					searchType: ["organizations"],
					filters: {
						'$or': {
							"name": {
								'$exists': true
							},
						}
					}
				};
			},
			results: function(data) {
				return {
					results: $.map(Object.values(data.results), function(item) {
						return {
							text: item.name,
							id: item._id.$id,
							img: item.profilImageUrl == "" ? modules.co2.url + '/images/thumbnail-default.jpg' : item.profilImageUrl,
							slug: item.slug
						}
					})
				};
			}
		}
	}).on('change', function(e) {
		if (exists(e.added)) {
			mylog.log('ADDED', e, e.added)
			var porteurData = {
				id: "<?php echo $parentForm["config"] ?>",
				collection: 'forms',
				path: 'campagne.' + $(e.target).parent().parent().data('id') + '.campPorteur',
				value: e.added
			}
			dataHelper.path2Value(porteurData, function(params) {
				toastr.success('Modification reussi avec succès');
			});
		}
	});

	$('.startdatecampagne').datetimepicker({
		format: 'Y-m-d H:i',
		minDate: todayFormatted,
		onShow: function(ct) {
			this.setOptions({
				maxDate: $('.endDate' + $(this).data('id')).val() ? $('.endDate' + $(this).data('id')).val() : false
			})
		}
	});

	$('.cofinancedate').datetimepicker({
		format: 'Y-m-d H:i',
		minDate: todayFormatted,
		onShow: function(ct) {
			this.setOptions({
				maxDate: $('.endDate' + $(this).data('id')).val() ? $('.endDate' + $(this).data('id')).val() : false
			})
		}
	});

	$('.panierdate').datetimepicker({
		format: 'Y-m-d H:i',
		onShow: function(ct) {
			this.setOptions({
				maxDate: $('.endDate' + $(this).data('id')).val() ? $('.endDate' + $(this).data('id')).val() : false
			})
		}
	});
	$('.enddatecampagne').datetimepicker({
		format: 'Y-m-d H:i',
		onShow: function(ct) {
			this.setOptions({
				minDate: $('.startDate' + $(this).data('id')).val() ? $('.startDate' + $(this).data('id')).val() : false
			})
		}
	});

	$('#campDebut').datetimepicker({
		format: 'Y-m-d H:i',
		minDate: todayFormatted,
		onShow: function(ct) {
			this.setOptions({
				maxDate: $('#campEnd').val() ? $('#campEnd').val() : false
			})
		}
	});

	$('#campEnd').datetimepicker({
		format: 'Y-m-d H:i',
		onShow: function(ct) {
			this.setOptions({
				minDate: $('#campDebut').val() ? $('#campDebut').val() : false
			})
		}
	});

	$(".numericInput").on('input', function() {
		var inputVal = $(this).val()
		if (/^\d*$/.test(inputVal)) {
			$(this).removeClass('inputError')
		} else {
			$(this).addClass('inputError')
		}
	})
	$(".campMontant, .campType, .campFinanc, .campIban, .slugHelloasso, .campColor").on('change', function() {
		var self = $(this)
		var key = self.data('id')
		if (self.hasClass("inputError")) {
			toastr.error('La valeur doit etre que des chiffres seulement');
		} else {
			var dateData = {
				id: "<?php echo $parentForm["config"] ?>",
				collection: 'forms',
				path: 'campagne.' + key + '.' + self.data('path'),
				value: self.val()
			};
			if (!isNaN(dateData.value))
				dateData.setType = 'int';
			dataHelper.path2Value(dateData, function(params) {
				toastr.success('Modification reussi avec succès');
				if (self.data('path') == "campType" && self.val() == "simple") {
					$(".hide-simple" + key).addClass('d-none')
				} else if (self.data('path') == "campType" && self.val() != "simple") {
					$(".hide-simple" + key).removeClass('d-none')
				}
			});
		}
	})
	$(".startdatecampagne, .enddatecampagne, .panierdate, .cofinancedate").on('change', function() {
		var self = $(this)
		var key = self.data('id')
		var start = $('.startDate' + key).val();
		var end = $('.endDate' + key).val();
		var dateData = {
			id: "<?php echo $parentForm["config"] ?>",
			collection: 'forms',
			path: 'campagne.' + self.data('id') + '.' + self.data('path'),
			value: moment(self.val()).format(),
			setType: 'isoDate'
		}
		if (new Date(end) < new Date(start)) {
			toastr.error('La date de fin ne peut pas être antérieure à la date de début');
			self.data('path') == 'endDate' ? self.val(fin[key]) : self.val(debut[key])
		} else {
			dataHelper.path2Value(dateData, function(params) {
				if (self.data('path') == 'startDate') {
					debut[key] = self.val()
				} else {
					fin[key] = self.val();
				}
				toastr.success('Date modifier avec succès');
			});
		}
	})
	$("#saveCamp").on('click', function() {
		dyFObj.setMongoId('forms', function() {
			var tplCtx = {
				id: "<?php echo $parentForm["config"] ?>",
				collection: 'forms',
				path: 'campagne.' + uploadObj.id,
				value: {
					__id: uploadObj.id,
					name: $('#campName').val(),
					slug: sanitazeString($('#campName').val()),
					startDate: $('#campDebut').val(),
					endDate: $('#campEnd').val(),
					activated: false,
					costumId: costum.contextId,
					costumType: costum.contextType,
					costumSlug: costum.slug
				},
				setType: [{
						path: 'startDate',
						type: 'isoDate'
					},
					{
						path: 'endDate',
						type: 'isoDate'
					},
					{
						path: 'activated',
						type: 'boolean'
					}
				]
			}
			dataHelper.path2Value(tplCtx, function(params) {
				toastr.success('Campagne créer avec succès');
				urlCtrl.loadByHash(location.hash);
			});
		});
	})

	$('.activecampagne').change(function() {
		var active = {
			id: "<?php echo $parentForm["config"] ?>",
			collection: 'forms',
			path: 'campagne.' + $(this).data('id') + '.activated',
			value: true,
			setType: 'boolean'
		}
		var pageKey = $(this).data('slug') != '' ? $(this).data('slug') : sanitazeString($(this).data('name'));

		var tplx = {
			id: costum.contextId,
			collection: costum.contextType,
			path: 'costum.app.#' + pageKey,
			value: {
				"name": {
					"fr": $(this).data('name')
				},
				"metaDescription": "",
				"selectApp": false,
				"coevent": false,
				"event": "",
				"urlExtra": "/page/" + $(this).data('path'),
				"hash": "#app.view",
			},
			setType: [{
					path: 'selectApp',
					type: 'boolean'
				},
				{
					path: 'coevent',
					type: 'boolean'
				},
				{
					path: 'restricted.draft',
					type: 'boolean'
				},
				{
					path: 'restricted.admins',
					type: 'boolean'
				},
			]

		}
		var keyApp = pageKey
		var formData = {
			"name": {
				"fr": $(this).data('name')
			},
			"metaDescription": "",
			"selectApp": false,
			"coevent": false,
			"event": "",
			"urlExtra": "/page/" + pageKey,
			"hash": "#app.view",
		}
		var activeOnMenuTop = {
			id: costum.contextId,
			collection: costum.contextType,
			path: 'costum.htmlConstruct.header.menuTop.left.buttonList.app.buttonList.#' + pageKey,
			value: true,
			setType: 'boolean'
		}
		var key = $(this).data('id');
		var selectedDate = new Date($('.endDate' + key).val());
		if (costum.editMode) {
			var pageList = costumizer.obj.app
			if ($(this).is(':checked') && selectedDate > today) {
				if (typeof pageList["#" + pageKey] != "undefined") {
					dataHelper.path2Value(active, function(params) {
						toastr.success('Campagne activer avec succès');
						dataHelper.path2Value(activeOnMenuTop, function(params) {
							toastr.success('Page activer sur le menu');
							if (costum.editMode) {
								jsonHelper.setValueByPath(costumizer.obj, "app.#" + keyApp, formData);
								jsonHelper.setValueByPath(themeParams.pages, "#" + keyApp, formData);
								jsonHelper.setValueByPath(costum, "appConfig.pages.#" + keyApp, formData);
								jsonHelper.setValueByPath(urlCtrl.loadableUrls, "#" + keyApp, formData);
								var params = {};
								params["app"] = costumizer.obj.app;
								var olderData = {};
								olderData["app"] = jsonHelper.getValueByPath(costumizer.costumDataOlder, "app");
								var pageSelected = $("#costum-form-pages input[name=name]").val();
								ajaxPost(
									null,
									baseUrl + "/" + moduleId + "/cms/updatecostum", {
										params: params,
										costumId: costum.contextId,
										costumType: costum.contextType,
										keyPage: "#" + keyApp,
										action: "costum/page",
										olderData: olderData,
										pathChanged: ["app"]
									},
									function(data) {
										location.href = baseUrl + "/costum/co/index/slug/" + costum.slug + "/edit/true#" + pageKey
										window.location.reload()
										// urlCtrl.loadByHash("#"+pageKey);
									}
								);
							} else {
								// urlCtrl.loadByHash("#"+pageKey);
							}
						});
					});
				} else {
					dataHelper.path2Value(active, function(params) {
						toastr.success('Campagne activer avec succès');
						dataHelper.path2Value(tplx, function(params) {
							toastr.success('Page créer avec succès');
							dataHelper.path2Value(activeOnMenuTop, function(params) {
								toastr.success('Page activer sur le menu');
								if (costum.editMode) {
									jsonHelper.setValueByPath(costumizer.obj, "app.#" + keyApp, formData);
									jsonHelper.setValueByPath(themeParams.pages, "#" + keyApp, formData);
									jsonHelper.setValueByPath(costum, "appConfig.pages.#" + keyApp, formData);
									jsonHelper.setValueByPath(urlCtrl.loadableUrls, "#" + keyApp, formData);
									var params = {};
									params["app"] = costumizer.obj.app;
									var olderData = {};
									olderData["app"] = jsonHelper.getValueByPath(costumizer.costumDataOlder, "app");
									var pageSelected = $("#costum-form-pages input[name=name]").val();
									ajaxPost(
										null,
										baseUrl + "/" + moduleId + "/cms/updatecostum", {
											params: params,
											costumId: costum.contextId,
											costumType: costum.contextType,
											keyPage: "#" + keyApp,
											action: "costum/page",
											olderData: olderData,
											pathChanged: ["app"]
										},
										function(data) {
											location.href = baseUrl + "/costum/co/index/slug/" + costum.slug + "/edit/true#" + pageKey
											window.location.reload()
											// urlCtrl.loadByHash("#"+pageKey);
										}
									);
								} else {
									// urlCtrl.loadByHash("#"+pageKey);
								}
							});
						});
					});
				}
			} else if (!$(this).is(':checked')) {
				active.value = false
				delete activeOnMenuTop.setType;
				activeOnMenuTop.value = null
				dataHelper.path2Value(active, function(params) {
					toastr.success('Campagne desactiver avec succès');
					dataHelper.path2Value(activeOnMenuTop, function(params) {
						if (params) {
							jsonHelper.deleteByPath(costumizer.obj, "app.#" + keyApp);
							jsonHelper.deleteByPath(themeParams.pages, "#" + keyApp);
							jsonHelper.deleteByPath(costum, "appConfig.pages.#" + keyApp);
							jsonHelper.deleteByPath(urlCtrl.loadableUrls, "#" + keyApp);
							var params = {};
							params["app"] = costumizer.obj.app;
							var olderData = {};
							olderData["app"] = jsonHelper.getValueByPath(costumizer.costumDataOlder, "app");
							var pageSelected = $("#costum-form-pages input[name=name]").val();
							ajaxPost(
								null,
								baseUrl + "/" + moduleId + "/cms/updatecostum", {
									params: params,
									costumId: costum.contextId,
									costumType: costum.contextType,
									keyPage: "#" + keyApp,
									action: "costum/page",
									olderData: olderData,
									pathChanged: ["app"]
								},
								function(data) {
									location.reload()
								}
							);
						}
					});

				});
			} else if (selectedDate < today) {
				$(this).removeAttr('checked')
				toastr.error('La date de la campagne est déja depasser');
			}
		} else {
			toastr.error('Passer en mode edit pour pouvoir generer un campagne, et refaire votre action');
		}
	});
	$(document).on("click", ".delete-payment-item", function(e){
		e.stopImmediatePropagation()
		let self = $(this);
		let id = self.data("payment");
		bootbox.confirm({
			message: `Vous êtes sur le point de supprimer le type de paiement ${self.parents(".type-item").find(".type-name").text()}<span class='text-red'></span>`,
			buttons: {
				confirm: {
					label: trad["yes"],
					className: 'btn-success'
				},
				cancel: {
					label: trad["no"],
					className: 'btn-danger'
				}
			},
			callback: function (result) {
				if (!result) {
					return;
				} else {
					param = new Object;
			    	param.id = id;
					ajaxPost(
				        null,
				        baseUrl+"/co2/paymentMethod/delete",
				        param,
				        function(data){
					    	if(data.result){
								toastr.success(data.msg);
								$("#item-paye-"+id).remove();
					    	}
				        }
				    );
				}
			}
		});
	});
	function getPaymentMethod(){
		ajaxPost(
			null,
			baseUrl + "/co2/paymentMethod/getpayments", 
			{},
			function(data) {
				let itemStr = "";
				$.each(data, function(k, v){
					if(v.type == "stripe"){
						itemStr += `<div class="type-item" id="item-paye-${k}">
									<span class="delete-payment-item" data-payment="${k}"><i class="fa fa-times"></i></span>
									<span class="type-name">Stripe</span>
									<div class="type-config">
										<span>Secret key : ${maskWord(v.config.secretKey)}</span>
										<span>Webhook key : ${maskWord(v.config.webhookKey)}</span>
									</div>
								</div>`
					}else if(v.type == "helloasso"){
						itemStr += `<div class="type-item" id="item-paye-${k}">
									<span class="delete-payment-item" data-payment="${k}"><i class="fa fa-times"></i></span>
									<span class="type-name">Hello Asso</span>
									<div class="type-config">
										<span>Client id : ${maskWord(v.config.clientId)}</span>
										<span>Client Secret : ${maskWord(v.config.clientSecret)}</span>
										<span>Slug de l'association : ${v.config.slug}</span>
									</div>
								</div>`
					}else if(v.type == "virement"){
						itemStr += `<div class="type-item" id="item-paye-${k}">
									<span class="delete-payment-item" data-payment="${k}"><i class="fa fa-times"></i></span>
									<span class="type-name">Virement Bancaire</span>
									<div class="type-config">
										<span>IBAN : ${maskWord(v.config.iban)}</span>
									</div>
								</div>`
					}
				})
				var itemParent = $('.type-list-content')
				itemParent.append(itemStr)
			}
		);
	}
	$(function(){
		getPaymentMethod();
	})
</script>