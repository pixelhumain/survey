<?php
    $configs = [
        "Notification" => [
            array(
                "label" => "Nouvelle proposition",
                "info" => "",
                "notShow" => false,
                "input" => [
                    "idCollection" => (string)$parentForm["_id"],
                    "collection" => Form::COLLECTION,
                    "path" => "paramsNotification.newproposition",
                    "type" => "checkbox",
                    "inputId" => "newproposition",
                    "isChecked" => !empty($parentForm["paramsNotification"]["newproposition"]) ? "checked" : ""
                ]
    
            ),
            array(
                "label" => "Suppression proposition",
                "info" => "",
                "notShow" => false,
                "input" => [
                    "idCollection" => (string)$parentForm["_id"],
                    "collection" => Form::COLLECTION,
                    "path" => "paramsNotification.deleteproposition",
                    "type" => "checkbox",
                    "inputId" => "deleteproposition",
                    "isChecked" => !empty($parentForm["paramsNotification"]["deleteproposition"]) ? "checked" : ""
                ]
            ),
            array(
                "label" => "Nouveau status",
                "info" => "",
                "notShow" => false,
                "input" => [
                    "idCollection" => (string)$parentForm["_id"],
                    "collection" => Form::COLLECTION,
                    "path" => "paramsNotification.newstatus",
                    "type" => "checkbox",
                    "inputId" => "newstatus",
                    "isChecked" => !empty($parentForm["paramsNotification"]["newstatus"]) ? "checked" : ""
                ]
            ),
            array(
                "label" => "Nouvelle dépense",
                "info" => "",
                "notShow" => false,
                "input" => [
                    "idCollection" => (string)$parentForm["_id"],
                    "collection" => Form::COLLECTION,
                    "path" => "paramsNotification.newdepense",
                    "type" => "checkbox",
                    "inputId" => "newdepense",
                    "isChecked" => !empty($parentForm["paramsNotification"]["newdepense"]) ? "checked" : ""
                ]
            ),
            array(
                "label" => "Suppression dépense",
                "info" => "",
                "notShow" => false,
                "input" => [
                    "idCollection" => (string)$parentForm["_id"],
                    "collection" => Form::COLLECTION,
                    "path" => "paramsNotification.deletedepense",
                    "type" => "checkbox",
                    "inputId" => "deletedepense",
                    "isChecked" => !empty($parentForm["paramsNotification"]["deletedepense"]) ? "checked" : ""
                ]
            ),
            array(
                "label" => "Nouveau financement",
                "info" => "",
                "notShow" => false,
                "input" => [
                    "idCollection" => (string)$parentForm["_id"],
                    "collection" => Form::COLLECTION,
                    "path" => "paramsNotification.newfinancement",
                    "type" => "checkbox",
                    "inputId" => "newfinancement",
                    "isChecked" => !empty($parentForm["paramsNotification"]["newfinancement"]) ? "checked" : ""
                ]
            ),
            array(
                "label" => "Suppression financement",
                "info" => "",
                "notShow" => false,
                "input" => [
                    "idCollection" => (string)$parentForm["_id"],
                    "collection" => Form::COLLECTION,
                    "path" => "paramsNotification.deletefinancement",
                    "type" => "checkbox",
                    "inputId" => "deletefinancement",
                    "isChecked" => !empty($parentForm["paramsNotification"]["deletefinancement"]) ? "checked" : ""
                ]
            ),
            array(
                "label" => "Nouveau payement",
                "info" => "",
                "notShow" => false,
                "input" => [
                    "idCollection" => (string)$parentForm["_id"],
                    "collection" => Form::COLLECTION,
                    "path" => "paramsNotification.newpayement",
                    "type" => "checkbox",
                    "inputId" => "newpayement",
                    "isChecked" => !empty($parentForm["paramsNotification"]["newpayement"]) ? "checked" : ""
                ]
            ),
            array(
                "label" => "Suppression payement",
                "info" => "",
                "notShow" => false,
                "input" => [
                    "idCollection" => (string)$parentForm["_id"],
                    "collection" => Form::COLLECTION,
                    "path" => "paramsNotification.deletepayement",
                    "type" => "checkbox",
                    "inputId" => "deletepayement",
                    "isChecked" => !empty($parentForm["paramsNotification"]["deletepayement"]) ? "checked" : ""
                ]
            ),
            array(
                "label" => "Nouvelle tache",
                "info" => "",
                "notShow" => false,
                "input" => [
                    "idCollection" => (string)$parentForm["_id"],
                    "collection" => Form::COLLECTION,
                    "path" => "paramsNotification.newtask",
                    "type" => "checkbox",
                    "inputId" => "newtask",
                    "isChecked" => !empty($parentForm["paramsNotification"]["newtask"]) ? "checked" : ""
                ]
            ),
            array(
                "label" => "Suppression tache",
                "info" => "",
                "notShow" => false,
                "input" => [
                    "idCollection" => (string)$parentForm["_id"],
                    "collection" => Form::COLLECTION,
                    "path" => "paramsNotification.deletetask",
                    "type" => "checkbox",
                    "inputId" => "deletetask",
                    "isChecked" => !empty($parentForm["paramsNotification"]["deletetask"]) ? "checked" : ""
                ]
            ),
            array(
                "label" => "Valider tache",
                "info" => "",
                "notShow" => false,
                "input" => [
                    "idCollection" => (string)$parentForm["_id"],
                    "collection" => Form::COLLECTION,
                    "path" => "paramsNotification.checktask",
                    "type" => "checkbox",
                    "inputId" => "checktask",
                    "isChecked" => !empty($parentForm["paramsNotification"]["checktask"]) ? "checked" : ""
                ]
            ),
            array(
                "label" => "Ajouter contributeur",
                "info" => "",
                "notShow" => false,
                "input" => [
                    "idCollection" => (string)$parentForm["_id"],
                    "collection" => Form::COLLECTION,
                    "path" => "paramsNotification.addperson",
                    "type" => "checkbox",
                    "inputId" => "addperson",
                    "isChecked" => !empty($parentForm["paramsNotification"]["addperson"]) ? "checked" : ""
                ]
            ),
            array(
                "label" => "Supprimer contributeur",
                "info" => "",
                "notShow" => false,
                "input" => [
                    "idCollection" => (string)$parentForm["_id"],
                    "collection" => Form::COLLECTION,
                    "path" => "paramsNotification.rmperson",
                    "type" => "checkbox",
                    "inputId" => "rmperson",
                    "isChecked" => !empty($parentForm["paramsNotification"]["rmperson"]) ? "checked" : ""
                ]
            ),
        ]
    ];
?>

<div class="container">
    <div class="row">
        <div class="">
            <h3 class="title">
                <span class="main"> Configurer la notifiction Rocket chat</span>
                <span class="status"> <?= !empty($parentForm["name"]) ? $parentForm["name"] : "" ?> </span>
            </h3>
        </div>
    </div>
</div>

<div class="container">
    <form>
        <?php echo Form::createFormConfigPanel($configs); ?>
    </form>
</div>
