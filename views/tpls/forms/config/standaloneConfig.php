<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="">
                <div class="form-group">
                    <div class="checkbox checbox-switch switch-success">
                        <label for="">
                            Image de couverture
                        </label>
                        <label class="pull-right">

                            <input data-updatepartial="true" data-collection="<?= Form::COLLECTION ?>" data-path="useBannerImg" data-id="<?= (string)$parentForm["_id"] ?>" class="aapchecboxconfig" id="useBannerImgparentform" type="checkbox" <?= !empty($parentForm["useBannerImg"]) && $parentForm["useBannerImg"] ? "checked" : "" ?> />
                            <span></span>

                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <button type="button" class="btn modifyBannerImg"> Modifier </button>
                </div>
                <div>
                    <?php if (count($arrayImg)!=0) {?>
                        <img class="content-image" src="<?php echo $arrayImg[0] ?>" alt="">
                    <?php }else{ ?>
                        <img src="<?= Yii::app()->getModule( "co2" )->assetsUrl."/images/thumbnail-default.jpg"; ?>" alt="" style="height:200px;width:auto"/>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="">
                <div class="form-group">
                    <div class="checkbox checbox-switch switch-success">
                        <label for="">
                            Texte sur le couverture
                        </label>
                        <label class="pull-right">

                            <input data-updatepartial="true" data-collection="<?= Form::COLLECTION ?>" data-path="useBannerText" data-id="<?= (string)$parentForm["_id"] ?>" class="aapchecboxconfig" id="useBannerTextparentform" type="checkbox" <?= !empty($parentForm["useBannerText"]) && $parentForm["useBannerText"] ? "checked" : "" ?> />
                            <span></span>

                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <textarea type="text" class="form-control active-markdown-textcouverture aapinputconfig" data-path="bannerText" data-id="<?= (string)$parentForm["_id"] ?>" data-collection="<?= Form::COLLECTION ?>" > <?= !empty($parentForm["bannerText"]) && $parentForm["bannerText"] ? $parentForm["bannerText"] : "" ?> </textarea>
                </div>
            </div>
        </div>
    </div>
</div>
