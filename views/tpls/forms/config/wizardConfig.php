<?php
$initFiles = Document::getListDocumentsWhere(
    array(
        "id"=> (string)$parentForm["_id"],
        "type"=>'forms',
        "subKey"=>'banner',
    ), "image"
);

$arrayImg = [];
foreach ($initFiles as $key => $value) {
    $arrayImg[]= $value["imagePath"];
}
?>

<div class="container">
    <div class="row">
        <div class="">
            <h3 class="title">
                <span class="main"> wizard et entete - </span>
                <span class="status"> <?= !empty($parentForm["name"]) ? $parentForm["name"] : "" ?> </span>
            </h3>
        </div>
    </div>
</div>

<div class="container">
    <form>

        <div class="col-md-12 panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <div class="checkbox checbox-switch switch-success">
                        <label for="hidden-btnObservatory">
                            Afficher le bouton de l'observatoire
                        </label>

                        <label class="pull-right">

                            <input data-updatepartial="true" data-collection="<?= Form::COLLECTION ?>" data-path="hidden-btnObservatory" data-id="<?= (string)$parentForm["_id"] ?>" class="aapchecboxconfig" id="hidden-btnObservatoryparentform" type="checkbox" <?= !empty($parentForm["hidden-btnObservatory"]) && $parentForm["hidden-btnObservatory"] ? "checked" : "" ?> />
                            <span></span>

                        </label>

                    </div>
                    <small id="" class="text-muted">
                        Afficher le bouton de l'observatoire
                    </small>
                </div>
            </div>
        </div>

        <div class="col-md-12 panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <div class="checkbox checbox-switch switch-success">
                        <label for="hidden-btnObservatory">
                            Utiliser le mode simplifié du payement
                        </label>

                        <label class="pull-right">

                            <input data-updatepartial="true" data-collection="<?= Form::COLLECTION ?>" data-path="usecoremudepense" data-id="<?= (string)$parentForm["_id"] ?>" class="aapchecboxconfig" id="usecoremudepense" type="checkbox" <?= !empty($parentForm["usecoremudepense"]) && $parentForm["usecoremudepense"] ? "checked" : "" ?> />
                            <span></span>

                        </label>

                    </div>
                    <small id="" class="text-muted">
                        Etape 4
                    </small>
                </div>
            </div>
        </div>

        <?php
        foreach ($steps as $stepId => $step) {
            ?>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h5 class=""> <?= $step["id"] ?></h5>

                        <div class="">
                            <div class="form-group">
                                <label for="">
                                    Nom
                                </label>
                                <?php if($isAap){ ?>
                                    <input type="text" class="form-control aapinputconfig" data-path="subForms.<?= $step["id"] ?>.name" value="<?= !empty($step["name"]) ? $step["name"] : "" ?>" data-id="<?= (string)$configForm["_id"] ?>" data-collection="<?= Form::COLLECTION ?>" id="" placeholder="">
                                <?php } ?>
                            </div>
                            <div class="form-group">
                                <div class="checkbox checbox-switch switch-success">
                                    <label for="">
                                        Verrouiller etape
                                    </label>
                                    <label class="pull-right">

                                        <input data-updatepartial="true" data-collection="<?= Form::COLLECTION ?>" data-path="subForms.<?= $step["id"] ?>.lockStep" data-id="<?= (string)$configForm["_id"] ?>" class="aapchecboxconfig" id="hidden-btnObservatoryparentform" type="checkbox" <?= !empty($configForm["subForms"][$step["id"]]["lockStep"]) && $configForm["subForms"][$step["id"]]["lockStep"] ? "checked" : "" ?> />
                                        <span></span>

                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="checkbox checbox-switch switch-success">
                                    <label for="">
                                        Cacher etape

                                    </label>
                                    <label class="pull-right">

                                        <input data-updatepartial="true" data-collection="<?= Form::COLLECTION ?>" data-path="subForms.<?= $step["id"] ?>.hideStep" data-id="<?= (string)$configForm["_id"] ?>" class="aapchecboxconfig" id="hidden-btnObservatoryparentform" type="checkbox" <?= !empty($configForm["subForms"][$step["id"]]["hideStep"]) && $configForm["subForms"][$step["id"]]["hideStep"] ? "checked" : "" ?> />
                                        <span></span>

                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="checkbox checbox-switch switch-success">
                                    <label for="">
                                        Cacher etape sur le standalone
                                    </label>
                                    <label class="pull-right">

                                        <input data-updatepartial="true" data-collection="<?= Form::COLLECTION ?>" data-path="subForms.<?= $step["id"] ?>.hideStepStandalone" data-id="<?= (string)$configForm["_id"] ?>" class="aapchecboxconfig" id="hidden-btnObservatoryparentform" type="checkbox" <?= !empty($configForm["subForms"][$step["id"]]["hideStepStandalone"]) && $configForm["subForms"][$step["id"]]["hideStepStandalone"] ? "checked" : "" ?> />
                                        <span></span>

                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="checkbox checbox-switch switch-success">
                                    <label for="">
                                        Envoie mail après validation de l'étape
                                    </label>
                                    <label class="pull-right">

                                        <input data-toogle="wizardconfigmaildest<?=$stepId?>,wizardconfigdestrolecont<?=$stepId?>,wizardconfigmailhtml<?=$stepId?>,wizardconfigmailhtmlmsg<?=$stepId?>,wizardconfiginputhtmlcont<?=$stepId?>"
                                               data-updatepartial="true" data-collection="<?= Form::COLLECTION ?>" data-path="subForms.<?= $step["id"] ?>.afterValidated.sendMail" data-id="<?= (string)$configForm["_id"] ?>" class="aapchecboxconfig" id="issendmail" type="checkbox" <?= !empty($configForm["subForms"][$step["id"]]["afterValidated"]["sendMail"]) && $configForm["subForms"][$step["id"]]["afterValidated"] ["sendMail"]? "checked" : "" ?> />
                                        <span></span>

                                    </label>
                                </div>
                            </div>
                            <div class="form-group" id="wizardconfigmaildest<?=$stepId?>">
                                <div class="checkbox checbox-switch switch-success">
                                    <label for="">
                                        Destinataire
                                    </label>
                                    <div class="container destcheckbox" >
                                        <div class="checkbox">
                                            <label><input data-value="user" data-path="subForms.<?= $step["id"] ?>.afterValidated.emailTo" class="" style="display: block" type="checkbox" value="">Utilisateur</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input data-value="admin" data-path="subForms.<?= $step["id"] ?>.afterValidated.emailTo" class="" style="display: block"  type="checkbox" value="">Administrateur</label>
                                        </div>
                                        <div class="checkbox ">
                                            <label><input data-value="role" data-path="subForms.<?= $step["id"] ?>.afterValidated.emailTo" data-toogle="wizardconfigdestrole<?=$stepId?>" class="aapchecboxconfig notswitch" style="display: block"  type="checkbox" value="" >Rôles</label>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div id="wizardconfigdestrolecont<?=$stepId?>">
                                <div class="form-group" id="wizardconfigdestrole<?=$stepId?>">
                                    <label for=""> Roles : </label>
                                    <input data-id="<?= (string)$configForm["_id"] ?>" data-collection="<?= Form::COLLECTION ?>" data-path="subForms.<?= $step["id"] ?>.afterValidated.destRole" type="text" class="tagsconfig" id="" placeholder="" value='<?= !empty($configForm["subForms"][$step["id"]]["afterValidated"]["destRole"]) ? (is_array($configForm["subForms"][$step["id"]]["afterValidated"]["destRole"]) ? implode("," , $configForm["subForms"][$step["id"]]["afterValidated"]["destRole"]) : $configForm["subForms"][$step["id"]]["afterValidated"]["destRole"]) : "" ?>'>
                                </div>
                            </div>

                            <div class="form-group" id="wizardconfigmailhtml<?=$stepId?>">
                                <label for=""> Contenu du mail </label> <br>
                                <textarea type="text" class="form-control aapinputconfig" data-path="subForms.<?= $step["id"] ?>.afterValidated.mailMessage" data-id="<?= (string)$configForm["_id"] ?>" data-collection="<?= Form::COLLECTION ?>" > <?= !empty($configForm["subForms"][$step["id"]]["afterValidated"]["mailMessage"]) && $configForm["subForms"][$step["id"]]["afterValidated"]["mailMessage"] ? $configForm["subForms"][$step["id"]]["afterValidated"]["mailMessage"] : "" ?> </textarea>
                            </div>

                            <div class="form-group" id="wizardconfigmailhtmlmsg<?=$stepId?>">
                                <div class="checkbox checbox-switch switch-success">
                                    <label for="">
                                        Ajouter réponse d'un champ au message
                                    </label>
                                    <label class="pull-right">

                                        <input data-toogle="wizardconfiinputhtml<?=$stepId?>" data-updatepartial="true" data-collection="<?= Form::COLLECTION ?>" data-path="subForms.<?= $step["id"] ?>.afterValidated.AddInputAnswer" data-id="<?= (string)$configForm["_id"] ?>" class="aapchecboxconfig" id="hidden-btnObservatoryparentform" type="checkbox" <?= !empty($configForm["subForms"][$step["id"]]["afterValidated"]["AddInputAnswer"]) && $configForm["subForms"][$step["id"]]["afterValidated"]["AddInputAnswer"] ? "checked" : "" ?> />
                                        <span></span>

                                    </label>
                                </div>
                            </div>

                            <div id="wizardconfiginputhtmlcont<?=$stepId?>">
                                <div class="form-group col-md-4" id="wizardconfiinputhtml<?=$stepId?>">
                                    <label for=""> Champ </label>
                                    <select data-collection="<?= Form::COLLECTION ?>" data-path="subForms.<?= $step["id"] ?>.afterValidated.inputAnswer" data-id="<?= (string)$configForm["_id"] ?>" class="form-control select-input" id="select-step" >
                                        <option value="" disabled selected="selected" >Choisir</option>
                                        <?php
                                        if(isset($step["input"])){
                                            foreach ($step["input"] as $inputId => $input){
                                                ?>
                                                <option <?php echo (!empty($configForm["subForms"][$step["id"]]["afterValidated"]["inputAnswer"]) && $configForm["subForms"][$step["id"]]["afterValidated"]["inputAnswer"] != "" && $configForm["subForms"][$step["id"]]["afterValidated"]["inputAnswer"] == $inputId ? "selected" : "") ?> value="<?= $inputId ?>" ><?= @$input["label"] ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <small id="" class="text-muted">

                                    </small>
                                </div>
                            </div>
                            <div class="form-group" >
                                <div class="checkbox checbox-switch switch-success">
                                    <label for="">
                                        Modal après validation de l'étape
                                    </label>
                                    <label class="pull-right">

                                        <input data-toogle="wizardconfigmodal<?=$stepId?>" data-updatepartial="true" data-collection="<?= Form::COLLECTION ?>" data-path="ssubForms.<?= $step["id"] ?>.afterValidated.showModal" data-id="<?= (string)$configForm["_id"] ?>" class="aapchecboxconfig" id="hidden-btnObservatoryparentform" type="checkbox" <?= !empty($configForm["subForms"][$step["id"]]["afterValidated"]["showModal"]) && $configForm["subForms"][$step["id"]]["afterValidated"]["showModal"] ? "checked" : "" ?> />
                                        <span></span>

                                    </label>
                                </div>
                            </div>

                            <div class="form-group" id="wizardconfigmodal<?=$stepId?>">
                                <label for=""> Message dans le modal </label> <br>
                                <textarea type="text" class="form-control aapinputconfig" data-path="subForms.<?= $step["id"] ?>.afterValidated.modalHtml" data-id="<?= (string)$configForm["_id"] ?>" data-collection="<?= Form::COLLECTION ?>" > <?= !empty($configForm["subForms"][$step["id"]]["afterValidated"]["modalHtml"]) && $configForm["subForms"][$step["id"]]["afterValidated"]["modalHtml"] ? $configForm["subForms"][$step["id"]]["afterValidated"]["modalHtml"] : "" ?> </textarea>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </form>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        dataHelper.activateMarkdown2('.active-markdown-textcouverture');

        sectionDyfBannerImg = {
            "jsonSchema" : {
                "title" : "Photo de couverture",
                "icon" : "fa-globe",
                "properties" : {
                    "file" : {
                        "label" : "Photo de couverture",
                        "placeholder" : "Photo de couverture",
                        "inputType" : "uploader",
                        "docType" : "image",
                        "itemLimit" : 1,
                        "filetypes": ["jpeg", "jpg", "gif", "png"],
                        initList : <?php echo json_encode($initFiles) ?>
                    }
                },
                beforeBuild : function(){
                    uploadObj.set("forms", "image", null, null, "/subKey/banner");
                },
                save : function () {}
            }
        };

        $(".modifyBannerImg").off().on("click",function() {
            dyFObj.openForm( sectionDyfBannerImg );
        });

    });
</script>

