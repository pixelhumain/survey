<?php
    use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;

    $configs = [
        "Autres" => [
            array(
                "label" => "Activer les sous-organisations",
                "info" => "",
                "notShow" => false,
                "input" => [
                    "idCollection" => (string)$context["_id"],
                    "collection" => $context["collection"] ?? Organization::COLLECTION,
                    "path" => "oceco.subOrganization",
                    "type" => "checkbox",
                    "inputId" => "activeSubOrganization",
                    "isChecked" => !empty($context["oceco"]["subOrganization"]) ? "checked" : ""
                ]
    
            ),
            array(
                "label" => "Activer la conversion d'un commun en projet",
                "info" => "",
                "notShow" => false,
                "input" => [
                    "idCollection" => (string)$context["_id"],
                    "collection" => $context["collection"] ?? Organization::COLLECTION,
                    "path" => "oceco.convertCommunToProject",
                    "type" => "checkbox",
                    "inputId" => "activeSubOrganization",
                    "isChecked" => !empty($context["oceco"]["convertCommunToProject"]) ? "checked" : ""
                ]
    
            ),
            array(
                "label" => "Cacher le nom de l'organisme à gauche du logo",
                "info" => "",
                "notShow" => empty($context["oceco"]["subOrganization"]),
                "input" => [
                    "idCollection" => (string)$context["_id"],
                    "collection" => $context["collection"] ?? Organization::COLLECTION,
                    "path" => "oceco.webConfig.hideLabelOrganism",
                    "type" => "checkbox",
                    "inputId" => "hideNameOrganizationBesideLogo",
                    "isChecked" => !empty($context["oceco"]["webConfig"]["hideLabelOrganism"]) ? "checked" : ""
                ],
            ),
            array(
                "label" => "Afficher tous les citoyens sur communecter dans les recherches de contributeurs",
                "info" => "",
                "notShow" => false,
                "input" => [
                    "idCollection" => (string)$parentForm["_id"],
                    "collection" => Form::COLLECTION,
                    "path" => "showAllUsersInvite",
                    "type" => "checkbox",
                    "inputId" => "showAllUsersInInvite",
                    "isChecked" => !empty($parentForm["showAllUsersInvite"]) ? "checked" : ""
                ],
            )
        ],

        "Graph de l'observatoire" => [] ,

        "Financement standalone" => [
            array(
                "label" => "Activer les sous-organisations",
                "info" => "",
                "notShow" => false,
                "input" => [
                    "idCollection" => (string)$context["_id"],
                    "collection" => Organization::COLLECTION,
                    "path" => "oceco.subOrganization",
                    "type" => "text",
                    "inputId" => "activeSubOrganization"
                ]
            ),
        ] ,
    ];
    
    $graphs = Aap::aapGraph($context["slug"]);

    foreach ($graphs as $key => $value) {
        $configs["Graph de l'observatoire"][] = array(
            "label" => $value,
            "info" => "",
            "notShow" => false,
            "input" => [
                "idCollection" => (string)$parentForm["_id"],
                "collection" => Form::COLLECTION,
                "path" => "GraphObservatory.".$key,
                "type" => "checkbox",
                "inputId" => "newproposition",
                "isChecked" => !isset($parentForm["GraphObservatory"][$key]) || !empty($parentForm["GraphObservatory"][$key]) ? "checked" : ""
            ]
        );
    }
?>

<style>
    .arrow__body {
        width: 100%;
        height: 95%;
        margin-left: 11px;
        border-width: 3px 0 0 3px;
        border-style: dashed;
        border-color: #7878ef;
        border-top-left-radius: 100%;
    }

    .object1 {
        position: absolute;
        left: 470px;
        top: 0px;
        width: 50px;
        height: 10px;
        z-index: 1;
        color: #7878ef;
    }

    .arrow1 {
        position: absolute;
        top: 30px;
        left: 250px;
        width: 200px;
        height: 38px;
        /* uncomment to see the arrow's rectangle bg*/
        /*   background: white;  */
    }

    .object2 {
        position: absolute;
        left: 470px;
        top: 40px;
        width: 50px;
        height: 10px;
        z-index: 1;
        color: #7878ef;
    }

    .arrow2 {
        position: absolute;
        top: 70px;
        left: 350px;
        width: 100px;
        height: 40px;
        /* uncomment to see the arrow's rectangle bg*/
        /*   background: white;  */
    }

    .object3 {
        position: absolute;
        left: 470px;
        top: 200px;
        width: 50px;
        height: 10px;
        z-index: 1;
        color: #7878ef;
    }

    .arrow3 {
        position: absolute;
        top: 230px;
        left: 250px;
        width: 200px;
        height: 80px;
        /* uncomment to see the arrow's rectangle bg*/
        /*   background: white;  */
    }

    .object__intro-title1{
        text-transform: none;
        font-size: 20px;
    }

</style>

<div class="container">
    <div class="row">
        <div class="mb-3">
            <span class="title">
                <span class="main"> Configurer l'organisation</span>
                <span class="status"> <?= !empty($parentForm["name"]) ? $parentForm["name"] : "" ?> </span>
            </span>
        </div>
    </div>
</div>

<div class="container">
    <form>
        <?php echo Form::createFormConfigPanel($configs); ?>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h5 class=""> Financement Standalone</h5>

                    <div class="">
                        <div class="aapconfi-tutoimg">
                            <div class="object1">
                                <h3 class="object__intro-title1">Text1</h3>
                            </div>

                            <div class="arrow1">
                                <div class="arrow__body"></div>
                            </div>

                            <div class="object2">
                                <h3 class="object__intro-title1">Text2</h3>
                            </div>

                            <div class="arrow2">
                                <div class="arrow__body"></div>
                            </div>

                            <div class="object3">
                                <h3 class="object__intro-title1">Text3</h3>
                            </div>

                            <div class="arrow3">
                                <div class="arrow__body"></div>
                            </div>
                            <img style='height:400px;width:500px;' src='<?php echo Yii::app()->getModule(Survey::MODULE)->getAssetsUrl()?>/images/financementStandalone/screen1.png'/>
                        </div>

                        <div class="form-group" id="wizardconfigmailhtml">
                            <label for=""> Text 1 </label> <br>
                            <textarea type="text" class="form-control aapinputconfig" data-path="custom.financer.text1" data-id="<?= (string)$parentForm["_id"] ?>" data-collection="<?= Form::COLLECTION ?>" > <?= !empty($parentForm["custom"]["financer"]["text1"]) ? $parentForm["custom"]["financer"]["text1"] : ""  ?> </textarea>
                        </div>

                        <div class="form-group" id="wizardconfigmailhtml">
                            <label for=""> Text 2 </label> <br>
                            <textarea type="text" class="form-control aapinputconfig" data-path="custom.financer.text2" data-id="<?= (string)$parentForm["_id"] ?>" data-collection="<?= Form::COLLECTION ?>" > <?= !empty($parentForm["custom"]["financer"]["text2"]) ? $parentForm["custom"]["financer"]["text2"] : ""  ?> </textarea>
                        </div>

                        <div class="form-group" id="wizardconfigmailhtml">
                            <label for=""> Text 3 </label> <br>
                            <textarea type="text" class="form-control aapinputconfig" data-path="custom.financer.text3" data-id="<?= (string)$parentForm["_id"] ?>" data-collection="<?= Form::COLLECTION ?>" > <?= !empty($parentForm["custom"]["financer"]["text3"]) ? $parentForm["custom"]["financer"]["text3"] : ""  ?> </textarea>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h5 class=""> Financement Standalone</h5>

                    <div class="">
                        <div class="aapconfi-tutoimg">
                            <div class="object1">
                                <h3 class="object__intro-title1">Text4</h3>
                            </div>

                            <div class="arrow1">
                                <div class="arrow__body"></div>
                            </div>

                            <div class="object3">
                                <h3 class="object__intro-title1">Text5</h3>
                            </div>

                            <div class="arrow3">
                                <div class="arrow__body"></div>
                            </div>

                            <!-- </div> -->
                            <img style='height:400px;width:500px;' src='<?php echo Yii::app()->getModule(Survey::MODULE)->getAssetsUrl()?>/images/financementStandalone/screen2.png'/>
                        </div>

                        <div class="form-group" id="wizardconfigmailhtml">
                            <label for=""> Text 4 </label> <br>
                            <textarea type="text" class="form-control aapinputconfig" data-path="custom.financer.text4" data-id="<?= (string)$parentForm["_id"] ?>" data-collection="<?= Form::COLLECTION ?>" > <?= !empty($parentForm["custom"]["financer"]["text4"]) ? $parentForm["custom"]["financer"]["text4"] : ""  ?> </textarea>
                        </div>

                        <div class="form-group" id="wizardconfigmailhtml">
                            <label for=""> Text 5 </label> <br>
                            <textarea type="text" class="form-control aapinputconfig" data-path="custom.financer.text5" data-id="<?= (string)$parentForm["_id"] ?>" data-collection="<?= Form::COLLECTION ?>" > <?= !empty($parentForm["custom"]["financer"]["text5"]) ? $parentForm["custom"]["financer"]["text5"] : ""  ?> </textarea>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
