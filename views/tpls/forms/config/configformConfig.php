<?php

?>

<div class="container">
    <div class="row">
        <div class="">
            <h3 class="title">
                <span class="main"> Detail du template appel à projet - </span>
                <span class="status"> <?= $contextConfig["name"] ?> </span>
            </h3>
        </div>
    </div>
</div>

<div class="container">
    <form>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <label for=""> Nom </label>
                    <input type="text" class="form-control aapinputconfig" data-path="name" value="<?= !empty($configForm["name"]) && $configForm["name"] ? $configForm["name"] : "" ?>" data-id="<?= (string)$configForm["_id"] ?>" data-collection="<?= Form::COLLECTION ?>" id="" placeholder="">
                    <small id="" class="text-muted">
                        Nom du template appel à projet
                    </small>
                </div>
                <div class="form-group">
                    <label for=""> Description </label>
                    <textarea type="text" class="form-control active-markdown-configdescrition aapinputconfig" data-path="description" data-id="<?= (string)$configForm["_id"] ?>" data-collection="<?= Form::COLLECTION ?>" > <?= !empty($configForm["description"]) && $configForm["description"] ? $configForm["description"] : "" ?> </textarea>
                    <small id="" class="text-muted">
                        Description du template appel à projet
                    </small>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <div class="checkbox checbox-switch switch-success">
                        <label for="">
                            Rendre votre questionnaire disponible et réutilisables pour d'autre organisation
                        </label>

                        <label class="pull-right">

                            <input data-updatepartial="true" data-collection="<?= Form::COLLECTION ?>" data-path="active" data-id="<?= (string)$configForm["_id"] ?>" class="aapchecboxconfig" id="reuseform" type="checkbox" <?= !empty($configForm["active"]) && $configForm["active"] ? "checked" : "" ?> />
                            <span></span>

                        </label>

                    </div>
                    <small id="" class="text-muted">
                        Partagez et mettre en commun vos questionnaires
                    </small>
                </div>
                <div class="form-group">
                    <div class="checkbox checbox-switch switch-success">
                        <label for="">
                            Privé
                        </label>
                        <label class="pull-right">

                            <input data-updatepartial="true" data-collection="<?= Form::COLLECTION ?>" data-path="private" data-id="<?= (string)$configForm["_id"] ?>" class="aapchecboxconfig" id="shareform" type="checkbox" <?= !empty($configForm["private"]) && $configForm["private"] ? "checked" : "" ?> />
                            <span></span>

                        </label>
                    </div>
                    <small id="" class="text-muted">
                        Seul les sous-organisation de cette element peuvent appliquer ce template
                    </small>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        dataHelper.activateMarkdown('.active-markdown-configdescrition');
    });
</script>

