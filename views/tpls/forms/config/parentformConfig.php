<?php
    $css_js = [
        '/js/action_modal/action_modal.js'
    ];
    HtmlHelper::registerCssAndScriptsFiles($css_js, Yii::app()->getModule(Costum::MODULE)->getAssetsUrl());
    $bgImg = [

    ];
    if(!empty($parentForm["useBackgroundImg"])){
        $bgArr = Document::getListDocumentsWhere(
            array(
                "id"=> (string)$parentForm["_id"],
                "type"=>'form',
                "subKey"=>'bgStandalone',
            ), "image"
        );
        if(!empty($bgArr))
        $bgImg = $bgArr[0];
    }
?>

<div class="container">
    <div class="row">
        <div class="">
            <h3 class="title">
                <span class="main"> Detail du formulaire - </span>
                <span class="status"> <?= !empty($parentForm["name"]) ? $parentForm["name"] : "" ?> </span>
            </h3>
        </div>
    </div>
</div>

<div class="container">
    <form>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <div class="checkbox checbox-switch switch-success">
                        <label for="">
                            Activer le formulaire
                        </label>

                        <label class="pull-right">

                            <input data-toogle="parentformstartdate,parentformenddate" data-updatepartial="true" data-collection="<?= Form::COLLECTION ?>" data-path="active" data-id="<?= (string)$parentForm["_id"] ?>" class="aapchecboxconfig" id="activeconfigform" type="checkbox" <?= !empty($parentForm["active"]) && $parentForm["active"] ? "checked" : "" ?> data-callback="canOrCannotCreateProposal" />
                            <span></span>

                        </label>

                    </div>
                    <small id="" class="text-muted">
                        Ouvert au reponse au utilisateurs autorisé
                    </small>
                </div>

                <div class="col-md-6" id="parentformstartdate">
                    <div class="form-group">
                        <label for=""> Date debut </label>
                        <input type="text" class="form-control aapinputconfig" data-setType="isoDate" data-path="startDate" value="<?= !empty($parentForm['startDate']) ? $parentForm['startDate']->toDateTime()->setTimeZone(new DateTimeZone(Yii::app()->getTimeZone() ?? 'UTC'))->format('Y-m-d H:i') : '' ?>" data-id="<?= (string)$parentForm["_id"] ?>" data-collection="<?= Form::COLLECTION ?>" id="startdateconfig" placeholder="" />
                        <small id="" class="text-muted">
                            Laisser vide si déja disponible
                        </small>
                    </div>
                </div>
                <div class="col-md-6" id="parentformenddate">
                    <div class="form-group">
                        <label for=""> Date fin </label>
                        <input type="text" class="form-control aapinputconfig" data-setType="isoDate" data-path="endDate" value="<?= !empty($parentForm['endDate']) ? $parentForm['endDate']->toDateTime()->setTimeZone(new DateTimeZone(Yii::app()->getTimeZone() ?? 'UTC'))->format('Y-m-d H:i') : '' ?>"  data-id="<?= (string)$parentForm["_id"] ?>" data-collection="<?= Form::COLLECTION ?>" id="enddateconfig" placeholder="" />
                        <small id="" class="text-muted">
                            Laisser vide si toujours disponible
                        </small>
                    </div>
                </div>

            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <div class="checkbox checbox-switch switch-success">
                        <label for="">
                            Activer la Corénumeration
                        </label>

                        <label class="pull-right">

                            <input data-updatepartial="true" data-collection="<?= Form::COLLECTION ?>" data-path="coremu"  data-callback="coremucallback" data-id="<?= (string)$parentForm["_id"] ?>" class="aapchecboxconfig" id="coremuparentform" type="checkbox" <?= !empty($parentForm["coremu"]) && $parentForm["coremu"] ? "checked" : "" ?> />
                            <span></span>

                        </label>

                    </div>
                    <!--<small id="" class="text-muted">
                        Activer la Corénumeration
                    </small>-->
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <div class="checkbox checbox-switch switch-success">
                        <label for="">
                            Le même formulaire peut servir pour différente session
                        </label>

                        <label class="pull-right">

                            <input data-toogle="parentformsessionstep,parentformsessioninput,parentformsessionactual" data-updatepartial="true" data-collection="<?= Form::COLLECTION ?>" data-path="session" data-id="<?= (string)$parentForm["_id"] ?>" class="aapchecboxconfig" id="isSeasonalparentform" type="checkbox" <?= !empty($parentForm["session"]) && $parentForm["session"] ? "checked" : "" ?> />
                            <span></span>

                        </label>

                    </div>
                    <!--<small id="" class="text-muted">
                        Le même formulaire peut engendrer des réponses classé par groupe
                    </small>-->
                </div>
                <div class="form-group col-md-4" id="parentformsessionstep">
                    <label for=""> Etape </label>
                    <select class="form-control" id="select-step" >
                        <option value="" disabled selected="selected" >Choisir</option>
                        <?php
                            $valueStep = "";
                            $valueInput = "";
                            $haveValue = false;
                            if(!empty($parentForm["sessionMapping"])){
                                $haveValue = true;
                                $valueStep = explode(".",$parentForm["sessionMapping"] )[1];
                                $valueStep = explode(".",$parentForm["sessionMapping"] )[2];
                            }
                            foreach ($steps as $stepId => $step){
                        ?>
                                <option <?php echo ($haveValue && $valueStep == $stepId ? "selected" : "") ?>  value="<?= $stepId ?>" ><?= $step["name"] ?></option>
                        <?php
                            }
                        ?>
                    </select>
                    <small id="" class="text-muted">

                    </small>
                </div>
                <div class="form-group col-md-4" id="parentformsessioninput">
                    <label for=""> Champs </label>
                    <select class="form-control select-input" id="selectInputsession" data-collection="<?= Form::COLLECTION ?>" data-path="sessionMapping" data-id="<?= (string)$parentForm["_id"] ?>">
                        <?php
                            if($haveValue){
                            if(isset($steps[$valueStep]["input"])){
                                foreach ($steps[$valueStep]["input"] as $inputId => $input){
                                    ?>
                                    <option <?php echo ($haveValue && $valueInput == $inputId ? "selected" : "") ?> value="<?= $inputId ?>" ><?= @$input["label"] ?></option>
                                    <?php
                                }
                            }
                        }else{ ?>
                            <option value="" disabled selected="selected">Veuillez choisir une étape</option>
                        <?php
                        }
                        ?>
                    </select>
                    <small id="" class="text-muted">

                    </small>
                </div>
                <div class="form-group col-md-4" id="parentformsessionactual">
                    <label for=""> Session actuel </label>
                    <input data-collection="<?= Form::COLLECTION ?>" data-path="actualSession" data-id="<?= (string)$parentForm["_id"] ?>" class="aapinputconfig form-control" id="actualSessionparentform" type="text" <?= !empty($parentForm["actualSession"]) ? $parentForm["actualSession"] : "" ?> />
                    <small id="" class="text-muted">

                    </small>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <label for=""> Nom </label>
                    <input type="text" class="form-control aapinputconfig" data-path="name" value="<?= !empty($parentForm["name"]) ? $parentForm["name"] : "" ?>" data-id="<?= (string)$parentForm["_id"] ?>" data-collection="<?= Form::COLLECTION ?>" id="" placeholder="">
                    <small id="" class="text-muted">
                        Nom du formulaire
                    </small>
                </div>
                <div class="form-group">
                    <label for=""> Nom d'une reponse </label>
                    <input type="text" class="form-control aapinputconfig" data-path="what" value="<?= !empty($parentForm["what"]) ? $parentForm["what"] : "" ?>" data-id="<?= (string)$parentForm["_id"] ?>" data-collection="<?= Form::COLLECTION ?>" id="" placeholder="">
                    <small id="" class="text-muted">
                        Label associé à une reponse
                    </small>
                </div>
                <div class="form-group">
                    <label for=""> Description </label> <br>
                    <small id="" class="text-muted">
                        Description du formulaire
                    </small>
                    <textarea type="text" class="form-control active-markdown-parentformdescrition aapinputconfig" data-path="description" data-id="<?= (string)$parentForm["_id"] ?>" data-collection="<?= Form::COLLECTION ?>" > <?= !empty($parentForm["description"]) && $parentForm["description"] ? $parentForm["description"] : "" ?> </textarea>
                </div>

            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <div class="checkbox checbox-switch switch-success">
                        <label for="">
                            <?= Yii::t("common","Use a banner image") ?>
                        </label>

                        <label class="pull-right">
                            <input data-updatepartial="true" data-collection="<?= Form::COLLECTION ?>" data-path="useBannerImg"  data-callback="" data-id="<?= (string)$parentForm["_id"] ?>" class="aapchecboxconfig" id="useBannerImg" type="checkbox" <?= !empty($parentForm["useBannerImg"]) && $parentForm["useBannerImg"] ? "checked" : "" ?> />
                            <span></span>
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <div class="checkbox checbox-switch switch-success">
                        <label for="">
                            <?= Yii::t("common","Use a banner text") ?>
                        </label>

                        <label class="pull-right">
                            <input data-updatepartial="true" data-collection="<?= Form::COLLECTION ?>" data-path="useBannerText"  data-callback="" data-id="<?= (string)$parentForm["_id"] ?>" class="aapchecboxconfig" id="useBannerText" type="checkbox" <?= !empty($parentForm["useBannerText"]) && $parentForm["useBannerText"] ? "checked" : "" ?> />
                            <span></span>
                        </label>
                    </div>
                </div>
        
                <div class="form-group" style="display : <?=!empty($parentForm["useBannerImg"]) || !empty($parentForm["useBannerText"]) ? '' : 'none' ?>">
                    <a href="javascript:;" id='go-to-banner-config' class="btn btn-sm btn-default">
                        <?= Yii::t("common","Go to the banner configuration page") ?>
                    </a>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <div class="checkbox checbox-switch switch-success">
                        <label for="">
                            <?= Yii::t("common","Use a background image in a link to respond") ?>
                        </label>

                        <label class="pull-right">
                            <input data-updatepartial="true" data-collection="<?= Form::COLLECTION ?>" data-path="useBackgroundImg"  data-callback="" data-id="<?= (string)$parentForm["_id"] ?>" class="aapchecboxconfig" id="useBackgroundImg" type="checkbox" <?= !empty($parentForm["useBackgroundImg"]) && $parentForm["useBackgroundImg"] ? "checked" : "" ?> />
                            <span></span>
                        </label>
                    </div>
                </div>
                
                <div class="form-group add-background-container" style="display : <?=!empty($parentForm["useBackgroundImg"]) ? '' : 'none' ?>">
                    <div class="form-group">
                        <form method="post" action="" enctype="multipart/form-data" id="myform" class="add-background-form-container" style="display:none">
                            <div class='preview-bg-standalone' style="display: <?=!empty($bgImg) ? "" : 'none'?>">
                                <img src="<?= $bgImg['imagePath'] ?? "" ?>" id="img" width="100" height="100" style="object-fit:contain;width:100%">
                                <a href="javascript:;" data-id="<?= (string)@$bgImg['_id'] ?? '' ?>" id="delete-bg-standalone" class="btn btn-sm btn-danger">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </div>
                            <div style="display: <?=!empty($bgImg['imagePath']) ? 'none' : 'flex' ?> ;margin-top:5px" >
                                <input type="file" id="file" name="file" class="form-control" accept=".jpg,.png" placeholder="ddd"/>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="form-group">
                    <div class="checkbox checbox-switch switch-success">
                        <label for="">
                            <?= Yii::t("common","Use a background color in a link to respond") ?>
                        </label>

                        <label class="pull-right">
                        <input data-updatepartial="true" data-collection="<?= Form::COLLECTION ?>" data-path="useBackgroundColor"  data-callback="" data-id="<?= (string)$parentForm["_id"] ?>" class="aapchecboxconfig" id="useBackgroundColor" type="checkbox" <?= !empty($parentForm["useBackgroundColor"]) && $parentForm["useBackgroundColor"] ? "checked" : "" ?> />
                            <span></span>
                        </label>
                    </div>
                </div>

                <div class="form-group add-backgroundColor-container" style="display : <?=!empty($parentForm["useBackgroundColor"]) ? '' : 'none' ?>">
                    <input type="color" class="form-control" id="useBackgroundColor-picker" value="<?= !empty($parentForm["standaloneBgColor"]) && !empty($parentForm["useBackgroundColor"]) ? $parentForm["standaloneBgColor"] : ""?>">
                </div>
            </div>
        </div>

    </form>
</div>

<script type="text/javascript">
    var formsteps = <?php echo json_encode($steps); ?>;
    function coremucallback(){
        var form = $('input[data-callback="coremucallback"]').data('id');
        ajaxPost("",baseUrl+'/survey/form/switchcoremu/form/'+form,{},function(data){});
    }
    
    function canOrCannotCreateProposal(response) {
        var newProposalButtonDom = $('.proposalDropdown>.dropdown-menu a:nth-child(3)');
        var form = response.elt;
        if(form.active) {
            newProposalButtonDom.hide();
            var url = baseUrl + '/co2/aap/proposition/request/user_have_answer';
            var post = {
                form: form._id.$id
            };
            ajaxPost(null, url, post, function(results) {
                if(form.oneAnswerPerPers) {
                    if(!results.hasAnswer && typeof results?.session == "string" && results.session == "include") newProposalButtonDom.show();
                } else {
                    if (typeof results?.session == "string" && results.session == "include") {
                        newProposalButtonDom.show();
                    } else {
                        newProposalButtonDom.hide();
                    }
                    // newProposalButtonDom.show();
                }
            });
        } else {
            newProposalButtonDom.hide();
        }
    }

    $(document).ready(function() {
        dataHelper.activateMarkdown('.active-markdown-parentformdescrition');

        $('#startdateconfig').datetimepicker({
            format : 'Y-m-d H:i'
        });

        $('#enddateconfig').datetimepicker({
            format : 'Y-m-d H:i'
        });

        $('#select-step').on( 'change' , function(){
            var thisselect = $(this);

            if(typeof thisselect.val() != 'undefined'){
                $('#selectInputsession').html('');
                $('#selectInputsession').append($('<option>', {
                    value: "",
                    text : "choisir",
                    disabled : "true"
                }));
                $.each(formsteps[thisselect.val()]['inputs'] , function(index, value){
                    $('#selectInputsession').append($('<option>', {
                        value: "answers." + formsteps[thisselect.val()]['step'] + "." + index,
                        text : value.label
                    }));
                });
            }
        });

        $('.select-input').on( 'change' , function(){
            var thisbtn = $(this);
            var tplCtx = {
                collection : thisbtn.data("collection"),
                id : thisbtn.data("id"),
                path : thisbtn.data("path"),
                value : thisbtn.val()
            };

            dataHelper.path2Value( tplCtx, function(params) {
                toastr.success('Configuration mis à jour');
            });
        });

        $('#useBackgroundColor-picker').on('change',function(){
            const val = $(this).val();
            dataHelper.path2Value({
                id : "<?= (string)$parentForm["_id"] ?>",
                collection : "forms",
                path : "standaloneBgColor",
                value : val,
            }, function ({params}) {
                toastr.success(trad.saved);
            });
        })
        if(aapObj?.common?.getQuery()?.context && aapObj?.common?.getQuery()?.formid){
            $('#go-to-banner-config').off().on('click',function(){
                urlCtrl.loadByHash("#configurationAap.context."+aapObj.common.getQuery().context+".formid."+aapObj.common.getQuery().formid+".aapview.inputlist");
            })
        }else{
            $('#go-to-banner-config').hide();
        }

        function backgroundImageStandalone(obj){
            var addSignature = function(){
                let res = null;
                var fd = new FormData();
                var files = $('#file')[0].files;
                if(files.length > 0 ){
                    fd.append('qquuid','fsdfsdf-yuiyiu-khfkjsdf-dfsd');
                    fd.append('qqfilename',files[0]['name']);
                    fd.append('qqtotalfilesize',files[0]['size']);
                    fd.append('qqfile',files[0]);
                    //fd.append('costumSlug', aapObj.context.slug);
                    $.ajax({
                        url: baseUrl+'/co2/document/upload-save/dir/communecter/folder/form/ownerId/<?= (string)$parentForm["_id"] ?>/input/qqfile/docType/image/contentKey/slider/subKey/bgStandalone',
                        type: 'post',
                        data: fd,
                        contentType: false,
                        async: false,
                        processData: false,
                        success: function(response){
                            if(response.result){
                                toastr.success(response.msg)
                                res = response;
                            }else{
                                toastr.error(trad["Size maximum 5Mo"])
                            }
                            
                        },
                    });
                }else{
                    toastr.error("Ajouter une image");
                }
                return res;
            };
            $('#file').on('change',function(){
                const [file] = $('#file')[0].files;
                if (file) {
                    var res = addSignature();
                    if(res.result){
                        $(".preview-bg-standalone").show(800);
                        $(".preview-bg-standalone img").attr('src',URL.createObjectURL(file));
                        $("#delete-bg-standalone").data('id',res?.id?.$id);
                        $("#file").parent().css("display","none");
                    }
                }
            })

            $('#delete-bg-standalone').off().on('click',function(){
                const imgId = $(this).data('id');
                const data = {
                    "parentId": "<?= (string)$parentForm["_id"] ?>",
                    "ids" : [imgId],
                    "parentType": "forms"
                };
                $.ajax({
                        url: baseUrl+'/co2/document/delete/contextType/forms/contextId/<?= (string)$parentForm["_id"] ?>',
                        type: 'post',
                        data: data,
                        dataType: 'json',
                        success: function(response){
                            if(response.result){
                                toastr.success(response.msg)
                                $(".preview-bg-standalone").hide(800);
                                $("#file").parent().css("display","flex");
                            }else{
                                toastr.error(response.msg)
                            }
                            
                        },
                    });
            })
        }
        backgroundImageStandalone()

    });
</script>

