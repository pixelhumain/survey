<div class="container">
    <div class="row">
        <div class="">
            <h3 class="title">
                <span class="main"> Accessibilité de l'appel à projet - </span>
                <span class="status"> <?= !empty($parentForm["name"]) ? $parentForm["name"] : "" ?> </span>
            </h3>
        </div>
    </div>
</div>

<div class="container">
    <form>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <div class="checkbox checbox-switch switch-success">
                        <label for="">
                            Seul la communauté peut acceder à l'interface appel à projet
                        </label>
                        <label class="pull-right">
                            <input data-collection="<?= Form::COLLECTION ?>" data-path="onlyMembersCanSeeListAnswer"
                                   data-id="<?= (string)$parentForm["_id"] ?>" class="aapchecboxconfig" id="onlyMembersCanSeeListAnswer"
                                   type="checkbox" <?= !empty($parentForm["onlyMembersCanSeeListAnswer"]) && $parentForm["onlyMembersCanSeeListAnswer"] ? "checked" : "" ?> />
                            <span></span>
                        </label>
                    </div>
                    <small id="" class="text-muted">
                        L'admin a toujours accès
                    </small>
                </div>
                <div class="form-group">
                    <div class="checkbox checbox-switch switch-success">
                        <label for="">
                            Restreindre l'accès par des rôles
                        </label>
                        <label class="pull-right">
                            <input data-toogle="formaccessrole" data-collection="<?= Form::COLLECTION ?>" data-path="listAccessHaveRules"
                                   data-id="<?= (string)$parentForm["_id"] ?>" class="aapchecboxconfig" id="listAccessHaveRules"
                                   type="checkbox" <?= !empty($parentForm["listAccessHaveRules"]) && $parentForm["listAccessHaveRules"] ? "checked" : "" ?> />
                            <span></span>
                        </label>
                    </div>
                    <small id="" class="text-muted">
                        L'admin a toujours accès
                    </small>
                </div>
                <div class="form-group" id="formaccessrole">
                    <label for=""> Rôles : </label>
                    <input data-id="<?= (string)$parentForm["_id"] ?>" data-collection="<?= Form::COLLECTION ?>" data-path="listAccessRules"
                           type="text" class="tagsconfig" id="" placeholder=""
                           value='<?= !empty($parentForm["listAccessRules"]) ? (is_array($parentForm["listAccessRules"]) ? implode(",", $parentForm["listAccessRules"]) : $parentForm["listAccessRules"]) : "" ?>'>
                </div>
                <div class="form-group">
                    <div class="checkbox checbox-switch switch-success">
                        <label for="">
                            Disponible sans compte
                        </label>
                        <label class="pull-right">
                            <input data-collection="<?= Form::COLLECTION ?>" data-path="DisconnectedView"
                                   data-id="<?= (string)$parentForm["_id"] ?>" class="aapchecboxconfig" id="DisconnectedView"
                                   type="checkbox" <?= !empty($parentForm["DisconnectedView"]) && $parentForm["DisconnectedView"] ? "checked" : "" ?> />
                            <span></span>
                        </label>
                    </div>
                    <small id="" class="text-muted">

                    </small>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-md-6 form-group">
                    <div class="checkbox checbox-switch switch-success">
                        <label for="">
                            Seul la communauté peut repondre à ce questionnaire
                        </label>

                        <label class="pull-right">

                            <input data-updatepartial="true" data-collection="<?= Form::COLLECTION ?>" data-path="onlymemberaccess"
                                   data-id="<?= (string)$parentForm["_id"] ?>" class="aapchecboxconfig" id="onlymemberaccessparentform"
                                   type="checkbox" <?= !empty($parentForm["onlymemberaccess"]) && $parentForm["onlymemberaccess"] ? "checked" : "" ?> />
                            <span></span>

                        </label>

                    </div>
                    <small id="" class="text-muted">
                        Seul les administrateurs et les membres pourons acceder et repondre.
                    </small>
                </div>
                <div class="col-md-6 form-group">
                    <div class="checkbox checbox-switch switch-success">
                        <label for="">
                            Une reponse par personne
                        </label>

                        <label class="pull-right">

                            <input data-updatepartial="true" data-collection="<?= Form::COLLECTION ?>" data-path="oneAnswerPerPers"
                                   data-id="<?= (string)$parentForm["_id"] ?>" class="aapchecboxconfig" id="oneAnswerPerPersparentform"
                                   type="checkbox" <?= !empty($parentForm["oneAnswerPerPers"]) && $parentForm["oneAnswerPerPers"] ? "checked" : "" ?> data-callback="canOrCannotCreateProposal" />
                            <span></span>

                        </label>

                    </div>
                    <small id="" class="text-muted">
                        Un utilisateur ne peut répondre qu'une seul fois, ils sera toujours rédirigé vers sa reponse
                    </small>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-md-6 form-group">
                    <div class="checkbox checbox-switch switch-success">
                        <label for="">
                            Pouvoir répondre sans compte
                        </label>
                        <label class="pull-right">

                            <input data-toggle="formaccessstartdate,formaccessenddate" data-updatepartial="true"
                                   data-collection="<?= Form::COLLECTION ?>" data-path="temporarymembercanreply"
                                   data-id="<?= (string)$parentForm["_id"] ?>" class="aapchecboxconfig" id="temporarymembercanreplyparentform"
                                   type="checkbox" <?= !empty($parentForm["temporarymembercanreply"]) && $parentForm["temporarymembercanreply"] ? "checked" : "" ?> />
                            <span></span>

                        </label>
                    </div>
                    <small id="" class="text-muted">
                        Le formulaire est accessible en fournissant un email qui va créer un compte temporaire
                    </small>
                </div>
                <div class="col-md-6 form-group">
                    <div class="checkbox checbox-switch switch-success">
                        <label for="">
                            Envoyer une confirmation mail pour les réponses avec un compte temporaire
                        </label>
                        <label class="pull-right">

                            <input data-updatepartial="true" data-collection="<?= Form::COLLECTION ?>" data-path="withconfirmation"
                                   data-id="<?= (string)$parentForm["_id"] ?>" class="aapchecboxconfig" id="withconfirmationparentform"
                                   type="checkbox" <?= !empty($parentForm["withconfirmation"]) && $parentForm["withconfirmation"] ? "checked" : "" ?> />
                            <span></span>

                        </label>
                    </div>
                    <small id="" class="text-muted">
                        Vérification de l'email de l'utilisateur
                    </small>
                </div>
                <div class="col-md-6" id="formaccessstartdate">
                    <div class="form-group">
                        <label for=""> Date debut </label>
                        <input type="text" class="form-control aapinputconfig" data-setType="isoDate" data-path="startDateNoconfirmation"
                               value="<?= !empty($parentForm["startDateNoconfirmation"]) ? $parentForm["startDateNoconfirmation"]->toDateTime()->setTimeZone(new DateTimeZone(Yii::app()->getTimeZone() ?? 'UTC'))->format('Y-m-d H:i') : "" ?>"
                               data-id="<?= (string)$parentForm["_id"] ?>" data-collection="<?= Form::COLLECTION ?>"
                               id="startDateNoconfirmationconfig" placeholder=""/>
                        <small id="" class="text-muted">
                            Laisser vide si la même que celle du formulaire
                        </small>
                    </div>
                </div>
                <div class="col-md-6" id="formaccessenddate">
                    <div class="form-group">
                        <label for=""> Date fin </label>
                        <input type="text" class="form-control aapinputconfig" data-setType="isoDate" data-path="endDateNoconfirmation"
                               value="<?= !empty($parentForm["endDateNoconfirmation"]) ? $parentForm["endDateNoconfirmation"]->toDateTime()->setTimeZone(new DateTimeZone(Yii::app()->getTimeZone() ?? 'UTC'))->format('Y-m-d H:i') : "" ?>"
                               data-id="<?= (string)$parentForm["_id"] ?>" data-collection="<?= Form::COLLECTION ?>" id="endDateNoconfirmationconfig"
                               placeholder=""/>
                        <small id="" class="text-muted">
                            Laisser vide si la même que celle du formulaire
                        </small>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-md-6 form-group">
                    <div class="checkbox checbox-switch switch-success">
                        <label for="">
                            Les reponses sont-elles accessible (mode lecture) au membres
                        </label>
                        <label class="pull-right">

                            <input data-updatepartial="true" data-collection="<?= Form::COLLECTION ?>" data-path="onlyusercanread"
                                   data-id="<?= (string)$parentForm["_id"] ?>" class="aapchecboxconfig" id="onlyusercanreadparentform"
                                   type="checkbox" <?= !empty($parentForm["onlyusercanread"]) && $parentForm["onlyusercanread"] ? "checked" : "" ?> />
                            <span></span>

                        </label>
                    </div>
                    <small id="" class="text-muted">
                        Les membres peuvent lire toutes les réponses
                    </small>
                </div>
                <div class="col-md-6 form-group">
                    <div class="checkbox checbox-switch switch-success">
                        <label for="">
                            Les reponses sont-elles accessible (mode ecriture) au membres
                        </label>
                        <label class="pull-right">

                            <input data-updatepartial="true" data-collection="<?= Form::COLLECTION ?>" data-path="onlyusercanedit"
                                   data-id="<?= (string)$parentForm["_id"] ?>" class="aapchecboxconfig" id="onlyusercaneditparentform"
                                   type="checkbox" <?= !empty($parentForm["onlyusercanedit"]) && $parentForm["onlyusercanedit"] ? "checked" : "" ?> />
                            <span></span>

                        </label>
                    </div>
                    <small id="" class="text-muted">
                        Les membres peuvent lire toutes les réponses
                    </small>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-md-6 form-group">
                    <div class="checkbox checbox-switch switch-success">
                        <label for="">
                            Le financement est-il accessible (mode lecture) en etant déconnecté
                        </label>
                        <label class="pull-right">

                            <input data-updatepartial="true" data-collection="<?= Form::COLLECTION ?>" data-path="notloggedview"
                                   data-id="<?= (string)$parentForm["_id"] ?>" class="aapchecboxconfig" id="notloggedviewparentform"
                                   type="checkbox" <?= !empty($parentForm["notloggedview"]) && $parentForm["notloggedview"] ? "checked" : "" ?> />
                            <span></span>

                        </label>
                    </div>
                    <small id="" class="text-muted">
                        Les membres peuvent lire toutes les réponses
                    </small>
                </div>
                <div class="col-md-6 form-group">
                    <div class="checkbox checbox-switch switch-success">
                        <label for="">
                            crowdfunding
                        </label>
                        <label class="pull-right">

                            <input data-updatepartial="true" data-collection="<?= Form::COLLECTION ?>" data-path="crowdfunding"
                                   data-id="<?= (string)$parentForm["_id"] ?>" class="aapchecboxconfig" id="crowdfundingparentform"
                                   type="checkbox" <?= !empty($parentForm["crowdfunding"]) && $parentForm["crowdfunding"] ? "checked" : "" ?> />
                            <span></span>

                        </label>
                    </div>
                    <small id="" class="text-muted">
                        Les membres peuvent lire toutes les réponses
                    </small>
                </div>
            </div>
        </div>
        
        <?php
            if ($canAdminConfig) {
                foreach ($steps as $stepId => $step) {
                    ?>
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h5 class=""> <?= $step["name"] ?></h5>

                                <div class="">
                                    <div class="form-group">
                                        <div class="checkbox checbox-switch switch-success">
                                            <label for="">
                                                Restreindre l'accès en lecture
                                            </label>
                                            <label class="pull-right">
                                                
                                                <?php if ($isAap) { ?>
                                                    <input data-toggle="formaccessrole<?= $stepId ?>" data-collection="<?= Form::COLLECTION ?>"
                                                           data-path="subForms.<?= $step["id"] ?>.params.haveReadingRules"
                                                           data-id="<?= (string)$configForm["_id"] ?>" class="aapchecboxconfig"
                                                           id="haveReadingRules<?= $step["id"] ?>parentform"
                                                           type="checkbox" <?= !empty($configForm["subForms"][$step["id"]]["params"]["haveReadingRules"]) && $configForm["subForms"][$step["id"]]["params"]["haveReadingRules"] ? "checked" : "" ?> />
                                                <?php } ?>
                                                <span></span>

                                            </label>
                                        </div>
                                        <small id="" class="text-muted">
                                            L'admin a toujours accès
                                        </small>
                                    </div>
                                    <div class="form-group" id="formaccessroleread<?= $stepId ?>">
                                        <label for=""> Rôle</label>
                                        <input data-id="<?= (string)$configForm["_id"] ?>" data-collection="<?= Form::COLLECTION ?>"
                                               data-path="subForms.<?= $step["id"] ?>.params.canRead" type="text" class="tagsconfig" id=""
                                               placeholder=""
                                               value='<?= !empty($configForm["subForms"][$step["id"]]["params"]["canRead"]) ? (is_array($configForm["subForms"][$step["id"]]["params"]["canRead"]) ? implode(",", $configForm["subForms"][$step["id"]]["params"]["canRead"]) : $configForm["subForms"][$step["id"]]["params"]["canRead"]) : "" ?>'>
                                    </div>
                                </div>

                                <div class="">
                                    <div class="form-group">
                                        <div class="checkbox checbox-switch switch-success">
                                            <label for="">
                                                Restreindre l'accès en ecriture
                                            </label>
                                            <label class="pull-right">
                                                
                                                <?php if ($isAap) { ?>
                                                    <input data-toggle="formaccèsrolewrite<?= $stepId ?>" data-collection="<?= Form::COLLECTION ?>"
                                                           data-path="subForms.<?= $step["id"] ?>.params.haveEditingRules"
                                                           data-id="<?= (string)$configForm["_id"] ?>" class="aapchecboxconfig"
                                                           id="haveEditingRules<?= $step["id"] ?>parentform"
                                                           type="checkbox" <?= !empty($configForm["subForms"][$step["id"]]["params"]["haveEditingRules"]) && $configForm["subForms"][$step["id"]]["params"]["haveEditingRules"] ? "checked" : "" ?> />
                                                <?php } ?>
                                                <span></span>

                                            </label>
                                        </div>
                                        <small id="" class="text-muted">
                                            L'admin a toujours accès
                                        </small>
                                    </div>
                                    <div class="form-group" id="formaccèsrolewrite<?= $stepId ?>">
                                        <label for=""> Rôle</label>
                                        <input data-id="<?= (string)$configForm["_id"] ?>" data-collection="<?= Form::COLLECTION ?>"
                                               data-path="subForms.<?= $step["id"] ?>.params.canEdit" type="text" class="tagsconfig" id=""
                                               placeholder=""
                                               value='<?= !empty($configForm["subForms"][$step["id"]]["params"]["canEdit"]) ? (is_array($configForm["subForms"][$step["id"]]["params"]["canEdit"]) ? implode(",", $configForm["subForms"][$step["id"]]["params"]["canEdit"]) : $configForm["subForms"][$step["id"]]["params"]["canEdit"]) : "" ?>'>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
        ?>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(".tagsconfig").select2({
            minimumInputLength : 3,
            //tags:["red", "green", "blue","bluedarkkk"],
            tags : true,
            multiple : true,
            initSelection : function (element, callback) {
                var temp = [];
                mylog.log(element.val().split(","), 'rakoto0')
                $(element.val().split(",")).each(function () {
                    temp.push({id : this, text : this});
                });
                callback(temp);
            },
            createSearchChoice : function (term, data) {
                if (!data.length) {
                    return {
                        id : term,
                        text : term
                    };
                }
            },
            ajax : {
                url : /*baseUrl + '/api/tags/search'*/baseUrl+'/co2/aap/searchroles/',
                dataType : 'json',
                type : "GET",
                quietMillis : 50,
                data : function (term) {
                    return {
                        q : term,
                        contextId : aapObj.context.id,
                        contextType: aapObj.context.collection,
						key:"params.tags.list"
                    };
                },
                results : function (data) {
                    return {
                        results : $.map(data, function (item) {
                            return {
                                text : item.tag,
                                id : item.tag
                            }
                        })
                    };
                }
            }
        }).on('change', function () {
            var thistags = $(this);
            var tplCtx = {
                id : thistags.data('id'),
                path : thistags.data('path'),
                value : thistags.select2('val'),
                collection : thistags.data('collection')
            }
            dataHelper.path2Value(tplCtx, function (params) {
                toastr.success('Configuration mis à jour');
            })
        });

        $('#startDateNoconfirmationconfig').datetimepicker({
            format : 'Y-m-d H:i'
        });

        $('#endDateNoconfirmationconfig').datetimepicker({
            format : 'Y-m-d H:i'
        });
        $(".tagsconfig").select2('data',);
    });
</script>

