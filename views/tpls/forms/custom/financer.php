<?php
#todo
if (!strpos($_SERVER["REQUEST_URI"], "costum")) {
    HtmlHelper::registerCssAndScriptsFiles(array(
        '/js/aap/aap.js',
        '/css/aap/aap.css'
    ), Yii::app()->getModule("co2")->getAssetsUrl());
    HtmlHelper::registerCssAndScriptsFiles([
        '/plugins/select2/select2.min.js' ,
        '/plugins/select2/select2.css' ,
    ], Yii::app()->request->baseUrl);
}
$ignore = array('_file_', '_params_', '_obInitialLevel_', 'ignore');
$params = array_diff_key(get_defined_vars() + array_flip($ignore), array_flip($ignore));

$isftl = $input["type"] == "tpls.forms.ocecoform.newDepenseList";
if (!$isftl) {
    HtmlHelper::registerCssAndScriptsFiles(array(
        '/css/custom/financer.css'
    ), Yii::app()->getModule(Survey::MODULE)->getAssetsUrl());
    HtmlHelper::registerCssAndScriptsFiles([
        '/js/aap/jointl.js'
    ], Yii::app()->getModule('costum')->getAssetsUrl());
}

$cssJS = array(
    '/plugins/swiper/swiper.min.css',
    '/plugins/swiper/swiper.min.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);

$bannerTitleStandalone = @$parentForm["name"];
$bannerFooterStandalone = "<p style='font-size:23px'>" . ucfirst(Yii::t("common", "the holder")) . " : <a href='#page.type." . $context["collection"] . ".id." . (string)$context["_id"] . "' class=' lbh-preview-element'>" . ucfirst($context["name"]) . "</a></p>";
if (isset($blockCms["bannerTitleStandalone"])) {
    $bannerTitleStandalone = $blockCms["bannerTitleStandalone"];
}
$bgImg = Yii::app()->getModule("survey")->assetsUrl . "/images/oceco_background.png";
$bgColor = !empty($parentForm["standaloneBgColor"]) && !empty($parentForm["useBackgroundColor"]) ? $parentForm["standaloneBgColor"] : "--aap-primary-color";
if (!empty($parentForm["useBackgroundImg"]) && !empty($params["Arr"])) {
    $bgImg = $params["Arr"][0]["imagePath"];
}
$depenseFinancer = [];
if (!empty(Yii::app()->session['userId']) && !empty($params["get_tls"])) {
    $depenseFinancer = $params["get_tls"];
}
if (!empty(Yii::app()->session['userId']) && !empty($params["get_orga"])) {
    $depenseFinancerOrga = $params["get_orga"];
}
$totalfinancement = $params["totalfinancement"];
?>

<style type="text/css">
    @font-face {
        font-family: "montserrat";
        src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/montserrat-regular.woff") format("woff"),
        url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/montserrat-regular.ttf") format("ttf")
    }

    .mst {
        font-family: 'montserrat' !important;
    }

    @font-face {
        font-family: "CoveredByYourGrace";
        src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/CoveredByYourGrace.ttf")
    }

    .cbyg {
        font-family: 'CoveredByYourGrace' !important;
    }

    /*div.mm-dropdown {
        border: 1px solid #ddd;
        width : auto;
        max-width: 250px;
        border-radius: 25px;
        margin: 5px;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        cursor: pointer;
        height: 30px;
        padding: 0px 0px;
    }

    div.mm-dropdown ul {
        list-style: none;
        position: absolute;
        z-index: 999999;
        background-color: var(--pfpb-tertiary-grey-color);
        border-radius: 15px;
        cursor: pointer;
        padding: 0;
    }

    div.mm-dropdown ul li {
        padding: 5px;
        color: var(--pfpb-primary-grey-color);

    }

    div.mm-dropdown ul li:hover {
        background-color: rgba(241,241,241,0.87);
        color: var(--pfpb-primary-blue-color);
    }

    div.mm-dropdown div.textfirst {
        height: 30px;
        color: var(--pfpb-primary-blue-color);
        font-weight: bold;
        width : auto;
        max-width: 250px;
        margin-right: 10px;
        !*margin-left: 10px;*!
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        font-size: 14px;
    }

    div.mm-dropdown ul li {
        display: none;
        padding-left: 5px;
    }

    div.mm-dropdown ul li.main {
        display: block;
    }

    div.mm-dropdown img {
        width: 25px;
        height: 25px;
        margin: 0px 5px;
        border-radius: 25px;
    }

    div.mm-dropdown .textfirst img {
        margin: 2px;
        margin-left: px;
        margin-right 10px;
        border: 2px solid var(--pfpb-primary-green-color);
    }*/

    @media (max-width : 400px) {
        .slideinnnerheight{
            padding-left: 0 !important;
            padding-right: 0 !important;
        }
        .slideinnnerheight .for-checkbox-budget {
            width: 80px !important;
        }
        .slideinnnerheight .wrapper {
            height: auto !important;
            max-height: 80% !important;
            min-height: 50% !important;
            padding: 0 10px !important;
        }
        .slideinnnerheight .selectAllLine {
            margin-left: 25px !important;
            margin-top: 0 !important;
        }
        .depensefinancerdropdown .inputfirst {
            width: 100% !important;
            margin-bottom: 10px;
        }
        .depensefinancerdropdown .inputfirst input {
            width: 100% !important;
        }
        .financerchoicetab .mm-dropdown-container {
            padding: 10px !important;
        }
        .financerchoicetab .mm-dropdown-container ul {
            min-height: 150px !important;
            max-height: 200px !important;
            overflow-x: hidden !important;
        }
        .financerchoicetab .btn-group-fc {
            margin: 0px !important;
            padding: 0 10px;
        }
        .financerchoicetab .btn-group-fc button{
            margin-bottom: 5px;
        }
        .btn-step-content {
            width: 100% !important;
            display: flex;
            justify-content: space-between;
        }
        .sfacc-container h5, .sfdispo-container h5 {
            font-size: 16px !important;
        }
        .sfacc-container h5 {
            margin-top: 20px;
            color: #4623C9;
        }
        .sfdispo-container h5 {
            color: #43C9B7;
        }
        .sfacc-container span, sfdispo-container span {
            font-size: 20px !important;
            font-weight: bold;
        }
        .sfacc-container span i, sfdispo-container span i{
            font-size: 17px !important;
        }

        .labelcofinance .doublefinancecheck {
            transform: scale(1.5);
            appearance: none;
            -webkit-appearance: none;
            border-radius: 3px;
            border: 1px solid #000;
            position: relative;
            width: 25px !important;
            height: 15px !important;
            padding: 1px;
            background-color: #fff;
        }

        .labelcofinance .doublefinancecheck:checked::after {
            content: '';
            background-color: #4623C9;
            font-size: 16px;
            position: absolute;
            top: 51%;
            left: 51%;
            transform: translate(-50%, -50%);
            font-weight: 600;
            width: 12px;
            height: 12px;
            /* border-radius: 10px; */
        }
        .recapcheckcont .recapcheck {
            transform: scale(1.5);
            appearance: none;
            -webkit-appearance: none;
            border-radius: 3px;
            border: 1px solid #000;
            position: relative;
            width: 40px !important;
            height: 15px !important;
            background-color: #fff;
            padding: 1px;
        }

        .recapcheckcont .recapcheck:checked::after {
            content: '';
            background-color: #4623C9;
            font-size: 16px;
            position: absolute;
            top: 51%;
            left: 51%;
            transform: translate(-50%, -50%);
            font-weight: 600;
            width: 12px;
            height: 12px;
            /* border-radius: 10px; */
        }
        .swiper-container-android .wrapper .depe-list{
            padding-left: 0 !important;
        }
        .co-popup-custom-question [type="checkbox"]:checked+span:before {
            background: #9BC125 !important;
            box-shadow: 0 0 0 0.25em #666;
        }
        .header-mobile .ap-starter-btn-close2 {
            z-index: 9999999;
        }
        .financerchoiceheader, .financerchoicedesc, .financerchoicecontainer div {
            float: none !important;
        }
        .userinfoco {
            height: auto !important;
        }
        .amountaffect-list.recaplist {
            margin: 10px 0px;
        }
    }

    <?php
	if ($isftl) {
	?>.aap-primary-color {
        color: #43C9B7 !important;
    }

    .btn-aap-primary {
        background-color: #43C9B7 !important;
        border-color: #43C9B7;
    }

    :root {
        --pfpb-primary-purple-color: <?= isset($configForm["campagne"][array_key_first($configForm["campagne"])]["campColor"]) ? $configForm["campagne"][array_key_first($configForm["campagne"])]["campColor"] : "#4623c9" ?>;
    }

    <?php
    }
    ?>

    #s2id_aacselect.select2-container {
        display: inline-block;
        text-align: left;
        width: 250px !important;
        margin-bottom: 5px;
        margin-right: 20px;
    }
    #s2id_aacselect.select2-container .select2-choice {
        background-color: #6200ea; /* Couleur principale Material */
        border: none;
        border-radius: 5px; /* Coins arrondis */
        padding: 10px 20px;
        font-size: 16px;
        cursor: pointer;
        transition: background-color 0.3s ease;
        height: auto;
    }
    #s2id_aacselect.select2-container .select2-choice:hover {
        background-color: #3700b3; /* Couleur au survol */
    }
    #s2id_aacselect.select2-container .select2-arrow {
        background-color: transparent; /* Enlever le fond de la flèche */
        border: none;
    }
    /* Style pour la liste déroulante */
    #s2id_aacselect.select2-container .select2-results {
        border: 1px solid #ddd;
        border-radius: 5px;
        box-shadow: 0px 2px 6px rgba(0, 0, 0, 0.2);
    }
    #s2id_aacselect.select2-container .select2-results li {
        padding: 8px 12px;
        font-size: 16px;
    }
    #s2id_aacselect.select2-container .select2-results li.select2-highlighted {
        background-color: #bb86fc; /* Couleur de surbrillance */
        color: black;
    }
</style>

<div class="co-popup-cofinance-loader-section">
    <!-- <div class="co-popup-cofinance-loader"></div> -->
</div>

<div class="custom-financer-bg <?= $isftl ? 'cofinanceftl hide' : '' ?>">
    <div class="co-popup-chooseline-container" style="display: none;">
        <div class="co-popup-chooseline">
            <div class="co-popup-chooseline-header margin-20">
                <h2 class="co-popup-chooseline-title"> Proposition </h2>
                <button class="pull-right inline close-co-popup-chooseline-header"><i class="fa fa-times"></i></button>
            </div>
            <div class="co-popup-chooseline-search margin-20">
                <div id="custom-search-input">
                    <div class="input-group col-md-12">
                        <input type="text" class="  search-query form-control" placeholder="Rechercher" />
                        <span class="input-group-btn">
							<span class=" glyphicon glyphicon-search"></span>
						</span>
                    </div>
                </div>
            </div>
            <div class="co-popup-chooseline-list ">

            </div>
            <div class="co-popup-chooseline-footer margin-20">
                <button class="pull-right inline close-co-popup-chooseline-header"> Fermer </button>
            </div>
        </div>
    </div>
    <div class="co-popup-custom-container <?= !empty($isInsideForm) && !filter_var($isInsideForm, FILTER_VALIDATE_BOOLEAN) ? 'cofinancemodal' : '' ?>">
        <input type="checkbox" class="checkbox-custom-start2 hide">
        <div class="co-popup-custom">
            <input type="checkbox" class="checkbox-custom-start hide">
            <div class="co-popup-custom-content">
                <div class="co-popup-custom-header">
                    <div class="co-popup-custom-header-logo">
                    </div>
                    <h4 class="co-popup-custom-header-h2 co-popup-custom-title financercustomtext1">Bienvenue</h4>
                    <div class="co-popup-custom-header-h4 co-popup-custom-subtitle financercustomtext2"> Nous sommes ravis de vous présenter notre passionnant projet </div>
                    <h3 class="co-popup-custom-header-h2 co-popup-custom-title aap-primary-color titre">(vide)</h3>

                    <div class="co-popup-custom-header-h4 co-popup-custom-subtitle aap-secondary-color description">(vide)</div>

                    <div class="row margin-top-20 statboard">
                        <div class="col-md-4 col-sm-12 ">
                            <h3 class="co-popup-custom-header-h2 co-popup-custom-title "><i class="fa fa-star aap-primary-color"></i><span id="nbFin" class="financementnbr">(vide)</h3>
                            <div class="co-popup-custom-header-h4 co-popup-custom-subtitle aap-secondary-color">Cofinanceur(s)</div>
                        </div>
                        <div class="col-md-4 col-sm-12 ">
                            <h3 class="co-popup-custom-header-h2 co-popup-custom-title"><i class="fa fa-euro aap-primary-color"></i><span id="totalEarn" class="aap-primary-color amountearn">(vide)</span></h3>
                            <div class="co-popup-custom-header-h4 co-popup-custom-subtitle aap-secondary-color">Récolté</div>
                        </div>
                        <div class="col-md-4 col-sm-12 ">
                            <h3 class="co-popup-custom-header-h2 co-popup-custom-title"><i class="fa fa-euro"></i><span id="total" class="total">(vide)</span></h3>
                            <div class="co-popup-custom-header-h4 co-popup-custom-subtitle aap-secondary-color">Objectif</div>
                        </div>
                    </div>

                    <div class="progress margin-top-20">
                        <div class="progress-bar btn-aap-primary" role="progressbar" aria-valuenow="  45" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                            <span class="skill-name"><strong></strong></span>
                        </div>
                    </div>

                    <p class="co-popup-custom-header-p financercustomtext3"> Nous sommes à <span class="custom-fin-percent finalpercent">(vide)</span><span class="custom-fin-percent">%</span>de notre objectif. Ensemble, nous pouvons transformer cette proposition en projet et le projet en réalité. Nous avons besoin de votre soutien, rejoignez-nous en participant à notre campagne de financement participatif. </p>
                </div>
                <div class="co-popup-custom-header-action">
                    <ul class="co-popup-custom-actions">
                        <?php if ($isftl) {
                            if ($canSeeAnswer) {
                                if (isset($parentForm["aapType"]) && $parentForm["aapType"] == "aac") {
                                    if(isset($answer["answers"]["aapStep2"]['choose'])){
                                    ?>
                                    
                                <?php }
                                }
                                ?>
                                <li class="li-start-custom"><a href="javascript:;" class="ap-starter-btn-start-custom"><span><i class="fa fa-flag-checkered" aria-hidden="true"></i></span>Cofinancer</a></li>
                            <?php } else { ?>
                                <li class="li-abord-custom"><a href="javascript:;" class="ap-starter-btn-connect" data-target="#modalLogin" data-toggle="modal" data-dismiss="modal"><span><i class="fa fa-sign-in" aria-hidden="true"></i></span> <span class="connect-label"> Se connecter </span> </a></li>
                            <?php } ?>
                            <li class="li-abord-cofinance"><a href="javascript:;" class="ap-starter-btn-close"> Annuler </a></li>
                        <?php } elseif (isset($parentForm["crowdfunding"]) && !$parentForm["crowdfunding"]) { ?>
                            Cofinancement actuellement indisponible
                        <?php } elseif ($canSeeAnswer) { ?>
                            <li class="li-start-custom"><a href="javascript:;" class="ap-starter-btn-start-custom"><span><i class="fa fa-flag-checkered" aria-hidden="true"></i></span>Soutenir le projet</a></li>
                            <li class="li-abord-custom">
                                <a href="<?php echo Yii::app()->getRequest()->getBaseUrl() . 'costum/co/index/slug/' . $context["slug"] . '#coform.context.' . $contextConfig["slug"] . '.formid.' . $parentForm["_id"] . '.answerId.' . $answer["_id"]; ?>" class="ap-starter-btn-sheet"><span><i class="fa fa-file-text " aria-hidden="true"></i></span> Fiche projet </a>
                                <a href="javascript:;" class="ap-starter-btn-close ap-starter-btn-changeProject"><span><i class="fa fa-refresh" style="padding-left: 10px;" aria-hidden="true"></i></span> <span class="labelspan"> Soutenir un autre projet </span></a>
                            </li>
                        <?php } else { ?>
                            <li class="li-start-custom">
                                <a href="javascript:;" class="ap-starter-btn-connect" data-target="#modalLogin" data-toggle="modal" data-dismiss="modal"><span><i class="fa fa-sign-in" aria-hidden="true"></i></span> <span class="connect-label"> Se connecter </span> </a>
                                <input class="inputemail form-control" id="name" type="text" />
                            </li>
                            <?php if (isset($parentForm["temporarymembercanreply"]) && $parentForm["temporarymembercanreply"]) { ?>
                                <li class="li-abord-custom"><a href="javascript:;" class="ap-starter-btn-email" data-type="toogle"> Se connecter avec un email </a></li>
                                <?php
                            }
                            ?>
                            <li class="li-abord-custom">
                                <a href="<?php echo Yii::app()->getRequest()->getBaseUrl() . 'costum/co/index/slug/' . $context["slug"] . '#coform.context.' . $contextConfig["slug"] . '.formid.' . $parentForm["_id"] . '.answerId.' . $answer["_id"]; ?>" class="ap-starter-btn-sheet"><span><i class="fa fa-file-text " aria-hidden="true"></i></span> Fiche projet </a>
                                <a href="javascript:;" class="ap-starter-btn-close ap-starter-btn-changeProject"><span><i class="fa fa-refresh" style="padding-left: 10px;" aria-hidden="true"></i></span> <span class="labelspan"> Soutenir un autre projet </span></a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>

                <div class="caption-container">
                    <div class="caption-title"></div>
                    <div class="caption"></div>
                </div>
            </div>
            <div class="co-popup-custom-question" style="display: none;">
                <div class="co-popup-custom-question-container">
                    <div class="col-md-4 co-popup-custom-menu">
                        <div class="co-popup-custom-projectresume padding-15">
                            <div class="title-info"><?php echo $context["name"] ?></div>
                            <div class="carddesc">
                                <div class="carddesc-tlname center">

                                </div>
                                <div class="">
                                    <button class="btn btn-aap-primary margin-20 ap-starter-btn-dashboard" style="border: 1px solid #FFF; color: #FFF !important;"><i class="fa fa-arrow-left"></i> retour </button>
                                </div>
                                <div class="title-cardpercent">
                                    <span id="title-percent" class="title-percent actpercent">(vide)</span>
                                    <span class="title-percent-icon"><i class="fa fa-percent"></i> </span>
                                    <div class="title-reste margin-bottom-5">Récolté : <span class="amountearn bold"> (vide) </span><span><i class="fa fa-euro"></i> </span> </div>
                                    <div class="title-reste">Reste à completer : <span class="actreste bold"> (vide) </span><span><i class="fa fa-euro"></i> </span> </div>
                                </div>

                                <div class="bottomdesc">
                                    <div class="title-projectname titre">(vide)</div>
                                    <div class="title-projectdesc description">(vide)</div>
                                </div>
                            </div>
                        </div>
                        <div class="co-popup-custom-financerlist padding-15">
                            <div class="">
                                <div class="co-popup-custom-header-h4 co-popup-custom-subtitle aap-secondary-color">
                                    Déposé par
                                </div>
                                <div class="custom-footer-container">
                                    <div class="custom-fin-creator">
                                        <a class='lbh-preview-element' href='#page.type.citoyens.id.<?= (string)$userData["_id"] ?>'>
                                            <img src="<?php echo (isset($userData["profilImageUrl"]) ? $userData["profilImageUrl"] : Yii::app()->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl() . "/images/thumbnail-organizations.png") ?>" alt="<?php echo (isset($userData["name"]) ? $userData["name"] : "") ?>" class="custom-fin-img">
                                            <div class="bold margin-top-5"><?php echo (isset($userData["name"]) ? $userData["name"] : "") ?></div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 padding-10 co-popup-custom-projectswiper">
                        <div class="swiper cofinancestandalone" id="swiper">
                            <div class="swiper-wrapper">

                            </div>
                        </div>
                        <div class="co-popup-custom-financerlist padding-15">
                            <div class="col-md-6">
                                <div class="co-popup-custom-header-h4 co-popup-custom-subtitle aap-secondary-color">
                                    Financeur(s)
                                </div>
                                <div class="custom-footer-container fin-container tooltips" data-placement="left" data-original-title="">


                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="co-popup-custom-header-h4 co-popup-custom-subtitle aap-secondary-color">
                                    Porteur(s)
                                </div>
                                <div class="custom-footer-container porter-container tooltips" data-placement="left" data-original-title="">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="co-popup-custom-finish" style="display: none; overflow: hidden;">
                <div class="co-popup-custom-header">
                    <div class="co-popup-custom-header-logo">
                    </div>
                    <h4 class="co-popup-custom-header-h2 co-popup-custom-title financercustomtext4" style="letter-spacing: 10px;">Merci</h4>
                    <h4 class="co-popup-custom-header-h2 co-popup-custom-title"><i class="fa fa-heart aap-primary-color" style="font-size: 40px"></i> </h4>
                    <div class="co-popup-custom-header-h4 co-popup-custom-subtitle">Grâce à votre contribution de <span class="aap-primary-color finaluseramount"></span> <i class="fa fa-euro"></i> , vous avez completé <span class="aap-primary-color finaluserpercent"></span> du montant total </div>

                    <div class="row margin-top-20">
                        <div class="col-md-6 col-sm-12 ">
                            <h3 class="co-popup-custom-header-h2 co-popup-custom-title"><i class="fa fa-euro aap-primary-color"></i><span id="totalEarnfinal" class="aap-primary-color finalamountearn"> </span></h3>
                            <div class="co-popup-custom-header-h4 co-popup-custom-subtitle aap-secondary-color">Obtenu(s)</div>
                        </div>
                        <div class="col-md-6 col-sm-12 ">
                            <h3 class="co-popup-custom-header-h2 co-popup-custom-title"><i class="fa fa-euro"></i><span id="total" class="total">vide</span></h3>
                            <div class="co-popup-custom-header-h4 co-popup-custom-subtitle aap-secondary-color">Total</div>
                        </div>
                    </div>

                    <div class="progress margin-top-20">
                        <div class="progress-bar btn-aap-primary" role="progressbar" aria-valuenow="  45" aria-valuemin="0" aria-valuemax="100" style="width: 0>%">
                            <span class="skill-name"><strong></strong></span>
                        </div>
                    </div>
                    <p class="co-popup-custom-header-p"> Nous sommes passé du <span id="totalEarnfinal" class="aap-primary-color amountearn"> </span> <span class="aap-primary-color"><i class="fa fa-euro"></i></span> à <span id="totalEarnfinal" class="aap-primary-color finalamountearn"> </span><span class="aap-primary-color"><i class="fa fa-euro"></i></span> . <br> <span class="financercustomtext5"> Votre acte de générosité ne passe pas inaperçu, chaque don compte, et le vôtre nous rapproche un peu plus de notre objectif. </span> </p>
                </div>
                <div class="co-popup-custom-header-action">
                    <ul class="co-popup-custom-actions">
                        <?php //if (empty($isInsideForm) || filter_var($isInsideForm, FILTER_VALIDATE_BOOLEAN)){
                        if ($isftl) { ?>
                            <!--<li class="li-start-custom"><a href="javascript:;" class="ap-starter-btn-cart"><span><i class="fa fa-money" aria-hidden="true"></i></span>Payer ce commun</a></li>-->
                            <li class="li-start-custom" style="margin-left: 20px"><a href="javascript:;" class="ap-starter-btn-min-cart"><span><i class="fa fa-shopping-cart" aria-hidden="true"></i></span>Ouvrir le panier</a></li>
                        <?php } else { ?>
                            <li class="li-start-custom"><a href="javascript:;" class="ap-starter-btn-dashboard"><span><i class="fa fa-flag-checkered" aria-hidden="true"></i></span>Revenir vers l'ecran d'accueil</a></li>
                        <?php }
                        //}
                        ?>
                        <li class="li-abord-custom">
                            <?php if ($isftl) { ?>
                            <a href="javascript:;" class="ap-starter-btn-close"><span></span>Fermer</a>
                        </li>
                    <?php } else { ?>
                        <a href="<?php echo Yii::app()->getRequest()->getBaseUrl() . 'costum/co/index/slug/' . $context["slug"] . '#coform.context.' . $contextConfig["slug"] . '.formid.' . $parentForm["_id"] . '.answerId.' . $answer["_id"]; ?>" class="ap-starter-btn-sheet"><span><i class="fa fa-file-text " aria-hidden="true"></i></span> Fiche projet </a>
                        <a href="javascript:;" class="ap-starter-btn-close ap-starter-btn-changeProject"><span><i class="fa fa-refresh" style="padding-left: 10px;" aria-hidden="true"></i></span> <span class="labelspan"> Soutenir un autre projet </span></a>
                    <?php } ?>
                        </li>
                    </ul>
                </div>


                <div class="caption-container">
                    <div class="caption-title"></div>
                    <div class="caption"></div>
                </div>
            </div>
            <div class="co-popup-custom-cart" style="display: none; overflow: hidden;">
                <div class="co-popup-custom-header">
                    <h5 class="co-popup-custom-header-h2 co-popup-custom-title financercustomtext4">Contribution(s) en attente pour <span class="ftlname"></span> </h5>

                    <div class="row margin-top-20 ftlfinwaiting">
                        <div class="co-popup-cofinance-loader-section">
                            <!-- <div class="co-popup-cofinance-loader"></div> -->
                        </div>
                    </div>
                </div>
                <div class="co-popup-custom-header-action">
                    <ul class="co-popup-custom-actions">
                        <!--<li class="li-start-custom"><a href="javascript:;" class="ap-starter-btn-cart"><span><i class="fa fa-money" aria-hidden="true"></i></span>Payer ce commun</a></li>-->
                        <li class="li-start-custom" style="margin-left: 20px"><a href="javascript:;" class="ap-starter-btn-min-cart"><span><i class="fa fa-shopping-cart" aria-hidden="true"></i></span>Ouvrir le panier</a></li>
                        <li class="li-abord-custom">
                            <a href="javascript:;" class="ap-starter-btn-close"><span></span>Fermer</a>
                        </li>
                        </li>
                    </ul>
                </div>


                <div class="caption-container">
                    <div class="caption-title"></div>
                    <div class="caption"></div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="custom-financer-mobile-bg <?= $isftl ? 'cofinancemobileftl hide' : '' ?>">
    <div class="co-popup-custom-content">
        <div class="co-popup-custom-header">
            <div class="co-popup-custom-header-logo">
            </div>
            <h4 class="co-popup-custom-header-h2 co-popup-custom-title financercustomtext1 text1-mobile">Bienvenue</h4>
            <div class="co-popup-custom-header-h4 co-popup-custom-subtitle financercustomtext2 text2-mobile"> Nous sommes ravis de vous présenter notre passionnant projet </div>
            <div class="titre-desc-mobile">
                <h3 class="co-popup-custom-header-h2 co-popup-custom-title aap-primary-color titre">(vide)</h3>
                <div class="co-popup-custom-header-h4 co-popup-custom-subtitle aap-secondary-color description">(vide)</div>
            </div>
            <div class=" margin-top-20 statboard">
                <div class="statcol">
                    <h3 class="co-popup-custom-header-h2 co-popup-custom-title "><i class="fa fa-star aap-primary-color"></i><span id="nbFin" class="financementnbr">(vide)</h3>
                    <div class="co-popup-custom-header-h4 co-popup-custom-subtitle aap-secondary-color">Cofinanceur(s)</div>
                </div>
                <div class="statcol">
                    <h3 class="co-popup-custom-header-h2 co-popup-custom-title"><i class="fa fa-euro aap-primary-color"></i><span id="totalEarn" class="aap-primary-color amountearn">(vide)</span></h3>
                    <div class="co-popup-custom-header-h4 co-popup-custom-subtitle aap-secondary-color">Obtenu(s)</div>
                </div>
                <div class="statcol">
                    <h3 class="co-popup-custom-header-h2 co-popup-custom-title"><i class="fa fa-euro"></i><span id="total" class="total">(vide)</span></h3>
                    <div class="co-popup-custom-header-h4 co-popup-custom-subtitle aap-secondary-color">Total</div>
                </div>
            </div>

            <div class="progress margin-top-20">
                <div class="progress-bar btn-aap-primary" role="progressbar" aria-valuenow="  45" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                    <span class="skill-name"><strong></strong></span>
                </div>
            </div>

        </div>
        <div class="co-popup-custom-header-action">
            <ul class="co-popup-custom-actions">
                <?php if ($isftl) {
                    if ($canSeeAnswer) { ?>
                        <li class="li-start-custom"><a href="javascript:;" class="ap-starter-btn-start-custom">Cofinancer</a></li>
                    <?php } else { ?>
                        <li class="li-abord-custom"><a href="javascript:;" class="ap-starter-btn-connect" data-target="#modalLogin" data-toggle="modal" data-dismiss="modal"><span><i class="fa fa-sign-in" aria-hidden="true"></i></span> <span class="connect-label"> Se connecter </span> </a></li>
                    <?php } ?>
                    <li class="li-abord-cofinance"><a href="javascript:;" class="ap-starter-btn-close"> Annuler </a></li>
                <?php } elseif (isset($parentForm["crowdfunding"]) && !$parentForm["crowdfunding"]) { ?>
                    Cofinancement actuellement indisponible
                <?php } elseif ($canSeeAnswer) { ?>
                    <li class="li-start-custom"><a href="javascript:;" class="ap-starter-btn-start-custom"><span><i class="fa fa-flag-checkered" aria-hidden="true"></i></span>Soutenir le projet <span class="pull-right"> > </span> </a> </li>
                    <li class="li-abord-custom">
                        <a href="<?php echo Yii::app()->getRequest()->getBaseUrl() . 'costum/co/index/slug/' . $context["slug"] . '#coform.context.' . $contextConfig["slug"] . '.formid.' . $parentForm["_id"] . '.answerId.' . $answer["_id"]; ?>" class="ap-starter-btn-sheet"><span><i class="fa fa-file-text " aria-hidden="true"></i></span> Fiche projet <span class="pull-right"> > </span> </a>
                    </li>
                    <li class="li-abord-custom">
                        <a href="javascript:;" class="ap-starter-btn-close ap-starter-btn-changeProject"><span><i class="fa fa-refresh" style="padding-left: 10px;" aria-hidden="true"></i></span> <span class="labelspan"> Soutenir un autre projet </span> <span class="pull-right"> > </span> </a>
                    </li>
                <?php } else { ?>
                    <li class="li-start-custom">
                        <a href="javascript:;" class="ap-starter-btn-connect" data-target="#modalLogin" data-toggle="modal" data-dismiss="modal"><span><i class="fa fa-sign-in" aria-hidden="true"></i></span> <span class="connect-label"> Se connecter </span> </a>
                        <input class="inputemail form-control" id="name" type="text" />
                    </li>
                    <?php if (isset($parentForm["temporarymembercanreply"]) && $parentForm["temporarymembercanreply"]) { ?>
                        <li class="li-abord-custom"><a href="javascript:;" class="ap-starter-btn-email" data-type="toogle"> Se connecter avec un email </a></li>
                        <?php
                    }
                    ?>
                    <li class="li-abord-custom">
                        <a href="<?php echo Yii::app()->getRequest()->getBaseUrl() . 'costum/co/index/slug/' . $context["slug"] . '#coform.context.' . $contextConfig["slug"] . '.formid.' . $parentForm["_id"] . '.answerId.' . $answer["_id"]; ?>" class="ap-starter-btn-sheet"><span><i class="fa fa-file-text " aria-hidden="true"></i></span> Fiche projet </a>
                    </li>
                    <li class="li-abord-custom">
                        <a href="javascript:;" class="ap-starter-btn-close ap-starter-btn-changeProject"><span><i class="fa fa-refresh" style="padding-left: 10px;" aria-hidden="true"></i></span> <span class="labelspan"> Soutenir un autre projet </span></a>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </div>

        <div class="caption-container">
            <div class="caption-title"></div>
            <div class="caption"></div>
        </div>
    </div>
    <div class="co-popup-custom-question" style="display: none">

        <div class="header-mobile">
            <button class="margin-20 ap-starter-btn-close2 btn"><i class="fa fa-times"></i></button>
            <div class="retour">
                <button class="margin-20 ap-starter-btn-dashboard back-to-dashboard-btn"><i class="fa fa-arrow-left pull-left"></i> <span> retour </span> </button>
                <div class="profilPicDropdown">
                </div>
            </div>
            <div class="mobile-presentation-info">
                <input type="checkbox" id="toogle-mobile-p-i" name="toogle-mobile-p-i" class="hide toogle-mobile-p-i">
                <label for="toogle-mobile-p-i" class="label-toogle-mobile-p-i"></label>
                <div class="title-projectname titre">(vide)</div>
                <div class="title-projectdesc description">(vide)</div>
                <div class="title-reste">Reste à completer : <span class="actreste bold"> (vide) </span><span><i class="fa fa-euro"></i> </span> </div>
            </div>
        </div>
        <div class="swiper cofinancestandalone" id="swiper">
            <div class="swiper-wrapper">

            </div>
        </div>
        <div class="custom-financer-header-mobile">
            <div class="chfm-indicator">
                <span class="cf-header-btn active"></span>
                <span class="cf-header-btn"></span>
                <span class="cf-header-btn"></span>
            </div>
            <div class="chfm-slider">
                <div class="chfm-slide-row" id="chfm-slide-row">
                    <div class="chfm-slide-col">
                        <div class="co-popup-custom-header-h4 co-popup-custom-subtitle aap-secondary-color">
                            Déposé par
                        </div>
                        <div class="custom-footer-container">
                            <div class="custom-fin-creator">
                                <a class='lbh-preview-element' href='#page.type.citoyens.id.<?= (string)$userData["_id"] ?>'>
                                    <img src="<?php echo (isset($userData["profilImageUrl"]) ? $userData["profilImageUrl"] : Yii::app()->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl() . "/images/thumbnail-organizations.png") ?>" alt="<?php echo (isset($userData["name"]) ? $userData["name"] : "") ?>" class="custom-fin-img">
                                    <div class="bold margin-top-5"><?php echo (isset($userData["name"]) ? $userData["name"] : "") ?></div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="chfm-slide-col">
                        <div class="co-popup-custom-header-h4 co-popup-custom-subtitle aap-secondary-color">
                            Financeur(s)
                        </div>
                        <div class="custom-footer-container fin-container">


                        </div>
                    </div>

                    <div class="chfm-slide-col">
                        <div class="co-popup-custom-header-h4 co-popup-custom-subtitle aap-secondary-color">
                            Porteur(s)
                        </div>
                        <div class="custom-footer-container porter-container">

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="co-popup-custom-finish" style="display: none;">
        <div class="custom-financer-header-mobile-finish">
            <h4 class="co-popup-custom-header-h2 co-popup-custom-title financercustomtext4" style="letter-spacing: 10px;">Merci</h4>
            <h4 class="co-popup-custom-header-h2 co-popup-custom-title"><i class="fa fa-heart aap-primary-color" style="font-size: 40px"></i> </h4>
            <div class="co-popup-custom-header-h4 co-popup-custom-subtitle">Grâce à votre contribution de <span class="aap-primary-color finaluseramount"></span>, vous avez completé <span class="aap-primary-color finaluserpercent"></span> du montant total </div>

            <div class="statboard">
                <div class="statcolfinish">
                    <h3 class="co-popup-custom-header-h2 co-popup-custom-title"><i class="fa fa-euro aap-primary-color"></i><span id="totalEarnfinal" class="aap-primary-color finalamountearn"> </span></h3>
                    <div class="co-popup-custom-header-h4 co-popup-custom-subtitle aap-secondary-color">Obtenu(s)</div>
                </div>
                <div class="statcolfinish">
                    <h3 class="co-popup-custom-header-h2 co-popup-custom-title"><i class="fa fa-euro"></i><span id="total" class="total">vide</span></h3>
                    <div class="co-popup-custom-header-h4 co-popup-custom-subtitle aap-secondary-color">Total</div>
                </div>
            </div>

            <div class="progress margin-top-20">
                <div class="progress-bar btn-aap-primary" role="progressbar" aria-valuenow="  45" aria-valuemin="0" aria-valuemax="100" style="width: 0>%">
                    <span class="skill-name"><strong></strong></span>
                </div>
            </div>
            <p class="co-popup-custom-header-p"> Nous sommes passé du <span id="totalEarnfinal" class="aap-primary-color amountearn"> </span> <span class="aap-primary-color"><i class="fa fa-euro"></i></span> à <span id="totalEarnfinal" class="aap-primary-color finalamountearn"> </span><span class="aap-primary-color"><i class="fa fa-euro"></i></span> . <br> <span class="financercustomtext5"> Votre acte de générosité ne passe pas inaperçu, chaque don compte, et le vôtre nous rapproche un peu plus de notre objectif. </span> </p>

        </div>
        <div class="co-popup-custom-header-action">
            <ul class="co-popup-custom-actions">
                <?php //if (empty($isInsideForm) || filter_var($isInsideForm, FILTER_VALIDATE_BOOLEAN)){
                if ($isftl) { ?>
                    <!--<li class="li-start-custom"><a href="javascript:;" class="ap-starter-btn-cart"><span><i class="fa fa-money" aria-hidden="true"></i></span>Payer ce commun</a></li>-->
                    <li class="li-start-custom"><a href="javascript:;" class="ap-starter-btn-min-cart"><span><i class="fa fa-shopping-cart" aria-hidden="true"></i></span>Ouvrir le panier</a></li>
                <?php } else { ?>
                    <li class="li-start-custom"><a href="javascript:;" class="ap-starter-btn-dashboard"><span><i class="fa fa-flag-checkered" aria-hidden="true"></i></span>Revenir vers l'ecran d'accueil</a></li>
                <?php }
                //}
                ?>
                <li class="li-abord-custom">
                    <?php if ($isftl) { ?>
                    <a href="javascript:;" class="ap-starter-btn-close"><span></span>Fermer</a>
                </li>
            <?php } else { ?>
                <a href="<?php echo Yii::app()->getRequest()->getBaseUrl() . 'costum/co/index/slug/' . $context["slug"] . '#coform.context.' . $contextConfig["slug"] . '.formid.' . $parentForm["_id"] . '.answerId.' . $answer["_id"]; ?>" class="ap-starter-btn-sheet"><span><i class="fa fa-file-text " aria-hidden="true"></i></span> Fiche projet </a>
                <a href="javascript:;" class="ap-starter-btn-close ap-starter-btn-changeProject"><span><i class="fa fa-refresh" style="padding-left: 10px;" aria-hidden="true"></i></span> <span class="labelspan"> Soutenir un autre projet </span></a>
            <?php } ?>
                </li>
            </ul>
        </div>

        <div class="caption-container">
            <div class="caption-title"></div>
            <div class="caption"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    let swiper;
    var depenseSubType = "<?php echo (!empty($input["subType"]) ? $input["subType"] : ""); ?>";
    var isftlCommun = "<?php echo (!empty($isftlCommun) ? $isftlCommun : ""); ?>";
    var depenseFinancer = <?php echo (!empty($depenseFinancer) ? json_encode($depenseFinancer) : json_encode([])); ?>;
    var depenseFinancerOrga = <?php echo (!empty($depenseFinancerOrga) ? json_encode($depenseFinancerOrga) : json_encode([])); ?>;
    var coForm_FormWizardParams = <?php echo (!empty($params) ? json_encode($params) : json_encode([])); ?>;
    var totalfinancement = <?php echo (!empty($totalfinancement) ? json_encode($totalfinancement) : json_encode([])); ?>;
    var aacParents = <?php echo (!empty($answer["answers"]["aapStep2"]['choose']) ? json_encode($answer["answers"]["aapStep2"]['choose']) : json_encode([])); ?>;
    if (typeof coForm_FormWizardParams == "undefined") {
        var coForm_FormWizardParams = <?php echo json_encode($params); ?>;
        aapObj.form = coForm_FormWizardParams.parentForm;
        aapObj.pageDetail = {
            "#proposalAap": {
                "hash": "#app.aap",
                "urlExtra": "/page/proposal",
                "subdomainName": "Tout",
                "icon": "trello menu-detail-proposal btn btn-aap-primary pull-right"
            }
        }
    }

    var customFinObj = {
        initialized: false,
        data: {
            lineToPay: {

            },
            lineToAdd: {

            },
            currentOrga: {
                name: '',
                id: ''
            },
            financertype : "tl",
            currentAacParent : {
                id : '',
                name : '',
            },
            currentUser: {
                name: userConnected != null && typeof userConnected["name"] != "undefined" ? userConnected["name"] : "",
                email: userConnected != null && typeof userConnected["email"] != "undefined" ? userConnected["email"] : "",
                profilPicture: userConnected != null && typeof userConnected["profilThumbImageUrl"] != "undefined" ? userConnected["profilThumbImageUrl"] : defaultImage,
                havChangeEmail: false,
                id: userConnected != null && typeof userConnected["_id"]["$id"] != "undefined" ? userConnected["_id"]["$id"] : "",
            },
            final: {
                cart : {},
                userpercent: 0,
                useramount: 0,
                double: false,
                ftlamount : 0,
                tlamount : 0
            }
        },
        views: {
            createNavButton: function(actualstep, nextstep, number, isObl = false, radioname = '', prevstep = '') {
                var htmlNv = "";
                var isDisabled = "";
                var label = "";
                var evtclass = "preconfig-btn-next";
                var prevstepHtml = "";
                var nextlabel = `Suivant`;
                var skipHtml = "";

                if (nextstep == '') {
                    evtclass = "ap-starter-btn-finish";
                    label = `Envoyer <span class="fa fa-check"></span>`;
                }

                if (prevstep != '') {
                    prevstepHtml = `<button class="btn btn-aap-secondary ap-starter-btn-prev"  data-prevstep="${ prevstep }" data-actualstep="${ actualstep }" >Précédent</button>`;
                }

                if (isObl) {
                    isDisabled = 'disabled';
                }

                if (actualstep == "step7") {
                    nextlabel = 'Je cofinance';
                }

                label = `${nextlabel} <span class="fa fa-long-arrow-right"></span>`;
                if (actualstep == "step7") {
                    label = `Je cofinance <span class="fa fa-money"></span>`;
                } else {
                    label = `Suivant <span class="fa fa-long-arrow-right"></span>`;
                }
                if (actualstep == "selectamount") {
                    skipHtml = `<button class="btn btn-aap-primary ${ evtclass }" data-radioname="${ radioname }" data-nextstep="recap" data-actualstep="${ actualstep }" data-next="${ number }" ${ isDisabled }> Suivant</button>`;
                }
                if (actualstep != "selectfin") {
                    return `<div class="btn-step-content">
                            ${prevstepHtml}
							<button class="btn btn-aap-primary ${evtclass}" data-radioname="${radioname}" data-nextstep="${nextstep}" data-actualstep="${actualstep}" data-next="${number}" ${isDisabled}>${label}</button>
                        </div>`;
                } else {
                    var selectfinext = "selectAction";
                    if (Object.values(customFinObj.data.lineToPay).length != 0) {
                        selectfinext = "selectamount";
                    }
                    return `<div class="btn-step-content">
                            ${prevstepHtml}
							<button class="btn btn-aap-primary ${evtclass}" data-radioname="${radioname}" data-nextstep="${selectfinext}" data-actualstep="${actualstep}" data-next="${number}" > Suivant </button>
                        </div>`;
                }
            },
            createLegend: function(number, legend, label) {
                var html = '';
                html += `<legend> ${ legend } </legend>`;
                if (label != null) {
                    html += `<div class="">
				            <label class="padding-5">${ label }</label>
				         </div>`
                }
                return html;
            },
            createInfo: function(label, tooltips) {
                if (label != "") {
                    return `  <div class="">
					          <label class="padding-20"> <i class="fa fa-exclamation-triangle"></i> ${label} </label>
					     </div>`;
                } else {
                    return '';
                }
            },
            createRadiobutton: function(actualstep, name, lists, path) {
                var renderhtml = '';
                renderhtml += `<div class="wrapper wrapperlist">`;
                $.each(lists, function(id, list) {
                    var pcTooltip = '';
                    if (typeof list.tooltip != "undefined") {
                        var pcTooltipimg = '';
                        if (typeof list.tooltipimg != "undefined") {
                            pcTooltipimg = `<img src="${ baseUrl + list.tooltipimg }" />`;
                        }
                        pcTooltip = `<span class="fa fa-info-circle ml-3 preconfig-info-has-tooltip"> <span class="preconfig-info-tooltip"><p>${ list.tooltip }</p>${ pcTooltipimg }</span> </span>`;
                    }
                    renderhtml += `<div class="radio-item">
						              <label>
						                  <input class="inputpreconfig" type="radio" data-actualstep="${ actualstep }" value="${ list.value }" path="${ path }" name="${ name }" > <span> ${ list.label } </span>
						              </label>
						          </div>`;
                    //list.inputCallback();
                });
                renderhtml += `</div>`;
                return renderhtml;
            },
            createSlide: function(component) {
                return `<div class="swiper-slide" >
							${ component.join(' ') }
					  </div>`;
            },
            createEntete: function() {
                return `<img class="logo-info" src="${ typeof aapObj.context.profilThumbImageUrl != "undefined" ?  baseUrl+aapObj.context.profilThumbImageUrl : baseUrl+defaultImage }">
                        <div class="title-info">${ aapObj.context.name }</div>`;
            },
            profilPicDropdown: function() {
                if (depenseSubType != "") {
                    //if(true){
                    //return $('.depensefinancerdropdown').html();
                    return '';
                } else {
                    return `<div class="btn-group pull-right dropleft">
                            <a href="javascript:;" type="button" class=" pull-right btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src="${baseUrl + customFinObj.data.currentUser.profilPicture}" alt="${customFinObj.data.currentUser.name}" class="custom-fin-img currentUserimg">
                            </a>
                            <div class="dropdown-menu padding-20 border-radius-10">
                                <div class="aap-primary-color center"> nom : </div>
                                <div class="center currentUsername"> ${customFinObj.data.currentUser.name} </div>
                                <div class="aap-primary-color center"> email : </div>
                                <div class="center currentUseremail"> ${customFinObj.data.currentUser.email} </div>
                                <div class="center"> <button class="next btn btn-aap-secondary center border-radius-20 preconfig-btn-next modifyfinbtn margin-top-20" data-nextstep="selectfin" data-actualstep="selectAction" data-next="2"> Modifier </button> </div>
                            </div>
                        </div>`;
                }
            },
            selectAction: function(number) {
                if (depenseSubType != '') {
                    return customFinObj.views.createSlide([
                        customFinObj.views.createLegend(number + 1, `Séléctionner les actions à soutenir ${customFinObj.views.profilPicDropdown()}${customFinObj.views.closeModal()}`, null),
                        customFinObj.views.createLines('selectAction'),
                        customFinObj.views.createNavButton('selectAction', 'recap', number + 1, true, 'radioline', 'selectamount')
                    ]);
                } else {
                    return customFinObj.views.createSlide([
                        customFinObj.views.createLegend(number + 1, `Séléctionner les actions à soutenir ${customFinObj.views.profilPicDropdown()}${customFinObj.views.closeModal()}`, null),
                        customFinObj.views.createLines('selectAction'),
                        customFinObj.views.createNavButton('selectAction', 'recap', number + 1, true, 'radioline', '', true)
                    ]);
                }
            },
            createLines: function(actualstep) {
                var returnHtml = '<div class="row padding-right-20 padding-left-20 slideinnnerheight">';
                if (notEmpty(customFinObj.data.final.arraylines)) {
                    returnHtml += `<div class="row ">
						                <div class="pull-right">
						                    <button class="btn btm-sm selectAllLine margin-15"> tout sélectionner </button>
                                        </div>
						           </div>`;
                    returnHtml += `<div class="wrapper wrapperlist">`;


                    $.each(customFinObj.data.final.arraylines, function(index, data){
                        var valueAmount = 0;
                        if(notEmpty(data.amount)){
                            valueAmount = data.amount;
                        }

                        var ar = typeof data.financer != "undefined" && customFinObj.data.final.arraylines[index].amountearn ? parseInt(customFinObj.data.final.arraylines[index].amountearn) : 0;
                        var iscomplete = data.price == ar;

                        if(!iscomplete) {
                            returnHtml += `<div class="radio-item ${iscomplete ? "disabled" : ""}">
                                              <label for="line${data.depenseindex}" class="${iscomplete ? "disabled" : ""}">
                                                  <input class="inputpreconfig" data-aliases="line${data.depenseindex}" id="line${data.depenseindex}" type="checkbox" ${iscomplete ? "disabled" : ""} data-actualstep="${actualstep}" value="${data.depenseindex}" name="radioline" data-num="${data.depenseindex}"  data-line="${data.poste}"  data-reste="${typeof customFinObj.data.final.arraylines[index].amountearn != "undefined" ? (data.price > customFinObj.data.final.arraylines[index].amountearn ? data.price - customFinObj.data.final.arraylines[index].amountearn : 0) : data.price}">
                                                  <span for="line${index}" class="lnlist ${iscomplete ? "disabled" : ""}"> <h5 style="" class="h5action ${iscomplete ? "grey" : ""}"> ${data.poste} </h5>  <h5 class="pull-right ${iscomplete ? "grey" : ""}"> ${typeof data.financer != "undefined" && notEmpty(customFinObj.data.final.arraylines[index].amountearn) ? customFinObj.actions.addSpace(customFinObj.data.final.arraylines[index].amountearn) : "0"} <i class="fa fa-euro"></i> / ${customFinObj.actions.addSpace(data.price)} <i class="fa fa-euro"></i></h5>  </span>
                                              </label>
                                           </div>`;
                        }
                        //list.inputCallback();
                    });
                    $.each(customFinObj.data.final.arraylines, function(index, data){
                        var valueAmount = 0;
                        if(notEmpty(data.amount)){
                            valueAmount = data.amount;
                        }

                        var ar = typeof data.financer != "undefined" && customFinObj.data.final.arraylines[index].amountearn ? parseInt(customFinObj.data.final.arraylines[index].amountearn) : 0;
                        var iscomplete = data.price == ar;

                        if(iscomplete) {
                            returnHtml += `<div class="radio-item ${iscomplete ? "disabled" : ""}">
                                          <label class="${iscomplete ? "disabled" : ""}">
                                              <input class="inputpreconfig" id="line${data.depenseindex}" type="checkbox" ${iscomplete ? "disabled" : ""} data-actualstep="${actualstep}" value="${data.depenseindex}" name="radioline" data-num="${data.depenseindex}"  data-line="${data.poste}"  data-reste="${typeof customFinObj.data.final.arraylines[index].amountearn != "undefined" ? (data.price > customFinObj.data.final.arraylines[index].amountearn ? data.price - customFinObj.data.final.arraylines[index].amountearn : 0) : data.price}">
                                              <span for="line${index}" class="lnlist ${iscomplete ? "disabled" : ""}"> <h5 style="" class="h5action ${iscomplete ? "grey" : ""}"> ${data.poste} </h5>  <h5 class="pull-right ${iscomplete ? "grey" : ""}"> ${typeof data.financer != "undefined" && notEmpty(customFinObj.data.final.arraylines[index].amountearn) ? customFinObj.actions.addSpace(customFinObj.data.final.arraylines[index].amountearn) : "0"} <i class="fa fa-euro"></i> / ${customFinObj.actions.addSpace(data.price)} <i class="fa fa-euro"></i></h5>  </span>
                                          </label>
                                      </div>`;
                        }
                        //list.inputCallback();
                    });
                    returnHtml += `</div>`;
                } else {
                    returnHtml = `Aucune ligne à afficher`;
                }
                returnHtml += `</div>`;
                return returnHtml;
            },
            selectftl : function(number) {
                return customFinObj.views.createSlide([
                    //customFinObj.views.createLegend(number + 1, `Choisir un tiers lieu  ${ customFinObj.views.profilPicDropdown()}${ customFinObj.views.closeModal()}` , null),
                    customFinObj.views.createLegend(number + 1, `Vous êtes...  ${ customFinObj.views.profilPicDropdown()}${ customFinObj.views.closeModal()}` , null),
                    customFinObj.views.createTLChoice('selectftl'),
                    customFinObj.views.createNavButton('selectftl', 'selectamount', number + 1 , true )
                ]);
            },
            selectamount : function(number) {
                if(depenseSubType != '') {
                    return customFinObj.views.createSlide([
                        customFinObj.views.createLegend(number + 1, `Ajouter un montant  ${customFinObj.views.profilPicDropdown()}${customFinObj.views.closeModal()}`, null),
                        customFinObj.views.createAmountChoice('selectamount'),
                        customFinObj.views.createNavButton('selectamount', 'selectAction', number + 1, true, 'radioamount', 'selectftl')
                    ]);
                }else{
                    return customFinObj.views.createSlide([
                        customFinObj.views.createLegend(number + 1, `Ajouter un montant  ${customFinObj.views.profilPicDropdown()}${customFinObj.views.closeModal()}`, null),
                        customFinObj.views.createAmountChoice('selectamount'),
                        customFinObj.views.createNavButton('selectamount', 'selectAction', number + 1, true, 'radioamount', 'selectftl')
                    ]);
                }
            },
            selectfin : function(number ) {
                return customFinObj.views.createSlide([
                    customFinObj.views.createLegend(number + 1, `Financeur` , `Complétez vos informations pour l'associer à notre projet`),
                    customFinObj.views.createFinChoice('selectfin'),
                    customFinObj.views.createNavButton('selectfin', 'selectAction', number + 1 , true , 'inputnameadress' )
                ]);
            },
            createAmountChoice : function (actualstep) {
                var returnHtml = '';
                returnHtml += `<div class="row padding-right-20 padding-left-20 slideinnnerheight">`;
                //if(depenseSubType == '') {
                returnHtml += `  <div class="col-md-12">
                                        <div class="tab">
                                            <h5 style="text-transform : none"> Choisir </h5>
                                            <div for="budget-3" class="inline">
                                                <input id="budget-1"  class="inputpreconfig checkbox-budget" type="radio" data-actualstep="${actualstep}" data-val='100' value="100" name="radioamount">
                                                <label class="for-checkbox-budget" for="budget-1" >  <span data-hover="100 €">100 €</i> </label>
                                            </div>
                                            <div for="budget-3" class="inline">
                                                <input id="budget-2"  class="inputpreconfig checkbox-budget" type="radio" data-actualstep="${actualstep}" data-val='200' value="200" name="radioamount">
                                                <label class="for-checkbox-budget" for="budget-2">  <span data-hover="200 €">200 €</i> </label>
                                           </div>
                                           <div for="budget-3" class="inline">
                                                <input id="budget-3"  class="inputpreconfig checkbox-budget" type="radio" data-actualstep="${actualstep}" data-val='500' value="500" name="radioamount">
                                                <label class="for-checkbox-budget" for="budget-3">  <span data-hover="500 €">500 €</i> </label>
                                           </div>
                                        </div>
                                    </div>`;
                //}
                returnHtml += `
                                    <div class="col-md-6">
                                        <div class="tab">
                                            <h5 style="text-transform : none">`;

                if (depenseSubType == '') {
                    returnHtml += `Ou`;
                }

                returnHtml += `
                        Ou indiquez un montant</h5>
                                            <p>
                                                <div class="input-group">
                                                    <input autofocus min="0" type="number" class="inputamount" placeholder="Valeur libre" name="freeamount" data-actualstep="${ actualstep }">
                                                <span class="input-group-addon"><i class="fa fa-euro"></i> </span>
                                                </div>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-12 errorsfacc mininputamount hide">Le montant doit être supérieur à 100 <i class="fa fa-euro"></i> pour bénéficier d'une cofinancement France Tiers-lieux </div>
                                    <div class="col-md-12 sfacc-container">
                                        <div class="col-md-6 sfacc-container">
                                            <div class="tab">
                                                <h5 style="text-transform : none"> Cofinancement FTL accordé </h5>
                                                <div class="input-group"> <span class="sfacc">0 <i class="fa fa-euro"></i></span></div>
                                                <!--<h5 style="text-transform : none"> Montant provisoire au payment </h5>
                                                <div class="input-group"> <span class="sfpro">0</span></div>-->
                                            </div>
                                        </div>
                                        <div class="col-md-6 sfdispo-container">
                                            <div class="tab">
                                                <h5 style="text-transform : none"> Cofinancement FTL disponible </h5>
                                                <div class="input-group"> <span class="sfdispo"></span></div>
                                                    <!--<h5 style="text-transform : none"> Dans le panier </h5>
                                                <div class="input-group"> <span class="sfcart"></span></div>-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">`;
                                    if (customFinObj.data.financertype == 'tl') {
                                     returnHtml += `   <div class="labelcofinance margin-top-20 margin-bottom-20">
                                                              <input class="doublefinancecheck" id="doublefinancecheck" type="checkbox" >
                                                             <label for="doublefinancecheck">Je souhaite que ce montant soit doublé par la campagne de cofinancement France Tiers-lieux </label>
                                                        </div>`;
                                    }
                                    returnHtml += `</div>

                                </div>`;
                if (depenseSubType == '') {
                    returnHtml += `<div class="row padding-right-20 padding-left-20 slideinnnerheight">
                                    <div class="col-md-10">
                                        <div class="tab">
                                            <h5 style="text-transform : none"> Afféctation du montant  </h5>
                                            <div class="amountaffect">`;
                }
                if (depenseSubType == '') {
                    $.each(customFinObj.data.lineToPay, function(index, value) {
                        returnHtml += `<div class="amountaffect-list" >
                                                                        <div class="aap-secondary-color inline" style="font-size: 15px; font-weight : bold " >
                                                                            <span class="aap-secondary-dark"> <i class="fa fa-hashtag aap-primary-color"></i> ${value.line} : </span>
                                                                            <span class="inline pull-right"> ${value.amount} <i class="fa fa-euro"></i> </span>
                                                                            <span class="block aap-primary-color"><small> Reste :  ${value.reste} <i class="fa fa-euro"></i> </small> </span>
                                                                        </div>
                                                                    </div>`;
                    });
                }
                if (depenseSubType == '') {
                    returnHtml += `           </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                }



                return returnHtml;
            },
            createFinChoice: function(actualstep) {
                var returnHtml = '';
                if (
                    typeof coForm_FormWizardParams != "undefined" &&
                    typeof coForm_FormWizardParams.answer != "undefined" &&
                    typeof coForm_FormWizardParams.answer.answers != "undefined" &&
                    typeof coForm_FormWizardParams.answer.answers.aapStep1.depense != "undefined"
                ) {
                    returnHtml += `<div class="wrapper">
                                            <label for="name"> Nom et Prénom : </label>
                                            <input class="inputnameadress form-control" id="name" type="text" data-actualstep="${actualstep}" value="${customFinObj.data.currentUser.name}">
                                            <label for="name"> Adresse e-mail : </label>
                                            <input class="inputnameadress form-control" id="email" type="text" data-actualstep="${actualstep}" value="${customFinObj.data.currentUser.email}">

                                    </div> `;
                } else {
                    returnHtml = `Aucune ligne à afficher`;
                }
                return returnHtml;
            },
            recap : function(number ) {
                if(depenseSubType != ''){
                    return customFinObj.views.createSlide([
                        customFinObj.views.createLegend(number + 1, `Aperçu de vos contributions ${ customFinObj.views.closeModal()}` , ''),
                        customFinObj.views.recapTable(),
                        customFinObj.views.recapCheckbox(),
                        customFinObj.views.createNavButton('step7', '', null , true, '' , 'selectAction')
                    ]);
                }else{
                    return customFinObj.views.createSlide([
                        customFinObj.views.createLegend(number + 1, `Sauvegarde ${ customFinObj.views.closeModal()}` , ''),
                        customFinObj.views.recapTable(),
                        customFinObj.views.createNavButton('step7', '', null , false, '' , 'selectAction')
                    ]);
                }

            },
            recapTable: function() {
                var returnHtml = '';
                returnHtml += `<div class="wrapper">`;
                if (depenseSubType != '') {
                    //if(true){
                    returnHtml += `<div class="padding-5">
                        <label for="email"> <span class=""> ${customFinObj.data.financertype == 'tl' ? "Tiers-lieux :" : "Financeur :"} </span> <span class=""><span class="aap-primary-color"> ${customFinObj.data.currentOrga.name} </span> </label>
                    </div>`
                } else {
                    returnHtml += `<div class="padding-5">
                        <label for="name"> <span class=""> Nom et Prénom : </span> <span class="aap-primary-color"> ${customFinObj.data.currentUser.name} </span> </label>
                    </div>
                    <div class="padding-5">
                        <label for="email"> <span class=""> Adresse e-mail : </span> <span class=""><span class="aap-primary-color"> ${customFinObj.data.currentUser.email} </span> </label>
                    </div>`
                }


                returnHtml += `<div class="padding-5">`;
                if (typeof customFinObj.data.final.double != "undefined" && customFinObj.data.final.double == true) {
                    returnHtml += `<label for="name"> <span class=""> Tiers-lieux ${customFinObj.data.currentOrga.name} : </span> <span class="totalToPayTL aap-primary-color"></span><span class=""> <i class="fa fa-euro"></i></span> </label> <br>`;
                    returnHtml += `<label for="name"> <span class=""> Cofinancement France Tiers-lieux : </span> <span class="totalToPayFTL aap-primary-color"></span><span class=""> <i class="fa fa-euro"></i></span> </label> <br>`;
                }
                returnHtml += `
                                            <label for="name"> <span class=""> Total : </span> <span class="totalToPay aap-primary-color"></span><span class=""> <i class="fa fa-euro"></i></span> </label> <br>
                                            <label for=""> <span class=""> Ligne financé: </span> </label>
                                            <ul class="ul depe-list">`;
                $.each(customFinObj.data.lineToPay, function(index, value) {
                    returnHtml += `<div class="amountaffect-list recaplist" >
                                        <div class="aap-secondary-color inline" style="font-size: 15px; font-weight : bold " >
                                            <span class="aap-secondary-dark"> <i class="fa fa-hashtag aap-primary-color"></i> ${value.line} : </span>
                                            <span class="inline pull-right"> ${value.amount} <i class="fa fa-euro"></i> </span>
                                        </div>
                                        ${ customFinObj.views.lineDetailRecap(customFinObj,index,value, index , value )}
                                    </div>`;
                });
                returnHtml += `</ul>
                                    </div>
                               </div>`;
                return returnHtml;
            },
            lineDetailRecap: function(customFinObj , lindex, lvalue) {
                var returnHtml = '';
                if (typeof customFinObj.data.final.double != "undefined" && customFinObj.data.final.double == true) {
                    var tlamount = 0;
                    var ftlamount = 0;

                    tlamount = lvalue.amount - customFinObj.data.lineToAdd[lindex].amount;
                    ftlamount = customFinObj.data.lineToAdd[lindex].amount;
                    
                    returnHtml += `<br><div class="aap-secondary-color inline" style="font-size: 12px; " >
                                        <span class="aap-secondary-dark"> ${customFinObj.data.currentOrga.name}  : </span>
                                        <span class="inline pull-right"> ${tlamount} <i class="fa fa-euro"></i> </span>
                                    </div>
                                    <br><div class="aap-secondary-color inline" style="font-size: 12px; color: var(--pfpb-primary-purple-color) " >
                                        <span class="aap-secondary-dark">  FTL : </span>
                                        <span class="inline pull-right"> ${ftlamount} <i class="fa fa-euro"></i> </span>
                                    </div>`;
                }
                return returnHtml;
            },
            createTLChoice: function(actualstep) {
                var returnHtml = '';
                returnHtml += `<div class="row padding-right-20 padding-left-20 slideinnnerheight">
                                    <div ${ customFinObj.data.financertype != "tl" ? 'style="display: none"' : ''} class="financerchoicetab" data-type="tl">
                                        <div class="financerchoiceheader">
                                            Je représente un tiers-lieu
                                        </div>
                                        <div class="financerchoicedesc">
                                            Veuillez choisir votre tiers-lieu
                                        </div>
                                        <div class="depensefinancerdropdown">
                                            <div class="mm-dropdown-container">
                                                <div class="mm-dropdown pull-right">
                                                    <!--<div class="textfirst"></div>-->
                                                    <div class="inputfirst"><input class="inputfirstsearch" data-type="searchftl" name="inputfirstsearch" placeholder="Filtrer"></div>
                                                    <ul>
                                                        <li class="input-option"  data-type="searchftl" data-value="" data-name="" data-actualstep="selectftl">
                                                            chargement...
                                                        </li>
                                                    </ul>
                                                    <input type="hidden" class="option"  placeholder="Rechercher" name="namesubmit" value="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="btn-group-fc">
                                            ${ customFinObj.views.financerchoicebtn('tl','organization')}
                                            ${ customFinObj.views.financerchoicebtn('tl','person')}
                                        </div>
                                    </div>
                                    <div ${ customFinObj.data.financertype != "organization" ? 'style="display: none"' : ''} class="financerchoicetab" data-type="organization">
                                        <div class="financerchoiceheader">
                                            Je représente une organisation
                                        </div>
                                        <div class="financerchoicedesc">
                                            Veuillez choisir votre organisation
                                        </div>
                                        <div class="financerchoicecontainer">
                                            <div class="depensefinancerdropdown">
                                                <div class="mm-dropdown-container">
                                                    <div class="mm-dropdown pull-right">
                                                        <!--<div class="textfirst"></div>-->
                                                        <div class="inputfirst"><input class="inputfirstsearch" data-type="searchorga" name="inputfirstsearchorga" placeholder="Filtrer"></div>
                                                        <ul>
                                                            <li class="input-option"  data-type="searchorga" data-value="" data-name="" data-actualstep="selectftl">
                                                                chargement...
                                                            </li>
                                                        </ul>
                                                        <input type="hidden" class="option"  placeholder="Rechercher" name="namesubmit" value="" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="btn-group-fc">
                                            ${ customFinObj.views.financerchoicebtn('organization','tl')}
                                            ${ customFinObj.views.financerchoicebtn('organization','person')}
                                        </div>
                                    </div>
                                    <div ${ customFinObj.data.financertype != "person" ? 'style="display: none"' : ''} class="financerchoicetab" data-type="person">
                                        <div class="financerchoiceheader">
                                            Je suis un citoyen
                                        </div>
                                        <div class="financerchoicedesc">
                                            Vos informations
                                        </div>
                                        <div class="financerchoicecontainer">
                                            <div class="userinfoco">
                                                <div>Nom : ${userConnected.name}</div>
                                                <div>Email : ${userConnected.email}</div>
                                                <div><button class="btn confirmperson">Confirmer</button></div>
                                            </div>
                                        </div>
                                        <div class="btn-group-fc">
                                            ${ customFinObj.views.financerchoicebtn('person','tl')}
                                            ${ customFinObj.views.financerchoicebtn('person','organization')}
                                        </div>
                                    </div>
                               </div>`;
                return returnHtml;
            },
            financerchoicebtn: function (actuel, choice){
                var financerchoicebtn = {
                    'tl' : `<button class="btn col-md-6 financerchoicebtn financewithtl" data-actuel="${actuel}" data-choice="tl"> Je représente un tiers-lieux </button>`,
                    'person' : `<button class="btn col-md-6 financerchoicebtn financewithperson" data-actuel="${actuel}" data-choice="person"> Je suis un citoyen </button>`,
                    'organization' : `<button class="btn col-md-6 financerchoicebtn financewithorga" data-actuel="${actuel}" data-choice="organization">Je représente une Organisation </button>`
                }
                return financerchoicebtn[choice];
            },
            recapCheckbox: function(actualstep) {
                var returnHtml = '';
                returnHtml += `<div class="row padding-right-20 padding-left-20 margin-top-20 margin-bottom-20">
                                    <div class="recapcheckcont">
                                          <input class="recapcheck" id="recapcheck" type="checkbox" >
                                         <label for="recapcheck"> En cliquant sur "Je cofinance", je formule une promesse de don à ce commun et je m'engage à payer cette promesse à la fin de la campagne de cofinancement </label>
                                    </div>
                               </div>`;
                return returnHtml;
            },
            closeModal: function() {
                var html = '';
                html += `<button class="btn ap-starter-btn-close2" style="border: 1px solid var(--aap-secondary-dark); color: var(--aap-secondary-dark) !important;"><i class="fa fa-times"></i> fermer </button>`;
                return html;
            },
            fincartlist: function(customFinObj) {
                var returnHtml = '';
                var panierlist = customFinObj.data.final.lines.filter(function(params) {
                    return !notEmpty(params.status) || params.status != "payed"
                })
                if (notEmpty(panierlist)) {
                    returnHtml += `<div>`;

                    returnHtml += `</div>`;
                }
                return returnHtml;
            }
        },
        events: {
            init: function() {

                //customFinObj.events.initWebSocket(customFinObj);

                $('#aacselect').select2();

                // Callback sur l'événement `change`
                $('#aacselect').on('change', function (e) {
                    var selectedValue = $(this).val();
                    customFinObj.data.currentAacParent.id = $(this).val();
                    customFinObj.data.currentAacParent.name = aacParents[$(this).val()].name;
                });

                $('#aacselect').trigger('change');

                $(".ap-starter-btn-start-custom").off().click(function() {
                    $(".checkbox-custom-start").trigger('click');
                    $(".checkbox-custom-start2").trigger('click');
                    if ($(".checkbox-cofinance-start-container").length > 0)
                        $(".checkbox-cofinance-start-container").trigger('click');
                    setTimeout(function() {
                        if (depenseSubType != '') {
                            $('.swiper-wrapper').append(customFinObj.views.selectftl);
                        } else {
                            $('.swiper-wrapper').append(customFinObj.views.selectAction);
                        }
                        $('.profilPicDropdown').append(customFinObj.views.profilPicDropdown);
                        $('.profilPicDropdown img').addClass('custom-fin-img-mobile');
                        customFinObj.events.btnEvent(customFinObj);
                        $('.co-popup-custom-content').hide();
                        $('.co-popup-custom-question').show();

                        swiper = new Swiper('.swiper.cofinancestandalone#swiper', {
                            loop: false,
                            slidesPerView: 1,
                            observer: true,
                            observeParents: true,
                            observeSlideChildren: true,
                            watchSlidesVisibility: false,
                            autoplayDisableOnInteraction: false,
                            keyboardControl: false,
                            mousewheelControl: false,
                            allowSlidePrev: false,
                            allowTouchMove: false,
                        });

                        if($('.co-popup-custom-projectresume .title-info').length > 0
                            && customFinObj.data.currentAacParent.id != ""
                            && customFinObj.data.currentAacParent.name != ""
                        ){
                            $('.co-popup-custom-projectresume .title-info').html(customFinObj.data.currentAacParent.name);
                        }

                    }, "700");

                });

                $(".ap-starter-btn-email").off().click(function() {
                    if ($(this).attr('data-type') == "toogle") {
                        if (!$(this).hasClass('send')) {
                            $(this).html('Envoyer');
                            $(this).attr('data-type', 'send');
                            $(".ap-starter-btn-connect").addClass('email');
                            $(".inputemail").addClass('active');
                        }
                    } else if ($(this).attr('data-type') == "send") {
                        if ($(".custom-financer-bg .inputemail").val() == "" && $(".custom-financer-mobile-bg .inputemail").val() == "") {
                            $(".inputemail").addClass('error');
                        } else {
                            $this = $(this);
                            var params = {};
                            var emailUser = "";
                            if (!$(".custom-financer-bg .inputemail").val() == "") {
                                emailUser = $(".custom-financer-bg .inputemail").val();
                            } else {
                                emailUser = $(".custom-financer-mobile-bg .inputemail").val()
                            }
                            ajaxPost(
                                null,
                                baseUrl + `/survey/answer/answerwithemail/`, {
                                    id: coForm_FormWizardParams.answer._id.$id,
                                    form: coForm_FormWizardParams.parentForm._id.$id,
                                    email: emailUser,
                                    resLocation: window.location.href
                                },
                                function(res) {
                                    if (coForm_FormWizardParams.parentForm.withconfirmation) {
                                        toastr.success(res.msg);
                                    } else {
                                        location.reload();
                                    }
                                });
                        }

                    }
                });
            },
            btnEvent: function(customFinObj) {
                if ($('.sfacc-container').length > 0 && customFinObj.data.financertype != "tl") {
                    $('.sfacc-container').hide();
                    $('.labelcofinance').hide();
                }

                customFinObj.actions.updateStat(customFinObj);

                $(`.cofinancemobileftl input.inputpreconfig[type="checkbox"]`).off().on("change", function() {
                    thisinput = $(this);
                    $(`[data-aliases='${thisinput.data("aliases")}']`).prop('checked', thisinput.prop('checked') );
                    $(`.cofinancemobileftl .preconfig-btn-next[data-actualstep="selectamount"]`).prop('disabled', false);
                    if (thisinput.prop('checked')) {
                        customFinObj.data.lineToPay[thisinput.data('num')] = {
                            "num": thisinput.data('num'),
                            "reste": thisinput.data('reste'),
                            "amount": 0,
                            "line": thisinput.data('line'),
                        };
                    } else {
                        if (typeof customFinObj.data.lineToPay[thisinput.data('num')] != "undefined") {
                            delete customFinObj.data.lineToPay[thisinput.data('num')];
                        }
                    }
                    customFinObj.actions.updateDistList(customFinObj, customFinObj.data.final.useramount);
                });

                $(`.cofinanceftl input.inputpreconfig[type="checkbox"]`).off().on("change", function() {
                    thisinput = $(this);
                    $(`[data-aliases='${thisinput.data("aliases")}']`).prop('checked', thisinput.prop('checked') );

                    if (thisinput.prop('checked')) {
                        customFinObj.data.lineToPay[thisinput.data('num')] = {
                            "num": thisinput.data('num'),
                            "reste": thisinput.data('reste'),
                            "amount": 0,
                            "line": thisinput.data('line'),
                        };
                    } else {
                        if (typeof customFinObj.data.lineToPay[thisinput.data('num')] != "undefined") {
                            delete customFinObj.data.lineToPay[thisinput.data('num')];
                        }
                    }
                    if ($(`.cofinanceftl input.inputpreconfig[type="checkbox"]:checked`).length > 0) {
                        $(`.preconfig-btn-next`).prop('disabled', false);
                    } else {
                        $(`.preconfig-btn-next`).prop('disabled', 'disabled');
                    }
                });

                $(`input.inputnameadress[type="text"]`).off().on('keyup ', function() {
                    $(`input.inputnameadress[type="text"]`).each(function(index) {
                        var thisinput = $(this);
                        if ($(this).val()) {
                            $(`.preconfig-btn-next[data-actualstep="${ thisinput.attr('data-actualstep') }"]`).prop('disabled', false);
                            return false; // breaks
                        }
                    });

                    customFinObj.data.currentUser.name = $(`input.inputnameadress[type="text"]#name`).val();
                    customFinObj.data.currentUser.email = $(`input.inputnameadress[type="text"]#email`).val();
                    customFinObj.data.currentUser.havChangeEmail = true;
                });

                $(".preconfig-btn-next").off().click(function(e) {
                    e.preventDefault();
                    if ($(this).attr('disabled') != 'disabled') {
                        customFinObj.actions.updateDistList(customFinObj, customFinObj.data.final.useramount);
                        var actualstep = "selectAction";
                        var nextpoint = 2;
                        var nextstep = "step2";
                        var radioname = "radio";
                        var fintype = "all";
                        var amount = 0;
                        var line = null;
                        var fintype = "all";
                        var needamount = 0;
                        var totalamount = 0;
                        if ($(this).data('actualstep')) {
                            actualstep = $(this).data('actualstep');
                        }
                        if ($(this).data('next')) {
                            nextpoint = $(this).data('next');
                        }
                        if ($(this).data('nextstep')) {
                            nextstep = $(this).data('nextstep');
                        }
                        if ($(this).data('radioname') && $(this).data('radioname') != '') {
                            radioname = $(this).data('radioname');
                        }

                        const swiperEl = document.querySelector('.custom-financer-bg .swiper.cofinancestandalone');

                        swiperEl.swiper.appendSlide(customFinObj.views[nextstep](nextpoint));

                        swiperEl.swiper.update();
                        swiperEl.swiper.slideNext();
                        swiperEl.swiper.removeSlide(0);
                        swiperEl.swiper.update();
                        //customFinObj.events.btnEvent(customFinObj);


                        const swiperElmobile = document.querySelector('.custom-financer-mobile-bg .swiper.cofinancestandalone');

                        swiperElmobile.swiper.appendSlide(customFinObj.views[nextstep](nextpoint));

                        swiperElmobile.swiper.update();
                        swiperElmobile.swiper.slideNext();
                        swiperElmobile.swiper.removeSlide(0);
                        swiperElmobile.swiper.update();
                        customFinObj.events.btnEvent(customFinObj);


                        if (depenseSubType != '') {
                            if ($(".inputpreconfig.checkbox-budget").length && notEmpty(customFinObj.data.final.tlamount)) {
                                if ($(".inputpreconfig.checkbox-budget[data-num='" + customFinObj.data.final.tlamount + "']").length > 0) {
                                    $(".inputpreconfig.checkbox-budget[data-num='" + customFinObj.data.final.tlamount + "']").prop('checked', 'checked');
                                } else {
                                    $(".inputamount").val(customFinObj.data.final.tlamount);
                                }
                                $(`.preconfig-btn-next[data-actualstep="selectamount"]`).prop('disabled', false);
                            }

                            /* if ($('.cofinanceftl .selectAllLine').length > 0 && actualstep == "selectAction") {
                                 $('.cofinanceftl .selectAllLine').trigger('click');
                             }

                             if ($('.cofinancemobileftl .selectAllLine').length > 0 && actualstep == "selectAction") {
                                 $('.cofinancemobileftl .selectAllLine').trigger('click');
                             }*/
                        }
                        customFinObj.actions.updateDistList(customFinObj, customFinObj.data.final.useramount);
                    }

                    if(notEmpty(customFinObj.data.lineToPay)){
                        $.each(customFinObj.data.lineToPay, function (index,value){
                            $(".inputpreconfig[type='checkbox'][data-num='"+index+"']").prop('checked','checked');
                            $(`.preconfig-btn-next[data-actualstep="selectAction"]`).prop('disabled', false);
                        });
                    }

                });

                $(".ap-starter-btn-prev").off().click(function(e) {
                    e.preventDefault();

                    if ($(this).data('actualstep')) {
                        var actualstep = $(this).data('actualstep');
                    }
                    if ($(this).data('prevstep')) {
                        var prevstep = $(this).data('prevstep');
                    }
                    if ($(this).data('nextstep')) {
                        var nextstep = $(this).data('nextstep');
                    }
                    if ($(this).data('radioname') && $(this).data('radioname') != '') {
                        var radioname = $(this).data('radioname');
                    }

                    const swiperEl = document.querySelector('.custom-financer-bg .swiper.cofinancestandalone');
                    const thisswiper = $(this);

                    swiperEl.swiper.appendSlide(customFinObj.views[prevstep](2));

                    swiperEl.swiper.update();
                    swiperEl.swiper.slideNext();
                    customFinObj.events.btnEvent(customFinObj);
                    swiperEl.swiper.removeSlide(0);
                    swiperEl.swiper.update();

                    const swiperElmobile = document.querySelector('.custom-financer-mobile-bg .swiper.cofinancestandalone');

                    swiperElmobile.swiper.appendSlide(customFinObj.views[prevstep](2));

                    swiperElmobile.swiper.update();
                    swiperElmobile.swiper.slideNext();
                    customFinObj.events.btnEvent(customFinObj);
                    swiperElmobile.swiper.removeSlide(0);
                    swiperElmobile.swiper.update();

                    if (depenseSubType != '') {
                        if (notEmpty(customFinObj.data.currentOrga.name) && $('div.mm-dropdown > ul > li.input-option').length > 0) {
                            //$('div.mm-dropdown > ul > li.input-option[data-name="' + customFinObj.data.currentOrga.name + '"]').trigger('click').addClass('active');
                        }

                        if ($(".inputpreconfig.checkbox-budget").length && notEmpty(customFinObj.data.final.tlamount)) {
                            $(".inputamount").val(customFinObj.data.final.tlamount);
                            $(`.preconfig-btn-next[data-actualstep="selectamount"]`).prop('disabled', false);
                        }

                        //if($('.cofinanceftl .selectAllLine').length > 0){
                        if(notEmpty(customFinObj.data.lineToPay)){
                            $.each(customFinObj.data.lineToPay, function (index,value){
                                $(".inputpreconfig[type='checkbox'][data-num='"+index+"']").prop('checked','checked');
                            });
                        }
                        //}
                    }

                });

                $(".selectAllLine").off().click(function(){
                    $(this).toggleClass('active');
                    $('input.inputpreconfig:checkbox:not([disabled])').prop('checked', $(this).hasClass('active')).trigger('change');
                    if($(this).hasClass('active')){
                        $(".selectAllLine").html("Tout désélectionner");
                    }else{
                        $(".selectAllLine").html("Tout sélectionner");
                    }
                });

                $(".doublefinancecheck:visible").off().on('change', function(){
                    if($(this).prop('checked')){
                        //$('.sfacc-container').show();
                        customFinObj.data.final.double = true;
                    }else{
                        //$('.sfacc-container').hide();
                        customFinObj.data.final.double = false;
                    }
                    if($('.inputamount').val() != '') {
                        $('.inputamount').keyup();
                        if(customFinObj.data.final.double == true && $('.inputamount').val() < 100){
                            $('.errorsfacc').removeClass('hide');
                        }else{
                            $('.errorsfacc').addClass('hide');
                        }
                    }

                    if($(`input.inputpreconfig[type="radio"]:checked`).length > 0){
                        $(`input.inputpreconfig[type="radio"]:checked`).trigger('change');
                    }

                });

                $(".recapcheck").off().on('change', function(){
                    if($(this).prop('checked') == true ) {
                        $(`.ap-starter-btn-finish`).prop('disabled', false);
                    }else{
                        $(`.ap-starter-btn-finish`).prop('disabled', 'disabled');
                    }
                });

                $(".ap-starter-btn-dashboard").off().click(function() {
                    /*$('.swiper-slide').eq(0).remove();
                    $('.swiper-slide').eq(1).remove();*/
                    $(".checkbox-custom-start").trigger('click');
                    $(".checkbox-custom-start2").trigger('click');
                    if ($(".checkbox-cofinance-start-container").length > 0)
                        $(".checkbox-cofinance-start-container").trigger('click');
                    setTimeout(function() {
                        $('.co-popup-custom-content').show();
                        $('.co-popup-custom-question').hide();
                        $('.co-popup-custom-finish').hide();
                        $('.swiper-slide').remove();
                        $('.dropleft').remove();
                        customFinObj.actions.updateStat(customFinObj);
                    }, "700");
                    ajaxPost(null, baseUrl + '/survey/answer/getparamsfinancerstandalone/answerid/' + coForm_FormWizardParams.answer._id.$id + '/formid/' + coForm_FormWizardParams.parentForm._id.$id, null, function(res) {
                        jQuery.extend(customFinObj.data.final, res);
                    }, null, "json", {
                        async: true
                    });
                });

                $(".ap-starter-btn-cart").off().click(function() {
                    $(".co-popup-cofinance-container").remove();
                    $('span.cos-cart').trigger('click');
                });

                $(".ap-starter-btn-min-cart").off().click(function() {
                    $(".co-popup-cofinance-container").remove();
                    $('span.cos-cart').trigger('click');
                    customFinObj.data.lineToPay = {};
                });

                $(".ap-starter-btn-close2").off().on("click", function() {
                    if ($(".co-popup-cofinance-container").length > 0)
                        $(".co-popup-cofinance-container").remove();
                    if (depenseSubType != '') {
                        customFinObj.data.lineToPay = {};
                    }
                });

                $(".ap-starter-btn-finish").off().click(function() {
                    //$.when(function () {
                    coInterface.showCostumLoader(`.pasl-container`);
                    coInterface.showCostumLoader(`.pfpb-container`);
                    //}).then(function () {
                    $('.ap-starter-btn-finish').html('Envoi..');
                    $('.ap-starter-btn-finish').attr('disabled', 'disabled');

                    $('.co-popup-custom-question').hide();
                    $('.co-popup-custom-finish').show();
                    if ($(".checkbox-cofinance-start-container").length > 0)
                        $(".checkbox-cofinance-start-container").trigger('click');

                    if (!notEmpty(costum.communityLinks.members[userId])) {
                        var postdata = {
                            parentId: costum.contextId,
                            parentType: costum.contextType,
                            listInvite: {
                                citoyens: {}
                            }
                        };
                        postdata.listInvite.citoyens[userId] = {
                            name: userConnected.name,
                            roles: ["Financeur"]
                        };
                        ajaxPost(
                            "",
                            baseUrl + '/' + moduleId + "/link/multiconnect", postdata,
                            function (res) {
                                console.log('eeeeee', res);
                            },
                            null,
                            null, {
                                async: false
                            }
                        )
                    }
                    setTimeout(function () {
                        delete coForm_FormWizardParams.aapview;
                        tplCtx = {
                            id: coForm_FormWizardParams.answer._id.$id,
                            collection: "answers",
                            arrayForm: true,
                            form: "aapStep1",
                            setType: [
                                {
                                    "path": "date",
                                    "type": "isoDate"
                                }
                            ]
                        }

                        //setTimeout(function() {
                        var ftlamountacc = 0;
                        var tlamountacc = 0;
                        var amountacc = 0;
                        var acc = 0;

                        $.each(customFinObj.data.lineToPay, function (index, value) {
                            tplCtx.path = "answers.aapStep1.depense." + index + ".financer";

                            var tlamount = 0;
                            var ftlamount = 0;
                            const chars = 'azertyuiopqsdfghjklmwxcvbn123456789';
                            var rand = (min = 0, max = 1000) => Math.floor(Math.random() * (max - min) + min);
                            randChar = (length = 6) => {
                                var randchars = [];
                                for (let i = 0; i < length; i++) {
                                    randchars.push(chars[rand(0, chars.length)]);
                                }

                                return randchars.join('');
                            }
                            keygenerator = (prefix = 'a-', sufix = '') => `${prefix}${randChar()}${sufix}`;
                            var finkey = keygenerator();
                            if (typeof customFinObj.data.final.double != "undefined" && customFinObj.data.final.double == true) {

                                //if (value.amount >= customFinObj.data.lineToAdd[index].amount) {
                                console.log('zzzzzzz4',tlamount,acc,amountacc,customFinObj.data.final.useramount );
                                    if(customFinObj.data.final.ftlamount == customFinObj.data.final.tlamount){
                                        tlamount = ftlamount = value.amount / 2;
                                        if (
                                            acc == Object.keys(customFinObj.data.lineToPay).length - 1 &&
                                            amountacc + value.amount != customFinObj.data.final.useramount
                                        ) {
                                            console.log('zzzzzzz5',(customFinObj.data.final.useramount - amountacc) / 2 );
                                            tlamount = ftlamount = (customFinObj.data.final.useramount - amountacc) / 2;
                                        }
                                    }else{
                                        tlamount = value.amount - customFinObj.data.lineToAdd[index].amount;
                                        ftlamount = roundToNearest(customFinObj.data.lineToAdd[index].amount);
                                    }
                                    amountacc = amountacc + parseFloat(value.amount);
                                /*} else {
                                    tlamount = 0;
                                    ftlamount = value.amount;
                                }*/
                            } else {
                                tlamount = value.amount;
                            }

                            console.log('zzzzzzz1',tlamount,value, tlamount > 0,acc,amountacc);

                            if (depenseSubType != "") {
                                tplCtx.value = {
                                    line: '',
                                    amount: tlamount,
                                    user: userId,
                                    date: "now",
                                    name: customFinObj.data.currentOrga.name,
                                    id: customFinObj.data.currentOrga.id,
                                    status: "pending",
                                    finkey: finkey,
                                    type: customFinObj.data.financertype
                                }
                            } else {
                                tplCtx.value = {
                                    line: '',
                                    amount: tlamount,
                                    user: userId,
                                    date: "now",
                                    name: customFinObj.data.currentUser.name,
                                    email: customFinObj.data.currentUser.email,
                                    id: customFinObj.data.currentUser.id,
                                    type: customFinObj.data.financertype
                                }
                            }

                            if(customFinObj.data.currentAacParent.id != ""){
                                tplCtx.value['aacParent'] = customFinObj.data.currentAacParent;
                            }

                            if (typeof customFinObj.data.final.double != "undefined" && customFinObj.data.final.double == true) {
                                tplCtx.value.iscofinancementftl = true;
                            } else {
                                tplCtx.value.iscofinancementftl = false;
                            }
                            console.log('zzzzzz2',acc);

                            if (tlamount > 0) {
                                console.log('zzzzzz3',acc);
                                dataHelper.path2Value(tplCtx, function (params) {
                                    if (customFinObj.data.final.double == false && index == Object.values(customFinObj.data.lineToPay).length - 1 ) {
                                        //$.when(function(){
                                        //    coInterface.showCostumLoader(`.cmsbuilder-block[data-id='${reloadpaslcontainer}`);
                                        //    coInterface.showCostumLoader(`.cmsbuilder-block[data-id='${reloadpfpbcontainer}`);
                                        //}).then(function(){
                                        // refresh_camp_count();
                                        // reloadpasl(function () {

                                        // });
                                        // reloadpfpb(function () {
                                        // });

                                        //});
                                    }
                                });

                                if (ftlamount > 0) {
                                    /*if (
                                        acc == Object.keys(customFinObj.data.lineToAdd).length &&
                                        ftlamountacc + ftlamount < customFinObj.data.final.ftlamount
                                    ) {
                                        ftlamount = customFinObj.data.final.ftlamount - ftlamountacc;
                                    }*/

                                    tplCtx.value = {
                                        line: '',
                                        amount: ftlamount,
                                        user: userId,
                                        date: "now",
                                        iscofinancementftl: true,
                                        name: typeof projectAnswer.context[costum.contextId] != "undefined" && typeof projectAnswer.context[costum.contextId].name != "undefined" ? projectAnswer.context[costum.contextId].name : "",
                                        id: costum.contextId,
                                        tlname: customFinObj.data.currentOrga.name,
                                        tlid: customFinObj.data.currentOrga.id,
                                        status: "pending",
                                        finkey: finkey
                                    }
                                    if(customFinObj.data.currentAacParent.id != ""){
                                        tplCtx.value['aacParent'] = customFinObj.data.currentAacParent;
                                    }
                                    dataHelper.path2Value(tplCtx, function (params) {
                                        if (customFinObj.data.final.double == true  && index == Object.values(customFinObj.data.lineToPay).length - 1 ) {
                                            //$.when(function(){
                                            //    coInterface.showCostumLoader(`.cmsbuilder-block[data-id='${reloadpaslcontainer}`);
                                            //    coInterface.showCostumLoader(`.cmsbuilder-block[data-id='${reloadpfpbcontainer}`);
                                            //}).then(function(){
                                            // refresh_camp_count();
                                            // reloadpasl(function () {
                                            // });
                                            // reloadpfpb(function () {
                                            // });

                                            //});
                                        }
                                    });

                                }

                            }
                            acc++;
                        });
                        //}, 100);
                        toastr.success("Effectué");
                        refresh_camp_count();
                        reloadpasl(function () {

                        });
                        reloadpfpb(function () {
                        });
                        customFinObj.data.final.percent = customFinObj.data.final.finalpercent;
                        customFinObj.data.final.reste = customFinObj.data.final.finalreste;
                        customFinObj.data.final.amountearn = customFinObj.data.final.finalamountearn;
                        if (depenseSubType == '') {
                            customFinObj.data.lineToPay = {};
                        }

                        ajaxPost("", baseUrl + '/survey/form/pingupdate/answerId/' + answerId, {}, function (data) {
                        });

                        /*setTimeout(function() {
                            reloadpasl(function() {
                                refresh_camp_count();
                            });
                            reloadpfpb(function() {

                            });
                        }, 500);*/

                    }, 200);
                    //});

                });

                $(".inputfirstsearch").off().on('input', function() {
                    var thisbtn = $(this);
                    var val = this.value.toLowerCase();
                    var dom = $(`div.mm-dropdown > ul > li.input-option[data-type="${thisbtn.data("type")}"]`);
                    dom.hide();
                    if (!val.length)
                        dom.show();
                    else {
                        dom.filter(function() {
                            var match = $(this).data('name') ? $(this).data('name') : '';
                            if (typeof match !== 'string')
                                return (false);
                            match = match.toLowerCase().match(new RegExp(val.toLowerCase(), 'g'));
                            return (match !== null);
                        }).show();
                    }
                });

                $('html').off().on('keypress', function(e) {
                    if (e.which == 13 && $(".preconfig-btn-next").length > 0) {
                        $(".preconfig-btn-next").trigger('click');
                    }
                });

                if($('.doublefinancecheck:visible').length > 0){
                    $('.doublefinancecheck:visible').trigger('click');
                }

                setTimeout(function() {
                    if ($('.depensefinancerdropdown ul').length > 0) {
                        var rhtml = '';
                        if (notEmpty(depenseFinancer)) {
                            $.each(depenseFinancer, function(i, v) {
                                var img = '';
                                if (notEmpty(v.image)) {
                                    img = v.image;
                                } else if (notEmpty(v.profilImageUrl)) {
                                    img = v.profilImageUrl;
                                } else {
                                    img = assetPath + "/images/thumbnail-default.jpg";
                                }
                                rhtml += `<li class="input-option" data-type="searchftl" data-value="${i}" data-name="${v.name}" data-actualstep="selectftl">
                                                                            <img src="${img}" alt="" width="20" height="20" /> ${v.name}
                                                                        </li>`;
                            });
                        } else {
                            rhtml += `<li class="input-option" data-type="searchftl" data-value="" data-actualstep="selectftl">
                                                                        <img height="140" src="${assetPath + '/images/thumbnail-default.jpg'}" class="answer-depense-img"> Vous n'êtes pas encore admin d'un tiers lieux
                                                                    </li>`;
                        }

                        var rhtmlorga = '';
                        if (notEmpty(depenseFinancerOrga)) {
                            $.each(depenseFinancerOrga, function(i, v) {
                                var img = '';
                                if (notEmpty(v.image)) {
                                    img = v.image;
                                } else if (notEmpty(v.profilImageUrl)) {
                                    img = v.profilImageUrl;
                                } else {
                                    img = assetPath + "/images/thumbnail-default.jpg";
                                }
                                rhtmlorga += `<li class="input-option" data-type="searchorga" data-value="${i}" data-name="${v.name}" data-actualstep="selectftl">
                                                                            <img src="${img}" alt="" width="20" height="20" /> ${v.name}
                                                                        </li>`;
                            });
                        } else {
                            rhtmlorga += `<li class="input-option" data-type="searchorga" data-value="" data-actualstep="selectftl">
                                                                        <img height="140" src="${assetPath + '/images/thumbnail-default.jpg'}" class="answer-depense-img"> Vous n'êtes pas encore admin d'un tiers lieux
                                                                    </li>`;
                        }

                        $.when($('.financerchoicetab[data-type="tl"] .depensefinancerdropdown ul').html(rhtml)).then(function() {
                            $.when($('.financerchoicetab[data-type="organization"] .depensefinancerdropdown ul').html(rhtmlorga)).then(function() {
                                $('div.mm-dropdown > ul > li.input-option').off().on('click', function () {
                                    var livalue = $(this).data('value');
                                    var lihtml = $(this).html();
                                    if (!livalue) {
                                        join_tl();
                                        $('.ap-starter-btn-close2').trigger('click');
                                        return;
                                    }
                                    $("div.mm-dropdown .option").val(livalue);
                                    if(customFinObj.data.financertype == 'tl'){
                                        customFinObj.data.currentOrga.name = depenseFinancer[livalue].name;
                                        customFinObj.data.currentOrga.id = livalue;
                                        $('.carddesc-tlname').html(depenseFinancer[livalue].name);
                                    }else if(customFinObj.data.financertype == 'organization'){
                                        customFinObj.data.currentOrga.name = depenseFinancerOrga[livalue].name;
                                        customFinObj.data.currentOrga.id = livalue;
                                        $('.carddesc-tlname').html(depenseFinancerOrga[livalue].name);
                                    }else if(customFinObj.data.financertype == 'person' && typeof userConnected != "undefined"){
                                        customFinObj.data.currentOrga.name = userConnected.name;
                                        customFinObj.data.currentOrga.id = userId;
                                        //$('.carddesc-tlname').html(depenseFinancer[livalue].name);
                                    }

                                    $('div.mm-dropdown > ul > li.input-option').removeClass('active');
                                    $(this).addClass('active');
                                    $(`.preconfig-btn-next[data-actualstep="selectftl"]`).prop('disabled', false);
                                    $('.preconfig-btn-next[data-actualstep="selectftl"]').trigger('click');
                                });

                                $('.confirmperson').off().on('click', function () {
                                    customFinObj.data.financertype = 'person';
                                    customFinObj.data.currentOrga.name = userConnected.name;
                                    customFinObj.data.currentOrga.id = userId;
                                    $('.carddesc-tlname').html(userConnected.name);
                                    $(`.preconfig-btn-next[data-actualstep="selectftl"]`).prop('disabled', false);
                                    $('.preconfig-btn-next[data-actualstep="selectftl"]').trigger('click');
                                });

                                /*if (Object.keys(depenseFinancer).length == 1) {
                                    $('.carddesc-tlname').html(depenseFinancer[Object.keys(depenseFinancer)[0]].name);
                                    $('div.mm-dropdown > ul > li.input-option').addClass('active');
                                    var livalue = $(this).data('value');
                                    $("div.mm-dropdown .option").val(livalue);
                                    customFinObj.data.currentOrga.name = depenseFinancer[Object.keys(depenseFinancer)[0]].name;
                                    customFinObj.data.currentOrga.id = Object.keys(depenseFinancer)[0];
                                    $(`.preconfig-btn-next[data-actualstep="selectftl"]`).prop('disabled', false);
                                    $('.cofinancemodal .preconfig-btn-next[data-actualstep="selectftl"]').trigger('click');
                                }*/
                            });
                        });
                    }
                    if($(".doublefinancecheck:visible").length > 0){
                        if(customFinObj.data.final.double == true){
                            $(".doublefinancecheck:visible").prop("checked" , "checked").trigger('change');
                        }
                    }
                }, "10");
                $('.doublefinancecheck:visible').prop('disabled','disabled');
                if ($(' .inputamount').length > 0) {
                    var post = {
                        form: projectAnswer.form
                    };
                    url = baseUrl + '/co2/aap/funding/request/cart';
                    if(typeof customFinObj.data.final.cart != "undefined" && notEmpty(customFinObj.data.final.cart)){
                        var resp = customFinObj.data.final.cart;

                        if(typeof resp.content.campagne[customFinObj.data.currentOrga.id] != "undefined" && parseInt(resp.content.campagne[customFinObj.data.currentOrga.id]) > 0){
                            $('.doublefinancecheck:visible').prop('disabled', false);
                        }

                        $(`input.inputpreconfig[type="radio"]`).off().on('change', function () {
                            $(`input.inputamount[type="number"]`).val('');
                            var thisinput = $(this);
                            var totalToFin = thisinput.prop('value');

                            if (thisinput.val() != "") {
                                $(`.preconfig-btn-next[data-actualstep="${thisinput.attr('data-actualstep')}"]`).prop('disabled', false);
                                totalToFin = thisinput.val();
                            }

                            var userinput = parseInt(thisinput.val());
                            if (userinput > 0) {
                                if ((userinput) < parseInt(resp.content.campagne[customFinObj.data.currentOrga.id])) {
                                    //$('.sfpro').html((userinput - (userinput / 2)) + "<i class='fa fa-euro'></i>");
                                    $('.sfacc').html(userinput + " <i class='fa fa-euro'></i>");
                                    customFinObj.data.final.ftlamount = customFinObj.data.final.tlamount = userinput;
                                } else {
                                    $('.sfacc').html(Math.round(resp.content.campagne[customFinObj.data.currentOrga.id]) + " <i class='fa fa-euro'></i>");
                                    $('.sfpro').html((userinput - (resp.content.campagne[customFinObj.data.currentOrga.id])) + " <i class='fa fa-euro'></i>");
                                    customFinObj.data.final.ftlamount = resp.content.campagne[customFinObj.data.currentOrga.id];
                                    customFinObj.data.final.tlamount = userinput;
                                }
                            } else {
                                $('.sfacc').html("0 <i class='fa fa-euro'></i>");
                                $('.sfpro').html("0 <i class='fa fa-euro'></i>");
                            }
                            if(customFinObj.data.final.double == false){
                                customFinObj.actions.updateDistList(customFinObj, customFinObj.data.final.tlamount);
                            }else{
                                customFinObj.actions.updateDistList(customFinObj, customFinObj.data.final.ftlamount + customFinObj.data.final.tlamount);
                            }

                        });
                        $('.inputamount').off().on('keyup ', function () {
                            var thisinput = $(this);
                            var totalToFin = 0;

                            if (thisinput.val() != "") {
                                $(`.checkbox-budget`).prop('checked', false);
                                $(`.preconfig-btn-next[data-actualstep="${thisinput.attr('data-actualstep')}"]`).prop('disabled', false);
                                totalToFin = thisinput.val();
                            } else {
                                $(`.preconfig-btn-next[data-actualstep="${thisinput.attr('data-actualstep')}"]`).prop('disabled', 'disabled');
                                totalToFin = 0;
                            }

                            var userinput = parseInt(thisinput.val());
                            if ((customFinObj.data.final.double == true && userinput >= 100) || (customFinObj.data.final.double == false && userinput > 0)) {
                                if (customFinObj.data.final.double == true) {
                                    $('.errorsfacc').addClass("hide");
                                }
                                if (userinput < parseInt(resp.content.campagne[customFinObj.data.currentOrga.id])) {
                                    $('.sfacc').html((userinput) + " <i class='fa fa-euro'></i>");
                                    //$('.sfpro').html((userinput - (userinput / 2)) + "<i class='fa fa-euro'></i>");
                                    customFinObj.data.final.ftlamount = customFinObj.data.final.tlamount = userinput;
                                } else {
                                    $('.sfacc').html((resp.content.campagne[customFinObj.data.currentOrga.id]) + " <i class='fa fa-euro'></i>");
                                    $('.sfpro').html((userinput - (resp.content.campagne[customFinObj.data.currentOrga.id])) + " <i class='fa fa-euro'></i>");
                                    customFinObj.data.final.ftlamount = resp.content.campagne[customFinObj.data.currentOrga.id];
                                    customFinObj.data.final.tlamount = userinput;
                                }
                            } else {
                                $('.sfacc').html("0 <i class='fa fa-euro'></i>");
                                $('.sfpro').html("0 <i class='fa fa-euro'></i>");
                                if (customFinObj.data.final.double == true) {
                                    $('.errorsfacc').removeClass("hide");
                                }
                            }

                            if(customFinObj.data.final.double == false){
                                customFinObj.actions.updateDistList(customFinObj, customFinObj.data.final.tlamount);
                            }else{
                                customFinObj.actions.updateDistList(customFinObj, customFinObj.data.final.ftlamount + customFinObj.data.final.tlamount);
                            }
                        });

                        if (notEmpty(customFinObj.data.currentOrga.id) && typeof resp.content.campagne != "undefined" && typeof resp.content.campagne[customFinObj.data.currentOrga.id] != "undefined" && resp.content.campagne[customFinObj.data.currentOrga.id] != 0) {
                            /*var panier = 0;
                            var dep = resp.content.funds[0].communs.map(function(x){
                                 if(notEmpty(x.depense)){
                                     return x.depense
                                 }
                             }).flat();
                             $.each(dep, function(index,value){
                                 if(notEmpty(value.financer)){
                                     panier += parseInt(value.financer.map(function(x){
                                         if(typeof x.status == "undefined" || x.status != "payed"){
                                             return x.amount;
                                         }
                                     }));
                                 }
                             });*/

                            $('.sfdispo-container').addClass('active').show();
                            $('.sfdispo').html(Math.round(resp.content.campagne[customFinObj.data.currentOrga.id]) + " <i class='fa fa-euro'></i>");
                            //$('.sfcart').html(panier+"<i class='fa fa-euro'></i>");
                            if (parseInt(resp.content.campagne[customFinObj.data.currentOrga.id]) !== 0 && $(".doublefinancecheck").prop('checked') && customFinObj.data.financertype == "tl") {
                                $('.sfacc-container').show();
                            }
                        } else {
                            $('.sfdispo-container').addClass('active').show();
                            $('.sfdispo').html("0 <i class='fa fa-euro'></i>");

                        }
                    }else {
                        ajaxPost(null, url, post, function (resp) {
                            customFinObj.data.final.cart = resp;
                            if(typeof resp.content.campagne[customFinObj.data.currentOrga.id] != "undefined" && parseInt(resp.content.campagne[customFinObj.data.currentOrga.id]) > 0){
                                $('.doublefinancecheck:visible').prop('disabled', false);
                            }
                            $(`input.inputpreconfig[type="radio"]`).off().on('change', function () {
                                $(`input.inputamount[type="number"]`).val('');
                                var thisinput = $(this);
                                var totalToFin = thisinput.prop('value');

                                if (thisinput.val() != "") {
                                    $(`.preconfig-btn-next[data-actualstep="${thisinput.attr('data-actualstep')}"]`).prop('disabled', false);
                                    totalToFin = thisinput.val();
                                }

                                var userinput = parseInt(thisinput.val());
                                if (userinput > 0) {
                                    if ((userinput) < parseInt(resp.content.campagne[customFinObj.data.currentOrga.id])) {
                                        $('.sfacc').html((userinput) + " <i class='fa fa-euro'></i>");
                                        //$('.sfpro').html((userinput - (userinput / 2)) + "<i class='fa fa-euro'></i>");
                                        customFinObj.data.final.ftlamount = customFinObj.data.final.tlamount = userinput;
                                    } else {
                                        $('.sfacc').html((resp.content.campagne[customFinObj.data.currentOrga.id]) + " <i class='fa fa-euro'></i>");
                                        $('.sfpro').html((userinput - (resp.content.campagne[customFinObj.data.currentOrga.id])) + " <i class='fa fa-euro'></i>");
                                        customFinObj.data.final.ftlamount = resp.content.campagne[customFinObj.data.currentOrga.id];
                                        customFinObj.data.final.tlamount = userinput;
                                    }
                                } else {
                                    $('.sfacc').html("0 <i class='fa fa-euro'></i>");
                                    $('.sfpro').html("0 <i class='fa fa-euro'></i>");
                                }

                                if(customFinObj.data.final.double == false){
                                    customFinObj.actions.updateDistList(customFinObj, customFinObj.data.final.tlamount);
                                }else{
                                    customFinObj.actions.updateDistList(customFinObj, customFinObj.data.final.ftlamount + customFinObj.data.final.tlamount);
                                }
                            });
                            $('.inputamount').off().on('keyup ', function () {
                                var thisinput = $(this);
                                var totalToFin = 0;

                                if (thisinput.val() != "") {
                                    $(`.checkbox-budget`).prop('checked', false);
                                    $(`.preconfig-btn-next[data-actualstep="${thisinput.attr('data-actualstep')}"]`).prop('disabled', false);
                                    totalToFin = thisinput.val();
                                } else {
                                    $(`.preconfig-btn-next[data-actualstep="${thisinput.attr('data-actualstep')}"]`).prop('disabled', 'disabled');
                                    totalToFin = 0;
                                }

                                var userinput = parseInt(thisinput.val());
                                if ((customFinObj.data.final.double == true && userinput >= 100) || (customFinObj.data.final.double == false && userinput > 0)) {
                                    if (customFinObj.data.final.double == true) {
                                        $('.errorsfacc').addClass("hide");
                                    }
                                    if (userinput < parseInt(resp.content.campagne[customFinObj.data.currentOrga.id])) {
                                        $('.sfacc').html((userinput) + " <i class='fa fa-euro'></i>");
                                        //$('.sfpro').html((userinput - (userinput / 2)) + "<i class='fa fa-euro'></i>");
                                        customFinObj.data.final.ftlamount = customFinObj.data.final.tlamount = userinput;
                                    } else {
                                        $('.sfacc').html((resp.content.campagne[customFinObj.data.currentOrga.id]) + " <i class='fa fa-euro'></i>");
                                        $('.sfpro').html((userinput - (resp.content.campagne[customFinObj.data.currentOrga.id])) + " <i class='fa fa-euro'></i>");
                                        customFinObj.data.final.ftlamount = resp.content.campagne[customFinObj.data.currentOrga.id];
                                        customFinObj.data.final.tlamount = userinput;
                                    }
                                } else {
                                    $('.sfacc').html("0 <i class='fa fa-euro'></i>");
                                    $('.sfpro').html("0 <i class='fa fa-euro'></i>");
                                    if (customFinObj.data.final.double == true) {
                                        $('.errorsfacc').removeClass("hide");
                                    }
                                }

                                if(customFinObj.data.final.double == false){
                                    customFinObj.actions.updateDistList(customFinObj, customFinObj.data.final.tlamount);
                                }else{
                                    customFinObj.actions.updateDistList(customFinObj, customFinObj.data.final.ftlamount + customFinObj.data.final.tlamount);
                                }
                            });

                            if (notEmpty(customFinObj.data.currentOrga.id) && typeof resp.content.campagne != "undefined" && typeof resp.content.campagne[customFinObj.data.currentOrga.id] != "undefined" && resp.content.campagne[customFinObj.data.currentOrga.id] != 0) {
                                /*var panier = 0;
                                var dep = resp.content.funds[0].communs.map(function(x){
                                     if(notEmpty(x.depense)){
                                         return x.depense
                                     }
                                 }).flat();
                                 $.each(dep, function(index,value){
                                     if(notEmpty(value.financer)){
                                         panier += parseInt(value.financer.map(function(x){
                                             if(typeof x.status == "undefined" || x.status != "payed"){
                                                 return x.amount;
                                             }
                                         }));
                                     }
                                 });*/

                                $('.sfdispo-container').addClass('active').show();
                                $('.sfdispo').html(resp.content.campagne[customFinObj.data.currentOrga.id] + "<i class='fa fa-euro'></i>");
                                //$('.sfcart').html(panier+"<i class='fa fa-euro'></i>");
                                if (parseInt(resp.content.campagne[customFinObj.data.currentOrga.id]) !== 0 && $(".doublefinancecheck:visible").prop('checked') && customFinObj.data.financertype == "tl") {
                                    $('.sfacc-container').show();
                                }
                            } else {
                                $('.sfdispo-container').addClass('active').show();
                                $('.sfdispo').html("0 <i class='fa fa-euro'></i>");

                            }
                        }, null, null, {
                            async: false
                        })
                    }
                }

                $('html').off().on('keypress', function(e) {
                    if (e.which == 13 && $(".preconfig-btn-next").length > 0) {
                        $(".preconfig-btn-next").trigger('click');
                    }
                });

                $('.financerchoicebtn').click(function (e) {
                    e.stopPropagation();
                    thisbtn = $(this);
                    customFinObj.data.financertype = thisbtn.data('choice');
                    if(thisbtn.data('choice') != 'tl'){
                        customFinObj.data.final.double = false;
                    }
                    $(`.financerchoicetab[data-type="${thisbtn.data('actuel')}"]`).hide();
                    $(`.financerchoicetab[data-type="${thisbtn.data('choice')}"]`).show('slide', {direction: 'left'}, 200);
                })
            },
            initWebSocket: function(customFinObj) {
                if (!notNull(wsCO) || !wsCO.hasListeners('refresh-coform-answer')) {
                    var wsCO = null

                    var socketConfigurationEnabled = coWsConfig.enable && coWsConfig.serverUrl && coWsConfig.pingRefreshCoformAnswerUrl;
                    if (socketConfigurationEnabled) {
                        wsCO = io(coWsConfig.serverUrl, {
                            query: {
                                answer: answerId
                            }
                        });

                        wsCO.on('refresh-coform-answer', function() {
                            /*ajaxPost(null, baseUrl+'/survey/answer/getparamsfinancerstandalone/answerid/'+coForm_FormWizardParams.answer._id.$id+'/formid/'+coForm_FormWizardParams.parentForm._id.$id, null , function (res) {
                                jQuery.extend(customFinObj.data.final , res);
                                customFinObj.data.final.finalamountearn = customFinObj.data.final.amountearn;
                                customFinObj.data.final.finalpercent = customFinObj.data.final.percent ;
                                customFinObj.data.final.finalreste = customFinObj.data.final.reste;
                                customFinObj.actions.updateStat(customFinObj);
                                customFinObj.actions.getFinancerList(customFinObj);
                                customFinObj.actions.getPorterList(customFinObj);
                            }, null, "json", {async : true});*/
                        });
                    }
                }
            },
        },
        actions: {
            open: function(aapObj) {
                ajaxPost(null, baseUrl + '/survey/answer/getparamsfinancerstandalone/answerid/' + coForm_FormWizardParams.answer._id.$id + '/formid/' + coForm_FormWizardParams.parentForm._id.$id, null, function(res) {
                    jQuery.extend(customFinObj.data.final, res);
                    //customFinObj.data.final.amountearn = customFinObj.data.final.finalamountearn;
                    customFinObj.data.final.finalpercent = customFinObj.data.final.percent;
                    customFinObj.data.final.finalreste = customFinObj.data.final.reste;
                    $.when(customFinObj.actions.updateStat(customFinObj)).then(function(x) {
                        $('.co-popup-cofinance-loader-section').addClass('hide');
                        $('.custom-financer-bg').removeClass('hide');
                        $('.custom-financer-mobile-bg').removeClass('hide');
                        $(".checkbox-custom-start").trigger('click');
                        $(".checkbox-custom-start2").trigger('click');
                        $(".checkbox-custom-start").trigger('click');
                        $(".checkbox-custom-start2").trigger('click');
                    });
                    //customFinObj.actions.updateStat(customFinObj);
                    mylog.log("customFinObj.initialized", aapObj.canEdit, customFinObj.initialized);
                    if (!customFinObj.initialized) {
                        customFinObj.events.init();
                    }
                }, null, "json", {
                    async: true
                });
            },
            close: function() {
                if ($(".co-popup-custom-container").length > 0)
                    $(".co-popup-custom-container").remove();
            },
            finished: function() {
                if ($(".co-popup-custom-container").length > 0)
                    $(".co-popup-custom-container").remove();
            },
            updateDistList: function(customFinObj, totalToFin) {

                // Calculer le total des restes en ignorant les décimales
                var totalReste = Object.values(customFinObj.data.lineToPay).reduce((pv, cv) => {
                    return pv + Math.floor(parseFloat(cv.reste)); // Ignorer les décimales
                }, 0);

                // Si le total des restes est inférieur au montant à financer, ajuster le montant à financer
                if (totalReste < totalToFin && totalReste != 0) {
                    totalToFin = totalReste;
                }

                customFinObj.data.lineToAdd = {};
                var lines = Object.keys(customFinObj.data.lineToPay);
                var totalDistributed = 0;

                // Première passe : distribuer la partie entière du montant
                $.each(customFinObj.data.lineToPay, function (index, value) {
                    var amount = Math.floor((value.reste / totalReste) * customFinObj.data.final.tlamount); // Ignorer les décimales
                    customFinObj.data.lineToPay[index].amount = amount;
                    totalDistributed += amount;
                });

                // Deuxième passe : distribuer le reste (différence entre totalToFin et totalDistributed)
                var remaining = totalToFin - totalDistributed;
                if (notEmpty(customFinObj.data.lineToPay)) {
                    for (var i = 0; i < remaining; i++) {
                        var index = lines[i % lines.length];
                        customFinObj.data.lineToPay[index].amount += 1;
                    }
                }

                // Gestion du financeur FTL (si défini)
                if (typeof customFinObj.data.final.double != "undefined" && customFinObj.data.final.double == true) {
                    var totalDistributedFTL = 0;

                    // Première passe : distribuer la partie entière pour FTL
                    $.each(customFinObj.data.lineToPay, function (index, value) {
                        var amount = Math.floor((value.reste / totalReste) * customFinObj.data.final.ftlamount); // Ignorer les décimales
                        customFinObj.data.lineToAdd[index] = {amount: amount};
                        totalDistributedFTL += amount;
                    });

                    // Deuxième passe : distribuer le reste pour FTL
                    var remainingFTL = customFinObj.data.final.ftlamount - totalDistributedFTL;
                    if (notEmpty(customFinObj.data.lineToAdd)) {
                        for (var i = 0; i < remainingFTL; i++) {
                            var index = lines[i % lines.length];
                            customFinObj.data.lineToAdd[index].amount += 1;
                        }
                    }
                }

                // Générer le HTML pour afficher les montants distribués
                var returnHtml = '';
                $.each(customFinObj.data.lineToPay, function (index, value) {
                    var valueAmount = 0;
                    if (notEmpty(value.amount)) {
                        valueAmount = value.amount;
                    }
                    returnHtml += `<div class="amountaffect-list" >
                        <div class="aap-secondary-color inline" style="font-size: 15px; font-weight : bold " >
                            <span class="aap-secondary-dark"> <i class="fa fa-hashtag aap-primary-color"></i> ${value.line} : </span>
                            <span class="inline pull-right"> ${valueAmount} <i class="fa fa-euro"></i> </span>
                            <span class="block aap-primary-color"><small> Reste :  ${value.reste} <i class="fa fa-euro"></i> </small> </span>
                        </div>
                    </div>`;
                });
                $('.amountaffect').html(returnHtml);

                // Mettre à jour les valeurs finales
                customFinObj.data.final.useramount = parseInt(totalToFin);
                customFinObj.data.final.finalamountearn = parseInt(customFinObj.data.final.amountearn) + customFinObj.data.final.useramount;
                customFinObj.data.final.userpercent = ((customFinObj.data.final.useramount * 100) / customFinObj.data.final.total) * 1;
                customFinObj.data.final.finalpercent = ((customFinObj.data.final.finalamountearn * 100) / customFinObj.data.final.total) * 1;
                customFinObj.data.final.finalreste = customFinObj.data.final.total - customFinObj.data.final.finalamountearn;

            },
            updateStat: function(customFinObj) {
                window.scroll(0, 0);
                $('.titre').html(customFinObj.data.final.titre);
                $('.description').html(customFinObj.data.final.description);
                if (typeof coForm_FormWizardParams.parentForm.custom != "undefined" &&
                    typeof coForm_FormWizardParams.parentForm.custom.financer != "undefined") {
                    if (typeof coForm_FormWizardParams.parentForm.custom.financer.text1 != "undefined") {
                        $('.financercustomtext1').html(coForm_FormWizardParams.parentForm.custom.financer.text1);
                    }
                    if (typeof coForm_FormWizardParams.parentForm.custom.financer.text2 != "undefined") {
                        $('.financercustomtext2').html(coForm_FormWizardParams.parentForm.custom.financer.text2);
                    }
                    if (typeof coForm_FormWizardParams.parentForm.custom.financer.text3 != "undefined") {
                        $('.financercustomtext3').html(coForm_FormWizardParams.parentForm.custom.financer.text3.replace('${percent}', '<span class="custom-fin-percent finalpercent">' + customFinObj.data.final.finalpercent + '</span><span class="custom-fin-percent">%</span>'));
                    }
                    if (typeof coForm_FormWizardParams.parentForm.custom.financer.text4 != "undefined") {
                        $('.financercustomtext4').html(coForm_FormWizardParams.parentForm.custom.financer.text4);
                    }
                    if (typeof coForm_FormWizardParams.parentForm.custom.financer.text3 != "undefined") {
                        $('.financercustomtext5').html(coForm_FormWizardParams.parentForm.custom.financer.text5);
                    }
                }

                $('.total').html(customFinObj.actions.addSpace(customFinObj.data.final.total));
                $('.financementnbr').html(typeof customFinObj.data.final.financer != "undefined" && customFinObj.data.final.financer != "" && customFinObj.data.final.financer != null ? Object.keys(customFinObj.data.final.financer).length : 0);
                $('.progress-bar.btn-aap-primary').css("width", (customFinObj.data.final.percent.toFixed(2) * 1) + "%");

                $('.finaluserpercent').html(customFinObj.data.final.userpercent.toFixed(2) + " <i class='fa fa-percent'>");
                $('.finaluseramount').html(customFinObj.actions.addSpace(customFinObj.data.final.useramount));

                $('.totalToPay').html(customFinObj.actions.addSpace(customFinObj.data.final.useramount));
                if (typeof customFinObj.data.final.ftlamount != 'undefined') {
                    $('.totalToPayTL').html(customFinObj.actions.addSpace(customFinObj.data.final.tlamount));
                    $('.totalToPayFTL').html(customFinObj.actions.addSpace(customFinObj.data.final.ftlamount));
                }
                $('.actpercent').html(customFinObj.data.final.percent.toFixed(2) * 1);
                $('.finalpercent').html(customFinObj.data.final.percent.toFixed(2) * 1);
                $('.amountearn').html(customFinObj.actions.addSpace(Math.round(customFinObj.data.final.amountearn)));
                $('.finalamountearn').html(customFinObj.actions.addSpace(customFinObj.data.final.finalamountearn));
                $('.actreste').html(customFinObj.actions.addSpace(customFinObj.data.final.reste));
                $('.finalreste').html(customFinObj.actions.addSpace(customFinObj.data.final.finalreste));

                $('.currentUsername').html(customFinObj.data.currentUser.name);
                $('.currentUseremail').html(customFinObj.data.currentUser.email);
                if (userConnected != null && customFinObj.data.currentUser.name != userConnected["name"]) {
                    $('.currentUserimg').attr("src", baseUrl + defaultImage);
                }

                customFinObj.actions.getFinancerList(customFinObj);
                customFinObj.actions.getPorterList(customFinObj);

            },
            addSpace: function(number) {
                if (notEmpty(number)) {
                    number = number.toString();
                    var pattern = /(-?\d+)(\d{3})/;
                    while (pattern.test(number))
                        number = number.replace(pattern, "$1 $2");
                } else {
                    number = 0;
                }
                return number;
            },
            getActionList: function(customFinObj, searchkey = "") {
                coInterface.showLoader(".co-popup-chooseline-list");
                var nameAction = document.getElementsByClassName("search-query").value;
                ajaxPost(null, baseUrl + "/" + moduleId + "/search/globalautocomplete", {
                    searchType: ["answers"],
                    filters: {
                        form: coForm_FormWizardParams.parentForm._id.$id
                    },
                    fields: ["answers"],
                    notSourceKey: true,
                    "answers.aapStep1.titre": nameAction
                }, function(res) {
                    if (notEmpty(res.results)) {
                        $(".co-popup-chooseline-list").html(``);
                        $.each(res.results, function(k, v) {
                            if (
                                notEmpty(v.answers) &&
                                notEmpty(v.answers.aapStep1) &&
                                notEmpty(v.answers.aapStep1.titre) &&
                                (searchkey == "" || v.answers.aapStep1.titre.toLowerCase().indexOf(searchkey) != -1)
                            ) {
                                var total = 0;
                                var totalEarn = 0;
                                var totalArray = [0, 0]
                                if (notEmpty(v.answers.aapStep1.depense)) {
                                    totalArray = v.answers.aapStep1.depense.reduce((pv, cv) => {
                                        var one = parseInt(pv[0]) + (Number.isInteger(parseInt(cv.price)) ? parseInt(cv.price) : 0);
                                        var two = parseInt(pv[1]);
                                        if (notEmpty(cv.financer)) {
                                            two = cv.financer.reduce((pv2, cv2) => {
                                                return parseInt(pv2) + (Number.isInteger(parseInt(cv2.amount)) ? parseInt(cv2.amount) : 0);
                                            }, pv[1]);
                                        }
                                        return [one, two];
                                    }, [0, 0]);
                                }
                                total = totalArray[0];
                                totalEarn = totalArray[1];

                                $(".co-popup-chooseline-list").append(`
                                <div class=" margin-10 amountaffect-list" >
                                    <h5 class=" col-md-9"> <a href="${baseUrl}/costum/co/index/slug/${costum.slug}#answer.answer.id.${v["_id"]["$id"]}.form.${coForm_FormWizardParams.parentForm["_id"]["$id"]}.step.aapStep3.input.financer.view.standalone"> <i class="fa fa-hashtag aap-primary-color"></i>${v.answers.aapStep1.titre} </a> </h5>
                                    <span class="col-md-3 ">
                                        <span class="aap-primary-color block bold">
                                            Reste : ${ customFinObj.actions.addSpace(totalEarn) } €
                                        </span>
                                        <span class="block">
                                            Total : ${ customFinObj.actions.addSpace(total) } €
                                        </span>
                                    </span>
                                </div>
                            `);
                            }
                        });
                    }
                }, null, null, {
                    async: true
                });
            },
            getFinancerList: function(customFinObj) {
                $('.fin-container').html('');
                var financerstext = "<div class='bold margin-top-5'>";
                var financersimg = "";
                var financertooltips = "";
                if (notEmpty(customFinObj.data.final.financer)) {
                    $.each(customFinObj.data.final.financer, function(index, value) {
                        financertooltips += value.name + ' \r\n ';
                    });
                    $('.fin-container').attr('data-original-title', financertooltips);
                    var count = 0;
                    var limit = 3;
                    var finId = "";
                    if (notEmpty(userConnected)) {
                        var finId = (userConnected["name"] + userConnected["email"]).replace(/[^A-Z0-9]+/ig, "");
                    }
                    /*if(notEmpty(customFinObj.data.final.financer[finId]) && customFinObj.data.currentUser.id == customFinObj.data.final.financer[finId]._id.id){
                        count++;
                        financerstext += "vous" + (Object.values(customFinObj.data.final.financer).length > 2 ? ", " : "") ;
                        financersimg += `<img src="${baseUrl + customFinObj.data.currentUser.profilPicture}" alt="${ customFinObj.data.currentUser.name }" class="custom-fin-mult-img">`;
                    }*/
                    if (Object.values(customFinObj.data.final.financer).length <= 3) {
                        limit = Object.values(customFinObj.data.final.financer).length;
                    }

                    for (i = count; i < limit; i++) {
                        if (notEmpty(Object.values(customFinObj.data.final.financer)[i].name)) {
                            financerstext += " " + Object.values(customFinObj.data.final.financer)[i].name;
                        } else {
                            financerstext += " " + Object.values(customFinObj.data.final.financer)[i].email;
                        }
                        if (notEmpty(Object.values(customFinObj.data.final.financer)[i].profilThumbImageUrl)) {
                            financersimg += `<img src="${baseUrl + Object.values(customFinObj.data.final.financer)[i].profilThumbImageUrl}" alt="${ customFinObj.data.currentUser.profilPicture }" class="custom-fin-mult-img">`
                        } else {
                            financersimg += `<img src="${baseUrl + defaultImage}" alt="" class="custom-fin-mult-img">`
                        }
                        if (i != 2) {
                            financerstext += ",";
                        }
                    }

                    if (Object.values(customFinObj.data.final.financer).length > 3) {
                        financerstext += ` et ${ Object.values(customFinObj.data.final.financer).length } autres personne`;
                        financersimg += `<span class="custom-fin-mult-img plustext"> +${ Object.values(customFinObj.data.final.financer).length - 3 } </span>`
                    }
                }
                financerstext += "</div>";
                $('.fin-container').html(financersimg + financerstext);
            },
            getPorterList: function(customFinObj) {
                $('.porter-container').html('');
                var financerstext = "<div class='bold margin-top-5'>";
                var financersimg = "";
                var portertooltips = "";
                if (notEmpty(customFinObj.data.final.porter)) {
                    $.each(customFinObj.data.final.porter, function(index, value) {
                        if(value != null)
                        portertooltips += value.name + ' / ';
                    });
                    $('.porter-container').attr('data-original-title', portertooltips);
                    var count = 0;
                    var limit = 3;
                    if (notEmpty(customFinObj.data.final.porter[userId]) && customFinObj.data.currentUser.id == customFinObj.data.final.porter[userId]) {
                        count++;
                        financerstext += "vous" + (Object.values(customFinObj.data.final.financer).length > 2 ? ", " : "");
                        financersimg += `<img src="${baseUrl + customFinObj.data.currentUser.profilPicture}" alt="${ customFinObj.data.currentUser.name }" class="custom-fin-mult-img">`;
                    }
                    if (Object.values(customFinObj.data.final.porter).length <= 3) {
                        limit = Object.values(customFinObj.data.final.porter).length;
                    }

                    for (i = count; i < limit; i++) {
                        if (notEmpty(Object.values(customFinObj.data.final.porter)[i].name)) {
                            financerstext += " " + Object.values(customFinObj.data.final.porter)[i].name;
                        } else {
                            financerstext += " " + Object.values(customFinObj.data.final.porter)[i].email;
                        }
                        if (notEmpty(Object.values(customFinObj.data.final.porter)[i].profilThumbImageUrl)) {
                            financersimg += `<img src="${baseUrl + Object.values(customFinObj.data.final.porter)[i].profilThumbImageUrl}" alt="${ customFinObj.data.currentUser.profilPicture }" class="custom-fin-mult-img">`
                        } else {
                            financersimg += `<img src="${baseUrl + defaultImage}" alt="" class="custom-fin-mult-img">`
                        }
                        if (i != 2) {
                            financerstext += ",";
                        }
                    }

                    if (Object.values(customFinObj.data.final.porter).length > 3) {
                        financerstext += ` et ${ Object.values(customFinObj.data.final.financer).length - 3 } autres personne`;
                        financersimg += `<span class="custom-fin-mult-img plustext"> +${ Object.values(customFinObj.data.final.porter).length - 3 } </span>`
                    }
                }
                financerstext += "</div>";
                $('.porter-container').html(financersimg + financerstext);
            },
            addViewmore: function(customFinObj) {

            }
        }
    };

    function roundToNearest(value) {
        const factor = Math.pow(value, 3);
        const r1 = (Math.round(value / 0.05) * 0.05);
        //return Math.round(r1 * factor) / factor;
        return r1;
    }

    jQuery(document).ready(function() {
        coInterface.showCostumLoader(".co-popup-cofinance-loader-section")
        customFinObj.actions.open(customFinObj);

        $(".ap-starter-btn-changeProject").off().click(function() {
            $(".co-popup-chooseline-container").show();
            customFinObj.actions.getActionList(customFinObj, "");
        });

        $(".search-query").off().on('keyup ', function() {
            var val = $.trim($(this).val().toLowerCase());
            customFinObj.actions.getActionList(customFinObj, val);
        });

        $(".ap-starter-btn-sheet").off().click(function() {
            aapObj.common.modalProposalDetail(aapObj, coForm_FormWizardParams.answer._id.$id, true);
            htmlExists("[href='#proposition-evaluation']", function(sel) {
                sel.remove();
            })
            htmlExists(".pdf-download", function(sel) {
                sel.remove();
            })
            $(".portfolio-modal .modal-content").css({
                cssText: `
                        border-radius: 10px;
                        overflow-y: unset;
                        `
            });
            return false;
        });

        $(".close-co-popup-chooseline-header").off().click(function() {
            $(".co-popup-chooseline-container").hide();
        });

        let currentCfhsIndex = 0;
        const btns = document.querySelectorAll(".cf-header-btn");
        const slideRow = document.querySelector(".chfm-slide-row");
        const main = document.querySelector(".custom-financer-header-mobile");
        let currentIndex = 0;

        var startX,
            startY,
            dist,
            threshold = 50,
            allowedTime = 2000,
            elapsedTime,
            startTime;

        function handleswipe(direction) {
            if (direction == -1) {
                currentIndex = currentIndex + 1;
                updateSlide();
            } else if (direction == 1) {
                currentIndex = currentIndex - 1;
                updateSlide();
            }
        }

        slideRow.addEventListener('touchstart', function(e) {
            var touchobj = e.changedTouches[0];
            dist = 0;
            startX = touchobj.pageX;
            startY = touchobj.pageY;
            startTime = newDate.getTime();
            e.preventDefault();
        }, false);

        slideRow.addEventListener('touchmove', function(e) {
            e.preventDefault();
        }, false);

        slideRow.addEventListener('touchend', function(e) {
            var touchobj = e.changedTouches[0];
            dist = touchobj.pageX - startX;
            elapsedTime = new Date().getTime() - startTime;
            var direction = "";
            if (
                //elapsedTime <= allowedTime &&
                Math.abs(dist) >= threshold &&
                Math.abs(touchobj.pageY - startY) <= 100 &&
                !(Math.sign(dist) == 1 && currentIndex == 0) &&
                !(Math.sign(dist) == -1 && currentIndex == 2)
            ) {
                handleswipe(Math.sign(dist));
            }

            e.preventDefault();
        }, false);

        function updateSlide() {
            const mainWidth = main.offsetWidth;
            const translateValue = currentIndex * -mainWidth;
            slideRow.style.transform = `translateX(${translateValue}px)`;

            btns.forEach((btn, index) => {
                btn.classList.toggle("active", index === currentIndex);
            });
        }

        btns.forEach((btn, index) => {
            btn.addEventListener("click", () => {
                currentIndex = index;
                updateSlide();
            });
        });

        window.addEventListener("resize", () => {
            updateSlide();
        });
    });
</script>

