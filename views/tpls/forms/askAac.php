<div id="depositMainContainer" class="col-xs-12 no-padding deposit-main-container">
    <style>
        .deposit-main-container .content-title {
            display: flex;
            justify-content: space-between;
        }
        .deposit-main-container .content-title .title {
            font-size: 20px;
            font-weight: 600;
            color: #333;
        }
        #myOthersCommuns {
            flex-direction: row;
            display: flex;
            flex-wrap: wrap;
            gap: 1em;
            justify-content: center;
        }
        .deposit-main-container .card {
            height: 254px;
            padding: .8em;
            border-radius: 8px;
            position: relative;
            overflow: visible;
            box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
        }

        .deposit-main-container .card-img {
            background-color: #ffcaa6;
            height: 40%;
            width: 100%;
            border-radius: .5rem;
            transition: .3s ease;
        }

        .deposit-main-container .card-info {
            padding-top: 10%;
        }

        .deposit-main-container svg {
            width: 20px;
            height: 20px;
        }

        .deposit-main-container .card-footer {
            width: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
            padding-top: 10px;
            border-top: 1px solid #ddd;
        }

        /*Text*/
        .deposit-main-container .text-title {
            font-weight: 600;
            font-size: 1em;
            line-height: 1.5;
            white-space: nowrap;
            text-overflow: ellipsis;
            overflow: hidden;
        }

        .deposit-main-container .text-body {
            font-size: .9em;
            padding-bottom: 10px;
            color: #8f918f;
            font-weight: 500;
        }

        /*Button*/
        .deposit-main-container .card-button {
            border: 1px solid #252525;
            display: flex;
            padding: .3em;
            cursor: pointer;
            border-radius: 50px;
            transition: .3s ease-in-out;
        }

        .deposit-main-container .card-button:hover {
            border: 1px solid #e6e6e6;
            background-color: #e6e6e6;
        }

    </style>
    <div class="col-xs-12">
        <div class="col-xs-12 no-padding content-title d-flex justify-content-between">
            <span class="title">Choisir parmi vos communs existants</span>
            <span class="cursor-pointer generate-new-commun btn btn-default">
                <i class="fa fa-plus"></i>
                <span class="hidden-xs">Déposer un nouveau commun</span>
            </span>
        </div>
        <div class="col-xs-12 no-padding" id="myOthersCommuns">

        </div>
    </div>
</div>
<script type="text/javascript">
    const _thisDepositPhp = {
        myOthersCommuns: JSON.parse(JSON.stringify(<?= isset($myOthersCommuns) ? json_encode($myOthersCommuns) : json_encode([]) ?>)),
        parentForm: JSON.parse(JSON.stringify(<?= isset($parentForm) ? json_encode($parentForm) : json_encode([]) ?>))
    }
    $(function () {
        $.each(_thisDepositPhp.myOthersCommuns, function(key, commun) {
            $("#myOthersCommuns").append(`
                <div class="card col-lg-2 col-sm-6 col-xs-10" id="commun${ key }">
                    <div class="card-img">
                        <img src="${ commun.image ? commun.image : defaultImage }" alt="${ (commun.name ? commun.name : "commun").substring(0,1) }" style="width: 100%; height: 100%; object-fit: cover; border-start-start-radius: 8px; border-start-end-radius: 8px">
                    </div>
                    <div class="card-info">
                        <p class="text-title">${ commun.name }</p>
                        <p class="text-body">Déposé le ${ 
                            new Date(commun.created * 1000).toLocaleString('fr', {
                                day: 'numeric', month: 'numeric', year: 'numeric'
                            }) }
                        </p>
                    </div>
                    <div class="card-footer">
                        <div class="card-button connect-this-commun">
                            <svg xmlns="http://www.w3.org/2000/svg" width="1.2em" height="1.2em" viewBox="0 0 24 24">
                                <path fill="currentColor" d="m12.11 15.39l-3.88 3.88a2.47 2.47 0 0 1-3.5 0a2.46 2.46 0 0 1 0-3.5l3.88-3.88a1 1 0 1 0-1.42-1.42l-3.88 3.89a4.48 4.48 0 0 0 6.33 6.33l3.89-3.88a1 1 0 0 0-1.42-1.42m-3.28-.22a1 1 0 0 0 .71.29a1 1 0 0 0 .71-.29l4.92-4.92a1 1 0 1 0-1.42-1.42l-4.92 4.92a1 1 0 0 0 0 1.42M21 18h-1v-1a1 1 0 0 0-2 0v1h-1a1 1 0 0 0 0 2h1v1a1 1 0 0 0 2 0v-1h1a1 1 0 0 0 0-2m-4.19-4.47l3.88-3.89a4.48 4.48 0 0 0-6.33-6.33l-3.89 3.88a1 1 0 1 0 1.42 1.42l3.88-3.88a2.47 2.47 0 0 1 3.5 0a2.46 2.46 0 0 1 0 3.5l-3.88 3.88a1 1 0 0 0 0 1.42a1 1 0 0 0 1.42 0" />
                            </svg>
                            <span class="bs-ml-2">Faire le lien</span>
                        </div>
                    </div>
                </div>
            `)
        })
        if ($("#myOthersCommuns .text-title").length > 0) {
            $("#myOthersCommuns .text-title").each(function() {
                const currentElem = $(this);
                if (currentElem[0].scrollWidth - 1 > currentElem[0].clientWidth) {
                    currentElem.tooltip({
                        title: currentElem.text(),
                        placement: "top",
                        trigger: "hover",
                        container: "#depositMainContainer",
                        template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner large custom-bg"></div></div>',
                    });
                }
                if (typeof coInterface.eventTohideTooltip != "undefined" && currentElem[0]) {
                    currentElem[0].removeEventListener("click", coInterface.eventTohideTooltip)
                    currentElem[0].addEventListener("click", coInterface.eventTohideTooltip)
                }
            });
            // $("#myOthersCommuns .connect-this-commun").tooltip({
            //     title: "Faire le lien",
            //     placement: "top",
            //     trigger: "hover",
            //     container: "#depositMainContainer",
            //     template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner large custom-bg"></div></div>',
            // });
        }
        $("#depositMainContainer .generate-new-commun").on("click", function () {
            if (notEmpty(_thisDepositPhp.parentForm)) {
                coInterface.showCostumLoader("#depositMainContainer")
                ajaxPost(
                    "#depositMainContainer",
                    baseUrl + "/survey/answer/answer/id/new/form/" + _thisDepositPhp.parentForm._id.$id,
                    {
                        aacAsk: false,
                    },
                    function () {}
                )
            }
        })
        $("#myOthersCommuns .connect-this-commun").on("click", function () {
            const communId = $(this).closest(".card").attr("id").replace("commun", "");
            if (notEmpty(_thisDepositPhp.parentForm)) {
                dataHelper.path2Value({
                    id: communId,
                    collection: "answers",
                    path: "links.aacForm."+_thisDepositPhp.parentForm._id.$id,
                    value: true
                })
            }
            coInterface.showCostumLoader("#depositMainContainer")
            ajaxPost(
                "#depositMainContainer",
                baseUrl + "/survey/answer/answer/id/" + communId,
                {},
                function () {}
            )
        })
    });
</script>