<?php if(!$isajax){ ?>
    <style>
        :root {
            /* --aap-primary-color: #9BC125;
            --aap-primary-dark: #79981C; */
            --aap-tertiary-color: #6c757da8;

        }
        .wizard-banner #col-banner,#form_banner_uploader-contentBanner,#form_banner_uploader-contentBanner img{
            min-height: 300px;
        }
        .wizard-banner #col-banner a.fileupload-new{
            background-color: #7da53d;
            /* display: none; */
        }
        .wizard-banner #col-banner .fileupload-new:hover,#col-banner .fileupload-new-text:hover{
            color: #fff;
        }
        .wizard-banner #col-banner .fileupload-new-text{
            position: absolute;
            top: 43px;
            left: 10px;
            z-index: 1000;
            background-color: #7da53d;
            display: none;
        }
        .wizard-banner #col-banner:hover .fileupload-new-text{
            display: initial;
        }
        .wizard-banner .form-banner-text{
            /*position: absolute;
            bottom: 0;
            z-index: 1;
            width: 100%;
            background-color: #00000047;
            color: #fff;
            display: flex;
            justify-content: center;
            align-items: center;
            text-align: justify;
            padding: 0 40px;
            line-height: 1.5 !important;*/
        }
        .form-banner-text a{
            color:#7da53d;
        }
        .wizard-banner #form_banner_uploader-contentBanner .form-banner img{
            object-fit: cover;
            margin-bottom: 20px;
        }
        /* .btn-aap-primary {
            color: white !important;
            font-weight: 700;
            transition: all .3s ease;
            word-spacing: 5px;
        }
        .btn-aap-primary {
            background-color: var(--clr_var);
            border-color: var(--clr_var);
        }
        .btn-aap-primary:hover {
            background-color: var(--clr_dark_var);
            border-color: var(--clr_dark_var);
        } */

        .btn-aap-primary[disabled] {
            background-color: var(--aap-tertiary-color);
            border-color: var(--aap-tertiary-color);
        }

        .mb-2 {
            margin-bottom: 0.5rem !important;
        }
    </style>

    <?php if(!empty($parentForm["useBannerImg"])){  ?>
    <div class="col-md-4 col-xs-4">
        <h1><?= $parentForm["name"] ?></h1>
    </div>
    <?php }  ?>
    <div class="col-md-8 col-xs-8 margin-top-20 no-padding wizard-banner">
        <?php if(!empty($parentForm["useBannerImg"])){  ?>
            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 text-left no-padding" id="col-banner">
                <?php  if(!empty($parentForm["useBannerImg"])){
                    $parentForm["collection"] = Form::COLLECTION;
                    if($mode == 'fa' && !empty($canAdminAnswer)){
                        echo $this->renderPartial("co2.views.element.modalBanner", array(
                                "id" => "form_banner_uploader",
                                "edit" => $canAdminAnswer,
                                "openEdition" => $canAdminAnswer,
                                "profilBannerUrl"=> @$element["profilBannerUrl"],
                                "element"=> array(
                                    "_id" => $parentForm["_id"],
                                    "name" => $parentForm["name"],
                                    "collection" => $parentForm["collection"],
                                ),
                            ));
                    }
                    ?>
                    <div id="form_banner_uploader-contentBanner" class="col-md-12 col-sm-12 col-xs-12 no-padding">
                        <div class="form-banner">
                            <?php if (@$parentForm["profilBannerUrl"] && !empty($parentForm["profilBannerUrl"])){
                                    $imgHtml='<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" alt="'.Yii::t("common","Banner").'"
                                        src="'.Yii::app()->createUrl('/'.$parentForm["profilBannerUrl"]).'">';
                                    if (@$parentForm["profilRealBannerUrl"] && !empty($parentForm["profilRealBannerUrl"])){
                                        $imgHtml='<a href="'.Yii::app()->createUrl('/'.$parentForm["profilRealBannerUrl"]).'"
                                                    class="thumb-info"  
                                                    data-title="'.Yii::t("common","Cover image of")." ".$parentForm["name"].'"
                                                    data-lightbox="all">'.
                                                    $imgHtml.
                                                '</a>';
                                    }
                                    echo $imgHtml;
                                }else{
                                    if(isset($pageConfig["banner"]["img"]) && !empty($pageConfig["banner"]["img"]))
                                        $url=Yii::app()->getModule( "costum" )->assetsUrl.$pageConfig["banner"]["img"];
                                    else if(in_array($parentForm["collection"], [Event::COLLECTION, Project::COLLECTION, Person::COLLECTION, Organization::COLLECTION]))
                                        $url=Yii::app()->theme->baseUrl.'/assets/img/background-onepage/'.$parentForm["collection"].'.png';
                                    else
                                        $url=Yii::app()->theme->baseUrl.'/assets/img/background-onepage/connexion-lines.jpg';

                                    echo '<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" alt="'.Yii::t("common","Banner").'"
                                        src="'.$url.'">';
                            } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
    <div class="col-xs-12">
        <?php //if(!isset($parentForm["useBannerImg"]) || (isset($parentForm["useBannerImg"]) && ($parentForm["useBannerImg"]) == false)){  ?>
            <!-- <div class="col-md-12 col-xs-12">
                <h1 class="header-title"><?= $parentForm["name"] ?></h1>
            </div> -->
        <?php //}  ?>
        <?php  if(!empty($parentForm["useBannerText"])){  ?>
            <div class="form-banner-text" ><?= @$parentForm["bannerText"] ?></div>
            <?php if($mode == 'fa' && !empty($canAdminAnswer)){ ?>
                <a href="javascript:;" class="edit-banner-text btn btn-aap-primary mb-2">
                    Modifier le texte
                </a>
            <?php } ?>
        <?php } ?>
    </div>
    <script>
        $(function(){
            if(location.hash?.includes("standalone.true")){
                $(".body-section,.banner-section").css("padding","0");
                $("#coFormbody").removeClass('margin-top-20')
            }
            var originalText = $(".form-banner-text").text();
            var parsedText = curlyBrace(originalText);
            $(".form-banner-text").text(parsedText);
            var descHtml = dataHelper.markdownToHtml($(".form-banner-text").html(),{
                parseImgDimensions:true,
                simplifiedAutoLink:true,
                strikethrough:true,
                tables:true,
                openLinksInNewWindow:true
            });
            $(".form-banner-text").html(descHtml);

            <?php if($mode == 'fa' && !empty($canAdminAnswer)){ ?>
                $('.edit-banner-text').off().on('click',function(){
                    var bannerText = {
                        jsonSchema: {
                            title: "Modifier le texte du banner",
                            description: "",
                            icon: "fa-question",
                            properties : {
                                bannerText : {
                                    inputType : "textarea",
                                    markdown:true,
                                    label : "Texte",
                                    value : <?= isset($parentForm["bannerText"]) && $parentForm["bannerText"] ? json_encode($parentForm["bannerText"]) : json_encode("") ?>
                                }
                            },
                            onLoads : {
                                onload: function(){

                                }
                            },
                            beforeBuild : function(){
                            },
                            save: function (formData) {
                                mylog.log('save tplCtx formData', formData)
                                delete formData.collection
                                var tplCtx = {
                                    id: "<?= (string)$parentForm["_id"] ?>",
                                    collection: "forms",
                                    value: formData,
                                    format: true,
                                    path: "allToRoot",
                                };

                                if (typeof tplCtx.value == "undefined")
                                    toastr.error('value cannot be empty!');
                                else {
                                    dataHelper.path2Value(tplCtx, function (prms) {
                                        if(prms.result)
                                            toastr.success(trad.saved);
                                        urlCtrl.loadByHash(location.hash);
                                    });
                                }

                            }
                        }
                    };

                    dyFObj.openForm(bannerText);
                })
            <?php } ?>
        })
    </script>
<?php } ?>
