<?php
$ignore = array('_file_', '_params_', '_obInitialLevel_' ,'ignore');
$params = array_diff_key(get_defined_vars() + array_flip($ignore), array_flip($ignore));
!isset($requiredInputs) ? $requiredInputs = array() : "";
!isset($stepOblList) ? $stepOblList = array() : "";
if(!empty($step["sections"])){
?>
<div class="col-xs-12 no-padding row">
        <div class="coform-nav col-xs-12 col-lg-2 mt-3 mb-5 my-lg-0 mr-lg-4 mb-lg-5 px-sm-3 text-body-secondary">
            <div class="schu-btn-sticky d-flex justify-content-center">
                <button class="btn justify-content-between p-md-0 mb-2 mb-md-0 text-decoration-none visible-xs visible-sm visible-md" type="button" data-schu-toggle="collapse" data-target="#tocContents<?= isset($step["_id"]) ? $step["_id"] : "" ?>" aria-expanded="false" aria-controls="tocContents" style="width: 90vw;">
                    <span class="pull-left">Sections</span>
                    <i class="fa fa-chevron-down pull-right"></i>
                </button>
            </div>
            <div class="schu-sticky d-none-xs d-none-sm d-none-md section-nav-body coform-visible-lg bg-white" id="tocContents<?= isset($step["_id"]) ? $step["_id"] : "" ?>">
                <strong class="hidden-xs hidden-sm hidden-md visible-lg h6 my-2 mx-3 section-lg-header" style="font-size: 1.8rem;">Sections</strong>
                <hr class="hidden-xs hidden-sm hidden-md visible-lg my-2 ml-3 mr-5 section-lg-header-separator">
                <nav id="TableOfContents visible-lg">
                    <ul>
                        <?php
                            $stepIteration = 0;
                            foreach ($step["sections"] as $sectionId => $section){
                        ?>
                            <li class="<?= $stepIteration == 0 ? 'active' : '' ?>"><a href="javascript:;" class="sectionScroll" data-target="#titleSeparator<?= $sectionId ?>"><?= ucfirst(strtolower($section["label"])) ?></a></li>
                        <?php
                            $stepIteration++;
                        }
                        ?>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- <details class="coformSectionMenu"> -->
            <!-- <summary class="coformSectionMenuSummary"></summary> -->
            <!-- <nav class="coformSectionMenuNav"> -->
            <!-- </nav> -->
        <!-- </details> -->
<?php
}
?>
<?php
if(!empty($step["sections"])){
?>
    <div class="px-lg-0 px-sm-4 col-lg-9 col-xs-12">
<?php } ?>
<div class="hide step-second-container coform-chart-container" id="stepContainer<?= isset($step["step"]) ? $step["step"] : "" ?>">
    <span class="cursor-pointer switch-to-step"><i class="fa fa-times fa-2x"></i></span>
    <div class="second-container-body col-xs-12 no-padding"></div>
</div>
<div class="questionList" id="questionList<?= isset($step["step"]) ? $step["step"] : "" ?>" data-id="<?= isset($step["step"]) ? $step["step"] : "" ?>" data-ids="<?= isset($step["_id"]) ? $step["_id"] : "" ?>">
    <?php
    if (!empty($step["inputs"])){
        if (isset($step["hasMultiEval"]) && $step["hasMultiEval"] == true) {
            echo "
                    <div class='eval-multi no-padding'>
                        <span class='cursor-pointer btn-multiEval' data-answerid='" . (isset($answer["_id"]) ? (string) $answer["_id"] : "") . "' data-input-paramsid='" . (string) $parentForm['_id'] . "' ><i class='fa fa-line-chart fa-2x'></i></span>
                    </div>
                ";
        }

        //Sort inputs
        $orderInputs = [];
        $orderInputsKeys = [];

        foreach ( $step["inputs"] as $key => $input) {
            if(isset($input["position"])){
                $orderInputs[(int)$input["position"]] = $input;
                $orderInputsKeys[(int)$input["position"]] = $key;
            }
        }

        $inV = $step["inputs"];

        if ($isAap) {
            foreach ($step["inputs"] as $key => $input) {
                if (!empty($input['positions'][$parentForm['_id']])) {
                    unset($step["inputs"][$key]);
                } else {
                    unset($inV[$key]);
                }
            }
        } else {
            foreach ($step["inputs"] as $key => $input) {
                if (!isset($input["position"])) {
                    unset($step["inputs"][$key]);
                } else {
                    unset($inV[$key]);
                }
            }
        }


        if(!function_exists('sortByPosOrder'))
        {
            if (!$isAap){
                function sortByPosOrder($a, $b) {
                    return (int)$a['position'] - (int)$b['position'];
                }
            }
        }

        if ($isAap) {

            if (empty($sorter)){
                $sorter = new Answer();
                $sorter->parentFo = $parentForm;
            }

            uasort($step["inputs"], array($sorter, 'sortByPosOrder'));
        } else {
            uasort($step["inputs"], sortByPosOrder);
        }

        $step["inputs"] = array_merge($step["inputs"], $inV);

        foreach ($step["inputs"] as $inputId => $input) {
            if(isset($input["hide"]) && $input["hide"] == true){
                $conditionalHide = "hide";
            } else {
                $conditionalHide = "";
            }
            $tpl = "";
            $input["docinputsid"] = isset($step["_id"]) ? $step["_id"] : $inputId;
            if(!empty($input["type"]) && in_array($input["type"], ["textarea","markdown","wysiwyg"])) {
                $tpl = "tpls.forms.textarea";
            } else if(empty($input["type"]) || in_array($input["type"], ["text","button","color","date","datetime-local","email","image","month","number","radio","range","tel","time","url","week","tags","hidden"])) {
                $tpl = "tpls.forms.text";
            } else if(in_array( "multiDecide", explode( ".",  $input["type"]) ) && isset($parentForm["inputConfig"]["multiDecide"])  ){
                $kunikDecide = explode( ".", $parentForm["inputConfig"]["multiDecide"]);
                $kunikDecideName = $kunikDecide[count($kunikDecide) - 1];
                $step["inputs"][$kunikDecideName] = $input;
                $tpl = $step["inputs"][$kunikDecideName]["type"] = $input["type"] = $parentForm["inputConfig"]["multiDecide"];
                $input["border"] = false;
            }else {
                $tpl = $input["type"];
            }

            if(!empty($input["type"]) && stripos( $input["type"] , "tpls.forms.cplx" ) !== false ) {
                $params["saveOneByOne"] = false;
            }else{
                $params["saveOneByOne"] = true;
            }
            $kunikT = explode( ".", @$input["type"]);
            $params["keyTpl"] = ( count($kunikT)>1 ) ? $kunikT[ count($kunikT)-1 ] : @$input["type"];
            $params["kunik"] = $params["keyTpl"].$inputId;
            $params["formId"] = $step["step"];
            $params["form"] = $step;
            $params["key"] = $inputId;
            $params["type"] = @$input["type"];
            $params["label"] = @$input["label"];
            $params["info"] = @$input["info"];
            $params["inpClass"] = " ";
            $params["placeholder"] = @$input["placeholder"];
            $params["titleColor"] = (isset($this->costum["colors"]["pink"])) ? $this->costum["colors"]["pink"] : "#000";
            $params["answerPath"] = "answers.".$params["formId"].".".$params["key"].".";
            $params["canEditForm"] = $canAdminAnswer && $mode == "fa";
            if(!empty($answer["answers"][$params["formId"]][$params["key"]])) {
                $params["answers"] = $answer["answers"][$params["formId"]][$params["key"]];
            }else{
                $params["answers"] = null;
            }
            $count = 0;
            if($mode == "w" && isset($input["activeComments"]) && $input["activeComments"] == true) {
                $where = array("path" => (isset($params["form"]["formParent"]) ? $params["form"]["formParent"] : ""). "." .(isset($params["form"]["_id"]) ? (string) $params["form"]["_id"] : ""). '.' . (isset($answer["_id"]) ? (string) $answer["_id"] : "") . '.' .$inputId.".question_".$inputId.".comment");
                $count = PHDB::count(Comment::COLLECTION, $where);
            }
            $params["editQuestionBtn"] = isset($input['isRequired']) && $input["isRequired"] == true ? " * " : "";
            $params["editQuestionBtn"] .= $mode == "w" && isset($input["activeComments"]) && $input["activeComments"] == true ? " <a class='btn btn-xs btn-tertiaire btn-question-comment' href='javascript:;' data-collection='".Form::INPUTS_COLLECTION."' data-count='".$count."' data-answer='". (isset($answer["_id"]) ? (string) $answer["_id"] : "") ."'  data-id='".(isset($params["form"]["_id"]) ? (string) $params["form"]["_id"] : "")."' data-inputkey='".$inputId."' data-key='question_".$inputId."' data-parentid='".(isset($params["form"]["formParent"]) ? $params["form"]["formParent"] : "")."'><i class='fa fa-comments fa-2x'></i><span class='comment-pastil'>".$count."<span></a>" : ""; 
            if($params["canEditForm"]) {
                $params["isAap"] ? (
                    $params["editQuestionBtn"] .= ' <a class="btn btn-xs btn-danger descSec'.$inputId.' editQuestionaap" href="javascript:;" data-isrequired ="'.(isset($input['isRequired']) ? json_encode($input['isRequired']) : "false").'" data-position="'.(isset($input['position']) ? str_replace('"', "'", $input['position']) : "").'" data-positions="'.(isset($input['positions']) ? str_replace('"', "'", json_encode($input['positions'])) : "").'" data-collection="'.Form::INPUTS_COLLECTION.'" data-key="'.$inputId.'" data-path="inputs.'.$inputId.'" data-id="'.(isset($params["form"]["_id"]) ? (string) $params["form"]["_id"] : "").'" data-parentid="'.(isset($params["form"]["formParent"]) ? $params["form"]["formParent"] : "").'" data-formid="'. $params["formId"] .'" data-label="'.(isset($input["label"]) ? $input["label"] : "").'" data-info="'.(isset($input["info"]) ? $input["info"] : "").'" data-placeholder="'.(isset($input["placeholder"]) ? $input["placeholder"] : "").'" data-activemultieval="'. json_encode($input["activeMultieval"] ?? 'false' ) .'" data-evaluationkey="'. (isset($input["evaluationKey"]) ? $input["evaluationKey"] : 'A')  .'" data-activecomments="'. json_encode($input["activeComments"] ?? 'false' ) .'" data-type="'.(isset($input["type"]) ? $input["type"] : "text").'"><i class="fa fa-pencil"></i></a>'.
                    " <a class='btn btn-xs btn-danger deleteLine' href='javascript:;'  data-id='".(isset($params["form"]["_id"]) ? (string) $params["form"]["_id"] : "")."' data-collection='".Form::INPUTS_COLLECTION."' data-inputkey='".$inputId."' data-key='question_".$inputId."' data-path='inputs.".$inputId."' data-parentid='".(isset($params["form"]["formParent"]) ? $params["form"]["formParent"] : "")."'><i class='fa fa-trash'></i></a>"
                ) : ($params["editQuestionBtn"] = "");
            }
            if(!isset($params["input"]))
                $params["input"] = $input;
            if(isset($input["isRequired"]) && json_encode($input["isRequired"]) == "true") {
                $requiredInputs[$inputId] = $input;
                if(!function_exists('toStepArrayId')) {
                    function toStepArrayId($n)
                    {
                        if (isset($n["step"]))
                            return ($n["step"]);
                    }
                }
                $stepArrayId = array_search($step["step"] , array_map('toStepArrayId' , array_values($steps))) ;
                $stepOblList[$stepArrayId][$inputId] = $input;
            }
                $isToShowInForm = true;
                if (isset($input["isAdminOnly"]) && $input["isAdminOnly"] == true && !filter_var($params["canAdminAnswer"], FILTER_VALIDATE_BOOL)) {
                    $isToShowInForm = false;
                }
                if(!(isset($input["hideInForm"]) && $input["hideInForm"] == true) && $isToShowInForm) {
            ?>
                    <div id="question_<?= $inputId ?>" style="padding: 0 1% !important; margin-bottom: 10px;" class="<?= $input["moreClass"] ?? "" ?> coforminput questionBlock col-xs-12 ui-corner-all no-padding questionBlock " data-id="<?= (isset($params["form"]["_id"]) ? (string) $params["form"]["_id"] : "") ?>" data-path="inputs.<?= $inputId ?>.position" data-key="<?= $inputId ?>" data-docinputsid="$stepValidationInput['docinputsid']" data-form="<?= $step["step"] ?>">
                        <?php
                            echo $this->renderPartial( "survey.views.".$tpl , $params , true );
                            if(isset($params["input"]))
                                unset($params["input"]);
                        ?>
                    </div>
            <?php
                }
        }
    } else if($mode == 'fa' && isset($canAdminAnswer) && $canAdminAnswer) {
        echo '<span class="col-md-12 bg-txt DnDPlaceholder">'.Yii::t("survey", "Drop your question here").'</span>';
    }
    ?>
</div>

<?php
    if($mode == 'fa' && isset($canAdminAnswer) && $canAdminAnswer) {
        if($isAap) {
?>
            <div class="text-center">
                <a href="javascript:;" class="addQuestion btn btn-danger" data-collection="<?= Form::INPUTS_COLLECTION ?>" data-id="<?= isset($step["_id"]) ? $step["_id"] : "" ?>" data-form="<?= isset($step["step"]) ? $step["step"] : "" ?>" data-formparentid="<?= isset($parentForm["_id"]) ? $parentForm["_id"] : "" ?>"><i class="fa fa-plus"></i> <?php echo isset($parentForm["type"]) && $parentForm["type"] == 'aapConfig' ?  Yii::t("survey","Add a Required Question") : Yii::t("survey","Add a specific question") ?></a>
            </div>
<?php
        } else {
?>
            <div class="text-center">
                <a href="javascript:;" class="addQuestion btn btn-danger" data-collection="<?= Form::COLLECTION ?>" data-form="<?= isset($step["step"]) ? $step["step"] : "" ?>"  data-id="<?= isset($step["_id"]) ? $step["_id"] : "" ?>" data-formparentid="<?php echo (string)$parentForm['_id'] ?>" ><i class="fa fa-plus"></i> <?php echo Yii::t("survey","Add Question") ?></a>
            </div>
<?php
        }
    }
if(!empty($step["sections"])){
?>
    </div>
</div>
<?php
}
?>

<script type="text/javascript">
    $('.btn-question-comment').on('click', function(){
        if ($("#dialogContent").find(this).length > 0) {
            $('#modal-preview-comment').addClass('comment-modal-aac')
        }
        var self = $(this)
        var id = self.data('id')
        var collection = self.data('collection')
        var parentid = self.data('parentid')
        var inputkey = self.data('inputkey')
        var key = self.data('key')
        var titre = typeof coForm_FormWizardParams != "undefined" && coForm_FormWizardParams.steps?.[id]?.inputs?.[inputkey]?.label ? coForm_FormWizardParams.steps?.[id]?.inputs?.[inputkey]?.label : self.parent().text()
        const thisAnswerId = self.data("answer");
        const commentPath = `${parentid}.${id}.${thisAnswerId}.${inputkey}.${key}.comment`;
        commentObj.openPreview('forms', parentid, commentPath, titre)
        commentObj.afterSaveCallBack = function () {
            if ($("#question_" + inputkey + " .btn-question-comment").length > 0) {
                ajaxPost(
                    null,
                    baseUrl + '/' + moduleId + "/comment/countcommentsfrom",
                    {
                        "type": "forms",
                        "id": parentid,
                        "path": commentPath
                    },
                    function (data) {
                        $("#question_" + inputkey + " .btn-question-comment").attr("data-count", data.count).find(".comment-pastil").text(data.count)
                    }
                );
            }
        }
        commentObj.closePreview = function(){
            ajaxPost(
                null,
                baseUrl + '/' + moduleId + "/comment/countcommentsfrom",
                {
                    "type": "forms",
                    "id": parentid,
                    "path": commentPath
                },
                function (data) {
                    $("#question_" + inputkey + " .btn-question-comment").attr("data-count", data.count).find(".comment-pastil").text(data.count)
                }
            );
            $('#modal-preview-comment').removeClass('comment-modal-aac')
            $(".main-container").off();
            $("#modal-preview-comment").css("display", "none");
        }
    })

    if(typeof coForm_FormWizardParams == "undefined") {
        var coForm_FormWizardParams = <?php echo json_encode($params); ?>;
    }

    if(typeof inputObliList == "undefined") {
        var inputObliList = <?php echo json_encode($requiredInputs); ?>
    }

    if(typeof stepOblList == "undefined") {
        var stepOblList = <?php echo json_encode($stepOblList); ?>
    }

    let newRequiredInputs = <?php echo json_encode($requiredInputs); ?>;
    if(notEmpty(newRequiredInputs)) {
        inputObliList = {...inputObliList, ...newRequiredInputs}
    }

    if(typeof inputsList == "undefined") {
        var inputsList = {}
        $.each( coForm_FormWizardParams.steps , function (key, index) {
            if(typeof index.inputs != "undefined") {
                inputsList[index["step"]] = index.inputs;
            }
        });
    }
    if(typeof viewMode == "undefined") {
        var viewMode = <?php echo json_encode($mode); ?>;
    }
    viewMode = <?php echo json_encode($mode); ?>;

    if(typeof ownAnswer == 'undefined') {
        var ownAnswer = {};
    }
    if(typeof answerFormKey == "undefined")
        var answerFormKey = <?php echo isset($step["step"]) ? json_encode($step["step"]) : json_encode(null); ?>;
    answerFormKey = <?php echo isset($step["step"]) ? json_encode($step["step"]) : json_encode(null); ?>;
    var formKey = <?php echo isset($step["step"]) ? json_encode($step["step"]) : json_encode(null); ?>;
    var $step_id = <?php echo isset($step["_id"]) ? json_encode($step["_id"]) : json_encode(null); ?>;
    ownAnswer[formKey] = <?php echo isset($answer['answers'][$step["step"]]) ? json_encode($answer['answers'][$step["step"]]) : json_encode(array()) ?>;

    if(typeof stepId == "undefined") {
        var stepId = <?php echo json_encode(isset($step["step"]) ? $step["step"] : null) ?>
    }

    if(typeof isAjaxRequest == "undefined") {
        var isAjaxRequest = <?php echo json_encode($isajax) ?>
    }
    isAjaxRequest = <?php echo json_encode($isajax) ?>

    if(typeof buildScrollValidation == "undefined") {
        function buildScrollValidation(requiredInputs = {}, subFormId) {
            var showMsg = true;
            if(Object.keys(requiredInputs).length > 0) {
                for([inKey, inVal] of Object.entries(requiredInputs)) {
                    if($(`div#question_${inKey}.coforminput`).length > 0 && $(`div#question_${inKey}.coforminput`).is(":visible") && viewMode != 'r' && viewMode != 'pdf' && viewMode != 'fa') {
                        if(
                            inVal["type"] &&
                            viewMode != 'fa' && viewMode != 'r' && viewMode != 'pdf' &&
                            (showMsg == 'true' || showMsg == true)
                        ) {
                            if(document.querySelectorAll(`div#question_${inKey}.coforminput`).length > 0) {
                                var elem = $(`div#question_${inKey}.coforminput`);
                                if(checkVisible(document.querySelectorAll(`div#question_${inKey}.coforminput`)[0]) == true) {
                                    var inputKey = "";
                                    const inputTypeArr = inVal['type'].split('.');
                                    inputKey = inputTypeArr[inputTypeArr.length -1] ? inputTypeArr[inputTypeArr.length -1] : 'none';
                                    if(ownAnswer && ownAnswer[subFormId] && (ownAnswer[subFormId][inKey] || ownAnswer[subFormId][inputKey+inKey])) {
                                        if(ownAnswer[subFormId][inKey]) {
                                            if(notEmpty(ownAnswer[subFormId][inKey])) {
                                                removeError(
                                                    elem,
                                                    {
                                                        'border' : 'none',
                                                    },
                                                    'error-msg'
                                                )
                                            } else {
                                                addError(
                                                    elem,
                                                    {
                                                        'border' : '1px solid #a94442'
                                                    },
                                                    'error-msg'
                                                )
                                            }
                                        }
                                        if(ownAnswer[subFormId][inputKey+inKey]) {
                                            if(notEmpty(ownAnswer[subFormId][inputKey+inKey])) {
                                                removeError(
                                                    elem,
                                                    {
                                                        'border' : 'none',
                                                    },
                                                    'error-msg'
                                                )
                                            } else {
                                                addError(
                                                    elem,
                                                    {
                                                        'border' : '1px solid #a94442'
                                                    },
                                                    'error-msg'
                                                )
                                            }
                                        }
                                    } else {
                                        addError(
                                            elem,
                                            {
                                                'border' : '1px solid #a94442'
                                            },
                                            'error-msg'
                                        )
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if(typeof tocItems == 'undefined') {
        var tocItems;
    }

    if(typeof drawPathSection == 'undefined') {
        function drawPathSection(navContainer) {
            tocItems = [].slice.call( $(navContainer+' li' ) );

            // Cache element references and measurements
            tocItems = tocItems.map( function( item ) {
                var anchor = $(item).find( 'a' );
                var target = document.getElementById(anchor.attr( 'data-target' ).slice( 1 ));

                return {
                    listItem: item,
                    anchor: anchor,
                    target: target
                };
            } );

            // Remove missing targets
            tocItems = tocItems.filter( function( item ) {
                return !!item.target;
            } );

            // sync();

        }
    }

    if(typeof formInputRules == 'undefined') {
        var formInputRules = {}
    }

    if(typeof buildValidationGlobal == 'undefined') {
        function buildValidationGlobal(subFormId = '') {
            var showMsg = true;
            if(!formInputRules['rules'+subFormId+$step_id])
                formInputRules['rules'+subFormId+$step_id] = [];
            // if(stepValidationInput[subFormId]['p']['parentForm'] && stepValidationInput[subFormId]['p']['parentForm']['params'] && stepValidationInput[subFormId]['p']['parentForm']['params'][$step_id] && stepValidationInput[subFormId]['p']['parentForm']['params'][$step_id]['showMsg']) {
            //     showMsg = stepValidationInput[subFormId]['p']['parentForm']['params'][$step_id]['showMsg'];
            // }
            // formAllInputs['inputs'+$step_id] = formInputs;
            if(Object.keys(inputObliList).length > 0) {
                for([inKey, inVal] of Object.entries(inputObliList)) {
                    formInputRules['rules'+subFormId+$step_id].push(inVal);
                    let eventRulesInterval = setInterval(function() {
                        if($(`div#question_${inKey}.coforminput`).length > 0 && $(`div#question_${inKey}.coforminput`).is(":visible") && viewMode != 'r' && viewMode != 'pdf' && viewMode != 'fa') {
                            if($(`div#question_${inKey}.coforminput textarea`).length > 0) {
                                var textareaInterval = setInterval(() => {
                                    $(`div#question_${inKey}.coforminput textarea`).on('blur', function(e) {
                                        if($(this).val().trim() == '' || $(this).val().trim() == null) {
                                            // $(`small#${$step_id}_${inVal}Error`).remove();
                                            $(`div#question_${inKey}.coforminput`).find('.CodeMirror.cm-s-paper.CodeMirror-wrap').css('border-color', '#a94442');
                                            /* $(`li#question${inVal}`).find('.CodeMirror.cm-s-paper.CodeMirror-wrap').after(`
                                                <small id="${$step_id}_${inVal}Error" class="text-danger">Champ obligatoire</small>
                                            `) */
                                        } else {
                                            var elem = $(`div#question_${inKey}.coforminput`);
                                            // $(`small#${$step_id}_${inVal}Error`).remove();
                                            removeError(
                                                elem,
                                                {
                                                    'border' : 'none',
                                                },
                                                'error-msg'
                                            );
                                            $(`div#question_${inKey}.coforminput`).find('.CodeMirror.cm-s-paper.CodeMirror-wrap').css('border-color', '#ddd');
                                        }
                                        updateWizardBtn(coForm_FormWizardParams , null , null);
                                    });
                                    $(`div#question_${inKey}.coforminput textarea`).on('keyup', function(e) {
                                        if($(this).val().trim() != '' || $(this).val().trim() != null) {
                                            var elem = $(`div#question_${inKey}.coforminput`);
                                            // $(`small#${$step_id}_${inVal}Error`).remove();
                                            removeError(
                                                elem,
                                                {
                                                    'border' : 'none',
                                                },
                                                'error-msg'
                                            );
                                            $(`div#question_${inKey}.coforminput`).find('.CodeMirror.cm-s-paper.CodeMirror-wrap').css('border-color', '#ddd');
                                        }
                                        updateWizardBtn(coForm_FormWizardParams , null , null);
                                    })
                                    clearInterval(textareaInterval);
                                    textareaInterval = null;
                                }, 1000);
                            }
                            if($(`div#question_${inKey}.coforminput input`).length > 0 &&
                                inVal["type"] &&
                                (
                                    inVal["type"] != 'tpls.forms.uploader' &&
                                    inVal["type"] != 'tpls.forms.cplx.address'
                                )
                            ) {
                                // var blurInterval = setInterval(() => {
                                    $(`div#question_${inKey}.coforminput input`).on('blur', function(e) {
                                        if($(this).val().trim() == '' || $(this).val().trim() == null) {
                                            // $(`small#${$step_id}_${inVal}Error`).remove();
                                            $(this).css('border-color', '#a94442')
                                            /* $(this).after(`
                                                <small id="${$step_id}_${inVal}Error" class="text-danger">Champ obligatoire</small>
                                            `) */
                                        } else {
                                            var elem = $(`div#question_${inKey}.coforminput`);
                                            // $(`small#${$step_id}_${inVal}Error`).remove();
                                            removeError(
                                                elem,
                                                {
                                                    'border' : 'none',
                                                },
                                                'error-msg'
                                            );
                                            var errorAttr = $(`div#question_${inKey}.coforminput`).find(`.rules-container${inKey}`).attr('data-rules');
                                            if(typeof errorAttr != 'undefined') {
                                                var rulesAttr = JSON.parse(errorAttr.replace(/'/g, '"'));
                                                if(
                                                    ((typeof rulesAttr == 'object' && rulesAttr.width && rulesAttr.width.min && rulesAttr.width.min > 0) || (typeof rulesAttr == 'object' && rulesAttr.width && rulesAttr.width.max && rulesAttr.width.max > 0)) ||
                                                    (typeof rulesAttr == 'object' && rulesAttr.syntaxe)
                                                ) {

                                                } else {
                                                    $(this).css('border-color', '#ddd')
                                                }
                                            } else
                                                $(this).css('border-color', '#ddd')

                                        }
                                        updateWizardBtn(coForm_FormWizardParams , null , null);
                                    })
                                    $(`div#question_${inKey}.coforminput input`).on('keyup', function(e) {
                                        if($(this).val().trim() != '' || $(this).val().trim() != null) {
                                            var elem = $(`div#question_${inKey}.coforminput`);
                                            // $(`small#${$step_id}_${inVal}Error`).remove();
                                            removeError(
                                                elem,
                                                {
                                                    'border' : 'none',
                                                },
                                                'error-msg'
                                            );
                                            $(this).css('border-color', '#ddd')
                                        }
                                        updateWizardBtn(coForm_FormWizardParams , null , null);
                                    })
                                    // clearInterval(blurInterval);
                                    // blurInterval = null
                                // }, 1000);
                            }
                            clearInterval(eventRulesInterval)
                            eventRulesInterval = null
                        }
                    }, 700)
                }
            }
        }
        function bindCoformChartEvent() {
            $(".switch-to-answer").off("click").on("click", function () {
                const $thisBtn = $(this)
                const parentContainerSelector = $thisBtn.attr("data-parentcontainer")
                const toHideElemSelector = $thisBtn.attr("data-chartcontainer")
                if (parentContainerSelector && toHideElemSelector) {
                    $(parentContainerSelector).children(toHideElemSelector).fadeOut(400).siblings().fadeIn(400);
                }
            })
        }
    }

    jQuery(document).ready(function() {
        $('.tooltips').tooltip();
        var activeInterval = setInterval(() => {
            syncSection()
            drawPathSection(".coform-nav");
            clearInterval(activeInterval)
            activeInterval = null;
        }, 700);

        $.each($(".markdown"), function(k,v){
            descHtml = dataHelper.markdownToHtml($(v).html());
            $(v).html(descHtml);
        });

        $('.sectionScroll').click(function(){
            var tthis = $(this);
            if ($("#dialogContent .sectionScroll").length > 0) {
                const toSrollTop = $(tthis.data('target')).offset().top - $(tthis.data('target')).height() - (($(window).height() - $(tthis.data('target')).outerHeight(true)) / 3) + $('#dialogContent .modal-content').scrollTop() - window.scrollY;
                $('#dialogContent .modal-content').animate({
                    scrollTop: toSrollTop
                }, 500);
            } else {
                $('html, body').animate({
                    scrollTop: $(tthis.data('target')).offset().top - $(tthis.data('target')).height()
                }, 500);
            }
            tthis.parent().parent().parent().parent().hide();
            return false;
        });

        buildValidationGlobal(formKey)

        $(".addQuestion").off("click").on("click",function() {
            var activeForm = {
                "jsonSchema" : {
                    "title" : tradForm.addQuestion,
                    "type" : "object",
                    "properties" : {
                        label : {
                            label : tradForm.titleOfQuestion,
                            rules : {
                                required : true
                            }
                        },
                        placeholder : { label : tradForm.titleInQuestion },
                        info : {
                            inputType : "textarea",
                            label : tradForm.additionalInfoQuestion,
                            markdown : true
                        },
                        type : { label : tradForm.typeOfQuestion,
                            inputType : "select",
                            options : <?php echo json_encode(Form::inputTypes()); ?>,
                            value : "text"
                        }
                    }
                }
            };
            const formIdKey = $(this).data("form");
            const idSubForm = $(this).data("id");
            if (typeof tplCtx == "undefined" || (typeof tplCtx != "undefined" && tplCtx == null))
                var tplCtx = {};
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.form = $(this).data("form");
            tplCtx.formParentId = $(this).data("formparentid");
            tplCtx.updatePartial = true;

            activeForm.jsonSchema.save = function () {
                var inputCt = Date.now().toString(36) + Math.random().toString(36).substring(2);
                const inputKey = formIdKey+inputCt;

                if(inputsList[tplCtx.form] && notNull(inputsList[tplCtx.form][tplCtx.form+inputCt]))
                    inputCt = inputCt+"x";

                tplCtx.path = "inputs."+tplCtx.form+inputCt;
                tplCtx.inputId = tplCtx.form+inputCt;
                tplCtx.value = {
                    label : $("#label").val(),
                    type : $("#type").val()
                };
                if( $("#placeholder").val() != "" )
                    tplCtx.value.placeholder = $("#placeholder").val();
                if( $("#info").val() != "" )
                    tplCtx.value.info = $("#info").val();


                mylog.log("activeForm save tplCtx",tplCtx);
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    if (!inputsList[tplCtx.form]) {
                        $(".DnDPlaceholder").remove()
                    }
                    delete tplCtx.form;
                    $("#questionList"+formIdKey).append(`
                        <div id="question_${formIdKey}${inputCt}" class="coforminput questionBlock col-xs-12 ui-corner-all no-padding questionBlock" data-id="${idSubForm}" data-form="${formIdKey}" data-path="${ coForm_FormWizardParams.isAap == true || coForm_FormWizardParams.isAap == 'true' ? "inputs" : "forms" }.${inputKey}.position" data-key="${inputKey}" draggable="false" role="option" aria-grabbed="false"></div>
                    `)
                    dataHelper.path2Value( tplCtx, function(params) {
                        if(tplCtx.value.type == "tpls.forms.cplx.openDynform" ){
                            delete tplCtx.path;
                            delete tplCtx.id;
                            tplCtx.value = {
                                id : tplCtx.inputId,
                                name : tplCtx.value.label,
                                type : "dynform"
                            };
                            mylog.log("create openDynform save tplCtx",tplCtx);
                            dataHelper.path2Value( tplCtx, function(params) {
                                dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
                                // reloadWizard();
                                if(typeof reloadInput != "undefined" && typeof stepObj != "undefined") {
                                    reloadInput(formIdKey+inputCt, formIdKey, () => stepObj.bindEvent.inputEvents(stepObj))
                                }
                            } );

                        } else {
                            dyFObj.closeForm()
                            $("#ajax-modal").modal('hide');
                            // reloadWizard();
                            if(typeof reloadInput != "undefined" && typeof stepObj != "undefined") {
                                reloadInput(
                                    formIdKey+inputCt,
                                    formIdKey,
                                    () => {
                                        stepObj.bindEvent.inputEvents(stepObj);
                                        // if(typeof bindDnDEvent != 'undefined')
                                        //     // bindDnDEvent()
                                    }
                                )
                            }
                        }
                        if(coForm_FormWizardParams.steps?.[idSubForm]?.inputs != undefined) {
                            coForm_FormWizardParams.steps[idSubForm].inputs[formIdKey+inputCt] = tplCtx.value
                        }
                    } );
                }

            }

            dyFObj.openForm( activeForm );
        });

        $(".btn-multiEval").off("click").on("click", function() {
            const thisCurrentStep = coForm_FormWizardParams?.step?.step ? coForm_FormWizardParams.step.step : "steprandom"
            const $thisBtn = $(this);
            const chartUrl = baseUrl + "/survey/common/publicanswer/request/get_multieval_view";
            $("#stepContainer"+thisCurrentStep).removeClass("hide").fadeIn(300);
            coInterface.showCostumLoader("#stepContainer"+thisCurrentStep + " .second-container-body");
            ajaxPost(
                "#stepContainer"+thisCurrentStep + " .second-container-body", 
                chartUrl,
                {
                    containerId: thisCurrentStep+"_multiEval",
                    dataChartBuilder : {
                        stepId: coForm_FormWizardParams?.step?._id.$id ? coForm_FormWizardParams.step._id.$id : thisCurrentStep,
                        stepKey: thisCurrentStep,
                        stepType: "inputs",
                        answerId: $thisBtn.attr("data-answerid"),
                        inputParamsId: $thisBtn.attr("data-input-paramsid"),
                    }
                },
                function () {

                }
            )
        });
        $(".switch-to-step").off("click").on("click", function() {
            const thisCurrentStep = coForm_FormWizardParams?.step?.step ? coForm_FormWizardParams.step.step : "steprandom"
            const targetStep = $(this).data("step");
            $("#stepContainer"+thisCurrentStep).fadeOut(300);
        })

        if(typeof eventScrollAlreadyCalled != 'undefined' && eventScrollAlreadyCalled == false && viewMode != 'r' && viewMode != 'pdf' && viewMode != 'fa') {
            eventScrollAlreadyCalled = true;
            $(window).add('.modal-content').on('resize scroll', function(e) {
                buildScrollValidation(inputObliList, formKey);
                updateWizardBtn(coForm_FormWizardParams , null , null);
            });
        }
    });
</script>
