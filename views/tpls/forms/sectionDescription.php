<style type="text/css">
	.sectionDescri p{
		font-size: 18px !important;
	}

	#menu-top-profil-social{
		display: none;
	}
	.contentHeaderInformation{
		display: none;
	}
</style>
<div class="col-xs-12">
    <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
		<?php echo $label.$editQuestionBtn; ?>
	</h4>
	<?php if(!empty($info)){ ?>
    	<p style="font-size: 18px !important" id="<?php echo $key ?>Help" class="form-text text-dark markdown sectionDescri"><?php echo $info ?></p>
    <?php } ?>
</div>
<?php if($mode != "pdf"){ ?>
<script type="text/javascript">
jQuery(document).ready(function() {
    $('.descSec<?= $key ?>').removeClass('editQuestion');

    sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "Configurer votre section",
				"description" : "Personnaliser votre section",
				"icon" : "cog",
				"properties" : {
						label : {
							label : "Titre",
							"inputType" : "text",
							value :  "<?php echo $label ?>"
						},
						info : {
							label : "<?= $label ?>",
							"inputType" : "textarea",
							"markdown" : true,
							value :  `<?php echo $info ?>`
						},
						type : {
							inputType : "hidden",
							value : "tpls.forms.sectionDescription"
						}
					},
			/*	beforeBuild : function(){
				
				},*/
				save : function () {  
					tplCtx.value = {};

					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
		                  dataHelper.path2Value( tplCtx, function(params) {
		                    dyFObj.commonAfterSave(params,function(){
		                      toastr.success("Élément bien ajouter");
		                      dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
		                      urlCtrl.loadByHash(location.hash);
		                    });
		                  } );
					}
				}
			}
		};
		$(".descSec<?= $key ?>").click(function() {  
			var key = $(this).data("key");
			tplCtx.id = "<?= @$form["_id"] ?>";
			tplCtx.key = "<?= $key ?>";
			tplCtx.collection = "<?= Form::COLLECTION ?>";            
			tplCtx.path = "inputs.<?= $key ?>";

			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
		});
});
/**/
</script>
<?php } ?>