<style>
    .select<?= $kunik ?>.select2-container {
        display: block !important;
    }
</style>
<?php 
$inpClass = " select".$kunik;

$paramsData = [
    "type" => "single",
    "options" => [],
    "groupsOptions" => [],
    "groups" => [],
];

if( isset($parentForm["params"][$key]) ) 
    $paramsData = array_merge($paramsData,$parentForm["params"][$key]);

    unset($paramsData["list"]);


if(!isset($options) && isset($parentForm["params"][$key]['options']))
    $options = $parentForm["params"][$key]['options'];

if(!isset($groupsOptions) && isset($paramsData['groupsOptions'])){
    $groupsOptions = $paramsData['groupsOptions'];
    $temp = [];
    foreach ($groupsOptions as $kgp => $vgp) {
        if(empty($temp[$vgp["group"]]))
            $temp[$vgp["group"]]=[];
        $temp[$vgp["group"]][] = $vgp["option"];
    }
    $groupsOptions = $temp;
}

$type = "single";
if(!empty($parentForm["params"][$key]['type']))  
    $type = $parentForm["params"][$key]['type'];
else
    $paramsData['type'] = $type;

if($mode == "r" || $mode == "pdf"){ ?>
    <div class="col-xs-12 no-padding" id="<?php echo $kunik ?>">
        <label for="<?php echo $kunik ?>">
            <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label ?></h4>
        </label><br/>
        <?php echo (!empty($options) && !empty($options[$answers])) ? $options[$answers] : "" ; ?>
    </div>
<?php 
}else{
    $editParamsBtn = ($canEditForm) ? " <a href='javascript:;' data-id='".$parentForm["_id"]."' data-collection='".Form::COLLECTION."' data-path='params.".$key."' class='previewTpl edit".$key."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";
?>

    <div class="form-group col-xs-12 col-md-12 no-padding">
        <label for="<?php echo $key ?>">
            <h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>">
                <?php echo $label.$editQuestionBtn.$editParamsBtn ?>
            </h4>
        </label><br>

        <?php if( !isset($options) && ($type == "single" || $type == "multiple")) 
            echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> THIS FIELD HAS TO BE CONFIGURED FIRST ".$editParamsBtn."</span>"; 
            elseif($type == "single" || $type == "multiple") { 
                ?>
                <select class="select2Input <?= $inpClass ?>" id="<?php echo $key ?>" data-form='<?php echo $form["id"] ?>' required <?= $type=="multiple"? "multiple" : "" ?>>
                    <option value="">Selectionner</option>
                    <?php foreach ($options as $k => $v) {
                        echo '<option value="'.$v.'" '.((!empty($answers) && (string)$k == $answers ) ? "selected" : "").' >'.$v.'</option>';
                    } ?>
                </select>
        
        <?php }  ?>

        <?php if( !isset($groupsOptions) && ($type == "singleGrouped" || $type == "multipleGrouped")) 
            echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> THIS FIELD HAS TO BE CONFIGURED FIRST ".$editParamsBtn."</span>"; 
            elseif($type == "singleGrouped" || $type == "multipleGrouped") { ?>
                <select class="select2Input <?php echo $inpClass ?>" id="<?php echo $key ?>" data-form='<?php echo $form["id"] ?>' required <?= $type=="multipleGrouped"? "multiple" : "" ?>>
                    <?php foreach ($groupsOptions as $kgo => $vgo) { ?>    
                        <optgroup label="<?= $kgo ?>">
                        <?php foreach ($vgo as $koption => $voption) { ?>   
                            <option value="<?= $voption ?>" <?= (is_array($answers) && in_array($voption,$answers)) ? "selected" : "" ?>><?= $voption ?></option>
                        <?php } ?>       
                        </optgroup>
                    <?php } ?>                    
                </select>
        <?php }  ?>
    </div>
<?php } ?>
<?php if($mode != "pdf"){ ?>
<script type="text/javascript">
sectionDyf.<?php echo $key ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
jQuery(document).ready(function() {
    mylog.log("render form input","/modules/costum/views/tpls/forms/select.php");
    $(".select<?= $kunik ?>").select2().change(function(e){
        var select = $(this);
        var optGroupLabelArray = [];
        var params = {
            id : "<?= (string) $answer["_id"] ?>",
            collection: "<?= Form::ANSWER_COLLECTION ?>",
            path : "answers.<?= $form["id"] ?>.<?= $key ?>",
            value :  select.val()
        }
        dataHelper.path2Value(params, function (data) {
            if(data.result)
                toastr.success(trad.saved);
                if("aap" == "<?= $parentForm["type"] ?>" && ("<?= $paramsData["type"] ?>" == "multipleGrouped" ) && costum.contextSlug == 'coSindniSmarterre'){
                    var ans = data.elt.answers.<?= $form["id"] ?>.<?= $key ?>;
                    var groupOption = <?= json_encode($groupsOptions) ?>;
                    var pilier = [];
                    const keyOfInput = "principalDomaine";
                    $.each(groupOption,function(k,v){
                        if (typeof ans != "undefined") {
                            const filteredArray = ans.filter(value => v.includes(value));
                            if (filteredArray.length != 0)
                                pilier.push(k);
                        }
                    })
                    pilier = [...new Set(pilier)]
                    
                    var prms = {
                        id : "<?= (string) $answer["_id"] ?>",
                        collection: "<?= Form::ANSWER_COLLECTION ?>",
                        path : "answers.<?= $form["id"] ?>."+keyOfInput,
                        value :  pilier
                    }
                    dataHelper.path2Value(prms, function () {
                        //location.reload();
                        if(typeof newReloadStepValidationInputGlobal != "undefined") {
                            newReloadStepValidationInputGlobal({
                                inputKey : keyOfInput,
                                inputType : "tpls.forms.tagsFix"
                            })
                        }
                    })
                }
        })
    });
    sectionDyf.<?php echo $key ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo $key ?> config",
            "description" : "Liste de question possible",
            "icon" : "fa-cog",
            "properties" : {
                type : {
                    inputType : "select",
                    label : "Type",
                    options : {
                        single :"unique",
                        multiple :"multiple",
                        singleGrouped :"unique groupé",
                        multipleGrouped :"multiple groupé",
                    }
                },
                options : {
                    inputType : "array",
                    label : "Liste des titres",
                },
                groups : {
                    inputType : "array",
                    label : "Groupes",
                },
                groupsOptions :{
                    inputType: "lists",
                    label: "Groupes et options",
                    "entries":{
                        "group":{
                            "type":"select",
                            "label" : "groupe",
                            "class":"col-md-6 col-sm-6 col-xs-11",
                            options : <?= json_encode($paramsData["groups"]) ?>
                        },
                        "option":{
                            "label":"Options",
                            "type":"text",
                            "class":"col-md-5 col-sm-5 col-xs-11"
                        }
                    }
                },
            },
            onLoads : {
                onload : function(data){
                    var toogle = function(){
                        if($('.typeselect #type').val() == "single" || $('.typeselect #type').val() == "multiple"){
                            $('.groupsarray,.groupsOptionslists').hide();
                            $('.optionsarray').show();
                        }else{
                            $('.groupsarray,.groupsOptionslists').show();
                            $('.optionsarray').hide();
                        }
                    }
                    toogle();
                    $('.typeselect #type').change(function(){
                        toogle();
                    })


                    $(document).on('mouseenter',"#listContainergroupsOptions [data-entry='group']",function(){
                        var optHtml = "";
                        $.each(getArray('.groupsarray'),function(k,v){
                            optHtml+= `<option value="${v}">${v}</option>`;
                        })

                        if($(this).children().length != getArray('.groupsarray').length){
                            $(this).html(optHtml);
                        }
                    })
                }
            },
            save : function (formData) {  
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $key ?>Params.jsonSchema.properties , function(k,val) { 
                    if(val.inputType == "array"){
                        tplCtx.value[k] = getArray('.'+k+val.inputType);
                        if($("#type").val()=="single" || $("#type").val()=="multiple")
                            tplCtx.value["list"] = getArray('.'+k+val.inputType);
                    }else if(k == "groupsOptions"){
                        tplCtx.value[k] = formData[k];
                        if($("#type").val()=="singleGrouped" || $("#type").val()=="multipleGrouped"){
                            var list = [];
                            $.each(formData[k],function(i,j){
                                list.push(j["option"])
                            })
                            tplCtx.value["list"] = list;
                        }
                    }else
                        tplCtx.value[k] = $("#"+k).val();
                });
                mylog.log("save tplCtx",tplCtx);
                
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) {
                        if("aap" == "<?= $parentForm["type"] ?>" && $('.typeselect #type').val() == "multipleGrouped" && costum.contextSlug == 'coSindniSmarterre'){
                            var prms = {
                                id : "<?= (string) $parentForm["_id"] ?>",
                                collection: "<?= Form::COLLECTION ?>",
                                value :  getArray('.groupsarray')
                            }
                            dataHelper.path2Value({...prms,path : "params.principalDomaine.list"}, function () {
                                dataHelper.path2Value({...prms,path : "params.principalDomaine.options"}, function () {
                                    dataHelper.path2Value({...prms,value:"multiple",path : "params.principalDomaine.type"}, function () {
                                        location.reload();
                                    })
                                })
                            })
                        }else{
                            dyFObj.closeForm(); //$("#ajax-modal").modal('hide');
                            location.reload();
                        }
                    } );
                }

            }
        }
    };

    $(".edit<?php echo $key ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $key ?>Params,null, sectionDyf.<?php echo $key ?>ParamsData);
    });
});
</script>
<?php } ?>